DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer ft
		where ft.file_transfer_type_cd = 15
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_transfer_name in ('KIOSK-DEFAULT-CFG', 'PUBLICPC-DEFAULT-CFG')
		order by ft.file_transfer_name;	
	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_setting_count NUMBER;
BEGIN
	FOR rec_cur IN cur LOOP					
		INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
		SELECT rec_cur.file_transfer_name, DECODE(rec_cur.file_transfer_name, 'KIOSK-DEFAULT-CFG', 1, 2), 11 FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
		IF SQL%ROWCOUNT > 0 THEN		
			SELECT CONFIG_TEMPLATE_ID 
			INTO ln_config_template_id
			FROM DEVICE.CONFIG_TEMPLATE 
			WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
		
			PKG_DEVICE_CONFIGURATION.sp_update_cfg_tmpl_settings(rec_cur.file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
			
			COMMIT;
		END IF;
	END LOOP;
END;
/

DROP FUNCTION DEVICE.FILE_CONTENT_LENGTH;
DROP PUBLIC SYNONYM FILE_CONTENT_LENGTH;
