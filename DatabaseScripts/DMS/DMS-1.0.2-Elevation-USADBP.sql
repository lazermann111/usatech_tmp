WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DEFAULT-CFG-13-9.sql?rev=HEAD
DECLARE
	ln_property_list_version NUMBER := 9;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_file_transfer_type_cd file_transfer_type.file_transfer_type_cd%TYPE := 22;
	lv_file_transfer_name file_transfer.file_transfer_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_file_transfer_id file_transfer.file_transfer_id%TYPE;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_setting_count NUMBER;
	lv_db_name VARCHAR2(255);
BEGIN
	SELECT MAX(file_transfer_id) INTO ln_file_transfer_id
	FROM device.file_transfer
	WHERE file_transfer_name = lv_file_transfer_name;
	
	IF ln_file_transfer_id IS NULL THEN
		INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
		VALUES(ln_file_transfer_type_cd, lv_file_transfer_name);
		
		SELECT MAX(file_transfer_id) INTO ln_file_transfer_id
		FROM device.file_transfer
		WHERE file_transfer_name = lv_file_transfer_name;
	END IF;
	
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '') INTO lv_db_name FROM GLOBAL_NAME;
	
	UPDATE DEVICE.FILE_TRANSFER
	SET FILE_TRANSFER_TYPE_CD = ln_file_transfer_type_cd,
		FILE_TRANSFER_CONTENT = RAWTOHEX(
		'0=' || DECODE(lv_db_name, 'USADBP', 'a1.eport-usatech.net', 'USASTT01', 'a1.ecc.eport-usatech.net', 'USADEV02', 'a1.int.eport-usatech.net', 'a1.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'1=' || DECODE(lv_db_name, 'USADBP', 'a2.eport-usatech.net', 'USASTT01', 'a2.ecc.eport-usatech.net', 'USADEV02', 'a2.int.eport-usatech.net', 'a2.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'2=' || DECODE(lv_db_name, 'USADBP', '192.168.79.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'3=' || DECODE(lv_db_name, 'USADBP', '208.116.216.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'4=14109' || PKG_CONST.ASCII__LF ||
		'5=14109' || PKG_CONST.ASCII__LF ||
		'6=14109' || PKG_CONST.ASCII__LF ||
		'7=14109' || PKG_CONST.ASCII__LF ||
		'10=15' || PKG_CONST.ASCII__LF ||
		'20=' || DECODE(lv_db_name, 'USADBP', 'b1.eport-usatech.net', 'USASTT01', 'b1.ecc.eport-usatech.net', 'USADEV02', 'b1.int.eport-usatech.net', 'b1.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'21=' || DECODE(lv_db_name, 'USADBP', 'b2.eport-usatech.net', 'USASTT01', 'b2.ecc.eport-usatech.net', 'USADEV02', 'b2.int.eport-usatech.net', 'b2.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'22=' || DECODE(lv_db_name, 'USADBP', '192.168.79.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'23=' || DECODE(lv_db_name, 'USADBP', '208.116.216.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'24=14108' || PKG_CONST.ASCII__LF ||
		'25=14108' || PKG_CONST.ASCII__LF ||
		'26=14108' || PKG_CONST.ASCII__LF ||
		'27=14108' || PKG_CONST.ASCII__LF ||
		'30=20' || PKG_CONST.ASCII__LF ||
		'31=50' || PKG_CONST.ASCII__LF ||
		'32=2' || PKG_CONST.ASCII__LF ||
		'33=27' || PKG_CONST.ASCII__LF ||
		'70=0' || PKG_CONST.ASCII__LF ||
		'71=' || PKG_CONST.ASCII__LF ||
		'85=W^0200^0' || PKG_CONST.ASCII__LF ||
		'86=I^7200^21600' || PKG_CONST.ASCII__LF ||
		'87=D^0300' || PKG_CONST.ASCII__LF ||
		'215=20' || PKG_CONST.ASCII__LF ||
		'1001=10' || PKG_CONST.ASCII__LF ||
		'1002=100' || PKG_CONST.ASCII__LF ||
		'1003=1' || PKG_CONST.ASCII__LF ||
		'1004=0' || PKG_CONST.ASCII__LF ||
		'1005=10' || PKG_CONST.ASCII__LF ||
		'1006=4' || PKG_CONST.ASCII__LF ||
		'1007=20' || PKG_CONST.ASCII__LF ||
		'1008=3' || PKG_CONST.ASCII__LF ||
		'1009=2' || PKG_CONST.ASCII__LF ||
		'1010=12' || PKG_CONST.ASCII__LF ||
		'1011=8' || PKG_CONST.ASCII__LF ||
		'1012=16' || PKG_CONST.ASCII__LF ||
		'1013=0' || PKG_CONST.ASCII__LF ||
		'1014=0' || PKG_CONST.ASCII__LF ||
		'1015=0' || PKG_CONST.ASCII__LF ||
		'1016=0' || PKG_CONST.ASCII__LF ||
		'1017=0' || PKG_CONST.ASCII__LF ||
		'1018=20' || PKG_CONST.ASCII__LF ||
		'1019=15' || PKG_CONST.ASCII__LF ||
		'1020=5' || PKG_CONST.ASCII__LF ||
		'1021=300' || PKG_CONST.ASCII__LF ||
		'1022=0' || PKG_CONST.ASCII__LF ||
		'1023=1' || PKG_CONST.ASCII__LF ||
		'1024=0' || PKG_CONST.ASCII__LF ||
		'1025=360' || PKG_CONST.ASCII__LF ||
		'1101=' || PKG_CONST.ASCII__LF ||
		'1102=2' || PKG_CONST.ASCII__LF ||
		'1103=0' || PKG_CONST.ASCII__LF ||
		'1104=1' || PKG_CONST.ASCII__LF ||
		'1106=' || PKG_CONST.ASCII__LF ||
		'1200=1000' || PKG_CONST.ASCII__LF ||
		'1201=1' || PKG_CONST.ASCII__LF ||
		'1202=0' || PKG_CONST.ASCII__LF ||
		'1300=60' || PKG_CONST.ASCII__LF ||
		'1400=100' || PKG_CONST.ASCII__LF ||
		'1401=17' || PKG_CONST.ASCII__LF ||
		'1402=3' || PKG_CONST.ASCII__LF ||
		'1403=Hotel Business Express Center' || PKG_CONST.ASCII__LF ||
		'1404=' || PKG_CONST.ASCII__LF ||
		'1405=---Thank You---' || PKG_CONST.ASCII__LF ||
		'1406=Inquiries 1 888 561 4748' || PKG_CONST.ASCII__LF ||
		'1407=25' || PKG_CONST.ASCII__LF ||
		'1408=50' || PKG_CONST.ASCII__LF ||
		'1409=100' || PKG_CONST.ASCII__LF ||
		'1410=100' || PKG_CONST.ASCII__LF ||
		'1411=100' || PKG_CONST.ASCII__LF ||
		'1412=0' || PKG_CONST.ASCII__LF ||
		'1413=0' || PKG_CONST.ASCII__LF ||
		'1414=0' || PKG_CONST.ASCII__LF ||
		'1415=2' || PKG_CONST.ASCII__LF ||
		'1500=1' || PKG_CONST.ASCII__LF ||
		'1501=15' || PKG_CONST.ASCII__LF ||
		'1502=25' || PKG_CONST.ASCII__LF ||
		'1503=25' || PKG_CONST.ASCII__LF ||
		'1504=20' || PKG_CONST.ASCII__LF ||
		'1505=2' || PKG_CONST.ASCII__LF ||
		'1506=25' || PKG_CONST.ASCII__LF ||
		'1507=' || PKG_CONST.ASCII__LF ||
		'1508=0' || PKG_CONST.ASCII__LF ||
		'1509=' || PKG_CONST.ASCII__LF ||
		'1510=0' || PKG_CONST.ASCII__LF ||
		'1511=' || PKG_CONST.ASCII__LF ||
		'1512=0' || PKG_CONST.ASCII__LF ||
		'1513=' || PKG_CONST.ASCII__LF ||
		'1514=0' || PKG_CONST.ASCII__LF ||
		'1515=' || PKG_CONST.ASCII__LF ||
		'1516=0' || PKG_CONST.ASCII__LF ||
		'1517=' || PKG_CONST.ASCII__LF ||
		'1518=0' || PKG_CONST.ASCII__LF ||
		'1519=' || PKG_CONST.ASCII__LF ||
		'1520=0' || PKG_CONST.ASCII__LF ||
		'1521=' || PKG_CONST.ASCII__LF ||
		'1522=30' || PKG_CONST.ASCII__LF ||
		'1523=100' || PKG_CONST.ASCII__LF ||
		'1524=2' || PKG_CONST.ASCII__LF ||
		'1525=10' || PKG_CONST.ASCII__LF ||
		'1526=25' || PKG_CONST.ASCII__LF
	) WHERE FILE_TRANSFER_ID = ln_file_transfer_id;

	pkg_device_configuration.sp_update_cfg_tmpl_settings(ln_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
		AND property_list_version = ln_property_list_version;
	
	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'0-7|10|20-27|30-33|50-52|60-64|70|71|80|81|85-87|100-108|200-208|215|300|301|1001-1025|1101-1106|1200-1202|1300|1400-1415|1500-1526',
		'0-7|10|20-27|30-33|70|71|85-87|215|1001-1025|1101-1106|1200-1202|1300|1400-1415|1500-1526');
		
	COMMIT;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.2-USADBP.sql?rev=HEAD
DECLARE
	lv_db VARCHAR2(30);
BEGIN
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '')
	INTO lv_db
	FROM GLOBAL_NAME;
	
	IF lv_db = 'USADBP' THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'usaapr31', '192.168.77.76' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1' AND HOST_NAME = 'usaapr31');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'usaapr32', '192.168.77.77' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2' AND HOST_NAME = 'usaapr32');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_3', 'usaapr33', '192.168.77.78' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_3' AND HOST_NAME = 'usaapr33');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_4', 'usaapr34', '192.168.77.79' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_4' AND HOST_NAME = 'usaapr34');
	ELSIF lv_db = 'USASTT01' THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'eccapr11', '192.168.4.43' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1' AND HOST_NAME = 'eccapr11');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'eccapr12', '192.168.4.44' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2' AND HOST_NAME = 'eccapr12');
	ELSIF lv_db = 'USADEV02' THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'intapr11', '10.0.0.249' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1' AND HOST_NAME = 'intapr11');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'intapr12', '10.0.0.250' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2' AND HOST_NAME = 'intapr12');
	ELSIF lv_db IN ('USADEV03', 'USADEV04') THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'devapr11', '10.0.0.214' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1' AND HOST_NAME = 'devapr11');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'devapr12', '10.0.0.216' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2' AND HOST_NAME = 'devapr12');
	END IF;
	
	COMMIT;
END;
/

GRANT SELECT ON DEVICE.SETTING_PARAMETER_TYPE TO USAT_DMS_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE ON DEVICE.SETTING_PARAMETER_TYPE TO USATECH_UPD_TRANS;

INSERT INTO DEVICE.SETTING_PARAMETER_TYPE(SETTING_PARAMETER_TYPE_DESC, SHOW_IN_UI_FLAG, EDIT_IN_UI_FLAG, SEND_TO_DEVICE_FLAG)
SELECT 'SERVER_ONLY_DEVICE_SETTING', 'Y', 'Y', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.SETTING_PARAMETER_TYPE WHERE SETTING_PARAMETER_TYPE_DESC = 'SERVER_ONLY_DEVICE_SETTING');

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_START', 'Call-in Time Window Start', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START');
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_HOURS', 'Call-in Time Window Hours', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS');

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'Call-in Time Window Start - Activated', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_ACTIVATED');
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', 'Call-in Time Window Hours - Activated', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED');

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'Call-in Time Window Start - Non-activated', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED');
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', 'Call-in Time Window Hours - Non-activated', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED');

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'Call-in Time Window Start - Settlement', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_SETTLEMENT');
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', 'Call-in Time Window Hours - Settlement', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT');

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_START_DEX', 'Call-in Time Window Start - DEX', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_DEX');
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME, DEVICE_SETTING_UI_CONFIGURABLE)
SELECT 'CALL_IN_TIME_WINDOW_HOURS_DEX', 'Call-in Time Window Hours - DEX', 'N' FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_DEX');

UPDATE DEVICE.DEVICE_SETTING_PARAMETER 
SET SETTING_PARAMETER_TYPE_ID = (
	SELECT SETTING_PARAMETER_TYPE_ID 
	FROM DEVICE.SETTING_PARAMETER_TYPE 
	WHERE SETTING_PARAMETER_TYPE_DESC = 'SERVER_ONLY_DEVICE_SETTING'
)
WHERE DEVICE_SETTING_PARAMETER_CD LIKE 'CALL_IN_TIME_WINDOW_%'
	OR DEVICE_SETTING_PARAMETER_CD = 'FILL_CARD_CLEAR_TIME';

INSERT INTO DEVICE.CONFIG_TEMPLATE_CATEGORY(CONFIG_TEMPLATE_CATEGORY_ID, CONFIG_TEMPLATE_CATEGORY_NAME, CONFIG_TEMPLATE_CATEGORY_DESC)
SELECT 39, 'Server-Only Device Settings', 'Device settings stored and processed only on the server' FROM DUAL WHERE NOT EXISTS(
SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_CATEGORY WHERE CONFIG_TEMPLATE_CATEGORY_ID = 39);

INSERT INTO DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP(CONFIG_TEMPLATE_CATEGORY_ID, DEVICE_TYPE_ID, DISPLAY, CATEGORY_DISPLAY_ORDER)
SELECT 39, 13, 'N', 9 FROM DUAL WHERE NOT EXISTS(
SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP WHERE CONFIG_TEMPLATE_CATEGORY_ID = 39 AND DEVICE_TYPE_ID = 13);

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'FILL_CARD_CLEAR_TIME',NULL,NULL,'N','The server will instruct the device to display the previous counters for this period of time in seconds after the initial fill card swipe on subsequent fill card swipes','TEXT:0 to 5','','N',NULL,NULL,10000,NULL,NULL,0,10000,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'GX-GENERIC-MAP'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 0 AND DEVICE_SETTING_PARAMETER_CD = 'FILL_CARD_CLEAR_TIME');

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_START',NULL,NULL,'N','Start time of call-in time window in military format. Valid values: 0000 - 2359.','TEXT:0 to 4','','N',NULL,NULL,10010,NULL,NULL,0,10010,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'GX-GENERIC-MAP'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 0 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_HOURS',NULL,NULL,'N','Duration of call-in time window in hours. Valid values: 1 - 23.','TEXT:0 to 2','','N',NULL,NULL,10020,NULL,NULL,0,10020,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'GX-GENERIC-MAP'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 0 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS');

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'FILL_CARD_CLEAR_TIME',NULL,39,'N','The server will instruct the device to display the previous counters for this period of time in seconds after the initial fill card swipe on subsequent fill card swipes','TEXT:0 to 5','','N',NULL,NULL,NULL,NULL,NULL,13,20000,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'FILL_CARD_CLEAR_TIME');

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_START_ACTIVATED',NULL,39,'N','Start time of call-in time window in military format when device is activated. Valid values: 0000 - 2359.','TEXT:0 to 4','','N',NULL,NULL,NULL,NULL,NULL,13,20010,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_ACTIVATED');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED',NULL,39,'N','Duration of call-in time window in hours when device is activated. Valid values: 1 - 23.','TEXT:0 to 2','','N',NULL,NULL,NULL,NULL,NULL,13,20020,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED');

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED',NULL,39,'N','Start time of call-in time window in military format when device is not activated. Valid values: 0000 - 2359.','TEXT:0 to 4','','N',NULL,NULL,NULL,NULL,NULL,13,20030,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED',NULL,39,'N','Duration of call-in time window in hours when device is not activated. Valid values: 1 - 23.','TEXT:0 to 2','','N',NULL,NULL,NULL,NULL,NULL,13,20040,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED');

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_START_SETTLEMENT',NULL,39,'N','Start time of call-in time window in military format for generating and sending Settlement messages to the server. Valid values: 0000 - 2359.','TEXT:0 to 4','','N',NULL,NULL,NULL,NULL,NULL,13,20050,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_SETTLEMENT');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT',NULL,39,'N','Duration of call-in time window in hours for generating and sending Settlement messages to the server. Valid values: 1 - 23.','TEXT:0 to 2','','N',NULL,NULL,NULL,NULL,NULL,13,20060,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT');

INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_START_DEX',NULL,39,'N','Start time of DEX call-in time window in military format. Valid values: 0000 - 2359.','TEXT:0 to 4','','N',NULL,NULL,NULL,NULL,NULL,13,20070,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_START_DEX');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE) 
SELECT FILE_TRANSFER_ID, 'CALL_IN_TIME_WINDOW_HOURS_DEX',NULL,39,'N','Duration of DEX call-in time window in hours. Valid values: 1 - 23.','TEXT:0 to 2','','N',NULL,NULL,NULL,NULL,NULL,13,20080,NULL,NULL,NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = 'CALL_IN_TIME_WINDOW_HOURS_DEX');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET EDITOR = 'SELECT:1=1;2=2;3=3;4=4;5=5',
	DESCRIPTION = '1 - Standard MDB<br />2 - eTrans MDB<br />3 - Coin Pulse<br />4 - Coin Pulse Dual Enable<br />5 - Top-off Coin Pulse'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1500';

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD) SELECT '1522' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = '1522');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID,DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE)
SELECT FILE_TRANSFER_ID,'1522',NULL,4,'Y','Number of seconds associated with Coin Pulse Value [1503]. This tells Edge how long a Top-off session is running. For a unit with Coin Pulse Value [1503] of 25 and Coin Item Price #1 [1506] of 250, the time would be 300 seconds.','TEXT:1 to 5',NULL,'Y',NULL,NULL,NULL,NULL,0,13,23,'Pulse Time Value','A',NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1522');
	
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD) SELECT '1523' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = '1523');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID,DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE)
SELECT FILE_TRANSFER_ID,'1523',NULL,4,'Y','The equivalent monetary value that the consumer will be charged per each additional Card Swipe in pennies. If the VMC / connected equipment expect that each pulse is equal to 25 cents, then 4 pulses will be sent to the VMC and an additional 120 seconds will be added to the running Top-off time. Valid Values: 1 - 65535.','TEXT:1 to 5',NULL,'Y',NULL,NULL,NULL,NULL,0,13,24,'Top-off Value','A',NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1523');

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD) SELECT '1524' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = '1524');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID,DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE)
SELECT FILE_TRANSFER_ID,'1524',NULL,4,'Y','This property determines whether the Cash Input Pulse is active high or active low.<br />1 - Active Low<br />2 - Active High','SELECT:1=1;2=2',NULL,'Y',NULL,NULL,NULL,NULL,0,13,25,'Pulse Capture Polarity','A',NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1524');	

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD) SELECT '1525' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = '1525');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID,DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE)
SELECT FILE_TRANSFER_ID,'1525',NULL,4,'Y','Used to set up how long each input pulse will have to stay in the active state (in milliseconds) for a Pulse Cash Transaction to be acknowledged (i.e. pulse width). Valid Values: 1 - 255.','TEXT:1 to 3',NULL,'Y',NULL,NULL,NULL,NULL,0,13,26,'Pulse Capture Debounce','A',NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1525');
	
INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD) SELECT '1526' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = '1526');
INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(FILE_TRANSFER_ID,DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE)
SELECT FILE_TRANSFER_ID,'1526',NULL,4,'Y','The equivalent monetary value of each Cash input pulse in pennies. Valid Values: 1 - 65535.','TEXT:1 to 5',NULL,'Y',NULL,NULL,NULL,NULL,0,13,27,'Pulse Capture Value','A',NULL,3,'Y'
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = 'CFG-METADATA-13'
	AND NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1526');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Number of 10ms clock ticks for which a pulse is active in Gx pulse interface mode. Note this field is a hexadecimal value. Examples: 0A=100ms, 0F=150ms, 14=200ms. Default is 0A=100ms.'
WHERE DEVICE_TYPE_ID = 0 AND DEVICE_SETTING_PARAMETER_CD = 'Pulse Duration';

UPDATE DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP SET DISPLAY = 'N' WHERE DEVICE_TYPE_ID = 13 AND CONFIG_TEMPLATE_CATEGORY_ID = (
	SELECT CONFIG_TEMPLATE_CATEGORY_ID FROM DEVICE.CONFIG_TEMPLATE_CATEGORY WHERE CONFIG_TEMPLATE_CATEGORY_NAME = 'eTransact Settings'
);

UPDATE DEVICE.CONFIG_TEMPLATE_REGEX SET REGEX_VALUE = REPLACE(REGEX_VALUE, '+', '*') WHERE REGEX_NAME IN ('ALPHA_AND_PRINTABLE_CHARS', 'PRINTABLE_CHARS') AND REGEX_VALUE LIKE '%+%';
	
COMMIT;

DROP INDEX ENGINE.IX_DS_SESSION_END_UTC_TS;
CREATE INDEX ENGINE.IX_DS_DEVICE_NAME_START_UTC_TS ON ENGINE.DEVICE_SESSION(DEVICE_NAME, SESSION_START_UTC_TS) LOCAL TABLESPACE ENGINE_INDX ONLINE;
DROP INDEX ENGINE.IX_DS_DEVICE_NAME;

DROP INDEX PSS.INX_TRANLEGACYTRANSNO;
CREATE INDEX PSS.IDX_TRAN_POS_PTA_TRAN_START_TS ON PSS.TRAN(POS_PTA_ID, TRAN_START_TS) LOCAL TABLESPACE PSS_INDX ONLINE;
DROP INDEX PSS.IX_TRAN_POS_PTA_ID;

CREATE INDEX DEVICE.IDX_EVENT_HOST_ID_START_TS ON DEVICE.EVENT(HOST_ID, EVENT_START_TS) LOCAL TABLESPACE DEVICE_INDX ONLINE;
DROP INDEX DEVICE.IDX_EVENT_HOST_ID;

CREATE UNIQUE INDEX DEVICE.UDX_DEV_CUST_LOC_DEV_SERIAL_CD ON DEVICE.DEVICE_CUST_LOC(DEVICE_SERIAL_CD) TABLESPACE DEVICE_INDX ONLINE;

