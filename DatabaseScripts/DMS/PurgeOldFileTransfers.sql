DECLARE
	ld_sysdate DATE := SYSDATE;
	ln_file_transfer_days NUMBER;
BEGIN
	UPDATE DEVICE.FILE_TRANSFER
	SET FILE_TRANSFER_TYPE_CD = 1
	WHERE FILE_TRANSFER_TYPE_CD = 19 AND FILE_TRANSFER_ID IN (
		SELECT FILE_TRANSFER_ID FROM DEVICE.CONFIG_TEMPLATE_SETTING);
	COMMIT;

	SELECT DECODE(REPLACE(GLOBAL_NAME, '.WORLD', ''), 'USADBP', 1, 180)
	INTO ln_file_transfer_days
	FROM GLOBAL_NAME;

	DELETE FROM device.device_file_transfer_event
	WHERE device_file_transfer_id IN (SELECT device_file_transfer_id FROM device.device_file_transfer WHERE file_transfer_id IN
		(SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (0, 2, 3, 11, 12, 19, 20, 21, 101) AND created_ts < ld_sysdate - ln_file_transfer_days))
		AND device_file_transfer_id NOT IN (SELECT TO_NUMBER(command) FROM engine.machine_cmd_pending WHERE data_type IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(command, '^[0-9]+$'));
	COMMIT;

	DELETE FROM device.device_file_transfer
	WHERE file_transfer_id IN (SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (0, 2, 3, 11, 12, 19, 20, 21, 101) AND created_ts < ld_sysdate - ln_file_transfer_days)
		AND device_file_transfer_id NOT IN (SELECT TO_NUMBER(command) FROM engine.machine_cmd_pending WHERE data_type IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(command, '^[0-9]+$'));
	COMMIT;

	DELETE FROM device.file_transfer
	WHERE file_transfer_type_cd IN (0, 2, 3, 11, 12, 19, 20, 21, 101) AND created_ts < ld_sysdate - ln_file_transfer_days
		AND file_transfer_id NOT IN (SELECT file_transfer_id FROM device.device_file_transfer WHERE device_file_transfer_id IN (
			SELECT TO_NUMBER(command) FROM engine.machine_cmd_pending WHERE data_type IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(command, '^[0-9]+$')));
	COMMIT;
END;
