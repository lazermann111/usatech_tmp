GRANT SELECT ON DEVICE.SETTING_PARAMETER_TYPE TO USAT_DMS_ROLE, USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON DEVICE.SP_UPDATE_CONFIG_AND_RETURN TO USAT_DMS_ROLE;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER
SET DEVICE_SETTING_UI_CONFIGURABLE = 'N'
WHERE DEVICE_SETTING_PARAMETER_CD IN ('60', '61');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET EDITOR = 'SELECT:1=1;2=2;3=3;4=4;5=5;6=6',
DESCRIPTION = '1 - Standard MDB<br />2 - eTrans MDB<br />3 - Coin Pulse<br />4 - Coin Pulse Dual Enable<br />5 - Top-off Coin Pulse<br />6 - Serial Edge'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1500';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = 'Disable Message Line 1', DESCRIPTION = 'The device will display this message on the top line of the display when disabled by the VMC'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1527';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = 'Disable Message Line 2', DESCRIPTION = 'The device will display this message on the bottom line of the display when disabled by the VMC'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1528';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'SSN (silicone serial number), terminal serial number with the bytes in decimal format and a comma as the byte separator' WHERE DEVICE_TYPE_ID = 11 AND DEVICE_SETTING_PARAMETER_CD = 'SSN';
COMMIT;

DECLARE
  LV_SQL VARCHAR2(4000);
BEGIN
  SELECT 'CREATE SEQUENCE DEVICE.RERIX_MACHINE_ID_SEQ INCREMENT BY 1 MINVALUE ' || MIN_VALUE || ' MAXVALUE ' || MAX_VALUE 
    || ' START WITH ' || LAST_NUMBER || ' ORDER NOCYCLE' 
  INTO LV_SQL
  FROM DBA_SEQUENCES 
  WHERE SEQUENCE_OWNER = 'TAZDBA' AND SEQUENCE_NAME = 'RERIX_MACHINE_ID_SEQ';

  EXECUTE IMMEDIATE LV_SQL;
  
  SELECT 'CREATE SEQUENCE ENGINE.SEQ_OB_EMAIL_QUEUE_ID INCREMENT BY 1 MINVALUE ' || MIN_VALUE || ' MAXVALUE ' || MAX_VALUE 
    || ' START WITH ' || LAST_NUMBER || ' NOORDER NOCYCLE' 
  INTO LV_SQL
  FROM DBA_SEQUENCES 
  WHERE SEQUENCE_OWNER = 'TAZDBA' AND SEQUENCE_NAME = 'SEQ_OB_EMAIL_QUEUE_ID';

  EXECUTE IMMEDIATE LV_SQL;
END;
/

DROP PUBLIC SYNONYM RERIX_MACHINE_ID_SEQ;

ALTER TABLE TAZDBA.OB_EMAIL_QUEUE RENAME TO OB_EMAIL_QUEUE_OLD;

CREATE TABLE ENGINE.OB_EMAIL_QUEUE
(
  OB_EMAIL_QUEUE_ID              NUMBER(20),
  OB_EMAIL_FROM_EMAIL_ADDR       VARCHAR2(60) NOT NULL,
  OB_EMAIL_FROM_NAME             VARCHAR2(60) NOT NULL,
  OB_EMAIL_TO_EMAIL_ADDR         VARCHAR2(60) NOT NULL,
  OB_EMAIL_TO_NAME               VARCHAR2(60) NOT NULL,
  OB_EMAIL_SUBJECT               VARCHAR2(100) NOT NULL,
  OB_EMAIL_MSG                   VARCHAR2(4000) NOT NULL,
  OB_EMAIL_SCHEDULED_SEND_TS     DATE,
  OB_EMAIL_LAST_SEND_ATTEMPT_TS  DATE,
  OB_EMAIL_TOT_SEND_ATTEMP_NUM   NUMBER(2),
  OB_EMAIL_SENT_SUCCESS_TS       DATE,
  CREATED_BY                     VARCHAR2(30) NOT NULL,
  CREATED_TS                     DATE           NOT NULL,
  LAST_UPDATED_BY                VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS                DATE           NOT NULL
)
TABLESPACE ENGINE_DATA
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            BUFFER_POOL      DEFAULT
           )
LOGGING
PARTITION BY RANGE (CREATED_TS)
(  
  PARTITION "2012-02-01" VALUES LESS THAN (TO_DATE(' 2012-03-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')), 
  PARTITION "2012-03-01" VALUES LESS THAN (TO_DATE(' 2012-04-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),  
  PARTITION "2012-04-01" VALUES LESS THAN (TO_DATE(' 2012-05-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-05-01" VALUES LESS THAN (TO_DATE(' 2012-06-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-06-01" VALUES LESS THAN (TO_DATE(' 2012-07-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-07-01" VALUES LESS THAN (TO_DATE(' 2012-08-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-08-01" VALUES LESS THAN (TO_DATE(' 2012-09-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-09-01" VALUES LESS THAN (TO_DATE(' 2012-10-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-10-01" VALUES LESS THAN (TO_DATE(' 2012-11-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-11-01" VALUES LESS THAN (TO_DATE(' 2012-12-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-12-01" VALUES LESS THAN (TO_DATE(' 2013-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) 
);

CREATE UNIQUE INDEX ENGINE.PK_OB_EMAIL_QUEUE ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_QUEUE_ID)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IDX_0B_00001 ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_TOT_SEND_ATTEMP_NUM)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IDX_OB_00002 ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_SCHEDULED_SEND_TS)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IX_OB_EMAIL_QUEUE_TS ON ENGINE.OB_EMAIL_QUEUE
(CREATED_TS)
  TABLESPACE ENGINE_INDX
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              INITIAL          1M
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
	      FREELISTS        1
              FREELIST GROUPS  1
              BUFFER_POOL      DEFAULT
             )
LOGGING
LOCAL NOPARALLEL;

CREATE OR REPLACE TRIGGER ENGINE.TRBU_OB_EMAIL_QUEUE
   BEFORE UPDATE
   ON ENGINE.OB_EMAIL_QUEUE
   FOR EACH ROW
BEGIN
	SELECT 
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
        USER
    INTO 
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
        :NEW.last_updated_by
    FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER ENGINE.TRBI_OB_EMAIL_QUEUE
 BEFORE
  INSERT
 ON ENGINE.OB_EMAIL_QUEUE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
    IF :new.ob_email_queue_id IS NULL THEN
      SELECT seq_ob_email_queue_id.nextval
        into :new.ob_email_queue_id
        FROM dual;
    END IF;

    IF :new.ob_email_scheduled_send_ts is null THEN
        :new.ob_email_scheduled_send_ts := sysdate;
    END IF;

    -- this is a minor fix for java, it's inserting date with no time
    IF(:new.ob_email_scheduled_send_ts = trunc(sysdate)) THEN
        :new.ob_email_scheduled_send_ts := sysdate;
    END IF;

	SELECT
           sysdate,
           user,
           sysdate,
           user,
           0
      into
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.OB_EMAIL_TOT_SEND_ATTEMP_NUM
      FROM dual;
End;
/

ALTER TABLE ENGINE.OB_EMAIL_QUEUE ADD (
  CONSTRAINT PK_OB_EMAIL_QUEUE
  PRIMARY KEY
  (OB_EMAIL_QUEUE_ID)
  USING INDEX ENGINE.PK_OB_EMAIL_QUEUE
  ENABLE NOVALIDATE);

insert /*+append nologging */ into ENGINE.OB_EMAIL_QUEUE select * from tazdba.OB_EMAIL_QUEUE_OLD;
commit;

GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO USAT_SECURITY_ROLE;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO USAT_ESUDSOPER_ROLE;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO ESUDS_DEVELOPER;
GRANT DELETE, INSERT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USATECH_UPD_TRANS;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_DEV_READ_ONLY;
GRANT DELETE, INSERT, SELECT, UPDATE, DEBUG ON ENGINE.OB_EMAIL_QUEUE TO USAT_ADMIN_ROLE;
GRANT DELETE, INSERT, SELECT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_APP_LAYER_ROLE;

CREATE OR REPLACE PUBLIC SYNONYM SEQ_OB_EMAIL_QUEUE_ID FOR ENGINE.SEQ_OB_EMAIL_QUEUE_ID;
CREATE OR REPLACE PUBLIC SYNONYM OB_EMAIL_QUEUE FOR ENGINE.OB_EMAIL_QUEUE;

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name, d.device_type_id
		from device.vw_device_last_active d
		where d.device_type_id in (0, 1)
			and dbadmin.to_number_or_null(substr(d.device_name, 3)) > :max_device_number
		order by d.device_id;
		
	ll_default_content LONG;
	lv_default_content_g4 VARCHAR2(1024);
	lv_default_content_g5 VARCHAR2(1024);
	ll_content LONG;
	lv_content VARCHAR2(1024);	
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer_oldnopart
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G4-DEFAULT-CFG';
		
	lv_default_content_g4 := UPPER(ll_default_content);

	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer_oldnopart
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G5-DEFAULT-CFG';
		
	lv_default_content_g5 := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT /*+INDEX(ft INX_FILE_XFER_TYPE_NAME_OLD)*/ file_transfer_content
				FROM device.file_transfer_oldnopart ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
					AND file_content_length(rowid) = 1024
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			IF rec_cur.device_type_id = 0 THEN 
				lv_content := lv_default_content_g4; 
			ELSE
				lv_content := lv_default_content_g5;
			END IF;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		MERGE INTO device.device_setting O
            USING (
              SELECT rec_cur.device_id DEVICE_ID,
                     device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size,
                     REPLACE(SUBSTR(lv_content, field_offset * 2 + 1, field_size * 2), ' ', '0') DEVICE_SETTING_VALUE
                from device.config_template_setting 
                where device_type_id = 0 and field_offset < 512) N
              ON (O.DEVICE_ID = N.DEVICE_ID AND O.device_setting_parameter_cd = N.device_setting_parameter_cd)
              WHEN MATCHED THEN
               UPDATE
                  SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE
              WHEN NOT MATCHED THEN
               INSERT (O.device_id, O.device_setting_parameter_cd, O.device_setting_value)
                VALUES(N.device_id, N.device_setting_parameter_cd, N.device_setting_value);			
		
		COMMIT;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name
		from device.vw_device_last_active d
		where d.device_type_id = 6
			and dbadmin.to_number_or_null(substr(d.device_name, 3)) > :max_device_number
		order by d.device_id;
		
	ll_default_content LONG;
	lv_default_content VARCHAR2(114);
	ll_content LONG;
	lv_content VARCHAR2(114);	
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer_oldnopart
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'MEI-DEFAULT-CFG';
		
	lv_default_content := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT /*+INDEX(ft INX_FILE_XFER_TYPE_NAME_OLD)*/ file_transfer_content
				FROM device.file_transfer_oldnopart ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
					AND file_content_length(rowid) = 114
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			lv_content := lv_default_content;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		MERGE INTO device.device_setting O
            USING (
              SELECT rec_cur.device_id DEVICE_ID,
                     device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size,
                     REPLACE(SUBSTR(lv_content, field_offset * 2 + 1, field_size * 2), ' ', '0') DEVICE_SETTING_VALUE
                from device.config_template_setting 
                where device_type_id = 6 and field_offset < 57) N
              ON (O.DEVICE_ID = N.DEVICE_ID AND O.device_setting_parameter_cd = N.device_setting_parameter_cd)
              WHEN MATCHED THEN
               UPDATE
                  SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE
              WHEN NOT MATCHED THEN
               INSERT (O.device_id, O.device_setting_parameter_cd, O.device_setting_value)
                VALUES(N.device_id, N.device_setting_parameter_cd, N.device_setting_value);				
		
		COMMIT;
	END LOOP;
END;
/

ALTER TABLE DEVICE.DEVICE_TYPE ADD (DEFAULT_CONFIG_TEMPLATE_NAME VARCHAR2(200));

UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'G4-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 0;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'G5-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 1;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'MEI-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 6;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'KIOSK-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 11;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'T2-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 12;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-13-' WHERE DEVICE_TYPE_ID = 13;
COMMIT;


CREATE TABLE DEVICE.DEVICE_SETTING_BACKUP
(
    DEVICE_ID                   NUMBER(20,0) NOT NULL,
    DEVICE_SETTING_PARAMETER_CD VARCHAR2(60) NOT NULL,
    DEVICE_SETTING_VALUE        VARCHAR2(200),
    CREATED_BY                  VARCHAR2(30) NOT NULL,
    CREATED_TS 					DATE NOT NULL,
    LAST_UPDATED_BY 			VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS 			DATE NOT NULL,
    FILE_ORDER 					NUMBER(20,0),
	CONSTRAINT PK_DEVICE_SETTING_BACKUP PRIMARY KEY(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD),
	CONSTRAINT FK_DEVICE_SETTING_BKP_DEVICE FOREIGN KEY (DEVICE_ID) REFERENCES DEVICE.DEVICE (DEVICE_ID),
	CONSTRAINT FK_DEVICE_SETTING_BKP_PARAM_CD FOREIGN KEY (DEVICE_SETTING_PARAMETER_CD) REFERENCES DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD)
) TABLESPACE DEVICE_DATA;

CREATE INDEX DEVICE.IDX_DEVICE_SETING_BKP_VALUE ON DEVICE.DEVICE_SETTING_BACKUP(DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;
CREATE INDEX DEVICE.IDX_DEVICE_SET_BKP_PARAM_VALUE ON DEVICE.DEVICE_SETTING_BACKUP(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_SETTING_BACKUP BEFORE INSERT ON DEVICE.DEVICE_SETTING_BACKUP
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_SETTING_BACKUP BEFORE UPDATE ON DEVICE.DEVICE_SETTING_BACKUP
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_SETTING_BACKUP TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.DEVICE_SETTING_BACKUP TO USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_SETTING_BACKUP TO USATECH_UPD_TRANS;

CREATE OR REPLACE PUBLIC SYNONYM DEVICE_SETTING_BACKUP FOR DEVICE.DEVICE_SETTING_BACKUP;


CREATE TABLE DEVICE.CONFIG_TEMPLATE_TYPE
(
  CONFIG_TEMPLATE_TYPE_ID NUMBER(20, 0) NOT NULL,
  CONFIG_TEMPLATE_TYPE_NAME VARCHAR2(200) NOT NULL,  
  CREATED_BY	VARCHAR2(30) NOT NULL,
  CREATED_TS	DATE NOT NULL,
  LAST_UPDATED_BY	VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS	DATE NOT NULL, 
  CONSTRAINT PK_CONFIG_TEMPLATE_TYPE PRIMARY KEY(CONFIG_TEMPLATE_TYPE_ID)  
) TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE_TYPE BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE_TYPE
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE_TYPE BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.CONFIG_TEMPLATE_TYPE TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.CONFIG_TEMPLATE_TYPE TO PSS, USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE_TYPE TO USATECH_UPD_TRANS;

INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(1, 'Default');
INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(2, 'Custom');
INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(3, 'Metadata');
COMMIT;

CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE_TYPE FOR DEVICE.CONFIG_TEMPLATE_TYPE;


CREATE SEQUENCE DEVICE.SEQ_CONFIG_TEMPLATE_ID INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1 NOCACHE;
GRANT SELECT ON DEVICE.SEQ_CONFIG_TEMPLATE_ID TO USAT_DMS_ROLE;

CREATE TABLE DEVICE.CONFIG_TEMPLATE
(
  CONFIG_TEMPLATE_ID NUMBER(20, 0) NOT NULL,
  CONFIG_TEMPLATE_NAME VARCHAR2(200) NOT NULL,
  CONFIG_TEMPLATE_TYPE_ID NUMBER(20, 0) NOT NULL,
  DEVICE_TYPE_ID NUMBER(20, 0) NOT NULL,
  CREATED_BY	VARCHAR2(30) NOT NULL,
  CREATED_TS	DATE NOT NULL,
  LAST_UPDATED_BY	VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS	DATE NOT NULL, 
  PROPERTY_LIST_VERSION NUMBER(20, 0) DEFAULT 0 NOT NULL,
  CONSTRAINT PK_CONFIG_TEMPLATE PRIMARY KEY(CONFIG_TEMPLATE_ID),
  CONSTRAINT FK_CONFIG_TEMPLATE_CT_TYPE_ID FOREIGN KEY(CONFIG_TEMPLATE_TYPE_ID) REFERENCES DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID),
  CONSTRAINT FK_CONFIG_TEMPLATE_DT_ID FOREIGN KEY(DEVICE_TYPE_ID) REFERENCES DEVICE.DEVICE_TYPE(DEVICE_TYPE_ID)
) TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX DEVICE.UDX_CONFIG_TEMPLATE_CT_NAME ON DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME) TABLESPACE DEVICE_INDX;
CREATE UNIQUE INDEX DEVICE.UDX_CFG_TMPL_DEV_TYPE_TYPE_PLV ON DEVICE.CONFIG_TEMPLATE(DEVICE_TYPE_ID, CONFIG_TEMPLATE_TYPE_ID, DECODE(CONFIG_TEMPLATE_TYPE_ID, 2, CONFIG_TEMPLATE_ID, PROPERTY_LIST_VERSION)) TABLESPACE DEVICE_INDX;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE
  FOR EACH ROW
BEGIN
	IF :NEW.CONFIG_TEMPLATE_ID IS NULL THEN
		SELECT SEQ_CONFIG_TEMPLATE_ID.NEXTVAL
        INTO :NEW.CONFIG_TEMPLATE_ID
        FROM DUAL;
    END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.CONFIG_TEMPLATE TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.CONFIG_TEMPLATE TO PSS, USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE TO USATECH_UPD_TRANS;

CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE FOR DEVICE.CONFIG_TEMPLATE;

ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD (CONFIG_TEMPLATE_ID NUMBER(20, 0));
CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE_SETTING FOR DEVICE.CONFIG_TEMPLATE_SETTING;

DECLARE
	CURSOR cur IS 
		select distinct ft.file_transfer_id, ft.file_transfer_type_cd, ft.file_transfer_name, 
			file_content_length(ft.rowid) file_content_length
		from device.config_template_setting cts
		join device.file_transfer_oldnopart ft on cts.file_transfer_id = ft.file_transfer_id
		where cts.config_template_id is null
			and not exists (select 1 from device.config_template where config_template_name = ft.file_transfer_name)
		order by ft.file_transfer_id;
		
	ln_config_template_type_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_TYPE_ID%TYPE;
	ln_property_list_version CONFIG_TEMPLATE.PROPERTY_LIST_VERSION%TYPE;
	ln_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE;
BEGIN
	FOR rec_cur IN cur LOOP
		ln_property_list_version := 0;
		IF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'GX-GENERIC-MAP' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 0;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'MEI-GENERIC-MAP' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 6;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'CFG-METADATA-13' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 13;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'CFG-METADATA-11' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 11;
		ELSIF rec_cur.file_transfer_type_cd = 6 THEN
			IF rec_cur.file_transfer_name = 'G4-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 0;
			ELSIF rec_cur.file_transfer_name = 'G5-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 1;
			ELSIF rec_cur.file_transfer_name = 'MEI-DEFAULT-CFG' THEN			
				ln_config_template_type_id := 1;
				ln_device_type_id := 6;
			ELSE
				ln_config_template_type_id := 2;
				IF rec_cur.file_content_length < 1024 THEN 
					ln_device_type_id := 6;
				ELSE
					ln_device_type_id := 0;
				END IF;
			END IF;
		ELSIF rec_cur.file_transfer_type_cd = 15 THEN
			IF rec_cur.file_transfer_name = 'KIOSK-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 11;
			ELSIF rec_cur.file_transfer_name = 'T2-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 12;
			ELSE
				ln_config_template_type_id := 2;
				ln_device_type_id := 11;
			END IF;
		ELSIF rec_cur.file_transfer_type_cd = 16 THEN
			ln_config_template_type_id := 2;
			ln_device_type_id := 11;
		ELSIF rec_cur.file_transfer_type_cd = 22 AND REGEXP_LIKE(rec_cur.file_transfer_name, '^DEFAULT-CFG-13-[0-9][0-9]*$') THEN
			ln_config_template_type_id := 1;
			ln_device_type_id := 13;
			ln_property_list_version := NVL(TO_NUMBER_OR_NULL(SUBSTR(rec_cur.file_transfer_name, INSTR(rec_cur.file_transfer_name, '-', 1, 3) + 1)), 0);
		ELSIF rec_cur.file_transfer_type_cd = 23 THEN
			ln_config_template_type_id := 2;
			ln_device_type_id := 13;
		ELSE
			ln_config_template_type_id := 2;
			ln_device_type_id := 13;
		END IF;		
		
		INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID, PROPERTY_LIST_VERSION)
		SELECT rec_cur.file_transfer_name, ln_config_template_type_id, ln_device_type_id, ln_property_list_version FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
		IF SQL%ROWCOUNT > 0 THEN
			UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
			SET CONFIG_TEMPLATE_ID = (SELECT CONFIG_TEMPLATE_ID FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name)
			WHERE FILE_TRANSFER_ID = rec_cur.file_transfer_id;
			
			COMMIT;
		END IF;
	END LOOP;
END;
/

ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING MODIFY (CONFIG_TEMPLATE_ID NUMBER(20, 0) NOT NULL);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD CONSTRAINT FK_CFG_TMPL_CONFIG_TEMPLATE_ID FOREIGN KEY(CONFIG_TEMPLATE_ID) REFERENCES DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_ID);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP CONSTRAINT PK_CONFIG_TEMPLATE_SETTING;
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD CONSTRAINT PK_CONFIG_TEMPLATE_SETTING PRIMARY KEY (CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP (FILE_TRANSFER_ID);


DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer_oldnopart ft
		where ft.file_transfer_type_cd = 6 
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_content_length(ft.rowid) = 1024
		order by ft.file_transfer_name;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 0 and field_offset < 512 
		order by field_offset;
	
	ll_content LONG;
	lv_content VARCHAR2(1024);	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_exists NUMBER;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;	
		END IF;
		
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		ll_content := NULL;
		
		BEGIN
			SELECT file_transfer_content
			INTO ll_content
			FROM device.file_transfer_oldnopart
			WHERE file_transfer_id = rec_cur.file_transfer_id;
		EXCEPTION
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NOT NULL THEN
			lv_content := UPPER(ll_content);
			
			INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
			SELECT rec_cur.file_transfer_name, 2, 0 FROM DUAL
			WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
			IF SQL%ROWCOUNT > 0 THEN				
				SELECT CONFIG_TEMPLATE_ID 
				INTO ln_config_template_id
				FROM DEVICE.CONFIG_TEMPLATE 
				WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
			
				FOR rec_cur_map IN cur_map LOOP
					SP_UPSERT_CFG_TEMPLATE_SETTING(ln_config_template_id, rec_cur_map.device_setting_parameter_cd, 
						REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
				END LOOP;
				
				COMMIT;
			END IF;
		END IF;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer_oldnopart ft
		where ft.file_transfer_type_cd = 6 
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_content_length(ft.rowid) = 114
		order by ft.file_transfer_name;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 6 and field_offset < 57 
		order by field_offset;
	
	ll_content LONG;
	lv_content VARCHAR2(114);	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_exists NUMBER;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;	
		END IF;
		
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		ll_content := NULL;
		
		BEGIN
			SELECT file_transfer_content
			INTO ll_content
			FROM device.file_transfer_oldnopart
			WHERE file_transfer_id = rec_cur.file_transfer_id;
		EXCEPTION
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NOT NULL THEN
			lv_content := UPPER(ll_content);
			
			INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
			SELECT rec_cur.file_transfer_name, 2, 6 FROM DUAL
			WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
			IF SQL%ROWCOUNT > 0 THEN			
				SELECT CONFIG_TEMPLATE_ID 
				INTO ln_config_template_id
				FROM DEVICE.CONFIG_TEMPLATE 
				WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
			
				FOR rec_cur_map IN cur_map LOOP
					SP_UPSERT_CFG_TEMPLATE_SETTING(ln_config_template_id, rec_cur_map.device_setting_parameter_cd, 
						REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
				END LOOP;
				
				COMMIT;
			END IF;
		END IF;
	END LOOP;
END;
/
