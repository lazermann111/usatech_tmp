WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.pbk?rev=1.80
create or replace PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION IS
    FUNCTION LOB_TO_HEX_LONG(pl_lob BLOB)
    RETURN LONG
    IS
        ll_tmp LONG := '';
        tot BINARY_INTEGER;
    BEGIN
        tot := DBMS_LOB.GETLENGTH(pl_lob);
        FOR i IN 1..tot LOOP
            ll_tmp := ll_tmp || RAWTOHEX(DBMS_LOB.SUBSTR(pl_lob, 1, i));
        END LOOP;
        RETURN ll_tmp;
    END;

    FUNCTION LOB_TO_HEX_LONG(pl_lob CLOB)
    RETURN LONG
    IS
        ll_tmp LONG := '';
        tot BINARY_INTEGER;
        n PLS_INTEGER;
    BEGIN
        tot := DBMS_LOB.GETLENGTH(pl_lob);
        FOR i IN 1..tot LOOP
            n := ASCII(DBMS_LOB.SUBSTR(pl_lob, 1, i));
            ll_tmp := ll_tmp || TO_CHAR(n, 'FM0X');
        END LOOP;
        RETURN ll_tmp;
    END;
    
PROCEDURE SP_GET_FILE_TRANSFER_BLOB
(
    pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
    pbl_file_transfer_content OUT BLOB
)
IS
    lcl_file_transfer_content_hex CLOB;
    ln_file_content_hex_length NUMBER;
    ln_position NUMBER := 1;
    ln_max_size NUMBER := 32766; -- Oracle limitation
    ln_length NUMBER;

BEGIN
    DELETE FROM device.gtmp_file_transfer WHERE file_transfer_id = pn_file_transfer_id;

    INSERT INTO device.gtmp_file_transfer(file_transfer_id, file_transfer_content_hex)
    SELECT file_transfer_id, TO_LOB(file_transfer_content)
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    SELECT file_transfer_content_hex INTO lcl_file_transfer_content_hex
    FROM device.gtmp_file_transfer
    WHERE file_transfer_id = pn_file_transfer_id AND ROWNUM = 1;

    ln_file_content_hex_length := dbms_lob.getlength(lcl_file_transfer_content_hex);
    dbms_lob.createtemporary(pbl_file_transfer_content, TRUE, dbms_lob.call);

    WHILE ln_position <= ln_file_content_hex_length LOOP
        ln_length := LEAST(ln_max_size, ln_file_content_hex_length - ln_position + 1);
        dbms_lob.writeappend(pbl_file_transfer_content, ln_length / 2, REPLACE(dbms_lob.substr(lcl_file_transfer_content_hex, ln_length, ln_position), ' ', '0'));
        ln_position := ln_position + ln_length;
    END LOOP;

    DELETE FROM device.gtmp_file_transfer WHERE file_transfer_id = pn_file_transfer_id;
END;

FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
RETURN VARCHAR
IS
    ln_cnt PLS_INTEGER; 
BEGIN
	SELECT COUNT(*)
      INTO ln_cnt
      FROM DEVICE.DEVICE d
      JOIN DEVICE.FILE_TRANSFER ft
        ON ft.FILE_TRANSFER_NAME = CASE d.DEVICE_TYPE_ID
                WHEN 0 THEN 'G4-DEFAULT-CFG'
                WHEN 1 THEN 'G5-DEFAULT-CFG'
                WHEN 6 THEN 'MEI-DEFAULT-CFG'
                WHEN 12 THEN 'T2-DEFAULT-CFG'
                ELSE 'DEFAULT-CFG-' || d.DEVICE_TYPE_ID || '-' || (
                   SELECT dsplv.DEVICE_SETTING_VALUE
                     FROM DEVICE.DEVICE_SETTING dsplv
                    WHERE dsplv.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
                      AND dsplv.DEVICE_ID = d.DEVICE_ID)
            END
        AND ft.FILE_TRANSFER_TYPE_CD IN(6,15,22)
      CROSS JOIN DEVICE.DEVICE_SETTING_PARAMETER dsp
       LEFT JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts ON ft.FILE_TRANSFER_ID = cts.FILE_TRANSFER_ID
        AND cts.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
      WHERE d.DEVICE_ID = pn_device_id
        AND (cts.DEVICE_SETTING_PARAMETER_CD IS NOT NULL OR (dsp.DEVICE_SETTING_UI_CONFIGURABLE = 'Y' AND pv_device_setting_value IS NOT NULL))
        AND dsp.device_setting_parameter_cd = pv_device_setting_parameter_cd;
    IF ln_cnt = 0 THEN
        RETURN 'N';
    ELSE
        RETURN 'Y';
    END IF;
END;

	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	IS
		lv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	BEGIN
		SELECT DEVICE_SETTING_VALUE
		INTO lv_device_setting_value
		FROM DEVICE.DEVICE_SETTING
		WHERE DEVICE_ID = pn_device_id
			AND DEVICE_SETTING_PARAMETER_CD = pv_device_setting_parameter_cd;
		
		RETURN lv_device_setting_value;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

PROCEDURE SP_UPDATE_DEVICE_SETTING(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
IS
	ln_exists NUMBER;
BEGIN
	SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, ln_exists);
END;

	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	)
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		NULL;
	        END;	
		END IF;
		
		UPDATE device.device_setting
		SET device_setting_value = pv_device_setting_value
		WHERE device_id = pn_device_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
				VALUES(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_exists);
	        END;
		ELSE
			pn_exists := 1;
		END IF;
    END;

PROCEDURE SP_UPDATE_DEVICE_SETTINGS
(
    pn_device_id IN device.device_id%TYPE,
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(3000);
    ls_record VARCHAR2(300);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value device_setting.device_setting_value%TYPE;
    ln_return NUMBER := 0;
    ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SELECT file_transfer_type_cd, file_transfer_content
    INTO ln_file_transfer_type_cd, ll_file_transfer_content
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
        DELETE FROM device.device_setting
        WHERE device_id = pn_device_id
            AND device_setting_parameter_cd IN (
                SELECT device_setting_parameter_cd
                FROM device.device_setting_parameter
                WHERE device_setting_ui_configurable = 'Y');
    ELSE
        DELETE FROM device.config_template_setting
        WHERE file_transfer_id = pn_file_transfer_id;
    END IF;

    LOOP
        ls_content := ls_content || SUBSTR(utl_raw.cast_to_varchar2(HEXTORAW(ll_file_transfer_content)), ln_content_pos, ln_content_size);
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
        IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
            ln_return := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            IF ln_return = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
            ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');

            IF NVL(LENGTH(ls_param), 0) > 0 THEN
				BEGIN
					INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
					SELECT ls_param FROM dual
					WHERE NOT EXISTS (
						SELECT 1 FROM device.device_setting_parameter
						WHERE device_setting_parameter_cd = ls_param);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;

				BEGIN
					IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
						INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
						VALUES(pn_device_id, ls_param, ls_value);
					ELSE
						INSERT INTO device.config_template_setting(file_transfer_id, device_setting_parameter_cd, config_template_setting_value)
						VALUES(pn_file_transfer_id, ls_param, ls_value);
					END IF;
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;					
            END IF;

            IF ln_return = 1 THEN
                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.device_setting
                    WHERE device_id = pn_device_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                ELSE
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.config_template_setting
                    WHERE file_transfer_id = pn_file_transfer_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                END IF;

                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := PKG_CONST.ERROR__NO_ERROR;
                RETURN;
            END IF;
        END LOOP;

    END LOOP;
END;

PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS
(
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
BEGIN
    SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
END;

PROCEDURE SP_GET_HOST_BY_PORT_NUM
(
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_host_id OUT host.host_id%TYPE
)
IS
BEGIN
    SELECT host_id INTO pn_host_id FROM (
        SELECT host_id
        FROM device.host h
        INNER JOIN device.device d ON h.device_id = d.device_id
        WHERE d.device_name = pv_device_name
            AND h.host_port_num = pn_host_port_num
            AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
        ORDER BY h.created_ts DESC
    ) WHERE ROWNUM = 1;
END;

PROCEDURE SP_GET_HOST
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_id OUT host.host_id%TYPE
)
IS
    ln_new_host_count NUMBER;
BEGIN
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__FAILURE;
        pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

        sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                -- fail over to the base host
                sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        sp_create_default_hosts(pn_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                            RETURN;
                        END IF;

                        sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
                            pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
                            RETURN;
                    END;
            END;
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_CREATE_DEFAULT_HOSTS
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_new_host_count                       OUT NUMBER,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
    ln_base_host_count                      NUMBER;
    ln_other_host_count                     NUMBER;
    lv_device_serial_cd                     device.device_serial_cd%TYPE;
    ln_device_type_id                       device.device_type_id%TYPE;
    ln_def_base_host_type_id                device_type.def_base_host_type_id%TYPE;
       ln_def_base_host_equipment_id           device_type.def_base_host_equipment_id%TYPE;
       ln_def_prim_host_type_id                device_type.def_prim_host_type_id%TYPE;
       ln_def_prim_host_equipment_id           device_type.def_prim_host_equipment_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_new_host_count := 0;

    SELECT COUNT(1)
    INTO ln_base_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND (dtht.device_type_host_type_cd = 'B'
            OR h.host_port_num = 0);

    SELECT COUNT(1)
    INTO ln_other_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND dtht.device_type_host_type_cd <> 'B'
       AND h.host_port_num <> 0;

    -- if exists both a base and another host, then there's nothing to do here
    IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        RETURN;
    END IF;

    SELECT
        d.device_serial_cd,
        d.device_type_id,
        dt.def_base_host_type_id,
        dt.def_base_host_equipment_id,
        dt.def_prim_host_type_id,
        dt.def_prim_host_equipment_id
    INTO
        lv_device_serial_cd,
        ln_device_type_id,
        ln_def_base_host_type_id,
           ln_def_base_host_equipment_id,
           ln_def_prim_host_type_id,
           ln_def_prim_host_equipment_id
    FROM device.device_type dt, device.device d
    WHERE d.device_type_id = dt.device_type_id
       AND d.device_id = pn_device_id;

    -- Create base host if it doesn't exist
    IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_serial_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            lv_device_serial_cd,
            0,
            0,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_base_host_type_id,
            ln_def_base_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    -- Create primary host if no other hosts exist
    IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            0,
            1,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_prim_host_type_id,
            ln_def_prim_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE
    IS
        ln_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    BEGIN
        SELECT MAX(HOST_EQUIPMENT_ID), 1
          INTO ln_host_equipment_id, pn_existing_cnt
          FROM DEVICE.HOST_EQUIPMENT
         WHERE HOST_EQUIPMENT_MFGR = pv_host_equipment_mfgr
           AND HOST_EQUIPMENT_MODEL = pv_host_equipment_model;
        IF ln_host_equipment_id IS NULL THEN
            SELECT SEQ_HOST_EQUIPMENT_ID.NEXTVAL, 0
              INTO ln_host_equipment_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST_EQUIPMENT(HOST_EQUIPMENT_ID, HOST_EQUIPMENT_MFGR, HOST_EQUIPMENT_MODEL)
                 VALUES(ln_host_equipment_id, pv_host_equipment_mfgr, pv_host_equipment_model);
        END IF;
        RETURN ln_host_equipment_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, pn_existing_cnt);
    END;
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
    BEGIN
        UPDATE DEVICE.HOST
           SET (HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_ACTIVE_YN_FLAG) =
               (SELECT pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'Y' FROM DUAL)
         WHERE DEVICE_ID = pn_device_id
           AND HOST_PORT_NUM = pn_host_port_num
           AND HOST_POSITION_NUM = pn_host_position_num
           RETURNING HOST_ID, 1 INTO pn_host_id, pn_existing_cnt;
        IF pn_host_id IS NULL THEN
            SELECT SEQ_HOST_ID.NEXTVAL, 0
              INTO pn_host_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST(HOST_ID, DEVICE_ID, HOST_PORT_NUM, HOST_POSITION_NUM, HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_SETTING_UPDATED_YN_FLAG, HOST_ACTIVE_YN_FLAG)
                 VALUES(pn_host_id, pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'N', 'Y');	        		
       END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
        ln_host_equipment_existing_cnt PLS_INTEGER;
    BEGIN
        UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, ln_host_equipment_existing_cnt), pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;

    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
       UPDATE DEVICE.HOST_SETTING
           SET HOST_SETTING_VALUE = pv_host_setting_value
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
	    IF SQL%NOTFOUND THEN
            INSERT INTO DEVICE.HOST_SETTING(HOST_ID, HOST_SETTING_PARAMETER, HOST_SETTING_VALUE)
                VALUES(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
        SELECT MAX(HOST_SETTING_VALUE)
          INTO pv_old_host_setting_value
          FROM DEVICE.HOST_SETTING
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;         
        UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
     PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE)
    IS
        ln_host_id HOST.HOST_ID%TYPE;
    BEGIN
		UPDATE DEVICE.GPRS_DEVICE GD
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = pn_device_id
			AND ICCID NOT IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = GD.DEVICE_ID AND HOST_TYPE_ID = 202);
	
		UPDATE DEVICE.GPRS_DEVICE GD
		   SET RSSI = pn_rssi || ',' || pn_ber, 
               RSSI_TS = pd_update_ts, 
               LAST_FILE_TRANSFER_ID = NULL, 
               MODEM_INFO_RECEIVED_TS = SYSDATE, 
               MODEM_INFO = pv_modem_info,
		   	   DEVICE_ID = pn_device_id, 
               GPRS_DEVICE_STATE_ID = 5,
		       ASSIGNED_BY = DECODE(DEVICE_ID, pn_device_id, ASSIGNED_BY, 'APP_LAYER'), 
		   	   ASSIGNED_TS = DECODE(DEVICE_ID, pn_device_id, ASSIGNED_TS, SYSDATE)
		 WHERE ICCID IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = pn_device_id AND HOST_TYPE_ID = 202)
		   AND (RSSI_TS IS NULL OR RSSI_TS < pd_update_ts);
        SELECT MAX(HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST
         WHERE DEVICE_ID = pn_device_id
           AND HOST_TYPE_ID IN(202,204);
        IF ln_host_id IS NOT NULL THEN
            PKG_DEVICE_CONFIGURATION.UPSERT_HOST_SETTING(ln_host_id,'CSQ',pn_rssi || ',' || pn_ber);
        END IF;
	END;
    
    PROCEDURE APPEND(
        l_text IN VARCHAR2,
        l_clob IN OUT NOCOPY CLOB)
    IS
    BEGIN
        IF l_text IS NOT NULL AND LENGTH(l_text) > 0 THEN
           DBMS_LOB.WRITEAPPEND(l_clob, LENGTH(l_text), l_text);
        END IF;
    END;

    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB)
    IS
        lv_cfg_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        CURSOR l_cur (cv_cfg_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE) IS
            SELECT dsp.DEVICE_SETTING_PARAMETER_CD,
                   DECODE(ds.DEVICE_SETTING_PARAMETER_CD, NULL, cts.CONFIG_TEMPLATE_SETTING_VALUE, ds.DEVICE_SETTING_VALUE) DEVICE_SETTING_VALUE,
                   DECODE(ds.DEVICE_SETTING_PARAMETER_CD, NULL, 'Y', 'N') IS_DEFAULT
              FROM DEVICE.DEVICE_SETTING_PARAMETER dsp
              LEFT JOIN DEVICE.DEVICE_SETTING ds
                ON ds.DEVICE_ID = pn_device_id
               AND ds.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
              LEFT JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts ON cts.FILE_TRANSFER_ID = cv_cfg_file_id
               AND cts.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
             WHERE (cts.DEVICE_SETTING_PARAMETER_CD IS NOT NULL OR (dsp.DEVICE_SETTING_UI_CONFIGURABLE = 'Y' AND ds.DEVICE_SETTING_PARAMETER_CD IS NOT NULL))
             ORDER BY CASE WHEN REGEXP_LIKE(NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD), '^[0-9]+$') THEN TO_NUMBER(NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD)) ELSE 9999999999 END,
                    NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD);
    BEGIN
        SELECT ft.FILE_TRANSFER_ID
          INTO lv_cfg_file_id
          FROM DEVICE.DEVICE d
          JOIN DEVICE.FILE_TRANSFER ft
            ON ft.FILE_TRANSFER_NAME = CASE d.DEVICE_TYPE_ID
                    WHEN 0 THEN 'G4-DEFAULT-CFG'
                    WHEN 1 THEN 'G5-DEFAULT-CFG'
                    WHEN 6 THEN 'MEI-DEFAULT-CFG'
                    WHEN 12 THEN 'T2-DEFAULT-CFG'
                    ELSE 'DEFAULT-CFG-' || d.DEVICE_TYPE_ID || '-' || (
                       SELECT dsplv.DEVICE_SETTING_VALUE
                         FROM DEVICE.DEVICE_SETTING dsplv
                        WHERE dsplv.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
                          AND dsplv.DEVICE_ID = d.DEVICE_ID)
                END
            AND ft.FILE_TRANSFER_TYPE_CD IN(6,15,22)
            AND d.DEVICE_ID = pn_device_id;
        DBMS_LOB.CREATETEMPORARY(pl_file, TRUE, DBMS_LOB.CALL);
        FOR l_rec IN l_cur(lv_cfg_file_id) LOOP
            -- For now we just implement the property list format for Edge
            APPEND(l_rec.DEVICE_SETTING_PARAMETER_CD, pl_file);
            APPEND('=', pl_file);
            APPEND(l_rec.DEVICE_SETTING_VALUE, pl_file);
            APPEND(PKG_CONST.ASCII__LF, pl_file);
        END LOOP;
    END;

    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG)
    IS
        lv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_last_lock_utc_ts VARCHAR2(128);
    BEGIN
		SELECT DEVICE_NAME || '-CFG'
		INTO lv_file_transfer_name
		FROM DEVICE.DEVICE
		WHERE DEVICE_ID = pn_device_id;
		
		lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('DEVICE.FILE_TRANSFER', lv_file_transfer_name);

		UPDATE DEVICE.FILE_TRANSFER
		SET FILE_TRANSFER_CONTENT = pl_config_file
		WHERE FILE_TRANSFER_ID = (
			SELECT FILE_TRANSFER_ID FROM (
				SELECT FILE_TRANSFER_ID
				FROM DEVICE.FILE_TRANSFER
				WHERE FILE_TRANSFER_NAME = lv_file_transfer_name
					AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG
				ORDER BY CREATED_TS, FILE_TRANSFER_ID
			) WHERE ROWNUM = 1
		);
		
		IF SQL%NOTFOUND THEN
			INSERT INTO DEVICE.FILE_TRANSFER (
				FILE_TRANSFER_NAME,
				FILE_TRANSFER_TYPE_CD,
				FILE_TRANSFER_CONTENT
			) VALUES (
				lv_file_transfer_name,
				PKG_CONST.FILE_TYPE__CONFIG,
				pl_config_file
			);
		END IF;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_config_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE := pv_device_name || '-CFG';
        pl_config_file CLOB;
        ll_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
        ln_default_file_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
        lv_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        lv_old_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        ln_result_cd NUMBER;
        lv_error_message VARCHAR2(255);
        ln_setting_count NUMBER;
     BEGIN
        IF pn_new_device_type_id = 0 THEN
            lv_default_name := 'G4-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 1 THEN
            lv_default_name := 'G5-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 6 THEN
            lv_default_name := 'MEI-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 12 THEN
            lv_default_name := 'T2-DEFAULT-CFG';
            ln_default_file_type_id := 15;
        ELSE
            lv_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_new_property_list_version;
            ln_default_file_type_id := 22;
        END IF;

        SELECT MIN(FILE_TRANSFER_ID)
        INTO pn_file_transfer_id
        FROM DEVICE.FILE_TRANSFER
        WHERE FILE_TRANSFER_NAME = lv_config_name
            AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG;

        -- for legacy only
    IF pn_new_device_type_id in (0, 1, 6) THEN
        IF pn_file_transfer_id is NULL THEN
            SELECT FILE_TRANSFER_CONTENT
            INTO ll_file_transfer_content
            FROM (
                SELECT FILE_TRANSFER_CONTENT
                FROM DEVICE.FILE_TRANSFER
                WHERE FILE_TRANSFER_NAME = lv_default_name
                    AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                ORDER BY CREATED_TS
            ) WHERE ROWNUM = 1;

            SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
              INTO pn_file_transfer_id
              FROM DUAL;

            INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CONTENT)
            VALUES(pn_file_transfer_id, lv_config_name, PKG_CONST.FILE_TYPE__CONFIG, ll_file_transfer_content);
        END IF;

        RETURN;
    ELSE    -- ensure default config exists
        SELECT COUNT(*)
          INTO ln_setting_count
          FROM DEVICE.FILE_TRANSFER
         WHERE FILE_TRANSFER_NAME = lv_default_name
           AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id;
        IF ln_setting_count = 0 THEN
            RAISE_APPLICATION_ERROR(-20700, 'Default Config File ''' || lv_default_name || ''' does not exist');
        END IF;
    END IF;

        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM (SELECT DEVICE_ID
             FROM DEVICE.DEVICE
            WHERE DEVICE_NAME = pv_device_name
            ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;

        IF ln_device_id IS NOT NULL THEN
            SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
              INTO pn_old_property_list_version
              FROM DEVICE.DEVICE_SETTING ds
             WHERE ds.DEVICE_ID = ln_device_id
               AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
               AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
            IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(pn_old_property_list_version, -1) THEN
                SP_UPDATE_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__PROPERTY_LIST_VERSION, pn_new_property_list_version);
				
               -- Update all values that equal the default in the old version and where the default in the new version is differnt
               lv_old_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_old_property_list_version;
               MERGE INTO DEVICE.DEVICE_SETTING O
                 USING (
                       SELECT ln_device_id DEVICE_ID, cts.DEVICE_SETTING_PARAMETER_CD SETTING_NAME, cts.CONFIG_TEMPLATE_SETTING_VALUE SETTING_VALUE
                        FROM DEVICE.FILE_TRANSFER ft
                        JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
                          ON cts.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
                        JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts0
                          ON cts0.DEVICE_SETTING_PARAMETER_CD = cts.DEVICE_SETTING_PARAMETER_CD
                        JOIN DEVICE.FILE_TRANSFER ft0
                          ON cts0.FILE_TRANSFER_ID = ft0.FILE_TRANSFER_ID
                        JOIN DEVICE.DEVICE_SETTING ds
                          ON cts.DEVICE_SETTING_PARAMETER_CD = ds.DEVICE_SETTING_PARAMETER_CD
                       WHERE cts.CONFIG_TEMPLATE_SETTING_VALUE != cts0.CONFIG_TEMPLATE_SETTING_VALUE
                         AND cts0.CONFIG_TEMPLATE_SETTING_VALUE = ds.DEVICE_SETTING_VALUE
                         AND cts.DEVICE_SETTING_PARAMETER_CD NOT IN('50','51','52','60','61','62','63','64','80','81','70','100','101','102','103','104','105','106','107','108','200','201','202','203','204','205','206','207','208','300','301')
                         AND TO_NUMBER_OR_NULL(cts.DEVICE_SETTING_PARAMETER_CD) IS NOT NULL
                         AND ft.FILE_TRANSFER_NAME = lv_default_name
                         AND ft.FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                         AND ft0.FILE_TRANSFER_NAME = lv_old_default_name
                         AND ft0.FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                         AND ds.DEVICE_ID = ln_device_id) N
                      ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
                      WHEN MATCHED THEN
                       UPDATE
                          SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
                      WHEN NOT MATCHED THEN
                       INSERT (O.DEVICE_ID,
                               O.DEVICE_SETTING_PARAMETER_CD,
                               O.DEVICE_SETTING_VALUE)
                        VALUES(N.DEVICE_ID,
                               N.SETTING_NAME,
                               N.SETTING_VALUE
                        );
               --Create config file
               SP_CONSTRUCT_PROPERTIES_FILE(ln_device_id, pl_config_file);
               ll_file_transfer_content := LOB_TO_HEX_LONG(pl_config_file);
            END IF;
        END IF;

        -- handle change of property list version
        IF ll_file_transfer_content IS NULL AND (ln_device_id IS NULL OR pn_file_transfer_id IS NULL) THEN
            -- Use default config file
            SELECT FILE_TRANSFER_CONTENT
            INTO ll_file_transfer_content
            FROM (
                SELECT FILE_TRANSFER_CONTENT
                FROM DEVICE.FILE_TRANSFER
                WHERE FILE_TRANSFER_NAME = lv_default_name
                    AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                ORDER BY CREATED_TS
            ) WHERE ROWNUM = 1;
         END IF;

        IF ll_file_transfer_content IS NOT NULL THEN
            IF pn_file_transfer_id IS NULL THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CONTENT)
                   VALUES(pn_file_transfer_id, lv_config_name, PKG_CONST.FILE_TYPE__CONFIG, ll_file_transfer_content);
            ELSE
               UPDATE DEVICE.FILE_TRANSFER
                  SET FILE_TRANSFER_CONTENT = ll_file_transfer_content
                WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
            END IF;

            SP_UPDATE_DEVICE_SETTINGS(ln_device_id, pn_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
            IF ln_result_cd = PKG_CONST.RESULT__SUCCESS THEN
                pn_updated := PKG_CONST.BOOLEAN__TRUE;
            ELSE
                pn_updated := PKG_CONST.BOOLEAN__FALSE;
            END IF;
        ELSE
            pn_updated := PKG_CONST.BOOLEAN__FALSE;
        END IF;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER)
    IS
        ln_old_property_list_version NUMBER;
    BEGIN
        SP_INITIALIZE_CONFIG_FILE(pv_device_name, pn_new_device_type_id, pn_new_property_list_version, pn_file_transfer_id, pn_updated, ln_old_property_list_version);
    END;

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE
        )
    IS
        ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
    BEGIN
      ADD_PENDING_FILE_TRANSFER(
        pv_device_name,pv_file_transfer_name,pn_file_transfer_type_id,
        pc_data_type,pn_file_transfer_group,pn_file_transfer_packet_size,pn_execute_order,pn_command_id, pn_file_transfer_id,
        ll_command);
        
    END; -- stub for ADD_PENDING_FILE_TRANFER without returned data parameters
    
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
        )
    IS
        ln_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
          INTO pn_file_transfer_id, ln_dft_id, pn_command_id, ln_device_id
          FROM (SELECT DEVICE_ID
          FROM DEVICE.DEVICE
         WHERE DEVICE_NAME = pv_device_name
         ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
           VALUES(pn_file_transfer_id, pn_file_transfer_type_id, pv_file_transfer_name);
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
            VALUES(ln_dft_id, ln_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
           VALUES(pn_command_id, pv_device_name, pc_data_type, TO_CHAR(ln_dft_id), 'S', NVL(pn_execute_order, 999), SYSDATE)
           RETURNING TRIM(COMMAND) into pl_command;
    END;

    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT BLOB,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
         l_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT FILE_TRANSFER_ID, CREATED_TS
        INTO l_file_transfer_id, pd_file_transfer_create_ts
        FROM (
            SELECT FILE_TRANSFER_ID, CREATED_TS
            FROM DEVICE.FILE_TRANSFER
            WHERE FILE_TRANSFER_NAME = pv_file_transfer_name
                AND FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id
            ORDER BY CREATED_TS
        ) WHERE ROWNUM = 1;
        SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
          INTO l_dft_id
          FROM DEVICE.DEVICE_FILE_TRANSFER dft
          JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
         WHERE d.DEVICE_NAME = pv_device_name
           AND dft.FILE_TRANSFER_ID = l_file_transfer_id
           AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
           AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
       IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
         SELECT MAX(mcp.MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
           INTO pn_mcp_id
           FROM ENGINE.MACHINE_CMD_PENDING mcp
          WHERE mcp.MACHINE_ID = pv_device_name
            AND mcp.EXECUTE_CD IN('P', 'S')
            AND UPPER(mcp.DATA_TYPE) IN('7C', 'A4', 'C7', 'C8')
            AND TRIM(mcp.COMMAND) = TRIM(TO_CHAR(l_dft_id));
        ELSE
            SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
              INTO l_dft_id, l_device_id
              FROM (SELECT DEVICE_ID
              FROM DEVICE.DEVICE
             WHERE DEVICE_NAME = pv_device_name
             ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
             WHERE ROWNUM = 1;
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
              VALUES(l_dft_id, l_device_id, l_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        END IF;
        IF pn_mcp_id IS NULL THEN
            SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
              INTO pn_mcp_id
              FROM DUAL;
            INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
              VALUES(pn_mcp_id, pv_device_name, pc_data_type, TO_CHAR(l_dft_id), 'S', 999, SYSDATE);
        END IF;
        SP_GET_FILE_TRANSFER_BLOB(l_file_transfer_id, pl_file_transfer_content);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
               NULL; -- let variables be null
    END;

    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pl_file_content_hex FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    )
    IS
        ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
		ln_device_id := GET_DEVICE_ID_BY_NAME(pv_device_name, pd_file_transfer_ts);
		
		-- duplicate detection of already stored files re-delivered by QueueLayer
		SELECT MAX(DEVICE_FILE_TRANSFER_ID)
		INTO ln_device_file_transfer_id
		FROM DEVICE.DEVICE_FILE_TRANSFER
		WHERE DEVICE_ID = ln_device_id
			AND DEVICE_SESSION_ID = pn_session_id
			AND DEVICE_FILE_TRANSFER_TS = pd_file_transfer_ts;
		IF ln_device_file_transfer_id IS NOT NULL THEN
			RETURN;
		END IF;
		
        BEGIN
            SELECT FILE_TRANSFER_ID
              INTO pn_file_transfer_id
              FROM (SELECT /*+ index(FT INX_FILE_TRANSFER_TYPE_NAME) index(DFT IDX_DEVICE_FILE_TRANSFER_ID) */ ft.FILE_TRANSFER_ID
              FROM DEVICE.FILE_TRANSFER ft
              LEFT JOIN DEVICE.DEVICE_FILE_TRANSFER dft on ft.FILE_TRANSFER_ID = dft.FILE_TRANSFER_ID
             WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
               AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd
               AND NVL(dft.DEVICE_FILE_TRANSFER_DIRECT, 'I') = 'I'
               AND NVL(dft.DEVICE_FILE_TRANSFER_STATUS_CD, 1) = 1
               AND pc_overwrite_flag = 'Y'
             ORDER BY ft.FILE_TRANSFER_ID DESC)
             WHERE ROWNUM = 1;
            UPDATE DEVICE.FILE_TRANSFER SET FILE_TRANSFER_CONTENT = pl_file_content_hex
             WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER (
                    FILE_TRANSFER_ID,
                    FILE_TRANSFER_NAME,
                    FILE_TRANSFER_TYPE_CD,
                    FILE_TRANSFER_CONTENT)
                  VALUES(
                    pn_file_transfer_id,
                    pv_file_transfer_name,
                    pn_file_transfer_type_cd,
                    pl_file_content_hex);
        END;
        SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL
          INTO ln_device_file_transfer_id
          FROM DUAL;
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
            DEVICE_FILE_TRANSFER_ID,
            DEVICE_ID,
            FILE_TRANSFER_ID,
            DEVICE_FILE_TRANSFER_DIRECT,
            DEVICE_FILE_TRANSFER_STATUS_CD,
            DEVICE_FILE_TRANSFER_TS,
            DEVICE_SESSION_ID)
         VALUES(
            ln_device_file_transfer_id,
            ln_device_id,
            pn_file_transfer_id,
            'I',
            1,
            pd_file_transfer_ts,
            pn_session_id);
        IF pv_event_global_trans_cd IS NOT NULL THEN
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID, DEVICE_FILE_TRANSFER_ID)
              SELECT EVENT_ID, ln_device_file_transfer_id
                FROM DEVICE.EVENT
                WHERE EVENT_GLOBAL_TRANS_CD = pv_event_global_trans_cd;
        END IF;        
    END;
	
	FUNCTION GET_NORMALIZED_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2
	) RETURN VARCHAR2
	IS
		ln_call_in_start_min NUMBER;
		ln_call_in_end_min NUMBER;
		ln_call_in_window_min NUMBER;
		ln_call_in_time_min NUMBER;
		ln_call_in_window_start NUMBER;
		ln_call_in_window_hours NUMBER;
		ln_seed_4 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -4)), 0);
		ln_convert_time_zone NUMBER := PKG_CONST.BOOLEAN__TRUE;
	BEGIN
		ln_call_in_window_start := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_start_cd)), -1));
		ln_call_in_window_hours := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_hours_cd)), 0));
		IF ln_call_in_window_start BETWEEN 0 AND 2359 AND ln_call_in_window_hours BETWEEN 1 AND 23 THEN
			ln_call_in_start_min := FLOOR(ln_call_in_window_start / 100) * 60 + MOD(ln_call_in_window_start, 100);
			ln_call_in_window_min := ln_call_in_window_hours * 60;
			ln_convert_time_zone := PKG_CONST.BOOLEAN__FALSE;
		ELSE
			ln_call_in_start_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_start_min_cd));
			ln_call_in_end_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_end_min_cd));
			
			IF ln_call_in_start_min < 0 OR ln_call_in_start_min = ln_call_in_end_min THEN
				RETURN NULL;
			END IF;

			ln_call_in_window_min := MOD(ln_call_in_end_min - ln_call_in_start_min, 1440);
			IF ln_call_in_window_min < 0 THEN
				ln_call_in_window_min := ln_call_in_window_min + 1440;
			END IF;
		END IF;
			
		-- use last 4 digits of device serial number to calculate its call-in time
		ln_call_in_time_min := MOD(ln_call_in_start_min + ROUND(ln_call_in_window_min * ln_seed_4 / 9999), 1440);
		
		IF ln_convert_time_zone = PKG_CONST.BOOLEAN__TRUE THEN
			-- convert call-in time from server time zone to device local time zone
			ln_call_in_time_min := MOD(ln_call_in_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(pv_device_time_zone_guid, PKG_CONST.DB_TIME_ZONE), 1440);
			IF ln_call_in_time_min < 0 THEN
				ln_call_in_time_min := ln_call_in_time_min + 1440;
			END IF;
		END IF;
		
		RETURN LPAD(FLOOR(ln_call_in_time_min / 60), 2, '0') || LPAD(MOD(ln_call_in_time_min, 60), 2, '0');			
	END;	

    PROCEDURE NORMALIZE_SCHEDULE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_schedule_type DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_normalized_offset NUMBER,
		pn_result_cd IN OUT NUMBER,
		pv_schedule IN OUT DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	)
    IS
		lv_current_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_interval NUMBER;
		ln_min_allowed_interval NUMBER;
		lc_reoccurrence_type CHAR(1);
		ln_exists NUMBER;
		lv_normalized_time VARCHAR2(4);
		lv_schedule DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE := NULL;
    BEGIN
		lv_current_schedule := NVL(GET_DEVICE_SETTING(pn_device_id, pv_schedule_type), '0');
		IF lv_current_schedule = '0' THEN
			IF pv_schedule_type = PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED THEN
				lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__INTERVAL || PKG_CONST.SCHEDULE__FS || pn_normalized_offset || PKG_CONST.SCHEDULE__FS || DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EDGE_NON_ACTIVATED_CALL_IN_INTERVAL_SEC');
			ELSIF pv_schedule_type = PKG_CONST.DSP__SETTLEMENT_SCHEDULE THEN
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__WEEKLY || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || PKG_CONST.SCHEDULE__SUNDAY;
				END IF;
			ELSE
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		ELSE
			lc_reoccurrence_type := SUBSTR(lv_current_schedule, 1, 1);
			IF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__INTERVAL THEN
				ln_current_interval := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1)), -1);
				ln_min_allowed_interval := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INTERVAL_SCHEDULE_SEC'));
				IF ln_current_interval < ln_min_allowed_interval THEN
					ln_current_interval := ln_min_allowed_interval;
				END IF;
				lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || pn_normalized_offset || PKG_CONST.SCHEDULE__FS || ln_current_interval;
			ELSIF lc_reoccurrence_type IN (PKG_CONST.REOCCURRENCE_TYPE__MONTHLY, PKG_CONST.REOCCURRENCE_TYPE__WEEKLY) THEN
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1);
				END IF;
			ELSE
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		END IF;
		IF lv_schedule IS NOT NULL AND lv_schedule != lv_current_schedule THEN
			SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_schedule_type, lv_schedule, ln_exists);
			pn_result_cd := 1;
			pv_schedule := lv_schedule;
		END IF;		
    END;
	
    /**
    * r28 compatible call-in normalization version
    */ 
  PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	)
	IS
    ln_command_id       ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    lv_data_type        ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
    ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
  BEGIN
    SP_NORMALIZE_CALL_IN_TIME(
      pn_device_id, 
      pn_device_type_id, 
      pv_device_serial_cd, 
      pv_device_time_zone_guid, 
      pn_result_cd, 
      pv_activated_call_in_schedule, 
      pv_non_activ_call_in_schedule, 
      pv_settlement_schedule, 
      pv_dex_schedule,
      ln_command_id, 
      lv_data_type, 
      ll_command);
  END; -- SP_NORMALIZE_CALL_IN_TIME stub
  
  /**
    * r29 Call-in normalization version
    */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	)
	IS
		lv_return_msg VARCHAR2(2048);
		ll_file CLOB;
		ln_seed_5 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -5)), 0);
		ln_normalized_offset NUMBER;
		ln_exists NUMBER;
		ln_field_offset CONFIG_TEMPLATE_SETTING.FIELD_OFFSET%TYPE;
	BEGIN
		-- normalize device call-in time to avoid load peaks
		pn_result_cd := 0;
		pv_activated_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_non_activ_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_settlement_schedule := PKG_CONST.ASCII__NUL;
		pv_dex_schedule := PKG_CONST.ASCII__NUL;
		
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, 'GX_CALL_IN_NORMALIZER_START_MIN', 'GX_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule BETWEEN '0000' AND '0004' THEN
                pv_activated_call_in_schedule := '001' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
            ELSIF pv_activated_call_in_schedule BETWEEN '2356' AND '2359' THEN
                pv_activated_call_in_schedule := '234' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
            END IF;
			ln_field_offset := 172;
			SP_UPDATE_CONFIG_AND_RETURN(pn_device_id, ln_field_offset, pv_activated_call_in_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			IF pn_result_cd = 1 THEN
				SP_UPSERT_DEVICE_SETTING(pn_device_id, TO_CHAR(ln_field_offset), pv_activated_call_in_schedule, ln_exists);
			END IF;
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
			ln_normalized_offset := ROUND(86400 * ln_seed_5 / 99999);
			
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_activated_call_in_schedule);
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_non_activ_call_in_schedule);
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__SETTLEMENT_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_settlement_schedule);

			IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('NORMALIZE_EDGE_DEX_SCHEDULE_FLAG') = 'Y'
				AND NVL(GET_DEVICE_SETTING(pn_device_id, PKG_CONST.DSP__VMC_INTERFACE_TYPE), PKG_CONST.VMC_INTERFACE__STANDARD_MDB) = PKG_CONST.VMC_INTERFACE__STANDARD_MDB THEN
				NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__DEX_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
					'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_dex_schedule);
			END IF;
				
			IF pn_result_cd = 1 THEN
				SP_CONSTRUCT_PROPERTIES_FILE(pn_device_id, ll_file);
				SP_UPDATE_DEVICE_CONFIG_FILE(pn_device_id, LOB_TO_HEX_LONG(ll_file));
			END IF;
		END IF;
		
		UPDATE DEVICE.DEVICE
		SET CALL_IN_TIME_NORMALIZED_TS = SYSDATE
		WHERE DEVICE_ID = pn_device_id;
	END;

/**
  * r28 compatible version of SP_NEXT_PENDING_COMMAND
  * Does not support the stateful session attribute parameter
  */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL)
    IS
        lv_session_attributes VARCHAR2(20); -- contains character 'C' if configuration has been requested previously
    BEGIN
    
    lv_session_attributes := 'CGPE'; -- ensure that we don't continual do the same command
    SP_NEXT_PENDING_COMMAND(
      pv_device_name, 
      pn_command_id, 
      pv_data_type, 
      pr_command_bytes,
      pn_file_transfer_id,
      pv_file_transfer_name,
      pn_file_transfer_type_id,
      pl_file_transfer_content,
      pn_file_transfer_group_num,
      pn_file_transfer_pkt_size,
      pd_file_transfer_created_ts,
      pn_max_execute_order,
      pn_priority_command_id,
      lv_session_attributes);
  END; --    SP_NEXT_PENDING_COMMAND 
         
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for ESUDS Diagnostics, if existing diagnostics are beyond 16 hours old.
    */
    PROCEDURE SP_NPC_HELPER_ESUDS_DIAG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'E' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_oldest_diag DATE;
    BEGIN
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__ESUDS) AND INSTR(NVL(pv_session_attributes,'-'),'E') = 0 THEN        
            pv_session_attributes := NVL(pv_session_attributes,'') || 'E';       
            -- if any records for the given device are old, (no records = old records)       
            SELECT MIN(MOST_RECENT_PORT_DIAG_DATE) 
              INTO ld_oldest_diag
              FROM (
                  SELECT d.device_id, h.host_port_num, MAX(NVL(hds.host_diag_last_reported_ts, MIN_DATE)) AS MOST_RECENT_PORT_DIAG_DATE
                    FROM device d  
                    LEFT OUTER JOIN host h ON (d.device_id = h.device_id) 
                    LEFT OUTER JOIN host_diag_status hds ON (hds.host_id = h.host_id) 
                    WHERE d.device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS
                    AND d.device_name = pv_device_name
                    AND d.device_active_yn_flag = 'Y'
                    AND NVL(h.host_active_yn_flag,'Y') = 'Y'
                    AND h.host_port_num <> 0
                    GROUP BY d.device_id, h.host_port_num            
              );
    
            IF ld_oldest_diag < SYSDATE - 1 THEN           
                pv_data_type := '9A61';
                pl_command := '';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);                                   
            END IF;
		END IF;  
    END; -- SP_NPC_HELPER_ESUDS_DIAGNOSTICS

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for GPRS configuration files, if needed.
    */
    PROCEDURE SP_NPC_HELPER_MODEM(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration test has been requested previously, R if configuration requested       
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_update_ts DATE;
    BEGIN
        -- For Gx and MEI, test to see if modem data is outdated by 7 days. If so, send the appropriate message (both happen to be 87)	
        -- Some G4's only send garbage for modem info, so don't keep requesting it for G4 (PKG_CONST.DEVICE_TYPE__G4)
    	IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI) AND INSTR(NVL(pv_session_attributes,'-'),'C') = 0 THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'C';
            SELECT NVL(MAX(H.LAST_UPDATED_TS), MIN_DATE)
              INTO ld_last_update_ts
              FROM DEVICE.DEVICE D
              JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
               AND H.HOST_PORT_NUM = 2;
            IF ld_last_update_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_MODEM_INFO_REFRESH_HR')) / 24, 7) THEN				
                pv_data_type := '87';					
                IF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI) THEN 
                    pl_command := '4200000000000000FF';							
                ELSIF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__GX OR pn_device_type_id = PKG_CONST.DEVICE_TYPE__G4) THEN 
                    pl_command := '4400802C0000000190';
                END IF;
				pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
            END IF;
        END IF;
    END;

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for G4 configuration files, if existing ones are stale.
    */
    PROCEDURE SP_NPC_HELPER_STALE_CONFIG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'G' if GX configuration has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_config_ts DATE;
    BEGIN
        -- For MEI/Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI, PKG_CONST.DEVICE_TYPE__G4) AND INSTR(NVL(pv_session_attributes,'-'),'G') = 0 THEN              
	        pv_session_attributes := NVL(pv_session_attributes,'') || 'G';	        
	        IF NOT(pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI AND INSTR(NVL(pv_session_attributes,'-'), 'R') > 0 ) THEN	        	 
	        	-- Now check last configuration received timestamp 	        	
		        SELECT NVL(D.RECEIVED_CONFIG_FILE_TS, MIN_DATE)
		          INTO ld_last_config_ts
		          FROM DEVICE.DEVICE D 
		         WHERE D.DEVICE_NAME = pv_device_name 
		           AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
                IF ld_last_config_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CONFIG_REFRESH_HR')) / 24, 30) THEN				
                    pv_data_type := '87';
					-- same command for all devices handled by this procedure					
					pl_command := '4200000000000000FF';		
					pv_session_attributes := NVL(pv_session_attributes,'') || 'R';	
					UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);	                						
				END IF; -- actual test	
			END IF; -- 'R' flag			
    	END IF; -- 'G' flag    	
    END; -- SP_NPC_HELPER_STALE_CONFIG
    
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands to set the Gx verbosity flag 
    * on a non-remotely updatable device.
    */
    PROCEDURE SP_NPC_HELPER_PHILLY_COKE(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'P' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pv_firmware_version IN DEVICE.FIRMWARE_VERSION%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        l_is_philly_coke NUMBER;
    BEGIN
        -- For Gx test age of configuration data
        IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX) AND INSTR(NVL(pv_session_attributes,'-'),'P') = 0 AND REGEXP_LIKE(pv_firmware_version, 'USA-G5[0-9] ?[vV]4\.2\.(0|1[A-S]).*') THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'P';      
            pv_data_type := '88';
            pl_command := '4400807FE001';
            UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 2, pn_commands_inserted, pn_command_id);
		END IF;   
    END; -- SP_NPC_HELPER_PHILLY_COKE

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2 -- contains character 'C' if configuration has been requested previously       
        )
    IS
        ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
        ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ld_call_in_time_normalized_ts DEVICE.CALL_IN_TIME_NORMALIZED_TS%TYPE;
		lv_device_time_zone_guid VARCHAR2(60);
		ln_result_cd NUMBER;
		lv_activated_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_non_activ_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_settlement_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_dex_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        l_addr NUMBER;
        l_len NUMBER;
        l_total_len NUMBER;
        l_pos NUMBER;
        l_commands_inserted NUMBER;
		ln_sending_file_cnt NUMBER;
		ln_sending_file_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DOWNLOAD_LIMIT_APP_UPGRADE'));
    BEGIN
		-- throttle the number of concurrent app upgrade file downloads
		SELECT COUNT(1) INTO ln_sending_file_cnt
		FROM engine.machine_cmd_pending mcp
		JOIN device.device_file_transfer dft ON mcp.command = dft.device_file_transfer_id
		JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
		WHERE mcp.execute_cd = 'S'
			AND mcp.execute_date > SYSDATE - 15/1440
			AND UPPER(mcp.data_type) IN('7C', 'A4', 'C8')
			AND REGEXP_LIKE(mcp.command, '^[0-9]+$')
			AND ft.file_transfer_type_cd = PKG_CONST.FILE_TYPE__APP_UPGRADE;
	
        UPDATE ENGINE.MACHINE_CMD_PENDING MCP
           SET EXECUTE_CD = 'S',
               EXECUTE_DATE = SYSDATE
         WHERE MCP.MACHINE_ID = pv_device_name
           AND MCP.EXECUTE_ORDER <= pn_max_execute_order
           AND MCP.EXECUTE_CD IN('S', 'P')
		   AND (ln_sending_file_cnt < ln_sending_file_limit
				OR UPPER(MCP.DATA_TYPE) NOT IN('7C', 'A4', 'C8')
				OR UPPER(MCP.DATA_TYPE) IN('7C', 'A4', 'C8')
					AND REGEXP_LIKE(MCP.COMMAND, '^[0-9]+$')
					AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.COMMAND
							AND FT.FILE_TRANSFER_TYPE_CD != PKG_CONST.FILE_TYPE__APP_UPGRADE
					)
			)
           AND NOT EXISTS(
                        SELECT 1
                          FROM ENGINE.MACHINE_CMD_PENDING MCP2
                         WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
                           AND MCP2.EXECUTE_CD IN('S', 'P')
                           AND MCP.MACHINE_COMMAND_PENDING_ID != MCP2.MACHINE_COMMAND_PENDING_ID
                           AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
                           AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
                               OR MCP2.MACHINE_COMMAND_PENDING_ID = pn_priority_command_id
                               OR (MCP.MACHINE_COMMAND_PENDING_ID != NVL(pn_priority_command_id, 0)
                                    AND MCP.CREATED_TS > MCP2.CREATED_TS
                                    OR (MCP.CREATED_TS = MCP2.CREATED_TS AND MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))))
            RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, TRIM(COMMAND) INTO pn_command_id, pv_data_type, ll_command;
			
		IF pn_command_id IS NULL OR pv_data_type = '88' THEN
			SELECT D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SERIAL_CD, D.CALL_IN_TIME_NORMALIZED_TS, TZ.TIME_ZONE_GUID, D.FIRMWARE_VERSION
			  INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, ld_call_in_time_normalized_ts, lv_device_time_zone_guid, lv_firmware_version
              FROM DEVICE.DEVICE D
			  JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
			  JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
			  JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
		END IF;
		
		IF pn_command_id IS NULL THEN
			IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__EDGE)
				AND NVL(ld_call_in_time_normalized_ts, MIN_DATE) < SYSDATE - TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CALL_IN_NORMALIZATION_INTERVAL_HR')) / 24 THEN
				SP_NORMALIZE_CALL_IN_TIME(ln_device_id, ln_device_type_id, lv_device_serial_cd, lv_device_time_zone_guid, ln_result_cd, lv_activated_call_in_schedule, lv_non_activ_call_in_schedule, lv_settlement_schedule, lv_dex_schedule, pn_command_id, pv_data_type, ll_command);
				IF ln_result_cd = 1 THEN       
                    IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
                        pv_data_type := 'C7';
                        ADD_PENDING_FILE_TRANSFER(pv_device_name, pv_device_name || '-CFG-' || DBADMIN.TIMESTAMP_TO_MILLIS(SYSTIMESTAMP), 
                          PKG_CONST.FILE_TYPE__PROPERTY_LIST, pv_data_type, 0, 1024, 1, ln_command_id, ln_file_transfer_id, ll_command);
                        pn_command_id := ln_command_id; 
                        
                        UPDATE DEVICE.FILE_TRANSFER
                        SET FILE_TRANSFER_CONTENT = RAWTOHEX(DECODE(lv_settlement_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__SETTLEMENT_SCHEDULE || '=' || lv_settlement_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_non_activ_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED || '=' || lv_non_activ_call_in_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_activated_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED || '=' || lv_activated_call_in_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_dex_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__DEX_SCHEDULE || '=' || lv_dex_schedule || PKG_CONST.ASCII__LF))
                        WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
                    ELSE
                        UPDATE ENGINE.MACHINE_CMD_PENDING MCP
                           SET EXECUTE_CD = 'S',
                               EXECUTE_DATE = SYSDATE
                         WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
                    END IF;
                END IF;
            END IF;
            IF pn_command_id IS NULL THEN
                SP_NPC_HELPER_MODEM(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command); 
                IF pn_command_id IS NULL THEN
                    SP_NPC_HELPER_STALE_CONFIG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
                    IF pn_command_id IS NULL THEN
                        SP_NPC_HELPER_ESUDS_DIAG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
                        IF pn_command_id IS NULL THEN
                            SP_NPC_HELPER_PHILLY_COKE(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, lv_firmware_version, l_commands_inserted, ll_command);
                            IF pn_command_id IS NULL THEN
                                RETURN;
                            END IF;
                        END IF;
                    END IF;
                END IF;
            END IF;
        END IF;
        IF UPPER(pv_data_type) IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(ll_command, '^[0-9]+$') THEN
            SELECT ft.FILE_TRANSFER_ID, ft.FILE_TRANSFER_NAME, ft.FILE_TRANSFER_TYPE_CD,
                   dft.DEVICE_FILE_TRANSFER_GROUP_NUM, dft.DEVICE_FILE_TRANSFER_PKT_SIZE, dft.CREATED_TS
              INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
                   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
              FROM DEVICE.DEVICE_FILE_TRANSFER dft
              JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
             WHERE DEVICE_FILE_TRANSFER_ID = TO_NUMBER(ll_command);

            SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
        ELSIF UPPER(pv_data_type) = '88' THEN
            l_addr :=to_number(substr(ll_command, 3,8),'XXXXXXXX');
            IF substr(ll_command, 1, 2) = '42' THEN -- EEROM: get data from file transfer            
                l_len := to_number(substr(ll_command, 11,8),'XXXXXXXX');
    
                IF ln_device_type_id in (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
                      l_pos :=l_addr*2;
                ELSE
                      l_pos :=l_addr;
                END IF;
    
                IF l_pos>0 THEN
                    l_pos :=l_pos+1;
                END IF;
    
                select file_transfer_id into pn_file_transfer_id
                from (
                    select file_transfer_id from device.file_transfer
                    where file_transfer_name = pv_device_name||'-CFG'
                        and file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG
                    order by created_ts
                ) where rownum = 1;
    
                SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
                l_total_len :=UTL_RAW.LENGTH(pl_file_transfer_content);
                IF (l_pos+l_len) > l_total_len THEN
                        pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1,10)), UTL_RAW.SUBSTR(pl_file_transfer_content, l_pos)) ;
                ELSE
                        pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1,10)), UTL_RAW.SUBSTR(pl_file_transfer_content, l_pos, l_len)) ;
                END IF;
            ELSE
                pr_command_bytes := HEXTORAW(ll_command);
            END IF;
        ELSE
            pr_command_bytes := HEXTORAW(ll_command);
        END IF;
    END;

    /**
      * r28 compatible version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER)
    IS
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
      UPSERT_PENDING_COMMAND(pv_device_name,pv_date_type,pv_command,'P',pv_execute_order,pn_rows_inserted,ln_command_id);
    END;
    
    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
      ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        select SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL into ln_command_id from DUAL;
        
        -- This isn't atomic so could result in duplicate pending commands but it's not critical to avoid duplicates only nice
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_DATE, EXECUTE_ORDER)
            SELECT ln_command_id, pv_device_name, pv_data_type, pv_command, pv_execute_cd, DECODE(pv_execute_cd, 'S', SYSDATE, NULL), pv_execute_order FROM DUAL
             WHERE NOT EXISTS(SELECT 1 FROM ENGINE.MACHINE_CMD_PENDING
                               WHERE MACHINE_ID = pv_device_name
                                 AND DATA_TYPE = pv_data_type
                                 AND COMMAND = pv_command
                                 AND EXECUTE_CD in ('P', 'S'));
        
        pn_rows_inserted := SQL%ROWCOUNT;
        
        if pn_rows_inserted = 1 THEN
          pn_command_id := ln_command_id;
        END IF;
    END;

  /**
    * pre-R28 compatible version
    */
     PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999)
    IS
        ln_rows_inserted PLS_INTEGER;
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        UPSERT_PENDING_COMMAND(pv_device_name, pv_date_type, pv_command, 'P', pv_execute_order, ln_rows_inserted, ln_command_id);
    END;
    
    PROCEDURE CONFIG_POKE
    (pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE)
    IS
        l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
        l_file_len NUMBER;
        l_num_parts NUMBER;
        l_pos NUMBER;
        l_poke_size NUMBER:=200;
        l_len NUMBER;
    BEGIN
        IF pv_device_type_id in (0, 1) THEN
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND EXECUTE_ORDER > 2
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 0);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 1);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 2);
        ELSE
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            select FILE_CONTENT_LENGTH(rowid)/2 into l_file_len
            from (
                select rowid from device.file_transfer
                where file_transfer_name = pv_device_name||'-CFG'
                    and file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG
                order by created_ts
            ) where rownum = 1;

            l_num_parts:= CEIL(l_file_len/l_poke_size)-1;

            FOR i in 0 .. l_num_parts LOOP
                l_pos:= i*l_poke_size;
                IF (l_pos+l_poke_size) > l_file_len THEN
                    l_len:= l_file_len-l_pos;
                ELSE
                    l_len:= l_poke_size;
                END IF;

                l_command:= '42'||LPAD(PKG_CONVERSIONS.TO_HEX(l_pos), 8, '0')||LPAD(PKG_CONVERSIONS.TO_HEX(l_len), 8, '0');

                UPSERT_PENDING_COMMAND(pv_device_name, '88', l_command, i);
            END LOOP;

        END IF;
    END;
    
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_NAME = pv_device_name
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_NAME ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_NAME = pv_device_name)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
            IF ln_device_id IS NULL THEN
                SELECT MAX(DEVICE_ID)
                  INTO ln_device_id
                  FROM (SELECT DEVICE_ID
                          FROM DEVICE.DEVICE
                         WHERE DEVICE_NAME = pv_device_name
                           AND CREATED_TS > pd_effective_date
                         ORDER BY CREATED_TS ASC)
                 WHERE ROWNUM = 1;
            END IF;
        END IF;
        RETURN ln_device_id;        
    END;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_SERIAL_CD = pv_device_serial_cd
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_SERIAL_CD ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
        END IF;
        RETURN ln_device_id;        
    END;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.4-USADBP.sql?rev=HEAD
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD (NAME VARCHAR2(200));

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD) 
SELECT DISTINCT TO_CHAR(FIELD_OFFSET) FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
WHERE DEVICE_TYPE_ID IN (0, 6)
	AND NOT EXISTS (SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = TO_CHAR(CTS.FIELD_OFFSET));
	
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = DEVICE_SETTING_PARAMETER_CD
WHERE DEVICE_TYPE_ID IN (0, 6)
	AND NAME IS NULL
	AND FIELD_OFFSET < 512;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET DEVICE_SETTING_PARAMETER_CD = TO_CHAR(FIELD_OFFSET)
WHERE DEVICE_TYPE_ID IN (0, 6)
	AND DEVICE_SETTING_PARAMETER_CD != TO_CHAR(FIELD_OFFSET)
	AND FIELD_OFFSET < 512;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = ALT_NAME
WHERE DEVICE_TYPE_ID = 13
	AND NAME IS NULL
	AND ALT_NAME IS NOT NULL;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET ALT_NAME = NULL
WHERE DEVICE_TYPE_ID = 13
	AND ALT_NAME IS NOT NULL;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET ALT_NAME = NULL
WHERE ALT_NAME IS NOT NULL
	AND ALT_NAME = NAME;
	
COMMIT;

DECLARE
	CURSOR cur IS 
		select ds.device_id, ds.device_setting_parameter_cd old_key, cts.device_setting_parameter_cd new_key
		from device.device_setting ds
		join device.device d on ds.device_id = d.device_id
		join device.config_template_setting cts on ds.device_setting_parameter_cd = cts.name
			and decode(d.device_type_id, 6, 6, 0) = cts.device_type_id
		join device.device_setting_parameter dsp on ds.device_setting_parameter_cd = dsp.device_setting_parameter_cd
		where d.device_type_id in (0, 1, 6)
			and dsp.device_setting_ui_configurable = 'Y';
BEGIN
	FOR rec_cur IN cur LOOP
		BEGIN
			UPDATE DEVICE.DEVICE_SETTING
			SET DEVICE_SETTING_PARAMETER_CD = rec_cur.new_key
			WHERE DEVICE_ID = rec_cur.device_id
				AND DEVICE_SETTING_PARAMETER_CD = rec_cur.old_key;
			COMMIT;
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;
	END LOOP;
END;
/

