WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.0-USARDB.User.Creation.sql?rev=HEAD
CREATE ROLE USAT_DMS_ROLE;

DECLARE
	lv_dms_user_pwd VARCHAR2(30) := 'DMS_USER_1';
	ln_i NUMBER;
BEGIN
	FOR ln_i IN 1..5 LOOP
		BEGIN
            EXECUTE IMMEDIATE 'CREATE USER DMS_USER_' || ln_i || ' IDENTIFIED BY ' || lv_dms_user_pwd || ' DEFAULT TABLESPACE USATAPPS_DEFAULT_TBS TEMPORARY TABLESPACE TEMPTS1';
        EXCEPTION
            WHEN OTHERS THEN
                IF SQLCODE != -1920 THEN
                    RAISE;
                END IF;
        END;			
		EXECUTE IMMEDIATE 'ALTER USER DMS_USER_' || ln_i || ' PROFILE USAT_APPS';
		EXECUTE IMMEDIATE 'GRANT CONNECT TO DMS_USER_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT CREATE SESSION TO DMS_USER_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT USAT_DMS_ROLE TO DMS_USER_' || ln_i;
		EXECUTE IMMEDIATE 'ALTER USER DMS_USER_' || ln_i || ' DEFAULT ROLE USAT_DMS_ROLE';
	END LOOP;
END;
/

DECLARE
	lv_db VARCHAR2(30);
BEGIN
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '')
	INTO lv_db
	FROM GLOBAL_NAME;
	
	IF lv_db = 'USARDB' THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'usaapr21', '192.168.77.71' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'usaapr22', '192.168.77.72' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_3', 'usaapr23', '192.168.77.73' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_3');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_4', 'usaapr24', '192.168.77.74' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_4');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_5', 'usaapr25', '192.168.77.75' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_5');
	ELSIF lv_db = 'USASTR01' THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'eccapr01', '192.168.4.41' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'eccapr01', '192.168.4.41' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_3', 'eccapr01', '192.168.4.41' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_3');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_4', 'eccapr01', '192.168.4.41' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_4');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_5', 'eccapr01', '192.168.4.41' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_5');	
	ELSIF lv_db = 'USADEV02' THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'intapr01', '10.0.0.246' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'intapr01', '10.0.0.246' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_3', 'intapr01', '10.0.0.246' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_3');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_4', 'intapr01', '10.0.0.246' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_4');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_5', 'intapr01', '10.0.0.246' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_5');	
	ELSIF lv_db IN ('USADEV03', 'USADEV04') THEN
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_1', 'devapr01', '10.0.0.248' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_1');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_2', 'devapr01', '10.0.0.248' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_2');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_3', 'devapr01', '10.0.0.248' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_3');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_4', 'devapr01', '10.0.0.248' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_4');
		
		INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS (APPS_USERNAME, HOST_NAME, HOST_IP) 
		SELECT 'DMS_USER_5', 'devapr01', '10.0.0.248' FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS WHERE APPS_USERNAME = 'DMS_USER_5');
	END IF;
	
	COMMIT;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.0-USARDB.1.sql?rev=HEAD
CREATE OR REPLACE PROCEDURE CORP.LICENSE_INSERT 
   (l_license_id OUT LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE
)
IS
BEGIN
	 SELECT LICENSE_SEQ.NEXTVAL INTO l_license_id FROM DUAL;
	 INSERT INTO LICENSE(LICENSE_ID, TITLE, DESCRIPTION)
	     VALUES(l_license_id, l_title, l_desc);
END LICENSE_INSERT;
/

GRANT EXECUTE ON CORP.LICENSE_INSERT TO USAT_DMS_ROLE, USATECH_UPD_TRANS;

CREATE OR REPLACE PROCEDURE CORP.LICENSE_UPDATE (
    l_license_id IN LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE)
IS
BEGIN
	 UPDATE LICENSE SET (TITLE, DESCRIPTION) =
	     (SELECT l_title, l_desc FROM DUAL) WHERE LICENSE_ID = l_license_id;
END;
/

GRANT EXECUTE ON CORP.LICENSE_UPDATE TO USAT_DMS_ROLE, USATECH_UPD_TRANS;

CREATE OR REPLACE PROCEDURE CORP.LICENSE_PF_UPDATE 
(
  pn_license_id CORP.LICENSE_PROCESS_FEES.LICENSE_ID%TYPE,
  pn_trans_type_id CORP.LICENSE_PROCESS_FEES.TRANS_TYPE_ID%TYPE,
  pn_fee_percent CORP.LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_fee_amount CORP.LICENSE_PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_min_amount CORP.LICENSE_PROCESS_FEES.MIN_AMOUNT%TYPE
) AS 
BEGIN
    DELETE FROM CORP.LICENSE_PROCESS_FEES
     WHERE LICENSE_ID = pn_license_id
       AND TRANS_TYPE_ID = pn_trans_type_id;
    IF NVL(pn_fee_percent, 0) != 0 OR NVL(pn_fee_amount, 0) != 0 OR NVL(pn_min_amount, 0) > 0 THEN
        INSERT INTO CORP.LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT)
          VALUES(pn_license_id, pn_trans_type_id, NVL(pn_fee_percent, 0), NVL(pn_fee_amount, 0),  NVL(pn_min_amount, 0));
    END IF;
END;
/

GRANT EXECUTE ON CORP.LICENSE_PF_UPDATE TO USAT_DMS_ROLE, USATECH_UPD_TRANS;

CREATE OR REPLACE PROCEDURE CORP.LICENSE_SF_UPDATE 
(
  pn_license_id CORP.LICENSE_SERVICE_FEES.LICENSE_ID%TYPE,
  pn_fee_id CORP.LICENSE_SERVICE_FEES.FEE_ID%TYPE,
  pn_amount CORP.LICENSE_SERVICE_FEES.AMOUNT%TYPE,
  pn_immediate CORP.LICENSE_SERVICE_FEES.START_IMMEDIATELY_FLAG%TYPE
) AS 
BEGIN
    DELETE FROM CORP.LICENSE_SERVICE_FEES
     WHERE LICENSE_ID = pn_license_id
       AND FEE_ID = pn_fee_id;
    IF NVL(pn_amount, 0) != 0 THEN
        INSERT INTO CORP.LICENSE_SERVICE_FEES(LICENSE_ID, FEE_ID, AMOUNT, START_IMMEDIATELY_FLAG)
          VALUES(pn_license_id, pn_fee_id, NVL(pn_amount, 0), NVL(pn_immediate, 'N'));
    END IF;
END;
/

GRANT EXECUTE ON CORP.LICENSE_SF_UPDATE TO USAT_DMS_ROLE, USATECH_UPD_TRANS;

/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/TERMINAL_COLUMN_MAP_INS.prc?rev=1.3
CREATE OR REPLACE PROCEDURE REPORT.TERMINAL_COLUMN_MAP_INS(
	l_terminal_id IN REPORT.TERMINAL_COLUMN_MAP.TERMINAL_ID%TYPE,
	l_mdb_number IN REPORT.TERMINAL_COLUMN_MAP.MDB_NUMBER%TYPE,
	l_vend_column IN REPORT.TERMINAL_COLUMN_MAP.VEND_COLUMN%TYPE
)
IS
BEGIN
	INSERT INTO REPORT.TERMINAL_COLUMN_MAP (TERMINAL_ID, MDB_NUMBER, VEND_COLUMN)
    SELECT l_terminal_id, l_mdb_number, l_vend_column
	FROM DUAL
	WHERE NOT EXISTS (
		SELECT 1
		FROM REPORT.TERMINAL_COLUMN_MAP
		WHERE TERMINAL_ID = l_terminal_id
			AND MDB_NUMBER = l_mdb_number
	);
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.0-USARDB.Privileges.sql?rev=HEAD
GRANT SELECT ON CORP.business_unit                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.currency                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank_terminal                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.payment_schedule                          TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.terminal                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_bank_acct                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_customer                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_user                                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_license                        TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_nbr                               TO USAT_DMS_ROLE;

GRANT SELECT ON REPORT.eport                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.location                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_column_map                     TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_changes                        TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_eport                          TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_new_terminals                        TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_activity                             TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.VW_TERMINAL_COLUMN_MAP_COUNT            TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_terminal_eport                       TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_current_device_location              TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.VW_CURRENT_DEVICE_LOCATION			   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.VW_UNASSIGNED_EPORTS			           TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_terminal         			           TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.region         			               TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_region         			       TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.machine         			               TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.USER_LOGIN      			               TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.vw_license                                TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license                                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer_license                            TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_changed_customer_bank                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_approved_payments                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.VW_PENDING_OR_LOCKED_PAYMENTS             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.pay_cycle                                 TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_no_license                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_adjustments                            TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_payment_process_fees                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.batch                                     TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.ledger                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_payment_service_fees                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.doc                                       TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.DEALER_EPORT                              TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.PROCESS_FEES                              TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.SERVICE_FEES                              TO USAT_DMS_ROLE;

GRANT INSERT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT UPDATE ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT DELETE ON REPORT.TERMINAL_COLUMN_MAP                        TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.accept_customer                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_del                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_upd                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.terminal_reassign_bank_account           TO USAT_DMS_ROLE;

GRANT EXECUTE ON REPORT.accept_new_terminal                    TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.accept_terminal_changes                TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_eport_upd                     TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_column_map_del                TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_column_map_ins                TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.TERMINAL_COLUMN_MAP_COPY_MODEL         TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.get_eports                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.dealer_ins                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_upd                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_del                               TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.license_ins                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.license_upd                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.license_del                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.accept_customer_bank                      TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.payments_pkg                              TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_license_ins                        TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_license_del                        TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.customer_bank_del                         TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_bank_ins                         TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_bank_upd                         TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.received_custom_license                   TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.received_license                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON report.GET_OR_CREATE_EPORT                     TO USAT_DMS_ROLE;
GRANT EXECUTE ON corp.DEALER_EPORT_UPD                        	TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.business_unit                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.currency                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank_terminal                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.payment_schedule                          TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.device_type                             TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.eport                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.location                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_changes                        TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_eport                          TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_new_terminals                        TO USAT_DMS_ROLE;

GRANT EXECUTE ON REPORT.accept_new_terminal                    TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.accept_terminal_changes                TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.vw_current_bank_acct                      TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans                                   TO USAT_DMS_ROLE;

GRANT UPDATE ON REPORT.terminal                                TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.terminal_reassign_bank_account           TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_eport_upd                     TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_addr_seq                         TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_nbr                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.terminal                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_license                        TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_customer                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_license                                TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_user                                   TO USAT_DMS_ROLE;

GRANT INSERT ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT UPDATE ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT UPDATE ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.accept_customer                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_del                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_upd                             TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.vw_current_bank_acct                      TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans                                   TO USAT_DMS_ROLE;

-- UPDATE PRIVILEGES
GRANT UPDATE ON REPORT.terminal                                TO USAT_DMS_ROLE;

-- EXECUTE PRIVILEGES
GRANT EXECUTE ON CORP.terminal_reassign_bank_account           TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_eport_upd                     TO USAT_DMS_ROLE;


--------------------------------------------------------------------------------
-- Grants for "Customer Management"

-- SELECT PRIVILEGES
GRANT SELECT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_addr_seq                         TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_nbr                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.terminal                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_license                        TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_customer                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_license                                TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license                                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_process_fees                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_service_fees                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.FEES                                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.LICENSE_SERVICE_FEES                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_user                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans_type                              TO USAT_DMS_ROLE;

GRANT INSERT ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT UPDATE ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT UPDATE ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.accept_customer                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_del                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_upd                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.LICENSE_DEL                              TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_INSERT                           TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_UPDATE                           TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_PF_UPDATE                        TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_SF_UPDATE                        TO USAT_DMS_ROLE;

/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/RecompileInvalidObjects.sql?rev=HEAD
DECLARE
    CURSOR l_cur IS
select 'alter '||decode(object_type,'PACKAGE BODY'
,'package '||owner||'.'||object_name||
'  compile body'
,object_type||' '||owner||'.'||object_name
||' compile') sql_text
from dba_objects 
where status = 'INVALID' and object_type NOT IN('MATERIALIZED VIEW', 'SYNONYM')
and owner in (
	'APP_EXEC_HIST',
	'APP_LAYER',
	'APP_LOG',
	'APP_USER',
	'AUTHORITY',
	'CORP',
	'DBADMIN',
	'DEVICE',
	'ENGINE',
	'FOLIO_CONF',
	'FRONT',
	'G4OP',
	'LOCATION',
	'PSS',
	'RDW_LOADER',
	'REAL_TIME',
	'RECON',
	'REPL_MGR',
	'REPORT',
	'SONY',
	'TAZDBA',
	'TRACKING',
	'UITOOL',
	'UPDATER',
	'USAQA',
	'USAT_REPORTING',
	'WEB_CONTENT'
);
BEGIN
    FOR i IN 1..2 LOOP
        FOR l_rec in l_cur LOOP
            DBMS_OUTPUT.PUT_LINE('Executing "' || l_rec.sql_text || '"');
            BEGIN
                EXECUTE IMMEDIATE l_rec.sql_text;
            EXCEPTION
                WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('Failed "' || l_rec.sql_text || '":');
                    DBMS_OUTPUT.PUT_LINE(SQLERRM);
            END;
        END LOOP;
    END LOOP;
END;
/

