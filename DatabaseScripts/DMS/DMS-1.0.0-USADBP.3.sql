DECLARE
	CURSOR cur IS 
		select device_id, device_setting_parameter_cd
		from device.device_setting
		where DBADMIN.PKG_UTL.EQL(device_setting_value, trim(device_setting_value)) = 'N';
BEGIN
	FOR rec_cur IN cur LOOP
		UPDATE DEVICE.DEVICE_SETTING
		SET DEVICE_SETTING_VALUE = TRIM(DEVICE_SETTING_VALUE)
		WHERE DEVICE_ID = rec_cur.device_id
			AND DEVICE_SETTING_PARAMETER_CD = rec_cur.device_setting_parameter_cd;
		COMMIT;
	END LOOP;
END;
/

DECLARE
	LV_FIRMWARE_VERSION DEVICE.FIRMWARE_VERSION%TYPE;
	CURSOR cur IS 
		select device_id, device_type_id
		from device.device
		order by device_id desc;
BEGIN
	FOR rec_cur IN cur LOOP
		SELECT MAX(device_setting_value)
		INTO LV_FIRMWARE_VERSION
		FROM DEVICE.DEVICE_SETTING
		WHERE DEVICE_ID = rec_cur.device_id
			AND device_setting_parameter_cd = DECODE(rec_cur.device_type_id, 5, 'eSuds Application Version', 11, 'Public_PC_Version', 'Firmware Version');
			
		IF LV_FIRMWARE_VERSION IS NULL AND rec_cur.device_type_id = 11 THEN
			SELECT MAX(device_setting_value)
			INTO LV_FIRMWARE_VERSION
			FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = rec_cur.device_id
				AND device_setting_parameter_cd = 'SoftwareVersion';
		END IF;
		
		UPDATE DEVICE.DEVICE
		SET FIRMWARE_VERSION = LV_FIRMWARE_VERSION
		WHERE DEVICE_ID = rec_cur.device_id
			AND DBADMIN.PKG_UTL.EQL(LV_FIRMWARE_VERSION, FIRMWARE_VERSION) = 'N';
		COMMIT;
	END LOOP;
END;
/

BEGIN 
	sys.dbms_scheduler.disable('"PSS"."CONSUMER PERMISSION ACTION"');
END;
/
