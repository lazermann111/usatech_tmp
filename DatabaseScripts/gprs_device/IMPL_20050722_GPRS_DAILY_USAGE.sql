/* 
Description:


This implementation will create the gprs_daily_usage table which will be used on the 
sim card device usage report. It also adds four columns on gprs_device to retrieve 
puk/pin info from Cigular web site. 

Created By:
Wei Guo 

Date:
2005-07-22

Grant Privileges (run as):
SYSTEM 

*/ 



-- Create gprs_daily_usage table under device schema.
@DEVICE.GPRS_DAILY_USAGE_ddl.sql;
-- Add pin/puk columns on gprs_device
@DEVICE.GPRS_DEVICE_add_columns.sql;

