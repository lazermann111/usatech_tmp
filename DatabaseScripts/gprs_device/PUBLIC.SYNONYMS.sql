-- Create synonym GPRS_DEVICE_STATE
CREATE PUBLIC SYNONYM gprs_device_state
  FOR device.gprs_device_state
/

-- Create synonym GPRS_DEVICE_HIST
CREATE PUBLIC SYNONYM gprs_device_hist
  FOR device.gprs_device_hist
/

-- Create synonym GPRS_DEVICE
CREATE PUBLIC SYNONYM gprs_device
  FOR device.gprs_device
/
