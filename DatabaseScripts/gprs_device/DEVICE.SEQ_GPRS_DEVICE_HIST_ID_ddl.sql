-- Start of DDL Script for Sequence DEVICE.SEQ_GPRS_DEVICE_HIST_ID
-- Generated 2/16/2005 1:52:20 PM from DEVICE@USADBD03.USATECH.COM

CREATE SEQUENCE device.seq_gprs_device_hist_id
  INCREMENT BY 1
  START WITH 37
  MINVALUE 1
  MAXVALUE 9999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- Grants for Sequence
GRANT SELECT ON device.seq_gprs_device_hist_id TO web_user
/

-- End of DDL Script for Sequence DEVICE.SEQ_GPRS_DEVICE_HIST_ID

