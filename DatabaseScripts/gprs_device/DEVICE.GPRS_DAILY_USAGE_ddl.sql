-- Start of DDL Script for Table DEVICE.GPRS_DAILY_USAGE
-- Generated 7/22/2005 1:57:04 PM from DEVICE@USADBD03

CREATE TABLE device.gprs_daily_usage
    (report_date                    DATE,
    iccid                          NUMBER(20,0),
    msisdn                         NUMBER(16,0),
    included_usage                 NUMBER(10,0),
    aws_kb_usage                   NUMBER(10,0),
    roaming_kb_usage               NUMBER(10,0),
    can_kb_usage                   NUMBER(10,0),
    int_kb_usage                   NUMBER(10,0),
    total_kb_usage                 NUMBER(10,0))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/




-- Indexes for GPRS_DAILY_USAGE

CREATE INDEX device.ix_daily_usage_report_date ON device.gprs_daily_usage
  (
    report_date                     ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

CREATE INDEX device.ix_daily_usage_iccid ON device.gprs_daily_usage
  (
    iccid                           ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/


grant select, update, insert on device.gprs_daily_usage to web_user;


-- End of DDL Script for Table DEVICE.GPRS_DAILY_USAGE

