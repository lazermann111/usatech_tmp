-- Start of DDL Script for Table DEVICE.GPRS_DEVICE_HIST
-- Generated 2/16/2005 1:38:42 PM from DEVICE@USADBD03.USATECH.COM

CREATE TABLE device.gprs_device_hist
    (gprs_device_hist_id            NUMBER(20,0) NOT NULL,
    gprs_device_id                 NUMBER(20,0) NOT NULL,
    gprs_device_state_id           NUMBER(1,0) DEFAULT 1  NOT NULL,
    ordered_by                     VARCHAR2(128),
    ordered_ts                     DATE,
    provider_order_id              VARCHAR2(64),
    ordered_notes                  VARCHAR2(2048),
    iccid                          NUMBER(20,0) NOT NULL,
    imsi                           NUMBER(16,0),
    allocated_by                   VARCHAR2(128),
    allocated_ts                   DATE,
    allocated_to                   VARCHAR2(128),
    allocated_notes                VARCHAR2(2048),
    billable_to_name               VARCHAR2(256),
    billable_to_notes              VARCHAR2(2048),
    activated_by                   VARCHAR2(128),
    activated_ts                   DATE,
    activated_notes                VARCHAR2(2048),
    provider_activation_id         VARCHAR2(64),
    provider_activation_ts         DATE,
    msisdn                         NUMBER(16,0),
    phone_number                   NUMBER(16,0),
    rate_plan_name                 VARCHAR2(64),
    assigned_by                    VARCHAR2(128),
    assigned_ts                    DATE,
    assigned_notes                 VARCHAR2(2048),
    device_id                      NUMBER(20,0),
    imei                           NUMBER(15,0),
    device_type_name               VARCHAR2(1024),
    device_firmware_name           VARCHAR2(1024),
    rssi                           VARCHAR2(64),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL
  ,
  CONSTRAINT PK_GPRS_DEVICE_HIST
  PRIMARY KEY (gprs_device_hist_id)
  USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     131072
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  ))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     131072
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

-- Grants for Table
GRANT DELETE ON device.gprs_device_hist TO web_user
/
GRANT INSERT ON device.gprs_device_hist TO web_user
/
GRANT SELECT ON device.gprs_device_hist TO web_user
/
GRANT UPDATE ON device.gprs_device_hist TO web_user
/

-- Indexes for DEVICE.GPRS_DEVICE_HIST

CREATE INDEX device.idx_gprs_hist_gprs_device_id ON device.gprs_device_hist
  (
    gprs_device_id                  ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

CREATE INDEX device.idx_gprs_device_hist_iccid ON device.gprs_device_hist
  (
    iccid                           ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

CREATE INDEX device.idx_gprs_device_hist_imsi ON device.gprs_device_hist
  (
    imsi                            ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

CREATE INDEX device.idx_gprs_device_hist_device_id ON device.gprs_device_hist
  (
    device_id                       ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/



-- Constraints for DEVICE.GPRS_DEVICE_HIST



-- Triggers for DEVICE.GPRS_DEVICE_HIST

CREATE TRIGGER device.trbi_gprs_device_hist
BEFORE INSERT
ON device.gprs_device_hist
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    IF  :new.gprs_device_hist_id IS NULL THEN
        SELECT seq_gprs_device_hist_id.nextval
        INTO :new.gprs_device_hist_id
        FROM dual;
    END IF;

    SELECT  sysdate, 
            user, 
            sysdate, 
            user
      INTO  :new.created_ts,
            :new.created_by,
            :new.last_updated_ts,
            :new.last_updated_by
      FROM  dual;
END;
/

CREATE TRIGGER device.trbu_gprs_device_hist
BEFORE UPDATE
ON device.gprs_device_hist
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    SELECT  sysdate, user
    INTO    :new.last_updated_ts, 
            :new.last_updated_by
    FROM    dual;    
END;
/


-- Comments for DEVICE.GPRS_DEVICE_HIST

COMMENT ON TABLE device.gprs_device_hist IS 'Contains history of a GPRM modem in cases where the new state is less-activated that the prior.'
/

-- End of DDL Script for Table DEVICE.GPRS_DEVICE_HIST

-- Foreign Key
ALTER TABLE device.gprs_device_hist
ADD CONSTRAINT fk_gprs_device_hist_state_id FOREIGN KEY (gprs_device_state_id)
REFERENCES DEVICE.gprs_device_state (gprs_device_state_id)
/
-- End of DDL script for Foreign Key(s)
