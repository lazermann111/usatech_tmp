-- Start of DDL Script for Table DEVICE.GPRS_DEVICE_STATE
-- Generated 2/16/2005 1:39:31 PM from DEVICE@USADBD03.USATECH.COM

CREATE TABLE device.gprs_device_state
    (gprs_device_state_id           NUMBER(1,0) NOT NULL,
    gprs_device_state_desc         VARCHAR2(64) NOT NULL
  ,
  CONSTRAINT PK_GPRS_DEVICE_STATE
  PRIMARY KEY (gprs_device_state_id)
  USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     131072
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  ))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     131072
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

-- Grants for Table
GRANT DELETE ON device.gprs_device_state TO web_user
/
GRANT INSERT ON device.gprs_device_state TO web_user
/
GRANT SELECT ON device.gprs_device_state TO web_user
/
GRANT UPDATE ON device.gprs_device_state TO web_user
/

-- Comments for DEVICE.GPRS_DEVICE_STATE

COMMENT ON TABLE device.gprs_device_state IS 'Lookup table for all possible gprs_device states'
/

-- End of DDL Script for Table DEVICE.GPRS_DEVICE_STATE

