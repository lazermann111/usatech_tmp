-- Start of DDL Script for Table DEVICE.GPRS_DEVICE
-- Generated 2/16/2005 1:38:13 PM from DEVICE@USADBD03.USATECH.COM

CREATE TABLE device.gprs_device
    (gprs_device_id                 NUMBER(20,0) NOT NULL,
    gprs_device_state_id           NUMBER(1,0) DEFAULT 1  NOT NULL,
    ordered_by                     VARCHAR2(128),
    ordered_ts                     DATE,
    provider_order_id              VARCHAR2(64),
    ordered_notes                  VARCHAR2(2048),
    iccid                          NUMBER(20,0) NOT NULL,
    imsi                           NUMBER(16,0),
    allocated_by                   VARCHAR2(128),
    allocated_ts                   DATE,
    allocated_to                   VARCHAR2(128),
    allocated_notes                VARCHAR2(2048),
    billable_to_name               VARCHAR2(256),
    billable_to_notes              VARCHAR2(2048),
    activated_by                   VARCHAR2(128),
    activated_ts                   DATE,
    activated_notes                VARCHAR2(2048),
    provider_activation_id         VARCHAR2(64),
    provider_activation_ts         DATE,
    msisdn                         NUMBER(16,0),
    phone_number                   NUMBER(16,0),
    rate_plan_name                 VARCHAR2(64),
    assigned_by                    VARCHAR2(128),
    assigned_ts                    DATE,
    assigned_notes                 VARCHAR2(2048),
    device_id                      NUMBER(20,0),
    imei                           NUMBER(15,0),
    device_type_name               VARCHAR2(1024),
    device_firmware_name           VARCHAR2(1024),
    rssi                           VARCHAR2(64),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL
  ,
  CONSTRAINT pk_gprs_device
  PRIMARY KEY (gprs_device_id)
  USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     131072
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  ))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     131072
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

-- Grants for Table
GRANT DELETE ON device.gprs_device TO web_user
/
GRANT INSERT ON device.gprs_device TO web_user
/
GRANT SELECT ON device.gprs_device TO web_user
/
GRANT UPDATE ON device.gprs_device TO web_user
/

-- Indexes for DEVICE.GPRS_DEVICE

CREATE UNIQUE INDEX device.idx_gprs_device_iccid ON device.gprs_device
  (
    iccid                           ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/
CREATE UNIQUE INDEX device.idx_gprs_device_imsi ON device.gprs_device
  (
    imsi                            ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/
CREATE INDEX device.idx_gprs_device_device_id ON device.gprs_device
  (
    device_id                       ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/


-- Constraints for DEVICE.GPRS_DEVICE



-- Triggers for DEVICE.GPRS_DEVICE

CREATE TRIGGER trbi_gprs_device
   BEFORE INSERT
   ON gprs_device
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
BEGIN
   IF :NEW.gprs_device_id IS NULL
   THEN
      SELECT seq_gprs_device_id.NEXTVAL
        INTO :NEW.gprs_device_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE, USER, SYSDATE,
          USER
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/
CREATE TRIGGER trbu_gprs_device
   BEFORE UPDATE
   ON gprs_device
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
BEGIN
   SELECT SYSDATE, USER
     INTO :NEW.last_updated_ts, :NEW.last_updated_by
     FROM DUAL;

   IF (   :NEW.gprs_device_state_id < :OLD.gprs_device_state_id
       OR :NEW.iccid <> :OLD.iccid
       OR (:OLD.imsi <> NULL AND :NEW.imsi <> :OLD.imsi)
       OR (:OLD.msisdn <> NULL AND :NEW.msisdn <> :OLD.msisdn)
       OR (:OLD.phone_number <> NULL AND :NEW.phone_number <>
                                                             :OLD.phone_number
          )
       OR (    :OLD.rate_plan_name <> NULL
           AND :NEW.rate_plan_name <> :OLD.rate_plan_name
          )
       OR (:OLD.device_id <> NULL AND :NEW.device_id <> :OLD.device_id)
       OR (:OLD.imei <> NULL AND :NEW.imei <> :OLD.imei)
      )
   THEN
      INSERT INTO device.gprs_device_hist
                  (gprs_device_id, gprs_device_state_id,
                   ordered_by, ordered_ts, provider_order_id,
                   ordered_notes, iccid, imsi,
                   allocated_by, allocated_ts, allocated_to,
                   allocated_notes, billable_to_name,
                   billable_to_notes, activated_by,
                   activated_ts, activated_notes,
                   provider_activation_id, provider_activation_ts,
                   msisdn, phone_number, rate_plan_name,
                   assigned_by, assigned_ts, assigned_notes,
                   device_id, imei, device_type_name,
                   device_firmware_name, rssi
                  )
           VALUES (:OLD.gprs_device_id, :OLD.gprs_device_state_id,
                   :OLD.ordered_by, :OLD.ordered_ts, :OLD.provider_order_id,
                   :OLD.ordered_notes, :OLD.iccid, :OLD.imsi,
                   :OLD.allocated_by, :OLD.allocated_ts, :OLD.allocated_to,
                   :OLD.allocated_notes, :OLD.billable_to_name,
                   :OLD.billable_to_notes, :OLD.activated_by,
                   :OLD.activated_ts, :OLD.activated_notes,
                   :OLD.provider_activation_id, :OLD.provider_activation_ts,
                   :OLD.msisdn, :OLD.phone_number, :OLD.rate_plan_name,
                   :OLD.assigned_by, :OLD.assigned_ts, :OLD.assigned_notes,
                   :OLD.device_id, :OLD.imei, :OLD.device_type_name,
                   :OLD.device_firmware_name, :OLD.rssi
                  );
   END IF;
END;
/

-- Comments for DEVICE.GPRS_DEVICE

COMMENT ON TABLE device.gprs_device IS 'Contains current state information of GPRS modems.'
/
COMMENT ON COLUMN device.gprs_device.device_firmware_name IS 'Modem Firmware Version (CGMR)'
/
COMMENT ON COLUMN device.gprs_device.device_type_name IS 'Modem Type Identifier (CGMI)'
/
COMMENT ON COLUMN device.gprs_device.iccid IS 'Integrated Circuit Card Identifier (SIM Card Serial Number) (CCID)'
/
COMMENT ON COLUMN device.gprs_device.imei IS 'International Mobile Equipment Identifier (Modem Serial Number) (CGSN)'
/
COMMENT ON COLUMN device.gprs_device.imsi IS 'International Mobile Subscriber Identifier (Used by carrier for provisioning and routing) (CIMI)'
/
COMMENT ON COLUMN device.gprs_device.msisdn IS 'Mobile Station International Integrated Service Digital Network Number (SMS Number)'
/
COMMENT ON COLUMN device.gprs_device.rssi IS 'Received Signal Strength Indication (CSQ)'
/
-- End of DDL Script for Table DEVICE.GPRS_DEVICE

-- Foreign Key
ALTER TABLE device.gprs_device
ADD CONSTRAINT fk_gprs_device_state_id FOREIGN KEY (gprs_device_state_id)
REFERENCES device.gprs_device_state (gprs_device_state_id)
/
-- End of DDL script for Foreign Key(s)

