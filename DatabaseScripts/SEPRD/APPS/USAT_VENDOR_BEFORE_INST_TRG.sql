ALTER TABLE AP.USAT_VENDOR_INTERFACE
 ADD (BANK_COUNTRY_S  VARCHAR2(2));

ALTER TABLE AP.USAT_VENDOR_INTERFACE
 ADD (B_STATUS  VARCHAR2(15));

ALTER TABLE AP.USAT_VENDOR_INTERFACE
 ADD (ATTRIBUTE28  VARCHAR2(100));

ALTER TABLE AP.USAT_VENDOR_INTERFACE
 ADD (ATTRIBUTE29  VARCHAR2(100));

ALTER TABLE AP.USAT_VENDOR_INTERFACE
 ADD (ATTRIBUTE30  VARCHAR2(100));



CREATE OR REPLACE TRIGGER AP.USAT_VENDOR_BEFORE_INST_TRG
BEFORE INSERT OR UPDATE
ON AP.USAT_VENDOR_INTERFACE 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
 l_sysdate DATE := sysdate;
 c_start_of_time constant date := to_date ('01/01/0001','DD/MM/YYYY');
 l_user_id VARCHAR2(30):=NULL;
begin
-- SELECT  USAT_VENDOR_SEQ.NEXTVAL INTO  l_trx_id FROM DUAL;
 --select fnd_global.user_id into l_user_id from dual;
  if inserting and
   --  :new.created_by_V    is null  and
      :new.creation_date_V is null  and
      :new.x_transaction_id is null and
	  :new.c_status         is null and
	  :new.s_status         is null and
        :new.b_status         is null and
	  :new.v_status         is null then
    --  :new.created_by_V      := fnd_global.user_id;
      :new.creation_date_V   := l_sysdate;
      select USAT_VENDOR_SEQ.NEXTVAL into :new.x_transaction_id  from dual;
      :new.c_status     :='NEW';
      :new.s_status     :='NEW';
      :new.v_status     :='NEW';
      :new.b_status     :='NEW';

   end if;
   if :new.last_update_date_V is null
      or :new.last_update_date_V = nvl(:old.last_update_date_V,
                                     C_start_of_time)
      or :new.last_update_date_V = trunc(:new.last_update_date_V)
   then
      :new.last_update_date_V  := l_sysdate;
  end if;
  :new.last_updated_by_V   :=  l_user_id;
 -- :new.last_update_login := fnd_global.login_id;

end;
/


