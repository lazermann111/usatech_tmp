CREATE OR REPLACE PACKAGE BODY APPS.USAT_VENDOR_BANKS_PKG
AS

PROCEDURE VALIDATE_VENDOR_BANKS(ERRBUF        OUT    VARCHAR2
                           ,RETCODE        OUT    NUMBER)  AS


l_user_id                         NUMBER :=FND_GLOBAL.USER_ID;
l_login_id                      NUMBER :=FND_GLOBAL.LOGIN_ID;
l_conc_request_id                NUMBER  := FND_GLOBAL.conc_request_id;
l_org_id                          NUMBER  := FND_PROFILE.VALUE('ORG_ID');
l_set_of_books_id                NUMBER  := FND_PROFILE.VALUE('GL_SET_OF_BKS_ID');
-- Define processing variables
l_error_flag                     BOOLEAN;
l_errm                           VARCHAR2(150);
l_sql_code                         VARCHAR2(100);
l_sql_errm                         VARCHAR2(250);
l_vendor_site_name                 VARCHAR2(240);
l_vendor_id                     NUMBER;
l_vendor_interface_id                NUMBER;
l_vendor_site_interface_id                NUMBER;
l_vendor_site_code              varchar2(15);
l_error                         VARCHAR2(2000);
v_CNT_CUST_BANK                           NUMBER;
l_ATTRIBUTE28                    VARCHAR2(2000):=NULL;
l_ATTRIBUTE29                    VARCHAR2(2000):=NULL;
l_ATTRIBUTE30                    VARCHAR2(2000):=NULL;
l_negative_records                              NUMBER:=0;
l_positive_records                              NUMBER:=0;
l_total_records                                 NUMBER:=0;
l_BANK_ID                    NUMBER;
l_BRANCH_ID                    NUMBER;
l_BANK_NAME                    VARCHAR2(360);
l_BRANCH_NAME                    VARCHAR2(360);
l_BRANCH_NUMBER                    VARCHAR2(15);
l_COUNTRY_CODE                    VARCHAR2(2);
l_CURRENCY_CODE	VARCHAR2(15);
l_OBJECT_VERSION_NUMBER NUMBER;

/* Cursor to select all the Vendors  */
CURSOR c_usat_vendor_banks(p_status VARCHAR2) IS
  SELECT DISTINCT *
  FROM USAT_VENDOR_INTERFACE
  WHERE B_STATUS =P_STATUS
  ORDER BY VENDOR_NAME_V;
--l_error        VARCHAR2(2000):=NULL;
BEGIN-- VALIDATE_VENDOR_BANKS
FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'          	  ''PROGRAM NAME:            Usat Vendor Banks Conversion''                     ''Date: '||SYSDATE);
            FND_FILE.PUT_LINE(FND_FILE.LOG,'                      ''PROGRAM NAME:            Usat Vendor Banks Conversion');
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'                      ''--------------------------------------');
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'                      ''--------------------------------------');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'   ''Usat Vendor Banks Conversion Rejections');
  FOR j IN c_usat_vendor_banks('NEW') LOOP
     BEGIN
l_error_flag                     :=FALSE;
l_errm                           :=NULL;
l_sql_code                         :=NULL;
l_sql_errm                         :=NULL;
l_vendor_site_name                :=NULL;
l_vendor_id                         :=NULL;
l_vendor_interface_id                :=NULL;
l_vendor_site_interface_id            :=NULL;
l_vendor_site_code              :=NULL;
l_error                         :=NULL;
v_CNT_CUST_BANK        :=0;
l_BANK_ID                    :=NULL;
l_BRANCH_ID                    :=NULL;
l_BANK_NAME                    :=NULL;
l_BRANCH_NAME                    :=NULL;
l_BRANCH_NUMBER                    :=NULL;
l_COUNTRY_CODE                    :=NULL;
l_CURRENCY_CODE	:=NULL;
l_OBJECT_VERSION_NUMBER :=NULL;

            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'');
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'');

    /* Validation for Vendor Existence */
       BEGIN
                   SELECT distinct first_value(VENDOR_INTERFACE_ID ) over (PARTITION BY ATTRIBUTE15 order by   last_update_date DESC) VENDOR_INTERFACE_ID  
                   INTO l_VENDOR_INTERFACE_ID  FROM ap_suppliers_int 
                   WHERE    TRIM(nvl(ATTRIBUTE15,0)) =TRIM(nvl(J.SEGMENT1_V,0))  
                   and nvl(STATUS,'NEW')  <>'REJECTED' and VENDOR_INTERFACE_ID is not null;
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
            l_error_flag:=TRUE;
            l_attribute28:='Vendor Does not Exist';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'VENDOR BANK REJECTED=======>>   For Transaction_id-'||j.X_transaction_id||': Vendor--'||j.VENDOR_NAME_V||'-- Does not Exist');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'VENDOR BANK REJECTED=======>>   For Transaction_id-'||j.X_transaction_id||': Vendor--'||j.VENDOR_NAME_V||'-- Does not Exist');
          WHEN OTHERS THEN
            l_error_flag:=TRUE;
            l_attribute28:='INVALID VENDOR';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'INVALID VENDOR');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'INVALID VENDOR ');
        END;
            /* End of  Validation for Vendor Existence */

            /* Validation for Vendor site existence */

    BEGIN
        SELECT  distinct first_value(vendor_id ) over (PARTITION BY VENDOR_SITE_CODE order by last_update_date DESC)  vendor_id,  
        first_value(VENDOR_SITE_INTERFACE_ID ) over (PARTITION BY VENDOR_SITE_CODE order by   last_update_date DESC) VENDOR_SITE_INTERFACE_ID  
         INTO l_vendor_id , l_vendor_site_interface_id
         FROM ap_supplier_sites_int  
             WHERE  TRIM(nvl(VENDOR_SITE_CODE,0)) =TRIM(nvl(j.VENDOR_SITE_CODE_s,0)) and  TRIM(nvl(ATTRIBUTE6,0)) =TRIM(nvl(j.VENDOR_SITE_NAME_S,0))  
              and nvl(STATUS,'NEW') <>'REJECTED'  and VENDOR_SITE_INTERFACE_ID is not null;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
            l_error_flag:=TRUE;
            l_attribute29:='Vendor site Does not Exist';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'VENDOR BANK REJECTED=======>>   For Transaction_id-'||j.X_transaction_id||': Vendor Site--'||j.vendor_site_code_s||'-- Does not Exist');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'VENDOR BANK REJECTED=======>>  For Transaction_id-'||j.X_transaction_id||': Vendor Site--'||j.vendor_site_code_s||'-- Does not Exist');
          WHEN OTHERS THEN
            l_error_flag:=TRUE;
            l_attribute29:='INVALID VENDOR SITE';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'INVALID VENDOR SITE');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'INVALID VENDOR SITE ');
        END;
         /* End of Validation for Vendor site existence */

                             /* Validation for Bank existence */
    BEGIN
                        select distinct bb.branch_number, 
               first_value( b.bank_party_id) over (PARTITION BY bb.branch_number order by   b.CREATION_DATE DESC)  bank_party_id,  
                first_value( bb.Branch_Party_Id) over (PARTITION BY bb.branch_number order by   b.CREATION_DATE DESC)  branch_party_id,  
               first_value( b.BANK_NAME) over (PARTITION BY bb.branch_number order by   b.creation_DATE DESC)  bank_name,  
               first_value(  bb.BANK_BRANCH_NAME) over (PARTITION BY bb.branch_number order by   b.CREATION_DATE DESC)  BANK_BRANCH_NAME,  
               UPPER(NVL(first_value( b.HOME_COUNTRY ) over (PARTITION BY bb.branch_number order by   b.CREATION_DATE DESC) ,'US') ) HOME_COUNTRY,
        DECODE((UPPER(NVL(first_value( b.HOME_COUNTRY ) over (PARTITION BY bb.branch_number order by   b.CREATION_DATE DESC) ,'US')) ),'US','USD','CA','CAD','USD' ) CURRENCY_CODE,
        NVL(first_value( b.OBJECT_VERSION_NUMBER ) over (PARTITION BY bb.branch_number order by   b.CREATION_DATE DESC) ,1)   OBJECT_VERSION_NUMBER
               INTO l_BRANCH_NUMBER, l_BANK_ID, l_BRANCH_ID, l_BANK_NAME, l_BRANCH_NAME,l_COUNTRY_CODE,l_CURRENCY_CODE, l_OBJECT_VERSION_NUMBER
        from iby_ext_bank_branches_v  bb, iby_ext_banks_v b
        where  b.bank_party_id =  bb.bank_party_id 
        and NVL(b.END_DATE,SYSDATE+1)>SYSDATE and NVL(bb.END_DATE,SYSDATE+1)>SYSDATE    
          and trim(lower(b.home_country) )=trim(lower(j.bank_country_s)) and trim(bb.BRANCH_NUMBER) =trim(j.BANK_NUM_V);
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
            l_error_flag:=TRUE;
            l_ATTRIBUTE30:='Bank Does not Exist';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'VENDOR BANK REJECTED====>> For Transaction_id-'||j.X_transaction_id||': Bank--'||j.bank_name_v||': Number--'||j.BANK_NUM_V||': Country--'||j.bank_country_s||' for Supplier: '||j.vendor_name_v||' Site_name: '||j.vendor_site_code_s||' -- Does not Exist');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'VENDOR BANK REJECTED====>> For Transaction_id-'||j.X_transaction_id||': Bank--'||j.bank_name_v||': Number--'||j.BANK_NUM_V||': Country--'||j.bank_country_s||' for Supplier: '||j.vendor_name_v||' Site_name: '||j.vendor_site_code_s||' -- Does not Exist');
          WHEN OTHERS THEN
            l_error_flag:=TRUE;
            l_attribute30:='INVALID Bank';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'INVALID BANK');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'INVALID BANK ');
        END;
           /* End of Validation for Bank existence */
  
         /* Validation for Duplicate Vendor Bank Account */

                   BEGIN
              SELECT COUNT(*)  into v_CNT_CUST_BANK 
           FROM (SELECT DISTINCT B.BANK_ACCOUNT_NAME "Account Name",B.BANK_ACCOUNT_NUM "Account Number"
        FROM IBY_EXT_BANK_ACCOUNTS B,  IBY_ACCOUNT_OWNERS o,  hz_party_sites h
        WHERE b.EXT_BANK_ACCOUNT_ID = o.EXT_BANK_ACCOUNT_ID
        AND H.party_id=O.ACCOUNT_OWNER_PARTY_ID
        AND  NVL(b.END_DATE,SYSDATE+1)>SYSDATE
        AND  NVL(O.END_DATE,SYSDATE+1)>SYSDATE
        AND  NVL(H.END_DATE_ACTIVE,SYSDATE+1)>SYSDATE
        AND TRIM(upper(H.party_site_name))=TRIM(upper(j.vendor_site_code_s))
        AND trim(B.BANK_ID)=trim(l_bank_id)
        AND trim(B.BRANCH_ID)=trim(l_branch_id)
        AND tRIM(upper(B.COUNTRY_CODE))=TRIM(upper(l_COUNTRY_CODE)));
          EXCEPTION
          WHEN OTHERS THEN
           l_error_flag:=TRUE;
            l_errm              := 'INVALID CUSTOMER BANK';
            l_sql_code              :=  SQLCODE;
            l_sql_errm              :=  SUBSTR(SQLERRM,1,255);
            l_error:= l_error||'-'||'INVALID CUSTOMER BANK';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'INVALID CUSTOMER BANK');
            FND_FILE.PUT_LINE(FND_FILE.LOG,'INVALID CUSTOMER BANK');
            END;
 		IF v_CNT_CUST_BANK >=1 THEN
            l_error_flag:=TRUE;
            l_error:= l_error||'-'||'Customer Bank Number Already Exists IN IBY_EXT_BANK_ACCOUNTS';
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Customer Bank Number REJECTED=======>>  For Transaction_id-'||j.X_transaction_id||':Cust Bank Nber--'||j.BANK_ACCOUNT_NUM_V||'-- Already Exists For Vendor_Site:'||j.vendor_site_code_s||'');
           FND_FILE.PUT_LINE(FND_FILE.LOG,'Customer Bank Number REJECTED=======>>  For Transaction_id-'||j.X_transaction_id||': Cust Bank Nber --'||j.BANK_ACCOUNT_NUM_V||'-- Already Exists For Vendor SITE:'||j.vendor_site_code_s||'');
          END IF;

                  /* Validation for Duplicate Vendor Bank Account  */

      IF l_error_flag = TRUE THEN
            BEGIN

              UPDATE  USAT_VENDOR_INTERFACE
                SET   B_STATUS = 'REJECTED',
                  ATTRIBUTE28=l_ATTRIBUTE28,
                  ATTRIBUTE29=l_ATTRIBUTE29,
                  ATTRIBUTE30=l_ATTRIBUTE30
                  WHERE   x_transaction_id = j.x_transaction_id;
                   l_negative_records:=l_negative_records+1;
                    COMMIT;
                  l_ATTRIBUTE28:=NULL;
                  l_ATTRIBUTE29:=NULL;
                  l_ATTRIBUTE30:=NULL;
           EXCEPTION
          WHEN OTHERS THEN
            ROLLBACK;
            l_error_flag        := TRUE;
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Error IN UPDATING USAT_VENDOR_INTERFACE_N WITH status REJECTED');
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Error IN UPDATING USAT_VENDOR_INTERFACE_N WITH status REJECTED');
       END;
      ELSE
            BEGIN
            INSERT INTO IBY_TEMP_EXT_BANK_ACCTS(
                         BANK_ACCOUNT_NUM
                          ,BANK_ACCOUNT_NAME
                          ,BANK_ACCOUNT_TYPE
	          ,CURRENCY_CODE
                          ,COUNTRY_CODE
                          ,IBAN
                          ,BIC
                          ,TEMP_EXT_BANK_ACCT_ID
                          ,LAST_UPDATE_DATE
                          ,LAST_UPDATED_BY
                     --      ,CALLING_APP_UNIQUE_REF1
                          ,CALLING_APP_UNIQUE_REF2
                          ,FOREIGN_PAYMENT_USE_FLAG
                          ,PAYMENT_FACTOR_FLAG
                          ,BANK_ID
                          ,BANK_NAME
                          ,BRANCH_ID
                          ,BRANCH_NAME
              ,CREATION_DATE
              ,CREATED_BY
              ,PROGRAM_APPLICATION_ID
              ,PROGRAM_ID
              ,PROGRAM_UPDATE_DATE
              ,REQUEST_ID
              ,OBJECT_VERSION_NUMBER
              ,STATUS)
              VALUES
              (
                           J.BANK_ACCOUNT_NUM_V        --BANK_ACCOUNT_NUM
                          ,J.VENDOR_NAME_V        -- BANK_ACCOUNT_NAME   
                          ,UPPER(NVL(J.BANK_ACCOUNT_TYPE_V,'CHECKING'))        --  BANK_ACCOUNT_type            
                        , l_CURRENCY_CODE			-- CUREENCY CODE ,
	        , l_COUNTRY_CODE                --COUNTRY_CODE
                          ,NULL                                  --IBAN
                          ,NULL                                       --BIC
                          ,IBY_TEMP_EXT_BANK_ACCTS_S.NEXTVAL -- TEMP_EXT_BANK_ACCT_ID
                          ,SYSDATE                        --LAST_UPDATE_DATE
                          ,l_user_id                        --LAST_UPDATED_BY
                        --  ,l_VENDOR_INTERFACE_ID                        --VENDOR_INTERFACE_ID
                          ,l_VENDOR_SITE_INTERFACE_ID            --VENDOR_SITE_INTERFACE_ID
                          ,'N'                --foreign_payment_use_flag
                          ,'N'		--PAYMENT_FACTOR_FLAG
                          ,trim(l_BANK_ID)        -- Bank_id
                          ,trim(l_BANK_NAME)        -- Bank_name
                          ,trim(l_BRANCH_ID)        --Branch_id
                          ,trim(l_BRANCH_NAME)        --Branch_name
                          ,SYSDATE                   --CREATION_DATE
                           ,l_user_id                   -- CREATED_BY                         
                          ,NULL                            --PROGRAM_APPLICATION_ID
                          ,NULL                            --PROGRAM_ID
                         ,NULL                            --PROGRAM_UPDATE_DATE
                         ,l_conc_request_id            --REQUEST_ID
                         ,l_OBJECT_VERSION_NUMBER			--OBJECT_VERSION_NUMBER
                          ,NULL                         --STATUS
                           );
               UPDATE    USAT_VENDOR_INTERFACE SET  B_STATUS = 'PROCESSED'   WHERE x_transaction_id= j.x_transaction_id;
                l_positive_records:=l_positive_records+1;
            FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'VENDOR BANK IMPORTED=======>>'' Transaction_id:'||j.X_transaction_id||' Vendor_Name:'||trim(j.VENDOR_NAME_V)||' --Vendor_Sitecode:'||j.vendor_site_CODE_s||' --Bank_Name :'||substr(nvl(J.BANK_NAME_V,'Unknown'),1,25)||'  --Bank_Acct_Num :'||nvl(J.BANK_ACCOUNT_NUM_V,'Unknown'));
                  COMMIT;
                 EXCEPTION
          WHEN OTHERS THEN
                    ROLLBACK;
            l_error_flag := TRUE;
                    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Error WHILE UPDATING STATUS TO PROCESSED IN INTERFACE TABLE');
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Error WHILE UPDATING STATUS TO PROCESSED IN INTERFACE TABLE');
            END;
          END IF;
      l_error:=NULL;
      l_errm:=NULL;
      l_error_flag:=FALSE;
       l_total_records:=l_total_records+1;
      END;
  END LOOP; --End Vendor validate Loop
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
               FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
                           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'----------------------------------------------------------------------------------------------');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'   ''Usat Vendor Banks Conversion Summary Report');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'   ''------------------------------------------------');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total no of VENDOR BANKS Rejected from USAT_VENDOR_INTERFACE Table             :'||l_negative_records);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Total no of VENDOR BANKS Rejected from USAT_VENDOR_INTERFACE Table :'||l_negative_records);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total no of VENDOR BANKS Processed from USAT_VENDOR_INTERFACE Table            :'||l_positive_records);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Total no of VENDOR BANKS Processed from USAT_VENDOR_INTERFACE Table :'||l_positive_records);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Total no of VENDOR BANKS Taken for Validation from USAT_VENDOR_INTERFACE Table :'||l_total_records);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Total no of VENDOR BANKS Taken for Validation from USAT_VENDOR_INTERFACE Table :'||l_total_records);
  END;--VALIDATE_VENDOR_BANKS

END USAT_VENDOR_BANKS_PKG;    --End of pkg body
/
