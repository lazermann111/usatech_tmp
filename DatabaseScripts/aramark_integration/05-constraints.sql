ALTER TABLE pss.aramark_payment_type ADD CONSTRAINT fk_aramark_payment_type_aa FOREIGN KEY (aramark_authority_id) REFERENCES PSS.aramark_authority (aramark_authority_id);

ALTER TABLE pss.aramark_authority ADD CONSTRAINT fk_aramark_authority_ast FOREIGN KEY (authority_service_type_id) REFERENCES PSS.authority_service_type (authority_service_type_id);
