ALTER TABLE pss.pos_pta ADD (POS_PTA_ENCRYPT_KEY2 VARCHAR2(192));
ALTER TABLE pss.tran_line_item MODIFY tran_line_item_amount NUMBER(7,2);

CREATE TABLE pss.aramark_authority(
	aramark_authority_id		NUMBER(20,0) PRIMARY KEY,
	aramark_authority_name		VARCHAR2(60),
	aramark_remote_server_addr	VARCHAR2(255) NOT NULL,
	aramark_gateway_port_num	NUMBER(5,0),
	aramark_gateway_addr		VARCHAR2(30),
	authority_service_type_id	NUMBER(20,0) NOT NULL,
	created_by					VARCHAR2(30) NOT NULL,
	created_ts					DATE NOT NULL,
	last_updated_by				VARCHAR2(30) NOT NULL,
	last_updated_ts				DATE NOT NULL
)
TABLESPACE pss_data;
COMMENT ON TABLE pss.aramark_authority IS 'Contains every payment authority that can be used by Aramark.';

CREATE TABLE pss.aramark_payment_type(
	aramark_payment_type_id		NUMBER(20,0) PRIMARY KEY,
	aramark_payment_type_desc	VARCHAR2(60),
	aramark_authority_id		NUMBER(20,0) NOT NULL,
	created_by					VARCHAR2(30) NOT NULL,
	created_ts					DATE NOT NULL,
	last_updated_by				VARCHAR2(30) NOT NULL,
	last_updated_ts				DATE NOT NULL
)
TABLESPACE pss_data;