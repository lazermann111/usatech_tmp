-- sequences
CREATE SEQUENCE pss.seq_aramark_authority_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

CREATE SEQUENCE pss.seq_aramark_payment_type_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- triggers
CREATE OR REPLACE TRIGGER pss.trbi_aramark_authority
 BEFORE
  INSERT
 ON pss.aramark_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.aramark_authority_id IS NULL THEN

      SELECT pss.seq_aramark_authority_id.nextval
        into :new.aramark_authority_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER pss.trbu_aramark_authority
 BEFORE
  UPDATE
 ON pss.aramark_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER pss.trbi_aramark_payment_type
 BEFORE
  INSERT
 ON pss.aramark_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.aramark_payment_type_id IS NULL THEN

      SELECT pss.seq_aramark_payment_type_id.nextval
        into :new.aramark_payment_type_id
        FROM dual;

    END IF;


   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER pss.trbu_aramark_payment_type
 BEFORE
  UPDATE
 ON pss.aramark_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- Start of DDL Script for Package DEVICE.PKG_DEVICE_MAINT
-- Generated 2/15/2005 5:41:57 PM from DEVICE@USADBD03.USATECH.COM

CREATE OR REPLACE 
PACKAGE device.pkg_device_maint
IS
   /*******************************************************************************
  Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
  Description: Descriptions of the function
  Parameters:

              Function Return Value
                 Indicates if the exception was recorded successfully.  A value of
                 0 indicates a successful execution.  A value of 1 indicates an
                 unsuccessful execution.  A Boolean value was not used because it
                 may be misinterpreted by non-Oracle systems.
  MODIFICATION HISTORY:
  Person     Date          Comments
  ---------  ----------    -------------------------------------------
  MDH       24-NOV-2001    Intial Creation
 *******************************************************************************/
PROCEDURE SP_ASSIGN_NEW_DEVICE_ID
(
    pn_old_device_id  IN       device.device_id%TYPE,
    pn_new_device_id  OUT      device.device_id%TYPE,
    pn_return_code    OUT      exception_code.exception_code_id%TYPE,
    pv_error_message            OUT        exception_data.additional_information%TYPE
);

END;
/


CREATE OR REPLACE 
PACKAGE BODY        device.pkg_device_maint IS
   e_device_id_notfound       EXCEPTION;
   cv_server_name    CONSTANT VARCHAR2 (30) := 'USADBD02';
   cv_package_name   CONSTANT VARCHAR2 (30) := 'PKG_DEVICE_MAINT';

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    MDH       24-NOV-2001    Intial Creation
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
   *******************************************************************************/
   PROCEDURE sp_assign_new_device_id (
      pn_old_device_id   IN       device.device_id%TYPE,
      pn_new_device_id   OUT      device.device_id%TYPE,
      pn_return_code     OUT      exception_code.exception_code_id%TYPE,
      pv_error_message   OUT      exception_data.additional_information%TYPE
   ) AS
      v_error_msg                  exception_data.additional_information%TYPE;
      n_old_device_id              device.device_id%TYPE;
      n_new_device_id              device.device_id%TYPE;
      n_old_pos_id                 pos.pos_id%TYPE;
      n_new_pos_id                 pos.pos_id%TYPE;
      b_device_id_found_flag       BOOLEAN;
      e_device_id_notfound         EXCEPTION;
      cv_procedure_name   CONSTANT VARCHAR2 (50)             := 'SP_ASSIGN_NEW_DEVICE_ID';
      cv_server_name      CONSTANT VARCHAR2 (50)                            := 'USADBD02';
      cv_package_name     CONSTANT VARCHAR2 (50)                    := 'PKG_DEVICE_MAINT';
   BEGIN
      --Initialise all variables
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- Verify the N_EXCEPTION_CODE_ID parameter contains a value stored in the EXCEPTION_CODE table.
      UPDATE device
         SET device_active_yn_flag = 'N'
       WHERE device_id = pn_old_device_id;

      -- Check if error information exists for the exception
      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_device_id_notfound;
      END IF;

      SELECT seq_device_id.NEXTVAL
        INTO n_new_device_id
        FROM DUAL;

      INSERT INTO device
                  (device_id,
                   device_name,
                   device_serial_cd,
                   device_type_id,
                   encryption_key,
                   device_active_yn_flag,
                   device_client_serial_cd
                  )
         SELECT n_new_device_id,
                device_name,
                device_serial_cd,
                device_type_id,
                encryption_key,
                'Y',
                device_client_serial_cd
           FROM device
          WHERE device_id = pn_old_device_id;

      -- Check if exception information exists for the UNKNOWN_ERROR_ID exception code id
      IF SQL%NOTFOUND = TRUE THEN
         b_device_id_found_flag := FALSE;
      END IF;

      pn_new_device_id := n_new_device_id;

      -- copy and create new POS and POS_Payment_Type_Authority records
      SELECT pos.pos_id
        INTO n_old_pos_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      SELECT pss.seq_pos_id.NEXTVAL
        INTO n_new_pos_id
        FROM DUAL;

      INSERT INTO pos
                  (pos_id,
                   location_id,
                   customer_id,
                   device_id
                  )
         SELECT n_new_pos_id,
                location_id,
                customer_id,
                n_new_device_id
           FROM pos
          WHERE pos.pos_id = n_old_pos_id;

      INSERT INTO pos_pta
                  (pos_id,
                   payment_subtype_id,
                   pos_pta_encrypt_key,
                   pos_pta_encrypt_key2,
                   pos_pta_activation_ts,
                   pos_pta_deactivation_ts,
                   payment_subtype_key_id,
                   pos_pta_pin_req_yn_flag,
                   pos_pta_regex,
                   pos_pta_regex_bref,
                   pos_pta_device_serial_cd
                  )
         SELECT n_new_pos_id,
                payment_subtype_id,
                pos_pta_encrypt_key,
                pos_pta_encrypt_key2,
                pos_pta_activation_ts,
                pos_pta_deactivation_ts,
                payment_subtype_key_id,
                pos_pta_pin_req_yn_flag,
                pos_pta_regex,
                pos_pta_regex_bref,
                pos_pta_device_serial_cd
           FROM pos_pta
          WHERE pos_id = n_old_pos_id;

 /*     INSERT INTO pos_payment_type_authority
                  (pos_id,
                   authority_id,
                   payment_type_id
                  )
         SELECT n_new_pos_id,
                authority_id,
                payment_type_id
           FROM pos_payment_type_authority
          WHERE pos_id = n_old_pos_id; */

      -- change any waiting outgoing file transfers over to this new device
      UPDATE device_file_transfer
         SET device_id = n_new_device_id
       WHERE device_id = pn_old_device_id
         AND device_file_transfer_direct = 'O'
         AND device_file_transfer_status_cd = 0;

      -- copy any existing device settings to the new device
      INSERT INTO device_setting
                  (device_id,
                   device_setting_parameter_cd,
                   device_setting_value
                  )
         SELECT n_new_device_id,
                device_setting_parameter_cd,
                device_setting_value
           FROM device_setting
          WHERE device_setting.device_id = pn_old_device_id;
   EXCEPTION
      WHEN e_device_id_notfound THEN
         pv_error_message :=
               'The device_id = '
            || pn_old_device_id
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             v_error_msg,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   --pn_return_code := 0;

   --    WHEN OTHERS THEN
--
--       pv_error_message := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
--       pkg_exception_processor.sp_log_exception(PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id, v_error_msg, cv_server_name, cv_package_name || '.' || cv_procedure_name);
--       pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
--       --pn_return_code := 0;
   END;
END;
/


-- End of DDL Script for Package DEVICE.PKG_DEVICE_MAINT
