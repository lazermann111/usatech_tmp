INSERT INTO pss.aramark_authority (aramark_authority_id, aramark_authority_name, aramark_remote_server_addr, aramark_gateway_port_num, aramark_gateway_addr, authority_service_type_id) 
VALUES (1, 'Aramark Test Server', 'http://demo.aramarkcampusit.com/Relay_V2/RelayService.asmx', 9100, '10.0.0.24', 1);

INSERT INTO pss.aramark_payment_type (aramark_payment_type_id, aramark_payment_type_desc, aramark_authority_id) 
VALUES (1, 'Special Card', 1);

INSERT INTO pss.payment_subtype (
	payment_subtype_id, 
	payment_subtype_name,
	payment_subtype_class, 
	payment_subtype_key_name,
	client_payment_type_cd, 
	payment_subtype_regex,
	payment_subtype_regex_bref, 
	payment_subtype_table_name,
	payment_subtype_key_desc_name, 
	payment_subtype_desc
)
VALUES (
	10, 
	'Special Card - Aramark Generic', 
	'ReRix::POS::Handler::Aramark', 
	'ARAMARK_PAYMENT_TYPE_ID', 
	'S', 
	'([0-9]{14})([0-9]{2})?', 
	'1:1|2:7',
	'ARAMARK_PAYMENT_TYPE', 
	'ARAMARK_PAYMENT_TYPE_DESC', 
	NULL
);

INSERT INTO pss.tran_line_item_type (tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn)
VALUES (200, 'ePort Vend', 'P');
INSERT INTO pss.tran_line_item_type (tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn)
VALUES (201, 'Transaction Amount Summary', 'P');

INSERT INTO device.host_type (host_type_id, host_type_desc, host_type_manufacturer, host_model_cd, host_default_complete_minut, host_type_cd)
VALUES (200, 'Unknown Vending Machine', 'Unknown', 'Unknown', 0, 'V');

INSERT INTO pss.tran_state VALUES ('D', 'TRANSACTION_COMPLETE');
INSERT INTO pss.tran_state VALUES ('E', 'TRANSACTION_COMPLETE_ERROR');
UPDATE pss.tran_state SET tran_state_desc = 'SERVER_PROCESSING_BATCH_LOCAL' WHERE tran_state_cd = '1';
UPDATE pss.tran_state SET tran_state_desc = 'SERVER_PROCESSING_BATCH_INTEND' WHERE tran_state_cd = '3';
UPDATE pss.tran_state SET tran_state_desc = 'SERVER_PROCESSING_BATCH' WHERE tran_state_cd = '4';
UPDATE pss.tran_state SET tran_state_desc = 'TRANSACTION_INCOMPLETE' WHERE tran_state_cd = 'I';
UPDATE pss.tran_state SET tran_state_desc = 'PROCESSED_SERVER_AUTH_FAILURE' WHERE tran_state_cd = '5';
INSERT INTO pss.settlement_batch_state VALUES (6, 'SERVER_SETTLEMENT_COMPLETE_ERROR');

UPDATE pss.tran SET tran_state_cd = 'D' WHERE tran_state_cd = '8';
