CREATE PUBLIC SYNONYM aramark_authority FOR pss.aramark_authority;
CREATE PUBLIC SYNONYM aramark_payment_type FOR pss.aramark_payment_type;

CREATE PUBLIC SYNONYM tran_refund FOR pss.tran_refund;
CREATE PUBLIC SYNONYM settlement_batch_state FOR pss.settlement_batch_state;
CREATE PUBLIC SYNONYM tran_refund_state FOR pss.tran_refund_state;

GRANT ALL ON pss.aramark_authority TO web_user;
GRANT ALL ON pss.aramark_payment_type TO web_user;
GRANT SELECT ON pss.seq_aramark_authority_id TO web_user;
GRANT SELECT ON pss.seq_aramark_payment_type_id TO web_user;
