insert into device.device_serial_format(device_type_id, device_serial_format_name, device_serial_format_desc, device_serial_format_prefix, device_serial_format_length, device_serial_format_num_base)
values(1, 'G8 Serial Format', 'Format of G8 serial code', 'G8', 8, 10);

update device.device_type set device_type_desc = 'Gx ePort', device_type_serial_cd_regex = '^G(5|8)[0-9]{6}$' where device_type_id = 1;