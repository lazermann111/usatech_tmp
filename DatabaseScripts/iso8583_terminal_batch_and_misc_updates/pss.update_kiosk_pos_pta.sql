UPDATE pos_pta SET pos_pta_activation_ts = NULL 
WHERE pos_pta_id IN (SELECT pp.pos_pta_id FROM pos_pta pp, pos p, device d 
WHERE d.device_type_id = 11 
    AND p.device_id = d.device_id 
    AND pp.pos_id = p.pos_id 
    AND pp.pos_pta_activation_ts IS NOT NULL
    AND pp.pos_pta_deactivation_ts IS NULL)