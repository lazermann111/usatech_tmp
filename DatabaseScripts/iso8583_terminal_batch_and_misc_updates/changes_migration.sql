REM ----------------------------------------------------------------------
CREATE TABLE AUTHORITY.HANDLER_RESP_ACTION
  (
    HANDLER_RESP_ACTION_CD    char(1),
    HANDLER_RESP_ACTION_DESC  VARCHAR2(255)   not null,
    CONSTRAINT PK_HANDLER_RESP_ACTION primary key(HANDLER_RESP_ACTION_CD)
  )
TABLESPACE AUTHORITY_DATA;

CREATE PUBLIC SYNONYM HANDLER_RESP_ACTION for AUTHORITY.HANDLER_RESP_ACTION;

COMMENT ON TABLE HANDLER_RESP_ACTION is 'Result state code based on authority response.';

INSERT INTO HANDLER_RESP_ACTION VALUES ('Y', 'Action successful');
INSERT INTO HANDLER_RESP_ACTION VALUES ('N', 'Action unsuccessful or denied');
INSERT INTO HANDLER_RESP_ACTION VALUES ('O', 'Action unsuccessful or denied (permanently)');
INSERT INTO HANDLER_RESP_ACTION VALUES ('P', 'Action partially successful -- for amount less than original action request');
INSERT INTO HANDLER_RESP_ACTION VALUES ('F', 'Action failure');

REM ----------------------------------------------------------------------
CREATE TABLE PSS.TERMINAL_BATCH
  (
    TERMINAL_BATCH_ID        NUMBER(20,0),
    TERMINAL_ID              NUMBER(20,0)   not null,
    TERMINAL_BATCH_NUM       INTEGER        not null,
    TERMINAL_BATCH_CYCLE_NUM  INTEGER        not null,
    TERMINAL_BATCH_OPEN_TS   DATE,
    TERMINAL_BATCH_CLOSE_TS  DATE,
    CREATED_BY               VARCHAR2(30)   not null,
    CREATED_TS               DATE           not null,
    LAST_UPDATED_BY          VARCHAR2(30)   not null,
    LAST_UPDATED_TS          DATE           not null,
    CONSTRAINT PK_TERMINAL_BATCH primary key(TERMINAL_BATCH_ID),
    CONSTRAINT UK_TERMINAL_BATCH_2 unique(TERMINAL_ID,TERMINAL_BATCH_NUM,TERMINAL_BATCH_CYCLE_NUM),
    CONSTRAINT FK_TERMINAL_BATCH_TERMINAL_ID foreign key(TERMINAL_ID) references TERMINAL(TERMINAL_ID)
  )
TABLESPACE PSS_DATA; 
 
CREATE PUBLIC SYNONYM TERMINAL_BATCH for PSS.TERMINAL_BATCH;
REM ----------------------------------------------------------------------

alter table authority.HANDLER_GATEWAY_RESP drop column HANDLER_ID;
alter table authority.HANDLER_RESP modify RESP_RETRY_ID not null;
alter table authority.HANDLER_RESP add (HANDLER_RESP_ACTION_CD char(1) not null);

alter table pss.AUTH rename column AUTH_BATCH_NUM to TERMINAL_BATCH_ID;

-- add update statement
UPDATE PSS.AUTH SET TERMINAL_BATCH_ID = null where TERMINAL_BATCH_ID is not null;
alter table pss.AUTH add (CONSTRAINT FK_AUTH_TERMINAL_BATCH_ID foreign key(TERMINAL_BATCH_ID) references PSS.TERMINAL_BATCH(TERMINAL_BATCH_ID));

-- change this to null;
-- alter table pss.DUP_AUTH modify AUTH_RESULT_CD null;
-- change modify to add 
alter table pss.DUP_AUTH add TERMINAL_BATCH_ID number(20);
alter table pss.DUP_AUTH add (AUTH_AUTHORITY_MISC_DATA VARCHAR2(4000));
alter table pss.DUP_AUTH add (CONSTRAINT FK_DUP_AUTH_TERMINAL_BATCH_ID foreign key(TERMINAL_BATCH_ID) references TERMINAL_BATCH(TERMINAL_BATCH_ID));

-- modify index
CREATE INDEX pss.IDX_DUP_TRAN_LINE_ITEM_1 ON DUP_TRAN_LINE_ITEM(TRAN_ID) TABLESPACE PSS_INDX;

alter table pss.REFUND rename column REFUND_BATCH_NUM to TERMINAL_BATCH_ID;
-- add update statement
UPDATE pss.REFUND SET TERMINAL_BATCH_ID = null where TERMINAL_BATCH_ID is not null;
alter table pss.REFUND add (CONSTRAINT FK_REFUND_TERMINAL_BATCH_ID foreign key(TERMINAL_BATCH_ID) references TERMINAL_BATCH(TERMINAL_BATCH_ID));

alter table pss.SETTLEMENT_BATCH drop column TERMINAL_ID;
alter table pss.SETTLEMENT_BATCH drop column SETTLEMENT_BATCH_BATCH_NUM;
alter table pss.SETTLEMENT_BATCH add (TERMINAL_BATCH_ID NUMBER(20,0));
alter table pss.SETTLEMENT_BATCH add (CONSTRAINT FK_SETTLEMENT_B_TERMINAL_B_ID foreign key(TERMINAL_BATCH_ID) references TERMINAL_BATCH(TERMINAL_BATCH_ID));

alter table pss.TERMINAL rename column TERMINAL_BATCH_NUM to TERMINAL_NEXT_BATCH_NUM;
alter table pss.TERMINAL modify TERMINAL_NEXT_BATCH_NUM integer;

alter table pss.TERMINAL add TERMINAL_MAX_BATCH_NUM INTEGER;
-- add update statement
update pss.TERMINAL set TERMINAL_MAX_BATCH_NUM =999;
alter table pss.TERMINAL modify TERMINAL_MAX_BATCH_NUM not null;
alter table pss.TERMINAL add (
	TERMINAL_BATCH_MAX_TRAN   INTEGER,
	TERMINAL_BATCH_CYCLE_NUM  INTEGER default 1
);
update pss.TERMINAL set TERMINAL_BATCH_MAX_TRAN=999;
alter table pss.TERMINAL modify TERMINAL_BATCH_MAX_TRAN not null;
update pss.TERMINAL set TERMINAL_BATCH_CYCLE_NUM=1;
alter table pss.TERMINAL modify TERMINAL_BATCH_CYCLE_NUM not null;

alter table pss.TERMINAL rename column TERMINAL_ENCRTPY_KEY2 to TERMINAL_ENCRYPT_KEY2;

alter table pss.REFUND add (ref_tran_id NUMBER(20,0));
alter table pss.REFUND add (CONSTRAINT FK_REFUND_REF_TRAN_ID foreign key(REF_TRAN_ID) references TRAN(TRAN_ID));
COMMENT ON COLUMN PSS.REFUND.REF_TRAN_ID is 'Alternate TRAN_ID that this refund references to logically associate this event apply to a past transaction';

insert into pss.terminal_state VALUES (4, 'BUSY_INTERNAL_DATA_MANAGEMENT', 'Terminal busy and unable to process any other events at this time');
insert into pss.terminal_state VALUES (5, 'BUSY_SETTLEMENT_INCOMPLETE', 'Terminal busy and unable to process any events other than settlements at this time');
INSERT into pss.tran_state VALUES ('N', 'PROCESSED_SERVER_SETTLEMENT_INCOMPLETE');

insert into auth_type values ('C','Auth cancellation');
insert into auth_type values ('E','Sale cancellation');
insert into auth_type values ('V','Auth void');
insert into auth_type values ('I','Sale void');
insert into refund_type values ('C','Cancellation');
insert into refund_type values ('V','Void');

CREATE OR REPLACE TRIGGER PSS.TRBI_TERMINAL_BATCH BEFORE INSERT ON TERMINAL_BATCH
  FOR EACH ROW 
BEGIN
	IF :NEW.terminal_batch_id IS NULL
	THEN
		SELECT SEQ_terminal_batch_id.NEXTVAL
		INTO :NEW.terminal_batch_id
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_TERMINAL_BATCH BEFORE UPDATE ON TERMINAL_BATCH
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------
CREATE SEQUENCE pss.SEQ_TERMINAL_BATCH_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
CREATE PUBLIC SYNONYM SEQ_TERMINAL_BATCH_ID for pss.SEQ_TERMINAL_BATCH_ID;

GRANT SELECT, INSERT, UPDATE on pss.terminal_batch to web_user;
GRANT SELECT, INSERT, UPDATE on pss.terminal_batch to web_user;
GRANT SELECT, INSERT, UPDATE on AUTHORITY.HANDLER_RESP_ACTION to web_user;
GRANT SELECT on pss.SEQ_TERMINAL_BATCH_ID to web_user;
