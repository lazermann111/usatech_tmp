CREATE ROLE activemq_user LOGIN PASSWORD 'activemq_user';

CREATE TABLESPACE activemq_master_data LOCATION '/opt/USAT/postgres/data/tblspace/activemq_master_data';
CREATE TABLESPACE activemq_slave_data LOCATION '/opt/USAT/postgres/data/tblspace/activemq_slave_data';

ALTER DATABASE activemq_master SET TABLESPACE activemq_master_data;
ALTER DATABASE activemq_slave SET TABLESPACE activemq_slave_data;

GRANT CREATE ON TABLESPACE activemq_master_data TO activemq_user;
GRANT CREATE ON TABLESPACE activemq_slave_data TO activemq_user;
GRANT CREATE ON DATABASE activemq_master TO activemq_user;
GRANT CREATE ON DATABASE activemq_slave TO activemq_user;
