-- ACTION ---------------------------------------------------

CREATE SEQUENCE SEQ_ACTION_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE DEVICE.ACTION
  (
    ACTION_ID        NUMBER(20,0)     not null,
    ACTION_NAME      VARCHAR2(1024)     not null,
    ACTION_DESC      VARCHAR2(2048),
    CREATED_BY       VARCHAR2(30)     not null,
    CREATED_TS       DATE             not null,
    LAST_UPDATED_BY  VARCHAR2(30)     not null,
    LAST_UPDATED_TS  DATE             not null,
    ACTION_PARAM_TYPE_CD	VARCHAR2(1),
    ACTION_PARAM_SIZE		NUMBER(3,0)),
    CONSTRAINT PK_ACTION primary key(ACTION_ID) USING INDEX TABLESPACE DEVICE_INDX
  )
  TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER TRBI_ACTION BEFORE INSERT ON ACTION
  FOR EACH ROW 
BEGIN
   IF :NEW.action_id IS NULL
   THEN
      SELECT SEQ_ACTION_ID.NEXTVAL
        INTO :NEW.action_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER TRBU_ACTION BEFORE UPDATE ON ACTION
  FOR EACH ROW 
BEGIN
   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

GRANT select, insert, update, delete, references ON ACTION TO pss;

-- ACTION_PARAM ---------------------------------------------------

CREATE SEQUENCE SEQ_ACTION_PARAM_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE DEVICE.ACTION_PARAM
  (
    ACTION_PARAM_ID    NUMBER(20,0)     not null,
    ACTION_ID          NUMBER(20,0)     not null,
    ACTION_PARAM_NAME  VARCHAR2(64)     not null,
    ACTION_PARAM_DESC  VARCHAR2(2048),
    ACTION_PARAM_CD    NUMBER(6,0)      not null,
    CREATED_BY         VARCHAR2(30)     not null,
    CREATED_TS         DATE             not null,
    LAST_UPDATED_BY    VARCHAR2(30)     not null,
    LAST_UPDATED_TS    DATE             not null,
    CONSTRAINT PK_ACTION_PARAM primary key(ACTION_PARAM_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT FK_ACTION_PARAM_ACTION_ID foreign key(ACTION_ID) references ACTION(ACTION_ID)
  )
  TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX UDX_ACTION_PARAM_1 ON ACTION_PARAM(ACTION_ID,ACTION_PARAM_CD);

CREATE OR REPLACE TRIGGER TRBI_ACTION_PARAM BEFORE INSERT ON ACTION_PARAM
  FOR EACH ROW 
BEGIN
   IF :NEW.action_param_id IS NULL
   THEN
      SELECT SEQ_ACTION_PARAM_ID.NEXTVAL
        INTO :NEW.action_param_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER TRBU_ACTION_PARAM BEFORE UPDATE ON ACTION_PARAM
  FOR EACH ROW 
BEGIN
   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

GRANT select, insert, update, delete, references ON ACTION_PARAM TO pss;

-- DEVICE_TYPE_ACTION ---------------------------------------------------

CREATE TABLE DEVICE.DEVICE_TYPE_ACTION
  (
    DEVICE_TYPE_ID   NUMBER(20,0)   not null,
    ACTION_ID        NUMBER(20,0)   not null,
    DEVICE_TYPE_ACTION_CD        NUMBER(3,0)      not null,
    CREATED_BY       VARCHAR2(30)   not null,
    CREATED_TS       DATE           not null,
    LAST_UPDATED_BY  VARCHAR2(30)   not null,
    LAST_UPDATED_TS  DATE           not null,
    CONSTRAINT PK_DEVICE_TYPE_ACTION primary key(DEVICE_TYPE_ID,ACTION_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT FK_DEV_TYP_ACT_DEV_TYP_ID foreign key(DEVICE_TYPE_ID) references DEVICE_TYPE(DEVICE_TYPE_ID),
    CONSTRAINT FK_DEV_TYP_ACT_ACTION_ID foreign key(ACTION_ID) references ACTION(ACTION_ID)
  )
  TABLESPACE DEVICE_DATA;
  
CREATE UNIQUE INDEX UDX_DEVICE_TYPE_ACTION_1 ON DEVICE_TYPE_ACTION(DEVICE_TYPE_ID,DEVICE_TYPE_ACTION_CD);

CREATE OR REPLACE TRIGGER TRBI_DEVICE_TYPE_ACTION BEFORE INSERT ON DEVICE_TYPE_ACTION
  FOR EACH ROW 
BEGIN
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER TRBU_DEVICE_TYPE_ACTION BEFORE UPDATE ON DEVICE_TYPE_ACTION
  FOR EACH ROW 
BEGIN
   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

GRANT select, insert, update, delete, references ON DEVICE_TYPE_ACTION TO pss;

