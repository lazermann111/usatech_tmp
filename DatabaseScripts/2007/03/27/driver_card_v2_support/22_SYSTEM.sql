GRANT SELECT, INSERT, UPDATE, DELETE on pss.consumer_acct_permission to web_user;
GRANT SELECT, INSERT, UPDATE, DELETE on pss.permission_action to web_user;
GRANT SELECT, INSERT, UPDATE, DELETE on pss.permission_action_param to web_user;
GRANT SELECT on device.device_type_action to web_user;
GRANT SELECT on device.action to web_user;
GRANT SELECT on device.action_param to web_user;
GRANT SELECT on pss.consumer_acct to web_user;
GRANT SELECT on pss.consumer_acct_fmt to web_user;
GRANT SELECT on pss.consumer_acct_fmt_authority to web_user;
GRANT SELECT on pss.merchant_consumer_acct to web_user;

INSERT INTO device.action (action_id, action_name, action_desc, action_param_type_cd, action_param_size) VALUES (1,'Show all service fields, do not clear counters',NULL,'N',0); 
INSERT INTO device.action (action_id, action_name, action_desc, action_param_type_cd, action_param_size) VALUES (2,'Show all service fields, clear counters',NULL,'N',0);
INSERT INTO device.action (action_id, action_name, action_desc, action_param_type_cd, action_param_size) VALUES (3,'Show counters only, do not clear counters',NULL,'N',0);
INSERT INTO device.action (action_id, action_name, action_desc, action_param_type_cd, action_param_size) VALUES (4,'Show counters only, clear counters',NULL,'N',0);
INSERT INTO device.action (action_id, action_name, action_desc, action_param_type_cd, action_param_size) VALUES (5,'Show cashless counters, do not clear counters',NULL,'N',0);
INSERT INTO device.action (action_id, action_name, action_desc, action_param_type_cd, action_param_size) VALUES (6,'CCE Settlement Process','Show cashless counters, save current values, re-display on successive swipes.  ','B',1);

INSERT INTO device.action_param(action_param_id, action_id, action_param_name, action_param_desc, action_param_cd) VALUES (3,6,'Capture DEX Data',NULL,0);
INSERT INTO device.action_param(action_param_id, action_id, action_param_name, action_param_desc, action_param_cd) VALUES (4,6,'Call-in to server immediatly',NULL,1);

INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (0,1,0);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (0,2,1);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (0,3,2);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (0,4,3); 
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (0,5,4); 
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (0,6,5); 
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (1,1,0);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (1,2,1); 
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (1,3,2);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (1,4,3);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (1,5,4);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES (1,6,5);

INSERT INTO authority.authority_service VALUES (2,'Privledge-based Account','Account with no monetary balance, used only to check for privledges.',2);

INSERT INTO authority.handler VALUES (3,'USAT Internal Value','Internal::USAT');
INSERT INTO authority.handler VALUES (4,'USAT Internal Maintenance','Internal::USAT::Maint');

INSERT INTO authority.authority_type VALUES (3,'USAT Internal','USAT Internal',3);
INSERT INTO authority.authority_type VALUES (4,'USAT Internal Maint','USAT Internal Maint',4);

INSERT INTO authority.authority(authority_id, authority_name, authority_type_id, authority_service_id) VALUES (4,'USAT Stored Value Card Authority',3,1);
INSERT INTO authority.authority(authority_id, authority_name, authority_type_id, authority_service_id) VALUES (5,'USAT Maintenance Card Authority',4,2);

INSERT INTO pss.consumer_acct_fmt(consumer_acct_fmt_id, consumer_acct_fmt_regex, consumer_acct_fmt_regex_bref, consumer_acct_fmt_name, consumer_acct_fmt_desc) VALUES (0,'6396210([0-9]{3})([0-9]{8})([0-9]{1})=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})','1:15|2:1|3:13|4:3|5:10|6:16|7:14','Gift/Stored Value Card','USAT ISO Card Format Zero');
INSERT INTO pss.consumer_acct_fmt(consumer_acct_fmt_id, consumer_acct_fmt_regex, consumer_acct_fmt_regex_bref, consumer_acct_fmt_name, consumer_acct_fmt_desc) VALUES (1,'6396211([0-9]{1})([0-9]{3})([0-9]{6})([0-9]{1})=([0-9]{4})([0-9]{5})','1:17|2:15|3:1|4:13|5:3|6:16','Operator/Driver Maintenace Card','USAT ISO Card Format One');

INSERT INTO pss.consumer_acct_fmt_authority VALUES (0,4);
INSERT INTO pss.consumer_acct_fmt_authority VALUES (1,5);

COMMIT;
