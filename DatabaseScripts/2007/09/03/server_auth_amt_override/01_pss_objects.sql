ALTER TABLE AUTH ADD (
    AUTH_AUTHORITY_AMT_RQST   NUMBER(14,4),
    AUTH_AUTHORITY_AMT_RCVD   NUMBER(14,4)
);

COMMENT ON COLUMN AUTH.AUTH_AUTHORITY_AMT_RQST is 'Event amount request transmitted to authority, if different from AUTH_AMT.';
COMMENT ON COLUMN AUTH.AUTH_AUTHORITY_AMT_RCVD is 'Event amount response received from authority, if a value is returned that is different from AUTH_AMT_APPROVED.';

ALTER TABLE POS_PTA ADD (
    POS_PTA_PREF_AUTH_AMT           NUMBER(14,4),
    POS_PTA_PREF_AUTH_AMT_MAX       NUMBER(14,4)
);

COMMENT ON COLUMN POS_PTA.POS_PTA_PREF_AUTH_AMT is 'Preferred auth amount to send to authority: takes presidence over requested auth amount, unless requested auth amount exceeds POS_PTA_PREF_AUTH_AMT_MAX (if defined).';
COMMENT ON COLUMN POS_PTA.POS_PTA_PREF_AUTH_AMT_MAX is 'Max preferred auth amount allowed before requested auth amount takes presidence over POS_PTA_PREF_AUTH_AMT.';

ALTER TABLE POS_PTA_TMPL_ENTRY ADD (
    POS_PTA_PREF_AUTH_AMT           NUMBER(14,4),
    POS_PTA_PREF_AUTH_AMT_MAX       NUMBER(14,4)
);

COMMENT ON COLUMN POS_PTA_TMPL_ENTRY.POS_PTA_PREF_AUTH_AMT is 'Preferred auth amount to send to authority: takes presidence over requested auth amount, unless requested auth amount exceeds POS_PTA_PREF_AUTH_AMT_MAX (if defined).';
COMMENT ON COLUMN POS_PTA_TMPL_ENTRY.POS_PTA_PREF_AUTH_AMT_MAX is 'Max preferred auth amount allowed before requested auth amount takes presidence over POS_PTA_PREF_AUTH_AMT.';
