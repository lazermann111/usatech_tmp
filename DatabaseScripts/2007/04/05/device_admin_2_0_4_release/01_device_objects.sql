-- Start of DDL Script for View DEVICE.VW_DEVICE_LAST_ACTIVE
-- Generated 5-Apr-2007 16:04:58 from DEVICE@USADBD02.USATECH.COM

CREATE OR REPLACE VIEW device.vw_device_last_active (
   device_id,
   device_name,
   device_serial_cd,
   created_by,
   created_ts,
   last_updated_by,
   last_updated_ts,
   device_type_id,
   device_active_yn_flag,
   encryption_key,
   last_activity_ts,
   device_desc,
   device_client_serial_cd )
AS
SELECT device_id, device_name, device_serial_cd, created_by, created_ts,
       last_updated_by, last_updated_ts, device_type_id,
       device_active_yn_flag, encryption_key, last_activity_ts, device_desc,
       device_client_serial_cd
  FROM (SELECT   device_id, device_name, device_serial_cd, created_by,
                 created_ts, last_updated_by, last_updated_ts, device_type_id,
                 device_active_yn_flag, encryption_key, last_activity_ts,
                 device_desc, device_client_serial_cd
            FROM device
           WHERE device_active_yn_flag = 'Y'
        ORDER BY DECODE (last_activity_ts,
                         NULL, TO_DATE ('1970', 'YYYY'),
                         last_activity_ts
                        ) DESC)
UNION ALL
SELECT device_id, device_name, device_serial_cd, created_by, created_ts,
       last_updated_by, last_updated_ts, device_type_id,
       device_active_yn_flag, encryption_key, last_activity_ts, device_desc,
       device_client_serial_cd
  FROM (SELECT   d.device_id, d.device_name, d.device_serial_cd, d.created_by,
                 d.created_ts, d.last_updated_by, d.last_updated_ts,
                 d.device_type_id, d.device_active_yn_flag, d.encryption_key,
                 d.last_activity_ts, d.device_desc, d.device_client_serial_cd,
                 rn my_rn_____
            FROM device d,
                 (SELECT device_id, ROWNUM rn
                    FROM (SELECT   dx.device_id
                              FROM device dx,
                                   pos px,
                                   (SELECT   device_name,
                                             SUM
                                                (CASE device_active_yn_flag
                                                    WHEN 'Y'
                                                       THEN 1
                                                    ELSE 0
                                                 END
                                                ) y_cnt
                                        FROM device
                                    GROUP BY device_name) x
                             WHERE dx.device_name = x.device_name
                               AND dx.device_id = px.device_id
                               AND x.y_cnt = 0
                          ORDER BY pos_activation_ts DESC,
                                   DECODE (last_activity_ts,
                                           NULL, TO_DATE ('1970', 'YYYY'),
                                           last_activity_ts
                                          ) DESC) z) y
           WHERE d.device_id = y.device_id
        ORDER BY my_rn_____)
/

-- End of DDL Script for View DEVICE.VW_DEVICE_LAST_ACTIVE

