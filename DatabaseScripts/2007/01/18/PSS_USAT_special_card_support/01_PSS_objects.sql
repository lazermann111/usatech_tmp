/* create PSS.CONSUMER_ACCT_FMT and PSS.MERCHANT_CONSUMER_ACCT */
CREATE SEQUENCE SEQ_CONSUMER_ACCT_FMT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE PSS.CONSUMER_ACCT_FMT
  (
    CONSUMER_ACCT_FMT_ID          NUMBER(20,0),
    CONSUMER_ACCT_FMT_REGEX       VARCHAR2(255)   not null,
    CONSUMER_ACCT_FMT_REGEX_BREF  VARCHAR2(60)    not null,
    CONSUMER_ACCT_FMT_NAME        VARCHAR2(60)    not null,
    CONSUMER_ACCT_FMT_DESC        VARCHAR2(255),
    CREATED_BY                    VARCHAR2(30)    not null,
    CREATED_TS                    DATE            not null,
    LAST_UPDATED_BY               VARCHAR2(30)    not null,
    LAST_UPDATED_TS               DATE            not null,
    CONSTRAINT PK_CONSUMER_ACCT_FMT primary key(CONSUMER_ACCT_FMT_ID) USING INDEX TABLESPACE PSS_INDX
  )
  TABLESPACE PSS_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_CONSUMER_ACCT_FMT BEFORE INSERT ON CONSUMER_ACCT_FMT
  FOR EACH ROW 
BEGIN
   IF :NEW.consumer_acct_fmt_id IS NULL
   THEN
      SELECT SEQ_CONSUMER_ACCT_FMT_ID.NEXTVAL
        INTO :NEW.consumer_acct_fmt_id
        FROM DUAL;
   END IF;
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_CONSUMER_ACCT_FMT BEFORE UPDATE ON CONSUMER_ACCT_FMT
  FOR EACH ROW 
BEGIN
   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE CONSUMER_ACCT_FMT is 'Contains a definition for creating cards issued by USA Tech';

CREATE TABLE PSS.MERCHANT_CONSUMER_ACCT
  (
    MERCHANT_ID       NUMBER(20,0),
    CONSUMER_ACCT_ID  NUMBER(20,0),
    CREATED_BY        VARCHAR2(30)   not null,
    CREATED_TS        DATE           not null,
    LAST_UPDATED_BY   VARCHAR2(30)   not null,
    LAST_UPDATED_TS   DATE           not null,
    CONSTRAINT PK_MERCHANT_CONSUMER_ACCT primary key(MERCHANT_ID,CONSUMER_ACCT_ID) USING INDEX TABLESPACE PSS_INDX,
    CONSTRAINT FK_MERCHANT_CA_MERCHANT_ID foreign key(MERCHANT_ID) references MERCHANT(MERCHANT_ID),
    CONSTRAINT FK_MERCHANT_CA_CA_ID foreign key(CONSUMER_ACCT_ID) references CONSUMER_ACCT(CONSUMER_ACCT_ID)
  )
  TABLESPACE PSS_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_MERCHANT_CONSUMER_ACCT BEFORE INSERT ON MERCHANT_CONSUMER_ACCT
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_MERCHANT_CONSUMER_ACCT BEFORE UPDATE ON MERCHANT_CONSUMER_ACCT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE MERCHANT_CONSUMER_ACCT is 'Describes which merchants (and their associated terminals) consumer accounts may be used by';

/* reuse column PSS.CONSUMER_ACCT.AUTHORITY_ID for a different foreign key */
ALTER TABLE PSS.CONSUMER_ACCT DROP CONSTRAINT FK_CONSUMER_ACCT_AUTHORITY_ID;
ALTER TABLE PSS.CONSUMER_ACCT RENAME COLUMN AUTHORITY_ID TO CONSUMER_ACCT_FORMAT_ID;
ALTER TABLE PSS.CONSUMER_ACCT ADD CONSTRAINT FK_CONSUMER_ACCT_CAF_ID foreign key(CONSUMER_ACCT_FORMAT_ID) references CONSUMER_ACCT_FMT(CONSUMER_ACCT_FMT_ID);

/* add two columns to PSS.CONSUMER_ACCT */
ALTER TABLE PSS.CONSUMER_ACCT ADD ( CONSUMER_ACCT_ISSUE_CD      VARCHAR2(60) );
ALTER TABLE PSS.CONSUMER_ACCT ADD ( CONSUMER_ACCT_VALIDATION_CD VARCHAR2(60) );

/* recompile packages and views invalidated by consumer_acct change */
alter package pss.PKG_REPORTS compile;
alter package pss.PKG_REPORTS compile body;

alter view pss.VW_TLI_PIVOT_VIEW compile;
alter view pss.VW_TLI_PIVOT_VIEW compile;
alter view pss.VW_TRAN_SUMMARY compile;
alter view pss.VW_STUDENT_ACCOUNT compile;
