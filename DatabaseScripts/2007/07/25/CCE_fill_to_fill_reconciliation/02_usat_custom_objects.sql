REM ======================================================================
REM ===   Sql Script for Database : USARDB (Oracle)
REM ===
REM === Build : 264
REM ======================================================================

CREATE SEQUENCE SEQ_CCE_FILL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

REM ======================================================================

CREATE SEQUENCE SEQ_CCE_FILL_LOG_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

REM ======================================================================

CREATE SEQUENCE SEQ_FILE_CONTENT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

REM ======================================================================

CREATE SEQUENCE SEQ_FILE_TRNSFR_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

REM ======================================================================

CREATE SEQUENCE SEQ_FILE_TRNSFR_CCE_FILL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

REM ======================================================================

CREATE SEQUENCE SEQ_SYNC_ENGINE_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

REM ======================================================================

CREATE SEQUENCE SEQ_SYNC_EVENT_LOG_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
REM ======================================================================

CREATE SEQUENCE SEQ_CCE_FILL_EDI_INTRCHNG_CTL
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999 START WITH 10001 NOCACHE;

REM ======================================================================
/*
CREATE TABLE CUSTOMER
  (
    CUSTOMER_ID    NUMBER(20,0),
    CUSTOMER_NAME  VARCHAR2(255),
    CONSTRAINT PK_CUSTOMER primary key(CUSTOMER_ID)
  );
*/
REM ======================================================================
/*
CREATE TABLE TERMINAL
  (
    TERMINAL_ID  NUMBER(20,0),
    CUSTOMER_ID  NUMBER(20,0),
    CONSTRAINT PK_TERMINAL primary key(TERMINAL_ID),
    CONSTRAINT FK_TERMINAL_CUSTOMER_ID foreign key(CUSTOMER_ID) references CUSTOMER(CUSTOMER_ID)
  );
*/
REM ======================================================================

CREATE TABLE USAT_CUSTOM.CCE_FILL_LOG_TYPE
  (
    CCE_FILL_LOG_TYPE_ID    NUMBER(20,0),
    CCE_FILL_LOG_TYPE_NAME  VARCHAR2(60)    not null,
    CCE_FILL_LOG_TYPE_DESC  VARCHAR2(255),
    CONSTRAINT PK_CCE_FILL_LOG_TYPE primary key(CCE_FILL_LOG_TYPE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE CCE_FILL_LOG_TYPE is 'Types of information that may be logged during the processing of a CCE fill event.  Primarily intended for error reports.  Examples may include: no matching batch found, invalid fill information, etc.';

REM ======================================================================

CREATE TABLE USAT_CUSTOM.CCE_FILL_STATE
  (
    CCE_FILL_STATE_ID    NUMBER(20,0),
    CCE_FILL_STATE_NAME  VARCHAR2(60)    not null,
    CCE_FILL_STATE_DESC  VARCHAR2(255),
    CONSTRAINT PK_CCE_FILL_STATE primary key(CCE_FILL_STATE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE CCE_FILL_STATE is 'Current processing state of a CCE fill event.  Examples may include: New, Batch match incomplete (retry state), Batch matched (final state?).';

REM ----------------------------------------------------------------------

insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (1, 'New', 'CCE fill record received and stored');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (2, 'Processing', 'Interim state to indiciate record is currently being processed (semaphore)');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (3, 'Processing Incomplete', 'No USAT fill match could be found during processing (retryable state)');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (4, 'Processing Complete - Exact Match', 'USAT fill found: all criteria matched (final state)');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (5, 'Processing Complete - Best Match', 'Best-fit USAT fill found: all criteria matched within tolerance limits (final state)');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (6, 'Processing Complete Error', 'No USAT fill match could be found (final state)');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (7, 'Duplicate', 'Duplicate CCE fill event (final state)');
insert into CCE_FILL_STATE (CCE_FILL_STATE_ID, CCE_FILL_STATE_NAME, CCE_FILL_STATE_DESC)
 values (8, 'Processing Complete - Manually Reconciled', 'Best-fit USAT fill found: manually chosen');

REM ======================================================================

CREATE TABLE USAT_CUSTOM.FILE_TRNSFR_CCE_FILL_TYPE
  (
    FILE_TRNSFR_CCE_FILL_TYPE_ID    NUMBER(20,0),
    FILE_TRNSFR_CCE_FILL_TYPE_NAME  VARCHAR2(60)    not null,
    FILE_TRNSFR_CCE_FILL_TYPE_DESC  VARCHAR2(255),
    CONSTRAINT PK_FILE_TRNSFR_CCE_FILL_TYPE primary key(FILE_TRNSFR_CCE_FILL_TYPE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE FILE_TRNSFR_CCE_FILL_TYPE is 'Table that represents what information is associated to what kind of file.  For example, types might be: basis input file; edi output file.';

REM ----------------------------------------------------------------------

insert into FILE_TRNSFR_CCE_FILL_TYPE (FILE_TRNSFR_CCE_FILL_TYPE_ID, FILE_TRNSFR_CCE_FILL_TYPE_NAME, FILE_TRNSFR_CCE_FILL_TYPE_DESC)
 values (1, 'Input data file', 'File transfer received from CCE');
insert into FILE_TRNSFR_CCE_FILL_TYPE (FILE_TRNSFR_CCE_FILL_TYPE_ID, FILE_TRNSFR_CCE_FILL_TYPE_NAME, FILE_TRNSFR_CCE_FILL_TYPE_DESC)
 values (2, 'Output data file', 'File transfer to be sent to CCE');

REM ======================================================================

CREATE TABLE USAT_CUSTOM.FILE_TRNSFR_DIRECTION
  (
    FILE_TRNSFR_DIRECTION_CD    char(1),
    FILE_TRNSFR_DIRECTION_NAME  VARCHAR2(60)   not null,
    CONSTRAINT PK_FILE_TRNSFR_DIRECTION primary key(FILE_TRNSFR_DIRECTION_CD) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE FILE_TRNSFR_DIRECTION is 'States that a file transfer may be in.  Examples may include: receive in progress, receive error, received, pending to be sent, sent, sending error, etc.';

REM ----------------------------------------------------------------------

insert into FILE_TRNSFR_DIRECTION (FILE_TRNSFR_DIRECTION_CD, FILE_TRNSFR_DIRECTION_NAME)
 values ('I', 'Inbound');
insert into FILE_TRNSFR_DIRECTION (FILE_TRNSFR_DIRECTION_CD, FILE_TRNSFR_DIRECTION_NAME)
 values ('O', 'Outbound');

REM ======================================================================

CREATE TABLE USAT_CUSTOM.FILE_TRNSFR_STATE
  (
    FILE_TRNSFR_STATE_ID    NUMBER(20,0),
    FILE_TRNSFR_STATE_NAME  VARCHAR2(60)    not null,
    FILE_TRNSFR_STATE_DESC  VARCHAR2(255),
    CONSTRAINT PK_FILE_TRNSFR_STATE primary key(FILE_TRNSFR_STATE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE FILE_TRNSFR_STATE is 'States that a file transfer may be in.  Examples may include: receive in progress, receive error, received, pending to be sent, sent, sending error, etc.';

REM ----------------------------------------------------------------------

insert into FILE_TRNSFR_STATE (FILE_TRNSFR_STATE_ID, FILE_TRNSFR_STATE_NAME)
 values (1, 'Pending Transfer');
insert into FILE_TRNSFR_STATE (FILE_TRNSFR_STATE_ID, FILE_TRNSFR_STATE_NAME)
 values (2, 'Transfer in Progress');
insert into FILE_TRNSFR_STATE (FILE_TRNSFR_STATE_ID, FILE_TRNSFR_STATE_NAME)
 values (3, 'Transfer Send/Receive Success');
insert into FILE_TRNSFR_STATE (FILE_TRNSFR_STATE_ID, FILE_TRNSFR_STATE_NAME)
 values (4, 'Transfer Send/Receive Error');

REM ======================================================================

CREATE TABLE USAT_CUSTOM.MIME_TYPE
  (
    MIME_TYPE_ID     NUMBER(20,0),
    MIME_TYPE_VALUE  VARCHAR2(60)   not null,
    CREATED_BY       VARCHAR2(30)   not null,
    CREATED_TS       DATE           not null,
    CONSTRAINT PK_MIME_TYPE primary key(MIME_TYPE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

CREATE UNIQUE INDEX UDX_MIME_TYPE_1 ON MIME_TYPE(MIME_TYPE_VALUE) TABLESPACE USAT_CUST_INDX;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_MIME_TYPE BEFORE INSERT ON MIME_TYPE
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_MIME_TYPE BEFORE UPDATE ON MIME_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts
	INTO
		:NEW.created_by,
		:NEW.created_ts
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE MIME_TYPE is 'Standard MIME types.';

REM ----------------------------------------------------------------------

insert into mime_type (mime_type_id, mime_type_value)
 values (1, 'Application/BASIS');
insert into mime_type (mime_type_id, mime_type_value)
 values (2, 'Application/EDI-X12');

REM ======================================================================

CREATE TABLE USAT_CUSTOM.SYNC_ENGINE
  (
    SYNC_ENGINE_ID           NUMBER(20,0),
    SYNC_ENGINE_NAME         VARCHAR2(60)   not null,
    SYNC_ENGINE_ACT_YN_FLAG  char(1)        default 'N' not null,
    SYNC_ENGINE_LAST_ACT_TS  DATE           not null,
    CREATED_BY               VARCHAR2(30)   not null,
    CREATED_TS               DATE           not null,
    LAST_UPDATED_BY          VARCHAR2(30)   not null,
    LAST_UPDATED_TS          DATE           not null,
    CONSTRAINT PK_SYNC_ENGINE primary key(SYNC_ENGINE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_SYNC_ENGINE BEFORE INSERT ON SYNC_ENGINE
  FOR EACH ROW 
BEGIN
	IF :NEW.SYNC_ENGINE_ID IS NULL
	THEN
		SELECT SEQ_SYNC_ENGINE_ID.NEXTVAL
		INTO :NEW.SYNC_ENGINE_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_SYNC_ENGINE BEFORE UPDATE ON SYNC_ENGINE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE SYNC_ENGINE is 'Represents an application instance that synchronizes data to/from an external source.';
COMMENT ON COLUMN SYNC_ENGINE.SYNC_ENGINE_NAME is 'Description of engine (such as application name and host on which it is running)';

REM ======================================================================

CREATE TABLE USAT_CUSTOM.SYNC_EVENT_LOG_TYPE
  (
    SYNC_EVENT_LOG_TYPE_ID    NUMBER(20,0),
    SYNC_EVENT_LOG_TYPE_NAME  VARCHAR2(60)    not null,
    SYNC_EVENT_LOG_TYPE_DESC  VARCHAR2(255),
    CONSTRAINT PK_SYNC_EVENT_LOG_TYPE primary key(SYNC_EVENT_LOG_TYPE_ID) USING INDEX TABLESPACE USAT_CUST_INDX
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE SYNC_EVENT_LOG_TYPE is 'Types of logged events that a Sync Engine may generate.  Examples might include: Can''t connect to external source, file load error, file not found, etc.';

REM ======================================================================
/*
CREATE TABLE BATCH
  (
    BATCH_ID     NUMBER(20,0),
    TERMINAL_ID  NUMBER(20,0),
    START_DATE   DATE,
    END_DATE     DATE,
    CONSTRAINT PK_BATCH primary key(BATCH_ID),
    CONSTRAINT FK_BATCH_TERMINAL_ID foreign key(TERMINAL_ID) references REPORT.TERMINAL(TERMINAL_ID)
  );
*/
REM ======================================================================

CREATE TABLE USAT_CUSTOM.CCE_FILL
  (
    CCE_FILL_ID                NUMBER(20,0),
    CCE_FILL_STATE_ID          NUMBER(20,0)    not null,
    CCE_FILL_OUTLET            VARCHAR2(255)   not null,
    CCE_FILL_CREDIT_TO_OUTLET  VARCHAR2(255)   not null,
    CCE_FILL_AMT               NUMBER(20,2)    not null,
    CCE_FILL_DATE              DATE            not null,
    CCE_FILL_INVOICE_NUM       VARCHAR2(255)   not null,
    BATCH_ID                   NUMBER(20,0),
    BATCH_AMT                  NUMBER(20,2),
    CREATED_BY                 VARCHAR2(30)    not null,
    CREATED_TS                 DATE            not null,
    LAST_UPDATED_BY            VARCHAR2(30)    not null,
    LAST_UPDATED_TS            DATE            not null,
    CONSTRAINT PK_CCE_FILL primary key(CCE_FILL_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_CCE_FILL_CCE_FILL_STATE_ID foreign key(CCE_FILL_STATE_ID) references CCE_FILL_STATE(CCE_FILL_STATE_ID),
    CONSTRAINT FK_CCE_FILL_BATCH_ID foreign key(BATCH_ID) references CORP.BATCH(BATCH_ID)
  )
  TABLESPACE USAT_CUST_DATA;

CREATE UNIQUE INDEX UDX_CCE_FILL_1 ON CCE_FILL(BATCH_ID) TABLESPACE USAT_CUST_INDX;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_CCE_FILL BEFORE INSERT ON CCE_FILL
  FOR EACH ROW 
BEGIN
	IF :NEW.CCE_FILL_ID IS NULL
	THEN
		SELECT SEQ_CCE_FILL_ID.NEXTVAL
		INTO :NEW.CCE_FILL_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_CCE_FILL BEFORE UPDATE ON CCE_FILL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE CCE_FILL is 'CCE fill events, as extracted from BASIS reports transmitted by CCE';
COMMENT ON COLUMN CCE_FILL.CCE_FILL_OUTLET is 'Unique idenfitier for a given device in the field.';
COMMENT ON COLUMN CCE_FILL.CCE_FILL_CREDIT_TO_OUTLET is 'CCE sales center location name.';
COMMENT ON COLUMN CCE_FILL.BATCH_ID is 'USAT batch record that (best) matches this CCE fill event.';
COMMENT ON COLUMN CCE_FILL.BATCH_AMT is 'Sum of USAT transaction events associated with the USAT batch fill window.';

REM ======================================================================

CREATE TABLE USAT_CUSTOM.CCE_FILL_LOG
  (
    CCE_FILL_LOG_ID       NUMBER(20,0),
    CCE_FILL_ID           NUMBER(20,0)    not null,
    CCE_FILL_LOG_TYPE_ID  NUMBER(20,0)    not null,
    CCE_FILL_LOG_VALUE    VARCHAR2(255)   not null,
    BATCH_ID              NUMBER(20,0),
    BATCH_AMT             NUMBER(20,2),
    CREATED_BY            VARCHAR2(30)    not null,
    CREATED_TS            DATE            not null,
    LAST_UPDATED_BY       VARCHAR2(30)    not null,
    LAST_UPDATED_TS       DATE            not null,
    CONSTRAINT PK_CCE_FILL_LOG primary key(CCE_FILL_LOG_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_CCE_FILL_LOG_CCE_FILL_ID foreign key(CCE_FILL_ID) references CCE_FILL(CCE_FILL_ID) on delete CASCADE,
    CONSTRAINT FK_CCE_FILL_LOG_CFLT_ID foreign key(CCE_FILL_LOG_TYPE_ID) references CCE_FILL_LOG_TYPE(CCE_FILL_LOG_TYPE_ID)
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_CCE_FILL_LOG BEFORE INSERT ON CCE_FILL_LOG
  FOR EACH ROW 
BEGIN
	IF :NEW.CCE_FILL_LOG_ID IS NULL
	THEN
		SELECT SEQ_CCE_FILL_LOG_ID.NEXTVAL
		INTO :NEW.CCE_FILL_LOG_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_CCE_FILL_LOG BEFORE UPDATE ON CCE_FILL_LOG
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE CCE_FILL_LOG is 'Log of information regarding the processing of a CCE fill event.';

REM ----------------------------------------------------------------------

insert into CCE_FILL_LOG_TYPE (CCE_FILL_LOG_TYPE_ID, CCE_FILL_LOG_TYPE_NAME, CCE_FILL_LOG_TYPE_DESC)
 values (1, 'No matching batch found yet', 'For given date and outlet number, no matching batch_id was found yet');
insert into CCE_FILL_LOG_TYPE (CCE_FILL_LOG_TYPE_ID, CCE_FILL_LOG_TYPE_NAME, CCE_FILL_LOG_TYPE_DESC)
 values (2, 'Batch found; amounts do not match', 'For closest matching batch, amounts do not match (outside tolerance)');
insert into CCE_FILL_LOG_TYPE (CCE_FILL_LOG_TYPE_ID, CCE_FILL_LOG_TYPE_NAME, CCE_FILL_LOG_TYPE_DESC)
 values (3, 'No matching batch found', 'No matching batch found: retry attempts exceeded');
insert into CCE_FILL_LOG_TYPE (CCE_FILL_LOG_TYPE_ID, CCE_FILL_LOG_TYPE_NAME, CCE_FILL_LOG_TYPE_DESC)
 values (4, 'User comment', 'Manually entered user comment');
insert into CCE_FILL_LOG_TYPE (CCE_FILL_LOG_TYPE_ID, CCE_FILL_LOG_TYPE_NAME, CCE_FILL_LOG_TYPE_DESC)
 values (5, 'Automated reconciliation run', 'Event log of automated reconciliation analysis for this cce_fill record');

REM ======================================================================

CREATE TABLE USAT_CUSTOM.FILE_CONTENT
  (
    FILE_CONTENT_ID    NUMBER(20,0),
    MIME_TYPE_ID       NUMBER(20,0)   not null,
    FILE_CONTENT_DATA  blob           not null,
    CREATED_BY         VARCHAR2(30)   not null,
    CREATED_TS         DATE           not null,
    LAST_UPDATED_BY    VARCHAR2(30)   not null,
    LAST_UPDATED_TS    DATE           not null,
    CONSTRAINT PK_FILE_CONTENT primary key(FILE_CONTENT_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_FILE_CONTENT_MIME_TYPE_ID foreign key(MIME_TYPE_ID) references MIME_TYPE(MIME_TYPE_ID)
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_FILE_CONTENT BEFORE INSERT ON FILE_CONTENT
  FOR EACH ROW 
BEGIN
	IF :NEW.FILE_CONTENT_ID IS NULL
	THEN
		SELECT SEQ_FILE_CONTENT_ID.NEXTVAL
		INTO :NEW.FILE_CONTENT_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_FILE_CONTENT BEFORE UPDATE ON FILE_CONTENT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE FILE_CONTENT is 'Actual file content received by a file transfer.';

REM ======================================================================

CREATE TABLE USAT_CUSTOM.FILE_TRNSFR
  (
    FILE_TRNSFR_ID         NUMBER(20,0),
    FILE_TRNSFR_STATE_ID   NUMBER(20,0)    not null,
    FILE_TRNSFR_NAME       VARCHAR2(255),
    FILE_CONTENT_ID        NUMBER(20,0)    not null,
    FILE_TRNSFR_DIRECTION  char(1)         not null,
    CUSTOMER_ID            NUMBER(20,0),
    CREATED_BY             VARCHAR2(30)    not null,
    CREATED_TS             DATE            not null,
    LAST_UPDATED_BY        VARCHAR2(30)    not null,
    LAST_UPDATED_TS        DATE            not null,
    CONSTRAINT PK_FILE_TRNSFR primary key(FILE_TRNSFR_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_FILE_TRNSFR_FTS_ID foreign key(FILE_TRNSFR_STATE_ID) references FILE_TRNSFR_STATE(FILE_TRNSFR_STATE_ID),
    CONSTRAINT FK_FILE_TRNSFR_FILE_CONTENT_ID foreign key(FILE_CONTENT_ID) references FILE_CONTENT(FILE_CONTENT_ID),
    CONSTRAINT FK_FILE_TRNSFR_FTD foreign key(FILE_TRNSFR_DIRECTION) references FILE_TRNSFR_DIRECTION(FILE_TRNSFR_DIRECTION_CD),
    CONSTRAINT FK_FILE_TRNSFR_CUSTOMER_ID foreign key(CUSTOMER_ID) references CORP.CUSTOMER(CUSTOMER_ID)
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_FILE_TRNSFR BEFORE INSERT ON FILE_TRNSFR
  FOR EACH ROW 
BEGIN
	IF :NEW.FILE_TRNSFR_ID IS NULL
	THEN
		SELECT SEQ_FILE_TRNSFR_ID.NEXTVAL
		INTO :NEW.FILE_TRNSFR_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_FILE_TRNSFR BEFORE UPDATE ON FILE_TRNSFR
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE FILE_TRNSFR is 'Represents a file transfer event (pending transfer, sent, or received).';

REM ======================================================================

CREATE TABLE USAT_CUSTOM.FILE_TRNSFR_CCE_FILL
  (
    FILE_TRNSFR_ID                NUMBER(20,0),
    CCE_FILL_ID                   NUMBER(20,0),
    FILE_TRNSFR_CCE_FILL_TYPE_ID  NUMBER(20,0),
    CREATED_BY                    VARCHAR2(30)   not null,
    CREATED_TS                    DATE           not null,
    LAST_UPDATED_BY               VARCHAR2(30)   not null,
    LAST_UPDATED_TS               DATE           not null,
    CONSTRAINT PK_FILE_TRNSFR_CCE_FILL primary key(FILE_TRNSFR_ID,CCE_FILL_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_FILE_TRNSFR_CCE_F_FT_ID foreign key(FILE_TRNSFR_ID) references FILE_TRNSFR(FILE_TRNSFR_ID) on delete CASCADE,
    CONSTRAINT FK_FILE_TRNSFR_CCE_F_CF_ID foreign key(CCE_FILL_ID) references CCE_FILL(CCE_FILL_ID) on delete CASCADE,
    CONSTRAINT FK_FILE_TRNSFR_CCE_F_FTCFT_ID foreign key(FILE_TRNSFR_CCE_FILL_TYPE_ID) references FILE_TRNSFR_CCE_FILL_TYPE(FILE_TRNSFR_CCE_FILL_TYPE_ID)
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_FILE_TRNSFR_CCE_FILL BEFORE INSERT ON FILE_TRNSFR_CCE_FILL
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_FILE_TRNSFR_CCE_FILL BEFORE UPDATE ON FILE_TRNSFR_CCE_FILL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE FILE_TRNSFR_CCE_FILL is 'Represents a many-to-many mapping between fill events and files that they were generated from or exported to.';

REM ======================================================================

CREATE TABLE USAT_CUSTOM.SYNC_EVENT_LOG
  (
    SYNC_EVENT_LOG_ID       NUMBER(20,0),
    SYNC_ENGINE_ID          NUMBER(20,0)   not null,
    SYNC_EVENT_LOG_TYPE_ID  NUMBER(20,0)   not null,
    CREATED_BY              VARCHAR2(30)   not null,
    CREATED_TS              DATE           not null,
    LAST_UPDATED_BY         VARCHAR2(30)   not null,
    LAST_UPDATED_TS         DATE           not null,
    CONSTRAINT PK_SYNC_EVENT_LOG primary key(SYNC_EVENT_LOG_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_SYNC_EVENT_LSE_ID foreign key(SYNC_ENGINE_ID) references SYNC_ENGINE(SYNC_ENGINE_ID) on delete CASCADE,
    CONSTRAINT FK_SYNC_EVENT_LSE_LT_ID foreign key(SYNC_EVENT_LOG_TYPE_ID) references SYNC_EVENT_LOG_TYPE(SYNC_EVENT_LOG_TYPE_ID)
  )
  TABLESPACE USAT_CUST_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_SYNC_EVENT_LOG BEFORE INSERT ON SYNC_EVENT_LOG
  FOR EACH ROW 
BEGIN
	IF :NEW.SYNC_EVENT_LOG_ID IS NULL
	THEN
		SELECT SEQ_SYNC_EVENT_LOG_ID.NEXTVAL
		INTO :NEW.SYNC_EVENT_LOG_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_SYNC_EVENT_LOG BEFORE UPDATE ON SYNC_EVENT_LOG
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE SYNC_EVENT_LOG is 'Log of important events created by Sync Engines.';

REM ======================================================================

