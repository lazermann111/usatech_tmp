--new sequence
CREATE SEQUENCE SEQ_CCE_FILL_EDI_OUTPUT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

-- new table
CREATE TABLE USAT_CUSTOM.CCE_FILL_EDI_OUTPUT
  (
    CCE_FILL_EDI_OUTPUT_ID        NUMBER(20,0),
    DOC_ID                        INTEGER         not null,
    FILE_CONTENT_ID               NUMBER(20,0)    not null,
    CCE_FILL_EDI_OUTPUT_CRM_AMT   NUMBER(20,2)    not null,
    CCE_FILL_EDI_OUTPUT_CRM_DESC  VARCHAR2(255),
    CREATED_BY                    VARCHAR2(30)    not null,
    CREATED_TS                    DATE            not null,
    LAST_UPDATED_BY               VARCHAR2(30)    not null,
    LAST_UPDATED_TS               DATE            not null,
    CONSTRAINT PK_CCE_FILL_EDI_OUTPUT primary key(CCE_FILL_EDI_OUTPUT_ID) USING INDEX TABLESPACE USAT_CUST_INDX,
    CONSTRAINT FK_CCE_FILL_EDI_OUTPUT_DOC_ID foreign key(DOC_ID) references CORP.DOC(DOC_ID),
    CONSTRAINT FK_CCE_FILL_EDI_OUTPUT_FC_ID foreign key(FILE_CONTENT_ID) references FILE_CONTENT(FILE_CONTENT_ID)
  )
  TABLESPACE USAT_CUST_DATA;

CREATE UNIQUE INDEX UDX_CCE_FILL_EDI_OUTPUT_1 ON CCE_FILL_EDI_OUTPUT(DOC_ID) TABLESPACE USAT_CUST_INDX;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_CCE_FILL_EDI_OUTPUT BEFORE INSERT ON CCE_FILL_EDI_OUTPUT
  FOR EACH ROW 
BEGIN
	IF :NEW.CCE_FILL_EDI_OUTPUT_ID IS NULL
	THEN
		SELECT SEQ_CCE_FILL_EDI_OUTPUT_ID.NEXTVAL
		INTO :NEW.CCE_FILL_EDI_OUTPUT_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_CCE_FILL_EDI_OUTPUT BEFORE UPDATE ON CCE_FILL_EDI_OUTPUT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE CCE_FILL_EDI_OUTPUT is 'Excapsulates details and CORP.DOC functional constraints for EFT process: insures only one DOC is linked to an EDI, and also retains DOC-specific details such as CRM amount.';
