CREATE TABLESPACE "PSS_STAT_DATA" DATAFILE '/u03/oradata/usadbt03/pss_stat_data01.dbf' SIZE 100M AUTOEXTEND ON NEXT 128K MAXSIZE 2048M LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
CREATE TABLESPACE "PSS_STAT_INDX" DATAFILE '/u03/oradata/usadbt03/pss_stat_indx01.dbf' SIZE 100M AUTOEXTEND ON NEXT 128K MAXSIZE 2048M LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

ALTER USER PSS QUOTA UNLIMITED ON PSS_STAT_INDX; 
ALTER USER PSS QUOTA UNLIMITED ON PSS_STAT_DATA; 


CREATE OR REPLACE FUNCTION dbadmin.READ_LONG(
       l_long_value LONG)
 RETURN VARCHAR
AS
     l_lob CLOB;
BEGIN
     DBMS_LOB.CREATETEMPORARY(l_lob, TRUE); 
     DBMS_LOB.WRITEAPPEND (l_lob, LENGTH(l_long_value), l_long_value);
     RETURN DBMS_LOB.SUBSTR(l_lob, 4000, 1);
END;
/ 

CREATE OR REPLACE PROCEDURE dbadmin.add_monthly_partition(
          v_table_owner VARCHAR2,
          v_table_name VARCHAR2,
          v_month DATE)
AUTHID CURRENT_USER
AS
    v_part_name  VARCHAR2(12);
    v_part_value VARCHAR2(10);
    v_count NUMBER(1);
    v_sql VARCHAR2(4000);
BEGIN
    --Construct partition_name for next month with format "YYYY-MM-01";
    SELECT TO_CHAR(TRUNC(ADD_MONTHS(v_month, 1), 'MM'), 'YYYY-MM-DD') INTO v_part_name FROM DUAL;
    SELECT TO_CHAR(TRUNC(ADD_MONTHS(v_month, 2), 'MM'), 'YYYY/MM/DD') INTO v_part_value FROM DUAL;
    
    -- Check if the partition exists
    SELECT COUNT(*) 
      INTO v_count
      FROM DBA_TAB_PARTITIONS
     WHERE TABLE_OWNER = v_table_owner
       AND TABLE_NAME = v_table_name
       AND PARTITION_NAME = v_part_name;
    
    -- Add partition
    IF v_count = 0 THEN
       v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' ADD PARTITION "'||v_part_name||'" VALUES LESS THAN (TO_DATE ('''||v_part_value||''', ''YYYY/MM/DD''))';
       DBMS_OUTPUT.PUT_LINE (v_sql);
       EXECUTE IMMEDIATE v_sql;
    END IF;
END;
/

CREATE OR REPLACE PROCEDURE dbadmin.DROP_PARTITIONS_BEFORE(
    v_table_owner VARCHAR2,
    v_table_name VARCHAR2,
    v_date_before DATE)
AUTHID CURRENT_USER
AS
    v_sql VARCHAR2(4000);
    v_high_value DATE;
    CURSOR part_list_cursor (c_table_owner VARCHAR2, c_table_name VARCHAR2) IS
        SELECT PARTITION_NAME, HIGH_VALUE, PARTITION_POSITION,
               LAST_VALUE(PARTITION_POSITION) 
                 OVER(ORDER BY PARTITION_POSITION 
                 ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) LAST_POSITION
        FROM DBA_TAB_PARTITIONS
        WHERE TABLE_OWNER = c_table_owner
        AND TABLE_NAME = c_table_name
        ORDER BY PARTITION_POSITION;
BEGIN
    FOR part_rec IN part_list_cursor(v_table_owner, v_table_name) LOOP
        IF part_rec.LAST_POSITION > part_rec.PARTITION_POSITION THEN
            EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_high_value;
            IF v_high_value < v_date_before THEN 
                v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' DROP PARTITION "'|| part_rec.partition_name||'" UPDATE INDEXES';
                DBMS_OUTPUT.PUT_LINE (v_sql);
                EXECUTE IMMEDIATE v_sql;
            ELSE
                EXIT;
            END IF;
        ELSE
            EXIT;
        END IF;
    END LOOP;
END;
/

grant execute on DBADMIN.add_monthly_partition to system;
grant execute on DBADMIN.DROP_PARTITIONS_BEFORE to system;
