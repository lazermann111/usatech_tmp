INSERT INTO sony.alert_context (context_display_msg, alert_context_code)
VALUES ('SnapLab', 161);

INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (0, 'Media mismatch', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (1, 'Jamming', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (2, 'Ribbon End', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (3, 'Paper End', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (4, 'No Ribbon', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (5, 'No Paper', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (6, 'Door Open', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (7, 'Motor Sensor Trouble', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (8, 'Paper Tag Error', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (9, 'Ribbon Tag Error', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (10, 'Head is cooling', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (11, 'Paper Load Error', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (32, 'Data Transfer Error', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (33, 'Initializing Print Status', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (34, 'Invalid Command/Parameter', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (35, 'Abort by Stop Button', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (36, 'Invalid Print Size', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (37, 'Unknown Printer Error', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (38, 'Hardware Error', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (39, 'USB Error', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (40, 'System Error', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (48, 'Printer Error', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (49, 'Too Many Memory Cards', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (50, 'Unknown Memory Card', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (51, 'Ribbon  & Paper Size Change ', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (52, 'Memory card slot Error', 7, 10);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (64, 'Unknown System error', 7, 20);
INSERT INTO sony.alert_msg (alert_msg_code, alert_display_msg, alert_msg_type, priority)
values (65, 'Receipt Printer Paper Low', 7, 10);


UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 0;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 1;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 7;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 8;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 9;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 11;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 12;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 14;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 15;
UPDATE sony.alert_msg SET priority = 11
	WHERE alert_msg_type = 2 and alert_msg_code = 16;
