-- update to SONY_PRINTER triggers
CREATE OR REPLACE TRIGGER SONY.TRBI_SONY_PRINTER BEFORE INSERT ON SONY.SONY_PRINTER
  FOR EACH ROW 
Begin
	IF :NEW.SONY_PRINTER_ID IS NULL
	THEN
		SELECT SEQ_SONY_PRINTER_ID.NEXTVAL
		INTO :NEW.SONY_PRINTER_ID
		FROM DUAL;
	END IF;
    IF (:new.prints_remaining_threshold IS NULL AND
       (:new.printer_num = 1 or :new.printer_num = 2)) THEN
       
        :new.prints_remaining_threshold := 50;
        
    ELSIF (:new.prints_remaining_threshold IS NULL AND
          :new.printer_num = 3) THEN
       
        :new.prints_remaining_threshold := 50;
        
    END IF;
End;
/


-- new table SONY_PRINTER_STATUS_TYPE
CREATE TABLE SONY.SONY_PRINTER_STATUS_TYPE
  (
    SONY_PRINTER_STATUS_TYPE_ID    NUMBER(20,0),
    SONY_PRINTER_STATUS_TYPE_NAME  VARCHAR2(60)    not null,
    SONY_PRINTER_STATUS_TYPE_DESC  VARCHAR2(255),
    CREATED_BY                     VARCHAR2(30)    not null,
    CREATED_TS                     DATE            not null,
    LAST_UPDATED_BY                VARCHAR2(30)    not null,
    LAST_UPDATED_TS                DATE            not null,
    CONSTRAINT PK_SONY_PRINTER_STATUS_TYPE primary key(SONY_PRINTER_STATUS_TYPE_ID) USING INDEX TABLESPACE SONY_INDX
  )
  TABLESPACE SONY_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SONY.TRBI_SONY_PRINTER_STATUS_TYPE BEFORE INSERT ON SONY.SONY_PRINTER_STATUS_TYPE
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SONY.TRBU_SONY_PRINTER_STATUS_TYPE BEFORE UPDATE ON SONY.SONY_PRINTER_STATUS_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE SONY.SONY_PRINTER_STATUS_TYPE is 'Functional states of a Sony PictureStation printer.';

REM ----------------------------------------------------------------------

INSERT INTO sony.sony_printer_status_type (sony_printer_status_type_id, sony_printer_status_type_name)
 VALUES (0, 'Status Unknown');
INSERT INTO sony.sony_printer_status_type (sony_printer_status_type_id, sony_printer_status_type_name)
 VALUES (1, 'Available');
INSERT INTO sony.sony_printer_status_type (sony_printer_status_type_id, sony_printer_status_type_name)
 VALUES (2, 'Offline');
INSERT INTO sony.sony_printer_status_type (sony_printer_status_type_id, sony_printer_status_type_name)
 VALUES (3, 'N/A');


-- new sony SONY_PRINTER table columns
ALTER TABLE SONY.SONY_PRINTER ADD (SONY_PRINTER_STATUS_TYPE_ID NUMBER(20,0)    default 0 not null);
ALTER TABLE SONY.SONY_PRINTER ADD (PRINTER_MODEL               VARCHAR2(255));
ALTER TABLE SONY.SONY_PRINTER ADD (PRINTER_LABEL               VARCHAR2(255));
ALTER TABLE SONY.SONY_PRINTER ADD (PRINTER_SERIAL_CD           VARCHAR2(255));
ALTER TABLE SONY.SONY_PRINTER ADD (CONSTRAINT FK_SP_SONY_PRINTER_STATUS_TYPE
 foreign key(SONY_PRINTER_STATUS_TYPE_ID) references SONY.SONY_PRINTER_STATUS_TYPE(SONY_PRINTER_STATUS_TYPE_ID));


-- new sony ALERT_EMAIL table objects
CREATE SEQUENCE SONY.SEQ_ALERT_EMAIL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE SONY.ALERT_EMAIL
  (
    ALERT_EMAIL_ID    NUMBER(20,0),
    DEVICE_SERIAL_CD  VARCHAR2(20)    not null,
    ALERT_EMAIL_ADDR  VARCHAR2(255)   not null,
    ALERT_EMAIL_NAME  VARCHAR2(255)   not null,
    CREATED_BY        VARCHAR2(30)    not null,
    CREATED_TS        DATE            not null,
    LAST_UPDATED_BY   VARCHAR2(30)    not null,
    LAST_UPDATED_TS   DATE            not null,
    CONSTRAINT PK_ALERT_EMAIL primary key(ALERT_EMAIL_ID) USING INDEX TABLESPACE SONY_INDX
  )
  TABLESPACE SONY_DATA;

CREATE UNIQUE INDEX SONY.UDX_ALERT_EMAIL_1 ON SONY.ALERT_EMAIL(DEVICE_SERIAL_CD,ALERT_EMAIL_ADDR) TABLESPACE SONY_INDX;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SONY.TRBI_ALERT_EMAIL BEFORE INSERT ON SONY.ALERT_EMAIL
  FOR EACH ROW 
BEGIN
	IF :NEW.ALERT_EMAIL_ID IS NULL
	THEN
		SELECT SEQ_ALERT_EMAIL_ID.NEXTVAL
		INTO :NEW.ALERT_EMAIL_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SONY.TRBU_ALERT_EMAIL BEFORE UPDATE ON SONY.ALERT_EMAIL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE SONY.ALERT_EMAIL is 'E-mail to device serial cd mapping for use when sending alerts.';
COMMENT ON COLUMN SONY.ALERT_EMAIL.ALERT_EMAIL_NAME is 'Display name to use for e-mail address recipient.';


-- update to prints remaining alert trigger

-- Start of DDL Script for Trigger SONY.TRAIU_SONY_PRINTER
-- Generated 5/11/2007 12:48:11 PM from SONY@USADBD02.USATECH.COM

CREATE OR REPLACE TRIGGER sony.traiu_sony_printer
 AFTER
  INSERT OR UPDATE
 ON sony.sony_printer
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
   n_return_code     FLOAT (10);
   v_error_message   VARCHAR2 (2000);
   v_location_name   location.location.location_name%TYPE;
   v_device_serial_cd   device.device_serial_cd%TYPE;
   v_alert_email_addr   sony.alert_email.alert_email_addr%TYPE;
   v_alert_email_name   sony.alert_email.alert_email_name%TYPE;

   CURSOR c_get_location_name IS
      SELECT l.location_name, d.device_serial_cd
        FROM location.location l,
             device.device d,
             pss.pos p
       WHERE d.device_name = :NEW.machine_id
         AND d.device_id = p.device_id
         AND p.location_id = l.location_id;

   CURSOR c_get_email_list
   (v_serial_cd IN v_device_serial_cd%TYPE) IS
      SELECT alert_email_addr, alert_email_name
        FROM sony.alert_email
       WHERE device_serial_cd = v_serial_cd;

BEGIN
   IF (       :NEW.total_prints_remaining < :NEW.prints_remaining_threshold
          AND (   :OLD.total_prints_remaining >= :NEW.prints_remaining_threshold
               OR :OLD.total_prints_remaining IS NULL
              )
       OR (:NEW.total_prints_remaining <= 0)
      ) THEN

      OPEN c_get_location_name;
      FETCH c_get_location_name
       INTO v_location_name, v_device_serial_cd;
      CLOSE c_get_location_name;

      OPEN c_get_email_list (v_device_serial_cd);
      LOOP
         FETCH c_get_email_list
          INTO v_alert_email_addr, v_alert_email_name;
         EXIT WHEN c_get_email_list%NOTFOUND;

         INSERT INTO ob_email_queue
                     (ob_email_from_email_addr, ob_email_from_name,
                      ob_email_to_email_addr, ob_email_to_name,
                      ob_email_subject,
                      ob_email_msg
                     )
              VALUES ('monitor@usatech.com', 'USA Tech Monitor',
                      --'sonykioskmonitor@usatech.com', 'Kiosk Monitor',
                      --'kiosk@am.sony.com', 'Kiosk Monitor',
                      v_alert_email_addr, v_alert_email_name,
                      'Paper Low',
                         'The paper level ('
                      || :NEW.total_prints_remaining
                      || ') of printer number '
                      || :NEW.printer_num
                      || ' at the location '
                      || v_location_name
                      || ' is below the threshold('
                      || :NEW.prints_remaining_threshold
                      || ').'
                     );

      END LOOP;
      CLOSE c_get_email_list;

   END IF;
END;
/


-- End of DDL Script for Trigger SONY.TRAIU_SONY_PRINTER

