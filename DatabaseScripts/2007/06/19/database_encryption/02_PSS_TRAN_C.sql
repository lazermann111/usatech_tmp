CREATE TABLE pss.tran_c
(   
    tran_id                        NUMBER(20,0) NOT NULL,
    kid                            NUMBER(9,0) NOT NULL,
    hash_type_cd                   VARCHAR2(16) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    tran_account_pin_v             VARCHAR2(2048),
    tran_account_pin_h             VARCHAR2(2048),
    tran_parsed_acct_name_v        VARCHAR2(2048),
    tran_parsed_acct_name_h        VARCHAR2(2048),
    tran_parsed_acct_exp_date_v    VARCHAR2(2048),
    tran_parsed_acct_exp_date_h    VARCHAR2(2048),
    tran_parsed_acct_num_v         VARCHAR2(2048),
    tran_parsed_acct_num_h         VARCHAR2(2048)
)
  TABLESPACE  pss_data;

ALTER TABLE pss.tran_c
ADD CONSTRAINT pk_tran_c PRIMARY KEY (tran_id)
USING INDEX
  TABLESPACE  pss_data;

CREATE INDEX pss.idx_tran_c_kid ON pss.tran_c
(
    kid                             ASC
)
  TABLESPACE  pss_indx;

CREATE OR REPLACE TRIGGER pss.trbu_tran_c
 BEFORE
  UPDATE
 ON pss.tran_c
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;/

CREATE OR REPLACE TRIGGER pss.trbi_tran_c
 BEFORE
  INSERT
 ON pss.tran_c
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;/

ALTER TABLE pss.tran_c
ADD CONSTRAINT fk_tran_c_hash_type_cd FOREIGN KEY (hash_type_cd)
REFERENCES pss.hash_type (hash_type_cd);

ALTER TABLE pss.tran_c
ADD CONSTRAINT pk_tran_c_tran_id FOREIGN KEY (tran_id)
REFERENCES pss.tran (tran_id);

GRANT SELECT, INSERT, UPDATE ON pss.tran_c TO web_user;

