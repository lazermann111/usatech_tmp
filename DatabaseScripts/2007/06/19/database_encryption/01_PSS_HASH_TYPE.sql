CREATE TABLE pss.hash_type
(
    hash_type_cd                   VARCHAR2(16) NOT NULL,
    hash_type_desc                 VARCHAR2(256) NOT NULL
)
  TABLESPACE  pss_data;

ALTER TABLE pss.hash_type
ADD CONSTRAINT pk_hash_type PRIMARY KEY (hash_type_cd)
USING INDEX
  TABLESPACE  pss_data;

INSERT INTO pss.hash_type VALUES ('SHA1','SHA-1');

GRANT SELECT ON pss.hash_type TO web_user;

