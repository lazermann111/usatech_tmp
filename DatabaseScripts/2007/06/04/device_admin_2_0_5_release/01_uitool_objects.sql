-- New da_file_cache table
CREATE SEQUENCE UITOOL.SEQ_DA_FILE_CACHE_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE UITOOL.DA_FILE_CACHE
  (
    DA_FILE_CACHE_ID           NUMBER(20,0),
    DA_FILE_CACHE_OBJ_PATH     VARCHAR2(255)   not null,
    DA_FILE_CACHE_LAST_MOD_TS  DATE            not null,
    CREATED_BY                 VARCHAR2(30)    not null,
    CREATED_TS                 DATE            not null,
    LAST_UPDATED_BY            VARCHAR2(30)    not null,
    LAST_UPDATED_TS            DATE            not null,
    CONSTRAINT PK_DA_FILE_CACHE primary key(DA_FILE_CACHE_ID) USING INDEX TABLESPACE UITOOL_INDX
  )
  TABLESPACE UITOOL_DATA;

CREATE UNIQUE INDEX UITOOL.UDX_DA_FILE_CACHE_1 ON UITOOL.DA_FILE_CACHE(DA_FILE_CACHE_OBJ_PATH) TABLESPACE UITOOL_INDX;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER UITOOL.TRBI_DA_FILE_CACHE BEFORE INSERT ON UITOOL.DA_FILE_CACHE
  FOR EACH ROW 
BEGIN
	IF :NEW.DA_FILE_CACHE_ID IS NULL
	THEN
		SELECT SEQ_DA_FILE_CACHE_ID.NEXTVAL
		INTO :NEW.DA_FILE_CACHE_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER UITOOL.TRBU_DA_FILE_CACHE BEFORE UPDATE ON UITOOL.DA_FILE_CACHE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE UITOOL.DA_FILE_CACHE is 'Stores the last modified date for (auto-generated) static content files, for use with cache-buster algorithms.';
