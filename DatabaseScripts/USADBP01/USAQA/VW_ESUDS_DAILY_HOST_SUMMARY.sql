CREATE OR REPLACE VIEW USAQA.VW_ESUDS_DAILY_HOST_SUMMARY
(LOCATION_NAME, DEVICE_NAME, HOST_ID, TRAN_DT, TRAN_CNT)
AS 
SELECT   replace(l.location_name,',',' ') as location_name,
         d.device_name,
         a.host_id,
         TRUNC (t.tran_start_ts) AS tran_dt,
         COUNT (a.tran_line_item_id) AS tran_cnt
    FROM pss.tran_line_item a,
         device.HOST h,
         device.device d,
         device.host_type ht,
         device.host_type_host_group_type hthgt,    --added after schema changes of 5/24/06
         LOCATION.LOCATION l,
         pss.pos p,
         pss.tran t
   WHERE hthgt.host_group_type_id in (1,2,3,4)      --added after schema changes of 5/24/06
        --ht.host_type_cd IN ('A', 'D', 'G', 'H', 'R', 'S', 'U', 'V', 'W') --invalid after schema changes of 5/24/06
     AND h.host_type_id = hthgt.host_type_id
     AND t.tran_start_ts between (trunc(sysdate)-1) and (trunc(sysdate))
     AND a.tran_id = t.tran_id
     AND a.host_id = h.host_id
     AND d.device_id = h.device_id
     AND p.device_id = d.device_id
     AND l.location_id = p.location_id
GROUP BY l.location_name,
         d.device_name,
         a.host_id,
         TRUNC (t.tran_start_ts);
