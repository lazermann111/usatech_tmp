CREATE OR REPLACE VIEW USAQA.VW_BILLY_BEAVER_TEST
(DEVICE_NAME, HOST_ID, LOCATION_NAME, CONSUMER_EMAIL_ADDR1, CONSUMER_FNAME, 
 CONSUMER_LNAME, TRAN_ID, CARD_DATA, STUDENT_ID, TRAN_START_TS, 
 TRAN_STATE_DESC, TRAN_LINE_ITEM_AMOUNT)
AS 
(/* Formatted on 2006/03/31 09:36 (Formatter Plus v4.8.0) */
SELECT   d.device_name,
         tli.host_id,
         l.location_name,
         c.consumer_email_addr1,
         c.consumer_fname,
         c.consumer_lname,
         a.tran_id,
         a.tran_received_raw_acct_data,
         a.tran_parsed_acct_num,
         to_char(a.tran_start_ts,'dd-mon-yyyy hh24:mi:ss') as tran_start_ts,
         ts.tran_state_desc,
         MAX (tli.tran_line_item_amount) AS max_amount
    FROM pss.tran a,
         pss.consumer c,
         pss.consumer_acct ca,
         pss.tran_line_item tli,
         pss.tran_state ts,
         device.HOST h,
         device.device d,
         LOCATION.LOCATION l,
         pss.pos p
   WHERE c.consumer_id = 12021
     AND ca.consumer_id = c.consumer_id
     AND a.consumer_acct_id = ca.consumer_acct_id
     AND tli.tran_id = a.tran_id
     AND ts.tran_state_cd = a.tran_state_cd
     AND a.tran_start_ts BETWEEN (SYSDATE - 480 / 1440) AND SYSDATE
     AND h.host_id = tli.host_id
     AND d.device_id = h.device_id
     AND p.device_id = h.device_id
     AND l.location_id = p.location_id
GROUP BY d.device_name,
         l.location_name,
         a.tran_id,
         tli.host_id,
         a.tran_start_ts,
         ts.tran_state_desc,
         c.consumer_email_addr1,
         c.consumer_fname,
         c.consumer_lname,
         a.tran_received_raw_acct_data,
         a.tran_parsed_acct_num
);
