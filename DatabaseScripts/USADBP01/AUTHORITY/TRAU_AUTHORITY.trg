CREATE OR REPLACE TRIGGER AUTHORITY.TRAU_AUTHORITY 
AFTER UPDATE ON AUTHORITY.AUTHORITY
FOR EACH ROW
BEGIN
	IF :NEW.AUTHORITY_NAME != :OLD.AUTHORITY_NAME
		OR :NEW.AUTHORITY_SERVICE_ID != :OLD.AUTHORITY_SERVICE_ID
		OR DBADMIN.PKG_UTL.EQL(:NEW.REMOTE_SERVER_ADDR, :OLD.REMOTE_SERVER_ADDR) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.REMOTE_SERVER_ADDR_ALT, :OLD.REMOTE_SERVER_ADDR_ALT) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.REMOTE_SERVER_PORT_NUM, :OLD.REMOTE_SERVER_PORT_NUM) = 'N' THEN
		ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
			'AUTHORITY',
			'AUTHORITY.AUTHORITY',
			'U',
			:NEW.AUTHORITY_ID,
			:NEW.AUTHORITY_ID
		);
	END IF;
  IF DBADMIN.PKG_UTL.EQL(:NEW.POS_PTA_CD_EXPRESSION, :OLD.POS_PTA_CD_EXPRESSION) = 'N' THEN
      DECLARE
          CURSOR l_cur IS
              SELECT D.DEVICE_NAME, MPP.POS_PTA_NUM , M.MERCHANT_ID
                FROM DEVICE.DEVICE D
                JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
                JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
                JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                JOIN AUTHORITY.HANDLER H ON PST.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
                JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
                JOIN PSS.TERMINAL T ON PP.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID
                JOIN PSS.MERCHANT M ON M.MERCHANT_ID = T.MERCHANT_ID
                LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON MPP.MERCHANT_ID = T.MERCHANT_ID AND MPP.DEVICE_NAME = D.DEVICE_NAME
               WHERE M.AUTHORITY_ID = :NEW.AUTHORITY_ID
                 AND PST.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
                 AND PST.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
                 AND AUT.AUTHORITY_TYPE_ID = :NEW.AUTHORITY_TYPE_ID;                
          ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
      BEGIN
          FOR l_rec IN l_cur LOOP
              ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
                  'DEVICE_PTAS',
                  'AUTHORITY.AUTHORITY',
                  'U',
                  :NEW.AUTHORITY_ID,
                  l_rec.DEVICE_NAME);
              IF :NEW.POS_PTA_CD_EXPRESSION IS NOT NULL AND l_rec.POS_PTA_NUM IS NULL THEN
                  ln_pos_pta_num := PSS.PKG_POS_PTA.CREATE_POS_PTA_NUM(l_rec.MERCHANT_ID, :NEW.MIN_POS_PTA_NUM, :NEW.MAX_POS_PTA_NUM, l_rec.DEVICE_NAME);
              END IF;
          END LOOP;
      END;
  END IF;
	
END;
/
