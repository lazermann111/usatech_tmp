
CREATE OR REPLACE VIEW tracking.session_counter_excpt (
   device_call_in_record_id,
   device_id,
   machine_id,
   device_serial_cd,
   device_type,
   firmware_version,
   server_date,
   call_in_start_ts,
   call_in_finish_ts,
   network_layer,
   ip_address,
   session_count,
   calls_after_prior_batch,
   batch_session_count )
AS
SELECT "DEVICE_CALL_IN_RECORD_ID","DEVICE_ID","MACHINE_ID","DEVICE_SERIAL_CD","DEVICE_TYPE","FIRMWARE_VERSION","SERVER_DATE","CALL_IN_START_TS","CALL_IN_FINISH_TS","NETWORK_LAYER","IP_ADDRESS","SESSION_COUNT","CALLS_AFTER_PRIOR_BATCH","BATCH_SESSION_COUNT"
FROM (
    SELECT sc.device_call_in_record_id, 
    sc.device_id, 
    sc.machine_id,
    sc.device_serial_cd,
    sc.device_type, 
    sc.firmware_version,
    sc.server_date,
    sc.call_in_start_ts,
    sc.call_in_finish_ts,
    sc.network_layer,
    sc.ip_address,
    sc.session_count,
    sc.calls_after_prior_batch,
    sc.session_count - sc.calls_after_prior_batch batch_session_count
    FROM (
        SELECT cci.device_call_in_record_id,
    	cci.device_id,
    	cci.machine_id,
        cci.device_serial_cd,
    	cci.device_type,
    	cci.firmware_version,
        cci.server_date,
    	dci.call_in_start_ts,
    	dci.call_in_finish_ts,
    	dci.network_layer,
    	dci.ip_address,
    	
        (CASE cci.session_count_rolled 
            WHEN 'N' THEN (cci.session_count_curr - cci.session_count_prev)
            ELSE (cci.session_count_curr - cci.session_count_prev + 1000000)
        END) session_count,
        
        tracking.fn_calls_after_prior_batch(cci.device_call_in_record_id, cci.device_serial_cd) calls_after_prior_batch
        
    	FROM tracking.counter_call_in_rec cci
    	JOIN device_call_in_record dci
    	ON cci.device_call_in_record_id = dci.device_call_in_record_id
    	 
    ) sc
) c
WHERE c.batch_session_count != 1
/



