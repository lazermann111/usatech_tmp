ALTER TABLE tracking.counter_call_in_rec ADD
    (session_count_id_curr        NUMBER(20,0) DEFAULT 0    NOT NULL,
    session_count_curr           NUMBER(20,0) DEFAULT 0    NOT NULL,
    session_count_id_prev        NUMBER(20,0) DEFAULT 0    NOT NULL,
    session_count_prev           NUMBER(20,0) DEFAULT 0    NOT NULL,
    session_count_rolled         CHAR(1) DEFAULT 'N'    NOT NULL,
    byte_count_id_curr           NUMBER(20,0) DEFAULT 0    NOT NULL,
    byte_count_curr              NUMBER(20,0) DEFAULT 0    NOT NULL,
    byte_count_id_prev           NUMBER(20,0) DEFAULT 0    NOT NULL,
    byte_count_prev              NUMBER(20,0) DEFAULT 0    NOT NULL,
    byte_count_rolled            CHAR(1) DEFAULT 'N'    NOT NULL)
/
