/* 
Description:


This is an implementation for Auth Round Trip Monitoring from an External Device. It includes a new table, a sequence (for the table) and a package that contains a stored proc to insert data into the table. 

Created By:
J Bradley 

Date:
2005-11-11 

Grant Privileges (run as):
SYSTEM 

*/ 



-- SEQ_AUTH_MONITOR_ID - sequence for the following table
@TRACKING.SEQ_AUTH_MONITOR_ID_ddl.sql;

-- AUTH_MONITOR - new table to track the auth round trip monitoring
@TRACKING.AUTH_MONITOR_ddl.sql;

-- PKG_MONITOR - Package that contains a stored proc for inserts
@TRACKING.PKG_MONITOR_ddl.sql;



