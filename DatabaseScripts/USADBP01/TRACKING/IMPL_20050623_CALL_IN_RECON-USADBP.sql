/* 
Description:


This is a new implementation for daily call in record to transaction reconciliation and device counters to call in record reconciliation tracking. It includes new tables to store the data and a package that contains stored procedures for data generation. 

Created By:
J Bradley 

Date:
2005-06-23 

Grant Privileges (run as):
TRACKING 

*/ 



-- CALL_IN_TRANS_REC_ID sequence
@TRACKING.SEQ_CALL_IN_TRANS_REC_ID_ddl.sql;

-- call in - transaction record table
@TRACKING.CALL_IN_TRANS_REC_ddl.sql; 

-- COUNTER_CALL_IN_REC_ID sequence
@TRACKING.SEQ_COUNTER_CALL_IN_REC_ID_ddl.sql;

-- device counters - call in record table
@TRACKING.COUNTER_CALL_IN_REC_ddl.sql;

-- device counters - call in exception view
@TRACKING.COUNTER_CALL_IN_EXCPT_ddl.sql; 

-- call in - transaction scratch table (for data generation)
@TRACKING.SCR_CALL_IN_TRANS_REC_ddl.sql;

-- device counters - call in scratch table (for data generation)
@TRACKING.SCR_COUNTER_CALL_IN_REC_ddl.sql;

-- reconciliation package (contains stored procedures for data generation)
@TRACKING.PKG_RECONCILIATION_ddl.sql;


