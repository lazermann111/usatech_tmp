-- Start of DDL Script for Table TRACKING.HTTP_CHECK_TYPE

CREATE TABLE tracking.http_check_type
    (http_check_type_id             NUMBER(20,0) NOT NULL,
    http_check_type_desc           VARCHAR2(60) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  tracking_data
/


-- Grants for Table
GRANT SELECT ON tracking.http_check_type TO web_user
/
GRANT SELECT ON tracking.http_check_type TO net_user
/


-- Indexes for TRACKING.HTTP_CHECK_TYPE

CREATE INDEX tracking.inx_http_check_type_1 ON tracking.http_check_type
  (
    http_check_type_desc                ASC,
    http_check_type_id                  ASC
  )
  TABLESPACE  tracking_indx
/

CREATE INDEX tracking.inx_http_check_type_2 ON tracking.http_check_type
  (
    http_check_type_id                  ASC,
    http_check_type_desc                ASC
  )
  TABLESPACE  tracking_indx
/



-- Constraints for TRACKING.HTTP_CHECK_TYPE

ALTER TABLE tracking.http_check_type
ADD CONSTRAINT pk_http_check_type PRIMARY KEY (http_check_type_id)
/


-- Triggers for TRACKING.HTTP_CHECK_TYPE

CREATE OR REPLACE TRIGGER tracking.trbi_http_check_type
 BEFORE
  INSERT
 ON tracking.http_check_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.http_check_type_id IS NULL THEN

      SELECT seq_http_check_type_id.NEXTVAL
        INTO :new.http_check_type_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER tracking.trbu_http_check_type
 BEFORE
  UPDATE
 ON tracking.http_check_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

-- End of DDL Script for Table TRACKING.HTTP_CHECK_TYPE

