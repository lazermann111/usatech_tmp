-- Start of DDL Script for Table TRACKING.SCR_CALL_IN_TRANS_REC
-- Generated 22-Jun-2005 15:07:20 from TRACKING@USADBD03

CREATE TABLE tracking.scr_call_in_trans_rec
    (device_call_in_record_id       NUMBER(20,0) NOT NULL,
    device_id                      NUMBER(20,0) NOT NULL,
    call_in_credit_count           NUMBER(7,0),
    trans_credit_count             NUMBER(7,0),
    call_in_credit_amount          NUMBER(7,2),
    trans_credit_amount            NUMBER(7,2),
    call_in_cash_count             NUMBER(7,0),
    trans_cash_count               NUMBER(7,0),
    call_in_cash_amount            NUMBER(7,2),
    trans_cash_amount              NUMBER(7,2),
    call_in_pass_count             NUMBER(7,0),
    trans_pass_count               NUMBER(7,0),
    call_in_pass_amount            NUMBER(7,2),
    trans_pass_amount              NUMBER(7,2))
  TABLESPACE  tracking_data
/




-- End of DDL Script for Table TRACKING.SCR_CALL_IN_TRANS_REC

