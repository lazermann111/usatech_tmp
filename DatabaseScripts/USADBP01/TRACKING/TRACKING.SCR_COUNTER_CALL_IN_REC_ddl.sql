-- Start of DDL Script for Table TRACKING.SCR_COUNTER_CALL_IN_REC
-- Generated 22-Jun-2005 15:07:29 from TRACKING@USADBD03

CREATE TABLE tracking.scr_counter_call_in_rec
    (device_call_in_record_id       NUMBER(20,0) NOT NULL,
    device_id                      NUMBER(20,0) NOT NULL,
    trans_cred_vend_count          NUMBER(20,0),
    cred_vend_count_id_curr        NUMBER(20,0),
    cred_vend_count_curr           NUMBER(20,0),
    cred_vend_count_id_prev        NUMBER(20,0),
    cred_vend_count_prev           NUMBER(20,0),
    cred_vend_count_rolled         CHAR(1),
    call_in_credit_amount          NUMBER(20,2),
    cred_amt_count_id_curr         NUMBER(20,0),
    cred_amt_count_curr            NUMBER(20,2),
    cred_amt_count_id_prev         NUMBER(20,0),
    cred_amt_count_prev            NUMBER(20,2),
    cred_amt_count_rolled          CHAR(1),
    trans_cash_vend_count          NUMBER(20,0),
    cash_vend_count_id_curr        NUMBER(20,0),
    cash_vend_count_curr           NUMBER(20,0),
    cash_vend_count_id_prev        NUMBER(20,0),
    cash_vend_count_prev           NUMBER(20,0),
    cash_vend_count_rolled         CHAR(1),
    call_in_cash_amount            NUMBER(20,2),
    cash_amt_count_id_curr         NUMBER(20,0),
    cash_amt_count_curr            NUMBER(20,2),
    cash_amt_count_id_prev         NUMBER(20,0),
    cash_amt_count_prev            NUMBER(20,2),
    cash_amt_count_rolled          CHAR(1),
    trans_pass_vend_count          NUMBER(20,0),
    pass_vend_count_id_curr        NUMBER(20,0),
    pass_vend_count_curr           NUMBER(20,0),
    pass_vend_count_id_prev        NUMBER(20,0),
    pass_vend_count_prev           NUMBER(20,0),
    pass_vend_count_rolled         CHAR(1),
    call_in_pass_amount            NUMBER(20,2),
    pass_amt_count_id_curr         NUMBER(20,0),
    pass_amt_count_curr            NUMBER(20,2),
    pass_amt_count_id_prev         NUMBER(20,0),
    pass_amt_count_prev            NUMBER(20,2),
    pass_amt_count_rolled          CHAR(1))
  TABLESPACE  tracking_data
/




-- End of DDL Script for Table TRACKING.SCR_COUNTER_CALL_IN_REC

