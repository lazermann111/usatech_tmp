/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will implement session and byte counters for the reconciliaton. It includes an update to the data tables, an update to stored procedures (in a package) and a new view for exception reporting. 

Created By:
J Bradley 

Date:
2005-09-15 

Grant Privileges (run as):
TRACKING 

*/ 



-- SCR_COUNTER_CALL_IN_REC - alter the table to include fields for session counts and byte counts
@TRACKING.SCR_COUNTER_CALL_IN_REC_alter_ddl.sql;

-- COUNTER_CALL_IN_REC - alter the table to include fields for session counts and byte counts
@TRACKING.COUNTER_CALL_IN_REC_alter_ddl.sql;

-- reconciliation package - update the stored procs that generate the data for the above tables
@TRACKING.PKG_RECONCILIATION_ddl.sql;

-- session counter - exception view
@TRACKING.SESSION_COUNTER_EXCPT_ddl.sql; 


