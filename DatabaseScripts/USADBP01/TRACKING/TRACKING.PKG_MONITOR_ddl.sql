-- Start of DDL Script for Package TRACKING.PKG_MONITOR

CREATE OR REPLACE 
PACKAGE tracking.pkg_monitor IS
   PROCEDURE sp_add_auth_monitor (
      ps_ev_number IN auth_monitor.device_name%TYPE,
      pc_auth_status IN auth_monitor.auth_status%TYPE,
      pn_response_code IN auth_monitor.response_code%TYPE,
      ps_response_msg IN auth_monitor.response_msg%TYPE,
      pd_session_start_ts IN auth_monitor.session_start_ts%TYPE,
      pd_session_finish_ts IN auth_monitor.session_finish_ts%TYPE,
      pn_session_time_ms IN auth_monitor.session_time_ms%TYPE,
      pn_auth_time_ms IN auth_monitor.auth_time_ms%TYPE
   );
END;
/


CREATE OR REPLACE 
PACKAGE BODY          tracking.pkg_monitor IS

   PROCEDURE sp_add_auth_monitor (
      ps_ev_number IN auth_monitor.device_name%TYPE,
      pc_auth_status IN auth_monitor.auth_status%TYPE,
      pn_response_code IN auth_monitor.response_code%TYPE,
      ps_response_msg IN auth_monitor.response_msg%TYPE,
      pd_session_start_ts IN auth_monitor.session_start_ts%TYPE,
      pd_session_finish_ts IN auth_monitor.session_finish_ts%TYPE,
      pn_session_time_ms IN auth_monitor.session_time_ms%TYPE,
      pn_auth_time_ms IN auth_monitor.auth_time_ms%TYPE
   ) AS
   BEGIN
		
		-- insert the record
		
INSERT INTO tracking.auth_monitor (device_name, auth_status, response_code, response_msg, session_start_ts, session_finish_ts, session_time_ms, auth_time_ms )
VALUES (ps_ev_number, pc_auth_status, pn_response_code, ps_response_msg, pd_session_start_ts, pd_session_finish_ts, pn_session_time_ms, pn_auth_time_ms	);
		
   --EXCEPTION
	
   END;
   
END;
/


-- Grants for Package
GRANT EXECUTE ON tracking.pkg_monitor TO web_user
/

-- End of DDL Script for Package TRACKING.PKG_MONITOR

