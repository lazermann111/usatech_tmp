/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will update the session counter tracking to add network layer and ip address information. It includes an update to the view for exception reporting. 

Created By:
J Bradley 

Date:
2005-09-19 

Grant Privileges (run as):
TRACKING 

*/ 



-- session counter - exception view update 
@TRACKING.SESSION_COUNTER_EXCPT_ddl.sql; 


