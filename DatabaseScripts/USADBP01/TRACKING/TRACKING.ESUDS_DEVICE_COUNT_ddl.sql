
grant select on location.location to tracking;
grant select on pss.pos to tracking;
grant select on device.host to tracking;
grant select on device.host_type to tracking;
grant select on device.host_type_host_group_type to tracking;
grant select on device.host_group_type to tracking;
grant select on location.vw_location_hierarchy to tracking;

-- Start of DDL Script for View TRACKING.ESUDS_DEVICE_COUNT

CREATE OR REPLACE VIEW tracking.vw_esuds_device_count (
   device_type_desc,
   device_count )
AS
SELECT
(CASE
    WHEN b.ep_type = 'RC' THEN 'Room Controller'
    ELSE 'Washer/Dryer'
END) device_type_desc,
SUM(b.host_count) device_count
FROM location.vw_location_hierarchy vlh
JOIN location.location school
ON vlh.ancestor_location_id = school.location_id
JOIN (
    SELECT
    a.ep_type,
    a.location_id,
    a.pos_activation_ts,
    LAG(a.pos_activation_ts, 1, ADD_MONTHS(SYSDATE, 1)) OVER (PARTITION BY a.device_name, a.ep_type ORDER BY a.pos_activation_ts DESC) pos_deactivation_ts,
    a.host_count
    FROM (
        SELECT 
        (CASE WHEN hogt.host_group_type_cd = 'BASE_HOST' THEN 'RC' ELSE 'WD' END) ep_type,
        pos.location_id,
        dev.device_name,
        pos.pos_activation_ts,
        COUNT(1) host_count
        FROM pss.pos pos
        JOIN device.device dev
        ON pos.device_id = dev.device_id
        AND dev.device_type_id = 5
        JOIN device.host hos
        ON hos.device_id = dev.device_id
        JOIN device.host_type ht
        ON ht.host_type_id = hos.host_type_id
        JOIN device.host_type_host_group_type htgt
        ON htgt.host_type_id = hos.host_type_id
        JOIN device.host_group_type hogt
        ON hogt.host_group_type_id = htgt.host_group_type_id
        GROUP BY (CASE WHEN hogt.host_group_type_cd = 'BASE_HOST' THEN 'RC' ELSE 'WD' END), pos.location_id, dev.device_name, pos.pos_activation_ts
    ) a
    ORDER BY a.device_name, a.ep_type, a.pos_activation_ts
) b
ON vlh.descendent_location_id = b.location_id
AND vlh.depth = 3
AND b.pos_deactivation_ts > SYSDATE
AND school.location_name != 'USA Technologies School'
GROUP BY b.ep_type
/



-- End of DDL Script for View TRACKING.ESUDS_DEVICE_COUNT

