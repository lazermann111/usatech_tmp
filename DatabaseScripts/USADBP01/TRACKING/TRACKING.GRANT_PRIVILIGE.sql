GRANT SELECT ON device.device
TO TRACKING;

GRANT SELECT ON device.device_type
TO TRACKING;

GRANT SELECT ON device.device_setting
TO TRACKING;

GRANT SELECT ON device.device_call_in_record
TO TRACKING;

GRANT SELECT ON device.device_counter
TO TRACKING;

GRANT SELECT ON device.device_counter
TO TRACKING;



GRANT SELECT ON tazdba.transaction_record_hist
TO TRACKING;

GRANT SELECT ON tazdba.transaction_failures
TO TRACKING;

GRANT SELECT ON tazdba.cash_transaction_record
TO TRACKING;
