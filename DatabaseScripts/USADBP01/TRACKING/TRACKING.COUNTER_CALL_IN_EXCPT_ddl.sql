-- Start of DDL Script for View TRACKING.COUNTER_CALL_IN_EXCPT
-- Generated 20-Jul-2005 9:43:43 from TRACKING@USADBD03

CREATE OR REPLACE VIEW tracking.counter_call_in_excpt (
   device_call_in_record_id,
   device_id,
   machine_id,
   device_serial_cd,
   device_type,
   firmware_version,
   server_date,
   call_in_start_ts,
   call_in_finish_ts,
   trans_cred_vend_count,
   counter_cred_vend_count,
   call_in_credit_amount,
   counter_credit_amount,
   trans_cash_vend_count,
   counter_cash_vend_count,
   call_in_cash_amount,
   counter_cash_amount,
   trans_pass_vend_count,
   counter_pass_vend_count,
   call_in_pass_amount,
   counter_pass_amount )
AS
SELECT "DEVICE_CALL_IN_RECORD_ID","DEVICE_ID","MACHINE_ID","DEVICE_SERIAL_CD","DEVICE_TYPE","FIRMWARE_VERSION","SERVER_DATE","CALL_IN_START_TS","CALL_IN_FINISH_TS","TRANS_CRED_VEND_COUNT","COUNTER_CRED_VEND_COUNT","CALL_IN_CREDIT_AMOUNT","COUNTER_CREDIT_AMOUNT","TRANS_CASH_VEND_COUNT","COUNTER_CASH_VEND_COUNT","CALL_IN_CASH_AMOUNT","COUNTER_CASH_AMOUNT","TRANS_PASS_VEND_COUNT","COUNTER_PASS_VEND_COUNT","CALL_IN_PASS_AMOUNT","COUNTER_PASS_AMOUNT"
FROM (
    SELECT cci.device_call_in_record_id, 
	cci.device_id, 
	cci.machine_id,
    cci.device_serial_cd, 
	cci.device_type, 
	cci.firmware_version,
    cci.server_date,
	dci.call_in_start_ts,
	dci.call_in_finish_ts, 
	
	cci.trans_cred_vend_count,
    (CASE cci.cred_vend_count_rolled 
        WHEN 'N' THEN (cci.cred_vend_count_curr - cci.cred_vend_count_prev)
        ELSE (cci.cred_vend_count_curr - cci.cred_vend_count_prev + 1000000)
    END) counter_cred_vend_count,
     
	cci.call_in_credit_amount,
    (CASE cci.cred_amt_count_rolled 
        WHEN 'N' THEN (cci.cred_amt_count_curr - cci.cred_amt_count_prev) / 100
        ELSE (cci.cred_amt_count_curr - cci.cred_amt_count_prev + 1000000) / 100
    END) counter_credit_amount, 
    
	cci.trans_cash_vend_count,
    (CASE cci.cash_vend_count_rolled 
        WHEN 'N' THEN (cci.cash_vend_count_curr - cci.cash_vend_count_prev)
        ELSE (cci.cash_vend_count_curr - cci.cash_vend_count_prev + 1000000)
    END) counter_cash_vend_count, 
    
	cci.call_in_cash_amount,
    (CASE cci.cash_amt_count_rolled 
        WHEN 'N' THEN (cci.cash_amt_count_curr - cci.cash_amt_count_prev) / 100
        ELSE (cci.cash_amt_count_curr - cci.cash_amt_count_prev + 1000000) / 100
    END) counter_cash_amount, 
    
	cci.trans_pass_vend_count,
    (CASE cci.pass_vend_count_rolled 
        WHEN 'N' THEN (cci.pass_vend_count_curr - cci.pass_vend_count_prev)
        ELSE (cci.pass_vend_count_curr - cci.pass_vend_count_prev + 1000000)
    END) counter_pass_vend_count, 
    
	cci.call_in_pass_amount,
    (CASE cci.pass_amt_count_rolled 
        WHEN 'N' THEN (cci.pass_amt_count_curr - cci.pass_amt_count_prev) / 100
        ELSE (cci.pass_amt_count_curr - cci.pass_amt_count_prev + 1000000) / 100
    END) counter_pass_amount
	FROM tracking.counter_call_in_rec cci
	JOIN device_call_in_record dci
	ON cci.device_call_in_record_id = dci.device_call_in_record_id
) cc
WHERE cc.trans_cred_vend_count != cc.counter_cred_vend_count
OR cc.call_in_credit_amount != cc.counter_credit_amount
OR cc.trans_cash_vend_count != cc.counter_cash_vend_count
OR cc.call_in_cash_amount != cc.counter_cash_amount
OR cc.trans_pass_vend_count != cc.counter_pass_vend_count
OR cc.call_in_pass_amount != cc.counter_pass_amount
/


-- End of DDL Script for View TRACKING.COUNTER_CALL_IN_EXCPT
