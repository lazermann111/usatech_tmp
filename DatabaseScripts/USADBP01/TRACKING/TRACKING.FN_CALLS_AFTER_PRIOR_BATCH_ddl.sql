-- Start of DDL Script for Function TRACKING.FN_CALLS_AFTER_PRIOR_BATCH
-- Generated 19-Sep-2005 10:35:30 from TRACKING@USADBD03

CREATE OR REPLACE 
FUNCTION tracking.fn_calls_after_prior_batch (inCallInRecordId IN NUMBER, 
                                             inSerialNumber IN VARCHAR2)
RETURN NUMBER AS
    tCount NUMBER;
BEGIN
	
	SELECT COUNT(1) INTO tCount
    FROM device_call_in_record dc
    WHERE dc.device_call_in_record_id < inCallInRecordId
    AND dc.device_call_in_record_id > (
        SELECT MAX(device_call_in_record_id)
        FROM device_call_in_record
        WHERE serial_number = dc.serial_number
        AND device_call_in_record_id < inCallInRecordId
        AND call_in_type = 'B'
    )
    AND dc.serial_number = inSerialNumber;
    
    RETURN tCount;
  
END;
/



-- End of DDL Script for Function TRACKING.FN_CALLS_AFTER_PRIOR_BATCH

