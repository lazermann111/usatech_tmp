-- Start of DDL Script for Table TRACKING.AUTH_MONITOR

CREATE TABLE tracking.auth_monitor
    (auth_monitor_id                NUMBER(20,0) NOT NULL,
    device_name                    VARCHAR2(60) NOT NULL,
    auth_status                    CHAR(1) DEFAULT 'U'  NOT NULL,
    response_code                  NUMBER(20,0) NOT NULL,
    response_msg                   VARCHAR2(100) NOT NULL,
    session_start_ts               DATE NOT NULL,
    session_finish_ts              DATE,
    session_time_ms                NUMBER(20,0) NOT NULL,
    auth_time_ms                   NUMBER(20,0) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  tracking_data
/



-- Grants for Table
GRANT SELECT, UPDATE, INSERT, DELETE ON tracking.auth_monitor TO web_user
/



-- Indexes for TRACKING.AUTH_MONITOR

CREATE INDEX tracking.ix_auth_monitor_1 ON tracking.auth_monitor
  (
    device_name                     ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- Constraints for TRACKING.AUTH_MONITOR

ALTER TABLE tracking.auth_monitor
ADD CONSTRAINT pk_auth_monitor_id PRIMARY KEY (auth_monitor_id)
USING INDEX
  TABLESPACE  tracking_data
/


-- Triggers for TRACKING.AUTH_MONITOR

CREATE OR REPLACE TRIGGER tracking.trbu_auth_monitor
 BEFORE
  UPDATE
 ON tracking.auth_monitor
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER tracking.trbi_auth_monitor
 BEFORE
  INSERT
 ON tracking.auth_monitor
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.auth_monitor_id IS NULL THEN

      SELECT seq_auth_monitor_id.NEXTVAL
        INTO :new.auth_monitor_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table TRACKING.AUTH_MONITOR

