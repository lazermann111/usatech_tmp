-- Start of DDL Script for Table TRACKING.COUNTER_CALL_IN_REC
-- Generated 22-Jun-2005 15:07:12 from TRACKING@USADBD03

CREATE TABLE tracking.counter_call_in_rec
    (counter_call_in_rec_id         NUMBER(20,0) NOT NULL,
    device_call_in_record_id       NUMBER(20,0) NOT NULL,
    device_id                      NUMBER(20,0) NOT NULL,
    machine_id                     VARCHAR2(60) NOT NULL,
    device_serial_cd               VARCHAR2(20) NOT NULL,
    device_type                    VARCHAR2(60) NOT NULL,
    firmware_version               VARCHAR2(60) DEFAULT 'Unknown'  NOT NULL,
    server_date                    DATE NOT NULL,
    trans_cred_vend_count          NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_vend_count_id_curr        NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_vend_count_curr           NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_vend_count_id_prev        NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_vend_count_prev           NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_vend_count_rolled         CHAR(1) DEFAULT 'N'   NOT NULL,
    call_in_credit_amount          NUMBER(20,2) DEFAULT 0   NOT NULL,
    cred_amt_count_id_curr         NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_amt_count_curr            NUMBER(20,2) DEFAULT 0   NOT NULL,
    cred_amt_count_id_prev         NUMBER(20,0) DEFAULT 0   NOT NULL,
    cred_amt_count_prev            NUMBER(20,2) DEFAULT 0   NOT NULL,
    cred_amt_count_rolled          CHAR(1) DEFAULT 'N'   NOT NULL,
    trans_cash_vend_count          NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_vend_count_id_curr        NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_vend_count_curr           NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_vend_count_id_prev        NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_vend_count_prev           NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_vend_count_rolled         CHAR(1) DEFAULT 'N'  NOT NULL,
    call_in_cash_amount            NUMBER(20,2) DEFAULT 0   NOT NULL,
    cash_amt_count_id_curr         NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_amt_count_curr            NUMBER(20,2) DEFAULT 0   NOT NULL,
    cash_amt_count_id_prev         NUMBER(20,0) DEFAULT 0   NOT NULL,
    cash_amt_count_prev            NUMBER(20,2) DEFAULT 0   NOT NULL,
    cash_amt_count_rolled          CHAR(1) DEFAULT 'N'   NOT NULL,
    trans_pass_vend_count          NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_vend_count_id_curr        NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_vend_count_curr           NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_vend_count_id_prev        NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_vend_count_prev           NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_vend_count_rolled         CHAR(1) DEFAULT 'N'   NOT NULL,
    call_in_pass_amount            NUMBER(20,2) DEFAULT 0   NOT NULL,
    pass_amt_count_id_curr         NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_amt_count_curr            NUMBER(20,2) DEFAULT 0   NOT NULL,
    pass_amt_count_id_prev         NUMBER(20,0) DEFAULT 0   NOT NULL,
    pass_amt_count_prev            NUMBER(20,2) DEFAULT 0   NOT NULL,
    pass_amt_count_rolled          CHAR(1) DEFAULT 'N'   NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    version_code                   VARCHAR2(10) DEFAULT '1'

  NOT NULL)
  TABLESPACE  tracking_data
/




-- Indexes for TRACKING.COUNTER_CALL_IN_REC

CREATE INDEX tracking.ix_counter_call_in_rec_1 ON tracking.counter_call_in_rec
  (
    device_call_in_record_id        ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX tracking.ix_counter_call_in_rec_2 ON tracking.counter_call_in_rec
  (
    device_id                       ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX tracking.ix_counter_call_in_rec_3 ON tracking.counter_call_in_rec
  (
    machine_id                      ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX tracking.ix_counter_call_in_rec_4 ON tracking.counter_call_in_rec
  (
    device_serial_cd                ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- Constraints for TRACKING.COUNTER_CALL_IN_REC

ALTER TABLE tracking.counter_call_in_rec
ADD CONSTRAINT pk_counter_call_in_rec_id PRIMARY KEY (counter_call_in_rec_id)
USING INDEX
  TABLESPACE  tracking_data
/


-- Triggers for TRACKING.COUNTER_CALL_IN_REC

CREATE OR REPLACE TRIGGER tracking.trbu_counter_call_in_rec
 BEFORE
  UPDATE
 ON tracking.counter_call_in_rec
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER tracking.trbi_counter_call_in_rec
 BEFORE
  INSERT
 ON tracking.counter_call_in_rec
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.counter_call_in_rec_id IS NULL THEN

      SELECT seq_counter_call_in_rec_id.NEXTVAL
        INTO :new.counter_call_in_rec_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table TRACKING.COUNTER_CALL_IN_REC

