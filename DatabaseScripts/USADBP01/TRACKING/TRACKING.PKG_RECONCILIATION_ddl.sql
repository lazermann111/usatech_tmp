-- Start of DDL Script for Package TRACKING.PKG_RECONCILIATION
-- Generated 2/9/2006 4:13:32 PM from TRACKING@USADBP

CREATE OR REPLACE 
PACKAGE tracking.pkg_reconciliation IS
   PROCEDURE sp_gen_call_in_trans (
      inDate IN DATE
   );
   PROCEDURE sp_gen_counter_call_in (
      inDate IN DATE
   );
   PROCEDURE sp_gen_call_in_trans_excpt (
      inDate IN DATE
   );
   PROCEDURE sp_gen_counter_call_in_excpt (
      inDate IN DATE
   );
   PROCEDURE sp_call_in_trans_credit (
      inDate IN DATE
   );
   PROCEDURE sp_call_in_trans_cash (
      inDate IN DATE
   );
   PROCEDURE sp_call_in_trans_pass (
      inDate IN DATE
   );
   PROCEDURE sp_counter_call_in_tmp (
      inDateOffset IN NUMBER
   );
END;
/


CREATE OR REPLACE 
PACKAGE BODY          tracking.pkg_reconciliation IS

   PROCEDURE sp_gen_call_in_trans (inDate IN DATE) AS
      tDate DATE;
   BEGIN
		-- ensure tDate is truncated to a day (i.e. time = 00:00:00)
		tDate := TRUNC(inDate);
		
		-- truncate the scratch table
		EXECUTE IMMEDIATE 'TRUNCATE TABLE tracking.scr_call_in_trans_rec';
		
		-- generate the temp data	  
		sp_call_in_trans_credit(tDate);	  
		sp_call_in_trans_cash(tDate);	  
		sp_call_in_trans_pass(tDate);
		
		-- roll up the data from the scratch table and populate the perm table
		
		INSERT INTO tracking.call_in_trans_rec (
			device_call_in_record_id,
			device_id,
			machine_id,
			device_serial_cd,
			device_type,
			firmware_version,
			server_date,    
			call_in_credit_count,
			trans_credit_count,
			call_in_credit_amount,
			trans_credit_amount,
			call_in_cash_count,
			trans_cash_count,
			call_in_cash_amount,
			trans_cash_amount,
			call_in_pass_count,
			trans_pass_count,
			call_in_pass_amount,
			trans_pass_amount
		)
		SELECT sctr.device_call_in_record_id,
        sctr.device_id,
        dev.device_name,
        dev.device_serial_cd,
        dt.device_type_desc,
        NVL(ds.device_setting_value, 'Unknown') firmware_version,
        (tDate - 1) server_date,
        sctr.call_in_credit_count,
        sctr.trans_credit_count,
        sctr.call_in_credit_amount,
        sctr.trans_credit_amount,
        sctr.call_in_cash_count,
        sctr.trans_cash_count,
        sctr.call_in_cash_amount,
        sctr.trans_cash_amount,
        sctr.call_in_pass_count,
        sctr.trans_pass_count,
        sctr.call_in_pass_amount,
        sctr.trans_pass_amount
        FROM (
            SELECT citr.device_call_in_record_id,
        	citr.device_id,
        	SUM(citr.call_in_credit_count) call_in_credit_count,
        	SUM(citr.trans_credit_count) trans_credit_count,
        	SUM(citr.call_in_credit_amount) call_in_credit_amount,
        	SUM(citr.trans_credit_amount) trans_credit_amount,
        	SUM(citr.call_in_cash_count) call_in_cash_count,
        	SUM(citr.trans_cash_count) trans_cash_count,
        	SUM(citr.call_in_cash_amount) call_in_cash_amount,
        	SUM(citr.trans_cash_amount) trans_cash_amount,
        	SUM(citr.call_in_pass_count) call_in_pass_count,
        	SUM(citr.trans_pass_count) trans_pass_count,
        	SUM(citr.call_in_pass_amount) call_in_pass_amount,
        	SUM(citr.trans_pass_amount) trans_pass_amount
        	FROM tracking.scr_call_in_trans_rec citr
        	GROUP BY citr.device_call_in_record_id, citr.device_id
        ) sctr
        JOIN device.device dev
        ON sctr.device_id = dev.device_id
        JOIN device.device_type dt
        ON dt.device_type_id = dev.device_type_id
        LEFT OUTER JOIN device.device_setting ds
        ON dev.device_id = ds.device_id
        AND ds.device_setting_parameter_cd = 'Firmware Version';
		
		
		-- truncate the scratch table (keep it empty)
		EXECUTE IMMEDIATE 'TRUNCATE TABLE tracking.scr_call_in_trans_rec';
		
   --EXCEPTION
	
   END;


   PROCEDURE sp_gen_counter_call_in (inDate IN DATE) AS
      tDate DATE;
      tDateOffset NUMBER;
   BEGIN
		-- ensure tDate is truncated to a day (i.e. time = 00:00:00)
		tDate := TRUNC(inDate);
		
		tDateOffset := TRUNC(SYSDATE) - tDate;
		
		-- truncate the scratch table
		EXECUTE IMMEDIATE 'TRUNCATE TABLE tracking.scr_counter_call_in_rec';
		
		-- generate the temp data
		
		sp_counter_call_in_tmp(tDateOffset);
		
		-- roll up the data from the scratch table and populate the perm table
		
		INSERT INTO tracking.counter_call_in_rec (
			device_call_in_record_id,
		    device_id,
		    machine_id,
		    device_serial_cd,
		    device_type,
		    firmware_version,
		    server_date,
		    
		    -- credit vend count    
		    trans_cred_vend_count,
		    cred_vend_count_id_curr,
		    cred_vend_count_curr,
		    cred_vend_count_id_prev,
		    cred_vend_count_prev,
		    cred_vend_count_rolled,
		    -- credit amount
		    call_in_credit_amount,
		    cred_amt_count_id_curr,
		    cred_amt_count_curr,
		    cred_amt_count_id_prev,
		    cred_amt_count_prev,
		    cred_amt_count_rolled,
		    -- cash vend count
		    trans_cash_vend_count,
		    cash_vend_count_id_curr,
		    cash_vend_count_curr,
		    cash_vend_count_id_prev,
		    cash_vend_count_prev,
		    cash_vend_count_rolled,
		    -- cash amount
		    call_in_cash_amount,
		    cash_amt_count_id_curr,
		    cash_amt_count_curr,
		    cash_amt_count_id_prev,
		    cash_amt_count_prev,
		    cash_amt_count_rolled,
		    -- pass vend count
		    trans_pass_vend_count,
		    pass_vend_count_id_curr,
		    pass_vend_count_curr,
		    pass_vend_count_id_prev,
		    pass_vend_count_prev,
		    pass_vend_count_rolled,
		    -- pass amount
		    call_in_pass_amount,
		    pass_amt_count_id_curr,
		    pass_amt_count_curr,
		    pass_amt_count_id_prev,
		    pass_amt_count_prev,
		    pass_amt_count_rolled,
		    -- session count
    		session_count_id_curr,
    		session_count_curr,
    		session_count_id_prev,
    		session_count_prev,
    		session_count_rolled,
    		-- byte count
    		byte_count_id_curr,
    		byte_count_curr,
    		byte_count_id_prev,
    		byte_count_prev,
    		byte_count_rolled
		)
		SELECT scci.device_call_in_record_id,
		scci.device_id,
		
		dev.device_name,
		dev.device_serial_cd,
		dt.device_type_desc,
		NVL(ds.device_setting_value, 'Unknown') firmware_version,
		(tDate - 1) server_date,
		
		scci.trans_cred_vend_count,
		scci.cred_vend_count_id_curr,
		scci.cred_vend_count_curr,
		scci.cred_vend_count_id_prev,
		scci.cred_vend_count_prev,
		scci.cred_vend_count_rolled,
		
		scci.call_in_credit_amount,
		scci.cred_amt_count_id_curr,
		scci.cred_amt_count_curr,
		scci.cred_amt_count_id_prev,
		scci.cred_amt_count_prev,
		scci.cred_amt_count_rolled,
		
		scci.trans_cash_vend_count,
		scci.cash_vend_count_id_curr,
		scci.cash_vend_count_curr,
		scci.cash_vend_count_id_prev,
		scci.cash_vend_count_prev,
		scci.cash_vend_count_rolled,
		
		scci.call_in_cash_amount,
		scci.cash_amt_count_id_curr,
		scci.cash_amt_count_curr,
		scci.cash_amt_count_id_prev,
		scci.cash_amt_count_prev,
		scci.cash_amt_count_rolled,
		
		scci.trans_pass_vend_count,
		scci.pass_vend_count_id_curr,
		scci.pass_vend_count_curr,
		scci.pass_vend_count_id_prev,
		scci.pass_vend_count_prev,
		scci.pass_vend_count_rolled,
		
		scci.call_in_pass_amount,
		scci.pass_amt_count_id_curr,
		scci.pass_amt_count_curr,
		scci.pass_amt_count_id_prev,
		scci.pass_amt_count_prev,
		scci.pass_amt_count_rolled,
		
		scci.session_count_id_curr,
		scci.session_count_curr,
		scci.session_count_id_prev,
		scci.session_count_prev,
		scci.session_count_rolled,

		scci.byte_count_id_curr,
		scci.byte_count_curr,
		scci.byte_count_id_prev,
		scci.byte_count_prev,
		scci.byte_count_rolled
		FROM tracking.scr_counter_call_in_rec scci
        JOIN device.device dev
        ON scci.device_id = dev.device_id
        JOIN device.device_type dt
        ON dt.device_type_id = dev.device_type_id
        LEFT OUTER JOIN device.device_setting ds
        ON dev.device_id = ds.device_id
        AND ds.device_setting_parameter_cd = 'Firmware Version';	
		
		-- truncate the scratch table (keep it empty)
		EXECUTE IMMEDIATE 'TRUNCATE TABLE tracking.scr_counter_call_in_rec';

   --EXCEPTION
      
   END;

   
   PROCEDURE sp_gen_call_in_trans_excpt (inDate IN DATE) AS
      tDate DATE;
   BEGIN
		-- ensure tDate is truncated to a day (i.e. time = 00:00:00)
		tDate := TRUNC(inDate);
		
		/* This code is currently being run on USADBT01.
		  Once USARDB01 is in place this code should be run from here
		
		-- migrate the exception data to reporting
		INSERT INTO report.call_in_trans_except@reptp_report (
    		device_call_in_record_id,
            device_id,
            machine_id,
            device_serial_cd,
            device_type,
            firmware_version,
            server_date,
            call_in_start_ts,
            call_in_finish_ts,
            call_in_credit_count,
            trans_credit_count,
            call_in_credit_amount,
            trans_credit_amount,
            call_in_cash_count,
            trans_cash_count,
            call_in_cash_amount,
            trans_cash_amount,
            call_in_pass_count,
            trans_pass_count,
            call_in_pass_amount,
            trans_pass_amount
		)
		SELECT citr.device_call_in_record_id,
        citr.device_id,
        citr.machine_id,
        citr.device_serial_cd,
        citr.device_type,
        citr.firmware_version,
        citr.server_date,
        dci.call_in_start_ts,
        dci.call_in_finish_ts,
        citr.call_in_credit_count,
        citr.trans_credit_count,
        citr.call_in_credit_amount,
        citr.trans_credit_amount,
        citr.call_in_cash_count,
        citr.trans_cash_count,
        citr.call_in_cash_amount,
        citr.trans_cash_amount,
        citr.call_in_pass_count,
        citr.trans_pass_count,
        citr.call_in_pass_amount,
        citr.trans_pass_amount
        FROM tracking.call_in_trans_rec citr
        JOIN device.device_call_in_record dci
        ON citr.device_call_in_record_id = dci.device_call_in_record_id
        WHERE citr.server_date = (tDate - 1)
        AND (citr.call_in_credit_count != citr.trans_credit_count
        	OR citr.call_in_credit_amount != citr.trans_credit_amount
        	OR citr.call_in_cash_count != citr.trans_cash_count
        	OR citr.call_in_cash_amount != citr.trans_cash_amount
        	OR citr.call_in_pass_count != citr.trans_pass_count
        	OR citr.call_in_pass_amount != citr.trans_pass_amount);			
		
      */
   --EXCEPTION
      
   END;


   PROCEDURE sp_gen_counter_call_in_excpt (inDate IN DATE) AS
      tDate DATE;
   BEGIN
		-- ensure tDate is truncated to a day (i.e. time = 00:00:00)
		tDate := TRUNC(inDate);
		
		/* This code is currently being run on USADBT01.
		  Once USARDB01 is in place this code should be run from here
		
          
		INSERT INTO report.counter_call_in_except@reptp_report (
            device_call_in_record_id,
            device_id, machine_id,
            device_serial_cd,
            device_type,
            firmware_version,
            server_date,
            call_in_start_ts,
            call_in_finish_ts,
            trans_cred_vend_count,
            counter_cred_vend_count,
            call_in_credit_amount,
            counter_credit_amount,
            trans_cash_vend_count,
            counter_cash_vend_count,
            call_in_cash_amount,
            counter_cash_amount,
            trans_pass_vend_count,
            counter_pass_vend_count,
            call_in_pass_amount,
            counter_pass_amount
		)
		SELECT cc.device_call_in_record_id,
        cc.device_id, machine_id,
        cc.device_serial_cd,
        cc.device_type,
        cc.firmware_version,
        cc.server_date,
        cc.call_in_start_ts,
        cc.call_in_finish_ts,
        cc.trans_cred_vend_count,
        cc.counter_cred_vend_count,
        cc.call_in_credit_amount,
        cc.counter_credit_amount,
        cc.trans_cash_vend_count,
        cc.counter_cash_vend_count,
        cc.call_in_cash_amount,
        cc.counter_cash_amount,
        cc.trans_pass_vend_count,
        cc.counter_pass_vend_count,
        cc.call_in_pass_amount,
        cc.counter_pass_amount
        FROM tracking.counter_call_in_excpt cc
        WHERE cc.server_date = (tDate - 1);

   */
   --EXCEPTION
      
   END;


   PROCEDURE sp_call_in_trans_credit (inDate IN DATE) AS
   BEGIN
        INSERT INTO tracking.scr_call_in_trans_rec
		SELECT cit.device_call_in_record_id,
	    cit.device_id,
	    cit.credit_trans_count call_in_credit_count,
	    SUM(cit.trans_count) trans_credit_count,
	    cit.credit_trans_total call_in_credit_amount,
	    SUM(cit.trans_cc_sum) trans_credit_amount,
	    0 call_in_cash_count,
	    0 trans_cash_count,
	    0 call_in_cash_amount,
	    0 trans_cash_amount,
	    0 call_in_pass_count,
	    0 trans_pass_count,
	    0 call_in_pass_amount,
	    0 trans_pass_amount
	    FROM (	
			SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.credit_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.credit_trans_total,
			NVL2(gtrh.trans_cc_sum, gtrh.trans_cc_sum, 0) trans_cc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(		
				SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trh.trans_no) trans_count,
		        SUM(trh.transaction_amount) trans_cc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_record_hist trh
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trh.machine_id = dev.device_name
		        AND trh.batch_date >= dcir.call_in_start_ts
		        AND trh.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND trh.card_type IN ('VI', 'MC', 'DS', 'AE', 'DC')
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.credit_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
	     
	        UNION ALL
	        
	        SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.credit_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.credit_trans_total,
			NVL2(gtrh.trans_cc_sum, gtrh.trans_cc_sum, 0) trans_cc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trf.trans_no) trans_count,
		        SUM(trf.transaction_amount) trans_cc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_failures trf
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trf.machine_id = dev.device_name
		        AND trf.batch_date >= dcir.call_in_start_ts
		        AND trf.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND trf.card_type IN ('VI', 'MC', 'DS', 'AE', 'DC')
		        AND trf.cancel_cd = 'C' -- Canceled
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.credit_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)     
	        
	        UNION ALL
	        
	        SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.credit_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.credit_trans_total,
			NVL2(gtrh.trans_cc_sum, gtrh.trans_cc_sum, 0) trans_cc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(        
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trf.trans_no) trans_count,
		        SUM(trf.transaction_amount) trans_cc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_failures trf
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trf.machine_id = dev.device_name
		        AND trf.batch_date >= dcir.call_in_start_ts
		        AND trf.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND trf.card_type IN ('VI', 'MC', 'DS', 'AE', 'DC')
		        AND trf.cancel_cd = 'T' -- Vend Time-out
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.credit_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
	        
	        UNION ALL
	        
	        SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.credit_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.credit_trans_total,
			NVL2(gtrh.trans_cc_sum, gtrh.trans_cc_sum, 0) trans_cc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(        
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trf.trans_no) trans_count,
		        SUM(trf.transaction_amount) trans_cc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_failures trf
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trf.machine_id = dev.device_name
		        AND trf.batch_date >= dcir.call_in_start_ts
		        AND trf.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND trf.card_type IN ('VI', 'MC', 'DS', 'AE', 'DC')
		        AND trf.cancel_cd = 'D' -- Denied
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.credit_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
			
	    ) cit
        GROUP BY cit.device_call_in_record_id,
	    cit.device_id,
	    cit.credit_trans_count,
	    cit.credit_trans_total;
	    
   --EXCEPTION
      
   END;


   PROCEDURE sp_call_in_trans_cash (inDate IN DATE) AS
   BEGIN
        INSERT INTO tracking.scr_call_in_trans_rec
		SELECT 
	    dci.device_call_in_record_id,
	    dev.device_id,
	    0 call_in_credit_count,
		0 trans_credit_count,
		0 call_in_credit_amount,
		0 trans_credit_amount,
	    dci.cash_trans_count call_in_cash_count,
	    NVL2(gtr.trans_count, gtr.trans_count, 0) trans_cash_count,
	    dci.cash_trans_total call_in_cash_amount,
	    NVL2(gtr.trans_amount, gtr.trans_amount, 0) trans_cash_amount,
	    0 call_in_pass_count,
		0 trans_pass_count,
		0 call_in_pass_amount,
		0 trans_pass_amount
		FROM device.device dev, device.device_call_in_record dci,
		(
		    SELECT 
		    dcir.device_call_in_record_id,
		    COUNT(tr.cash_trans_no) trans_count,
		    SUM(tr.amount) trans_amount
		    FROM device.device dev, device.device_call_in_record dcir,
		    tazdba.cash_transaction_record tr
		    WHERE dcir.serial_number = dev.device_serial_cd
		    AND tr.machine_id = dev.device_name
		    AND tr.processed_date >= dcir.call_in_start_ts
		    AND tr.processed_date <= dcir.call_in_finish_ts
		    AND dcir.created_ts >= (inDate - 1)
		    AND dcir.created_ts < inDate
		    GROUP BY dcir.device_call_in_record_id
		) gtr
		WHERE dci.serial_number = dev.device_serial_cd
		AND dci.created_ts >= inDate - 1
		AND dci.created_ts < inDate
		AND dci.cash_trans_count > 0
		AND dci.device_call_in_record_id = gtr.device_call_in_record_id (+);
		
   --EXCEPTION
      
   END;


   PROCEDURE sp_call_in_trans_pass (inDate IN DATE) AS
   BEGIN
        INSERT INTO tracking.scr_call_in_trans_rec
		SELECT cit.device_call_in_record_id,
	    cit.device_id,
	    0 call_in_credit_count,
		0 trans_credit_count,
		0 call_in_credit_amount,
		0 trans_credit_amount,
	    0 call_in_cash_count,
	    0 trans_cash_count,
	    0 call_in_cash_amount,
	    0 trans_cash_amount,	    
	    cit.passcard_trans_count call_in_pass_count,
	    SUM(cit.trans_count) trans_pass_count,
	    cit.passcard_trans_total call_in_pass_amount,
	    SUM(cit.trans_pc_sum) trans_pass_amount	    
	    FROM (
	        
			SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.passcard_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.passcard_trans_total,
			NVL2(gtrh.trans_pc_sum, gtrh.trans_pc_sum, 0) trans_pc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trh.trans_no) trans_count,
		        SUM(trh.transaction_amount) trans_pc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_record_hist trh
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trh.machine_id = dev.device_name
		        AND trh.batch_date >= dcir.call_in_start_ts
		        AND trh.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND (trh.card_type = 'SP' OR trh.card_type IS NULL)
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.passcard_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
	     	
	        UNION ALL
			
	        SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.passcard_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.passcard_trans_total,
			NVL2(gtrh.trans_pc_sum, gtrh.trans_pc_sum, 0) trans_pc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(     
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trf.trans_no) trans_count,
		        SUM(trf.transaction_amount) trans_pc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_failures trf
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trf.machine_id = dev.device_name
		        AND trf.batch_date >= dcir.call_in_start_ts
		        AND trf.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND (trf.card_type = 'SP' OR trf.card_type IS NULL)
		        AND trf.cancel_cd = 'C' -- Canceled
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.passcard_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
	     	
	        UNION ALL
			
	        SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.passcard_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.passcard_trans_total,
			NVL2(gtrh.trans_pc_sum, gtrh.trans_pc_sum, 0) trans_pc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(        
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trf.trans_no) trans_count,
		        SUM(trf.transaction_amount) trans_pc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_failures trf
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trf.machine_id = dev.device_name
		        AND trf.batch_date >= dcir.call_in_start_ts
		        AND trf.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND dcir.passcard_trans_count > 0
		        AND (trf.card_type = 'SP' OR trf.card_type IS NULL)
		        AND trf.cancel_cd = 'T' -- Vend Time-out
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.passcard_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
	        
	        UNION ALL
	        
	        SELECT dci.device_call_in_record_id,
			dev.device_id,
			dci.passcard_trans_count,
			NVL2(gtrh.trans_count, gtrh.trans_count, 0) trans_count,
			dci.passcard_trans_total,
			NVL2(gtrh.trans_pc_sum, gtrh.trans_pc_sum, 0) trans_pc_sum
			FROM device.device dev, device.device_call_in_record dci,
			(        
		        SELECT 
		        dcir.device_call_in_record_id,
		        COUNT(trf.trans_no) trans_count,
		        SUM(trf.transaction_amount) trans_pc_sum
		        FROM device.device dev, device.device_call_in_record dcir,
		        tazdba.transaction_failures trf
		        WHERE dcir.serial_number = dev.device_serial_cd
		        AND trf.machine_id = dev.device_name
		        AND trf.batch_date >= dcir.call_in_start_ts
		        AND trf.batch_date <= dcir.call_in_finish_ts
		        AND dcir.created_ts >= (inDate - 1)
		        AND dcir.created_ts < inDate
		        AND (trf.card_type = 'SP' OR trf.card_type IS NULL)
		        AND trf.cancel_cd = 'D' -- Denied
		        GROUP BY dcir.device_call_in_record_id
	        ) gtrh
			WHERE dci.serial_number = dev.device_serial_cd
			AND dci.created_ts >= (inDate - 1)
			AND dci.created_ts < inDate
			AND dci.passcard_trans_count > 0
			AND dci.device_call_in_record_id = gtrh.device_call_in_record_id (+)
			
	    ) cit
	    GROUP BY cit.device_call_in_record_id,
	    cit.device_id,
	    cit.passcard_trans_count,
	    cit.passcard_trans_total;
   
   --EXCEPTION
      
   END;
   
   
   PROCEDURE sp_counter_call_in_tmp (inDateOffset IN NUMBER) AS
      TYPE device_aat IS TABLE OF device.device_id%TYPE INDEX BY PLS_INTEGER;
      devs device_aat;
   BEGIN
        SELECT dev.device_id
	    BULK COLLECT INTO devs
        FROM device.device dev;
        
        
        FORALL adev IN devs.FIRST .. devs.LAST
			
			INSERT INTO tracking.scr_counter_call_in_rec
			
			SELECT dcir.device_call_in_record_id,
			dev.device_id,
			
			dcir.credit_vend_count trans_cred_vend_count,
			ccc.cred_vend_count_id_curr,
			ccc.cred_vend_count_curr,
			ccc.cred_vend_count_id_prev,
			ccc.cred_vend_count_prev,
			ccc.cred_vend_count_rolled,
			
			dcir.credit_trans_total call_in_credit_amount,
			cca.cred_amt_count_id_curr,
			cca.cred_amt_count_curr,
			cca.cred_amt_count_id_prev,
			cca.cred_amt_count_prev,
			cca.cred_amt_count_rolled,
			
			dcir.cash_vend_count trans_cash_vend_count,
			csc.cash_vend_count_id_curr,
			csc.cash_vend_count_curr,
			csc.cash_vend_count_id_prev,
			csc.cash_vend_count_prev,
			csc.cash_vend_count_rolled,
			
			dcir.cash_trans_total call_in_cash_amount,
			csa.cash_amt_count_id_curr,
			csa.cash_amt_count_curr,
			csa.cash_amt_count_id_prev,
			csa.cash_amt_count_prev,
			csa.cash_amt_count_rolled,
			
			dcir.passcard_vend_count trans_pass_vend_count,
			pcc.pass_vend_count_id_curr,
			pcc.pass_vend_count_curr,
			pcc.pass_vend_count_id_prev,
			pcc.pass_vend_count_prev,
			pcc.pass_vend_count_rolled,
			
			dcir.passcard_trans_total call_in_pass_amount,
			pca.pass_amt_count_id_curr,
			pca.pass_amt_count_curr,
			pca.pass_amt_count_id_prev,
			pca.pass_amt_count_prev,
			pca.pass_amt_count_rolled,
			
			sc.session_count_id_curr,
            sc.session_count_curr,
            sc.session_count_id_prev,
            sc.session_count_prev,
            sc.session_count_rolled,
			
			bc.byte_count_id_curr,
            bc.byte_count_curr,
            bc.byte_count_id_prev,
            bc.byte_count_prev,
            bc.byte_count_rolled
			FROM device.device dev, device.device_call_in_record dcir,
			(
				SELECT dcc.device_id,
				dcc.created_ts, 
				dcc.device_counter_id cred_vend_count_id_curr,
				dcc.device_counter_value cred_vend_count_curr,
				dcp.device_counter_id cred_vend_count_id_prev,
				dcp.device_counter_value cred_vend_count_prev,
				(CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
				    WHEN -1 THEN 'Y'
				    ELSE 'N'
				END) cred_vend_count_rolled
				FROM (
				    SELECT ROWNUM rowno, c.*
				    FROM (
				        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
				        dvcc.device_counter_value
				        FROM device.device_counter dvcc
				        WHERE dvcc.device_id = devs(adev)
				        AND dvcc.device_counter_parameter = 'CASHLESS_TRANSACTION'
				        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
				        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcc.created_ts DESC
				    ) c
				) dcc,
				(
				    SELECT (ROWNUM - 1) rowno, p.*
				    FROM (
				        SELECT dvcp.device_counter_id, dvcp.created_ts,
				        dvcp.device_counter_value
				        FROM device.device_counter dvcp
				        WHERE dvcp.device_id = devs(adev)
				        AND dvcp.device_counter_parameter = 'CASHLESS_TRANSACTION'
				        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcp.created_ts DESC
				    ) p
				) dcp
				WHERE dcc.rowno = dcp.rowno
			) ccc,
			(
				SELECT dcc.device_id,
				dcc.created_ts,
				dcc.device_counter_id cred_amt_count_id_curr,
				dcc.device_counter_value cred_amt_count_curr,
				dcp.device_counter_id cred_amt_count_id_prev,
				dcp.device_counter_value cred_amt_count_prev,
				(CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
				    WHEN -1 THEN 'Y'
				    ELSE 'N'
				END) cred_amt_count_rolled
				FROM (
				    SELECT ROWNUM rowno, c.*
				    FROM (
				        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
				        dvcc.device_counter_value
				        FROM device.device_counter dvcc
				        WHERE dvcc.device_id = devs(adev)
				        AND dvcc.device_counter_parameter = 'CASHLESS_MONEY'
				        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
				        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcc.created_ts DESC
				    ) c
				) dcc,
				(
				    SELECT (ROWNUM - 1) rowno, p.*
				    FROM (
				        SELECT dvcp.device_counter_id, dvcp.created_ts,
				        dvcp.device_counter_value
				        FROM device.device_counter dvcp
				        WHERE dvcp.device_id = devs(adev)
				        AND dvcp.device_counter_parameter = 'CASHLESS_MONEY'
				        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcp.created_ts DESC
				    ) p
				) dcp
				WHERE dcc.rowno = dcp.rowno
			) cca,
			(	
				SELECT dcc.device_id,
				dcc.created_ts, 
				dcc.device_counter_id cash_vend_count_id_curr,
				dcc.device_counter_value cash_vend_count_curr,
				dcp.device_counter_id cash_vend_count_id_prev,
				dcp.device_counter_value cash_vend_count_prev,
				(CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
				    WHEN -1 THEN 'Y'
				    ELSE 'N'
				END) cash_vend_count_rolled
				FROM (
				    SELECT ROWNUM rowno, c.*
				    FROM (
				        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
				        dvcc.device_counter_value
				        FROM device.device_counter dvcc
				        WHERE dvcc.device_id = devs(adev)
				        AND dvcc.device_counter_parameter = 'CURRENCY_TRANSACTION'
				        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
				        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcc.created_ts DESC
				    ) c
				) dcc,
				(
				    SELECT (ROWNUM - 1) rowno, p.*
				    FROM (
				        SELECT dvcp.device_counter_id, dvcp.created_ts,
				        dvcp.device_counter_value
				        FROM device.device_counter dvcp
				        WHERE dvcp.device_id = devs(adev)
				        AND dvcp.device_counter_parameter = 'CURRENCY_TRANSACTION'
				        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcp.created_ts DESC
				    ) p
				) dcp
				WHERE dcc.rowno = dcp.rowno
			) csc,
			(
				SELECT dcc.device_id,
				dcc.created_ts,
				dcc.device_counter_id cash_amt_count_id_curr,
				dcc.device_counter_value cash_amt_count_curr,
				dcp.device_counter_id cash_amt_count_id_prev,
				dcp.device_counter_value cash_amt_count_prev,
				(CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
				    WHEN -1 THEN 'Y'
				    ELSE 'N'
				END) cash_amt_count_rolled
				FROM (
				    SELECT ROWNUM rowno, c.*
				    FROM (
				        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
				        dvcc.device_counter_value
				        FROM device.device_counter dvcc
				        WHERE dvcc.device_id = devs(adev)
				        AND dvcc.device_counter_parameter = 'CURRENCY_MONEY'
				        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
				        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcc.created_ts DESC
				    ) c
				) dcc,
				(
				    SELECT (ROWNUM - 1) rowno, p.*
				    FROM (
				        SELECT dvcp.device_counter_id, dvcp.created_ts,
				        dvcp.device_counter_value
				        FROM device.device_counter dvcp
				        WHERE dvcp.device_id = devs(adev)
				        AND dvcp.device_counter_parameter = 'CURRENCY_MONEY'
				        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcp.created_ts DESC
				    ) p
				) dcp
				WHERE dcc.rowno = dcp.rowno
			) csa,
			(	
				SELECT dcc.device_id,
				dcc.created_ts, 
				dcc.device_counter_id pass_vend_count_id_curr,
				dcc.device_counter_value pass_vend_count_curr,
				dcp.device_counter_id pass_vend_count_id_prev,
				dcp.device_counter_value pass_vend_count_prev,
				(CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
				    WHEN -1 THEN 'Y'
				    ELSE 'N'
				END) pass_vend_count_rolled
				FROM (
				    SELECT ROWNUM rowno, c.*
				    FROM (
				        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
				        dvcc.device_counter_value
				        FROM device.device_counter dvcc
				        WHERE dvcc.device_id = devs(adev)
				        AND dvcc.device_counter_parameter = 'PASSCARD_TRANSACTION'
				        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
				        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcc.created_ts DESC
				    ) c
				) dcc,
				(
				    SELECT (ROWNUM - 1) rowno, p.*
				    FROM (
				        SELECT dvcp.device_counter_id, dvcp.created_ts,
				        dvcp.device_counter_value
				        FROM device.device_counter dvcp
				        WHERE dvcp.device_id = devs(adev)
				        AND dvcp.device_counter_parameter = 'PASSCARD_TRANSACTION'
				        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcp.created_ts DESC
				    ) p
				) dcp
				WHERE dcc.rowno = dcp.rowno
			) pcc,
			(
				SELECT dcc.device_id,
				dcc.created_ts,
				dcc.device_counter_id pass_amt_count_id_curr,
				dcc.device_counter_value pass_amt_count_curr,
				dcp.device_counter_id pass_amt_count_id_prev,
				dcp.device_counter_value pass_amt_count_prev,
				(CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
				    WHEN -1 THEN 'Y'
				    ELSE 'N'
				END) pass_amt_count_rolled
				FROM (
				    SELECT ROWNUM rowno, c.*
				    FROM (
				        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
				        dvcc.device_counter_value
				        FROM device.device_counter dvcc
				        WHERE dvcc.device_id = devs(adev)
				        AND dvcc.device_counter_parameter = 'PASSCARD_MONEY'
				        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
				        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcc.created_ts DESC
				    ) c
				) dcc,
				(
				    SELECT (ROWNUM - 1) rowno, p.*
				    FROM (
				        SELECT dvcp.device_counter_id, dvcp.created_ts,
				        dvcp.device_counter_value
				        FROM device.device_counter dvcp
				        WHERE dvcp.device_id = devs(adev)
				        AND dvcp.device_counter_parameter = 'PASSCARD_MONEY'
				        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
				        ORDER BY dvcp.created_ts DESC
				    ) p
				) dcp
				WHERE dcc.rowno = dcp.rowno
			) pca,
			(
				SELECT dcc.device_id,
                dcc.created_ts,
                dcc.device_counter_id session_count_id_curr,
                dcc.device_counter_value session_count_curr,
                dcp.device_counter_id session_count_id_prev,
                dcp.device_counter_value session_count_prev,
                (CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
                    WHEN -1 THEN 'Y'
                    ELSE 'N'
                END) session_count_rolled
                FROM (
                    SELECT ROWNUM rowno, c.*
                    FROM (
                        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
                        dvcc.device_counter_value
                        FROM device.device_counter dvcc
                        WHERE dvcc.device_id = devs(adev)
                        AND dvcc.device_counter_parameter = 'TOTAL_SESSIONS'
                        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
                        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
                        ORDER BY dvcc.created_ts DESC
                    ) c
                ) dcc,
                (
                    SELECT (ROWNUM - 1) rowno, p.*
                    FROM (
                        SELECT dvcp.device_counter_id, dvcp.created_ts,
                        dvcp.device_counter_value
                        FROM device.device_counter dvcp
                        WHERE dvcp.device_id = devs(adev)
                        AND dvcp.device_counter_parameter = 'TOTAL_SESSIONS'
                        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
                        ORDER BY dvcp.created_ts DESC
                    ) p
                ) dcp
                WHERE dcc.rowno = dcp.rowno
			) sc,
			(
				SELECT dcc.device_id,
                dcc.created_ts,
                dcc.device_counter_id byte_count_id_curr,
                dcc.device_counter_value byte_count_curr,
                dcp.device_counter_id byte_count_id_prev,
                dcp.device_counter_value byte_count_prev,
                (CASE SIGN(dcc.device_counter_value - dcp.device_counter_value)
                    WHEN -1 THEN 'Y'
                    ELSE 'N'
                END) byte_count_rolled
                FROM (
                    SELECT ROWNUM rowno, c.*
                    FROM (
                        SELECT dvcc.device_id, dvcc.device_counter_id, dvcc.created_ts,
                        dvcc.device_counter_value
                        FROM device.device_counter dvcc
                        WHERE dvcc.device_id = devs(adev)
                        AND dvcc.device_counter_parameter = 'TOTAL_BYTES'
                        AND dvcc.created_ts >= TRUNC(SYSDATE - inDateOffset - 1)
                        AND dvcc.created_ts < TRUNC(SYSDATE - inDateOffset)
                        ORDER BY dvcc.created_ts DESC
                    ) c
                ) dcc,
                (
                    SELECT (ROWNUM - 1) rowno, p.*
                    FROM (
                        SELECT dvcp.device_counter_id, dvcp.created_ts,
                        dvcp.device_counter_value
                        FROM device.device_counter dvcp
                        WHERE dvcp.device_id = devs(adev)
                        AND dvcp.device_counter_parameter = 'TOTAL_BYTES'
                        AND dvcp.created_ts < TRUNC(SYSDATE - inDateOffset)
                        ORDER BY dvcp.created_ts DESC
                    ) p
                ) dcp
                WHERE dcc.rowno = dcp.rowno
			) bc
			WHERE dcir.serial_number = dev.device_serial_cd
			AND dev.device_id = ccc.device_id
			AND ccc.created_ts >= dcir.call_in_start_ts
			AND ccc.created_ts <= dcir.call_in_finish_ts			
			AND cca.created_ts >= dcir.call_in_start_ts
			AND cca.created_ts <= dcir.call_in_finish_ts			
			AND csc.created_ts >= dcir.call_in_start_ts
			AND csc.created_ts <= dcir.call_in_finish_ts			
			AND csa.created_ts >= dcir.call_in_start_ts
			AND csa.created_ts <= dcir.call_in_finish_ts			
			AND pcc.created_ts >= dcir.call_in_start_ts
			AND pcc.created_ts <= dcir.call_in_finish_ts			
			AND pca.created_ts >= dcir.call_in_start_ts
			AND pca.created_ts <= dcir.call_in_finish_ts			
			AND sc.created_ts >= dcir.call_in_start_ts
			AND sc.created_ts <= dcir.call_in_finish_ts			
			AND bc.created_ts >= dcir.call_in_start_ts
			AND bc.created_ts <= dcir.call_in_finish_ts;
		
   
   --EXCEPTION
      
   END;
   
END;
/


-- End of DDL Script for Package TRACKING.PKG_RECONCILIATION
