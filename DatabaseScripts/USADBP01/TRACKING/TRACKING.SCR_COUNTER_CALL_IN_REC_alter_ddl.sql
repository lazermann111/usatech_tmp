ALTER TABLE tracking.scr_counter_call_in_rec ADD
    (session_count_id_curr        NUMBER(20,0),
    session_count_curr           NUMBER(20,0),
    session_count_id_prev        NUMBER(20,0),
    session_count_prev           NUMBER(20,0),
    session_count_rolled         CHAR(1),
    byte_count_id_curr           NUMBER(20,0),
    byte_count_curr              NUMBER(20,0),
    byte_count_id_prev           NUMBER(20,0),
    byte_count_prev              NUMBER(20,0),
    byte_count_rolled            CHAR(1))
/
