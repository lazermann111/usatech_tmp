-- Start of DDL Script for Table TRACKING.HTTP_CHECK

CREATE TABLE tracking.http_check
    (http_check_id                  NUMBER(20,0) NOT NULL,
    http_check_type_id             NUMBER(20,0) NOT NULL,
    http_check_status              CHAR(1) DEFAULT 'U'  NOT NULL,
    response_code                  NUMBER(20,0) NOT NULL,
    response_msg                   VARCHAR2(60) NOT NULL,
    response_time_ms               NUMBER(20,0) NOT NULL,
    content_length                 NUMBER(20,0) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  tracking_data
/


-- Grants for Table
GRANT SELECT ON tracking.http_check TO web_user
/
GRANT SELECT ON tracking.http_check TO net_user
/


-- Indexes for TRACKING.HTTP_CHECK

CREATE INDEX tracking.ix_http_check_1 ON tracking.http_check
  (
    http_check_type_id               ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/


-- Constraints for TRACKING.HTTP_CHECK

ALTER TABLE tracking.http_check
ADD CONSTRAINT pk_http_check_id PRIMARY KEY (http_check_id)
USING INDEX
  TABLESPACE  tracking_data
/


-- Triggers for TRACKING.HTTP_CHECK

CREATE OR REPLACE TRIGGER tracking.trbi_http_check
 BEFORE
  INSERT
 ON tracking.http_check
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.http_check_id IS NULL THEN

      SELECT seq_http_check_id.NEXTVAL
        INTO :new.http_check_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER tracking.trbu_http_check
 BEFORE
  UPDATE
 ON tracking.http_check
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table TRACKING.HTTP_CHECK

-- Foreign Key
ALTER TABLE tracking.http_check
ADD CONSTRAINT fk_http_check_http_check_type FOREIGN KEY (http_check_type_id)
REFERENCES TRACKING.http_check_type (http_check_type_id)
/
-- End of DDL script for Foreign Key(s)
