/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will implement vend counters for the reconciliaton. It includes an update to a stored procedure (in a package). 

Created By:
J Bradley 

Date:
2005-07-18 

Grant Privileges (run as):
TRACKING 

*/ 



-- reconciliation package - update to the sp_counter_call_in_tmp stored proc (for the above)
@TRACKING.PKG_RECONCILIATION_ddl.sql;

-- reconciliation package - update to the call in exception view (for the above)
@TRACKING.COUNTER_CALL_IN_EXCPT_ddl.sql; 


