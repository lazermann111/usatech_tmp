/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will update the session counter tracking to include looking at previous calls. It includes a new function and an update to the view for exception reporting. 

Created By:
J Bradley 

Date:
2005-09-19 

Grant Privileges (run as):
TRACKING 

*/ 



-- FN_CALLS_AFTER_PRIOR_BATCH - new function to count call-ins after the batch prior to the argument
@TRACKING.FN_CALLS_AFTER_PRIOR_BATCH_ddl.sql;

-- session counter - exception view update 
@TRACKING.SESSION_COUNTER_EXCPT_ddl.sql; 


