/* 
Description:


This is an implementation for Customer Reporting Web Monitoring to track the monitor checking for later reporting. It includes two new tables, two sequences (for the tables) and an initial data load for one of the tables. 

Created By:
J Bradley 

Date:
2005-10-24 

Grant Privileges (run as):
SYSTEM 

*/ 



-- HTTP_CHECK_TYPE - new table to identify the check (monitoring) type (this is made to be extensible)
@TRACKING.HTTP_CHECK_TYPE_ddl.sql;

-- SEQ_HTTP_CHECK_TYPE_ID - sequence for the above table
@TRACKING.SEQ_HTTP_CHECK_TYPE_ID_ddl.sql;

-- Initial data load for the HTTP_CHECK_TYPE table
@TRACKING.INSERT_HTTP_CHECK_TYPE.sql;

-- HTTP_CHECK - new table to track the customer reporting monitoring
@TRACKING.HTTP_CHECK_ddl.sql;

-- SEQ_HTTP_CHECK_ID - sequence for the above table
@TRACKING.SEQ_HTTP_CHECK_ID_ddl.sql;



