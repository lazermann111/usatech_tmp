-- Start of DDL Script for Table TRACKING.CALL_IN_TRANS_REC
-- Generated 22-Jun-2005 15:05:57 from TRACKING@USADBD03

CREATE TABLE tracking.call_in_trans_rec
    (call_in_trans_rec_id           NUMBER(20,0) NOT NULL,
    device_call_in_record_id       NUMBER(20,0) NOT NULL,
    device_id                      NUMBER(20,0) NOT NULL,
    machine_id                     VARCHAR2(60) NOT NULL,
    device_serial_cd               VARCHAR2(20) NOT NULL,
    device_type                    VARCHAR2(60) NOT NULL,
    firmware_version               VARCHAR2(60) DEFAULT 'Unknown'

  NOT NULL,
    server_date                    DATE NOT NULL,
    call_in_credit_count           NUMBER(7,0) DEFAULT 0   NOT NULL,
    trans_credit_count             NUMBER(7,0) DEFAULT 0   NOT NULL,
    call_in_credit_amount          NUMBER(7,2) DEFAULT 0   NOT NULL,
    trans_credit_amount            NUMBER(7,2) DEFAULT 0   NOT NULL,
    call_in_cash_count             NUMBER(7,0) DEFAULT 0   NOT NULL,
    trans_cash_count               NUMBER(7,0) DEFAULT 0   NOT NULL,
    call_in_cash_amount            NUMBER(7,2) DEFAULT 0   NOT NULL,
    trans_cash_amount              NUMBER(7,2) DEFAULT 0   NOT NULL,
    call_in_pass_count             NUMBER(7,0) DEFAULT 0   NOT NULL,
    trans_pass_count               NUMBER(7,0) DEFAULT 0   NOT NULL,
    call_in_pass_amount            NUMBER(7,2) DEFAULT 0   NOT NULL,
    trans_pass_amount              NUMBER(7,2) DEFAULT 0   NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  tracking_data
/




-- Indexes for TRACKING.CALL_IN_TRANS_REC

CREATE INDEX tracking.ix_call_in_trans_rec_4 ON tracking.call_in_trans_rec
  (
    device_serial_cd                ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX tracking.ix_call_in_trans_rec_5 ON tracking.call_in_trans_rec
  (
    server_date                     ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX tracking.ix_call_in_trans_rec_2 ON tracking.call_in_trans_rec
  (
    device_id                       ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX tracking.ix_call_in_trans_rec_3 ON tracking.call_in_trans_rec
  (
    machine_id                      ASC
  )
  TABLESPACE  tracking_indx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- Constraints for TRACKING.CALL_IN_TRANS_REC

ALTER TABLE tracking.call_in_trans_rec
ADD CONSTRAINT pk_call_in_trans_rec_id PRIMARY KEY (call_in_trans_rec_id)
USING INDEX
  TABLESPACE  tracking_data
/


-- Triggers for TRACKING.CALL_IN_TRANS_REC

CREATE OR REPLACE TRIGGER tracking.trbu_call_in_trans_rec
 BEFORE
  UPDATE
 ON tracking.call_in_trans_rec
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER tracking.trbi_call_in_trans_rec
 BEFORE
  INSERT
 ON tracking.call_in_trans_rec
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.call_in_trans_rec_id IS NULL THEN

      SELECT seq_call_in_trans_rec_id.NEXTVAL
        INTO :new.call_in_trans_rec_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table TRACKING.CALL_IN_TRANS_REC

