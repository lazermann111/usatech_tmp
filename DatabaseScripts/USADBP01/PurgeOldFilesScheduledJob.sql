DECLARE
	ld_sysdate DATE := SYSDATE;
BEGIN
	DELETE FROM device.device_file_transfer_event
	WHERE device_file_transfer_id IN (SELECT device_file_transfer_id FROM device.device_file_transfer WHERE file_transfer_id IN
		(SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (0, 3) AND created_ts < ld_sysdate - 90));
	COMMIT;

	DELETE FROM device.device_file_transfer
	WHERE file_transfer_id IN (SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (0, 3) AND created_ts < ld_sysdate - 90);
	COMMIT;

	DELETE FROM device.file_transfer
	WHERE file_transfer_type_cd IN (0, 3) AND created_ts < ld_sysdate - 90;
	COMMIT;

	DELETE FROM device.device_file_transfer_event
	WHERE device_file_transfer_id IN (SELECT device_file_transfer_id FROM device.device_file_transfer WHERE file_transfer_id IN
		(SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (19) AND created_ts < ld_sysdate - 7 AND file_transfer_name LIKE 'TD%-PLV'));
	COMMIT;

	DELETE FROM device.device_file_transfer
	WHERE file_transfer_id IN (SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (19) AND created_ts < ld_sysdate - 7 AND file_transfer_name LIKE 'TD%-PLV');
	COMMIT;

	DELETE FROM device.file_transfer
	WHERE file_transfer_type_cd IN (19) AND created_ts < ld_sysdate - 7 AND file_transfer_name LIKE 'TD%-PLV';
	COMMIT;
	
	DELETE FROM device.device_file_transfer_event
	WHERE device_file_transfer_id IN (SELECT device_file_transfer_id FROM device.device_file_transfer WHERE file_transfer_id IN
		(SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (2, 9) AND created_ts < ld_sysdate - 14)
		AND file_transfer_id NOT IN (SELECT last_file_transfer_id FROM device.gprs_device WHERE last_file_transfer_id IS NOT NULL));
	COMMIT;

	DELETE FROM device.device_file_transfer
	WHERE file_transfer_id IN (SELECT file_transfer_id FROM device.file_transfer WHERE file_transfer_type_cd IN (2, 9) AND created_ts < ld_sysdate - 14)
		AND file_transfer_id NOT IN (SELECT last_file_transfer_id FROM device.gprs_device WHERE last_file_transfer_id IS NOT NULL);
	COMMIT;

	DELETE FROM device.file_transfer
	WHERE file_transfer_type_cd IN (2, 9) AND created_ts < ld_sysdate - 14
		AND file_transfer_id NOT IN (SELECT last_file_transfer_id FROM device.gprs_device WHERE last_file_transfer_id IS NOT NULL);
	COMMIT;
END;
