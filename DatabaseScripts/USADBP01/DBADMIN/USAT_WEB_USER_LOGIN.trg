create or replace TRIGGER DBADMIN.USAT_WEB_USER_LOGIN after LOGON on DATABASE
DECLARE
PRAGMA AUTONOMOUS_TRANSACTION;
    v_username   varchar2(30);
    v_ip         varchar2(30);
    v_machine    varchar2(64);
    v_known      number;
    v_access     number;

  begin
    select count(1) into v_known from DBADMIN.USAT_KNOWN_APPS_HOSTS
  where APPS_USERNAME=sys_context('userenv', 'session_user');
  if v_known>0
 then
SELECT sys_context('userenv', 'session_user'), NVL(SYS_CONTEXT('USERENV','ip_address'),SYS_CONTEXT('USERENV','host')),SYS_CONTEXT('USERENV','host')
  INTO v_username, v_ip, v_machine from dual;
select count(1) into v_access from DBADMIN.USAT_KNOWN_APPS_HOSTS
where APPS_USERNAME=v_username and HOST_IP=v_ip;
if v_access=1
 then
       null;
       else
                      insert into dbadmin.usat_audit_table(username, ip, machine, logon_date)
					  values (v_username, v_ip, v_machine, sysdate);
                      commit;
           raise_application_error(-20999,'Apps User, Access Denied!');
              end if;
   end if;
end;
/