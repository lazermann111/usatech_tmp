CREATE OR REPLACE FUNCTION DBADMIN.GET_AUTHED_CARD (
	l_parsed_acct_data IN VARCHAR2)
RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
/*
SELECT '''' || REGEXP_REPLACE(AUTHORITY_PAYMENT_MASK_REGEX, '([^\\])\(\?\:', '\1(') || ''', -- ' || AUTHORITY_PAYMENT_MASK_DESC 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
 WHERE AUTHORITY_ASSN_ID IS NOT NULL
*/
    lt_cc_regex VARCHAR2_TABLE :=  VARCHAR2_TABLE(
        '^;?(4[0-9]{15})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Visa Credit Card on Track 2
        '^;?(5[1-5][0-9]{14})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- MasterCard Credit Card on Track 2
        '^;?(3[47][0-9]{13})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- American Express Credit Card on Track 2
        '^;?((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Discover Card Credit Card on Track 2
        '^;?(36[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club International Credit Card on Track 2
        '^;?(30[0-5][0-9]{11})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club Carte Blanche Credit Card on Track 2
        '^%?B(4[0-9]{15})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Visa Credit Card on Track 1
        '^%?B(5[1-5][0-9]{14})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- MasterCard Credit Card on Track 1
        '^%?B(3[47][0-9]{13})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- American Express Credit Card on Track 1
        '^%?B((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Discover Card Credit Card on Track 1
        '^%?B(36[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Diners Club International Credit Card on Track 1
        '^%?B(30[0-5][0-9]{11})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$' -- Diners Club Carte Blanche Credit Card on Track 1
    ); 
    lv_auth_card_data VARCHAR2(19);
BEGIN
    SELECT REGEXP_REPLACE(l_parsed_acct_data, a.COLUMN_VALUE, '\1')
      INTO lv_auth_card_data
      FROM TABLE(lt_cc_regex) a
     WHERE REGEXP_LIKE(l_parsed_acct_data, a.COLUMN_VALUE)
       AND ROWNUM = 1;
    RETURN lv_auth_card_data;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN l_parsed_acct_data;
END; 
/