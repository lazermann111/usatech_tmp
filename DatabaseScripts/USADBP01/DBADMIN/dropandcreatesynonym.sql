-- Drop public synonym on tran and auth and reacreate as private

drop public synonym AUTH;

CREATE SYNONYM WEB_USER.AUTH FOR PSS.AUTH;
CREATE SYNONYM WEB_USER1.AUTH FOR PSS.AUTH;
CREATE SYNONYM RDW_LOADER.AUTH FOR PSS.AUTH;

drop public synonym TRAN;

CREATE SYNONYM RDW_LOADER.TRAN FOR PSS.TRAN;
CREATE SYNONYM DEVICE.TRAN FOR PSS.TRAN;
CREATE SYNONYM WEB_USER1.TRAN FOR PSS.TRAN;
CREATE SYNONYM WEB_USER.TRAN FOR PSS.TRAN;
