CREATE OR REPLACE FUNCTION DBADMIN.MASK_TRACK_DATA (PV_TRACK_DATA VARCHAR2)
   RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE
IS
	ln_1d_pos PLS_INTEGER;
    ln_eq_pos PLS_INTEGER;
BEGIN  
    ln_1d_pos := REGEXP_INSTR(pv_track_data, '[0-9]{13,}');
    IF ln_1d_pos  = 0 THEN
        RETURN pv_track_data;
    END IF;
    ln_eq_pos := REGEXP_INSTR(pv_track_data, '[^0-9]', ln_1d_pos + 12);
    IF ln_eq_pos = 0 THEN
        RETURN SUBSTR(pv_track_data, 1, ln_1d_pos - 1) || RPAD(SUBSTR(pv_track_data, ln_1d_pos, 6), LENGTH(pv_track_data) - 3 - ln_1d_pos, '*') || SUBSTR(pv_track_data, -4);
    ELSE
        RETURN SUBSTR(pv_track_data, 1, ln_1d_pos - 1) || RPAD(SUBSTR(pv_track_data, ln_1d_pos, 6), ln_eq_pos - 4 - ln_1d_pos, '*') || SUBSTR(pv_track_data, ln_eq_pos - 4, 4)
            || REGEXP_REPLACE(SUBSTR(pv_track_data, ln_eq_pos), '[0-9A-Za-z]' ,'*');
    END IF;
END; 
/