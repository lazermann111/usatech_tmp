CREATE OR REPLACE FUNCTION DBADMIN.TIMESTAMP_TO_MILLIS(pt_timestamp TIMESTAMP)
   RETURN NUMBER DETERMINISTIC PARALLEL_ENABLE
IS
    li_diff INTERVAL DAY(9) TO SECOND(3);
    c_epoch_ts CONSTANT TIMESTAMP := TO_TIMESTAMP('01/01/1970', 'MM/DD/YYYY');
BEGIN
    IF pt_timestamp IS NULL THEN
        RETURN NULL;
    ELSE
        li_diff := pt_timestamp - c_epoch_ts;
        RETURN EXTRACT(DAY FROM li_diff) * 86400000
             + EXTRACT(HOUR FROM li_diff) * 3600000
             + EXTRACT(MINUTE FROM li_diff) * 60000
             + EXTRACT(SECOND FROM li_diff)  * 1000;
    END IF;
END;
/