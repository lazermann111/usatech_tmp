create or replace FUNCTION         DBADMIN.GET_UNSALTED_HASH 
(
  PR_DATA IN RAW 
, PV_ALGORITHM IN VARCHAR2 
) RETURN RAW
AS LANGUAGE JAVA NAME 'simple.security.SecureHash.getUnsaltedHash(byte[], java.lang.String) return byte[]';
/