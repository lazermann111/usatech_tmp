CREATE OR REPLACE FUNCTION DBADMIN.usat_mask_exp_data(pv_exp_data VARCHAR2)
   RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE
IS
      lv_exp_num VARCHAR2(4000);
BEGIN
      IF pv_exp_data IS NULL THEN
            RETURN NULL;
      END IF;
      
      lv_exp_num := REGEXP_SUBSTR(pv_exp_data, '[0-9]{4,}');

      IF lv_exp_num IS NULL THEN
            RETURN pv_exp_data;
      ELSE
            RETURN SUBSTR('********',1,LENGTH(lv_exp_num));
      END IF;
END;
/