CREATE OR REPLACE FUNCTION DBADMIN.GET_PRIMARY_POSTAL(
    pv_postal_cd VARCHAR2,
    pv_country_cd CORP.COUNTRY.COUNTRY_CD%TYPE)
    RETURN VARCHAR2
IS
    lv_postal_replace VARCHAR2(4000) := '';
    lv_postal_regex CORP.COUNTRY.POSTAL_REGEX%TYPE;
    ln_parts CORP.COUNTRY.POSTAL_PRIMARY_PARTS%TYPE;
BEGIN
    SELECT POSTAL_REGEX, POSTAL_PRIMARY_PARTS
      INTO lv_postal_regex, ln_parts
      FROM CORP.COUNTRY
     WHERE COUNTRY_CD = pv_country_cd;
    FOR n IN 1..ln_parts LOOP
        lv_postal_replace := lv_postal_replace || '\' || n;
    END LOOP;
    RETURN REGEXP_REPLACE(pv_postal_cd, lv_postal_regex, lv_postal_replace);
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN pv_postal_cd;
END;
/