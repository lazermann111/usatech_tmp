CREATE OR REPLACE PROCEDURE DBADMIN."USAT_ADD_WEEKLY_PARTITION" (
          v_table_owner VARCHAR2,
          v_table_name VARCHAR2,
          v_until NUMBER)
AUTHID CURRENT_USER
AS
    v_part_name  VARCHAR2(12);
    v_part_value VARCHAR2(10);
    v_last_value LONG;
    v_high_value DATE;
    v_this_week DATE;
    v_highest_value DATE;
    v_count NUMBER(1);
    v_sql VARCHAR2(4000);
BEGIN
    --Get the High value of the partition
       SELECT  max(to_date(to_char(partition_name),'YYYY-MM-DD'))
      INTO v_this_week
      FROM DBA_TAB_PARTITIONS
     WHERE TABLE_OWNER = v_table_owner
       AND TABLE_NAME = v_table_name;
    --Construct partition_name for next month with format "YYYY-MM-01";
    SELECT *
     INTO v_last_value 
      FROM (
        SELECT HIGH_VALUE
          FROM ALL_TAB_PARTITIONS
         WHERE TABLE_OWNER = v_table_owner
           AND TABLE_NAME = v_table_name
           AND to_date(to_char(partition_name),'YYYY-MM-DD')=v_this_week
           ORDER BY PARTITION_POSITION DESC)
           WHERE ROWNUM = 1;
     -- Calculate high value as Date 
    EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(v_last_value) || ' FROM DUAL' INTO v_high_value;
    SELECT TRUNC(DBADMIN.USAT_ADD_WEEKS(v_high_value, v_until), 'DD') INTO v_highest_value FROM DUAL;
        
   -- Add all need partitions
    WHILE v_highest_value >= v_high_value LOOP
         SELECT TRUNC(DBADMIN.USAT_ADD_WEEKS(v_high_value, 1), 'DD') INTO v_high_value FROM DUAL;
         v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' ADD PARTITION "'
                   ||TO_CHAR(v_high_value, 'YYYY-MM-DD')||'" VALUES LESS THAN (TO_DATE ('''
             ||TO_CHAR(v_high_value, 'YYYY/MM/DD')||''', ''YYYY/MM/DD''))';
         DBMS_OUTPUT.PUT_LINE (v_sql);
         EXECUTE IMMEDIATE v_sql;
    END LOOP;
END;
/


CREATE PUBLIC SYNONYM USAT_ADD_WEEKLY_PARTITION FOR DBADMIN.USAT_ADD_WEEKLY_PARTITION;


GRANT EXECUTE ON DBADMIN.USAT_ADD_WEEKLY_PARTITION TO SYSTEM;
