CREATE OR REPLACE FUNCTION DBADMIN.READ_LONG (
       l_long_value LONG)
 RETURN VARCHAR
AS
     l_lob CLOB;
BEGIN
     DBMS_LOB.CREATETEMPORARY(l_lob, TRUE); 
     DBMS_LOB.WRITEAPPEND (l_lob, LENGTH(l_long_value), l_long_value);
     RETURN DBMS_LOB.SUBSTR(l_lob, 4000, 1);
END;

 
 
 
 
/