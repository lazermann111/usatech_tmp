CREATE OR REPLACE FUNCTION DBADMIN.DATE_TO_MILLIS (PD_DATE DATE)
   RETURN NUMBER DETERMINISTIC PARALLEL_ENABLE
IS
    li_diff INTERVAL DAY(9) TO SECOND(0);
    lt_epoch TIMESTAMP WITH TIME ZONE := TIMESTAMP '1970-01-01 0:00:00 +0:00';
    INVALID_DATE EXCEPTION;   
    PRAGMA EXCEPTION_INIT(INVALID_DATE, -1878);
BEGIN
    IF pd_date IS NULL THEN
        RETURN NULL;
    ELSE
        li_diff := TO_TIMESTAMP_TZ(TO_CHAR(pd_date, 'YYYY-MM-DD HH24:MI:SS') || ' ' || PKG_CONST.DB_TIME_ZONE, 'YYYY-MM-DD HH24:MI:SS TZR') - lt_epoch;
        RETURN EXTRACT(DAY FROM li_diff) * 86400000 
             + EXTRACT(HOUR FROM li_diff) * 3600000
             + EXTRACT(MINUTE FROM li_diff) * 60000
             + EXTRACT(SECOND FROM li_diff)  * 1000;
    END IF;
EXCEPTION
    WHEN INVALID_DATE THEN
        RETURN DBADMIN.DATE_TO_MILLIS(pd_date + (1/24));
END;  
/