CREATE OR REPLACE PROCEDURE DBADMIN."USAT_CHECK_CREATE_WEEKLY_PARTS" AUTHID CURRENT_USER is
--Created by Marie Njanje on 01/14/08 to check partitions daily
Begin
DECLARE
   CURSOR part_list_cursor IS
  select b.table_owner, b.table_name, partition_name, num_rows, high_value
    from dba_tab_partitions b,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_name not like 'BIN$%'
    AND TABLE_NAME IN ('MACHINE_CMD_OUTBOUND_HIST','MACHINE_CMD_INBOUND_HIST','DEVICE_CALL_IN_RECORD') AND T.TABLE_NAME=K.NAME AND TABLE_OWNER=K.OWNER
    group by table_owner, table_name ) a
    where a.part_pos = PARTITION_POSITION
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM');

     part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_sql VARCHAR2(4000);
     v_sql2 VARCHAR2(4000);
     v_added_value constant VARCHAR2(30):= 'SYSDATE +30';
     v_added_num constant NUMBER:= 4;

BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_high_value := part_rec.high_value;
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value;
           IF v_highest_value < SYSDATE +14 and v_table_name!='DEVICE_CALL_IN_RECORD' THEN                   
                       v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_WEEKLY_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql2;  
                      ELSE
           IF v_highest_value < SYSDATE +14 and v_table_name='DEVICE_CALL_IN_RECORD' THEN        
                        v_sql := 'begin'||chr(10)||'DBADMIN.USAT_ADD_WEEKLY_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_num||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql;          
           END IF;
       END IF;
       END IF;
    END LOOP;
END;
END;
/


GRANT DEBUG ON DBADMIN.USAT_CHECK_CREATE_WEEKLY_PARTS TO USAT_DEV_READ_ONLY;

