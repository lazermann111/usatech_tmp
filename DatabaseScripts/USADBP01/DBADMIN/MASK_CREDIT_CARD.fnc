CREATE OR REPLACE FUNCTION DBADMIN.MASK_CREDIT_CARD (PV_CARD_DATA VARCHAR2)
   RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE
IS
	lv_card_number VARCHAR2(4000);
BEGIN
	lv_card_number := REGEXP_SUBSTR(pv_card_data, '[0-9]{13,}');

	IF lv_card_number IS NULL THEN
		RETURN pv_card_data;
	ELSE
		RETURN RPAD(SUBSTR(lv_card_number, 1, 6), LENGTH(lv_card_number) - 4, '*') || SUBSTR(lv_card_number, -4);
	END IF;
END;
/