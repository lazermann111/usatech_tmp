CREATE OR REPLACE FUNCTION DBADMIN.VERSION_COMPARE(
    pv_version_1 VARCHAR,
    pv_version_2 VARCHAR)
    RETURN PLS_INTEGER
IS
    ln_pos_1 PLS_INTEGER := 1;
    ln_pos_2 PLS_INTEGER := 1;    
    ln_next_1 PLS_INTEGER := 1;
    ln_next_2 PLS_INTEGER := 1;    
    lv_sub_1 VARCHAR(4000);
    lv_sub_2 VARCHAR(4000);    
    ln_part_1 NUMBER;
    ln_part_2 NUMBER;    
BEGIN
	IF pv_version_1 IS NULL THEN
        IF pv_version_2 IS NULL THEN
            RETURN 0;
        ELSE
            RETURN -1;
        END IF;
    ELSIF pv_version_2 IS NULL THEN
        RETURN 1;
    ELSIF pv_version_1 = pv_version_2 THEN
        RETURN 0;
    END IF;
    LOOP
        ln_next_1 := INSTR(pv_version_1, '.', ln_pos_1);
        ln_next_2 := INSTR(pv_version_2, '.', ln_pos_2);
        IF ln_next_1 = 0 THEN
            ln_next_1 := REGEXP_INSTR(pv_version_1, '[^0-9]', ln_pos_1);
            IF ln_next_1 <= ln_pos_1 THEN
                ln_next_1 := LENGTH(pv_version_1) + 1;
            END IF;
        END IF;
        IF ln_next_2 = 0 THEN
            ln_next_2 := REGEXP_INSTR(pv_version_2, '[^0-9]', ln_pos_2);
            IF ln_next_2 <= ln_pos_2 THEN
                ln_next_2 := LENGTH(pv_version_2) + 1;
            END IF;
        END IF;
        lv_sub_1 := SUBSTR(pv_version_1, ln_pos_1, ln_next_1 - ln_pos_1);
        lv_sub_2 := SUBSTR(pv_version_2, ln_pos_2, ln_next_2 - ln_pos_2);
        ln_part_1 := TO_NUMBER_OR_NULL(lv_sub_1);
        ln_part_2 := TO_NUMBER_OR_NULL(lv_sub_2);
        IF ln_part_1 IS NOT NULL THEN
            IF ln_part_2 IS NULL OR ln_part_1 < ln_part_2 THEN
                RETURN -1;
            ELSIF ln_part_1 > ln_part_2 THEN
                RETURN 1;
            END IF;
        ELSIF ln_part_2 IS NOT NULL THEN
            RETURN 1;
        ELSIF lv_sub_1 IS NOT NULL THEN
            IF lv_sub_2 IS NULL OR lv_sub_1 < lv_sub_2 THEN
                RETURN -1;
            ELSIF lv_sub_1 > lv_sub_2 THEN
                RETURN 1;
            END IF;
        ELSIF lv_sub_2 IS NOT NULL THEN
            RETURN 1;
        END IF;
        IF SUBSTR(pv_version_1, ln_next_1, 1) = '.' THEN
            ln_pos_1 := ln_next_1 + 1;
        ELSE
            ln_pos_1 := ln_next_1;
        END IF;
        IF SUBSTR(pv_version_2, ln_next_2, 1) = '.' THEN
            ln_pos_2 := ln_next_2 + 1;
        ELSE
            ln_pos_2 := ln_next_2;
        END IF;
        IF ln_pos_1 > LENGTH(pv_version_1) THEN
            IF ln_pos_2 > LENGTH(pv_version_2) THEN
                RETURN 0;
            ELSE
                RETURN -1;
            END IF;
        ELSIF ln_pos_2 > LENGTH(pv_version_2) THEN
            RETURN 1;
        END IF;
    END LOOP;
END;
/