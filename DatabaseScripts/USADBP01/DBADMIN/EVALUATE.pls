CREATE OR REPLACE FUNCTION DBADMIN.EVALUATE(
    pv_expression VARCHAR2,
    pn_param NUMBER)
    RETURN VARCHAR2
AS
    ln_result VARCHAR2(4000);
BEGIN
    EXECUTE IMMEDIATE 'SELECT ' || pv_expression || ' FROM (SELECT :1 VALUE FROM DUAL)' INTO ln_result USING pn_param;
    RETURN ln_result;
END;
/