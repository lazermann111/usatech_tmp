CREATE TABLE DBADMIN.USAT_KNOWN_APPS_HOSTS
(
  APPS_USERNAME     VARCHAR2(30),
  HOST_NAME  VARCHAR2(30),
  HOST_IP    VARCHAR2(30),
  CREATED_BY        VARCHAR2(30) NOT NULL,
  CREATED_TS        DATE NOT NULL,
  LAST_UPDATED_BY   VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS   DATE  NOT NULL
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;

CREATE TABLE DBADMIN.USAT_AUDIT_TABLE
(
  USERNAME    VARCHAR2(30 BYTE),
  IP          VARCHAR2(30 BYTE),
  MACHINE     VARCHAR2(64 BYTE),
  LOGON_DATE  DATE
)
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER DBADMIN.USAT_TRBU_KNOWN_APPS_HOSTS BEFORE UPDATE ON DBADMIN.USAT_KNOWN_APPS_HOSTS   FOR EACH ROW
BEGIN
 SELECT
  :OLD.created_by,
  :OLD.created_ts,
  SYSDATE,
  USER
 INTO
  :NEW.created_by,
  :NEW.created_ts,
  :NEW.last_updated_ts,
  :NEW.last_updated_by
 FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER DBADMIN.USAT_TRBI_KNOWN_APPS_HOSTS BEFORE INSERT ON DBADMIN.USAT_KNOWN_APPS_HOSTS   FOR EACH ROW
BEGIN
 SELECT
  SYSDATE,
  USER,
  SYSDATE,
  USER
 INTO
  :NEW.created_ts,
  :NEW.created_by,
  :NEW.last_updated_ts,
  :NEW.last_updated_by
 FROM DUAL;
END;
/

ALTER TABLE DBADMIN.USAT_KNOWN_APPS_HOSTS ADD 
CONSTRAINT PK_USAT_KNOWN_APPS_HOST
 PRIMARY KEY (APPS_USERNAME, HOST_IP)
 ENABLE
 VALIDATE;

--- create logon trigger
CREATE OR REPLACE TRIGGER DBADMIN.usat_web_user_login after LOGON on DATABASE
DECLARE
PRAGMA AUTONOMOUS_TRANSACTION;
    v_username   varchar2(30);
    v_ip         varchar2(30);
    v_machine    varchar2(64);
    v_known      number;
    v_access     number;

  begin
    select count(1) into v_known from DBADMIN.USAT_KNOWN_APPS_HOSTS
  where APPS_USERNAME=sys_context('userenv', 'session_user');
  if v_known>0
 then
SELECT sys_context('userenv', 'session_user'), SYS_CONTEXT('USERENV','ip_address'),SYS_CONTEXT('USERENV','host')
  INTO v_username, v_ip, v_machine from dual;
select count(1) into v_access from DBADMIN.USAT_KNOWN_APPS_HOSTS
where APPS_USERNAME=v_username and HOST_IP=v_ip;
if v_access=1
 then
       null;
       else
                      insert into dbadmin.usat_audit_table values (v_username, v_ip, v_machine, sysdate);
                      commit;
             raise_application_error(-20999,'Apps User, Access Denied!');
              end if;
   end if;
  end;
/









