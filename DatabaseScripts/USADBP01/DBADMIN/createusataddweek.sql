CREATE OR REPLACE function DBADMIN.USAT_ADD_WEEKS
      ( in_date    DATE ,
        weeks      INTEGER) RETURN DATE IS

        add_days       NUMBER;
        return_date    DATE;
      BEGIN
         
        add_days := weeks*7; 
        SELECT TO_DATE(in_date) + add_days INTO return_date
        FROM DUAL;
              return ( TO_CHAR(return_date));
      END usat_add_weeks;
/