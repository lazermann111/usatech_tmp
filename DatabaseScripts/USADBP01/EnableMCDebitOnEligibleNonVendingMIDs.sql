UPDATE PSS.POS_PTA_TMPL_ENTRY
SET POS_PTA_DISABLE_DEBIT_DENIAL = 'Y'
WHERE PAYMENT_SUBTYPE_ID IN (
	SELECT PAYMENT_SUBTYPE_ID 
	FROM PSS.PAYMENT_SUBTYPE 
	WHERE LOWER(PAYMENT_SUBTYPE_NAME) LIKE '%mastercard%' AND PAYMENT_SUBTYPE_CLASS = 'Tandem' AND STATUS_CD = 'A'
)
AND PAYMENT_SUBTYPE_KEY_ID IN (
	SELECT T.TERMINAL_ID 
	FROM PSS.TERMINAL T 
	JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID 
	JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
	WHERE A.AUTHORITY_NAME = 'Tandem' AND LOWER(MERCHANT_NAME) NOT LIKE '%vending%' AND LOWER(MERCHANT_NAME) NOT LIKE '%taxi%'
	AND M.STATUS_CD = 'A' AND T.STATUS_CD = 'A'
)
AND (POS_PTA_DISABLE_DEBIT_DENIAL IS NULL OR POS_PTA_DISABLE_DEBIT_DENIAL != 'Y');
COMMIT;

DECLARE
	CURSOR L_CUR IS
	SELECT POS_PTA_ID
	FROM PSS.POS_PTA
	WHERE PAYMENT_SUBTYPE_ID IN (
		SELECT PAYMENT_SUBTYPE_ID 
		FROM PSS.PAYMENT_SUBTYPE 
		WHERE LOWER(PAYMENT_SUBTYPE_NAME) LIKE '%mastercard%' AND PAYMENT_SUBTYPE_CLASS = 'Tandem' AND STATUS_CD = 'A'
	)
	AND PAYMENT_SUBTYPE_KEY_ID IN (
		SELECT T.TERMINAL_ID 
		FROM PSS.TERMINAL T 
		JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID 
		JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
		WHERE A.AUTHORITY_NAME = 'Tandem' AND LOWER(MERCHANT_NAME) NOT LIKE '%vending%' AND LOWER(MERCHANT_NAME) NOT LIKE '%taxi%'
		AND M.STATUS_CD = 'A' AND T.STATUS_CD = 'A'
	)
	AND POS_PTA_ACTIVATION_TS < SYSDATE AND (POS_PTA_DEACTIVATION_TS IS NULL OR POS_PTA_DEACTIVATION_TS > SYSDATE)
	AND (POS_PTA_DISABLE_DEBIT_DENIAL IS NULL OR POS_PTA_DISABLE_DEBIT_DENIAL != 'Y');
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE PSS.POS_PTA
		SET POS_PTA_DISABLE_DEBIT_DENIAL = 'Y'
		WHERE POS_PTA_ID = L_REC.POS_PTA_ID;
		COMMIT;
	END LOOP;
END;
/