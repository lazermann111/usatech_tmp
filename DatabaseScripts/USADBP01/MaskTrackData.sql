DECLARE
    ln_increment PLS_INTEGER := 10000;
    ln_min_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_max_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_start_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT MIN(TRAN_ID), MAX(TRAN_ID)
      INTO ln_min_tran_id, ln_max_tran_id
      FROM PSS.TRAN;
    ln_start_tran_id := ln_min_tran_id;
    WHILE ln_start_tran_id <  ln_max_tran_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating TRAN table from TRAN_ID ' || ln_start_tran_id || ' to ' || (ln_start_tran_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
		INSERT INTO PSS.TRAN_C(
			TRAN_ID, 
            KID, 
            HASH_TYPE_CD, 
            TRAN_PARSED_ACCT_NUM_H) 
		SELECT 
			TRAN_ID,
            -1,
            'SHA1',
            DBADMIN.HASH_CARD(NVL(TRAN_PARSED_ACCT_NUM, REGEXP_SUBSTR(TRAN_RECEIVED_RAW_ACCT_DATA,'[0-9]{13,}')))
		FROM PSS.TRAN T
        WHERE TRAN_ID >= ln_start_tran_id AND TRAN_ID < ln_start_tran_id + ln_increment
			AND REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{13,}')
			AND NOT EXISTS (SELECT 1 FROM PSS.TRAN_C WHERE TRAN_ID = T.TRAN_ID);
        UPDATE PSS.TRAN
           SET TRAN_RECEIVED_RAW_ACCT_DATA = DBADMIN.MASK_CREDIT_CARD(TRAN_RECEIVED_RAW_ACCT_DATA)               
         WHERE TRAN_ID >= ln_start_tran_id AND TRAN_ID < ln_start_tran_id + ln_increment
           AND REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{13,}');
        ln_cnt := SQL%ROWCOUNT;   
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on TRAN table from TRAN_ID ' || ln_start_tran_id || ' to ' || (ln_start_tran_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_tran_id := ln_start_tran_id + ln_increment; 
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Update complete');    
END;
/

DECLARE
    ln_increment PLS_INTEGER := 10000;
    ln_min_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_max_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_start_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT MIN(AUTH_ID), MAX(AUTH_ID)
      INTO ln_min_auth_id, ln_max_auth_id
      FROM PSS.AUTH;
    ln_start_auth_id := ln_min_auth_id;
    WHILE ln_start_auth_id <  ln_max_auth_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating AUTH table from AUTH_ID ' || ln_start_auth_id || ' to ' || (ln_start_auth_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        UPDATE PSS.AUTH
           SET AUTH_PARSED_ACCT_DATA = DBADMIN.GET_AUTHED_CARD(AUTH_PARSED_ACCT_DATA)
         WHERE AUTH_ID >= ln_start_auth_id AND AUTH_ID < ln_start_auth_id + ln_increment
           AND REGEXP_LIKE(AUTH_PARSED_ACCT_DATA, '[0-9]{13,}');
        ln_cnt := SQL%ROWCOUNT;   
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on AUTH table from AUTH_ID ' || ln_start_auth_id || ' to ' || (ln_start_auth_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_auth_id := ln_start_auth_id + ln_increment; 
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Update complete');    
END;
/

DECLARE
    ln_increment PLS_INTEGER := 10000;
    ln_min_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_max_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_start_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT MIN(REFUND_ID), MAX(REFUND_ID)
      INTO ln_min_refund_id, ln_max_refund_id
      FROM PSS.REFUND;
    ln_start_refund_id := ln_min_refund_id;
    WHILE ln_start_refund_id <  ln_max_refund_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating REFUND table from REFUND_ID ' || ln_start_refund_id || ' to ' || (ln_start_refund_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        UPDATE PSS.REFUND
           SET REFUND_PARSED_ACCT_DATA = DBADMIN.GET_AUTHED_CARD(REFUND_PARSED_ACCT_DATA)
         WHERE REFUND_ID >= ln_start_refund_id AND REFUND_ID < ln_start_refund_id + ln_increment
           AND REGEXP_LIKE(REFUND_PARSED_ACCT_DATA, '[0-9]{13,}');
        ln_cnt := SQL%ROWCOUNT;   
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on REFUND table from REFUND_ID ' || ln_start_refund_id || ' to ' || (ln_start_refund_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_refund_id := ln_start_refund_id + ln_increment; 
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Update complete');    
END;
/
