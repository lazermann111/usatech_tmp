UPDATE pss.pos_pta_tmpl_entry SET pos_pta_activation_oset_hr = 0 WHERE pos_pta_activation_oset_hr IS NULL;

UPDATE pss.pos_pta SET pos_pta_activation_ts = SYSDATE WHERE pos_pta_id IN (
	SELECT pp.pos_pta_id FROM pss.pos_pta pp
	JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
	WHERE pp.pos_pta_activation_ts IS NULL
		AND ps.payment_subtype_name IN ('Special Card - Generic', 'RFID Special Card - Generic')
);

COMMIT;
