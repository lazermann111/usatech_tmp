DECLARE
    CURSOR cur_callins IS
		SELECT DEVICE_CALL_IN_RECORD_ID, SERIAL_NUMBER, IP_ADDRESS
		FROM DEVICE.DEVICE_CALL_IN_RECORD
		WHERE DEVICE_CALL_IN_RECORD_ID BETWEEN (
			SELECT DEVICE_CALL_IN_RECORD_ID 
			FROM DEVICE.DEVICE_CALL_IN_RECORD 
			WHERE CALL_IN_START_TS BETWEEN SYSDATE - 21 AND SYSDATE - 14
				AND ROWNUM = 1
		)
			AND (SELECT MAX(DEVICE_CALL_IN_RECORD_ID) FROM DEVICE.DEVICE_CALL_IN_RECORD)
			AND IP_ADDRESS IS NOT NULL
		ORDER BY DEVICE_CALL_IN_RECORD_ID DESC;
		
	lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
	ln_cnt NUMBER := 0;
BEGIN
    DBMS_OUTPUT.put_line('Script is starting...');
	
	FOR rec_callin IN cur_callins LOOP
		ln_cnt := ln_cnt + 1;
		DBMS_OUTPUT.put_line('Processing call-in ' || rec_callin.device_call_in_record_id || ', iteration ' || ln_cnt || '...');
		
		IF SUBSTR(rec_callin.ip_address, 1, 3) IN ('172', '166', '32.') THEN
			lc_comm_method_cd := 'G'; -- GPRS
		ELSIF rec_callin.ip_address = '192.168.79.162' THEN
			lc_comm_method_cd := 'P'; -- POTS
		ELSE
			lc_comm_method_cd := 'E'; -- Ethernet
		END IF;		
	
		UPDATE DEVICE.DEVICE
		SET COMM_METHOD_CD = lc_comm_method_cd
		WHERE DEVICE_SERIAL_CD = rec_callin.serial_number
			AND DEVICE_ACTIVE_YN_FLAG = 'Y'
			AND COMM_METHOD_CD IS NULL;
		
		IF SQL%ROWCOUNT > 0 THEN
			COMMIT;
		END IF;
    END LOOP;
	
	COMMIT;	
	DBMS_OUTPUT.put_line('Script finished, processed ' || ln_cnt || ' call-ins.');
END;
