UPDATE PSS.PAYMENT_SUBTYPE 
SET AUTHORITY_PAYMENT_MASK_ID = (SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Generic Special Card Track 2')
WHERE PAYMENT_SUBTYPE_NAME = 'Apriva - CardSmith';

COMMIT;
