CREATE TABLE DEVICE.EVENT_STATE
  (
    EVENT_STATE_ID    NUMBER(20,0),
    EVENT_STATE_NAME  VARCHAR2(60)    not null,
    EVENT_STATE_DESC  VARCHAR2(255),
    CONSTRAINT PK_EVENT_STATE primary key(EVENT_STATE_ID)
  )
  TABLESPACE DEVICE_DATA;

COMMENT ON TABLE DEVICE.EVENT_STATE is 'Represents possible processing and functional states of an event.  Identifies when an event has completed collection/analytics lifecycle (and thus is ready for consumption), or if event could not be completely loaded.';

GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT_STATE TO WEB_USER, USAT_WEB_ROLE;

INSERT INTO DEVICE.EVENT_STATE (event_state_id, event_state_name, event_state_desc) VALUES (1, 'Incomplete', 'Processing: data still being collected/analyzed');
INSERT INTO DEVICE.EVENT_STATE (event_state_id, event_state_name, event_state_desc) VALUES (2, 'Complete - Final', 'Final: Data collection complete and validation successful');
INSERT INTO DEVICE.EVENT_STATE (event_state_id, event_state_name, event_state_desc) VALUES (3, 'Complete - Error', 'Final: Unrecoverable data collection and/or validation error');
INSERT INTO DEVICE.EVENT_STATE (event_state_id, event_state_name, event_state_desc) VALUES (4, 'Incomplete - Error', 'Manual review required: data error detected during collection and/or validation');



CREATE TABLE DEVICE.EVENT_TYPE
  (
    EVENT_TYPE_ID    NUMBER(20,0),
    EVENT_TYPE_NAME  VARCHAR2(60)    not null,
    EVENT_TYPE_DESC  VARCHAR2(255),
    CONSTRAINT PK_EVENT_TYPE primary key(EVENT_TYPE_ID)
  )
  TABLESPACE DEVICE_DATA;

COMMENT ON TABLE DEVICE.EVENT_TYPE is 'Represent business types for events.  Examples may include server-process oriented types, or host-level alert contexts.';

GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT_TYPE TO WEB_USER, USAT_WEB_ROLE;

INSERT INTO DEVICE.EVENT_TYPE (event_type_id, event_type_name, event_type_desc) VALUES (1, 'Batch', 'Scheduled device batch session');
INSERT INTO DEVICE.EVENT_TYPE (event_type_id, event_type_name, event_type_desc) VALUES (2, 'Fill', 'Products have been replenished');



CREATE TABLE DEVICE.EVENT_TYPE_HOST_TYPE
  (
    EVENT_TYPE_ID             NUMBER(20,0),
    HOST_TYPE_ID              NUMBER(20,0),
    EVENT_TYPE_HOST_TYPE_NUM  NUMBER(20,0)   not null,
    CREATED_BY                VARCHAR2(30)   not null,
    CREATED_TS                DATE           not null,
    LAST_UPDATED_BY           VARCHAR2(30)   not null,
    LAST_UPDATED_TS           DATE           not null,
    CONSTRAINT PK_EVENT_TYPE_HOST_TYPE primary key(EVENT_TYPE_ID,HOST_TYPE_ID),
    CONSTRAINT FK_ETHT_EVENT_TYPE_ID foreign key(EVENT_TYPE_ID) references DEVICE.EVENT_TYPE(EVENT_TYPE_ID),
    CONSTRAINT FK_ETHT_HOST_TYPE_ID foreign key(HOST_TYPE_ID) references DEVICE.HOST_TYPE(HOST_TYPE_ID)    
  )
  TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX DEVICE.UDX_EVENT_TYPE_HOST_TYPE_NUM ON DEVICE.EVENT_TYPE_HOST_TYPE(HOST_TYPE_ID,EVENT_TYPE_HOST_TYPE_NUM) TABLESPACE DEVICE_INDX;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_EVENT_TYPE_HOST_TYPE BEFORE INSERT ON DEVICE.EVENT_TYPE_HOST_TYPE
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER DEVICE.TRBU_EVENT_TYPE_HOST_TYPE BEFORE UPDATE ON DEVICE.EVENT_TYPE_HOST_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

COMMENT ON TABLE DEVICE.EVENT_TYPE_HOST_TYPE is 'Mapping table that restricts specific event types to only be allowed by specific host types.';
COMMENT ON COLUMN DEVICE.EVENT_TYPE_HOST_TYPE.EVENT_TYPE_HOST_TYPE_NUM is 'Unique identifier per host type, that represents allowed event types per host type.  Intended to be supported directly by hosts when reporting events.';

GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT_TYPE_HOST_TYPE TO WEB_USER, USAT_WEB_ROLE;

INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE (event_type_id, host_type_id, event_type_host_type_num) SELECT 1, host_type_id, 66 FROM device.host_type WHERE host_type_desc = 'ePort G5';
INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE (event_type_id, host_type_id, event_type_host_type_num) SELECT 2, host_type_id, 70 FROM device.host_type WHERE host_type_desc = 'ePort G5';
INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE (event_type_id, host_type_id, event_type_host_type_num) SELECT 1, host_type_id, 66 FROM device.host_type WHERE host_type_desc = 'ePort G4';
INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE (event_type_id, host_type_id, event_type_host_type_num) SELECT 2, host_type_id, 70 FROM device.host_type WHERE host_type_desc = 'ePort G4';
INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE (event_type_id, host_type_id, event_type_host_type_num) SELECT 1, host_type_id, 66 FROM device.host_type WHERE host_type_desc = 'RDP';
INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE (event_type_id, host_type_id, event_type_host_type_num) SELECT 2, host_type_id, 70 FROM device.host_type WHERE host_type_desc = 'RDP';



CREATE SEQUENCE DEVICE.SEQ_EVENT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE DEVICE.EVENT
  (
    EVENT_ID                  NUMBER(20,0),
    EVENT_TYPE_ID             NUMBER(20,0)    not null,
    EVENT_STATE_ID            NUMBER(20,0)    not null,
    EVENT_DEVICE_TRAN_CD      VARCHAR2(20),
    EVENT_GLOBAL_TRANS_CD     VARCHAR2(255),
    EVENT_START_TS            DATE            not null,
    EVENT_END_TS              DATE,
    EVENT_UPLOAD_COMPLETE_TS  DATE,
    HOST_ID                   NUMBER(20,0)    not null,
    CREATED_BY                VARCHAR2(30)    not null,
    CREATED_TS                DATE            not null,
    LAST_UPDATED_BY           VARCHAR2(30)    not null,
    LAST_UPDATED_TS           DATE            not null,
    CONSTRAINT FK_EVENT_EVENT_TYPE_ID foreign key(EVENT_TYPE_ID) references DEVICE.EVENT_TYPE(EVENT_TYPE_ID),
    CONSTRAINT FK_EVENT_EVENT_STATE_ID foreign key(EVENT_STATE_ID) references DEVICE.EVENT_STATE(EVENT_STATE_ID),
    CONSTRAINT FK_EVENT_HOST_ID foreign key(HOST_ID) references DEVICE.HOST(HOST_ID)
  )
  PARTITION BY RANGE (EVENT_ID)
  (
  PARTITION EVENT_1 VALUES LESS THAN (1000000),
  PARTITION EVENT_2 VALUES LESS THAN (2000000),
  PARTITION EVENT_3 VALUES LESS THAN (3000000)
  )
  TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX DEVICE.UDX_EVENT_EVENT_ID ON DEVICE.EVENT(EVENT_ID) TABLESPACE DEVICE_INDX LOCAL;
ALTER TABLE DEVICE.EVENT ADD CONSTRAINT PK_EVENT PRIMARY KEY (EVENT_ID) USING INDEX DEVICE.UDX_EVENT_EVENT_ID;
CREATE INDEX DEVICE.IDX_EVENT_GLOBAL_TRANS_CD ON DEVICE.EVENT(EVENT_GLOBAL_TRANS_CD) TABLESPACE DEVICE_INDX;
ALTER TABLE DEVICE.EVENT ADD CONSTRAINT UK_EVENT_EVENT_GLOBAL_TRANS_CD UNIQUE (EVENT_GLOBAL_TRANS_CD) USING INDEX DEVICE.IDX_EVENT_GLOBAL_TRANS_CD;
CREATE INDEX DEVICE.IDX_EVENT_HOST_ID ON DEVICE.EVENT(HOST_ID) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_EVENT_DEVICE_TRAN_CD ON DEVICE.EVENT(EVENT_DEVICE_TRAN_CD) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_EVENT_STATE_ID ON DEVICE.EVENT(EVENT_STATE_ID) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_EVENT_START_TS ON DEVICE.EVENT(EVENT_START_TS) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_EVENT_UPLOAD_COMPLETE_TS ON DEVICE.EVENT(EVENT_UPLOAD_COMPLETE_TS) TABLESPACE DEVICE_INDX LOCAL;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_EVENT BEFORE INSERT ON DEVICE.EVENT
  FOR EACH ROW 
BEGIN
	IF :NEW.event_id IS NULL
	THEN
		SELECT SEQ_EVENT_ID.NEXTVAL
		INTO :NEW.event_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_EVENT BEFORE UPDATE ON DEVICE.EVENT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

COMMENT ON TABLE DEVICE.EVENT is 'Represents logical association between asynchronously-collected (i.e. disjoint collection policy) data elements.';
COMMENT ON COLUMN DEVICE.EVENT.EVENT_DEVICE_TRAN_CD is 'The event id/code generated by the device.  The code will typically be unique for each client.  The code may be reset or start over causing duplicate order codes over time.';
COMMENT ON COLUMN DEVICE.EVENT.EVENT_GLOBAL_TRANS_CD is 'Globally unique event code, based on stateful characteristics of the device and event_device_tran_cd at the time of the event being reported to the srever.';
COMMENT ON COLUMN DEVICE.EVENT.EVENT_START_TS is 'Represents event start time, from the perspective of the reporting host.';
COMMENT ON COLUMN DEVICE.EVENT.EVENT_END_TS is 'Represents event end time, from the perspective of the reporting host.';
COMMENT ON COLUMN DEVICE.EVENT.EVENT_UPLOAD_COMPLETE_TS is 'Represents the time when all event details have been uploaded by a client.';
COMMENT ON COLUMN DEVICE.EVENT.HOST_ID is 'Host that reported the event.  Event details are allowed to reference other hosts.';

GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT TO WEB_USER, USAT_WEB_ROLE;



CREATE TABLE DEVICE.EVENT_DETAIL_TYPE
  (
    EVENT_DETAIL_TYPE_ID    NUMBER(20,0),
    EVENT_DETAIL_TYPE_NAME  VARCHAR2(60)    not null,
    EVENT_DETAIL_TYPE_DESC  VARCHAR2(255),
    CONSTRAINT PK_EVENT_DETAIL_TYPE primary key(EVENT_DETAIL_TYPE_ID)
  )
  TABLESPACE DEVICE_DATA;

COMMENT ON TABLE DEVICE.EVENT_DETAIL_TYPE is 'Represents business types for event details, as interpreted by server software when analyzing event content.  All client (device/host) level content shall be represented in separate entities.';

GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT_DETAIL_TYPE TO WEB_USER, USAT_WEB_ROLE;

INSERT INTO DEVICE.EVENT_DETAIL_TYPE (event_detail_type_id, event_detail_type_name, event_detail_type_desc) VALUES (1, 'Event Type Version', 'Version of Event Type reported by host');
INSERT INTO DEVICE.EVENT_DETAIL_TYPE (event_detail_type_id, event_detail_type_name, event_detail_type_desc) VALUES (2, 'Host Event Timestamp', 'Event date and time reported by host');



CREATE SEQUENCE DEVICE.SEQ_EVENT_DETAIL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE TABLE DEVICE.EVENT_DETAIL
  (
    EVENT_DETAIL_ID        NUMBER(20,0),
    EVENT_ID               NUMBER(20,0)     not null,
    EVENT_DETAIL_TYPE_ID   NUMBER(20,0)     not null,
    EVENT_DETAIL_VALUE     VARCHAR2(4000),
    EVENT_DETAIL_VALUE_TS  DATE,
    CREATED_BY             VARCHAR2(30)     not null,
    CREATED_TS             DATE             not null,
    LAST_UPDATED_BY        VARCHAR2(30)     not null,
    LAST_UPDATED_TS        DATE             not null,
    CONSTRAINT PK_EVENT_DETAIL primary key(EVENT_DETAIL_ID),
    CONSTRAINT FK_EVENT_DETAIL_EVENT_ID foreign key(EVENT_ID) references DEVICE.EVENT(EVENT_ID),
    CONSTRAINT FK_EVENT_DETAIL_ED_TYPE_ID foreign key(EVENT_DETAIL_TYPE_ID) references DEVICE.EVENT_DETAIL_TYPE(EVENT_DETAIL_TYPE_ID)
  )
  PARTITION BY RANGE (EVENT_ID)
  (
  PARTITION EVENT_DETAIL_1 VALUES LESS THAN (1000000),
  PARTITION EVENT_DETAIL_2 VALUES LESS THAN (2000000),
  PARTITION EVENT_DETAIL_3 VALUES LESS THAN (3000000)
  )  
  TABLESPACE DEVICE_DATA;
  
CREATE INDEX DEVICE.IDX_EVENT_DETAIL_EVENT_ID ON DEVICE.EVENT_DETAIL(EVENT_ID) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_EVENT_DETAIL_TYPE_VALUE ON DEVICE.EVENT_DETAIL(EVENT_DETAIL_TYPE_ID, EVENT_DETAIL_VALUE) TABLESPACE DEVICE_INDX LOCAL;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_EVENT_DETAIL BEFORE INSERT ON DEVICE.EVENT_DETAIL
  FOR EACH ROW 
BEGIN
	IF :NEW.event_detail_id IS NULL
	THEN
		SELECT SEQ_EVENT_DETAIL_ID.NEXTVAL
		INTO :NEW.event_detail_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_EVENT_DETAIL BEFORE UPDATE ON DEVICE.EVENT_DETAIL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

COMMENT ON TABLE DEVICE.EVENT_DETAIL is 'Represents specific event details, as interpreted by server software when analyzing event content.  All client (device/host) level content shall be represented in separate entities.';
COMMENT ON COLUMN DEVICE.EVENT_DETAIL.EVENT_DETAIL_VALUE_TS is 'Date/time of when event detail occured or was generated; may also represent content of the event detail (dependending on event_detail_type_id), if no event_detail_value is specified.';

GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT_DETAIL TO WEB_USER, USAT_WEB_ROLE;



CREATE TABLE DEVICE.EVENT_NL_DEVICE_SESSION
  (
    SESSION_ID       NUMBER(20,0),
    EVENT_ID         NUMBER(20,0),
    CREATED_BY       VARCHAR2(30)   not null,
    CREATED_TS       DATE           not null,
    LAST_UPDATED_BY  VARCHAR2(30)   not null,
    LAST_UPDATED_TS  DATE           not null,
    CONSTRAINT PK_EVENT_NL_DEVICE_SESSION primary key(SESSION_ID,EVENT_ID),
    CONSTRAINT FK_EVENT_NL_DEV_SESS_EVENT_ID foreign key(EVENT_ID) references DEVICE.EVENT(EVENT_ID)
  )
  PARTITION BY RANGE (EVENT_ID)
  (
  PARTITION EVENT_NL_DEVICE_SESSION_1 VALUES LESS THAN (1000000),
  PARTITION EVENT_NL_DEVICE_SESSION_2 VALUES LESS THAN (2000000),
  PARTITION EVENT_NL_DEVICE_SESSION_3 VALUES LESS THAN (3000000)
  )  
  TABLESPACE DEVICE_DATA;
  
CREATE INDEX DEVICE.IDX_EVENT_NL_DEV_SESS_EVT_ID ON DEVICE.EVENT_NL_DEVICE_SESSION(EVENT_ID) TABLESPACE DEVICE_INDX LOCAL;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_EVENT_NL_DEVICE_SESSION BEFORE INSERT ON DEVICE.EVENT_NL_DEVICE_SESSION
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_EVENT_NL_DEVICE_SESSION BEFORE UPDATE ON DEVICE.EVENT_NL_DEVICE_SESSION
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

COMMENT ON TABLE DEVICE.EVENT_NL_DEVICE_SESSION is 'Represents the physical session(s) that event content was received within.  Note: this table is primarily intended for tracking Fill Counters V1 (which requires session correlation), and is not intended for general purpose use.';
  
GRANT SELECT, INSERT, UPDATE ON DEVICE.EVENT_NL_DEVICE_SESSION TO WEB_USER, USAT_WEB_ROLE;



CREATE SEQUENCE DEVICE.SEQ_HOST_COUNTER_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_HOST_COUNTER BEFORE INSERT ON DEVICE.HOST_COUNTER
  FOR EACH ROW 
BEGIN
	IF :NEW.host_counter_id IS NULL
	THEN
		SELECT SEQ_HOST_COUNTER_ID.NEXTVAL
		INTO :NEW.host_counter_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_HOST_COUNTER BEFORE UPDATE ON DEVICE.HOST_COUNTER
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE INDEX DEVICE.IDX_HOST_COUNTER_HOST_ID_PARAM ON DEVICE.HOST_COUNTER(HOST_ID, HOST_COUNTER_PARAMETER) TABLESPACE DEVICE_INDX;

GRANT SELECT, INSERT, UPDATE ON DEVICE.HOST_COUNTER TO WEB_USER, USAT_WEB_ROLE;



CREATE TABLE DEVICE.HOST_COUNTER_EVENT
  (
    HOST_COUNTER_ID  NUMBER(20,0),
    EVENT_ID         NUMBER(20,0),
    CONSTRAINT PK_HOST_COUNTER_EVENT primary key(HOST_COUNTER_ID,EVENT_ID),
    CONSTRAINT FK_HOST_COUNTER_EVENT_EVENT_ID foreign key(EVENT_ID) references DEVICE.EVENT(EVENT_ID),
    CONSTRAINT FK_HOST_COUNTER_EVENT_HCID foreign key(HOST_COUNTER_ID) references DEVICE.HOST_COUNTER(HOST_COUNTER_ID)
  )
  PARTITION BY RANGE (EVENT_ID)
  (
  PARTITION HOST_COUNTER_EVENT_1 VALUES LESS THAN (1000000),
  PARTITION HOST_COUNTER_EVENT_2 VALUES LESS THAN (2000000),
  PARTITION HOST_COUNTER_EVENT_3 VALUES LESS THAN (3000000)
  )  
  TABLESPACE DEVICE_DATA;

COMMENT ON TABLE DEVICE.HOST_COUNTER_EVENT is 'Mapping table that identifies counters reported by hosts as they pertain to a specific event.  Note that a given counter may be associated with only one event.';

CREATE INDEX DEVICE.IDX_HOST_COUNTER_EVENT_EVT_ID ON DEVICE.HOST_COUNTER_EVENT(EVENT_ID) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_HOST_COUNTER_EVENT_HCID ON DEVICE.HOST_COUNTER_EVENT(HOST_COUNTER_ID) TABLESPACE DEVICE_INDX;
ALTER TABLE DEVICE.HOST_COUNTER_EVENT ADD CONSTRAINT UK_HOST_COUNTER_EVENT_HCID UNIQUE (HOST_COUNTER_ID) USING INDEX DEVICE.IDX_HOST_COUNTER_EVENT_HCID;
  
GRANT SELECT, INSERT, UPDATE ON DEVICE.HOST_COUNTER_EVENT TO WEB_USER, USAT_WEB_ROLE;


    
CREATE TABLE DEVICE.DEVICE_FILE_TRANSFER_EVENT
  (
    DEVICE_FILE_TRANSFER_ID  NUMBER(20,0),
    EVENT_ID                 NUMBER(20,0),
    CONSTRAINT PK_DEVICE_FILE_TRANSFER_EVENT primary key(DEVICE_FILE_TRANSFER_ID,EVENT_ID),
    CONSTRAINT FK_DEV_FILE_TRANS_EVENT_EV_ID foreign key(EVENT_ID) references DEVICE.EVENT(EVENT_ID),
    CONSTRAINT FK_DEV_FILE_TRANS_EVENT_DFTID foreign key(DEVICE_FILE_TRANSFER_ID) references DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID)
  )
  PARTITION BY RANGE (EVENT_ID)
  (
  PARTITION DEVICE_FILE_TRANSFER_EVENT_1 VALUES LESS THAN (1000000),
  PARTITION DEVICE_FILE_TRANSFER_EVENT_2 VALUES LESS THAN (2000000),
  PARTITION DEVICE_FILE_TRANSFER_EVENT_3 VALUES LESS THAN (3000000)
  )  
  TABLESPACE DEVICE_DATA;

COMMENT ON TABLE DEVICE.DEVICE_FILE_TRANSFER_EVENT is 'Mapping table that identifies file transfers asspciated with hosts as they pertain to a specific event.  Note that a given file transfer may be associated with only one event.';

CREATE INDEX DEVICE.IDX_DEV_FILE_TRANS_EVT_EVT_ID ON DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID) TABLESPACE DEVICE_INDX LOCAL;
CREATE INDEX DEVICE.IDX_DEV_FILE_TRANS_EVENT_DFTID ON DEVICE.DEVICE_FILE_TRANSFER_EVENT(DEVICE_FILE_TRANSFER_ID) TABLESPACE DEVICE_INDX;
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_EVENT ADD CONSTRAINT UK_DEV_FILE_TRANS_EVENT_DFTID UNIQUE (DEVICE_FILE_TRANSFER_ID) USING INDEX DEVICE.IDX_DEV_FILE_TRANS_EVENT_DFTID;

GRANT SELECT, INSERT, UPDATE ON DEVICE.DEVICE_FILE_TRANSFER_EVENT TO WEB_USER, USAT_WEB_ROLE;



ALTER TABLE device.device_counter RENAME TO device_counter_old;

CREATE OR REPLACE VIEW device.device_counter (device_counter_id,
                                              device_counter_value,
                                              device_id,
                                              device_counter_parameter,
                                              device_counter_ts,
                                              created_by,
                                              created_ts,
                                              last_updated_by,
                                              last_updated_ts,
                                              session_id
                                             )
AS
   SELECT hc.host_counter_id, hc.host_counter_value, h.device_id,
   	  hc.host_counter_parameter, hc.host_counter_ts,
          hc.created_by, hc.created_ts, hc.last_updated_by,
          hc.last_updated_ts, ends.session_id
     FROM device.host_counter hc, device.host h, device.host_counter_event hce, device.event_nl_device_session ends
    WHERE hc.host_id = h.host_id
    	AND hc.host_counter_id = hce.host_counter_id
    	AND hce.event_id = ends.event_id;

GRANT SELECT ON DEVICE.device_counter TO TRACKING, WEB_USER, USAT_WEB_ROLE;


COMMIT;
