-- compile device.device_counter dependents
DROP PUBLIC SYNONYM device_counter;
CREATE PUBLIC SYNONYM device_counter FOR device.device_counter;
ALTER PACKAGE tracking.pkg_reconciliation COMPILE BODY;

-- compile pss.blackbrd_authority dependents
ALTER FUNCTION pss.get_auth_name COMPILE;
ALTER FUNCTION pss.get_auth_service_type_id COMPILE;
ALTER PACKAGE pss.pkg_reports COMPILE BODY;
ALTER PROCEDURE pss.sp_nxt_blackbrd_sequence_num COMPILE;
ALTER TRIGGER pss.trbi_blackbrd_authority COMPILE;
ALTER TRIGGER pss.trbu_blackbrd_authority COMPILE;
ALTER VIEW pss.vw_esuds_tran_line_item_dtl COMPILE;
ALTER VIEW pss.vw_tot_sales_by_campus COMPILE;
ALTER VIEW pss.vw_tot_sales_by_dorm COMPILE;
ALTER VIEW pss.vw_tot_sales_by_laundry_room COMPILE;
ALTER VIEW pss.vw_tot_sales_by_school COMPILE;
ALTER VIEW pss.vw_total_sales_by_host COMPILE;
ALTER VIEW pss.vw_tran_summary COMPILE;
DROP PUBLIC SYNONYM blackbrd_authority;
CREATE PUBLIC SYNONYM blackbrd_authority FOR pss.blackbrd_authority;

-- compile pss.sp_nxt_blackbrd_sequence_num dependents
DROP PUBLIC SYNONYM sp_nxt_blackbrd_sequence_num;
CREATE PUBLIC SYNONYM sp_nxt_blackbrd_sequence_num FOR pss.sp_nxt_blackbrd_sequence_num;

-- compile other affected objects
alter function DASHBOARD.GET_AUTH_NAME compile;
alter trigger device.TRBI_DEVICE_COUNTER compile;
