CREATE OR REPLACE VIEW TAZDBA.VW_MC_NOTRANS
(SERIAL_NUM, LOCATION, LAST_CALL_IN, COMM_SOURCE)
AS 
SELECT d.device_serial_cd,
       l.location_name,
       cr.call_in_start_ts AS last_call_in,
       (CASE
           WHEN cr.modem_id IS NULL THEN cr.ip_address
           ELSE cr.modem_id
        END
       ) AS comm_source
  FROM device.device_call_in_record cr,
       LOCATION.customer c,
       LOCATION.LOCATION l,
       pss.pos p,
       device.device d
 WHERE d.device_serial_cd IN
          ('E4073013',
           'E4073015',
           'E4073023',
           'E4073025',
           'E4073026',
           'E4073030',
           'E4073031',
           'E4073032',
           'E4073756',
           'E4073759',
           'E4073760',
           'E4073761',
           'E4073763',
           'E4073764',
           'E4073765',
           'E4073811',
           'E4073813',
           'E4073840',
           'E4073851',
           'E4073852',
           'E4073853',
           'E4073854',
           'E4073855',
           'E4073856',
           'E4073857',
           'E4073859',
           'E4073860',
           'E4073861',
           'E4073862',
           'E4073863',
           'E4073864',
           'E4073914',
           'E4073958',
           'E4073959',
           'E4073960',
           'E4073961',
           'E4073962',
           'E4073963',
           'E4073964',
           'E4073965',
           'E4073966',
           'E4073967',
           'E4073976',
           'E4074048',
           'E4074049',
           'E4074050',
           'E4074052',
           'E4074053',
           'E4074054',
           'E4074056',
           'E4074057',
           'E4074058',
           'E4074069',
           'E4074188',
           'E4074255',
           'E4074258',
           'E4074296',
           'E4074299',
           'E4074308',
           'E4074309',
           'E4074599',
           'E4074781',
           'E4074782',
           'E4074786',
           'E4074790',
           'E4074791',
           'E4074792',
           'E4074793',
           'E4074794',
           'E4074795',
           'E4075370',
           'E4075372',
           'E4075373',
           'E4075375',
           'E4075376',
           'E4075379',
           'E4075380',
           'E4075381',
           'E4075382',
           'E4075383',
           'E4075384',
           'E4075389',
           'G5060285',
           'G5060290',
           'G5060291',
           'G5060298',
           'G5060380',
           'G5060385',
           'G5060389',
           'G5060391',
           'G5060392',
           'G5060393',
           'G5060396',
           'G5060398',
           'G5060402',
           'G5060403',
           'G5060408',
           'G5060409',
           'G5060411',
           'G5060412',
           'G5060414',
           'G5060415',
           'G5060417',
           'G5060419',
           'G5060420',
           'G5060435',
           'G5060468',
           'G5060523',
           'G5060524',
           'G5060529',
           'G5060535',
           'G5060537',
           'G5060539',
           'G5060543',
           'G5060544',
           'G5060545',
           'G5060549',
           'G5060557'
          )
   AND p.device_id = d.device_id
   AND c.customer_id = p.customer_id
   AND l.location_id = p.location_id
   AND UPPER (l.location_name) NOT IN ('UNKNOWN')
   AND cr.serial_number = d.device_serial_cd
   AND cr.call_in_start_ts =
          (SELECT MAX (call_in_start_ts)
             FROM device.device_call_in_record
            WHERE serial_number = d.device_serial_cd
              AND call_in_start_ts BETWEEN TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 8) )
                                       AND TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 1) ) )
   AND NOT EXISTS (
          SELECT created_ts
            FROM tazdba.transaction_record_hist
           WHERE transaction_date BETWEEN TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 8) )
                                      AND TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 1) )
             AND machine_id = d.device_name)
   AND NOT EXISTS (SELECT a2.created_ts
                     FROM pss.tran a2 INNER JOIN pss.pos_pta pta ON a2.pos_pta_id =
                                                                            pta.pos_pta_id INNER JOIN pss.pos p ON pta.pos_id =
                                                                                                                     p.pos_id INNER JOIN device.device d2 ON p.device_id =
                                                                                                                                                               d2.device_id
                    WHERE d2.device_serial_cd = d.device_serial_cd);
