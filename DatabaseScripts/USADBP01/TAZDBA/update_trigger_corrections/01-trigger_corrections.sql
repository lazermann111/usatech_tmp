ALTER TABLE tazdba.location RENAME COLUMN last_udpated_by TO last_updated_by;

-- Start of DDL Script for Trigger TAZDBA.TRBU_LOCATION
-- Generated 10-Jan-2005 16:38:46 from TAZDBA@USADBD03.USATECH.COM

-- Drop the old instance of TRBU_LOCATION
--DROP TRIGGER tazdba.trbu_location
--/

CREATE OR REPLACE TRIGGER tazdba.trbu_location
 BEFORE
  UPDATE
 ON tazdba.location
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger TAZDBA.TRBU_LOCATION

-- Start of DDL Script for Trigger TAZDBA.TRBU_MACHINE
-- Generated 10-Jan-2005 16:37:52 from TAZDBA@USADBD03.USATECH.COM

-- Drop the old instance of TRBU_MACHINE
--DROP TRIGGER tazdba.trbu_machine
--/

CREATE OR REPLACE TRIGGER tazdba.trbu_machine
 BEFORE
  UPDATE
 ON tazdba.machine
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger TAZDBA.TRBU_MACHINE

-- Start of DDL Script for Trigger TAZDBA.TRBU_MACHINE_HIERARCHY
-- Generated 10-Jan-2005 16:40:33 from TAZDBA@USADBD03.USATECH.COM

-- Drop the old instance of TRBU_MACHINE_HIERARCHY
--DROP TRIGGER tazdba.trbu_machine_hierarchy
--/

CREATE OR REPLACE TRIGGER tazdba.trbu_machine_hierarchy
 BEFORE
  UPDATE
 ON tazdba.machine_hierarchy
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger TAZDBA.TRBU_MACHINE_HIERARCHY

-- Start of DDL Script for Trigger TAZDBA.TRBU_TRANSACTION_FAILURES
-- Generated 10-Jan-2005 16:49:54 from TAZDBA@USADBD03.USATECH.COM

-- Drop the old instance of TRBU_TRANSACTION_FAILURES
--DROP TRIGGER tazdba.trbu_transaction_failures
--/

CREATE OR REPLACE TRIGGER tazdba.trbu_transaction_failures
 BEFORE
  UPDATE
 ON tazdba.transaction_failures
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger TAZDBA.TRBU_TRANSACTION_FAILURES

-- Start of DDL Script for Trigger TAZDBA.TRBU_TRANSACTION_RECORD_HIST
-- Generated 10-Jan-2005 16:42:00 from TAZDBA@USADBD03.USATECH.COM

-- Drop the old instance of TRBU_TRANSACTION_RECORD_HIST
--DROP TRIGGER tazdba.trbu_transaction_record_hist
--/

CREATE OR REPLACE TRIGGER tazdba.trbu_transaction_record_hist
 BEFORE
  UPDATE
 ON tazdba.transaction_record_hist
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger TAZDBA.TRBU_TRANSACTION_RECORD_HIST
