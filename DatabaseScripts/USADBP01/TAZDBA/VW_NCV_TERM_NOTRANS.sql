CREATE OR REPLACE VIEW TAZDBA.VW_NCV_TERM_NOTRANS
(SERIAL_NUM, LOCATION, LAST_CALL_IN, COMM_SOURCE)
AS 
(/* Formatted on 2006/04/11 10:23 (Formatter Plus v4.8.0) */
(SELECT d.device_serial_cd,
        l.location_name,
        cr.call_in_start_ts AS last_call_in,
        (CASE
            WHEN cr.modem_id IS NULL THEN cr.ip_address
            ELSE cr.modem_id
         END
        ) AS comm_source
   FROM device.device_call_in_record cr,
        LOCATION.customer c,
        LOCATION.LOCATION l,
        pss.pos p,
        device.device d
  WHERE c.customer_id = 29
    AND p.customer_id = c.customer_id
    AND l.location_id = p.location_id
    AND d.device_id = p.device_id
    AND d.device_serial_cd NOT IN
           ('E4072813',
            'E4072817',
            'E4072823',
            'E4072868',
            'E4072870',
            'E4072877',
            'E4072880',
            'E4072957',
            'E4072978',
            'E4072984',
            'E4072985',
            'E4072990',
            'E4072991',
            'E4073007',
            'E4073266',
            'E4073278',
            'E4073285',
            'E4073286',
            'E4073288',
            'E4073295',
            'E4073306',
            'E4073309',
            'E4073313',
            'E4073318',
            'E4073320',
            'E4073329',
            'E4073330',
            'E4073331',
            'E4073338',
            'E4073341',
            'E4073343',
            'E4073346',
            'E4073350',
            'E4073354',
            'E4073355',
            'E4073356',
            'E4073364',
            'E4073368',
            'E4073374',
            'E4073376',
            'E4073402',
            'E4073411',
            'E4073412',
            'E4073420',
            'E4073421',
            'E4073422',
            'E4073423',
            'E4073425',
            'E4073431',
            'E4073438',
            'E4073452',
            'E4073454',
            'E4073459',
            'E4073468',
            'E4073509',
            'E4073520',
            'E4073521',
            'E4073524',
            'E4073529',
            'E4073532',
            'E4074969',
            'E4073321'
           )
    AND cr.serial_number = d.device_serial_cd
    AND cr.call_in_start_ts =
           (SELECT MAX (call_in_start_ts)
              FROM device.device_call_in_record
             WHERE serial_number = d.device_serial_cd
               AND call_in_start_ts BETWEEN TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 8) )
                                        AND TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 1) ) )
    --AND call_in_start_ts BETWEEN '07-Feb-2005' AND '14-Feb-2005')
    AND NOT EXISTS (
           SELECT t.machine_id
             FROM tazdba.transaction_record_hist t
            --WHERE t.transaction_date BETWEEN '07-Feb-2005' AND '14-Feb-2005'
           WHERE  t.transaction_date BETWEEN TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 8) )
                                         AND TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 1) )
              AND t.machine_id = d.device_name)
    AND NOT EXISTS (SELECT a2.created_ts
                      FROM pss.tran a2 INNER JOIN pss.pos_pta pta ON a2.pos_pta_id =
                                                                            pta.pos_pta_id INNER JOIN pss.pos p ON pta.pos_id =
                                                                                                                     p.pos_id INNER JOIN device.device d2 ON p.device_id =
                                                                                                                                                               d2.device_id
                     WHERE d2.device_serial_cd = d.device_serial_cd) )
);
