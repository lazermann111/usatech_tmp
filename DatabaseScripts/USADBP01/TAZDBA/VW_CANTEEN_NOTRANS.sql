CREATE OR REPLACE VIEW TAZDBA.VW_CANTEEN_NOTRANS
(SERIAL_NUM, LOCATION, LAST_CALL_IN, COMM_SOURCE)
AS 
(/* Formatted on 2006/04/11 10:13 (Formatter Plus v4.8.0) */
(SELECT d.device_serial_cd,
        l.location_name,
        cr.call_in_start_ts AS last_call_in,
        (CASE
            WHEN cr.modem_id IS NULL THEN cr.ip_address
            ELSE cr.modem_id
         END
        ) AS comm_source
   FROM device.device_call_in_record cr,
        LOCATION.customer c,
        LOCATION.LOCATION l,
        pss.pos p,
        device.device d
  WHERE c.customer_id = 7
    AND p.customer_id = c.customer_id
    AND l.location_id = p.location_id
    AND UPPER (l.location_name) NOT IN ('UNKNOWN')
    AND d.device_id = p.device_id
    AND cr.serial_number = d.device_serial_cd
    AND cr.call_in_start_ts =
           (SELECT MAX (call_in_start_ts)
              FROM device.device_call_in_record
             WHERE serial_number = d.device_serial_cd
               AND call_in_start_ts BETWEEN TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 8) )
                                        AND TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 1) ) )
    AND NOT EXISTS (
           SELECT *
             FROM tazdba.transaction_record_hist t
            WHERE t.transaction_date BETWEEN TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 8) )
                                         AND TO_DATE (TO_CHAR (TRUNC (SYSDATE) - 1) )
              AND t.machine_id = d.device_name)
    AND NOT EXISTS (SELECT a2.created_ts
                      FROM pss.tran a2 INNER JOIN pss.pos_pta pta ON a2.pos_pta_id =
                                                                            pta.pos_pta_id INNER JOIN pss.pos p ON pta.pos_id =
                                                                                                                     p.pos_id INNER JOIN device.device d2 ON p.device_id =
                                                                                                                                                               d2.device_id
                     WHERE d2.device_serial_cd = d.device_serial_cd) )
);
