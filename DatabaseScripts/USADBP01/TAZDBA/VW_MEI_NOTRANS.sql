CREATE OR REPLACE VIEW TAZDBA.VW_MEI_NOTRANS
(SERIAL_NUM, LAST_CALL_IN, COMM_SOURCE)
AS 
SELECT d.device_serial_cd,
       cr.call_in_start_ts AS last_call_in,
       (CASE
           WHEN cr.modem_id IS NULL THEN cr.ip_address
           ELSE cr.modem_id
        END
       ) AS comm_source
  FROM device.device_call_in_record cr,
       device.device d
 WHERE 
      d.device_serial_cd like 'M10%'
     AND d.device_serial_cd not like 'M199%'
     And d.device_serial_cd not in ('M1001513',  'M1004325', 'M1004319' , 'M1004333' , 'M1004461' , 'M1004318' , 'M1002444' , 'M1001509' , 'M1004327' , 'M1004605' , 'M1004462' , 'M1004249' , 'M1004245', 'M1004240' , 'M1001585' , 'M1005982' , 'M1005985' ,  'M1004453')


   AND cr.call_in_start_ts =
          (SELECT MAX (call_in_start_ts)
             FROM device.device_call_in_record
            WHERE serial_number = d.device_serial_cd
            AND call_in_start_ts between (sysdate - 1) and sysdate)
          
   AND NOT EXISTS (
          SELECT *
            FROM tazdba.transaction_record_hist t
            WHERE t.transaction_date between (sysdate - 1) and sysdate
             AND t.machine_id = d.device_name)
                 AND NOT EXISTS (SELECT a2.created_ts
                      FROM pss.tran a2 INNER JOIN pss.pos_pta pta ON a2.pos_pta_id =
                                                                            pta.pos_pta_id INNER JOIN pss.pos p ON pta.pos_id =
                                                                                                                     p.pos_id INNER JOIN device.device d2 ON p.device_id =
                                                                                                                                                               d2.device_id
                     WHERE d2.device_serial_cd = d.device_serial_cd);
