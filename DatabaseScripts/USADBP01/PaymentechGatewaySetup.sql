INSERT INTO authority.handler(handler_name, handler_class)
VALUES('Paymentech NetConnect', 'Authority::FHMS::ISO8583');

INSERT INTO authority.authority_type(authority_type_name, authority_type_desc, handler_id)
SELECT 'Paymentech', 'Chase Paymentech', handler_id FROM authority.handler WHERE handler_name = 'Paymentech NetConnect';

INSERT INTO authority.authority(authority_name, authority_type_id, authority_service_id)
SELECT 'Paymentech', authority_type_id, 1 FROM authority.authority_type WHERE authority_type_name = 'Paymentech';

-- Replace {App Server N IP Address} with the IP address of the appropriate application server in single quotes.
INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'Paymentech Gateway 1', {App Server 1 IP Address}, 9100, 1, handler_id FROM authority.handler WHERE handler_name = 'Paymentech NetConnect';

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'Paymentech Gateway 2', {App Server 2 IP Address}, 9100, 2, handler_id FROM authority.handler WHERE handler_name = 'Paymentech NetConnect';

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'Paymentech Production Server 1', '127.0.0.1', 1, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Paymentech Gateway 1';

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'Paymentech Production Server 2', '127.0.0.1', 2, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Paymentech Gateway 2';

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
VALUES(
    (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'Paymentech'),
    (SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Paymentech Gateway 1')
);

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
VALUES(
    (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'Paymentech'),
    (SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Paymentech Gateway 2')
);

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'Credit Card (Track 2) - Paymentech', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'C', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_desc = 'Credit Card (Track 2)';

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'RFID Credit Card (Track 2) - Paymentech', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'R', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_desc = 'Credit Card (Track 2)';

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'Credit Card (Track 1) - Paymentech', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'C', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_desc = 'Credit Card (Track 1)';

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'RFID Credit Card (Track 1) - Paymentech', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'R', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_desc = 'Credit Card (Track 1)';

-- For merchant and terminal records below replace the items in curly brackets {} with their values in single quotes. If Merchant ID or Terminal ID are not available, these records will need to be created later.
INSERT INTO pss.merchant(merchant_cd, merchant_name, merchant_desc, merchant_bus_name, authority_id)
SELECT {Merchant ID}, {Merchant Name}, {Merchant Description}, 'USA Technologies, Inc.', authority_id FROM authority.authority WHERE authority_name = 'Paymentech';

INSERT INTO pss.terminal(terminal_cd, terminal_next_batch_num, merchant_id, terminal_desc, terminal_state_id, terminal_max_batch_num, terminal_batch_max_tran, terminal_batch_cycle_num, terminal_min_batch_num, terminal_min_batch_close_hr, terminal_max_batch_close_hr)
SELECT {Terminal ID}, 1, merchant_id, {Terminal Description}, 1, 999, 950, 1, 850, 0, 1 FROM pss.merchant WHERE merchant_cd = {Merchant ID} AND authority_id = (SELECT authority_id FROM authority.authority WHERE authority_name = 'Paymentech');