DECLARE
    ln_increment PLS_INTEGER := 10000;
    ln_min_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_max_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_start_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_min_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_max_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_start_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_min_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_max_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_start_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_cnt PLS_INTEGER;
    ld_cutoff DATE := SYSDATE - 7;
BEGIN
    SELECT MIN(TRAN_ID), MAX(TRAN_ID)
      INTO ln_min_tran_id, ln_max_tran_id
      FROM PSS.TRAN;
    ln_start_tran_id := ln_min_tran_id;
    WHILE ln_start_tran_id <  ln_max_tran_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating TRAN table from TRAN_ID ' || ln_start_tran_id || ' to ' || (ln_start_tran_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
		INSERT INTO PSS.TRAN_C(
			TRAN_ID, 
            KID, 
            HASH_TYPE_CD, 
            TRAN_PARSED_ACCT_NUM_H) 
		SELECT 
			TRAN_ID,
            -1,
            'SHA1',
            DBADMIN.HASH_CARD(TRAN_PARSED_ACCT_NUM)
		FROM PSS.TRAN T
        WHERE TRAN_ID >= ln_start_tran_id AND TRAN_ID < ln_start_tran_id + ln_increment
			AND REGEXP_LIKE(TRAN_PARSED_ACCT_NUM, '[0-9]{13,}')
			AND CREATED_TS < ld_cutoff
			AND NOT EXISTS (SELECT 1 FROM PSS.TRAN_C WHERE TRAN_ID = T.TRAN_ID);
        UPDATE PSS.TRAN
           SET TRAN_PARSED_ACCT_NUM = NULL,
               TRAN_PARSED_ACCT_NAME = NULL,
               TRAN_PARSED_ACCT_EXP_DATE = NULL,
               TRAN_STATE_CD = DECODE(TRAN_STATE_CD, 
                    '0', 'M',
                    '1', 'E', 
                    '2', 'E',
                    '3', 'E',
                    '4', 'E',
                    '6', 'L',
                    '8', 'E',
                    '9', 'E',
                    'A', 'E',
                    'B', 'E',
                    'I', 'E',
                    'J', 'E',                    
                    'N', 'E',
                    'P', 'E',
                    'Q', 'E',
                    'R', 'E',
                    'S', 'E',
                    'T', 'E',
                    TRAN_STATE_CD)
         WHERE TRAN_ID >= ln_start_tran_id AND TRAN_ID < ln_start_tran_id + ln_increment
           AND (REGEXP_LIKE(TRAN_PARSED_ACCT_NUM, '[0-9]{13,}')
               OR TRAN_PARSED_ACCT_NAME IS NOT NULL
               OR TRAN_PARSED_ACCT_EXP_DATE IS NOT NULL)
           AND CREATED_TS < ld_cutoff;
        ln_cnt := SQL%ROWCOUNT;   
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on TRAN table from TRAN_ID ' || ln_start_tran_id || ' to ' || (ln_start_tran_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_tran_id := ln_start_tran_id + ln_increment; 
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('TRAN Update complete');   
     
    SELECT MIN(AUTH_ID), MAX(AUTH_ID)
      INTO ln_min_auth_id, ln_max_auth_id
      FROM PSS.AUTH;
    ln_start_auth_id := ln_min_auth_id;
    WHILE ln_start_auth_id <  ln_max_auth_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating AUTH table from AUTH_ID ' || ln_start_auth_id || ' to ' || (ln_start_auth_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        UPDATE PSS.AUTH
           SET AUTH_PARSED_ACCT_DATA = DBADMIN.MASK_CREDIT_CARD(AUTH_PARSED_ACCT_DATA)
         WHERE AUTH_ID >= ln_start_auth_id AND AUTH_ID < ln_start_auth_id + ln_increment
           AND REGEXP_LIKE(AUTH_PARSED_ACCT_DATA, '[0-9]{13,}')
           AND CREATED_TS < ld_cutoff;
        ln_cnt := SQL%ROWCOUNT;   
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on AUTH table from AUTH_ID ' || ln_start_auth_id || ' to ' || (ln_start_auth_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_auth_id := ln_start_auth_id + ln_increment; 
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('AUTH Update complete');    

    SELECT MIN(REFUND_ID), MAX(REFUND_ID)
      INTO ln_min_refund_id, ln_max_refund_id
      FROM PSS.REFUND;
    ln_start_refund_id := ln_min_refund_id;
    WHILE ln_start_refund_id <  ln_max_refund_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating REFUND table from REFUND_ID ' || ln_start_refund_id || ' to ' || (ln_start_refund_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        UPDATE PSS.REFUND
           SET REFUND_PARSED_ACCT_DATA = DBADMIN.MASK_CREDIT_CARD(REFUND_PARSED_ACCT_DATA)
         WHERE REFUND_ID >= ln_start_refund_id AND REFUND_ID < ln_start_refund_id + ln_increment
           AND REGEXP_LIKE(REFUND_PARSED_ACCT_DATA, '[0-9]{13,}')
           AND CREATED_TS < ld_cutoff;
        ln_cnt := SQL%ROWCOUNT;   
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on REFUND table from REFUND_ID ' || ln_start_refund_id || ' to ' || (ln_start_refund_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_refund_id := ln_start_refund_id + ln_increment;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('REFUND Update complete');    
END;
/
