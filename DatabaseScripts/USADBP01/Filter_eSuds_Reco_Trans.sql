DECLARE
    ln_min_tran_id PSS.MV_ESUDS_SETTLED_TRAN.TRAN_ID%TYPE;
    ln_max_tran_id PSS.MV_ESUDS_SETTLED_TRAN.TRAN_ID%TYPE;
    ln_batch_size PLS_INTEGER := 10000000;
BEGIN
    SELECT MIN(TRAN_ID), MAX(TRAN_ID)
      INTO ln_min_tran_id, ln_max_tran_id
      FROM PSS.MV_ESUDS_SETTLED_TRAN;
    WHILE ln_min_tran_id < ln_max_tran_id LOOP
        DELETE
          FROM PSS.MV_ESUDS_TRAN_LINE_ITEM
         WHERE TRAN_ID IN(SELECT TRAN_ID FROM PSS.MV_ESUDS_SETTLED_TRAN WHERE TRAN_STATE_CD = 'Z' AND TRAN_ID BETWEEN ln_min_tran_id AND ln_min_tran_id + ln_batch_size);
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Deleted ' || SQL%ROWCOUNT || ' rows from eSuds line item table between tran id ' || ln_min_tran_id || ' and ' || (ln_min_tran_id + ln_batch_size));
        
        DELETE
          FROM PSS.MV_ESUDS_SETTLED_TRAN
         WHERE TRAN_STATE_CD = 'Z'
           AND TRAN_ID BETWEEN ln_min_tran_id AND ln_min_tran_id + ln_batch_size;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Deleted ' || SQL%ROWCOUNT || ' rows from eSuds tran table between tran id ' || ln_min_tran_id || ' and ' || (ln_min_tran_id + ln_batch_size));
        ln_min_tran_id := ln_min_tran_id + ln_batch_size;
    END LOOP;
END;
/
