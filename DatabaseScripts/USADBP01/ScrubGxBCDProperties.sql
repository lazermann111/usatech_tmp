/*
-- Needs NUMERIC regex and check
SELECT cts.DEVICE_SETTING_PARAMETER_CD PROPERTY_NAME,
       cts.CONVERTER,
       cts.FIELD_SIZE,
       cts.FIELD_OFFSET,
       --CASE WHEN cts.FIELD_SIZE + cts.FIELD_OFFSET NEXT_FIELD_OFFSET = 
       LAG(cts.FIELD_SIZE + cts.FIELD_OFFSET, 1) OVER (ORDER BY CTS.FIELD_OFFSET) PREV_FIELD_END,
       DECODE(cts.FIELD_OFFSET, LAG(cts.FIELD_SIZE + cts.FIELD_OFFSET, 1) OVER (ORDER BY CTS.FIELD_OFFSET), 'Y', 'N') CONT,
       DECODE(cts.FIELD_OFFSET, LAG(cts.FIELD_SIZE + cts.FIELD_OFFSET, 1) OVER (ORDER BY CTS.FIELD_OFFSET), NULL, cts.FIELD_OFFSET) FROM_OFFSET,
       DECODE(cts.FIELD_OFFSET, LAG(cts.FIELD_SIZE + cts.FIELD_OFFSET, 1) OVER (ORDER BY CTS.FIELD_OFFSET), NULL, 1 - LAG(cts.FIELD_SIZE + cts.FIELD_OFFSET, 1) OVER (ORDER BY CTS.FIELD_OFFSET)) TO_OFFSET,
       cts.PAD_CHAR,
       cts.DATA_MODE, 
       cts.DATA_MODE_AUX,
       regex.REGEX_NAME,
       LPAD(NVL(cts.CONFIG_TEMPLATE_SETTING_VALUE, CTS.PAD_CHAR), CTS.FIELD_SIZE * 2, CTS.PAD_CHAR)
 FROM DEVICE.CONFIG_TEMPLATE_SETTING cts
      LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_REGEX regex 
        ON cts.regex_id = regex.regex_id
where cts.device_type_id IN(0,1) -- DECODE(d.device_type_id, 1, 0, d.device_type_id)
  and cts.active = 'Y'
  and cts.DEVICE_SETTING_PARAMETER_CD IN(
--settings (12)
'USALive Phone In Time',
'USALive Retry Interval',
'USALive Day-Cycle Interval',
'Transaction Auto Time-Out',
'After Use Auto Time-Out',
'Maximum Vend Items Per Transaction',
'Day Before Terminal OUT Of Service',
'MDB Interval Speed Setting',
'MDB Response Setting',
'USALive Retry Attempts',
'Card Authorization Amount',
'Convenience Fee',
-- counters (9)
'Total Cashless Money Counter',
'Total Cashless Transaction Counter',
'Total Currency Money Counter',
'Total Currency Transaction Counter',
'Total Pass Card Money Counter',
'Total Pass Card Transaction Counter',
'Total Stitch Bytes Counter',
'Total Stitch Sessions Attepted Counter',
'Total Stitch Transaction Counter')
order by FIELD_OFFSET;
*/

-- Update regex
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING CTS
   SET REGEX_ID = (SELECT MIN(REGEX_ID) FROM DEVICE.CONFIG_TEMPLATE_REGEX WHERE REGEX_NAME = 'NUMERIC')
 WHERE NVL(CTS.REGEX_ID, 0) != (SELECT MIN(REGEX_ID) FROM DEVICE.CONFIG_TEMPLATE_REGEX WHERE REGEX_NAME = 'NUMERIC')
   AND CTS.DEVICE_TYPE_ID IN(0, 1)
   AND CTS.ACTIVE = 'Y'
   AND CTS.DEVICE_SETTING_PARAMETER_CD IN(
--settings (12)
'USALive Phone In Time',
'USALive Retry Interval',
'USALive Day-Cycle Interval',
'Transaction Auto Time-Out',
'After Use Auto Time-Out',
'Maximum Vend Items Per Transaction',
'Day Before Terminal OUT Of Service',
'MDB Interval Speed Setting',
'MDB Response Setting',
'USALive Retry Attempts',
'Card Authorization Amount',
'Convenience Fee',
-- counters (9)
'Total Cashless Money Counter',
'Total Cashless Transaction Counter',
'Total Currency Money Counter',
'Total Currency Transaction Counter',
'Total Pass Card Money Counter',
'Total Pass Card Transaction Counter',
'Total Stitch Bytes Counter',
'Total Stitch Sessions Attepted Counter',
'Total Stitch Transaction Counter');

COMMIT;

DECLARE
    CURSOR l_cur IS
SELECT FILE_CONTENT_SUBSTR(FT.ROWID, CTS.FIELD_OFFSET, CTS.FIELD_SIZE) CORRUPTED_VALUE, 
       LPAD(NVL(cts.CONFIG_TEMPLATE_SETTING_VALUE, CTS.PAD_CHAR), CTS.FIELD_SIZE * 2, CTS.PAD_CHAR) DEFAULT_VALUE, 
       CTS.FIELD_OFFSET, CTS.FIELD_SIZE, FT.FILE_TRANSFER_CONTENT, 
       FT.FILE_TRANSFER_ID 
FROM DEVICE.FILE_TRANSFER FT 
JOIN DEVICE.DEVICE D ON FT.FILE_TRANSFER_NAME = D.DEVICE_NAME || '-CFG'
CROSS JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
/*(SELECT 0 START_OFFSET, 0 END_OFFSET FROM DUAL WHERE 1=0
UNION ALL SELECT 172, 175 FROM DUAL
UNION ALL SELECT 194 , 195 FROM DUAL
UNION ALL SELECT 204 , 207 FROM DUAL
UNION ALL SELECT 320 , 357 FROM DUAL
UNION ALL SELECT 362 , 363 FROM DUAL
UNION ALL SELECT 368 , 369 FROM DUAL
) o*/
WHERE FT.FILE_TRANSFER_NAME LIKE 'EV%-CFG' 
  AND FT.FILE_TRANSFER_TYPE_CD = 1
  AND D.DEVICE_TYPE_ID = 1
  AND NOT REGEXP_LIKE(FILE_CONTENT_SUBSTR(FT.ROWID, CTS.FIELD_OFFSET, CTS.FIELD_SIZE), '^[0-9]*$')
  AND CTS.DEVICE_TYPE_ID IN(0, 1)
  AND CTS.ACTIVE = 'Y'
  AND CTS.DEVICE_SETTING_PARAMETER_CD IN(
--settings (12)
'USALive Phone In Time',
'USALive Retry Interval',
'USALive Day-Cycle Interval',
'Transaction Auto Time-Out',
'After Use Auto Time-Out',
'Maximum Vend Items Per Transaction',
'Day Before Terminal OUT Of Service',
'MDB Interval Speed Setting',
'MDB Response Setting',
'USALive Retry Attempts',
'Card Authorization Amount',
'Convenience Fee',
-- counters (9)
'Total Cashless Money Counter',
'Total Cashless Transaction Counter',
'Total Currency Money Counter',
'Total Currency Transaction Counter',
'Total Pass Card Money Counter',
'Total Pass Card Transaction Counter',
'Total Stitch Bytes Counter',
'Total Stitch Sessions Attepted Counter',
'Total Stitch Transaction Counter')
ORDER BY FILE_TRANSFER_ID;
    ll_result LONG;
    ll_orig LONG;
    ln_prev_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
BEGIN
    FOR l_rec IN l_cur LOOP
        IF ln_prev_file_transfer_id IS NULL THEN
            ll_result := l_rec.FILE_TRANSFER_CONTENT;
            ll_orig := l_rec.FILE_TRANSFER_CONTENT;
        ELSIF ln_prev_file_transfer_id != l_rec.FILE_TRANSFER_ID THEN
            DBMS_OUTPUT.PUT_LINE('Updating File Transfer Content for File Transfer id ' ||ln_prev_file_transfer_id||' from:');
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 1, 200));
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 201, 200));
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 401, 200));           
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 601, 200));           
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 801, 200));           
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 1001, 200));           
            DBMS_OUTPUT.PUT_LINE('To:');
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 1, 200));
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 201, 200));
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 401, 200));           
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 601, 200));           
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 801, 200));           
            DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 1001, 200));           
            --/*
            UPDATE DEVICE.FILE_TRANSFER
               SET FILE_TRANSFER_CONTENT = ll_result
             WHERE FILE_TRANSFER_ID = ln_prev_file_transfer_id;
            COMMIT; 
             --*/
            ll_result := l_rec.FILE_TRANSFER_CONTENT;
            ll_orig := l_rec.FILE_TRANSFER_CONTENT;
        END IF;
        ln_prev_file_transfer_id := l_rec.FILE_TRANSFER_ID;
        ll_result := SUBSTR(ll_result, 1, l_rec.FIELD_OFFSET * 2) || l_rec.DEFAULT_VALUE || SUBSTR(ll_result, 1 + ((l_rec.FIELD_OFFSET + l_rec.FIELD_SIZE) * 2));
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Updating File Transfer Content for File Transfer id ' ||ln_prev_file_transfer_id||' from:');
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 1, 200));
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 201, 200));
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 401, 200));           
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 601, 200));           
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 801, 200));           
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_orig, 1001, 200));           
    DBMS_OUTPUT.PUT_LINE('To:');
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 1, 200));
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 201, 200));
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 401, 200));           
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 601, 200));           
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 801, 200));           
    DBMS_OUTPUT.PUT_LINE(SUBSTR(ll_result, 1001, 200));           
    --/*
    UPDATE DEVICE.FILE_TRANSFER
       SET FILE_TRANSFER_CONTENT = ll_result
     WHERE FILE_TRANSFER_ID = ln_prev_file_transfer_id;
    COMMIT;
     --*/ 
END;
/
