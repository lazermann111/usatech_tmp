select d.device_serial_cd, count(1) from pss.auth a join pss.tran t on a.tran_id = t.tran_id
join pss.pos_pta pp on t.pos_pta_id = pp.pos_pta_id join pss.pos p on pp.pos_id = p.pos_id
join device.device d on p.device_id = d.device_id
where a.auth_ts > sysdate - 1 and a.auth_type_cd = 'L' and d.device_type_id in (0, 1)
group by d.device_serial_cd order by count(1) desc, d.device_serial_cd