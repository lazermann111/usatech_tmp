CREATE TABLE PSS.TERMINAL_GROUP
(
	TERMINAL_GROUP_ID NUMBER,
	TERMINAL_GROUP_NAME VARCHAR2(60) NOT NULL,
	CONSTRAINT PK_TERMINAL_GROUP_ID PRIMARY KEY(TERMINAL_GROUP_ID)
)
TABLESPACE PSS_DATA;

GRANT SELECT ON PSS.TERMINAL_GROUP TO USAT_WEB_ROLE, USAT_DEV_READ_ONLY;

INSERT INTO PSS.TERMINAL_GROUP(TERMINAL_GROUP_ID, TERMINAL_GROUP_NAME) VALUES(1, 'POSM Terminal Group 1');
INSERT INTO PSS.TERMINAL_GROUP(TERMINAL_GROUP_ID, TERMINAL_GROUP_NAME) VALUES(2, 'POSM Terminal Group 2');
INSERT INTO PSS.TERMINAL_GROUP(TERMINAL_GROUP_ID, TERMINAL_GROUP_NAME) VALUES(3, 'POSM Terminal Group 3');
COMMIT;

ALTER TABLE PSS.TERMINAL ADD TERMINAL_GROUP_ID NUMBER;

DECLARE
	CURSOR L_CUR IS SELECT TERMINAL_ID FROM PSS.TERMINAL ORDER BY TERMINAL_ID;
	L_REC L_CUR%ROWTYPE;
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE PSS.TERMINAL SET TERMINAL_GROUP_ID = PSS.SEQ_TERMINAL_GROUP_ID.NEXTVAL WHERE TERMINAL_ID = L_REC.TERMINAL_ID;
	END LOOP;
	COMMIT;
END;
/

ALTER TABLE PSS.TERMINAL MODIFY(TERMINAL_GROUP_ID NUMBER NOT NULL);
ALTER TABLE PSS.TERMINAL ADD CONSTRAINT FK_TERMINAL_TERMINAL_GROUP_ID FOREIGN KEY(TERMINAL_GROUP_ID) REFERENCES PSS.TERMINAL_GROUP(TERMINAL_GROUP_ID);
