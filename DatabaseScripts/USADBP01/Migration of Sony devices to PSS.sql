--import locations
insert into location.location
(
    location_name, 
    location_addr1,
    location_addr2, 
    location_city, 
    location_county,
    location_postal_cd,
    location_country_cd,
    location_type_id, 
    location_state_cd,
    location_time_zone_cd
)
select distinct
    l.location_name,
    substr(l.street_address_1, 1, 60),
    substr(l.street_address_2, 1, 60),
    substr(l.city, 1, 28),
    substr(l.county, 1, 28),
    l.zip,
    'US',
    3,
    DECODE(l.state, 'XX', NULL, state),
    l.time_zone
from tazdba.location l, location.customer c
where c.customer_type_id = 4 and l.customer_id = c.customer_id
and not exists (select 1 from location.location where location_name = l.location_name)
/

--import devices
declare l_seed number;
begin

L_SEED := USERENV('SESSIONID');
IF L_SEED > 999999999 THEN
   	L_SEED := TO_NUMBER(SUBSTR(TO_CHAR(L_SEED), LENGTH(TO_CHAR(L_SEED)) - 8, 9));
END IF;

DBMS_RANDOM.INITIALIZE (L_SEED);       

insert into device.device(device_name, device_serial_cd, device_type_id, device_active_yn_flag, encryption_key)
select distinct m.machine_id, nvl(m.vm_serial_number, ' '), 4, decode(upper(m.in_use), 'Y', 'Y', 'N'), TRUNC(DBMS_RANDOM.VALUE(1000000000000000, 9999999999999999)) 
from tazdba.machine m, tazdba.location l, location.customer c 
where c.customer_type_id = 4 and l.customer_id = c.customer_id and m.location_number = l.location_number
and not exists (select 1 from device.device where device_name = m.machine_id);

DBMS_RANDOM.TERMINATE;

end;
/

--create POS records
insert into pss.pos(location_id, customer_id, device_id, pos_active_yn_flag)
select distinct pl.location_id, c.customer_id, d.device_id, 'Y'
from device.device d, tazdba.machine m, tazdba.location l, location.customer c, location.location pl
where d.device_type_id = 4 and d.device_name = m.machine_id and m.location_number = l.location_number
and l.customer_id = c.customer_id and c.customer_type_id = 4 and pl.location_name = l.location_name
and not exists (select 1 from pss.pos where device_id = d.device_id)
/

-- SONY Views

-- Start of DDL Script for View SONY.VW_MONTHLY_EFT_BY_TERMINAL
-- Generated 3/4/2005 5:32:27 PM from SONY@USADBD03

CREATE OR REPLACE VIEW sony.vw_monthly_eft_by_terminal (
   cust_name,
   location,
   machine,
   trangrossamt,
   trangrosscnt )
AS
(/* Formatted on 2004/11/02 12:13 (Formatter Plus v4.8.0) */
SELECT   cust_name,
         LOCATION,
         machine,
         SUM (tran_amount) AS trangrossamt,
         SUM (tranid) AS trangrosscnt
    FROM (SELECT *
            FROM (SELECT replace(c.customer_name,',',' ') AS cust_name,
                         replace(l.location_name,',',' ') AS LOCATION,
                         a.machine_id AS machine,
                         sum(a.transaction_amount) AS tran_amount,
                         count(a.trans_no) AS tranid
                    FROM tazdba.transaction_record_hist a,
                         location.location l,
                         device.device d,
                         location.customer c,
                         pss.pos p
                   WHERE d.device_type_id = 4 -- Sony PictureStation
                     AND p.device_id = d.device_id
                     AND p.customer_id = c.customer_id
                     AND p.location_id = l.location_id
                    /*
                     AND a.transaction_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                     AND a.transaction_date <
                              LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                            + 1
                            - (1 / 24 / 60 / 60)
                            */
                     AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                     AND a.created_ts <
                              LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                            + 1
                            - (1 / 24 / 60 / 60)
                     AND a.machine_id = d.device_name (+)
                     group by c.customer_name,
                         l.location_name,
                         a.machine_id
                  UNION
                  SELECT replace(c.customer_name,',',' ') AS cust_name,
                         replace(l.location_name,',',' ') AS LOCATION,
                         d.device_name AS machine,
                         0 AS tran_amount,
                         0 AS tranid
                    FROM location.location l,
                         device.device d,
                         location.customer c,
                         pss.pos p
                   WHERE d.device_type_id = 4 -- Sony PictureStation
                        AND p.device_id = d.device_id
                        AND p.customer_id = c.customer_id
                        AND p.location_id = l.location_id) )
GROUP BY cust_name,
         LOCATION,
         machine
)
/


-- End of DDL Script for View SONY.VW_MONTHLY_EFT_BY_TERMINAL

-- Start of DDL Script for View SONY.VW_MONTHLY_EFT_BY_TRAN_TYPE
-- Generated 3/4/2005 5:32:41 PM from SONY@USADBD03

CREATE OR REPLACE VIEW sony.vw_monthly_eft_by_tran_type (
   customer,
   machine_id,
   location,
   card_type,
   tran_amount,
   tran_number_fee,
   processing_fee )
AS
(/* Formatted on 2005/02/01 10:39 (Formatter Plus v4.8.0) */
( ( (SELECT "CUSTOMER_NAME",
            "MACHINE_ID",
            "LOCATION_NAME",
            "TYPE",
            "TRAN_AMOUNT_FEE",
            "TRAN_NUMBER_FEE",
            "PROCESSING_FEE"
       FROM (SELECT   REPLACE (c.customer_name,
                               ',',
                               ' '
                              ) AS customer_name,
                      a.machine_id,
                      REPLACE (l.location_name,
                               ',',
                               ' '
                              ) AS location_name,
                      DECODE (card_type,
                              NULL, 'SP',
                              'SP', 'SP',
                              'Credit'
                             ) TYPE,
                      TO_CHAR (SUM (transaction_amount) ) tran_amount_fee,
                      '-' tran_number_fee,
                      ROUND (SUM (transaction_amount) * .05, 2) processing_fee
                 FROM transaction_record_hist a,
                      device.device d,
                      pss.pos p,
                      location.location l,
                      location.customer c
                WHERE a.machine_id = d.device_name
                  AND d.device_type_id = 4 -- Sony PictureStation
                  /*
                  AND transaction_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                  AND transaction_date <
                           LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                         + 1
                         - (1 / 24 / 60 / 60)
                         */
                  AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                  AND a.created_ts <
                           LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                         + 1
                         - (1 / 24 / 60 / 60)
                  AND p.device_id = d.device_id
                  AND p.location_id = l.location_id
                  AND p.customer_id = c.customer_id
             GROUP BY c.customer_name,
                      a.machine_id,
                      DECODE (card_type,
                              NULL, 'SP',
                              'SP', 'SP',
                              'Credit'
                             ),
                      location_name
               HAVING DECODE (card_type,
                              NULL, 'SP',
                              'SP', 'SP',
                              'Credit'
                             ) = 'Credit'
             UNION
             SELECT   REPLACE (c.customer_name,
                               ',',
                               ' '
                              ) AS customer_name,
                      a.machine_id,
                      REPLACE (l.location_name,
                               ',',
                               ' '
                              ) AS location_name,
                      'Tran' TYPE,
                      '-' tran_amount_fee,
                      TO_CHAR (COUNT (*) ) tran_number_fee,
                      COUNT (*) * .05 processing_fee
                 FROM transaction_record_hist a,
                      device.device d,
                      pss.pos p,
                      location.location l,
                      location.customer c
                WHERE a.machine_id = d.device_name
                  AND d.device_type_id = 4 -- Sony PictureStation
                  /*
                  AND transaction_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                  AND transaction_date <
                           LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                         + 1
                         - (1 / 24 / 60 / 60)
                  */
                  AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                  AND a.created_ts <
                           LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                         + 1
                         - (1 / 24 / 60 / 60)
                  AND p.device_id = d.device_id
                  AND p.location_id = l.location_id
                  AND p.customer_id = c.customer_id
             GROUP BY c.customer_name,
                      a.machine_id,
                      location_name,
                      '-',
                      'ALL') ) ) )
)
/


-- End of DDL Script for View SONY.VW_MONTHLY_EFT_BY_TRAN_TYPE

-- Start of DDL Script for View SONY.VW_MONTHLY_EFT_REPORT
-- Generated 3/4/2005 5:32:51 PM from SONY@USADBD03

CREATE OR REPLACE VIEW sony.vw_monthly_eft_report (
   trancnt,
   customer,
   machine_id,
   location,
   credit_amount,
   credit_cnt,
   failed_amt,
   failed_cnt,
   process_fee_amt,
   trans_fee_amt,
   service_fee_amt,
   net_eft_amt )
AS
SELECT   SUM (COUNT) trancnt,
         customer,
         machine_id,
         LOCATION,
         ROUND (SUM (gross_tran_amt), 2) AS credit_amount,
         SUM (gross_tran_cnt) AS credit_cnt,
         ROUND (SUM (failed_credit_amt), 2) AS failed_amt,
         SUM (failed_credit_cnt) AS failed_cnt,
         ROUND (SUM (process_fee), 2) AS process_fee_amt,
         ROUND (SUM (trans_fee), 2) AS trans_fee_amt,
         ROUND (SUM (service_fee), 2) AS service_fee_amt,
         ROUND (SUM (  gross_tran_amt
                     - failed_credit_amt
                     - process_fee
                     - trans_fee
                     - service_fee),
                2) AS net_eft_amt
    FROM (SELECT   *
              FROM (SELECT   COUNT (*) COUNT,
                             replace(c.customer_name,',',' ') customer,
                             a.machine_id,
                             replace(l.location_name,',',' ') LOCATION,
                             0 gross_tran_amt,
                             0 gross_tran_cnt,
                             0 failed_credit_amt,
                             0 failed_credit_cnt,
                             0 process_fee,
                             COUNT (*) * .05 AS trans_fee,
                             0 service_fee
                        FROM transaction_record_hist a,
                             device.device d,
                             pss.pos p,
                             location.location l,
                             location.customer c
                       WHERE d.device_type_id = 4 -- Sony PictureStation
                         AND a.machine_id = d.device_name
                         AND p.device_id = d.device_id
                         AND p.location_id = l.location_id
                         AND p.customer_id = c.customer_id
                         /*
                         AND transaction_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                         AND transaction_date <
                                  LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                                + 1
                                - (1 / 24 / 60 / 60)
                         */
                         AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                         AND a.created_ts <
                                  LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                                + 1
                                - (1 / 24 / 60 / 60)
                         AND (card_type NOT IN ('MC', 'VI', 'DS', 'AE', 'DC', 'JC', 'ENR') or card_type is null)
                        --AND (card_type is null or card_type = 'SP')

                    GROUP BY c.customer_name,
                             a.machine_id,
                             l.location_name
                    UNION
                    SELECT   COUNT (*) COUNT,
                             replace(c.customer_name,',',' ') as customer,
                             a.machine_id,
                             replace(l.location_name, ',',' ') as location,
                             SUM (DECODE (a.force_action_cd,		--sum successful trans
                                          'A', a.transaction_amount,
                                          0
                                         ) ) AS gross_tran_amt,
                             SUM (DECODE (a.force_action_cd,		--count successful trans
                                          'A', 1,
                                          0
                                         ) ) AS gross_tran_cnt,
                             SUM (DECODE (a.force_action_cd,		--sum failed trans
                                          'E', a.transaction_amount,
                                          0
                                         ) ) AS failed_credit_amt,
                             SUM (DECODE (a.force_action_cd,		--count failed trans
                                          'E', 1,
                                          0
                                         ) ) AS failed_credit_cnt,
                             SUM (   (  a.transaction_amount		--process fee
                                      - DECODE (a.force_action_cd,
                                                'E', a.transaction_amount,
                                                0
                                               )
                                     )
                                  * .05) AS process_fee,
                             COUNT (*) * .05 AS trans_fee,
                             0 AS service_fee
                        FROM transaction_record_hist a,
                             device.device d,
                             pss.pos p,
                             location.location l,
                             location.customer c
                       WHERE a.machine_id = d.device_name
                         AND d.device_type_id = 4 -- Sony PictureStation
                         /*
                         AND transaction_date >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                         AND transaction_date <
                                  LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                                + 1
                                - (1 / 24 / 60 / 60)
                         */
                         AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
                         AND a.created_ts <
                                  LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                                + 1
                                - (1 / 24 / 60 / 60)
                         AND card_type IN ('MC', 'VI', 'DS', 'AE', 'DC', 'JC', 'ENR')
                         AND p.device_id = d.device_id
                         AND p.location_id = l.location_id
                         AND p.customer_id = c.customer_id
                    GROUP BY c.customer_name,
                             a.machine_id,
                             location_name
                    UNION
                    SELECT 0 AS COUNT,
                           replace(c.customer_name, ',',' ') AS customer,
                           d.device_name AS machine_id,
                           replace(location_name, ',',' ') AS LOCATION,
                           0 AS gross_tran_amt,
                           0 AS gross_tran_cnt,
                           0 AS failed_credit_amt,
                           0 AS failed_credit_cnt,
                           0 AS process_fee,
                           0 AS trans_fee,
                           25 AS service_fee
                      FROM device.device d,
                             pss.pos p,
                             location.location l,
                             location.customer c
                     WHERE d.device_type_id = 4 -- Sony PictureStation
                        AND p.device_id = d.device_id
                        AND p.location_id = l.location_id
                        AND p.customer_id = c.customer_id
                                                  )
          ORDER BY machine_id ASC)
GROUP BY customer,
         machine_id,
         LOCATION
/


-- End of DDL Script for View SONY.VW_MONTHLY_EFT_REPORT

-- Start of DDL Script for View SONY.VW_MONTHLY_EFT_TRAN_DETAIL
-- Generated 3/4/2005 5:33:01 PM from SONY@USADBD03

CREATE OR REPLACE VIEW sony.vw_monthly_eft_tran_detail (
   trans_no,
   transaction_date,
   owner,
   machine_id,
   transaction_amount,
   transtatus,
   card_type,
   card_number )
AS
(/* Formatted on 2005/02/01 10:24 (Formatter Plus v4.8.0) */
SELECT trans_no,
       transaction_date,
       REPLACE (c.customer_name,
                ',',
                ' '
               ) AS owner,
       a.machine_id,
       transaction_amount,
       a.force_action_cd AS transtatus,
       card_type,
       card_number
  FROM transaction_record_hist a,
       device.device d,
       pss.pos p,
       location.location l,
       location.customer c
 WHERE a.machine_id = d.device_name
   AND d.device_type_id = 4 -- Sony PictureStation
   /*
   AND transaction_date >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
   AND transaction_date <
            LAST_DAY(ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1) )
          + 1
          - (1 / 24 / 60 / 60)
   */

   AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
   AND a.created_ts <
                 LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) ) + 1
                 - (1 / 24 / 60 / 60)

   AND p.device_id = d.device_id
   AND p.location_id = l.location_id
   AND p.customer_id = c.customer_id
   AND a.cancel_cd IS NULL
)
/


-- End of DDL Script for View SONY.VW_MONTHLY_EFT_TRAN_DETAIL

-- Start of DDL Script for View SONY.VW_MONTHLY_LOG_COUNTS_AND_AMTS
-- Generated 3/4/2005 5:33:12 PM from SONY@USADBD03

CREATE OR REPLACE VIEW sony.vw_monthly_log_counts_and_amts (
   machine,
   logamt,
   logcnt )
AS
SELECT   machine,
         SUM (sales_total) AS logamt,
         SUM (total_transactions) AS logcnt
    FROM (SELECT a.machine_id AS machine,
                 a.sales_total AS sales_total,
                 a.total_transactions AS total_transactions
            FROM sony_sales_summary a,
                 device.device d,
                 pss.pos p,
                 location.location l,
                 location.customer c
           WHERE d.device_type_id = 4 -- Sony PictureStation
             AND a.machine_id = d.device_name
             AND p.device_id = d.device_id
             AND p.location_id = l.location_id
             AND p.customer_id = c.customer_id
             /*
             AND a.sales_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
             AND a.sales_ts <
                      LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                    + 1
                    - (1 / 24 / 60 / 60)
             */
             AND a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
             AND a.created_ts <
                      LAST_DAY (ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1) )
                    + 1
                    - (1 / 24 / 60 / 60)
          UNION
          SELECT d.device_name AS machine,
                 0 AS sales_total,
                 0 AS total_transactions
            FROM device.device d,
                 pss.pos p,
                 location.location l,
                 location.customer c
           WHERE d.device_type_id = 4 -- Sony PictureStation
             AND p.device_id = d.device_id
             AND p.location_id = l.location_id
             AND p.customer_id = c.customer_id)
GROUP BY machine
/


-- End of DDL Script for View SONY.VW_MONTHLY_LOG_COUNTS_AND_AMTS
