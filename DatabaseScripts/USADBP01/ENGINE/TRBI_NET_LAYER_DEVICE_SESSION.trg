CREATE OR REPLACE TRIGGER engine.trbi_net_layer_device_session
BEFORE INSERT ON engine.net_layer_device_session
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO :NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;

   IF :NEW.session_id IS NULL
   THEN
      SELECT seq_net_layer_device_sess_id.NEXTVAL
        INTO :NEW.session_id
        FROM DUAL;
   END IF;

	-- logic distributed design engine choice
	BEGIN
		SELECT sf_select_best_logic_engine()
		INTO :NEW.logic_engine_id
		FROM dual;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				:NEW.logic_engine_id := 0;
	END;
END;

/