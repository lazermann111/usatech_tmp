CREATE OR REPLACE FUNCTION ENGINE.SF_SELECT_BEST_LOGIC_ENGINE ( 
    pn_net_layer_id IN engine.net_layer_device_session.session_id%type DEFAULT 0
) RETURN engine.logic_engine.logic_engine_id%type IS
    n_logic_engine_id   engine.logic_engine.logic_engine_id%type;
    n_engine_timeout_sec NUMBER(6,0) := 30;
    n_mci_consecutive_growth_max engine.logic_engine_stats.mci_consecutive_growth_cnt%TYPE := 25;
BEGIN
    BEGIN
		SELECT logic_engine_id
		INTO n_logic_engine_id
		FROM net_layer_device_session
		WHERE session_id = pn_net_layer_id;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			n_logic_engine_id := 0;
    END;
    BEGIN
		IF n_logic_engine_id = 0 THEN --find next best engine alternative (alive or dead)
			SELECT logic_engine_id
			INTO n_logic_engine_id
			FROM (
				SELECT rex.logic_engine_id, rex.logic_engine_active_yn_flag, MIN(resx.mci_consecutive_growth_cnt) grow_cnt
				FROM engine.logic_engine rex, engine.logic_engine_stats resx, engine.logic_engine_device_type_excl redtex
				WHERE rex.logic_engine_id = resx.logic_engine_id
				AND rex.logic_engine_id = redtex.logic_engine_id(+)
				AND rex.logic_engine_active_yn_flag = 'Y'
				AND NOT (	--check for inactive engines that were unable to set 'N' flag
					resx.engine_last_activity_ts + (n_engine_timeout_sec / 86400) < SYSDATE
					AND resx.mci_consecutive_growth_cnt > n_mci_consecutive_growth_max
				)
				GROUP BY rex.logic_engine_id, logic_engine_active_yn_flag
				ORDER BY logic_engine_active_yn_flag, grow_cnt, logic_engine_id
			) x
			WHERE ROWNUM = 1;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			n_logic_engine_id := 0;
	END;
	BEGIN
		IF n_logic_engine_id = 0 THEN --find next best engine alternative (alive or dead)
			SELECT logic_engine_id
			INTO n_logic_engine_id
			FROM (
				SELECT rex.logic_engine_id, rex.logic_engine_active_yn_flag, MIN(resx.mci_consecutive_growth_cnt) grow_cnt
				FROM logic_engine rex, engine.logic_engine_stats resx
				WHERE rex.logic_engine_id = resx.logic_engine_id
				AND rex.logic_engine_active_yn_flag = 'Y'
				GROUP BY rex.logic_engine_id, logic_engine_active_yn_flag
				ORDER BY logic_engine_active_yn_flag, grow_cnt, logic_engine_id
			) x
			WHERE ROWNUM = 1;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			n_logic_engine_id := 0;
	END;
	RETURN n_logic_engine_id;
END;
/