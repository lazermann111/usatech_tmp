CREATE OR REPLACE TRIGGER engine.trbu_net_layer_device_session
BEFORE UPDATE ON engine.net_layer_device_session
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

	/* Copy completed sessions to Hist table */
	IF (:NEW.session_active_yn_flag = 'N') THEN
        IF :NEW.session_end_ts IS NULL THEN
            :NEW.session_end_ts := SYSDATE;
        END IF;
		INSERT INTO net_layer_device_session_hist(
			session_id,
			net_layer_id,
			device_name,
			session_active_yn_flag,
			logic_engine_id,
			session_start_ts,
			session_end_ts,
			session_client_ip_addr,
			session_client_port,
			session_inbound_msg_cnt,
			session_outbound_msg_cnt,
			session_inbound_byte_cnt,
			session_outbound_byte_cnt,
            created_by,
            created_ts,
            last_updated_ts,
            last_updated_by
		) VALUES (
			:NEW.session_id,
			:NEW.net_layer_id,
			:NEW.device_name,
			:NEW.session_active_yn_flag,
			:NEW.logic_engine_id,
			:NEW.session_start_ts,
			:NEW.session_end_ts,
			:NEW.session_client_ip_addr,
			:NEW.session_client_port,
			:NEW.session_inbound_msg_cnt,
			:NEW.session_outbound_msg_cnt,
			:NEW.session_inbound_byte_cnt,
			:NEW.session_outbound_byte_cnt,
            :NEW.created_by,
            :NEW.created_ts,
            :NEW.last_updated_ts,
            :NEW.last_updated_by
		);
	END IF;
END;

/