CREATE OR REPLACE TRIGGER engine.tia_machine_cmd_inbound
AFTER INSERT ON engine.machine_cmd_inbound
REFERENCING new AS NEW old AS OLD
BEGIN
    DECLARE
        n_logic_engine_id logic_engine.logic_engine_id%TYPE;
        n_cur_row_cnt NUMBER(6,0);
        CURSOR c_logic_engine_info IS
			SELECT le.logic_engine_id, NVL(COUNT(mci.logic_engine_id), 0)
			INTO n_logic_engine_id, n_cur_row_cnt
			FROM machine_cmd_inbound mci, logic_engine le
			WHERE le.logic_engine_id = mci.logic_engine_id(+)
			GROUP BY le.logic_engine_id
            ORDER BY le.logic_engine_id;
    BEGIN
        -- logic distributed design stats update
        OPEN c_logic_engine_info;
        LOOP
            FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
            EXIT WHEN c_logic_engine_info%NOTFOUND;

        	UPDATE logic_engine_stats
        	SET mci_last_row_cnt = n_cur_row_cnt,
                mci_consecutive_growth_ts = DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
					(mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
					0,
					mci_consecutive_growth_ts,	--mci_last_row_cnt >= n_cur_row_cnt
					SYSDATE --mci_last_row_cnt < n_cur_row_cnt
				)
                /*mci_consecutive_growth_cnt = DECODE(
                    mci_last_row_cnt - n_cur_row_cnt,
                    0,
                    mci_consecutive_growth_cnt,  --mci_last_row_cnt = n_cur_row_cnt
                    DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
                        (mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
                        0,
                        0,	--mci_last_row_cnt > n_cur_row_cnt
                        mci_consecutive_growth_cnt + 1	--mci_last_row_cnt < n_cur_row_cnt
                    )
                )*/
        	WHERE logic_engine_id = n_logic_engine_id;
        END LOOP;
        CLOSE c_logic_engine_info;
    END;
END;
/