CREATE OR REPLACE TRIGGER engine.trbu_logic_engine
BEFORE UPDATE ON engine.logic_engine
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

	-- logic distributed design stats default values
	IF :NEW.logic_engine_active_yn_flag = 'Y' THEN
		UPDATE logic_engine_stats
		SET mci_last_row_cnt = 0,
            mci_last_row_cnt_activity_ts = SYSDATE,
			mci_consecutive_growth_ts = SYSDATE,
			mci_consecutive_growth_cnt = 0,
			engine_last_activity_ts = SYSDATE,
			engine_session_processed_msgs = 0
		WHERE logic_engine_id = :OLD.logic_engine_id;
	END IF;
END;
/