CREATE OR REPLACE TRIGGER engine.trbu_net_layer
BEFORE UPDATE ON engine.net_layer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

    /* mark device session records inactive */
    IF (:NEW.net_layer_active_yn_flag = 'N') THEN
        UPDATE net_layer_device_session
        SET session_active_yn_flag = 'N'
        WHERE net_layer_id = :OLD.net_layer_id;
   END IF;

END;

/