CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_outbound
BEFORE INSERT ON engine.machine_cmd_outbound
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF (:New.command_id IS NULL) THEN
		SELECT seq_machine_cmd_outbound_id.NEXTVAL
		INTO :NEW.command_id
		FROM dual;
	END IF;
	:NEW.command_date := SYSDATE;
	/*Execute_CD will be null unless it is populated with an 'S'*/
	IF (:NEW.execute_cd is null) THEN
		:NEW.execute_cd := 'N';
	END IF;
END;
/