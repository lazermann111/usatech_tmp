CREATE OR REPLACE TRIGGER engine.trai_logic_engine
AFTER INSERT ON engine.logic_engine
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	-- logic distributed design stats default values
	INSERT INTO logic_engine_stats (
		logic_engine_id,
		mci_last_row_cnt_activity_ts,
		mci_last_row_cnt,
		mci_consecutive_growth_ts,
		mci_consecutive_growth_cnt,
		engine_last_activity_ts,
		engine_session_processed_msgs
	)
	VALUES (
		:NEW.logic_engine_id,
		SYSDATE,
		0,
		SYSDATE,
		0,
		SYSDATE,
		0
	); 
END;

/