CREATE OR REPLACE TRIGGER engine.tua_net_layer_device_session
AFTER UPDATE ON engine.net_layer_device_session
REFERENCING new AS NEW old AS OLD
BEGIN
	/* Purge net_layer_device_session of inactive sessions */
	DELETE FROM engine.net_layer_device_session WHERE session_active_yn_flag = 'N';
END;

/