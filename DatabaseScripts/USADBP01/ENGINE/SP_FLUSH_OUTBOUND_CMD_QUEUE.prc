CREATE OR REPLACE PROCEDURE ENGINE.SP_FLUSH_OUTBOUND_CMD_QUEUE (

/*This procedure will clear all outbound messages for the specified machine*/

   pv_machine_id              IN       machine_cmd_outbound.modem_id%TYPE,
   pn_error_number            OUT      NUMBER,
   pv_error_message           OUT      VARCHAR2)
IS
   v_error_msg                  exception_data.additional_information%TYPE;
   cv_procedure_name   CONSTANT VARCHAR2 (30)                                := 'SP_FLUSH_OUTBOUND_CMD_QUEUE';
   cv_server_name      CONSTANT VARCHAR2 (30)                                := 'TAZ1';
BEGIN
   DELETE FROM machine_cmd_outbound
         WHERE modem_id = pv_machine_id;

   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      v_error_msg :=
                'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
      pkg_exception_processor.sp_log_exception (
         PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id, v_error_msg, cv_server_name,
         cv_procedure_name);
      --pn_return_code := pkg_bottler_globals.unsuccessful_execution;
      pn_error_number := 1;
      pv_error_message := v_error_msg;
      ROLLBACK;
      RAISE;
END;
/