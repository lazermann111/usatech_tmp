CREATE OR REPLACE TRIGGER engine.trbu_session_state
BEFORE UPDATE ON engine.session_state
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_utc_ts,
          :OLD.created_by,
          SYS_EXTRACT_UTC(CURRENT_TIMESTAMP),
          USER
     INTO :NEW.created_utc_ts,
          :NEW.created_by,
          :NEW.last_updated_utc_ts,
          :NEW.last_updated_by
     FROM DUAL;
END; 
/