CREATE OR REPLACE TRIGGER engine.tau_machine_command_pending
AFTER UPDATE ON engine.machine_cmd_pending
REFERENCING new AS NEW old AS OLD
BEGIN
    DELETE FROM MACHINE_COMMAND_PENDING WHERE execute_cd in ('A', 'C');
END;
/