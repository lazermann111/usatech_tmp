CREATE OR REPLACE TRIGGER engine.tua_machine_cmd_inbound
AFTER UPDATE ON engine.machine_cmd_inbound
REFERENCING new AS NEW old AS OLD
BEGIN
	DECLARE
		n_logic_engine_id logic_engine.logic_engine_id%TYPE;
		n_cur_row_cnt NUMBER(6,0);
		CURSOR c_logic_engine_info IS
			SELECT logic_engine_id, COUNT(logic_engine_id)
			INTO n_logic_engine_id, n_cur_row_cnt
			FROM machine_cmd_inbound
			GROUP BY logic_engine_id
            ORDER BY logic_engine_id;
	BEGIN
		--dbms_output.put_line('Before update trigger fired');

		/* Purge machine_cmd_inbound of completed or error messages */
		DELETE FROM machine_cmd_inbound WHERE execute_cd IN ('C', 'X');

		-- logic distributed design stats update (only mci row cnt)
	/*	OPEN c_logic_engine_info;
		LOOP
			FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
			EXIT WHEN c_logic_engine_info%NOTFOUND;

			UPDATE logic_engine_stats
			SET mci_last_row_cnt = n_cur_row_cnt
			WHERE logic_engine_id = n_logic_engine_id;
		END LOOP;
		CLOSE c_logic_engine_info;*/
	END;
END;
/