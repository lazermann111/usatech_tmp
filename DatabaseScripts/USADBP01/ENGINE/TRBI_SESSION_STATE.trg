CREATE OR REPLACE TRIGGER engine.trbi_session_state
BEFORE INSERT ON engine.session_state
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT SYS_EXTRACT_UTC(CURRENT_TIMESTAMP),
           USER,
           SYS_EXTRACT_UTC(CURRENT_TIMESTAMP),
           USER
      INTO :NEW.created_utc_ts,
           :NEW.created_by,
           :NEW.last_updated_utc_ts,
           :NEW.last_updated_by
      FROM DUAL;
END; 
/