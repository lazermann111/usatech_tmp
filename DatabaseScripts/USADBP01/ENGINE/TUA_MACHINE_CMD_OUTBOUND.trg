CREATE OR REPLACE TRIGGER engine.tua_machine_cmd_outbound
AFTER UPDATE ON engine.machine_cmd_outbound
REFERENCING new AS NEW old AS OLD
BEGIN
    /* Y's will be saved in Machine_command_hist - C's will be deleted - X's are Errors*/
    DELETE FROM machine_cmd_outbound WHERE execute_cd in  ('Y','C', 'X');
END;
/