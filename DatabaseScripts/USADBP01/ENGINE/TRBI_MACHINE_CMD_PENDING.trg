CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_pending
BEFORE INSERT ON engine.machine_cmd_pending
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	ln_device_id DEVICE.DEVICE_ID%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
	lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
	lv_comm_method_name COMM_METHOD.COMM_METHOD_NAME%TYPE;
	lv_firmware_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	lv_diag_app_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	ln_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
	ln_file_transfer_type_cd FILE_TRANSFER_TYPE.FILE_TRANSFER_TYPE_CD%TYPE;
	lv_file_transfer_type_name FILE_TRANSFER_TYPE.FILE_TRANSFER_TYPE_NAME%TYPE;
	ln_file_size NUMERIC;
	ln_max_file_size NUMERIC := 0;
	lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
	lv_new_firmware_version VARCHAR2(20);
	lv_new_full_firmware_version VARCHAR2(34);
	lv_flash_model HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE;
	lv_bezel_mfgr HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE;
	lv_bezel_model HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE;
	lv_bezel_firmware_version HOST_SETTING.HOST_SETTING_VALUE%TYPE;
	lv_bezel_regexp ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
	lv_min_bezel_firmware_version HOST_SETTING.HOST_SETTING_VALUE%TYPE;
BEGIN
	IF :NEW.DATA_TYPE IN('7C', 'A4', 'C7', 'C8') AND :NEW.DEVICE_FILE_TRANSFER_ID IS NULL THEN
		:NEW.DEVICE_FILE_TRANSFER_ID := DBADMIN.TO_NUMBER_OR_NULL(:NEW.COMMAND);
	END IF;

	IF :NEW.DATA_TYPE IN('A4', 'C7', 'C8') THEN
		SELECT MAX(D.DEVICE_ID), MAX(D.DEVICE_TYPE_ID), MAX(D.DEVICE_SERIAL_CD), MAX(D.COMM_METHOD_CD), MAX(CM.COMM_METHOD_NAME),
            MAX(D.FIRMWARE_VERSION), MAX(DD.DIAG_APP_VERSION), MAX(HE.HOST_EQUIPMENT_MODEL), 
            MAX(HE2.HOST_EQUIPMENT_MFGR), MAX(HE2.HOST_EQUIPMENT_MODEL), MAX(HS.HOST_SETTING_VALUE)
		INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, lc_comm_method_cd, lv_comm_method_name,
            lv_firmware_version, lv_diag_app_version, lv_flash_model, 
            lv_bezel_mfgr, lv_bezel_model, lv_bezel_firmware_version
		FROM DEVICE.DEVICE_LAST_ACTIVE DLA
		JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
		LEFT OUTER JOIN DEVICE.DEVICE_DATA DD ON DLA.DEVICE_NAME = DD.DEVICE_NAME
		LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
		LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_TYPE_ID = 401
		LEFT OUTER JOIN DEVICE.HOST_EQUIPMENT HE ON H.HOST_EQUIPMENT_ID = HE.HOST_EQUIPMENT_ID
		LEFT OUTER JOIN DEVICE.HOST H2 ON D.DEVICE_ID = H2.DEVICE_ID AND H2.HOST_TYPE_ID = 400
		LEFT OUTER JOIN DEVICE.HOST_EQUIPMENT HE2 ON H2.HOST_EQUIPMENT_ID = HE2.HOST_EQUIPMENT_ID
		LEFT OUTER JOIN DEVICE.HOST_SETTING HS ON H2.HOST_ID = HS.HOST_ID AND HS.HOST_SETTING_PARAMETER = 'Application Version'
		WHERE DLA.DEVICE_NAME = :NEW.MACHINE_ID;

		IF ln_device_type_id IN (1, 13) THEN
			SELECT FT.FILE_TRANSFER_ID, FT.FILE_TRANSFER_NAME, FTT.FILE_TRANSFER_TYPE_CD, FTT.FILE_TRANSFER_TYPE_NAME, COALESCE(DBMS_LOB.GETLENGTH(FT.FILE_TRANSFER_CONTENT), 0)
			INTO ln_file_transfer_id, ln_file_transfer_name, ln_file_transfer_type_cd, lv_file_transfer_type_name, ln_file_size
			FROM DEVICE.DEVICE_FILE_TRANSFER DFT
			JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
			JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
			WHERE DFT.DEVICE_FILE_TRANSFER_ID = :NEW.DEVICE_FILE_TRANSFER_ID;

			IF ln_device_type_id = 1 THEN
				IF ln_file_transfer_type_cd NOT IN (5, 24, 25, 27, 28, 33) THEN
					RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' internal file download is not supported by Gx');
				END IF;

				lv_firmware_version := SUBSTR(lv_firmware_version, 9);
				lv_diag_app_version := REGEXP_REPLACE(lv_diag_app_version, 'Diag |Diagnostic = N/A', '');

				lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFERS');
				IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
					RAISE_APPLICATION_ERROR(-20208, 'Invalid Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for internal file downloads');
				END IF;

				IF ln_file_transfer_type_cd = 5 THEN --Application Software Upgrade
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_DIAGNOSTIC_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
					IF lv_diag_app_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_diag_app_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid ' || lv_comm_method_name || ' Gx diagnostic app version: ' || lv_diag_app_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;

					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
					IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid ' || lv_comm_method_name || ' Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;

					SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 19, 82)), '-')
					INTO lv_new_firmware_version
					FROM DEVICE.FILE_TRANSFER
					WHERE FILE_TRANSFER_ID = ln_file_transfer_id;

					IF lv_new_firmware_version NOT LIKE 'USA-%' AND lv_new_firmware_version NOT LIKE 'Diag%' AND lv_new_firmware_version NOT LIKE 'PTest%' THEN
						RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for Gx');
					END IF;
				ELSIF ln_file_transfer_type_cd IN (27, 28) THEN --Card Reader Application Firmware, Isis SmartTap Configuration
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES');
					IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
				END IF;
			ELSIF ln_device_type_id = 13 THEN
				IF lv_device_serial_cd LIKE 'VJ%' THEN
					IF ln_file_transfer_type_cd NOT IN (5, 16, 19, 24, 25, 27, 28, 31, 33, 34, 35, 36) THEN
						RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' file download is not supported by G9');
					END IF;					
					IF ln_file_transfer_type_cd IN (27, 31) AND lv_firmware_version IS NOT NULL THEN
						lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES');
						IF DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
							RAISE_APPLICATION_ERROR(-20208, 'Invalid G9 firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
						END IF;
					END IF;
					IF ln_file_transfer_type_cd IN (36) AND lv_firmware_version IS NOT NULL THEN
						lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMV_CAPK_UPDATE_MIN_FIRMWARE_VERSION');
						IF DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
							RAISE_APPLICATION_ERROR(-20208, 'Invalid G9 firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
						END IF;
					END IF;
					IF ln_file_transfer_type_cd IN (36) THEN
						lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMV_CAPK_UPDATE_CARD_READERS');
						lv_bezel_regexp := '(\s|;)*'|| lv_bezel_mfgr ||'(\s)*,(\s)*'|| lv_bezel_model ||'(\s)*,(\s)*([^;]+)(\s|;)*';
						SELECT MAX(REGEXP_SUBSTR(lv_app_setting_value, lv_bezel_regexp, 1, 1, '', 6))
						INTO lv_min_bezel_firmware_version
						FROM DUAL WHERE REGEXP_LIKE(lv_app_setting_value, lv_bezel_regexp);
						IF lv_min_bezel_firmware_version IS NULL THEN
							RAISE_APPLICATION_ERROR(-20208, 'Invalid G9 bezel model: ' || lv_bezel_mfgr || ' ' || lv_bezel_model
							|| ', one of ' || REPLACE(REGEXP_REPLACE(lv_app_setting_value, ',([^,;]+)(;|$)', '\2'), ',', '') || ' is required for ' || lv_file_transfer_type_name);
						ELSIF DBADMIN.VERSION_COMPARE(lv_bezel_firmware_version, lv_min_bezel_firmware_version) < 0 THEN
							RAISE_APPLICATION_ERROR(-20208, 'Invalid G9 bezel firmware version: ' || lv_bezel_firmware_version || ', '
							|| lv_min_bezel_firmware_version || ' or greater is required for ' || lv_file_transfer_type_name);
						END IF;
					END IF;
				ELSIF lv_device_serial_cd LIKE 'EE%' THEN
					IF ln_file_transfer_type_cd NOT IN (5, 19) THEN
						RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' file download is not supported by Edge');
					END IF;
				END IF;

				IF lv_flash_model = '128Mbit' THEN
					ln_max_file_size := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_MAX_FILE_SIZE_BYTES_FOR_128MBIT_FLASH');
				ELSE
					ln_max_file_size := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_EDGE_DEFAULT_MAX_FILE_SIZE_BYTES');
				END IF;

				IF ln_file_size > ln_max_file_size THEN
					RAISE_APPLICATION_ERROR(-20208, 'File size ' || ROUND(ln_file_size / 1024 / 1024, 1) || ' MB is bigger than maximum file size ' || ROUND(ln_max_file_size / 1024 / 1024, 1) || ' MB supported by device');
				END IF;

				IF ln_file_transfer_type_cd = 5 THEN --Application Software Upgrade
					SELECT COALESCE(RTRIM(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 17, 93))), '-')
					INTO lv_new_full_firmware_version
					FROM DEVICE.FILE_TRANSFER
					WHERE FILE_TRANSFER_ID = ln_file_transfer_id;

					lv_new_firmware_version := SUBSTR(lv_new_full_firmware_version, 1, 10);

					IF lv_device_serial_cd LIKE 'VJ%' THEN
						IF NOT REGEXP_LIKE(lv_new_firmware_version, '^2\.0[2-9]\.') THEN
							RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' G9');
						END IF;
						IF lc_comm_method_cd = 'G' THEN
							IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.02.008i') < 0 THEN
								RAISE_APPLICATION_ERROR(-20208, 'Firmware version 2.02.008i or greater is required for ' || lv_comm_method_name || ' G9');
							END IF;
						END IF;
						IF lv_flash_model = '128Mbit' THEN
							lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_MIN_FIRMWARE_VERSION_FOR_128MBIT_FLASH');
							IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, lv_app_setting_value) < 0 THEN
								RAISE_APPLICATION_ERROR(-20208, 'Firmware version ' || lv_app_setting_value || ' or greater is required for G9 with ' || lv_flash_model || ' flash');
							END IF;
						END IF;						
						-- USAT-526 Reject firmware incompatible with new Telit modems that require longer timeout on powerup
						IF LENGTH(lv_device_serial_cd) = 11 AND (lv_device_serial_cd BETWEEN 'VJ300054199' AND 'VJ399999999' OR lv_device_serial_cd BETWEEN 'VJ600001000' AND 'VJ699999999') THEN
							IF NOT (DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.04.005f') > 0 OR lv_new_firmware_version LIKE '2.04.005e%' AND LENGTH(lv_new_full_firmware_version) > 10) THEN
								RAISE_APPLICATION_ERROR(-20208, 'Firmware version ' || lv_new_firmware_version || ' is incompatible with modem in device ' || lv_device_serial_cd);
							END IF;
						END IF;
					ELSIF lv_device_serial_cd LIKE 'EE%' THEN
						IF lc_comm_method_cd = 'C' THEN
							IF lv_new_firmware_version NOT LIKE '1.02.%' THEN
								RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' Edge');
							END IF;
						ELSIF lc_comm_method_cd = 'G' THEN
							IF lv_new_firmware_version NOT LIKE '1.00.%' AND lv_new_firmware_version NOT LIKE '1.01.%' THEN
								RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' Edge');
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

   if (:NEW.machine_command_pending_id is null) then
      SELECT SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
      INTO :NEW.machine_command_pending_id
      FROM dual ;
   end if;
	SELECT	sysdate,
			user,
			sysdate,
			user
	into	:new.created_ts,
			:new.created_by,
			:new.last_updated_ts,
			:new.last_updated_by
	FROM	dual;
END;
/