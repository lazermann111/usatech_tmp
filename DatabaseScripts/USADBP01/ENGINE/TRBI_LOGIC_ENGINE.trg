CREATE OR REPLACE TRIGGER engine.trbi_logic_engine
BEFORE INSERT ON engine.logic_engine
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.logic_engine_id IS NULL
   THEN
      SELECT seq_logic_engine_id.NEXTVAL
        INTO :NEW.logic_engine_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;

/