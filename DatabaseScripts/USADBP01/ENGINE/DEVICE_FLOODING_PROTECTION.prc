CREATE OR REPLACE PROCEDURE ENGINE.DEVICE_FLOODING_PROTECTION AS
	ln_flood_msg_cnt_threshold NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FLOOD_MSG_CNT_THRESHOLD'));
	ln_flood_rejection_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FLOOD_REJECTION_SEC'));
	ld_sysdate DATE := SYSDATE;
	ld_reject_until_ts DATE := ld_sysdate + ln_flood_rejection_sec/86400;
BEGIN
	-- detection and prevention of devices flooding the server
	IF ln_flood_rejection_sec > 0 THEN
		UPDATE DEVICE.DEVICE
		SET REJECT_UNTIL_TS = ld_reject_until_ts
		WHERE DEVICE_ID IN (
			SELECT d.device_id
			FROM device.device d
			LEFT OUTER JOIN (
				SELECT /*+ INDEX(dm IX_DM_INBOUND_MESSAGE_TS) */ device_name, COUNT(1) message_count
				FROM engine.device_message dm
				WHERE inbound_message_ts > ld_sysdate - 1/24
					AND inbound_message_type NOT IN ('7F', 'A7', 'CB')
				GROUP BY device_name
			) this_hour ON d.device_name = this_hour.device_name
			LEFT OUTER JOIN (
				SELECT /*+ INDEX(dm IX_DM_INBOUND_MESSAGE_TS) */ device_name, COUNT(1) message_count
				FROM engine.device_message dm
				WHERE inbound_message_ts BETWEEN ld_sysdate - 2/24 AND ld_sysdate - 1/24
					AND inbound_message_type NOT IN ('7F', 'A7', 'CB')
				GROUP BY device_name
			) last_hour ON d.device_name = last_hour.device_name
			WHERE d.device_active_yn_flag = 'Y'
				AND NVL(this_hour.message_count, 0) - NVL(last_hour.message_count, 0) > ln_flood_msg_cnt_threshold
		) AND DBADMIN.PKG_UTL.COMPARE(ld_sysdate, REJECT_UNTIL_TS) = 1;
		COMMIT;
	END IF;
END;
/
