CREATE OR REPLACE TRIGGER engine.trbu_machine_cmd_outbound
BEFORE UPDATE ON engine.machine_cmd_outbound
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	:NEW.execute_date := SYSDATE;
	/* Move Errors to Hist table */
	IF (SUBSTR (:NEW.execute_cd, 1, 1) = 'E') THEN
		INSERT INTO machine_cmd_outbound_hist(
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id,
			net_layer_id
		) VALUES (
			:NEW.command_id,
			:NEW.modem_id,
			:NEW.command,
			:NEW.command_date,
			:NEW.execute_date,
			:NEW.execute_cd,
			:NEW.session_id,
			:NEW.net_layer_id
		);
		/* Change execute_cd to X */
		:NEW.execute_cd := 'X';
	END IF;
END;
/