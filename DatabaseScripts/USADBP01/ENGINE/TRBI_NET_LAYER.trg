CREATE OR REPLACE TRIGGER engine.trbi_net_layer
BEFORE INSERT ON engine.net_layer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.net_layer_id IS NULL
   THEN
      SELECT seq_net_layer_id.NEXTVAL
        INTO :NEW.net_layer_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;

/