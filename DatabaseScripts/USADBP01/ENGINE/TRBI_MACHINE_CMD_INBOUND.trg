CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_inbound
BEFORE INSERT ON engine.machine_cmd_inbound
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF (:NEW.inbound_id IS NULL) THEN
		SELECT seq_machine_cmd_inbound_id.NEXTVAL
		INTO :NEW.inbound_id
		FROM DUAL;
	END IF;

	IF (:NEW.inbound_date IS NULL) THEN
		:NEW.inbound_date := SYSDATE;
	END IF;

	:NEW.num_times_executed := 0;

	-- Execute_CD will be null unless it is populated with an 'S'
	IF (:NEW.execute_cd IS NULL) THEN
		:NEW.execute_cd := 'N';
	END IF;

	:NEW.original_execute_cd := :NEW.execute_cd;
	:NEW.insert_ts := SYSDATE;
	:NEW.logic_engine_id := 0;

	-- logic distributed design engine choice
    BEGIN
        SELECT sf_select_best_logic_engine(:NEW.session_id)
        INTO :NEW.logic_engine_id
        FROM dual;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.logic_engine_id := 0;
    END;

	-- logic distributed design stats update
/*	UPDATE logic_engine_stats
	SET mci_last_row_cnt_activity_ts = SYSDATE,
		mci_consecutive_growth_cnt = mci_consecutive_growth_cnt + 1
	WHERE logic_engine_id = :NEW.logic_engine_id;*/
END;
/