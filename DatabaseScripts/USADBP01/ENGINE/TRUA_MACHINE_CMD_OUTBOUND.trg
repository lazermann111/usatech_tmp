CREATE OR REPLACE TRIGGER engine.trua_machine_cmd_outbound
AFTER UPDATE ON engine.machine_cmd_outbound
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF (:NEW.execute_cd = 'Y') THEN
		INSERT INTO machine_cmd_outbound_hist (
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id,
			net_layer_id
		) VALUES (
			:NEW.command_id,
			:NEW.Modem_id,
			:NEW.command,
			:NEW.command_date,
			:NEW.execute_date,
			:NEW.execute_cd,
			:NEW.session_id,
			:NEW.net_layer_id
		);
	END IF;
END;
/