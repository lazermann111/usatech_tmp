CREATE OR REPLACE TRIGGER engine.trbi_logic_engine_stats
BEFORE INSERT ON engine.logic_engine_stats
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;

/