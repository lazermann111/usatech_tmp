SET SERVEROUTPUT ON
  declare

  l_act_STATUS CHAR;
  l_act_TERM_CHANGES_CNT NUMBER;
  l_TERMINAL_ID NUMBER := 39794;
  
  err_num NUMBER;
  err_msg VARCHAR2(200);
  
  procedure test_terminal_edit_custom_sp
  (l_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
   l_asset IN REPORT.TERMINAL.ASSET_NBR%TYPE,
   l_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
   l_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
   l_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
   l_location IN REPORT.LOCATION.LOCATION_NAME%TYPE,
   l_loc_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
   l_address_id IN REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
   l_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
   l_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
   l_state IN REPORT.TERMINAL_ADDR.STATE%TYPE,
   l_zip IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
   l_cust_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   l_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
   l_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
   l_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
   l_loc_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
   l_loc_type_spec IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
   l_prod_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
   l_prod_type_spec IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
   l_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
   l_tz_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
   l_dex IN REPORT.TERMINAL.DEX_DATA%TYPE,
   l_receipt IN REPORT.TERMINAL.RECEIPT%TYPE,
   l_vends IN REPORT.TERMINAL.VENDS%TYPE,
   l_term_var_1 IN REPORT.TERMINAL.term_var_1%TYPE,
   l_term_var_2 IN REPORT.TERMINAL.term_var_2%TYPE,
   l_term_var_3 IN REPORT.TERMINAL.term_var_3%TYPE,
   l_term_var_4 IN REPORT.TERMINAL.term_var_4%TYPE,
   l_term_var_5 IN REPORT.TERMINAL.term_var_5%TYPE,
   l_term_var_6 IN REPORT.TERMINAL.term_var_6%TYPE,
   l_term_var_7 IN REPORT.TERMINAL.term_var_7%TYPE,
   l_term_var_8 IN REPORT.TERMINAL.term_var_8%TYPE
  )
  is
  
BEGIN

  REPORT.TERMINAL_EDIT_CUSTOM(
    L_TERMINAL_ID => L_TERMINAL_ID,
    L_ASSET => L_ASSET,
    L_MACHINE_ID => L_MACHINE_ID,
    L_TELEPHONE => L_TELEPHONE,
    L_PREFIX => L_PREFIX,
    L_LOCATION => L_LOCATION,
    L_LOC_DETAILS => L_LOC_DETAILS,
    L_ADDRESS_ID => L_ADDRESS_ID,
    L_ADDRESS1 => L_ADDRESS1,
    L_CITY => L_CITY,
    L_STATE => L_STATE,
    L_ZIP => L_ZIP,
    L_CUST_BANK_ID => L_CUST_BANK_ID,
    L_USER_ID => L_USER_ID,
    L_PC_ID => L_PC_ID,
    L_SC_ID => L_SC_ID,
    L_LOC_TYPE_ID => L_LOC_TYPE_ID,
    L_LOC_TYPE_SPEC => L_LOC_TYPE_SPEC,
    L_PROD_TYPE_ID => L_PROD_TYPE_ID,
    L_PROD_TYPE_SPEC => L_PROD_TYPE_SPEC,
    L_AUTH_MODE => L_AUTH_MODE,
    L_TZ_ID => L_TZ_ID,
    L_DEX => L_DEX,
    L_RECEIPT => L_RECEIPT,
    L_VENDS => L_VENDS,
    L_TERM_VAR_1 => L_TERM_VAR_1,
    L_TERM_VAR_2 => L_TERM_VAR_2,
    L_TERM_VAR_3 => L_TERM_VAR_3,
    L_TERM_VAR_4 => L_TERM_VAR_4,
    L_TERM_VAR_5 => L_TERM_VAR_5,
    L_TERM_VAR_6 => L_TERM_VAR_6,
    L_TERM_VAR_7 => L_TERM_VAR_7,
    L_TERM_VAR_8 => L_TERM_VAR_8
  );
  
  EXCEPTION WHEN OTHERS THEN
    err_num := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 100);
    dbms_output.put_line('### ALERT!EXCEPTION OCCURRED DURING procedure_test_terminal_edit_custom_sp: ERR_NUM: '|| l_TERMINAL_ID ||', ERR_MSG: '||err_msg||'.');
  
  END;
  
  BEGIN
  --TEST 1 - Change Receipt Flag, 10th IN param from last param in sp
  dbms_output.put_line('---');
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - Change Receipt Flag - TERMINAL_ID = '|| l_TERMINAL_ID ||', EXPECTING: ' || 0 ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || 'A');
    test_terminal_edit_custom_sp(l_TERMINAL_ID, '123456789',1653,'6105551212',null,'TD000009',null,56164,'100 Deerfield Road','Marvern','PA','31955',
	null,null,null,null,
	15,null,13,null,
	'F',0,'N','Y',10,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  select count(1) into l_act_TERM_CHANGES_CNT from REPORT.TERMINAL_CHANGES WHERE TERMINAL_ID = l_TERMINAL_ID ;
  SELECT STATUS INTO l_act_STATUS FROM REPORT.TERMINAL WHERE TERMINAL_ID = l_TERMINAL_ID;
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - TERMINAL_ID = '|| l_TERMINAL_ID ||', ACTUAL: ' || l_act_TERM_CHANGES_CNT ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || l_act_STATUS);
  dbms_output.put_line('---');
  l_act_STATUS := '';
  l_act_TERM_CHANGES_CNT := -1;
  
  --TEST 2 - Change Auth Mode, 13th IN param from last param in sp
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - Change Auth Mode - TERMINAL_ID = '|| l_TERMINAL_ID ||', EXPECTING: ' || 0 ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || 'A');
    test_terminal_edit_custom_sp(l_TERMINAL_ID, '123456789',1653,'6105551212',null,'TD000009',null,56164,'100 Deerfield Road','Marvern','PA','31955',
	null,null,null,null,
	15,null,13,null,
	'Y',0,'N','Y',10,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  select count(1) into l_act_TERM_CHANGES_CNT from REPORT.TERMINAL_CHANGES WHERE TERMINAL_ID = l_TERMINAL_ID ;
  SELECT STATUS INTO l_act_STATUS FROM REPORT.TERMINAL WHERE TERMINAL_ID = l_TERMINAL_ID;
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - TERMINAL_ID = '|| l_TERMINAL_ID ||', ACTUAL: ' || l_act_TERM_CHANGES_CNT ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || l_act_STATUS);
  dbms_output.put_line('---');
  
  l_act_STATUS := '';
  l_act_TERM_CHANGES_CNT := -1;
  
  --TEST 3 - Change Dex_Data, 11th IN param from last param in sp
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - Change Dex_Data - TERMINAL_ID = '|| l_TERMINAL_ID ||', EXPECTING: ' || 1 ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || 'U');
    test_terminal_edit_custom_sp(l_TERMINAL_ID, '123456789',1653,'6105551212',null,'TD000009',null,56164,'100 Deerfield Road','Marvern','PA','31955',
	null,null,null,null,
	15,null,13,null,
	'Y',0,'Y','Y',10,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  select count(1) into l_act_TERM_CHANGES_CNT from REPORT.TERMINAL_CHANGES WHERE TERMINAL_ID = l_TERMINAL_ID ;
  SELECT STATUS INTO l_act_STATUS FROM REPORT.TERMINAL WHERE TERMINAL_ID = l_TERMINAL_ID;
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - TERMINAL_ID = '|| l_TERMINAL_ID ||', ACTUAL: ' || l_act_TERM_CHANGES_CNT ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || l_act_STATUS);
  dbms_output.put_line('---');
  
  l_act_STATUS := '';
  l_act_TERM_CHANGES_CNT := -1;
  
  --TEST 4 - Change Time Zone, 12th IN param from last param in sp
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - Change Time Zone - TERMINAL_ID = '|| l_TERMINAL_ID ||', EXPECTING: ' || 2 ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || 'U');
    test_terminal_edit_custom_sp(l_TERMINAL_ID, '123456789',1653,'6105551212',null,'TD000009',null,56164,'100 Deerfield Road','Marvern','PA','31955',
	null,null,null,null,
	15,null,13,null,
	'Y',12,'Y','Y',10,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  select count(1) into l_act_TERM_CHANGES_CNT from REPORT.TERMINAL_CHANGES WHERE TERMINAL_ID = l_TERMINAL_ID ;
  SELECT STATUS INTO l_act_STATUS FROM REPORT.TERMINAL WHERE TERMINAL_ID = l_TERMINAL_ID;
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - TERMINAL_ID = '|| l_TERMINAL_ID ||', ACTUAL: ' || l_act_TERM_CHANGES_CNT ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || l_act_STATUS);
  dbms_output.put_line('---');
  
  l_act_STATUS := '';
  l_act_TERM_CHANGES_CNT := -1;
  
  --TEST 5 - Change Vends, 9th IN param from last param in sp
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - Change Vends - TERMINAL_ID = '|| l_TERMINAL_ID ||', EXPECTING: ' || 3 ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || 'U');
   test_terminal_edit_custom_sp(l_TERMINAL_ID, '123456789',1653,'6105551212',null,'TD000009',null,56164,'100 Deerfield Road','Marvern','PA','31955',
	null,null,null,null,
	15,null,13,null,
	'Y',12,'Y','Y',5,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  select count(1) into l_act_TERM_CHANGES_CNT from REPORT.TERMINAL_CHANGES WHERE TERMINAL_ID = l_TERMINAL_ID ;
  SELECT STATUS INTO l_act_STATUS FROM REPORT.TERMINAL WHERE TERMINAL_ID = l_TERMINAL_ID;
  dbms_output.put_line('procedure_test_terminal_edit_custom_sp - TERMINAL_ID = '|| l_TERMINAL_ID ||', ACTUAL: ' || l_act_TERM_CHANGES_CNT ||' TERMINAL_CHANGES RECORDS, TERMINAL STATUS OF    ' || l_act_STATUS);
  dbms_output.put_line('---');
  
  l_act_STATUS := '';
  l_act_TERM_CHANGES_CNT := -1;
  
  END;
  /