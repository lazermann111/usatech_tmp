CREATE OR REPLACE TRIGGER app_user.trbi_app_object_type
BEFORE INSERT ON app_user.app_object_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.app_object_type_id IS NULL THEN

      SELECT seq_app_object_type_id.nextval
        into :new.app_object_type_id
        FROM dual;

    END IF;

 SELECT 
           sysdate,
           user,
           sysdate,
           user
      into 
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/