CREATE OR REPLACE TRIGGER app_user.trbi_app
BEFORE INSERT ON app_user.app
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.app_id IS NULL THEN

      SELECT seq_app_id.nextval
        into :new.app_id
        FROM dual;

    END IF;

 SELECT 
           sysdate,
           user,
           sysdate,
           user
      into 
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/