CREATE OR REPLACE TRIGGER app_user.trbu_app_object_type
BEFORE UPDATE ON app_user.app_object_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
SELECT

           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into

           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/