-- Start of DDL Script for Constraint APP_USER.PK_APP_USER_OBJECT_PERMISSION
-- Generated 18-Jul-2005 10:23:23 from SYSTEM@USADBD03

-- Drop the old instance of PK_APP_USER_OBJECT_PERMISSION
ALTER TABLE app_user.app_user_object_permission
DROP CONSTRAINT pk_app_user_object_permission
/
DROP INDEX app_user.pk_app_user_object_permission
/
ALTER TABLE app_user.app_user_object_permission
ADD CONSTRAINT pk_app_user_object_permission PRIMARY KEY (app_user_id, app_id, 
  app_object_type_id, object_cd)
  USING INDEX tablespace APP_USER_INDX
/

