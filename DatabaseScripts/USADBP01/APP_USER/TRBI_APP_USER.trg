CREATE OR REPLACE TRIGGER app_user.trbi_app_user
BEFORE INSERT ON app_user.app_user
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.app_user_id IS NULL THEN

      SELECT seq_app_user_id.nextval
        into :new.app_user_id
        FROM dual;

    END IF;

 SELECT 
           sysdate,
           user,
           sysdate,
           user,
           lower(:new.app_user_name)
      into 
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.app_user_name
      FROM dual;

End;
/