INSERT INTO device.device_type(device_type_id, device_type_desc) VALUES(11, 'Kiosk')
/

ALTER TABLE PSS.TRAN MODIFY(TRAN_AUTH_AMOUNT NUMBER(14, 4), TRAN_SETTLEMENT_AMOUNT NUMBER(14, 4))
/
ALTER MATERIALIZED VIEW PSS.MV_ESUDS_SETTLED_TRAN MODIFY(TRAN_SETTLEMENT_AMOUNT NUMBER(14, 4))
/
ALTER TABLE PSS.MLOG$_TRAN MODIFY(TRAN_SETTLEMENT_AMOUNT NUMBER(14, 4))
/
ALTER TABLE TAZDBA.TRANSACTION_RECORD MODIFY(AUTH_AMT NUMBER(14, 4), TRANSACTION_AMOUNT NUMBER(14, 4))
/
ALTER TABLE TAZDBA.TRANSACTION_RECORD_HIST MODIFY(AUTH_AMT NUMBER(14, 4), TRANSACTION_AMOUNT NUMBER(14, 4))
/
ALTER TABLE TAZDBA.TRANSACTION_FAILURES MODIFY(AUTH_AMT NUMBER(14, 4), TRANSACTION_AMOUNT NUMBER(14, 4))
/
ALTER TABLE TAZDBA.TRANS_DTL_RPT MODIFY(TRANSACTION_AMT NUMBER(14, 4))
/
ALTER TABLE TAZDBA.TRANS_DTL_RPT_TMP MODIFY(TRANSACTION_AMT NUMBER(14, 4))
/
ALTER TABLE DEVICE.DEVICE_SETTING MODIFY(DEVICE_SETTING_VALUE VARCHAR2(200))
/


INSERT INTO pss.authority_payment_mask
(
	authority_payment_mask_id, 
	authority_payment_mask_name, 
	authority_payment_mask_desc, 
	authority_payment_mask_regex
)
VALUES
(
	20, 
	'Default', 
	'Credit Card - Generic', 
	'^(.+)$'
)
/

INSERT INTO pss.payment_subtype
(
    payment_subtype_id, 
    payment_subtype_name, 
    payment_subtype_key_name, 
    client_payment_type_cd, 
    payment_subtype_table_name, 
    payment_subtype_key_desc_name, 
    authority_payment_mask_id
)
VALUES
(
    11,
    'Credit Card - Generic', 
    'Not Applicable',
    'C',
    'Not Applicable',
    'Not Applicable',
    20
)
/

UPDATE pss.authority_payment_mask 
SET payment_subtype_id = 11
WHERE authority_payment_mask_id = 20
/

INSERT INTO pss.authority_payment_mask
(
	authority_payment_mask_id, 
	authority_payment_mask_name, 
	authority_payment_mask_desc, 
	authority_payment_mask_regex
)
VALUES
(
	21, 
	'Default', 
	'Special Card - Generic', 
	'^(.+)$'
)
/

INSERT INTO pss.payment_subtype
(
    payment_subtype_id, 
    payment_subtype_name, 
    payment_subtype_key_name, 
    client_payment_type_cd, 
    payment_subtype_table_name, 
    payment_subtype_key_desc_name, 
    authority_payment_mask_id
)
VALUES
(
    12,
    'Special Card - Generic', 
    'Not Applicable',
    'S',
    'Not Applicable',
    'Not Applicable',
    21
)
/

UPDATE pss.authority_payment_mask 
SET payment_subtype_id = 12
WHERE authority_payment_mask_id = 21
/

UPDATE pss.client_payment_type
SET client_payment_type_desc = 'RFID Credit Card'
WHERE client_payment_type_cd = 'R'
/

INSERT INTO pss.client_payment_type
(
    client_payment_type_cd,
    client_payment_type_desc
)
VALUES
(
    'P',
    'RFID Special Card'   
)
/

INSERT INTO pss.authority_payment_mask
(
	authority_payment_mask_id, 
	authority_payment_mask_name, 
	authority_payment_mask_desc, 
	authority_payment_mask_regex
)
VALUES
(
	22, 
	'Default', 
	'RFID Credit Card - Generic', 
	'^(.+)$'
)
/

INSERT INTO pss.payment_subtype
(
    payment_subtype_id, 
    payment_subtype_name, 
    payment_subtype_key_name, 
    client_payment_type_cd, 
    payment_subtype_table_name, 
    payment_subtype_key_desc_name, 
    authority_payment_mask_id
)
VALUES
(
    13,
    'RFID Credit Card - Generic', 
    'Not Applicable',
    'R',
    'Not Applicable',
    'Not Applicable',
    22
)
/

UPDATE pss.authority_payment_mask 
SET payment_subtype_id = 13
WHERE authority_payment_mask_id = 22
/

INSERT INTO pss.authority_payment_mask
(
	authority_payment_mask_id, 
	authority_payment_mask_name, 
	authority_payment_mask_desc, 
	authority_payment_mask_regex
)
VALUES
(
	23, 
	'Default', 
	'RFID Special Card - Generic', 
	'^(.+)$'
)
/

INSERT INTO pss.payment_subtype
(
    payment_subtype_id, 
    payment_subtype_name, 
    payment_subtype_key_name, 
    client_payment_type_cd, 
    payment_subtype_table_name, 
    payment_subtype_key_desc_name, 
    authority_payment_mask_id
)
VALUES
(
    14,
    'RFID Special Card - Generic', 
    'Not Applicable',
    'P',
    'Not Applicable',
    'Not Applicable',
    23
)
/

UPDATE pss.authority_payment_mask 
SET payment_subtype_id = 14
WHERE authority_payment_mask_id = 23
/

INSERT INTO pss.authority_payment_mask
(
	authority_payment_mask_id, 
	authority_payment_mask_name, 
	authority_payment_mask_desc, 
	authority_payment_mask_regex,
	authority_payment_mask_bref
)
VALUES
(
	24, 
	'Default', 
	'Credit Card (Track 1)', 
	'^(%?)B(([0-9]{16}[0-9]{0,3})|(([0-9]{4} ){3}[0-9]{4}))\^([A-Za-z0-9 \/]{0,26})\^(([0-9]{0,4})|\^)([A-Za-z0-9 \/]{0,57})(\??)([0-9]{0,3})$',
	'2:1|6:2|8:3|9:5'
)
/

INSERT INTO pss.payment_subtype
(
    payment_subtype_id, 
    payment_subtype_name, 
    payment_subtype_key_name, 
    client_payment_type_cd, 
    payment_subtype_table_name, 
    payment_subtype_key_desc_name, 
    authority_payment_mask_id
)
VALUES
(
    15,
    'Credit Card (Track 1)', 
    'CC_TERMINAL_ID',
    'C',
    'CC_TERMINAL',
    'CC_TERMINAL_NAME',
    24
)
/

UPDATE pss.authority_payment_mask 
SET payment_subtype_id = 15
WHERE authority_payment_mask_id = 24
/

UPDATE pss.authority_payment_mask 
SET authority_payment_mask_regex = '^(;?)([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,20})(\??)([0-9]{0,3})$',
	authority_payment_mask_bref = '2:1|3:3|4:5'
WHERE authority_payment_mask_id = 1
/

UPDATE pss.authority_payment_mask 
SET authority_payment_mask_regex = '^(;?)([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,20})(\??)([0-9]{0,3})$',
	authority_payment_mask_bref = '2:1|3:4|4:5'
WHERE authority_payment_mask_id = 2
/

UPDATE pss.authority_payment_mask 
SET authority_payment_mask_regex = '^(%?)(SEVEND1)([a-zA-Z]+)([0-9]+)\^(.+)$',
	authority_payment_mask_bref = '2:8|3:7|4:1|5:9'
WHERE authority_payment_mask_id = 6
/

UPDATE pss.authority_payment_mask 
SET authority_payment_mask_regex = '^(;?)(99[0-9]{4})([0-9]{10})=(0+)(\??)([0-9]{0,3})$',
	authority_payment_mask_bref = '2:7|3:1|4:8'
WHERE authority_payment_mask_id = 7
/


INSERT INTO device.host_type
(
    host_type_desc, 
    host_type_manufacturer, 
    host_model_cd,
    host_default_complete_minut,
    host_type_cd
)
VALUES
(
    'PublicPC',
    'USA Technologies',
    '1.0',
    '0',
    'P'
)
/


INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(300, 'Start Date')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(301, 'Start Time')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(302, 'Usage Minutes')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(303, 'Printer 1 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(304, 'Printer 2 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(305, 'Service 1 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(306, 'Service 2 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(307, 'Service 3 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(308, 'Service 4 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(309, 'Service 5 Count')
/

INSERT INTO pss.tran_line_item_type(tran_line_item_type_id, tran_line_item_type_desc)
VALUES(310, 'Service 6 Count')
/


CREATE OR REPLACE 
PROCEDURE pss.load_tran_details
(
    l_device_id IN device.device_id%TYPE,
    l_device_type_id IN device.device_type_id%TYPE,
    l_tran_id IN tran.tran_id%TYPE,
    l_tran_details IN VARCHAR2
)
   IS
    l_host_type_id host_type.host_type_id%TYPE;
    l_host_id host.host_id%TYPE;
    l_payload_code VARCHAR2(2);

    l_cnt NUMBER;
    l_pos NUMBER;
    l_list VARCHAR2(1000);

    TYPE payload_type IS TABLE OF VARCHAR2(100)
    INDEX BY BINARY_INTEGER;

    payload_rcd payload_type;
BEGIN
    -- currently transaction payload is implemented for BEX devices only
    IF l_device_type_id <> 11 OR LENGTH(l_tran_details) < 4 THEN
        RETURN;
    END IF;
    
    DELETE FROM tran_line_item
    WHERE tran_id = l_tran_id;

    SELECT host_type_id INTO l_host_type_id
    FROM host_type
    WHERE host_type_desc = 'PublicPC' AND ROWNUM = 1;

    BEGIN
        SELECT host_id INTO l_host_id
        FROM host
        WHERE
            device_id = l_device_id
            AND host_type_id = l_host_type_id
            AND host_active_yn_flag = 'Y'
            AND ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO host
            (
                device_id,
                host_type_id,
                host_status_cd,
                host_serial_cd,
                host_est_complete_minut,
                host_port_num,
                host_setting_updated_yn_flag,
                host_position_num,
                host_active_yn_flag
            )
            VALUES
            (
                l_device_id,
                l_host_type_id,
                1,
                'PublicPC',
                0,
                0,
                'N',
                1,
                'Y'
            );
        
            SELECT host_id INTO l_host_id
            FROM host
            WHERE
                device_id = l_device_id
                AND host_type_id = l_host_type_id
                AND host_active_yn_flag = 'Y'
                AND ROWNUM = 1;
    END;
    
    l_cnt := 0;
    l_list := l_tran_details;

    LOOP
        l_pos := INSTR(l_list, '|');
        l_cnt := l_cnt + 1;
        IF l_pos > 0 THEN
            payload_rcd(l_cnt) := TRIM(SUBSTR(l_list, 1, l_pos - 1));
            l_list := SUBSTR(l_list, l_pos + 1);
        ELSE
            payload_rcd(l_cnt) := TRIM(l_list);
            EXIT;
        END IF;
    END LOOP;
    
    IF payload_rcd(1) IS NULL THEN
        RETURN;
    ELSE
        l_payload_code := SUBSTR(payload_rcd(1), 1, 2);
    END IF;
    
    IF l_payload_code = 'A2' THEN -- long format
        IF l_cnt = 1 THEN
            RETURN;
        ELSIF payload_rcd(2) IS NOT NULL THEN -- Start Date
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 300, payload_rcd(2), 'A');
        END IF;
        
        IF l_cnt = 2 THEN
            RETURN;
        ELSIF payload_rcd(3) IS NOT NULL THEN -- Start Time
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 301, payload_rcd(3), 'A');
        END IF;
        
        IF l_cnt < 5 THEN
            RETURN;
        ELSIF payload_rcd(4) IS NOT NULL AND payload_rcd(5) IS NOT NULL THEN -- Usage Minutes and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 302, 'Usage Minutes', TO_NUMBER(payload_rcd(4)), TO_NUMBER(payload_rcd(5)) / 100, 'A');
        END IF;
        
        IF l_cnt < 7 THEN
            RETURN;
        ELSIF payload_rcd(6) IS NOT NULL AND payload_rcd(7) IS NOT NULL THEN -- Printer 1 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 303, 'Printer 1 Count', TO_NUMBER(payload_rcd(6)), TO_NUMBER(payload_rcd(7)) / 100, 'A');
        END IF;
        
        IF l_cnt < 9 THEN
            RETURN;
        ELSIF payload_rcd(8) IS NOT NULL AND payload_rcd(9) IS NOT NULL THEN -- Printer 2 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 304, 'Printer 2 Count', TO_NUMBER(payload_rcd(8)), TO_NUMBER(payload_rcd(9)) / 100, 'A');
        END IF;
        
        IF l_cnt < 11 THEN
            RETURN;
        ELSIF payload_rcd(10) IS NOT NULL AND payload_rcd(11) IS NOT NULL THEN -- Service 1 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 305, 'Service 1 Count', TO_NUMBER(payload_rcd(10)), TO_NUMBER(payload_rcd(11)) / 100, 'A');
        END IF;
        
        IF l_cnt < 13 THEN
            RETURN;
        ELSIF payload_rcd(12) IS NOT NULL AND payload_rcd(13) IS NOT NULL THEN -- Service 2 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 306, 'Service 2 Count', TO_NUMBER(payload_rcd(12)), TO_NUMBER(payload_rcd(13)) / 100, 'A');
        END IF;
        
        IF l_cnt < 15 THEN
            RETURN;
        ELSIF payload_rcd(14) IS NOT NULL AND payload_rcd(15) IS NOT NULL THEN -- Service 3 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 307, 'Service 3 Count', TO_NUMBER(payload_rcd(14)), TO_NUMBER(payload_rcd(15)) / 100, 'A');
        END IF;
        
        IF l_cnt < 17 THEN
            RETURN;
        ELSIF payload_rcd(16) IS NOT NULL AND payload_rcd(17) IS NOT NULL THEN -- Service 4 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 308, 'Service 4 Count', TO_NUMBER(payload_rcd(16)), TO_NUMBER(payload_rcd(17)) / 100, 'A');
        END IF;
        
        IF l_cnt < 19 THEN
            RETURN;
        ELSIF payload_rcd(18) IS NOT NULL AND payload_rcd(19) IS NOT NULL THEN -- Service 5 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 309, 'Service 5 Count', TO_NUMBER(payload_rcd(18)), TO_NUMBER(payload_rcd(19)) / 100, 'A');
        END IF;
        
        IF l_cnt < 21 THEN
            RETURN;
        ELSIF payload_rcd(20) IS NOT NULL AND payload_rcd(21) IS NOT NULL THEN -- Service 6 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 310, 'Service 6 Count', TO_NUMBER(payload_rcd(20)), TO_NUMBER(payload_rcd(21)) / 100, 'A');
        END IF;
    ELSIF l_payload_code = 'A1' AND l_cnt > 1 THEN -- short format
        IF l_cnt = 1 THEN
            RETURN;
        ELSIF payload_rcd(2) IS NOT NULL THEN -- Usage Minutes
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 302, 'Usage Minutes', TO_NUMBER(payload_rcd(2)), 'A');
        END IF;

        IF l_cnt = 2 THEN
            RETURN;
        ELSIF payload_rcd(3) IS NOT NULL THEN -- Printer 1 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 303, 'Printer 1 Count', TO_NUMBER(payload_rcd(3)), 'A');
        END IF;

        IF l_cnt = 3 THEN
            RETURN;
        ELSIF payload_rcd(4) IS NOT NULL THEN -- Printer 2 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 304, 'Printer 2 Count', TO_NUMBER(payload_rcd(4)), 'A');
        END IF;

        IF l_cnt = 4 THEN
            RETURN;
        ELSIF payload_rcd(5) IS NOT NULL THEN -- Service 1 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 305, 'Service 1 Count', TO_NUMBER(payload_rcd(5)), 'A');
        END IF;

        IF l_cnt = 5 THEN
            RETURN;
        ELSIF payload_rcd(6) IS NOT NULL THEN -- Service 2 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 306, 'Service 2 Count', TO_NUMBER(payload_rcd(6)), 'A');
        END IF;

        IF l_cnt = 6 THEN
            RETURN;
        ELSIF payload_rcd(7) IS NOT NULL THEN -- Service 3 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 307, 'Service 3 Count', TO_NUMBER(payload_rcd(7)), 'A');
        END IF;

        IF l_cnt = 7 THEN
            RETURN;
        ELSIF payload_rcd(8) IS NOT NULL THEN -- Service 4 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 308, 'Service 4 Count', TO_NUMBER(payload_rcd(8)), 'A');
        END IF;

        IF l_cnt = 8 THEN
            RETURN;
        ELSIF payload_rcd(9) IS NOT NULL THEN -- Service 5 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 309, 'Service 5 Count', TO_NUMBER(payload_rcd(9)), 'A');
        END IF;

        IF l_cnt = 9 THEN
            RETURN;
        ELSIF payload_rcd(10) IS NOT NULL THEN -- Service 6 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 310, 'Service 6 Count', TO_NUMBER(payload_rcd(10)), 'A');
        END IF;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        NULL;
END;
/

GRANT EXECUTE ON PSS.LOAD_TRAN_DETAILS TO WEB_USER
/


CREATE TABLE pss.TRAN_CLIENT_TRAN
  (
    TRAN_CLIENT_TRAN_CD    VARCHAR(1),
    TRAN_CLIENT_TRAN_DESC  VARCHAR(60)   not null,
    primary key(TRAN_CLIENT_TRAN_CD)

  );

-- ----------------------------------------------------------------------
COMMENT ON TABLE pss.TRAN_CLIENT_TRAN is 'Contains a listing of all the different states a transaction can be reported as by a client device.';
-- ----------------------------------------------------------------------
DELETE PUBLIC SYNONYM tran_client_tran;
CREATE PUBLIC SYNONYM tran_client_tran FOR pss.tran_client_tran;

INSERT INTO tran_client_tran VALUES ('F', 'Vend failed');
INSERT INTO tran_client_tran VALUES ('C', 'Vend was cancelled by user or vending machine');
INSERT INTO tran_client_tran VALUES ('T', 'Vend timed out');
INSERT INTO tran_client_tran VALUES ('S', 'Vend was successful and receipt printed');
INSERT INTO tran_client_tran VALUES ('R', 'Vend was successful but there was a receipt printing problem');
INSERT INTO tran_client_tran VALUES ('N', 'Vend was successful and no receipt was requested');
INSERT INTO tran_client_tran VALUES ('I', 'Vend was incomplete (all transaction data not available)');
INSERT INTO tran_client_tran VALUES ('Q', 'Vend was successful but Use Printer was set to FALSE');
INSERT INTO tran_client_tran VALUES ('U', 'Unable to authorize, failover turned off');

ALTER TABLE pss.tran ADD (TRAN_CLIENT_TRAN_CD VARCHAR(1));
ALTER TABLE pss.tran ADD CONSTRAINT fk_tran_tran_client_tran FOREIGN KEY (tran_client_tran_cd) REFERENCES pss.tran_client_tran (tran_client_tran_cd);

GRANT SELECT ON PSS.TRAN_CLIENT_TRAN TO WEB_USER
/

-- synchronize database sequences
DECLARE 
    l_id NUMBER;
    l_max_id NUMBER;
BEGIN
    SELECT MAX(authority_payment_mask_id) INTO l_max_id FROM pss.authority_payment_mask;
    
    LOOP
        SELECT pss.seq_authority_payment_mask_id.NEXTVAL INTO l_id FROM dual;
        dbms_output.put_line('seq_authority_payment_mask_id: ' || TO_CHAR(l_id));
        
        IF l_id >= l_max_id THEN
            EXIT;
        END IF;
    END LOOP;
    
    SELECT MAX(payment_subtype_id) INTO l_max_id FROM pss.payment_subtype;
    
    LOOP
        SELECT pss.seq_payment_subtype_id.NEXTVAL INTO l_id FROM dual;
        dbms_output.put_line('seq_payment_subtype_id: ' || TO_CHAR(l_id));
        
        IF l_id >= l_max_id THEN
            EXIT;
        END IF;
    END LOOP;
END;
/

