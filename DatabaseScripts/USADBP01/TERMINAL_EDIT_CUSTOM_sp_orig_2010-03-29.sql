create or replace
PROCEDURE        TERMINAL_EDIT_CUSTOM (l_terminal_id IN TERMINAL.TERMINAL_ID%TYPE,
   l_asset IN TERMINAL.ASSET_NBR%TYPE,
   l_machine_id IN TERMINAL.MACHINE_ID%TYPE,
   l_telephone IN TERMINAL.TELEPHONE%TYPE,
   l_prefix IN TERMINAL.PREFIX%TYPE,
   l_location IN LOCATION.LOCATION_NAME%TYPE,
   l_loc_details IN LOCATION.DESCRIPTION%TYPE,
   l_address_id TERMINAL_ADDR.ADDRESS_ID%TYPE,
   l_address1 IN TERMINAL_ADDR.ADDRESS1%TYPE,
   l_city IN TERMINAL_ADDR.CITY%TYPE,
   l_state IN TERMINAL_ADDR.STATE%TYPE,
   l_zip IN TERMINAL_ADDR.ZIP%TYPE,
   l_cust_bank_id IN USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   l_user_id IN USER_LOGIN.USER_ID%TYPE,
   l_pc_id IN TERMINAL.PRIMARY_CONTACT_ID%TYPE,
   l_sc_id IN TERMINAL.SECONDARY_CONTACT_ID%TYPE,
   l_loc_type_id IN LOCATION.LOCATION_TYPE_ID%TYPE,
   l_loc_type_spec IN LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
   l_prod_type_id IN TERMINAL.PRODUCT_TYPE_ID%TYPE,
   l_prod_type_spec IN TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
   l_auth_mode IN TERMINAL.AUTHORIZATION_MODE%TYPE,
   l_tz_id IN TERMINAL.TIME_ZONE_ID%TYPE,
   l_dex IN TERMINAL.DEX_DATA%TYPE,
   l_receipt TERMINAL.RECEIPT%TYPE,
   l_vends TERMINAL.VENDS%TYPE,
   l_term_var_1 TERMINAL.term_var_1%TYPE,
   l_term_var_2 TERMINAL.term_var_2%TYPE,
   l_term_var_3 TERMINAL.term_var_3%TYPE,
   l_term_var_4 TERMINAL.term_var_4%TYPE,
   l_term_var_5 TERMINAL.term_var_5%TYPE,
   l_term_var_6 TERMINAL.term_var_6%TYPE,
   l_term_var_7 TERMINAL.term_var_7%TYPE,
   l_term_var_8 TERMINAL.term_var_8%TYPE
 )
IS
  l_location_id TERMINAL.LOCATION_ID%TYPE;
  l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
  l_old_cb_id NUMBER;
  l_old_lname LOCATION.LOCATION_NAME%TYPE;
  l_old_ldesc LOCATION.DESCRIPTION%TYPE;
  l_new_address_id TERMINAL_ADDR.ADDRESS_ID%TYPE;
  l_old_address_id LOCATION.ADDRESS_ID%TYPE;
  l_old_dex TERMINAL.DEX_DATA%TYPE;
  isnew BOOLEAN;
  l_date DATE := SYSDATE;
BEGIN
     -- if a new location is named than create it
     SELECT T.LOCATION_ID, T.CUSTOMER_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CUSTOMER_BANK_ID, DEX_DATA
     INTO l_location_id, l_customer_id, l_old_lname, l_old_ldesc, l_old_address_id, l_old_cb_id, l_old_dex
     FROM TERMINAL T, LOCATION L, CORP.VW_CURRENT_BANK_ACCT CB
     WHERE T.TERMINAL_ID = l_terminal_id AND T.LOCATION_ID = L.LOCATION_ID (+) AND T.TERMINAL_ID = CB.TERMINAL_ID (+);
     IF EQL(l_old_lname, l_location) <> -1 THEN  -- location has changed
         UPDATE LOCATION SET status = 'D' WHERE LOCATION_ID = l_location_id;
        INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'LOCATION',l_old_lname, l_location);
        BEGIN
           SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE LOCATION_NAME = l_location AND TERMINAL_ID = l_terminal_id;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
                 isnew := true;
           WHEN OTHERS THEN
              RAISE;
        END;
     END IF;
     --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
     DECLARE
         l_t TERMINAL.TELEPHONE%TYPE;
         l_p TERMINAL.PREFIX%TYPE;
         l_a TERMINAL.AUTHORIZATION_MODE%TYPE;
         l_z TERMINAL.TIME_ZONE_ID%TYPE;
         l_d TERMINAL.DEX_DATA%TYPE;
         l_r TERMINAL.RECEIPT%TYPE;
         l_v TERMINAL.VENDS%TYPE;
         l_cnt NUMBER;
     BEGIN
          SELECT TELEPHONE, PREFIX, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS INTO
             l_t, l_p, l_a, l_z, l_d, l_r, l_v FROM TERMINAL WHERE TERMINAL_ID = l_terminal_id;
         IF EQL(l_t, l_telephone) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_telephone WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'TELEPHONE' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                     VALUES(l_terminal_id, 'TELEPHONE',l_t, l_telephone);
            END IF;
         END IF;
         IF EQL(l_p, l_prefix) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_prefix WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'PREFIX' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'PREFIX',l_p, l_prefix);
            END IF;
         END IF;
         IF EQL(l_a, l_auth_mode) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_auth_mode WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'AUTHORIZATION_MODE' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'AUTHORIZATION_MODE',l_a, l_auth_mode);
            END IF;
         END IF;
         IF EQL(l_z, l_tz_id) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_tz_id WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'TIME_ZONE',l_z, l_tz_id);
            END IF;
         END IF;
         IF EQL(l_d, l_dex) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_dex WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'DEX_DATA' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'DEX_DATA',l_d, l_dex);
            END IF;
         END IF;
         IF EQL(l_r, l_receipt) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_receipt WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'RECEIPT' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'RECEIPT',l_r, l_receipt);
            END IF;
         END IF;
         IF EQL(l_v, l_vends) <> -1 THEN
             UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_vends WHERE TERMINAL_ID = l_terminal_id
               AND ATTRIBUTE = 'VENDS' RETURNING 1 INTO l_cnt;
            IF NVL(l_cnt, 0) = 0 THEN
                INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
               VALUES(l_terminal_id, 'VENDS',l_v, l_vends);
            END IF;
         END IF;
     END;
     -- UPDATE ADDRESS
     IF l_address_id IS NULL THEN    -- none specified
        l_new_address_id := NULL;
     ELSIF l_address_id = 0 THEN    --IS NEW
        SELECT TERMINAL_ADDR_SEQ.NEXTVAL INTO l_new_address_id FROM DUAL;
        INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP)
               VALUES(l_new_address_id, l_customer_id, l_address1, l_city, l_state, l_zip);
     ELSE
         UPDATE TERMINAL_ADDR SET ADDRESS1 = l_address1, CITY = l_city, STATE = l_state, ZIP = l_zip
            WHERE ADDRESS_ID = l_address_id AND (EQL(ADDRESS1, l_address1) = 0
            OR EQL(CITY, l_city) = 0 OR EQL(STATE, l_state) = 0 OR EQL(ZIP, l_zip) = 0);
        l_new_address_id := l_address_id;
     END IF;
     IF isnew THEN
         SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
         INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
               VALUES(l_location_id,l_location,l_loc_details,l_new_address_id,l_user_id,l_terminal_id,l_loc_type_id,l_loc_type_spec);
     ELSE
         UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
               (SELECT l_loc_details,l_new_address_id,l_date,l_loc_type_id,l_loc_type_spec FROM DUAL) WHERE location_id = l_location_id;
     END IF;
      UPDATE TERMINAL SET (LOCATION_ID, ASSET_NBR, MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY,
         PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS, STATUS,
         TERM_VAR_1, TERM_VAR_2, TERM_VAR_3, TERM_VAR_4, TERM_VAR_5, TERM_VAR_6, TERM_VAR_7, TERM_VAR_8) =
        (SELECT l_location_id, l_asset, l_machine_id, l_telephone, l_prefix, l_prod_type_id, l_prod_type_spec,
         l_pc_id, l_sc_id, l_auth_mode, l_tz_id, l_dex, l_receipt, l_vends, 'U',
         l_term_var_1, l_term_var_2, l_term_var_3, l_term_var_4, l_term_var_5, l_term_var_6, l_term_var_7, l_term_var_8 FROM DUAL)
         WHERE TERMINAL_ID = l_terminal_id;
     --UPDATE BANK ACCOUNT
     IF EQL(l_old_cb_id, l_cust_bank_id) = 0 THEN
         DECLARE
             l_cb_date DATE;
        BEGIN
            UPDATE CORP.CUSTOMER_BANK_TERMINAL SET END_DATE = CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END
             WHERE TERMINAL_ID = l_terminal_id
             RETURNING MAX(CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END) INTO l_cb_date;
            IF NVL(l_cust_bank_id, 0) <> 0 THEN
                   INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
                    VALUES(CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_cb_date);
            END IF;
        END;
     END IF;
     /*
     IF NVL(l_old_cb_id, 0) = 0 AND NVL(l_cust_bank_id, 0) <> 0 THEN
         DECLARE
             l_cb_date DATE;
        BEGIN
             SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = l_terminal_id AND CLOSE_DATE >=
                 (SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CORP.CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = l_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
             CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(l_terminal_id, l_cust_bank_id, l_cb_date);
        END;
     ELSIF EQL(l_old_cb_id, l_cust_bank_id) = 0 THEN
         CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(l_terminal_id, l_cust_bank_id, l_date);
     END IF;
     */
     IF l_old_dex = 'N' AND l_dex IN('D','A')  THEN
         DECLARE
            ln_service_fee_id CORP.SERVICE_FEES.SERVICE_FEE_ID%TYPE;
            lc_copy CHAR(1);
         BEGIN
            SELECT SERVICE_FEE_ID, DECODE(END_DATE, NULL, 'N', 'Y')
              INTO ln_service_fee_id, lc_copy
              FROM (SELECT SERVICE_FEE_ID, END_DATE
                      FROM CORP.SERVICE_FEES
                     WHERE TERMINAL_ID = l_terminal_id
                       AND FEE_ID = 4
                     ORDER BY NVL(END_DATE, MAX_DATE) DESC, LAST_PAYMENT DESC, SERVICE_FEE_ID DESC)
             WHERE ROWNUM = 1;
            IF lc_copy = 'Y' THEN
            INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                 SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, GREATEST(l_date, END_DATE)
                   FROM CORP.SERVICE_FEES
                  WHERE SERVICE_FEE_ID = ln_service_fee_id;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
                 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
         END;
     ELSIF l_dex = 'N' AND l_old_dex IN('D','A')  THEN
          UPDATE CORP.SERVICE_FEES SET END_DATE = l_date WHERE TERMINAL_ID = l_terminal_id
              AND FEE_ID = 4;
     END IF;
EXCEPTION
     WHEN NO_DATA_FOUND THEN
          RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
     WHEN OTHERS THEN
         ROLLBACK;
         RAISE;
END TERMINAL_EDIT_CUSTOM;