-- This script will disable Local Auth for G4 and Gx devices
DECLARE
    i_debug INT;
    return_code NUMBER(20);
    return_msg  VARCHAR2(2048);
    CURSOR cur IS 
        select max(d.device_id) device_id, d.device_name, d.device_type_id
		from device.file_transfer ft join device.device d on ft.file_transfer_name = d.device_name || '-CFG'
		where d.device_type_id in (0, 1) and upper(file_content_substr(ft.rowid, 387, 1)) != '4E'
		group by d.device_name, d.device_type_id;
BEGIN
    i_debug := 0;
    DBMS_OUTPUT.put_line('Script is starting, debug: ' || i_debug || '...');
    
    FOR rec_cur IN cur LOOP
        return_code := -2;
        return_msg := 'Did not run.';
       
        sp_update_config(rec_cur.device_id, 387, '4E', 'H', rec_cur.device_type_id, i_debug, return_code, return_msg);
        IF return_code < 0 THEN
            DBMS_OUTPUT.put_line('** Device update failed (Disabling Local Auth) (device_id: ' || rec_cur.device_id || '): ' || return_msg);
        ELSE
            DBMS_OUTPUT.put_line('device_id: ' || rec_cur.device_id || ': Local Auth has been disabled');
        END IF;
    END LOOP;
	
	DBMS_OUTPUT.put_line('Script finished.');
	COMMIT;
END;
