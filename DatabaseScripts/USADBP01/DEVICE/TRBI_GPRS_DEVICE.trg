CREATE OR REPLACE TRIGGER device.trbi_gprs_device
BEFORE INSERT ON device.gprs_device
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.gprs_device_id IS NULL
   THEN
      SELECT seq_gprs_device_id.NEXTVAL
        INTO :NEW.gprs_device_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE, USER, SYSDATE,
          USER
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END; 
/