CREATE OR REPLACE TRIGGER device.trbi_host
BEFORE INSERT ON device.host
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF :new.HOST_id IS NULL THEN
      SELECT seq_HOST_id.nextval
        into :new.HOST_id
        FROM dual;
    END IF;

	SELECT sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
	  
	IF :NEW.HOST_ACTIVE_YN_FLAG = 'Y' THEN
		:NEW.HOST_ACTIVATION_TS := SYSDATE;
	ELSIF :NEW.HOST_ACTIVE_YN_FLAG = 'N' THEN
		:NEW.HOST_DEACTIVATION_TS := SYSDATE;
	END IF;
END;
/