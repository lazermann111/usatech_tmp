CREATE OR REPLACE TRIGGER device.trbi_host_counter
BEFORE INSERT ON device.host_counter
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.host_counter_id IS NULL
	THEN
		SELECT SEQ_HOST_COUNTER_ID.NEXTVAL
		INTO :NEW.host_counter_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/