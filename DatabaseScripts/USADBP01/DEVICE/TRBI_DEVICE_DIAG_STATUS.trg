CREATE OR REPLACE TRIGGER device.trbi_device_diag_status
BEFORE INSERT ON device.device_diag_status
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.DEVICE_DIAG_STATUS_id IS NULL THEN

      SELECT seq_DEVICE_DIAG_STATUS_id.nextval
        into :new.DEVICE_DIAG_STATUS_id
        FROM dual;

    END IF;
    
 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;

/