CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_CONVERSIONS IS

    ex_conversion_error             EXCEPTION;

    FUNCTION hex_to_string
    (
        p_hex IN VARCHAR2
    )
    RETURN VARCHAR2 IS
        l_hex_len   INT;
        l_index     INT;
        l_str       VARCHAR2(32767);
    BEGIN
        l_hex_len := LENGTH(p_hex);
        IF MOD(l_hex_len, 2) <> 0 THEN
            RAISE ex_conversion_error;
        END IF;
        FOR l_index IN 1 .. l_hex_len LOOP
            IF MOD(l_index,2) = 1 THEN
                l_str := l_str || CHR(to_dec(SUBSTR(p_hex,l_index,2)));
            END IF;
        END LOOP;
        RETURN l_str;
    END hex_to_string;

    FUNCTION string_to_hex
    (
        p_str IN VARCHAR2
    )
    RETURN VARCHAR2 IS
        l_str_len   INT;
        l_index     INT;
        l_hex       VARCHAR2(32767);
    BEGIN
        l_str_len := LENGTH(p_str);
        IF l_str_len > (32767/2) THEN
            RAISE ex_conversion_error;
        END IF;
        FOR l_index IN 1 .. l_str_len LOOP
            l_hex := l_hex || to_hex(ASCII(SUBSTR(p_str,l_index,1)));
        END LOOP;
        RETURN UPPER(l_hex);
    END string_to_hex;

    FUNCTION to_bin
    (
        p_dec IN NUMBER
    )
    RETURN VARCHAR2 IS
    BEGIN
	   RETURN to_base(p_dec, 2);
    END to_bin;

    FUNCTION to_dec
    (
        p_str IN VARCHAR2,
        p_from_base IN NUMBER DEFAULT 16
    )
    RETURN NUMBER IS
        l_num   NUMBER DEFAULT 0;
        l_hex   VARCHAR2(16) DEFAULT '0123456789ABCDEF';
    BEGIN
        FOR i IN 1 .. LENGTH(p_str) LOOP
            l_num := l_num * p_from_base + INSTR(l_hex,UPPER(SUBSTR(p_str,i,1)))-1;
        END LOOP;
        RETURN l_num;
    END to_dec;

    FUNCTION to_hex
    (
        p_dec IN NUMBER
    )
    RETURN VARCHAR2 IS
    BEGIN
        RETURN to_base(p_dec, 16);
    END to_hex;

    FUNCTION to_oct
    (
        p_dec IN NUMBER
    )
    RETURN VARCHAR2 IS
    BEGIN
	   RETURN to_base(p_dec, 8);
    END to_oct;

    FUNCTION to_base
    (
        p_dec IN NUMBER,
        p_base IN NUMBER
    )
    RETURN VARCHAR2 IS
        l_str	VARCHAR2(255) DEFAULT NULL;
        l_num	NUMBER        DEFAULT p_dec;
        l_hex	VARCHAR2(16)  DEFAULT '0123456789ABCDEF';
    BEGIN
        IF (TRUNC(p_dec) <> p_dec OR p_dec < 0) THEN
            RAISE ex_conversion_error;
        END IF;
        LOOP
            l_str := SUBSTR(l_hex, MOD(l_num,p_base)+1, 1) || l_str;
            l_num := TRUNC(l_num/p_base);
            EXIT WHEN (l_num = 0);
        END LOOP;
        RETURN l_str;
    END to_base;

END;
/
