CREATE OR REPLACE FUNCTION DEVICE.DEVICE_HOST_COUNT (
   l_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
   l_month DATE)
 RETURN PLS_INTEGER
IS
  l_count PLS_INTEGER;
BEGIN
 SELECT b.HOST_COUNT
   INTO l_count
   FROM (SELECT
          a.POS_ACTIVATION_TS,
          LAG(a.POS_ACTIVATION_TS, 1, ADD_MONTHS(SYSDATE, 1)) OVER (ORDER BY a.POS_ACTIVATION_TS DESC) POS_DEACTIVATION_TS,
          a.HOST_COUNT
          FROM (
              SELECT
              POS.POS_ACTIVATION_TS,
              COUNT(dev.DEVICE_ID) HOST_COUNT
              FROM PSS.POS pos
              JOIN DEVICE.DEVICE dev
              ON pos.DEVICE_ID = dev.DEVICE_ID
              --AND dev.DEVICE_TYPE_ID = 5
              JOIN DEVICE.HOST hos
              ON hos.DEVICE_ID = dev.DEVICE_ID
              JOIN DEVICE.HOST_TYPE ht
              ON ht.HOST_TYPE_ID = hos.HOST_TYPE_ID
              JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE htgt
              ON htgt.HOST_TYPE_ID = hos.HOST_TYPE_ID
              JOIN DEVICE.HOST_GROUP_TYPE hgt
              ON hgt.HOST_GROUP_TYPE_ID = htgt.HOST_GROUP_TYPE_ID
              WHERE hgt.HOST_GROUP_TYPE_CD <> 'BASE_HOST'
                AND dev.DEVICE_SERIAL_CD = l_device_serial_cd
              GROUP BY pos.POS_ACTIVATION_TS
          ) a
          ORDER BY a.POS_ACTIVATION_TS
    ) b
    WHERE TRUNC(b.POS_ACTIVATION_TS, 'MM') < TRUNC(ADD_MONTHS(l_month, 1), 'MM')
    AND TRUNC(b.POS_DEACTIVATION_TS, 'MM') >= TRUNC(ADD_MONTHS(l_month, 1), 'MM');
    RETURN l_count;
END;
/