CREATE OR REPLACE TRIGGER DEVICE.TRAU_CREDENTIAL 
AFTER UPDATE ON DEVICE.CREDENTIAL
FOR EACH ROW
DECLARE
	LN_CURRENT_CREDENTIAL_CHANGE NUMBER := 0;
BEGIN
	IF DBADMIN.PKG_UTL.EQL(:NEW.USERNAME, :OLD.USERNAME) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.PASSWORD_HASH, :OLD.PASSWORD_HASH) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.PASSWORD_SALT, :OLD.PASSWORD_SALT) = 'N' THEN
		LN_CURRENT_CREDENTIAL_CHANGE := 1;
		
		INSERT INTO DEVICE.CREDENTIAL_HIST(CREDENTIAL_ID, USERNAME, PASSWORD_HASH, PASSWORD_SALT, CREDENTIAL_UTC_TS, USERNAME_PASSWORD_CREATED_BY)
        VALUES(:OLD.CREDENTIAL_ID, :OLD.USERNAME, :OLD.PASSWORD_HASH, :OLD.PASSWORD_SALT, :OLD.CREDENTIAL_UTC_TS, :OLD.USERNAME_PASSWORD_CREATED_BY);
		
		INSERT INTO DEVICE.DEVICE_INFO(DEVICE_NAME, TEMP_PASSCODE_YN_FLAG)
        SELECT DEVICE_NAME, 'C'
          FROM DEVICE.DEVICE D
         WHERE CREDENTIAL_ID = :NEW.CREDENTIAL_ID 
           AND NOT EXISTS (
                SELECT 1
                  FROM DEVICE.DEVICE_INFO
                 WHERE DEVICE_NAME = D.DEVICE_NAME);
	END IF;
	
	IF LN_CURRENT_CREDENTIAL_CHANGE = 1
		OR DBADMIN.PKG_UTL.EQL(:NEW.PREVIOUS_PASSWORD_HASH, :OLD.PREVIOUS_PASSWORD_HASH) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.PREVIOUS_PASSWORD_SALT, :OLD.PREVIOUS_PASSWORD_SALT) = 'N' THEN
		ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
			'CREDENTIAL',
			'DEVICE.CREDENTIAL',
			'U',
			:NEW.CREDENTIAL_ID,
			:NEW.CREDENTIAL_ID
		);
	END IF;
END;
/