CREATE OR REPLACE FUNCTION DEVICE.SF_GENERATE_DEVICE_NAME(
    pn_device_type_id IN DEVICE_TYPE.DEVICE_TYPE_ID%TYPE,
    pn_device_sub_type_id IN DEVICE_SUB_TYPE.DEVICE_SUB_TYPE_ID%TYPE)
RETURN VARCHAR2 IS
    ls_device_name VARCHAR2(8);
BEGIN
    IF pn_device_type_id = 13 THEN
        SELECT 'TD' || LPAD(SEQ_TD_DEVICE_NAME.NEXTVAL, 6, '0') INTO ls_device_name FROM DUAL;
    ELSIF pn_device_type_id = 11 AND pn_device_sub_type_id = 3 THEN
        SELECT 'WS' || LPAD(SEQ_WS_DEVICE_NAME.NEXTVAL, 6, '0') INTO ls_device_name FROM DUAL;
    ELSE
        SELECT 'EV' || LPAD(RERIX_MACHINE_ID_SEQ.NEXTVAL, 6, '0') INTO ls_device_name FROM DUAL;
    END IF;

    RETURN ls_device_name;
END;
/