CREATE OR REPLACE TRIGGER device.trbi_device
BEFORE INSERT ON device.device
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.device_id IS NULL
   THEN
      SELECT seq_device_id.NEXTVAL
        INTO :NEW.device_id
        FROM DUAL;
   END IF;
   
   IF :NEW.CMD_PENDING_CNT < 0 THEN
		:NEW.CMD_PENDING_CNT := 0;
   END IF;   

   SELECT SYSDATE, USER, SYSDATE,
          USER, NVL (:NEW.device_active_yn_flag, 'Y'),
          SYSDATE
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by, :NEW.device_active_yn_flag,
          :NEW.device_encr_key_gen_ts
     FROM DUAL;
END;
/