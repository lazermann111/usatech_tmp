/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will implement vend counters for the call in record. It includes a table update and updates a stored procedure in a package. 

Created By:
J Bradley 

Date:
2005-07-18 

Grant Privileges (run as):
SYSTEM 

*/ 



-- DEVICE_CALL_IN_RECORD - altering table, adding 3 new fields
@DEVICE.DEVICE_CALL_IN_RECORD_alter.sql;

-- call in package - update to the sp_add_trans stored proc (for the above)
@DEVICE.PKG_CALL_IN_RECORD_MAINT_ddl.sql;


