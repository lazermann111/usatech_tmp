ALTER TABLE device.device_call_in_record
ADD (last_auth_in_ts DATE)
ADD (last_trans_in_ts DATE)
ADD (last_message_out_ts DATE);
