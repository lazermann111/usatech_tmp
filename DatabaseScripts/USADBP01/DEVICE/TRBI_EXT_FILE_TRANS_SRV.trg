CREATE OR REPLACE TRIGGER device.trbi_ext_file_trans_srv
BEFORE INSERT ON device.ext_file_trans_srv
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/