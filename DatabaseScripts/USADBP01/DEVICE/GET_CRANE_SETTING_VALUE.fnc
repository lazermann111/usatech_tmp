CREATE OR REPLACE FUNCTION DEVICE.GET_CRANE_SETTING_VALUE (pn_device_id IN NUMBER, pv_device_setting_type IN VARCHAR)
   RETURN varchar
IS
   l_device_setting_value varchar(100);
   ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
  
BEGIN
   SELECT MAX(D.DEVICE_TYPE_ID)
		INTO ln_device_type_id
		FROM DEVICE.DEVICE D
		WHERE D.DEVICE_ID = pn_device_id and D.DEVICE_ACTIVE_YN_FLAG='Y';

   
   IF (ln_device_type_id = 11 or pv_device_setting_type = 'Cashless Enabled') and pv_device_setting_type!='Two-Tier Pricing' THEN
   		select max(device_setting_value) into l_device_setting_value from device.device_setting where device_id=pn_device_id and device_setting_parameter_cd=pv_device_setting_type;
      IF pv_device_setting_type = 'Cashless Enabled' and l_device_setting_value is null THEN
        SELECT CASE WHEN MAX(PP.POS_PTA_ID) IS NULL THEN 'N' ELSE 'Y' END into l_device_setting_value
        FROM PSS.POS P
        JOIN PSS.POS_PTA PP ON P.POS_ID=PP.POS_ID AND NVL(PP.POS_PTA_ACTIVATION_TS, MAX_DATE)<=SYSDATE AND NVL(PP.POS_PTA_DEACTIVATION_TS, MAX_DATE)>SYSDATE
        JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID=PS.PAYMENT_SUBTYPE_ID AND PAYMENT_SUBTYPE_CLASS<>'Authority::NOP'
        JOIN REPORT.TRANS_TYPE TT ON PS.TRANS_TYPE_ID=TT.TRANS_TYPE_ID AND TT.CASH_IND='N'
        WHERE P.DEVICE_ID=pn_device_id;
      END IF;
   		return l_device_setting_value;
   END IF;
   IF pv_device_setting_type = 'Cash Enabled' THEN
      SELECT max(case when d.device_type_id =13 THEN 
      CASE WHEN TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '1022')) = 1 THEN 'Y' ELSE 'N' END
     when d.device_type_id =0 or d.device_type_id=1 THEN 
     CASE WHEN TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '203')) = 55 THEN 'Y' ELSE 'N' END
     when d.device_type_id =11 and d.device_serial_cd like 'K3CMS%' THEN 
     PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, 'Cash Enabled')
     END )
      INTO l_device_setting_value
    FROM device.device_setting ds
      JOIN device.device d
      ON ds.device_id = d.device_id
    JOIN device.device_setting_parameter dsp
    ON ds.device_setting_parameter_cd = dsp.device_setting_parameter_cd
    WHERE d.device_id=pn_device_id;
   ELSIF pv_device_setting_type = 'Two-Tier Pricing' THEN
      SELECT case when d.device_type_id =13 THEN max(case when ds.device_setting_parameter_cd='1202' THEN CASE when cts.stored_in_pennies = 'Y' AND DBADMIN.TO_NUMBER_OR_NULL(ds.device_setting_value) IS NOT NULL THEN TO_CHAR(TO_NUMBER(ds.device_setting_value) / 100, 'FM999,999,999,999,990.00')
                          ELSE ds.device_setting_value END END)
     when d.device_type_id =0 or d.device_type_id=1 THEN max(case when ds.device_setting_parameter_cd='362' THEN case when cts.stored_in_pennies = 'Y' AND DBADMIN.TO_NUMBER_OR_NULL(ds.device_setting_value) IS NOT NULL THEN TO_CHAR(TO_NUMBER(ds.device_setting_value) / 100, 'FM999,999,999,999,990.00')
                          ELSE ds.device_setting_value END END)
     when d.device_type_id =11 and d.device_serial_cd like 'K3CMS%' THEN max(case when ds.device_setting_parameter_cd='Two-Tier Pricing' THEN case when cts.stored_in_pennies = 'Y' AND DBADMIN.TO_NUMBER_OR_NULL(ds.device_setting_value) IS NOT NULL THEN TO_CHAR(TO_NUMBER(ds.device_setting_value) / 100, 'FM999,999,999,999,990.00')
                          ELSE ds.device_setting_value END END) 
     END ||'|'||coalesce(max(case when ds.device_setting_parameter_cd='Two-Tier Pricing Vend/Tran Flag' then ds.device_setting_value end ),'V') 
      INTO l_device_setting_value
    FROM device.device_setting ds
      JOIN device.device d
      ON ds.device_id = d.device_id
    JOIN device.device_setting_parameter dsp
    ON ds.device_setting_parameter_cd = dsp.device_setting_parameter_cd
    LEFT OUTER JOIN device.config_template_setting cts
    ON DECODE(d.device_type_id, 1, 0, d.device_type_id) = cts.device_type_id
    AND ds.device_setting_parameter_cd = cts.device_setting_parameter_cd
    WHERE d.device_id =pn_device_id
    group by d.device_id, d.device_type_id,device_serial_cd;
    
   ELSIF  pv_device_setting_type = 'DEX Schedule' THEN
     SELECT max(case when d.device_type_id =0  THEN
     CASE WHEN TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '385')) = 59 and TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '172')) is not null 
     THEN 'D^'||LPAD(TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '172')),4,0) END
     WHEN d.device_type_id =1 and DBADMIN.VERSION_COMPARE(SUBSTR(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, 'Firmware Version'), 9),'6.0.7')<0 THEN
      CASE WHEN TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '385')) = 59 and TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '172')) is not null 
     THEN 'D^'||LPAD(TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '172')),4,0) END
     WHEN d.device_type_id =1 and DBADMIN.VERSION_COMPARE(SUBSTR(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, 'Firmware Version'), 9),'6.0.7')>=0 THEN
      CASE WHEN TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '358')) is not null THEN
        CASE WHEN TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '244')) is null or TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '244'))=0 THEN
          'D^'||LPAD(TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '358')),4,0)
        ELSE
          'I^'||LPAD(TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '358')),4,0)||'^'||TO_NUMBER_OR_NULL(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '244'))
        END
      END
      WHEN d.device_type_id =13 THEN
        CASE WHEN regexp_like(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '1101'), '^D|W|M.*') THEN
        PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '1101')
        WHEN regexp_like(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '1101'), '^I.*') THEN
          GET_DEX_INTERVAL_SCHEDULE(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(d.device_id, '1101'),DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN('GMT', COALESCE(T_TZ.TIME_ZONE_GUID, TZ.TIME_ZONE_GUID, 'US/Eastern')))
        END
     END )
      INTO l_device_setting_value
    FROM device.device_setting ds
      JOIN device.device d
      ON ds.device_id = d.device_id
    JOIN device.device_setting_parameter dsp
    ON ds.device_setting_parameter_cd = dsp.device_setting_parameter_cd
    LEFT OUTER JOIN pss.pos p
          ON d.device_id = p.device_id
         LEFT OUTER JOIN location.customer c
          ON p.customer_id = c.customer_id
         LEFT OUTER JOIN location.location l
          ON p.location_id = l.location_id
          LEFT OUTER JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
         LEFT OUTER JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID AND SYSDATE>= COALESCE(TE.START_DATE, MIN_DATE) AND SYSDATE < COALESCE(TE.END_DATE, MAX_DATE)
         LEFT OUTER JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
          LEFT OUTER JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
         LEFT OUTER JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
    WHERE d.device_id =pn_device_id;
   END IF;
   
   return l_device_setting_value;
END;
/
