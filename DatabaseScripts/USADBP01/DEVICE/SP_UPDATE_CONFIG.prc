CREATE OR REPLACE PROCEDURE DEVICE.SP_UPDATE_CONFIG
(
    p_device_id         IN NUMBER,
    p_offset            IN INT,
    p_new_data          IN VARCHAR2,
    p_data_type         IN CHAR,
    p_device_type_id    IN NUMBER,
    p_debug             IN INT,
    p_return_code       OUT NUMBER,
    p_return_msg        OUT VARCHAR2
) IS
    l_ev_number         VARCHAR2 (12);
    l_file_transfer_id  NUMBER;
    l_orig_complete     VARCHAR2 (32000);
    l_substr_length     INT;
    l_orig_substr       VARCHAR2 (32000);
    l_file_offset       INT;
    l_replace_with      VARCHAR2 (32000);
    l_new_complete      VARCHAR2 (32000);
    l_poke_location     NUMBER(5,1);
    l_poke_length       INT;
    l_poke_command      VARCHAR2 (32000);
    l_pending_count     INT;
    l_execute_order     INT;
    
BEGIN

    BEGIN
        -- load data into a varchar for usage
        -- entire file is hex encoded
        SELECT device_name, file_transfer_id, file_transfer_content
        INTO l_ev_number, l_file_transfer_id, l_orig_complete
        FROM (
        SELECT device.device_name, file_transfer.file_transfer_id, file_transfer.file_transfer_content
        FROM device, file_transfer
        WHERE device.device_id = p_device_id
        AND device.device_name || '-CFG' = file_transfer.file_transfer_name
        AND file_transfer.file_transfer_type_cd = 1
        ORDER BY file_transfer.created_ts
        ) WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_return_code := -1;
            p_return_msg := 'Device or configuration record not found for device_id: ' || p_device_id;
            RETURN;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    p_return_msg := p_device_id || ',' || l_ev_number || ',' || p_offset || ': ';

    -- expect hex data will be passed in hex encoded, ascii and choice data will not
    IF p_data_type = 'H' THEN
        l_replace_with := p_new_data;
    ELSE
        l_replace_with := pkg_conversions.string_to_hex(p_new_data);
    END IF;

    l_substr_length := LENGTH(l_replace_with);

    -- expect p_offset refers to actual data location
    l_orig_substr := SUBSTR(l_orig_complete, ((p_offset*2)+1), l_substr_length);

    IF p_data_type = 'H' THEN
        p_return_msg := p_return_msg || UPPER(l_orig_substr) || ' = ' || UPPER(l_replace_with);
    ELSE
        p_return_msg := p_return_msg || pkg_conversions.hex_to_string(l_orig_substr) || ' = ' || pkg_conversions.hex_to_string(l_replace_with);
    END IF;

    -- compare the hex values to see if there was a change
    IF UPPER(l_orig_substr) = UPPER(l_replace_with) THEN
        p_return_code := 0;
        p_return_msg :=  p_return_msg || ': TRUE - NO CHANGE!';
        RETURN;
    END IF;

    p_return_msg :=  p_return_msg || ': FALSE - CHANGED!';
    p_return_code := 1;

    -- there was a change, so replace the original substring with the new one
    l_new_complete := substr(l_orig_complete, 0,(p_offset*2)) || l_replace_with || substr(l_orig_complete, ((p_offset*2)+l_substr_length+1));

    -- now we need to create a pending Poke for this change
    l_poke_length := (l_substr_length/2);

    IF p_device_type_id IN (0,1) THEN
        -- G4/G5 devices addresses memory in words
        l_poke_location := trunc((p_offset/2));
        -- must send at least 2 bytes at a time to a G4/G5
        IF l_poke_length = 1 THEN
            l_poke_length := 2;
        END IF;
    ELSIF p_device_type_id IN (6) THEN
        -- MEI device should always Poke the entire config
        l_poke_location := 0;
        l_poke_length := (length(l_new_complete)/2);
    ELSE
        l_poke_location := p_offset;
    END IF;
    
    l_poke_command := '42' || lpad(pkg_conversions.to_hex(l_poke_location), 8, '0') || lpad(pkg_conversions.to_hex(l_poke_length), 8, '0');

    -- check if the Poke already exists; no need to send 2 pokes for the same memory segment
    SELECT count(1)
    INTO l_pending_count
    FROM machine_command_pending
    WHERE machine_id = l_ev_number
    AND data_type = '88'
    AND command = l_poke_command
    AND execute_cd IN ('P', 'S');

    -- query for the max pending command execute_order
    -- we want to put the Poke at the end of the pending list
    SELECT nvl(max(execute_order)+1, 0)
    INTO l_execute_order
    FROM machine_command_pending
    WHERE machine_id = l_ev_number
    AND execute_cd IN ('P', 'S');

    p_return_msg := p_return_msg || ' - POKE: ' || l_ev_number || ' 88 ' || l_poke_command || ' P ' || l_execute_order;

    -- if not debugging then update the file contents and save the poke
    IF l_pending_count = 0 THEN
        p_return_msg := p_return_msg || ', poke does not exist';
        IF p_debug = 0 THEN
            INSERT INTO machine_command_pending(machine_id, data_type, command, execute_cd, execute_order)
            VALUES (l_ev_number, '88', l_poke_command, 'P', l_execute_order);
            p_return_msg := p_return_msg || ', poke inserted';
        ELSE
            p_return_msg := p_return_msg || ', poke not inserted (debug on)';
        END IF;
    ELSE
        p_return_msg := p_return_msg || ', poke not inserted (already exists)';
    END IF;

    IF p_debug = 0 THEN
        UPDATE file_transfer set file_transfer_content = l_new_complete where file_transfer_id = l_file_transfer_id;
        p_return_msg := p_return_msg || ', file_transfer updated';
    ELSE
        p_return_msg := p_return_msg || ', file_transfer not updated (debug on)';
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            p_return_code := -1;
            p_return_msg := 'Unexpected error occured! ' || SQLCODE || ': ' || SQLERRM;
            RETURN;
        END;
END;
/