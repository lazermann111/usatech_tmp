/* 
Description:


This is an update implementation to add additional timestamps for call in record metrics tracking. The additional timestamp fields are last_auth_in_ts (last auth message sent), last_trans_in_ts (last transaction message sent) and last_message_out_ts (last message sent back to device). It includes a table update and updates stored procedures in a package. 

Created By:
J Bradley 

Date:
2005-10-18 

Grant Privileges (run as):
SYSTEM 

*/ 



-- DEVICE_CALL_IN_RECORD - altering table, adding 3 new fields
@DEVICE.DEVICE_CALL_IN_RECORD_alter2.sql;

-- call in package - update to the sp_add_auth, sp_add_trans and sp_add_message stored procs (for the above)
@DEVICE.PKG_CALL_IN_RECORD_MAINT_ddl.sql;


