CREATE OR REPLACE TRIGGER device.trbi_device_type_host_type
BEFORE INSERT ON device.device_type_host_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT SYSDATE, USER, SYSDATE,
          USER
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/