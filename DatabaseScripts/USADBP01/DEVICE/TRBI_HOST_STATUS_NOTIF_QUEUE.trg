CREATE OR REPLACE TRIGGER device.trbi_host_status_notif_queue
BEFORE INSERT ON device.host_status_notif_queue
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.HOST_STATUS_NOTIF_QUEUE_id IS NULL THEN

      SELECT seq_HOST_STATUS_NOTIF_QUEUE_id.nextval
        into :new.HOST_STATUS_NOTIF_QUEUE_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;

/