CREATE OR REPLACE TRIGGER device.trbi_device_default
BEFORE INSERT ON device.device_default
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.device_default_id IS NULL
	THEN
		SELECT SEQ_DEVICE_ID.NEXTVAL
		INTO :NEW.device_default_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/