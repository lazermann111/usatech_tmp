CREATE OR REPLACE TRIGGER device.trbi_device_setting
BEFORE INSERT ON device.device_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	LN_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
Begin
	SELECT DEVICE_TYPE_ID
	INTO LN_DEVICE_TYPE_ID
	FROM DEVICE.DEVICE
	WHERE DEVICE_ID = :NEW.DEVICE_ID;

 SELECT    sysdate,
           user,
           sysdate,
           user		   
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by		   
      FROM dual;
	  
	  IF :new.device_setting_parameter_cd NOT IN ('1203', '1204', '1205', '1206', '1527', '1528') THEN
		:new.device_setting_value := TRIM(:new.device_setting_value);
	  END IF;
	  
	  IF LN_DEVICE_TYPE_ID IN (0, 1) THEN
		IF :NEW.DEVICE_SETTING_PARAMETER_CD = '387' THEN
			-- disable local auth
			:NEW.DEVICE_SETTING_VALUE := '4E';
		ELSIF :NEW.DEVICE_SETTING_PARAMETER_CD = '389' THEN
			-- force hex MDB Inventory Format
			:NEW.DEVICE_SETTING_VALUE := '59';
		END IF;
	  END IF;
End;

/