CREATE OR REPLACE TRIGGER device.trbi_action_param
BEFORE INSERT ON device.action_param
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.action_param_id IS NULL
   THEN
      SELECT SEQ_ACTION_PARAM_ID.NEXTVAL
        INTO :NEW.action_param_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/