CREATE OR REPLACE TRIGGER device.trbi_gprs_device_hist
BEFORE INSERT ON device.gprs_device_hist
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF  :new.gprs_device_hist_id IS NULL THEN
        SELECT seq_gprs_device_hist_id.nextval
        INTO :new.gprs_device_hist_id
        FROM dual;
    END IF;

    SELECT  sysdate,
            user,
            sysdate,
            user
      INTO  :new.created_ts,
            :new.created_by,
            :new.last_updated_ts,
            :new.last_updated_by
      FROM  dual;
END; 
/