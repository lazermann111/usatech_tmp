CREATE OR REPLACE TRIGGER device.trbi_device_setting_parameter
BEFORE INSERT ON device.device_setting_parameter
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;

/