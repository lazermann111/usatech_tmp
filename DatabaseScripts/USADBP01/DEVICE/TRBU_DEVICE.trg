CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE
BEFORE UPDATE ON DEVICE.DEVICE
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF (:OLD.encryption_key IS NULL AND :NEW.encryption_key IS NOT NULL)
      OR (:OLD.encryption_key IS NOT NULL AND :NEW.encryption_key IS NULL)
      OR (:OLD.encryption_key <> :NEW.encryption_key)
   THEN
      :NEW.device_encr_key_gen_ts := SYSDATE;
      :NEW.previous_encryption_key := utl_raw.cast_to_raw(:OLD.encryption_key);
   END IF;
   
   IF :NEW.CMD_PENDING_CNT < 0 THEN
		:NEW.CMD_PENDING_CNT := 0;
   END IF;
   
   IF :OLD.CMD_PENDING_CNT != :NEW.CMD_PENDING_CNT THEN
		:NEW.CMD_PENDING_UPDATED_TS := SYSDATE;
   END IF;
   
   IF DBADMIN.PKG_UTL.COMPARE(:OLD.REJECT_UNTIL_TS, :NEW.REJECT_UNTIL_TS) != 0 THEN
		:NEW.REJECT_UNTIL_UPDATED_TS := SYSDATE;
   END IF;

   SELECT :OLD.created_by, :OLD.created_ts, SYSDATE,
          USER
     INTO :NEW.created_by, :NEW.created_ts, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/