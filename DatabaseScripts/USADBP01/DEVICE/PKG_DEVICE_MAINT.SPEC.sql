CREATE OR REPLACE 
PACKAGE device.pkg_device_maint 
IS

PROCEDURE SP_CREATE_NEW_POS_ID
(
    pn_old_pos_id       IN       pos.pos_id%TYPE,
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_location_id      IN       location.location_id%TYPE,
    pn_customer_id      IN       customer.customer_id%TYPE,
    pb_dup_base_host    IN       BOOLEAN,
    pb_dup_other_hosts  IN       BOOLEAN,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_ASSIGN_NEW_DEVICE_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_UPDATE_LOC_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_location_id  IN       location.location_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_UPDATE_CUST_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_customer_id  IN       customer.customer_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_UPDATE_LOC_ID_CUST_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_location_id  IN       location.location_id%TYPE := -1,
    pn_new_customer_id  IN       customer.customer_id%TYPE := -1,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

END;
/
