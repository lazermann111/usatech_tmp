CREATE OR REPLACE PROCEDURE DEVICE.SP_UPDATE_CONFIG_AND_RETURN
(
    p_device_id         IN NUMBER,
    p_offset            IN INT,
    p_new_data          IN VARCHAR2,
    p_data_type         IN CHAR,
    p_device_type_id    IN NUMBER,
    p_debug             IN INT,
    p_return_code       OUT NUMBER,
    p_return_msg        OUT VARCHAR2,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE    
) IS
	ln_device_id 		DEVICE.DEVICE_ID%TYPE;
    l_device_name       DEVICE.DEVICE_NAME%TYPE;
	l_orig_data			DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	l_orig_data_hex		DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
    l_replace_with_length INT;
    l_replace_with      DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
    l_poke_location     NUMBER(5,1);
    l_poke_length       INT;
    l_poke_command      ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
    l_pending_count     INT;
    l_execute_order     INT;
    
BEGIN
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = p_device_id;

    BEGIN
        SELECT d.device_name, ds.device_setting_value
        INTO l_device_name, l_orig_data_hex
        FROM device.device d
		JOIN device.device_setting ds
		ON d.device_id = ds.device_id
			AND ds.device_setting_parameter_cd = TO_CHAR(p_offset)
        WHERE d.device_id = ln_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_return_code := -1;
            p_return_msg := 'Device or device setting not found for device_id: ' || ln_device_id || ', offset: ' || p_offset;
            RETURN;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    p_return_msg := ln_device_id || ',' || l_device_name || ',' || p_offset || ': ';

    -- expect hex data will be passed in hex encoded, ascii and choice data will not
    IF p_data_type = 'H' THEN
        l_replace_with := p_new_data;
		p_return_msg := p_return_msg || UPPER(l_orig_data_hex) || ' = ' || UPPER(l_replace_with);
    ELSE
        l_replace_with := pkg_conversions.string_to_hex(p_new_data);
		BEGIN
			l_orig_data := pkg_conversions.hex_to_string(l_orig_data_hex);
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		p_return_msg := p_return_msg || l_orig_data || ' = ' || p_new_data;
    END IF;
	
	l_replace_with_length := LENGTH(l_replace_with);

    -- compare the hex values to see if there was a change
    IF UPPER(l_orig_data_hex) = UPPER(l_replace_with) THEN
        p_return_code := 0;
        p_return_msg :=  p_return_msg || ': TRUE - NO CHANGE!';
        RETURN;
    END IF;

    p_return_msg :=  p_return_msg || ': FALSE - CHANGED!';
    p_return_code := 1;

    -- now we need to create a pending Poke for this change
    l_poke_length := l_replace_with_length/2;

    IF p_device_type_id IN (0,1) THEN
        -- G4/G5 devices addresses memory in words
        l_poke_location := trunc((p_offset/2));
        -- must send at least 2 bytes at a time to a G4/G5
        IF l_poke_length = 1 THEN
            l_poke_length := 2;
        END IF;
    ELSIF p_device_type_id IN (6) THEN
        -- MEI device should always Poke the entire config
        l_poke_location := 0;
        l_poke_length := 57;
    ELSE
        l_poke_location := p_offset;
    END IF;
    
    l_poke_command := '42' || lpad(pkg_conversions.to_hex(l_poke_location), 8, '0') || lpad(pkg_conversions.to_hex(l_poke_length), 8, '0');

    -- check if the Poke already exists; no need to send 2 pokes for the same memory segment
    SELECT count(1)
    INTO l_pending_count
    FROM engine.machine_cmd_pending
    WHERE machine_id = l_device_name
    AND data_type = '88'
    AND command = l_poke_command
    AND execute_cd IN ('P', 'S');

    -- query for the max pending command execute_order
    -- we want to put the Poke at the end of the pending list
    SELECT nvl(max(execute_order)+1, 0)
    INTO l_execute_order
    FROM engine.machine_cmd_pending
    WHERE machine_id = l_device_name
    AND execute_cd IN ('P', 'S');

    p_return_msg := p_return_msg || ' - POKE: ' || l_device_name || ' 88 ' || l_poke_command || ' P ' || l_execute_order;

    -- if not debugging then update the file contents and save the poke
    IF l_pending_count = 0 THEN
        p_return_msg := p_return_msg || ', poke does not exist';
        IF p_debug = 0 THEN
            INSERT INTO engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order)
            VALUES (l_device_name, '88', l_poke_command, 'P', l_execute_order)
            RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, COMMAND INTO pn_command_id, pv_data_type, pl_command;
            p_return_msg := p_return_msg || ', poke inserted';
        ELSE
            p_return_msg := p_return_msg || ', poke not inserted (debug on)';
        END IF;
    ELSE
        p_return_msg := p_return_msg || ', poke not inserted (already exists)';
    END IF;

    IF p_debug = 0 THEN
		UPDATE device.device_setting
		SET device_setting_value = l_replace_with
		WHERE device_id = ln_device_id
			AND device_setting_parameter_cd = TO_CHAR(p_offset);
		p_return_msg := p_return_msg || ', device setting updated';
    ELSE
        p_return_msg := p_return_msg || ', device setting not updated (debug on)';
    END IF;
END;
/