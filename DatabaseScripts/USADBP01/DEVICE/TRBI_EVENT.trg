CREATE OR REPLACE TRIGGER device.trbi_event
BEFORE INSERT ON device.event
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.event_id IS NULL
	THEN
		SELECT SEQ_EVENT_ID.NEXTVAL
		INTO :NEW.event_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/