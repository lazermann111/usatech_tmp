CREATE OR REPLACE FORCE VIEW DEVICE.VW_FILL_EVENT (EVENT_ID, EVENT_TS, HOST_ID, DEVICE_ID, DEVICE_NAME, DEVICE_SERIAL_CD, DEVICE_TYPE_ID, FILL_VERSION_NUM, PERMISSION_ACTION_ID, IS_DUPLICATE, COUNTERS_RESET_FLAG, COUNTER_EVENT_ID) AS 
  SELECT   E.EVENT_ID,
            CAST (
               FROM_TZ (CAST (E_C.EVENT_START_TS AS TIMESTAMP),
                        'America/New_York') AT TIME ZONE tz.TIME_ZONE_GUID AS DATE
            )
               EVENT_TS,
            e.HOST_ID,
            d.DEVICE_ID,
            d.DEVICE_NAME,
            d.DEVICE_SERIAL_CD,
            D.DEVICE_TYPE_ID,
            1 FILL_VERSION_NUM,
            0 PERMISSION_ACTION_ID,
            (SELECT   DECODE (COUNT ( * ), NULL, 'N', 0, 'N', 'Y')
               FROM      DEVICE.EVENT DUP_E
                      JOIN
                         DEVICE.EVENT_NL_DEVICE_SESSION ES_DUP
                      ON ES_DUP.EVENT_ID = DUP_E.EVENT_ID
              WHERE       ES_DUP.SESSION_ID = ES.SESSION_ID
                      AND DUP_E.EVENT_TYPE_ID = 2
                      AND DUP_E.EVENT_ID < E.EVENT_ID)
               IS_DUPLICATE,
            'N' COUNTERS_RESET_FLAG,
            E_C.EVENT_ID COUNTER_EVENT_ID
     FROM                           DEVICE.EVENT E
                                 JOIN
                                    DEVICE.HOST h
                                 ON e.HOST_ID = h.HOST_ID
                              JOIN
                                 DEVICE.DEVICE d
                              ON h.DEVICE_ID = d.DEVICE_ID
                           JOIN
                              PSS.POS P
                           ON d.DEVICE_ID = p.DEVICE_ID
                        JOIN
                           LOCATION.LOCATION L
                        ON P.LOCATION_ID = L.LOCATION_ID
                     JOIN
                        LOCATION.TIME_ZONE TZ
                     ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
                  JOIN
                     DEVICE.EVENT_NL_DEVICE_SESSION ES
                  ON E.EVENT_ID = ES.EVENT_ID
               JOIN
                  DEVICE.EVENT_NL_DEVICE_SESSION ES_C
               ON ES.SESSION_ID = ES_C.SESSION_ID
                  AND ES.EVENT_ID != ES_C.EVENT_ID
            JOIN
               DEVICE.EVENT E_C
            ON     ES_C.EVENT_ID = E_C.EVENT_ID
               AND E_C.EVENT_TYPE_ID = 1
               AND E_C.EVENT_STATE_ID = 2
    WHERE   E.EVENT_TYPE_ID = 2 AND E.EVENT_STATE_ID = 2
            AND 1 IN
                     (SELECT   TO_NUMBER_OR_NULL (ED_v.EVENT_DETAIL_VALUE)
                        FROM   DEVICE.EVENT_DETAIL ed_v
                       WHERE   E.EVENT_ID = ed_v.EVENT_ID
                               AND ed_v.EVENT_DETAIL_TYPE_ID = 1 /* Event Type Version */
                                                                )
   UNION ALL
   SELECT   E.EVENT_ID,
            ed_ts.EVENT_DETAIL_VALUE_TS EVENT_TS,
            e.HOST_ID,
            d.DEVICE_ID,
            d.DEVICE_NAME,
            d.DEVICE_SERIAL_CD,
            D.DEVICE_TYPE_ID,
            3 FILL_VERSION_NUM,
            A.AUTH_ACTION_ID PERMISSION_ACTION_ID,
            'N' IS_DUPLICATE,
            'Y' COUNTERS_RESET_FLAG,
            E.EVENT_ID COUNTER_EVENT_ID
     FROM                  DEVICE.EVENT E
                        JOIN
                           DEVICE.HOST h
                        ON e.HOST_ID = h.HOST_ID
                     JOIN
                        DEVICE.DEVICE d
                     ON h.DEVICE_ID = d.DEVICE_ID
                  JOIN
                     DEVICE.EVENT_DETAIL ed_ts
                  ON E.EVENT_ID = ed_ts.EVENT_ID
                     AND ed_ts.EVENT_DETAIL_TYPE_ID = 2 /* Host Event Timestamp */
               JOIN
                  PSS.TRAN T
               ON E.EVENT_GLOBAL_TRANS_CD = T.TRAN_GLOBAL_TRANS_CD
            JOIN
               PSS.AUTH A
            ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N'
    WHERE   E.EVENT_TYPE_ID = 2 AND E.EVENT_STATE_ID = 2
            AND 3 IN
                     (SELECT   TO_NUMBER_OR_NULL (ED_v.EVENT_DETAIL_VALUE)
                        FROM   DEVICE.EVENT_DETAIL ed_v
                       WHERE   E.EVENT_ID = ed_v.EVENT_ID
                               AND ed_v.EVENT_DETAIL_TYPE_ID = 1 /* Event Type Version */
                                                                )
   UNION ALL
   SELECT   E.EVENT_ID,
            ed_ts.EVENT_DETAIL_VALUE_TS EVENT_TS,
            e.HOST_ID,
            d.DEVICE_ID,
            d.DEVICE_NAME,
            d.DEVICE_SERIAL_CD,
            D.DEVICE_TYPE_ID,
            2 FILL_VERSION_NUM,
            PA.ACTION_ID PERMISSION_ACTION_ID,
            'N' IS_DUPLICATE,
            DECODE (PA.ACTION_ID,
                    1, 'N',
                    2, 'A',
                    3, 'N',
                    4, 'A',
                    5, 'N',
                    6, 'C',
                    DECODE (D.DEVICE_TYPE_ID,
                            1,
                            'C',
                            6,
                            'N'))
               COUNTERS_RESET_FLAG,
            E.EVENT_ID COUNTER_EVENT_ID
     FROM               DEVICE.EVENT E
                     JOIN
                        DEVICE.HOST h
                     ON e.HOST_ID = h.HOST_ID
                  JOIN
                     DEVICE.DEVICE d
                  ON h.DEVICE_ID = d.DEVICE_ID
               JOIN
                  DEVICE.EVENT_DETAIL ed_ts
               ON E.EVENT_ID = ed_ts.EVENT_ID
                  AND ed_ts.EVENT_DETAIL_TYPE_ID = 2 /* Host Event Timestamp */
            LEFT OUTER JOIN
               (      PSS.TRAN T
                   JOIN
                      PSS.CONSUMER_ACCT_PERMISSION CAP
                   ON T.CONSUMER_ACCT_ID = CAP.CONSUMER_ACCT_ID
                JOIN
                   PSS.PERMISSION_ACTION PA
                ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
                JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON PA.ACTION_ID = DTA.ACTION_ID)
            ON E.EVENT_GLOBAL_TRANS_CD = T.TRAN_GLOBAL_TRANS_CD AND DTA.DEVICE_TYPE_ID = D.DEVICE_TYPE_ID
    WHERE   E.EVENT_TYPE_ID = 2 AND E.EVENT_STATE_ID = 2
            AND 2 IN
                     (SELECT   TO_NUMBER_OR_NULL (ED_v.EVENT_DETAIL_VALUE)
                        FROM   DEVICE.EVENT_DETAIL ed_v
                       WHERE   E.EVENT_ID = ed_v.EVENT_ID
                               AND ed_v.EVENT_DETAIL_TYPE_ID = 1 /* Event Type Version */
                                                                )
/
