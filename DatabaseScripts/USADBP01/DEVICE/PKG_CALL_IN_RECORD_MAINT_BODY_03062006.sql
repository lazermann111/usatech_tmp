-- Start of DDL Script for Package Body DEVICE.PKG_CALL_IN_RECORD_MAINT
-- Generated 2-Mar-2006 16:13:20 from DEVICE@USADBD02.USATECH.COM

CREATE OR REPLACE 
PACKAGE BODY        pkg_call_in_record_maint
IS
   -- Status constants
   cc_status_incomplete   CONSTANT CHAR (1)                            := 'I';
   cc_status_complete     CONSTANT CHAR (1)                            := 'S';
   cc_status_failed       CONSTANT CHAR (1)                            := 'U';
   cc_status_unknown      CONSTANT CHAR (1)                            := 'X';
   -- Call type contants
   cc_call_type_unknown   CONSTANT CHAR (1)                            := 'U';
   cc_call_type_auth      CONSTANT CHAR (1)                            := 'A';
   cc_call_type_batch     CONSTANT CHAR (1)                            := 'B';
   cc_call_type_combo     CONSTANT CHAR (1)                            := 'C';
   -- Approved constants
   cc_auth_approved       CONSTANT CHAR (1)                            := 'Y';
   cc_auth_denied         CONSTANT CHAR (1)                            := 'N';
   cc_auth_failed         CONSTANT CHAR (1)                            := 'F';
   -- Transaction type constants
   cc_credit_trans        CONSTANT CHAR (1)                            := 'C';
   cc_rfid_credit_trans   CONSTANT CHAR (1)                            := 'R';
   cc_passcard_trans      CONSTANT CHAR (1)                            := 'S';
   cc_rfid_passcard_trans CONSTANT CHAR (1)                            := 'P';
   cc_cash_trans          CONSTANT CHAR (1)                            := 'M';
   -- Message direction constants
   cc_inbound             CONSTANT CHAR (1)                            := 'I';
   cc_outbound            CONSTANT CHAR (1)                            := 'O';
   -- Call in detail constants
--   cc_detail_summary      CONSTANT CHAR (1)                            := 'S';
--   cc_detail_individual   CONSTANT CHAR (1)                            := 'I';
--   cc_detail_both         CONSTANT CHAR (1)                            := 'B';
--   cc_detail_none         CONSTANT CHAR (1)                            := 'N';
   -- Logging contants
   cs_server_name         CONSTANT VARCHAR2 (30)                    := 'TAZ3';
   cs_package_name        CONSTANT VARCHAR2 (30)
                                                := 'PKG_CALL_IN_RECORD_MAINT';
   -- Usefull variables
--   vs_serial_number                device.device_serial_cd%TYPE;
--   vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
   -- Exception stuff
--   ex_device_not_found             EXCEPTION;
--   vs_error_msg                    exception_data.additional_information%TYPE;

   PROCEDURE sp_start (
      ps_ev_number       IN   device.device_name%TYPE,
      ps_network_layer   IN   device_call_in_record.network_layer%TYPE,
      ps_ip_address      IN   device_call_in_record.ip_address%TYPE,
      ps_modem_id        IN   device_call_in_record.modem_id%TYPE,
      ps_rssi            IN   device_call_in_record.rssi%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)                := 'SP_START';
      vn_last_call_in_record_id    device_call_in_record.device_call_in_record_id%TYPE;
      vs_location_name             LOCATION.location_name%TYPE;
      vs_customer_name             customer.customer_name%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_start(ps_ev_number,ps_network_layer,ps_ip_address,ps_modem_id,ps_rssi);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vs_serial_number := fn_get_serial_number (ps_ev_number);

      IF vs_serial_number IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

        -- first look for an unfinished call in the last 3 minutes
      SELECT MAX (device_call_in_record.device_call_in_record_id)
        INTO vn_last_call_in_record_id
        FROM device_call_in_record
       WHERE device_call_in_record.serial_number = vs_serial_number
         AND ((device_call_in_record.call_in_status = 'I' OR device_call_in_record.call_in_finish_ts IS NULL) AND device_call_in_record.last_updated_ts >= (sysdate-(3/1440)));
         
      IF vn_last_call_in_record_id IS NOT NULL
      THEN
         -- A recent record exists, assume it's the same call-in and update it
         -- Don't overwrite any existing information, unless it's null
         UPDATE device_call_in_record
            SET network_layer = NVL (network_layer, ps_network_layer),
                ip_address = NVL (ip_address, ps_ip_address),
                modem_id = NVL (modem_id, ps_modem_id),
                rssi = NVL (rssi, ps_rssi)
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_last_call_in_record_id;
      ELSE
         -- this is a new call
         -- if there are any existing calls with incomplete status, update them to failed

          -- If any extra in-progress records exists, mark them uncessfull
         UPDATE device_call_in_record
            SET device_call_in_record.call_in_status = 'U'
          WHERE device_call_in_record.serial_number = vs_serial_number
            AND device_call_in_record.call_in_status = cc_status_incomplete;

         -- Start a new device_call_in_record
         -- Lookup the location and customer
         vs_location_name := fn_get_location_name (ps_ev_number);
         vs_customer_name := fn_get_customer_name (ps_ev_number);
         
         

         INSERT INTO device_call_in_record
                     (serial_number, call_in_start_ts, call_in_status,
                      network_layer, ip_address, modem_id, rssi,
                      location_name, customer_name
                     )
              VALUES (vs_serial_number, SYSDATE, cc_status_incomplete,
                      ps_network_layer, ps_ip_address, ps_modem_id, ps_rssi,
                      vs_location_name, vs_customer_name
                     );
      END IF;
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
/*      WHEN OTHERS
      THEN

         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 ); */
   END;

   PROCEDURE sp_add_trans (
      ps_ev_number      IN   device.device_name%TYPE,
      pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
      pn_trans_amount   IN   device_call_in_record.credit_trans_total%TYPE,
      pn_vend_count     IN   device_call_in_record.credit_vend_count%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_TRANS';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_add_trans(ps_ev_number,pc_card_type,pn_trans_amount,pn_vend_count);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      IF pc_card_type IN (cc_credit_trans, cc_rfid_credit_trans)
      THEN
         UPDATE device_call_in_record
            SET credit_trans_count = (credit_trans_count + 1),
                credit_trans_total = (credit_trans_total + pn_trans_amount),
                credit_vend_count = (credit_vend_count + pn_vend_count),
                last_trans_in_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      ELSIF pc_card_type = cc_cash_trans
      THEN
         UPDATE device_call_in_record
            SET cash_trans_count = (cash_trans_count + 1),
                cash_trans_total = (cash_trans_total + pn_trans_amount),
                cash_vend_count = (cash_vend_count + pn_vend_count),
                last_trans_in_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      ELSIF pc_card_type IN (cc_passcard_trans, cc_rfid_passcard_trans)
      THEN
         UPDATE device_call_in_record
            SET passcard_trans_count = (passcard_trans_count + 1),
                passcard_trans_total =
                                     (passcard_trans_total + pn_trans_amount
                                     ),
                passcard_vend_count = (passcard_vend_count + pn_vend_count),
                last_trans_in_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      END IF;

      sp_set_type (vn_device_call_in_record_id, cc_call_type_batch);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_add_auth (
      ps_ev_number       IN   device.device_name%TYPE,
      pc_card_type       IN   device_call_in_record.auth_card_type%TYPE,
      pn_auth_amount     IN   device_call_in_record.auth_amount%TYPE,
      pc_approved_flag   IN   device_call_in_record.auth_approved_flag%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_AUTH';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_add_auth(ps_ev_number,pc_card_type,pn_auth_amount,pc_approved_flag);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET auth_amount = pn_auth_amount,
             auth_approved_flag = pc_approved_flag,
             auth_card_type = pc_card_type,
             last_auth_in_ts = SYSDATE
       WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;

      sp_set_type (vn_device_call_in_record_id, cc_call_type_auth);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_dex_received (
      ps_ev_number       IN   device.device_name%TYPE,
      pn_dex_file_size   IN   device_call_in_record.dex_file_size%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_DEX_RECEIVED';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_set_dex_received(ps_ev_number,pn_dex_file_size);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET dex_received_flag = 'Y',
             dex_file_size = pn_dex_file_size
       WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;

      sp_set_type (vn_device_call_in_record_id, cc_call_type_batch);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_initialized (ps_ev_number IN device.device_name%TYPE)
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_INITIALIZED';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_set_initialized(ps_ev_number);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET device_initialized_flag = 'Y'
       WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;

      sp_set_type (vn_device_call_in_record_id, cc_call_type_batch);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_device_sent_config (
      ps_ev_number   IN   device.device_name%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)
                                               := 'SP_SET_DEVICE_SENT_CONFIG';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_set_device_sent_config(ps_ev_number);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET device_sent_config_flag = 'Y'
       WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;

      sp_set_type (vn_device_call_in_record_id, cc_call_type_batch);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_server_sent_config (
      ps_ev_number   IN   device.device_name%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)
                                               := 'SP_SET_SERVER_SENT_CONFIG';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_set_server_sent_config(ps_ev_number);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET server_sent_config_flag = 'Y'
       WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;

      sp_set_type (vn_device_call_in_record_id, cc_call_type_batch);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_status (
      ps_ev_number     IN   device.device_name%TYPE,
      pc_status_flag   IN   device_call_in_record.call_in_status%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_STATUS';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_set_status(ps_ev_number,pc_status_flag);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      -- if new_status is Success, overwrite all
      -- if new_status is Failed, overwrite all but success
      -- if new_status is Unknown, overwrite only incomplete
      -- if new_status is Incomplete, overwrite none
      IF pc_status_flag = cc_status_complete
      THEN
         UPDATE device_call_in_record
            SET call_in_status = pc_status_flag
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      ELSIF pc_status_flag = cc_status_failed
      THEN
         UPDATE device_call_in_record
            SET call_in_status =
                   DECODE (call_in_status,
                           cc_status_complete, call_in_status,
                           pc_status_flag
                          )
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      ELSIF pc_status_flag = cc_status_unknown
      THEN
         UPDATE device_call_in_record
            SET call_in_status =
                   DECODE (call_in_status,
                           cc_status_incomplete, pc_status_flag,
                           call_in_status
                          )
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      END IF;
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_type (
      ps_ev_number   IN   device.device_name%TYPE,
      pc_type_flag   IN   device_call_in_record.call_in_type%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_TYPE';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_set_type(ps_ev_number,pc_type_flag);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      sp_set_type (vn_device_call_in_record_id, pc_type_flag);
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_set_type (
      pn_device_call_in_record_id   IN   device_call_in_record.device_call_in_record_id%TYPE,
      pc_type_flag                  IN   device_call_in_record.call_in_type%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)             := 'SP_SET_TYPE';
      vc_current_type_flag         device_call_in_record.call_in_type%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
      SELECT call_in_type
        INTO vc_current_type_flag
        FROM device_call_in_record
       WHERE device_call_in_record.device_call_in_record_id =
                                                   pn_device_call_in_record_id;

      IF     vc_current_type_flag = cc_call_type_auth
         AND pc_type_flag = cc_call_type_batch
      THEN
         UPDATE device_call_in_record
            SET call_in_type = cc_call_type_combo
          WHERE device_call_in_record.device_call_in_record_id =
                                                   pn_device_call_in_record_id;
      ELSIF     vc_current_type_flag = cc_call_type_batch
            AND pc_type_flag = cc_call_type_auth
      THEN
         UPDATE device_call_in_record
            SET call_in_type = cc_call_type_combo
          WHERE device_call_in_record.device_call_in_record_id =
                                                   pn_device_call_in_record_id;
      ELSE
         UPDATE device_call_in_record
            SET call_in_type = pc_type_flag
          WHERE device_call_in_record.device_call_in_record_id =
                                                   pn_device_call_in_record_id;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_finish (ps_ev_number IN device.device_name%TYPE)
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_FINISH';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_finish(ps_ev_number);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      -- if call_in_status is Incomplete, update it to Unknown
      UPDATE device_call_in_record
         SET call_in_finish_ts = SYSDATE,
             call_in_status =
                DECODE (call_in_status,
                        cc_status_incomplete, cc_status_failed,
                        call_in_status
                       )
       WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   PROCEDURE sp_add_message (
      ps_ev_number   IN   device.device_name%TYPE,
      pc_direction   IN   CHAR,
      pn_num_bytes   IN   device_call_in_record.inbound_byte_count%TYPE
   )
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_MESSAGE';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vc_detail_cd                 CHAR;
   BEGIN
/*
      vc_detail_cd := fn_get_call_in_detail(ps_ev_number);
      IF vc_detail_cd = cc_detail_none THEN
        RETURN;
      END IF;

      IF vc_detail_cd = cc_detail_summary OR vc_detail_cd = cc_detail_both THEN
        pkg_call_in_summary_maint.sp_add_message(ps_ev_number,pc_direction,pn_num_bytes);
        IF vc_detail_cd = cc_detail_summary THEN
          RETURN;
        END IF;
      END IF;
*/
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      IF pc_direction = cc_inbound
      THEN
         UPDATE device_call_in_record
            SET inbound_message_count = (inbound_message_count + 1),
                inbound_byte_count = (inbound_byte_count + pn_num_bytes)
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      ELSIF pc_direction = cc_outbound
      THEN
         UPDATE device_call_in_record
            SET outbound_message_count = (outbound_message_count + 1),
                outbound_byte_count = (outbound_byte_count + pn_num_bytes),
                last_message_out_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                   vn_device_call_in_record_id;
      END IF;
   EXCEPTION
      WHEN ex_device_not_found
      THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                (pkg_app_exec_hist_globals.invalid_machine_id,
                                 vs_error_msg,
                                 NULL,
                                 cs_package_name || cs_procedure_name
                                );
      WHEN OTHERS
      THEN
         vs_error_msg :=
             'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                 (pkg_app_exec_hist_globals.unknown_error_id,
                                  vs_error_msg,
                                  cs_server_name,
                                  cs_package_name || '.' || cs_procedure_name
                                 );
   END;

   FUNCTION fn_get_current_call_in_id (
      ps_ev_number   IN   device.device_name%TYPE
   )
      RETURN device_call_in_record.device_call_in_record_id%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)
                                               := 'FN_GET_CURRENT_CALL_IN_ID';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
      -- First get the serial number
      vs_serial_number := fn_get_serial_number (ps_ev_number);

      IF vs_serial_number IS NULL
      THEN
         RETURN NULL;
      END IF;

        -- first look for an unfinished call in the last 3 minutes
      SELECT MAX (device_call_in_record.device_call_in_record_id)
        INTO vn_device_call_in_record_id
        FROM device_call_in_record
       WHERE device_call_in_record.serial_number = vs_serial_number
         AND ((device_call_in_record.call_in_status = 'I' OR device_call_in_record.call_in_finish_ts IS NULL) AND device_call_in_record.last_updated_ts >= (sysdate-(3/1440)));
         --AND (device_call_in_record.call_in_status <> 'I' AND device_call_in_record.last_updated_ts >= (sysdate-(3/1440)));
         --AND (device_call_in_record.call_in_status = 'I' OR device_call_in_record.call_in_finish_ts IS NULL);

        -- If we didn't find an unfinished call, look for any call in the last 3 minutes.
        -- This is different than sp_start logic because in this case we're in the middle of a call so
        -- something abnormal is going on...
      IF vn_device_call_in_record_id IS NULL
      THEN
          SELECT MAX (device_call_in_record.device_call_in_record_id)
            INTO vn_device_call_in_record_id
            FROM device_call_in_record
           WHERE device_call_in_record.serial_number = vs_serial_number
             AND device_call_in_record.last_updated_ts >= (sysdate-(3/1440));
      END IF;

        -- if we still didn't find a call, we'll start a new one here and mark any old ones as failures
      IF vn_device_call_in_record_id IS NULL
      THEN
         -- we never started a call-in record, so start it now... this isn't ideal,
         -- but should only happen in the case where the network layer doesn't
         -- start the call-in record, which is possble if it's new or legacy
         sp_start (ps_ev_number, NULL, NULL, NULL, NULL);

         SELECT MAX (device_call_in_record.device_call_in_record_id)
           INTO vn_device_call_in_record_id
           FROM device_call_in_record
          WHERE device_call_in_record.serial_number = vs_serial_number
            AND device_call_in_record.call_in_status = 'I';
      END IF;

      RETURN vn_device_call_in_record_id;
   END;

   FUNCTION fn_get_serial_number (ps_ev_number IN device.device_name%TYPE)
      RETURN device.device_serial_cd%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_SERIAL_NUMBER';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
      SELECT device.device_serial_cd
        INTO vs_serial_cd
        FROM device
       WHERE device.device_name = ps_ev_number
         AND device.device_active_yn_flag = 'Y';

      RETURN vs_serial_cd;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;

   FUNCTION fn_get_location_name (ps_ev_number IN device.device_name%TYPE)
      RETURN LOCATION.location_name%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_LOCATION_NAME';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_location_name             LOCATION.location_name%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
      SELECT LOCATION.location_name
        INTO vs_location_name
        FROM LOCATION.LOCATION, device, pss.pos
       WHERE LOCATION.location_id = pss.pos.location_id
         AND pss.pos.device_id = device.device_id
         AND device.device_name = ps_ev_number;

      RETURN vs_location_name;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;

   FUNCTION fn_get_customer_name (ps_ev_number IN device.device_name%TYPE)
      RETURN customer.customer_name%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_CUSTOMER_NAME';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_customer_name             customer.customer_name%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
      SELECT customer.customer_name
        INTO vs_customer_name
        FROM LOCATION.customer, device, pss.pos
       WHERE LOCATION.customer.customer_id = pss.pos.customer_id
         AND pss.pos.device_id = device.device_id
         AND device.device_name = ps_ev_number;

      RETURN vs_customer_name;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;
/*
   FUNCTION fn_get_call_in_detail (ps_ev_number IN device.device_name%TYPE)
      RETURN CHAR
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_CALL_IN_DETAIL';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vc_call_in_detail            CHAR;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
      SELECT device_setting.device_setting_value
        INTO vc_call_in_detail
        FROM device_setting, device
       WHERE device.device_id = device_setting.device_id
         AND device_setting.device_setting_parameter_cd = 'CALL_IN_RECORD_DETAIL'
         AND device.device_name = ps_ev_number
         and device.device_active_yn_flag = 'Y';
         RETURN vc_call_in_detail;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN cc_detail_none;
    END;
*/
END;
/

-- Grants for Package Body
GRANT EXECUTE ON pkg_call_in_record_maint TO public
/


-- End of DDL Script for Package Body DEVICE.PKG_CALL_IN_RECORD_MAINT

