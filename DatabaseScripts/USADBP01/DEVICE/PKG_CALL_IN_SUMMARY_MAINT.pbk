CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_CALL_IN_SUMMARY_MAINT IS
   -- Status constants
   cc_status_incomplete   CONSTANT CHAR (1)                            := 'I';
   cc_status_complete     CONSTANT CHAR (1)                            := 'S';
   cc_status_failed       CONSTANT CHAR (1)                            := 'U';
   cc_status_unknown      CONSTANT CHAR (1)                            := 'X';
   -- Call type contants
   cc_call_type_unknown   CONSTANT CHAR (1)                            := 'U';
   cc_call_type_auth      CONSTANT CHAR (1)                            := 'A';
   cc_call_type_batch     CONSTANT CHAR (1)                            := 'B';
   cc_call_type_combo     CONSTANT CHAR (1)                            := 'C';
   -- Approved constants
   cc_auth_approved       CONSTANT CHAR (1)                            := 'Y';
   cc_auth_denied         CONSTANT CHAR (1)                            := 'N';
   cc_auth_failed         CONSTANT CHAR (1)                            := 'F';
   -- Transaction type constants
   cc_credit_trans        CONSTANT CHAR (1)                            := 'C';
   cc_passcard_trans      CONSTANT CHAR (1)                            := 'P';
   cc_cash_trans          CONSTANT CHAR (1)                            := 'M';
   -- Message direction constants
   cc_inbound             CONSTANT CHAR (1)                            := 'I';
   cc_outbound            CONSTANT CHAR (1)                            := 'O';
   -- Call in detail constants
   cc_detail_summary      CONSTANT CHAR (1)                            := 'S';
   cc_detail_individual   CONSTANT CHAR (1)                            := 'I';
   cc_detail_both         CONSTANT CHAR (1)                            := 'B';
   cc_detail_none         CONSTANT CHAR (1)                            := 'N';
   -- Current status constants
   cc_current_status_online CONSTANT CHAR(1)                           := 'Y';
   cc_current_status_offline CONSTANT CHAR(1)                          := 'N';
   -- Logging contants
   cs_server_name         CONSTANT VARCHAR2 (30)                       := 'USADBP';
   cs_package_name        CONSTANT VARCHAR2 (30)                       := 'PKG_CALL_IN_SUMMARY_MAINT';
   
   vd_temp_date             DATE;

   PROCEDURE sp_start (
      ps_ev_number       IN   device.device_name%TYPE,
      ps_network_layer   IN   device_call_in_record.network_layer%TYPE,
      ps_ip_address      IN   device_call_in_record.ip_address%TYPE,
      ps_modem_id        IN   device_call_in_record.modem_id%TYPE,
      ps_rssi            IN   device_call_in_record.rssi%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)                  := 'SP_START';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
      vs_location_name             LOCATION.location_name%TYPE;
      vs_customer_name             customer.customer_name%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;

   BEGIN
   
      vs_serial_number := fn_get_serial_number (ps_ev_number);

      IF vs_serial_number IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;
   
      SELECT MAX(device_call_in_summary.device_call_in_summary_id)
        INTO vn_device_call_in_summary_id
        FROM device_call_in_summary
       WHERE device_call_in_summary.serial_number = vs_serial_number
         AND device_call_in_summary.first_connection_ts >= trunc(sysdate);
         
      IF vn_device_call_in_summary_id IS NULL
      THEN
          -- This is the first call of the day
          
      
          -- Cleanup any old records
          UPDATE device_call_in_summary
             SET device_call_in_summary.current_status = cc_current_status_offline
           WHERE device_call_in_summary.serial_number = vs_serial_number
             AND device_call_in_summary.first_connection_ts < trunc(sysdate);

         -- Lookup the location and customer
         vs_location_name := fn_get_location_name (ps_ev_number);
         vs_customer_name := fn_get_customer_name (ps_ev_number);

         -- Set first connection timestamp = last connection timestamp = sysdate
         -- Set current status = Online
         INSERT INTO device_call_in_summary
                     (serial_number,    first_connection_ts,    last_connection_ts, current_status,             last_network_layer, last_ip_address,    last_modem_id,  location_name,      customer_name)
              VALUES (vs_serial_number, SYSDATE,                SYSDATE,            cc_current_status_online,   ps_network_layer,   ps_ip_address,      ps_modem_id,    vs_location_name,   vs_customer_name);
      ELSE
         -- A record for the current day already exists
         
            UPDATE device_call_in_summary
               SET last_connection_ts = sysdate,
                   current_status = cc_current_status_online,
                   last_network_layer = nvl(ps_network_layer, last_network_layer),
                   last_ip_address = nvl(ps_ip_address, last_ip_address),
                   last_modem_id = nvl(ps_modem_id, last_modem_id)
             WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

      END IF;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;

   PROCEDURE sp_add_trans (
      ps_ev_number      IN   device.device_name%TYPE,
      pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
      pn_trans_amount   IN   device_call_in_record.credit_trans_total%TYPE,
      pn_vend_count     IN   device_call_in_record.credit_vend_count%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_TRANS';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      IF pc_card_type = cc_credit_trans
      THEN
         UPDATE device_call_in_summary
            SET credit_trans_count = (credit_trans_count + 1),
                credit_trans_total = (credit_trans_total + pn_trans_amount),
                last_credit_ts = SYSDATE
          WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;
      ELSIF pc_card_type = cc_cash_trans
      THEN
         UPDATE device_call_in_summary
            SET cash_trans_count = (cash_trans_count + 1),
                cash_trans_total = (cash_trans_total + pn_trans_amount),
                last_cash_ts = SYSDATE
          WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

      ELSIF pc_card_type = cc_passcard_trans
      THEN
         UPDATE device_call_in_summary
            SET passcard_trans_count = (passcard_trans_count + 1),
                passcard_trans_total = (passcard_trans_total + pn_trans_amount),
                last_passcard_ts = SYSDATE
          WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;
      END IF;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;

   PROCEDURE sp_add_auth (
      ps_ev_number       IN   device.device_name%TYPE,
      pc_card_type       IN   device_call_in_record.auth_card_type%TYPE,
      pn_auth_amount     IN   device_call_in_record.auth_amount%TYPE,
      pc_approved_flag   IN   device_call_in_record.auth_approved_flag%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_AUTH';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_summary
         SET auth_count = (auth_count + 1),
             auth_total = (auth_total + pn_auth_amount),
             last_auth_ts = SYSDATE,
             auth_approved_count = decode(pc_approved_flag, 'Y', (auth_approved_count+1), auth_approved_count)
       WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;


   PROCEDURE sp_set_dex_received (
      ps_ev_number       IN   device.device_name%TYPE,
      pn_dex_file_size   IN   device_call_in_record.dex_file_size%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_DEX_RECEIVED';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_summary
         SET dex_received_count = (dex_received_count + 1),
             dex_total_bytes = (dex_total_bytes + pn_dex_file_size)
       WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;


   PROCEDURE sp_set_initialized (
      ps_ev_number IN device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_INITIALIZED';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_summary
         SET device_initialized_count = (device_initialized_count + 1)
       WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;

   PROCEDURE sp_set_device_sent_config (
      ps_ev_number   IN   device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_DEVICE_SENT_CONFIG';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_summary
         SET device_sent_config_count = (device_sent_config_count + 1)
       WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;

   PROCEDURE sp_set_server_sent_config (
      ps_ev_number   IN   device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_SERVER_SENT_CONFIG';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_summary
         SET server_sent_config_count = (server_sent_config_count + 1)
       WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;


   PROCEDURE sp_set_status (
      ps_ev_number     IN   device.device_name%TYPE,
      pc_status_flag   IN   device_call_in_record.call_in_status%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_STATUS';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      -- There is no real "status" notion in the summary table
      select sysdate into vd_temp_date from dual;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;
   
   PROCEDURE sp_set_type (
      ps_ev_number   IN   device.device_name%TYPE,
      pc_type_flag   IN   device_call_in_record.call_in_type%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_TYPE';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      -- There is no real "type" notion in the summary table
      select sysdate into vd_temp_date from dual;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;
   
   PROCEDURE sp_finish (
      ps_ev_number IN device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_FINISH';
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;

      -- only update if status is online, otherwise it could really throw off the total
      UPDATE device_call_in_summary
         SET device_call_in_summary.current_status = 'N',
             total_connection_time = (total_connection_time + (sysdate - last_connection_ts))
       WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id
         AND device_call_in_summary.current_status = 'Y';

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;

   PROCEDURE sp_add_message (
      ps_ev_number   IN   device.device_name%TYPE,
      pc_direction   IN   CHAR,
      pn_num_bytes   IN device_call_in_record.inbound_byte_count%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_MESSAGE';
      ex_device_not_found             EXCEPTION;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN

      vn_device_call_in_summary_id := fn_get_current_call_in_id (ps_ev_number);
      IF vn_device_call_in_summary_id IS NULL
      THEN
         RAISE ex_device_not_found;
      END IF;
      
      IF pc_direction = cc_inbound
      THEN
        UPDATE device_call_in_summary
           SET device_call_in_summary.inbound_message_count = (device_call_in_summary.inbound_message_count+1),
               device_call_in_summary.inbound_byte_count = (device_call_in_summary.inbound_byte_count+pn_num_bytes),
               device_call_in_summary.last_inbound_message_ts = sysdate
         WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;
      ELSIF pc_direction = cc_outbound
      THEN
        UPDATE device_call_in_summary
           SET device_call_in_summary.outbound_message_count = (device_call_in_summary.outbound_message_count+1),
               device_call_in_summary.outbound_byte_count = (device_call_in_summary.outbound_byte_count+pn_num_bytes),
               device_call_in_summary.last_outbound_message_ts = sysdate
         WHERE device_call_in_summary.device_call_in_summary_id = vn_device_call_in_summary_id;
      END IF;

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg := 'The device_name  = ' || ps_ev_number || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.invalid_machine_id, vs_error_msg, NULL, cs_package_name || cs_procedure_name);
      WHEN OTHERS THEN
         vs_error_msg := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception(pkg_app_exec_hist_globals.unknown_error_id, vs_error_msg, cs_server_name, cs_package_name || '.' || cs_procedure_name);
   END;
   
   FUNCTION fn_get_current_call_in_id (
      ps_ev_number   IN   device.device_name%TYPE
   )
      RETURN device_call_in_summary.device_call_in_summary_id%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'FN_GET_CURRENT_CALL_IN_ID';
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
   
      -- First get the serial number
      vs_serial_number := fn_get_serial_number (ps_ev_number);

      IF vs_serial_number IS NULL
      THEN
         RETURN NULL;
      END IF;

        -- look for a record for the current day
      SELECT MAX(device_call_in_summary.device_call_in_summary_id)
        INTO vn_device_call_in_summary_id
        FROM device_call_in_summary
       WHERE device_call_in_summary.serial_number = vs_serial_number
--         AND device_call_in_summary.current_status = 'Y'
         AND device_call_in_summary.first_connection_ts >= trunc(sysdate);
         
         -- If we find a record for the current day, but it's not online, we could set the device
         -- online, but this likely will cause more problems due to race conditions inherent with this application.

        -- if we didn't find a record for the current day, start a new one
      IF vn_device_call_in_summary_id IS NULL
      THEN
      
         sp_start (ps_ev_number, NULL, NULL, NULL, NULL);

         SELECT MAX(device_call_in_summary.device_call_in_summary_id)
           INTO vn_device_call_in_summary_id
           FROM device_call_in_summary
          WHERE device_call_in_summary.serial_number = vs_serial_number
            AND device_call_in_summary.first_connection_ts >= trunc(sysdate);
      END IF;

      RETURN vn_device_call_in_summary_id;
      
   END;

   FUNCTION fn_get_serial_number (ps_ev_number IN device.device_name%TYPE)
      RETURN device.device_serial_cd%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_SERIAL_NUMBER';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
   
      SELECT device.device_serial_cd
        INTO vs_serial_cd
        FROM device
       WHERE device.device_name = ps_ev_number
         AND device.device_active_yn_flag = 'Y';

      RETURN vs_serial_cd;
      
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;

   FUNCTION fn_get_location_name (ps_ev_number IN device.device_name%TYPE)
      RETURN LOCATION.location_name%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_LOCATION_NAME';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_location_name             LOCATION.location_name%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
   
      SELECT LOCATION.location_name
        INTO vs_location_name
        FROM LOCATION.LOCATION, device, pss.pos
       WHERE LOCATION.location_id = pss.pos.location_id
         AND pss.pos.device_id = device.device_id
         AND device.device_name = ps_ev_number;

      RETURN vs_location_name;
      
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;

   FUNCTION fn_get_customer_name (ps_ev_number IN device.device_name%TYPE)
      RETURN customer.customer_name%TYPE
   AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)    := 'FN_GET_CUSTOMER_NAME';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_customer_name             customer.customer_name%TYPE;
      vs_serial_number                device.device_serial_cd%TYPE;
      vn_device_call_in_summary_id     device_call_in_summary.device_call_in_summary_id%TYPE;
      ex_device_not_found             EXCEPTION;
      vs_error_msg                    exception_data.additional_information%TYPE;
   BEGIN
   
      SELECT customer.customer_name
        INTO vs_customer_name
        FROM LOCATION.customer, device, pss.pos
       WHERE LOCATION.customer.customer_id = pss.pos.customer_id
         AND pss.pos.device_id = device.device_id
         AND device.device_name = ps_ev_number;

      RETURN vs_customer_name;
      
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;

END;
/
