CREATE OR REPLACE TRIGGER device.trbi_host_equipment
BEFORE INSERT ON device.host_equipment
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.host_equipment_id IS NULL
   THEN
      SELECT seq_host_equipment_id.NEXTVAL
        INTO :NEW.host_equipment_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE, USER, SYSDATE,
          USER
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/