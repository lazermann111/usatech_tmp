CREATE OR REPLACE TRIGGER device.trbi_device_counter
BEFORE INSERT ON device.device_counter_old
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   -- If a new value comes in for the same machine, at the same time,
   -- then the old value will be deleted and the new value will be added.
   -- It is assumed the new value is correct.
   --DELETE FROM device_counter
   --      WHERE :NEW.device_id = device_id
   --        AND :NEW.device_counter_ts = device_counter_ts
   --        AND :NEW.device_counter_parameter = device_counter_parameter;

   IF :NEW.device_counter_id IS NULL THEN
      SELECT seq_device_counter_id.NEXTVAL
        INTO :NEW.device_counter_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/