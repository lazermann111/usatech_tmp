CREATE OR REPLACE FUNCTION DEVICE.FILE_CONTENT_LENGTH (
    p_rowid     IN rowid
)
RETURN number IS
    l_cur INTEGER DEFAULT dbms_sql.open_cursor;
    l_val LONG;
    l_val_len INTEGER;
	l_pos NUMBER := 0;
BEGIN 
    dbms_sql.parse(l_cur,
        'SELECT file_transfer_content FROM device.file_transfer WHERE rowid = :p_rowid',
        dbms_sql.native);
    dbms_sql.bind_variable(l_cur, ':p_rowid', p_rowid);
    dbms_sql.define_column_long(l_cur, 1);
    
    IF dbms_sql.execute_and_fetch(l_cur, FALSE) > 0 THEN
		LOOP
			dbms_sql.column_value_long(l_cur, 1, 32760, l_pos, l_val, l_val_len);
			EXIT WHEN l_val_len = 0;
			l_pos := l_pos + l_val_len;
		END LOOP;
    END IF;
    
    dbms_sql.close_cursor(l_cur);
    RETURN l_pos;
EXCEPTION
    WHEN OTHERS THEN
        IF dbms_sql.is_open(l_cur) THEN
            dbms_sql.close_cursor(l_cur);
        END IF;
        RAISE;
END;
/