CREATE OR REPLACE TRIGGER device.trbu_device_setting
BEFORE UPDATE ON device.device_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	LN_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
Begin
	SELECT DEVICE_TYPE_ID
	INTO LN_DEVICE_TYPE_ID
	FROM DEVICE.DEVICE
	WHERE DEVICE_ID = :NEW.DEVICE_ID;

	SELECT 
		:OLD.created_by, 
		:OLD.created_ts, 
		SYSDATE,
		USER
	INTO
		:NEW.created_by, 
		:NEW.created_ts, 
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
	
	IF UPDATING AND DBADMIN.PKG_UTL.EQL(:OLD.DEVICE_SETTING_VALUE, :NEW.DEVICE_SETTING_VALUE) = 'Y' THEN
		:NEW.CHANGED_BY := :OLD.CHANGED_BY;
		:NEW.CHANGED_TS := :OLD.CHANGED_TS;
		:NEW.OLD_DEVICE_SETTING_VALUE := :OLD.OLD_DEVICE_SETTING_VALUE;
	ELSE
		:NEW.CHANGED_TS := SYSDATE;
		:NEW.OLD_DEVICE_SETTING_VALUE := :OLD.DEVICE_SETTING_VALUE;
	END IF;
	
	IF :NEW.device_setting_parameter_cd NOT IN ('1203', '1204', '1205', '1206', '1527', '1528') THEN
		:NEW.device_setting_value := TRIM(:NEW.device_setting_value);
	END IF;
	
	IF LN_DEVICE_TYPE_ID IN (0, 1) THEN
		IF :NEW.DEVICE_SETTING_PARAMETER_CD = '387' THEN
			-- disable local auth
			:NEW.DEVICE_SETTING_VALUE := '4E';
		ELSIF :NEW.DEVICE_SETTING_PARAMETER_CD = '389' THEN
			-- force hex MDB Inventory Format
			:NEW.DEVICE_SETTING_VALUE := '59';
		END IF;
	END IF;
End;

/