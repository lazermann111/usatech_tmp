CREATE OR REPLACE FUNCTION DEVICE.GET_OR_CREATE_HOST_ID(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(1000);
    ln_new_host_count NUMBER;
BEGIN
    FOR i IN 1.. 10 LOOP
      SELECT MAX(H.HOST_ID)
        INTO ln_host_id
        FROM DEVICE.HOST H
       WHERE H.DEVICE_ID = pn_device_id
         AND H.HOST_PORT_NUM = pn_host_port_num
         AND H.HOST_POSITION_NUM = pn_host_position_num;
  
      IF ln_host_id IS NOT NULL THEN
          RETURN ln_host_id;
      END IF;
      IF pn_host_port_num != 0 THEN
          -- Use base host
          SELECT MAX(H.HOST_ID)
            INTO ln_host_id
            FROM DEVICE.HOST H
           WHERE H.DEVICE_ID = pn_device_id
             AND H.HOST_PORT_NUM = 0;
          IF ln_host_id IS NOT NULL THEN
              RETURN ln_host_id;
          END IF;
      END IF;
      -- create default hosts
      PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(pn_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
      IF ln_result_cd != PKG_CONST.RESULT__SUCCESS OR ln_new_host_count = 0 THEN
          RETURN NULL;
      END IF;
    END LOOP;
    RETURN NULL;
END;
/