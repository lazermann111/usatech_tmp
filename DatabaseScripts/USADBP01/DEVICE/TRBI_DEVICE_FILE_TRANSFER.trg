CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_FILE_TRANSFER
BEFORE INSERT
ON DEVICE.DEVICE_FILE_TRANSFER
FOR EACH ROW
BEGIN
   IF :NEW.device_file_transfer_id IS NULL THEN
      SELECT seq_device_file_transfer_id.NEXTVAL
        INTO :NEW.device_file_transfer_id
        FROM DUAL;
   END IF;
   
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
	 
	IF :NEW.device_file_transfer_status_cd = 1 AND :NEW.device_file_transfer_ts IS NULL THEN
		:NEW.device_file_transfer_ts := SYSDATE;
    END IF;
	 
	IF :NEW.device_file_transfer_status_cd = 0 AND :NEW.device_file_transfer_direct = 'I' OR :NEW.device_file_transfer_status_cd = 1 THEN
		SELECT DECODE(PARTITION_TS, EPOCH_DATE, NVL(:NEW.DEVICE_FILE_TRANSFER_TS, :NEW.CREATED_TS), PARTITION_TS)
		INTO :NEW.PARTITION_TS
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = :NEW.FILE_TRANSFER_ID;

		IF :NEW.device_file_transfer_status_cd = 1 THEN
			UPDATE DEVICE.FILE_TRANSFER FT
			SET FT.PARTITION_TS = :NEW.PARTITION_TS
			WHERE FILE_TRANSFER_ID = :NEW.FILE_TRANSFER_ID
				AND EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER_TYPE FTT 
					WHERE FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD AND FTT.DELETABLE_AFTER_TRANSFER_IND = 'Y');
		END IF;
	ELSE
		:NEW.PARTITION_TS := EPOCH_DATE;
	END IF;
END;
/
