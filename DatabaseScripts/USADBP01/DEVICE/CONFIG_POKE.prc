CREATE OR REPLACE PROCEDURE DEVICE.CONFIG_POKE
(
	pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
	pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE
)
IS
    l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
BEGIN
    IF pv_device_type_id in (0, 1) THEN
        UPDATE ENGINE.MACHINE_CMD_PENDING
        SET EXECUTE_CD='C'
        WHERE MACHINE_ID = pv_device_name
        AND DATA_TYPE = '88' 
        AND EXECUTE_ORDER > 2
        AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

    UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 0);
    UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 1);
    UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 2);
    END IF;

    
END; 
/