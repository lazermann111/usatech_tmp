CREATE OR REPLACE PROCEDURE DEVICE.ADD_DEVICE_SETTING_EXCHANGE (pn_device_id IN NUMBER, pv_device_setting_type IN VARCHAR)
IS
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;  
    lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
    lv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE;
BEGIN
	SELECT MAX(D.DEVICE_TYPE_ID), MAX(FIRMWARE_VERSION)
		INTO ln_device_type_id, lv_firmware_version
		FROM DEVICE.DEVICE D
		WHERE D.DEVICE_ID = pn_device_id and D.DEVICE_ACTIVE_YN_FLAG='Y';
     IF pv_device_setting_type IN ('Cash Enabled','Cashless Enabled','Two-Tier Pricing','DEX Schedule') THEN
        lv_device_setting_parameter_cd:=pv_device_setting_type;  
     ELSIF pv_device_setting_type = 'Two-Tier Pricing Vend/Tran Flag' THEN
        lv_device_setting_parameter_cd:='Two-Tier Pricing';  
     ELSIF (ln_device_type_id = 13 AND pv_device_setting_type='1022') or (ln_device_type_id in (0,1) and pv_device_setting_type='203') THEN
     	lv_device_setting_parameter_cd:='Cash Enabled';
	 ELSIF pv_device_setting_type='Cashless Enabled' THEN
	   lv_device_setting_parameter_cd:='Cashless Enabled';
	 ELSIF (ln_device_type_id = 13 AND pv_device_setting_type='1202') or (ln_device_type_id in (0,1) and pv_device_setting_type='362') THEN
	  	lv_device_setting_parameter_cd:='Two-Tier Pricing';
	 ELSIF ln_device_type_id = 0 AND pv_device_setting_type in ('385','172') THEN
	  	lv_device_setting_parameter_cd:='DEX Schedule';
	 ELSIF ln_device_type_id = 1 AND DBADMIN.VERSION_COMPARE(SUBSTR(lv_firmware_version, 9),'6.0.7')<0 and pv_device_setting_type in ('385','172') THEN
	 	lv_device_setting_parameter_cd:='DEX Schedule';
	 ELSIF ln_device_type_id = 1 AND DBADMIN.VERSION_COMPARE(SUBSTR(lv_firmware_version, 9),'6.0.7')>=0 and pv_device_setting_type in ('358','244') THEN
	 	lv_device_setting_parameter_cd:='DEX Schedule';
     ELSIF ln_device_type_id = 13 AND pv_device_setting_type = '1101' THEN
     	lv_device_setting_parameter_cd:='DEX Schedule';
	 END IF;
      
      IF lv_device_setting_parameter_cd is not null THEN
      	INSERT INTO REPORT.DATA_EXCHANGE_ITEM(DATA_EXCHANGE_MAPPING_ID,DATA_EXCHANGE_ITEM_TYPE_ID,ENTITY_ID, ENTITY_KEY_CD, DATA_EXCHANGE_ITEM_ACTION_CD,DATA_EXCHANGE_ITEM_SOURCE)
    		SELECT DISTINCT FIRST_VALUE(DEM.DATA_EXCHANGE_MAPPING_ID) OVER (PARTITION BY T.TERMINAL_ID ORDER BY DEM.TERMINAL_ID ASC NULLS LAST), 4,
    		E.EPORT_ID,
    		lv_device_setting_parameter_cd,
    		'A',
    		'DEVICE.ADD_DEVICE_SETTING_EXCHANGE'
    		FROM REPORT.DATA_EXCHANGE_MAPPING DEM JOIN REPORT.TERMINAL T on DEM.CUSTOMER_ID=T.CUSTOMER_ID and (DEM.TERMINAL_ID IS NULL OR DEM.TERMINAL_ID = T.TERMINAL_ID)
	    	JOIN REPORT.VW_TERMINAL_EPORT TE on T.TERMINAL_ID=TE.TERMINAL_ID 
	    	JOIN REPORT.EPORT E ON E.EPORT_ID=TE.EPORT_ID
	    	JOIN DEVICE.DEVICE D on D.DEVICE_SERIAL_CD=E.EPORT_SERIAL_NUM AND D.DEVICE_ID=pn_device_id
	    	WHERE NOT EXISTS (SELECT 1 FROM REPORT.DATA_EXCHANGE_ITEM WHERE DATA_EXCHANGE_ITEM_TYPE_ID=4 AND ENTITY_ID=E.EPORT_ID AND ENTITY_KEY_CD=lv_device_setting_parameter_cd AND PROCESS_STATUS_CD='P')
	    	AND DEM.DATA_EXCHANGE_TYPE_ID != NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_TRANSACTION_VARIABLE('SOURCE_DATA_EXCHANGE_TYPE_ID')), 0);
	    END IF;
END;
/
