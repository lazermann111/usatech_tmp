CREATE OR REPLACE TRIGGER device.trbi_host_setting
BEFORE INSERT ON device.host_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;

/