CREATE OR REPLACE PROCEDURE DEVICE.CREATE_PENDING_COMMANDS(
		pv_device_query VARCHAR2,
		pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pn_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pv_command IN OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
		pv_result_message OUT VARCHAR2
	) IS
		ln_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		lv_data_type VARCHAR2(2);
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
		ln_file_size NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
		lv_device_name DEVICE.DEVICE_NAME%TYPE;
		ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
		ln_execute_order NUMBER;
		ln_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE;
		ln_command_count NUMBER;
		ln_created_commands_count NUMBER := 0;
		TYPE DevCurType IS REF CURSOR;
		l_dev_cur DevCurType;
		ERR20208 EXCEPTION;
		PRAGMA EXCEPTION_INIT(ERR20208, -20208);
	BEGIN
		IF pn_file_transfer_type_cd IS NULL THEN
			ln_file_transfer_type_cd := 36;
		ELSE
			ln_file_transfer_type_cd := pn_file_transfer_type_cd;
		END IF;

		SELECT MAX(FILE_TRANSFER_ID)
		INTO ln_file_transfer_id
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_TYPE_CD = ln_file_transfer_type_cd AND COALESCE(STATUS_CD, 'A') != 'D';

		IF COALESCE(ln_file_transfer_id, 0) = 0 THEN
			pv_result_message := 'No file tranfer of type ' || ln_file_transfer_type_cd || ' found!';
			RETURN;
		END IF;

		SELECT COALESCE(pv_command, RAWTOHEX(file_transfer_name)), DBMS_LOB.GETLENGTH(FILE_TRANSFER_CONTENT)
		INTO pv_command, ln_file_size
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id;

 		IF pn_packet_size IS NULL THEN
			ln_packet_size := 1024;
		ELSE
			ln_packet_size := pn_packet_size;
		END IF;

		OPEN l_dev_cur FOR pv_device_query USING ln_file_transfer_id;

		LOOP
			FETCH l_dev_cur 
			INTO ln_device_id, lv_device_name, ln_device_type_id;
			EXIT WHEN l_dev_cur%NOTFOUND;

			IF ln_device_type_id = 13 THEN
				IF ln_file_size > pn_packet_size THEN
					lv_data_type := 'C8';
				ELSE
					lv_data_type := 'C7';
				END IF;
			ELSIF ln_device_type_id IN (1, 11) THEN
				lv_data_type := 'A4';
			ELSE
				lv_data_type := '7C';
			END IF;

			select /*+INDEX(mcp IDX_MACHINE_CMD_PENDING_1)*/ count(1)
			into ln_command_count
			from device.device_file_transfer dft
			join engine.machine_cmd_pending mcp on dft.device_file_transfer_id = mcp.device_file_transfer_id
			where dft.device_id = ln_device_id and dft.file_transfer_id = ln_file_transfer_id 
				and dft.device_file_transfer_status_cd = 0
				and dft.device_file_transfer_direct = 'O' and mcp.machine_id = lv_device_name
				and mcp.data_type = lv_data_type AND dft.created_ts > SYSDATE - 2 / 24;

			IF ln_command_count > 0 THEN
				CONTINUE;
			END IF;

			BEGIN
			UPDATE engine.machine_cmd_pending
			SET execute_cd = 'C'
			WHERE machine_command_pending_id IN (
				SELECT mcp.machine_command_pending_id
				FROM engine.machine_cmd_pending mcp
				JOIN device.device_file_transfer dft ON mcp.device_file_transfer_id = dft.device_file_transfer_id
				JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
				JOIN device.file_transfer_type ftt ON ft.file_transfer_type_cd = ftt.file_transfer_type_cd
				WHERE mcp.machine_id = lv_device_name AND (
					ft.file_transfer_type_cd = ln_file_transfer_type_cd AND ftt.cancel_pending_transfers_ind = 'Y'
					OR mcp.data_type = '9B' AND mcp.command = pv_command
				)
				AND dft.created_ts < SYSDATE - 2 / 24
			);

			SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;

			INSERT INTO device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size)
			VALUES(ln_device_file_transfer_id, ln_device_id, ln_file_transfer_id, 'O', 0, pn_packet_size);

			SELECT /*+INDEX(mcp IDX_MACHINE_CMD_PENDING_1)*/ NVL(MAX(execute_order), 0) + 1
			INTO ln_execute_order
			FROM engine.machine_cmd_pending
			WHERE machine_id = lv_device_name;

			INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_order, device_file_transfer_id)
			SELECT lv_device_name, lv_data_type, pv_command, ln_execute_order, ln_device_file_transfer_id
			FROM DUAL WHERE NOT EXISTS (
				SELECT 1 FROM engine.machine_cmd_pending
				WHERE machine_id = lv_device_name
					AND data_type = lv_data_type
					AND DBADMIN.PKG_UTL.EQL(command, pv_command) = 'Y'
					AND DBADMIN.PKG_UTL.EQL(device_file_transfer_id, ln_device_file_transfer_id) = 'Y'
			);

			COMMIT;
			ln_created_commands_count := ln_created_commands_count + 1;
			EXCEPTION
				WHEN ERR20208 THEN
					ROLLBACK;
			END;
		END LOOP;
		pv_result_message := 'Created ' || ln_created_commands_count || ' pending commands';
	END;
/
