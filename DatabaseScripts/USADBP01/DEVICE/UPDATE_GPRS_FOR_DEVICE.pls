CREATE OR REPLACE PROCEDURE DEVICE.UPDATE_GPRS_FOR_DEVICE(
    pn_host_id HOST.HOST_ID%TYPE,
    pv_component_info GPRS_DEVICE.MODEM_INFO%TYPE,
    pn_local_time NUMBER)
IS
    lc_comm_method_cd HOST_TYPE.COMM_METHOD_CD%TYPE;
    ln_ccid GPRS_DEVICE.ICCID%TYPE;
    ln_device_id GPRS_DEVICE.DEVICE_ID%TYPE;
    ln_prev_device_id	GPRS_DEVICE.DEVICE_ID%TYPE;
    lv_device_type_name GPRS_DEVICE.DEVICE_TYPE_NAME%TYPE;
    lv_device_firmware_name GPRS_DEVICE.DEVICE_FIRMWARE_NAME%TYPE;
    ln_imei GPRS_DEVICE.IMEI%TYPE;
    lv_rssi GPRS_DEVICE.RSSI%TYPE;
    ld_rssi_ts GPRS_DEVICE.RSSI_TS%TYPE := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_local_time) AS DATE);
BEGIN
    -- does device have a gprs modem?
    SELECT H.DEVICE_ID, CASE WHEN REGEXP_LIKE(H.HOST_SERIAL_CD, '^[0-9]{19,22}$') THEN DBADMIN.TO_NUMBER_OR_NULL(H.HOST_SERIAL_CD) END CCID, 
           HE.HOST_EQUIPMENT_MFGR DEVICE_TYPE_NAME, HS_FV.HOST_SETTING_VALUE DEVICE_FIRMWARE_NAME, 
           DBADMIN.TO_NUMBER_OR_NULL(H.HOST_LABEL_CD) IMEI, HS_CSQ.HOST_SETTING_VALUE RSSI, HT.COMM_METHOD_CD
      INTO ln_device_id, ln_ccid, lv_device_type_name, lv_device_firmware_name, ln_imei, lv_rssi, lc_comm_method_cd  
      FROM DEVICE.HOST H
      JOIN DEVICE.HOST_EQUIPMENT HE ON H.HOST_EQUIPMENT_ID = HE.HOST_EQUIPMENT_ID
      LEFT OUTER JOIN DEVICE.HOST_SETTING HS_FV ON H.HOST_ID = HS_FV.HOST_ID AND HS_FV.HOST_SETTING_PARAMETER = 'Firmware Version'
      LEFT OUTER JOIN DEVICE.HOST_SETTING HS_CSQ ON H.HOST_ID = HS_CSQ.HOST_ID AND HS_CSQ.HOST_SETTING_PARAMETER = 'CSQ'
      JOIN DEVICE.HOST_TYPE HT ON H.HOST_TYPE_ID = HT.HOST_TYPE_ID
     WHERE H.HOST_ID = pn_host_id;
    
    IF lc_comm_method_cd IS NULL THEN
        RETURN;
    END IF;
    
    IF lc_comm_method_cd IN('G', 'S', 'R', 'A', 'Y', 'O', '5', 'I') THEN
        IF ln_ccid IS NULL THEN
            RETURN;
        END IF;
        UPDATE DEVICE.GPRS_DEVICE
        SET DEVICE_ID = NULL,
          GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
        WHERE DEVICE_ID = ln_device_id
          AND ICCID != ln_ccid;		
		
        BEGIN
            SELECT DEVICE_ID 
              INTO ln_prev_device_id 
              FROM DEVICE.GPRS_DEVICE 
             WHERE ICCID = ln_ccid;
        
            IF ln_prev_device_id IS NULL OR ln_prev_device_id != ln_device_id THEN
              UPDATE DEVICE.GPRS_DEVICE
              SET DEVICE_ID = ln_device_id,
              GPRS_DEVICE_STATE_ID = 5,
              ASSIGNED_BY = 'APP_LAYER',
              ASSIGNED_TS = SYSDATE,
              DEVICE_TYPE_NAME = lv_device_type_name,
              DEVICE_FIRMWARE_NAME = lv_device_firmware_name,
              IMEI = ln_imei,
              RSSI = lv_rssi,
              RSSI_TS = ld_rssi_ts,
              LAST_FILE_TRANSFER_ID = NULL,
              MODEM_INFO_RECEIVED_TS = SYSDATE,
              MODEM_INFO = pv_component_info
              WHERE ICCID = ln_ccid
                AND (RSSI_TS IS NULL OR RSSI_TS < ld_rssi_ts);
            ELSE
              UPDATE DEVICE.GPRS_DEVICE
              SET DEVICE_TYPE_NAME = lv_device_type_name,
              DEVICE_FIRMWARE_NAME = lv_device_firmware_name,
              IMEI = ln_imei,
              RSSI = lv_rssi,
              RSSI_TS = ld_rssi_ts,
              LAST_FILE_TRANSFER_ID = NULL,
              MODEM_INFO_RECEIVED_TS = SYSDATE,
              MODEM_INFO = pv_component_info
              WHERE ICCID = ln_ccid
                AND (RSSI_TS IS NULL OR RSSI_TS < ld_rssi_ts);
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
              BEGIN
                  INSERT INTO DEVICE.GPRS_DEVICE(DEVICE_ID, DEVICE_TYPE_NAME, DEVICE_FIRMWARE_NAME, IMEI, RSSI, RSSI_TS, GPRS_DEVICE_STATE_ID, ASSIGNED_BY, ASSIGNED_TS, 
                    ICCID, MODEM_INFO_RECEIVED_TS, MODEM_INFO)
                  VALUES(ln_device_id, lv_device_type_name, lv_device_firmware_name, ln_imei, lv_rssi, ld_rssi_ts, 5, 'APP_LAYER', SYSDATE, 
                    ln_ccid, SYSDATE, pv_component_info);
                EXCEPTION
                  WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
                END;
        END;
    ELSE
        UPDATE DEVICE.GPRS_DEVICE
           SET DEVICE_ID = NULL,
               DEVICE_TYPE_NAME = NULL,
               DEVICE_FIRMWARE_NAME = NULL, 
               IMEI = NULL,
               ASSIGNED_BY = NULL,
               ASSIGNED_TS = NULL,
               GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)	   
         WHERE DEVICE_ID = ln_device_id;
    END IF;
END;
/