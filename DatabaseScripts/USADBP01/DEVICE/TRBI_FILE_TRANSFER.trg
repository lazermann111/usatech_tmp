CREATE OR REPLACE TRIGGER device.trbi_file_transfer
BEFORE INSERT ON device.file_transfer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.file_transfer_id IS NULL THEN

      SELECT seq_file_transfer_id.nextval
        into :new.file_transfer_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;

/