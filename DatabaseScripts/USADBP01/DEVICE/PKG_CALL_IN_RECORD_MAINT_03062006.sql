-- Start of DDL Script for Package DEVICE.PKG_CALL_IN_RECORD_MAINT
-- Generated 2-Mar-2006 16:12:46 from DEVICE@USADBD02.USATECH.COM

CREATE OR REPLACE 
PACKAGE pkg_call_in_record_maint
IS
   PROCEDURE sp_start (
      ps_ev_number       IN   device.device_name%TYPE,
      ps_network_layer   IN   device_call_in_record.network_layer%TYPE,
      ps_ip_address      IN   device_call_in_record.ip_address%TYPE,
      ps_modem_id        IN   device_call_in_record.modem_id%TYPE,
      ps_rssi            IN   device_call_in_record.rssi%TYPE
   );

   PROCEDURE sp_add_trans (
      ps_ev_number      IN   device.device_name%TYPE,
      pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
      pn_trans_amount   IN   device_call_in_record.credit_trans_total%TYPE,
      pn_vend_count     IN   device_call_in_record.credit_vend_count%TYPE
   );

   PROCEDURE sp_add_auth (
      ps_ev_number       IN   device.device_name%TYPE,
      pc_card_type       IN   device_call_in_record.auth_card_type%TYPE,
      pn_auth_amount     IN   device_call_in_record.auth_amount%TYPE,
      pc_approved_flag   IN   device_call_in_record.auth_approved_flag%TYPE
   );

   PROCEDURE sp_set_dex_received (
      ps_ev_number       IN   device.device_name%TYPE,
      pn_dex_file_size   IN   device_call_in_record.dex_file_size%TYPE
   );

   PROCEDURE sp_set_initialized (ps_ev_number IN device.device_name%TYPE);

   PROCEDURE sp_set_device_sent_config (
      ps_ev_number   IN   device.device_name%TYPE
   );

   PROCEDURE sp_set_server_sent_config (
      ps_ev_number   IN   device.device_name%TYPE
   );

   PROCEDURE sp_set_status (
      ps_ev_number     IN   device.device_name%TYPE,
      pc_status_flag   IN   device_call_in_record.call_in_status%TYPE
   );

   PROCEDURE sp_set_type (
      ps_ev_number   IN   device.device_name%TYPE,
      pc_type_flag   IN   device_call_in_record.call_in_type%TYPE
   );

   PROCEDURE sp_set_type (
      pn_device_call_in_record_id   IN   device_call_in_record.device_call_in_record_id%TYPE,
      pc_type_flag                  IN   device_call_in_record.call_in_type%TYPE
   );

   PROCEDURE sp_finish (ps_ev_number IN device.device_name%TYPE);

   PROCEDURE sp_add_message (
      ps_ev_number   IN   device.device_name%TYPE,
      pc_direction   IN   CHAR,
      pn_num_bytes   IN   device_call_in_record.inbound_byte_count%TYPE
   );

   FUNCTION fn_get_current_call_in_id (
      ps_ev_number   IN   device.device_name%TYPE
   )
      RETURN device_call_in_record.device_call_in_record_id%TYPE;

   FUNCTION fn_get_serial_number (ps_ev_number IN device.device_name%TYPE)
      RETURN device.device_serial_cd%TYPE;

   FUNCTION fn_get_location_name (ps_ev_number IN device.device_name%TYPE)
      RETURN LOCATION.location_name%TYPE;

   FUNCTION fn_get_customer_name (ps_ev_number IN device.device_name%TYPE)
      RETURN customer.customer_name%TYPE;
/*
   FUNCTION fn_get_call_in_detail (ps_ev_number IN device.device_name%TYPE)
      RETURN CHAR;
*/
END;
/

-- Grants for Package
GRANT EXECUTE ON pkg_call_in_record_maint TO public
/


-- End of DDL Script for Package DEVICE.PKG_CALL_IN_RECORD_MAINT

