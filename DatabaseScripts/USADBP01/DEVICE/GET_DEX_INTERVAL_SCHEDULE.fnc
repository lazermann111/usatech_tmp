CREATE OR REPLACE FUNCTION DEVICE.GET_DEX_INTERVAL_SCHEDULE (pv_value IN varchar, pn_device_utc_offset_min IN NUMBER)
   RETURN varchar
IS
   l_interval_schedule varchar(100);
   l_utc_offset_sec NUMBER;
   l_hour NUMBER;
   l_minutes NUMBER;
   l_interval_sec NUMBER;
   l_interval_minutes NUMBER;
   l_utc_local_offset_min NUMBER;
   l_hour_var VARCHAR(2);
   l_min_var VARCHAR(2);
BEGIN
   select to_number(substr(pv_value,INSTR(pv_value,'^', 1,1)+1,INSTR(pv_value,'^', 1,2)-INSTR(pv_value,'^', 1,1)-1)) into l_utc_offset_sec from dual;
   select to_number(substr(pv_value,INSTR(pv_value,'^', 1,2)+1)) into l_interval_sec from dual;
   l_utc_local_offset_min:=MOD(((l_utc_offset_sec - pn_device_utc_offset_min * 60) / 60),  1440);
   IF l_utc_local_offset_min < 0 THEN
				l_utc_local_offset_min := l_utc_local_offset_min+1440;
   END IF;
   l_hour := FLOOR(l_utc_local_offset_min / 60);
   l_minutes:=MOD(l_utc_local_offset_min,60);
   l_interval_minutes:=MOD(l_interval_sec/60,1440);
   IF l_hour < 10 THEN
    l_hour_var:='0'||l_hour;
   ELSE
    l_hour_var:=to_char(l_hour);
   END IF;
   IF l_minutes < 10 THEN
    l_min_var:='0'||l_minutes;
   ELSE
    l_min_var:=to_char(l_minutes);
   END IF;
   RETURN 'I^'||l_hour_var||l_min_var||'^'||l_interval_minutes;
END;
/
