CREATE OR REPLACE TRIGGER device.trbi_device_decode_fail
BEFORE INSERT ON device.device_decode_fail
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.device_decode_fail_id IS NULL
	THEN
		SELECT SEQ_DEVICE_DECODE_FAIL_ID.NEXTVAL
		INTO :NEW.device_decode_fail_id
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/