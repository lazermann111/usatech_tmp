CREATE OR REPLACE TRIGGER device.trbu_gprs_device
BEFORE UPDATE ON device.gprs_device
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
   SELECT SYSDATE, USER
     INTO :NEW.last_updated_ts, :NEW.last_updated_by
     FROM DUAL;

   IF (   :NEW.gprs_device_state_id < :OLD.gprs_device_state_id
       OR :NEW.iccid <> :OLD.iccid
       OR (:OLD.imsi IS NOT NULL AND :NEW.imsi <> :OLD.imsi)
       OR (:OLD.msisdn IS NOT NULL AND :NEW.msisdn <> :OLD.msisdn)
       OR (:OLD.phone_number IS NOT NULL AND :NEW.phone_number <> :OLD.phone_number)
       OR (:OLD.rate_plan_name IS NOT NULL AND :NEW.rate_plan_name <> :OLD.rate_plan_name)
       OR (:OLD.device_id IS NOT NULL AND (:NEW.device_id IS NULL OR :NEW.device_id <> :OLD.device_id))
       OR (:OLD.imei IS NOT NULL AND :NEW.imei <> :OLD.imei)
       OR (:OLD.profile_name IS NOT NULL AND :NEW.profile_name <> :OLD.profile_name)
      )
   THEN
      INSERT INTO device.gprs_device_hist
                  (gprs_device_id, gprs_device_state_id,
                   ordered_by, ordered_ts, provider_order_id,
                   ordered_notes, iccid, imsi,
                   allocated_by, allocated_ts, allocated_to,
                   allocated_notes, billable_to_name,
                   billable_to_notes, activated_by,
                   activated_ts, activated_notes,
                   provider_activation_id, provider_activation_ts,
                   msisdn, phone_number, rate_plan_name,
                   assigned_by, assigned_ts, assigned_notes,
                   device_id, imei, device_type_name,
                   device_firmware_name, rssi,
                   pin1, puk1, pin2, puk2,
                   last_file_transfer_id,
                   profile_name,
				   rssi_ts,
				   modem_info_received_ts,
				   modem_info
                  )
           VALUES (:OLD.gprs_device_id, :OLD.gprs_device_state_id,
                   :OLD.ordered_by, :OLD.ordered_ts, :OLD.provider_order_id,
                   :OLD.ordered_notes, :OLD.iccid, :OLD.imsi,
                   :OLD.allocated_by, :OLD.allocated_ts, :OLD.allocated_to,
                   :OLD.allocated_notes, :OLD.billable_to_name,
                   :OLD.billable_to_notes, :OLD.activated_by,
                   :OLD.activated_ts, :OLD.activated_notes,
                   :OLD.provider_activation_id, :OLD.provider_activation_ts,
                   :OLD.msisdn, :OLD.phone_number, :OLD.rate_plan_name,
                   :OLD.assigned_by, :OLD.assigned_ts, :OLD.assigned_notes,
                   :OLD.device_id, :OLD.imei, :OLD.device_type_name,
                   :OLD.device_firmware_name, :OLD.rssi,
                   :OLD.pin1, :OLD.puk1, :OLD.pin2, :OLD.puk2,
                   :OLD.last_file_transfer_id,
                   :OLD.profile_name,
				   :OLD.rssi_ts,
				   :OLD.modem_info_received_ts,
				   :OLD.modem_info
                  );
   END IF;
END;
/