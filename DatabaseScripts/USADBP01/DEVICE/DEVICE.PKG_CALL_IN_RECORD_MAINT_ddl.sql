CREATE OR REPLACE 
PACKAGE device.pkg_call_in_record_maint IS
   PROCEDURE sp_start (
      ps_ev_number IN device.device_name%TYPE,
      ps_network_layer IN device_call_in_record.network_layer%TYPE,
      ps_ip_address IN device_call_in_record.ip_address%TYPE,
      ps_modem_id IN device_call_in_record.modem_id%TYPE,
      ps_rssi IN device_call_in_record.rssi%TYPE
   );

   PROCEDURE sp_add_trans (
      ps_ev_number IN device.device_name%TYPE,
      pc_card_type IN device_call_in_record.auth_card_type%TYPE,
      pn_trans_amount IN device_call_in_record.credit_trans_total%TYPE,
      pn_vend_count IN device_call_in_record.credit_vend_count%TYPE
   );

   PROCEDURE sp_add_auth (
      ps_ev_number IN device.device_name%TYPE,
      pc_card_type IN device_call_in_record.auth_card_type%TYPE,
      pn_auth_amount IN device_call_in_record.auth_amount%TYPE,
      pc_approved_flag IN device_call_in_record.auth_approved_flag%TYPE
   );

   PROCEDURE sp_set_dex_received (
      ps_ev_number IN device.device_name%TYPE,
      pn_dex_file_size IN device_call_in_record.dex_file_size%TYPE
   );

   PROCEDURE sp_set_initialized (
      ps_ev_number IN device.device_name%TYPE
   );

   PROCEDURE sp_set_device_sent_config (
      ps_ev_number IN device.device_name%TYPE
   );

   PROCEDURE sp_set_server_sent_config (
      ps_ev_number IN device.device_name%TYPE
   );

   PROCEDURE sp_set_status (
      ps_ev_number IN device.device_name%TYPE,
      pc_status_flag IN device_call_in_record.call_in_status%TYPE
   );

   PROCEDURE sp_set_type (
      ps_ev_number IN device.device_name%TYPE,
      pc_type_flag IN device_call_in_record.call_in_type%TYPE
   );

   PROCEDURE sp_set_type (
      pn_device_call_in_record_id IN device_call_in_record.device_call_in_record_id%TYPE,
      pc_type_flag IN device_call_in_record.call_in_type%TYPE
   );

   PROCEDURE sp_finish (
      ps_ev_number IN device.device_name%TYPE
   );

   PROCEDURE sp_add_message (
      ps_ev_number IN device.device_name%TYPE,
      pc_direction IN CHAR,
      pn_num_bytes IN device_call_in_record.inbound_byte_count%TYPE
   );

   FUNCTION fn_get_current_call_in_id (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN device_call_in_record.device_call_in_record_id%TYPE;

   FUNCTION fn_get_serial_number (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN device.device_serial_cd%TYPE;

   FUNCTION fn_get_LOCATION_NAME (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN LOCATION.LOCATION_NAME%TYPE;

   FUNCTION fn_get_customer_name (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN customer.customer_name%TYPE;
END;
/


CREATE OR REPLACE 
PACKAGE BODY               device.pkg_call_in_record_maint IS
   -- Status constants
   cc_status_incomplete   CONSTANT CHAR (1)                              := 'I';
   cc_status_complete     CONSTANT CHAR (1)                              := 'S';
   cc_status_failed       CONSTANT CHAR (1)                              := 'U';
   cc_status_unknown      CONSTANT CHAR (1)                              := 'X';
   -- Call type contants
   cc_call_type_unknown   CONSTANT CHAR (1)                              := 'U';
   cc_call_type_auth      CONSTANT CHAR (1)                              := 'A';
   cc_call_type_batch     CONSTANT CHAR (1)                              := 'B';
   cc_call_type_combo     CONSTANT CHAR (1)                              := 'C';
   -- Approved constants
   cc_auth_approved       CONSTANT CHAR (1)                              := 'Y';
   cc_auth_denied         CONSTANT CHAR (1)                              := 'N';
   cc_auth_failed         CONSTANT CHAR (1)                              := 'F';
   -- Transaction type constants
   cc_credit_trans        CONSTANT CHAR (1)                              := 'C';
   cc_passcard_trans      CONSTANT CHAR (1)                              := 'P';
   cc_cash_trans          CONSTANT CHAR (1)                              := 'M';
   -- Message direction constants
   cc_inbound             CONSTANT CHAR (1)                              := 'I';
   cc_outbound            CONSTANT CHAR (1)                              := 'O';
   -- Logging contants
   cs_server_name         CONSTANT VARCHAR2 (30)                  := 'TAZ3';
   cs_package_name        CONSTANT VARCHAR2 (30)  := 'PKG_CALL_IN_RECORD_MAINT';
   -- Usefull variables
   vs_serial_number                device.device_serial_cd%TYPE;
   vn_device_call_in_record_id     device_call_in_record.device_call_in_record_id%TYPE;
   -- Exception stuff
   ex_device_not_found             EXCEPTION;
   vs_error_msg                    exception_data.additional_information%TYPE;

   PROCEDURE sp_start (
      ps_ev_number IN device.device_name%TYPE,
      ps_network_layer IN device_call_in_record.network_layer%TYPE,
      ps_ip_address IN device_call_in_record.ip_address%TYPE,
      ps_modem_id IN device_call_in_record.modem_id%TYPE,
      ps_rssi IN device_call_in_record.rssi%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)                  := 'SP_START';
      vn_last_call_in_record_id    device_call_in_record.device_call_in_record_id%TYPE;
      vs_LOCATION_NAME             LOCATION.LOCATION_NAME%TYPE;
      vs_customer_name             customer.customer_name%TYPE;
   BEGIN
      -- First get the serial number
      vs_serial_number := fn_get_serial_number (ps_ev_number);

      IF vs_serial_number IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      -- First check if there is an active record for this device
      -- An active record is one that has been updated in the last minute, has an
      -- incomplete status, and finished date is not set
      SELECT MAX (device_call_in_record.device_call_in_record_id)
        INTO vn_last_call_in_record_id
        FROM device_call_in_record
       WHERE device_call_in_record.serial_number = vs_serial_number
         AND device_call_in_record.call_in_status = cc_status_incomplete
         AND device_call_in_record.call_in_finish_ts IS NULL
         AND device_call_in_record.last_updated_ts >= (SYSDATE - (1 / 1440));

      IF vn_last_call_in_record_id IS NOT NULL THEN
         -- A recent record exists, assume it's the same call-in and update it
         -- Don't overwrite any existing information, unless it's null
         UPDATE device_call_in_record
            SET network_layer = NVL (network_layer, ps_network_layer),
                ip_address = NVL (ip_address, ps_ip_address),
                modem_id = NVL (modem_id, ps_modem_id),
                rssi = NVL (rssi, ps_rssi)
          WHERE device_call_in_record.device_call_in_record_id = vn_last_call_in_record_id;
          
      ELSE
         -- this is a new call
         -- if there are any existing calls with incomplete status, update them to failed

          SELECT MAX (device_call_in_record.device_call_in_record_id)
          INTO vn_last_call_in_record_id
          FROM device_call_in_record
          WHERE device_call_in_record.serial_number = vs_serial_number
          AND device_call_in_record.call_in_status = cc_status_incomplete;

          IF vn_last_call_in_record_id IS NOT NULL THEN
            -- An in-progress record exists, mark it uncessfull
              UPDATE device_call_in_record
              SET device_call_in_record.call_in_status = 'U'
              WHERE device_call_in_record.device_call_in_record_id = vn_last_call_in_record_id;
          END IF;

         -- Start a new device_call_in_record
         -- Lookup the location and customer
         vs_LOCATION_NAME := fn_get_LOCATION_NAME (ps_ev_number);
         vs_customer_name := fn_get_customer_name (ps_ev_number);

         INSERT INTO device_call_in_record
                     (serial_number, call_in_start_ts, call_in_status,
                      network_layer, ip_address, modem_id, rssi,
                      LOCATION_NAME, customer_name
                     )
              VALUES (vs_serial_number, SYSDATE, cc_status_incomplete,
                      ps_network_layer, ps_ip_address, ps_modem_id, ps_rssi,
                      vs_LOCATION_NAME, vs_customer_name
                     );
      END IF;
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_add_trans (
      ps_ev_number IN device.device_name%TYPE,
      pc_card_type IN device_call_in_record.auth_card_type%TYPE,
      pn_trans_amount IN device_call_in_record.credit_trans_total%TYPE,
      pn_vend_count IN device_call_in_record.credit_vend_count%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_TRANS';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      IF pc_card_type = cc_credit_trans THEN
         UPDATE device_call_in_record
            SET credit_trans_count = (credit_trans_count + 1),
                credit_trans_total = (credit_trans_total + pn_trans_amount),
                credit_vend_count = (credit_vend_count + pn_vend_count),
                last_trans_in_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      ELSIF pc_card_type = cc_cash_trans THEN
         UPDATE device_call_in_record
            SET cash_trans_count = (cash_trans_count + 1),
                cash_trans_total = (cash_trans_total + pn_trans_amount),
                cash_vend_count = (cash_vend_count + pn_vend_count),
                last_trans_in_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      ELSIF pc_card_type = cc_passcard_trans THEN
         UPDATE device_call_in_record
            SET passcard_trans_count = (passcard_trans_count + 1),
                passcard_trans_total = (passcard_trans_total + pn_trans_amount),
                passcard_vend_count = (passcard_vend_count + pn_vend_count),
                last_trans_in_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      END IF;
      
      sp_set_type(vn_device_call_in_record_id,cc_call_type_batch);
      
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_add_auth (
      ps_ev_number IN device.device_name%TYPE,
      pc_card_type IN device_call_in_record.auth_card_type%TYPE,
      pn_auth_amount IN device_call_in_record.auth_amount%TYPE,
      pc_approved_flag IN device_call_in_record.auth_approved_flag%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_AUTH';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET auth_amount = pn_auth_amount,
             auth_approved_flag = pc_approved_flag,
             auth_card_type = pc_card_type,
             last_auth_in_ts = SYSDATE
       WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
                                                     
       sp_set_type(vn_device_call_in_record_id,cc_call_type_auth);
       
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_set_dex_received (
      ps_ev_number IN device.device_name%TYPE,
      pn_dex_file_size IN device_call_in_record.dex_file_size%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_DEX_RECEIVED';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET dex_received_flag = 'Y',
             dex_file_size = pn_dex_file_size

       WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
                                                     
       sp_set_type(vn_device_call_in_record_id,cc_call_type_batch);

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_set_initialized (
      ps_ev_number IN device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_INITIALIZED';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET device_initialized_flag = 'Y'
       WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;

       sp_set_type(vn_device_call_in_record_id,cc_call_type_batch);

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_set_device_sent_config (
      ps_ev_number IN device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_DEVICE_SENT_CONFIG';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET device_sent_config_flag = 'Y'
       WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;

       sp_set_type(vn_device_call_in_record_id,cc_call_type_batch);

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_set_server_sent_config (
      ps_ev_number IN device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_SERVER_SENT_CONFIG';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      UPDATE device_call_in_record
         SET server_sent_config_flag = 'Y'
       WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;

       sp_set_type(vn_device_call_in_record_id,cc_call_type_batch);

   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_set_status (
      ps_ev_number IN device.device_name%TYPE,
      pc_status_flag IN device_call_in_record.call_in_status%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_STATUS';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      -- if new_status is Success, overwrite all
      -- if new_status is Failed, overwrite all but success
      -- if new_status is Unknown, overwrite only incomplete
      -- if new_status is Incomplete, overwrite none
      IF pc_status_flag = cc_status_complete THEN
         UPDATE device_call_in_record
            SET call_in_status = pc_status_flag
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      ELSIF pc_status_flag = cc_status_failed THEN
         UPDATE device_call_in_record
            SET call_in_status =
                   DECODE (call_in_status,
                           cc_status_complete, call_in_status,
                           pc_status_flag
                          )
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      ELSIF pc_status_flag = cc_status_unknown THEN
         UPDATE device_call_in_record
            SET call_in_status =
                   DECODE (call_in_status,
                           cc_status_incomplete, pc_status_flag,
                           call_in_status
                          )
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      END IF;
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;
   
   PROCEDURE sp_set_type (
      ps_ev_number IN device.device_name%TYPE,
      pc_type_flag IN device_call_in_record.call_in_type%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_TYPE';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;
      
      sp_set_type(vn_device_call_in_record_id, pc_type_flag);
      
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;
   
   PROCEDURE sp_set_type (
      pn_device_call_in_record_id IN device_call_in_record.device_call_in_record_id%TYPE,
      pc_type_flag IN device_call_in_record.call_in_type%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_SET_TYPE';
      vc_current_type_flag        device_call_in_record.call_in_type%TYPE;
   BEGIN
   
      SELECT call_in_type
      INTO vc_current_type_flag
      FROM device_call_in_record
      WHERE device_call_in_record.device_call_in_record_id =
                                                     pn_device_call_in_record_id;
      IF vc_current_type_flag = cc_call_type_auth AND pc_type_flag = cc_call_type_batch THEN
          UPDATE device_call_in_record
          SET call_in_type = cc_call_type_combo
          WHERE device_call_in_record.device_call_in_record_id = pn_device_call_in_record_id;

      ELSIF vc_current_type_flag = cc_call_type_batch AND pc_type_flag = cc_call_type_auth THEN
          UPDATE device_call_in_record
          SET call_in_type = cc_call_type_combo
          WHERE device_call_in_record.device_call_in_record_id = pn_device_call_in_record_id;

      ELSE
          UPDATE device_call_in_record
          SET call_in_type = pc_type_flag
          WHERE device_call_in_record.device_call_in_record_id = pn_device_call_in_record_id;

      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_finish (
      ps_ev_number IN device.device_name%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_FINISH';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      -- if call_in_status is Incomplete, update it to Unknown
      UPDATE device_call_in_record
         SET call_in_finish_ts = SYSDATE,
             call_in_status =
                DECODE (call_in_status,
                        cc_status_incomplete, cc_status_failed,
                        call_in_status
                       )
       WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   PROCEDURE sp_add_message (
      ps_ev_number IN device.device_name%TYPE,
      pc_direction IN CHAR,
      pn_num_bytes IN device_call_in_record.inbound_byte_count%TYPE
   ) AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'SP_ADD_MESSAGE';
   BEGIN
      vn_device_call_in_record_id := fn_get_current_call_in_id (ps_ev_number);

      IF vn_device_call_in_record_id IS NULL THEN
         RAISE ex_device_not_found;
      END IF;

      IF pc_direction = cc_inbound THEN
         UPDATE device_call_in_record
            SET inbound_message_count = (inbound_message_count + 1),
                inbound_byte_count = (inbound_byte_count + pn_num_bytes)
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      ELSIF pc_direction = cc_outbound THEN
         UPDATE device_call_in_record
            SET outbound_message_count = (outbound_message_count + 1),
                outbound_byte_count = (outbound_byte_count + pn_num_bytes),
                last_message_out_ts = SYSDATE
          WHERE device_call_in_record.device_call_in_record_id =
                                                     vn_device_call_in_record_id;
      END IF;
   EXCEPTION
      WHEN ex_device_not_found THEN
         vs_error_msg :=
               'The device_name  = '
            || ps_ev_number
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                  (pkg_app_exec_hist_globals.invalid_machine_id,
                                   vs_error_msg,
                                   NULL,
                                   cs_package_name || cs_procedure_name
                                  );
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;

   FUNCTION fn_get_current_call_in_id (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN device_call_in_record.device_call_in_record_id%TYPE AS
      cs_procedure_name   CONSTANT VARCHAR2 (50) := 'FN_GET_CURRENT_CALL_IN_ID';
   BEGIN
      -- First get the serial number
      vs_serial_number := fn_get_serial_number (ps_ev_number);

      IF vs_serial_number IS NULL THEN
         RETURN NULL;
      END IF;

      SELECT MAX (device_call_in_record.device_call_in_record_id)
        INTO vn_device_call_in_record_id
        FROM device_call_in_record
       WHERE device_call_in_record.serial_number = vs_serial_number
         --AND device_call_in_record.call_in_status = 'I' OR (device_call_in_record.call_in_status <> 'I' AND device_call_in_record.last_updated_ts >= (sysdate-(3/1440)));
         AND (device_call_in_record.call_in_status = 'I' OR device_call_in_record.call_in_finish_ts IS NULL);

      IF vn_device_call_in_record_id IS NULL THEN
         -- we never started a call-in record, so start it now... this isn't ideal,
         -- but should only happen in the case where the network layer doesn't
         -- start the call-in record, which is possble if it's new or legacy
         sp_start (ps_ev_number,
                   NULL,
                   NULL,
                   NULL,
                   NULL
                  );

         SELECT MAX (device_call_in_record.device_call_in_record_id)
           INTO vn_device_call_in_record_id
           FROM device_call_in_record
          WHERE device_call_in_record.serial_number = vs_serial_number
            AND device_call_in_record.call_in_status = 'I';
      END IF;

      RETURN vn_device_call_in_record_id;
   END;

   FUNCTION fn_get_serial_number (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN device.device_serial_cd%TYPE AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)      := 'FN_GET_SERIAL_NUMBER';
      vs_serial_cd                 device.device_serial_cd%TYPE;
   BEGIN
      SELECT device.device_serial_cd
        INTO vs_serial_cd
        FROM device
       WHERE device.device_name = ps_ev_number
         AND device.device_active_yn_flag = 'Y';

      RETURN vs_serial_cd;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RETURN NULL;
   END;

   FUNCTION fn_get_LOCATION_NAME (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN LOCATION.LOCATION_NAME%TYPE AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)      := 'FN_GET_LOCATION_NAME';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_LOCATION_NAME             LOCATION.LOCATION_NAME%TYPE;
   BEGIN
      SELECT LOCATION.LOCATION_NAME
        INTO vs_LOCATION_NAME
        FROM LOCATION.LOCATION,
             device,
             pss.pos
       WHERE LOCATION.location_id = pss.pos.location_id
         AND pss.pos.device_id = device.device_id
         AND device.device_name = ps_ev_number;

      RETURN vs_LOCATION_NAME;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RETURN NULL;
   END;

   FUNCTION fn_get_customer_name (
      ps_ev_number IN device.device_name%TYPE
   )
      RETURN customer.customer_name%TYPE AS
      cs_procedure_name   CONSTANT VARCHAR2 (50)      := 'FN_GET_CUSTOMER_NAME';
      vs_serial_cd                 device.device_serial_cd%TYPE;
      vs_customer_name             customer.customer_name%TYPE;
   BEGIN
      SELECT customer.customer_name
        INTO vs_customer_name
        FROM LOCATION.customer,
             device,
             pss.pos
       WHERE LOCATION.customer.customer_id = pss.pos.customer_id
         AND pss.pos.device_id = device.device_id
         AND device.device_name = ps_ev_number;

      RETURN vs_customer_name;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RETURN NULL;
   END;
END;
/

