ALTER TABLE device.device_call_in_record
ADD (credit_vend_count NUMBER(7,0) DEFAULT 0)
ADD (cash_vend_count NUMBER(7,0) DEFAULT 0)
ADD (passcard_vend_count NUMBER(7,0) DEFAULT 0);
