CREATE OR REPLACE TRIGGER device.trbi_config_template_setting
BEFORE INSERT ON device.config_template_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT 
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO 
		:NEW.created_utc_ts,
		:NEW.created_by,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/