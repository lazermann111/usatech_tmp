CREATE OR REPLACE TRIGGER device.trbi_ext_file
BEFORE INSERT ON device.ext_file
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.ext_file_id IS NULL THEN

      SELECT seq_ext_file_id.nextval
        into :new.ext_file_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/