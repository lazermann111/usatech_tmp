CREATE OR REPLACE 
PACKAGE BODY        device.pkg_device_maint IS
   e_device_id_notfound         EXCEPTION;
   e_location_id_notfound       EXCEPTION;
   e_customer_id_notfound       EXCEPTION;
   e_pos_id_notfound            EXCEPTION;
   cv_package_name              CONSTANT VARCHAR2 (50) := 'PKG_DEVICE_MAINT';

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    MDH       24-NOV-2001    Intial Creation
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
    JKB       11-JUL-2006    does not exist (replaced by SP_ASSIGN_NEW_DEVICE_ID ??)
   *******************************************************************************/



     /*******************************************************************************
    Function Name: SP_CREATE_NEW_POS_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
    EJR       04-DEC-2006    Updated to optionally copy or insert base host and/or
                             other hosts based on input boolean flags.
    PWC       23-MAR-2007    Updated to update gprs_modem table
    PWC       29-MAR-2007    Updated to copy host_settings table values for each host
   *******************************************************************************/

   PROCEDURE sp_create_new_pos_id (
      pn_old_pos_id       IN       pos.pos_id%TYPE,
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_location_id      IN       location.location_id%TYPE,
      pn_customer_id      IN       customer.customer_id%TYPE,
      pb_dup_base_host    IN       BOOLEAN,
      pb_dup_other_hosts  IN       BOOLEAN,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_CREATE_NEW_POS_ID';
      
      n_old_device_id              device.device_id%TYPE;
      n_old_host_id                host.host_id%TYPE;
      n_new_host_id                host.host_id%TYPE;
      n_old_device_type_id         device_type.device_type_id%TYPE;
      v_device_name                device.device_name%TYPE;
      n_def_prim_host_type_id      device_type.def_prim_host_type_id%TYPE;
      n_def_prim_host_equipment_id device_type.def_prim_host_equipment_id%TYPE;
      
      CURSOR c_get_base_host_info (device_id_in device.device_id%TYPE)
      IS
         SELECT host_id
         FROM host h, host_type_host_group_type hthgt, host_group_type hgt
         WHERE h.host_type_id = hthgt.host_type_id
         AND hgt.host_group_type_id = hthgt.host_group_type_id
         AND hgt.host_group_type_cd = 'BASE_HOST'
         AND device_id = device_id_in;

      CURSOR c_get_other_host_info (device_id_in device.device_id%TYPE)
      IS
         SELECT host_id
         FROM host h, host_type_host_group_type hthgt, host_group_type hgt
         WHERE h.host_type_id = hthgt.host_type_id(+)
         AND hthgt.host_group_type_id = hgt.host_group_type_id(+)
         AND (hgt.host_group_type_cd IS NULL OR hgt.host_group_type_cd != 'BASE_HOST')
         AND device_id = device_id_in;

      CURSOR c_get_def_prim_host_info (device_id_in device.device_id%TYPE)
      IS
         SELECT def_prim_host_type_id, def_prim_host_equipment_id
         FROM device d, device_type dt
         WHERE d.device_type_id = dt.device_type_id
         AND device_id = device_id_in;

   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_new_pos_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_pos_id IS NULL THEN
         RAISE e_pos_id_notfound;
      END IF;

      -- this should never happen
      IF pn_customer_id IS NULL OR pn_customer_id < 1 THEN
         RAISE e_customer_id_notfound;
      END IF;

      -- this should never happen
      IF pn_location_id IS NULL OR pn_location_id < 1 THEN
         RAISE e_customer_id_notfound;
      END IF;

      BEGIN
          SELECT device_name
          INTO v_device_name
          FROM device
          WHERE device_id = pn_old_device_id;
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
                RAISE e_device_id_notfound;
      END;

      -- use the current active device
      SELECT NVL(MAX(device_id), pn_old_device_id)
      INTO n_old_device_id
      FROM device
      WHERE device_name = v_device_name
            AND device_active_yn_flag = 'Y';

      -- set the old devices to inactive
      UPDATE device
         SET device_active_yn_flag = 'N'
       WHERE device_name = v_device_name;

      -- create the new device record by copying old device record
      SELECT seq_device_id.NEXTVAL
        INTO pn_new_device_id
        FROM DUAL;

      INSERT INTO device
                  (device_id,
                   device_name,
                   device_serial_cd,
                   device_type_id,
                   device_active_yn_flag,
                   encryption_key,
                   last_activity_ts,
                   device_client_serial_cd,
                   device_desc,
                   device_encr_key_gen_ts
                  )
         SELECT pn_new_device_id,
                device_name,
                device_serial_cd,
                device_type_id,
               'Y',
                encryption_key,
                last_activity_ts,
                device_client_serial_cd,
                device_desc,
                device_encr_key_gen_ts
           FROM device
          WHERE device_id = n_old_device_id;
          
      -- copy or create base host record (assume business rule of only 1 per device)
      IF (pb_dup_base_host = TRUE) THEN
          OPEN c_get_base_host_info (n_old_device_id);
          FETCH c_get_base_host_info INTO n_old_host_id;

          IF (c_get_base_host_info%NOTFOUND) THEN
              -- create the default base host
              INSERT INTO HOST
                          (host_last_start_ts, host_status_cd, host_serial_cd,
                           host_est_complete_minut, host_port_num,
                           host_setting_updated_yn_flag, device_id, host_position_num,
                           host_label_cd, host_active_yn_flag, host_type_id,
                           host_equipment_id)
              (SELECT NULL, 0, d.device_serial_cd,
                      0, 0,
                      'N', d.device_id, 0,
                      NULL, 'Y', dt.def_base_host_type_id,
                      dt.def_base_host_equipment_id
                 FROM device d, device_type dt
                WHERE d.device_type_id = dt.device_type_id
                  AND d.device_id = pn_new_device_id);
          ELSE
          
                SELECT seq_HOST_id.nextval
                INTO n_new_host_id
                FROM dual;
          
              -- copy the base host
              INSERT INTO HOST
                          (host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                           host_est_complete_minut, host_port_num,
                           host_setting_updated_yn_flag, device_id, host_position_num,
                           host_label_cd, host_active_yn_flag, host_type_id,
                           host_equipment_id)
              (SELECT n_new_host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                      host_est_complete_minut, host_port_num,
                      host_setting_updated_yn_flag, pn_new_device_id, host_position_num,
                      host_label_cd, host_active_yn_flag, host_type_id,
                      host_equipment_id
                 FROM host
                WHERE host_id = n_old_host_id);

             -- copy any host_settings
             INSERT INTO host_setting (host_id, host_setting_parameter, host_setting_value)
             (SELECT n_new_host_id, host_setting_parameter, host_setting_value
                FROM host_setting
               WHERE host_id = n_old_host_id);
               
          END IF;
      
          CLOSE c_get_base_host_info;
      END IF;

      -- determine which host records should be copied and which should be created
      IF (pb_dup_other_hosts = TRUE) THEN
          OPEN c_get_other_host_info (n_old_device_id);
          FETCH c_get_other_host_info INTO n_old_host_id;

          IF (c_get_other_host_info%NOTFOUND) THEN
              -- determine if we need to create primary host or not
              OPEN c_get_def_prim_host_info (pn_new_device_id);
              IF (c_get_def_prim_host_info%FOUND) THEN
              
                  -- create the default primary host
                  FETCH c_get_def_prim_host_info INTO n_def_prim_host_type_id, n_def_prim_host_equipment_id;
                  INSERT INTO HOST
                              (host_last_start_ts, host_status_cd, host_serial_cd,
                               host_est_complete_minut, host_port_num,
                               host_setting_updated_yn_flag, device_id, host_position_num,
                               host_label_cd, host_active_yn_flag, host_type_id,
                               host_equipment_id)
                  (SELECT NULL, 0, device_serial_cd,
                          0, 0,
                          'N', device_id, 0,
                          NULL, 'Y', n_def_prim_host_type_id,
                          n_def_prim_host_equipment_id
                     FROM device
                    WHERE device_id = pn_new_device_id);
              END IF;
              CLOSE c_get_def_prim_host_info;
          ELSE
              -- copy all other host info
              LOOP
              
                    SELECT seq_HOST_id.nextval
                    INTO n_new_host_id
                    FROM dual;
                    
                  INSERT INTO HOST
                              (host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                               host_est_complete_minut, host_port_num,
                               host_setting_updated_yn_flag, device_id, host_position_num,
                               host_label_cd, host_active_yn_flag, host_type_id,
                               host_equipment_id)
                  (SELECT n_new_host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                          host_est_complete_minut, host_port_num,
                          host_setting_updated_yn_flag, pn_new_device_id, host_position_num,
                          host_label_cd, host_active_yn_flag, host_type_id,
                          host_equipment_id
                     FROM host
                    WHERE host_id = n_old_host_id);

                    -- copy any host_settings
                    INSERT INTO host_setting (host_id, host_setting_parameter, host_setting_value)
                    (SELECT n_new_host_id, host_setting_parameter, host_setting_value
                       FROM host_setting
                      WHERE host_id = n_old_host_id);
                      
              FETCH c_get_other_host_info INTO n_old_host_id;
              EXIT WHEN c_get_other_host_info%NOTFOUND;
              END LOOP;
          END IF;
              
          CLOSE c_get_other_host_info;
      END IF;

      -- copy and create new POS and POS_Payment_Type_Authority records
      SELECT pss.seq_pos_id.NEXTVAL
        INTO pn_new_pos_id
        FROM DUAL;

      INSERT INTO pos
                  (
                   pos_id,
                   location_id,
                   customer_id,
                   device_id
                  )
         VALUES
         (
                pn_new_pos_id,
                pn_location_id,
                pn_customer_id,
                pn_new_device_id
         );

      INSERT INTO pos_pta
                  (pos_id,
                    payment_subtype_id,
                    pos_pta_encrypt_key,
                    pos_pta_deactivation_ts,
                    created_by,
                    created_ts,
                    last_updated_by,
                    last_updated_ts,
                    payment_subtype_key_id,
                    pos_pta_regex,
                    pos_pta_regex_bref,
                    pos_pta_activation_ts,
                    pos_pta_device_serial_cd,
                    pos_pta_pin_req_yn_flag,
                    pos_pta_encrypt_key2,
                    authority_payment_mask_id,
                    pos_pta_priority,
                    terminal_id,
                    merchant_bank_acct_id,
                    currency_cd,
                    pos_pta_passthru_allow_yn_flag,
                    pos_pta_pref_auth_amt,
                    pos_pta_pref_auth_amt_max
                  )
         SELECT pn_new_pos_id,
                payment_subtype_id,
                pos_pta_encrypt_key,
                pos_pta_deactivation_ts,
                created_by,
                created_ts,
                last_updated_by,
                last_updated_ts,
                payment_subtype_key_id,
                pos_pta_regex,
                pos_pta_regex_bref,
                pos_pta_activation_ts,
                pos_pta_device_serial_cd,
                pos_pta_pin_req_yn_flag,
                pos_pta_encrypt_key2,
                authority_payment_mask_id,
                pos_pta_priority,
                terminal_id,
                merchant_bank_acct_id,
                currency_cd,
                pos_pta_passthru_allow_yn_flag,
                pos_pta_pref_auth_amt,
                pos_pta_pref_auth_amt_max
           FROM pos_pta
          WHERE pos_id = pn_old_pos_id;


      -- change any waiting outgoing file transfers over to this new device
      UPDATE device_file_transfer
         SET device_id = pn_new_device_id
       WHERE device_id = n_old_device_id
         AND device_file_transfer_direct = 'O'
         AND device_file_transfer_status_cd = 0;

      -- copy any existing device settings to the new device
      INSERT INTO device_setting
                  (device_id,
                   device_setting_parameter_cd,
                   device_setting_value
                  )
         SELECT pn_new_device_id,
                device_setting_parameter_cd,
                device_setting_value
           FROM device_setting
          WHERE device_setting.device_id = n_old_device_id;
          
      -- update gps_device row if exists
      UPDATE gprs_device
         SET gprs_device.device_id = pn_new_device_id
       WHERE gprs_device.device_id = n_old_device_id;

   EXCEPTION
      WHEN e_pos_id_notfound THEN
         pv_error_message := 'The pos_id = ' || pn_old_pos_id ||
               ' , could not be found in the pos table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_location_id_notfound THEN
         pv_error_message := 'The location_id = ' || pn_location_id ||
               ' , could not be found in the location table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_customer_id_notfound THEN
         pv_error_message := 'The customer_id = ' || pn_customer_id ||
               ' , could not be found in the customer table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

   END;


     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_DEVICE_ID
    Description: Copies specific device, host, pos, and pos_pta state information
                 linked to a new device_id.  *WARNING*: This method should ONLY be used
                 in the context of a device that is updating host/component information.
                 In any other context, all host information may be lost resulting in 
                 potentially lost transactions (as the device will be unaware that
                 the server no longer knows its host/component state(s)).
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    ???       ??-???-????    converted from SP_ASSIGN_NEW_ROOM_CNTRL_ID ??
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
    JKB       11-JUL-2006    Updated to use SP_CREATE_NEW_POS_ID
    EJR       04-DEC-2006    Updated to use additional params of SP_CREATE_NEW_POS_ID
   *******************************************************************************/

   PROCEDURE sp_assign_new_device_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      n_old_pos_id                 pos.pos_id%TYPE;
      n_location_id                location.location_id%TYPE;
      n_customer_id                customer.customer_id%TYPE;
      n_max_old_pos_date           pos.pos_activation_ts%TYPE;
      n_new_pos_id                 pos.pos_id%TYPE;
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_ASSIGN_NEW_DEVICE_ID';
      b_dup_base_host              BOOLEAN := FALSE;
      b_dup_other_hosts            BOOLEAN := FALSE;
      n_old_device_type_id         device_type.device_type_id%TYPE;
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;


      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- get the max pos activation time stamp for this device
      SELECT MAX(pos.pos_activation_ts)
        INTO n_max_old_pos_date
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      -- get the old pos_id, location_id and customer_id
      SELECT pos.pos_id, pos.location_id, pos.customer_id
        INTO n_old_pos_id, n_location_id, n_customer_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id
         AND pos.pos_activation_ts = n_max_old_pos_date;

      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_pos_id_notfound;
      END IF;
      
      -- determine what types of host data should be retained
      SELECT device_type_id
        INTO n_old_device_type_id
        FROM device
       WHERE device_id = pn_old_device_id;

      IF (n_old_device_type_id = 5) THEN
          -- assume eSuds sends all host info except for base host
          b_dup_base_host   := TRUE;
          b_dup_other_hosts := FALSE;
      ELSIF n_old_device_type_id >= 13 THEN
          -- device types starting with Edge: send all host info including base host
          b_dup_base_host   := FALSE;
          b_dup_other_hosts := FALSE;
      ELSE
          -- all other legacy device types: none send host info (yet)
          b_dup_base_host   := TRUE;
          b_dup_other_hosts := TRUE;
      END IF;

      sp_create_new_pos_id(n_old_pos_id, pn_old_device_id, n_location_id,
                           n_customer_id, b_dup_base_host, b_dup_other_hosts,
                           pn_new_device_id, n_new_pos_id, pn_return_code,
                           pv_error_message);


   EXCEPTION

      WHEN e_pos_id_notfound THEN
         pv_error_message := 'A pos_id could not be found in the pos table for the device_id = ' ||
               pn_old_device_id || '.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

   END;


     /*******************************************************************************
    Function Name: SP_UPDATE_LOC_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/

   PROCEDURE sp_update_loc_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_location_id  IN       location.location_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_LOC_ID';
   BEGIN

      sp_update_loc_id_cust_id(pn_old_device_id, pn_new_location_id, -1,
                               pn_new_device_id, pn_new_pos_id,
                               pn_return_code, pv_error_message);

   END;


     /*******************************************************************************
    Function Name: SP_UPDATE_CUST_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/

   PROCEDURE sp_update_cust_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_customer_id  IN       customer.customer_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_CUST_ID';
   BEGIN

      sp_update_loc_id_cust_id(pn_old_device_id, -1, pn_new_customer_id,
                               pn_new_device_id, pn_new_pos_id,
                               pn_return_code, pv_error_message);

   END;


     /*******************************************************************************
    Function Name: SP_UPDATE_CUST_ID_LOC_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
    EJR       04-DEC-2006    Updated to use additional params of SP_CREATE_NEW_POS_ID
   *******************************************************************************/

   PROCEDURE sp_update_loc_id_cust_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_location_id  IN       location.location_id%TYPE := -1,
      pn_new_customer_id  IN       customer.customer_id%TYPE := -1,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      n_old_device_id              device.device_id%TYPE;
      n_location_id                location.location_id%TYPE;
      n_customer_id                customer.customer_id%TYPE;
      n_old_pos_id                 pos.pos_id%TYPE;
      n_max_old_pos_date           pos.pos_activation_ts%TYPE;
      b_create_new_pos             BOOLEAN;
      b_update_pos                 BOOLEAN;
      n_tran_count                 NUMBER;
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_CUST_ID_LOC_ID';
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_new_pos_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      b_create_new_pos := FALSE;
      b_update_pos := FALSE;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- this should never happen
      IF pn_new_customer_id IS NULL THEN
         RAISE e_customer_id_notfound;
      END IF;

      -- this should never happen
      IF pn_new_location_id IS NULL THEN
         RAISE e_location_id_notfound;
      END IF;


      -- get the max pos activation time stamp for this device
      SELECT MAX(pos.pos_activation_ts)
        INTO n_max_old_pos_date
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      -- get the old pos_id, location_id and customer_id
      SELECT pos.pos_id, pos.location_id, pos.customer_id
        INTO n_old_pos_id, n_location_id, n_customer_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id
         AND pos.pos_activation_ts = n_max_old_pos_date;

      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_pos_id_notfound;
      END IF;


      -- check if updating customer
      IF pn_new_customer_id != -1 THEN

         IF n_customer_id != pn_new_customer_id THEN

            b_update_pos := TRUE;

            IF n_customer_id != 1 THEN
               b_create_new_pos := TRUE;
            END IF;

            -- check to make sure this customer exists
            SELECT customer_id
              INTO n_customer_id
              FROM location.customer
             WHERE customer_id = pn_new_customer_id;

            IF SQL%NOTFOUND = TRUE THEN
               RAISE e_customer_id_notfound;
            END IF;

         END IF;

      END IF;

      -- check if updating location
      IF pn_new_location_id != -1 THEN

         IF n_location_id != pn_new_location_id THEN

            b_update_pos := TRUE;

            IF n_location_id != 1 THEN
               b_create_new_pos := TRUE;
            END IF;

            -- check to make sure this location exists
            SELECT location_id
              INTO n_location_id
              FROM location.location
             WHERE location_id = pn_new_location_id;

            IF SQL%NOTFOUND = TRUE THEN
               RAISE e_location_id_notfound;
            END IF;

         END IF;

      END IF;

      -- check to see if we really need a new pos (i.e. does it have transactions)
      IF b_create_new_pos THEN

         SELECT COUNT(tran.tran_id) tran_count
           INTO n_tran_count
           FROM tran, pos_pta
          WHERE tran.pos_pta_id = pos_pta.pos_pta_id
            AND pos_pta.pos_id = n_old_pos_id;

         IF n_tran_count = 0 THEN

             b_create_new_pos := FALSE;

             -- we are going to make these negative to use the backdoor on the
             -- pos table trigger (bu)
             n_location_id := -(ABS(n_location_id));
             n_customer_id := -(ABS(n_customer_id));

         END IF;

      END IF;


      -- do the correct update

      IF b_create_new_pos THEN
         -- create a new POS (and retain all current host information)
         sp_create_new_pos_id(n_old_pos_id, pn_old_device_id,
                              n_location_id, n_customer_id, TRUE, TRUE,
                              pn_new_device_id, pn_new_pos_id,
                              pn_return_code, pv_error_message);

      ELSE

         pn_new_device_id := pn_old_device_id;
         pn_new_pos_id := n_old_pos_id;

         -- double check that we have to do anything
         -- (don't update unless something has changed)
         IF b_update_pos THEN

            UPDATE pos
               SET pos.location_id = n_location_id,
                   pos.customer_id = n_customer_id
             WHERE pos.pos_id = n_old_pos_id;

         END IF;

      END IF;


   EXCEPTION

      WHEN e_pos_id_notfound THEN
         pv_error_message := 'A pos_id could not be found in the pos table for the device_id = ' ||
               pn_old_device_id || '.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_location_id_notfound THEN
         pv_error_message := 'The location_id = ' || pn_new_location_id ||
               ' , could not be found in the location table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_customer_id_notfound THEN
         pv_error_message := 'The customer_id = ' || pn_new_customer_id ||
               ' , could not be found in the customer table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;


   END;

END;
/
