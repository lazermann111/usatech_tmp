CREATE OR REPLACE TRIGGER device.trbi_event_detail
BEFORE INSERT ON device.event_detail
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.event_detail_id IS NULL
	THEN
		SELECT SEQ_EVENT_DETAIL_ID.NEXTVAL
		INTO :NEW.event_detail_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;

	IF :NEW.event_detail_value LIKE '%CAPK UPDATE: Success' AND :NEW.event_detail_type_id = 3 THEN
		DECLARE
			ln_device_id NUMBER;
			ln_device_file_transfer_id NUMBER;
			ln_dft_status_cd NUMBER;
			lv_global_session_cd DEVICE_FILE_TRANSFER.GLOBAL_SESSION_CD%TYPE;
		BEGIN
			SELECT h.DEVICE_ID, e.GLOBAL_SESSION_CD
			INTO ln_device_id, lv_global_session_cd
			FROM DEVICE.EVENT e JOIN DEVICE.HOST h ON e.HOST_ID = h.HOST_ID
			WHERE e.event_id = :NEW.event_id;

			SELECT MAX(d.DEVICE_FILE_TRANSFER_ID) INTO ln_device_file_transfer_id
			FROM DEVICE.DEVICE_FILE_TRANSFER d JOIN FILE_TRANSFER f ON d.FILE_TRANSFER_ID = f.FILE_TRANSFER_ID
			WHERE f.FILE_TRANSFER_TYPE_CD = 36 AND d.DEVICE_ID = ln_device_id AND d.DEVICE_FILE_TRANSFER_DIRECT = 'O';

			SELECT d.DEVICE_FILE_TRANSFER_STATUS_CD INTO ln_dft_status_cd
			FROM DEVICE.DEVICE_FILE_TRANSFER d
			WHERE d.DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id;

			IF ln_dft_status_cd = 0 THEN
				UPDATE ENGINE.MACHINE_CMD_PENDING
				   SET EXECUTE_CD = 'A'
				 WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id
				   AND EXECUTE_CD IN('S', 'P');

				UPDATE DEVICE.DEVICE_FILE_TRANSFER
				   SET DEVICE_FILE_TRANSFER_STATUS_CD = 1,
				       DEVICE_FILE_TRANSFER_TS = SYSDATE,
				       GLOBAL_SESSION_CD = lv_global_session_cd
				 WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id
					AND DEVICE_FILE_TRANSFER_STATUS_CD = 0;
			END IF;
		END;
	END IF;
END;
/