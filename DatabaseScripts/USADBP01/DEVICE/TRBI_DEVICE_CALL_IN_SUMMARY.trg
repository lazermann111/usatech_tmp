CREATE OR REPLACE TRIGGER device.trbi_device_call_in_summary
BEFORE INSERT ON device.device_call_in_summary
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.device_call_in_summary_id IS NULL THEN
      SELECT seq_device_call_in_summary_id.NEXTVAL
        INTO :NEW.device_call_in_summary_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/