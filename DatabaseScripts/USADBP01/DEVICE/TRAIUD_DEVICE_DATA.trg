CREATE OR REPLACE TRIGGER DEVICE.TRAIUD_DEVICE_DATA
AFTER INSERT OR UPDATE OR DELETE ON DEVICE.DEVICE_DATA
FOR EACH ROW
BEGIN	
	IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RDW_ENABLED') = 'Y' AND (
		DBADMIN.PKG_UTL.EQL(:NEW.DIAG_APP_VERSION, :OLD.DIAG_APP_VERSION) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.PROPERTY_LIST_VERSION, :OLD.PROPERTY_LIST_VERSION) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.PROPERTY_LIST_VERSION_TS, :OLD.PROPERTY_LIST_VERSION_TS) = 'N'
		OR DBADMIN.PKG_UTL.EQL(:NEW.PTEST_VERSION, :OLD.PTEST_VERSION) = 'N'		
		) THEN
		INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1)
            VALUES('DEVICE.TRAIUD_DEVICE_DATA', 'DEVICE_DIM', 'DEVICE_UNIQUE_NAME', :NEW.DEVICE_NAME);
	END IF;
END;
/