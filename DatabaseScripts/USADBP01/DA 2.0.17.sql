declare
	ln_file_transfer_id file_transfer.file_transfer_id%type;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_setting_count NUMBER;
begin
	SELECT file_transfer_id INTO ln_file_transfer_id
	FROM device.file_transfer
	WHERE file_transfer_name = 'DEFAULT-CFG-13-5';

	UPDATE device.file_transfer SET file_transfer_type_cd = 22 WHERE file_transfer_id = ln_file_transfer_id;
	
    pkg_device_configuration.sp_update_cfg_tmpl_settings(ln_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
	
	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(13, 5, 
		'0-7|10|20-27|30-33|50-52|60-64|70|71|80|81|85-87|100-108|200-208|300|301|1001-1023|1101-1106|1200|1300|1400-1415|1500-1521',
		'0-7|10|20-27|30-33|70|71|85-87|1001-1023|1101-1106|1200|1300|1400-1415|1500-1521');
		
	COMMIT;
end;
/
