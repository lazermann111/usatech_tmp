CREATE INDEX app_exec_hist.ix_exception_data_created_ts ON app_exec_hist.exception_data
  (
    created_ts                      ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  logging_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/
