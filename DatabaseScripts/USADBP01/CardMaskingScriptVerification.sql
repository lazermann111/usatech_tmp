-- check if there is unmasked card data in pss.tran records older than 7 days
SELECT TRAN_ID
  FROM PSS.TRAN 
 WHERE (REGEXP_LIKE(TRAN_PARSED_ACCT_NUM, '[0-9]{13,}')
               OR TRAN_PARSED_ACCT_NAME IS NOT NULL
               OR TRAN_PARSED_ACCT_EXP_DATE IS NOT NULL)
  AND CREATED_TS < SYSDATE - 7;
  
-- check if there is unmasked card data in pss.auth records older than 7 days  
SELECT AUTH_ID
  FROM PSS.AUTH
 WHERE REGEXP_LIKE(AUTH_PARSED_ACCT_DATA, '[0-9]{13,}')
   AND CREATED_TS < SYSDATE - 7;

-- check if there is unmasked TRAN_RECEIVED_RAW_ACCT_DATA in pss.tran
SELECT * FROM PSS.TRAN WHERE REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{13,}');
