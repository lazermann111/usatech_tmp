CREATE OR REPLACE TRIGGER pss.trbi_est_tran_complete_time
BEFORE INSERT ON pss.est_tran_complete_time
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

   -- If the user decides to insert values for all the columns it is
   -- assumed the user knows what they want in each column and the
   -- user has performed any necessary calculations.  If the user
   -- only insert a record with a last_tran_complete_minut then this
   -- trigger will insert default values for the other columns.
   IF (    :NEW.avg_tran_complete_minut IS NULL
       AND :NEW.num_cycle_in_avg IS NULL) THEN
       
      :NEW.num_cycle_in_avg := 1;
      :NEW.avg_tran_complete_minut := :NEW.last_tran_complete_minut;
      
   END IF;

END;

/