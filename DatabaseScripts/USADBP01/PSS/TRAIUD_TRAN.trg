CREATE OR REPLACE TRIGGER pss.traiud_tran
AFTER INSERT OR UPDATE OR DELETE ON pss.tran
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
    l_enqueue_options     DBMS_AQ.enqueue_options_t;
    l_message_properties  DBMS_AQ.message_properties_t;
    l_message_handle      RAW(16);
    l_tran_msg            t_tran_msg;
    l_dml_type            CHAR(1);
BEGIN
    IF INSERTING THEN
        l_dml_type := 'I';
    ELSIF UPDATING THEN
        l_dml_type := 'U';
    ELSIF DELETING THEN
        l_dml_type := 'D';
    END IF;
    
    l_tran_msg := t_tran_msg(:NEW.TRAN_ID, l_dml_type);

    DBMS_AQ.enqueue(queue_name          => 'q_tran_msg',
                    enqueue_options     => l_enqueue_options,
                    message_properties  => l_message_properties,
                    payload             => l_tran_msg,
                    msgid               => l_message_handle);
END;
/