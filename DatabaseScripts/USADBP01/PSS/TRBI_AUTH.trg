CREATE OR REPLACE TRIGGER pss.trbi_auth
BEFORE INSERT ON pss.auth
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.auth_id IS NULL
	THEN
		SELECT SEQ_auth_id.NEXTVAL
		INTO :NEW.auth_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
	
	IF :NEW.auth_type_cd = 'L' THEN
		:NEW.auth_state_id := 4;
		:NEW.auth_result_cd := 'F';
	END IF;
	
	IF :NEW.auth_parsed_acct_data IS NOT NULL THEN
		IF :NEW.auth_type_cd = 'L' OR :NEW.auth_type_cd = 'N' AND :NEW.auth_state_id IN (3, 4, 7) THEN
			:NEW.auth_parsed_acct_data := DBADMIN.MASK_CREDIT_CARD(:NEW.auth_parsed_acct_data);
		ELSE
			:NEW.auth_parsed_acct_data := DBADMIN.GET_AUTHED_CARD(:NEW.auth_parsed_acct_data);
		END IF;
	END IF;
END;
/