CREATE OR REPLACE TRIGGER pss.trbu_permission_action_param
BEFORE UPDATE ON pss.permission_action_param
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/