CREATE OR REPLACE TRIGGER pss.trbu_pos_pta_tmpl
BEFORE UPDATE ON pss.pos_pta_tmpl
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/