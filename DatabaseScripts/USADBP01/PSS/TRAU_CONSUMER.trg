CREATE OR REPLACE TRIGGER PSS.TRAU_CONSUMER
AFTER UPDATE ON PSS.CONSUMER
FOR EACH ROW
BEGIN	
	IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RDW_ENABLED') = 'Y' AND (
        DBADMIN.PKG_UTL.EQL(:NEW.CONSUMER_FNAME, :OLD.CONSUMER_FNAME) = 'N'
        OR DBADMIN.PKG_UTL.EQL(:NEW.CONSUMER_MNAME, :OLD.CONSUMER_MNAME) = 'N'
        OR DBADMIN.PKG_UTL.EQL(:NEW.CONSUMER_LNAME, :OLD.CONSUMER_LNAME) = 'N'
        OR DBADMIN.PKG_UTL.EQL(:NEW.CONSUMER_IDENTIFIER, :OLD.CONSUMER_IDENTIFIER) = 'N') THEN
        INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1, DATA_DETAIL_LABEL_2, DATA_DETAIL_VALUE_2)
            SELECT 'PSS.TRAU_CONSUMER', 'CONSUMER_DIM', 'SOURCE_CONSUMER_ACCT_ID', CA.CONSUMER_ACCT_ID, 'CARD_TYPE', PS.CARD_TYPE_LABEL
              FROM PSS.CONSUMER_ACCT CA JOIN PSS.PAYMENT_SUBTYPE PS ON CA.PAYMENT_SUBTYPE_ID=PS.PAYMENT_SUBTYPE_ID
             WHERE CA.CONSUMER_ID IN(:NEW.CONSUMER_ID, :OLD.CONSUMER_ID);
    END IF;
END;
/