-- Start of DDL Script for Table PSS.CONSUMER_UPLOAD_LOG
-- Generated 2/9/2005 12:19:43 PM from PSS@USADBT01

CREATE TABLE pss.consumer_upload_log
    (consumer_upload_log_id         NUMBER NOT NULL,
    location_id                    NUMBER NOT NULL,
    app_user_id                    NUMBER NOT NULL,
    processed_cnt                  NUMBER NOT NULL,
    ignored_cnt                    NUMBER NOT NULL,
    error_cnt                      NUMBER NOT NULL,
    first_error_row                NUMBER,
    error_message                  VARCHAR2(4000),
    upload_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  PRIMARY KEY (consumer_upload_log_id)
  USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  pss_data)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  pss_data
/




-- Constraints for CONSUMER_UPLOAD_LOG




-- End of DDL Script for Table PSS.CONSUMER_UPLOAD_LOG

-- Foreign Key
ALTER TABLE pss.consumer_upload_log
ADD CONSTRAINT fk_cul_location_id FOREIGN KEY (location_id)
REFERENCES location.location (location_id)
/
ALTER TABLE pss.consumer_upload_log
ADD CONSTRAINT fk_cul_app_user_id FOREIGN KEY (app_user_id)
REFERENCES app_user.app_user (app_user_id)
/
-- End of DDL script for Foreign Key(s)
