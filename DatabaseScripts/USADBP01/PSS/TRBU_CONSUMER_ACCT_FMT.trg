CREATE OR REPLACE TRIGGER pss.trbu_consumer_acct_fmt
BEFORE UPDATE ON pss.consumer_acct_fmt
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/