CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_ACCOUNT
BEFORE UPDATE
ON PSS.CONSUMER_ACCT
FOR EACH ROW
BEGIN
	SELECT
		:old.created_by,
		:old.created_ts,
		sysdate,
		user
	INTO
		:new.created_by,
		:new.created_ts,
		:new.last_updated_ts,
		:new.last_updated_by
	FROM dual;
END;
/