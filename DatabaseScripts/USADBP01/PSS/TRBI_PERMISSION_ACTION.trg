CREATE OR REPLACE TRIGGER pss.trbi_permission_action
BEFORE INSERT ON pss.permission_action
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.permission_action_id IS NULL
   THEN
      SELECT SEQ_PERMISSION_ACTION_ID.NEXTVAL
        INTO :NEW.permission_action_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/