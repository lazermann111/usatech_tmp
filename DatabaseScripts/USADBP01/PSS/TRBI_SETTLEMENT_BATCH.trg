CREATE OR REPLACE TRIGGER pss.trbi_settlement_batch
BEFORE INSERT ON pss.settlement_batch
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.settlement_batch_id IS NULL
   THEN
      SELECT seq_settlement_batch_id.NEXTVAL
        INTO :NEW.settlement_batch_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
     
    IF :NEW.settlement_batch_start_ts IS NULL
    THEN
        :NEW.settlement_batch_start_ts := SYSDATE;
    END IF;
END;
/