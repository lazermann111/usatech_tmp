CREATE OR REPLACE FUNCTION PSS.SF_PIVOT_ESUDS_TRAN_LINE_ITEM (PN_TRAN_ID IN TRAN.TRAN_ID%TYPE)
   RETURN VARCHAR2
IS
   v_return_string   VARCHAR2 (4000) DEFAULT NULL;
   v_delimiter       VARCHAR2 (1)    DEFAULT NULL;
BEGIN
   FOR x IN (SELECT tran_line_item_desc
               FROM MV_ESUDS_TRAN_LINE_ITEM
              WHERE tran_id = pn_tran_id
                AND tran_line_item_batch_type_cd = 'A')
   LOOP
      v_return_string := v_return_string || v_delimiter || x.tran_line_item_desc;
      v_delimiter := CHR (13);
   END LOOP;

   RETURN v_return_string;
END; 
/