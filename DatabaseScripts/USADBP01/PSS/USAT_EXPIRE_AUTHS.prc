CREATE OR REPLACE PROCEDURE PSS.USAT_EXPIRE_AUTHS
IS
    ln_min_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_max_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_cnt PLS_INTEGER;
    ld_start_ts DATE := SYSDATE - 7;
    ld_end_ts DATE := SYSDATE - 6;
BEGIN
    SELECT NVL(MIN(CASE WHEN X.CREATED_TS > ld_end_ts THEN X.TRAN_ID END), MAX(CASE WHEN P.ROWCNT > 0 THEN P.HIGH_TRAN_ID END)) MAX_TRAN_ID,
           NVL(MAX(CASE WHEN X.CREATED_TS < ld_start_ts THEN X.TRAN_ID END), 0) MIN_TRAN_ID
      INTO ln_max_tran_id, ln_min_tran_id
      FROM PSS.TRAN X 
      RIGHT OUTER JOIN (SELECT TO_NUMBER_OR_NULL(DBADMIN.GET_LONG('sys.tabpart$', 'hiboundval', TP.ROWID)) HIGH_TRAN_ID, TP.ROWCNT
      FROM SYS.TABPART$ TP
      JOIN SYS.OBJ$ O ON O.OBJ# = TP.OBJ# 
      JOIN SYS.USER$ U ON U.USER# = O.OWNER#
      WHERE O.NAME = 'TRAN'
      AND U.NAME = 'PSS') P ON X.TRAN_ID BETWEEN P.HIGH_TRAN_ID - 5 AND P.HIGH_TRAN_ID;

    UPDATE PSS.TRAN
       SET TRAN_STATE_CD = 'W'
     WHERE TRAN_STATE_CD IN('0', '6')
       AND CREATED_TS BETWEEN ld_start_ts AND ld_end_ts
       AND AUTH_HOLD_USED = 'Y'
       AND TRAN_ID BETWEEN ln_min_tran_id AND ln_max_tran_id;

    ln_cnt := SQL%ROWCOUNT;
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Updated ' || ln_cnt || ' open auths to be reversed from TRAN_ID ' || ln_min_tran_id || ' to ' || ln_max_tran_id || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
END;
/
