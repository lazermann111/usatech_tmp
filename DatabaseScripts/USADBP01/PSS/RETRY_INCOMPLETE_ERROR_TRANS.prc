CREATE OR REPLACE PROCEDURE PSS.RETRY_INCOMPLETE_ERROR_TRANS IS
  ln_max_retries NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RETRY_INCOMPLETE_ERROR_MAX_RETRIES'));
  ln_max_sale_amount NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RETRY_INCOMPLETE_ERROR_MAX_SALE_AMOUNT'));
  ln_retry_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RETRY_INCOMPLETE_ERROR_RETRY_LIMIT'));
  ld_sysdate DATE := SYSDATE;
  lv_from VARCHAR2(128) := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
  lv_to VARCHAR2(128) := 'SoftwareDevelopmentTeam@usatech.com'; -- DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
  lv_dms_url VARCHAR2(512) := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DMS_URL');
  lt_tran_ids NUMBER_TABLE;
  lv_subject VARCHAR(256);
  lv_msg VARCHAR(4000);
BEGIN
  IF ln_max_retries <= 0 THEN
    RETURN;
  END IF;

  select 
    tran_id
  bulk collect into lt_tran_ids
  from (
    select t.tran_id, 
      a.auth_amt,
      min(a.auth_ts) first_sale_ts,
      max(a.auth_ts) last_sale_ts,
      count(distinct a.auth_id) auth_count
    from pss.tran t
    join pss.auth a on a.tran_id = t.tran_id
      and a.auth_type_cd in ('U','S')
      and a.auth_state_id = 4
    where t.tran_state_cd = 'J'
    group by t.tran_id, a.auth_amt)
  where
    ((auth_count = 1 and first_sale_ts > (sysdate - (2/24))) or
    ((auth_count between 1 and ln_max_retries) and last_sale_ts < (sysdate - 1) and first_sale_ts > (sysdate - (ln_max_retries + 1))))
    and auth_amt < ln_max_sale_amount;
    
  IF lt_tran_ids.COUNT > ln_retry_limit THEN
    lv_subject := lt_tran_ids.COUNT || ' TRANSACTION_INCOMPLETE_ERROR RETRIES ' || TO_CHAR(ld_sysdate, 'MM/DD/YYYY HH24:MI:SS') || ') EXCEEDS LIMIT';
    lv_msg := lt_tran_ids.COUNT || ' TRANSACTION_INCOMPLETE_ERROR transactions exceeds the limit of ' || ln_retry_limit || ' per execution. There was likely some mass failure that should be investigated. These transactions must be set to state I if it is not the first attempt or 8 manually.';
    INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_MSG, OB_EMAIL_SUBJECT, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_CONTENT)
    SELECT lv_from, lv_from, ' ', lv_subject, lv_to, lv_to, lv_msg
    FROM DUAL, (
      select count(1) n
      from engine.ob_email_queue
      where created_ts > (sysdate - 1)
      and ob_email_subject like 'TRANSACTION_INCOMPLETE_ERROR RETRIES%EXCEEDS LIMIT') existing_emails
    WHERE existing_emails.n = 0;
  ELSIF lt_tran_ids.COUNT > 0 THEN
    FOR i IN lt_tran_ids.FIRST .. lt_tran_ids.LAST LOOP
      UPDATE pss.tran
      SET tran_state_cd = '8'
      WHERE tran_id = lt_tran_ids(i)
      AND tran_state_cd = 'J';
      lv_msg := lv_msg || lv_dms_url || 'tran.i?tran_id=' || lt_tran_ids(i) || CHR(10);
    END LOOP;

    lv_subject := 'TRANSACTION_INCOMPLETE_ERROR RETRIES (' || lt_tran_ids.COUNT || ') ' || TO_CHAR(ld_sysdate, 'MM/DD/YYYY HH24:MI:SS');
    INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_MSG, OB_EMAIL_SUBJECT, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_CONTENT)
    SELECT lv_from, lv_from, ' ', lv_subject, lv_to, lv_to, lv_msg
    FROM DUAL;
    
  END IF;  
  
END;
/
