CREATE OR REPLACE TRIGGER pss.trbi_refund_type
BEFORE INSERT ON pss.refund_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.refund_type_cd IS NULL
	THEN
		SELECT SEQ_refund_type_id.NEXTVAL
		INTO :NEW.refund_type_cd
		FROM DUAL;
	END IF;
END;
/