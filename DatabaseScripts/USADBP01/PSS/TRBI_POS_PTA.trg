CREATE OR REPLACE TRIGGER pss.trbi_pos_pta
BEFORE INSERT ON pss.pos_pta
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.POS_PTA_id IS NULL THEN

      SELECT seq_POS_PTA_id.nextval
        into :new.POS_PTA_id
        FROM dual;

    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/