CREATE OR REPLACE TRIGGER pss.trbi_tran_refund
BEFORE INSERT ON pss.tran_refund
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.tran_refund_id IS NULL
   THEN
      SELECT seq_tran_refund_id.NEXTVAL
        INTO :NEW.tran_refund_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/