CREATE OR REPLACE TRIGGER pss.trbi_webpos_authority
BEFORE INSERT ON pss.webpos_authority
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF :new.webpos_authority_id IS NULL THEN
      SELECT pss.seq_webpos_authority_id.nextval
        into :new.webpos_authority_id
        FROM dual;
    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/