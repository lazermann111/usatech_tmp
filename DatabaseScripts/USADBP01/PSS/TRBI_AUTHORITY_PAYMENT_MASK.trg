CREATE OR REPLACE TRIGGER pss.trbi_authority_payment_mask
BEFORE INSERT ON pss.authority_payment_mask
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.authority_payment_mask_id IS NULL THEN

      SELECT pss.seq_authority_payment_mask_id.nextval
        into :new.authority_payment_mask_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;

/