CREATE OR REPLACE FUNCTION PSS.GET_AUTH_SERVICE_TYPE_ID (
    l_payment_subtype_key_name PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_KEY_NAME%TYPE,
    l_payment_subtype_key_id POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE)
  RETURN  NUMBER
  PARALLEL_ENABLE
  IS
--
-- Figures out the authority service type id and returns it
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
-- BKRUG        01/20/05 NEW
-- ERYBSKI      11/23/05 Added Aramark, Banorte, WebPOS, Authority.Authority
    l_auth_service_type_id NUMBER;
BEGIN
    IF UPPER(l_payment_subtype_key_name) = 'INTERNAL_PAYMENT_TYPE_ID' THEN
        SELECT IA.AUTHORITY_SERVICE_TYPE_ID
          INTO l_auth_service_type_id
          FROM INTERNAL_PAYMENT_TYPE IPT, INTERNAL_AUTHORITY IA
         WHERE IPT.INTERNAL_AUTHORITY_ID = IA.INTERNAL_AUTHORITY_ID
           AND IPT.INTERNAL_PAYMENT_TYPE_ID = l_payment_subtype_key_id;

    ELSIF UPPER(l_payment_subtype_key_name) = 'BLACKBRD_TENDER_ID' THEN
        SELECT BA.AUTHORITY_SERVICE_TYPE_ID
          INTO l_auth_service_type_id
          FROM BLACKBRD_TENDER BT, BLACKBRD_AUTHORITY BA
         WHERE BT.BLACKBRD_AUTHORITY_ID = BA.BLACKBRD_AUTHORITY_ID
           AND BT.BLACKBRD_TENDER_ID = l_payment_subtype_key_id;

    ELSIF UPPER(l_payment_subtype_key_name) = 'ARAMARK_PAYMENT_TYPE_ID' THEN
        SELECT AA.AUTHORITY_SERVICE_TYPE_ID
          INTO l_auth_service_type_id
          FROM ARAMARK_PAYMENT_TYPE APT, ARAMARK_AUTHORITY AA
         WHERE APT.ARAMARK_AUTHORITY_ID = AA.ARAMARK_AUTHORITY_ID
           AND APT.ARAMARK_PAYMENT_TYPE_ID = l_payment_subtype_key_id;

    ELSIF UPPER(l_payment_subtype_key_name) = 'BANORTE_TERMINAL_ID' THEN
        SELECT BA.AUTHORITY_SERVICE_TYPE_ID
          INTO l_auth_service_type_id
          FROM BANORTE_TERMINAL BT, BANORTE_AUTHORITY BA
         WHERE BT.BANORTE_AUTHORITY_ID = BA.BANORTE_AUTHORITY_ID
           AND BT.BANORTE_TERMINAL_ID = l_payment_subtype_key_id;

    ELSIF UPPER(l_payment_subtype_key_name) = 'WEBPOS_TERMINAL_ID' THEN
        SELECT WA.AUTHORITY_SERVICE_TYPE_ID
          INTO l_auth_service_type_id
          FROM WEBPOS_TERMINAL WT, WEBPOS_AUTHORITY WA
         WHERE WT.WEBPOS_AUTHORITY_ID = WA.WEBPOS_AUTHORITY_ID
           AND WT.WEBPOS_TERMINAL_ID = l_payment_subtype_key_id;

    ELSIF UPPER(l_payment_subtype_key_name) = 'TERMINAL_ID' THEN
        SELECT ASE.AUTHORITY_SERVICE_TYPE_ID
          INTO l_auth_service_type_id
          FROM TERMINAL T, MERCHANT M, AUTHORITY.AUTHORITY A, AUTHORITY.AUTHORITY_SERVICE ASE
         WHERE T.MERCHANT_ID = M.MERCHANT_ID
           AND M.AUTHORITY_ID = A.AUTHORITY_ID
           AND A.AUTHORITY_SERVICE_ID = ASE.AUTHORITY_SERVICE_ID
           AND T.TERMINAL_ID = l_payment_subtype_key_id;

    END IF;
    RETURN l_auth_service_type_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN l_auth_service_type_id;
    WHEN OTHERS THEN
        RAISE;
END;
/