CREATE OR REPLACE PROCEDURE PSS.REFUND_SPROUT_REPLENISH (
	pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
IS
	ln_amount pss.sale.sale_amount%TYPE;
	ln_bonus_amount pss.sale.sale_amount%TYPE;
	lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ld_refundsale_ts DATE;
    ld_refundsale_time NUMBER;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
    ln_host_id HOST.HOST_ID%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_next_master_id NUMBER;
    lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_refundsale_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_refundsale_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    ln_refund_tran_id PSS.TRAN.TRAN_ID%TYPE;
    pn_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
    pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE;
    pv_reversal_reference_id PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE;
    lc_hash_function VARCHAR2(100);
BEGIN
	
   select max('replenish-'||refund_id) into pv_reversal_reference_id from pss.refund where tran_id=(select tran_id from pss.tran where parent_tran_id=pn_tran_id);
   
   IF pv_reversal_reference_id is null THEN
    RETURN;
   END IF;
	select COALESCE (CASE WHEN  tran_line_item_type_id = 550 THEN tran_line_item_amount END,0),
	COALESCE (CASE WHEN  tran_line_item_type_id = 555 THEN tran_line_item_amount END,0)
	INTO ln_amount, ln_bonus_amount
	from pss.tran_line_item where tran_id=pn_tran_id;
	
	SELECT CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
	INTO lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id
	FROM PSS.CONSUMER_ACCT
	WHERE CONSUMER_ACCT_ID=pn_consumer_acct_id;
	
	SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
    INTO ln_minor_currency_factor, lc_currency_symbol, ld_refundsale_time, ld_refundsale_ts
    FROM PSS.CURRENCY C
    LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
    WHERE C.CURRENCY_CD = lv_currency_cd;
    
    -- create sale to refund replenishment
    select 'V1-' || CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_corp_customer_id ELSE 1 END || '-' || lv_currency_cd into lv_device_serial_cd from DUAL;
                    PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(lv_device_serial_cd, lv_device_name, ln_device_next_master_id);
				    SELECT
				    	APP_SETTING_VALUE
				    INTO
				    	lc_hash_function
				    FROM
				    	ENGINE.APP_SETTING
				    WHERE
				   		APP_SETTING_CD='SALES_HASH_FUNCTION';
                    PKG_TRAN.SP_CREATE_SALE('A', lv_device_name, ln_device_next_master_id,  0, 'C', ld_refundsale_time, 
                        DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, (ln_amount+ln_bonus_amount)* ln_minor_currency_factor, 'U', 'A', lc_hash_function, 
                        RAWTOHEX(DBADMIN.GET_UNSALTED_HASH(UTL_RAW.CAST_TO_RAW('Replenish Refund on sprout ' || pn_consumer_acct_id || ' of ' || ln_amount), lc_hash_function)),
                        NULL, ln_result_cd, lv_error_message, ln_refundsale_tran_id, lv_refundsale_tran_state_cd);
                    IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                        RAISE_APPLICATION_ERROR(-20118, 'Could not create replenish refund sprout transaction: ' || lv_error_message);
                    END IF;
                   
			UPDATE PSS.TRAN
			SET (PARENT_TRAN_ID,
			TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            POS_PTA_ID,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            DEVICE_NAME)=
			(SELECT pn_tran_id,
				  '8',
                  pn_consumer_acct_id,
                  pp.POS_PTA_ID, 
                  pp.PAYMENT_SUBTYPE_KEY_ID, 
                  ps.PAYMENT_SUBTYPE_CLASS, 
                  ps.CLIENT_PAYMENT_TYPE_CD,
                  d.DEVICE_NAME 
          	FROM pss.pos_pta pp 
          	JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class='Sprout' and client_payment_type_cd='T'
          	JOIN pss.pos p ON pp.pos_id = p.pos_id
          	JOIN device.device d ON p.device_id = d.device_id
          	where d.device_serial_cd=lv_device_serial_cd
                    ) WHERE TRAN_ID = ln_refundsale_tran_id;
                    
                    SELECT HOST_ID, 'Replenish refund for sprout ' || lc_currency_symbol || TO_CHAR(ln_amount, 'FM9,999,999.00') 
                      INTO ln_host_id, lv_desc
                      FROM (SELECT H.HOST_ID
                              FROM DEVICE.HOST H
                              JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                             WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                               AND H.HOST_PORT_NUM IN(0,1)
                               AND D.DEVICE_NAME = lv_device_name
                               AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                             ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                     WHERE ROWNUM = 1;
                    INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID)
                    SELECT
                        ln_refundsale_tran_id,
                        ln_amount,
                        NULL,
                        NULL,
                        558,
                        1,
                        lv_desc,
                        ln_host_id,
                        'A',
                        ld_refundsale_ts,
                        0,
                        pn_consumer_acct_id
                    FROM DUAL;
  	IF ln_bonus_amount > 0 THEN                   
                    INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID)
                    SELECT
                        ln_refundsale_tran_id,
                        ln_bonus_amount,
                        NULL,
                        NULL,
                        559,
                        1,
                        'Replenish bonus refund for sprout ' || lc_currency_symbol || TO_CHAR(ln_bonus_amount, 'FM9,999,999.00') ,
                        ln_host_id,
                        'A',
                        ld_refundsale_ts,
                        0,
                        pn_consumer_acct_id
                    FROM DUAL;
  END IF;
  
   -- create offline sale auth record
  
   SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_auth_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_AUTHORITY_MISC_DATA)
                 SELECT pn_auth_auth_id,
                        ln_refundsale_tran_id,
                        'L',
                        6,
                        null, --null
                        2, -- pss.acct_entry_method use 2 MANUAL
                        (ln_amount+ln_bonus_amount),	-- amount    
                        ld_refundsale_ts,
                        null, -- null
                        null, -- null, is this ok?
                        pv_reversal_reference_id
                   FROM dual;
   SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        ln_refundsale_tran_id,
                        'O',
                        6,
                        null, --null
                        2, -- pss.acct_entry_method use 2 MANUAL
                        (ln_amount+ln_bonus_amount),	-- amount    
                        ld_refundsale_ts,
                        null, -- null
                        null -- null, is this ok?
                   FROM dual;

END;
/
