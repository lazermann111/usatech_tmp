CREATE OR REPLACE 
PROCEDURE load_tran_details
(
    l_device_id IN device.device_id%TYPE,
    l_device_type_id IN device.device_type_id%TYPE,
    l_tran_id IN tran.tran_id%TYPE,
    l_batch_amount IN tran_settlement_batch.tran_settlement_b_amt%TYPE,
    l_tran_result IN tran.tran_device_result_type_cd%TYPE,
    l_tran_details IN VARCHAR2
)
   IS
    l_host_type_id host_type.host_type_id%TYPE;
    l_host_id host.host_id%TYPE;
    l_host_serial_cd host.host_serial_cd%TYPE;
    l_tli_total tran_settlement_batch.tran_settlement_b_amt%TYPE;
    l_tli_desc tran_line_item_type.tran_line_item_type_desc%TYPE;
    l_adj_ind NUMBER := 0;
    l_adj_tli tran_line_item_type.tran_line_item_type_id%TYPE;
    l_adj_amt tran_settlement_batch.tran_settlement_b_amt%TYPE;
    l_usage_minutes NUMBER;
    l_base_host_count NUMBER;
    l_other_host_count NUMBER;

    l_cnt NUMBER;
    l_pos NUMBER;
    l_total_items NUMBER;
    l_list VARCHAR2(1000);
    l_sp_name CONSTANT VARCHAR2(30) := 'pss.load_tran_details';
    l_err_msg exception_data.additional_information%TYPE := '';

    TYPE payload_type IS TABLE OF VARCHAR2(100)
    INDEX BY BINARY_INTEGER;

    payload_rcd payload_type;
BEGIN

    -- find the appropriate host

	select count(1)
	into l_base_host_count
	from device_type, device, host, host_type, device_type_host_type
	where device.device_id = host.device_id
	and host_type.host_type_id = device_type_host_type.host_type_id
	and device_type.device_type_id = device_type_host_type.device_type_id
	and device.device_type_id = device_type.device_type_id
	and host.host_type_id = host_type.host_type_id
    and device.device_id = l_device_id
    and device_type_host_type.device_type_host_type_cd = 'B';

	select count(1)
	into l_other_host_count
	from device_type, device, host, host_type, device_type_host_type
	where device.device_id = host.device_id
	and host_type.host_type_id = device_type_host_type.host_type_id
	and device_type.device_type_id = device_type_host_type.device_type_id
	and device.device_type_id = device_type.device_type_id
	and host.host_type_id = host_type.host_type_id
    and device.device_id = l_device_id
    and device_type_host_type.device_type_host_type_cd != 'B';

    if (l_base_host_count = 0 AND l_other_host_count = 0) then
        -- no host exists, can not continue
        l_err_msg := 'Failed to find a host for tran id ' || l_tran_id;
        pkg_exception_processor.sp_log_exception
        (
            pkg_app_exec_hist_globals.insert_line_item_failure,
            l_err_msg,
            pkg_app_exec_hist_globals.cv_server_name,
            l_sp_name
        );
        RAISE_APPLICATION_ERROR(-20000, l_err_msg);
    end if;

    if(l_base_host_count = 0 OR l_other_host_count = 1) then

        -- use other
	    select min(host.host_id)
    	into l_host_id
    	from device_type, device, host, host_type, device_type_host_type
	    where device.device_id = host.device_id
    	and host_type.host_type_id = device_type_host_type.host_type_id
	    and device_type.device_type_id = device_type_host_type.device_type_id
    	and device.device_type_id = device_type.device_type_id
	    and host.host_type_id = host_type.host_type_id
        and device.device_id = l_device_id
        and device_type_host_type.device_type_host_type_cd != 'B';

     else

        -- use base
	    select min(host.host_id)
    	into l_host_id
    	from device_type, device, host, host_type, device_type_host_type
	    where device.device_id = host.device_id
    	and host_type.host_type_id = device_type_host_type.host_type_id
	    and device_type.device_type_id = device_type_host_type.device_type_id
    	and device.device_type_id = device_type.device_type_id
	    and host.host_type_id = host_type.host_type_id
        and device.device_id = l_device_id
        and device_type_host_type.device_type_host_type_cd = 'B';

    end if;

    -- clear out any existing line items and reload from this data
    DELETE FROM tran_line_item
    WHERE tran_id = l_tran_id;

    -- load the line items
    BEGIN
        IF NVL(LENGTH(l_tran_details), 0) > 3 THEN

            l_cnt := 0;
            l_list := l_tran_details;

            LOOP
                l_pos := INSTR(l_list, '|');
                l_cnt := l_cnt + 1;
                IF l_pos > 0 THEN
                    payload_rcd(l_cnt) := TRIM(SUBSTR(l_list, 1, l_pos - 1));
                    l_list := SUBSTR(l_list, l_pos + 1);
                ELSE
                    payload_rcd(l_cnt) := TRIM(l_list);
                    EXIT;
                END IF;
            END LOOP;

            IF NVL(LENGTH(payload_rcd(1)), 0) > 0 AND l_cnt > 0 THEN

                IF payload_rcd(1) = 'A0' THEN -- generic format

                    l_pos := 1;
                    LOOP
                        EXIT WHEN l_pos + 4 > l_cnt;

                        IF payload_rcd(l_pos + 1) IS NOT NULL THEN
                            BEGIN
                                SELECT TRIM(SUBSTR(tran_line_item_type_desc || ' ' || payload_rcd(l_pos + 4), 1, 60))
                                INTO l_tli_desc
                                FROM tran_line_item_type
                                WHERE tran_line_item_type_id = TO_NUMBER(payload_rcd(l_pos + 1));

                                INSERT INTO tran_line_item
                                (
                                    tran_id,
                                    host_id,
                                    tran_line_item_type_id,
                                    tran_line_item_amount,
                                    tran_line_item_quantity,
                                    tran_line_item_desc,
                                    tran_line_item_batch_type_cd,
                                    tran_line_item_tax
                                )
                                VALUES
                                (
                                    l_tran_id,
                                    l_host_id,
                                    TO_NUMBER(payload_rcd(l_pos + 1)),
                                    TO_NUMBER(NVL(payload_rcd(l_pos + 2), 0)) / 100,
                                    TO_NUMBER(NVL(payload_rcd(l_pos + 3), 0)),
                                    l_tli_desc,
                                    'A',
                                    0
                                );
                            EXCEPTION
                                WHEN OTHERS THEN
                                    l_err_msg := 'Error storing tran line item type ' || payload_rcd(l_pos + 1) || ' for tran id ' || l_tran_id || ', code: ' || SQLCODE || ', message: ' || SQLERRM;
                                    pkg_exception_processor.sp_log_exception
                                    (
                                        pkg_app_exec_hist_globals.insert_line_item_failure,
                                        l_err_msg,
                                        pkg_app_exec_hist_globals.cv_server_name,
                                        l_sp_name
                                    );
                            END;
                        END IF;

                        l_pos := l_pos + 4;
                    END LOOP;

                END IF;

            END IF;

        ELSE
            SELECT COUNT(1) INTO l_cnt FROM device
            WHERE device_id = l_device_id AND device_serial_cd LIKE '%63636363';

            IF l_cnt > 0 THEN
                l_adj_ind := 1;
                l_adj_tli := 202; -- PictureStation Photo Prints
                l_adj_amt := NVL(l_batch_amount, 0);
            END IF;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            l_err_msg := 'Error storing tran line items for tran id ' || l_tran_id || ', code: ' || SQLCODE || ', message: ' || SQLERRM;
            pkg_exception_processor.sp_log_exception
            (
                pkg_app_exec_hist_globals.insert_line_item_failure,
                l_err_msg,
                pkg_app_exec_hist_globals.cv_server_name,
                l_sp_name
            );
    END;

    SELECT NVL(SUM((NVL(tran_line_item_amount, 0) + NVL(tran_line_item_tax, 0)) * NVL(tran_line_item_quantity, 1)), 0)
    INTO l_tli_total
    FROM tran_line_item
    WHERE tran_id = l_tran_id;

    IF l_tran_result IN ('C', 'U', 'F', 'T') OR NVL(l_batch_amount, 0) = 0 THEN
        IF l_tli_total > 0 THEN
            l_adj_ind := 1;
            l_adj_tli := 312; -- Cancellation Adjustment
            l_adj_amt := -l_tli_total;
        ELSE
            l_adj_ind := 0;
        END IF;
    ELSIF l_adj_ind = 0 THEN
        l_adj_amt := NVL(l_batch_amount, 0) - l_tli_total;
        IF l_adj_amt > 0 THEN
            l_adj_ind := 1;
            l_adj_tli := 314; -- Positive Discrepancy Adjustment
        ELSIF l_adj_amt < 0 THEN
            l_adj_ind := 1;
            l_adj_tli := 313; -- Negative Discrepancy Adjustment
        END IF;
    END IF;

    IF l_adj_ind = 1 THEN
	    SELECT tran_line_item_type_desc
	    INTO l_tli_desc
	    FROM tran_line_item_type
	    WHERE tran_line_item_type_id = l_adj_tli;

	    INSERT INTO tran_line_item
	    (
    		tran_id,
    		host_id,
    		tran_line_item_type_id,
    		tran_line_item_amount,
    		tran_line_item_quantity,
    		tran_line_item_desc,
    		tran_line_item_batch_type_cd,
    		tran_line_item_tax
	    )
	    VALUES
	    (
    		l_tran_id,
    		l_host_id,
    		l_adj_tli,
    		l_adj_amt,
    		1,
    		l_tli_desc,
    		'A',
    		0
	    );
    END IF;

    SELECT NVL(SUM(NVL(tran_line_item_quantity, 0)), 0)
    INTO l_usage_minutes
    FROM tran_line_item
    WHERE tran_id = l_tran_id
        AND tran_line_item_type_id = 302; -- Usage Minutes

    IF l_usage_minutes > 0 THEN
        UPDATE tran
        SET tran_end_ts = tran_start_ts + l_usage_minutes / 1440
        WHERE tran_id = l_tran_id;
    END IF;

    COMMIT;

    IF LENGTH(l_err_msg) > 0 THEN
        RAISE_APPLICATION_ERROR(-20000, l_err_msg);
    END IF;
END;
/