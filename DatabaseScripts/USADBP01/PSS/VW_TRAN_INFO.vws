CREATE OR REPLACE FORCE VIEW PSS.VW_TRAN_INFO (TRAN_ID, PRE_AUTH_TS, PRE_AUTH_AMOUNT, PRE_AUTH_RESULT_CD, PRE_AUTH_TRAN_CD, ACCOUNT_ENTRY_METHOD_CD, SETTLEMENT_AMOUNT, SETTLEMENT_TS, SETTLEMENT_RESULT_CD, SETTLEMENT_TRAN_CD, TRAN_AMOUNT, LINE_ITEM_COUNT) AS 
  SELECT tli.tran_id,
       a.auth_ts AS pre_auth_ts,
       a.auth_amt AS pre_auth_amount,
       a.auth_result_cd AS pre_auth_result_cd,
       a.auth_authority_tran_cd AS pre_auth_tran_cd,
       NVL(sa.acct_entry_method_cd, a.acct_entry_method_cd) AS account_entry_method_cd,
       CASE WHEN tsb.tran_settlement_b_amt IS NOT NULL THEN tsb.tran_settlement_b_amt WHEN T.TRAN_STATE_CD IN('C', 'D', 'E', 'V') THEN SA.AUTH_AMT_APPROVED END settlement_amount,
       CASE WHEN SB.SETTLEMENT_BATCH_END_TS IS NOT NULL THEN SB.SETTLEMENT_BATCH_END_TS WHEN T.TRAN_STATE_CD IN('C', 'D', 'E', 'V') THEN SA.AUTH_AUTHORITY_TS END SETTLEMENT_TS,
       sa.auth_result_cd AS settlement_result_cd,
       sa.auth_authority_tran_cd AS settlement_tran_cd,
       tli.tran_line_tot_amount AS tran_amount,
       tli.line_item_count AS line_item_count
  FROM (
    SELECT tran_id, tran_line_item_batch_type_cd,
            SUM(tran_line_item_amount * tran_line_item_quantity) tran_line_tot_amount,
            SUM(tran_line_item_quantity) line_item_count,
            FIRST_VALUE(tran_line_item_batch_type_cd) 
            OVER (PARTITION BY tran_id ORDER BY tran_line_item_batch_type_cd DESC)
            AS use_batch_type_cd
        FROM tran_line_item
        GROUP BY tran_id, tran_line_item_batch_type_cd) tli
        JOIN PSS.TRAN T ON TLI.TRAN_ID = T.TRAN_ID
  LEFT OUTER JOIN pss.auth a ON tli.tran_id = a.tran_id AND a.auth_type_cd IN('N', 'L')
  LEFT OUTER JOIN pss.auth sa ON tli.tran_id = sa.tran_id AND sa.auth_type_cd NOT IN('N', 'L', 'A') AND SA.AUTH_STATE_ID IN(2,5)
  LEFT OUTER JOIN (PSS.TRAN_SETTLEMENT_BATCH TSB
                      JOIN PSS.SETTLEMENT_BATCH SB ON TSB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID)
                        ON SA.AUTH_ID = TSB.AUTH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 1
 WHERE tli.tran_line_item_batch_type_cd = tli.use_batch_type_cd
/
