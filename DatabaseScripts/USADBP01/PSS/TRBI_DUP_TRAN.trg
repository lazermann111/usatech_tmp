CREATE OR REPLACE TRIGGER pss.trbi_dup_tran
BEFORE INSERT ON pss.dup_tran
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/