CREATE OR REPLACE TRIGGER pss.trbi_terminal_batch
BEFORE INSERT ON pss.terminal_batch
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.terminal_batch_id IS NULL
	THEN
		SELECT SEQ_terminal_batch_id.NEXTVAL
		INTO :NEW.terminal_batch_id
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/