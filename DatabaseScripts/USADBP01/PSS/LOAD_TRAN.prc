CREATE OR REPLACE PROCEDURE PSS.LOAD_TRAN (
    l_device_name device.device_name%TYPE,
    l_tran_start_ts tran.tran_start_ts%TYPE,
    l_tran_received_raw_acct_data tran.tran_received_raw_acct_data%TYPE,
    l_tran_parsed_acct_name tran.tran_parsed_acct_name%TYPE,
    l_tran_parsed_acct_exp_date tran.tran_parsed_acct_exp_date%TYPE,
    l_tran_parsed_acct_num tran.tran_parsed_acct_num%TYPE,
    l_tran_auth_amount tran.tran_auth_amount%TYPE,
    --l_trans_no tran.trans_no%TYPE,
    l_card_type VARCHAR2
)
   IS
   l_pos_pta_id NUMBER(20,0);
BEGIN
    SELECT pp.pos_pta_id
    INTO l_pos_pta_id
    FROM pos p, pos_pta pp, payment_subtype ps, client_payment_type cpt
    WHERE p.pos_id = pp.pos_id
        AND ps.payment_subtype_id = pp.payment_subtype_id
        AND cpt.client_payment_type_cd = ps.client_payment_type_cd
		AND p.device_id = (SELECT device_id FROM device.device WHERE device_name = l_device_name)
		AND cpt.client_payment_type_cd = DECODE(NVL(l_card_type, 'SP'), 'SP', 'S', 'C')
		AND pp.pos_pta_activation_ts <= l_tran_start_ts
		AND (pp.pos_pta_deactivation_ts IS NULL OR l_tran_start_ts < pp.pos_pta_deactivation_ts);

    INSERT INTO tran
    (
        tran_start_ts,
        tran_received_raw_acct_data,
        tran_parsed_acct_name,
        tran_parsed_acct_exp_date,
        tran_parsed_acct_num,
        tran_auth_amount,
        pos_pta_id,
        --trans_no,
        tran_state_cd,
        tran_auth_result_cd,
        tran_auth_type_cd
    )
    VALUES
    (
        l_tran_start_ts,
        l_tran_received_raw_acct_data,
        l_tran_parsed_acct_name,
        l_tran_parsed_acct_exp_date,
        l_tran_parsed_acct_num,
        l_tran_auth_amount,
        NVL(l_pos_pta_id, 0),
        --l_trans_no,
        'N',
        'Y',
        'N'
    );
END;
/