CREATE OR REPLACE TRIGGER pss.trbi_aramark_payment_type
BEFORE INSERT ON pss.aramark_payment_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.aramark_payment_type_id IS NULL THEN

      SELECT pss.seq_aramark_payment_type_id.nextval
        into :new.aramark_payment_type_id
        FROM dual;

    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/