CREATE OR REPLACE PROCEDURE PSS.UPDATE_DFR_AUTH (
    pv_merchant_order_num VARCHAR2,
    pc_action_code CHAR,
    pn_amount PSS.AUTH.AUTH_AMT%TYPE,
    pn_file_cache_id PSS.AUTH.DFR_FILE_CACHE_ID%TYPE,
    pc_card_type_cd PSS.AUTH.CARD_TYPE_CD%TYPE,
    pv_fee_desc PSS.AUTH.FEE_DESC%TYPE,
    pn_fee_amount PSS.AUTH.FEE_AMOUNT%TYPE,
    pd_batch_date PSS.AUTH.BATCH_DATE%TYPE,
    pn_entity_num PSS.AUTH.ENTITY_NUM%TYPE,
    pv_submission_num PSS.AUTH.SUBMISSION_NUM%TYPE,
    pv_record_num PSS.AUTH.RECORD_NUM%TYPE,
    pn_line_number REPORT.FILE_CACHE_ERROR.LINE_NUMBER%TYPE)
IS
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
    lc_match_flag CHAR(1);
BEGIN
    -- find trans and auth or refund
    IF pc_action_code = 'A' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id,
                   lc_match_flag
              FROM (
            SELECT /*+INDEX(X IDX_DEVICE_TRAN_CD )*/ X.TRAN_ID,
                   A.AUTH_ID,
                   CASE WHEN A.SUBMISSION_NUM = pv_submission_num AND A.RECORD_NUM = pv_record_num THEN 'Y' 
                        WHEN A.SUBMISSION_NUM IS NOT NULL AND A.RECORD_NUM IS NOT NULL AND pv_submission_num IS NOT NULL AND pv_record_num IS NOT NULL THEN 'D'
                        ELSE 'N' END MATCH_FLAG
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N'
             WHERE X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1') 
               AND X.TRAN_GLOBAL_TRANS_CD LIKE 'A:' || REPLACE(pv_merchant_order_num, '-', ':') || '%'
             ORDER BY 3 DESC, CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find auth A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || pn_amount FROM DUAL;
                RETURN;
        END;
        IF lc_match_flag != 'Y' THEN
            UPDATE /*+ index(A PK_AUTH_ID) */ PSS.AUTH A
               SET DFR_FILE_CACHE_ID = pn_file_cache_id,
                   CARD_TYPE_CD = pc_card_type_cd,
                   FEE_DESC = pv_fee_desc,
                   FEE_AMOUNT = pn_fee_amount,
                   BATCH_DATE = pd_batch_date,
                   ENTITY_NUM = pn_entity_num,
                   SUBMISSION_NUM = pv_submission_num,
                   RECORD_NUM = pv_record_num
             WHERE AUTH_ID = ln_auth_id;
        END IF;
    ELSIF TRIM(pv_fee_desc) IS NULL OR pn_fee_amount = 0 THEN
        RETURN; -- early exit
    ELSIF pc_action_code = 'D' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id
              FROM (
            SELECT X.TRAN_ID,
                   A.AUTH_ID
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'U'
             WHERE X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1') 
               AND X.TRAN_GLOBAL_TRANS_CD LIKE 'A:' || REPLACE(pv_merchant_order_num, '-', ':') || '%'
               AND A.SUBMISSION_NUM = pv_submission_num 
               AND A.RECORD_NUM = pv_record_num
               AND A.PARTIAL_REVERSAL_FLAG = 'Y'
             ORDER BY CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find sale A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' with partial reversal' FROM DUAL;
                RETURN;
        END;
        UPDATE PSS.AUTH 
           SET FEE_DESC = CASE WHEN FEE_DESC IS NOT NULL THEN FEE_DESC || '; ' END || pv_fee_desc,
               FEE_AMOUNT = NVL(FEE_AMOUNT, 0) + pn_fee_amount
         WHERE AUTH_ID = ln_auth_id
           AND (LENGTH(FEE_DESC) < LENGTH(pv_fee_desc) OR SUBSTR(FEE_DESC, -LENGTH(pv_fee_desc)) != pv_fee_desc);
    ELSE
        --RAISE_APPLICATION_ERROR(-20170, 'Invalid action code "' || pc_action_code ||'" for merchant order ' || pv_merchant_order_num);
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Invalid action code "' || pc_action_code ||'" for auth update of merchant order ' || pv_merchant_order_num FROM DUAL;
        RETURN;
    END IF;
END;
/
