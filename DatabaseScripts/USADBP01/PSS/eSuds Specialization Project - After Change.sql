DECLARE
  l_last_settle_ts DATE;
  l_last_upload_ts DATE;
  l_min_tran_id NUMBER;
  l_max_tran_id NUMBER;
BEGIN
    SELECT MAX(SETTLEMENT_BATCH_TS)
      INTO l_last_settle_ts
      FROM PSS.MV_ESUDS_SETTLED_TRAN
     WHERE CLIENT_PAYMENT_TYPE_CD IN('M');
       
    SELECT /*+ index(MV IX_MV_EST_CPT_CD) */ MAX(TR.TRAN_UPLOAD_TS)
      INTO l_last_upload_ts
      FROM PSS.TRAN TR
      JOIN PSS.MV_ESUDS_SETTLED_TRAN MV ON TR.TRAN_ID = MV.TRAN_ID
     WHERE MV.CLIENT_PAYMENT_TYPE_CD IN('M')
       AND TR.TRAN_UPLOAD_TS > SYSDATE - 2;
    
    -- insert cash where upload_ts between l_last_upload_ts and  l_last_settle_ts
       INSERT INTO PSS.MV_ESUDS_SETTLED_TRAN (
               SETTLEMENT_BATCH_ID,
               SCHOOL_ID,
               SCHOOL_NAME,
               CAMPUS_ID,
               CAMPUS_NAME,
               DORM_ID,
               DORM_NAME,
               ROOM_ID,
               ROOM_NAME,
               PAYMENT_SUBTYPE_KEY_NAME,
               PAYMENT_SUBTYPE_KEY_ID,
               TRAN_ID,
               TRAN_START_TS,
               TRAN_STATE_CD,
               CONSUMER_ACCT_ID,
               POS_PTA_ID,
               TRAN_SETTLEMENT_AMOUNT,
               SETTLEMENT_BATCH_TS,
               OPERATOR_ID,
               CLIENT_PAYMENT_TYPE_CD,
               TRAN_MASKED_ACCT_NUM,
               DEVICE_ID,
               PAYMENT_SUBTYPE_ID,
               PAYMENT_SUBTYPE_NAME)
           SELECT
                NULL,
                LOC1.LOCATION_ID SCHOOL_ID,
                LOC1.LOCATION_NAME SCHOOL_NAME,
                LOC2.LOCATION_ID CAMPUS_ID,
                LOC2.LOCATION_NAME CAMPUS_NAME,
                LOC3.LOCATION_ID DORM_ID,
                LOC3.LOCATION_NAME DORM_NAME,
                LOC4.LOCATION_ID ROOM_ID,
                LOC4.LOCATION_NAME ROOM_NAME,
                PST.PAYMENT_SUBTYPE_KEY_NAME,
                PTA.PAYMENT_SUBTYPE_KEY_ID,
                TR.TRAN_ID,
                TR.TRAN_START_TS,
                TR.TRAN_STATE_CD,
                TR.CONSUMER_ACCT_ID,
                TR.POS_PTA_ID,
                SUM(TLI.TRAN_LINE_ITEM_QUANTITY * TLI.TRAN_LINE_ITEM_AMOUNT) TRAN_SETTLEMENT_AMOUNT,
                TR.TRAN_UPLOAD_TS SETTLEMENT_BATCH_TS,
                PS.CUSTOMER_ID,
                PST.CLIENT_PAYMENT_TYPE_CD,
                PSS.MASK_CARD(TR.TRAN_PARSED_ACCT_NUM),
                PS.DEVICE_ID,
                PTA.PAYMENT_SUBTYPE_ID,
                PST.PAYMENT_SUBTYPE_NAME
               FROM PSS.TRAN TR,
                    PSS.POS PS,
                    PSS.POS_PTA PTA,
                    LOCATION.LOCATION LOC1,
                    LOCATION.LOCATION LOC2,
                    LOCATION.LOCATION LOC3,
                    LOCATION.LOCATION LOC4,
                    PSS.PAYMENT_SUBTYPE PST,
                    PSS.TRAN_LINE_ITEM TLI
              WHERE TR.POS_PTA_ID = PTA.POS_PTA_ID
                AND PTA.POS_ID = PS.POS_ID
                AND PS.LOCATION_ID = LOC4.LOCATION_ID
                AND LOC4.LOCATION_TYPE_ID = 18
                AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('M')
                AND TR.TRAN_ID = TLI.TRAN_ID
                AND TR.TRAN_UPLOAD_TS > l_last_upload_ts
                AND TR.TRAN_UPLOAD_TS <= l_last_settle_ts
              GROUP BY LOC1.LOCATION_ID,
                LOC1.LOCATION_NAME,
                LOC2.LOCATION_ID,
                LOC2.LOCATION_NAME,
                LOC3.LOCATION_ID,
                LOC3.LOCATION_NAME,
                LOC4.LOCATION_ID,
                LOC4.LOCATION_NAME,
                PST.PAYMENT_SUBTYPE_KEY_NAME,
                PTA.PAYMENT_SUBTYPE_KEY_ID,
                TR.TRAN_ID,
                TR.TRAN_START_TS,
                TR.TRAN_STATE_CD,
                TR.CONSUMER_ACCT_ID,
                TR.POS_PTA_ID,
                TR.TRAN_UPLOAD_TS,
                PS.CUSTOMER_ID,
                PST.CLIENT_PAYMENT_TYPE_CD,
                TR.TRAN_PARSED_ACCT_NUM,
                PS.DEVICE_ID,
                PTA.PAYMENT_SUBTYPE_ID,
                PST.PAYMENT_SUBTYPE_NAME;
       COMMIT;
       
       -- insert all refunds
       SELECT MAX(SETTLEMENT_BATCH_TS)
         INTO l_last_settle_ts
         FROM PSS.MV_ESUDS_SETTLED_TRAN
        WHERE CLIENT_PAYMENT_TYPE_CD IN('S', 'P');
    
       INSERT INTO PSS.MV_ESUDS_SETTLED_REFUND (
               REFUND_ID, 
               SETTLEMENT_BATCH_ID,
               REFUND_SETTLEMENT_B_AMT, 
               REFUND_DESC, 
               SETTLEMENT_BATCH_TS, 
               TRAN_ID)
           SELECT
                R.REFUND_ID,
                SB.SETTLEMENT_BATCH_ID SETTLEMENT_BATCH_ID,
                TS.REFUND_SETTLEMENT_B_AMT TRAN_SETTLEMENT_AMOUNT,
                R.REFUND_DESC,
                SB.SETTLEMENT_BATCH_END_TS SETTLEMENT_BATCH_TS,
                R.TRAN_ID
               FROM PSS.TRAN TR,
                    PSS.POS PS,
                    PSS.POS_PTA PTA,
                    LOCATION.LOCATION LOC1,
                    LOCATION.LOCATION LOC2,
                    LOCATION.LOCATION LOC3,
                    LOCATION.LOCATION LOC4,
                    PSS.PAYMENT_SUBTYPE PST,
                    PSS.REFUND R,
                    PSS.REFUND_SETTLEMENT_BATCH TS,
                    PSS.SETTLEMENT_BATCH SB
              WHERE TR.POS_PTA_ID = PTA.POS_PTA_ID
                AND PTA.POS_ID = PS.POS_ID
                AND PS.LOCATION_ID = LOC4.LOCATION_ID
                AND LOC4.LOCATION_TYPE_ID = 18
                AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                AND TR.TRAN_ID = R.TRAN_ID
                AND R.REFUND_ID = TS.REFUND_ID
                AND R.REFUND_STATE_ID = 1
                AND TS.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID
                AND SB.SETTLEMENT_BATCH_END_TS <= l_last_settle_ts
                AND SB.SETTLEMENT_BATCH_STATE_ID = 1;
       COMMIT;
       
       SELECT MIN(TRAN_ID), MAX(TRAN_ID)
         INTO l_min_tran_id, l_max_tran_id
         FROM PSS.MV_ESUDS_SETTLED_TRAN;
       WHILE l_min_tran_id <= l_max_tran_id LOOP
       -- insert all line item entries (in batches) for all records in MV_ESUDS_SETTLED_TRAN  
       INSERT INTO PSS.MV_ESUDS_TRAN_LINE_ITEM (
               TRAN_LINE_ITEM_ID, 
               TRAN_LINE_ITEM_AMOUNT, 
               TRAN_LINE_ITEM_TAX, 
               TRAN_LINE_ITEM_TS, 
               TRAN_LINE_ITEM_POSITION_CD, 
               TRAN_LINE_ITEM_DESC, 
               TRAN_LINE_ITEM_TYPE_ID, 
               TRAN_ID, 
               TRAN_LINE_ITEM_QUANTITY, 
               HOST_ID, 
               TRAN_LINE_ITEM_BATCH_TYPE_CD)
           SELECT DISTINCT
               tli.TRAN_LINE_ITEM_ID, 
               tli.TRAN_LINE_ITEM_AMOUNT, 
               tli.TRAN_LINE_ITEM_TAX, 
               tli.TRAN_LINE_ITEM_TS, 
               tli.TRAN_LINE_ITEM_POSITION_CD, 
               tli.TRAN_LINE_ITEM_DESC, 
               tli.TRAN_LINE_ITEM_TYPE_ID, 
               tli.TRAN_ID, 
               tli.TRAN_LINE_ITEM_QUANTITY, 
               tli.HOST_ID, 
               tli.TRAN_LINE_ITEM_BATCH_TYPE_CD
               FROM PSS.TRAN_LINE_ITEM TLI,
                    PSS.MV_ESUDS_SETTLED_TRAN MV
              WHERE TLI.TRAN_ID = MV.TRAN_ID
                AND MV.TRAN_ID >= l_min_tran_id 
                AND MV.TRAN_ID < l_min_tran_id + 10000;
           COMMIT;
           l_min_tran_id := l_min_tran_id + 10000;
      END LOOP;
END;
/

