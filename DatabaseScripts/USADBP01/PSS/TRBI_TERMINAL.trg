CREATE OR REPLACE TRIGGER pss.trbi_terminal
BEFORE INSERT ON pss.terminal
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.terminal_id IS NULL
	THEN
		SELECT SEQ_terminal_id.NEXTVAL
		INTO :NEW.terminal_id
		FROM DUAL;
	END IF;
	
	IF :NEW.terminal_group_id IS NULL
	THEN
		SELECT seq_terminal_group_id.NEXTVAL
		INTO :NEW.terminal_group_id
		FROM DUAL;
	END IF;

	SELECT
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/