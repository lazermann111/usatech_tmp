CREATE OR REPLACE TRIGGER pss.trbi_pos
BEFORE INSERT ON pss.pos
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
    IF :new.pos_id IS NULL THEN
      SELECT pss.seq_pos_id.nextval
        into :new.pos_id
        FROM dual;
    END IF;
 SELECT    sysdate,
           user,
           sysdate,
           user,
           nvl(:new.pos_active_yn_flag, 'Y'),
           nvl(:new.pos_activation_ts, sysdate)
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.pos_active_yn_flag,
           :new.pos_activation_ts
      FROM dual;
End;
/