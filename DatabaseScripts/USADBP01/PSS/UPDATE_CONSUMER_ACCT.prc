CREATE OR REPLACE PROCEDURE PSS.UPDATE_CONSUMER_ACCT(
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_balance_change NUMBER,
    pc_active_flag PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVE_YN_FLAG%TYPE,
    pn_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE,
    pn_location_id PSS.CONSUMER_ACCT.LOCATION_ID%TYPE,
    pn_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE,
    pc_allow_negative_balance PSS.CONSUMER_ACCT.ALLOW_NEGATIVE_BALANCE%TYPE,
    pv_user_name VARCHAR,
    pc_close_account_flag PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVE_YN_FLAG%TYPE,
    pn_effective_balance_change OUT NUMBER,
    pn_refund_amt OUT NUMBER,
    pn_eft_amt OUT NUMBER)
IS
    ln_old_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_old_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_old_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    ln_new_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;      
    ln_replenish_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_REPLEN_BALANCE%TYPE;      
    ln_promo_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_PROMO_BALANCE%TYPE; 
    ln_replenish_change PSS.CONSUMER_ACCT.CONSUMER_ACCT_REPLEN_BALANCE%TYPE;
    ln_promo_change PSS.CONSUMER_ACCT.CONSUMER_ACCT_PROMO_BALANCE%TYPE; 
    ld_refund_ts DATE;
    ln_auth_hold_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    ln_next_master_id NUMBER;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE;
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    lv_refund_desc VARCHAR2(4000);
    lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_REPLEN_BALANCE, 
           CONSUMER_ACCT_PROMO_BALANCE, CONSUMER_ACCT_SUB_TYPE_ID, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER
      INTO ln_old_type_id, ln_old_balance, ln_promo_balance, ln_replenish_balance, 
           ln_old_sub_type_id, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier
      FROM PSS.CONSUMER_ACCT
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
       FOR UPDATE;
    IF ln_old_type_id = 1 THEN -- don't allow balance change
        ln_new_balance := ln_old_balance;
        pn_effective_balance_change := 0;
    ELSE
        -- get Auth Holds
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO ln_auth_hold_amt
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
            JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
           WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
             AND CAAH.EXPIRATION_TS > SYSDATE
             AND CAAH.CLEARED_YN_FLAG = 'N';
        pn_effective_balance_change := pn_balance_change;
        IF pc_close_account_flag = 'Y' THEN
            IF ln_auth_hold_amt > 0 THEN
                RAISE_APPLICATION_ERROR(-20202, 'Account still has auth holds. Cannot close account until cleared.');
            END IF;
            pn_effective_balance_change := -ln_old_balance;
        END IF;
        ln_new_balance := ln_old_balance + pn_effective_balance_change;
        IF ln_new_balance - ln_auth_hold_amt < 0 AND (ln_old_type_id != 3 OR ln_old_sub_type_id != 2) THEN
              ln_new_balance := ln_auth_hold_amt;
              pn_effective_balance_change := ln_new_balance - ln_old_balance;
        ELSIF ln_old_type_id = 3 AND ln_new_balance > 2000 THEN
             ln_new_balance := 2000;
             pn_effective_balance_change := ln_new_balance - ln_old_balance;
        END IF;   
        IF ln_old_type_id = 3 AND pn_effective_balance_change < 0 THEN
            IF pc_close_account_flag = 'Y' THEN
                ln_replenish_change := -ln_replenish_balance;
                ln_promo_change := -ln_promo_balance;
                -- Handle when tran is no longer in table (b/c of retention policy)
                BEGIN
                    SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED, POS_PTA_ID, DEVICE_SERIAL_CD
                      INTO ln_orig_tran_id, ln_pos_pta_id, lv_replenish_card_masked, ln_pos_pta_id, lv_device_serial_cd
                      FROM (SELECT CAR.LAST_REPLENISH_TRAN_ID, CAR.REPLENISH_POS_PTA_ID, CAR.REPLENISH_CARD_MASKED, PP.POS_PTA_ID, D.DEVICE_SERIAL_CD
                              FROM PSS.CONSUMER_ACCT_REPLENISH CAR
                              JOIN PSS.AUTH A ON CAR.LAST_REPLENISH_TRAN_ID  = A.TRAN_ID
                              JOIN PSS.TRAN X ON CAR.LAST_REPLENISH_TRAN_ID = X.TRAN_ID
                              JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
                              JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                              JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
                             WHERE CAR.CONSUMER_ACCT_ID = pn_consumer_acct_id
                               AND CAR.LAST_REPLENISH_TRAN_TS > SYSDATE - 90
                               AND A.CARD_KEY IS NOT NULL
                             ORDER BY CAR.LAST_REPLENISH_TRAN_TS DESC, CAR.PRIORITY, CAR.LAST_REPLENISH_TRAN_ID DESC)
                     WHERE ROWNUM = 1;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR(-20201, 'Last replenished more than 90 days ago. Cannot refund replenishment');
                END;
                SELECT PSS.SEQ_TRAN_ID.NEXTVAL, SYSDATE
                  INTO ln_tran_id, ld_refund_ts
                  FROM DUAL;                 
                PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(lv_device_serial_cd, lv_device_name, ln_next_master_id);
                INSERT INTO PSS.TRAN (
                            TRAN_ID,
                            PARENT_TRAN_ID,
                            TRAN_START_TS,
                            TRAN_END_TS,
                            TRAN_UPLOAD_TS,
                            TRAN_GLOBAL_TRANS_CD,
                            TRAN_STATE_CD,
                            TRAN_DEVICE_TRAN_CD,
                            POS_PTA_ID,
                            TRAN_DEVICE_RESULT_TYPE_CD,
                            TRAN_RECEIVED_RAW_ACCT_DATA,
                            PAYMENT_SUBTYPE_KEY_ID,
                            PAYMENT_SUBTYPE_CLASS,
                            CLIENT_PAYMENT_TYPE_CD,
                            DEVICE_NAME
                            )
                     SELECT ln_tran_id,
                            ln_orig_tran_id,
                            ld_refund_ts,
                            ld_refund_ts,
                            NULL,
                            'RF:' || lv_device_name || ':' || ln_next_master_id || ':R1',
                            PKG_CONST.TRAN_STATE__BATCH,
                            ln_next_master_id,
                            ln_pos_pta_id,
                            PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                            lv_replenish_card_masked,
                            pp.PAYMENT_SUBTYPE_KEY_ID,
                            ps.PAYMENT_SUBTYPE_CLASS,
                            ps.CLIENT_PAYMENT_TYPE_CD,
                            lv_device_name
                    FROM pss.pos_pta pp
                    JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
                    WHERE pp.pos_pta_id = ln_pos_pta_id;
                lv_refund_desc := 'Prepaid account closure';
                INSERT INTO PSS.TRAN_LINE_ITEM (
                    TRAN_ID,
                    TRAN_LINE_ITEM_AMOUNT,
                    TRAN_LINE_ITEM_POSITION_CD,
                    TRAN_LINE_ITEM_TAX,
                    TRAN_LINE_ITEM_TYPE_ID,
                    TRAN_LINE_ITEM_QUANTITY,
                    TRAN_LINE_ITEM_DESC,
                    TRAN_LINE_ITEM_BATCH_TYPE_CD,
                    SALE_RESULT_ID,
                    APPLY_TO_CONSUMER_ACCT_ID
                ) VALUES (
                    ln_tran_id,
                    ln_replenish_change,
                    '0',
                    0,
                    500,
                    1,
                    lv_refund_desc,
                    PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
                    PKG_CONST.SALE_RES__SUCCESS,
                    pn_consumer_acct_id
                );
                    
                INSERT INTO PSS.REFUND (
                    TRAN_ID,
                    REFUND_AMT,
                    REFUND_DESC,
                    REFUND_ISSUE_TS,
                    REFUND_ISSUE_BY,
                    REFUND_TYPE_CD,
                    REFUND_STATE_ID,
                    ACCT_ENTRY_METHOD_CD
                ) VALUES (
                    ln_tran_id,
                    ln_replenish_change,
                    lv_refund_desc,
                    ld_refund_ts,
                    pv_user_name,
                    'G',
                    6,
                    2);
                    
                pn_refund_amt := -ln_replenish_change;
                /* I don't think I need this because a refund will go into the EFT already
                IF ln_old_sub_type_id = 2 AND ln_corp_customer_id IS NOT NULL AND ln_corp_customer_id != 0 THEN
                    DECLARE
                        ln_doc_id CORP.DOC.DOC_ID%TYPE;
                        ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
                    BEGIN
                        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, pv_user_name, lv_currency_cd,
                            lv_refund_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                            ln_replenish_change, ln_doc_id, ln_ledger_id);
                        pn_eft_amt := ln_replenish_change;
                    END;
                END IF;  */
            ELSIF -pn_effective_balance_change > ln_promo_balance - ln_auth_hold_amt THEN
                RAISE_APPLICATION_ERROR(-20203, 'Cannot reduce account balance below replenish balance of ' || (ln_replenish_balance + ln_auth_hold_amt) ||'. Use Refund Editor to refund replenisnments instead');
            ELSE
                ln_replenish_change := 0;
                ln_promo_change := pn_effective_balance_change;
            END IF;
        ELSE
            ln_replenish_change := 0;
            ln_promo_change := pn_effective_balance_change;
        END IF;
        
        IF ln_promo_change != 0 AND ln_old_type_id = 3 AND ln_old_sub_type_id = 1 AND ln_corp_customer_id IS NOT NULL AND ln_corp_customer_id != 0 THEN
            DECLARE
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
                lv_adj_desc VARCHAR(4000);
            BEGIN
                IF pc_close_account_flag = 'Y' THEN
                    lv_adj_desc := 'Promo credit for prepaid account closure';
                ELSIF ln_promo_change > 0 THEN
                    lv_adj_desc := 'Promo debit for prepaid account';
                ELSE
                    lv_adj_desc := 'Promo credit for prepaid account';
                END IF;
                CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, pv_user_name, lv_currency_cd,
                    lv_adj_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,                     
                    -ln_promo_change, ln_doc_id, ln_ledger_id);
                pn_eft_amt := -ln_promo_change;
            END;
        END IF;
    END IF;
                
    UPDATE PSS.CONSUMER_ACCT 
       SET CONSUMER_ACCT_BALANCE = ln_new_balance,
           CONSUMER_ACCT_ACTIVE_YN_FLAG = DECODE(pc_close_account_flag, 'Y', 'N', COALESCE(pc_active_flag, CONSUMER_ACCT_ACTIVE_YN_FLAG)),
           CORP_CUSTOMER_ID = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN COALESCE(pn_corp_customer_id, CORP_CUSTOMER_ID) ELSE CORP_CUSTOMER_ID END,
           LOCATION_ID = COALESCE(pn_location_id, LOCATION_ID),
           CONSUMER_ACCT_SUB_TYPE_ID = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN COALESCE(pn_consumer_acct_sub_type_id, CONSUMER_ACCT_SUB_TYPE_ID) ELSE CONSUMER_ACCT_SUB_TYPE_ID END,
           CONSUMER_ACCT_PROMO_BALANCE = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN CONSUMER_ACCT_PROMO_BALANCE + ln_promo_change ELSE CONSUMER_ACCT_PROMO_BALANCE END,
           CONSUMER_ACCT_PROMO_TOTAL = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN CONSUMER_ACCT_PROMO_TOTAL + ln_promo_change ELSE CONSUMER_ACCT_PROMO_TOTAL END,
           CONSUMER_ACCT_REPLEN_BALANCE = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN CONSUMER_ACCT_REPLEN_BALANCE + ln_replenish_change ELSE CONSUMER_ACCT_REPLEN_BALANCE END,
           CONSUMER_ACCT_REPLENISH_TOTAL = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN CONSUMER_ACCT_REPLENISH_TOTAL + ln_replenish_change ELSE CONSUMER_ACCT_REPLENISH_TOTAL END,
           ALLOW_NEGATIVE_BALANCE = CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 AND CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN COALESCE(pc_allow_negative_balance, ALLOW_NEGATIVE_BALANCE) ELSE ALLOW_NEGATIVE_BALANCE END
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
END;
/
