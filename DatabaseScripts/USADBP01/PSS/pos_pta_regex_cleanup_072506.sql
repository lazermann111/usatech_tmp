-- NOTE: update pos_pta.regex == null where pos_pta.regex pos_pta.apm.regex
select *
from pos_pta, authority_payment_mask
where pos_pta.authority_payment_mask_id = authority_payment_mask.authority_payment_mask_id
and pos_pta.pos_pta_regex = authority_payment_mask.authority_payment_mask_regex
and pos_pta.pos_pta_regex_bref = authority_payment_mask.authority_payment_mask_bref

--select count(1) from pos_pta
update pos_pta set pos_pta.pos_pta_regex = null, pos_pta.pos_pta_regex_bref = null
where exists
(
    select 1
    from authority_payment_mask
    where pos_pta.authority_payment_mask_id = authority_payment_mask.authority_payment_mask_id
    and pos_pta.pos_pta_regex = authority_payment_mask.authority_payment_mask_regex
    and pos_pta.pos_pta_regex_bref = authority_payment_mask.authority_payment_mask_bref
)

-- NOTE: update pos_pta.apm_id == null where pos_pta.apm_id == payment_subtype.amp_id
select *
from pos_pta, payment_subtype
where pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
and pos_pta.authority_payment_mask_id = payment_subtype.authority_payment_mask_id

--select count(1) from pos_pta
update pos_pta set pos_pta.authority_payment_mask_id = null
where exists
(
    select 1
    from payment_subtype
    where pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
    and pos_pta.authority_payment_mask_id = payment_subtype.authority_payment_mask_id
)

-- NOTE: update pos_pta.regex == null where pos_pta.apm_id == null and pos_pta.regex == payment_subtype.amp.regex
select *
from pos_pta, payment_subtype, authority_payment_mask
where pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
and payment_subtype.authority_payment_mask_id = authority_payment_mask.authority_payment_mask_id
and pos_pta.pos_pta_regex = authority_payment_mask.authority_payment_mask_regex
and pos_pta.pos_pta_regex_bref = authority_payment_mask.authority_payment_mask_bref

--select count(1) from pos_pta
update pos_pta set pos_pta.pos_pta_regex = null, pos_pta.pos_pta_regex_bref = null
where exists
(
    select 1
    from payment_subtype, authority_payment_mask
    where pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
    and payment_subtype.authority_payment_mask_id = authority_payment_mask.authority_payment_mask_id
    and pos_pta.pos_pta_regex = authority_payment_mask.authority_payment_mask_regex
    and pos_pta.pos_pta_regex_bref = authority_payment_mask.authority_payment_mask_bref
)


