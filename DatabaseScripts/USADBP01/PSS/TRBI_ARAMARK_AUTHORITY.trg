CREATE OR REPLACE TRIGGER pss.trbi_aramark_authority
BEFORE INSERT ON pss.aramark_authority
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.aramark_authority_id IS NULL THEN

      SELECT pss.seq_aramark_authority_id.nextval
        into :new.aramark_authority_id
        FROM dual;

    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/