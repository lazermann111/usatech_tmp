CREATE OR REPLACE PROCEDURE PSS.FIX_LEGACY_KIOSK_TRAN
(
    pv_machine_trans_no     IN transaction_record_hist.machine_trans_no%TYPE,
    pv_kiosk_tran_details   IN VARCHAR2
)
IS
    vn_device_id            device.device_id%TYPE;
    vn_device_type_id       device.device_type_id%TYPE;
    vn_pos_pta_id           pos_pta.pos_pta_id%TYPE;
    vn_tran_id              tran.tran_id%TYPE;
    vn_auth_id              auth.auth_id%TYPE;
    vn_sale_id              auth.auth_id%TYPE;
    vn_settlement_batch_id  settlement_batch.settlement_batch_id%TYPE;
    vn_batch_amount         tran_settlement_batch.tran_settlement_b_amt%TYPE;

BEGIN

    select
        device.device_id,
        device.device_type_id
    into
        vn_device_id,
        vn_device_type_id
    from device
    where device.device_name =
    (
        select machine_id
        from transaction_record_hist
        where machine_trans_no = pv_machine_trans_no
    );

    SELECT  max(pp.pos_pta_id)
    INTO    vn_pos_pta_id
    FROM    pos_pta pp, pos p
    WHERE   p.pos_id = pp.pos_id
    AND     p.device_id = vn_device_id
    AND     pp.pos_pta_regex = '^$'
    and     pp.payment_subtype_id = 1;

    select  PSS.SEQ_TRAN_ID.NEXTVAL
    into    vn_tran_id
    from    dual;
    
    insert into PSS.TRAN
    (
        tran_id,
        tran_start_ts,
        tran_end_ts,
        tran_upload_ts,
        tran_state_cd,
        tran_device_result_type_cd,
        tran_device_tran_cd,
        tran_received_raw_acct_data,
        tran_parsed_acct_name,
        tran_parsed_acct_exp_date,
        tran_parsed_acct_num,
        tran_reportable_acct_num,
        pos_pta_id,
        tran_legacy_trans_no,
        tran_global_trans_cd
    )
    select
        vn_tran_id,
        transaction_date,
        transaction_date,
        batch_date,
        'D',
        'S',
        substr(machine_trans_no, 12),
        mag_stripe,
        '',
        expiration_date,
        card_number,
        substr(card_number, length(card_number)-3),
        vn_pos_pta_id,
        trans_no,
        pv_machine_trans_no
    from transaction_record_hist
    where machine_trans_no = pv_machine_trans_no;
    
    select  PSS.SEQ_AUTH_ID.NEXTVAL
    into    vn_auth_id
    from    dual;

    insert into PSS.AUTH
    (
        auth_id,
        tran_id,
        auth_type_cd,
        auth_state_id,
        auth_parsed_acct_data,
        acct_entry_method_cd,
        auth_amt,
        auth_amt_approved,
        auth_ts,
        auth_result_cd,
        auth_resp_cd,
        auth_resp_desc
    )
    select
        vn_auth_id,
        vn_tran_id,
        'N',
        '2',
        mag_stripe,
        '3',
        auth_amt,
        auth_amt,
        transaction_date,
        'Y',
        '',
        response_msg
    from transaction_record_hist
    where machine_trans_no = pv_machine_trans_no;
    
    select  PSS.SEQ_AUTH_ID.NEXTVAL
    into    vn_sale_id
    from    dual;
    
    insert into PSS.AUTH
    (
        auth_id,
        tran_id,
        auth_type_cd,
        auth_state_id,
        auth_parsed_acct_data,
        acct_entry_method_cd,
        auth_amt,
        auth_amt_approved,
        auth_ts,
        auth_result_cd,
        auth_resp_cd,
        auth_resp_desc
    )
    select
        vn_sale_id,
        vn_tran_id,
        'U',
        '2',
        mag_stripe,
        '3',
        transaction_amount,
        transaction_amount,
        force_date,
        'Y',
        '',
        force_response_msg
    from transaction_record_hist
    where machine_trans_no = pv_machine_trans_no;
    
    select  PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
    into    vn_settlement_batch_id
    from    dual;
    
    insert into PSS.SETTLEMENT_BATCH
    (
        settlement_batch_id,
        settlement_batch_start_ts,
        settlement_batch_end_ts,
        settlement_batch_state_id,
        settlement_batch_resp_cd,
        settlement_batch_resp_desc
    )
    select
        vn_settlement_batch_id,
        force_date,
        force_date,
        '1',
        'A',
        force_response_msg
    from transaction_record_hist
    where machine_trans_no = pv_machine_trans_no;

    insert into PSS.TRAN_SETTLEMENT_BATCH
    (
        auth_id,
        settlement_batch_id,
        tran_settlement_b_resp_cd,
        tran_settlement_b_resp_desc,
        tran_settlement_b_amt
    )
    select
        vn_sale_id,
        vn_settlement_batch_id,
        'A',
        force_response_msg,
        transaction_amount
    from transaction_record_hist
    where machine_trans_no = pv_machine_trans_no;
    
    
    select  auth.auth_amt
    into    vn_batch_amount
    from    auth
    where   auth.auth_id = vn_sale_id;
    
    if pv_kiosk_tran_details is not null then
        LOAD_TRAN_DETAILS
        (
            vn_device_id,
            vn_device_type_id,
            vn_tran_id,
            vn_batch_amount,
            'S',
            pv_kiosk_tran_details
        );
    end if;

END;
/
