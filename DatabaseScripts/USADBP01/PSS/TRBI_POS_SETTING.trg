CREATE OR REPLACE TRIGGER pss.trbi_pos_setting
BEFORE INSERT ON pss.pos_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/