
/* 

Sony data migration from TAZDBA to PSS --

Be sure to set the max date on the trans so we dont get dups


*/

DECLARE
	        
    vn_device_id            device.device_id%TYPE;
    vn_pos_pta_id           pos_pta.pos_pta_id%TYPE;
    vn_tran_id              tran.tran_id%TYPE;
    vn_auth_id              auth.auth_id%TYPE;
    vn_sale_id              auth.auth_id%TYPE;    
    
BEGIN
	
	
	
	FOR vn_transnos IN (
	    SELECT trh.trans_no, trh.machine_trans_no, trh.card_type, trh.force_action_cd
		FROM tazdba.transaction_record_hist trh
	    WHERE trh.machine_id IN (
	        SELECT dev.device_name
	        FROM device dev
	        WHERE dev.device_type_id = 4
	    )
	    AND trh.transaction_date >= TO_DATE('2006-05-01', 'YYYY-MM-DD')
	    AND trh.transaction_date < TO_DATE('2006-05-25 12:25:00', 'YYYY-MM-DD HH24:MI:SS')
	)		
    LOOP
  
    
        SELECT device.device_id
        INTO vn_device_id
        FROM device
        WHERE device.device_name = (
            SELECT machine_id
            FROM transaction_record_hist
            WHERE machine_trans_no = vn_transnos.machine_trans_no
        );
    	
    	
        SELECT  MAX(pp.pos_pta_id)
        INTO    vn_pos_pta_id
        FROM    pos_pta pp, pos p
        WHERE   p.pos_id = pp.pos_id
        AND     p.device_id = vn_device_id
        AND     pp.payment_subtype_id = (CASE
				WHEN vn_transnos.card_type = 'SP' THEN 12
				ELSE 15
			END);
    
        SELECT  PSS.SEQ_TRAN_ID.NEXTVAL
        INTO    vn_tran_id
        FROM    dual;
        
        INSERT INTO PSS.TRAN
        (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_result_type_cd,
            tran_device_tran_cd,
            tran_received_raw_acct_data,
            tran_parsed_acct_name,
            tran_parsed_acct_exp_date,
            tran_parsed_acct_num,
            tran_reportable_acct_num,
            pos_pta_id,
            tran_legacy_trans_no,
            tran_global_trans_cd
        )
        SELECT
            vn_tran_id,
            transaction_date,
            transaction_date,
            batch_date,
            (CASE
				WHEN vn_transnos.force_action_cd = 'E' THEN 'E'
				ELSE 'D'
			END), 
            'S',
            SUBSTR(machine_trans_no, -10),
            mag_stripe,
            '',
            expiration_date,
            card_number,
            SUBSTR(card_number, LENGTH(card_number)-3),
            vn_pos_pta_id,
            trans_no,
            vn_transnos.machine_trans_no
        FROM transaction_record_hist
        WHERE machine_trans_no = vn_transnos.machine_trans_no;
        
        SELECT  PSS.SEQ_AUTH_ID.NEXTVAL
        INTO    vn_auth_id
        FROM    dual;
    
        INSERT INTO PSS.AUTH
        (
            auth_id,
            tran_id,
            auth_type_cd,
            auth_state_id,
            auth_parsed_acct_data,
            acct_entry_method_cd,
            auth_amt,
            auth_amt_approved,
            auth_ts,
            auth_result_cd,
            auth_resp_cd,
            auth_resp_desc
        )
        SELECT
            vn_auth_id,
            vn_tran_id,
            'N',
            '2',
            mag_stripe,
            '3',
            (CASE
				WHEN auth_amt IS NULL THEN 0
				ELSE auth_amt
			END),
            (CASE
				WHEN auth_amt IS NULL THEN 0
				ELSE auth_amt
			END),
            transaction_date,
            'Y',
            '',
            response_msg
        FROM transaction_record_hist
        WHERE machine_trans_no = vn_transnos.machine_trans_no;
        
        SELECT  PSS.SEQ_AUTH_ID.NEXTVAL
        INTO    vn_sale_id
        FROM    dual;
        
        INSERT INTO PSS.AUTH
        (
            auth_id,
            tran_id,
            auth_type_cd,
            auth_state_id,
            auth_parsed_acct_data,
            acct_entry_method_cd,
            auth_amt,
            auth_amt_approved,
            auth_ts,
            auth_result_cd,
            auth_resp_cd,
            auth_resp_desc
        )
        SELECT
            vn_sale_id,
            vn_tran_id,
            'U',
            '2',
            mag_stripe,
            '3',
            (CASE
				WHEN transaction_amount IS NULL THEN 0
				ELSE transaction_amount
			END),
			(CASE
				WHEN transaction_amount IS NULL THEN 0
				ELSE transaction_amount
			END),
            force_date,
            'Y',
            '',
            force_response_msg
        FROM transaction_record_hist
        WHERE machine_trans_no = vn_transnos.machine_trans_no;
        
        
    END LOOP;
    
END;
