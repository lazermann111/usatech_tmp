CREATE OR REPLACE TRIGGER pss.trbi_consumer_type
BEFORE INSERT ON pss.consumer_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
 SELECT seq_consumer_type_id.nextval,
           sysdate,
           user,
           sysdate,
           user
      into :new.consumer_type_id,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
          :new.last_updated_by
      FROM dual;
End;

/