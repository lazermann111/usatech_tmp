CREATE OR REPLACE TRIGGER pss.trbu_pos_setting
BEFORE UPDATE ON pss.pos_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

     SELECT
           sysdate,
           user
      into
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/