CREATE OR REPLACE TRIGGER pss.tri_tran_line_item_batch_type
AFTER INSERT ON pss.tran_line_item_batch_type
REFERENCING NEW AS NEWROW OLD AS OLD
FOR EACH ROW
begin  Insert into Rep_LogTable values ( Seq_Rep_LogTable.nextVal , 'PSS.TRAN_LINE_ITEM_BATCH_TYPE');  Insert Into PSS.REP_SHADOW_TRAN_LINE_ITE_0 ( Rep_sync_id, Rep_common_id, Rep_operationType, Rep_status, TRAN_LINE_ITEM_BATCH_TYPE_CD , TRAN_LINE_ITEM_BATCH_TYPE_DESC , rep_old_TRAN_LINE_ITEM_BATCH_0 , Rep_server_name , Rep_PK_Changed ) Values ( PSS.Seq_REP_SHADOW_TRAN_LINE_ITE_0.nextVal, null ,'I', null , :newRow.TRAN_LINE_ITEM_BATCH_TYPE_CD , :newRow.TRAN_LINE_ITEM_BATCH_TYPE_DESC , :newRow.TRAN_LINE_ITEM_BATCH_TYPE_CD , 'krugger_3001',null) ; end ; 
/