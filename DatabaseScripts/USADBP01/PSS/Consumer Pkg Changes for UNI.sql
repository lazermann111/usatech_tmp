INSERT INTO pss.consumer_type(consumer_type_id, consumer_type_desc)
VALUES(2, 'Staff');

INSERT INTO pss.consumer_type(consumer_type_id, consumer_type_desc)
VALUES(3, 'Custodial');

UPDATE pss.consumer_type SET consumer_type_id = 2 WHERE consumer_type_desc = 'Staff';
UPDATE pss.consumer_type SET consumer_type_id = 3 WHERE consumer_type_desc = 'Custodial';

COMMIT;
--select * from pss.consumer_type;

@@PKG_CONSUMER_MAINT.psk;
@@PKG_CONSUMER_MAINT.pbk;

@@VW_STUDENT_ACCOUNT.vws;

