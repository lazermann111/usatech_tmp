CREATE OR REPLACE TRIGGER pss.trbi_cc_merchant
BEFORE INSERT ON pss.cc_merchant
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/