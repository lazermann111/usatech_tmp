CREATE OR REPLACE TRIGGER pss.trbi_tran
BEFORE INSERT ON pss.tran
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.tran_id IS NULL
   THEN
      SELECT SEQ_tran_id.NEXTVAL
        INTO :NEW.tran_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
	 
	IF :NEW.tran_received_raw_acct_data IS NOT NULL THEN
		:NEW.tran_received_raw_acct_data := DBADMIN.MASK_CREDIT_CARD(:NEW.tran_received_raw_acct_data);
	END IF;
END;
/