CREATE OR REPLACE TRIGGER pss.trbi_refund
BEFORE INSERT ON pss.refund
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.refund_id IS NULL
   THEN
      SELECT seq_refund_id.NEXTVAL
        INTO :NEW.refund_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/