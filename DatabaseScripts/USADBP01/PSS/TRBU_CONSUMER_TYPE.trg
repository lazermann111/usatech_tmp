CREATE OR REPLACE TRIGGER pss.trbu_consumer_type
BEFORE UPDATE ON pss.consumer_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
 SELECT
            :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
                 :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
          :new.last_updated_by
      FROM dual;
End;

/