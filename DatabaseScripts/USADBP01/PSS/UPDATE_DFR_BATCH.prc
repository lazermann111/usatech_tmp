CREATE OR REPLACE PROCEDURE PSS.UPDATE_DFR_BATCH(
    pn_file_cache_id PSS.AUTH.DFR_FILE_CACHE_ID%TYPE)
IS
    ld_first_ts DATE;
    ld_last_ts DATE;
BEGIN
    SELECT NVL(MIN(AUTH_AUTHORITY_TS), MAX_DATE), NVL(MAX(AUTH_AUTHORITY_TS), MIN_DATE)
      INTO ld_first_ts, ld_last_ts
      FROM PSS.AUTH
     WHERE DFR_FILE_CACHE_ID = pn_file_cache_id 
       AND AUTH_TYPE_CD NOT IN('N', 'L');
    SELECT LEAST(ld_first_ts, NVL(MIN(REFUND_AUTHORITY_TS), MAX_DATE)), 
           GREATEST(ld_last_ts, NVL(MAX(REFUND_AUTHORITY_TS), MIN_DATE))
      INTO ld_first_ts, ld_last_ts
      FROM PSS.REFUND
     WHERE DFR_FILE_CACHE_ID = pn_file_cache_id;
    IF ld_first_ts <= ld_last_ts THEN
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'I'
         WHERE TRAN_STATE_CD = 'J'
           AND TRAN_ID IN(
           SELECT A.TRAN_ID 
             FROM PSS.AUTH A JOIN PSS.TRAN T ON A.TRAN_ID=T.TRAN_ID
            WHERE A.AUTH_TYPE_CD NOT IN('N', 'L') 
              AND A.DFR_FILE_CACHE_ID IS NULL
              AND T.PAYMENT_SUBTYPE_CLASS = 'Tandem'
              AND A.AUTH_AUTHORITY_TS BETWEEN ld_first_ts AND ld_last_ts
              AND A.AUTH_STATE_ID = 4
            UNION ALL
           SELECT R.TRAN_ID
             FROM PSS.REFUND R JOIN PSS.TRAN T ON R.TRAN_ID=T.TRAN_ID
            WHERE R.DFR_FILE_CACHE_ID IS NULL
              AND T.PAYMENT_SUBTYPE_CLASS = 'Tandem'
              AND R.REFUND_AUTHORITY_TS BETWEEN ld_first_ts AND ld_last_ts
              AND R.REFUND_STATE_ID = 3);
        -- Update sale transactions that are in PROCESSING_SERVER_TRAN (A) state to be reprocessed (8)
        -- Where the sale was sent in the timespan but it was not in the DFR
        update pss.tran
        set tran_state_cd = '8'
        where tran_id in (
          select /*+ INDEX(T IDX_TRAN_STATE_CD) INDEX(SA IDX_AUTH_TRAN_ID) */ t.tran_id
          from pss.tran t
          join pss.auth sa on sa.tran_id = t.tran_id
            and sa.auth_type_cd = 'U'
          where t.tran_state_cd = 'A'
          and t.payment_subtype_class = 'Tandem'
          AND sa.auth_ts between ld_first_ts and ld_last_ts
          and sa.dfr_file_cache_id is null);
        -- Update auth adjust transactions that are in PROCESSING_SERVER_TRAN (A) state to be FAILED (E)
        -- Where the sale was sent in the timespan but it was not in the DFR
        update pss.tran
        set tran_state_cd = 'E'
        where tran_id in (
          select /*+ INDEX(T IDX_TRAN_STATE_CD) INDEX(SA IDX_AUTH_TRAN_ID) */ t.tran_id
          from pss.tran t
          join pss.auth sa on sa.tran_id = t.tran_id
            and sa.auth_type_cd = 'C'
          where t.tran_state_cd = 'A'
          and t.payment_subtype_class = 'Tandem'
          AND sa.auth_ts between ld_first_ts and ld_last_ts
          and sa.dfr_file_cache_id is null);
        -- Handle refunds?
    END IF;
END;
/