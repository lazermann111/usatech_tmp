CREATE OR REPLACE PROCEDURE PSS.UPDATE_SPROUT_CONSUMER_ACCT (
	pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	pn_parent_tran_id PSS.TRAN.TRAN_ID%TYPE,
	pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
    pc_auth_type_cd VARCHAR2,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pd_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE)
IS
	ln_cash_back_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
    ln_cash_back_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
    ln_cash_back_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
    ln_cash_back_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    ln_used_cash_back_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
    ln_cash_back_amount pss.sale.sale_amount%TYPE;
    lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
    ld_cash_back_time NUMBER;
    ld_cash_back_ts DATE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
    ln_host_id HOST.HOST_ID%TYPE;
    lv_cash_back_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_cash_back_next_master_id NUMBER;
    lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_cash_back_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_cash_back_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    l_sprout_refund_ts DATE;
    ln_amount pss.sale.sale_amount%TYPE;
    ln_loyalty_discount NUMBER;
    ln_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_replenish_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    ln_replenish_bonus PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    lc_auth_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
    lc_hash_function VARCHAR2(100);
BEGIN
	SELECT MAX(APPLY_TO_CONSUMER_ACCT_ID)
          INTO ln_apply_to_consumer_acct_id
          FROM PSS.TRAN_LINE_ITEM
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_LINE_ITEM_TYPE_ID =556;
         IF ln_apply_to_consumer_acct_id is not null THEN
         	-- replenish setup to remove pending
           SELECT DEVICE_NAME, TRAN_DEVICE_TRAN_CD into lv_device_name, lv_device_tran_cd
           FROM PSS.TRAN 
           WHERE TRAN_ID =pn_parent_tran_id;
           DELETE FROM PSS.CONSUMER_ACCT_PEND_REPLENISH 
           WHERE DEVICE_NAME = lv_device_name
             AND DEVICE_TRAN_CD = TO_NUMBER_OR_NULL(lv_device_tran_cd)
             AND EXISTS(SELECT 1 FROM PSS.CONSUMER_ACCT where consumer_acct_id=ln_apply_to_consumer_acct_id and CONSUMER_ACCT_TYPE_ID=6);
         END IF;
         
         IF pn_sale_auth_id IS NOT NULL THEN
          IF pn_consumer_acct_id IS NOT NULL THEN
          SELECT MAX(CONSUMER_ACCT_TYPE_ID)
          INTO ln_consumer_acct_type_id
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
         
         select MAX(auth_type_cd) into lc_auth_auth_type_cd from pss.auth where tran_id=pn_tran_id and auth_type_cd in ('L','N');
         
          -- apply sprout logic
            IF ln_consumer_acct_type_id = 6 and lc_auth_auth_type_cd!= 'L' THEN
                    SELECT SUM(CASE WHEN TRAN_LINE_ITEM_TYPE_ID != 204 THEN NVL(TLI.TRAN_LINE_ITEM_AMOUNT, 0) * NVL(TLI.TRAN_LINE_ITEM_QUANTITY, 0) ELSE NULL END) tran_amount,
        SUM(CASE WHEN TRAN_LINE_ITEM_TYPE_ID = 204 THEN NVL(TLI.TRAN_LINE_ITEM_AMOUNT, 0) * NVL(TLI.TRAN_LINE_ITEM_QUANTITY, 0) ELSE NULL END) loyalty_discount
          INTO ln_amount,ln_loyalty_discount
          FROM PSS.TRAN_LINE_ITEM tli
         WHERE tli.TRAN_ID = pn_tran_id ;  
         
	UPDATE PSS.CONSUMER_ACCT
    SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE - ln_amount ,
    LOYALTY_DISCOUNT_TOTAL = NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ABS(ln_loyalty_discount),                       
	TRAN_COUNT_TOTAL=NVL(TRAN_COUNT_TOTAL, 0) + 1 
    WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
    
	SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
	INTO ln_cash_back_percent, ln_cash_back_threshhold, ln_cash_back_campaign_id
	FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
	FROM REPORT.CAMPAIGN C
	JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
	WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
	AND pd_tran_start_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
	AND C.CAMPAIGN_TYPE_ID = 3 /* Spend reward - Cash back */
	AND C.DISCOUNT_PERCENT > 0
	AND C.DISCOUNT_PERCENT < 1
	AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, pd_tran_start_ts) = 'Y')
	ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
	WHERE ROWNUM = 1;
	
	IF ln_cash_back_campaign_id IS NOT NULL AND ln_cash_back_percent > 0 THEN
		UPDATE PSS.CONSUMER_ACCT
        SET TOWARD_CASH_BACK_BALANCE = NVL(TOWARD_CASH_BACK_BALANCE, 0) + ln_amount
        WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
        RETURNING TOWARD_CASH_BACK_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_IDENTIFIER
        INTO ln_cash_back_balance, lv_currency_cd, ln_corp_customer_id,lv_consumer_acct_identifier;
        
        IF ln_cash_back_balance >= ln_cash_back_threshhold THEN
        	ln_used_cash_back_balance := TRUNC(ln_cash_back_balance / ln_cash_back_threshhold) * ln_cash_back_threshhold;
            ln_cash_back_amount := ROUND(ln_used_cash_back_balance * ln_cash_back_percent, 2);
            UPDATE PSS.CONSUMER_ACCT
            SET TOWARD_CASH_BACK_BALANCE = TOWARD_CASH_BACK_BALANCE - ln_used_cash_back_balance,
            CASH_BACK_TOTAL = NVL(CASH_BACK_TOTAL, 0) + ln_cash_back_amount,
            CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + ln_cash_back_amount,
            CONSUMER_ACCT_PROMO_BALANCE = NVL(CONSUMER_ACCT_PROMO_BALANCE,0) + ln_cash_back_amount,
            CONSUMER_ACCT_PROMO_TOTAL = NVL(CONSUMER_ACCT_PROMO_TOTAL,0) + ln_cash_back_amount
            WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;                         
            -- add trans to virtual terminal
            SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
            INTO ln_minor_currency_factor, lc_currency_symbol, ld_cash_back_time, ld_cash_back_ts
            FROM PSS.CURRENCY C
            LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
            WHERE C.CURRENCY_CD = lv_currency_cd;
            
                            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || ln_corp_customer_id || '-' || lv_currency_cd, lv_cash_back_device_name, ln_cash_back_next_master_id);
						    SELECT
						    	APP_SETTING_VALUE
						    INTO
						    	lc_hash_function
						    FROM
						    	ENGINE.APP_SETTING
						    WHERE
						   		APP_SETTING_CD='SALES_HASH_FUNCTION';
                            PKG_TRAN.SP_CREATE_SALE('A', lv_cash_back_device_name, ln_cash_back_next_master_id,  0, 'C', ld_cash_back_time, 
                                DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, ln_cash_back_amount * ln_minor_currency_factor, 'U', 'A', lc_hash_function, 
                                RAWTOHEX(DBADMIN.GET_UNSALTED_HASH(UTL_RAW.CAST_TO_RAW('Bonus Cash on ' || pn_consumer_acct_id || ' of ' || ln_cash_back_amount), lc_hash_function)),
                                NULL, ln_result_cd, lv_error_message, ln_cash_back_tran_id, lv_cash_back_tran_state_cd);
                            IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                RAISE_APPLICATION_ERROR(-20118, 'Could not create cash back transaction: ' || lv_error_message);
                            END IF;
                            SELECT HOST_ID, 'Bonus Cash for ' || lc_currency_symbol || TO_NUMBER(ln_used_cash_back_balance, 'FM9,999,999.00') || ' in purchases'
                              INTO ln_host_id, lv_desc
                              FROM (SELECT H.HOST_ID
                                      FROM DEVICE.HOST H
                                      JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                                     WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                                       AND H.HOST_PORT_NUM IN(0,1)
                                       AND D.DEVICE_NAME = lv_cash_back_device_name
                                       AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                                     ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                             WHERE ROWNUM = 1;
                            INSERT INTO PSS.TRAN_LINE_ITEM(
                                TRAN_ID,
                                TRAN_LINE_ITEM_AMOUNT,
                                TRAN_LINE_ITEM_POSITION_CD,
                                TRAN_LINE_ITEM_TAX,
                                TRAN_LINE_ITEM_TYPE_ID,
                                TRAN_LINE_ITEM_QUANTITY,
                                TRAN_LINE_ITEM_DESC,
                                HOST_ID,
                                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                                TRAN_LINE_ITEM_TS,
                                SALE_RESULT_ID,
                                APPLY_TO_CONSUMER_ACCT_ID,
                                CAMPAIGN_ID)
                            SELECT
                                ln_cash_back_tran_id,
                                ln_used_cash_back_balance,
                                NULL,
                                NULL,
                                554,
                                ln_cash_back_percent,
                                lv_desc,
                                ln_host_id,
                                'A',
                                ld_cash_back_ts,
                                0,
                                pn_consumer_acct_id,
                                ln_cash_back_campaign_id
                            FROM DUAL;
                            UPDATE PSS.TRAN
                               SET PARENT_TRAN_ID = pn_tran_id
                             WHERE TRAN_ID = ln_cash_back_tran_id;
                             
          select PSS.SEQ_TRAN_ID.NEXTVAL into ln_tran_id from dual;
          select sysdate into l_sprout_refund_ts from dual;
          PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || ln_corp_customer_id || '-' || lv_currency_cd, lv_cash_back_device_name, ln_cash_back_next_master_id);
          INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            DEVICE_NAME
            )
           SELECT ln_tran_id, 
                  pn_tran_id, 
                  l_sprout_refund_ts,
                  l_sprout_refund_ts,
                  NULL, /* Must be NULL so that PSSUpdater will not pick it up */
                  'RF:'||lv_cash_back_device_name||':'||ln_cash_back_next_master_id,
                  '8',
                  pn_consumer_acct_id, 
                  ln_cash_back_next_master_id, 
                  pp.POS_PTA_ID, -- ? use V1-1-USD entry Sprout Prepaid Card Manual Entry
                  null, 
                  null, 
                  pp.PAYMENT_SUBTYPE_KEY_ID, 
                  ps.PAYMENT_SUBTYPE_CLASS, 
                  ps.CLIENT_PAYMENT_TYPE_CD,
                  d.DEVICE_NAME 
          FROM pss.pos_pta pp 
          JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class='Sprout' and client_payment_type_cd='T'
          JOIN pss.pos p ON pp.pos_id = p.pos_id
          JOIN device.device d ON p.device_id = d.device_id
          where d.device_serial_cd='V1-' || ln_corp_customer_id || '-' || lv_currency_cd;
          
          INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_tran_id,
                        ln_cash_back_amount,
                        NULL,
                        NULL,
                        557,
                        1,
                         'Sprout cashback for consumer_acct_id ' || pn_consumer_acct_id,
                        NULL,
                        'A',
                        l_sprout_refund_ts,
                        0,
                        pn_consumer_acct_id,
                        ln_cash_back_campaign_id
                    FROM DUAL;
          
          INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD,
            OVERRIDE_TRANS_TYPE_ID,
            REFUND_PARSED_ACCT_DATA
        ) VALUES (
            ln_tran_id,  -- seq
            -ABS(ln_cash_back_amount),
            'Cashback for sprout consumer_acct_id '||pn_consumer_acct_id,
            l_sprout_refund_ts,
            'Sprout Cashback', 
            'S',
            6,
            '2',
            31,
            lv_consumer_acct_identifier);
        END IF;
    END IF;
            END IF;
          END IF;
         ELSIF pn_refund_id IS NOT NULL THEN
          IF  pc_auth_type_cd ='S' THEN
         	-- sprout replenishment
            SELECT  SUM(CASE WHEN TRAN_LINE_ITEM_TYPE_ID = 556 THEN NVL(TLI.TRAN_LINE_ITEM_AMOUNT, 0) * NVL(TLI.TRAN_LINE_ITEM_QUANTITY, 0) ELSE NULL END) replenish_amount,
            SUM(CASE WHEN TRAN_LINE_ITEM_TYPE_ID = 560 THEN NVL(TLI.TRAN_LINE_ITEM_AMOUNT, 0) * NVL(TLI.TRAN_LINE_ITEM_QUANTITY, 0) ELSE NULL END) replenish_bonus
            INTO ln_replenish_amount, ln_replenish_bonus
            FROM PSS.TRAN_LINE_ITEM tli
            WHERE tli.TRAN_ID = pn_tran_id;
            
            IF ln_replenish_amount is not NULL THEN
              UPDATE PSS.CONSUMER_ACCT
              SET CONSUMER_ACCT_BALANCE = NVL(CONSUMER_ACCT_BALANCE,0) + ln_replenish_amount+NVL(ln_replenish_bonus,0),
              CONSUMER_ACCT_REPLEN_BALANCE = NVL(CONSUMER_ACCT_REPLEN_BALANCE,0) + ln_replenish_amount,
              CONSUMER_ACCT_REPLENISH_TOTAL = NVL(CONSUMER_ACCT_REPLENISH_TOTAL,0) + ln_replenish_amount,
              REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) + NVL(ln_replenish_bonus,0),
              CONSUMER_ACCT_PROMO_BALANCE = NVL(CONSUMER_ACCT_PROMO_BALANCE, 0) + NVL(ln_replenish_bonus,0),
              CONSUMER_ACCT_PROMO_TOTAL = NVL(CONSUMER_ACCT_PROMO_TOTAL, 0) + NVL(ln_replenish_bonus,0)
              WHERE CONSUMER_ACCT_ID =ln_apply_to_consumer_acct_id;
            END IF;
          ELSIF ln_consumer_acct_type_id = 6 AND pc_auth_type_cd ='G' THEN
            -- sprout purchase refund
            SELECT SUM(CASE WHEN TRAN_LINE_ITEM_TYPE_ID != 204 THEN NVL(TLI.TRAN_LINE_ITEM_AMOUNT, 0) * NVL(TLI.TRAN_LINE_ITEM_QUANTITY, 0) ELSE NULL END) tran_amount,
            SUM(CASE WHEN TRAN_LINE_ITEM_TYPE_ID = 204 THEN NVL(TLI.TRAN_LINE_ITEM_AMOUNT, 0) * NVL(TLI.TRAN_LINE_ITEM_QUANTITY, 0) ELSE NULL END) loyalty_discount
            INTO ln_amount,ln_loyalty_discount
            FROM PSS.TRAN_LINE_ITEM tli
            WHERE tli.TRAN_ID =(select parent_tran_id from pss.tran where tran_id=pn_tran_id);  
            
            UPDATE PSS.CONSUMER_ACCT
            SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE - ln_amount ,
            LOYALTY_DISCOUNT_TOTAL = NVL(LOYALTY_DISCOUNT_TOTAL, 0) - ABS(ln_loyalty_discount),                       
            --TRAN_COUNT_TOTAL=TRAN_COUNT_TOTAL - 1,
            TOWARD_CASH_BACK_BALANCE = TOWARD_CASH_BACK_BALANCE - ln_amount
            WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
          END IF;
         END IF;
	
END;
/
