CREATE OR REPLACE PROCEDURE PSS.GET_VALID_PASSCODE(
    pn_consumer_id PSS.CONSUMER_PASSCODE.CONSUMER_ID%TYPE,
    pv_passcode_type_cd PSS.PASSCODE_TYPE.PASSCODE_TYPE_CD%TYPE,
    pv_new_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
    pn_consumer_passcode_id OUT PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE,
    pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
IS
    ln_passcode_type_id PSS.PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE;
    ln_duration_days PSS.PASSCODE_TYPE.DURATION_DAYS%TYPE;
BEGIN
    SELECT PASSCODE_TYPE_ID, DURATION_DAYS
      INTO ln_passcode_type_id, ln_duration_days
      FROM PSS.PASSCODE_TYPE
     WHERE PASSCODE_TYPE_CD = pv_passcode_type_cd;
    SELECT MAX(CONSUMER_PASSCODE_ID), MAX(PASSCODE)
      INTO pn_consumer_passcode_id, pv_passcode
      FROM (SELECT CP.CONSUMER_PASSCODE_ID, CP.PASSCODE
              FROM PSS.CONSUMER_PASSCODE CP
             WHERE CP.CONSUMER_ID = pn_consumer_id
               AND CP.PASSCODE_TYPE_ID = ln_passcode_type_id 
               AND (CP.EXPIRATION_TS > SYSDATE OR CP.EXPIRATION_TS IS NULL)
             ORDER BY CP.EXPIRATION_TS DESC, CP.CONSUMER_PASSCODE_ID DESC)
     WHERE ROWNUM = 1;
    IF pn_consumer_passcode_id IS NULL THEN
        SELECT PSS.SEQ_CONSUMER_PASSCODE_ID.NEXTVAL, pv_new_passcode
          INTO pn_consumer_passcode_id, pv_passcode
          FROM DUAL;
        INSERT INTO PSS.CONSUMER_PASSCODE(CONSUMER_PASSCODE_ID, CONSUMER_ID, PASSCODE, PASSCODE_TYPE_ID, EXPIRATION_TS)
            VALUES(pn_consumer_passcode_id, pn_consumer_id, pv_new_passcode, ln_passcode_type_id, CASE WHEN ln_duration_days IS NOT NULL THEN SYSDATE + ln_duration_days END);
    END IF;
END;
/