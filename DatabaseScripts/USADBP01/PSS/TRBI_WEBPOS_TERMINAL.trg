CREATE OR REPLACE TRIGGER pss.trbi_webpos_terminal
BEFORE INSERT ON pss.webpos_terminal
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF :new.webpos_terminal_id IS NULL THEN
      SELECT pss.seq_webpos_terminal_id.nextval
        into :new.webpos_terminal_id
        FROM dual;
    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/