CREATE OR REPLACE TRIGGER pss.trbi_seq_tran_local_auth_ba_id
BEFORE INSERT ON pss.tran_local_auth_batch_archive
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.tran_local_auth_ba_id IS NULL THEN

      SELECT seq_tran_local_auth_ba_id.nextval
        into :new.tran_local_auth_ba_id
        FROM dual;

    END IF;

End;
/