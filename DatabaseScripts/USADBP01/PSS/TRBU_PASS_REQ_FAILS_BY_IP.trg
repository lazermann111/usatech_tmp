create or replace TRIGGER PSS.TRBU_PASS_REQ_FAILS_BY_IP BEFORE UPDATE ON PSS.PASS_REQ_FAILS_BY_IP
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/
