CREATE OR REPLACE TRIGGER pss.trbi_merchant_bank_acct
BEFORE INSERT ON pss.merchant_bank_acct
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.merchant_bank_acct_id IS NULL
	THEN
		SELECT SEQ_merchant_bank_acct_id.NEXTVAL
		INTO :NEW.merchant_bank_acct_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/