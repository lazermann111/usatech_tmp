create or replace TRIGGER PSS.TRBI_PASS_REQ_FAILS_BY_IP BEFORE INSERT ON PSS.PASS_REQ_FAILS_BY_IP
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/
