CREATE OR REPLACE FUNCTION PSS.GET_DEVICE_TYPE_ACTION_CD (
    n_consumer_acct_id IN NUMBER
)
RETURN VARCHAR2 IS

    cd  VARCHAR2(8);
    
BEGIN

    select max(dta.device_type_action_cd)
    into cd
    from consumer_acct ca2, pss.consumer_acct_permission cap, pss.permission_action pa, device.action a, device.device_type_action dta
    where ca2.consumer_acct_id = cap.consumer_acct_id
    and cap.permission_action_id = pa.permission_action_id
    and pa.action_id = a.action_id
    and a.action_id = dta.action_id
    and dta.device_type_id in (0,1)
    and ca2.consumer_acct_id = n_consumer_acct_id;

    RETURN (cd);
END;
/