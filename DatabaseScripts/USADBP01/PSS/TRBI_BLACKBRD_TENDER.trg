CREATE OR REPLACE TRIGGER pss.trbi_blackbrd_tender
BEFORE INSERT ON pss.blackbrd_tender
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.blackbrd_tender_id IS NULL THEN

      SELECT seq_blackbrd_tender_id.nextval
        into :new.blackbrd_tender_id
        FROM dual;

    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/