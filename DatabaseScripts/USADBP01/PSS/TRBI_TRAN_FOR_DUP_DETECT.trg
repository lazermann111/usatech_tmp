CREATE OR REPLACE TRIGGER pss.trbi_tran_for_dup_detect
BEFORE INSERT ON pss.tran_for_dup_detect
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.tran_for_dup_detect_id IS NULL THEN

      SELECT seq_tran_for_dup_detect_id.nextval
        into :new.tran_for_dup_detect_id
        FROM dual;

    END IF;

End;
/