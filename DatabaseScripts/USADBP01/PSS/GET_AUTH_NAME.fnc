CREATE OR REPLACE FUNCTION PSS.GET_AUTH_NAME (
    l_payment_subtype_key_name PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_KEY_NAME%TYPE,
    l_payment_subtype_key_id POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE)
  RETURN  VARCHAR2
  PARALLEL_ENABLE
  IS
--
-- Figures out the authority service type id and returns it
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- BKRUG        01/20/05 NEW
-- ERYBSKI      11/23/05 Added Aramark, Banorte, WebPOS, Authority.Authority
-- PCOWAN       03/08/06 Changed NOT APPLICABLE to return N/A instead of Cash
    l_auth_name VARCHAR2(100);
BEGIN 
    IF UPPER(l_payment_subtype_key_name) = 'INTERNAL_PAYMENT_TYPE_ID' THEN
        SELECT IA.INTERNAL_AUTHORITY_NAME
          INTO l_auth_name
          FROM INTERNAL_PAYMENT_TYPE IPT, INTERNAL_AUTHORITY IA
         WHERE IPT.INTERNAL_AUTHORITY_ID = IA.INTERNAL_AUTHORITY_ID
           AND IPT.INTERNAL_PAYMENT_TYPE_ID = l_payment_subtype_key_id;
           
    ELSIF UPPER(l_payment_subtype_key_name) = 'BLACKBRD_TENDER_ID' THEN
        SELECT BA.BLACKBRD_AUTHORITY_NAME
          INTO l_auth_name
          FROM BLACKBRD_TENDER BT, BLACKBRD_AUTHORITY BA
         WHERE BT.BLACKBRD_AUTHORITY_ID = BA.BLACKBRD_AUTHORITY_ID
           AND BT.BLACKBRD_TENDER_ID = l_payment_subtype_key_id;
           
    ELSIF UPPER(l_payment_subtype_key_name) = 'ARAMARK_PAYMENT_TYPE_ID' THEN
        SELECT AA.ARAMARK_AUTHORITY_NAME
          INTO l_auth_name
          FROM ARAMARK_PAYMENT_TYPE APT, ARAMARK_AUTHORITY AA
         WHERE APT.ARAMARK_AUTHORITY_ID = AA.ARAMARK_AUTHORITY_ID
           AND APT.ARAMARK_PAYMENT_TYPE_ID = l_payment_subtype_key_id;
           
    ELSIF UPPER(l_payment_subtype_key_name) = 'BANORTE_TERMINAL_ID' THEN
        SELECT BA.BANORTE_AUTHORITY_NAME
          INTO l_auth_name
          FROM BANORTE_TERMINAL BT, BANORTE_AUTHORITY BA
         WHERE BT.BANORTE_AUTHORITY_ID = BA.BANORTE_AUTHORITY_ID
           AND BT.BANORTE_TERMINAL_ID = l_payment_subtype_key_id;
           
    ELSIF UPPER(l_payment_subtype_key_name) = 'WEBPOS_TERMINAL_ID' THEN
        SELECT WA.WEBPOS_AUTHORITY_NAME
          INTO l_auth_name
          FROM WEBPOS_TERMINAL WT, WEBPOS_AUTHORITY WA
         WHERE WT.WEBPOS_AUTHORITY_ID = WA.WEBPOS_AUTHORITY_ID
           AND WT.WEBPOS_TERMINAL_ID = l_payment_subtype_key_id;

    ELSIF UPPER(l_payment_subtype_key_name) = 'TERMINAL_ID' THEN
        SELECT A.AUTHORITY_NAME
          INTO l_auth_name
          FROM TERMINAL T, MERCHANT M, AUTHORITY.AUTHORITY A
         WHERE T.MERCHANT_ID = M.MERCHANT_ID
           AND M.AUTHORITY_ID = A.AUTHORITY_ID
           AND T.TERMINAL_ID = l_payment_subtype_key_id;
           
    ELSIF UPPER(l_payment_subtype_key_name) = 'NOT APPLICABLE' THEN
        l_auth_name := 'Cash';
        
    END IF;
    RETURN l_auth_name;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN l_auth_name;
    WHEN OTHERS THEN
        RAISE;
END;
/