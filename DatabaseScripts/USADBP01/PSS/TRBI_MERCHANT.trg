CREATE OR REPLACE TRIGGER pss.trbi_merchant
BEFORE INSERT ON pss.merchant
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.merchant_id IS NULL
	THEN
		SELECT SEQ_merchant_id.NEXTVAL
		INTO :NEW.merchant_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/