CREATE OR REPLACE TRIGGER pss.tri_consumer_type
AFTER INSERT ON pss.consumer_type
REFERENCING NEW AS NEWROW OLD AS OLD
FOR EACH ROW
begin  Insert into Rep_LogTable values ( Seq_Rep_LogTable.nextVal , 'PSS.CONSUMER_TYPE');  Insert Into PSS.REP_SHADOW_CONSUMER_TYPE ( Rep_sync_id, Rep_common_id, Rep_operationType, Rep_status, CONSUMER_TYPE_ID , CONSUMER_TYPE_DESC , CREATED_BY , CREATED_TS , LAST_UPDATED_BY , LAST_UPDATED_TS , rep_old_CONSUMER_TYPE_ID , Rep_server_name , Rep_PK_Changed ) Values ( PSS.Seq_REP_SHADOW_CONSUMER_TYPE.nextVal, null ,'I', null , :newRow.CONSUMER_TYPE_ID , :newRow.CONSUMER_TYPE_DESC , :newRow.CREATED_BY , :newRow.CREATED_TS , :newRow.LAST_UPDATED_BY , :newRow.LAST_UPDATED_TS , :newRow.CONSUMER_TYPE_ID , 'krugger_3001',null) ; end ; 
/