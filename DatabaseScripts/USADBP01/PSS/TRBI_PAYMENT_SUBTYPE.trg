CREATE OR REPLACE TRIGGER pss.trbi_payment_subtype
BEFORE INSERT ON pss.payment_subtype
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.payment_subtype_id IS NULL THEN

      SELECT seq_payment_subtype_id.nextval
        into :new.payment_subtype_id
        FROM dual;

    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/