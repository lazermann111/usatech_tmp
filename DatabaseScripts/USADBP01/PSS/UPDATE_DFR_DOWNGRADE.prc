CREATE OR REPLACE PROCEDURE PSS.UPDATE_DFR_DOWNGRADE (
    pv_merchant_order_num VARCHAR2,
    pn_amount PSS.AUTH.AUTH_AMT%TYPE,
    pn_file_cache_id PSS.AUTH.DFR_FILE_CACHE_ID%TYPE,
    pc_card_type_cd PSS.AUTH.CARD_TYPE_CD%TYPE,
    pc_interchange PSS.AUTH.INTERCHANGE_CD%TYPE,
    pv_downgrade_reason_cd PSS.AUTH.DOWNGRADE_REASON_CD%TYPE,
    pv_downgrade_desc PSS.DOWNGRADE_REASON.DOWNGRADE_REASON_DESC%TYPE,
    pn_line_number REPORT.FILE_CACHE_ERROR.LINE_NUMBER%TYPE)
IS
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
BEGIN
    -- Create DOWNGRADE_REASON record
    BEGIN
        INSERT INTO PSS.DOWNGRADE_REASON(DOWNGRADE_REASON_CD, DOWNGRADE_REASON_DESC)
            SELECT pv_downgrade_reason_cd, pv_downgrade_desc
              FROM DUAL
             WHERE NOT EXISTS(SELECT 1 FROM PSS.DOWNGRADE_REASON WHERE DOWNGRADE_REASON_CD = pv_downgrade_reason_cd);
    EXCEPTION     
        WHEN DUP_VAL_ON_INDEX THEN
            NULL; -- Ignore
    END;
    -- find trans and auth or refund
    IF pn_amount >= 0 THEN
        BEGIN
            SELECT *
              INTO ln_auth_id
              FROM (
            SELECT /*+INDEX(X IDX_DEVICE_TRAN_CD )*/ A.AUTH_ID
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N'
             WHERE X.TRAN_GLOBAL_TRANS_CD = 'A:' || REPLACE(pv_merchant_order_num, '-', ':')
             AND X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1')
             ORDER BY CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find auth A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || pn_amount FROM DUAL;
                RETURN;
        END;
        UPDATE PSS.AUTH 
           SET DOWNGRADE_REASON_CD = pv_downgrade_reason_cd
         WHERE AUTH_ID = ln_auth_id;
    ELSE
        BEGIN
            SELECT *
              INTO ln_auth_id
              FROM (
            SELECT R.REFUND_ID               
              FROM PSS.TRAN X
              JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
             WHERE X.TRAN_GLOBAL_TRANS_CD = 'RF:' || REPLACE(pv_merchant_order_num, '-', ':')
             ORDER BY CASE WHEN R.REFUND_STATE_ID IN(1) THEN 1 WHEN R.REFUND_STATE_ID = 3 THEN 2 ELSE 3 END, R.REFUND_AUTHORITY_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find refund RF:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || ABS(pn_amount) FROM DUAL;
                RETURN;
        END;
        UPDATE PSS.REFUND
           SET DOWNGRADE_REASON_CD = pv_downgrade_reason_cd
         WHERE REFUND_ID = ln_auth_id;
    END IF;
END;
/
