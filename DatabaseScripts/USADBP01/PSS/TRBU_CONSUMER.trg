CREATE OR REPLACE TRIGGER pss.trbu_consumer
BEFORE UPDATE ON pss.consumer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
SELECT
         UPPER(:new.consumer_country_cd),
           UPPER(:new.consumer_state_cd),
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
           :new.consumer_country_cd,
           :new.consumer_state_cd,
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/