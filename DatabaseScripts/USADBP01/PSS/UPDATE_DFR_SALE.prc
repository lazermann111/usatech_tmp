CREATE OR REPLACE PROCEDURE PSS.UPDATE_DFR_SALE (
    pv_merchant_order_num VARCHAR2,
    pc_action_code CHAR,
    pn_amount PSS.AUTH.AUTH_AMT%TYPE,
    pn_file_cache_id PSS.AUTH.DFR_FILE_CACHE_ID%TYPE,
    pc_card_type_cd PSS.AUTH.CARD_TYPE_CD%TYPE,
    pc_interchange PSS.AUTH.INTERCHANGE_CD%TYPE,
    pv_fee_desc PSS.AUTH.FEE_DESC%TYPE,
    pn_fee_amount PSS.AUTH.FEE_AMOUNT%TYPE,
    pd_batch_date PSS.AUTH.BATCH_DATE%TYPE,
    pn_entity_num PSS.AUTH.ENTITY_NUM%TYPE,
    pv_submission_num PSS.AUTH.SUBMISSION_NUM%TYPE,
    pv_record_num PSS.AUTH.RECORD_NUM%TYPE,
    pn_line_number REPORT.FILE_CACHE_ERROR.LINE_NUMBER%TYPE,
    pc_sub_action_code CHAR DEFAULT '-')
IS
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
    lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE := NULL;
    lc_match_flag CHAR(1);
    lc_cancel_flag CHAR(1);
    ln_auth_amount PSS.AUTH.AUTH_AMT%TYPE;
    ln_reverse_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ld_current_ts DATE;
    lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    ln_duplicate_completion_id PSS.DUPLICATE_COMPLETION.DUPLICATE_COMPLETION_ID%TYPE;
    ln_override_trans_type_id PSS.AUTH.OVERRIDE_TRANS_TYPE_ID%TYPE;
    ln_host_id HOST.HOST_ID%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_reverse_amount PSS.AUTH.AUTH_AMT%TYPE;
    lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
    lc_report_trans_update_needed CHAR(1) := 'Y';
BEGIN
    -- find trans and auth or refund
    IF pc_action_code = 'D' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id,
                   lc_tran_state_cd,
                   lc_match_flag,
                   lc_cancel_flag,
                   ln_auth_amount,
                   lc_entry_method_cd,
                   ln_override_trans_type_id,
                   lv_tran_global_trans_cd
              FROM (
            SELECT /*+INDEX(X IDX_DEVICE_TRAN_CD )*/ X.TRAN_ID,
                   A.AUTH_ID,
                   X.TRAN_STATE_CD,
                   CASE WHEN pc_sub_action_code = 'E' THEN 'N'
                   		WHEN A.SUBMISSION_NUM = pv_submission_num AND A.RECORD_NUM = pv_record_num THEN 'Y' 
                        WHEN A.SUBMISSION_NUM IS NOT NULL AND A.RECORD_NUM IS NOT NULL AND pv_submission_num IS NOT NULL AND pv_record_num IS NOT NULL THEN 'D'
                        ELSE 'N' END MATCH_FLAG,
                   CASE WHEN A.AUTH_AMT = 0 THEN 'Y' ELSE 'N' END CANCEL_FLAG,
                   A.AUTH_AMT,
                   A.ACCT_ENTRY_METHOD_CD,
                   A.OVERRIDE_TRANS_TYPE_ID,
                   X.TRAN_GLOBAL_TRANS_CD
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD NOT IN('N', 'L')
             WHERE X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1') 
               AND X.TRAN_GLOBAL_TRANS_CD LIKE 'A:' || REPLACE(pv_merchant_order_num, '-', ':') || '%'
             ORDER BY 4 DESC, CASE WHEN A.AUTH_AMT = pn_amount THEN 1 ELSE 2 END, CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find sale A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || pn_amount FROM DUAL;
                RETURN;
        END;
    ELSIF pc_action_code = 'R' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id,
                   lc_tran_state_cd,
                   lc_match_flag,
                   lc_cancel_flag,
                   ln_auth_amount,
                   lc_entry_method_cd,
                   ln_override_trans_type_id,
                   lv_tran_global_trans_cd
              FROM (
            SELECT X.TRAN_ID,
                   R.REFUND_ID,
                   X.TRAN_STATE_CD,
                   CASE WHEN pc_sub_action_code = 'E' THEN 'N'
                   		WHEN R.SUBMISSION_NUM = pv_submission_num AND R.RECORD_NUM = pv_record_num THEN 'Y'
                        WHEN R.SUBMISSION_NUM IS NOT NULL AND R.RECORD_NUM IS NOT NULL AND pv_submission_num IS NOT NULL AND pv_record_num IS NOT NULL THEN 'D'
                        ELSE 'N' END MATCH_FLAG,
                   CASE WHEN R.REFUND_AMT = 0 THEN 'Y' ELSE 'N' END CANCEL_FLAG,
                   R.REFUND_AMT,
                   R.ACCT_ENTRY_METHOD_CD,
                   R.OVERRIDE_TRANS_TYPE_ID,
                   X.TRAN_GLOBAL_TRANS_CD
              FROM PSS.TRAN X
              JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
             WHERE X.TRAN_GLOBAL_TRANS_CD IN('RF:' || REPLACE(pv_merchant_order_num, '-', ':'), 'RV:' || REPLACE(pv_merchant_order_num, '-', ':')) 
             ORDER BY 4 DESC, CASE WHEN R.REFUND_AMT = pn_amount THEN 1 ELSE 2 END, CASE WHEN R.REFUND_STATE_ID IN(1) THEN 1 WHEN R.REFUND_STATE_ID = 3 THEN 2 ELSE 3 END, R.REFUND_AUTHORITY_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find refund RF:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || ABS(pn_amount) FROM DUAL;
                RETURN;
        END;
    ELSE
        --RAISE_APPLICATION_ERROR(-20170, 'Invalid action code "' || pc_action_code ||'" for merchant order ' || pv_merchant_order_num);
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Invalid action code "' || pc_action_code ||'" for merchant order ' || pv_merchant_order_num FROM DUAL;
        RETURN;
    END IF;
    IF lc_tran_state_cd NOT IN('D', 'C', 'V', 'A', 'B', 'E', 'I', 'J', 'X', 'Y') THEN
        --RAISE_APPLICATION_ERROR(-20172, 'Transaction '||ln_tran_id||' is in an invalid state "' || lc_tran_state_cd ||'"');
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Transaction '||ln_tran_id||' is in an invalid state "' || lc_tran_state_cd ||'"' FROM DUAL;
        RETURN;
    ELSIF ABS(ln_auth_amount) != ABS(pn_amount) THEN
        --RAISE_APPLICATION_ERROR(-20173, 'Transaction '||ln_tran_id||' amount does not match (' || ln_auth_amount ||' vs ' || pn_amount || ')');
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Transaction '||ln_tran_id||' amount does not match (' || ln_auth_amount ||' vs ' || pn_amount || ')' FROM DUAL;
        RETURN;        
    END IF;
    IF lc_match_flag = 'D' AND pn_amount != 0 THEN
        -- already updated but does not match
        BEGIN
            SELECT PSS.SEQ_DUPLICATE_COMPLETION_ID.NEXTVAL
              INTO ln_duplicate_completion_id
              FROM DUAL;
            INSERT INTO PSS.DUPLICATE_COMPLETION(DUPLICATE_COMPLETION_ID, TRAN_ID, SIGN_CD, AUTH_ID, DFR_FILE_CACHE_ID, SUBMISSION_NUM, RECORD_NUM,  
                  CARD_TYPE_CD, INTERCHANGE_CD, FEE_DESC, FEE_AMOUNT, BATCH_DATE, ENTITY_NUM)
                SELECT ln_duplicate_completion_id, ln_tran_id, CASE WHEN pc_action_code = 'D' THEN '+' ELSE '-' END, ln_auth_id, pn_file_cache_id, pv_submission_num, pv_record_num,
                       pc_card_type_cd, pc_interchange, pv_fee_desc,  pn_fee_amount, pd_batch_date, pn_entity_num  
                  FROM DUAL;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                RETURN; -- already processed this
        END;
        -- find sum of already reversed
        SELECT -pn_amount * COUNT_DUPS + NVL(EXISTING_REFUND_AMOUNT, 0) - NVL(EXISTING_CHARGE_AMOUNT, 0)
          INTO ln_reverse_amount
          FROM (SELECT COUNT(DISTINCT DUPLICATE_COMPLETION_ID) COUNT_DUPS
          FROM PSS.DUPLICATE_COMPLETION
         WHERE TRAN_ID = ln_tran_id)
         CROSS JOIN (SELECT SUM(ABS(R.REFUND_AMT)) EXISTING_REFUND_AMOUNT
          FROM PSS.REFUND R
          JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
         WHERE X.PARENT_TRAN_ID = ln_tran_id)
         CROSS JOIN (SELECT SUM(ABS(A.AUTH_AMT)) EXISTING_CHARGE_AMOUNT
          FROM PSS.AUTH A
          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
         WHERE X.PARENT_TRAN_ID = ln_tran_id
           AND A.AUTH_TYPE_CD NOT IN('N', 'L'));
        
        IF ln_reverse_amount != 0 THEN 
        -- create tran to reverse it
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL, SYSDATE
          INTO ln_reverse_tran_id, ld_current_ts
          FROM DUAL; 
        INSERT INTO PSS.TRAN (
                TRAN_ID,
                PARENT_TRAN_ID,
                TRAN_START_TS,
                TRAN_END_TS,
                TRAN_UPLOAD_TS,
                TRAN_GLOBAL_TRANS_CD,
                TRAN_STATE_CD,
                CONSUMER_ACCT_ID,
                TRAN_DEVICE_TRAN_CD,
                POS_PTA_ID,
                TRAN_DEVICE_RESULT_TYPE_CD,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                CLIENT_PAYMENT_TYPE_CD,
                DEVICE_NAME)
         SELECT ln_reverse_tran_id,
                ln_tran_id,
                O.TRAN_START_TS,
                O.TRAN_END_TS,
                NULL, /* Must be NULL so that it will NOT be imported */
                'RV' || SUBSTR(O.TRAN_GLOBAL_TRANS_CD, INSTR(O.TRAN_GLOBAL_TRANS_CD, ':'), 56) || ':' || (SELECT COUNT(*) + 1 FROM PSS.TRAN C WHERE O.TRAN_ID = C.PARENT_TRAN_ID), 
                '!',
                O.CONSUMER_ACCT_ID,
                O.TRAN_DEVICE_TRAN_CD,
                O.POS_PTA_ID,
                O.TRAN_DEVICE_RESULT_TYPE_CD,
                O.TRAN_RECEIVED_RAW_ACCT_DATA,
                O.PAYMENT_SUBTYPE_KEY_ID,
                O.PAYMENT_SUBTYPE_CLASS,
                O.CLIENT_PAYMENT_TYPE_CD,
                O.DEVICE_NAME
           FROM PSS.TRAN O
          WHERE O.TRAN_ID = ln_tran_id;
        IF pc_action_code = 'D' AND ln_reverse_amount < 0 THEN
            INSERT INTO PSS.REFUND (
                TRAN_ID,
                REFUND_AMT,
                REFUND_DESC,
                REFUND_ISSUE_TS,
                REFUND_ISSUE_BY,
                REFUND_TYPE_CD,
                REFUND_STATE_ID,
                ACCT_ENTRY_METHOD_CD,
                OVERRIDE_TRANS_TYPE_ID)
              VALUES (
                ln_reverse_tran_id,
                ln_reverse_amount,
                'Reversal of Duplicate Submission',
                ld_current_ts,
                'PSS',
                'R',
                6,
                lc_entry_method_cd,
                ln_override_trans_type_id);
        ELSIF ln_reverse_amount > 0 THEN
            INSERT INTO PSS.AUTH (
                TRAN_ID,
                AUTH_STATE_ID,
                AUTH_TYPE_CD,
                ACCT_ENTRY_METHOD_CD,
                AUTH_TS,
                AUTH_AMT,
                TRACE_NUMBER,
                CARD_KEY,
                OVERRIDE_TRANS_TYPE_ID)
             SELECT
                ln_reverse_tran_id, /* TRAN_ID */
                6, /* AUTH_STATE_ID */
                'E', /* AUTH_TYPE_CD */
                lc_entry_method_cd, /* ACCT_ENTRY_METHOD_CD */
                ld_current_ts, /* AUTH_TS */
                ln_reverse_amount, /* AUTH_AMT */
                ln_auth_id, /* TRACE_NUMBER */
                MAX(A.CARD_KEY),
                ln_override_trans_type_id
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.PARENT_TRAN_ID = A.TRAN_ID
             WHERE A.AUTH_TYPE_CD = 'N'
               AND X.TRAN_ID = ln_tran_id;
        END IF;
        -- Insert line item
        SELECT P.DEVICE_ID
          INTO ln_device_id
          FROM PSS.TRAN O
          JOIN PSS.POS_PTA PP ON O.POS_PTA_ID = PP.POS_PTA_ID
          JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
         WHERE O.TRAN_ID = ln_tran_id;
        ln_host_id := GET_OR_CREATE_HOST_ID(ln_device_id, 0, 0);
        INSERT INTO pss.tran_line_item (
            tran_line_item_id,
            tran_id,
            tran_line_item_amount,
            tran_line_item_position_cd,
            tran_line_item_tax,
            tran_line_item_type_id,
            tran_line_item_quantity,
            tran_line_item_desc,
            host_id,
            tran_line_item_batch_type_cd,
            tran_line_item_ts,
            sale_result_id)
          VALUES(
            PSS.SEQ_TLI_ID.NEXTVAL,
            ln_reverse_tran_id,
            ln_reverse_amount,
            NULL,
            NULL,
            505,
            1,
            'Reversal of Duplicate Submission ' || ln_duplicate_completion_id,
            ln_host_id,
            'A',
            ld_current_ts,
            0);
        END IF;
    END IF;
    IF lc_match_flag = 'Y' THEN
    	RETURN;
    END IF;
    IF pc_action_code = 'D' THEN
        UPDATE PSS.AUTH 
           SET DFR_FILE_CACHE_ID = pn_file_cache_id,
               CARD_TYPE_CD = pc_card_type_cd,
               INTERCHANGE_CD = pc_interchange,
               FEE_DESC = pv_fee_desc,
               FEE_AMOUNT = pn_fee_amount,
               BATCH_DATE = pd_batch_date,
               ENTITY_NUM = pn_entity_num,
               SUBMISSION_NUM = pv_submission_num,
               RECORD_NUM = pv_record_num,
               AUTH_STATE_ID = CASE pc_sub_action_code WHEN 'E' THEN 8 ELSE AUTH_STATE_ID END
         WHERE AUTH_ID = ln_auth_id;
    ELSE
        UPDATE PSS.REFUND
           SET DFR_FILE_CACHE_ID = pn_file_cache_id,
               CARD_TYPE_CD = pc_card_type_cd,
               INTERCHANGE_CD = pc_interchange,
               FEE_DESC = pv_fee_desc,
               FEE_AMOUNT = pn_fee_amount,
               BATCH_DATE = pd_batch_date,
               ENTITY_NUM = pn_entity_num,
               SUBMISSION_NUM = pv_submission_num,
               RECORD_NUM = pv_record_num,
               REFUND_STATE_ID = CASE pc_sub_action_code WHEN 'E' THEN 7 ELSE REFUND_STATE_ID END
         WHERE REFUND_ID = ln_auth_id;
    END IF;
    IF SQL%ROWCOUNT != 1 THEN
        --RAISE_APPLICATION_ERROR(-20171, 'Transaction ' || ln_tran_id ||' is being updated by another process');
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Transaction ' || ln_tran_id ||' is being updated by another process' FROM DUAL;
        RETURN;
    END IF;
    IF lc_tran_state_cd IN('A', 'B', 'E', 'I', 'J', 'X') THEN
    	IF lc_cancel_flag = 'Y' THEN 
    		lc_new_tran_state_cd := 'C';
    	ELSE
    		lc_new_tran_state_cd := 'D';    	    	
	        
    		IF lc_tran_state_cd = 'X' AND lv_tran_global_trans_cd NOT LIKE 'RV:%' THEN
	        	-- update settlement state
	        	BEGIN
		        	lc_report_trans_update_needed := 'N';
	        		REPORT.DATA_IN_PKG.UPDATE_SETTLE_INFO(lv_tran_global_trans_cd, 'PSS', 3, SYSDATE, NULL, NULL, NULL, NULL, ln_tran_id, pv_submission_num);
				EXCEPTION
					WHEN OTHERS THEN
						-- transaction is most likely not in REPORT.TRANS yet, set to PROCESSED_TRAN_DEPOSIT_CONFIRMED to be picked up by UPDATE_DEPOSITED_TRANSACTIONS job later
						lc_new_tran_state_cd := 'Y';
				END;
	        END IF;
        END IF;
    END IF;
    IF lc_report_trans_update_needed = 'Y' AND pv_submission_num IS NOT NULL AND lv_tran_global_trans_cd NOT LIKE 'RV:%' THEN
		UPDATE REPORT.TRANS
		SET SUBMISSION_NUM = pv_submission_num
		WHERE SOURCE_TRAN_ID = ln_tran_id;
		
		IF SQL%ROWCOUNT = 0 AND (lc_tran_state_cd = 'D' OR lc_new_tran_state_cd = 'D') THEN
			-- transaction is not in REPORT.TRANS yet, set to PROCESSED_TRAN_DEPOSIT_CONFIRMED to be picked up by UPDATE_DEPOSITED_TRANSACTIONS job later
			lc_new_tran_state_cd := 'Y';
		END IF;
	END IF;
	IF lc_new_tran_state_cd IS NOT NULL THEN
		UPDATE PSS.TRAN
        SET TRAN_STATE_CD = lc_new_tran_state_cd
        WHERE TRAN_ID = ln_tran_id
           AND TRAN_STATE_CD != lc_new_tran_state_cd;
	END IF;
END;
/
