CREATE OR REPLACE TRIGGER pss.trbi_tran_batch_archive
BEFORE INSERT ON pss.tran_batch_archive
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.tran_batch_archive_id IS NULL THEN

      SELECT seq_tran_batch_archive_id.nextval
        into :new.tran_batch_archive_id
        FROM dual;

    END IF;

End;
/