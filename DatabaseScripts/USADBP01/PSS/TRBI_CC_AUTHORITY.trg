CREATE OR REPLACE TRIGGER pss.trbi_cc_authority
BEFORE INSERT ON pss.cc_authority
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.CC_AUTHORITY_id IS NULL THEN

      SELECT seq_CC_AUTHORITY_id.nextval
        into :new.CC_AUTHORITY_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/