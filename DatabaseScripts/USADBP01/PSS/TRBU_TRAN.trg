CREATE OR REPLACE TRIGGER pss.trbu_tran
BEFORE UPDATE ON pss.tran
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
	 
	IF :NEW.tran_received_raw_acct_data IS NOT NULL AND
		(:OLD.tran_received_raw_acct_data IS NULL OR :OLD.tran_received_raw_acct_data <> :NEW.tran_received_raw_acct_data) THEN
		:NEW.tran_received_raw_acct_data := DBADMIN.MASK_CREDIT_CARD(:NEW.tran_received_raw_acct_data);
	END IF;
	
	IF :OLD.tran_state_cd <> :NEW.tran_state_cd THEN
		:NEW.previous_tran_state_cd := :OLD.tran_state_cd;
	END IF;
END;
/