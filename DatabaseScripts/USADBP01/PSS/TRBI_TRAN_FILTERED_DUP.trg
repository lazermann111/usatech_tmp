CREATE OR REPLACE TRIGGER pss.trbi_tran_filtered_dup
BEFORE INSERT ON pss.tran_filtered_dup
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.tran_filtered_dup_id IS NULL THEN

      SELECT seq_tran_filtered_dup_id.nextval
        into :new.tran_filtered_dup_id
        FROM dual;

    END IF;

End;
/