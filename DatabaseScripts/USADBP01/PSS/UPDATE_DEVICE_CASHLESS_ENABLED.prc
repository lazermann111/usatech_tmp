CREATE OR REPLACE PROCEDURE PSS.UPDATE_DEVICE_CASHLESS_ENABLED (pn_pos_id PSS.POS.POS_ID%TYPE)
IS
l_device_id NUMBER;
l_cashless_enabled char(1);
BEGIN
	 SELECT D.DEVICE_ID
      INTO l_device_id
      FROM PSS.POS P
      JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
     WHERE P.POS_ID = pn_pos_id;
     
  SELECT CASE WHEN MAX(PP.POS_PTA_ID) IS NULL THEN 'N' ELSE 'Y' END
  into l_cashless_enabled
    FROM PSS.POS P
    JOIN PSS.POS_PTA PP ON P.POS_ID=PP.POS_ID AND NVL(PP.POS_PTA_ACTIVATION_TS, MAX_DATE)<=SYSDATE AND NVL(PP.POS_PTA_DEACTIVATION_TS, MAX_DATE)>SYSDATE
    JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID=PS.PAYMENT_SUBTYPE_ID AND PAYMENT_SUBTYPE_CLASS<>'Authority::NOP'
    JOIN REPORT.TRANS_TYPE TT ON PS.TRANS_TYPE_ID=TT.TRANS_TYPE_ID AND TT.CASH_IND='N'
    WHERE P.DEVICE_ID=l_device_id;
    
    update device.device_setting 
    set device_setting_value =l_cashless_enabled
    where device_id=l_device_id and DEVICE_SETTING_PARAMETER_CD='Cashless Enabled'
     and (device_setting_value is null or device_setting_value <> l_cashless_enabled);
END;
/
