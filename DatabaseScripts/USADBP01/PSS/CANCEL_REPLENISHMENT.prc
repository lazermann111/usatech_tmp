CREATE OR REPLACE PROCEDURE PSS.CANCEL_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE)
IS
    ld_sale_utc_ts PSS.SALE.SALE_START_UTC_TS%TYPE;
    ld_sale_local_date PSS.TRAN.TRAN_START_TS%TYPE;
    lv_global_session_cd PSS.SALE.SALE_GLOBAL_SESSION_CD%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
    lc_hash_function VARCHAR2(100);
BEGIN
    SELECT A.AUTH_TS, A.AUTH_HOLD_USED
      INTO ld_auth_ts, lc_auth_hold_used
      FROM PSS.AUTH A
     WHERE A.AUTH_ID = pn_auth_id;
    
    UPDATE PSS.TRAN X
       SET TRAN_STATE_CD = DECODE(lc_auth_hold_used, 'Y', '8', 'C'),
           TRAN_END_TS = TRAN_START_TS,
           TRAN_UPLOAD_TS = SYSDATE,
           TRAN_DEVICE_RESULT_TYPE_CD = 'U'
     WHERE TRAN_ID = pn_tran_id
     RETURNING  TRAN_START_TS, AUTH_GLOBAL_SESSION_CD, DEVICE_NAME, TRAN_DEVICE_TRAN_CD
      INTO ld_sale_local_date, lv_global_session_cd, lv_device_name, lv_device_tran_cd;
    
    ld_sale_utc_ts := TO_TIMESTAMP(ld_auth_ts) AT TIME ZONE 'GMT';
    SELECT
    	APP_SETTING_VALUE
    INTO
    	lc_hash_function
    FROM
    	ENGINE.APP_SETTING
    WHERE
   		APP_SETTING_CD='SALES_HASH_FUNCTION';
    INSERT INTO PSS.SALE (
            TRAN_ID,
            DEVICE_BATCH_ID,
            SALE_TYPE_CD,
            SALE_START_UTC_TS,
            SALE_END_UTC_TS,
            SALE_UTC_OFFSET_MIN,
            SALE_RESULT_ID,
            SALE_AMOUNT,
            RECEIPT_RESULT_CD,
            HASH_TYPE_CD,
            TRAN_LINE_ITEM_HASH,
            SALE_GLOBAL_SESSION_CD,
            IMPORTED,
            VOID_ALLOWED) 
     SELECT pn_tran_id,
            pn_device_batch_id,
            'A',
            ld_sale_utc_ts,
            ld_sale_utc_ts,
            (ld_sale_local_date - CAST(ld_sale_utc_ts AS DATE)) * 24 * 60,
            1,
            0,
            pc_receipt_result_cd,
            lc_hash_function,
            '00',
            lv_global_session_cd,
            '-',
            'N'
       FROM DUAL;
END;
/