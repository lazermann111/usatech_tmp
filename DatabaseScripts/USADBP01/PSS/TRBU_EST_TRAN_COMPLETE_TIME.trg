CREATE OR REPLACE TRIGGER pss.trbu_est_tran_complete_time
BEFORE UPDATE ON pss.est_tran_complete_time
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

   -- If the user decides to update values for all the columns it is
   -- assumed the user knows what they want in each column and the
   -- user has performed any necessary calculations.  If the user
   -- only updates the a last_tran_complete_minut column then this
   -- trigger will recalculate the average based on the last_tran_complete_minut.
   IF (    :OLD.avg_tran_complete_minut = :NEW.avg_tran_complete_minut
       AND :OLD.num_cycle_in_avg = :NEW.num_cycle_in_avg) THEN
      -- Calculate the average time it took for the transaction line item to complete.
      -- If a transaction line item time was reported that was under 15 minutes or over
      -- 120 minutes then the value will be ignored.
      IF (    :NEW.last_tran_complete_minut >= 15
          AND :NEW.last_tran_complete_minut <= 120) THEN
         -- Calculate the new average.
         -- We only keep the average of the last 10 transactions, then reset.  This makes the
         --   calculation more sensitive to changes than using 999 or all transactions.
         IF (:OLD.num_cycle_in_avg = 10) THEN
            :NEW.num_cycle_in_avg := 2;
            :NEW.avg_tran_complete_minut := ((:OLD.avg_tran_complete_minut+:NEW.last_tran_complete_minut)/:NEW.num_cycle_in_avg);
         ELSE
            -- Calculate the new average
            :NEW.num_cycle_in_avg := :OLD.num_cycle_in_avg + 1;
            :NEW.avg_tran_complete_minut := (((:OLD.avg_tran_complete_minut*:OLD.num_cycle_in_avg)+:NEW.last_tran_complete_minut)/:NEW.num_cycle_in_avg);
         END IF;
      ELSE
         -- Ignore the new value because the complete time was out of
         -- the predefined threshold.
         :NEW.num_cycle_in_avg := :OLD.num_cycle_in_avg;
         :NEW.avg_tran_complete_minut := :OLD.avg_tran_complete_minut;
         :NEW.last_tran_complete_minut := :OLD.last_tran_complete_minut;
      END IF;
   END IF;
END;
/