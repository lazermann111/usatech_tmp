/*    *  Create table PSS.MV_ESUDS_SETTLED_REFUND with data drawn from the following columns:
          o PSS.REFUND_SETTLEMENT_BATCH.REFUND_SETTLEMENT_B_AMT
          o PSS.REFUND.REFUND_DESC
          o PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS
          o PSS.REFUND.TRAN_ID 
    * Create table PSS.MV_ESUDS_SETTLED_REFUND_LINE_ITEM with data drawn from PSS.TRAN_LINE_ITEM
    * Create new function PSS.SF_PIVOT_ESUDS_TRAN_LINE_ITEM based on PSS.SF_PIVOT_TRAN_LINE_ITEM but using PSS.MV_ESUDS_SETTLED_TRAN_LINE_ITEM instead of PSS.TRAN_LINE_ITEM
    * Update PKG_REPORTS (both population and retrieving) to load the two additional tables and to use the new tables in the retrieval functions 

CREATED_BY                    VARCHAR2(30) NOT NULL,
  CREATED_TS                    DATE            NOT NULL,
  LAST_UPDATED_BY               VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS               DATE            NOT NULL,  
*/
/*
SELECT '  PARTITION ' || 'MV_ESUDS_TRAN_LINE_ITEM' || '_' || to_char(n) || ' VALUES LESS THAN (' || to_char(n*1000000) || '),'
   from (select level n from dual connect by level < 11) ;
   
   select max(tran_id), min(tran_id) from pss.tran;
   PSS.TRAN_LINE_ITEM
   select Column_name || ' ' || substr('                                              ',1,30 - length(column_name)) || data_type || 
    case when data_scale is not null then '(' || NVL(to_char(data_precision), '*') || ',' || data_Scale || ')'
        when data_type <> 'DATE' then '(' || data_length || ')' end || decode(nullable, 'N', ' NOT NULL', '') "Text"
         from all_tab_columns where owner = 'PSS' and table_name = 'TRAN_LINE_ITEM';
   
*/
CREATE TABLE PSS.MV_ESUDS_SETTLED_REFUND
    (REFUND_ID                      NUMBER(20) NOT NULL,
    SETTLEMENT_BATCH_ID             NUMBER(20) NOT NULL,
    REFUND_SETTLEMENT_B_AMT         NUMBER(14,4),
    REFUND_DESC                     VARCHAR2(4000) ,
    SETTLEMENT_BATCH_TS             DATE,
    TRAN_ID                         NUMBER(20,0) NOT NULL,
    CONSTRAINT PK_MV_ESUDS_SETTLED_REFUND PRIMARY KEY (REFUND_ID,SETTLEMENT_BATCH_ID))
  PARTITION BY RANGE (TRAN_ID)
  (
  --/* FOR USADBD02:
  PARTITION MV_ESUDS_SETTLED_REFUND_1 VALUES LESS THAN (1000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_2 VALUES LESS THAN (2000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_3 VALUES LESS THAN (3000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_4 VALUES LESS THAN (4000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_5 VALUES LESS THAN (5000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_6 VALUES LESS THAN (6000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_7 VALUES LESS THAN (7000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_8 VALUES LESS THAN (8000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_9 VALUES LESS THAN (9000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_10 VALUES LESS THAN (10000000))
  /*/-- FOR USADBT03:
  PARTITION MV_ESUDS_SETTLED_REFUND_1 VALUES LESS THAN (81000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_2 VALUES LESS THAN (82000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_3 VALUES LESS THAN (83000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_4 VALUES LESS THAN (84000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_5 VALUES LESS THAN (85000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_6 VALUES LESS THAN (86000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_7 VALUES LESS THAN (87000000))
  /*/ /*-- FOR USASTT01:
  PARTITION MV_ESUDS_SETTLED_REFUND_1 VALUES LESS THAN (10000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_2 VALUES LESS THAN (20000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_3 VALUES LESS THAN (30000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_4 VALUES LESS THAN (40000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_5 VALUES LESS THAN (50000000))
  /*/ /*-- FOR USADBP: 111,602,305
  PARTITION MV_ESUDS_SETTLED_REFUND_1 VALUES LESS THAN (10000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_2 VALUES LESS THAN (20000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_3 VALUES LESS THAN (30000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_4 VALUES LESS THAN (40000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_5 VALUES LESS THAN (50000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_6 VALUES LESS THAN (60000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_7 VALUES LESS THAN (70000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_8 VALUES LESS THAN (80000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_9 VALUES LESS THAN (90000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_10 VALUES LESS THAN (100000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_11 VALUES LESS THAN (110000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_12 VALUES LESS THAN (120000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_13 VALUES LESS THAN (130000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_14 VALUES LESS THAN (140000000),
  PARTITION MV_ESUDS_SETTLED_REFUND_15 VALUES LESS THAN (150000000))
  -- */
  TABLESPACE  PSS_DATA
/

CREATE TABLE PSS.MV_ESUDS_TRAN_LINE_ITEM (
  TRAN_LINE_ITEM_ID             NUMBER(20)      NOT NULL,
  TRAN_LINE_ITEM_AMOUNT         NUMBER(7,2),
  TRAN_LINE_ITEM_TAX            NUMBER(5,2),
  TRAN_LINE_ITEM_TS             DATE,
  TRAN_LINE_ITEM_POSITION_CD    VARCHAR2(6),
  TRAN_LINE_ITEM_DESC           VARCHAR2(4000),
  TRAN_LINE_ITEM_TYPE_ID        NUMBER(20)      NOT NULL,
  TRAN_ID                       NUMBER(20)      NOT NULL,
  TRAN_LINE_ITEM_QUANTITY       NUMBER(*,0),
  HOST_ID                       NUMBER(20),
  TRAN_LINE_ITEM_BATCH_TYPE_CD  CHAR(1)    NOT NULL,
  CONSTRAINT PK_MV_ESUDS_TRAN_LINE_ITEM PRIMARY KEY (TRAN_LINE_ITEM_ID))
  PARTITION BY RANGE (TRAN_ID)
  (
  --/* FOR USADBD02:
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_1 VALUES LESS THAN (1000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_2 VALUES LESS THAN (2000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_3 VALUES LESS THAN (3000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_4 VALUES LESS THAN (4000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_5 VALUES LESS THAN (5000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_6 VALUES LESS THAN (6000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_7 VALUES LESS THAN (7000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_8 VALUES LESS THAN (8000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_9 VALUES LESS THAN (9000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_10 VALUES LESS THAN (10000000))
  /*/ /*-- FOR USADBT03:
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_1 VALUES LESS THAN (81000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_2 VALUES LESS THAN (82000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_3 VALUES LESS THAN (83000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_4 VALUES LESS THAN (84000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_5 VALUES LESS THAN (85000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_6 VALUES LESS THAN (86000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_7 VALUES LESS THAN (87000000))
  /*/ /*-- FOR USASTT01:
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_1 VALUES LESS THAN (10000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_2 VALUES LESS THAN (20000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_3 VALUES LESS THAN (30000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_4 VALUES LESS THAN (40000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_5 VALUES LESS THAN (50000000))
  /*/ /*-- FOR USADBP: 111,602,305
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_1 VALUES LESS THAN (10000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_2 VALUES LESS THAN (20000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_3 VALUES LESS THAN (30000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_4 VALUES LESS THAN (40000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_5 VALUES LESS THAN (50000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_6 VALUES LESS THAN (60000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_7 VALUES LESS THAN (70000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_8 VALUES LESS THAN (80000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_9 VALUES LESS THAN (90000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_10 VALUES LESS THAN (100000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_11 VALUES LESS THAN (110000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_12 VALUES LESS THAN (120000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_13 VALUES LESS THAN (130000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_14 VALUES LESS THAN (140000000),
  PARTITION MV_ESUDS_TRAN_LINE_ITEM_15 VALUES LESS THAN (150000000))
  -- */
  TABLESPACE  PSS_DATA
/


CREATE INDEX PSS.IDX_MV_EST_CPTC_SETTLE_TS ON PSS.MV_ESUDS_SETTLED_TRAN(CLIENT_PAYMENT_TYPE_CD,SETTLEMENT_BATCH_TS)
TABLESPACE PSS_INDX LOCAL;

CREATE INDEX PSS.IDX_MV_EST_TRAN_ID ON PSS.MV_ESUDS_SETTLED_TRAN(TRAN_ID)
TABLESPACE PSS_INDX LOCAL;

CREATE INDEX PSS.IDX_ESUDS_TLI_TRAN_ID ON PSS.MV_ESUDS_TRAN_LINE_ITEM(TRAN_ID)
TABLESPACE PSS_INDX LOCAL;

CREATE INDEX PSS.IDX_ESUDS_TLI_HOST_ID ON PSS.MV_ESUDS_TRAN_LINE_ITEM(HOST_ID)
TABLESPACE PSS_INDX LOCAL;

CREATE INDEX PSS.IDX_ESUDS_REFUND_TRAN_ID ON PSS.MV_ESUDS_SETTLED_REFUND(TRAN_ID)
TABLESPACE PSS_INDX LOCAL;

CREATE INDEX PSS.IDX_ESUDS_REFUND_REFUND_ID ON PSS.MV_ESUDS_SETTLED_REFUND(REFUND_ID)
TABLESPACE PSS_INDX LOCAL;

CREATE INDEX PSS.IDX_ESUDS_REFUND_SETTLE_TS ON PSS.MV_ESUDS_SETTLED_REFUND(SETTLEMENT_BATCH_TS)
TABLESPACE PSS_INDX LOCAL;

CREATE OR REPLACE FUNCTION PSS.SF_PIVOT_ESUDS_TRAN_LINE_ITEM (pn_tran_id IN tran.tran_id%TYPE)
   RETURN VARCHAR2
IS
   v_return_string   VARCHAR2 (4000) DEFAULT NULL;
   v_delimiter       VARCHAR2 (1)    DEFAULT NULL;
BEGIN
   FOR x IN (SELECT tran_line_item_desc
               FROM MV_ESUDS_TRAN_LINE_ITEM
              WHERE tran_id = pn_tran_id
                AND tran_line_item_batch_type_cd = 'A')
   LOOP
      v_return_string := v_return_string || v_delimiter || x.tran_line_item_desc;
      v_delimiter := CHR (13);
   END LOOP;

   RETURN v_return_string;
END;
/
