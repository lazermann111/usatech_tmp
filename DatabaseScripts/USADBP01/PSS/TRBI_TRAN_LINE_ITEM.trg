CREATE OR REPLACE TRIGGER pss.trbi_tran_line_item
BEFORE INSERT ON pss.tran_line_item
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.tran_line_item_id IS NULL
   THEN
      SELECT SEQ_TLI_ID.NEXTVAL
        INTO :NEW.tran_line_item_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/