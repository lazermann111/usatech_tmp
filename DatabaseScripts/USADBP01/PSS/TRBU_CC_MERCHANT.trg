CREATE OR REPLACE TRIGGER pss.trbu_cc_merchant
BEFORE UPDATE ON pss.cc_merchant
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/