create or replace PROCEDURE   PSS.UPDATE_PENDING_TRAN 
IS
    ln_increment PLS_INTEGER := 10000;
    ln_min_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_max_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_start_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_cnt PLS_INTEGER;
    ld_cutoff DATE := SYSDATE - 8;
BEGIN
    SELECT MIN(TRAN_ID)
      INTO ln_min_tran_id
      FROM PSS.TRAN;
    SELECT  MAX(TRAN_ID)
      INTO ln_max_tran_id
      FROM PSS.TRAN;
	SELECT NVL((SELECT /*+ index(t IX_TRAN_UPLOAD_TS) */ tran_id FROM pss.tran t WHERE tran_upload_ts BETWEEN ld_cutoff - 14 AND ld_cutoff - 7 AND ROWNUM = 1),
		ln_min_tran_id) INTO ln_min_tran_id FROM dual;
    ln_start_tran_id := ln_min_tran_id;
    WHILE ln_start_tran_id <  ln_max_tran_id LOOP
        DBMS_OUTPUT.PUT_LINE('Updating TRAN table from TRAN_ID ' || ln_start_tran_id || ' to ' || (ln_start_tran_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD = DECODE(TRAN_STATE_CD, '0', 'M', '6', 'L', PSS.PKG_SETTLEMENT.AFTER_SETTLE_TRAN_STATE_CD(
                 NVL((SELECT MAX(sa.AUTH_TYPE_CD)
                    FROM PSS.AUTH SA 
                   WHERE SA.TRAN_ID = X.TRAN_ID 
                     AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')), 
                    (SELECT MAX(AA.AUTH_TYPE_CD)
                    FROM PSS.AUTH AA 
                   WHERE AA.TRAN_ID = X.TRAN_ID 
                     AND AA.AUTH_TYPE_CD = 'N')),
                 (SELECT MAX(R.REFUND_TYPE_CD)
                    FROM PSS.REFUND R
                   WHERE R.TRAN_ID = X.TRAN_ID),
                 'O', X.TRAN_DEVICE_RESULT_TYPE_CD))
         WHERE TRAN_ID >= ln_start_tran_id AND TRAN_ID < ln_start_tran_id + ln_increment
           AND CREATED_TS < ld_cutoff
		    AND TRAN_STATE_CD IN (SELECT TRAN_STATE_CD FROM PSS.TRAN_STATE WHERE PENDING_IND = 'Y');
        ln_cnt := SQL%ROWCOUNT;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows on TRAN table from TRAN_ID ' || ln_start_tran_id || ' to ' || (ln_start_tran_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
        ln_start_tran_id := ln_start_tran_id + ln_increment;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('TRAN Update complete');
END;