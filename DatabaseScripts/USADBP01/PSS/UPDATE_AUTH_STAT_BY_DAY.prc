CREATE OR REPLACE PROCEDURE PSS.UPDATE_AUTH_STAT_BY_DAY (
	pn_auth_id PSS.AUTH.AUTH_ID%TYPE) 
IS
	lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
	lv_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
	ld_auth_ts DATE;
	ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
	lv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE;
	lv_country_cd PSS.CONSUMER_ACCT_BASE.COUNTRY_CODE%TYPE;
	ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
	lv_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE;
	lv_credit_debit_ind PSS.CONSUMER_ACCT_BASE.CREDIT_DEBIT_IND%TYPE;
	lv_prepaid_flag PSS.CONSUMER_ACCT_BASE.PREPAID_FLAG%TYPE;
BEGIN
	select t.device_name, a.auth_type_cd, trunc(a.auth_ts), ps.trans_type_id, pp.currency_cd, cab.country_code, a.auth_amt, a.auth_result_cd, cab.credit_debit_ind, cab.prepaid_flag
	into lv_device_name, lv_auth_type_cd, ld_auth_ts, ln_trans_type_id, lv_currency_cd, lv_country_cd, ln_auth_amt, lv_auth_result_cd, lv_credit_debit_ind, lv_prepaid_flag
	from pss.auth a
	join pss.tran t on t.tran_id = a.tran_id
	join pss.pos_pta pp on pp.pos_pta_id = t.pos_pta_id
	join pss.payment_subtype ps on ps.payment_subtype_id = pp.payment_subtype_id
	left outer join pss.consumer_acct_base cab on cab.consumer_acct_id = t.consumer_acct_id
	where a.auth_id = pn_auth_id;

	IF lv_auth_type_cd <> 'N' THEN
		RETURN;
	END IF;
	
	LOOP
		update pss.auth_stat_by_day set 
			auth_count = nvl(auth_count, 0) + 1,
			auth_amount = nvl(auth_amount, 0) + ln_auth_amt,
			auth_amount_s = dbadmin.welford_s(auth_count, auth_amount, auth_amount_s, ln_auth_amt),
			auth_amount_stdev = dbadmin.welford_stdev(auth_count, auth_amount, auth_amount_s, ln_auth_amt),
			auth_declined_count = auth_declined_count + decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0),
			auth_failed_count = auth_failed_count + decode(lv_auth_result_cd, 'F', 1, 0),
			credit_count = credit_count + decode(lv_credit_debit_ind, 'C', 1, 0),
			debit_count = debit_count + decode(lv_credit_debit_ind, 'D', 1, 0),
			prepaid_count = prepaid_count + decode(lv_prepaid_flag, 'Y', 1, 0)
		where device_name = lv_device_name
			and auth_date = ld_auth_ts
			and trans_type_id = ln_trans_type_id
			and currency_cd = lv_currency_cd
			and nvl(consumer_acct_coutry_code,'X') = nvl(lv_country_cd,'X');
		EXIT WHEN SQL%ROWCOUNT > 0;
		BEGIN
			insert into pss.auth_stat_by_day(
				device_name,
				auth_date,
				trans_type_id,
				currency_cd,
				consumer_acct_coutry_code,
				auth_count,
				auth_amount,
				auth_amount_s,
				auth_amount_stdev,
				auth_declined_count,
				auth_failed_count,
				credit_count,
				debit_count,
				prepaid_count)
			select
				lv_device_name,
				ld_auth_ts,
				ln_trans_type_id,
				lv_currency_cd,
				lv_country_cd,
				1,
				ln_auth_amt,
				0,
				null,
				decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0),
				decode(lv_auth_result_cd, 'F', 1, 0),
				decode(lv_credit_debit_ind, 'C', 1, 0),
				decode(lv_credit_debit_ind, 'D', 1, 0),
				decode(lv_prepaid_flag, 'Y', 1, 0)
			from dual;
      EXIT;
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;
	END LOOP;
END;
/

GRANT EXECUTE ON PSS.UPDATE_AUTH_STAT_BY_DAY TO USAT_APP_LAYER_ROLE, USAT_LOADER_ROLE;
