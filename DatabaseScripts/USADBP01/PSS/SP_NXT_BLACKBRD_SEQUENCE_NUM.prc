CREATE OR REPLACE PROCEDURE PSS.SP_NXT_BLACKBRD_SEQUENCE_NUM (
   pn_blackbrd_authority_id	IN       pss.blackbrd_authority.blackbrd_authority_id%TYPE,
   pn_nxt_seq_num     		OUT      pss.blackbrd_authority.blackbrd_authority_seq_num%TYPE,
   pn_return_code     		OUT      exception_code.exception_code_id%TYPE,
   pv_error_message   		OUT      exception_data.additional_information%TYPE
) AS
--
-- Purpose: Gets next sequence num from pss.blackbrd_authority
-- for a blackboard authority
--
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ---------    ------      ---------------------------------------
-- erybski      04.09.16    Initial release.
-- erybski      04.09.27    Updated variable names to reflect content meaning change
-- erybski      06.07.21    Updated to set random initial sequence num
-- dkouznetsov  08.03.24    Moved sequence numbers into pss.blackbrd_authority table
--
   cv_package_name              CONSTANT VARCHAR2 (30)		:= '';
   cv_procedure_name            CONSTANT VARCHAR2 (30)      := 'SP_NXT_BLACKBRD_SEQ_NUM';
   e_device_id_notfound         EXCEPTION;

   CURSOR c_seq_num IS
      SELECT blackbrd_authority_seq_num
            FROM pss.blackbrd_authority
           WHERE blackbrd_authority_id = pn_blackbrd_authority_id
      FOR UPDATE;

   r_seq_num c_seq_num%ROWTYPE;
BEGIN
   pn_nxt_seq_num := NULL;

   FOR r_seq_num IN c_seq_num LOOP
      pn_nxt_seq_num := NVL(r_seq_num.blackbrd_authority_seq_num, 0);

      IF pn_nxt_seq_num = 999999 THEN
         pn_nxt_seq_num := 1;
      ELSE
         pn_nxt_seq_num := pn_nxt_seq_num + 1;
      END IF;

      UPDATE pss.blackbrd_authority
      SET blackbrd_authority_seq_num = pn_nxt_seq_num
      WHERE CURRENT OF c_seq_num;
   END LOOP;
   
   COMMIT;

   IF pn_nxt_seq_num IS NULL THEN
      RAISE e_device_id_notfound;
   END IF;
EXCEPTION
   WHEN e_device_id_notfound THEN
      pv_error_message :=
         'Could not find the following blackbrd_authority_id in the database: ' || pn_blackbrd_authority_id;
      pn_return_code := pkg_app_exec_hist_globals.device_id_not_found;
      pkg_exception_processor.sp_log_exception (pn_return_code,
                                                pv_error_message,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
      pn_nxt_seq_num := NULL;
   WHEN OTHERS THEN
      pv_error_message :=
         'An unknown exception occurred in ' || cv_procedure_name || ' = ' || SQLCODE || ', '
         || SQLERRM;
      pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
      pkg_exception_processor.sp_log_exception (pn_return_code,
                                                pv_error_message,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
      pn_nxt_seq_num := NULL;
END;

/

GRANT EXECUTE ON PSS.SP_NXT_BLACKBRD_SEQUENCE_NUM TO web_user;