CREATE OR REPLACE TRIGGER PSS.TRBI_CONSUMER_ACCT_FMT
BEFORE INSERT ON PSS.CONSUMER_ACCT_FMT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.consumer_acct_fmt_id IS NULL
   THEN
      SELECT SEQ_CONSUMER_ACCT_FMT_ID.NEXTVAL
        INTO :NEW.consumer_acct_fmt_id
        FROM DUAL;
   END IF;
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/