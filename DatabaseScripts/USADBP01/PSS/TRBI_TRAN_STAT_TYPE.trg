CREATE OR REPLACE TRIGGER PSS.TRBI_TRAN_STAT_TYPE
BEFORE INSERT ON PSS.TRAN_STAT_TYPE
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    IF :NEW.TRAN_STAT_TYPE_ID IS NULL
    THEN
        SELECT SEQ_TRAN_STAT_TYPE_ID.NEXTVAL
        INTO :NEW.TRAN_STAT_TYPE_ID
        FROM DUAL;
    END IF;
    SELECT
        SYSDATE,
        USER,
        SYSDATE,
        USER
    INTO
        :NEW.created_ts,
        :NEW.created_by,
        :NEW.last_updated_ts,
        :NEW.last_updated_by
    FROM DUAL;
END;
/
