CREATE OR REPLACE TRIGGER PSS.TRAU_POS 
AFTER UPDATE ON PSS.POS
FOR EACH ROW
BEGIN
    IF DBADMIN.PKG_UTL.EQL(:NEW.LOCATION_ID, :OLD.LOCATION_ID) = 'N' THEN
        DECLARE
            CURSOR l_cur IS
                SELECT MAX(DEVICE_ID) DEVICE_ID, DEVICE_NAME, DEVICE_INFO_FLAG, DEVICE_DETAIL_FLAG
                  FROM (
                SELECT D.DEVICE_ID,
                       D.DEVICE_NAME,
                       CASE WHEN T.TIME_ZONE_ID IS NULL AND DBADMIN.PKG_UTL.EQL(L_O.LOCATION_TIME_ZONE_CD, L_N.LOCATION_TIME_ZONE_CD) = 'N' THEN 'Y' ELSE 'N' END DEVICE_INFO_FLAG,
                       CASE WHEN TA.ADDRESS_ID IS NULL AND 
                                (DBADMIN.PKG_UTL.EQL(L_O.LOCATION_ADDR1, L_N.LOCATION_ADDR1) = 'N' 
                              OR DBADMIN.PKG_UTL.EQL(L_O.LOCATION_CITY, L_N.LOCATION_CITY) = 'N'
                              OR DBADMIN.PKG_UTL.EQL(L_O.LOCATION_STATE_CD, L_N.LOCATION_STATE_CD) = 'N'
                              OR DBADMIN.PKG_UTL.EQL(L_O.LOCATION_POSTAL_CD, L_N.LOCATION_POSTAL_CD) = 'N'
                              OR DBADMIN.PKG_UTL.EQL(L_O.LOCATION_COUNTRY_CD, L_N.LOCATION_COUNTRY_CD) = 'N') THEN 'Y' ELSE 'N' END DEVICE_DETAIL_FLAG
                  FROM DEVICE.DEVICE D
                  LEFT OUTER JOIN LOCATION.LOCATION L_O ON L_O.LOCATION_ID = :OLD.LOCATION_ID
                  LEFT OUTER JOIN LOCATION.LOCATION L_N ON L_N.LOCATION_ID = :NEW.LOCATION_ID
                  LEFT OUTER JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
                  LEFT OUTER JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
                  LEFT OUTER JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID	    
                  LEFT OUTER JOIN REPORT.LOCATION TL ON T.LOCATION_ID = TL.LOCATION_ID
                  LEFT OUTER JOIN REPORT.TERMINAL_ADDR TA ON TL.ADDRESS_ID = TA.ADDRESS_ID 
                 WHERE D.DEVICE_ID IN(:NEW.DEVICE_ID, :OLD.DEVICE_ID)
                   AND D.DEVICE_ACTIVE_YN_FLAG = 'Y')
                 WHERE DEVICE_INFO_FLAG = 'Y'
                    OR DEVICE_DETAIL_FLAG = 'Y'
                GROUP BY DEVICE_NAME, DEVICE_INFO_FLAG, DEVICE_DETAIL_FLAG;
        BEGIN   
            FOR l_rec IN l_cur LOOP
                IF l_rec.DEVICE_INFO_FLAG = 'Y' THEN
                    ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
                        'DEVICE_INFO',
                        'PSS.POS',
                        'U',
                        l_rec.DEVICE_ID,
                        l_rec.DEVICE_NAME);
                END IF;
                IF l_rec.DEVICE_DETAIL_FLAG = 'Y' THEN
                    ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
                        'DEVICE_DETAIL',
                        'PSS.POS',
                        'U',
                        l_rec.DEVICE_ID,
                        l_rec.DEVICE_NAME);
                END IF;
                
            END LOOP;
        END;
    END IF;    
		
END;
/
