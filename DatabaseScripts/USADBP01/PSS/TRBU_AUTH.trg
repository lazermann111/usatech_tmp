CREATE OR REPLACE TRIGGER pss.trbu_auth
BEFORE UPDATE ON pss.auth
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
	
	IF :NEW.auth_type_cd = 'L' THEN
		:NEW.auth_state_id := 4;
		:NEW.auth_result_cd := 'F';
	END IF;	

	IF :NEW.auth_parsed_acct_data IS NOT NULL THEN
		IF :NEW.auth_type_cd = 'L' OR :NEW.auth_type_cd = 'N' AND :NEW.auth_state_id IN (3, 4, 7) THEN
			:NEW.auth_parsed_acct_data := DBADMIN.MASK_CREDIT_CARD(:NEW.auth_parsed_acct_data);
		ELSIF :OLD.auth_parsed_acct_data IS NULL OR :OLD.auth_parsed_acct_data <> :NEW.auth_parsed_acct_data THEN
			:NEW.auth_parsed_acct_data := DBADMIN.GET_AUTHED_CARD(:NEW.auth_parsed_acct_data);
		END IF;
	END IF;
	
	IF :OLD.auth_state_id <> :NEW.auth_state_id THEN
		:NEW.previous_auth_state_id := :OLD.auth_state_id;
	END IF;
END;
/