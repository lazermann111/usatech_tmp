CREATE OR REPLACE FUNCTION PSS.CALC_LUHN (
    accountNumberWithoutCheckDigit IN VARCHAR2
)
RETURN VARCHAR2 IS
    
    n         NUMBER         := 0;
    len       NUMBER         := 0;
    temp      NUMBER         := 0;
    checksum  NUMBER         := 0;

BEGIN

    len := LENGTH(accountNumberWithoutCheckDigit);

    FOR i IN 1 .. len LOOP
    
        n := to_number(substr(accountNumberWithoutCheckDigit, (len-i+1), 1));
        temp := n * (1 + mod(i,2));
        
        if (temp < 10) THEN
            checksum := checksum + temp;
        else
            checksum :=  checksum + (temp - 9);
        end if;
        
    END LOOP;
    
    checksum := mod((10 - mod(checksum,10)), 10);

    RETURN (checksum);
END;
/