CREATE OR REPLACE TRIGGER pss.trbi_cc_payment_type
BEFORE INSERT ON pss.cc_payment_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

    IF :new.CC_payment_type_id IS NULL THEN

      SELECT seq_CC_payment_type_id.nextval
        into :new.CC_payment_type_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/