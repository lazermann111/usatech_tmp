CREATE OR REPLACE TRIGGER pss.trbi_consumer
BEFORE INSERT ON pss.consumer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.consumer_id IS NULL THEN

      SELECT seq_consumer_id.nextval
        into :new.consumer_id
        FROM dual;

    END IF;

 SELECT UPPER(:new.consumer_country_cd),
           UPPER(:new.consumer_state_cd),
           sysdate,
           user,
           sysdate,
           user
      into :new.consumer_country_cd,
           :new.consumer_state_cd,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/