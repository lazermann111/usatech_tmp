-- This script will create Credit and Special Error Bin payment types for devices that don't already have them
DECLARE
	ln_pos_pta_tmpl_id pss.pos_pta_tmpl.pos_pta_tmpl_id%TYPE;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    CURSOR cur_credit IS 
        select dla.device_id from device.vw_device_last_active dla
		join pss.pos p on dla.device_id = p.device_id
		where dla.device_type_id in (0, 1, 5, 6, 11) and not exists (
			select 1 from pss.pos_pta pp
			join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id
			where pp.pos_id = p.pos_id and ps.payment_subtype_name = 'Credit Card Error Bin'
				and pp.pos_pta_deactivation_ts is null
		);
    CURSOR cur_special IS 
        select dla.device_id from device.vw_device_last_active dla
		join pss.pos p on dla.device_id = p.device_id
		where dla.device_type_id in (0, 1, 5, 6, 11) and not exists (
			select 1 from pss.pos_pta pp
			join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id
			where pp.pos_id = p.pos_id and ps.payment_subtype_name = 'Special Card - Error Bin'
				and pp.pos_pta_deactivation_ts is null
		);
BEGIN
	select pos_pta_tmpl_id into ln_pos_pta_tmpl_id
	from pss.pos_pta_tmpl where pos_pta_tmpl_name = 'CREDIT MERGE: Credit Card Error Bin';

    DBMS_OUTPUT.put_line('Credit Card Error Bin script is starting...');
	
    FOR rec_cur IN cur_credit LOOP
		ln_result_cd := PKG_CONST.RESULT__FAILURE;
		lv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
       
        pss.pkg_pos_pta.sp_import_pos_pta_template(rec_cur.device_id, ln_pos_pta_tmpl_id, 'MS', ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            DBMS_OUTPUT.put_line('Credit Card Error Bin template import failed, device_id: ' || rec_cur.device_id || ', result_cd: ' || ln_result_cd || ', error_message: ' || lv_error_message);
        ELSE
            DBMS_OUTPUT.put_line('Credit Card Error Bin template import succeeded, device_id: ' || rec_cur.device_id);
        END IF;
		COMMIT;
    END LOOP;
	
	DBMS_OUTPUT.put_line('Credit Card Error Bin script finished.');
	
	select pos_pta_tmpl_id into ln_pos_pta_tmpl_id
	from pss.pos_pta_tmpl where pos_pta_tmpl_name = 'SPECIAL MERGE: Special Card Error Bin';
	
    DBMS_OUTPUT.put_line('Special Card Error Bin script is starting...');
	
    FOR rec_cur IN cur_special LOOP
		ln_result_cd := PKG_CONST.RESULT__FAILURE;
		lv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
       
        pss.pkg_pos_pta.sp_import_pos_pta_template(rec_cur.device_id, ln_pos_pta_tmpl_id, 'MS', ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            DBMS_OUTPUT.put_line('Special Card Error Bin template import failed, device_id: ' || rec_cur.device_id || ', result_cd: ' || ln_result_cd || ', error_message: ' || lv_error_message);
        ELSE
            DBMS_OUTPUT.put_line('Special Card Error Bin template import succeeded, device_id: ' || rec_cur.device_id);
        END IF;
		COMMIT;
    END LOOP;
	
	DBMS_OUTPUT.put_line('Special Card Error Bin script finished.');
END;
/
