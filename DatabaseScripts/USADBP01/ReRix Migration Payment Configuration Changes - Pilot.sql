DECLARE
	-- if ln_pilot is set to 1 it means that the script is used only for pilot devices, payment templates will not be updated
	-- set ln_pilot to 0 to update all payment templates and deprecate old credit payment types
	ln_pilot NUMBER := 1;

    CURSOR cur_devices IS
		SELECT DEVICE_ID
		FROM DEVICE.VW_DEVICE_LAST_ACTIVE
		-- put a comma separated list of pilot device names in single quotes (i.e. 'EV000001', 'TD000001') in parenthesis below
		-- or comment out the WHERE clause below to update payment configuration of all devices
		WHERE DEVICE_NAME IN (
			'EV047270',
			'EV047531',
			'EV047566',
			'EV035222',
			'EV035221',
			'EV062223',
			'EV083058',
			'EV083075',
			'EV157925',
			'EV142446',
			'EV142514',
			'EV144415',
			'EV144420',
			'EV144423',
			'EV144428',
			'EV078064',
			'EV078889',
			'EV123514',
			'EV144086',
			'EV154968',
			'EV132120',
			'EV134307',
			'EV132100',
			'EV140409',
			'EV142080',
			'EV140589',
			'EV145576',
			'EV145573',
			'EV065197',
			'EV133003',
			'EV064753',
			'EV065189'
		)
		ORDER BY DEVICE_ID;
		
	lc_payment_entry_method_cd PSS.CLIENT_PAYMENT_TYPE.PAYMENT_ENTRY_METHOD_CD%TYPE;
	ln_pos_pta_priority PSS.POS_PTA.POS_PTA_PRIORITY%TYPE;
		
	CURSOR cur_device_ptas(pn_device_id IN DEVICE.DEVICE_ID%TYPE) IS
		SELECT PP.*,
			PS.PAYMENT_SUBTYPE_NAME,
			PS.CLIENT_PAYMENT_TYPE_CD,
			CPT.PAYMENT_ENTRY_METHOD_CD,
			CASE WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Visa%' THEN 1
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%MasterCard%' THEN 2
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%American Express%' THEN 3
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Discover%' THEN 4
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Gift/Stored Value Card' THEN 5
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Operator Maintenace Card' THEN 6

				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - eSuds Passcard' THEN 901
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE 'Special Card - USA Tech (Track%' THEN 902
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - USA Tech Local Auth Passcard' THEN 903
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - Aramark Generic' THEN 904
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE 'Special Card - Blackboard Generic%' THEN 905
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - eSuds Generic' THEN 906
			
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Special Card%Generic%' THEN 1001
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Blacklist%' THEN 1002
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Credit%Deactivation%Bin%' THEN 1003
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Special%Deactivation%Bin%' THEN 1004
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Credit%Error%Bin%' THEN 1005
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Special%Error%Bin%' THEN 1006
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Error%' THEN 1007
				ELSE 100
			END PRIORITY
		FROM DEVICE.DEVICE D
		JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
		JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
		JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
		JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
		WHERE D.DEVICE_ID = pn_device_id
			AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
			AND PS.CLIENT_PAYMENT_TYPE_CD <> 'M'
		ORDER BY CPT.PAYMENT_ENTRY_METHOD_CD DESC, PRIORITY, PS.PAYMENT_SUBTYPE_NAME;
	
    CURSOR cur_pta_templates IS
		SELECT POS_PTA_TMPL_ID
		FROM PSS.POS_PTA_TMPL
		ORDER BY POS_PTA_TMPL_ID;
		
	CURSOR cur_template_ptas(pn_pos_pta_tmpl_id IN PSS.POS_PTA_TMPL.POS_PTA_TMPL_ID%TYPE) IS
		SELECT PPTE.*,
			PS.PAYMENT_SUBTYPE_NAME,
			PS.CLIENT_PAYMENT_TYPE_CD,
			CPT.PAYMENT_ENTRY_METHOD_CD,
			CASE WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Visa%' THEN 1
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%MasterCard%' THEN 2
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%American Express%' THEN 3
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Discover%' THEN 4
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Gift/Stored Value Card' THEN 5
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Operator Maintenace Card' THEN 6

				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - eSuds Passcard' THEN 901
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE 'Special Card - USA Tech (Track%' THEN 902
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - USA Tech Local Auth Passcard' THEN 903
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - Aramark Generic' THEN 904
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE 'Special Card - Blackboard Generic%' THEN 905
				WHEN PS.PAYMENT_SUBTYPE_NAME = 'Special Card - eSuds Generic' THEN 906
			
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Special Card%Generic%' THEN 1001
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Blacklist%' THEN 1002
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Credit%Deactivation%Bin%' THEN 1003
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Special%Deactivation%Bin%' THEN 1004
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Credit%Error%Bin%' THEN 1005
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Special%Error%Bin%' THEN 1006
				WHEN PS.PAYMENT_SUBTYPE_NAME LIKE '%Error%' THEN 1007
				ELSE 100
			END PRIORITY
		FROM PSS.POS_PTA_TMPL_ENTRY PPTE
		JOIN PSS.PAYMENT_SUBTYPE PS ON PPTE.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
		JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
		WHERE PPTE.POS_PTA_TMPL_ID = pn_pos_pta_tmpl_id
			AND PS.CLIENT_PAYMENT_TYPE_CD <> 'M'
		ORDER BY CPT.PAYMENT_ENTRY_METHOD_CD DESC, PRIORITY, PS.PAYMENT_SUBTYPE_NAME;

	PROCEDURE UPDATE_CREDIT_PTAS (
		rec_pta cur_device_ptas%ROWTYPE,
		pv_payment_subtype_mask VARCHAR2
	)
	IS
	BEGIN
		INSERT INTO PSS.POS_PTA(
			POS_ID, 
			PAYMENT_SUBTYPE_ID, 
			POS_PTA_ENCRYPT_KEY,
			POS_PTA_DEACTIVATION_TS, 
			PAYMENT_SUBTYPE_KEY_ID, 
			POS_PTA_REGEX,
			POS_PTA_REGEX_BREF,
			POS_PTA_ACTIVATION_TS, 
			POS_PTA_DEVICE_SERIAL_CD,
			POS_PTA_PIN_REQ_YN_FLAG,
			POS_PTA_ENCRYPT_KEY2,
			AUTHORITY_PAYMENT_MASK_ID, 
			POS_PTA_PRIORITY, 
			TERMINAL_ID,
			MERCHANT_BANK_ACCT_ID,
			CURRENCY_CD, 
			POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
			POS_PTA_PREF_AUTH_AMT, 
			POS_PTA_PREF_AUTH_AMT_MAX
		)
		SELECT rec_pta.POS_ID, 
			PAYMENT_SUBTYPE_ID, 
			rec_pta.POS_PTA_ENCRYPT_KEY,
			rec_pta.POS_PTA_DEACTIVATION_TS, 
			rec_pta.PAYMENT_SUBTYPE_KEY_ID, 
			rec_pta.POS_PTA_REGEX,
			rec_pta.POS_PTA_REGEX_BREF,
			rec_pta.POS_PTA_ACTIVATION_TS, 
			rec_pta.POS_PTA_DEVICE_SERIAL_CD,
			rec_pta.POS_PTA_PIN_REQ_YN_FLAG,
			rec_pta.POS_PTA_ENCRYPT_KEY2,
			NULL, 
			rec_pta.POS_PTA_PRIORITY, 
			rec_pta.TERMINAL_ID,
			rec_pta.MERCHANT_BANK_ACCT_ID,
			rec_pta.CURRENCY_CD, 
			rec_pta.POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
			rec_pta.POS_PTA_PREF_AUTH_AMT, 
			rec_pta.POS_PTA_PREF_AUTH_AMT_MAX
		FROM PSS.PAYMENT_SUBTYPE PS
		WHERE PAYMENT_SUBTYPE_NAME LIKE pv_payment_subtype_mask
			AND PAYMENT_SUBTYPE_NAME NOT LIKE '%Diners Club%'
			AND CLIENT_PAYMENT_TYPE_CD = rec_pta.client_payment_type_cd
			AND NOT EXISTS (
				SELECT 1 FROM PSS.POS_PTA
				WHERE POS_ID = rec_pta.POS_ID
					AND PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
					AND (POS_PTA_DEACTIVATION_TS IS NULL OR POS_PTA_DEACTIVATION_TS > SYSDATE)
			);
		
		UPDATE PSS.POS_PTA SET POS_PTA_DEACTIVATION_TS = SYSDATE WHERE POS_PTA_ID = rec_pta.pos_pta_id;
	END;

	PROCEDURE UPDATE_TMPL_CREDIT_PTAS (
		rec_tmpl_pta cur_template_ptas%ROWTYPE,
		pv_payment_subtype_mask VARCHAR2
	)
	IS
	BEGIN
		INSERT INTO PSS.POS_PTA_TMPL_ENTRY(
			POS_PTA_TMPL_ID, 
			PAYMENT_SUBTYPE_ID, 
			POS_PTA_ENCRYPT_KEY,
			POS_PTA_ACTIVATION_OSET_HR,
			POS_PTA_DEACTIVATION_OSET_HR,
			PAYMENT_SUBTYPE_KEY_ID, 
			POS_PTA_PIN_REQ_YN_FLAG,
			POS_PTA_DEVICE_SERIAL_CD,			
			POS_PTA_ENCRYPT_KEY2,
			AUTHORITY_PAYMENT_MASK_ID, 
			POS_PTA_PRIORITY, 
			TERMINAL_ID,
			MERCHANT_BANK_ACCT_ID,
			CURRENCY_CD, 
			POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
			POS_PTA_PREF_AUTH_AMT, 
			POS_PTA_PREF_AUTH_AMT_MAX
		)
		SELECT rec_tmpl_pta.POS_PTA_TMPL_ID, 
			PAYMENT_SUBTYPE_ID, 
			rec_tmpl_pta.POS_PTA_ENCRYPT_KEY,
			rec_tmpl_pta.POS_PTA_ACTIVATION_OSET_HR,
			rec_tmpl_pta.POS_PTA_DEACTIVATION_OSET_HR,
			rec_tmpl_pta.PAYMENT_SUBTYPE_KEY_ID, 
			rec_tmpl_pta.POS_PTA_PIN_REQ_YN_FLAG,
			rec_tmpl_pta.POS_PTA_DEVICE_SERIAL_CD,
			rec_tmpl_pta.POS_PTA_ENCRYPT_KEY2,
			NULL, 
			rec_tmpl_pta.POS_PTA_PRIORITY, 
			rec_tmpl_pta.TERMINAL_ID,
			rec_tmpl_pta.MERCHANT_BANK_ACCT_ID,
			rec_tmpl_pta.CURRENCY_CD, 
			rec_tmpl_pta.POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
			rec_tmpl_pta.POS_PTA_PREF_AUTH_AMT, 
			rec_tmpl_pta.POS_PTA_PREF_AUTH_AMT_MAX
		FROM PSS.PAYMENT_SUBTYPE PS
		WHERE PAYMENT_SUBTYPE_NAME LIKE pv_payment_subtype_mask
			AND PAYMENT_SUBTYPE_NAME NOT LIKE '%Diners Club%'
			AND CLIENT_PAYMENT_TYPE_CD = rec_tmpl_pta.client_payment_type_cd
			AND NOT EXISTS (
				SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY
				WHERE POS_PTA_TMPL_ID = rec_tmpl_pta.POS_PTA_TMPL_ID
					AND PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
			);
		
		DELETE FROM PSS.POS_PTA_TMPL_ENTRY WHERE POS_PTA_TMPL_ENTRY_ID = rec_tmpl_pta.pos_pta_tmpl_entry_id;
	END;
BEGIN
    DBMS_OUTPUT.put_line('Script is starting...');
    
    FOR rec_device IN cur_devices LOOP
		DBMS_OUTPUT.put_line('Processing device_id ' || rec_device.device_id || '...');
	
		FOR rec_pta IN cur_device_ptas(rec_device.device_id) LOOP
			IF rec_pta.payment_subtype_name LIKE '%Track 1%' OR rec_pta.payment_subtype_name LIKE '%Diners Club%' THEN
				UPDATE PSS.POS_PTA SET POS_PTA_DEACTIVATION_TS = SYSDATE WHERE POS_PTA_ID = rec_pta.pos_pta_id;
			ELSIF rec_pta.payment_subtype_name LIKE '%Credit Card (Track 2) - Paymentech%' THEN
				UPDATE_CREDIT_PTAS(rec_pta, '%(Track 2) - Canada');
			ELSIF rec_pta.payment_subtype_name LIKE '%Credit Card (Track 2)%' THEN
				UPDATE_CREDIT_PTAS(rec_pta, '%(Track 2) - USA');
			END IF;
		END LOOP;
		
		lc_payment_entry_method_cd := 'X';
		ln_pos_pta_priority := 0;
		FOR rec_pta IN cur_device_ptas(rec_device.device_id) LOOP
			IF lc_payment_entry_method_cd <> rec_pta.payment_entry_method_cd THEN
				ln_pos_pta_priority := 1;
				lc_payment_entry_method_cd := rec_pta.payment_entry_method_cd;
			ELSE
				ln_pos_pta_priority := ln_pos_pta_priority + 1;
			END IF;
			
			UPDATE PSS.POS_PTA
			SET POS_PTA_PRIORITY = ln_pos_pta_priority
			WHERE POS_PTA_ID = rec_pta.pos_pta_id;
		END LOOP;		
		
		COMMIT;
    END LOOP;
	
	IF ln_pilot = 0 THEN
		FOR rec_pta_template IN cur_pta_templates LOOP
			DBMS_OUTPUT.put_line('Processing pos_pta_tmpl_id ' || rec_pta_template.pos_pta_tmpl_id || '...');
			
			FOR rec_tmpl_pta IN cur_template_ptas(rec_pta_template.pos_pta_tmpl_id) LOOP
				IF rec_tmpl_pta.payment_subtype_name LIKE '%Track 1%' OR rec_tmpl_pta.payment_subtype_name LIKE '%Diners Club%' THEN
					DELETE FROM PSS.POS_PTA_TMPL_ENTRY WHERE POS_PTA_TMPL_ENTRY_ID = rec_tmpl_pta.pos_pta_tmpl_entry_id;
				ELSIF rec_tmpl_pta.payment_subtype_name LIKE '%Credit Card (Track 2) - Paymentech%' THEN
					UPDATE_TMPL_CREDIT_PTAS(rec_tmpl_pta, '%(Track 2) - Canada');
				ELSIF rec_tmpl_pta.payment_subtype_name LIKE '%Credit Card (Track 2)%' THEN
					UPDATE_TMPL_CREDIT_PTAS(rec_tmpl_pta, '%(Track 2) - USA');
				END IF;
			END LOOP;
			
			lc_payment_entry_method_cd := 'X';
			ln_pos_pta_priority := 0;
			FOR rec_tmpl_pta IN cur_template_ptas(rec_pta_template.pos_pta_tmpl_id) LOOP
				IF lc_payment_entry_method_cd <> rec_tmpl_pta.payment_entry_method_cd THEN
					ln_pos_pta_priority := 1;
					lc_payment_entry_method_cd := rec_tmpl_pta.payment_entry_method_cd;
				ELSE
					ln_pos_pta_priority := ln_pos_pta_priority + 1;
				END IF;
				
				UPDATE PSS.POS_PTA_TMPL_ENTRY
				SET POS_PTA_PRIORITY = ln_pos_pta_priority
				WHERE POS_PTA_TMPL_ENTRY_ID = rec_tmpl_pta.pos_pta_tmpl_entry_id;
			END LOOP;		
			
			COMMIT;			
		END LOOP;
		
		UPDATE PSS.PAYMENT_SUBTYPE SET PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Operator Maintenance Card' WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Operator Maintenace Card';
		UPDATE PSS.PAYMENT_SUBTYPE SET PAYMENT_SUBTYPE_NAME = PAYMENT_SUBTYPE_NAME || ' - Deprecated' WHERE PAYMENT_SUBTYPE_NAME LIKE '%Credit Card (Track%' AND PAYMENT_SUBTYPE_NAME NOT LIKE '%Aramark%' AND PAYMENT_SUBTYPE_NAME NOT LIKE '% - Deprecated';
		UPDATE PSS.AUTHORITY_PAYMENT_MASK SET AUTHORITY_PAYMENT_MASK_NAME = AUTHORITY_PAYMENT_MASK_NAME || ' - Deprecated', AUTHORITY_PAYMENT_MASK_DESC = AUTHORITY_PAYMENT_MASK_DESC || ' - Deprecated' WHERE AUTHORITY_PAYMENT_MASK_DESC LIKE '%Credit Card (Track%' AND AUTHORITY_PAYMENT_MASK_DESC NOT LIKE '%Aramark%' AND AUTHORITY_PAYMENT_MASK_DESC NOT LIKE '% - Deprecated';
		
		update pss.pos_pta set authority_payment_mask_id = null where payment_subtype_id in
		(select payment_subtype_id from pss.payment_subtype where payment_subtype_name like '%(Track 2) - USA' or payment_subtype_name like '%(Track 2) - Canada')
		and authority_payment_mask_id is not null;

		update pss.pos_pta_tmpl_entry set authority_payment_mask_id = null where payment_subtype_id in
		(select payment_subtype_id from pss.payment_subtype where payment_subtype_name like '%(Track 2) - USA' or payment_subtype_name like '%(Track 2) - Canada')
		and authority_payment_mask_id is not null;		
		
		COMMIT;		
	END IF;
	
	DBMS_OUTPUT.put_line('Script finished.');
END;
/
