-- This script will disable the follow-behind sale for G4 and Gx devices
DECLARE
    i_debug INT := 0;
    return_code NUMBER(20);
    return_msg VARCHAR2(2048);
	n_changed NUMBER := 0;
	n_unchanged NUMBER := 0;
	n_failed NUMBER := 0;
    CURSOR cur IS 
		SELECT DISTINCT D.DEVICE_ID, D.DEVICE_TYPE_ID
		  FROM DEVICE.FILE_TRANSFER FT 
		  JOIN DEVICE.VW_DEVICE_LAST_ACTIVE D ON FT.FILE_TRANSFER_NAME = D.DEVICE_NAME || '-CFG'
		  JOIN DEVICE.DEVICE_SETTING DS ON D.DEVICE_ID = DS.DEVICE_ID AND DS.DEVICE_SETTING_PARAMETER_CD = 'Firmware Version'
		 WHERE D.DEVICE_TYPE_ID IN (1) 
		   AND FT.FILE_TRANSFER_TYPE_CD = 1
		   AND DS.DEVICE_SETTING_VALUE IN('USA-Gx1v6.0.1PT7')
		   AND DEVICE.FILE_CONTENT_SUBSTR(FT.ROWID, 360, 1) != '55';
BEGIN
    DBMS_OUTPUT.put_line('Script is starting, debug: ' || i_debug || '...');
    
    FOR rec_cur IN cur LOOP
        return_code := -2;
        return_msg := 'Did not run.';
       
        sp_update_config(rec_cur.device_id, 360, '55', 'H', rec_cur.device_type_id, i_debug, return_code, return_msg);
		COMMIT;
		
		IF return_code = 0 THEN
			n_unchanged := n_unchanged + 1;
            --DBMS_OUTPUT.put_line('Follow-behind sale was already enabled for device_id: ' || rec_cur.device_id);
		ELSIF return_code = 1 THEN
			n_changed := n_changed + 1;
            --DBMS_OUTPUT.put_line('Follow-behind sale has been enabled for device_id: ' || rec_cur.device_id);
        ELSE
			n_failed := n_failed + 1;
            --DBMS_OUTPUT.put_line('Enabling follow-behind sale failed for device_id: ' || rec_cur.device_id || ', error: ' || return_msg);
        END IF;
    END LOOP;
	
	DBMS_OUTPUT.put_line('Script finished, device counts are Changed: ' || n_changed || ', Unchanged: ' || n_unchanged || ', Failed: ' || n_failed);
	COMMIT;
END;
