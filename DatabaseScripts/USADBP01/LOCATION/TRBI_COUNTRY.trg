CREATE OR REPLACE TRIGGER location.trbi_country
BEFORE INSERT ON location.country
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
    SELECT UPPER(:new.country_cd),
           sysdate,
           user,
           sysdate,
           user
      into :new.country_cd,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by from dual;
End; 
/