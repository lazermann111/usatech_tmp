CREATE OR REPLACE TRIGGER location.trbi_contact
BEFORE INSERT ON location.contact
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
    IF :new.contact_id IS NULL then
      SELECT seq_contact_id.nextval
      into :new.contact_id
      FROM DUAL;
    END IF;

  SELECT UPPER(:new.contact_country_cd),
           UPPER(:new.contact_state_cd),
           sysdate,
           user,
           sysdate,
           user
      into :new.contact_country_cd,
           :new.contact_state_cd,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/