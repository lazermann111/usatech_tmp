CREATE OR REPLACE TRIGGER location.trbu_country
BEFORE UPDATE ON location.country
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
     SELECT UPPER(:new.country_cd),
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into :new.country_cd,
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End; 
/