CREATE OR REPLACE PROCEDURE LOCATION.GET_LOCATION_HIERARCHY (
    l_bottom_location_id LOCATION.LOCATION_ID%TYPE,
    l_top_location_ids NUMBER_TABLE,
    l_cur OUT EXTENSIONS.REF_CURSOR
)
AS
  l_location_ids NUMBER_TABLE := NUMBER_TABLE();
  i PLS_INTEGER := 1;
BEGIN
    l_location_ids.EXTEND;
    l_location_ids(i) := l_bottom_location_id;
    IF NOT l_location_ids(i) MEMBER OF l_top_location_ids THEN
        LOOP
            i := i + 1;
            l_location_ids.EXTEND;
            BEGIN
                SELECT PARENT_LOCATION_ID
                  INTO l_location_ids(i)
                  FROM LOCATION
                 WHERE LOCATION_ID = l_location_ids(i-1)
                   AND LOCATION_ACTIVE_YN_FLAG = 'Y';
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     --return ref cursor to
                     OPEN l_cur FOR
                     SELECT DISTINCT l.LOCATION_ID, l.LOCATION_NAME,
                            REGEXP_REPLACE(l.LOCATION_IMAGE_URL, '^[.]{2}[/][.]{2}[/]', '../'),
                            l.PARENT_LOCATION_ID, h.DEPTH + 1, CASE WHEN l_bottom_location_id = l.LOCATION_ID THEN 'Y' ELSE 'N' END, l.LOCATION_TYPE_ID
                       FROM LOCATION l, VW_LOCATION_HIERARCHY h
                      WHERE h.DESCENDENT_LOCATION_ID MEMBER OF l_top_location_ids
                        AND h.ANCESTOR_LOCATION_ID = l.LOCATION_ID
                        AND l.LOCATION_ACTIVE_YN_FLAG = 'Y'
                      ORDER BY h.DEPTH + 1 DESC, l.LOCATION_TYPE_ID, l.LOCATION_NAME;
                      EXIT;
            END;
            EXIT WHEN l_location_ids(i) MEMBER OF l_top_location_ids;
        END LOOP;
    END IF;
    IF l_cur IS NULL THEN
        OPEN l_cur FOR
        SELECT l.LOCATION_ID, l.LOCATION_NAME, REGEXP_REPLACE(l.LOCATION_IMAGE_URL, '^[.]{2}[/][.]{2}[/]', '../'), l.PARENT_LOCATION_ID,
               a.DEPTH,
               CASE WHEN l_bottom_location_id = l.LOCATION_ID THEN 'Y' ELSE 'N' END,
               l.LOCATION_TYPE_ID
          FROM LOCATION l
         INNER JOIN (SELECT ROWNUM DEPTH, COLUMN_VALUE FROM TABLE(l_location_ids)) a
            ON l.LOCATION_ID = a.COLUMN_VALUE
        UNION
        SELECT l.LOCATION_ID, l.LOCATION_NAME, REGEXP_REPLACE(l.LOCATION_IMAGE_URL, '^[.]{2}[/][.]{2}[/]', '../'), l.PARENT_LOCATION_ID,
               a.DEPTH - 1,
               CASE WHEN l_bottom_location_id = l.LOCATION_ID THEN 'Y' ELSE 'N' END,
               l.LOCATION_TYPE_ID
          FROM LOCATION l
         INNER JOIN (SELECT ROWNUM DEPTH, COLUMN_VALUE FROM TABLE(l_location_ids)) a
            ON a.DEPTH IN(2,3) AND l.PARENT_LOCATION_ID = a.COLUMN_VALUE
         ORDER BY DEPTH DESC, LOCATION_TYPE_ID, LOCATION_NAME;
    END IF;
END;
/