CREATE OR REPLACE TRIGGER location.trbi_cust_loc
BEFORE INSERT ON location.cust_loc
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

 SELECT 
           sysdate,
           user,
           sysdate,
           user
      into 
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;

/