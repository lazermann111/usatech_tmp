CREATE OR REPLACE PROCEDURE LOCATION.GET_ESUDS_LOCATION_HIERARCHY (
    l_location_id LOCATION.LOCATION_ID%TYPE,
    l_top_location_ids NUMBER_TABLE,
    l_cur OUT EXTENSIONS.REF_CURSOR,
    l_bottom_location_id OUT LOCATION.LOCATION_ID%TYPE
)
AS
    l_selected_location_ids NUMBER_TABLE;
    l_top_location_id LOCATION.LOCATION_ID%TYPE;
    l_parent_location_id LOCATION.LOCATION_ID%TYPE;
    l_from_top PLS_INTEGER;
    l_to_bottom PLS_INTEGER;
    l_cnt PLS_INTEGER := 0;
BEGIN
    IF l_location_id IS NOT NULL THEN
        BEGIN
            SELECT a.LOCATION_ID, a.DEPTH
              INTO l_top_location_id, l_from_top
              FROM (
                SELECT h.ANCESTOR_LOCATION_ID LOCATION_ID, h.DEPTH
                  FROM VW_LOCATION_HIERARCHY h
                 WHERE h.DESCENDENT_LOCATION_ID = l_location_id
                   AND h.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
                   AND h.ANCESTOR_LOCATION_ID MEMBER OF l_top_location_ids
                 ORDER BY h.DEPTH DESC, h.HIERARCHY_PATH
               ) a
            WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- location not in ancestry of any top location ids
                NULL;
        END;
    END IF;
    --Find the bottom location
    IF l_top_location_id IS NOT NULL THEN
        l_selected_location_ids := NUMBER_TABLE(l_location_id);
    ELSE
        l_selected_location_ids := l_top_location_ids;
        l_from_top := 0;
    END IF;
    SELECT a.LOCATION_ID, a.DEPTH
      INTO l_bottom_location_id, l_to_bottom
      FROM (
    	 SELECT h.DESCENDENT_LOCATION_ID LOCATION_ID, h.DEPTH
           FROM VW_LOCATION_HIERARCHY h
          WHERE h.ANCESTOR_LOCATION_ID MEMBER OF l_selected_location_ids
            AND h.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
          ORDER BY h.DEPTH DESC, h.HIERARCHY_PATH
           ) a
        WHERE ROWNUM = 1;
     
    IF l_to_bottom >= 2 THEN
       SELECT COUNT(*)
         INTO l_cnt
         FROM VW_LOCATION_HIERARCHY h
        WHERE h.ANCESTOR_LOCATION_ID MEMBER OF l_selected_location_ids
          AND h.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
          AND h.DEPTH = l_to_bottom - 2;
    END IF;
    IF l_cnt > 1 THEN
       OPEN l_cur FOR
            SELECT l.LOCATION_ID, l.LOCATION_NAME, REGEXP_REPLACE(l.LOCATION_IMAGE_URL, '^[.]{2}[/][.]{2}[/]', '../'), l.PARENT_LOCATION_ID,
                   a.LVL, 'N', l.LOCATION_TYPE_ID,
                   CASE WHEN a.LVL = 3 THEN 'Y' ELSE 'N' END
              FROM LOCATION l
             INNER JOIN (SELECT h.ANCESTOR_LOCATION_ID LOCATION_ID, l_to_bottom + h.DEPTH + 1 LVL
                           FROM VW_LOCATION_HIERARCHY h
                          WHERE h.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
                            AND h.DESCENDENT_LOCATION_ID MEMBER OF l_selected_location_ids
                            --AND h.DEPTH <= l_from_top
                          UNION
                          SELECT h.DESCENDENT_LOCATION_ID, l_to_bottom - h.DEPTH + 1
                           FROM VW_LOCATION_HIERARCHY h
                          WHERE h.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
                            AND h.ANCESTOR_LOCATION_ID MEMBER OF l_selected_location_ids
                            AND h.DEPTH BETWEEN 1 AND l_to_bottom - 2) a
                ON l.LOCATION_ID = a.LOCATION_ID
             ORDER BY LVL DESC, LOCATION_TYPE_ID, LOCATION_NAME;
    ELSE
        OPEN l_cur FOR
            SELECT l.LOCATION_ID, l.LOCATION_NAME, REGEXP_REPLACE(l.LOCATION_IMAGE_URL, '^[.]{2}[/][.]{2}[/]', '../'), l.PARENT_LOCATION_ID,
                   a.LVL,
                   CASE WHEN l_bottom_location_id = l.LOCATION_ID THEN 'Y' ELSE 'N' END,
                   l.LOCATION_TYPE_ID,
                   CASE WHEN a.LVL <= l_from_top + l_to_bottom + 1 THEN 'Y' ELSE 'N' END
              FROM LOCATION l
             INNER JOIN (SELECT h.ANCESTOR_LOCATION_ID LOCATION_ID, h.DEPTH + 1 LVL
                           FROM VW_LOCATION_HIERARCHY h
                          WHERE h.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
                            AND h.DESCENDENT_LOCATION_ID = l_bottom_location_id
                            --AND h.DEPTH <= l_from_top + l_to_bottom
                          UNION
                         SELECT hd.DESCENDENT_LOCATION_ID, hu.DEPTH
                           FROM VW_LOCATION_HIERARCHY hu
                          INNER JOIN VW_LOCATION_HIERARCHY hd
                             ON hu.ANCESTOR_LOCATION_ID = hd.ANCESTOR_LOCATION_ID
                          WHERE hu.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
                            AND hd.HIERARCHY_ACTIVE_YN_FLAG = 'Y'
                            AND hu.DESCENDENT_LOCATION_ID = l_bottom_location_id
                            AND hu.DEPTH IN(1,2)
                            AND hd.DEPTH = 1
                            AND hu.DEPTH <= l_from_top + l_to_bottom
                          ) a
                ON l.LOCATION_ID = a.LOCATION_ID
             ORDER BY LVL DESC, LOCATION_TYPE_ID, LOCATION_NAME;
    END IF;
END;
/