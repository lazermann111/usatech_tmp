CREATE OR REPLACE TRIGGER location.trbu_contact_type
BEFORE UPDATE ON location.contact_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
SELECT                :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
                 :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/