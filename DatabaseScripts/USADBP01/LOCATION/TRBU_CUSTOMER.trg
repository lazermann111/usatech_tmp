CREATE OR REPLACE TRIGGER location.trbu_customer
BEFORE UPDATE ON location.customer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
SELECT
         UPPER(:new.customer_country_cd),
           UPPER(:new.customer_state_cd),
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
           :new.customer_country_cd,
           :new.customer_state_cd,
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/