CREATE OR REPLACE TRIGGER location.trbi_customer_type
BEFORE INSERT ON location.customer_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.customer_type_id IS NULL
   THEN
      SELECT seq_customer_type_id.NEXTVAL
        INTO :NEW.customer_type_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/