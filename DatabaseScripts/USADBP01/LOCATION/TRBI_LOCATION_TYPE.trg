CREATE OR REPLACE TRIGGER location.trbi_location_type
BEFORE INSERT ON location.location_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
 SELECT seq_location_type_id.nextval,
           sysdate,
           user,
           sysdate,
           user
      into :new.location_type_id,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;

/