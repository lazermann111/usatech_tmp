CREATE OR REPLACE TRIGGER location.trbu_contact
BEFORE UPDATE ON location.contact
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
  SELECT UPPER(:new.contact_country_cd),
           UPPER(:new.contact_state_cd),
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into :new.contact_country_cd,
           :new.contact_state_cd,
                      :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/