CREATE OR REPLACE TRIGGER location.trbi_contact_type
BEFORE INSERT ON location.contact_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin
 SELECT seq_contact_type_id.nextval,
           sysdate,
           user,
           sysdate,
           user
      into :new.contact_type_id,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
          :new.last_updated_by
      FROM dual;

End;
/