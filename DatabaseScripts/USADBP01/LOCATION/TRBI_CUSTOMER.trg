CREATE OR REPLACE TRIGGER location.trbi_customer
BEFORE INSERT ON location.customer
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.customer_id IS NULL THEN

      SELECT seq_customer_id.nextval
        into :new.customer_id
        FROM dual;

    END IF;

 SELECT UPPER(:new.customer_country_cd),
           UPPER(:new.customer_state_cd),
           sysdate,
           user,
           sysdate,
           user
      into :new.customer_country_cd,
           :new.customer_state_cd,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

End;
/