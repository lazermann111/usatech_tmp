CREATE OR REPLACE TRIGGER location.trbu_state
BEFORE UPDATE ON location.state
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Declare

    CURSOR c_upper_state_name IS
          select nvl(state_name, :new.state_name)
        FROM state
       WHERE UPPER(:new.state_name) = UPPER(state_name);

Begin

  SELECT UPPER(:new.country_cd),
           UPPER(:new.state_cd),
                      :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into :new.country_cd,
           :new.state_cd,
                      :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

   --Try to match up the users state name with one that already exists
      OPEN c_upper_state_name;
      FETCH c_upper_state_name
        INTO :new.state_name;
      CLOSE c_upper_state_name;

End;
/