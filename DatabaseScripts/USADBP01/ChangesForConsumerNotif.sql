REM Execute as user DEVICE
GRANT REFERENCES ON device.host_status_notif_type TO pss;

REM Execute as user PSS
REM ======================================================================
REM ===   Sql Script for Database : Oracle db
REM ===
REM === Build : 12
REM ======================================================================

CREATE SEQUENCE PSS.SEQ_CONSUMER_PASSCODE_ID
    START WITH 1;

REM ======================================================================

CREATE SEQUENCE PSS.SEQ_CONSUMER_NOTIF_ID
    START WITH 1;

REM ======================================================================

CREATE SEQUENCE PSS.SEQ_PASSCODE_TYPE_ID
    START WITH 1;

REM ======================================================================

CREATE TABLE PSS.PASSCODE_TYPE
  (
    PASSCODE_TYPE_ID    NUMBER        not null,
    PASSCODE_TYPE_CD    VARCHAR(20)   unique not null,
    PASSCODE_TYPE_DESC  VARCHAR(60),
    DURATION_DAYS       NUMBER(30,15),
    CREATED_BY          VARCHAR(30)   not null,
    CREATED_TS          date          not null,
    LAST_UPDATED_BY     VARCHAR(30)   not null,
    LAST_UPDATED_TS     date          not null,
    primary key(PASSCODE_TYPE_ID)
  )
  TABLESPACE PSS_DATA;


REM ----------------------------------------------------------------------

CREATE TRIGGER PSS.TRBI_PASSCODE_TYPE BEFORE INSERT ON PSS.PASSCODE_TYPE
   FOR EACH ROW 
BEGIN
	SELECT SEQ_PASSCODE_TYPE_ID.NEXTVAL,
	       SYSDATE,
	       USER,
	       SYSDATE,
	       USER
	  INTO :NEW.PASSCODE_TYPE_ID,
	       :NEW.CREATED_TS,
	       :NEW.CREATED_BY,
	       :NEW.LAST_UPDATED_TS,
	       :NEW.LAST_UPDATED_BY
	  FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBU_PASSCODE_TYPE BEFORE UPDATE ON PSS.PASSCODE_TYPE
   FOR EACH ROW 
BEGIN
	SELECT :OLD.CREATED_TS,
	       :OLD.CREATED_BY,
           SYSDATE,
	       USER
	  INTO :NEW.CREATED_TS,
	       :NEW.CREATED_BY,
	       :NEW.LAST_UPDATED_TS,
	       :NEW.LAST_UPDATED_BY
	  FROM DUAL;
END;
/

REM ======================================================================

CREATE TABLE PSS.CONSUMER_PASSCODE
  (
    CONSUMER_PASSCODE_ID  NUMBER,
    CONSUMER_ID           NUMBER       not null,
    PASSCODE              VARCHAR(50)   not null,
    PASSCODE_TYPE_ID      NUMBER        not null,
    EXPIRATION_TS         date,
    CREATED_BY            VARCHAR(30)   not null,
    CREATED_TS            date          not null,
    LAST_UPDATED_BY       VARCHAR(30)   not null,
    LAST_UPDATED_TS       date          not null,
    primary key(CONSUMER_PASSCODE_ID),
    foreign key(CONSUMER_ID) references PSS.CONSUMER(CONSUMER_ID),
    foreign key(PASSCODE_TYPE_ID) references PSS.PASSCODE_TYPE(PASSCODE_TYPE_ID)
  )
  TABLESPACE PSS_DATA;

REM ----------------------------------------------------------------------
CREATE INDEX PSS.IX_CP_CONSUMER_ID ON PSS.CONSUMER_PASSCODE(CONSUMER_ID) TABLESPACE PSS_INDX;
CREATE UNIQUE INDEX PSS.UIX_CP_PASSCODE ON PSS.CONSUMER_PASSCODE(PASSCODE) TABLESPACE PSS_INDX;
CREATE BITMAP INDEX PSS.BIX_CP_PASSCODE_TYPE_ID ON PSS.CONSUMER_PASSCODE(PASSCODE_TYPE_ID) TABLESPACE PSS_INDX;

REM ----------------------------------------------------------------------

CREATE TRIGGER PSS.TRBI_CONSUMER_PASSCODE BEFORE INSERT ON PSS.CONSUMER_PASSCODE
   FOR EACH ROW 
BEGIN
	SELECT SEQ_CONSUMER_PASSCODE_ID.NEXTVAL,
	       SYSDATE,
	       USER,
	       SYSDATE,
	       USER
	  INTO :NEW.CONSUMER_PASSCODE_ID,
	       :NEW.CREATED_TS,
	       :NEW.CREATED_BY,
	       :NEW.LAST_UPDATED_TS,
	       :NEW.LAST_UPDATED_BY
	  FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_PASSCODE BEFORE UPDATE ON PSS.CONSUMER_PASSCODE
   FOR EACH ROW 
BEGIN
	SELECT :OLD.CREATED_TS,
	       :OLD.CREATED_BY,
           SYSDATE,
	       USER
	  INTO :NEW.CREATED_TS,
	       :NEW.CREATED_BY,
	       :NEW.LAST_UPDATED_TS,
	       :NEW.LAST_UPDATED_BY
	  FROM DUAL;
END;
/

REM ======================================================================

CREATE TABLE PSS.CONSUMER_NOTIF
  (
    CONSUMER_NOTIF_ID          NUMBER,
    HOST_STATUS_NOTIF_TYPE_ID  NUMBER       not null,
    CONSUMER_ID                NUMBER       not null,
    NOTIFY_ON                  char(1)       default 'Y' not null,
    CREATED_BY                 VARCHAR(30)   not null,
    CREATED_TS                 date          not null,
    LAST_UPDATED_BY            VARCHAR(30)   not null,
    LAST_UPDATED_TS            date          not null,
    primary key(CONSUMER_NOTIF_ID),
    foreign key(HOST_STATUS_NOTIF_TYPE_ID) references DEVICE.HOST_STATUS_NOTIF_TYPE(HOST_STATUS_NOTIF_TYPE_ID),
    foreign key(CONSUMER_ID) references PSS.CONSUMER(CONSUMER_ID)
  )
  TABLESPACE PSS_DATA;

REM ----------------------------------------------------------------------

CREATE UNIQUE INDEX PSS.UIX_CONSUMER_NOTIF_ALT_KEY1 ON PSS.CONSUMER_NOTIF(CONSUMER_ID, HOST_STATUS_NOTIF_TYPE_ID) TABLESPACE PSS_INDX;

REM ----------------------------------------------------------------------

CREATE TRIGGER PSS.TRBI_CONSUMER_NOTIF BEFORE INSERT ON PSS.CONSUMER_NOTIF
   FOR EACH ROW 
BEGIN
	SELECT SEQ_CONSUMER_NOTIF_ID.NEXTVAL,
	       SYSDATE,
	       USER,
	       SYSDATE,
	       USER
	  INTO :NEW.CONSUMER_NOTIF_ID,
	       :NEW.CREATED_TS,
	       :NEW.CREATED_BY,
	       :NEW.LAST_UPDATED_TS,
	       :NEW.LAST_UPDATED_BY
	  FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_NOTIF BEFORE UPDATE ON PSS.CONSUMER_NOTIF
   FOR EACH ROW 
BEGIN
	SELECT :OLD.CREATED_TS,
	       :OLD.CREATED_BY,
           SYSDATE,
	       USER
	  INTO :NEW.CREATED_TS,
	       :NEW.CREATED_BY,
	       :NEW.LAST_UPDATED_TS,
	       :NEW.LAST_UPDATED_BY
	  FROM DUAL;
END;
/

REM ======================================================================

INSERT INTO PSS.PASSCODE_TYPE(PASSCODE_TYPE_ID, PASSCODE_TYPE_CD, PASSCODE_TYPE_DESC, DURATION_DAYS)
VALUES(1, 'NOTIF_CONFIG', 'Used to change notify config for esuds auto-emails', 3.0);

COMMIT;
