DECLARE
	CURSOR cur_pos_ptas IS
		SELECT pp.pos_pta_id
		FROM location.customer c
		JOIN pss.pos p ON c.customer_id = p.customer_id
		JOIN device.vw_device_last_active d ON p.device_id = d.device_id
		JOIN pss.pos_pta pp ON p.pos_id = pp.pos_id
		WHERE c.customer_name = 'Coca-Cola Enterprises - All CCE Locations'
			AND d.device_type_id in (0, 1)
			AND (pp.pos_pta_deactivation_ts IS NULL OR pp.pos_pta_deactivation_ts > SYSDATE)
			AND pp.pos_pta_pref_auth_amt <= 1;
		
	ln_cnt NUMBER := 0;
BEGIN
	DBMS_OUTPUT.put_line('Script is starting...');
	
	FOR rec_pos_pta IN cur_pos_ptas LOOP
		--DBMS_OUTPUT.put_line('Processing pos_pta_id ' || rec_pos_pta.pos_pta_id || '...');
		ln_cnt := ln_cnt + 1;

		UPDATE pss.pos_pta
		SET pos_pta_pref_auth_amt = 1.50
		WHERE pos_pta_id = rec_pos_pta.pos_pta_id;
		
		COMMIT;
		DBMS_LOCK.SLEEP(0.08);
	END LOOP;
		
	DBMS_OUTPUT.put_line('Script finished, updated ' || ln_cnt || ' pos_pta records.');
END;
