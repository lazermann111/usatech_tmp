ALTER TABLE pss.authority_payment_mask_bref MODIFY authority_payment_mask_bref_id NUMBER(22,2);

INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.1, 'Primary Account Number, Part 1' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.1);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.2, 'Primary Account Number, Part 2' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.2);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.3, 'Primary Account Number, Part 3' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.3);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.4, 'Primary Account Number, Part 4' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.4);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.5, 'Primary Account Number, Part 5' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.5);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.6, 'Primary Account Number, Part 6' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.6);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.7, 'Primary Account Number, Part 7' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.7);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.8, 'Primary Account Number, Part 8' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.8);
INSERT INTO pss.authority_payment_mask_bref(authority_payment_mask_bref_id, regex_bref_name) SELECT 1.9, 'Primary Account Number, Part 9' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask_bref WHERE authority_payment_mask_bref_id = 1.9);

INSERT INTO authority.handler(handler_name, handler_class)
SELECT 'First Data Secure Transport', 'Authority::ISO8583::FHMS::FDMS' FROM DUAL 
WHERE NOT EXISTS (SELECT 1 FROM authority.handler WHERE handler_name = 'First Data Secure Transport');

INSERT INTO authority.authority_type(authority_type_name, authority_type_desc, handler_id)
SELECT 'First Data', 'First Data', handler_id FROM authority.handler WHERE handler_name = 'First Data Secure Transport'
AND NOT EXISTS (SELECT 1 FROM authority.authority_type WHERE authority_type_name = 'First Data');

INSERT INTO authority.authority(authority_name, authority_type_id, authority_service_id, terminal_capture_flag)
SELECT 'First Data', authority_type_id, 1, 'N' FROM authority.authority_type WHERE authority_type_name = 'First Data'
AND NOT EXISTS (SELECT 1 FROM authority.authority WHERE authority_name = 'First Data');

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'First Data Gateway 1', '127.0.0.1', 9100, 1, handler_id FROM authority.handler WHERE handler_name = 'First Data Secure Transport'
AND NOT EXISTS (SELECT 1 FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 1');

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'First Data Gateway 2', '127.0.0.1', 9100, 2, handler_id FROM authority.handler WHERE handler_name = 'First Data Secure Transport'
AND NOT EXISTS (SELECT 1 FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 2');

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'First Data Server 1', '127.0.0.1', 1, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 1'
AND NOT EXISTS (SELECT 1 FROM authority.authority_server WHERE authority_server_name = 'First Data Server 1');

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'First Data Server 2', '127.0.0.1', 2, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 2'
AND NOT EXISTS (SELECT 1 FROM authority.authority_server WHERE authority_server_name = 'First Data Server 2');

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
SELECT (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'First Data'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 1')
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM authority.authority_type_gateway WHERE
	authority_type_id = (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'First Data')
	AND authority_gateway_id = (SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 1'));

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
SELECT (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'First Data'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 2')
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM authority.authority_type_gateway WHERE
	authority_type_id = (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'First Data')
	AND authority_gateway_id = (SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'First Data Gateway 2'));

UPDATE pss.authority_payment_mask SET authority_payment_mask_name = 'Gift Card (Track 2) - First Data ValueLink'
WHERE authority_payment_mask_name = 'Gift Card (Track 2) - First Data';
	
INSERT INTO pss.authority_payment_mask(authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
SELECT 'Gift Card (Track 2) - First Data ValueLink', 'First Data ValueLink Gift Card on Track 2', '^;?601056([0-9]{1})([0-9]{8})[0-9]{1}=([0-9]{4})[0-9]{8}([0-9]{1})[0-9]{1}([0-9]{2})([0-9]{4})\??[0-9]{0,3}$', '1:1.3|2:1.4|3:3|4:1.1|5:1.2|6:1.5' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Gift Card (Track 2) - First Data ValueLink');

UPDATE pss.authority_payment_mask SET card_name = 'ValueLink',
	authority_payment_mask_desc = 'First Data ValueLink Gift Card on Track 2'
WHERE authority_payment_mask_name = 'Gift Card (Track 2) - First Data ValueLink';

UPDATE pss.payment_subtype SET payment_subtype_name = 'Gift Card (Track 2) - First Data ValueLink'
WHERE payment_subtype_name = 'Gift Card (Track 2) - First Data';

UPDATE pss.payment_subtype SET payment_subtype_name = 'RFID Gift Card (Track 2) - First Data ValueLink'
WHERE payment_subtype_name = 'RFID Gift Card (Track 2) - First Data';

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'Gift Card (Track 2) - First Data ValueLink', 'Authority::ISO8583::FHMS::FDMS', 'TERMINAL_ID', 'S', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Gift Card (Track 2) - First Data'
AND NOT EXISTS (SELECT 1 FROM pss.payment_subtype WHERE payment_subtype_name = 'Gift Card (Track 2) - First Data ValueLink');

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'RFID Gift Card (Track 2) - First Data ValueLink', 'Authority::ISO8583::FHMS::FDMS', 'TERMINAL_ID', 'P', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Gift Card (Track 2) - First Data'
AND NOT EXISTS (SELECT 1 FROM pss.payment_subtype WHERE payment_subtype_name = 'RFID Gift Card (Track 2) - First Data ValueLink');

UPDATE pss.authority_payment_mask SET payment_subtype_id = (SELECT payment_subtype_id FROM pss.payment_subtype WHERE payment_subtype_name = 'Gift Card (Track 2) - First Data ValueLink')
WHERE authority_payment_mask_name = 'Gift Card (Track 2) - First Data ValueLink';

INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) SELECT CORP.CARD_TYPE_SEQ.NEXTVAL, 'ValueLink', '*' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'ValueLink');

INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
SELECT (SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
	(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'ValueLink'),
	(SELECT AUTHORITY_ID FROM REPORT.AUTHORITY WHERE ALIASNAME = 'Generic Authority')
FROM DUAL WHERE NOT EXISTS (
	SELECT 1 FROM REPORT.CARDTYPE_AUTHORITY 
	WHERE CARDTYPE_ID = (SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'ValueLink')
);

COMMIT;
