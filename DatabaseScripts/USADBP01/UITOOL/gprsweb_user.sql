CREATE TABLE uitool.gprsweb_user
    (gprsweb_user_id    NUMBER(20,0) NOT NULL,
    gprsweb_user_name   VARCHAR2(255) NOT NULL,
    created_by          VARCHAR2(30) NOT NULL,
    created_ts          DATE NOT NULL,
    last_updated_by     VARCHAR2(30) NOT NULL,
    last_updated_ts     DATE NOT NULL)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  uitool_data
  STORAGE   (
    INITIAL     1048576
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/




-- Constraints for UITOOL.GPRSWEB_USER

ALTER TABLE uitool.gprsweb_user
ADD CONSTRAINT pk_gprsweb_user_id PRIMARY KEY (gprsweb_user_id)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  uitool_indx
  STORAGE   (
    INITIAL     1048576
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

CREATE SEQUENCE UITOOL.SEQ_GPRSWEB_USER_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1
/

-- Triggers for UITOOL.GPRSWEB_USER

CREATE OR REPLACE TRIGGER uitool.trbi_gprsweb_user
 BEFORE
  INSERT
 ON uitool.gprsweb_user
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	IF :NEW.GPRSWEB_USER_ID IS NULL
	THEN
		SELECT SEQ_GPRSWEB_USER_ID.NEXTVAL
		INTO :NEW.GPRSWEB_USER_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER uitool.gprsweb_user
 BEFORE
  UPDATE
 ON uitool.gprsweb_user
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

COMMENT ON TABLE UITOOL.GPRSWEB_USER is 'Stores GPRSWEB USER names to display in Drop downs used in GPRSWEB scripts.';

GRANT SELECT, INSERT, UPDATE, DELETE ON UITOOL.GPRSWEB_USER TO WEB_USER, USAT_WEB_ROLE;
