ALTER TABLE PSS.PAYMENT_SUBTYPE MODIFY PAYMENT_SUBTYPE_NAME VARCHAR2(100);

INSERT INTO authority.handler(handler_name, handler_class)
SELECT 'ComsGate', 'Authority::ISO8583::ComsGate' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM authority.handler WHERE handler_name = 'ComsGate');

INSERT INTO authority.authority_type(authority_type_name, authority_type_desc, handler_id)
SELECT 'ComsGate', 'CHARGE Anywhere ComsGate', handler_id FROM authority.handler WHERE handler_name = 'ComsGate'
AND NOT EXISTS (SELECT 1 FROM authority.authority_type WHERE authority_type_name = 'ComsGate');

INSERT INTO authority.authority(authority_name, authority_type_id, authority_service_id, terminal_capture_flag)
SELECT 'ComsGate', authority_type_id, 1, 'N' FROM authority.authority_type WHERE authority_type_name = 'ComsGate'
AND NOT EXISTS (SELECT 1 FROM authority.authority WHERE authority_name = 'ComsGate');

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'ComsGate Gateway 1', '127.0.0.1', 9100, 1, handler_id FROM authority.handler WHERE handler_name = 'ComsGate'
AND NOT EXISTS (SELECT 1 FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 1');

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'ComsGate Gateway 2', '127.0.0.1', 9100, 2, handler_id FROM authority.handler WHERE handler_name = 'ComsGate'
AND NOT EXISTS (SELECT 1 FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 2');

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'ComsGate Server 1', '127.0.0.1', 1, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 1'
AND NOT EXISTS (SELECT 1 FROM authority.authority_server WHERE authority_server_name = 'ComsGate Server 1');

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'ComsGate Server 2', '127.0.0.1', 2, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 2'
AND NOT EXISTS (SELECT 1 FROM authority.authority_server WHERE authority_server_name = 'ComsGate Server 2');

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
SELECT (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'ComsGate'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 1')
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM authority.authority_type_gateway WHERE
	authority_type_id = (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'ComsGate')
	AND authority_gateway_id = (SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 1'));

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
SELECT (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'ComsGate'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 2')
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM authority.authority_type_gateway WHERE
	authority_type_id = (SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'ComsGate')
	AND authority_gateway_id = (SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'ComsGate Gateway 2'));

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT REPLACE(payment_subtype_name, 'Elavon', 'ComsGate'), 'Authority::ISO8583::ComsGate', payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id
FROM pss.payment_subtype ps WHERE payment_subtype_class = 'Authority::ISO8583::Elavon' AND payment_subtype_name NOT LIKE '%Deprecated%'
AND NOT EXISTS(SELECT 1 FROM pss.payment_subtype WHERE payment_subtype_name = REPLACE(ps.payment_subtype_name, 'Elavon', 'ComsGate'));

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT REPLACE(payment_subtype_name, 'Elavon', 'ComsGate DEMO'), 'Authority::ISO8583::ComsGate', payment_subtype_key_name, DECODE(client_payment_type_cd, 'R', 'P', 'S'), payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id
FROM pss.payment_subtype ps WHERE payment_subtype_class = 'Authority::ISO8583::Elavon' AND payment_subtype_name NOT LIKE '%Deprecated%'
AND NOT EXISTS(SELECT 1 FROM pss.payment_subtype WHERE payment_subtype_name = REPLACE(ps.payment_subtype_name, 'Elavon', 'ComsGate DEMO'));

COMMIT;
