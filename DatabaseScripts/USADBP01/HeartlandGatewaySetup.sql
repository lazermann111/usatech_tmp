INSERT INTO authority.handler(handler_name, handler_class)
VALUES('Heartland', 'Authority::ISO8583::Heartland');

INSERT INTO authority.authority_type(authority_type_name, authority_type_desc, handler_id)
SELECT 'Heartland', 'Heartland Payment Systems POS Gateway', handler_id FROM authority.handler WHERE handler_name = 'Heartland';

INSERT INTO authority.authority(authority_name, authority_type_id, authority_service_id, terminal_capture_flag)
SELECT 'Heartland', authority_type_id, 1, 'N' FROM authority.authority_type WHERE authority_type_name = 'Heartland';

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'Heartland Gateway 1', '127.0.0.1', 9100, 1, handler_id FROM authority.handler WHERE handler_name = 'Heartland';

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'Heartland Gateway 2', '127.0.0.1', 9100, 2, handler_id FROM authority.handler WHERE handler_name = 'Heartland';

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'Heartland Server 1', '127.0.0.1', 1, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Heartland Gateway 1';

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'Heartland Server 2', '127.0.0.1', 2, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Heartland Gateway 2';

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
VALUES(
	(SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'Heartland'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Heartland Gateway 1')
);

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
VALUES(
	(SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'Heartland'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Heartland Gateway 2')
);

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT REPLACE(payment_subtype_name, 'Elavon', 'Heartland'), 'Authority::ISO8583::Heartland', payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id
FROM pss.payment_subtype WHERE payment_subtype_class = 'Authority::ISO8583::Elavon' AND payment_subtype_name NOT LIKE '%Deprecated%';

INSERT INTO pss.authority_payment_mask(authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
VALUES('Gift Card (Track 2) - Heartland Sprout', 'Heartland Sprout Gift Card on Track 2', '^;?(627722[0-9]{10})=([0-9]{4})([0-9]{3})([0-9]*)(?:\?([\x00-\xFF])?)?$', '1:1|2:3|3:4|4:5|5:12');

UPDATE PSS.AUTHORITY_PAYMENT_MASK
SET CARD_NAME = 'Sprout'
WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Gift Card (Track 2) - Heartland Sprout';

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'Gift Card (Track 2) - Heartland Sprout', 'Authority::ISO8583::Heartland', 'TERMINAL_ID', 'S', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Gift Card (Track 2) - Heartland Sprout';

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT 'RFID Gift Card (Track 2) - Heartland Sprout', 'Authority::ISO8583::Heartland', 'TERMINAL_ID', 'P', 'TERMINAL', 'TERMINAL_DESC', authority_payment_mask_id FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Gift Card (Track 2) - Heartland Sprout';

UPDATE pss.authority_payment_mask SET payment_subtype_id = (SELECT payment_subtype_id FROM pss.payment_subtype WHERE payment_subtype_name = 'Gift Card (Track 2) - Heartland Sprout')
WHERE authority_payment_mask_name = 'Gift Card (Track 2) - Heartland Sprout';

COMMIT;
