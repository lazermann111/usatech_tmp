DECLARE
    CURSOR cur_trans IS
		SELECT TRAN_ID
		FROM PSS.TRAN
		WHERE TRAN_STATE_CD IN ('0', '6', '8', '9', 'A', 'I', 'J', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W')
			AND PAYMENT_SUBTYPE_KEY_ID IS NULL;
BEGIN
    DBMS_OUTPUT.put_line('Script is starting...');
	
	FOR rec_tran IN cur_trans LOOP
		DBMS_OUTPUT.put_line('Processing tran_id ' || rec_tran.tran_id || '...');
	
		UPDATE PSS.TRAN SET (
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			AUTH_HOLD_USED,
			DEVICE_NAME
		) = (
			SELECT pp.payment_subtype_key_id,
				ps.payment_subtype_class,
				ps.client_payment_type_cd,
				a.auth_hold_used,
				d.device_name
			FROM pss.tran t
			JOIN pss.pos_pta pp ON t.pos_pta_id = pp.pos_pta_id
			JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
			JOIN pss.pos p ON pp.pos_id = p.pos_id
			JOIN device.device d ON p.device_id = d.device_id
			LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N' AND a.auth_result_cd IN ('Y', 'P')
			WHERE t.tran_id = rec_tran.tran_id
		) WHERE TRAN_ID = rec_tran.tran_id;
		
		COMMIT;
    END LOOP;
	
	COMMIT;	
	DBMS_OUTPUT.put_line('Script finished.');
END;
