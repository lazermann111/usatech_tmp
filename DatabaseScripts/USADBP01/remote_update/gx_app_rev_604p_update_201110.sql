DECLARE
	lv_gprs_command engine.machine_cmd_pending.command%TYPE := UPPER('46D074D8470015076A732D746573740B4750525354657374696E672E47782F50726F642F417070732F47785F4170705F32303131303832335F5265765F363034505F423833382E626165000500B83810FFEEDDCCBBAA998877665544332211004E832FF44E8DBBF40202');
	lv_ethernet_command engine.machine_cmd_pending.command%TYPE := UPPER('46D87FF7130015076A732D746573740B4750525354657374696E672E47782F50726F642F417070732F47785F4170705F32303131303832335F5265765F363034505F423833382E626165000500B83810FFEEDDCCBBAA998877665544332211004E84851B4E8F111B0202');
	lv_command engine.machine_cmd_pending.command%TYPE;

    CURSOR cur IS 
        SELECT
			d.device_name,
			d.comm_method_cd
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
			AND ds.device_setting_parameter_cd = 'Diagnostic App Rev'
			AND ds.device_setting_value IN (
				'1.00.019.D',
				'1.00.020.A',
				'1.00.021.A'
			)
		WHERE d.device_type_id = 1
			AND d.firmware_version != 'USA-Gx1v6.0.4P'
			AND d.device_serial_cd IN (
				'G8203386',
				'G8117436',
				'G5063235',
				'G5063709'
			);
BEGIN
    FOR rec_cur IN cur LOOP
		IF rec_cur.comm_method_cd = 'E' THEN
			lv_command := lv_ethernet_command;
		ELSE
			lv_command := lv_gprs_command;
		END IF;
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		SELECT rec_cur.device_name, 'A9', lv_command, 'P', 1
		FROM DUAL
		WHERE NOT EXISTS (
			SELECT 1
			FROM engine.machine_cmd_pending mcp
			WHERE mcp.machine_id = rec_cur.device_name
				AND mcp.data_type = 'A9'
				AND mcp.command IN (lv_command, LOWER(lv_command))
		);
		
		COMMIT;
        --DBMS_OUTPUT.put_line('device ' || rec_cur.device_name || ' is set up');
    END LOOP;
END;
/
