DECLARE
	ln_file_transfer_id file_transfer.file_transfer_id%TYPE := 7130488;
	lv_firmware_version device_setting.device_setting_value%TYPE := '1.00.034IP';
	ln_device_file_transfer_id device_file_transfer.device_file_transfer_id%TYPE;

    CURSOR cur IS 
        SELECT 
		d.device_id, 
		d.device_name
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
		WHERE d.device_serial_cd IN (
			'EE100014123',
			'EE100014122',
			'EE100014511',
			'EE100014413',
			'EE100014204',
			'EE100014330',
			'EE100014664',
			'EE100014281',
			'EE100014258',
			'EE100015974',
			'EE100014090',
			'EE100014089',
			'EE100014088',
			'EE100014341',
			'EE100014014',
			'EE100014180',
			'EE100014013',
			'EE100014516',
			'EE100014338',
			'EE100014030',
			'EE100014662',
			'EE100014665',
			'EE100014141',
			'EE100014339',
			'EE100014099',
			'EE100014329',
			'EE100014328',
			'EE100014085',
			'EE100014218',
			'EE100014026',
			'EE100014201',
			'EE100014202',
			'EE100014513',
			'EE100014512',
			'EE100014376',
			'EE100014175',
			'EE100014016',
			'EE100014661',
			'EE100014121',
			'EE100014487',
			'EE100014337',
			'EE100014125',
			'EE100014054',
			'EE100014342',
			'EE100015711',
			'EE100014543',
			'EE100014015',
			'EE100014340',
			'EE100014018',
			'EE100014029',
			'EE100014051',
			'EE100014027',
			'EE100014217',
			'EE100014220',
			'EE100014017',
			'EE100014377',
			'EE100014177',
			'EE100014412'
		)
			AND ds.device_setting_parameter_cd = 'Firmware Version'
			AND TRIM(ds.device_setting_value) <> lv_firmware_version
			AND NOT EXISTS (
				SELECT 1
				FROM device.device_file_transfer dft
				JOIN engine.machine_cmd_pending mcp ON mcp.command = dft.device_file_transfer_id
					AND mcp.data_type = 'C8'
					AND mcp.execute_cd = 'P'
				WHERE dft.file_transfer_id = ln_file_transfer_id
					AND dft.device_file_transfer_direct = 'O'
					AND dft.device_id = d.device_id
					AND dft.device_file_transfer_status_cd = 0
					AND mcp.machine_id = d.device_name
			);
BEGIN
	UPDATE engine.machine_cmd_pending
	SET execute_cd = 'C'
	WHERE machine_command_pending_id IN (
		SELECT machine_command_pending_id
		FROM engine.machine_cmd_pending mcp
		JOIN device.device_file_transfer dft ON mcp.command = dft.device_file_transfer_id
		JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
		WHERE ft.file_transfer_type_cd = 5 AND mcp.data_type = 'C8' AND ft.file_transfer_id != ln_file_transfer_id	
	);
        
    FOR rec_cur IN cur LOOP
        
		SELECT seq_device_file_transfer_id.NEXTVAL 
		INTO ln_device_file_transfer_id FROM DUAL;
		
		INSERT INTO device.device_file_transfer (device_file_transfer_id, file_transfer_id, 
			device_file_transfer_direct, device_id, 
			device_file_transfer_status_cd, device_file_transfer_pkt_size)
		VALUES (ln_device_file_transfer_id, ln_file_transfer_id, 'O', rec_cur.device_id, 0, 1024);
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		VALUES (rec_cur.device_name, 'C8', ln_device_file_transfer_id, 'P', 1);
		
		COMMIT;        
        DBMS_OUTPUT.put_line('device_id ' || rec_cur.device_id || ' is set up');
		
		DBMS_LOCK.SLEEP(60);
        
    END LOOP;
END;
/
