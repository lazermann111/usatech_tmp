DECLARE
	ln_file_transfer_id file_transfer.file_transfer_id%TYPE := 4742552;
	lv_firmware_version device_setting.device_setting_value%TYPE := '1.00.032PP';
	ln_device_file_transfer_id device_file_transfer.device_file_transfer_id%TYPE;

    CURSOR cur IS 
        SELECT 
		d.device_id, 
		d.device_name
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
		WHERE d.device_serial_cd IN (
			'EE100011026',
			'EE100011029',
			'EE100011030',
			'EE100011055',
			'EE100011057',
			'EE100011058',
			'EE100011059',
			'EE100011081',
			'EE100011082',
			'EE100011083',
			'EE100011084',
			'EE100011127',
			'EE100011128',
			'EE100011129',
			'EE100011229',
			'EE100011232',
			'EE100011234',
			'EE100011253',
			'EE100011255',
			'EE100011256',
			'EE100011257',
			'EE100011258',
			'EE100011302',
			'EE100011304',
			'EE100011305',
			'EE100011315',
			'EE100011316',
			'EE100011317',
			'EE100011391',
			'EE100011393',
			'EE100011541',
			'EE100011542',
			'EE100011546',
			'EE100011643',
			'EE100011644',
			'EE100011645',
			'EE100011646',
			'EE100011647',
			'EE100011648',
			'EE100011686',
			'EE100011687',
			'EE100011688',
			'EE100011689',
			'EE100011697',
			'EE100011701',
			'EE100011702',
			'EE100011703',
			'EE100011704',
			'EE100011705',
			'EE100011706',
			'EE100011707',
			'EE100011708',
			'EE100011769',
			'EE100011771',
			'EE100011774',
			'EE100011782',
			'EE100011783',
			'EE100011027',
			'EE100011056',
			'EE100011131',
			'EE100011132',
			'EE100011231',
			'EE100011233',
			'EE100011313',
			'EE100011314',
			'EE100011386',
			'EE100011392',
			'EE100011394',
			'EE100011395',
			'EE100011543',
			'EE100011544',
			'EE100011685',
			'EE100011700',
			'EE100011773',
			'EE100011781'
		)
			AND ds.device_setting_parameter_cd = 'Firmware Version'
			AND TRIM(ds.device_setting_value) <> lv_firmware_version
			AND NOT EXISTS (
				SELECT 1
				FROM device.device_file_transfer dft
				JOIN engine.machine_cmd_pending mcp ON mcp.command = dft.device_file_transfer_id
					AND mcp.data_type = 'C8'
					AND mcp.execute_cd = 'P'
				WHERE dft.file_transfer_id = ln_file_transfer_id
					AND dft.device_file_transfer_direct = 'O'
					AND dft.device_id = d.device_id
					AND dft.device_file_transfer_status_cd = 0
					AND mcp.machine_id = d.device_name
			);
BEGIN
        
    FOR rec_cur IN cur LOOP
        
		SELECT seq_device_file_transfer_id.NEXTVAL 
		INTO ln_device_file_transfer_id FROM DUAL;
		
		INSERT INTO device.device_file_transfer (device_file_transfer_id, file_transfer_id, 
			device_file_transfer_direct, device_id, 
			device_file_transfer_status_cd, device_file_transfer_pkt_size)
		VALUES (ln_device_file_transfer_id, ln_file_transfer_id, 'O', rec_cur.device_id, 0, 1024);
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		VALUES (rec_cur.device_name, 'C8', ln_device_file_transfer_id, 'P', 1);
		
		COMMIT;        
        DBMS_OUTPUT.put_line('device_id ' || rec_cur.device_id || ' is set up');
        
    END LOOP;
END;
/
