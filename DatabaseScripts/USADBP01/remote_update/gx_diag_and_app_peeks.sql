DECLARE
    CURSOR cur IS
        SELECT d.device_name
		FROM device.vw_device_last_active d
		WHERE d.device_type_id = 1
			AND d.device_serial_cd IN (
				'G8203386',
				'G8117436',
				'G5063235',
				'G5063709'
			);
BEGIN
    FOR rec_cur IN cur LOOP
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		SELECT rec_cur.device_name, '87', '4400807E9000000021', 'P', 1
		FROM DUAL
		WHERE NOT EXISTS (
			SELECT 1
			FROM engine.machine_cmd_pending mcp
			WHERE mcp.machine_id = rec_cur.device_name
				AND mcp.data_type = '87'
				AND mcp.command = '4400807E9000000021'
		);
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		SELECT rec_cur.device_name, '87', '4400007F200000000F', 'P', 2
		FROM DUAL
		WHERE NOT EXISTS (
			SELECT 1
			FROM engine.machine_cmd_pending mcp
			WHERE mcp.machine_id = rec_cur.device_name
				AND mcp.data_type = '87'
				AND mcp.command = '4400007F200000000F'
		);
		
		COMMIT;
        --DBMS_OUTPUT.put_line('Device ' || rec_cur.device_name || ' is set up');
    END LOOP;
END;
/
