DECLARE
    CURSOR cur IS
        SELECT 
		d.device_id, 
		d.device_name
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
		LEFT OUTER JOIN device.device_setting ds2 ON d.device_id = ds2.device_id
			AND ds2.device_setting_parameter_cd = 'Diagnostic App Rev'
		WHERE d.device_serial_cd IN (
			'G8101233'
		)
			AND ds.device_setting_parameter_cd = 'Firmware Version'
			AND (ds.device_setting_value LIKE 'USA-G51v5%' OR ds.device_setting_value LIKE 'USA-Gx1v5%')
			AND TRIM(NVL(ds2.device_setting_value, 'X')) <> '1.00.021.A'
			AND NOT EXISTS (
				SELECT 1
				FROM engine.machine_cmd_pending mcp
				WHERE mcp.machine_id = d.device_name
					AND mcp.data_type = '87'
					AND mcp.execute_cd = 'P'
					AND mcp.command = '4400807E9000000021'
			);
BEGIN
        
    FOR rec_cur IN cur LOOP
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		VALUES (rec_cur.device_name, '87', '4400807E9000000021', 'P', 1);
		
		COMMIT;
        DBMS_OUTPUT.put_line('device_id ' || rec_cur.device_id || ' is set up');
        
    END LOOP;
END;
/
