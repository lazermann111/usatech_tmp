DECLARE
	lv_gprs_command engine.machine_cmd_pending.command%TYPE := '46d074d8470015076d6c2d746573740c4750525354657374696e67323453412f47782f446961676e6f73746963732f47365f446961675f32303039303330335f5265765f303231415f303932442e626165000500092d10147147147147147147258258258258254b4b52ce4ba255ce020f';
	lv_ethernet_command engine.machine_cmd_pending.command%TYPE := '46d87ff7130015076d6c2d746573740c4750525354657374696e67323453412f47782f446961676e6f73746963732f47365f446961675f32303039303330335f5265765f303231415f303932442e626165000500092d10147147147147147147258258258258254b4b52ce4ba255ce020f';
	lv_command engine.machine_cmd_pending.command%TYPE := lv_ethernet_command;
	ln_count NUMBER;

    CURSOR cur IS 
        SELECT 
		d.device_id, 
		d.device_name,
		dcirx.ip_address
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
		LEFT OUTER JOIN device.device_setting ds2 ON d.device_id = ds2.device_id
			AND ds2.device_setting_parameter_cd = 'Diagnostic App Rev'
		LEFT OUTER JOIN (
			SELECT DISTINCT serial_number, FIRST_VALUE(ip_address) OVER(PARTITION BY serial_number ORDER BY call_in_start_ts DESC) ip_address
			FROM device.device_call_in_record
			WHERE call_in_start_ts > SYSDATE - 3
		) dcirx ON d.device_serial_cd = dcirx.serial_number
		WHERE d.device_serial_cd IN (
			'G8101233'
		)
			AND ds.device_setting_parameter_cd = 'Firmware Version'
			AND (ds.device_setting_value LIKE 'USA-G51v5%' OR ds.device_setting_value LIKE 'USA-Gx1v5%')
			AND TRIM(NVL(ds2.device_setting_value, 'X')) <> '1.00.021.A'
			AND NOT EXISTS (
				SELECT 1
				FROM engine.machine_cmd_pending mcp
				WHERE mcp.machine_id = d.device_name
					AND mcp.data_type = 'A9'
					AND mcp.execute_cd = 'P'
			);
BEGIN
        
    FOR rec_cur IN cur LOOP
        
		IF rec_cur.ip_address IS NOT NULL THEN
			IF rec_cur.ip_address LIKE '172.16.%'
				OR rec_cur.ip_address LIKE '172.24.%'
				OR rec_cur.ip_address LIKE '192.168.30.%' THEN
				lv_command := lv_gprs_command;
			END IF;
		ELSE
			SELECT COUNT(1) INTO ln_count
			FROM device.gprs_device
			WHERE device_id = rec_cur.device_id;
			
			IF ln_count > 0 THEN
				lv_command := lv_gprs_command;
			END IF;
		END IF;
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		VALUES (rec_cur.device_name, 'A9', lv_command, 'P', 1);
		
		COMMIT;
        DBMS_OUTPUT.put_line('device_id ' || rec_cur.device_id || ' is set up');
        
    END LOOP;
END;
/
