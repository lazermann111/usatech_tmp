DECLARE
	ln_file_transfer_id file_transfer.file_transfer_id%TYPE := 16247677;
	lv_firmware_version device_setting.device_setting_value%TYPE := 'I2.5.23';
	ln_device_file_transfer_id device_file_transfer.device_file_transfer_id%TYPE;
	ln_cnt NUMBER := 0;

    CURSOR cur IS 
        SELECT 
		d.device_id, 
		d.device_name
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
		WHERE d.device_type_id = 5
			AND ds.device_setting_parameter_cd = 'eSuds Application Version'
			AND SUBSTR(ds.device_setting_value, 1, 3) IN ('2.4', 'I2.')
			AND ds.device_setting_value <> lv_firmware_version
			AND NOT EXISTS (
				SELECT 1
				FROM device.device_file_transfer dft
				JOIN engine.machine_cmd_pending mcp ON mcp.command = dft.device_file_transfer_id
					AND mcp.data_type = '7C'
				WHERE dft.file_transfer_id = ln_file_transfer_id
					AND dft.device_file_transfer_direct = 'O'
					AND dft.device_id = d.device_id
					AND dft.device_file_transfer_status_cd = 0
					AND mcp.machine_id = d.device_name
			)
			AND NOT EXISTS (
				SELECT 1
				FROM device.device_file_transfer dft
				JOIN engine.machine_cmd_pending_hist mcph ON mcph.command = dft.device_file_transfer_id
					AND mcph.data_type = '7C'
				WHERE dft.file_transfer_id = ln_file_transfer_id
					AND dft.device_file_transfer_direct = 'O'
					AND dft.device_id = d.device_id
					AND dft.device_file_transfer_status_cd = 1
					AND mcph.machine_id = d.device_name
					AND mcph.execute_cd = 'A'
					AND mcph.execute_date > SYSDATE - 1/24
			);
BEGIN
    FOR rec_cur IN cur LOOP
	
		UPDATE engine.machine_cmd_pending
		SET execute_cd = 'C'
		WHERE machine_command_pending_id IN (
			SELECT mcp.machine_command_pending_id
			FROM engine.machine_cmd_pending mcp
			JOIN device.device_file_transfer dft ON mcp.command = dft.device_file_transfer_id
			JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
			WHERE mcp.machine_id = rec_cur.device_name
				AND mcp.data_type = '7C'
				AND ft.file_transfer_type_cd = 5
				AND ft.file_transfer_id != ln_file_transfer_id	
		);	
        
		SELECT seq_device_file_transfer_id.NEXTVAL 
		INTO ln_device_file_transfer_id FROM DUAL;
		
		INSERT INTO device.device_file_transfer (device_file_transfer_id, file_transfer_id, 
			device_file_transfer_direct, device_id, 
			device_file_transfer_status_cd, device_file_transfer_pkt_size)
		VALUES (ln_device_file_transfer_id, ln_file_transfer_id, 'O', rec_cur.device_id, 0, 1996);
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		VALUES (rec_cur.device_name, '7C', ln_device_file_transfer_id, 'P', 1);
		
		COMMIT;
        --DBMS_OUTPUT.put_line('device_id ' || rec_cur.device_id || ' is set up');
		ln_cnt := ln_cnt + 1;
        
    END LOOP;
	
	DBMS_OUTPUT.put_line('Scheduled update for ' || ln_cnt || ' devices');
END;
/
