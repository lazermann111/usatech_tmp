DECLARE
	ln_file_transfer_id file_transfer.file_transfer_id%TYPE := 14706617;
	lv_firmware_version device_setting.device_setting_value%TYPE := '1.00.040 P';
	ln_device_file_transfer_id device_file_transfer.device_file_transfer_id%TYPE;
	ln_cnt NUMBER := 0;

    CURSOR cur IS 
        SELECT 
		d.device_id, 
		d.device_name
		FROM device.vw_device_last_active d
		JOIN device.device_setting ds ON d.device_id = ds.device_id
		WHERE d.device_type_id = 13
			AND ds.device_setting_parameter_cd = 'Firmware Version'
			AND TRIM(ds.device_setting_value) <> lv_firmware_version
			AND NOT EXISTS (
				SELECT 1
				FROM device.device_file_transfer dft
				JOIN engine.machine_cmd_pending mcp ON mcp.command = dft.device_file_transfer_id
					AND mcp.data_type = 'C8'
				WHERE dft.file_transfer_id = ln_file_transfer_id
					AND dft.device_file_transfer_direct = 'O'
					AND dft.device_id = d.device_id
					AND dft.device_file_transfer_status_cd = 0
					AND mcp.machine_id = d.device_name
			);
BEGIN
	UPDATE engine.machine_cmd_pending
	SET execute_cd = 'C'
	WHERE machine_command_pending_id IN (
		SELECT machine_command_pending_id
		FROM engine.machine_cmd_pending mcp
		JOIN device.device_file_transfer dft ON mcp.command = dft.device_file_transfer_id
		JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
		WHERE ft.file_transfer_type_cd = 5 AND mcp.data_type = 'C8' AND ft.file_transfer_id != ln_file_transfer_id	
	);
        
    FOR rec_cur IN cur LOOP
        
		SELECT seq_device_file_transfer_id.NEXTVAL 
		INTO ln_device_file_transfer_id FROM DUAL;
		
		INSERT INTO device.device_file_transfer (device_file_transfer_id, file_transfer_id, 
			device_file_transfer_direct, device_id, 
			device_file_transfer_status_cd, device_file_transfer_pkt_size)
		VALUES (ln_device_file_transfer_id, ln_file_transfer_id, 'O', rec_cur.device_id, 0, 1024);
		
		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order)
		VALUES (rec_cur.device_name, 'C8', ln_device_file_transfer_id, 'P', 1);
		
		COMMIT;
        --DBMS_OUTPUT.put_line('device_id ' || rec_cur.device_id || ' is set up');
		ln_cnt := ln_cnt + 1;
        
    END LOOP;
	
	DBMS_OUTPUT.put_line('Scheduled update for ' || ln_cnt || ' devices');
END;
/
