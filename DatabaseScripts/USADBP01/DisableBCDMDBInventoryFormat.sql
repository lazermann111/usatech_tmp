-- This script will disable the BCD MDB Inventory Format for G4 and Gx devices
DECLARE
    i_debug INT := 0;
    return_code NUMBER(20);
    return_msg VARCHAR2(2048);
	n_changed NUMBER := 0;
	n_unchanged NUMBER := 0;
	n_failed NUMBER := 0;
    CURSOR cur IS 
		select distinct d.device_id, d.device_type_id
		from device.file_transfer ft join device.vw_device_last_active d on ft.file_transfer_name = d.device_name || '-CFG'
		where d.device_type_id in (0, 1) and ft.file_transfer_type_cd = 1;
BEGIN
    DBMS_OUTPUT.put_line('Script is starting, debug: ' || i_debug || '...');
    
    FOR rec_cur IN cur LOOP
        return_code := -2;
        return_msg := 'Did not run.';
       
        sp_update_config(rec_cur.device_id, 389, '59', 'H', rec_cur.device_type_id, i_debug, return_code, return_msg);
		COMMIT;
		
		IF return_code = 0 THEN
			n_unchanged := n_unchanged + 1;
            --DBMS_OUTPUT.put_line('BCD MDB Inventory Format was already disabled for device_id: ' || rec_cur.device_id);
		ELSIF return_code = 1 THEN
			n_changed := n_changed + 1;
            --DBMS_OUTPUT.put_line('BCD MDB Inventory Format has been disabled for device_id: ' || rec_cur.device_id);
        ELSE
			n_failed := n_failed + 1;
            --DBMS_OUTPUT.put_line('Disabling BCD MDB Inventory Format failed for device_id: ' || rec_cur.device_id || ', error: ' || return_msg);
        END IF;
    END LOOP;
	
	DBMS_OUTPUT.put_line('Script finished, device counts are Changed: ' || n_changed || ', Unchanged: ' || n_unchanged || ', Failed: ' || n_failed);
	COMMIT;
END;
