CREATE OR REPLACE VIEW SONY.VW_MONTHLY_EFT_BY_TERMINAL
(CUST_NAME, LOCATION, MACHINE, TRANGROSSAMT, TRANGROSSCNT)
AS 
(

SELECT
a.cust_name,
a.location,
a.machine,
SUM(a.tran_amount) AS trangrossamt,
SUM(a.tranid) AS trangrosscnt
FROM (
    SELECT 
    REPLACE(cus.customer_name, ',', ' ') cust_name,
    REPLACE(loc.location_name, ',', ' ') location,
    dev.device_name machine,
    SUM(au.auth_amt) tran_amount,
    COUNT(1) tranid
    FROM device.device dev
    JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = dev.device_id
    JOIN pss.pos pos
    ON pos.device_id = dev.device_id
    JOIN location.location loc
    ON pos.location_id = loc.location_id
    JOIN location.customer cus
    ON pos.customer_id = cus.customer_id
    JOIN pss.pos_pta pp
    ON pp.pos_id = pos.pos_id
    JOIN pss.tran tr
    ON tr.pos_pta_id = pp.pos_pta_id
    AND tr.tran_start_ts >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
    AND tr.tran_start_ts < TRUNC(SYSDATE, 'MM')
    JOIN pss.auth au
    ON au.tran_id = tr.tran_id
    AND au.auth_type_cd IN ('U', 'S')
    GROUP BY cus.customer_name, loc.location_name, dev.device_name
    
    UNION
    
    SELECT 
    REPLACE(cus.customer_name, ',', ' ') cust_name,
    REPLACE(loc.location_name, ',', ' ') location,
    dev.device_name machine,
    0 AS tran_amount,
    0 AS tranid
    FROM device.device dev
    JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = dev.device_id
    JOIN pss.pos pos
    ON pos.device_id = dev.device_id
    AND dev.device_active_yn_flag = 'Y'
    JOIN location.location loc
    ON pos.location_id = loc.location_id
    JOIN location.customer cus
    ON pos.customer_id = cus.customer_id
) a
GROUP BY a.cust_name, a.location, a.machine

);
