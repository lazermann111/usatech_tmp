CREATE OR REPLACE VIEW SONY.VW_MONTHLY_EFT_REPORT
(TRANCNT, CUSTOMER, MACHINE_ID, LOCATION, CREDIT_AMOUNT, 
 CREDIT_CNT, FAILED_AMT, FAILED_CNT, PROCESS_FEE_AMT, TRANS_FEE_AMT, 
 SERVICE_FEE_AMT, NET_EFT_AMT)
AS 
SELECT 
SUM(tran_count) trancnt,
customer,
machine_id,
location,
ROUND(SUM(gross_tran_amt), 2) AS credit_amount,
SUM(gross_tran_cnt) AS credit_cnt,
ROUND(SUM(failed_credit_amt), 2) AS failed_amt,
SUM(failed_credit_cnt) AS failed_cnt,
ROUND(SUM(process_fee), 2) AS process_fee_amt,
ROUND(SUM(trans_fee), 2) AS trans_fee_amt,
ROUND(SUM(service_fee), 2) AS service_fee_amt,
ROUND(SUM(gross_tran_amt - failed_credit_amt - process_fee - trans_fee - service_fee), 2) AS net_eft_amt
FROM (	
		
	SELECT 
	COUNT(1) AS tran_count,
	REPLACE(cus.customer_name, ',', ' ') AS customer,
	dev.device_name AS machine_id,
	REPLACE(loc.location_name,',',' ') AS location,
	0 AS gross_tran_amt,
	0 AS gross_tran_cnt,
	0 AS failed_credit_amt,
	0 AS failed_credit_cnt,
	0 AS process_fee,
	COUNT(1) * .05 AS trans_fee,
	0 AS service_fee
	FROM device.device dev
	JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = dev.device_id
	JOIN pss.pos pos
	ON pos.device_id = dev.device_id
	JOIN location.location loc
	ON pos.location_id = loc.location_id
	JOIN location.customer cus
	ON pos.customer_id = cus.customer_id
	JOIN pss.pos_pta pp
	ON pp.pos_id = pos.pos_id
	JOIN pss.payment_subtype pst
	ON pst.payment_subtype_id = pp.payment_subtype_id
	AND pst.client_payment_type_cd IN ('S', 'P') -- special card
	JOIN pss.client_payment_type cpt
	ON cpt.client_payment_type_cd = pst.client_payment_type_cd
	JOIN pss.tran tr
	ON tr.pos_pta_id = pp.pos_pta_id
	AND tr.tran_start_ts >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
	AND tr.tran_start_ts < LAST_DAY(ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)) + 1 - (1 / 24 / 60 / 60)
	JOIN pss.auth au
	ON au.tran_id = tr.tran_id
	AND au.auth_type_cd IN ('U', 'S')
	GROUP BY cus.customer_name, dev.device_name, loc.location_name
	
	UNION
	
	SELECT 
	COUNT(1) AS tran_count,
	REPLACE(cus.customer_name, ',', ' ') AS customer,
	dev.device_name AS machine_id,
	REPLACE(loc.location_name,',',' ') AS location,
	SUM(DECODE(tr.tran_state_cd,		--sum successful trans
	    'D', au.auth_amt,
	    0
	)) AS gross_tran_amt,
	SUM(DECODE(tr.tran_state_cd,		--count successful trans
	    'D', 1,
	    0
	)) AS gross_tran_cnt,
	SUM(DECODE(tr.tran_state_cd,		--sum successful trans
	    'E', au.auth_amt,
	    0
	)) AS failed_credit_amt,
	SUM(DECODE(tr.tran_state_cd,		--count failed trans
	    'E', 1,
	    0
	)) AS failed_credit_cnt,
	SUM((au.auth_amt - DECODE(tr.tran_state_cd,	 --process fee
	    'E', au.auth_amt,
	    0
	)) * .05) AS process_fee,
	COUNT(1) * .05 AS trans_fee,
	0 AS service_fee
	FROM device.device dev
	JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = dev.device_id
	JOIN pss.pos pos
	ON pos.device_id = dev.device_id
	JOIN location.location loc
	ON pos.location_id = loc.location_id
	JOIN location.customer cus
	ON pos.customer_id = cus.customer_id
	JOIN pss.pos_pta pp
	ON pp.pos_id = pos.pos_id
	JOIN pss.payment_subtype pst
	ON pst.payment_subtype_id = pp.payment_subtype_id
	AND pst.client_payment_type_cd IN ('C', 'R') -- credit card
	JOIN pss.client_payment_type cpt
	ON cpt.client_payment_type_cd = pst.client_payment_type_cd
	JOIN pss.tran tr
	ON tr.pos_pta_id = pp.pos_pta_id
	AND tr.tran_start_ts >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
	AND tr.tran_start_ts < TRUNC(SYSDATE, 'MM')
	JOIN pss.auth au
	ON au.tran_id = tr.tran_id
	AND au.auth_type_cd IN ('U', 'S')
	GROUP BY cus.customer_name, dev.device_name, loc.location_name
	
	UNION
	
	SELECT
	0 AS tran_count,
	REPLACE(cus.customer_name, ',',' ') AS customer,
	dev.device_name AS machine_id,
	REPLACE(loc.location_name, ',',' ') AS location,
	0 AS gross_tran_amt,
	0 AS gross_tran_cnt,
	0 AS failed_credit_amt,
	0 AS failed_credit_cnt,
	0 AS process_fee,
	0 AS trans_fee,
	25 AS service_fee
	FROM device.device dev
	JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = dev.device_id
    JOIN pss.pos pos
    ON pos.device_id = dev.device_id
    AND dev.device_active_yn_flag = 'Y'
    JOIN location.location loc
    ON pos.location_id = loc.location_id
    JOIN location.customer cus
    ON pos.customer_id = cus.customer_id

)
GROUP BY customer, machine_id, location
ORDER BY machine_id ASC;
