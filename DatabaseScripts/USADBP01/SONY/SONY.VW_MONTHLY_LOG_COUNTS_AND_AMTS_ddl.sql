-- Start View SONY.VW_MONTHLY_LOG_COUNTS_AND_AMTS

CREATE OR REPLACE VIEW sony.vw_monthly_log_counts_and_amts (
   machine,
   logamt,
   logcnt )
AS
SELECT
machine,
SUM(sales_total) logamt,
SUM(total_transactions) logcnt
FROM (
    SELECT 
    a.machine_id machine,
    a.sales_total sales_total,
    a.total_transactions total_transactions
    FROM sony_sales_summary a
    JOIN device.device d
    ON a.machine_id = d.device_name
    JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = d.device_id
    JOIN pss.pos p
    ON p.device_id = d.device_id
    JOIN location.location l
    ON p.location_id = l.location_id
    JOIN location.customer c
    ON p.customer_id = c.customer_id
    WHERE a.created_ts >= ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1)
    AND a.created_ts < TRUNC(SYSDATE, 'MM')

    UNION

    SELECT 
    d.device_name machine,
    0 sales_total,
    0 total_transactions
    FROM device.device d
    JOIN pss.pos p
    ON p.device_id = d.device_id
    JOIN (
        SELECT
        dev.device_id
        FROM device.device dev
        WHERE dev.device_type_id = 4
        UNION
        SELECT
        ds.device_id
        FROM device.device_setting ds
        WHERE ds.device_setting_parameter_cd = 'HostType'
        AND ds.device_setting_value = 'SONY'
    ) sdev
    ON sdev.device_id = d.device_id
    JOIN location.location l
    ON p.location_id = l.location_id
    JOIN location.customer c
    ON p.customer_id = c.customer_id
)
GROUP BY machine
/


-- End of DDL Script for View SONY.VW_MONTHLY_LOG_COUNTS_AND_AMTS

