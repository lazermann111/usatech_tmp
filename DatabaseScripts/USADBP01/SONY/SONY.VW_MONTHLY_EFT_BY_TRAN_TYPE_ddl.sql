CREATE OR REPLACE VIEW SONY.VW_MONTHLY_EFT_BY_TRAN_TYPE
(CUSTOMER, MACHINE_ID, LOCATION, CARD_TYPE, TRAN_AMOUNT, 
 TRAN_NUMBER_FEE, PROCESSING_FEE)
AS 
(
SELECT 
REPLACE(cus.customer_name, ',', ' ') AS customer_name,
dev.device_name machine_id,
REPLACE(loc.location_name, ',', ' ') AS location_name,
cpt.client_payment_type_desc type,
TO_CHAR(SUM(au.auth_amt)) tran_amount_fee,
'-' tran_number_fee,
ROUND(SUM(au.auth_amt) * .05, 2) processing_fee
FROM device.device dev
JOIN (
    SELECT
    dev.device_id
    FROM device.device dev
    WHERE dev.device_type_id = 4
    UNION
    SELECT
    ds.device_id
    FROM device.device_setting ds
    WHERE ds.device_setting_parameter_cd = 'HostType'
    AND ds.device_setting_value = 'SONY'
) sdev
ON sdev.device_id = dev.device_id
JOIN pss.pos pos
ON pos.device_id = dev.device_id
JOIN location.location loc
ON pos.location_id = loc.location_id
JOIN location.customer cus
ON pos.customer_id = cus.customer_id
JOIN pss.pos_pta pp
ON pp.pos_id = pos.pos_id
JOIN pss.payment_subtype pst
ON pst.payment_subtype_id = pp.payment_subtype_id
AND pst.client_payment_type_cd IN ('C', 'R')
JOIN pss.client_payment_type cpt
ON cpt.client_payment_type_cd = pst.client_payment_type_cd
JOIN pss.tran tr
ON tr.pos_pta_id = pp.pos_pta_id
AND tr.tran_start_ts >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
AND tr.tran_start_ts < TRUNC(SYSDATE, 'MM')
JOIN pss.auth au
ON au.tran_id = tr.tran_id
AND au.auth_type_cd IN ('U', 'S')
GROUP BY cus.customer_name, dev.device_name, loc.location_name, cpt.client_payment_type_desc

UNION

SELECT 
REPLACE(cus.customer_name, ',', ' ') AS customer_name,
dev.device_name machine_id,
REPLACE(loc.location_name, ',', ' ') AS location_name,
'Tran' type,
'-' tran_amount_fee,
TO_CHAR(COUNT(1)) tran_number_fee,
COUNT(1) * .05 processing_fee
FROM device.device dev
JOIN (
    SELECT
    dev.device_id
    FROM device.device dev
    WHERE dev.device_type_id = 4
    UNION
    SELECT
    ds.device_id
    FROM device.device_setting ds
    WHERE ds.device_setting_parameter_cd = 'HostType'
    AND ds.device_setting_value = 'SONY'
) sdev
ON sdev.device_id = dev.device_id
JOIN pss.pos pos
ON pos.device_id = dev.device_id
JOIN location.location loc
ON pos.location_id = loc.location_id
JOIN location.customer cus
ON pos.customer_id = cus.customer_id
JOIN pss.pos_pta pp
ON pp.pos_id = pos.pos_id
JOIN pss.payment_subtype pst
ON pst.payment_subtype_id = pp.payment_subtype_id
JOIN pss.client_payment_type cpt
ON cpt.client_payment_type_cd = pst.client_payment_type_cd
JOIN pss.tran tr
ON tr.pos_pta_id = pp.pos_pta_id
AND tr.tran_start_ts >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
AND tr.tran_start_ts < TRUNC(SYSDATE, 'MM')
JOIN pss.auth au
ON au.tran_id = tr.tran_id
AND au.auth_type_cd IN ('U', 'S')
GROUP BY cus.customer_name, dev.device_name, loc.location_name
);
