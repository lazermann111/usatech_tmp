CREATE OR REPLACE VIEW SONY.VW_MONTHLY_EFT_TRAN_DETAIL
(TRANS_NO, TRANSACTION_DATE, OWNER, MACHINE_ID, TRANSACTION_AMOUNT, 
 TRANSTATUS, CARD_TYPE, CARD_NUMBER)
AS 
(
SELECT 
tran_id, 
transaction_date, 
owner, 
machine_id, 
transaction_amount, 
tran_state_cd, 
card_type,  
(CASE 
    WHEN card_type = 'SP' 
    THEN card_number ELSE 
    SUBSTR(card_number, 1, 2) || SUBSTR('****************', 1, LENGTH(card_number) - 6) || SUBSTR(card_number, -4, 4) 
END) card_number
FROM (
	SELECT tr.tran_id,
	tr.tran_start_ts transaction_date,
	REPLACE(cus.customer_name, ',', ' ') AS owner,
	dev.device_name machine_id,
	au.auth_amt transaction_amount,
	tr.tran_state_cd,
	(CASE
		WHEN pst.client_payment_type_cd IN ('C', 'R')
		THEN (CASE
			WHEN SUBSTR(tr.tran_parsed_acct_num, 1, 1) = '3' THEN 'AE'
			WHEN SUBSTR(tr.tran_parsed_acct_num, 1, 1) = '4' THEN 'VI'
			WHEN SUBSTR(tr.tran_parsed_acct_num, 1, 1) = '5' THEN 'MC'
			WHEN SUBSTR(tr.tran_parsed_acct_num, 1, 1) = '6' THEN 'DS'
			ELSE ''
			END
		)
		WHEN pst.client_payment_type_cd IN ('S', 'P')
		THEN 'SP'
		ELSE ''
		END
	) card_type,
	tr.tran_parsed_acct_num card_number
	FROM device.device dev
	JOIN (
	    SELECT
	    dev.device_id
	    FROM device.device dev
	    WHERE dev.device_type_id = 4
	    UNION
	    SELECT
	    ds.device_id
	    FROM device.device_setting ds
	    WHERE ds.device_setting_parameter_cd = 'HostType'
	    AND ds.device_setting_value = 'SONY'
	) sdev
	ON sdev.device_id = dev.device_id
	JOIN pss.pos pos
	ON pos.device_id = dev.device_id
	JOIN location.location loc
	ON pos.location_id = loc.location_id
	JOIN location.customer cus
	ON pos.customer_id = cus.customer_id
	JOIN pss.pos_pta pp
	ON pp.pos_id = pos.pos_id
	JOIN pss.payment_subtype pst
	ON pst.payment_subtype_id = pp.payment_subtype_id
	JOIN pss.client_payment_type cpt
	ON cpt.client_payment_type_cd = pst.client_payment_type_cd
	JOIN pss.tran tr
	ON tr.pos_pta_id = pp.pos_pta_id
	AND tr.tran_start_ts >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
	AND tr.tran_start_ts < TRUNC(SYSDATE, 'MM')
	JOIN pss.auth au
	ON au.tran_id = tr.tran_id
	AND au.auth_type_cd IN ('U', 'S')
) trd

);
