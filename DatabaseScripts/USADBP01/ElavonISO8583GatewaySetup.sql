DROP INDEX PSS.UDX_MERCHANT_1;
CREATE INDEX PSS.IDX_MERCHANT_MRCH_CD_AUTH_ID ON PSS.MERCHANT(MERCHANT_CD, AUTHORITY_ID) TABLESPACE PSS_INDX ONLINE;

INSERT INTO authority.handler(handler_name, handler_class)
VALUES('Elavon ISO8583', 'Authority::ISO8583::Elavon');

INSERT INTO authority.authority_type(authority_type_name, authority_type_desc, handler_id)
SELECT 'Elavon ISO8583', 'Elavon Merchant Services', handler_id FROM authority.handler WHERE handler_name = 'Elavon ISO8583';

INSERT INTO authority.authority(authority_name, authority_type_id, authority_service_id, terminal_capture_flag)
SELECT 'Elavon ISO8583', authority_type_id, 1, 'Y' FROM authority.authority_type WHERE authority_type_name = 'Elavon ISO8583';

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'Elavon ISO8583 Gateway 1', '127.0.0.1', 9100, 1, handler_id FROM authority.handler WHERE handler_name = 'Elavon ISO8583';

INSERT INTO authority.authority_gateway(authority_gateway_name, authority_gateway_addr, authority_gateway_port, authority_gateway_priority, handler_id)
SELECT 'Elavon ISO8583 Gateway 2', '127.0.0.1', 9100, 2, handler_id FROM authority.handler WHERE handler_name = 'Elavon ISO8583';

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'Elavon ISO8583 Server 1', '127.0.0.1', 1, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Elavon ISO8583 Gateway 1';

INSERT INTO authority.authority_server(authority_server_name, authority_server_addr, authority_server_priority, authority_gateway_id)
SELECT 'Elavon ISO8583 Server 2', '127.0.0.1', 2, authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Elavon ISO8583 Gateway 2';

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
VALUES(
	(SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'Elavon ISO8583'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Elavon ISO8583 Gateway 1')
);

INSERT INTO authority.authority_type_gateway(authority_type_id, authority_gateway_id)
VALUES(
	(SELECT authority_type_id FROM authority.authority_type WHERE authority_type_name = 'Elavon ISO8583'),
	(SELECT authority_gateway_id FROM authority.authority_gateway WHERE authority_gateway_name = 'Elavon ISO8583 Gateway 2')
);

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id)
SELECT REPLACE(payment_subtype_name, ' - USA', ' - Elavon - USA'), 'Authority::ISO8583::Elavon', payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id
FROM pss.payment_subtype WHERE payment_subtype_class = 'Authority::ISO8583::FHMS' AND payment_subtype_name NOT LIKE '% - Deprecated';

COMMIT;
