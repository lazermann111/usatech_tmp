CREATE TABLE UITOOL.DA_SESSION
  (
    DA_SESSION_ID          VARCHAR2(32),
    DA_SESSION_HASH        VARCHAR2(192),
    DA_SESSION_EXPIRES_TS  DATE            not null,
    CREATED_BY             VARCHAR2(30)    not null,
    CREATED_TS             DATE            not null,
    LAST_UPDATED_BY        VARCHAR2(30)    not null,
    LAST_UPDATED_TS        DATE            not null,
    CONSTRAINT PK_DA_SESSION primary key(DA_SESSION_ID) USING INDEX TABLESPACE UITOOL_INDX
  )
  TABLESPACE UITOOL_DATA;
CREATE PUBLIC SYNONYM DA_SESSION FOR UITOOL.DA_SESSION;
REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER UITOOL.TRBI_DA_SESSION BEFORE INSERT ON UITOOL.DA_SESSION
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER UITOOL.TRBU_DA_SESSION BEFORE UPDATE ON UITOOL.DA_SESSION
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE UITOOL.DA_SESSION is 'Session state data for Device Admin.';
GRANT SELECT, INSERT, UPDATE, DELETE ON UITOOL.DA_SESSION TO web_user;
