-- run as system user

create tablespace UITOOL_DATA
datafile '/u03/oradata/USADBD02/uitool_data01.dbf' size 5M autoextend on
extent management local
uniform size 1M
;


create tablespace UITOOL_INDX
datafile '/u04/oradata/USADBD02/uitool_indx01.dbf' size 5M autoextend on
extent management local
uniform size 1M
;


create user uitool identified by uitool
default tablespace UITOOL_DATA
temporary tablespace TEMPTS1
quota unlimited on UITOOL_DATA quota unlimited on UITOOL_INDX;


grant create session to UITOOL;

-- grant other privs
