--sequences
CREATE SEQUENCE SEQ_DEVICE_SERIAL_FORMAT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
CREATE SEQUENCE SEQ_DEVICE_SERIAL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
CREATE SEQUENCE SEQ_DEVICE_SERIAL_INFO_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

-- tables
CREATE TABLE DEVICE.DEVICE_SERIAL_FORMAT
  (
    DEVICE_SERIAL_FORMAT_ID        NUMBER(20,0),
    DEVICE_TYPE_ID                 NUMBER(20,0)    not null,
    DEVICE_SERIAL_FORMAT_NAME      VARCHAR2(60)    not null,
    DEVICE_SERIAL_FORMAT_DESC      VARCHAR2(255),
    DEVICE_SERIAL_FORMAT_PREFIX    VARCHAR2(60),
    DEVICE_SERIAL_FORMAT_SUFFIX    VARCHAR2(60),
    DEVICE_SERIAL_FORMAT_LENGTH    NUMBER(2,0)     not null,
    DEVICE_SERIAL_FORMAT_NUM_BASE  NUMBER(2,0)     default 10 not null,
    CREATED_BY                     VARCHAR2(30)    not null,
    CREATED_TS                     DATE            not null,
    LAST_UPDATED_BY                VARCHAR2(30)    not null,
    LAST_UPDATED_TS                DATE            not null,
    CONSTRAINT PK_DEVICE_SERIAL_FORMAT primary key(DEVICE_SERIAL_FORMAT_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT UK_DEVICE_SERIAL_FORMAT_2 unique(DEVICE_TYPE_ID,DEVICE_SERIAL_FORMAT_PREFIX,DEVICE_SERIAL_FORMAT_SUFFIX,DEVICE_SERIAL_FORMAT_LENGTH),
    CONSTRAINT FK_DEVICE_SERIAL_FORMAT_DT_ID foreign key(DEVICE_TYPE_ID) references DEVICE_TYPE(DEVICE_TYPE_ID)
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_DEVICE_SERIAL_FORMAT BEFORE INSERT ON DEVICE_SERIAL_FORMAT
  FOR EACH ROW 
BEGIN
	IF :NEW.DEVICE_SERIAL_FORMAT_ID IS NULL
	THEN
		SELECT SEQ_DEVICE_SERIAL_FORMAT_ID.NEXTVAL
		INTO :NEW.DEVICE_SERIAL_FORMAT_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_DEVICE_SERIAL_FORMAT BEFORE UPDATE ON DEVICE_SERIAL_FORMAT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE DEVICE_SERIAL_FORMAT is 'Contains pre-allocated serial numbers per device type.';

--create table
CREATE TABLE DEVICE.DEVICE_SERIAL_INFO
  (
    DEVICE_SERIAL_INFO_ID        NUMBER(20,0),
    DEVICE_SERIAL_INFO_EMAIL     VARCHAR2(255)    not null,
    DEVICE_SERIAL_INFO_DESC      VARCHAR2(4000)   not null,
    DEVICE_SERIAL_INFO_ALLOC_TS  DATE             default SYSDATE not null,
    CREATED_BY                   VARCHAR2(30)     not null,
    CREATED_TS                   DATE             not null,
    LAST_UPDATED_BY              VARCHAR2(30)     not null,
    LAST_UPDATED_TS              DATE             not null,
    CONSTRAINT PK_DEVICE_SERIAL_INFO primary key(DEVICE_SERIAL_INFO_ID) USING INDEX TABLESPACE DEVICE_INDX
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_DEVICE_SERIAL_INFO BEFORE INSERT ON DEVICE_SERIAL_INFO
  FOR EACH ROW 
BEGIN
	IF :NEW.DEVICE_SERIAL_INFO_ID IS NULL
	THEN
		SELECT SEQ_DEVICE_SERIAL_INFO_ID.NEXTVAL
		INTO :NEW.DEVICE_SERIAL_INFO_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_DEVICE_SERIAL_INFO BEFORE UPDATE ON DEVICE_SERIAL_INFO
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE DEVICE_SERIAL_INFO is 'Contains details of allocated serial numbers, including an e-mail address of the creator and a description of the reason for the allocation.';

--create table
CREATE TABLE DEVICE.DEVICE_SERIAL
  (
    DEVICE_SERIAL_ID         NUMBER(20,0),
    DEVICE_SERIAL_FORMAT_ID  NUMBER(20,0)    not null,
    DEVICE_SERIAL_INFO_ID    NUMBER(20,0)    not null,
    DEVICE_SERIAL_CD         VARCHAR2(255)   not null,
    CREATED_BY               VARCHAR2(30)    not null,
    CREATED_TS               DATE            not null,
    LAST_UPDATED_BY          VARCHAR2(30)    not null,
    LAST_UPDATED_TS          DATE            not null,
    CONSTRAINT PK_DEVICE_SERIAL primary key(DEVICE_SERIAL_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT FK_DEVICE_SERIAL_DSF_ID foreign key(DEVICE_SERIAL_FORMAT_ID) references DEVICE_SERIAL_FORMAT(DEVICE_SERIAL_FORMAT_ID),
    CONSTRAINT FK_DEVICE_SERIAL_DSI_ID foreign key(DEVICE_SERIAL_INFO_ID) references DEVICE_SERIAL_INFO(DEVICE_SERIAL_INFO_ID)
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_DEVICE_SERIAL BEFORE INSERT ON DEVICE_SERIAL
  FOR EACH ROW 
BEGIN
	IF :NEW.DEVICE_SERIAL_ID IS NULL
	THEN
		SELECT SEQ_DEVICE_SERIAL_ID.NEXTVAL
		INTO :NEW.DEVICE_SERIAL_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_DEVICE_SERIAL BEFORE UPDATE ON DEVICE_SERIAL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE DEVICE_SERIAL is 'Contains basic format of serial numbers to allow for generation of new sets of serial numbers per device type.';

grant select, insert, update on DEVICE.DEVICE_SERIAL_FORMAT to web_user;
