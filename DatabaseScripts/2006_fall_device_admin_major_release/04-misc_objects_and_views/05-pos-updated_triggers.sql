-- Start of DDL Script for Trigger PSS.TRBU_POS
-- Generated 11/6/2006 1:46:04 PM from PSS@USADBD02.USATECH.COM

CREATE OR REPLACE TRIGGER pss.trbu_pos
 BEFORE
  UPDATE
 ON pss.pos
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   
   -- check to see if we are trying to update the location_id
   IF :NEW.location_id != :OLD.location_id THEN   
      IF :NEW.location_id < 0 THEN
         -- allow a backdoor to this with negative location_id (convert to aboslute val)
		 SELECT ABS(:NEW.location_id) INTO :NEW.location_id FROM DUAL;
      ELSE
	     -- check to see if we are trying to update a linked location_id
		 IF :OLD.location_id != 1 THEN 
		    SELECT :OLD.location_id INTO :NEW.location_id FROM DUAL;
		 END IF;
	  END IF;
   END IF;
   
   -- check to see if we are trying to update the customer_id
   IF :NEW.customer_id != :OLD.customer_id THEN   
      IF :NEW.customer_id < 0 THEN
         -- allow a backdoor to this with negative customer_id (convert to aboslute val)
		 SELECT ABS(:NEW.customer_id) INTO :NEW.customer_id FROM DUAL;
      ELSE
	     -- check to see if we are trying to update a linked customer_id
		 IF :OLD.customer_id != 1 THEN 
		    SELECT :OLD.customer_id INTO :NEW.customer_id FROM DUAL;
		 END IF;
	  END IF;
   END IF;
   
   -- check to see if we are trying to update the device_id
   IF :NEW.device_id != :OLD.device_id THEN   
      IF :NEW.device_id < 0 THEN
         -- allow a backdoor to this with negative device_id (convert to aboslute val)
		 SELECT ABS(:NEW.device_id) INTO :NEW.device_id FROM DUAL;
      ELSE
	     -- check to see if we are trying to update a linked device_id
		 IF :OLD.device_id IS NOT NULL THEN
		    SELECT :OLD.device_id INTO :NEW.device_id FROM DUAL;
		 END IF;
	  END IF;
   END IF;
   
   
   SELECT :OLD.created_by, :OLD.created_ts, SYSDATE,
          USER
     INTO :NEW.created_by, :NEW.created_ts, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
     
END;
/


-- End of DDL Script for Trigger PSS.TRBU_POS
