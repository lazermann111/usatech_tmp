-- Start of DDL Script for View DEVICE.VW_DEVICE_LAST_ACTIVE
-- Generated 10/31/2006 2:56:52 PM from DEVICE@USADBD02.USATECH.COM

CREATE OR REPLACE VIEW device.vw_device_last_active (
   device_id,
   device_name,
   device_serial_cd,
   created_by,
   created_ts,
   last_updated_by,
   last_updated_ts,
   device_type_id,
   device_active_yn_flag,
   encryption_key,
   last_activity_ts,
   device_desc,
   device_client_serial_cd )
AS
SELECT device_id, device_name, device_serial_cd, created_by,
       created_ts, last_updated_by, last_updated_ts, device_type_id,
       device_active_yn_flag, encryption_key, last_activity_ts,
       device_desc, device_client_serial_cd
  FROM device
 WHERE device_active_yn_flag = 'Y'
UNION ALL
SELECT d.device_id, d.device_name, d.device_serial_cd, d.created_by,
       d.created_ts, d.last_updated_by, d.last_updated_ts, d.device_type_id,
       d.device_active_yn_flag, d.encryption_key, d.last_activity_ts,
       d.device_desc, d.device_client_serial_cd
  FROM device d,
       (SELECT   dx.device_id
            FROM device dx,
                 (SELECT   device_name,
                           SUM (CASE device_active_yn_flag
                                   WHEN 'Y'
                                      THEN 1
                                   ELSE 0
                                END
                               ) y_cnt                    /*,
                                               SUM (CASE device_active_yn_flag
                                                       WHEN 'N'
                                                          THEN 1
                                                       ELSE 0
                                                    END
                                                   ) n_cnt*/
                      FROM device
                  GROUP BY device_name) x
           WHERE dx.device_name = x.device_name AND x.y_cnt = 0
        ORDER BY last_activity_ts DESC) y
 WHERE d.device_id = y.device_id
/

-- Create synonym VW_DEVICE_LAST_ACTIVE
CREATE PUBLIC SYNONYM vw_device_last_active
  FOR device.vw_device_last_active
/

-- End of DDL Script for View DEVICE.VW_DEVICE_LAST_ACTIVE

grant select on device.vw_device_last_active to web_user;
