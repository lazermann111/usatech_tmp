CREATE OR REPLACE VIEW location.vw_location_hierarchy (
   ancestor_location_id,
   descendent_location_id,
   depth,
   isroot,
   isleaf,
   hierarchy_path,
   path_separator )
AS
SELECT CONNECT_BY_ROOT LOCATION_ID ANCESTOR_LOCATION_ID,
       LOCATION_ID DESCENDENT_LOCATION_ID,
       LEVEL - 1 DEPTH,
       DECODE(parent_location_id, NULL, 1, 0),
       CONNECT_BY_ISLEAF ISLEAF,
       SYS_CONNECT_BY_PATH(LOCATION_NAME, ''),
       ''
FROM LOCATION
CONNECT BY PRIOR LOCATION_ID = PARENT_LOCATION_ID;
