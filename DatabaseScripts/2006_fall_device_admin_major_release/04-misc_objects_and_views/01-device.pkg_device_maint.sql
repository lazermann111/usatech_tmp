-- Start of DDL Script for Package DEVICE.PKG_DEVICE_MAINT
-- Generated 10/2/2006 1:40:08 PM from DEVICE@USADBD02.USATECH.COM

CREATE OR REPLACE 
PACKAGE device.pkg_device_maint
IS

PROCEDURE SP_CREATE_NEW_POS_ID
(
    pn_old_pos_id       IN       pos.pos_id%TYPE,
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_location_id      IN       location.location_id%TYPE,
    pn_customer_id      IN       customer.customer_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_ASSIGN_NEW_DEVICE_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_UPDATE_LOC_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_location_id  IN       location.location_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_UPDATE_CUST_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_customer_id  IN       customer.customer_id%TYPE,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

PROCEDURE SP_UPDATE_LOC_ID_CUST_ID
(
    pn_old_device_id    IN       device.device_id%TYPE,
    pn_new_location_id  IN       location.location_id%TYPE := -1,
    pn_new_customer_id  IN       customer.customer_id%TYPE := -1,
    pn_new_device_id    OUT      device.device_id%TYPE,
    pn_new_pos_id       OUT      pos.pos_id%TYPE,
    pn_return_code      OUT      exception_code.exception_code_id%TYPE,
    pv_error_message    OUT      exception_data.additional_information%TYPE
);

END;
/


CREATE OR REPLACE 
PACKAGE BODY               device.pkg_device_maint IS
   e_device_id_notfound         EXCEPTION;
   e_location_id_notfound       EXCEPTION;
   e_customer_id_notfound       EXCEPTION;
   e_pos_id_notfound            EXCEPTION;
   cv_package_name              CONSTANT VARCHAR2 (50) := 'PKG_DEVICE_MAINT';
   
     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    MDH       24-NOV-2001    Intial Creation
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
    JKB       11-JUL-2006    does not exist (replaced by SP_ASSIGN_NEW_DEVICE_ID ??)
   *******************************************************************************/
   
   

     /*******************************************************************************
    Function Name: SP_CREATE_NEW_POS_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/
   
   PROCEDURE sp_create_new_pos_id (
      pn_old_pos_id       IN       pos.pos_id%TYPE,
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_location_id      IN       location.location_id%TYPE,
      pn_customer_id      IN       customer.customer_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_CREATE_NEW_POS_ID';
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_new_pos_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_pos_id IS NULL THEN
         RAISE e_pos_id_notfound;
      END IF;
      
      -- this should never happen
      IF pn_customer_id IS NULL OR pn_customer_id < 1 THEN
         RAISE e_customer_id_notfound;
      END IF;
      
      -- this should never happen
      IF pn_location_id IS NULL OR pn_location_id < 1 THEN
         RAISE e_customer_id_notfound;
      END IF;
      
      
      -- check to make sure the old device exists
      -- and set the old device to inactive
      UPDATE device
         SET device_active_yn_flag = 'N'
       WHERE device_id = pn_old_device_id;
       
      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_device_id_notfound;
      END IF;
     
      -- create the new device record by copying old device record
      SELECT seq_device_id.NEXTVAL
        INTO pn_new_device_id
        FROM DUAL;

      INSERT INTO device
                  (device_id,
                   device_name,
                   device_serial_cd,
                   device_type_id,
                   encryption_key,
                   device_active_yn_flag,
                   device_client_serial_cd
                  )
         SELECT pn_new_device_id,
                device_name,
                device_serial_cd,
                device_type_id,
                encryption_key,
                'Y',
                device_client_serial_cd
           FROM device
          WHERE device_id = pn_old_device_id;      
             

      -- copy and create new POS and POS_Payment_Type_Authority records
      SELECT pss.seq_pos_id.NEXTVAL
        INTO pn_new_pos_id
        FROM DUAL;

      INSERT INTO pos
                  (
                   pos_id,
                   location_id,
                   customer_id,
                   device_id
                  )
         VALUES
         (
                pn_new_pos_id,
                pn_location_id,
                pn_customer_id,
                pn_new_device_id
         );

      INSERT INTO pos_pta
                  (pos_id,
					payment_subtype_id,
					pos_pta_encrypt_key,
					pos_pta_deactivation_ts,
					created_by,
					created_ts,
					last_updated_by,
					last_updated_ts,
					payment_subtype_key_id,
					pos_pta_regex,
					pos_pta_regex_bref,
					pos_pta_activation_ts,
					pos_pta_device_serial_cd,
					pos_pta_pin_req_yn_flag,
					pos_pta_encrypt_key2,
					authority_payment_mask_id,
					pos_pta_priority,
					terminal_id,
					merchant_bank_acct_id,
					currency_cd,
					pos_pta_passthru_allow_yn_flag
                  )
         SELECT pn_new_pos_id,
				payment_subtype_id,
				pos_pta_encrypt_key,
				pos_pta_deactivation_ts,
				created_by,
				created_ts,
				last_updated_by,
				last_updated_ts,
				payment_subtype_key_id,
				pos_pta_regex,
				pos_pta_regex_bref,
				pos_pta_activation_ts,
				pos_pta_device_serial_cd,
				pos_pta_pin_req_yn_flag,
				pos_pta_encrypt_key2,
				authority_payment_mask_id,
				pos_pta_priority,
				terminal_id,
				merchant_bank_acct_id,
				currency_cd,
				pos_pta_passthru_allow_yn_flag
           FROM pos_pta
          WHERE pos_id = pn_old_pos_id;
          
      
      -- change any waiting outgoing file transfers over to this new device
      UPDATE device_file_transfer
         SET device_id = pn_new_device_id
       WHERE device_id = pn_old_device_id
         AND device_file_transfer_direct = 'O'
         AND device_file_transfer_status_cd = 0;

      -- copy any existing device settings to the new device
      INSERT INTO device_setting
                  (device_id,
                   device_setting_parameter_cd,
                   device_setting_value
                  )
         SELECT pn_new_device_id,
                device_setting_parameter_cd,
                device_setting_value
           FROM device_setting
          WHERE device_setting.device_id = pn_old_device_id;
          
 
   EXCEPTION
      WHEN e_pos_id_notfound THEN
         pv_error_message := 'The pos_id = ' || pn_old_pos_id ||
               ' , could not be found in the pos table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
      WHEN e_location_id_notfound THEN
         pv_error_message := 'The location_id = ' || pn_location_id || 
               ' , could not be found in the location table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
      WHEN e_customer_id_notfound THEN
         pv_error_message := 'The customer_id = ' || pn_customer_id || 
               ' , could not be found in the customer table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
   END;
   

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_DEVICE_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    ???       ??-???-????    converted from SP_ASSIGN_NEW_ROOM_CNTRL_ID ??
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
    JKB       11-JUL-2006    Updated to use SP_CREATE_NEW_POS_ID
   *******************************************************************************/   
   
   PROCEDURE sp_assign_new_device_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      n_old_pos_id                 pos.pos_id%TYPE;
      n_location_id                location.location_id%TYPE;
      n_customer_id                customer.customer_id%TYPE;
      n_max_old_pos_date           pos.pos_activation_ts%TYPE;
      n_new_pos_id                 pos.pos_id%TYPE;
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_ASSIGN_NEW_DEVICE_ID';
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;
      
      
      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;
      
      -- get the max pos activation time stamp for this device
      SELECT MAX(pos.pos_activation_ts)
        INTO n_max_old_pos_date
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      -- get the old pos_id, location_id and customer_id        
      SELECT pos.pos_id, pos.location_id, pos.customer_id
        INTO n_old_pos_id, n_location_id, n_customer_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id
         AND pos.pos_activation_ts = n_max_old_pos_date;
       
      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_pos_id_notfound;
      END IF;
      
      
      sp_create_new_pos_id(n_old_pos_id, pn_old_device_id, n_location_id,
                           n_customer_id, pn_new_device_id, n_new_pos_id, 
                           pn_return_code, pv_error_message);
                           
                           
   EXCEPTION
   
      WHEN e_pos_id_notfound THEN
         pv_error_message := 'A pos_id could not be found in the pos table for the device_id = ' || 
               pn_old_device_id || '.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution; 
         
      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
   END;
   

     /*******************************************************************************
    Function Name: SP_UPDATE_LOC_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/   
   
   PROCEDURE sp_update_loc_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_location_id  IN       location.location_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_LOC_ID';
   BEGIN
      
      sp_update_loc_id_cust_id(pn_old_device_id, pn_new_location_id, -1,
                               pn_new_device_id, pn_new_pos_id, 
                               pn_return_code, pv_error_message);
   
   END;
   

     /*******************************************************************************
    Function Name: SP_UPDATE_CUST_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/   
   
   PROCEDURE sp_update_cust_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_customer_id  IN       customer.customer_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_CUST_ID';
   BEGIN
      
      sp_update_loc_id_cust_id(pn_old_device_id, -1, pn_new_customer_id,
                               pn_new_device_id, pn_new_pos_id, 
                               pn_return_code, pv_error_message);
   
   END;
   

     /*******************************************************************************
    Function Name: SP_UPDATE_CUST_ID_LOC_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/   
   
   PROCEDURE sp_update_loc_id_cust_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_location_id  IN       location.location_id%TYPE := -1,
      pn_new_customer_id  IN       customer.customer_id%TYPE := -1,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      n_old_device_id              device.device_id%TYPE;
      n_location_id                location.location_id%TYPE;
      n_customer_id                customer.customer_id%TYPE;
      n_old_pos_id                 pos.pos_id%TYPE;
      n_max_old_pos_date           pos.pos_activation_ts%TYPE;
      b_create_new_pos             BOOLEAN;
      b_update_pos                 BOOLEAN;
      n_tran_count                 NUMBER;
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_CUST_ID_LOC_ID';
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_new_pos_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;
      
      b_create_new_pos := FALSE;
      b_update_pos := FALSE;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;
      
      -- this should never happen
      IF pn_new_customer_id IS NULL THEN
         RAISE e_customer_id_notfound;
      END IF;
      
      -- this should never happen
      IF pn_new_location_id IS NULL THEN
         RAISE e_location_id_notfound;
      END IF;
      
      
      -- get the max pos activation time stamp for this device
      SELECT MAX(pos.pos_activation_ts)
        INTO n_max_old_pos_date
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      -- get the old pos_id, location_id and customer_id        
      SELECT pos.pos_id, pos.location_id, pos.customer_id
        INTO n_old_pos_id, n_location_id, n_customer_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id
         AND pos.pos_activation_ts = n_max_old_pos_date;
       
      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_pos_id_notfound;
      END IF;

      
      -- check if updating customer
      IF pn_new_customer_id != -1 THEN
      
         IF n_customer_id != pn_new_customer_id THEN
         	
			b_update_pos := TRUE;
         
            IF n_customer_id != 1 THEN
               b_create_new_pos := TRUE;
            END IF;
         
            -- check to make sure this customer exists
            SELECT customer_id
              INTO n_customer_id
              FROM location.customer
             WHERE customer_id = pn_new_customer_id;
          
            IF SQL%NOTFOUND = TRUE THEN
               RAISE e_customer_id_notfound;
            END IF;
                       
         END IF;
          
      END IF;
      
      -- check if updating location
      IF pn_new_location_id != -1 THEN
         
         IF n_location_id != pn_new_location_id THEN
         	
			b_update_pos := TRUE;
         
            IF n_location_id != 1 THEN
               b_create_new_pos := TRUE;
            END IF;
         
            -- check to make sure this location exists
            SELECT location_id
              INTO n_location_id
              FROM location.location
             WHERE location_id = pn_new_location_id;
          
            IF SQL%NOTFOUND = TRUE THEN
               RAISE e_location_id_notfound;
            END IF;
                       
         END IF;
         
      END IF;
      
      -- check to see if we really need a new pos (i.e. does it have transactions)
      IF b_create_new_pos THEN
      
         SELECT COUNT(tran.tran_id) tran_count
           INTO n_tran_count
           FROM tran, pos_pta
          WHERE tran.pos_pta_id = pos_pta.pos_pta_id
			AND pos_pta.pos_id = n_old_pos_id;
			
		 IF n_tran_count = 0 THEN
		 
		     b_create_new_pos := FALSE;
		     
		     -- we are going to make these negative to use the backdoor on the
		     -- pos table trigger (bu)
		     n_location_id := -(ABS(n_location_id));
		     n_customer_id := -(ABS(n_customer_id));
		     
		 END IF;	
      
      END IF;
      
      
      -- do the correct update
      
      IF b_create_new_pos THEN
         
         sp_create_new_pos_id(n_old_pos_id, pn_old_device_id, 
                              n_location_id, n_customer_id, 
                              pn_new_device_id, pn_new_pos_id, 
                              pn_return_code, pv_error_message);
         
      ELSE
         
         pn_new_device_id := pn_old_device_id;
         pn_new_pos_id := n_old_pos_id;
         
         -- double check that we have to do anything 
         -- (don't update unless something has changed)
         IF b_update_pos THEN
         
            UPDATE pos
               SET pos.location_id = n_location_id,
                   pos.customer_id = n_customer_id
             WHERE pos.pos_id = n_old_pos_id;
             
         END IF;
          
      END IF;
      
      
   EXCEPTION
   
      WHEN e_pos_id_notfound THEN
         pv_error_message := 'A pos_id could not be found in the pos table for the device_id = ' || 
               pn_old_device_id || '.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution; 
         
      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
      WHEN e_location_id_notfound THEN
         pv_error_message := 'The location_id = ' || pn_new_location_id || 
               ' , could not be found in the location table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
         
      WHEN e_customer_id_notfound THEN
         pv_error_message := 'The customer_id = ' || pn_new_customer_id || 
               ' , could not be found in the customer table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;   
      
         
   END;

END;
/


-- End of DDL Script for Package DEVICE.PKG_DEVICE_MAINT
