CREATE OR REPLACE VIEW pss.vw_pos_active (
   pos_id,
   location_id,
   customer_id,
   device_id,
   created_by,
   created_ts,
   last_updated_by,
   last_updated_ts,
   pos_active_yn_flag,
   pos_activation_ts )
AS
SELECT p.pos_id, p.location_id, p.customer_id, p.device_id, p.created_by,
       p.created_ts, p.last_updated_by, p.last_updated_ts,
       p.pos_active_yn_flag, p.pos_activation_ts
  FROM pos p,
       (SELECT   MAX (pos_activation_ts) pos_activation_ts, location_id,
                 customer_id, device_id
            FROM pos
        GROUP BY location_id, customer_id, device_id) x
 WHERE p.location_id = x.location_id
   AND p.customer_id = x.customer_id
   AND p.device_id = x.device_id
   AND p.pos_activation_ts = x.pos_activation_ts;

grant select on pss.vw_pos_active to web_user;
