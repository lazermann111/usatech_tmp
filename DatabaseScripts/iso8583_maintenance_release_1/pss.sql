-- state descriptive field content changes
UPDATE auth_result SET AUTH_RESULT_DESC = 'Successful' WHERE AUTH_RESULT_CD = 'Y';
UPDATE auth_result SET AUTH_RESULT_DESC = 'Declined' WHERE AUTH_RESULT_CD = 'N';
UPDATE auth_result SET AUTH_RESULT_DESC = 'Partial Authorization' WHERE AUTH_RESULT_CD = 'P';
UPDATE auth_result SET AUTH_RESULT_DESC = 'Failed' WHERE AUTH_RESULT_CD = 'F';
UPDATE auth_result SET AUTH_RESULT_DESC = 'Declined Permanently' WHERE AUTH_RESULT_CD = 'O';

UPDATE auth_type SET AUTH_TYPE_DESC = 'Local Auth'      WHERE AUTH_TYPE_CD = 'L';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Live Auth'       WHERE AUTH_TYPE_CD = 'N';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Pre-Authed Sale' WHERE AUTH_TYPE_CD = 'U';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Sale'            WHERE AUTH_TYPE_CD = 'S';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Offline Sale'    WHERE AUTH_TYPE_CD = 'O';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Auth Adjustment' WHERE AUTH_TYPE_CD = 'A';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Sale Adjustment' WHERE AUTH_TYPE_CD = 'D';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Auth Reversal'   WHERE AUTH_TYPE_CD = 'C';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Sale Reversal'   WHERE AUTH_TYPE_CD = 'E';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Auth Void'       WHERE AUTH_TYPE_CD = 'V';
UPDATE auth_type SET AUTH_TYPE_DESC = 'Sale Void'       WHERE AUTH_TYPE_CD = 'I';

grant select, insert, update on pss.terminal_state to web_user;
