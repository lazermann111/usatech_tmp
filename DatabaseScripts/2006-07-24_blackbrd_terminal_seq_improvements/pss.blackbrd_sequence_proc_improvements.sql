-- Start of DDL Script for Package PSS.RANDOM
-- Generated 21-Jul-2006 12:00:53 from PSS@USADBD02.USATECH.COM

CREATE OR REPLACE 
PACKAGE pss.pkg_random IS

  -- Returns random integer between [0, r-1]
  FUNCTION rndint(r IN NUMBER) RETURN NUMBER;

  -- Returns random real between [0, 1]
  FUNCTION rndflt RETURN NUMBER;

END;
/


CREATE OR REPLACE 
PACKAGE BODY     pss.pkg_random IS
-- Purpose: Linear congruential algorithm random num generation.
--
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ---------    ------      ---------------------------------------
-- erybski      06.07.21    Initial release.

  m         CONSTANT NUMBER:=100000000;  /* initial conditions */
  m1        CONSTANT NUMBER:=10000;      /* (for best results) */
  b         CONSTANT NUMBER:=31415821;   /*      */
  a         NUMBER;                      /* seed */
  the_date  DATE;                        /*      */
  days      NUMBER;                      /* for generating initial seed */
  secs      NUMBER;                      /*      */

  -- ------------------------
  -- Private utility FUNCTION
  -- ------------------------
  FUNCTION mult(p IN NUMBER, q IN NUMBER) RETURN NUMBER IS
    p1     NUMBER; 
    p0     NUMBER; 
    q1     NUMBER; 
    q0     NUMBER; 
  BEGIN 
    p1:=TRUNC(p/m1); 
    p0:=MOD(p,m1); 
    q1:=TRUNC(q/m1); 
    q0:=MOD(q,m1); 
    RETURN(MOD((MOD(p0*q1+p1*q0,m1)*m1+p0*q0),m)); 
  END;

  -- ---------------------------------------
  -- Returns random integer between [0, r-1]
  -- ---------------------------------------
  FUNCTION rndint (r IN NUMBER) RETURN NUMBER IS 
  BEGIN 
    -- Generate a random NUMBER, and set it to be the new seed
    a:=MOD(mult(a,b)+1,m); 

    -- Convert it to integer between [0, r-1] and return it
    RETURN(TRUNC((TRUNC(a/m1)*r)/m1));
  END;
 
  -- ----------------------------------
  -- Returns random real between [0, 1]
  -- ----------------------------------
  FUNCTION rndflt RETURN NUMBER IS
    BEGIN
      -- Generate a random NUMBER, and set it to be the new seed
      a:=MOD(mult(a,b)+1,m);
      RETURN(a/m);
    END;

BEGIN
  -- Generate initial seed "a" based on system date
  the_date:=SYSDATE;
  days:=TO_NUMBER(TO_CHAR(the_date, 'J'));
  secs:=TO_NUMBER(TO_CHAR(the_date, 'SSSSS'));
  a:=days*24*3600+secs;
END;
/


-- End of DDL Script for Package PSS.RANDOM

-- Start of DDL Script for Procedure PSS.SP_NXT_BLACKBRD_SEQUENCE_NUM
-- Generated 21-Jul-2006 12:02:20 from PSS@USADBD02.USATECH.COM

CREATE OR REPLACE 
PROCEDURE pss.sp_nxt_blackbrd_sequence_num (
   pn_device_id       IN       device.device_id%TYPE,
   pn_nxt_seq_num     OUT      device_counter.device_counter_value%TYPE,
   pn_return_code     OUT      exception_code.exception_code_id%TYPE,
   pv_error_message   OUT      exception_data.additional_information%TYPE
) AS
--
-- Purpose: Gets next device sequence num from device_counter
-- for a blackboard authority
--
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ---------    ------      ---------------------------------------
-- erybski      04.09.16    Initial release.
-- erybski      04.09.27    Updated variable names to reflect content meaning change
-- erybski      06.07.21    Updated to set random initial sequence num
--
   n_device_id                           NUMBER;
   v_device_counter_parameter   CONSTANT device_counter.device_counter_parameter%TYPE
                                                            := 'BLACKBRD_SEQUENCE_NUMBER';
   v_payment_subtype_key_name   CONSTANT payment_subtype.payment_subtype_key_name%TYPE
                                                                  := 'BLACKBRD_TENDER_ID';
   n_cur_seq_num                         NUMBER;
   cv_package_name              CONSTANT VARCHAR2 (30)                              := '';
   cv_procedure_name            CONSTANT VARCHAR2 (30)       := 'SP_NXT_BLACKBRD_SEQ_NUM';
   e_device_id_notfound                  EXCEPTION;

   CURSOR c_chk_device_id IS
      SELECT COUNT (d.device_id)
        FROM device.device d,
             pss.pos p,
             pss.pos_pta pp,
             pss.payment_subtype ps
       WHERE d.device_id = p.device_id
         AND p.pos_id = pp.pos_id
         AND ps.payment_subtype_id = pp.payment_subtype_id
         AND p.device_id = pn_device_id
         AND ps.payment_subtype_key_name = v_payment_subtype_key_name;

   CURSOR c_get_device_seq_num IS
      SELECT     TO_NUMBER (device_counter_value)
            FROM device.device_counter
           WHERE device_id = pn_device_id
             AND device_counter_parameter = v_device_counter_parameter
      FOR UPDATE;
BEGIN
   -- check that specified device_id exists and is configured with a blackboard authority
   pn_nxt_seq_num := NULL;

   OPEN c_chk_device_id;

   FETCH c_chk_device_id
    INTO n_device_id;

   IF n_device_id = 0 THEN
      CLOSE c_chk_device_id;

      RAISE e_device_id_notfound;
   END IF;

   CLOSE c_chk_device_id;

   -- get current blackboard sequence num
   OPEN c_get_device_seq_num;

   FETCH c_get_device_seq_num
    INTO n_cur_seq_num;

   IF c_get_device_seq_num%NOTFOUND = TRUE THEN
      -- if sequence num is not yet defined
      CLOSE c_get_device_seq_num;

      --pn_nxt_seq_num := 1;
      /* Minimizes chance of multiple devices using same authority terminal
         from starting at same number.  Required due to weak-ref blackbrd_*
         floating table architecture. */
      pn_nxt_seq_num := pss.pkg_random.rndint(999900) - pss.pkg_random.rndint(100);

      INSERT INTO device.device_counter
                  (device_counter_value,
                   device_id,
                   device_counter_parameter
                  )
           VALUES (pn_nxt_seq_num,
                   pn_device_id,
                   v_device_counter_parameter
                  );
   ELSE
      -- increment sequence num
      IF n_cur_seq_num = 999999 THEN
         pn_nxt_seq_num := 1;
      ELSE
         pn_nxt_seq_num := n_cur_seq_num + 1;
      END IF;

      UPDATE device.device_counter
         SET device_counter_value = pn_nxt_seq_num
       WHERE device_id = pn_device_id
         AND device_counter_parameter = v_device_counter_parameter;

      CLOSE c_get_device_seq_num;
   END IF;
EXCEPTION
   WHEN e_device_id_notfound THEN
      pv_error_message :=
               'Could not find the following device id in the database: ' || pn_device_id;
      pn_return_code := pkg_app_exec_hist_globals.device_id_not_found;
      pkg_exception_processor.sp_log_exception (pn_return_code,
                                                pv_error_message,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
      pn_nxt_seq_num := NULL;
   WHEN OTHERS THEN
      pv_error_message :=
         'An unknown exception occurred in sp_authorize(2) = ' || SQLCODE || ', '
         || SQLERRM;
      pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
      pkg_exception_processor.sp_log_exception (pn_return_code,
                                                pv_error_message,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
      pn_nxt_seq_num := NULL;
END;
/



-- End of DDL Script for Procedure PSS.SP_NXT_BLACKBRD_SEQUENCE_NUM
