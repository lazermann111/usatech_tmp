-- Start of DDL Script for View PSS.VW_ESUDS_TRAN
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_esuds_tran (
   tran_id,
   tran_start_ts,
   tran_auth_ts,
   tran_upload_ts,
   tran_auth_amount,
   tran_end_ts,
   tran_auth_cd,
   tran_state_cd,
   tran_desc,
   pos_id,
   authority_tran_cd,
   consumer_acct_id,
   device_tran_cd,
   pos_pta_id,
   rerix_payment_type_cd,
   tran_auth_result_cd,
   tran_auth_type_cd,
   tran_received_raw_acct_data,
   tran_parsed_acct_name,
   tran_parsed_acct_exp_date,
   tran_parsed_acct_num,
   tran_reportable_acct_num,
   tran_settlement_amount,
   tran_account_pin,
   tran_account_entry_method_cd )
AS
SELECT tr.tran_id,
       tr.tran_start_ts,
       tr.tran_auth_ts,
       tr.tran_upload_ts,
       tr.tran_auth_amount,
       tr.tran_end_ts,
       tr.tran_auth_cd,
       tr.tran_state_cd,
       tr.tran_desc,
  
       pta.pos_id,
       authority_tran_cd,
       tr.consumer_acct_id,
       tr.device_tran_cd,
       pta.pos_pta_id,
       ps.rerix_payment_type_cd,
       tr.tran_auth_result_cd,
       tr.tran_auth_type_cd,
       tr.tran_received_raw_acct_data,
       tr.tran_parsed_acct_name,
       tr.tran_parsed_acct_exp_date,
       tr.tran_parsed_acct_num,
       tr.tran_reportable_acct_num,
       tr.tran_settlement_amount,
       tr.tran_account_pin,
       tr.tran_account_entry_method_cd
  FROM tran tr, pos_pta pta, payment_subtype ps
 WHERE tr.pos_pta_id = pta.pos_pta_id
   AND pta.payment_subtype_id = ps.payment_subtype_id
/

-- Grants for View
GRANT SELECT ON vw_esuds_tran TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_ESUDS_TRAN

-- Start of DDL Script for View PSS.VW_ESUDS_TRAN_LINE_ITEM_DTL
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_esuds_tran_line_item_dtl (
   tran_id,
   tran_line_item_id,
   tran_start_ts,
   tran_line_item_desc,
   host_type_id,
   host_port_num,
   host_label_cd,
   customer_id,
   consumer_acct_id,
   tran_line_item_quantity,
   tran_line_item_amount,
   tran_line_item_tot_amount,
   campus_id,
   campus_name,
   dorm_id,
   dorm_name,
   room_id,
   room_name,
   rerix_payment_type_cd )
AS
SELECT tr.tran_id, tli.tran_line_item_id, tr.tran_start_ts,
          tli.tran_line_item_desc, HOST.host_type_id, HOST.host_port_num,
          HOST.host_label_cd, pos.customer_id, tr.consumer_acct_id,
          tli.tran_line_item_quantity,
          NVL (tli.tran_line_item_amount, 0) tran_line_item_amount,
            tli.tran_line_item_quantity
          * NVL (tli.tran_line_item_amount, 0) tran_line_item_tot_amount,
          campus.location_id campus_id, campus.location_name campus_name,
          dorm.location_id dorm_id, dorm.location_name dorm_name,
          room.location_id location_id, room.location_name room_name,
          pt.rerix_payment_type_cd
     FROM tran tr,
          tran_line_item tli,
          pos,
          device dv,
          HOST,
          location.LOCATION dorm,
          location.LOCATION room,
          location.LOCATION campus,
          payment_subtype pt,
          pos_pta pta
    WHERE tr.tran_id = tli.tran_id
      AND tr.pos_pta_id = pta.pos_pta_id
      AND pta.pos_id = pos.pos_id
      AND pos.device_id = dv.device_id
      AND HOST.device_id = dv.device_id
      AND tli.host_id = HOST.host_id
      AND pos.location_id = room.location_id
      AND room.parent_location_id = dorm.location_id
      AND dorm.parent_location_id = campus.location_id
      AND pta.payment_subtype_id = pt.payment_subtype_id
/

-- Grants for View
GRANT SELECT ON vw_esuds_tran_line_item_dtl TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_ESUDS_TRAN_LINE_ITEM_DTL

-- Start of DDL Script for View PSS.VW_LOCATION_BY_DEVICE_ID
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_location_by_device_id (
   location_id,
   location_name,
   device_id,
   device_serial_cd,
   location_timezone )
AS
select a.location_id, a.location_name, b.device_id, c.device_serial_cd, a.location_time_zone_cd
from location.location a, pss.pos b, device.device c
where a.location_id = b.location_id
and c.device_id = b.device_id
/

-- Grants for View
GRANT SELECT ON vw_location_by_device_id TO rerix_engine
/
GRANT SELECT ON vw_location_by_device_id TO web_user
WITH GRANT OPTION
/

-- End of DDL Script for View PSS.VW_LOCATION_BY_DEVICE_ID

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_CAMPUS
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_tot_sales_by_campus (
   campus_id,
   campus_name,
   school_id,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc2.location_id campus_id,
            loc2.location_name campus_name,
            loc2.parent_location_id school_id,
            ps.customer_id operator_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            SUM (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                                          --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tran_line_item,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc1,
            LOCATION loc2,
            LOCATION loc3,
            LOCATION loc4
      WHERE tran_line_item.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
   GROUP BY loc2.location_id,
            loc2.location_name,
            loc2.parent_location_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Grants for View
GRANT SELECT ON vw_tot_sales_by_campus TO esuds_student_report_user
/
GRANT SELECT ON vw_tot_sales_by_campus TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_TOT_SALES_BY_CAMPUS

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_DORM
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_tot_sales_by_dorm (
   dorm_id,
   dorm_name,
   campus_id,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc3.location_id campus_id,
            loc3.location_name campus_name,
            loc3.parent_location_id school_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            sum (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                           --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc3,
            LOCATION loc4
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
   GROUP BY loc3.location_id,
            loc3.location_name,
            loc3.parent_location_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Grants for View
GRANT SELECT ON vw_tot_sales_by_dorm TO esuds_student_report_user
/
GRANT SELECT ON vw_tot_sales_by_dorm TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_TOT_SALES_BY_DORM

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_LAUNDRY_ROOM
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_tot_sales_by_laundry_room (
   laundry_room_id,
   laundry_room_name,
   dorm_id,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc4.location_id campus_id,
            loc4.location_name campus_name,
            loc4.parent_location_id school_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            sum (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                           --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tli, tran tr, pos ps, pos_pta pta, LOCATION loc4
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
   GROUP BY loc4.location_id,
            loc4.location_name,
            loc4.parent_location_id,
            ps.customer_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Grants for View
GRANT SELECT ON vw_tot_sales_by_laundry_room TO esuds_student_report_user
/
GRANT SELECT ON vw_tot_sales_by_laundry_room TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_TOT_SALES_BY_LAUNDRY_ROOM

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_SCHOOL
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_tot_sales_by_school (
   school_id,
   school_name,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc1.location_id campus_id,
            loc1.location_name campus_name,
            ps.customer_id operator_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            sum (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                               --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc1,
            LOCATION loc2,
            LOCATION loc3,
            LOCATION loc4
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
   GROUP BY loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Grants for View
GRANT SELECT ON vw_tot_sales_by_school TO esuds_student_report_user
/
GRANT SELECT ON vw_tot_sales_by_school TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_TOT_SALES_BY_SCHOOL

-- Start of DDL Script for View PSS.VW_TRAN_LINE_ITEM_DTL
-- Generated 7/27/2004 8:18:51 AM from PSS@usadbd03

CREATE OR REPLACE VIEW vw_tran_line_item_dtl (
   tran_id,
   tran_line_item_id,
   tran_start_ts,
   tran_line_item_desc,
   host_type_id,
   host_port_num,
   host_label_cd,
   location_id,
   customer_id,
   consumer_acct_id,
   tran_line_item_quantity,
   tran_line_item_amount,
   tran_line_item_tot_amount )
AS
SELECT tr.tran_id,
       tli.tran_line_item_id,
       TR.TRAN_START_TS,
       tli.tran_line_item_desc,
       HOST.host_type_id,
       HOST.host_port_num,
       HOST.host_label_cd,
       pos.location_id,
       pos.customer_id,
       tr.consumer_acct_id,
       tli.tran_line_item_quantity,
       nvl(tli.tran_line_item_amount, 0) tran_line_item_amount,
              tli.tran_line_item_quantity*
       nvl(tli.tran_line_item_amount, 0) tran_line_item_tot_amount
  FROM tran tr,
       tran_line_item tli,
       pos,
       pos_pta pta,
       device dv,
       HOST, 
       location dorm
 WHERE tr.tran_id = tli.tran_id
        and tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = pos.pos_id
   AND pos.device_id = dv.device_id
   AND HOST.device_id = dv.device_id
   and pos.location_id = dorm.location_id
/

-- Grants for View
GRANT SELECT ON vw_tran_line_item_dtl TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_TRAN_LINE_ITEM_DTL

