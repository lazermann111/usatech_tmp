CREATE TABLE PSS.CLIENT_PAYMENT_TYPE
  (
  CLIENT_PAYMENT_TYPE_CD CHAR (1) NOT NULL,
  CLIENT_PAYMENT_TYPE_DESC VARCHAR2 (60) NOT NULL,
  CREATED_BY VARCHAR2 (30) NOT NULL,
  CREATED_TS DATE NOT NULL,
  LAST_UPDATED_BY VARCHAR2 (30) NOT NULL,
  LAST_UPDATED_TS DATE NOT NULL
 )
     TABLESPACE PSS_DATA
/

ALTER TABLE RERIX_PAYMENT_TYPE 
 ADD (
  CLIENT_PAYMENT_TYPE_CD VARCHAR2 (1) ,
  RERIX_PAYMENT_TYPE_REGEX VARCHAR2 (255)
 )
/

ALTER TABLE RERIX_PAYMENT_TYPE RENAME COLUMN RERIX_PAYMENT_TYPE_CD TO RERIX_PAYMENT_TYPE_ID;

ALTER TABLE PAYMENT_SUBTYPE RENAME COLUMN RERIX_PAYMENT_TYPE_CD TO RERIX_PAYMENT_TYPE_ID;


ALTER TABLE payment_subtype
DROP CONSTRAINT fk_payment_subtype_rerix_payme
/


TRUNCATE TABLE RERIX_PAYMENT_TYPE
/


ALTER TABLE PAYMENT_SUBTYPE   MODIFY (
  RERIX_PAYMENT_TYPE_ID NUMBER (22)

 )
 /
 

ALTER TABLE RERIX_PAYMENT_TYPE   MODIFY (
  RERIX_PAYMENT_TYPE_ID NUMBER (22)

 )
 
 /
 

 
  ALTER TABLE payment_subtype
ADD CONSTRAINT fk_payment_subtype_rerix_payme FOREIGN KEY (rerix_payment_type_id)
REFERENCES rerix_payment_type (rerix_payment_type_id)
/




CREATE OR REPLACE TRIGGER trbi_client_payment_type
 BEFORE
  INSERT
 ON client_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN


       SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
     
END;
/




CREATE OR REPLACE TRIGGER trbu_client_payment_type
 BEFORE
  UPDATE
 ON client_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE SEQUENCE PSS.SEQ_RERIX_PAYMENT_TYPE_ID
 START WITH  1
 MINVALUE  1
 NOCACHE 
/




CREATE OR REPLACE TRIGGER trbi_rerix_payment_type
 BEFORE
  INSERT
 ON rerix_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.rerix_payment_type_id IS NULL THEN

      SELECT seq_rerix_payment_type_id.nextval
        into :new.rerix_payment_type_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


grant select, update, insert, delete on client_payment_type to web_user;


    create public synonym CLIENT_PAYMENT_TYPE for pss.CLIENT_PAYMENT_TYPE;
create public synonym SEQ_RERIX_PAYMENT_TYPE_ID for pss.SEQ_RERIX_PAYMENT_TYPE_ID;
