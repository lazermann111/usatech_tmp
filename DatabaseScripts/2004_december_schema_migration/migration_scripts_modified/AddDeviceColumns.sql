ALTER TABLE DEVICE 
 ADD (
  device_client_serial_cd VARCHAR2 (20)
 )
/
COMMENT ON COLUMN DEVICE.device_client_serial_cd IS 'A unique id a customer would use to identify one of our devices to them.  Example would be and ID needed for blackboard to represent the TerminalNumber field'
/
