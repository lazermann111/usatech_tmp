-- Start of DDL Script for Package DEVICE.PKG_DEVICE_MAINT
-- Generated 11/23/2004 2:56:21 PM from DEVICE@USADBD03.USATECH.COM

-- Drop the old instance of PKG_DEVICE_MAINT
DROP PACKAGE device.pkg_device_maint
/

CREATE OR REPLACE 
PACKAGE device.pkg_device_maint
IS
   /*******************************************************************************
  Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
  Description: Descriptions of the function
  Parameters:

              Function Return Value
                 Indicates if the exception was recorded successfully.  A value of
                 0 indicates a successful execution.  A value of 1 indicates an
                 unsuccessful execution.  A Boolean value was not used because it
                 may be misinterpreted by non-Oracle systems.
  MODIFICATION HISTORY:
  Person     Date          Comments
  ---------  ----------    -------------------------------------------
  MDH       24-NOV-2001    Intial Creation
 *******************************************************************************/
PROCEDURE SP_ASSIGN_NEW_DEVICE_ID
(
    pn_old_device_id  IN       device.device_id%TYPE,
    pn_new_device_id  OUT      device.device_id%TYPE,
    pn_return_code    OUT      exception_code.exception_code_id%TYPE,
    pv_error_message            OUT        exception_data.additional_information%TYPE
);

END;
/

-- Grants for Package
GRANT EXECUTE ON device.pkg_device_maint TO web_user
WITH GRANT OPTION
/

CREATE OR REPLACE 
PACKAGE BODY device.pkg_device_maint IS
   e_device_id_notfound       EXCEPTION;
   cv_server_name    CONSTANT VARCHAR2 (30) := 'USADBD02';
   cv_package_name   CONSTANT VARCHAR2 (30) := 'PKG_DEVICE_MAINT';

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    MDH       24-NOV-2001    Intial Creation
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
   *******************************************************************************/
   PROCEDURE sp_assign_new_device_id (
      pn_old_device_id   IN       device.device_id%TYPE,
      pn_new_device_id   OUT      device.device_id%TYPE,
      pn_return_code     OUT      exception_code.exception_code_id%TYPE,
      pv_error_message   OUT      exception_data.additional_information%TYPE
   ) AS
      v_error_msg                  exception_data.additional_information%TYPE;
      n_old_device_id              device.device_id%TYPE;
      n_new_device_id              device.device_id%TYPE;
      n_old_pos_id                 pos.pos_id%TYPE;
      n_new_pos_id                 pos.pos_id%TYPE;
      b_device_id_found_flag       BOOLEAN;
      e_device_id_notfound         EXCEPTION;
      cv_procedure_name   CONSTANT VARCHAR2 (50)             := 'SP_ASSIGN_NEW_DEVICE_ID';
      cv_server_name      CONSTANT VARCHAR2 (50)                            := 'USADBD02';
      cv_package_name     CONSTANT VARCHAR2 (50)                    := 'PKG_DEVICE_MAINT';
   BEGIN
      --Initialise all variables
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- Verify the N_EXCEPTION_CODE_ID parameter contains a value stored in the EXCEPTION_CODE table.
      UPDATE device
         SET device_active_yn_flag = 'N'
       WHERE device_id = pn_old_device_id;

      -- Check if error information exists for the exception
      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_device_id_notfound;
      END IF;

      SELECT seq_device_id.NEXTVAL
        INTO n_new_device_id
        FROM DUAL;

      INSERT INTO device
                  (device_id,
                   device_name,
                   device_serial_cd,
                   device_type_id,
                   encryption_key,
                   device_active_yn_flag,
                   device_client_serial_cd
                  )
         SELECT n_new_device_id,
                device_name,
                device_serial_cd,
                device_type_id,
                encryption_key,
                'Y',
                device_client_serial_cd
           FROM device
          WHERE device_id = pn_old_device_id;

      -- Check if exception information exists for the UNKNOWN_ERROR_ID exception code id
      IF SQL%NOTFOUND = TRUE THEN
         b_device_id_found_flag := FALSE;
      END IF;

      pn_new_device_id := n_new_device_id;

      -- copy and create new POS and POS_Payment_Type_Authority records
      SELECT pos.pos_id
        INTO n_old_pos_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      SELECT pss.seq_pos_id.NEXTVAL
        INTO n_new_pos_id
        FROM DUAL;

      INSERT INTO pos
                  (pos_id,
                   location_id,
                   customer_id,
                   device_id
                  )
         SELECT n_new_pos_id,
                location_id,
                customer_id,
                n_new_device_id
           FROM pos
          WHERE pos.pos_id = n_old_pos_id;

      INSERT INTO pos_pta
                  (pos_id,
                   payment_subtype_id,
                   pos_pta_encrypt_key,
                   pos_pta_activation_ts,
                   pos_pta_deactivation_ts,
                   payment_subtype_key_id,
                   pos_pta_pin_req_yn_flag,
                   pos_pta_regex,
                   pos_pta_regex_bref,
                   pos_pta_device_serial_cd
                  )
         SELECT n_new_pos_id,
                payment_subtype_id,
                pos_pta_encrypt_key,
                pos_pta_activation_ts,
                pos_pta_deactivation_ts,
                payment_subtype_key_id,
                pos_pta_pin_req_yn_flag,
                pos_pta_regex,
                pos_pta_regex_bref,
                pos_pta_device_serial_cd
           FROM pos_pta
          WHERE pos_id = n_old_pos_id;

 /*     INSERT INTO pos_payment_type_authority
                  (pos_id,
                   authority_id,
                   payment_type_id
                  )
         SELECT n_new_pos_id,
                authority_id,
                payment_type_id
           FROM pos_payment_type_authority
          WHERE pos_id = n_old_pos_id; */

      -- change any waiting outgoing file transfers over to this new device
      UPDATE device_file_transfer
         SET device_id = n_new_device_id
       WHERE device_id = pn_old_device_id
         AND device_file_transfer_direct = 'O'
         AND device_file_transfer_status_cd = 0;

      -- copy any existing device settings to the new device
      INSERT INTO device_setting
                  (device_id,
                   device_setting_parameter_cd,
                   device_setting_value
                  )
         SELECT n_new_device_id,
                device_setting_parameter_cd,
                device_setting_value
           FROM device_setting
          WHERE device_setting.device_id = pn_old_device_id;
   EXCEPTION
      WHEN e_device_id_notfound THEN
         pv_error_message :=
               'The device_id = '
            || pn_old_device_id
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             v_error_msg,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   --pn_return_code := 0;

   --    WHEN OTHERS THEN
--
--       pv_error_message := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
--       pkg_exception_processor.sp_log_exception(PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id, v_error_msg, cv_server_name, cv_package_name || '.' || cv_procedure_name);
--       pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
--       --pn_return_code := 0;
   END;
END;
/


-- End of DDL Script for Package DEVICE.PKG_DEVICE_MAINT

-- Start of DDL Script for View LOCATION.VW_LOCATION_HIERARCHY
-- Generated 11/23/2004 3:01:44 PM from LOCATION@USADBD03.USATECH.COM

-- Drop the old instance of VW_LOCATION_HIERARCHY
DROP VIEW location.vw_location_hierarchy
/

CREATE OR REPLACE VIEW location.vw_location_hierarchy (
   ancestor_location_id,
   descendent_location_id,
   depth )
AS
SELECT LOCATION_ID ANCESTOR_LOCATION_ID, CONNECT_BY_ROOT LOCATION_ID DESCENDENT_LOCATION_ID, LEVEL - 1 DEPTH
FROM LOCATION
CONNECT BY LOCATION_ID = PRIOR PARENT_LOCATION_ID
/

-- Drop the old synonym VW_LOCATION_HIERARCHY
DROP PUBLIC SYNONYM VW_LOCATION_HIERARCHY
/

-- Create synonym VW_LOCATION_HIERARCHY
CREATE PUBLIC SYNONYM vw_location_hierarchy
  FOR location.vw_location_hierarchy
/


-- End of DDL Script for View LOCATION.VW_LOCATION_HIERARCHY

-- Start of DDL Script for View PSS.VW_ESUDS_TRAN
-- Generated 11/23/2004 3:03:23 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_ESUDS_TRAN
DROP VIEW pss.vw_esuds_tran
/

CREATE OR REPLACE VIEW pss.vw_esuds_tran (
   tran_id,
   tran_start_ts,
   tran_auth_ts,
   tran_upload_ts,
   tran_auth_amount,
   tran_end_ts,
   tran_auth_cd,
   tran_state_cd,
   tran_desc,
   pos_id,
   authority_tran_cd,
   consumer_acct_id,
   device_tran_cd,
   pos_pta_id,
   client_payment_type_cd,
   tran_auth_result_cd,
   tran_auth_type_cd,
   tran_received_raw_acct_data,
   tran_parsed_acct_name,
   tran_parsed_acct_exp_date,
   tran_parsed_acct_num,
   tran_reportable_acct_num,
   tran_settlement_amount,
   tran_account_pin,
   tran_account_entry_method_cd )
AS
SELECT tr.tran_id,
       tr.tran_start_ts,
       tr.tran_auth_ts,
       tr.tran_upload_ts,
       tr.tran_auth_amount,
       tr.tran_end_ts,
       tr.tran_auth_cd,
       tr.tran_state_cd,
       tr.tran_desc,
       pta.pos_id,
       authority_tran_cd,
       tr.consumer_acct_id,
       tr.device_tran_cd,
       pta.pos_pta_id,
       ps.CLIENT_PAYMENT_TYPE_CD,
       tr.tran_auth_result_cd,
       tr.tran_auth_type_cd,
       tr.tran_received_raw_acct_data,
       tr.tran_parsed_acct_name,
       tr.tran_parsed_acct_exp_date,
       tr.tran_parsed_acct_num,
       tr.tran_reportable_acct_num,
       tr.tran_settlement_amount,
       tr.tran_account_pin,
       tr.tran_account_entry_method_cd
  FROM tran tr, pos_pta pta, payment_subtype ps
 WHERE tr.pos_pta_id = pta.pos_pta_id
 AND pta.payment_subtype_id = ps.payment_subtype_id
/

-- Drop the old synonym VW_ESUDS_TRAN
DROP PUBLIC SYNONYM VW_ESUDS_TRAN
/

-- Create synonym VW_ESUDS_TRAN
CREATE PUBLIC SYNONYM vw_esuds_tran
  FOR pss.vw_esuds_tran
/


-- End of DDL Script for View PSS.VW_ESUDS_TRAN

-- Start of DDL Script for View PSS.VW_ESUDS_TRAN_LINE_ITEM_DTL
-- Generated 11/23/2004 3:03:43 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_ESUDS_TRAN_LINE_ITEM_DTL
DROP VIEW pss.vw_esuds_tran_line_item_dtl
/

CREATE OR REPLACE VIEW pss.vw_esuds_tran_line_item_dtl (
   tran_id,
   tran_line_item_id,
   tran_start_ts,
   tran_line_item_desc,
   host_type_id,
   host_port_num,
   host_label_cd,
   customer_id,
   consumer_acct_id,
   tran_line_item_quantity,
   tran_line_item_amount,
   tran_line_item_tot_amount,
   campus_id,
   campus_name,
   dorm_id,
   dorm_name,
   room_id,
   room_name,
   client_payment_type_cd )
AS
(/* Formatted on 2004/08/31 10:09 (Formatter Plus v4.8.0) */
SELECT tr.tran_id,
       tli.tran_line_item_id,
       tr.tran_start_ts,
       tli.tran_line_item_desc,
       HOST.host_type_id,
       HOST.host_port_num,
       HOST.host_label_cd,
       pos.customer_id,
       tr.consumer_acct_id,
       tli.tran_line_item_quantity,
       NVL (tli.tran_line_item_amount, 0) tran_line_item_amount,
       tli.tran_line_item_quantity * NVL (tli.tran_line_item_amount, 0)
                                                                tran_line_item_tot_amount,
       campus.location_id campus_id,
       campus.location_name campus_name,
       dorm.location_id dorm_id,
       dorm.location_name dorm_name,
       room.location_id location_id,
       room.location_name room_name,
       pt.CLIENT_PAYMENT_TYPE_CD
  FROM tran tr,
       tran_line_item tli,
       pos,
       device dv,
       HOST,
       LOCATION.LOCATION dorm,
       LOCATION.LOCATION room,
       LOCATION.LOCATION campus,
       payment_subtype pt,
       pos_pta pta
 WHERE tr.tran_id = tli.tran_id
   AND tr.pos_pta_id = pta.pos_pta_id
   AND pta.pos_id = pos.pos_id
   AND pos.device_id = dv.device_id
   AND HOST.device_id = dv.device_id
   AND tli.host_id = HOST.host_id
   AND pos.location_id = room.location_id
   AND room.parent_location_id = dorm.location_id
   AND dorm.parent_location_id = campus.location_id
   AND pta.payment_subtype_id = pt.payment_subtype_id
   and tli.tran_batch_type_cd = 'A'
)
/

-- Drop the old synonym VW_ESUDS_TRAN_LINE_ITEM_DTL
DROP PUBLIC SYNONYM VW_ESUDS_TRAN_LINE_ITEM_DTL
/

-- Create synonym VW_ESUDS_TRAN_LINE_ITEM_DTL
CREATE PUBLIC SYNONYM vw_esuds_tran_line_item_dtl
  FOR pss.vw_esuds_tran_line_item_dtl
/


-- End of DDL Script for View PSS.VW_ESUDS_TRAN_LINE_ITEM_DTL

-- Start of DDL Script for View PSS.VW_LOCATION_BY_DEVICE_ID
-- Generated 11/23/2004 3:59:04 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_LOCATION_BY_DEVICE_ID
DROP VIEW pss.vw_location_by_device_id
/

CREATE OR REPLACE VIEW pss.vw_location_by_device_id (
   location_id,
   location_name,
   device_id,
   device_serial_cd,
   location_timezone )
AS
select a.location_id, a.location_name, b.device_id, c.device_serial_cd, a.location_time_zone_cd
from location.location a, pss.pos b, device.device c
where a.location_id = b.location_id
and c.device_id = b.device_id
/

-- Drop the old synonym VW_LOCATION_BY_DEVICE_ID
DROP PUBLIC SYNONYM VW_LOCATION_BY_DEVICE_ID
/

-- Create synonym VW_LOCATION_BY_DEVICE_ID
CREATE PUBLIC SYNONYM vw_location_by_device_id
  FOR pss.vw_location_by_device_id
/


-- End of DDL Script for View PSS.VW_LOCATION_BY_DEVICE_ID

-- Start of DDL Script for View PSS.VW_TLI_PIVOT_VIEW
-- Generated 11/23/2004 3:03:58 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TLI_PIVOT_VIEW
DROP VIEW pss.vw_tli_pivot_view
/

CREATE OR REPLACE VIEW pss.vw_tli_pivot_view (
   tran_id,
   tran_start_ts,
   consumer_id,
   consumer_acct_id,
   customer_id,
   client_payment_type_cd,
   tran_settlement_amount,
   tran_line_item_desc )
AS
SELECT t.tran_id,
          t.tran_start_ts,
          consumer_id,
          t.consumer_acct_id,
          ps.client_payment_type_cd,
          t.tran_settlement_amount,
          sf_pivot_tran_line_item (t.tran_id),
          customer_id
     FROM tran_line_item tli, tran t, pos_pta pta, payment_subtype ps, consumer_acct ca, pos
    WHERE t.tran_id = tli.tran_id
      AND t.pos_pta_id = pta.pos_pta_id
      AND pta.payment_subtype_id = ps.payment_subtype_id
      and t.consumer_acct_id = ca.consumer_acct_id
      and pta.pos_id = pos.pos_id
      and tli.tran_batch_type_cd = 'A'
      GROUP BY t.tran_id,
         t.tran_start_ts,
         consumer_id,
         t.consumer_acct_id,
         ps.client_payment_type_cd,
         t.tran_settlement_amount,
         sf_pivot_tran_line_item (t.tran_id),
         customer_id
/

-- Drop the old synonym VW_TLI_PIVOT_VIEW
DROP PUBLIC SYNONYM VW_TLI_PIVOT_VIEW
/

-- Create synonym VW_TLI_PIVOT_VIEW
CREATE PUBLIC SYNONYM vw_tli_pivot_view
  FOR pss.vw_tli_pivot_view
/


-- End of DDL Script for View PSS.VW_TLI_PIVOT_VIEW

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_CAMPUS
-- Generated 11/23/2004 3:04:08 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TOT_SALES_BY_CAMPUS
DROP VIEW pss.vw_tot_sales_by_campus
/

CREATE OR REPLACE VIEW pss.vw_tot_sales_by_campus (
   campus_id,
   campus_name,
   school_id,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc2.location_id campus_id,
            loc2.location_name campus_name,
            loc2.parent_location_id school_id,
            ps.customer_id operator_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            SUM (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                                          --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tran_line_item,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc1,
            LOCATION loc2,
            LOCATION loc3,
            LOCATION loc4
      WHERE tran_line_item.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        and tran_line_item.tran_batch_type_cd = 'A'
   GROUP BY loc2.location_id,
            loc2.location_name,
            loc2.parent_location_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Drop the old synonym TOT_SALES_BY_CAMPUS
DROP PUBLIC SYNONYM TOT_SALES_BY_CAMPUS
/

-- Create synonym TOT_SALES_BY_CAMPUS
CREATE PUBLIC SYNONYM tot_sales_by_campus
  FOR pss.vw_tot_sales_by_campus
/


-- End of DDL Script for View PSS.VW_TOT_SALES_BY_CAMPUS

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_DORM
-- Generated 11/23/2004 3:04:38 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TOT_SALES_BY_DORM
DROP VIEW pss.vw_tot_sales_by_dorm
/

CREATE OR REPLACE VIEW pss.vw_tot_sales_by_dorm (
   dorm_id,
   dorm_name,
   campus_id,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc3.location_id campus_id,
            loc3.location_name campus_name,
            loc3.parent_location_id school_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            sum (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                           --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc3,
            LOCATION loc4
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        and tli.tran_batch_type_cd = 'A'
   GROUP BY loc3.location_id,
            loc3.location_name,
            loc3.parent_location_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Drop the old synonym TOT_SALES_BY_DORM
DROP PUBLIC SYNONYM TOT_SALES_BY_DORM
/

-- Create synonym TOT_SALES_BY_DORM
CREATE PUBLIC SYNONYM tot_sales_by_dorm
  FOR pss.vw_tot_sales_by_dorm
/


-- End of DDL Script for View PSS.VW_TOT_SALES_BY_DORM

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_LAUNDRY_ROOM
-- Generated 11/23/2004 3:04:58 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TOT_SALES_BY_LAUNDRY_ROOM
DROP VIEW pss.vw_tot_sales_by_laundry_room
/

CREATE OR REPLACE VIEW pss.vw_tot_sales_by_laundry_room (
   laundry_room_id,
   laundry_room_name,
   dorm_id,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc4.location_id campus_id,
            loc4.location_name campus_name,
            loc4.parent_location_id school_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            sum (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                           --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tli, tran tr, pos ps, pos_pta pta, LOCATION loc4
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        and tli.tran_batch_type_cd = 'A'
   GROUP BY loc4.location_id,
            loc4.location_name,
            loc4.parent_location_id,
            ps.customer_id,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Drop the old synonym TOT_SALES_BY_LAUNDRY_ROOM
DROP PUBLIC SYNONYM TOT_SALES_BY_LAUNDRY_ROOM
/

-- Create synonym TOT_SALES_BY_LAUNDRY_ROOM
CREATE PUBLIC SYNONYM tot_sales_by_laundry_room
  FOR pss.vw_tot_sales_by_laundry_room
/


-- End of DDL Script for View PSS.VW_TOT_SALES_BY_LAUNDRY_ROOM

-- Start of DDL Script for View PSS.VW_TOT_SALES_BY_SCHOOL
-- Generated 11/23/2004 3:05:11 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TOT_SALES_BY_SCHOOL
DROP VIEW pss.vw_tot_sales_by_school
/

CREATE OR REPLACE VIEW pss.vw_tot_sales_by_school (
   school_id,
   school_name,
   operator_id,
   tran_ts,
   tot_amount,
   tot_cycles,
   tot_deterg,
   tot_fabric_soft,
   tot_wash_cycles,
   tot_dry_cycles,
   tot_wash_amount,
   tot_dry_amount )
AS
SELECT   loc1.location_id campus_id,
            loc1.location_name campus_name,
            ps.customer_id operator_id,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            SUM (tran_line_item_amount) tot_amount,
            sum (tran_line_item_quantity) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         2, tran_line_item_quantity,
                         0
                        )
                ) tot_deterg,
            SUM (DECODE (tran_line_item_type_id,
                         3, tran_line_item_quantity,
                         0
                        )
                ) tot_fabric_soft,
                               --We are not using the tran_line_item_type_id.  A bug was posted to
            -- the issues database.
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_wash_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_quantity
                        )
                ) tot_dry_cycle,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Wash'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_wash_amount,
            SUM (DECODE (INSTR (tran_line_item_desc, 'Dry'),
                         0, 0,
                         tran_line_item_amount
                        )
                ) tot_dry_amount
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc1,
            LOCATION loc2,
            LOCATION loc3,
            LOCATION loc4
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        and tli.tran_batch_type_cd = 'A'
   GROUP BY loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Drop the old synonym TOT_SALES_BY_SCHOOL
DROP PUBLIC SYNONYM TOT_SALES_BY_SCHOOL
/

-- Create synonym TOT_SALES_BY_SCHOOL
CREATE PUBLIC SYNONYM tot_sales_by_school
  FOR pss.vw_tot_sales_by_school
/


-- End of DDL Script for View PSS.VW_TOT_SALES_BY_SCHOOL

-- Start of DDL Script for View PSS.VW_TOTAL_SALES_BY_HOST
-- Generated 11/23/2004 3:05:39 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TOTAL_SALES_BY_HOST
DROP VIEW pss.vw_total_sales_by_host
/

CREATE OR REPLACE VIEW pss.vw_total_sales_by_host (
   laundry_room_id,
   laundry_room_name,
   dorm_id,
   dorm_name,
   campus_id,
   campus_name,
   school_id,
   school_name,
   customer_id,
   device_id,
   device_name,
   host_id,
   host_label_cd,
   host_type_id,
   host_type_desc,
   payment_subtype_id,
   payment_subtype_desc,
   client_payment_type_cd,
   client_payment_type_desc,
   tran_ts,
   tot_items,
   tot_amount,
   tot_cycles,
   tot_cycle_amount,
   tot_top_offs,
   tot_top_off_amount,
   tot_misc_items,
   tot_misc_item_amount,
   tot_reg_washes,
   tot_reg_wash_amount,
   tot_reg_cold_washes,
   tot_reg_cold_wash_amount,
   tot_warm_washes,
   tot_warm_wash_amount,
   tot_reg_hot_washes,
   tot_reg_hot_wash_amount,
   tot_spec_washes,
   tot_spec_wash_amount,
   tot_spec_cold_washes,
   tot_spec_cold_wash_amount,
   spec_warm_washes,
   spec_warm_wash_amount,
   spec_hot_washes,
   spec_hot_wash_amount,
   super_washes,
   super_wash_amount,
   reg_dries,
   reg_dry_amount,
   spec_dries,
   spec_dry_amount,
   top_off_dries,
   top_off_dry_amount,
   tot_unused_purch_adjs,
   tot_unused_purch_adj_amount,
   tot_leftover_credit_adjs,
   tot_leftover_credit_adj_amount )
AS
SELECT   loc4.location_id,
            loc4.location_name,
            loc3.location_id,
            loc3.location_name,
            loc2.location_id,
            loc2.location_name,
            loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            dev.device_id,
            dev.device_name,
            hs.host_id,
            hs.host_label_cd,
            ht.host_type_id,
            ht.host_type_desc,
            pst.payment_subtype_id,
            pst.payment_subtype_name, -- added by BSK 11-05-04 because web app object is mapped to it
            pst.client_payment_type_cd,
            cpt.client_payment_type_desc,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            -- total tran line items per host
            SUM (tran_line_item_quantity) tot_items,
            SUM (NVL (tli.tran_line_item_quantity, 0) * NVL (tli.tran_line_item_amount, 0))
                                                                               tot_amount,
            -- total cycles
            SUM (DECODE (tran_line_item_type_id,
                         1, tran_line_item_quantity,
                         2, tran_line_item_quantity,
                         3, tran_line_item_quantity,
                         4, tran_line_item_quantity,
                         5, tran_line_item_quantity,
                         6, tran_line_item_quantity,
                         7, tran_line_item_quantity,
                         8, tran_line_item_quantity,
                         40, tran_line_item_quantity,
                         41, tran_line_item_quantity,
                         0
                        )
                ) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         1, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         2, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         3, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         4, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         5, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         6, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         7, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         8, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         40, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         41, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_cycle_amount,
            -- total top offs
            SUM (DECODE (tran_line_item_type_id,
                         9, tran_line_item_quantity,
                         42, tran_line_item_quantity,
                         0
                        )
                ) tot_top_offs,
            SUM (DECODE (tran_line_item_type_id,
                         9, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         42, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_top_off_amount,
            -- total misc items
            SUM (DECODE (tran_line_item_type_id, 0, tran_line_item_quantity, 0))
                                                                           tot_misc_items,
            SUM (DECODE (tran_line_item_type_id,
                         0, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_misc_item_amount,
            -- totals by item type
            SUM (DECODE (tran_line_item_type_id, 1, tran_line_item_quantity, 0))
                                                                           tot_reg_washes,
            SUM (DECODE (tran_line_item_type_id,
                         1, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 2, tran_line_item_quantity, 0))
                                                                      tot_reg_cold_washes,
            SUM (DECODE (tran_line_item_type_id,
                         2, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_cold_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 3, tran_line_item_quantity, 0))
                                                                          tot_warm_washes,
            SUM (DECODE (tran_line_item_type_id,
                         3, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_warm_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 4, tran_line_item_quantity, 0))
                                                                       tot_reg_hot_washes,
            SUM (DECODE (tran_line_item_type_id,
                         4, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_hot_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 5, tran_line_item_quantity, 0))
                                                                          tot_spec_washes,
            SUM (DECODE (tran_line_item_type_id,
                         5, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_spec_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 6, tran_line_item_quantity, 0))
                                                                     tot_spec_cold_washes,
            SUM (DECODE (tran_line_item_type_id,
                         6, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_spec_cold_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 7, tran_line_item_quantity, 0))
                                                                         spec_warm_washes,
            SUM (DECODE (tran_line_item_type_id,
                         7, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_warm_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 8, tran_line_item_quantity, 0))
                                                                          spec_hot_washes,
            SUM (DECODE (tran_line_item_type_id,
                         8, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_hot_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 9, tran_line_item_quantity, 0))
                                                                             super_washes,
            SUM (DECODE (tran_line_item_type_id,
                         9, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) super_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 40, tran_line_item_quantity, 0))
                                                                                reg_dries,
            SUM (DECODE (tran_line_item_type_id,
                         40, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) reg_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 41, tran_line_item_quantity, 0))
                                                                               spec_dries,
            SUM (DECODE (tran_line_item_type_id,
                         41, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 42, tran_line_item_quantity, 0))
                                                                            top_off_dries,
            SUM (DECODE (tran_line_item_type_id,
                         42, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) top_off_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 80, tran_line_item_quantity, 0))
                                                                    tot_unused_purch_adjs,
            SUM (DECODE (tran_line_item_type_id,
                         80, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_unused_purch_adj_amount,
            SUM (DECODE (tran_line_item_type_id, 81, tran_line_item_quantity, 0))
                                                                 tot_leftover_credit_adjs,
            SUM (DECODE (tran_line_item_type_id,
                         81, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_leftover_credit_adj_amount
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc4,
            LOCATION loc3,
            LOCATION loc2,
            LOCATION loc1,
            device dev,
            HOST hs,
            host_type ht,
            payment_subtype pst,
            client_payment_type cpt
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND cpt.client_payment_type_cd = pst.client_payment_type_cd
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        AND ps.device_id = dev.device_id
        AND dev.device_id = hs.device_id
        AND hs.host_type_id = ht.host_type_id
        AND hs.host_id = tli.host_id
        and tli.tran_line_item_type_id not in (80, 81)
        and tli.tran_batch_type_cd = 'A'
        AND pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc4.location_id,
            loc4.location_name,
            loc3.location_id,
            loc3.location_name,
            loc2.location_id,
            loc2.location_name,
            loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            dev.device_id,
            dev.device_name,
            hs.host_id,
            hs.host_label_cd,
            ht.host_type_id,
            ht.host_type_desc,
            pst.payment_subtype_id,
            pst.payment_subtype_name,
            pst.client_payment_type_cd,
            cpt.client_payment_type_desc,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Drop the old synonym VW_TOTAL_SALES_BY_HOST
DROP PUBLIC SYNONYM VW_TOTAL_SALES_BY_HOST
/

-- Create synonym VW_TOTAL_SALES_BY_HOST
CREATE PUBLIC SYNONYM vw_total_sales_by_host
  FOR pss.vw_total_sales_by_host
/


-- End of DDL Script for View PSS.VW_TOTAL_SALES_BY_HOST

-- Start of DDL Script for View PSS.VW_TRAN_LINE_ITEM_DTL
-- Generated 11/23/2004 3:05:53 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TRAN_LINE_ITEM_DTL
DROP VIEW pss.vw_tran_line_item_dtl
/

CREATE OR REPLACE VIEW pss.vw_tran_line_item_dtl (
   tran_id,
   tran_line_item_id,
   tran_start_ts,
   tran_line_item_desc,
   host_type_id,
   host_port_num,
   host_label_cd,
   location_id,
   customer_id,
   consumer_acct_id,
   tran_line_item_quantity,
   tran_line_item_amount,
   tran_line_item_tot_amount )
AS
SELECT tr.tran_id,
       tli.tran_line_item_id,
       TR.TRAN_START_TS,
       tli.tran_line_item_desc,
       HOST.host_type_id,
       HOST.host_port_num,
       HOST.host_label_cd,
       pos.location_id,
       pos.customer_id,
       tr.consumer_acct_id,
       tli.tran_line_item_quantity,
       nvl(tli.tran_line_item_amount, 0) tran_line_item_amount,
              tli.tran_line_item_quantity*
       nvl(tli.tran_line_item_amount, 0) tran_line_item_tot_amount
  FROM tran tr,
       tran_line_item tli,
       pos,
       pos_pta pta,
       device dv,
       HOST, 
       location dorm
 WHERE tr.tran_id = tli.tran_id
        and tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = pos.pos_id
   AND pos.device_id = dv.device_id
   AND HOST.device_id = dv.device_id
   and pos.location_id = dorm.location_id
   and tli.tran_batch_type_cd = 'A'
/

-- Drop the old synonym VW_TRAN_LINE_ITEM_DTL
DROP PUBLIC SYNONYM VW_TRAN_LINE_ITEM_DTL
/

-- Create synonym VW_TRAN_LINE_ITEM_DTL
CREATE PUBLIC SYNONYM vw_tran_line_item_dtl
  FOR pss.vw_tran_line_item_dtl
/


-- End of DDL Script for View PSS.VW_TRAN_LINE_ITEM_DTL

-- Start of DDL Script for View PSS.VW_TRAN_SUMMARY
-- Generated 11/23/2004 3:06:18 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TRAN_SUMMARY
DROP VIEW pss.vw_tran_summary
/

CREATE OR REPLACE VIEW pss.vw_tran_summary (
   tran_id,
   tran_start_ts,
   consumer_id,
   consumer_acct_id,
   client_payment_type_cd,
   tran_settlement_amount,
   tran_line_item_desc,
   customer_id )
AS
SELECT t.tran_id,
          t.tran_start_ts,
          consumer_id,
          t.consumer_acct_id,
          ps.client_payment_type_cd,
          CASE WHEN
            CASE WHEN ps.payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID' THEN ia.authority_service_type_id
                WHEN ps.payment_subtype_key_name = 'BLACKBRD_TENDER_ID' THEN ba.authority_service_type_id
                ELSE 0
            END
            IN(2, 3) THEN NULL
            ELSE NVL(t.tran_settlement_amount,0)
          END tran_amount,
          sf_pivot_tran_line_item (t.tran_id),
          customer_id
     FROM tran_line_item tli, tran t, pos_pta pta, payment_subtype ps, consumer_acct ca, pos,
        internal_payment_type ipt, blackbrd_tender bt,
        blackbrd_authority ba, internal_authority ia
    WHERE t.tran_id = tli.tran_id
      AND t.pos_pta_id = pta.pos_pta_id
      AND pta.payment_subtype_id = ps.payment_subtype_id
      and t.consumer_acct_id = ca.consumer_acct_id
      and pta.pos_id = pos.pos_id
      and tli.tran_batch_type_cd = 'A'
      and pta.payment_subtype_key_id = bt.blackbrd_tender_id (+)
      and bt.blackbrd_authority_id = ba.blackbrd_authority_id (+)
      and pta.payment_subtype_key_id = ipt.internal_payment_type_id (+)
      and ipt.internal_authority_id = ia.internal_authority_id (+)
      GROUP BY t.tran_id,
         t.tran_start_ts,
         consumer_id,
         t.consumer_acct_id,
         ps.client_payment_type_cd,
         ps.payment_subtype_key_name,
         ia.authority_service_type_id,
         ba.authority_service_type_id,
         t.tran_settlement_amount,
         sf_pivot_tran_line_item (t.tran_id),
         customer_id
/

-- Drop the old synonym VW_TRAN_SUMMARY
DROP PUBLIC SYNONYM VW_TRAN_SUMMARY
/

-- Create synonym VW_TRAN_SUMMARY
CREATE PUBLIC SYNONYM vw_tran_summary
  FOR pss.vw_tran_summary
/


-- End of DDL Script for View PSS.VW_TRAN_SUMMARY

-- Start of DDL Script for Procedure PSS.SP_NXT_BLACKBRD_SEQUENCE_NUM
-- Generated 11/23/2004 3:58:04 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of SP_NXT_BLACKBRD_SEQUENCE_NUM
DROP PROCEDURE pss.sp_nxt_blackbrd_sequence_num
/

CREATE OR REPLACE 
PROCEDURE pss.sp_nxt_blackbrd_sequence_num (
   pn_device_id       IN       device.device_id%TYPE,
   pn_nxt_seq_num     OUT      device_counter.device_counter_value%TYPE,
   pn_return_code     OUT      exception_code.exception_code_id%TYPE,
   pv_error_message   OUT      exception_data.additional_information%TYPE
) AS
--
-- Purpose: Gets next device sequence num from device_counter
-- for a blackboard authority
--
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ---------    ------      ---------------------------------------
-- erybski      04.09.16    Initial release.
-- erybski     04.09.27    Updated variable names to reflect content meaning change
--
   n_device_id                           NUMBER;
   v_device_counter_parameter   CONSTANT device_counter.device_counter_parameter%TYPE
                                                            := 'BLACKBRD_SEQUENCE_NUMBER';
   v_payment_subtype_key_name   CONSTANT payment_subtype.payment_subtype_key_name%TYPE
                                                                  := 'BLACKBRD_TENDER_ID';
   n_cur_seq_num                         NUMBER;
   cv_package_name              CONSTANT VARCHAR2 (30)                              := '';
   cv_procedure_name            CONSTANT VARCHAR2 (30)       := 'SP_NXT_BLACKBRD_SEQ_NUM';
   e_device_id_notfound                  EXCEPTION;

   CURSOR c_chk_device_id IS
      SELECT COUNT (d.device_id)
        FROM device.device d,
             pss.pos p,
             pss.pos_pta pp,
             pss.payment_subtype ps
       WHERE d.device_id = p.device_id
         AND p.pos_id = pp.pos_id
         AND ps.payment_subtype_id = pp.payment_subtype_id
         AND p.device_id = pn_device_id
         AND ps.payment_subtype_key_name = v_payment_subtype_key_name;

   CURSOR c_get_device_seq_num IS
      SELECT     TO_NUMBER (device_counter_value)
            FROM device.device_counter
           WHERE device_id = pn_device_id
             AND device_counter_parameter = v_device_counter_parameter
      FOR UPDATE;
BEGIN
   -- check that specified device_id exists and is configured with a blackboard authority
   pn_nxt_seq_num := NULL;

   OPEN c_chk_device_id;

   FETCH c_chk_device_id
    INTO n_device_id;

   IF n_device_id = 0 THEN
      CLOSE c_chk_device_id;

      RAISE e_device_id_notfound;
   END IF;

   CLOSE c_chk_device_id;

   -- get current blackboard sequence num
   OPEN c_get_device_seq_num;

   FETCH c_get_device_seq_num
    INTO n_cur_seq_num;

   IF c_get_device_seq_num%NOTFOUND = TRUE THEN
      -- if sequence num is not yet defined
      CLOSE c_get_device_seq_num;

      pn_nxt_seq_num := 1;

      INSERT INTO device.device_counter
                  (device_counter_value,
                   device_id,
                   device_counter_parameter
                  )
           VALUES (pn_nxt_seq_num,
                   pn_device_id,
                   v_device_counter_parameter
                  );
   ELSE
      -- increment sequence num
      IF n_cur_seq_num = 999999 THEN
         pn_nxt_seq_num := 1;
      ELSE
         pn_nxt_seq_num := n_cur_seq_num + 1;
      END IF;

      UPDATE device.device_counter
         SET device_counter_value = pn_nxt_seq_num
       WHERE device_id = pn_device_id
         AND device_counter_parameter = v_device_counter_parameter;

      CLOSE c_get_device_seq_num;
   END IF;
EXCEPTION
   WHEN e_device_id_notfound THEN
      pv_error_message :=
               'Could not find the following device id in the database: ' || pn_device_id;
      pn_return_code := pkg_app_exec_hist_globals.device_id_not_found;
      pkg_exception_processor.sp_log_exception (pn_return_code,
                                                pv_error_message,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
      pn_nxt_seq_num := NULL;
   WHEN OTHERS THEN
      pv_error_message :=
         'An unknown exception occurred in sp_authorize(2) = ' || SQLCODE || ', '
         || SQLERRM;
      pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
      pkg_exception_processor.sp_log_exception (pn_return_code,
                                                pv_error_message,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
      pn_nxt_seq_num := NULL;
END;
/



-- End of DDL Script for Procedure PSS.SP_NXT_BLACKBRD_SEQUENCE_NUM

-- Start of DDL Script for Function PSS.SF_PIVOT_TRAN_LINE_ITEM
-- Generated 11/23/2004 3:58:25 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of SF_PIVOT_TRAN_LINE_ITEM
DROP FUNCTION pss.sf_pivot_tran_line_item
/

CREATE OR REPLACE 
FUNCTION pss.sf_pivot_tran_line_item (pn_tran_id IN tran.tran_id%TYPE)
   RETURN VARCHAR2
IS
   v_return_string   VARCHAR2 (4000) DEFAULT NULL;
   v_delimiter       VARCHAR2 (1)    DEFAULT NULL;
BEGIN
   FOR x IN (SELECT tran_line_item_desc
               FROM tran_line_item
              WHERE tran_id = pn_tran_id
                AND tran_batch_type_cd = 'A')
   LOOP
      v_return_string := v_return_string || v_delimiter || x.tran_line_item_desc;
      v_delimiter := CHR (13);
   END LOOP;

   RETURN v_return_string;
END;
/



-- End of DDL Script for Function PSS.SF_PIVOT_TRAN_LINE_ITEM

-- Start of DDL Script for Package PSS.PKG_CONSUMER_MAINT
-- Generated 11/23/2004 3:58:45 PM from PSS@USADBD03.USATECH.COM

-- Drop the old instance of PKG_CONSUMER_MAINT
DROP PACKAGE pss.pkg_consumer_maint
/

CREATE OR REPLACE 
PACKAGE pss.pkg_consumer_maint IS
   PROCEDURE sp_add_consumer (
      pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
      pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
      pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
      pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
      pv_consumer_city                 IN       consumer.consumer_city%TYPE,
      pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
      pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
      pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
      pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
      pn_location_id                   IN       consumer.location_id%TYPE,
      pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
      pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
      pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
      pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
      pv_error_message                 OUT      exception_data.additional_information%TYPE
   );

   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   );
END pkg_consumer_maint;
/


CREATE OR REPLACE 
PACKAGE BODY     pss.pkg_consumer_maint IS
/* The following rules were applied to this procedure

1.       The only unique identifier we have is the student?s email address.
         We determine if a student already exists in our database by comparing the
         email address provided in the data file with the records in our database.

2.       If a student?s email address changes then a new student record will be
         created.  Transactions from the original student record will not be
         associated with the new student record.

3.       All fields, except the password and email address, will be updated when
         new information is sent about an existing student.   The only exception
         to this rule is:

        "When a student?s id number changes, and the password was never changed
        by the user, the password will be set to the last four digits of the new
        student id."

4.      Student accounts in the database will be deactivated if they are not
        present when loading the data file.

5.      When we receive data about a student that has been deactivated we will
        reactivate the account.  A new account will not be created and any
        historical data will be re-associated with the student.

*/
   cv_package_name   CONSTANT VARCHAR2 (30) := 'pkg_consumer_maint';

   PROCEDURE sp_add_consumer (
      pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
      pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
      pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
      pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
      pv_consumer_city                 IN       consumer.consumer_city%TYPE,
      pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
      pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
      pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
      pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
      pn_location_id                   IN       consumer.location_id%TYPE,
      pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
      pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
      pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
      pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
      pv_error_message                 OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)                     := 'sp_add_consumer';
      n_return_cd                  NUMBER;
      n_consumer_count             NUMBER;
      n_app_user_count             NUMBER;
      n_consumer_id                consumer.consumer_id%TYPE;
      n_app_user_id                app_user.app_user_id%TYPE;
      v_app_user_pw                app_user.app_user_password%TYPE;
      v_old_consumer_acct_cd       consumer_acct.consumer_acct_cd%TYPE;
      v_new_consumer_acct_cd       consumer_acct.consumer_acct_cd%TYPE;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      IF pv_consumer_email_addr1 IS NULL THEN
        RAISE_APPLICATION_ERROR(-20171, 'An email address must be provided');
      END IF;
      SELECT COUNT (*)
        INTO n_consumer_count
        FROM consumer
       WHERE UPPER(consumer_email_addr1) = UPPER(pv_consumer_email_addr1);

      IF n_consumer_count = 0 THEN
         SELECT seq_consumer_id.NEXTVAL
           INTO n_consumer_id
           FROM DUAL;

         INSERT INTO consumer
                     (consumer_id,
                      consumer_fname,
                      consumer_lname,
                      consumer_addr1,
                      consumer_addr2,
                      consumer_city,
                      consumer_state_cd,
                      consumer_postal_cd,
                      consumer_email_addr1,
                      consumer_type_id,
                      location_id
                     )
              VALUES (n_consumer_id,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_addr1,
                      pv_consumer_addr2,
                      pv_consumer_city,
                      pv_consumer_state,
                      pv_consumer_postal_cd,
                      pv_consumer_email_addr1,
                      pn_consumer_type_id,
                      pn_location_id
                     );

         INSERT INTO consumer_acct
                     (consumer_acct_cd,
                      consumer_acct_active_yn_flag,
                      consumer_acct_balance,
                      consumer_id,
                      payment_subtype_id
                     )
              VALUES (pv_consumer_account_cd,
                      pv_consumer_accnt_actv_yn_flag,
                      pv_consumer_account_balance,
                      n_consumer_id,
                      1
                     );
      ELSE
         SELECT consumer_id
           INTO n_consumer_id
           FROM consumer
          WHERE UPPER(consumer_email_addr1) = UPPER(pv_consumer_email_addr1);

         UPDATE consumer
            SET consumer_fname = pv_consumer_fname,
                consumer_lname = pv_consumer_lname,
                consumer_addr1 = pv_consumer_addr1,
                consumer_addr2 = pv_consumer_addr2,
                consumer_city = pv_consumer_city,
                consumer_state_cd = pv_consumer_state,
                consumer_postal_cd = pv_consumer_postal_cd,
                consumer_type_id = pn_consumer_type_id,
                location_id = pn_location_id
          WHERE consumer_id = n_consumer_id;

         UPDATE consumer_acct
            SET consumer_acct_active_yn_flag = pv_consumer_accnt_actv_yn_flag,
                consumer_acct_balance = pv_consumer_account_balance,
                consumer_acct_cd = pv_consumer_account_cd
          WHERE consumer_id = n_consumer_id;
      END IF;

      SELECT COUNT (*)
        INTO n_app_user_count
        FROM app_user
       WHERE UPPER(app_user_email_addr) = UPPER(pv_consumer_email_addr1);

      IF n_app_user_count = 0 THEN
         SELECT seq_app_user_id.NEXTVAL
           INTO n_app_user_id
           FROM DUAL;

         INSERT INTO app_user
                     (app_user_id,
                      app_user_password,
                      app_user_active_yn_flag,
                      force_pw_change_yn_flag,
                      app_user_name,
                      app_user_fname,
                      app_user_lname,
                      app_user_email_addr
                     )
              VALUES (n_app_user_id,
                      SUBSTR (pv_consumer_account_cd,
                              LENGTH (pv_consumer_account_cd) - 4,
                              4
                             ),
                      pv_consumer_accnt_actv_yn_flag,
                      'N',
                      pv_consumer_email_addr1,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_email_addr1
                     );

         INSERT INTO app_user_object_permission
                     (app_user_id,
                      app_id,
                      app_object_type_id,
                      allow_object_create_yn_flag,
                      allow_object_read_yn_flag,
                      allow_object_modify_yn_flag,
                      allow_object_delete_yn_flag,
                      object_cd
                     )
              VALUES (n_app_user_id,
                      1,
                      1,
                      'N',
                      'Y',
                      'Y',
                      'N',
                      n_consumer_id
                     );
      ELSE
         SELECT app_user_password,
                c.consumer_acct_cd
           INTO v_app_user_pw,
                v_old_consumer_acct_cd
           FROM app_user a,
                consumer b,
                consumer_acct c
          WHERE UPPER (app_user_email_addr) = UPPER (pv_consumer_email_addr1)
            AND UPPER (a.app_user_email_addr) = UPPER (b.consumer_email_addr1)
            AND b.consumer_id = c.consumer_id;

         IF SUBSTR (pv_consumer_account_cd,
                    LENGTH (v_old_consumer_acct_cd) - 3,
                    4
                   ) = v_app_user_pw THEN
            v_new_consumer_acct_cd :=
                   SUBSTR (pv_consumer_account_cd,
                           LENGTH (pv_consumer_account_cd) - 3,
                           4
                          );
         ELSE
            v_new_consumer_acct_cd := v_app_user_pw;
         END IF;

         UPDATE app_user
            SET app_user_password = v_new_consumer_acct_cd,
                app_user_active_yn_flag = pv_consumer_accnt_actv_yn_flag,
                force_pw_change_yn_flag = 'N',
                app_user_name = pv_consumer_email_addr1,
                app_user_fname = pv_consumer_fname,
                app_user_lname = pv_consumer_lname
          WHERE UPPER (app_user_email_addr) = UPPER (pv_consumer_email_addr1);
      END IF;
   /* changed this to report exceptions (BSK 11-10-04)
   EXCEPTION
      WHEN OTHERS THEN
         -- changed this slightly to set pv_error_message to only SQLERRM and
         -- pn_return_code to SQLCODE for web file upload functionality (BSK 11-05-04)
         pv_error_message := SQLERRM;
         pn_return_code := SQLCODE;
         pkg_exception_processor.sp_log_exception
                                               (pkg_app_exec_hist_globals.unknown_error_id,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
   --*/
   END;

   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)             := 'sp_add_consumer_account';
      n_return_cd                  NUMBER;
      n_consumer_account_count     NUMBER;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      UPDATE consumer_acct ca
         SET ca.consumer_acct_active_yn_flag = 'N'
       WHERE ca.consumer_id IN (
                       SELECT consumer_id
                         FROM consumer
                        WHERE UPPER (consumer_email_addr1) =
                                                           UPPER (pv_consumer_email_addr1) );

      UPDATE app_user ap
         SET ap.app_user_active_yn_flag = 'N'
       WHERE UPPER (ap.app_user_email_addr) = UPPER (pv_consumer_email_addr1);
   EXCEPTION
      WHEN OTHERS THEN
         pv_error_message :=
                         'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
         pkg_exception_processor.sp_log_exception
                                               (pn_return_code,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   END;
END;
/


-- End of DDL Script for Package PSS.PKG_CONSUMER_MAINT


