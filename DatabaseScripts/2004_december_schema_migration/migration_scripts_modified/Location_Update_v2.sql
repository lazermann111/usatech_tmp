

COMMENT ON TABLE LOCATION.COUNTRY IS 'Contains a listing of all the different countires a location can be assigned to.';



COMMENT ON TABLE LOCATION.CUST_LOC IS 'Contains the assignment of a customers to locations.';



COMMENT ON TABLE LOCATION.CUSTOMER IS 'Contains information about each of USA Technologies customers.  A customer is a person or organization that subscribes to services provided by USA Technologies.';

COMMENT ON TABLE LOCATION.LOCATION IS 'Contains information about the location of a device or host.  This table contains a parent/child relationship to allow for sub-locations (ie School -> Campus -> Dorm -> Room).';



COMMENT ON TABLE LOCATION.TIME_ZONE IS 'Not Used';

COMMENT ON TABLE LOCATION.STATE IS 'Contains a listing of all the different states / provinces  a location can be assigned to.';


