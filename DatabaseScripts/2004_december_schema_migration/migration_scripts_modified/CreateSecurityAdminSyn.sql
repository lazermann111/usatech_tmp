create synonym PKG_APPLICATION_SECURITY for app_user.PKG_APPLICATION_SECURITY;
create synonym APP for app_user.APP;
create synonym APP_OBJECT_TYPE for app_user.APP_OBJECT_TYPE;
create synonym APP_USER for app_user.APP_USER;
create synonym APP_USER_OBJECT_PERMISSION for app_user.APP_USER_OBJECT_PERMISSION;
