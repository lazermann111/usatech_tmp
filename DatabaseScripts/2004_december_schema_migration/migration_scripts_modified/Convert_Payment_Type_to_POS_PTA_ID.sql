--modified by wei guo

have every pos accept cash & sp's

insert into pos_pta(pos_id, payment_subtype_id)
(select pos_id, 4 from pos);

insert into pos_pta(pos_id, payment_subtype_id)
(select pos_id, 9 from pos );

commit;

-- replace these two select statments with to update statements shown as follows.
--select  'update tran set pos_pta_id = ' || b.pos_pta_id  || ' where tran_id = ' || tran_id || ';' from tran a, pos_pta b where a.pos_id = b.pos_id and payment_type_id = 2 and authority_id =138 and payment_subtype_id = 1 order by tran_id;

--select  'update tran set pos_pta_id = ' || b.pos_pta_id  || ' where tran_id = ' || tran_id || ';' from tran a, pos_pta b where a.pos_id = b.pos_id and payment_type_id = 4 and authority_id =138 and payment_subtype_id = 3 order by tran_id;

-- update multiple rows based on pos_id
UPDATE tran 
set  pos_pta_id =
(SELECT max(pos_pta.pos_pta_id) 
FROM pos_pta
WHERE 
tran.pos_id = pos_pta.pos_id
and tran.payment_type_id =2 
and tran.authority_id = 138 
and pos_pta.payment_subtype_id=9
)
WHERE EXISTS
(SELECT * 
FROM pos_pta
WHERE 
tran.pos_id = pos_pta.pos_id
and tran.payment_type_id =2 
and tran.authority_id = 138 
and pos_pta.payment_subtype_id=9
);

UPDATE tran 
set  pos_pta_id =
(SELECT max(pos_pta.pos_pta_id) 
FROM pos_pta
WHERE 
tran.pos_id = pos_pta.pos_id
and tran.payment_type_id =4 
and tran.authority_id = 138 
and pos_pta.payment_subtype_id=4
)
WHERE EXISTS
(SELECT * 
FROM pos_pta  
WHERE 
tran.pos_id = pos_pta.pos_id
and tran.payment_type_id =4 
and tran.authority_id = 138 
and pos_pta.payment_subtype_id=4
);

ALTER TABLE TRAN 
 MODIFY (
  POS_PTA_ID NOT NULL
 )
/

-- modified by wei guo

alter table tran drop column pos_id;

alter table consumer_acct drop column payment_type_id;

alter table tran drop column payment_type_id;

-- modified by wei guo, change authority to authority_id
alter table tran drop column authority_id;

drop table pos_payment_type_authority;

drop table payment_type_authority;

drop table authority;

drop table payment_type cascade constraints;
