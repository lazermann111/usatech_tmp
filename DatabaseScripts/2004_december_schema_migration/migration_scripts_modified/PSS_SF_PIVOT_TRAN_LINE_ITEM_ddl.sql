-- Start of DDL Script for Function PSS.SF_PIVOT_TRAN_LINE_ITEM
-- Generated 9/2/2004 17:39:25 from PSS@usadbd03

CREATE OR REPLACE 
FUNCTION sf_pivot_tran_line_item (pn_tran_id IN tran.tran_id%TYPE)
   RETURN VARCHAR2
IS
-- The purpose of this function is to take every line item description
-- for each transaction, and put it all into one row/column delimited by
-- return characters.

-- Example
-- The following values existed in a table:
-- TRAN_ID   TRAN_LINE_ITEM_DESC
--       1   DOG
--       1   CAT
--       1   BIRD
--       2   HOUSE
--       2   CAR
--
-- If you pass in tran_id = 1 the return string would be
-- dog <chr(13)> cat <chr(13)> bird <chr(13)>
    
   v_return_string   VARCHAR2 (4000) DEFAULT NULL;
   v_delimiter       VARCHAR2 (1)    DEFAULT NULL;
BEGIN
   FOR x IN (SELECT tran_line_item_desc
               FROM tran_line_item
              WHERE tran_id = pn_tran_id)
   LOOP
      v_return_string := v_return_string || v_delimiter || x.tran_line_item_desc;
      v_delimiter := CHR (13);
   END LOOP;

   RETURN v_return_string;
END;
/

-- Start of DDL Script for View PSS.VW_TLI_PIVOT_VIEW
-- Generated 9/3/2004 10:15:51 from PSS@usadbd03

CREATE OR REPLACE VIEW vw_tli_pivot_view (
   tran_id,
   tran_start_ts,
   consumer_id,
   consumer_acct_id,
   client_payment_type_cd,
   tran_settlement_amount,
   tran_line_item_desc )
AS
SELECT t.tran_id,
          t.tran_start_ts,
          consumer_id,
          t.consumer_acct_id,
          ps.client_payment_type_cd,
          t.tran_settlement_amount,
          sf_pivot_tran_line_item (t.tran_id)
     FROM tran_line_item tli, tran t, pos_pta pta, payment_subtype ps, consumer_acct ca
    WHERE t.tran_id = tli.tran_id
      AND t.pos_pta_id = pta.pos_pta_id
      AND pta.payment_subtype_id = ps.payment_subtype_id
      and t.consumer_acct_id = ca.consumer_acct_id
      GROUP BY t.tran_id,
         t.tran_start_ts,
         consumer_id,
         t.consumer_acct_id,
         ps.client_payment_type_cd,
         t.tran_settlement_amount,
         sf_pivot_tran_line_item (t.tran_id)
/

-- Grants for View
GRANT SELECT ON vw_tli_pivot_view TO esuds_operator_report_user
/

-- End of DDL Script for View PSS.VW_TLI_PIVOT_VIEW

-- End of DDL Script for Function PSS.SF_PIVOT_TRAN_LINE_ITEM
-- modified by wei guo, the following statement is converted by eric to one anonymous block in separated file
/* Formatted on 2004/09/02 17:45 (Formatter Plus v4.8.0) */
--SELECT      'update tran set tran_settlement_amount = '
--         || SUM (tli.tran_line_item_amount * tli.tran_line_item_quantity) 
--         || ' where tran_id = '
--         || tran_id || ';'
--    FROM tran_line_item tli
--GROUP BY tran_id
--/
