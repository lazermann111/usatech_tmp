-- Start of DDL Script for Table PSS.POS_PTA
-- Generated 14-Jul-2004 16:04:13 from PSS@usadbd02

CREATE TABLE pos_pta
    (pos_pta_id                     NUMBER(20,0) NOT NULL,
    pos_id                         NUMBER(20,0) NOT NULL,
    payment_subtype_id             NUMBER(20,0) NOT NULL,
    pos_pta_encrypt_key            VARCHAR2(192),
    pos_pta_activitation_ts        DATE,
    pos_pta_deactivation_ts        DATE,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    payment_subtype_key_id         NUMBER(20,0))

/

-- Grants for Table
GRANT DELETE ON pos_pta TO rerix_engine
/
GRANT INSERT ON pos_pta TO rerix_engine
/
GRANT SELECT ON pos_pta TO rerix_engine
/
GRANT UPDATE ON pos_pta TO rerix_engine
/
GRANT DELETE ON pos_pta TO web_user
/
GRANT INSERT ON pos_pta TO web_user
/
GRANT SELECT ON pos_pta TO web_user
/
GRANT UPDATE ON pos_pta TO web_user
/



-- Constraints for POS_PTA



ALTER TABLE pos_pta
ADD CONSTRAINT pk_pos_pta PRIMARY KEY (pos_pta_id)
USING INDEX

/


-- Triggers for POS_PTA

CREATE OR REPLACE TRIGGER trbi_pos_pta
 BEFORE
  INSERT
 ON pos_pta
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN


    IF :new.POS_PTA_id IS NULL THEN

      SELECT seq_POS_PTA_id.nextval
        into :new.POS_PTA_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_pos_pta
 BEFORE
  UPDATE
 ON pos_pta
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- Comments for POS_PTA

COMMENT ON TABLE pos_pta IS 'Contains the assignments of each POS to Authorities and Payment Types.'
/

-- End of DDL Script for Table PSS.POS_PTA

-- Foreign Key
ALTER TABLE pos_pta
ADD CONSTRAINT fk_pos_payment_type_auth FOREIGN KEY (pos_id)
REFERENCES pos (pos_id)
/

-- End of DDL script for Foreign Key(s)


-- Start of DDL Script for Table PSS.BLACKBRD_AUTHORITY
-- Generated 7/26/2004 2:29:47 PM from PSS@usadbd02

CREATE TABLE blackbrd_authority
    (blackbrd_authority_id          NUMBER(20,0) NOT NULL,
    blackbrd_authority_name        VARCHAR2(60),
    blackbrd_server_name           VARCHAR2(20) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/

-- Grants for Table
GRANT DELETE ON blackbrd_authority TO rerix_engine
/
GRANT INSERT ON blackbrd_authority TO rerix_engine
/
GRANT SELECT ON blackbrd_authority TO rerix_engine
/
GRANT UPDATE ON blackbrd_authority TO rerix_engine
/
GRANT DELETE ON blackbrd_authority TO web_user
/
GRANT INSERT ON blackbrd_authority TO web_user
/
GRANT SELECT ON blackbrd_authority TO web_user
/
GRANT UPDATE ON blackbrd_authority TO web_user
/



-- Indexes for BLACKBRD_AUTHORITY

CREATE INDEX inx_blkbrdauth_nm ON blackbrd_authority
  (
    blackbrd_authority_name         ASC
  )

/



-- Constraints for BLACKBRD_AUTHORITY

ALTER TABLE blackbrd_authority
ADD CONSTRAINT pk_blackbrd_authority PRIMARY KEY (blackbrd_authority_id)
USING INDEX

/


-- Triggers for BLACKBRD_AUTHORITY

CREATE OR REPLACE TRIGGER trbi_blackbrd_authority
 BEFORE
  INSERT
 ON blackbrd_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.blackbrd_authority_id IS NULL THEN

      SELECT seq_blackbrd_authority_id.nextval
        into :new.blackbrd_authority_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_blackbrd_authority
 BEFORE
  UPDATE
 ON blackbrd_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- Comments for BLACKBRD_AUTHORITY

COMMENT ON TABLE blackbrd_authority IS 'Contains every payment authority that can be used by the PSS. Examples are first horizon, first data, and USA Tech internal processing.'
/

-- End of DDL Script for Table PSS.BLACKBRD_AUTHORITY

-- Start of DDL Script for Table PSS.BLACKBRD_TENDER
-- Generated 7/26/2004 2:29:53 PM from PSS@usadbd02

CREATE TABLE blackbrd_tender
    (blackbrd_tender_id             CHAR(18) NOT NULL,
    blackbrd_tender_num            NUMBER(1,0) NOT NULL,
    blackbrd_authority_id          NUMBER(20,0) NOT NULL,
    blackbrd_tender_name           VARCHAR2(60) NOT NULL,
    blackbrd_tender_desc           VARCHAR2(60),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/

-- Grants for Table
GRANT DELETE ON blackbrd_tender TO rerix_engine
/
GRANT INSERT ON blackbrd_tender TO rerix_engine
/
GRANT SELECT ON blackbrd_tender TO rerix_engine
/
GRANT UPDATE ON blackbrd_tender TO rerix_engine
/
GRANT DELETE ON blackbrd_tender TO web_user
/
GRANT INSERT ON blackbrd_tender TO web_user
/
GRANT SELECT ON blackbrd_tender TO web_user
/
GRANT UPDATE ON blackbrd_tender TO web_user
/



-- Constraints for BLACKBRD_TENDER


ALTER TABLE blackbrd_tender
ADD CONSTRAINT pk_blackbrd_tender PRIMARY KEY (blackbrd_tender_id)
USING INDEX

/


-- Triggers for BLACKBRD_TENDER

CREATE OR REPLACE TRIGGER trbi_blackbrd_tender
 BEFORE
  INSERT
 ON blackbrd_tender
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN


    IF :new.blackbrd_tender_id IS NULL THEN

      SELECT seq_blackbrd_tender_id.nextval
        into :new.blackbrd_tender_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_blackbrd_tender
 BEFORE
  UPDATE
 ON blackbrd_tender
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.BLACKBRD_TENDER

-- Start of DDL Script for Table PSS.CC_AUTHORITY
-- Generated 7/26/2004 2:29:57 PM from PSS@usadbd02

CREATE TABLE cc_authority
    (cc_authority_id                NUMBER(20,0) NOT NULL,
    cc_authority_name              VARCHAR2(60),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)


/

-- Grants for Table
GRANT DELETE ON cc_authority TO rerix_engine
/
GRANT INSERT ON cc_authority TO rerix_engine
/
GRANT SELECT ON cc_authority TO rerix_engine
/
GRANT UPDATE ON cc_authority TO rerix_engine
/
GRANT DELETE ON cc_authority TO web_user
/
GRANT INSERT ON cc_authority TO web_user
/
GRANT SELECT ON cc_authority TO web_user
/
GRANT UPDATE ON cc_authority TO web_user
/



-- Indexes for CC_AUTHORITY

CREATE INDEX inx_cc_auth_name ON cc_authority
  (
    cc_authority_name               ASC
  )

/



-- Constraints for CC_AUTHORITY

ALTER TABLE cc_authority
ADD CONSTRAINT pk_cc_authority PRIMARY KEY (cc_authority_id)
USING INDEX

/


-- Triggers for CC_AUTHORITY

CREATE OR REPLACE TRIGGER trbi_cc_authority
 BEFORE
  INSERT
 ON cc_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.CC_AUTHORITY_id IS NULL THEN

      SELECT seq_CC_AUTHORITY_id.nextval
        into :new.CC_AUTHORITY_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_cc_authority
 BEFORE
  UPDATE
 ON cc_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- Comments for CC_AUTHORITY

COMMENT ON TABLE cc_authority IS 'Contains every payment authority that can be used by the PSS. Examples are first horizon, first data, and USA Tech internal processing.'
/

-- End of DDL Script for Table PSS.CC_AUTHORITY

-- Start of DDL Script for Table PSS.CC_MERCHANT
-- Generated 7/26/2004 2:30:02 PM from PSS@usadbd02

CREATE TABLE cc_merchant
    (cc_merchant_cd                 VARCHAR2(20) NOT NULL,
    cc_authority_id                NUMBER(20,0) NOT NULL,
    cc_merchant_doing_bus_as_name  VARCHAR2(30),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    cc_payment_type_id             NUMBER(20,0) NOT NULL)

/

-- Grants for Table
GRANT DELETE ON cc_merchant TO rerix_engine
/
GRANT INSERT ON cc_merchant TO rerix_engine
/
GRANT SELECT ON cc_merchant TO rerix_engine
/
GRANT UPDATE ON cc_merchant TO rerix_engine
/
GRANT DELETE ON cc_merchant TO web_user
/
GRANT INSERT ON cc_merchant TO web_user
/
GRANT SELECT ON cc_merchant TO web_user
/
GRANT UPDATE ON cc_merchant TO web_user
/



-- Constraints for CC_MERCHANT



ALTER TABLE cc_merchant
ADD CONSTRAINT pk_cc_merchant PRIMARY KEY (cc_merchant_cd, cc_authority_id, 
  cc_payment_type_id)
USING INDEX

/


-- Triggers for CC_MERCHANT

CREATE OR REPLACE TRIGGER trbu_cc_merchant
 BEFORE
  UPDATE
 ON cc_merchant
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbi_cc_merchant
 BEFORE
  INSERT
 ON cc_merchant
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN


    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.CC_MERCHANT

-- Start of DDL Script for Table PSS.CC_PAYMENT_TYPE
-- Generated 7/26/2004 2:30:07 PM from PSS@usadbd02

CREATE TABLE cc_payment_type
    (cc_payment_type_id             NUMBER(20,0) NOT NULL,
    cc_payment_type_desc           VARCHAR2(60),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/

-- Grants for Table
GRANT DELETE ON cc_payment_type TO rerix_engine
/
GRANT INSERT ON cc_payment_type TO rerix_engine
/
GRANT SELECT ON cc_payment_type TO rerix_engine
/
GRANT UPDATE ON cc_payment_type TO rerix_engine
/
GRANT DELETE ON cc_payment_type TO web_user
/
GRANT INSERT ON cc_payment_type TO web_user
/
GRANT SELECT ON cc_payment_type TO web_user
/
GRANT UPDATE ON cc_payment_type TO web_user
/



-- Constraints for CC_PAYMENT_TYPE

ALTER TABLE cc_payment_type
ADD CONSTRAINT pk_cc_payment_type PRIMARY KEY (cc_payment_type_id)
USING INDEX

/


-- Triggers for CC_PAYMENT_TYPE

CREATE OR REPLACE TRIGGER trbi_cc_payment_type
 BEFORE
  INSERT
 ON cc_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.CC_payment_type_id IS NULL THEN

      SELECT seq_CC_payment_type_id.nextval
        into :new.CC_payment_type_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_cc_payment_type
 BEFORE
  UPDATE
 ON cc_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.CC_PAYMENT_TYPE

-- Start of DDL Script for Table PSS.CC_TERMINAL
-- Generated 7/26/2004 2:30:12 PM from PSS@usadbd02

CREATE TABLE cc_terminal
    (cc_terminal_id                 NUMBER(20,0) NOT NULL,
    cc_terminal_cd                 VARCHAR2(20) NOT NULL,
    cc_merchant_cd                 VARCHAR2(20) NOT NULL,
    cc_authority_id                NUMBER(20,0) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    cc_payment_type_id             NUMBER(20,0) NOT NULL)

/

-- Grants for Table
GRANT DELETE ON cc_terminal TO rerix_engine
/
GRANT INSERT ON cc_terminal TO rerix_engine
/
GRANT SELECT ON cc_terminal TO rerix_engine
/
GRANT UPDATE ON cc_terminal TO rerix_engine
/
GRANT DELETE ON cc_terminal TO web_user
/
GRANT INSERT ON cc_terminal TO web_user
/
GRANT SELECT ON cc_terminal TO web_user
/
GRANT UPDATE ON cc_terminal TO web_user
/



-- Constraints for CC_TERMINAL

ALTER TABLE cc_terminal
ADD CONSTRAINT pk_cc_terminal PRIMARY KEY (cc_terminal_id)
USING INDEX

/


-- Triggers for CC_TERMINAL

CREATE OR REPLACE TRIGGER trbi_cc_terminal
 BEFORE
  INSERT
 ON cc_terminal
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.CC_TERMINAL_id IS NULL THEN

      SELECT seq_CC_TERMINAL_id.nextval
        into :new.CC_TERMINAL_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_cc_terminal
 BEFORE
  UPDATE
 ON cc_terminal
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.CC_TERMINAL

-- Start of DDL Script for Table PSS.INTERNAL_AUTHORITY
-- Generated 7/26/2004 2:30:16 PM from PSS@usadbd02

CREATE TABLE internal_authority
    (internal_authority_id          NUMBER(20,0) NOT NULL,
    internal_payment_type_id       NUMBER(20,0) NOT NULL,
    internal_authority_name        VARCHAR2(60),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/

-- Grants for Table
GRANT DELETE ON internal_authority TO rerix_engine
/
GRANT INSERT ON internal_authority TO rerix_engine
/
GRANT SELECT ON internal_authority TO rerix_engine
/
GRANT UPDATE ON internal_authority TO rerix_engine
/
GRANT DELETE ON internal_authority TO web_user
/
GRANT INSERT ON internal_authority TO web_user
/
GRANT SELECT ON internal_authority TO web_user
/
GRANT UPDATE ON internal_authority TO web_user
/



-- Indexes for INTERNAL_AUTHORITY

CREATE INDEX inx_intauthority_nm ON internal_authority
  (
    internal_authority_name         ASC
  )

/



-- Constraints for INTERNAL_AUTHORITY


ALTER TABLE internal_authority
ADD CONSTRAINT pk_internal_authority PRIMARY KEY (internal_authority_id)
USING INDEX

/


-- Triggers for INTERNAL_AUTHORITY

CREATE OR REPLACE TRIGGER trbi_internal_authority
 BEFORE
  INSERT
 ON internal_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN



    IF :new.internal_authority_id IS NULL THEN

      SELECT seq_internal_authority_id.nextval
        into :new.internal_authority_id
        FROM dual;

    END IF;
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_internal_authority
 BEFORE
  UPDATE
 ON internal_authority
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.INTERNAL_AUTHORITY

-- Start of DDL Script for Table PSS.INTERNAL_PAYMENT_TYPE
-- Generated 7/26/2004 2:30:21 PM from PSS@usadbd02

CREATE TABLE internal_payment_type
    (internal_payment_type_id       NUMBER(20,0) NOT NULL,
    internal_payment_type_desc     VARCHAR2(60),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/

-- Grants for Table
GRANT DELETE ON internal_payment_type TO rerix_engine
/
GRANT INSERT ON internal_payment_type TO rerix_engine
/
GRANT SELECT ON internal_payment_type TO rerix_engine
/
GRANT UPDATE ON internal_payment_type TO rerix_engine
/
GRANT DELETE ON internal_payment_type TO web_user
/
GRANT INSERT ON internal_payment_type TO web_user
/
GRANT SELECT ON internal_payment_type TO web_user
/
GRANT UPDATE ON internal_payment_type TO web_user
/



-- Constraints for INTERNAL_PAYMENT_TYPE

ALTER TABLE internal_payment_type
ADD CONSTRAINT pk_internal_payment_type PRIMARY KEY (internal_payment_type_id)
USING INDEX

/


-- Triggers for INTERNAL_PAYMENT_TYPE

CREATE OR REPLACE TRIGGER trbi_internal_payment_type
 BEFORE
  INSERT
 ON internal_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.internal_payment_type_id IS NULL THEN

      SELECT seq_internal_payment_type_id.nextval
        into :new.internal_payment_type_id
        FROM dual;

    END IF;


   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_internal_payment_type
 BEFORE
  UPDATE
 ON internal_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.INTERNAL_PAYMENT_TYPE

-- Start of DDL Script for Table PSS.PAYMENT_SUBTYPE
-- Generated 7/26/2004 2:30:25 PM from PSS@usadbd02

CREATE TABLE payment_subtype
    (payment_subtype_id             NUMBER(20,0) NOT NULL,
    rerix_payment_type_cd          CHAR(1) NOT NULL,
    payment_subtype_name           VARCHAR2(60) NOT NULL,
    payment_subtype_desc           VARCHAR2(60),
    payment_subtype_class          VARCHAR2(128),
    payment_subtype_table          VARCHAR2(30) NOT NULL,
    payment_subtype_key_name       VARCHAR2(30) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/




-- Constraints for PAYMENT_SUBTYPE


ALTER TABLE payment_subtype
ADD CONSTRAINT pk_payment_subtype PRIMARY KEY (payment_subtype_id)
USING INDEX

/


-- Triggers for PAYMENT_SUBTYPE

CREATE OR REPLACE TRIGGER trbi_payment_subtype
 BEFORE
  INSERT
 ON payment_subtype
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.payment_subtype_id IS NULL THEN

      SELECT seq_payment_subtype_id.nextval
        into :new.payment_subtype_id
        FROM dual;

    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbu_payment_subtype
 BEFORE
  UPDATE
 ON payment_subtype
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Table PSS.PAYMENT_SUBTYPE

-- Start of DDL Script for Table PSS.RERIX_PAYMENT_TYPE
-- Generated 7/26/2004 2:30:30 PM from PSS@usadbd02

CREATE TABLE rerix_payment_type
    (rerix_payment_type_cd          CHAR(1) NOT NULL,
    rerix_payment_type_desc        VARCHAR2(60),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)

/

-- Grants for Table
GRANT DELETE ON rerix_payment_type TO rerix_engine
/
GRANT INSERT ON rerix_payment_type TO rerix_engine
/
GRANT SELECT ON rerix_payment_type TO rerix_engine
/
GRANT UPDATE ON rerix_payment_type TO rerix_engine
/
GRANT DELETE ON rerix_payment_type TO web_user
/
GRANT INSERT ON rerix_payment_type TO web_user
/
GRANT SELECT ON rerix_payment_type TO web_user
/
GRANT UPDATE ON rerix_payment_type TO web_user
/



-- Constraints for RERIX_PAYMENT_TYPE

ALTER TABLE rerix_payment_type
ADD CONSTRAINT pk_rerix_payment_type PRIMARY KEY (rerix_payment_type_cd)
USING INDEX

/


-- Triggers for RERIX_PAYMENT_TYPE

CREATE OR REPLACE TRIGGER trbu_rerix_payment_type
 BEFORE
  UPDATE
 ON rerix_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
 SELECT
            :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
                 :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
          :new.last_updated_by
      FROM dual;
End;
/

CREATE OR REPLACE TRIGGER trbi_rerix_payment_type
 BEFORE
  INSERT
 ON rerix_payment_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin



 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- Comments for RERIX_PAYMENT_TYPE

COMMENT ON TABLE rerix_payment_type IS 'Lookup table containing every payment type accepted by the rerix. Examples are credit cards, student cards, work id tags, rfid, and cash.'
/

-- End of DDL Script for Table PSS.RERIX_PAYMENT_TYPE

-- Foreign Key
ALTER TABLE blackbrd_tender
ADD CONSTRAINT fk_blackbrd_tender_blackbrd_au FOREIGN KEY (blackbrd_authority_id)
REFERENCES blackbrd_authority (blackbrd_authority_id)
/
-- Foreign Key
ALTER TABLE cc_merchant
ADD CONSTRAINT fk_cc_merchant_cc_authority FOREIGN KEY (cc_authority_id)
REFERENCES cc_authority (cc_authority_id)
/
ALTER TABLE cc_merchant
ADD CONSTRAINT fk_cc_merchant_cc_payment_type FOREIGN KEY (cc_payment_type_id)
REFERENCES cc_payment_type (cc_payment_type_id)
/
-- Foreign Key
ALTER TABLE internal_authority
ADD CONSTRAINT fk_internal_authority_internal FOREIGN KEY (
  internal_payment_type_id)
REFERENCES internal_payment_type (internal_payment_type_id)
/
-- Foreign Key
ALTER TABLE payment_subtype
ADD CONSTRAINT fk_payment_subtype_rerix_payme FOREIGN KEY (rerix_payment_type_cd)
REFERENCES rerix_payment_type (rerix_payment_type_cd)
/
-- End of DDL script for Foreign Key(s)

ALTER TABLE pos_pta
ADD CONSTRAINT fk_pos_pta_payment_subtype FOREIGN KEY (payment_subtype_id)
REFERENCES payment_subtype (payment_subtype_id)
/



-- Start of DDL Script for Sequence PSS.SEQ_BLACKBRD_AUTHORITY_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_blackbrd_authority_id
  INCREMENT BY 1
  START WITH 2
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_BLACKBRD_AUTHORITY_ID

-- Start of DDL Script for Sequence PSS.SEQ_BLACKBRD_TENDER_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_blackbrd_tender_id
  INCREMENT BY 1
  START WITH 2
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_BLACKBRD_TENDER_ID

-- Start of DDL Script for Sequence PSS.SEQ_CC_AUTHORITY_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_cc_authority_id
  INCREMENT BY 1
  START WITH 3
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_CC_AUTHORITY_ID

-- Start of DDL Script for Sequence PSS.SEQ_CC_PAYMENT_TYPE_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_cc_payment_type_id
  INCREMENT BY 1
  START WITH 5
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_CC_PAYMENT_TYPE_ID

-- Start of DDL Script for Sequence PSS.SEQ_CC_TERMINAL_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_cc_terminal_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_CC_TERMINAL_ID

-- Start of DDL Script for Sequence PSS.SEQ_INTERNAL_AUTHORITY_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_internal_authority_id
  INCREMENT BY 1
  START WITH 2
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_INTERNAL_AUTHORITY_ID

-- Start of DDL Script for Sequence PSS.SEQ_INTERNAL_PAYMENT_TYPE_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_internal_payment_type_id
  INCREMENT BY 1
  START WITH 5
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence PSS.SEQ_INTERNAL_PAYMENT_TYPE_ID

-- Start of DDL Script for Sequence PSS.SEQ_POS_PTA_ID
-- Generated 7/26/2004 2:37:33 PM from PSS@usadbd02

CREATE SEQUENCE seq_pos_pta_id
  INCREMENT BY 1
  START WITH 516
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

INSERT INTO blackbrd_authority
(BLACKBRD_AUTHORITY_ID,BLACKBRD_AUTHORITY_NAME,BLACKBRD_SERVER_NAME)
VALUES
(1,'Blackboard Test Server','TEST')
/
INSERT INTO blackbrd_tender
(BLACKBRD_TENDER_ID,BLACKBRD_TENDER_NUM,BLACKBRD_AUTHORITY_ID,BLACKBRD_TENDER_NAME,BLACKBRD_TENDER_DESC)
VALUES
('1                 ',0,1,'Default','Default Tender')
/
INSERT INTO cc_payment_type
(CC_PAYMENT_TYPE_ID,CC_PAYMENT_TYPE_DESC)
VALUES
(1,'Visa')
/
INSERT INTO cc_payment_type
(CC_PAYMENT_TYPE_ID,CC_PAYMENT_TYPE_DESC)
VALUES
(2,'Mastercard')
/
INSERT INTO cc_payment_type
(CC_PAYMENT_TYPE_ID,CC_PAYMENT_TYPE_DESC)
VALUES
(3,'American Express')
/
INSERT INTO cc_payment_type
(CC_PAYMENT_TYPE_ID,CC_PAYMENT_TYPE_DESC)
VALUES
(4,'Diner''s Club')
/
INSERT INTO cc_authority
(CC_AUTHORITY_ID,CC_AUTHORITY_NAME)
VALUES
(1,'First Data')
/
INSERT INTO cc_authority
(CC_AUTHORITY_ID,CC_AUTHORITY_NAME)
VALUES
(2,'First Horizon')
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001362997',1,'AMERICAN COLLEGE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001195991',1,'AMERISUITE BUSINESS EXPR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001219999',1,'AMSUI FTLAUDPLNT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001220997',1,'AMSUI FTLDRL17TH',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001319997',1,'AMSUI MIAAIRPORT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001295999',1,'AMSUI OVERLND PK',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001279993',1,'AMSUI TULSA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001254996',1,'BESTWEST BUSINESS EXPRES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001259995',1,'BROWN HOTEL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001170994',1,'BUS EXP BW WEST PARTK',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001177999',1,'BUS EXP DOUB BOISE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001180993',1,'BUS EXP DOUB COL PARK',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001173998',1,'BUS EXP DOUB LARGO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001158999',1,'BUS EXP DOUB LOUIS 2',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001181991',1,'BUS EXP HI SEL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001116997',1,'BUSINESS EXP BELVEDERE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001118993',1,'BUSINESS EXP BW WA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001124991',1,'BUSINESS EXP CI BWI',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001140997',1,'BUSINESS EXP MARR HBG',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001141995',1,'BUSINESS EXP MARR HV',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001058991',1,'BUSINESS EXPRESS/111GST',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001111998',1,'BUSINESS EXPRESS/2222HAR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001092990',1,'BUSINESS EXPRESS/5401WIL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001074998',1,'BUSINESS EXPRESS/741OAKA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001010992',1,'BUSINESS EXPRESS/9461ROO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001008996',1,'BUSINESS EXPRESS/9700BLU',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001073990',1,'BUSINESS EXPRESS/ALDENLI',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001079997',1,'BUSINESS EXPRESS/FIRESTO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001043993',1,'BUSINESS EXPRESS/ONEILL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001360991',1,'BWESTRENOBUSINESSEXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001314998',1,'BWESTROMULUS BUS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001317991',1,'CARDINA INN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001183997',1,'CLARION BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001120999',1,'CLARION HOTELS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001305996',1,'COMFO ALBANY',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001016999',1,'COMFORT INN BUSINESS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001301995',1,'CRN PL WASH DC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001028994',1,'CROWNE PLAZA BUSINESSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001299991',1,'DBLETREE BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001310996',1,'DCLUB LAS VEGAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001324997',1,'DCLUB PALATINE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001307992',1,'DCLUB ST LOUIS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001306994',1,'DCLUB SYRACUSE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001312992',1,'DLUB HOUSTON',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001072992',1,'EMBASSY STE BUSINESS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001239997',1,'HILTO DESTIN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001132994',1,'HILTON BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001327990',1,'HOINN SEL ORLANDO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001358995',1,'HOINN SHREVEPORT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001328998',1,'HOINN STAMFORD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001344995',1,'HOLIDAYINN BUSINESS EXPR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001347998',1,'HOLNN ATHENS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001354994',1,'HOLNN BOLINGBROO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001238999',1,'HOLNN COLUMBIA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001237991',1,'HOLNN ELMHURST',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001351990',1,'HOLNN LAREDO CC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001346990',1,'HOLNN MCALLEN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001350992',1,'HOLNN MCALLEN CC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001268996',1,'HUNTSVILLE AIRPORT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001265992',1,'LENOX SUITES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001228990',1,'MARRI BETHESDA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001235995',1,'MARRI DURHAM CC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001233990',1,'MARRI MEMPHIS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001227992',1,'MARRI SPRINGFIELD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001245994',1,'MARRI WESTCHESTR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001451998',1,'MARRIOTT BUSINESSEXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001035999',1,'MICHGN_INST_RX8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001243999',1,'NEW ENGLAND LAW',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001479999',1,'PREPAIDEXPRES 8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001253998',1,'PUBLIC_FAX_888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001022997',1,'PUBLIC_PC_888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001256991',1,'PUBLICCOPIER888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001255993',1,'PUBLICPRINTR888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001145996',1,'QUALITYINN BUSINESS EXPR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001241993',1,'RADISON FAIRFIELD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001356999',1,'RADISSON MEMPHIS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001470998',1,'RAMADA BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001260993',1,'REDLION BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001304999',1,'SELF SERVE 888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001336991',1,'SHERATON DALLAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001240995',1,'SHERATONWEEHWKN BUS EXPR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001188996',1,'USA TECHNOLOGIES INC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001469990',1,'OMNICOM VENTURES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001387994',1,'VEND@AMSUI BOISE BUS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001377995',1,'VEND@AMSUI LOMBARDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001379991',1,'VEND@AMSUI MYSTIC BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001380999',1,'VEND@AMSUI PARAMUSBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001366998',1,'VEND@AMSUICHARCOL BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001382995',1,'VEND@AMSUIQUAILSPRBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001384991',1,'VEND@AMSUIRANCHOCOBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001386996',1,'VEND@AMSUISPORTLNDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001388992',1,'VEND@BONNIE RIDGE BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001393992',1,'VEND@CASINO AZTAR BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001394990',1,'VEND@CLARICARLISLEBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001395997',1,'VEND@CLARIDAVENPRTBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001396995',1,'VEND@CLARISACRAMNTBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001089996',1,'VEND@COCACOLA_8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001397993',1,'VEND@COMFO OAKLANDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001398991',1,'VEND@COMFOPANAMCTYBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001399999',1,'VEND@COMFOSALTLAKCBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001400995',1,'VEND@CRNPILAGUARDIBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001401993',1,'VEND@CRNPILAKOSWEGBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001403999',1,'VEND@CRNPILMEADWINDBUS-',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001405994',1,'VEND@DCLUB BOSTON BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001406992',1,'VEND@DCLUB DALLAS BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001407990',1,'VEND@DCLUB ORLANDOBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001408998',1,'VEND@DOYLEWASHNGTNBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001409996',1,'VEND@DRAKE OAKBROKBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001412990',1,'VEND@DTREE LOWELL BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001414996',1,'VEND@DTREESTAMONICBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001415993',1,'VEND@E-PORT 888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001439993',1,'VEND@E-PORT 888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001081993',1,'VEND@E-PORT 888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001416991',1,'VEND@ESUIT ATLAIRBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001417999',1,'VEND@ESUIT ATLANTABUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001418997',1,'VEND@FRENCHQTRBUSEXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001172990',1,'VEND@GEVALIA CAF�',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001421991',1,'VEND@HAMPT MTHOLLYBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001422999',1,'VEND@HAMPT SANJUANBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001420993',1,'VEND@HAMPTGREENBRGBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001424995',1,'VEND@HAWTHSCHUMBRGBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001426990',1,'VEND@HILTO JACKSONBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001427998',1,'VEND@HILTOSTLOUIS BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001434994',1,'VEND@HOLNN JFKBUSEXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001435991',1,'VEND@HOLNN LA BUSEXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001437997',1,'VEND@HOLNN LIVRPOLBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001438995',1,'VEND@HOLNN MUSCTINBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001429994',1,'VEND@HOLNNBATESVLEBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001430992',1,'VEND@HOLNNCLEVELNDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001431990',1,'VEND@HOLNNFTWORTH BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001440991',1,'VEND@HOLNNPLMBEACHBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001443995',1,'VEND@HOLNNSTATECOLBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001444993',1,'VEND@HOLNNSTRNGVLEBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001445990',1,'VEND@HOLNNWALLST BUS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001436999',1,'VEND@MACGRAY888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001448994',1,'VEND@MARRCSEATTLE BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001450990',1,'VEND@MARRI DULLES BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001456997',1,'VEND@MARRI OAKLND BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001461997',1,'VEND@MARRI TRUMBL BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001462995',1,'VEND@MARRI WESTLP BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001449992',1,'VEND@MARRICHATANOGBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001458993',1,'VEND@MARRIPITTSBRGHBUSEX',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001372996',1,'VEND@MCDONALDS8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001486994',1,'VEND@PARADISE PNT BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001463993',1,'VEND@RADIS GREENBYBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001464991',1,'VEND@RADIS KNOXVLEBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001465998',1,'VEND@RADIS TAMPA BUS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001468992',1,'VEND@RAMADAPLMBCH BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001471996',1,'VEND@REDLN EUREKA BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001472994',1,'VEND@REDLN KELSO BUSEXPR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001473992',1,'VEND@REDLN MEDFRD BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001474990',1,'VEND@REDLN PENDLTNBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001476995',1,'VEND@REDLN REDDNG BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001477993',1,'VEND@REDLN VANCUVRBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001475997',1,'VEND@REDLNPRTANGLSBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001478991',1,'VEND@REDLNWENATCHEBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001480997',1,'VEND@S4PT SHLLRPRKBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001481995',1,'VEND@SHERAPADRISLNDBUSEX',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001482993',1,'VEND@SHERASPRNGFLDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001483991',1,'VEND@SHONY HOUSTONBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001484999',1,'VEND@TARRYTOWNHSE BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001433996',1,'VEND@TRACFONE888-5614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001485996',1,'VEND@WESTIMORRSTWNBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001333998',1,'VILLANOVA UNIVERSITY',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001052994',1,'WYNDHAM BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001363995',1,'HYATT BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001027996',1,'RADISSON BUSINESS EXPRES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001218991',1,'RENAISSANCE BUSINESS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001365990',1,'SHERATON BUSINESS EXPRES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001389990',1,'WESTIN BUSINESS EXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001009994',1,'VEND@JETVEND877-588-2699',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001212994',1,'VEND@JETVEND877-588-2699',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001013996',1,'VEND@BWIAIRPRT8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001413998',1,'VEND@MSPAIRPRT8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001179995',1,'ACORTO COFFEE 8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001002999',1,'VEND@JACOB JAVITZ CTR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001321993',1,'VEND@CARWASH888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001348996',1,'VEND@LAUNDRY 8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001367996',1,'VEND@MACGRAY888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001176991',1,'VEND@ANDREW 888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001290990',1,'VEND@CINEMACHINE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001283995',1,'VEND@KODAK888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001189994',1,'VEND@UNIVSTDIO8885614748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001453994',1,'VEND@SHERATONHOTEL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001288994',1,'VEND@DR PEPPER',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001024993',1,'VEND@JFK AIRPORT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001229998',1,'ALTRIA CAFE REVALUE STN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001147992',1,'VEND@LEHMAN BROTHERS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001442997',1,'VEND@HOLNNSANTAANABUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001231994',1,'VEND@BLUE CROSS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001131996',1,'CITYOFOAKLAND BUSEXPRESS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001012998',1,'VEND@M\&M CANDIES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001337999',1,'WEB SERVICES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001251992',1,'STANDARD-CHANGE TOKENS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001467994',1,'VEND@GLAXOSMITHKLINE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001349994',1,'VEND@GENERAL CONF SDA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001359993',1,'VEND@CHEVRON-TEXACO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001459991',1,'VEND@HERCULES REVALUE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001204991',1,'VENDSTREAM VENDING MACH',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001210998',1,'REST ASSC CITIGROUP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001242991',1,'RADIS HOTL MEMPHIS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001230996',1,'MARRI DALLAS TX',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001411992',1,'VEND@DTREE DELMAR BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001125998',1,'BUSINESS EXP CI CANTON',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001006990',1,'BUSINESS EXPRESS/1450EAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001392994',1,'VEND@BWESTLONGVIEWBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001368994',1,'VEND@AMSUI COLUMB BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001454992',1,'VEND@MARRILINCLNSHBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001232992',1,'MARRI RALCRABTRE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001455999',1,'VEND@MARRIOAKBROK BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001224999',1,'MARRI DELRAY BEACH',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001011990',1,'BUSINESS EXPRESS/1111NEL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001281999',1,'AMSUI SECAUCUS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001194994',1,'AMSUI ALPHRETTA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001192998',1,'AMSUI ARLINGTON',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001193996',1,'AMSUI ARLINGTON HEIGHTS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001205998',1,'AMSUI BUSCHGARDEN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001211996',1,'AMSUI DALWESTEND',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001207994',1,'AMSUI DEERFIELD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001214990',1,'AMSUI FLAGSTAFF',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001217993',1,'AMSUI FTWRTHURST',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001272998',1,'AMSUI JACKSONVLL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001322991',1,'AMSUI LAS COLINAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001273996',1,'AMSUI LAS VEGAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001274994',1,'AMSUI LITTLE ROC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001320995',1,'AMSUI MEMCORDOVA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001318999',1,'AMSUI MIAKENDALL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001250994',1,'AMSUI NASHVILLE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001247990',1,'AMSUI OKLAH CITY',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001248998',1,'AMSUI OPRYLAND',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001296997',1,'AMSUI ORLANDO CC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001293994',1,'AMSUI PARKMEADOW',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001287996',1,'AMSUI RICHLNNSBR',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001285990',1,'AMSUI SAN ANTONIO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001284993',1,'AMSUI SAN ANTONIO NO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001282997',1,'AMSUI SCOTTSDALE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001326992',1,'AMSUI TOPEKA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001137993',1,'BUSINESS EXP HI KY',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001033994',1,'BUSINESS EXPRESS/1100DRU',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001303991',1,'COUNTRY INN CUYA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001311994',1,'DCLUB JERSEY CITY',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001330994',1,'HOINN CLINTON',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001353996',1,'HOLNN CENTER PNT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001343997',1,'HOLNN CLEARWATER',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001345992',1,'HOLNN STATESVILL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001226994',1,'MARRI SADDLEBROO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001370990',1,'VEND@AMSUI DUBLIN BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001371998',1,'VEND@AMSUI ENGLEWDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001369992',1,'VEND@AMSUI LIVONIABUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001378993',1,'VEND@AMSUI MEMPHISBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001383993',1,'VEND@AMSUI RALEIGHBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001391996',1,'VEND@BWEST LAREDO BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001402991',1,'VEND@CRNPILASVEGASBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001404997',1,'VEND@CRNPL OMAHA BUS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001410994',1,'VEND@DTREE BELLEVUBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001423997',1,'VEND@HANOVER INN BUS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001441999',1,'VEND@HOLNN RICHMNDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001446998',1,'VEND@HOTEL TRITON BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001447996',1,'VEND@MARRCNWCRRLLTNBUSEX',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001341991',1,'ROCKY GAP LODGE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001373994',1,'VEND@AMSUIGWINNETTBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001338997',1,'SHERATON BROOKHOLLOW',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001325994',1,'BWEST AKRON',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001277997',1,'AMSUI TAMPAIRP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001208992',1,'AMSUI COLUMBIA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001203993',1,'AMSUI BRENTWOOD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001202995',1,'AMSUI BLLUE ASH',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001201997',1,'AMSUI BRMLNVRNSS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001090994',1,'BUSINESS EXPRESS/UNIVER',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001045998',1,'BUSINESS EXPRESS/1501WES',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001018995',1,'BUSINESS EXPRESS/4545LIN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001004995',1,'BUSINESS EXPRESS/5301NW3',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001270992',1,'AMSUI  INDIANAPLS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001361999',1,'AMSUI ALBUQUERQUE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001191990',1,'AMSUI ALBUQUPTWN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001196999',1,'AMSUI ATLWNDWARD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001197997',1,'AMSUI AUBURN HILL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001199993',1,'AMSUI BATON ROUGE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001200999',1,'AMSUI BRMRVRCHAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001206996',1,'AMSUI CHARLOTTE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001209990',1,'AMSUI COLUMBUS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001213992',1,'AMSUI EDENPRAIRIE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001215997',1,'AMSUI FLORENCE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001216995',1,'AMSUI GREENSBOR0',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001222993',1,'AMSUI GREENSPOIN',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001221995',1,'AMSUI GREENVILLE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001223991',1,'AMSUI INDEPENDENCE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001271990',1,'AMSUI ITASCA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001275991',1,'AMSUI LOUISVILLE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001276999',1,'AMSUI MEDFORD',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001249996',1,'AMSUI ORLANDO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001294992',1,'AMSUI PARKCENTRL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001292996',1,'AMSUI PEACHTREE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001291998',1,'AMSUI PHOENIX MTC',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001289992',1,'AMSUI PRINCETON',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001278995',1,'AMSUI TEMPE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001264995',1,'AMSUI VERNHLLS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001286998',1,'AMSUI VLLYVW MALL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001263997',1,'AMSUI WARRENVILLE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001161993',1,'BUS EXP HI CORTER',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001168998',1,'BUS EXP LATHAM',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001163999',1,'BUS EXP MARR HART',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001178997',1,'BUS EXP MARR SCOTT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001113994',1,'BUSINESS EXP 4 PTS MEM',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001087990',1,'BUSINESS EXPRESS/1011EAS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001007998',1,'BUSINESS EXPRESS/4700SAL',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001019993',1,'BUSINESS EXPRESS/611OCEA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001316993',1,'CASA MARINA RSRT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001313990',1,'DCLUB HOLLYWOOD s/b',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001298993',1,'DRAWBRIDGE ESTATE',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001252990',1,'HAMPT CHICAGO',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001262999',1,'HAMPT MOREHEAD s/b',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001352998',1,'HOLNN CHICAGO MRT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001225996',1,'MARRI GREENBELT',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001381997',1,'VEND@AMSUI PLANO BUS EXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001374992',1,'VEND@AMSUI IRVING BUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001375999',1,'VEND@AMSUIJOHNSCRKBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001376997',1,'VEND@AMSUILAKELANDBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001385998',1,'VEND@AMSUISCHAUMBGBUSEXP',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001332990',1,'WESTM FAIRBANKS',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001362997',1,'AMERICAN COLLEGE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001195991',1,'AMERISUITE BUSINESS EXPR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001219999',1,'AMSUI FTLAUDPLNT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001220997',1,'AMSUI FTLDRL17TH',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001319997',1,'AMSUI MIAAIRPORT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001295999',1,'AMSUI OVERLND PK',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001279993',1,'AMSUI TULSA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001254996',1,'BESTWEST BUSINESS EXPRES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001259995',1,'BROWN HOTEL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001170994',1,'BUS EXP BW WEST PARTK',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001177999',1,'BUS EXP DOUB BOISE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001180993',1,'BUS EXP DOUB COL PARK',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001173998',1,'BUS EXP DOUB LARGO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001158999',1,'BUS EXP DOUB LOUIS 2',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001181991',1,'BUS EXP HI SEL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001116997',1,'BUSINESS EXP BELVEDERE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001118993',1,'BUSINESS EXP BW WA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001124991',1,'BUSINESS EXP CI BWI',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001140997',1,'BUSINESS EXP MARR HBG',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001141995',1,'BUSINESS EXP MARR HV',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001058991',1,'BUSINESS EXPRESS/111GST',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001111998',1,'BUSINESS EXPRESS/2222HAR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001092990',1,'BUSINESS EXPRESS/5401WIL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001074998',1,'BUSINESS EXPRESS/741OAKA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001010992',1,'BUSINESS EXPRESS/9461ROO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001008996',1,'BUSINESS EXPRESS/9700BLU',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001073990',1,'BUSINESS EXPRESS/ALDENLI',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001079997',1,'BUSINESS EXPRESS/FIRESTO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001043993',1,'BUSINESS EXPRESS/ONEILL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001360991',1,'BWESTRENOBUSINESSEXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001314998',1,'BWESTROMULUS BUS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001317991',1,'CARDINA INN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001183997',1,'CLARION BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001120999',1,'CLARION HOTELS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001305996',1,'COMFO ALBANY',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001016999',1,'COMFORT INN BUSINESS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001301995',1,'CRN PL WASH DC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001028994',1,'CROWNE PLAZA BUSINESSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001299991',1,'DBLETREE BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001310996',1,'DCLUB LAS VEGAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001324997',1,'DCLUB PALATINE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001307992',1,'DCLUB ST LOUIS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001306994',1,'DCLUB SYRACUSE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001312992',1,'DLUB HOUSTON',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001072992',1,'EMBASSY STE BUSINESS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001239997',1,'HILTO DESTIN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001132994',1,'HILTON BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001327990',1,'HOINN SEL ORLANDO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001358995',1,'HOINN SHREVEPORT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001328998',1,'HOINN STAMFORD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001344995',1,'HOLIDAYINN BUSINESS EXPR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001347998',1,'HOLNN ATHENS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001354994',1,'HOLNN BOLINGBROO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001238999',1,'HOLNN COLUMBIA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001237991',1,'HOLNN ELMHURST',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001351990',1,'HOLNN LAREDO CC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001346990',1,'HOLNN MCALLEN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001350992',1,'HOLNN MCALLEN CC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001268996',1,'HUNTSVILLE AIRPORT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001265992',1,'LENOX SUITES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001228990',1,'MARRI BETHESDA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001235995',1,'MARRI DURHAM CC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001233990',1,'MARRI MEMPHIS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001227992',1,'MARRI SPRINGFIELD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001245994',1,'MARRI WESTCHESTR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001451998',1,'MARRIOTT BUSINESSEXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001035999',1,'MICHGN_INST_RX8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001243999',1,'NEW ENGLAND LAW',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001479999',1,'PREPAIDEXPRES 8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001253998',1,'PUBLIC_FAX_888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001022997',1,'PUBLIC_PC_888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001256991',1,'PUBLICCOPIER888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001255993',1,'PUBLICPRINTR888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001145996',1,'QUALITYINN BUSINESS EXPR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001241993',1,'RADISON FAIRFIELD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001356999',1,'RADISSON MEMPHIS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001470998',1,'RAMADA BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001260993',1,'REDLION BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001304999',1,'SELF SERVE 888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001336991',1,'SHERATON DALLAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001240995',1,'SHERATONWEEHWKN BUS EXPR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001188996',1,'USA TECHNOLOGIES INC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001469990',1,'OMNICOM VENTURES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001387994',1,'VEND@AMSUI BOISE BUS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001377995',1,'VEND@AMSUI LOMBARDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001379991',1,'VEND@AMSUI MYSTIC BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001380999',1,'VEND@AMSUI PARAMUSBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001366998',1,'VEND@AMSUICHARCOL BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001382995',1,'VEND@AMSUIQUAILSPRBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001384991',1,'VEND@AMSUIRANCHOCOBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001386996',1,'VEND@AMSUISPORTLNDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001388992',1,'VEND@BONNIE RIDGE BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001393992',1,'VEND@CASINO AZTAR BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001394990',1,'VEND@CLARICARLISLEBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001395997',1,'VEND@CLARIDAVENPRTBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001396995',1,'VEND@CLARISACRAMNTBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001089996',1,'VEND@COCACOLA_8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001397993',1,'VEND@COMFO OAKLANDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001398991',1,'VEND@COMFOPANAMCTYBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001399999',1,'VEND@COMFOSALTLAKCBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001400995',1,'VEND@CRNPILAGUARDIBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001401993',1,'VEND@CRNPILAKOSWEGBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001403999',1,'VEND@CRNPILMEADWINDBUS-',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001405994',1,'VEND@DCLUB BOSTON BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001406992',1,'VEND@DCLUB DALLAS BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001407990',1,'VEND@DCLUB ORLANDOBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001408998',1,'VEND@DOYLEWASHNGTNBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001409996',1,'VEND@DRAKE OAKBROKBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001412990',1,'VEND@DTREE LOWELL BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001414996',1,'VEND@DTREESTAMONICBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001415993',1,'VEND@E-PORT 888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001439993',1,'VEND@E-PORT 888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001081993',1,'VEND@E-PORT 888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001416991',1,'VEND@ESUIT ATLAIRBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001417999',1,'VEND@ESUIT ATLANTABUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001418997',1,'VEND@FRENCHQTRBUSEXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001172990',1,'VEND@GEVALIA CAF�',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001421991',1,'VEND@HAMPT MTHOLLYBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001422999',1,'VEND@HAMPT SANJUANBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001420993',1,'VEND@HAMPTGREENBRGBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001424995',1,'VEND@HAWTHSCHUMBRGBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001426990',1,'VEND@HILTO JACKSONBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001427998',1,'VEND@HILTOSTLOUIS BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001434994',1,'VEND@HOLNN JFKBUSEXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001435991',1,'VEND@HOLNN LA BUSEXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001437997',1,'VEND@HOLNN LIVRPOLBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001438995',1,'VEND@HOLNN MUSCTINBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001429994',1,'VEND@HOLNNBATESVLEBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001430992',1,'VEND@HOLNNCLEVELNDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001431990',1,'VEND@HOLNNFTWORTH BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001440991',1,'VEND@HOLNNPLMBEACHBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001443995',1,'VEND@HOLNNSTATECOLBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001444993',1,'VEND@HOLNNSTRNGVLEBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001445990',1,'VEND@HOLNNWALLST BUS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001436999',1,'VEND@MACGRAY888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001448994',1,'VEND@MARRCSEATTLE BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001450990',1,'VEND@MARRI DULLES BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001456997',1,'VEND@MARRI OAKLND BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001461997',1,'VEND@MARRI TRUMBL BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001462995',1,'VEND@MARRI WESTLP BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001449992',1,'VEND@MARRICHATANOGBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001458993',1,'VEND@MARRIPITTSBRGHBUSEX',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001372996',1,'VEND@MCDONALDS8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001486994',1,'VEND@PARADISE PNT BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001463993',1,'VEND@RADIS GREENBYBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001464991',1,'VEND@RADIS KNOXVLEBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001465998',1,'VEND@RADIS TAMPA BUS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001468992',1,'VEND@RAMADAPLMBCH BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001471996',1,'VEND@REDLN EUREKA BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001472994',1,'VEND@REDLN KELSO BUSEXPR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001473992',1,'VEND@REDLN MEDFRD BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001474990',1,'VEND@REDLN PENDLTNBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001476995',1,'VEND@REDLN REDDNG BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001477993',1,'VEND@REDLN VANCUVRBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001475997',1,'VEND@REDLNPRTANGLSBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001478991',1,'VEND@REDLNWENATCHEBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001480997',1,'VEND@S4PT SHLLRPRKBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001481995',1,'VEND@SHERAPADRISLNDBUSEX',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001482993',1,'VEND@SHERASPRNGFLDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001483991',1,'VEND@SHONY HOUSTONBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001484999',1,'VEND@TARRYTOWNHSE BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001433996',1,'VEND@TRACFONE888-5614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001485996',1,'VEND@WESTIMORRSTWNBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001333998',1,'VILLANOVA UNIVERSITY',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001052994',1,'WYNDHAM BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001363995',1,'HYATT BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001027996',1,'RADISSON BUSINESS EXPRES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001218991',1,'RENAISSANCE BUSINESS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001365990',1,'SHERATON BUSINESS EXPRES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001389990',1,'WESTIN BUSINESS EXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001009994',1,'VEND@JETVEND877-588-2699',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001212994',1,'VEND@JETVEND877-588-2699',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001013996',1,'VEND@BWIAIRPRT8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001413998',1,'VEND@MSPAIRPRT8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001179995',1,'ACORTO COFFEE 8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001002999',1,'VEND@JACOB JAVITZ CTR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001321993',1,'VEND@CARWASH888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001348996',1,'VEND@LAUNDRY 8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001367996',1,'VEND@MACGRAY888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001176991',1,'VEND@ANDREW 888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001290990',1,'VEND@CINEMACHINE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001283995',1,'VEND@KODAK888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001189994',1,'VEND@UNIVSTDIO8885614748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001453994',1,'VEND@SHERATONHOTEL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001288994',1,'VEND@DR PEPPER',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001024993',1,'VEND@JFK AIRPORT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001229998',1,'ALTRIA CAFE REVALUE STN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001147992',1,'VEND@LEHMAN BROTHERS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001442997',1,'VEND@HOLNNSANTAANABUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001231994',1,'VEND@BLUE CROSS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001131996',1,'CITYOFOAKLAND BUSEXPRESS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001012998',1,'VEND@M\&M CANDIES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001337999',1,'WEB SERVICES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001251992',1,'STANDARD-CHANGE TOKENS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001467994',1,'VEND@GLAXOSMITHKLINE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001349994',1,'VEND@GENERAL CONF SDA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001359993',1,'VEND@CHEVRON-TEXACO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001459991',1,'VEND@HERCULES REVALUE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001204991',1,'VENDSTREAM VENDING MACH',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001210998',1,'REST ASSC CITIGROUP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001242991',1,'RADIS HOTL MEMPHIS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001230996',1,'MARRI DALLAS TX',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001411992',1,'VEND@DTREE DELMAR BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001125998',1,'BUSINESS EXP CI CANTON',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001006990',1,'BUSINESS EXPRESS/1450EAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001392994',1,'VEND@BWESTLONGVIEWBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001368994',1,'VEND@AMSUI COLUMB BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001454992',1,'VEND@MARRILINCLNSHBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001232992',1,'MARRI RALCRABTRE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001455999',1,'VEND@MARRIOAKBROK BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001224999',1,'MARRI DELRAY BEACH',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001011990',1,'BUSINESS EXPRESS/1111NEL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001281999',1,'AMSUI SECAUCUS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001194994',1,'AMSUI ALPHRETTA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001192998',1,'AMSUI ARLINGTON',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001193996',1,'AMSUI ARLINGTON HEIGHTS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001205998',1,'AMSUI BUSCHGARDEN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001211996',1,'AMSUI DALWESTEND',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001207994',1,'AMSUI DEERFIELD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001214990',1,'AMSUI FLAGSTAFF',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001217993',1,'AMSUI FTWRTHURST',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001272998',1,'AMSUI JACKSONVLL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001322991',1,'AMSUI LAS COLINAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001273996',1,'AMSUI LAS VEGAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001274994',1,'AMSUI LITTLE ROC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001320995',1,'AMSUI MEMCORDOVA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001318999',1,'AMSUI MIAKENDALL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001250994',1,'AMSUI NASHVILLE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001247990',1,'AMSUI OKLAH CITY',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001248998',1,'AMSUI OPRYLAND',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001296997',1,'AMSUI ORLANDO CC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001293994',1,'AMSUI PARKMEADOW',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001287996',1,'AMSUI RICHLNNSBR',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001285990',1,'AMSUI SAN ANTONIO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001284993',1,'AMSUI SAN ANTONIO NO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001282997',1,'AMSUI SCOTTSDALE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001326992',1,'AMSUI TOPEKA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001137993',1,'BUSINESS EXP HI KY',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001033994',1,'BUSINESS EXPRESS/1100DRU',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001303991',1,'COUNTRY INN CUYA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001311994',1,'DCLUB JERSEY CITY',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001330994',1,'HOINN CLINTON',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001353996',1,'HOLNN CENTER PNT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001343997',1,'HOLNN CLEARWATER',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001345992',1,'HOLNN STATESVILL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001226994',1,'MARRI SADDLEBROO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001370990',1,'VEND@AMSUI DUBLIN BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001371998',1,'VEND@AMSUI ENGLEWDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001369992',1,'VEND@AMSUI LIVONIABUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001378993',1,'VEND@AMSUI MEMPHISBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001383993',1,'VEND@AMSUI RALEIGHBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001391996',1,'VEND@BWEST LAREDO BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001402991',1,'VEND@CRNPILASVEGASBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001404997',1,'VEND@CRNPL OMAHA BUS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001410994',1,'VEND@DTREE BELLEVUBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001423997',1,'VEND@HANOVER INN BUS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001441999',1,'VEND@HOLNN RICHMNDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001446998',1,'VEND@HOTEL TRITON BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001447996',1,'VEND@MARRCNWCRRLLTNBUSEX',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001341991',1,'ROCKY GAP LODGE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001373994',1,'VEND@AMSUIGWINNETTBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001338997',1,'SHERATON BROOKHOLLOW',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001325994',1,'BWEST AKRON',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001277997',1,'AMSUI TAMPAIRP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001208992',1,'AMSUI COLUMBIA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001203993',1,'AMSUI BRENTWOOD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001202995',1,'AMSUI BLLUE ASH',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001201997',1,'AMSUI BRMLNVRNSS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001090994',1,'BUSINESS EXPRESS/UNIVER',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001045998',1,'BUSINESS EXPRESS/1501WES',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001018995',1,'BUSINESS EXPRESS/4545LIN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001004995',1,'BUSINESS EXPRESS/5301NW3',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001270992',1,'AMSUI  INDIANAPLS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001361999',1,'AMSUI ALBUQUERQUE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001191990',1,'AMSUI ALBUQUPTWN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001196999',1,'AMSUI ATLWNDWARD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001197997',1,'AMSUI AUBURN HILL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001199993',1,'AMSUI BATON ROUGE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001200999',1,'AMSUI BRMRVRCHAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001206996',1,'AMSUI CHARLOTTE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001209990',1,'AMSUI COLUMBUS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001213992',1,'AMSUI EDENPRAIRIE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001215997',1,'AMSUI FLORENCE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001216995',1,'AMSUI GREENSBOR0',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001222993',1,'AMSUI GREENSPOIN',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001221995',1,'AMSUI GREENVILLE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001223991',1,'AMSUI INDEPENDENCE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001271990',1,'AMSUI ITASCA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001275991',1,'AMSUI LOUISVILLE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001276999',1,'AMSUI MEDFORD',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001249996',1,'AMSUI ORLANDO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001294992',1,'AMSUI PARKCENTRL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001292996',1,'AMSUI PEACHTREE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001291998',1,'AMSUI PHOENIX MTC',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001289992',1,'AMSUI PRINCETON',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001278995',1,'AMSUI TEMPE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001264995',1,'AMSUI VERNHLLS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001286998',1,'AMSUI VLLYVW MALL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001263997',1,'AMSUI WARRENVILLE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001161993',1,'BUS EXP HI CORTER',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001168998',1,'BUS EXP LATHAM',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001163999',1,'BUS EXP MARR HART',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001178997',1,'BUS EXP MARR SCOTT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001113994',1,'BUSINESS EXP 4 PTS MEM',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001087990',1,'BUSINESS EXPRESS/1011EAS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001007998',1,'BUSINESS EXPRESS/4700SAL',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001019993',1,'BUSINESS EXPRESS/611OCEA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001316993',1,'CASA MARINA RSRT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001313990',1,'DCLUB HOLLYWOOD s/b',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001298993',1,'DRAWBRIDGE ESTATE',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001252990',1,'HAMPT CHICAGO',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001262999',1,'HAMPT MOREHEAD s/b',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001352998',1,'HOLNN CHICAGO MRT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001225996',1,'MARRI GREENBELT',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001381997',1,'VEND@AMSUI PLANO BUS EXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001374992',1,'VEND@AMSUI IRVING BUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001375999',1,'VEND@AMSUIJOHNSCRKBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001376997',1,'VEND@AMSUILAKELANDBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001385998',1,'VEND@AMSUISCHAUMBGBUSEXP',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('179001332990',1,'WESTM FAIRBANKS',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2376941712',1,'VEND AT KODAK',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2376939278',1,'VEND AT COCA-COLA',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2206685190',1,'VEND AT E-PORT 888-561-4748',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2376955027',1,'SONY PICTURESTATION',1)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2376941712',1,'VEND AT KODAK',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2376939278',1,'VEND AT COCA-COLA',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2206685190',1,'VEND AT E-PORT 888-561-4748',2)
/
INSERT INTO cc_merchant
(CC_MERCHANT_CD,CC_AUTHORITY_ID,CC_MERCHANT_DOING_BUS_AS_NAME,CC_PAYMENT_TYPE_ID)
VALUES
('2376955027',1,'SONY PICTURESTATION',2)
/
INSERT INTO internal_payment_type
(INTERNAL_PAYMENT_TYPE_ID,INTERNAL_PAYMENT_TYPE_DESC)
VALUES
(1,'Sony Special Card')
/
INSERT INTO internal_payment_type
(INTERNAL_PAYMENT_TYPE_ID,INTERNAL_PAYMENT_TYPE_DESC)
VALUES
(2,'CMU Student Card')
/
INSERT INTO internal_payment_type
(INTERNAL_PAYMENT_TYPE_ID,INTERNAL_PAYMENT_TYPE_DESC)
VALUES
(3,'USA Tech Employee Card')
/
CREATE SEQUENCE seq_payment_subtype_id
  INCREMENT BY 1
  START WITH 6
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


INSERT INTO rerix_payment_type
(RERIX_PAYMENT_TYPE_CD,RERIX_PAYMENT_TYPE_DESC)
VALUES
('M','Cash')
/
INSERT INTO rerix_payment_type
(RERIX_PAYMENT_TYPE_CD,RERIX_PAYMENT_TYPE_DESC)
VALUES
('C','Credit Card')
/
INSERT INTO rerix_payment_type
(RERIX_PAYMENT_TYPE_CD,RERIX_PAYMENT_TYPE_DESC)
VALUES
('S','Special Card')
/
INSERT INTO rerix_payment_type
(RERIX_PAYMENT_TYPE_CD,RERIX_PAYMENT_TYPE_DESC)
VALUES
('R','RFID')
/

-- modified by wei guo, add entries so the table has PAYMENT_SUBTYPE_ID from 1 to 9
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(1,'S','Special Card',NULL,NULL,'INTERNAL_AUTHORITY','INTERNAL_AUTHORITY_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(2,'S','Special Card',NULL,NULL,'INTERNAL_AUTHORITY','INTERNAL_AUTHORITY_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(3,'M','Cash',NULL,NULL,'Not Applicable','Not Applicable')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(4,'R','RFID',NULL,NULL,'INTERNAL_AUTHORITY','INTERNAL_AUTHORITY_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(5,'C','Credit Card',NULL,NULL,'CC_TERMINAL','CC_TERMINAL_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(6,'S','Blackboard',NULL,NULL,'BLACKBRD_TENDER','BLACKBRD_TENDER_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(7,'S','Blackboard',NULL,NULL,'BLACKBRD_TENDER','BLACKBRD_TENDER_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(8,'S','Blackboard',NULL,NULL,'BLACKBRD_TENDER','BLACKBRD_TENDER_ID')
/
INSERT INTO payment_subtype
(PAYMENT_SUBTYPE_ID,RERIX_PAYMENT_TYPE_CD,PAYMENT_SUBTYPE_NAME,PAYMENT_SUBTYPE_DESC,PAYMENT_SUBTYPE_CLASS,PAYMENT_SUBTYPE_TABLE,PAYMENT_SUBTYPE_KEY_NAME)
VALUES
(9,'S','Blackboard',NULL,NULL,'BLACKBRD_TENDER','BLACKBRD_TENDER_ID')
/

