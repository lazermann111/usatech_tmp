ALTER TABLE MACHINE 
 ADD (
  CREATED_BY VARCHAR2 (30),
  CREATED_TS DATE,
  LAST_UPDATED_BY VARCHAR2 (30),
  LAST_UPDATED_TS DATE
 )
/

ALTER TABLE LOCATION 
 ADD (
  CREATED_BY VARCHAR2 (30),
  CREATED_TS DATE,
  LAST_UDPATED_BY VARCHAR2 (30),
  LAST_UPDATED_TS DATE
 )
/

ALTER TABLE machine_hierarchy 
 ADD (
  CREATED_BY VARCHAR2 (30),
  CREATED_TS DATE,
  LAST_UPDATED_BY VARCHAR2 (30),
  LAST_UPDATED_TS DATE
 )
/




CREATE OR REPLACE TRIGGER trbi_machine
 BEFORE
  INSERT
 ON machine
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin


 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- End of DDL Script for Trigger PSS.TRBI_machine
-- Start of DDL Script for Trigger PSS.TRBU_machine
-- Generated 8/10/2004 10:55:41 AM from PSS@usadbd03

CREATE OR REPLACE TRIGGER trbu_machine
 BEFORE
  UPDATE
 ON machine
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER trbi_location
 BEFORE
  INSERT
 ON location
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin


 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- End of DDL Script for Trigger PSS.TRBI_location
-- Start of DDL Script for Trigger PSS.TRBU_location
-- Generated 8/10/2004 10:55:41 AM from PSS@usadbd03

CREATE OR REPLACE TRIGGER trbu_location
 BEFORE
  UPDATE
 ON location
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER trbi_transaction_record_hist
 BEFORE
  INSERT
 ON transaction_record_hist
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin


 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- End of DDL Script for Trigger PSS.TRBI_transaction_record_hist
-- Start of DDL Script for Trigger PSS.TRBU_transaction_record_hist
-- Generated 8/10/2004 10:55:41 AM from PSS@usadbd03

CREATE OR REPLACE TRIGGER trbu_transaction_record_hist
 BEFORE
  UPDATE
 ON transaction_record_hist
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER trbi_transaction_failures
 BEFORE
  INSERT
 ON transaction_failures
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin


 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- End of DDL Script for Trigger PSS.TRBI_transaction_failures
-- Start of DDL Script for Trigger PSS.TRBU_transaction_failures
-- Generated 8/10/2004 10:55:41 AM from PSS@usadbd03

CREATE OR REPLACE TRIGGER trbu_transaction_failures
 BEFORE
  UPDATE
 ON transaction_failures
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER trbi_machine_hierarchy
 BEFORE
  INSERT
 ON machine_hierarchy
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin


 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- End of DDL Script for Trigger PSS.TRBI_machine_hierarchy
-- Start of DDL Script for Trigger PSS.TRBU_machine_hierarchy
-- Generated 8/10/2004 10:55:41 AM from PSS@usadbd03

CREATE OR REPLACE TRIGGER trbu_machine_hierarchy
 BEFORE
  UPDATE
 ON machine_hierarchy
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
     FROM DUAL;
END;
/


