-- schema app_user data
-- modified wei guo, duplicate insert to avoid unique constraint error.
INSERT INTO app_user.app_object_type (app_object_cd) VALUES ('LOCATION_ACCOUNTS');
INSERT INTO app_user.app_object_type (app_object_cd) VALUES ('LOCATION_ACCOUNTS');

--don't forget to customize app_user.domain_name! Example for dev: INSERT INTO app_user.domain_name (domain_name_url, domain_name_desc) VALUES ('esuds-dev.net', 'Development eSuds');

-- schema device data
INSERT INTO device.device_setting_parameter (device_setting_parameter_cd) VALUES ('Multiplexor Application Version');

-- schema pss data
--don't forget to customize pss.blackbrd_authority!
--don't forget to customize pss.blackbrd_tender!
INSERT INTO pss.internal_authority (internal_authority_id, internal_authority_name, authority_service_type_id) VALUES (2, 'USA Tech (auth only)', 3);
INSERT INTO pss.internal_payment_type (internal_payment_type_id, internal_payment_type_desc, internal_authority_id) VALUES (2, 'Special Card (auth only)', 2);

-- update legacy card parsing settings
UPDATE pos_pta
SET pos_pta_regex = '(.{1,9})(.*)',
	pos_pta_regex_bref = '1:1|2:7'
WHERE pos_id IN (
	SELECT p.pos_id
	FROM pos p, device d, device_setting ds
	WHERE d.device_id = p.device_id
	AND d.device_id = ds.device_id
	AND ds.device_setting_parameter_cd = 'Card Data Parser'
	AND ds.device_setting_value = 'first_9_characters_parser'
)
AND payment_subtype_id = 9;

UPDATE pos_pta
SET pos_pta_regex = NULL,
	pos_pta_regex_bref = NULL
WHERE pos_id IN (
	SELECT p.pos_id
	FROM pos p, device d, device_setting ds
	WHERE d.device_id = p.device_id
	AND d.device_id = ds.device_id
	AND ds.device_setting_parameter_cd = 'Card Data Parser'
	AND (
		ds.device_setting_value = 'none'
		OR ds.device_setting_value IS NULL
	)
)
AND payment_subtype_id = 9;

-- create settlement records for all batched transactions
BEGIN
	DECLARE
		n_tran_id pss.tran.tran_id%TYPE;
		CURSOR c_get_tran_ids IS
			SELECT t.tran_id
			FROM pss.tran t, pss.tran_settlement_batch tsb
			WHERE t.tran_id = tsb.tran_id(+)
			AND tsb.settlement_batch_id IS NULL
			AND t.tran_state_cd = '8';
	BEGIN
		OPEN c_get_tran_ids;
		LOOP
			FETCH c_get_tran_ids INTO n_tran_id;
			EXIT WHEN c_get_tran_ids%NOTFOUND;
			INSERT INTO pss.settlement_batch (
				SETTLEMENT_BATCH_ID,
				SETTLEMENT_BATCH_TS,
				SETTLEMENT_BATCH_STATE_ID
			) VALUES (
				seq_settlement_batch_id.NEXTVAL,
				( SELECT tran_upload_ts FROM tran WHERE tran_id = n_tran_id ),
				1
			);
			INSERT INTO pss.tran_settlement_batch (
				TRAN_ID,
				SETTLEMENT_BATCH_ID
			) VALUES (
				n_tran_id,
				seq_settlement_batch_id.CURRVAL
			);
		END LOOP;
		COMMIT;
		CLOSE c_get_tran_ids;
	END;
END;
/

-- default config for legacy compatibility
UPDATE pss.pos_pta 
SET payment_subtype_key_id = 1
WHERE payment_subtype_id = 9;
