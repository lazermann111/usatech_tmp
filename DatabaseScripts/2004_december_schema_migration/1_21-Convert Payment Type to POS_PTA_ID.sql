--DO NOT RUN....payment type id's have changed.  Figure out new payment types

-- have every pos accept cash & sp's

insert into pos_pta(pos_id, payment_subtype_id)
(select pos_id, 1 from pos);

insert into pos_pta(pos_id, payment_subtype_id)
(select pos_id, 3 from pos );

commit;

select  'update tran set pos_pta_id = ' || b.pos_pta_id  || ' where tran_id = ' || tran_id || ';' from tran a, pos_pta b where a.pos_id = b.pos_id and payment_type_id = 2 and authority_id =138 and payment_subtype_id = 1 order by tran_id;

select  'update tran set pos_pta_id = ' || b.pos_pta_id  || ' where tran_id = ' || tran_id || ';' from tran a, pos_pta b where a.pos_id = b.pos_id and payment_type_id = 4 and authority_id =138 and payment_subtype_id = 3 order by tran_id;

ALTER TABLE TRAN 
 MODIFY (
  POS_PTA_ID NOT NULL

 )
/

alter table consumer_acct drop column payment_type_id;

alter table tran drop column payment_type_id;

alter table tran drop column authority;

drop table pos_payment_type_authority;

drop table payment_type_authority;

drop table authority;

drop table payment_type cascade constraints;
