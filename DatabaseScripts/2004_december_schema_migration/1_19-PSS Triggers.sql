/* Formatted on 2004/09/09 11:47 (Formatter Plus v4.8.0) */
-- Start of DDL Script for Trigger PSS.TRBU_CONSUMER
-- Generated 9/9/2004 11:46:51 AM from PSS@usadbp01

CREATE TRIGGER trbu_tran_line_item
   BEFORE UPDATE
   ON tran_line_item
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- End of DDL Script for Trigger PSS.TRBU_tran_line_item

-- Start of DDL Script for Trigger PSS.TRBI_tran_line_item
-- Generated 9/9/2004 11:46:52 AM from PSS@usadbp01


CREATE or replace TRIGGER trbi_tran_line_item
   BEFORE INSERT
   ON tran_line_item
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   IF :NEW.tran_line_item_id IS NULL
   THEN
      SELECT SEQ_TLI_ID.NEXTVAL
        INTO :NEW.tran_line_item_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- End of DDL Script for Trigger PSS.TRBI_tran_line_item
CREATE SEQUENCE PSS.SEQ_TRAN_LINE_ITEM_TYPE_ID
 START WITH  82
 NOCACHE 
/



CREATE TRIGGER trbu_tran_line_item_type
   BEFORE UPDATE
   ON tran_line_item_type
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- End of DDL Script for Trigger PSS.TRBU_tran_line_item_type

-- Start of DDL Script for Trigger PSS.TRBI_tran_line_item_type
-- Generated 9/9/2004 11:46:52 AM from PSS@usadbp01


CREATE or replace TRIGGER trbi_tran_line_item_type
   BEFORE INSERT
   ON tran_line_item_type
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   IF :NEW.tran_line_item_type_id IS NULL
   THEN
      SELECT SEQ_tran_line_item_type_id.NEXTVAL
        INTO :NEW.tran_line_item_type_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/



CREATE TRIGGER trbu_tran
   BEFORE UPDATE
   ON tran
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- End of DDL Script for Trigger PSS.TRBU_tran

-- Start of DDL Script for Trigger PSS.TRBI_tran
-- Generated 9/9/2004 11:46:52 AM from PSS@usadbp01


CREATE or replace TRIGGER trbi_tran
   BEFORE INSERT
   ON tran
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
BEGIN
   IF :NEW.tran_id IS NULL
   THEN
      SELECT SEQ_tran_id.NEXTVAL
        INTO :NEW.tran_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/
