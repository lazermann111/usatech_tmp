-- remove authority_id from consumer_acct table
ALTER TABLE pss.consumer_acct DROP COLUMN authority_id;

-- more pos_pta changes
ALTER TABLE pss.pos_pta ADD (pos_pta_device_serial_cd VARCHAR2(60) NULL);
ALTER TABLE pss.pos_pta ADD (pos_pta_pin_req_yn_flag CHAR(1) DEFAULT 'N');
UPDATE pss.pos_pta SET pos_pta_pin_req_yn_flag = 'N';

-- drop incorrectly located table
drop table pss.device_contact;

-- blackbrd constraints
ALTER TABLE pss.blackbrd_authority MODIFY BLACKBRD_SERVER_NAME NOT NULL;
ALTER TABLE pss.blackbrd_tender MODIFY blackbrd_tender_num NOT NULL;

-- consumer_acct fkey
ALTER TABLE pss.consumer_acct ADD CONSTRAINT fk_consumer_account_payment_ty FOREIGN KEY (payment_subtype_id) REFERENCES pss.payment_subtype(payment_subtype_id);

-- update data in tran table
BEGIN
	DECLARE
		n_tran_id pss.tran.tran_id%TYPE;
		CURSOR c_get_tran_ids IS
			SELECT tran_id FROM pss.tran;
	BEGIN
		OPEN c_get_tran_ids;
		LOOP
			FETCH c_get_tran_ids into n_tran_id;
			EXIT WHEN c_get_tran_ids%notFound;
			UPDATE pss.tran
				SET tran_settlement_amount = (
					SELECT SUM (tran_line_item_amount * tran_line_item_quantity)
					FROM tran_line_item
					WHERE tran_id = n_tran_id
					AND tran_batch_type_cd = 'A'
			)
			WHERE tran_id = n_tran_id;
		END LOOP;
		COMMIT;
		CLOSE c_get_tran_ids;
	END;
END;
/

-- insert client_payment_type data
INSERT INTO client_payment_type (CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC) VALUES ('C', 'Credit Card');
INSERT INTO client_payment_type (CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC) VALUES ('S', 'Special Card');
INSERT INTO client_payment_type (CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC) VALUES ('R', 'RFID');
INSERT INTO client_payment_type (CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC) VALUES ('M', 'Cash');

-- update payment_subtype table with correct data
UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Credit Card (Track 2)', 
	payment_subtype_class = NULL,  
	payment_subtype_key_name = 'CC_TERMINAL_ID', 
	client_payment_type_cd = 'C',  
	payment_subtype_regex = '([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,4})', 
	payment_subtype_regex_bref = '1:1|2:3|3:5',  
	payment_subtype_table_name = 'CC_TERMINAL', 
	payment_subtype_key_desc_name = 'CC_TERMINAL_NAME',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 1;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Credit Card (Track 2, no Exp. Date)', 
	payment_subtype_class = NULL,  
	payment_subtype_key_name = 'CC_TERMINAL_ID', 
	client_payment_type_cd = 'C',  
	payment_subtype_regex = '([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,4})', 
	payment_subtype_regex_bref = '1:1|2:4|3:5',  
	payment_subtype_table_name = 'CC_TERMINAL', 
	payment_subtype_key_desc_name = 'CC_TERMINAL_NAME',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 2;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'RFID', 
	payment_subtype_class = NULL,  
	payment_subtype_key_name = 'Not Applicable', 
	client_payment_type_cd = 'R',  
	payment_subtype_regex = '(.+)', 
	payment_subtype_regex_bref = NULL,  
	payment_subtype_table_name = 'Not Applicable', 
	payment_subtype_key_desc_name = 'Not Applicable',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 3;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Cash', 
	payment_subtype_class = 'ReRix::POS::Handler::Cash',  
	payment_subtype_key_name = 'Not Applicable', 
	client_payment_type_cd = 'M',  
	payment_subtype_regex = '(.*)', 
	payment_subtype_regex_bref = NULL,  
	payment_subtype_table_name = 'Not Applicable', 
	payment_subtype_key_desc_name = 'Not Applicable',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 4;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Special Card - USA Tech Local Auth Passcard', 
	payment_subtype_class = 'ReRix::POS::Handler::Internal',  
	payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID', 
	client_payment_type_cd = 'S',  
	payment_subtype_regex = '([0-9]+)', 
	payment_subtype_regex_bref = '1:1',  
	payment_subtype_table_name = 'INTERNAL_PAYMENT_TYPE', 
	payment_subtype_key_desc_name = 'INTERNAL_PAYMENT_TYPE_DESC',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 5;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Special Card - USA Tech (Track 1)', 
	payment_subtype_class = 'ReRix::POS::Handler::Internal',  
	payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID', 
	client_payment_type_cd = 'S',  
	payment_subtype_regex = '(SEVEND1)([a-zA-Z]+)([0-9]+)\^(PRIMA\/)([a-zA-Z0-9]+)\^', 
	payment_subtype_regex_bref = '1:8|2:7|3:1|5:9',  
	payment_subtype_table_name = 'INTERNAL_PAYMENT_TYPE', 
	payment_subtype_key_desc_name = 'INTERNAL_PAYMENT_TYPE_DESC',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 6;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Special Card - USA Tech (Track 2)', 
	payment_subtype_class = 'ReRix::POS::Handler::Internal',  
	payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID', 
	client_payment_type_cd = 'S',  
	payment_subtype_regex = '(99[0-9]{4})([0-9]{10})=(0+)', 
	payment_subtype_regex_bref = '1:7|2:1|3:8',  
	payment_subtype_table_name = 'INTERNAL_PAYMENT_TYPE', 
	payment_subtype_key_desc_name = 'INTERNAL_PAYMENT_TYPE_DESC',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 7;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Special Card - eSuds Blackboard', 
	payment_subtype_class = 'ReRix::POS::Handler::BlackBoard',  
	payment_subtype_key_name = 'BLACKBRD_TENDER_ID', 
	client_payment_type_cd = 'S',  
	payment_subtype_regex = '(.+)', 
	payment_subtype_regex_bref = NULL,  
	payment_subtype_table_name = 'BLACKBRD_TENDER', 
	payment_subtype_key_desc_name = 'BLACKBRD_TENDER_NAME',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 8;

UPDATE pss.payment_subtype SET
	payment_subtype_name = 'Special Card - eSuds Generic', 
	payment_subtype_class = 'ReRix::POS::Handler::Internal',  
	payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID', 
	client_payment_type_cd = 'S',  
	payment_subtype_regex = '(.+)', 
	payment_subtype_regex_bref = NULL,  
	payment_subtype_table_name = 'INTERNAL_PAYMENT_TYPE', 
	payment_subtype_key_desc_name = 'INTERNAL_PAYMENT_TYPE_DESC',  
	payment_subtype_desc = NULL
WHERE payment_subtype_id = 9;

-- remove unused packages
DROP PACKAGE pss.pkg_process_transaction;
DROP PACKAGE app_user.pkg_app_user_maint;