-- public synonyms
CREATE PUBLIC SYNONYM domain_name FOR app_user.domain_name;
CREATE PUBLIC SYNONYM subdomain_name FOR app_user.subdomain_name;

-- location modification to support eSuds room images
ALTER TABLE location.location ADD (location_image_url VARCHAR2(512) NULL);

-- new tran_batch_type table integration
CREATE TABLE pss.tran_batch_type(
	tran_batch_type_cd CHAR(1),
	tran_batch_type_desc VARCHAR(255) NOT NULL,
	CONSTRAINT pk_tran_batch_type PRIMARY KEY (tran_batch_type_cd) VALIDATE
)
TABLESPACE pss_data;
INSERT INTO pss.tran_batch_type VALUES ('A', 'Actual Batch');
INSERT INTO pss.tran_batch_type VALUES ('I', 'Intended Batch');
ALTER TABLE tran_line_item ADD (tran_batch_type_cd CHAR(1) CONSTRAINT fk_tran_batch_type_cd REFERENCES pss.tran_batch_type(tran_batch_type_cd));
UPDATE pss.tran_line_item set tran_batch_type_cd = 'A';
ALTER TABLE tran_line_item MODIFY tran_batch_type_cd NOT NULL;

-- settlement_batch modifications
ALTER TABLE settlement_batch ADD (created_by VARCHAR2(30) NOT NULL);
ALTER TABLE settlement_batch ADD (created_ts DATE NOT NULL);
ALTER TABLE settlement_batch ADD (last_updated_by VARCHAR2(30) NOT NULL);
ALTER TABLE settlement_batch ADD (last_updated_ts DATE NOT NULL);

INSERT INTO settlement_batch_state VALUES (1, 'SERVER_SETTLEMENT_SUCCESS');
INSERT INTO settlement_batch_state VALUES (2, 'SERVER_SETTLEMENT_DECLINE');
INSERT INTO settlement_batch_state VALUES (3, 'SERVER_SETTLEMENT_FAILURE');
INSERT INTO settlement_batch_state VALUES (4, 'SERVER_WAITING_FOR_SETTLEMENT');
INSERT INTO settlement_batch_state VALUES (5, 'SERVER_SETTLEMENT_CANCELLED');
ALTER TABLE settlement_batch_state MODIFY settlement_batch_state_name VARCHAR2(60) NOT NULL;
ALTER TABLE settlement_batch DROP COLUMN settlement_batch_cd;

-- new tran_refund_state table integration
CREATE TABLE pss.tran_refund_state(
	tran_refund_state_id NUMBER(20,0),
	tran_refund_state_name VARCHAR2(60) NOT NULL,
	CONSTRAINT pk_tran_refund_state PRIMARY KEY (tran_refund_state_id) VALIDATE
)
TABLESPACE pss_data;
INSERT INTO tran_refund_state VALUES (1, 'SERVER_REFUND_SUCCESS');
INSERT INTO tran_refund_state VALUES (2, 'SERVER_REFUND_DECLINE');
INSERT INTO tran_refund_state VALUES (3, 'SERVER_REFUND_FAILURE');
INSERT INTO tran_refund_state VALUES (4, 'SERVER_WAITING_FOR_SETTLEMENT');
INSERT INTO tran_refund_state VALUES (5, 'SERVER_REFUND_CANCELLED');

-- tran_refund modifications
ALTER TABLE tran_refund ADD (tran_refund_state_id NUMBER(20,0));
ALTER TABLE tran_refund ADD (created_by VARCHAR2(30) NOT NULL);
ALTER TABLE tran_refund ADD (created_ts DATE NOT NULL);
ALTER TABLE tran_refund ADD (last_updated_by VARCHAR2(30) NOT NULL);
ALTER TABLE tran_refund ADD (last_updated_ts DATE NOT NULL);
ALTER TABLE tran_refund MODIFY tran_refund_id NUMBER(20,0);
ALTER TABLE tran_refund MODIFY tran_refund_amount NUMBER(20,2);
ALTER TABLE tran_refund MODIFY tran_refund_desc VARCHAR2(60);
ALTER TABLE tran_refund MODIFY tran_refund_ts DATE;
ALTER TABLE tran_refund MODIFY tran_refund_issed_by VARCHAR2(60);
ALTER TABLE tran_refund MODIFY tran_refund_settlement_ts DATE;
ALTER TABLE tran_refund ADD CONSTRAINT fk_tran_refund_state_id FOREIGN KEY (tran_refund_state_id) REFERENCES pss.tran_refund_state(tran_refund_state_id);

-- tran_refund_type default data
INSERT INTO tran_refund_type VALUES ('G', 'Generic Refund');

-- sequences and triggers for new PSS schema tables
CREATE SEQUENCE pss.seq_settlement_batch_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

CREATE OR REPLACE TRIGGER pss.trbu_settlement_batch
 BEFORE
  UPDATE
 ON settlement_batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER pss.trbi_settlement_batch
 BEFORE
  INSERT
 ON settlement_batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF :NEW.settlement_batch_id IS NULL
   THEN
      SELECT seq_settlement_batch_id.NEXTVAL
        INTO :NEW.settlement_batch_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE SEQUENCE pss.seq_tran_refund_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

CREATE OR REPLACE TRIGGER pss.trbu_tran_refund
 BEFORE
  UPDATE
 ON tran_refund
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER pss.trbi_tran_refund
 BEFORE
  INSERT
 ON tran_refund
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF :NEW.tran_refund_id IS NULL
   THEN
      SELECT seq_tran_refund_id.NEXTVAL
        INTO :NEW.tran_refund_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- tran_state data modifications
DELETE FROM tran_state WHERE tran_state_cd IN ('3', '4', '5', '9', 'S');
INSERT INTO tran_state VALUES ('1', 'SERVER_WAITING_FOR_BATCH_LOCAL');
INSERT INTO tran_state VALUES ('3', 'SERVER_WAITING_FOR_BATCH_INTEND');
INSERT INTO tran_state VALUES ('4', 'SERVER_WAITING_FOR_BATCH');
INSERT INTO tran_state VALUES ('5', 'SERVER_WAITING_FOR_BATCH_INTEND');
INSERT INTO tran_state VALUES ('9', 'PROCESSED_SERVER_BATCH_INTENDED');
INSERT INTO tran_state VALUES ('0', 'PROCESSED_SERVER_AUTH_SUCCESS_CONDITIONAL');
INSERT INTO tran_state VALUES ('Q', 'PROCESSED_CLIENT_PROCESSED_VEND');

-- authority_service_type new table
CREATE TABLE pss.authority_service_type(
	authority_service_type_id NUMBER(20, 0),
	authority_service_type_name VARCHAR(60) NOT NULL,
	authority_service_type_desc VARCHAR(255),
	CONSTRAINT pk_authority_service_type PRIMARY KEY (authority_service_type_id) VALIDATE
)
TABLESPACE pss_data;
CREATE PUBLIC SYNONYM authority_service_type FOR pss.authority_service_type;
GRANT SELECT ON pss.authority_service_type TO web_user WITH GRANT OPTION;
INSERT INTO authority_service_type VALUES (1, 'Balance-based Account', 'Account with monetary balance that can be authed against, debited from, or refunded to');
INSERT INTO authority_service_type VALUES (2, 'Privileges Account', 'Account that can only be authed against; has no monetary or other type balance (to determine if privilege has been granted)');
INSERT INTO authority_service_type VALUES (3, 'Pseudo-Privileges Account', 'Account with monetary balance that can only be authed against');

-- internal_authority modifications
DELETE FROM internal_authority;
ALTER TABLE pss.internal_authority DROP COLUMN internal_payment_type_id;
ALTER TABLE pss.internal_authority ADD (
    authority_service_type_id NUMBER(20,0) NOT NULL 
    CONSTRAINT fk_ast_id_internal REFERENCES pss.authority_service_type(authority_service_type_id)
);
INSERT INTO internal_authority (internal_authority_id, internal_authority_name, authority_service_type_id) VALUES (1, 'USA Technologies, Inc.', 1);

-- internal_payment_type modifications
DELETE FROM internal_payment_type;
ALTER TABLE pss.internal_payment_type ADD (internal_authority_id NUMBER(20,0) NOT NULL CONSTRAINT fk_internal_authority_id REFERENCES pss.internal_authority(internal_authority_id)); 
INSERT INTO internal_payment_type (internal_payment_type_id, internal_payment_type_desc, internal_authority_id) VALUES (1, 'Special Card', 1);

-- blackbrd_authority modifications
DELETE FROM blackbrd_tender;
ALTER TABLE pss.blackbrd_tender MODIFY blackbrd_tender_num NUMBER(9,0) NULL;
ALTER TABLE pss.blackbrd_tender MODIFY blackbrd_tender_id NUMBER(20,0);

DELETE FROM blackbrd_authority;
ALTER TABLE pss.blackbrd_authority MODIFY BLACKBRD_SERVER_NAME VARCHAR2(255) NULL;
ALTER TABLE pss.blackbrd_authority ADD (
    blackbrd_gateway_port_num NUMBER(5,0) NULL 
);
ALTER TABLE pss.blackbrd_authority ADD (
    blackbrd_gateway_addr VARCHAR2(30) NULL 
);
ALTER TABLE pss.blackbrd_authority ADD (
    authority_service_type_id NUMBER(20,0) NOT NULL 
    CONSTRAINT fk_ast_id_blackbrd REFERENCES pss.authority_service_type(authority_service_type_id)
);
INSERT INTO blackbrd_authority (blackbrd_authority_id, blackbrd_authority_name, blackbrd_server_name, blackbrd_gateway_port_num, blackbrd_gateway_addr, authority_service_type_id) 
	VALUES (1, 'Blackboard Test Server', 'Blackboard Test Server', 9003, '10.0.0.217', 1);
INSERT INTO blackbrd_authority (blackbrd_authority_id, blackbrd_authority_name, blackbrd_server_name, blackbrd_gateway_port_num, blackbrd_gateway_addr, authority_service_type_id) 
	VALUES (2, 'Blackboard Test Server (auth only)', 'Blackboard Test Server', 9003, '10.0.0.217', 3);

INSERT INTO blackbrd_tender (blackbrd_tender_id, blackbrd_tender_num, blackbrd_authority_id, blackbrd_tender_name, blackbrd_tender_desc) 
	VALUES (1, 910, 1, 'Default', 'Default Tender');
INSERT INTO blackbrd_tender (blackbrd_tender_id, blackbrd_tender_num, blackbrd_authority_id, blackbrd_tender_name, blackbrd_tender_desc) 
	VALUES (2, 910, 2, 'Type 3 Authority', 'Default Tender (type 3 auth)');

-- tran line item positive/negative sign related
ALTER TABLE pss.tran_line_item_type ADD (tran_line_item_type_sign_pn CHAR(1) DEFAULT 'P' NOT NULL);
UPDATE pss.tran_line_item_type SET tran_line_item_type_sign_pn = 'P';
UPDATE pss.tran_line_item_type SET tran_line_item_type_sign_pn = 'N' WHERE tran_line_item_type_id = 81;
UPDATE pss.tran_line_item 
SET tran_line_item_amount = -1 * tran_line_item_amount,
    tran_line_item_tax = -1 * tran_line_item_tax
WHERE tran_line_item_type_id = 81;

-- regex backref related
CREATE TABLE pss.payment_subtype_regex_bref(
	payment_subtype_regex_bref_id NUMBER(20,0),
	regex_bref_name VARCHAR2(60) NOT NULL,
	regex_bref_desc VARCHAR2(255),
	CONSTRAINT pk_payment_subtype_regex_bref PRIMARY KEY (payment_subtype_regex_bref_id) VALIDATE
)
TABLESPACE pss_data;
CREATE PUBLIC SYNONYM payment_subtype_regex_bref FOR pss.payment_subtype_regex_bref;

ALTER TABLE pss.payment_subtype ADD (payment_subtype_regex_bref VARCHAR(60) NULL);

-- addition of backref fields, authority_serial_cd, default for PIN to pos_pta
-- pos_pta_regex_bref format is as follows:
--  Given the Special Card - USA Tech (track 1) format of:
--  (SEVEND1)([a-zA-Z0-9]+)\^(PRIMA\/)([a-zA-Z0-9]+)\^
--
--  we have:
--  \2 = account number
--  \4 = card holder name
--
--  and then standardize the backref format something like the following:
--  [regex_backref_type_id 1]:[backref num 1]|[regex_backref_type_id 2]:[backref num 2]|...
--  
--  So, the format would then be:
--  1:2|2:4
--
ALTER TABLE pss.pos_pta ADD (pos_pta_regex VARCHAR(255) NULL);
ALTER TABLE pss.pos_pta ADD (pos_pta_regex_bref VARCHAR(60) NULL);

-- changes to pkg_device_maint.sp_assign_new_device_id to handle new fields
CREATE OR REPLACE 
PACKAGE device.pkg_device_maint
IS
   /*******************************************************************************
  Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
  Description: Descriptions of the function
  Parameters:

              Function Return Value
                 Indicates if the exception was recorded successfully.  A value of
                 0 indicates a successful execution.  A value of 1 indicates an
                 unsuccessful execution.  A Boolean value was not used because it
                 may be misinterpreted by non-Oracle systems.
  MODIFICATION HISTORY:
  Person     Date          Comments
  ---------  ----------    -------------------------------------------
  MDH       24-NOV-2001    Intial Creation
 *******************************************************************************/
PROCEDURE SP_ASSIGN_NEW_DEVICE_ID
(
    pn_old_device_id  IN       device.device_id%TYPE,
    pn_new_device_id  OUT      device.device_id%TYPE,
    pn_return_code    OUT      exception_code.exception_code_id%TYPE,
    pv_error_message            OUT        exception_data.additional_information%TYPE
);

END;
/

-- Grants for Package
GRANT EXECUTE ON device.pkg_device_maint TO web_user
WITH GRANT OPTION
/

CREATE OR REPLACE 
PACKAGE BODY device.pkg_device_maint IS
   e_device_id_notfound       EXCEPTION;
   cv_server_name    CONSTANT VARCHAR2 (30) := 'USADBD02';
   cv_package_name   CONSTANT VARCHAR2 (30) := 'PKG_DEVICE_MAINT';

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    MDH       24-NOV-2001    Intial Creation
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
   *******************************************************************************/
   PROCEDURE sp_assign_new_device_id (
      pn_old_device_id   IN       device.device_id%TYPE,
      pn_new_device_id   OUT      device.device_id%TYPE,
      pn_return_code     OUT      exception_code.exception_code_id%TYPE,
      pv_error_message   OUT      exception_data.additional_information%TYPE
   ) AS
      v_error_msg                  exception_data.additional_information%TYPE;
      n_old_device_id              device.device_id%TYPE;
      n_new_device_id              device.device_id%TYPE;
      n_old_pos_id                 pos.pos_id%TYPE;
      n_new_pos_id                 pos.pos_id%TYPE;
      b_device_id_found_flag       BOOLEAN;
      e_device_id_notfound         EXCEPTION;
      cv_procedure_name   CONSTANT VARCHAR2 (50)             := 'SP_ASSIGN_NEW_DEVICE_ID';
      cv_server_name      CONSTANT VARCHAR2 (50)                            := 'USADBD02';
      cv_package_name     CONSTANT VARCHAR2 (50)                    := 'PKG_DEVICE_MAINT';
   BEGIN
      --Initialise all variables
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- Verify the N_EXCEPTION_CODE_ID parameter contains a value stored in the EXCEPTION_CODE table.
      UPDATE device
         SET device_active_yn_flag = 'N'
       WHERE device_id = pn_old_device_id;

      -- Check if error information exists for the exception
      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_device_id_notfound;
      END IF;

      SELECT seq_device_id.NEXTVAL
        INTO n_new_device_id
        FROM DUAL;

      INSERT INTO device
                  (device_id,
                   device_name,
                   device_serial_cd,
                   device_type_id,
                   encryption_key,
                   device_active_yn_flag,
                   device_client_serial_cd
                  )
         SELECT n_new_device_id,
                device_name,
                device_serial_cd,
                device_type_id,
                encryption_key,
                'Y',
                device_client_serial_cd
           FROM device
          WHERE device_id = pn_old_device_id;

      -- Check if exception information exists for the UNKNOWN_ERROR_ID exception code id
      IF SQL%NOTFOUND = TRUE THEN
         b_device_id_found_flag := FALSE;
      END IF;

      pn_new_device_id := n_new_device_id;

      -- copy and create new POS and POS_Payment_Type_Authority records
      SELECT pos.pos_id
        INTO n_old_pos_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      SELECT pss.seq_pos_id.NEXTVAL
        INTO n_new_pos_id
        FROM DUAL;

      INSERT INTO pos
                  (pos_id,
                   location_id,
                   customer_id,
                   device_id
                  )
         SELECT n_new_pos_id,
                location_id,
                customer_id,
                n_new_device_id
           FROM pos
          WHERE pos.pos_id = n_old_pos_id;

      INSERT INTO pos_pta
                  (pos_id,
                   payment_subtype_id,
                   pos_pta_encrypt_key,
                   pos_pta_activation_ts,
                   pos_pta_deactivation_ts,
                   payment_subtype_key_id,
                   pos_pta_pin_req_yn_flag,
                   pos_pta_regex,
                   pos_pta_regex_bref
                  )
         SELECT n_new_pos_id,
                payment_subtype_id,
                pos_pta_encrypt_key,
                pos_pta_activation_ts,
                pos_pta_deactivation_ts,
                payment_subtype_key_id,
                pos_pta_pin_req_yn_flag,
                pos_pta_regex,
                pos_pta_regex_bref
           FROM pos_pta
          WHERE pos_id = n_old_pos_id;

 /*     INSERT INTO pos_payment_type_authority
                  (pos_id,
                   authority_id,
                   payment_type_id
                  )
         SELECT n_new_pos_id,
                authority_id,
                payment_type_id
           FROM pos_payment_type_authority
          WHERE pos_id = n_old_pos_id; */

      -- change any waiting outgoing file transfers over to this new device
      UPDATE device_file_transfer
         SET device_id = n_new_device_id
       WHERE device_id = pn_old_device_id
         AND device_file_transfer_direct = 'O'
         AND device_file_transfer_status_cd = 0;

      -- copy any existing device settings to the new device
      INSERT INTO device_setting
                  (device_id,
                   device_setting_parameter_cd,
                   device_setting_value
                  )
         SELECT n_new_device_id,
                device_setting_parameter_cd,
                device_setting_value
           FROM device_setting
          WHERE device_setting.device_id = pn_old_device_id;
   EXCEPTION
      WHEN e_device_id_notfound THEN
         pv_error_message :=
               'The device_id = '
            || pn_old_device_id
            || ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             v_error_msg,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   --pn_return_code := 0;

   --    WHEN OTHERS THEN
--
--       pv_error_message := 'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
--       pkg_exception_processor.sp_log_exception(PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id, v_error_msg, cv_server_name, cv_package_name || '.' || cv_procedure_name);
--       pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
--       --pn_return_code := 0;
   END;
END;
/

-- payment_subtype modifications
UPDATE payment_subtype SET payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID' WHERE payment_subtype_key_name = 'INTERNAL_AUTHORITY_ID';

ALTER TABLE pss.payment_subtype ADD (payment_subtype_table_name VARCHAR(30) NULL);
UPDATE payment_subtype SET payment_subtype_table_name = 'Not Applicable';
UPDATE payment_subtype SET payment_subtype_table_name = 'BLACKBRD_TENDER' WHERE payment_subtype_key_name = 'BLACKBRD_TENDER_ID';
UPDATE payment_subtype SET payment_subtype_table_name = 'INTERNAL_PAYMENT_TYPE' WHERE payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID';
UPDATE payment_subtype SET payment_subtype_table_name = 'CC_TERMINAL' WHERE payment_subtype_key_name = 'CC_TERMINAL_ID';
ALTER TABLE payment_subtype MODIFY payment_subtype_table_name NOT NULL;

ALTER TABLE pss.payment_subtype ADD (payment_subtype_key_desc_name VARCHAR(30) NULL);
UPDATE payment_subtype SET payment_subtype_key_desc_name = 'Not Applicable';
UPDATE payment_subtype SET payment_subtype_key_desc_name = 'BLACKBRD_TENDER_NAME' WHERE payment_subtype_key_name = 'BLACKBRD_TENDER_ID';
UPDATE payment_subtype SET payment_subtype_key_desc_name = 'INTERNAL_PAYMENT_TYPE_DESC' WHERE payment_subtype_key_name = 'INTERNAL_PAYMENT_TYPE_ID';
UPDATE payment_subtype SET payment_subtype_key_desc_name = 'CC_TERMINAL_NAME' WHERE payment_subtype_key_name = 'CC_TERMINAL_ID';
ALTER TABLE payment_subtype MODIFY payment_subtype_key_desc_name NOT NULL;

-- cc_terminal modifications
ALTER TABLE pss.cc_terminal ADD (cc_terminal_name VARCHAR(60) NOT NULL);

-- new payment_subtype_regex_bref data
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (1, 'Primary Account Number');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (2, 'Name (Card Holder)');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (3, 'Expiration Date (MMYY or MMYYYY)');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (4, 'Additional Data (service code, etc.)');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (5, 'Discretionary Data (PVKI, PVV, Offset, CVV, CVC, etc.)');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (6, 'Use and Security Data (Track 3)');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (7, 'Custom Data 1');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (8, 'Custom Data 2');
INSERT INTO payment_subtype_regex_bref (payment_subtype_regex_bref_id, regex_bref_name) VALUES (9, 'Custom Data 3');

-- more public synonyms for pss schema
CREATE PUBLIC SYNONYM seq_tran_id FOR pss.seq_tran_id;
CREATE PUBLIC SYNONYM seq_tli_id FOR pss.seq_tli_id;
CREATE PUBLIC SYNONYM seq_settlement_batch_id FOR pss.seq_settlement_batch_id;
CREATE PUBLIC SYNONYM settlement_batch FOR pss.settlement_batch;
CREATE PUBLIC SYNONYM tran_settlement_batch FOR pss.tran_settlement_batch;
CREATE PUBLIC SYNONYM sp_nxt_blackbrd_sequence_num FOR pss.sp_nxt_blackbrd_sequence_num;

-- location.customer_type miscellaneous items (triggers, sequence, synonym)
CREATE SEQUENCE location.seq_customer_type_id
  INCREMENT BY 1
  START WITH 4
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/
CREATE PUBLIC SYNONYM seq_customer_type_id FOR location.seq_customer_type_id;

CREATE OR REPLACE TRIGGER location.trbu_customer_type
 BEFORE
  UPDATE
 ON customer_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER location.trbi_customer_type
 BEFORE
  INSERT
 ON customer_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF :NEW.customer_type_id IS NULL
   THEN
      SELECT seq_customer_type_id.NEXTVAL
        INTO :NEW.customer_type_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

-- enable activation/deactivation functionality
ALTER TABLE pos_pta ADD (pos_pta_activation_ts DATE NULL);
UPDATE pos_pta SET pos_pta_activation_ts = POS_PTA_ACTIVITATION_TS;
UPDATE pos_pta SET POS_PTA_ACTIVITATION_TS = NULL;
ALTER TABLE pos_pta DROP COLUMN POS_PTA_ACTIVITATION_TS;
UPDATE pos_pta SET pos_pta_activation_ts = SYSDATE WHERE pos_pta_activation_ts IS NULL;

-- update for pkg_consumer_maint
-- Start of DDL Script for Package PSS.PKG_CONSUMER_MAINT
-- Generated 19-Nov-2004 11:11:29 from PSS@USADBD03.USATECH.COM

-- Drop the old instance of PKG_CONSUMER_MAINT
DROP PACKAGE pss.pkg_consumer_maint
/

CREATE OR REPLACE 
PACKAGE pss.pkg_consumer_maint IS
   PROCEDURE sp_add_consumer (
      pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
      pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
      pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
      pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
      pv_consumer_city                 IN       consumer.consumer_city%TYPE,
      pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
      pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
      pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
      pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
      pn_location_id                   IN       consumer.location_id%TYPE,
      pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
      pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
      pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
      pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
      pv_error_message                 OUT      exception_data.additional_information%TYPE
   );

   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   );
END pkg_consumer_maint;
/


CREATE OR REPLACE 
PACKAGE BODY     pss.pkg_consumer_maint IS
/* The following rules were applied to this procedure

1.       The only unique identifier we have is the student?s email address.
         We determine if a student already exists in our database by comparing the
         email address provided in the data file with the records in our database.

2.       If a student?s email address changes then a new student record will be
         created.  Transactions from the original student record will not be
         associated with the new student record.

3.       All fields, except the password and email address, will be updated when
         new information is sent about an existing student.   The only exception
         to this rule is:

        "When a student?s id number changes, and the password was never changed
        by the user, the password will be set to the last four digits of the new
        student id."

4.      Student accounts in the database will be deactivated if they are not
        present when loading the data file.

5.      When we receive data about a student that has been deactivated we will
        reactivate the account.  A new account will not be created and any
        historical data will be re-associated with the student.

*/
   cv_package_name   CONSTANT VARCHAR2 (30) := 'pkg_consumer_maint';

   PROCEDURE sp_add_consumer (
      pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
      pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
      pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
      pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
      pv_consumer_city                 IN       consumer.consumer_city%TYPE,
      pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
      pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
      pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
      pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
      pn_location_id                   IN       consumer.location_id%TYPE,
      pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
      pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
      pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
      pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
      pv_error_message                 OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)                     := 'sp_add_consumer';
      n_return_cd                  NUMBER;
      n_consumer_count             NUMBER;
      n_app_user_count             NUMBER;
      n_consumer_id                consumer.consumer_id%TYPE;
      n_app_user_id                app_user.app_user_id%TYPE;
      v_app_user_pw                app_user.app_user_password%TYPE;
      v_old_consumer_acct_cd       consumer_acct.consumer_acct_cd%TYPE;
      v_new_consumer_acct_cd       consumer_acct.consumer_acct_cd%TYPE;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      IF pv_consumer_email_addr1 IS NULL THEN
        RAISE_APPLICATION_ERROR(-20171, 'An email address must be provided');
      END IF;
      SELECT COUNT (*)
        INTO n_consumer_count
        FROM consumer
       WHERE UPPER(consumer_email_addr1) = UPPER(pv_consumer_email_addr1);

      IF n_consumer_count = 0 THEN
         SELECT seq_consumer_id.NEXTVAL
           INTO n_consumer_id
           FROM DUAL;

         INSERT INTO consumer
                     (consumer_id,
                      consumer_fname,
                      consumer_lname,
                      consumer_addr1,
                      consumer_addr2,
                      consumer_city,
                      consumer_state_cd,
                      consumer_postal_cd,
                      consumer_email_addr1,
                      consumer_type_id,
                      location_id
                     )
              VALUES (n_consumer_id,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_addr1,
                      pv_consumer_addr2,
                      pv_consumer_city,
                      pv_consumer_state,
                      pv_consumer_postal_cd,
                      pv_consumer_email_addr1,
                      pn_consumer_type_id,
                      pn_location_id
                     );

         INSERT INTO consumer_acct
                     (consumer_acct_cd,
                      consumer_acct_active_yn_flag,
                      consumer_acct_balance,
                      consumer_id,
                      payment_subtype_id
                     )
              VALUES (pv_consumer_account_cd,
                      pv_consumer_accnt_actv_yn_flag,
                      pv_consumer_account_balance,
                      n_consumer_id,
                      1
                     );
      ELSE
         SELECT consumer_id
           INTO n_consumer_id
           FROM consumer
          WHERE UPPER(consumer_email_addr1) = UPPER(pv_consumer_email_addr1);

         UPDATE consumer
            SET consumer_fname = pv_consumer_fname,
                consumer_lname = pv_consumer_lname,
                consumer_addr1 = pv_consumer_addr1,
                consumer_addr2 = pv_consumer_addr2,
                consumer_city = pv_consumer_city,
                consumer_state_cd = pv_consumer_state,
                consumer_postal_cd = pv_consumer_postal_cd,
                consumer_type_id = pn_consumer_type_id,
                location_id = pn_location_id
          WHERE consumer_id = n_consumer_id;

         UPDATE consumer_acct
            SET consumer_acct_active_yn_flag = pv_consumer_accnt_actv_yn_flag,
                consumer_acct_balance = pv_consumer_account_balance,
                consumer_acct_cd = pv_consumer_account_cd
          WHERE consumer_id = n_consumer_id;
      END IF;

      SELECT COUNT (*)
        INTO n_app_user_count
        FROM app_user
       WHERE UPPER(app_user_email_addr) = UPPER(pv_consumer_email_addr1);

      IF n_app_user_count = 0 THEN
         SELECT seq_app_user_id.NEXTVAL
           INTO n_app_user_id
           FROM DUAL;

         INSERT INTO app_user
                     (app_user_id,
                      app_user_password,
                      app_user_active_yn_flag,
                      force_pw_change_yn_flag,
                      app_user_name,
                      app_user_fname,
                      app_user_lname,
                      app_user_email_addr
                     )
              VALUES (n_app_user_id,
                      SUBSTR (pv_consumer_account_cd,
                              LENGTH (pv_consumer_account_cd) - 4,
                              4
                             ),
                      pv_consumer_accnt_actv_yn_flag,
                      'N',
                      pv_consumer_email_addr1,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_email_addr1
                     );

         INSERT INTO app_user_object_permission
                     (app_user_id,
                      app_id,
                      app_object_type_id,
                      allow_object_create_yn_flag,
                      allow_object_read_yn_flag,
                      allow_object_modify_yn_flag,
                      allow_object_delete_yn_flag,
                      object_cd
                     )
              VALUES (n_app_user_id,
                      1,
                      1,
                      'N',
                      'Y',
                      'Y',
                      'N',
                      n_consumer_id
                     );
      ELSE
         SELECT app_user_password,
                c.consumer_acct_cd
           INTO v_app_user_pw,
                v_old_consumer_acct_cd
           FROM app_user a,
                consumer b,
                consumer_acct c
          WHERE UPPER (app_user_email_addr) = UPPER (pv_consumer_email_addr1)
            AND UPPER (a.app_user_email_addr) = UPPER (b.consumer_email_addr1)
            AND b.consumer_id = c.consumer_id;

         IF SUBSTR (pv_consumer_account_cd,
                    LENGTH (v_old_consumer_acct_cd) - 3,
                    4
                   ) = v_app_user_pw THEN
            v_new_consumer_acct_cd :=
                   SUBSTR (pv_consumer_account_cd,
                           LENGTH (pv_consumer_account_cd) - 3,
                           4
                          );
         ELSE
            v_new_consumer_acct_cd := v_app_user_pw;
         END IF;

         UPDATE app_user
            SET app_user_password = v_new_consumer_acct_cd,
                app_user_active_yn_flag = pv_consumer_accnt_actv_yn_flag,
                force_pw_change_yn_flag = 'N',
                app_user_name = pv_consumer_email_addr1,
                app_user_fname = pv_consumer_fname,
                app_user_lname = pv_consumer_lname
          WHERE UPPER (app_user_email_addr) = UPPER (pv_consumer_email_addr1);
      END IF;
   /* changed this to report exceptions (BSK 11-10-04)
   EXCEPTION
      WHEN OTHERS THEN
         -- changed this slightly to set pv_error_message to only SQLERRM and
         -- pn_return_code to SQLCODE for web file upload functionality (BSK 11-05-04)
         pv_error_message := SQLERRM;
         pn_return_code := SQLCODE;
         pkg_exception_processor.sp_log_exception
                                               (pkg_app_exec_hist_globals.unknown_error_id,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
   --*/
   END;

   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)             := 'sp_add_consumer_account';
      n_return_cd                  NUMBER;
      n_consumer_account_count     NUMBER;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      UPDATE consumer_acct ca
         SET ca.consumer_acct_active_yn_flag = 'N'
       WHERE ca.consumer_id IN (
                       SELECT consumer_id
                         FROM consumer
                        WHERE UPPER (consumer_email_addr1) =
                                                           UPPER (pv_consumer_email_addr1) );

      UPDATE app_user ap
         SET ap.app_user_active_yn_flag = 'N'
       WHERE UPPER (ap.app_user_email_addr) = UPPER (pv_consumer_email_addr1);
   EXCEPTION
      WHEN OTHERS THEN
         pv_error_message :=
                         'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
         pkg_exception_processor.sp_log_exception
                                               (pn_return_code,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   END;
END;
/


-- End of DDL Script for Package PSS.PKG_CONSUMER_MAINT

-- update for vw_total_sales_by_host
-- Start of DDL Script for View PSS.VW_TOTAL_SALES_BY_HOST
-- Generated 19-Nov-2004 11:12:41 from PSS@USADBD03.USATECH.COM

-- Drop the old instance of VW_TOTAL_SALES_BY_HOST
DROP VIEW pss.vw_total_sales_by_host
/

CREATE OR REPLACE VIEW pss.vw_total_sales_by_host (
   laundry_room_id,
   laundry_room_name,
   dorm_id,
   dorm_name,
   campus_id,
   campus_name,
   school_id,
   school_name,
   customer_id,
   device_id,
   device_name,
   host_id,
   host_label_cd,
   host_type_id,
   host_type_desc,
   payment_subtype_id,
   payment_subtype_desc,
   client_payment_type_cd,
   client_payment_type_desc,
   tran_ts,
   tot_items,
   tot_amount,
   tot_cycles,
   tot_cycle_amount,
   tot_top_offs,
   tot_top_off_amount,
   tot_misc_items,
   tot_misc_item_amount,
   tot_reg_washes,
   tot_reg_wash_amount,
   tot_reg_cold_washes,
   tot_reg_cold_wash_amount,
   tot_warm_washes,
   tot_warm_wash_amount,
   tot_reg_hot_washes,
   tot_reg_hot_wash_amount,
   tot_spec_washes,
   tot_spec_wash_amount,
   tot_spec_cold_washes,
   tot_spec_cold_wash_amount,
   spec_warm_washes,
   spec_warm_wash_amount,
   spec_hot_washes,
   spec_hot_wash_amount,
   super_washes,
   super_wash_amount,
   reg_dries,
   reg_dry_amount,
   spec_dries,
   spec_dry_amount,
   top_off_dries,
   top_off_dry_amount,
   tot_unused_purch_adjs,
   tot_unused_purch_adj_amount,
   tot_leftover_credit_adjs,
   tot_leftover_credit_adj_amount )
AS
SELECT   loc4.location_id,
            loc4.location_name,
            loc3.location_id,
            loc3.location_name,
            loc2.location_id,
            loc2.location_name,
            loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            dev.device_id,
            dev.device_name,
            hs.host_id,
            hs.host_label_cd,
            ht.host_type_id,
            ht.host_type_desc,
            pst.payment_subtype_id,
            pst.payment_subtype_name, -- added by BSK 11-05-04 because web app object is mapped to it
            pst.client_payment_type_cd,
            cpt.client_payment_type_desc,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            -- total tran line items per host
            SUM (tran_line_item_quantity) tot_items,
            SUM (NVL (tli.tran_line_item_quantity, 0) * NVL (tli.tran_line_item_amount, 0))
                                                                               tot_amount,
            -- total cycles
            SUM (DECODE (tran_line_item_type_id,
                         1, tran_line_item_quantity,
                         2, tran_line_item_quantity,
                         3, tran_line_item_quantity,
                         4, tran_line_item_quantity,
                         5, tran_line_item_quantity,
                         6, tran_line_item_quantity,
                         7, tran_line_item_quantity,
                         8, tran_line_item_quantity,
                         40, tran_line_item_quantity,
                         41, tran_line_item_quantity,
                         0
                        )
                ) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         1, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         2, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         3, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         4, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         5, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         6, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         7, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         8, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         40, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         41, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_cycle_amount,
            -- total top offs
            SUM (DECODE (tran_line_item_type_id,
                         9, tran_line_item_quantity,
                         42, tran_line_item_quantity,
                         0
                        )
                ) tot_top_offs,
            SUM (DECODE (tran_line_item_type_id,
                         9, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         42, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_top_off_amount,
            -- total misc items
            SUM (DECODE (tran_line_item_type_id, 0, tran_line_item_quantity, 0))
                                                                           tot_misc_items,
            SUM (DECODE (tran_line_item_type_id,
                         0, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_misc_item_amount,
            -- totals by item type
            SUM (DECODE (tran_line_item_type_id, 1, tran_line_item_quantity, 0))
                                                                           tot_reg_washes,
            SUM (DECODE (tran_line_item_type_id,
                         1, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 2, tran_line_item_quantity, 0))
                                                                      tot_reg_cold_washes,
            SUM (DECODE (tran_line_item_type_id,
                         2, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_cold_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 3, tran_line_item_quantity, 0))
                                                                          tot_warm_washes,
            SUM (DECODE (tran_line_item_type_id,
                         3, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_warm_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 4, tran_line_item_quantity, 0))
                                                                       tot_reg_hot_washes,
            SUM (DECODE (tran_line_item_type_id,
                         4, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_hot_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 5, tran_line_item_quantity, 0))
                                                                          tot_spec_washes,
            SUM (DECODE (tran_line_item_type_id,
                         5, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_spec_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 6, tran_line_item_quantity, 0))
                                                                     tot_spec_cold_washes,
            SUM (DECODE (tran_line_item_type_id,
                         6, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_spec_cold_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 7, tran_line_item_quantity, 0))
                                                                         spec_warm_washes,
            SUM (DECODE (tran_line_item_type_id,
                         7, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_warm_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 8, tran_line_item_quantity, 0))
                                                                          spec_hot_washes,
            SUM (DECODE (tran_line_item_type_id,
                         8, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_hot_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 9, tran_line_item_quantity, 0))
                                                                             super_washes,
            SUM (DECODE (tran_line_item_type_id,
                         9, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) super_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 40, tran_line_item_quantity, 0))
                                                                                reg_dries,
            SUM (DECODE (tran_line_item_type_id,
                         40, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) reg_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 41, tran_line_item_quantity, 0))
                                                                               spec_dries,
            SUM (DECODE (tran_line_item_type_id,
                         41, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 42, tran_line_item_quantity, 0))
                                                                            top_off_dries,
            SUM (DECODE (tran_line_item_type_id,
                         42, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) top_off_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 80, tran_line_item_quantity, 0))
                                                                    tot_unused_purch_adjs,
            SUM (DECODE (tran_line_item_type_id,
                         80, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_unused_purch_adj_amount,
            SUM (DECODE (tran_line_item_type_id, 81, tran_line_item_quantity, 0))
                                                                 tot_leftover_credit_adjs,
            SUM (DECODE (tran_line_item_type_id,
                         81, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_leftover_credit_adj_amount
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc4,
            LOCATION loc3,
            LOCATION loc2,
            LOCATION loc1,
            device dev,
            HOST hs,
            host_type ht,
            payment_subtype pst,
            client_payment_type cpt
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND cpt.client_payment_type_cd = pst.client_payment_type_cd
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        AND ps.device_id = dev.device_id
        AND dev.device_id = hs.device_id
        AND hs.host_type_id = ht.host_type_id
        AND hs.host_id = tli.host_id
        and tli.tran_line_item_type_id not in (80, 81)
        and tli.tran_batch_type_cd = 'A'
        AND pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc4.location_id,
            loc4.location_name,
            loc3.location_id,
            loc3.location_name,
            loc2.location_id,
            loc2.location_name,
            loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            dev.device_id,
            dev.device_name,
            hs.host_id,
            hs.host_label_cd,
            ht.host_type_id,
            ht.host_type_desc,
            pst.payment_subtype_id,
            pst.payment_subtype_name,
            pst.client_payment_type_cd,
            cpt.client_payment_type_desc,
            TRUNC (tr.tran_start_ts, 'DD')
/

-- Drop the old synonym VW_TOTAL_SALES_BY_HOST
DROP PUBLIC SYNONYM VW_TOTAL_SALES_BY_HOST
/

-- Create synonym VW_TOTAL_SALES_BY_HOST
CREATE PUBLIC SYNONYM vw_total_sales_by_host
  FOR pss.vw_total_sales_by_host
/


-- End of DDL Script for View PSS.VW_TOTAL_SALES_BY_HOST

-- create new view vw_location_hierarchy
-- Start of DDL Script for View LOCATION.VW_LOCATION_HIERARCHY
-- Generated 19-Nov-2004 11:14:49 from LOCATION@USADBD03.USATECH.COM

-- Drop the old instance of VW_LOCATION_HIERARCHY
DROP VIEW location.vw_location_hierarchy
/

CREATE OR REPLACE VIEW location.vw_location_hierarchy (
   ancestor_location_id,
   descendent_location_id,
   depth )
AS
SELECT LOCATION_ID ANCESTOR_LOCATION_ID, CONNECT_BY_ROOT LOCATION_ID DESCENDENT_LOCATION_ID, LEVEL - 1 DEPTH
FROM LOCATION
CONNECT BY LOCATION_ID = PRIOR PARENT_LOCATION_ID
/

-- Drop the old synonym VW_LOCATION_HIERARCHY
DROP PUBLIC SYNONYM VW_LOCATION_HIERARCHY
/

-- Create synonym VW_LOCATION_HIERARCHY
CREATE PUBLIC SYNONYM vw_location_hierarchy
  FOR location.vw_location_hierarchy
/


-- End of DDL Script for View LOCATION.VW_LOCATION_HIERARCHY
CREATE PUBLIC SYNONYM vw_location_hierarchy FOR location.vw_location_hierarchy;


-- ESUDS_STUDENT_REPORT_USER, ESUDS_OPERATOR_REPORT_USER privilages
GRANT SELECT ON device.SEQ_HOST_STATUS_NOTIF_QUEUE_ID TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;
GRANT SELECT, INSERT, DELETE, UPDATE ON device.HOST_STATUS_NOTIF_QUEUE TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;
GRANT SELECT ON device.HOST_STATUS_NOTIF_TYPE TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;
GRANT SELECT ON device.HOST_TYPE TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;
GRANT SELECT ON device.HOST TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;
GRANT SELECT ON device.HOST_SETTING TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;
GRANT SELECT ON device.HOST_DIAG_STATUS TO ESUDS_STUDENT_REPORT_USER WITH GRANT OPTION;

GRANT SELECT ON device.seq_host_status_notif_queue_id TO esuds_operator_report_user WITH GRANT OPTION;
GRANT SELECT, INSERT, DELETE, UPDATE ON device.host_status_notif_queue TO esuds_operator_report_user WITH GRANT OPTION;
GRANT SELECT ON device.host_status_notif_type TO esuds_operator_report_user WITH GRANT OPTION;
GRANT SELECT ON device.host_type TO esuds_operator_report_user WITH GRANT OPTION;
GRANT SELECT ON device.host TO esuds_operator_report_user WITH GRANT OPTION;
GRANT SELECT ON device.host_setting TO esuds_operator_report_user WITH GRANT OPTION;
GRANT SELECT ON device.host_diag_status TO esuds_operator_report_user WITH GRANT OPTION;

-- update card type to support new pos_pta table
UPDATE pos_pta SET payment_subtype_id = 9 WHERE payment_subtype_id = 7;

-- new data for device_setting_parameter 
INSERT INTO device_setting_parameter (device_setting_parameter_cd) VALUES ('MAC Address');
INSERT INTO device_setting_parameter (device_setting_parameter_cd) VALUES ('Multiplexor Bootloader Version');

-- public synonym for app_object_type
CREATE PUBLIC SYNONYM app_object_type FOR app_user.app_object_type;

-- Start of DDL Script for Package LOCATION.PKG_CUSTOMER_MAINT
-- Generated 29-Nov-2004 14:04:12 from LOCATION@USADBD03.USATECH.COM

-- Drop the old instance of PKG_CUSTOMER_MAINT
DROP PACKAGE location.pkg_customer_maint
/

CREATE OR REPLACE 
PACKAGE location.pkg_customer_maint
  IS
--
-- Contains procedures and functions for setting up schools and adding operators
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- BKRUG       11-17-2004  NEW


  FUNCTION ADD_CUSTOMER(
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_customer_addr1 CUSTOMER.CUSTOMER_ADDR1%TYPE,
        l_customer_addr2 CUSTOMER.CUSTOMER_ADDR2%TYPE,
        l_customer_city CUSTOMER.CUSTOMER_CITY%TYPE,
        l_customer_county CUSTOMER.CUSTOMER_COUNTY%TYPE,
        l_customer_postal_cd CUSTOMER.CUSTOMER_POSTAL_CD%TYPE,
        l_customer_state_cd CUSTOMER.CUSTOMER_STATE_CD%TYPE,
        l_customer_country_cd CUSTOMER.CUSTOMER_COUNTRY_CD%TYPE,  -- US / CA
        l_customer_type_id CUSTOMER.CUSTOMER_TYPE_ID%TYPE,
        l_app_user_name  APP_USER.APP_USER_NAME%TYPE,
        l_email_addr APP_USER.APP_USER_EMAIL_ADDR%TYPE,
        l_subdomain SUBDOMAIN_NAME.SUBDOMAIN_NAME_CD%TYPE)
     RETURN CUSTOMER.CUSTOMER_ID%TYPE;
END; -- Package spec
/


CREATE OR REPLACE 
PACKAGE BODY location.pkg_customer_maint
IS
--
-- Contains procedures and functions for setting up schools and adding operators
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- BKRUG       11-17-2004  NEW
    FUNCTION ADD_CUSTOMER(
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_customer_addr1 CUSTOMER.CUSTOMER_ADDR1%TYPE,
        l_customer_addr2 CUSTOMER.CUSTOMER_ADDR2%TYPE,
        l_customer_city CUSTOMER.CUSTOMER_CITY%TYPE,
        l_customer_county CUSTOMER.CUSTOMER_COUNTY%TYPE,
        l_customer_postal_cd CUSTOMER.CUSTOMER_POSTAL_CD%TYPE,
        l_customer_state_cd CUSTOMER.CUSTOMER_STATE_CD%TYPE,
        l_customer_country_cd CUSTOMER.CUSTOMER_COUNTRY_CD%TYPE,  -- US / CA
        l_customer_type_id CUSTOMER.CUSTOMER_TYPE_ID%TYPE,
        l_app_user_name  APP_USER.APP_USER_NAME%TYPE,
        l_email_addr APP_USER.APP_USER_EMAIL_ADDR%TYPE,
        l_subdomain SUBDOMAIN_NAME.SUBDOMAIN_NAME_CD%TYPE)
     RETURN CUSTOMER.CUSTOMER_ID%TYPE
    IS
        l_customer_id CUSTOMER.CUSTOMER_ID%TYPE;
        l_app_user_id APP_USER.APP_USER_ID%TYPE;
        l_cnt PLS_INTEGER;
    BEGIN
        -- check for unique user name and subdomain
        SELECT COUNT(*)
          INTO l_cnt
          FROM APP_USER
         WHERE APP_USER_NAME = l_app_user_name;
        IF l_cnt > 0 THEN
            RAISE_APPLICATION_ERROR(-20111, 'The user name, "'||l_app_user_name||'", is already taken. Please choose another.');
        END IF;

        SELECT COUNT(*)
          INTO l_cnt
          FROM SUBDOMAIN_NAME
         WHERE SUBDOMAIN_NAME_CD = l_subdomain;
        IF l_cnt > 0 THEN
            RAISE_APPLICATION_ERROR(-20112, 'The sub domain, "'||l_subdomain||'", is already taken. Please choose another.');
        END IF;

        -- create customer
        SELECT SEQ_CUSTOMER_ID.NEXTVAL, SEQ_APP_USER_ID.NEXTVAL
          INTO l_customer_id, l_app_user_id
          FROM DUAL;
        INSERT INTO CUSTOMER(
            CUSTOMER_ID,
            CUSTOMER_NAME,
            CUSTOMER_ADDR1,
            CUSTOMER_ADDR2,
            CUSTOMER_CITY,
            CUSTOMER_COUNTY,
            CUSTOMER_POSTAL_CD,
            CUSTOMER_STATE_CD,
            CUSTOMER_COUNTRY_CD,
            CUSTOMER_TYPE_ID,
            CUSTOMER_ACTIVE_YN_FLAG)
        VALUES(
            l_customer_id,
            l_customer_name,
            l_customer_addr1,
            l_customer_addr2,
            l_customer_city,
            l_customer_county,
            l_customer_postal_cd,
            l_customer_state_cd,
            l_customer_country_cd,
            l_customer_type_id,
            'Y');

        -- create app user
        IF l_app_user_name IS NOT NULL THEN
            INSERT INTO APP_USER(
                APP_USER_ID,
                APP_USER_PASSWORD,
                APP_USER_ACTIVE_YN_FLAG,
                FORCE_PW_CHANGE_YN_FLAG,
                APP_USER_NAME,
                APP_USER_FNAME,
                APP_USER_LNAME,
                APP_USER_EMAIL_ADDR)
            VALUES(
                l_app_user_id,
                SUBSTR(l_app_user_name, 1, 30),
                'Y',
                'Y',
                l_app_user_name,
                l_customer_name,
                NULL,
                l_email_addr);

            -- create app user permissions
            INSERT INTO APP_USER_OBJECT_PERMISSION(
                APP_USER_ID,
                APP_ID,
                APP_OBJECT_TYPE_ID,
                OBJECT_CD,
                ALLOW_OBJECT_CREATE_YN_FLAG,
                ALLOW_OBJECT_READ_YN_FLAG,
                ALLOW_OBJECT_MODIFY_YN_FLAG,
                ALLOW_OBJECT_DELETE_YN_FLAG)
            SELECT
                l_app_user_id,
                APP_ID,
                2,
                l_customer_id,
                'N',
                'Y',
                'Y',
                'N'
            FROM APP_USER.APP
            WHERE APP_ID IN(2, 5);
        END IF;

        -- create subdomain
        IF l_subdomain IS NOT NULL THEN
            INSERT INTO SUBDOMAIN_NAME(
                DOMAIN_NAME_URL,
                SUBDOMAIN_NAME_CD,
                APP_OBJECT_TYPE_ID,
                SUBDOMAIN_NAME_DESC,
                OBJECT_CD)
            SELECT
                DOMAIN_NAME_URL,
                l_subdomain,
                2,
                l_customer_name,
                l_customer_id
              FROM DOMAIN_NAME;
        END IF;
        RETURN l_customer_id;
    END;

   -- Enter further code below as specified in the Package spec.
END;
/


-- End of DDL Script for Package LOCATION.PKG_CUSTOMER_MAINT
CREATE PUBLIC SYNONYM pkg_customer_maint FOR location.pkg_customer_maint;
GRANT EXECUTE ON LOCATION.pkg_customer_maint TO web_user WITH GRANT OPTION;

-- constraint & forced validation for app_user.app_user_name
ALTER TABLE APP_USER.APP_USER ADD CONSTRAINT AK_APP_USER_APP_USER_NAME
  UNIQUE (
  APP_USER_NAME
)
/

CREATE OR REPLACE TRIGGER APP_USER.TRBI_APP_USER
 BEFORE 
 INSERT
 ON APP_USER.APP_USER
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
Begin

    IF :new.app_user_id IS NULL THEN

      SELECT seq_app_user_id.nextval
        into :new.app_user_id
        FROM dual;

    END IF;

 SELECT 
           sysdate,
           user,
           sysdate,
           user,
           lower(:new.app_user_name)
      into 
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.app_user_name
      FROM dual;

End;
/

