create public synonym BLACKBRD_AUTHORITY for pss.BLACKBRD_AUTHORITY;
create public synonym BLACKBRD_TENDER for pss.BLACKBRD_TENDER;
create public synonym CC_AUTHORITY for pss.CC_AUTHORITY;
create public synonym CC_MERCHANT for pss.CC_MERCHANT;
create public synonym CC_PAYMENT_TYPE for pss.CC_PAYMENT_TYPE;
create public synonym CC_TERMINAL for pss.CC_TERMINAL;
create public synonym INTERNAL_AUTHORITY for pss.INTERNAL_AUTHORITY;
create public synonym INTERNAL_PAYMENT_TYPE for pss.INTERNAL_PAYMENT_TYPE;
create public synonym RERIX_PAYMENT_TYPE for pss.RERIX_PAYMENT_TYPE;
create public synonym PAYMENT_SUBTYPE for pss.PAYMENT_SUBTYPE;

create public synonym location_contact for location.PAYMENT_SUBTYPE;
create public synonym device_contact for location.PAYMENT_SUBTYPE;
create public synonym pos_contact for location.PAYMENT_SUBTYPE;

create public synonym customer_contact for location.PAYMENT_SUBTYPE;
create public synonym CONTACT_contact_TYPE for location.CONTACT_contact_TYPE;

create public synonym pos_setting for pss.pos_setting;
create public synonym pos_pta for pss.pos_pta;

create public synonym VW_ESUDS_TRAN_LINE_ITEM_DTL for pss.VW_ESUDS_TRAN_LINE_ITEM_DTL;
create public synonym VW_ESUDS_TRAN_LINE_ITEM_DTL for pss.VW_ESUDS_TRAN_LINE_ITEM_DTL;
CREATE PUBLIC SYNONYM VW_TLI_PIVOT_VIEW FOR PSS.VW_TLI_PIVOT_VIEW;

CREATE PUBLIC SYNONYM vw_total_sales_by_host FOR PSS.vw_total_sales_by_host;


CREATE PUBLIC SYNONYM SP_NXT_DEVICE_TRAN_ID_BLCK_BRD for pss.SP_NXT_DEVICE_TRAN_ID_BLCK_BRD;
