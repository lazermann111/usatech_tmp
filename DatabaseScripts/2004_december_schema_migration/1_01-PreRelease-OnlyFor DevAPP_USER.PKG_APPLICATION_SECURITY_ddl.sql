-- Start of DDL Script for Package Body APP_USER.PKG_APPLICATION_SECURITY
-- Generated 14-Jul-2004 14:52:22 from APP_USER@usadbd02

CREATE OR REPLACE 
PACKAGE BODY          pkg_application_security
IS
   e_invalid_login_credentials EXCEPTION;
   cv_server_name    CONSTANT VARCHAR2 (30) := 'usadbd02';
   cv_package_name   CONSTANT VARCHAR2 (30) := 'PKG_APPLICATION_SECURITY';
   cv_encryption_key CONSTANT VARCHAR2 (54) := ';*3h(*^Y';
   

/*******************************************************************************
 Function Name: sf_is_logon_info_valid

 Description: The sf_is_logon_info_valid function will verify if the
              user supplied a valid logon name and password for the application
              they are attempting to access.

 Parameters:
             pv_user_name
                The logon name the user supplied.
             pv_password
                The password the user supplied.
             pn_error_number
                Indicates if an error/exception occurred during the execution
                of this procedure.  The most severe exception will be returned
                if more than one exception occurred.
                    0 = Successful execution.
                    <Any number but 0> = The error number of the error/exception
                        that occurred.
             pv_error_message
                An optional descriptive message to give more information about the
                error/exception (if it occurred).

Function Return Value:
             Boolean value that indicates if the user's name and password are valid.
                FALSE = The username and/or the password provided are not valid
                TRUE = The username and password provided are valid.

 MODIFICATION HISTORY:
 Person     Date          Comments
 ---------  ----------    -------------------------------------------
 MDH       30-MAY-2003    Initial Creation
*******************************************************************************/
   PROCEDURE sp_get_user_access
            ( pv_user_name                IN         APP_USER.APP_USER_NAME%TYPE,
              pv_password                 IN         APP_USER.APP_USER_PASSWORD%TYPE,
              pv_app_cd                   IN         APP.APP_CD%TYPE,
              pv_user_id                  OUT        APP_USER.APP_USER_ID%TYPE,
              pv_force_pw_change_yn_flag  OUT        APP_USER.FORCE_PW_CHANGE_YN_FLAG%TYPE,
              pn_return_code              OUT        exception_code.exception_code_id%TYPE,
              pv_error_message            OUT        exception_data.additional_information%TYPE)


   IS
      v_error_msg               exception_data.additional_information%TYPE;
      cv_procedure_name         CONSTANT VARCHAR2 (30)        := 'sf_is_logon_info_valid';
      n_return_cd               NUMBER;

      CURSOR cr_get_user_role IS
            SELECT au.app_user_id, au.force_pw_change_yn_flag
              FROM app_user au, app_user_object_permission auop, app ap
             WHERE APP_USER_ACTIVE_YN_FLAG = 'Y'
               AND upper(app_user_name) = upper(pv_user_name)
               AND upper(app_user_password) = upper(pv_password)
               AND upper(app_cd) = upper(pv_app_cd)
               AND ap.app_id = auop.app_id
               AND au.app_user_id = auop.app_user_id;

   BEGIN

        pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.successful_execution;

        OPEN cr_get_user_role;

        FETCH cr_get_user_role
         INTO pv_user_id, pv_force_pw_change_yn_flag;

        IF cr_get_user_role%NOTFOUND THEN
        
            CLOSE cr_get_user_role;
    
            RAISE e_invalid_login_credentials;
        
        END IF;
        
        CLOSE cr_get_user_role;

   EXCEPTION
        WHEN e_invalid_login_credentials THEN
            pv_error_message :=
                    'Invalid username or password';
            pn_return_code :=
                        pkg_app_exec_hist_globals.invalid_login_credentials;
                       
            pkg_exception_processor.sp_log_exception (pn_return_code,
                                                      pv_error_message,
                                                      cv_server_name,
                                                          cv_package_name
                                                       || '.'
                                                       || cv_procedure_name);
        WHEN OTHERS THEN
         pv_error_message :=
                'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;

         pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id;

         pkg_exception_processor.sp_log_exception (
            pn_return_code, v_error_msg, cv_server_name,
            cv_package_name || '.' || cv_procedure_name);


         pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.unsuccessful_execution;

 END;

   PROCEDURE sp_change_user_password
            ( pv_user_name                IN         APP_USER.APP_USER_NAME%TYPE,
              pv_current_password         IN         APP_USER.APP_USER_PASSWORD%TYPE,
              pv_new_password             IN         APP_USER.APP_USER_PASSWORD%TYPE,
              pn_return_code              OUT        exception_code.exception_code_id%TYPE,
              pv_error_message            OUT        exception_data.additional_information%TYPE)
    IS

      v_padded_password         VARCHAR2(4000);
      v_encrypted_password      VARCHAR2(4000);
      v_error_msg               exception_data.additional_information%TYPE;
      cv_procedure_name         CONSTANT VARCHAR2 (30)        := 'sp_change_user_password';
      n_return_cd               NUMBER;
      n_user_pw_match           NUMBER;

    BEGIN

        -- The dbms_obfuscation_toolkit requires the length of the input string
        -- to be a multiple of 8
        v_padded_password := RPAD(pv_current_password,
                                 length(pv_current_password) + length(pv_current_password) mod 8,
                                 chr(0));



        dbms_obfuscation_toolkit.DESEncrypt(input_string => v_padded_password,
                                             key_string => cv_encryption_key,
                                             encrypted_string => v_encrypted_password);

        SELECT count(*)
          INTO n_user_pw_match
          FROM app_user
         WHERE app_user_name = pv_user_name
           AND app_user_password = v_encrypted_password;


        dbms_output.put_line(pv_current_password);
        dbms_output.put_line(v_padded_password);
        dbms_output.put_line(v_encrypted_password);

    EXCEPTION

        WHEN OTHERS THEN
         pv_error_message :=
                'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;

         pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id;

         pkg_exception_processor.sp_log_exception (
            pn_return_code, v_error_msg, cv_server_name,
            cv_package_name || '.' || cv_procedure_name);


         pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.unsuccessful_execution;

    END;


PROCEDURE SP_GET_ALL_APP_PERM
            ( pcr_app_perm                OUT        rc_app_perm,
              pn_return_code              OUT        exception_code.exception_code_id%TYPE,
              pv_error_message            OUT        exception_data.additional_information%TYPE)
    IS


      v_error_msg               exception_data.additional_information%TYPE;
      cv_procedure_name         CONSTANT VARCHAR2 (30)        := 'sp_change_user_password';
      n_return_cd               NUMBER;


    BEGIN

        -- The dbms_obfuscation_toolkit requires the length of the input string
        -- to be a multiple of 8
        
        OPEN pcr_app_perm FOR SELECT AP.APP_CD, AU.APP_USER_ID, AU.APP_USER_name, AOT.APP_OBJECT_CD, 
AUOP.OBJECT_CD, AUOP.ALLOW_OBJECT_CREATE_YN_FLAG,
AUOP.ALLOW_OBJECT_READ_YN_FLAG, AUOP.ALLOW_OBJECT_MODIFY_YN_FLAG,
AUOP.ALLOW_OBJECT_DELETE_YN_FLAG FROM APP_USER_OBJECT_PERMISSION AUOP, APP_USER AU, APP AP, APP_OBJECT_TYPE AOT
 WHERE AUOP.APP_ID = AP.APP_ID AND
AUOP.APP_USER_ID = AU.APP_USER_ID AND
AUOP.APP_OBJECT_TYPE_ID = AOT.APP_OBJECT_TYPE_ID ORDER BY AU.APP_USER_ID;
        

    EXCEPTION

        WHEN OTHERS THEN
         pv_error_message :=
                'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;

         pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id;

         pkg_exception_processor.sp_log_exception (
            pn_return_code, v_error_msg, cv_server_name,
            cv_package_name || '.' || cv_procedure_name);


         pn_return_code := PKG_APP_EXEC_HIST_GLOBALS.unsuccessful_execution;

    END;
    
 END;
/

-- Grants for Package Body
GRANT EXECUTE ON pkg_application_security TO esuds_developer
/
GRANT EXECUTE ON pkg_application_security TO security_admin
/


-- End of DDL Script for Package Body APP_USER.PKG_APPLICATION_SECURITY

