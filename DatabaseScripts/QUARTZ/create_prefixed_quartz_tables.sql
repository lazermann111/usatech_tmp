-- Default Quartz Configuration, updated with the following:
-- * QUARTZ.<Initials>_QRTZ_ prefix
-- * TABLESPACES
-- * PK and FK names
-- DELETE statements removed
-- DROP statements moved to drop_quartz_tables.sql
--
-- A hint submitted by a user: Oracle DB MUST be created as "shared" and the 
-- job_queue_processes parameter  must be greater than 2, otherwise a DB lock 
-- will happen.   However, these settings are pretty much standard after any
-- Oracle install, so most users need not worry about this.
--
-- Many other users (including the primary author of Quartz) have had success
-- runing in dedicated mode, so only consider the above as a hint ;-)
--

CREATE TABLE QUARTZ.<initials>_qrtz_job_details
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR2(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_JOB_DETAILS PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL, 
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_<INITIALS>_QJD_SCHED_N_JOB_N_AND_G FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP) 
	REFERENCES QUARTZ.<initials>_qrtz_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP) 
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_simple_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(10) NOT NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_SIMPLE_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_<INITIALS>_QT_SCHED_N_TRG_N_AND_G FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES QUARTZ.<initials>_qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_cron_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSION VARCHAR2(120) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    CONSTRAINT PK_<INITIALS>_QRTZ_CRON_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_<INITIALS>_QCTQT_SCH_N_TRG_N_AND_G FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES QUARTZ.<initials>_qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_simprop_triggers
  (          
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    STR_PROP_1 VARCHAR(512) NULL,
    STR_PROP_2 VARCHAR(512) NULL,
    STR_PROP_3 VARCHAR(512) NULL,
    INT_PROP_1 NUMBER(10) NULL,
    INT_PROP_2 NUMBER(10) NULL,
    LONG_PROP_1 NUMBER(13) NULL,
    LONG_PROP_2 NUMBER(13) NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR(1) NULL,
    BOOL_PROP_2 VARCHAR(1) NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_SIMPROP_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_<INITIALS>_QSTQT_SCH_N_TRG_N_AND_G FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES QUARTZ.<initials>_qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_blob_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_BLOB_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES QUARTZ.<initials>_qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_calendars
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    CALENDAR_NAME  VARCHAR2(200) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_CALENDARS PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_paused_trigger_grps
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL, 
    CONSTRAINT PK_<INITIALS>_QRTZ_PAUSED_TRG_GRPS PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_fired_triggers 
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_NONCONCURRENT VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_FIRED_TRIGGERS PRIMARY KEY (SCHED_NAME,ENTRY_ID)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_scheduler_state 
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    CONSTRAINT PK_<INITIALS>_QRTZ_SCHEDULER_STATE PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.<initials>_qrtz_locks
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    CONSTRAINT PK_<INITIALS>_QRTZ_LOCKS PRIMARY KEY (SCHED_NAME,LOCK_NAME)
) TABLESPACE QUARTZ_DATA;

create index QUARTZ.idx_<initials>_qrtz_j_req_recovery on QUARTZ.<initials>_qrtz_job_details(SCHED_NAME,REQUESTS_RECOVERY) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_j_grp on QUARTZ.<initials>_qrtz_job_details(SCHED_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;

create index QUARTZ.idx_<initials>_qrtz_t_j on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_jg on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_c on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,CALENDAR_NAME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_g on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,TRIGGER_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_state on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_n_state on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_n_g_state on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_next_fire_time on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,NEXT_FIRE_TIME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_nft_st on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_nft_misfire on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_nft_st_misfire on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_t_nft_st_misf_grp on QUARTZ.<initials>_qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;

create index QUARTZ.idx_<initials>_qrtz_ft_trig_inst_name on QUARTZ.<initials>_qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_ft_inst_req_rcvry on QUARTZ.<initials>_qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_ft_j_g on QUARTZ.<initials>_qrtz_fired_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_ft_jg on QUARTZ.<initials>_qrtz_fired_triggers(SCHED_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_ft_t_g on QUARTZ.<initials>_qrtz_fired_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_<initials>_qrtz_ft_tg on QUARTZ.<initials>_qrtz_fired_triggers(SCHED_NAME,TRIGGER_GROUP) TABLESPACE QUARTZ_INDX;

grant select on QUARTZ.<initials>_qrtz_calendars to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_fired_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_blob_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_cron_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_simple_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_simprop_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_job_details to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_paused_trigger_grps to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_locks to USAT_DEV_READ_ONLY;
grant select on QUARTZ.<initials>_qrtz_scheduler_state to USAT_DEV_READ_ONLY;

grant select,insert,update,delete on QUARTZ.<initials>_qrtz_calendars to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_fired_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_blob_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_cron_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_simple_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_simprop_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_job_details to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_paused_trigger_grps to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_locks to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;
grant select,insert,update,delete on QUARTZ.<initials>_qrtz_scheduler_state to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE, USAT_RPT_REQ_ROLE, USAT_RPT_GEN_ROLE;

grant insert,update,delete on QUARTZ.<initials>_qrtz_calendars to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_fired_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_blob_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_cron_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_simple_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_simprop_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_job_details to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_paused_trigger_grps to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_locks to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.<initials>_qrtz_scheduler_state to USATECH_UPD_TRANS;

