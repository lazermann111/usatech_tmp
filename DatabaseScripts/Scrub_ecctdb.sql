-- user password and email
UPDATE REPORT.USER_LOGIN
   SET PASSWORD_HASH= null,
       EMAIL = NULL,
       USER_NAME = 'user' || USER_ID,
       FIRST_NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       LAST_NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       TELEPHONE = NULL,
       FAX = NULL;

-- bank acct info
UPDATE CORP.CUSTOMER_BANK
   SET BANK_ACCT_NBR = TRUNC(DBMS_RANDOM.VALUE(1000000, 99999999)),
       BANK_ROUTING_NBR = TRUNC(DBMS_RANDOM.VALUE(10000000, 999999999)),
       CONTACT_NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       CONTACT_TELEPHONE = NULL,
       CONTACT_FAX = NULL,
       BANK_NAME = 'Bank ' || DBMS_RANDOM.STRING('U', 3),
       ACCOUNT_TITLE = 'Account For #' || CUSTOMER_ID,
       BANK_ADDRESS = '100 Deerfield Ln.',
       BANK_CITY = 'Malvern',
       BANK_STATE = 'PA',
       BANK_ZIP = '19355',
       DESCRIPTION = 'Customer #' || CUSTOMER_ID || ' Acct',
 TAX_ID_NBR = TRUNC(DBMS_RANDOM.VALUE(10000, 999999));

--corp.doc

UPDATE CORP.DOC D
   SET (BANK_ACCT_NBR, BANK_ROUTING_NBR) = (SELECT CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR FROM CORP.CUSTOMER_BANK CB WHERE CB.CUSTOMER_BANK_ID = D.CUSTOMER_BANK_ID);

--CORP.CUSTOMER

UPDATE CORP.CUSTOMER
   SET CUSTOMER_NAME = 'Customer #' || CUSTOMER_ID,
       CUSTOMER_ALT_NAME = NULL,
       SALES_TERM = NULL,
DOING_BUSINESS_AS= 'Customer #' || CUSTOMER_ID;

--CORP.LOCATION

UPDATE report.location
   SET location_NAME = 'Location #' || LOCATION_ID,
DESCRIPTION=NULL;

-- LOCATION.CUSTOMER

UPDATE LOCATION.CUSTOMER
   SET CUSTOMER_NAME = 'Customer #' || CUSTOMER_ID,
Customer_ADDR1= '100 Deerfield Ln.',
       customer_CITY = 'Malvern',
       customer_STATE_cd = 'PA',
       customer_postal_cd = '19355' where NVL(customer_COUNTRY_CD,'US') ='US';

UPDATE LOCATION.CUSTOMER
   SET CUSTOMER_NAME = 'Customer #' || CUSTOMER_ID,
Customer_ADDR1= '100 Deerfield Ln.',
       customer_CITY = 'San Juan',
       customer_STATE_cd = null,
       customer_postal_cd = null where customer_COUNTRY_CD='PR';

UPDATE LOCATION.CUSTOMER
   SET CUSTOMER_NAME = 'Customer #' || CUSTOMER_ID,
Customer_ADDR1= '100 Deerfield Ln.',
       customer_CITY = 'Edmonton',
       customer_STATE_cd = 'AB',
       customer_postal_cd = '99999' where customer_COUNTRY_CD='CA';

UPDATE LOCATION.CUSTOMER
   SET CUSTOMER_NAME = 'Customer #' || CUSTOMER_ID,
Customer_ADDR1= '100 Deerfield Ln.',
       customer_CITY = null,
       customer_STATE_cd = null,
       customer_postal_cd = null where NVL(customer_COUNTRY_CD,'US') not in ('PR','CA','US');

--LOCATION.LOCATION

UPDATE LOCATION.location
   SET location_NAME  = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
      LOCATION_ADDR1= '100 Deerfield Ln.',
       location_CITY = 'Malvern',
       location_STATE_cd = 'PA',
       location_postal_cd = '19355' where NVL(LOCATION_COUNTRY_CD,'US')='US';

UPDATE LOCATION.location
   SET location_NAME  = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
          LOCATION_ADDR1= '100 Deerfield Ln.',
       location_CITY = 'Edmonton',
       location_STATE_cd = 'AB',
       location_postal_cd = '99999' where LOCATION_COUNTRY_CD='CA';

UPDATE LOCATION.location
   SET location_NAME  = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
          LOCATION_ADDR1= '100 Deerfield Ln.',
       location_CITY = 'San Juan',
       location_STATE_cd = NULL,
       location_postal_cd = NULL where LOCATION_COUNTRY_CD='PR';

UPDATE LOCATION.location
   SET location_NAME  = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
             LOCATION_ADDR1= '100 Deerfield Ln.',
       location_CITY = null,
       location_STATE_cd = null,
       location_postal_cd = null where NVL(LOCATION_COUNTRY_CD,'US') not in ('CA','PR','US');

--CORP.CUSTOMER_ADDR

UPDATE CORP.CUSTOMER_ADDR
   SET NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       ATTN_TO = NULL,
       ADDRESS1 = '100 Deerfield Ln.',
       CITY = 'Malvern',
       STATE = 'PA',
       ZIP = '19355' where NVL(COUNTRY_CD,'US')='US';

UPDATE CORP.CUSTOMER_ADDR
   SET NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       ATTN_TO = NULL,
       ADDRESS1 = '100 Deerfield Ln.',
       CITY = 'Edmonton',
       STATE = 'AB',
       ZIP = '99999' where COUNTRY_CD='CA';

UPDATE CORP.CUSTOMER_ADDR
   SET NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       ATTN_TO = NULL,
       ADDRESS1 = '100 Deerfield Ln.',
       CITY = 'San Juan',
       STATE = NULL,
       ZIP =NULL  where COUNTRY_CD='PR';

UPDATE CORP.CUSTOMER_ADDR
   SET NAME = DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) || ' ' || DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
       ATTN_TO = NULL,
       ADDRESS1 = '100 Deerfield Ln.',
       CITY = null,
       STATE = null,
       ZIP = null where COUNTRY_CD not in ('CA','PR','US');

--REPORT.TERMINAL_ADDR

update report.terminal_addr set ADDRESS1='100 Deerfield Ln.',
       CITY = 'Malvern',
       STATE = 'PA',
       ZIP = '19355' where NVL(country_cd,'US')='US';

update report.terminal_addr set ADDRESS1='100 Deerfield Ln.',
       CITY = 'Edmonton',
       STATE = 'AB',
       ZIP = '99999' where country_cd='CA';

update report.terminal_addr set ADDRESS1='100 Deerfield Ln.',
       CITY = null,
       STATE = null,
       ZIP = '08000' where country_cd not in ('US','CA');

-- disable user_reports
UPDATE REPORT.USER_REPORT
  SET STATUS = 'D'
 WHERE STATUS = 'A';

-- ccs_transport_properties
DELETE
FROM REPORT.CCS_TRANSPORT_PROPERTY;

-- pss.consumer_acct
 
    update pss.consumer_acct ca set 
CONSUMER_ACCT_CD='639621*********0807', CONSUMER_ACCT_IDENTIFIER=NULL where consumer_id not in (select consumer_id from pss.consumer where lower(CONSUMER_EMAIL_ADDR1) like '%usatech.com%') ;

--pss.consumer

update pss.consumer c set CONSUMER_WORK_PHONE_NUM='610-989-0340',
CONSUMER_CELL_PHONE_NUM=null,CONSUMER_FAX_PHONE_NUM=null, CONSUMER_LNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3), CONSUMER_FNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),CONSUMER_EMAIL_ADDR1=null,CONSUMER_EMAIL_ADDR2=null,
CONSUMER_ADDR1= '100 Deerfield Ln.',
       CONSUMER_CITY= 'Malvern',
       CONSUMER_STATE_CD= 'PA',
       CONSUMER_POSTAL_CD= '19355'
where lower(CONSUMER_EMAIL_ADDR1) not like '%usatech.com%'
and NVL(CONSUMER_COUNTRY_CD,'US')='US';

update pss.consumer c set CONSUMER_WORK_PHONE_NUM='610-989-0340',
CONSUMER_CELL_PHONE_NUM=null,CONSUMER_FAX_PHONE_NUM=null, CONSUMER_LNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3), CONSUMER_FNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),CONSUMER_EMAIL_ADDR1=null,CONSUMER_EMAIL_ADDR2=null,
CONSUMER_ADDR1= '100 Deerfield Ln.',
       CONSUMER_CITY= 'Edmonton',
       CONSUMER_STATE_CD= 'AB',
       CONSUMER_POSTAL_CD= '99999'
where lower(CONSUMER_EMAIL_ADDR1) not like '%usatech.com%'
and NVL(CONSUMER_COUNTRY_CD,'US')='CA';


update pss.consumer c set CONSUMER_WORK_PHONE_NUM='610-989-0340',
CONSUMER_CELL_PHONE_NUM=null,CONSUMER_FAX_PHONE_NUM=null, CONSUMER_LNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3), CONSUMER_FNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),CONSUMER_EMAIL_ADDR1=null,CONSUMER_EMAIL_ADDR2=null,
CONSUMER_ADDR1= '100 Deerfield Ln.',
       CONSUMER_CITY= NULL,
       CONSUMER_STATE_CD= NULL,
       CONSUMER_POSTAL_CD= null
where lower(CONSUMER_EMAIL_ADDR1) not like '%usatech.com%'
and NVL(CONSUMER_COUNTRY_CD,'US') not in ('US','CA');


-- Some staging tables
delete from corp.routing_num;
drop table corp.routing_num_us_new;
delete from corp.usat_routing_num;

 -- corp.app_setting 
update corp.app_setting set APP_SETTING_VALUE ='26280@eccapr11:15F06F2FA13:3fb643c36429af20' where APP_SETTING_CD='RECUR_CHARGE_LOCK';  

update corp.app_setting set APP_SETTING_VALUE ='2457@eccapr12.usatech.com:15EB9F8E422:3fb0f00d6994f348' where APP_SETTING_CD='OUTBOUND_DATA_EXCHANGE_LOCK';
COMMIT;

-- app_user.app_user 
update app_user.app_user set 
APP_USER_NAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3)||APP_USER_ID||'@usatech.com',  
APP_USER_EMAIL_ADDR=null, APP_USER_FNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3), APP_USER_LNAME=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3),
APP_USER_PASSWORD=DBMS_RANDOM.STRING('U', 1) || DBMS_RANDOM.STRING('L', 3) where lower(APP_USER_EMAIL_ADDR) not like '%usatech.com%';

-- app_user.domain_name -- disable constraints

ALTER TABLE APP_USER.DOMAIN_NAME DISABLE CONSTRAINT PK_DOMAIN_NAME CASCADE;

ALTER TABLE APP_USER.SUBDOMAIN_NAME DISABLE CONSTRAINT PK_SUBDOMAIN_NAME CASCADE;

update app_user.domain_name set DOMAIN_NAME_URL = 'e-suds-tst.netnew' where DOMAIN_NAME_URL ='e-suds.net';
update app_user.domain_name set DOMAIN_NAME_URL = 'esuds-tst.netnew2' where DOMAIN_NAME_URL = 'esuds.net';

-- app_user.subdomain_name
update app_user.subdomain_name set DOMAIN_NAME_URL = 'e-suds-tst.netnew' where DOMAIN_NAME_URL ='e-suds.net';
update app_user.subdomain_name set DOMAIN_NAME_URL = 'esuds-tst.netnew' where DOMAIN_NAME_URL = 'esuds.net';

-- app_user.subdomain_feature

update  app_user.subdomain_feature set DOMAIN_NAME_URL = 'e-suds-tst.netnew' where DOMAIN_NAME_URL ='e-suds.net';
update app_user.subdomain_feature set DOMAIN_NAME_URL = 'esuds-tst.netnew' where DOMAIN_NAME_URL = 'esuds.net';
 
-- Re-enable constraints

ALTER TABLE APP_USER.DOMAIN_NAME ENABLE NOVALIDATE CONSTRAINT PK_DOMAIN_NAME;
ALTER TABLE APP_USER.SUBDOMAIN_NAME ENABLE NOVALIDATE CONSTRAINT FK_SUBDOMAIN_NAME_DOMAIN_NAME;
ALTER TABLE APP_USER.SUBDOMAIN_NAME ENABLE NOVALIDATE CONSTRAINT PK_SUBDOMAIN_NAME;
ALTER TABLE APP_USER.SUBDOMAIN_FEATURE ENABLE NOVALIDATE CONSTRAINT FK_SF_SUBDOMAIN_NAME2;


-- authority.authority. Take a backup and spool update script from ECC
create table authority.authority_bkp as select * from authority.authority;

--select 'update authority.authority set remote_server_addr='''||remote_server_addr||''' where AUTHORITY_NAME='''||AUTHORITY_NAME||''';'  from authority.authority where   remote_server_addr is not null

update authority.authority set remote_server_addr='http://127.0.0.1:9100/posinterface/services/posinterface' where AUTHORITY_NAME='Aqua Fill';
update authority.authority set remote_server_addr='http://eccnet11.usatech.com:9100/posinterface/services/posinterface' where AUTHORITY_NAME='POSGateway Test Authority';
update authority.authority set remote_server_addr='https://www.bsd.ufl.edu/G1CO/Com/VendingWebServ.asmx' where AUTHORITY_NAME='University of Florida';
update authority.authority set remote_server_addr='http://css171.vivonfc.com:8580/VivoGateService/services/posinterface' where AUTHORITY_NAME='ViVOplatform Test';
update authority.authority set remote_server_addr='https://eccapr21.usatech.com:1443/VivoGateService/services/posinterface' where AUTHORITY_NAME='ViVOplatform';
update authority.authority set remote_server_addr='https://api.card.v4dev.com/integrations/usatech/' where AUTHORITY_NAME like '%Atrium Campus%';
update authority.authority set remote_server_addr='http://204.51.112.180:8080/active-bpel/services/soap12/m2p_Value_Transaction' where AUTHORITY_NAME='VZM2P';
update authority.authority set remote_server_addr='https://account-cert.bytevampire.com' where AUTHORITY_NAME='Sprout';
update authority.authority set remote_server_addr='http://67.141.61.110/apps/api/vendingMachine/vend.cfc?wsdl' where AUTHORITY_NAME='Lindsey Wilson College';

-- Authority_server

update  authority.authority_server set AUTHORITY_SERVER_PORT='9100',AUTHORITY_SERVER_ADDR='http://127.0.0.1:9100/posinterface/services/posinterface' where AUTHORITY_SERVER_ADDR<>'127.0.0.1';

-- Authority_gateway
update  authority.authority_gateway set AUTHORITY_GATEWAY_ADDR='127.0.0.1' where AUTHORITY_GATEWAY_ADDR<>'127.0.0.1';

-- host_setting
update device.host_setting  set HOST_SETTING_PARAMETER=host_id||'.'||HOST_SETTING_PARAMETER where host_id not  in (select  host_id from device.host where HOST_SERIAL_CD='0000000000');

--host
update device.host  set HOST_SERIAL_CD='0000000000', HOST_LABEL_CD=null where HOST_SERIAL_CD<>'0000000000';

--CONFIG_TEMPLATE_SETTING
create table DEVICE.CONFIG_TEMPLATE_SETTING_bkp as select * from DEVICE.CONFIG_TEMPLATE_SETTING;

-- run below os ECC and update on ECCTDB 
--select distinct 'update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='''||DEVICE_SETTING_PARAMETER_CD||''', CONFIG_TEMPLATE_SETTING_VALUE='''||CONFIG_TEMPLATE_SETTING_VALUE||''' 
where DEVICE_SETTING_PARAMETER_CD='''||DEVICE_SETTING_PARAMETER_CD||''';'  from   DEVICE.CONFIG_TEMPLATE_SETTING CTS where CONFIG_TEMPLATE_SETTING_VALUE   like '%ecc%' or  CONFIG_TEMPLATE_SETTING_VALUE  like '208%';

update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='1', CONFIG_TEMPLATE_SETTING_VALUE='a2.ecc.eport-usatech.net' 
where DEVICE_SETTING_PARAMETER_CD='1';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='2', CONFIG_TEMPLATE_SETTING_VALUE='208.116.216.61' 
where DEVICE_SETTING_PARAMETER_CD='2';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='ServerAddress', CONFIG_TEMPLATE_SETTING_VALUE='208.116.216.163' 
where DEVICE_SETTING_PARAMETER_CD='ServerAddress';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='0', CONFIG_TEMPLATE_SETTING_VALUE='a1.ecc.eport-usatech.net' 
where DEVICE_SETTING_PARAMETER_CD='0';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='3', CONFIG_TEMPLATE_SETTING_VALUE='208.116.216.61' 
where DEVICE_SETTING_PARAMETER_CD='3';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='21', CONFIG_TEMPLATE_SETTING_VALUE='b2.ecc.eport-usatech.net' 
where DEVICE_SETTING_PARAMETER_CD='21';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='23', CONFIG_TEMPLATE_SETTING_VALUE='208.116.216.61' 
where DEVICE_SETTING_PARAMETER_CD='23';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='22', CONFIG_TEMPLATE_SETTING_VALUE='208.116.216.61' 
where DEVICE_SETTING_PARAMETER_CD='22';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='ServerSecondaryAddress', CONFIG_TEMPLATE_SETTING_VALUE='208.116.216.163' 
where DEVICE_SETTING_PARAMETER_CD='ServerSecondaryAddress';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='20', CONFIG_TEMPLATE_SETTING_VALUE='b1.ecc.eport-usatech.net' 
where DEVICE_SETTING_PARAMETER_CD='20';
update DEVICE.CONFIG_TEMPLATE_SETTING set  DEVICE_SETTING_PARAMETER_CD='284', CONFIG_TEMPLATE_SETTING_VALUE='208116216163' 
where DEVICE_SETTING_PARAMETER_CD='284';

-- engine.app_setting
-- select 'update  engine.app_setting set APP_SETTING_VALUE='''||APP_SETTING_VALUE||''' where APP_SETTING_CD='''||APP_SETTING_CD||''';' from engine.app_setting  where lower(APP_SETTING_VALUE) like '%com%'

update  engine.app_setting set APP_SETTING_VALUE='USATNetworkECC@usatech.com' where APP_SETTING_CD='EMAIL_FROM_ADDRESS';
update  engine.app_setting set APP_SETTING_VALUE='RMA@usatech.com' where APP_SETTING_CD='EMAIL_TO_ADDRESS_CUSTOMER_SERVICE';
update  engine.app_setting set APP_SETTING_VALUE='QualityAssurance@usatech.com' where APP_SETTING_CD='EFT_PROCESSING_EMAIL_TO';
update  engine.app_setting set APP_SETTING_VALUE='http://dms-ecc.usatech.com/' where APP_SETTING_CD='DMS_URL';
update  engine.app_setting set APP_SETTING_VALUE='QualityAssurance@usatech.com' where APP_SETTING_CD='EMAIL_TO_ADDRESS_OFFER_SIGNUP';
update  engine.app_setting set APP_SETTING_VALUE='QualityAssurance@usatech.com' where APP_SETTING_CD='ACH_PROCESSING_EMAIL_TO';
update  engine.app_setting set APP_SETTING_VALUE='USALiveReports-ECC@usatech.com' where APP_SETTING_CD='REPORT_TRANSPORT_ERROR_EMAIL_FROM';

-- pss.merchant
update pss.merchant set STATUS_CD='D' WHERE LOWER(MERCHANT_NAME) not LIKE '%test%' and LOWER(MERCHANT_NAME) not LIKE '%tns%';

-- pss.terminal
update pss.terminal set STATUS_CD='D'  where merchant_id in (select merchant_id from pss.merchant where status_cd='D');

--pss.blackbrd_authority

update pss.blackbrd_authority set
REMOTE_SERVER_ADDR='127.0.0.1',REMOTE_SERVER_ADDR_ALT='127.0.0.1', REMOTE_SERVER_PORT_NUM='9003',  BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003';

-- GET data from ECC: select 'update pss.blackbrd_authority set REMOTE_SERVER_ADDR='''||REMOTE_SERVER_ADDR||''', REMOTE_SERVER_ADDR_ALT='''||REMOTE_SERVER_ADDR_ALT||''', REMOTE_SERVER_PORT_NUM='''||REMOTE_SERVER_PORT_NUM||''',BLACKBRD_GATEWAY_ADDR=''127.0.0.1'' , BLACKBRD_GATEWAY_PORT_NUM=''9003'' where BLACKBRD_AUTHORITY_NAME='''||BLACKBRD_AUTHORITY_NAME||''';' from pss.blackbrd_authority;

update pss.blackbrd_authority set REMOTE_SERVER_ADDR='66.240.10.139', REMOTE_SERVER_ADDR_ALT='66.240.10.139', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Goucher College Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='66.210.59.124', REMOTE_SERVER_ADDR_ALT='69.26.224.124', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Blackboard Test Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='66.210.59.124', REMOTE_SERVER_ADDR_ALT='69.26.224.124', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Blackboard Test Server (auth only)';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='70.17.2.10', REMOTE_SERVER_ADDR_ALT='70.17.2.10', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Villa Julie College Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='199.111.95.71', REMOTE_SERVER_ADDR_ALT='199.111.95.71', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='University of Mary Washington Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='128.220.242.109', REMOTE_SERVER_ADDR_ALT='128.220.242.109', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Johns Hopkins University Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='144.126.1.102', REMOTE_SERVER_ADDR_ALT='144.126.1.102', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Loyola College in Maryland Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='134.53.12.205', REMOTE_SERVER_ADDR_ALT='134.53.12.205', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Miami University Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='146.186.144.113', REMOTE_SERVER_ADDR_ALT='146.186.144.113', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Pennsylvania State University Server - Vairo Village';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='129.22.114.74', REMOTE_SERVER_ADDR_ALT='129.22.114.74', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Case Western Reserve University Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='147.9.107.20', REMOTE_SERVER_ADDR_ALT='147.9.107.20', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='American University Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - 1300 Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - 1940 Park';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - Peabody Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - Temple Towers';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - White Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - Hardwick Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - Johnson Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - East Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - West Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='155.247.80.12', REMOTE_SERVER_ADDR_ALT='155.247.80.12', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Temple University Blackboard Server - Beech Hall';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='129.137.2.207', REMOTE_SERVER_ADDR_ALT='129.137.2.207', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='University of Cincinnati Blackboard Server';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='165.230.136.134', REMOTE_SERVER_ADDR_ALT='165.230.136.134', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Rutgers University Blackboard Server - Busch Campus';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='165.230.136.134', REMOTE_SERVER_ADDR_ALT='165.230.136.134', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Rutgers University Blackboard Server - Camden Campus';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='165.230.136.134', REMOTE_SERVER_ADDR_ALT='165.230.136.134', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Rutgers University Blackboard Server - College Avenue Campus';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='165.230.136.134', REMOTE_SERVER_ADDR_ALT='165.230.136.134', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Rutgers University Blackboard Server - Cook Campus';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='165.230.136.134', REMOTE_SERVER_ADDR_ALT='165.230.136.134', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Rutgers University Blackboard Server - Douglass Campus';
update pss.blackbrd_authority set REMOTE_SERVER_ADDR='165.230.136.134', REMOTE_SERVER_ADDR_ALT='165.230.136.134', REMOTE_SERVER_PORT_NUM='9003',BLACKBRD_GATEWAY_ADDR='127.0.0.1' , BLACKBRD_GATEWAY_PORT_NUM='9003' where BLACKBRD_AUTHORITY_NAME='Rutgers University Blackboard Server - Livingston Campus';

--pss.aramark_authority
update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='127.0.0.1', ARAMARK_GATEWAY_ADDR='127.0.0.1' , ARAMARK_GATEWAY_PORT_NUM='9100';

-- Get data from ECC: select 'update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='''||ARAMARK_REMOTE_SERVER_ADDR||''',ARAMARK_GATEWAY_ADDR=''127.0.0.1'' , ARAMARK_GATEWAY_PORT_NUM=''9100'' where ARAMARK_AUTHORITY_NAME='''||ARAMARK_AUTHORITY_NAME||''';' from pss.aramark_authority;

update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='http://rts.aramarkcampusit.com/relaytransactionservice/relayservice.asmx',ARAMARK_GATEWAY_ADDR='127.0.0.1' , ARAMARK_GATEWAY_PORT_NUM='9100' where ARAMARK_AUTHORITY_NAME='Aramark Towers/Aramark Test Server';
update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='http://152.13.202.2/RelayTransactionService/RelayService.asmx',ARAMARK_GATEWAY_ADDR='127.0.0.1' , ARAMARK_GATEWAY_PORT_NUM='9100' where ARAMARK_AUTHORITY_NAME='UNCG Server';
update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='http://152.17.51.202/RelayTransactionService/RelayService.asmx',ARAMARK_GATEWAY_ADDR='127.0.0.1' , ARAMARK_GATEWAY_PORT_NUM='9100' where ARAMARK_AUTHORITY_NAME='Wake Forest University';
update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='http://72.32.3.134/RelayTransactionService/RelayService.asmx',ARAMARK_GATEWAY_ADDR='127.0.0.1' , ARAMARK_GATEWAY_PORT_NUM='9100' where ARAMARK_AUTHORITY_NAME='LSG SkyChefs';
update pss.aramark_authority set ARAMARK_REMOTE_SERVER_ADDR='http://www.spquantum.net/relayWebService/relayService.asmx',ARAMARK_GATEWAY_ADDR='127.0.0.1' , ARAMARK_GATEWAY_PORT_NUM='9100' where ARAMARK_AUTHORITY_NAME='Quantum Aramark Server';

commit;