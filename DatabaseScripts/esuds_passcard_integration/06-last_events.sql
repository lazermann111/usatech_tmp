-- add passcard to all existing, active esuds devices
BEGIN
	DECLARE
		n_pos_id pos.pos_id%TYPE;
		CURSOR c_get_pos_id IS
			SELECT DISTINCT p.pos_id
			FROM pos p, pos_pta pp, device d
			WHERE d.device_id = p.device_id
			AND p.pos_id = pp.pos_id
			AND p.pos_id NOT IN (
				SELECT distinct pos_id
				FROM pos_pta ppx
				WHERE payment_subtype_id = 16
			)
			AND d.device_active_yn_flag = 'Y'
			AND d.device_type_id = 5;
	BEGIN
		OPEN c_get_pos_id;
		LOOP
			FETCH c_get_pos_id INTO n_pos_id;
			EXIT WHEN c_get_pos_id%NOTFOUND;
			INSERT INTO pos_pta (
				pos_id,
				payment_subtype_id,
				pos_pta_activation_ts,
				payment_subtype_key_id,
				pos_pta_pin_req_yn_flag,
				pos_pta_priority
			) (
				SELECT n_pos_id, 16, SYSDATE, 1, 'N', NVL(MAX(pos_pta_priority), 0) + 1 AS pos_pta_priority_new
				FROM pos_pta pp, payment_subtype ps
				WHERE ps.payment_subtype_id = pp.payment_subtype_id
				AND pos_id = n_pos_id
				AND client_payment_type_cd = (
					SELECT client_payment_type_cd
					FROM payment_subtype
					WHERE payment_subtype_id = 16
				)
			);
		END LOOP;
		COMMIT;
		CLOSE c_get_pos_id;
	END;
END;
