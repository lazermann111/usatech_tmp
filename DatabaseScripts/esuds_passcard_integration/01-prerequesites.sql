INSERT INTO pss.authority_payment_mask
(
	authority_payment_mask_id, 
	authority_payment_mask_name, 
	authority_payment_mask_desc, 
	authority_payment_mask_regex,
	authority_payment_mask_bref
)
VALUES
(
	25, 
	'Default', 
	'Special Card - eSuds Passcard', 
	'^(9{6}[0-9]{3})(=?(0044))?$',
	'1:1|3:7'
);

INSERT INTO pss.payment_subtype
(
    payment_subtype_id, 
    payment_subtype_name, 
    payment_subtype_class,
    payment_subtype_key_name, 
    client_payment_type_cd, 
    payment_subtype_table_name, 
    payment_subtype_key_desc_name, 
    authority_payment_mask_id
)
VALUES
(
    16,
    'Special Card - eSuds Passcard', 
    'Internal',
    'INTERNAL_PAYMENT_TYPE_ID',
    'S',
    'INTERNAL_PAYMENT_TYPE',
    'INTERNAL_PAYMENT_TYPE_DESC',
    25
);

UPDATE pss.authority_payment_mask 
SET payment_subtype_id = 16
WHERE authority_payment_mask_id = 25;
