CREATE USER WEB_CONTENT
IDENTIFIED BY WEB_CONTENT
DEFAULT TABLESPACE WEB_CONTENT_DATA
TEMPORARY TABLESPACE TEMPTS1
/
ALTER USER WEB_CONTENT QUOTA UNLIMITED ON WEB_CONTENT_data QUOTA UNLIMITED ON WEB_CONTENT_indx
/
GRANT CONNECT TO WEB_CONTENT
/
GRANT RESOURCE TO WEB_CONTENT
/

