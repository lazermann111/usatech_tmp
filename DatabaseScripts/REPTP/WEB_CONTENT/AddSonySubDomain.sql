INSERT INTO web_content.subdomain(subdomain_id, SUBDOMAIN_URL, SUBDOMAIN_DESCRIPTION)
VALUES(2, 'sony.usatech.com'/*'127.0.0.1'*//*sony.usatech.com*//*'sonydemo.usatech.com' */, 'Sony Website');
/*
SELECT * FROM web_content.subdomain;
SELECT * FROM web_content.literal WHERE subdomain_id IN (2,22);
*/
INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'main-title', 'Sony PictureStation Reporting');

INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'logo-image-url', './images/sony/sony_logo.jpg');

INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'banner-image-url', './images/sony/sony_banner.jpg');

INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'banner-ext-image-url', './images/sony/sony_banner_ext.jpg');

INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'main-stylesheets', 'css/sony-general-style.css');

INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'app-stylesheets', 'css/usalive-app-style.css,css/sony-app-style.css');

INSERT INTO web_content.literal(LITERAL_ID, SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
VALUES(web_content.seq_literal_id.NEXTVAL, 2, 'menu-arrow-image-url', './images/sony/sony_strip.gif');

-- for remote file update
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.schedule.failure','Could not upload the file to {1} because: {0}.');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.error.io','Could not schedule the file upload at this time. Please try again later or contact USA Technologies, Inc.');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.schedule.success','Successfully scheduled upload of file to {0}');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.error.invalid.filetype','The file type you specified ({0}) is invalid');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.error.toobig','The file you selected exceeds the size limit of {0} bytes');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.error.nodevices','You did not select any devices to which the file should be uploaded. Please select at least one device.');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.schedule.notpermitted','User ''''{1}'''' is not permitted to upload files to {0}');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.error.conversion','Could not schedule the file upload at this time. Please try again later or contact USA Technologies, Inc.');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY,LITERAL_VALUE)
VALUES('device.remoteupload.error.http','Could not schedule the file upload at this time. Please try again later or contact USA Technologies, Inc.');

COMMIT;

/*
SELECT * FROM web_content.requirement;
*/

INSERT INTO web_content.requirement
(REQUIREMENT_ID,REQUIREMENT_NAME,REQUIREMENT_DESCRIPTION,REQUIREMENT_CLASS,REQUIREMENT_PARAM_CLASS,REQUIREMENT_PARAM_VALUE)
VALUES
(30,'REQ_SONY',NULL,'com.usatech.usalive.link.SubdomainRequirement','java.lang.String','(sony\..*)')
/
INSERT INTO web_content.requirement
(REQUIREMENT_ID,REQUIREMENT_NAME,REQUIREMENT_DESCRIPTION,REQUIREMENT_CLASS,REQUIREMENT_PARAM_CLASS,REQUIREMENT_PARAM_VALUE)
VALUES
(31,'REQ_NOT_SONY',NULL,'com.usatech.usalive.link.SubdomainRequirement','java.lang.String','!(sony\..*)')
/
COMMIT;

INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID)
SELECT WEB_LINK_ID, 31 
FROM web_content.web_link
WHERE WEB_LINK_USAGE = 'M';

-------------NEW LINKS
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2100,'Login','./frame.i?content=i&requestHandler=login','Login to Customer Reporting','General','M',100)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2105,'Logout','./frame.i?content=i&requestHandler=logout','Logout','General','M',105)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2110,'Main Page','./frame.i?content=i&requestHandler=my','View your Main Page','General','M',110)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2125,'Saved Reports','./frame.i?content=i&requestHandler=activity_report_list','View a User Defined or Pre-Defined Sales Report','Reports','M',125)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2130,'Build a Report','./frame.i?content=i&requestHandler=tabs&usage=B','Create your own criteria for viewing Sales Information','Reports','M',130)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2135,'Alerts','./frame.i?content=sony_alerts_frame.i','View machine alerts','Reports','M',120)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2140,'Payment Detail','./frame.i?content=i&requestHandler=payments','View Details of Payment(s) by EFT or Check','Reports','M',140)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2145,'Machines','./frame.i?content=i&requestHandler=terminals','View/Change Info on Existing Picture Maker(s)','Administration','M',145)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2150,'Users','./frame.i?content=i&requestHandler=user_admin','Add, Update, Delete Users','Administration','M',150)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2151,'Users','./frame.i?content=i&requestHandler=user_admin','Update Your Profile','Administration','M',151)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2200,'Sales Summary','./frame.i?content=select_date_range_frame.i&folioId=428','View Sales Summary','Reports','M',118)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2205,'Prints Remaining','./frame.i?content=sony_printer_info.i','View Prints Remaining','Reports','M',125)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2210,'Remote File Update','./frame.i?content=remote_file_update.i','Upload a new file to your devices','Administration','M',155)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2215,'Alert Register','./frame.i?content=sony_alert_register.i','Register to receive notification when an alert occurs','Administration','M',165)
/

COMMIT;

/*
select * 
from web_content.web_link wl1 
inner join web_content.web_link wl2 on wl1.web_link_id + 2000 = wl2.web_link_id
where wl1.web_link_
*/
INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID)
SELECT WEB_LINK_ID + 2000, decode(REQUIREMENT_ID, 31, 30, REQUIREMENT_ID)
FROM web_content.web_link_requirement
WHERE WEB_LINK_ID IN(
 100,
105,
110, -- different
125,
130,
135, -- Alerts
140,
145,
150,
151);
/*
INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID)
SELECT WEB_LINK_ID + 2000, 30 
FROM web_content.web_link
WHERE WEB_LINK_ID IN(
 100,
105,
110, -- different
125,
130,
135, -- Alerts
140,
145,
150,
151);
*/
INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID)
SELECT WEB_LINK_ID, 30 
FROM web_content.web_link
WHERE WEB_LINK_ID IN(2200,2205,2210,2215);

INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID)
SELECT WEB_LINK_ID, 1
FROM web_content.web_link
WHERE WEB_LINK_ID IN(2200,2205,2210,2215);

/*
SELECT * FROM web_content.requirement
SELECT * FROM report.priv
*/
INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID)
SELECT WEB_LINK_ID, 8
FROM web_content.web_link
WHERE WEB_LINK_ID IN(2210);


COMMIT;
/*
SELECT * FROM web_content.web_link WHERE web_link_usage = 'M' AND web_link_id BETWEEN 2000 AND 3000;

SELECT * FROM web_content.web_link_requirement WHERE web_link_id = 155;

web_content.web_link_requirement

SELECT * FROM report.message

*/

SELECT L.WEB_LINK_ID, L.WEB_LINK_LABEL, L.WEB_LINK_URL, 'M' USAGE, L.WEB_LINK_DESC, L.WEB_LINK_GROUP, L.WEB_LINK_ORDER
		  FROM WEB_CONTENT.WEB_LINK L, (
			SELECT LINK_ID, USAGE, INCLUDE 
			  FROM report.VW_CUSTOM_LINKS
		     WHERE USER_ID = 7 AND USAGE = 'M'
            ) AL,
            (SELECT WEB_LINK_ID, COUNT(REQUIREMENT_ID) AS REQS FROM 
                WEB_CONTENT.WEB_LINK_REQUIREMENT GROUP BY WEB_LINK_ID) ALL_REQ,
            (SELECT WEB_LINK_ID, COUNT(REQUIREMENT_ID) AS REQS FROM 
                WEB_CONTENT.WEB_LINK_REQUIREMENT 
                WHERE REQUIREMENT_ID MEMBER OF report.id_list(1,7,30,11,3,9,8,4,6)
                GROUP BY WEB_LINK_ID) SAT_REQ
        WHERE L.WEB_LINK_ID = AL.LINK_ID (+)
          AND 'M' IN(L.WEB_LINK_USAGE, AL.USAGE)
          AND NVL(AL.INCLUDE, 'Y') = 'Y'
		  AND L.WEB_LINK_ID = ALL_REQ.WEB_LINK_ID (+)
		  AND L.WEB_LINK_ID = SAT_REQ.WEB_LINK_ID (+)
		  AND NVL(ALL_REQ.REQS,0) = NVL(SAT_REQ.REQS,0)
        ORDER BY L.WEB_LINK_ORDER ASC, UPPER(L.WEB_LINK_GROUP) ASC, UPPER(L.WEB_LINK_LABEL) ASC;

