CREATE OR REPLACE FUNCTION WEB_CONTENT.UPSERT_COTE(
    pv_cote_file_name WEB_CONTENT.COTE.COTE_FILE_NAME%TYPE,
    pv_cote_content_type WEB_CONTENT.COTE.COTE_CONTENT_TYPE%TYPE,
    pv_subdomain_url WEB_CONTENT.SUBDOMAIN.SUBDOMAIN_URL%TYPE,
    pl_cote_content WEB_CONTENT.COTE.COTE_CONTENT%TYPE)
    RETURN WEB_CONTENT.COTE.COTE_ID%TYPE
AS
    ln_cote_id WEB_CONTENT.COTE.COTE_ID%TYPE;
    ln_subdomain_id WEB_CONTENT.SUBDOMAIN.SUBDOMAIN_ID%TYPE;
BEGIN
    SELECT SUBDOMAIN_ID
      INTO ln_subdomain_id
      FROM WEB_CONTENT.SUBDOMAIN
     WHERE SUBDOMAIN_URL = pv_subdomain_url;
    SELECT MAX(COTE_ID)
      INTO ln_cote_id
      FROM WEB_CONTENT.COTE
     WHERE COTE_FILE_NAME = pv_cote_file_name
       AND SUBDOMAIN_ID = ln_subdomain_id;
    IF ln_cote_id IS NULL THEN
        SELECT WEB_CONTENT.SEQ_COTE_ID.NEXTVAL
          INTO ln_cote_id
          FROM DUAL;
        INSERT INTO WEB_CONTENT.COTE(COTE_ID, COTE_FILE_NAME, SUBDOMAIN_ID, COTE_CONTENT_TYPE, COTE_CONTENT)
            VALUES(ln_cote_id, pv_cote_file_name, ln_subdomain_id, pv_cote_content_type, pl_cote_content);
    ELSE
        UPDATE WEB_CONTENT.COTE
           SET COTE_CONTENT_TYPE = pv_cote_content_type,
               COTE_CONTENT = pl_cote_content
         WHERE COTE_ID = ln_cote_id;
    END IF;
    UPDATE WEB_CONTENT.COTE
       SET COTE_LENGTH = DBMS_LOB.GETLENGTH(COTE_CONTENT)
     WHERE COTE_ID = ln_cote_id;
    RETURN ln_cote_id;
END;
/

    