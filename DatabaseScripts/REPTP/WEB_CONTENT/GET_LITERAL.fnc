CREATE OR REPLACE FUNCTION WEB_CONTENT.GET_LITERAL(
    pv_literal_key WEB_CONTENT.LITERAL.LITERAL_KEY%TYPE,
    pv_subdomain_url WEB_CONTENT.SUBDOMAIN.SUBDOMAIN_URL%TYPE,
    pv_locale_cd WEB_CONTENT.LITERAL.LOCALE_CD%TYPE)
    RETURN WEB_CONTENT.LITERAL.LITERAL_VALUE%TYPE
IS
    lv_literal_value WEB_CONTENT.LITERAL.LITERAL_VALUE%TYPE;
BEGIN
    SELECT MAX(L.LITERAL_VALUE)
      INTO lv_literal_value
      FROM WEB_CONTENT.LITERAL L, WEB_CONTENT.SUBDOMAIN S
     WHERE L.SUBDOMAIN_ID = S.SUBDOMAIN_ID
       AND L.LOCALE_CD = pv_locale_cd
       AND S.SUBDOMAIN_URL = pv_subdomain_url
       AND L.LITERAL_KEY = pv_literal_key;
       
     IF lv_literal_value IS NOT NULL THEN
        RETURN lv_literal_value;
     END IF;
   
    SELECT MAX(L.LITERAL_VALUE)
      INTO lv_literal_value
      FROM WEB_CONTENT.LITERAL L, WEB_CONTENT.SUBDOMAIN S
     WHERE L.SUBDOMAIN_ID = S.SUBDOMAIN_ID
       AND L.LOCALE_CD IS NULL
       AND S.SUBDOMAIN_URL = pv_subdomain_url
       AND L.LITERAL_KEY = pv_literal_key;
           
     IF lv_literal_value IS NOT NULL THEN
        RETURN lv_literal_value;
     END IF;
   
	SELECT MAX(L.LITERAL_VALUE)
      INTO lv_literal_value
      FROM WEB_CONTENT.LITERAL L, WEB_CONTENT.SUBDOMAIN S
     WHERE L.SUBDOMAIN_ID = S.SUBDOMAIN_ID
       AND L.LOCALE_CD = pv_locale_cd
       AND S.SUBDOMAIN_URL IS NULL
       AND L.LITERAL_KEY = pv_literal_key;
       
     IF lv_literal_value IS NOT NULL THEN
        RETURN lv_literal_value;
     END IF;
   
    SELECT MAX(L.LITERAL_VALUE)
      INTO lv_literal_value
      FROM WEB_CONTENT.LITERAL L, WEB_CONTENT.SUBDOMAIN S
     WHERE L.SUBDOMAIN_ID = S.SUBDOMAIN_ID
       AND L.LOCALE_CD IS NULL
       AND S.SUBDOMAIN_URL IS NULL
       AND L.LITERAL_KEY = pv_literal_key;
       
     RETURN lv_literal_value;
END;
/

    