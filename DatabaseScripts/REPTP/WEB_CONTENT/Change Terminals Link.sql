UPDATE web_content.web_link SET web_link_usage = 'm'
WHERE WEB_LINK_ID IN(145,2145);

INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(146,'Terminals','./frame.i?content=terminal_customers.i&bannerImage=/images/banner_terminals.gif','View/Change Info on Existing Terminal(s) or Add a New Terminal','Administration','M',145)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(2146,'Machines','./frame.i?content=terminal_customers.i','View/Change Info on Existing Picture Maker(s)','Administration','M',145)
/

INSERT INTO web_content.web_link_requirement(web_link_id, requirement_id)
SELECT 146, requirement_id
from web_content.web_link_requirement
WHERE WEB_LINK_ID = 145;

INSERT INTO web_content.web_link_requirement(web_link_id, requirement_id)
SELECT 2146, requirement_id
from web_content.web_link_requirement
WHERE WEB_LINK_ID = 2145;
