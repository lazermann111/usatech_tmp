INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER, WEB_LINK_URL)
SELECT link_id,title,description,category,usage,seq, './i?'
  FROM REPORT.link ;

DECLARE
    CURSOR l_cur IS SELECT link_id, param_name, param_value FROM REPORT.link_param;
    l_str WEB_CONTENT.WEB_LINK.WEB_LINK_URL%TYPE;
BEGIN
    FOR l_rec IN l_cur LOOP
        l_str := l_rec.param_name || '=' || UTL_URL.ESCAPE(l_rec.param_value, TRUE);
        UPDATE WEB_CONTENT.WEB_LINK 
           SET WEB_LINK_URL = WEB_LINK_URL || l_str || '&'
         WHERE WEB_LINK_ID = l_rec.link_id;
    END LOOP;
END;
/
UPDATE WEB_CONTENT.WEB_LINK 
   SET WEB_LINK_URL = SUBSTR(WEB_LINK_URL, 1, LENGTH(WEB_LINK_URL) - 1)
  WHERE SUBSTR(WEB_LINK_URL, LENGTH(WEB_LINK_URL), 1) = '&';
   
COMMIT;

DECLARE
	l_start NUMBER;
BEGIN
	SELECT MAX(WEB_LINK_ID) + 1
	  INTO l_start
	  FROM WEB_CONTENT.web_link;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE WEB_CONTENT.SEQ_WEB_LINK_ID START WITH ' || TO_CHAR(l_start);
END;
/

INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL,'Refund Summary Report','./report_selection.i?folioId=311&searchAction=report_search','Customer Service Reports','U',5 FROM DUAL);
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.seq_web_link_id.NEXTVAL,'Revenue Summary Report','./report_selection.i?folioId=321&searchAction=report_search','Customer Service Reports','U',5 FROM DUAL);
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.seq_web_link_id.NEXTVAL,'Settlement Summary Report','./report_selection.i?folioId=309&searchAction=report_search','Customer Service Reports','U',5 FROM DUAL);
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.seq_web_link_id.NEXTVAL,'Pass-Access-Debit Summary Report','./report_selection.i?folioId=310&searchAction=report_search','Customer Service Reports','U',5 FROM DUAL);
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.seq_web_link_id.NEXTVAL,'Revenue Summary Under $50 Credit','./report_selection.i?folioId=341&searchAction=report_search','Customer Service Reports','U',5 FROM DUAL);
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.seq_web_link_id.NEXTVAL,'Revenue Fee Summary Report','./report_selection.i?folioId=342&searchAction=fee_search','Customer Service Reports','U',5 FROM DUAL);
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    (SELECT WEB_CONTENT.seq_web_link_id.NEXTVAL,'Credit Card Summary Report','./report_selection.i?folioId=221&searchAction=report_search','Customer Service Reports','U',5 FROM DUAL);

COMMIT;
