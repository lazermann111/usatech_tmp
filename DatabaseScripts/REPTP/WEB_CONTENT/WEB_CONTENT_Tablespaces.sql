CREATE TABLESPACE WEB_CONTENT_DATA
  DATAFILE '/u03/oradata/usardb01/web_content_data1.dbf'
  SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u03/oradata/usardb01/web_content_data1.dbf' AUTOEXTEND ON;
  
CREATE TABLESPACE WEB_CONTENT_INDX
  DATAFILE '/u02/oradata/usardb01/web_content_idx1.dbf'
  SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u02/oradata/usardb01/web_content_idx1.dbf' AUTOEXTEND ON;
  

