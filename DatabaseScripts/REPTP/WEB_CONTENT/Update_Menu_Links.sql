UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_login.gif&requestHandler=login' WHERE WEB_LINK_ID = 100;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './logout.i' WHERE WEB_LINK_ID = 105;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_menu.gif&requestHandler=my' WHERE WEB_LINK_ID = 110;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=contact' WHERE WEB_LINK_ID = 120;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_activity.gif&requestHandler=activity_report_list' WHERE WEB_LINK_ID = 125;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_activity.gif&requestHandler=tabs&usage=B' WHERE WEB_LINK_ID = 130;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_diagnostics.gif&requestHandler=diagnostics' WHERE WEB_LINK_ID = 135;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_payment.gif&requestHandler=payments' WHERE WEB_LINK_ID = 140;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_terminals.gif&requestHandler=terminals' WHERE WEB_LINK_ID = 145;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=user_admin.i' WHERE WEB_LINK_ID = 150;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=user_admin.i' WHERE WEB_LINK_ID = 151;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=regions' WHERE WEB_LINK_ID = 165;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=customer_admin' WHERE WEB_LINK_ID = 170;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_terminals.gif&requestHandler=new_terminal' WHERE WEB_LINK_ID = 175;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_payment.gif&requestHandler=new_bank_acct' WHERE WEB_LINK_ID = 180;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=license_instructions' WHERE WEB_LINK_ID = 185;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=eft_auth_instructions' WHERE WEB_LINK_ID = 190;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=tabs&usage=N' WHERE WEB_LINK_ID = 200;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=preferences' WHERE WEB_LINK_ID = 205;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_USAGE = 'm' WHERE WEB_LINK_ID = 200;

COMMIT;

/*
SELECT wl.WEB_LINK_URL, wl0.WEB_LINK_URL, 'UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = ''' || wl.WEB_LINK_URL || ''' WHERE WEB_LINK_ID = ' || wl.WEB_LINK_ID || ';'
from WEB_CONTENT.WEB_LINK@usadbd02 wl, WEB_CONTENT.WEB_LINK wl0
where wl.WEB_LINK_USAGE = 'M'
AND wl.WEB_LINK_ID = wl0.WEB_LINK_ID
AND wl.WEB_LINK_URL <> wl0.WEB_LINK_URL;


SELECT 'INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,' 
|| ' WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)' || CHR(13)
|| 'SELECT '|| wl.WEB_LINK_ID || ', WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER '
|| 'FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_ID = ' || wld.web_link_id || ';'
 || CHR(13) || 'UPDATE WEB_CONTENT.WEB_LINK_REQUIREMENT SET WEB_LINK_ID = ' 
 || wl.WEB_LINK_ID || ' WHERE WEB_LINK_ID = ' || wld.web_link_id || ';'
 || CHR(13) || 'UPDATE REPORT.USER_LINK SET LINK_ID = ' 
 || wl.WEB_LINK_ID || ' WHERE LINK_ID = ' || wld.web_link_id || ';',wl.*, wld.*
from WEB_CONTENT.WEB_LINK wl
left outer JOIN WEB_CONTENT.WEB_LINK@usadbd02 wld
ON nvl(wl.WEB_LINK_LABEL, '-') = nvl(wld.web_link_label, '-')
WHERE wl.WEB_LINK_ID <> wld.web_link_id
and nvl(wl.WEB_LINK_USAGE, 'M') = 'M' 
AND nvl(wld.WEB_LINK_USAGE, 'M') = 'M'
AND nvl(wld.WEB_LINK_ID, 0) < 2100
AND wl.WEB_LINK_ID IN(1316,1341,1317,1315)
;

SELECT wl.*
from WEB_CONTENT.WEB_LINK wl
where wl.WEB_LINK_USAGE = 'M'
and wl.web_link_id < 2100
wl.web_link_id IN(1315,1316,1317,1341)
;
SELECT * FROM dba_db_links

DROP public DATABASE LINK PROD_CORP.WORLD;

SELECT wl.*
from WEB_CONTENT.WEB_LINK  wl
where wl.WEB_LINK_label = 'Reports'
 
 SELECT * 
 FROM WEB_CONTENT.WEB_LINK_requirement wlr, WEB_CONTENT.requirement  r
 WHERE web_link_id = 200
 AND wlr.REQUIREMENT_ID = r.REQUIREMENT_ID;
 INSERT INTO WEB_CONTENT.WEB_LINK@usadbd02
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(100,'Login','./frame.i?content=i&bannerImage=/images/banner_login.gif&requestHandler=login','Login to Customer Reporting','General','M',100)
/
--*/
