ALTER SESSION SET CURRENT_SCHEMA = SYSTEM;
DECLARE
    CURSOR l_cur IS 
        SELECT column_value OLD_REPORT_ID, REPORT.REPORTS_SEQ.NEXTVAL NEW_REPORT_ID FROM TABLE(NUMBER_TABLE(2,3,4,9,84));
BEGIN 
    FOR l_rec IN l_cur LOOP
        INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, UPD_DT, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, CATEGORY)
            SELECT l_rec.NEW_REPORT_ID, TITLE, 6, SYSDATE, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, CATEGORY
              FROM REPORT.REPORTS
             WHERE REPORT_ID = l_rec.OLD_REPORT_ID;
        UPDATE REPORT.REPORTS
           SET REPORT_NAME = REPORT_NAME || ' - Deprecated',
               USAGE = 'D'
         WHERE REPORT_ID = l_rec.OLD_REPORT_ID;
        DELETE FROM REPORT.REPORT_PARAM WHERE REPORT_ID = l_rec.NEW_REPORT_ID; -- since this is no foreign key constraint
        INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
            SELECT l_rec.NEW_REPORT_ID, PARAM_NAME, PARAM_VALUE
              FROM (SELECT 0 REPORT_ID, '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
                    UNION ALL SELECT 2, 'reportId', '2' FROM DUAL 
                    UNION ALL SELECT 2, 'params.DocId', '{x}' FROM DUAL
                    UNION ALL SELECT 3, 'folioId', '938' FROM DUAL
                    UNION ALL SELECT 3, 'params.beginDate', '{b}' FROM DUAL
                    UNION ALL SELECT 3, 'params.endDate', '{e}' FROM DUAL
                    UNION ALL SELECT 4, 'folioId', '286' FROM DUAL
                    UNION ALL SELECT 4, 'params.DocId', '{x}' FROM DUAL
                    UNION ALL SELECT 9, 'folioId', '930' FROM DUAL
                    UNION ALL SELECT 9, 'params.ExportId', '{x}' FROM DUAL
                    UNION ALL SELECT 84, 'folioId', '936' FROM DUAL
                    UNION ALL SELECT 84, 'params.beginDate', '{b} -7d' FROM DUAL
                    UNION ALL SELECT 84, 'params.endDate', '{e}' FROM DUAL)
            WHERE REPORT_ID = l_rec.OLD_REPORT_ID;
        UPDATE REPORT.USER_REPORT
           SET REPORT_ID = l_rec.NEW_REPORT_ID
         WHERE REPORT_ID = l_rec.OLD_REPORT_ID;
    END LOOP;
    COMMIT;
END;
/
