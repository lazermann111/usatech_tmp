set serveroutput on

DECLARE
  l_create VARCHAR2(2000);
BEGIN
  DBMS_OUTPUT.ENABLE(1000000);
  -- for field
  FOR l_rec in (select distinct ancestor_field_category_id from FOLIO_CONF.vw_field_category_hierarchy where descendent_field_category_id in (
select distinct field_category_id from folio_conf.field where field_id in (select distinct field_id from (
select distinct field_id from folio_conf.folio_pillar_field, folio_conf.folio_pillar
        WHERE folio_conf.folio_pillar_field.folio_pillar_id=folio_conf.folio_pillar.folio_pillar_id
            AND folio_conf.folio_pillar.folio_id  in (select folio_id from folio_conf.folio where folio_name like 'RDW%')
union        
select distinct field_id from folio_conf.filter where filter_group_id in 
(select ANCESTOR_FILTER_GROUP_ID from folio_conf.vw_filter_group_hierarchy where ANCESTOR_FILTER_GROUP_ID in (
select filter_group_id from folio_conf.folio where folio_id in 
(select folio_id from folio_conf.folio where folio_name like 'RDW%')
))
))) order by ancestor_field_category_id)
LOOP
  select 'delete from folio_conf.field_priv where field_id in (select field_id from folio_conf.field where field_category_id='||l_rec.ancestor_field_category_id||');'||chr(13)
||'delete from folio_conf.filter_param where filter_id in (select filter_id from folio_conf.filter where field_id in (select field_id from folio_conf.field where field_category_id='||l_rec.ancestor_field_category_id||'));'||chr(13)
||'delete from folio_conf.filter where field_id in (select field_id from folio_conf.field where field_category_id='||l_rec.ancestor_field_category_id||');'||chr(13)
||'delete from folio_conf.field where field_category_id='||l_rec.ancestor_field_category_id||';'||chr(13)
  into l_create from dual;
  dbms_output.put_line(l_create);
END LOOP;

-- for field
  FOR l_rec in (select distinct ancestor_field_category_id from FOLIO_CONF.vw_field_category_hierarchy where descendent_field_category_id in (
select distinct field_category_id from folio_conf.field where field_id in (select distinct field_id from (
select distinct field_id from folio_conf.folio_pillar_field, folio_conf.folio_pillar
        WHERE folio_conf.folio_pillar_field.folio_pillar_id=folio_conf.folio_pillar.folio_pillar_id
            AND folio_conf.folio_pillar.folio_id  in (select folio_id from folio_conf.folio where folio_name like 'RDW%')
union        
select distinct field_id from folio_conf.filter where filter_group_id in 
(select ANCESTOR_FILTER_GROUP_ID from folio_conf.vw_filter_group_hierarchy where ANCESTOR_FILTER_GROUP_ID in (
select filter_group_id from folio_conf.folio where folio_id in 
(select folio_id from folio_conf.folio where folio_name like 'RDW%')
))
))) order by ancestor_field_category_id)
LOOP
  select 'delete from folio_conf.field_category where field_category_id='||l_rec.ancestor_field_category_id||';' 
  into l_create from dual;
  dbms_output.put_line(l_create);
END LOOP;

-- for join_filter
FOR l_rec in (select * from folio_conf.join_filter where from_table like 'MAIN.RDW%')
LOOP
  select 'delete from folio_conf.join_filter where join_filter_id='||l_rec.join_filter_id||';'
  into l_create
  from dual;
  dbms_output.put_line(l_create);
END LOOP;

END;