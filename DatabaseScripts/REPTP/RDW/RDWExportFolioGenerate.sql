set serveroutput on

DECLARE
  l_create VARCHAR2(2000);
BEGIN
  DBMS_OUTPUT.ENABLE(1000000);
  -- for field_category
  FOR l_rec in (select distinct ancestor_field_category_id from FOLIO_CONF.vw_field_category_hierarchy where descendent_field_category_id in (
select distinct field_category_id from folio_conf.field where field_id in (select distinct field_id from (
select distinct field_id from FOLIO_CONF.folio_pillar_field, FOLIO_CONF.folio_pillar
        WHERE folio_pillar_field.folio_pillar_id=folio_pillar.folio_pillar_id
            AND folio_pillar.folio_id  in (select folio_id from folio_conf.folio where folio_name like 'RDW%')
union        
select distinct field_id from folio_conf.filter where filter_group_id in 
(select DESCENDENT_FILTER_GROUP_ID from folio_conf.vw_filter_group_hierarchy where ANCESTOR_FILTER_GROUP_ID in (
select filter_group_id from folio_conf.folio where folio_id in 
(select folio_id from folio_conf.folio where folio_name like 'RDW%')
))
))) order by ancestor_field_category_id)
LOOP
  select 'insert into folio_conf.field_category (field_category_id, field_category_label) values('||field_category_id||', '''||field_category_label||''');' 
  into l_create
  from folio_conf.field_category where field_category_id=l_rec.ancestor_field_category_id;
  dbms_output.put_line(l_create);
END LOOP;

-- for field and field priv
  FOR l_rec in (select distinct field_id from (
select field_id from FOLIO_CONF.field WHERE DISPLAY_EXPRESSION = 'MAIN.RDW.USER_LOCATION_PRIV.USER_ID' or DISPLAY_EXPRESSION = 'MAIN.RDW.USER_BANK_ACCT_PRIV.USER_ID'
union
select distinct field_id from FOLIO_CONF.folio_pillar_field, FOLIO_CONF.folio_pillar
        WHERE folio_pillar_field.folio_pillar_id=folio_pillar.folio_pillar_id
            AND folio_pillar.folio_id  in (select folio_id from folio_conf.folio where folio_name like 'RDW%')
union        
select distinct field_id from folio_conf.filter where filter_group_id in 
(select DESCENDENT_FILTER_GROUP_ID from folio_conf.vw_filter_group_hierarchy where ANCESTOR_FILTER_GROUP_ID in (
select filter_group_id from folio_conf.folio where folio_id in 
(select folio_id from folio_conf.folio where folio_name like 'RDW%')
))
) order by field_id)
LOOP
  SELECT 'INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)'
|| chr(13) || ' VALUES(' 
|| FIELD_ID || ', ''' 
|| REPLACE(FIELD_LABEL, '''', '''''') || ''','''
|| REPLACE(FIELD_DESC, '''', '''''') || ''','''
|| REPLACE(DISPLAY_EXPRESSION, '''', '''''') || ''','''
|| REPLACE(SORT_EXPRESSION, '''', '''''') || ''','''
|| REPLACE(DISPLAY_FORMAT, '''', '''''') || ''','''
|| REPLACE(DISPLAY_SQL_TYPE, '''', '''''') || ''','''
|| REPLACE(SORT_SQL_TYPE, '''', '''''') || ''','
|| IMPORTANCE || ','
|| FIELD_CATEGORY_ID || ','''
|| REPLACE(ACTIVE_FLAG, '''', '''''') || ''');' into l_create
FROM FOLIO_CONF.FIELD where field_id=l_rec.field_id;
dbms_output.put_line(l_create);
END LOOP;

-- for join_filter
FOR l_rec in (select * from folio_conf.join_filter where from_table like 'MAIN.RDW%')
LOOP
  select 'insert into folio_conf.join_filter (join_filter_id, from_table,to_table, join_expression, join_type, active_flag, join_cardinality) values('
  ||l_rec.join_filter_id||', '''||l_rec.from_table||''', '''||l_rec.to_table||''', '''||REPLACE(l_rec.join_expression, '''', '''''')||''', '''||l_rec.join_type||''', '''||l_rec.active_flag||''', '''||l_rec.join_cardinality||''');' 
  into l_create
  from dual;
  dbms_output.put_line(l_create);
END LOOP;

END;