Transaction Processing Entry Summary

SELECT 
		led.entry_type,
		NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name,
		NVL(dt.monthly_payment, 'UNK') monthly_payment,
		cr.currency_code,
		NVL(dt.customer_name, '~ Terminal Orphan') customer_name,
		TO_CHAR(TRUNC(DECODE (led.entry_type,
		    'CC', tr.settle_date,
		    led.ledger_date
		), 'MONTH'), 'mm/dd/yyyy') month_for,
		COUNT(1) tran_count,
		SUM(tr.total_amount) total_tran_amount,
		SUM(led.amount) total_ledger_amount
		FROM report.trans tr
		JOIN corp.currency cr
		ON cr.currency_id = tr.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = tr.settle_state_id 
		LEFT OUTER JOIN (
			SELECT
			t.terminal_id,
			c.customer_name,
			bu.business_unit_name,
			DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment
			FROM report.terminal t
			JOIN corp.customer c
			ON c.customer_id = t.customer_id
			JOIN corp.business_unit bu
			ON bu.business_unit_id = t.business_unit_id
		) dt
		ON dt.terminal_id = tr.terminal_id
		JOIN corp.ledger led
		ON led.trans_id = tr.tran_id
		AND led.deleted = 'N'
		WHERE tr.trans_type_id IN (14, 16, 19, 20, 21) 
		AND tr.settle_state_id IN (2, 3) 
		AND led.entry_type IN ('CC', 'PF', 'CB', 'RF') 
		AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
		--AND NVL(tr.settle_date, led.create_date) >= CAST('01-Jan-2013' AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST('31-Mar-2013' AS DATE) + 1
		GROUP BY
		led.entry_type,
		NVL(dt.business_unit_name, '~ Terminal Orphan'),
		NVL(dt.monthly_payment, 'UNK'),
		cr.currency_code,
		NVL(dt.customer_name, '~ Terminal Orphan'),
		TRUNC(DECODE (led.entry_type,
		    'CC', tr.settle_date,
		    led.ledger_date
		), 'MONTH')

miss: resolved. tr.trans_type_id IN (14, 16, 19, 20, 21) filter, need to add it to lidd

miss: resolved
confirm that when led.entry_type is CC use report.trans.settle_Date, in LEDGER_ITEM_FACT.SETTLED_DATE_DIM_ID is using L.LEDGER_DATE, temp use tidd.SETTLED_TS(SETTLED_DATE_DIM_ID to get ACTUAL_MONTH) note: confirmed that always use tidd.SETTLED_TS because when tr.settle_state_id IN (2, 3) , it will have value and SETTLED_TS = lidd.SETTLED_TS( it is using LEDGER_DATE and it is correct, when insert it is the same as create_date, update it to settle when settled)
miss filter AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1

note always use settle_date because (2,3)

led.entry_type,MAIN.RDW.ITEM_TYPE_DIM.PAYMENT_ENTRY_TYPE_CD
		NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name, CASE WHEN pd.BUSINESS_UNIT is NULL THEN '~ Terminal Orphan' ELSE pd.BUSINESS_UNIT END
		NVL(dt.monthly_payment, 'UNK') monthly_payment,CASE WHEN pbd.BATCH_TYPE ='Monthly' THEN 'Y' ELSE 'N' END as monthly_payment,
		cr.currency_code,MAIN.RDW.CURRENCY_DIM.CURRENCY_CODE
		NVL(dt.customer_name, '~ Terminal Orphan') customer_name,CASE WHEN pd.CUSTOMER_NAME is NULL THEN '~ Terminal Orphan' ELSE pd.CUSTOMER_NAME END
		TO_CHAR(TRUNC(DECODE (led.entry_type,
		    'CC', tr.settle_date,
		    led.ledger_date
		), 'MONTH'), 'mm/dd/yyyy') month_for,CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD = 'CC' THEN TO_CHAR(date_trunc('month', tidd.SETTLED_TS),'mm/dd/yyyy') ELSE dd.ACTUAL_MONTH END
		COUNT(1) tran_count,
		SUM(tr.total_amount) total_tran_amount, sum(tif.amount)
		SUM(led.amount) total_ledger_amount, sum(lif.amount)
------------------------------------------------

select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
CASE WHEN pd.BUSINESS_UNIT is NULL THEN '~ Terminal Orphan' ELSE pd.BUSINESS_UNIT END as business_unit_name,
CASE WHEN pbd.BATCH_TYPE ='Monthly' THEN 'Y' ELSE 'N' END as monthly_payment,
cd.CURRENCY_CODE,
CASE WHEN pd.CUSTOMER_NAME is NULL THEN '~ Terminal Orphan' ELSE pd.CUSTOMER_NAME END as customer_name,
TO_CHAR(dd.actual_month,'mm/dd/yyyy') as month_for,
COUNT(1)  as tran_count,
round(sum(tidd.total_amount),2) as total_tran_amount,
round(sum(lif.amount),2) as total_ledger_amount
from RDW.LEDGER_ITEM_FACT lif JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
    LEFT OUTER JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd on lif.TRAN_ITEM_DETAIL_DIM_ID=tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.ITEM_TYPE_DIM itd ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.CURRENCY_DIM cd ON lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.DATE_DIM dd on lif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
where lidd.ITEM_PAYABLE_FLAG='Yes'
and lidd.tran_type_id in (14, 16, 19, 20, 21)
and itd.PAYMENT_ENTRY_TYPE_CD IN ('CC', 'PF', 'CB', 'RF') 
and dd.ACTUAL_DATE >= cast('01-Jan-2013' as DATE) and dd.ACTUAL_DATE < cast('31-Mar-2013' as DATE)+ interval '1 day'
group by
itd.PAYMENT_ENTRY_TYPE_CD,
CASE WHEN pd.BUSINESS_UNIT is NULL THEN '~ Terminal Orphan' ELSE pd.BUSINESS_UNIT END,
CASE WHEN pbd.BATCH_TYPE ='Monthly' THEN 'Y' ELSE 'N' END,
cd.CURRENCY_CODE,
CASE WHEN pd.CUSTOMER_NAME is NULL THEN '~ Terminal Orphan' ELSE pd.CUSTOMER_NAME END,
dd.actual_month


select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
CASE WHEN pd.BUSINESS_UNIT is NULL THEN '~ Terminal Orphan' ELSE pd.BUSINESS_UNIT END as business_unit_name,
CASE WHEN pbd.BATCH_TYPE ='Monthly' THEN 'Y' ELSE 'N' END as monthly_payment,
cd.CURRENCY_CODE,
CASE WHEN pd.CUSTOMER_NAME is NULL THEN '~ Terminal Orphan' ELSE pd.CUSTOMER_NAME END as customer_name,
TO_CHAR(dd.actual_month,'mm/dd/yyyy') as month_for,
COUNT(1)  as tran_count,
round(sum(tidd.total_amount),2) as total_tran_amount,
round(sum(lif.amount),2) as total_ledger_amount
from RDW.LEDGER_ITEM_FACT lif JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
    LEFT OUTER JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd on lif.TRAN_ITEM_DETAIL_DIM_ID=tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.ITEM_TYPE_DIM itd ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.CURRENCY_DIM cd ON lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.DATE_DIM dd on lif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
where lidd.ITEM_PAYABLE_FLAG='Yes'
and lidd.tran_type_id in (14, 16, 19, 20, 21)
and itd.PAYMENT_ENTRY_TYPE_CD IN ('CC', 'PF', 'CB', 'RF') 
and dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? as DATE)+ interval '1 day'
group by
itd.PAYMENT_ENTRY_TYPE_CD,
CASE WHEN pd.BUSINESS_UNIT is NULL THEN '~ Terminal Orphan' ELSE pd.BUSINESS_UNIT END,
CASE WHEN pbd.BATCH_TYPE ='Monthly' THEN 'Y' ELSE 'N' END,
cd.CURRENCY_CODE,
CASE WHEN pd.CUSTOMER_NAME is NULL THEN '~ Terminal Orphan' ELSE pd.CUSTOMER_NAME END,
dd.actual_month

test: results almost identical except the new is missing RF records