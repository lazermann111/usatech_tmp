-- ======================================================================
-- ===   Sql Script for Database : Postgresql
-- ===
-- === Build : 808
-- ======================================================================


DROP SCHEMA IF EXISTS RDW CASCADE;

CREATE SCHEMA RDW
  AUTHORIZATION admin;

CREATE SEQUENCE rdw.item_fact_item_fact_id_seq;
CREATE SEQUENCE rdw.item_fact_item_detail_dim_id_seq;
CREATE SEQUENCE rdw.paid_item_agg_paid_item_agg_id_seq;

-- ======================================================================

CREATE OR REPLACE FUNCTION RDW.FTRBU_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_TS := OLD.CREATED_TS;
	NEW.CREATED_BY := OLD.CREATED_BY;
	NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;


-- ======================================================================

CREATE OR REPLACE FUNCTION RDW.FTRBI_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_TS := STATEMENT_TIMESTAMP();
	NEW.CREATED_BY := SESSION_USER;
	NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION RDW.CONSTRUCT_NAME(pv_base_name VARCHAR, pv_suffix VARCHAR)
	RETURNS VARCHAR
   IMMUTABLE LEAKPROOF
	SECURITY DEFINER
AS $$
BEGIN
	IF LENGTH(pv_base_name) + LENGTH(pv_suffix) <= 60 THEN
		RETURN pv_base_name || '_' || pv_suffix;
	ELSIF LENGTH(pv_base_name) > 51 AND LENGTH(pv_suffix) > 51 THEN
		RETURN SUBSTR(pv_base_name, 1, 21) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || SUBSTR(pv_suffix, 1, 21) || '$' || SUBSTR(MD5(pv_suffix), 1, 8);
	ELSIF LENGTH(pv_base_name) > 21 THEN
		RETURN SUBSTR(pv_base_name, 1, 51 - LENGTH(pv_suffix)) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || pv_suffix;
	ELSE
		RETURN pv_base_name || '_' || SUBSTR(pv_suffix, 1,  51 - LENGTH(pv_base_name)) || '_$' || SUBSTR(MD5(pv_suffix), 1, 8);
	END IF;
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION RDW.COPY_INDEXES(pv_source_table VARCHAR, pv_suffix VARCHAR)
    RETURNS SMALLINT
    SECURITY DEFINER
AS $$
DECLARE
    lv_sql TEXT;
    ln_cnt SMALLINT := 0;
BEGIN
    FOR lv_sql IN SELECT 'CREATE '||CASE WHEN x.indisunique THEN 'UNIQUE ' ELSE '' END ||'INDEX ' || RDW.CONSTRUCT_NAME(CAST(i.relname AS VARCHAR), pv_suffix) || ' ON '
            || CASE WHEN n.nspname IS NOT NULL AND n.nspname != 'pg_catalog' THEN n.nspname || '.' ELSE '' END || RDW.CONSTRUCT_NAME(CAST(c.relname AS VARCHAR), pv_suffix) || CASE WHEN a.amname IS NOT NULL AND a.amname != 'btree' THEN ' USING ' || a.amname || ' ' ELSE '' END || '('
            || (SELECT ARRAY_TO_STRING(ARRAY_AGG(t.col_desc), cast(', ' as text))
                  FROM (SELECT CASE WHEN b.attname IS NOT NULL THEN b.attname || CASE WHEN x.indoption[s.s-1] & 3 = 3 THEN ' DESC'  ELSE '' END WHEN x.indexprs IS NOT NULL THEN PG_GET_EXPR(x.indexprs, x.indrelid) ELSE '' END 
            || CASE WHEN o.collname IS NOT NULL AND o.collname != 'default' THEN ' COLLATE ' || o.collname ELSE '' END
            || CASE WHEN p.opcname IS NOT NULL AND y.typname || '_ops' != p.opcname THEN ' ' || p.opcname ELSE '' END col_desc
            FROM (
            SELECT generate_series(1,x.indnatts) s) s
            LEFT JOIN pg_attribute b on x.indrelid = b.attrelid and x.indkey[s.s-1] = b.attnum
            LEFT JOIN pg_type y on x.indclass[s.s-1] = y.oid
            LEFT JOIN pg_collation o on x.indcollation[s.s-1] = o.oid
            LEFT JOIN pg_opclass p on x.indclass[s.s-1] = p.oid AND NOT(p.opcintype = b.atttypid AND p.opcdefault)
            ORDER BY s.s) t)
            ||')' || CASE WHEN t.spcname IS NOT NULL THEN ' TABLESPACE ' || t.spcname ELSE '' END
            || CASE WHEN x.indpred IS NOT NULL THEN ' WHERE ' || x.indpred ELSE '' END
          FROM pg_index x 
          JOIN pg_class i ON i.oid = x.indexrelid 
          JOIN pg_class c ON c.oid = x.indrelid 
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          LEFT JOIN pg_tablespace t ON t.oid = i.reltablespace
          LEFT JOIN pg_am a ON i.relam = a.oid
          LEFT JOIN (pg_index ox JOIN pg_class oi ON oi.oid = ox.indexrelid JOIN pg_class oc ON oc.oid = ox.indrelid) ON RDW.CONSTRUCT_NAME(CAST(i.relname AS VARCHAR), pv_suffix) = oi.relname AND oc.relname = RDW.CONSTRUCT_NAME(CAST(c.relname AS VARCHAR), pv_suffix)
        WHERE c.relkind = 'r'
          AND i.relkind = 'i'
          AND c.relname = LOWER(pv_source_table)
          AND n.nspname = 'rdw' 
          AND NOT x.indisprimary
          AND oi.oid IS NULL
    LOOP
        EXECUTE lv_sql;
        ln_cnt := ln_cnt + 1;
    END LOOP;
   RETURN ln_cnt;     
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.COPY_INDEXES_TO_CHILDREN(pv_source_table VARCHAR)
    RETURNS SMALLINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_cnt SMALLINT := 0;
BEGIN
    SELECT SUM(RDW.COPY_INDEXES(pv_source_table, 
           CASE 
               WHEN LOWER(LEFT(c.relname, LENGTH(p.relname) + 1)) = LOWER(p.relname) || '_' THEN SUBSTR(c.relname, LENGTH(p.relname) + 2)
               WHEN c.relname ~ '.+$\d{8}_.+' THEN (REGEXP_MATCHES(c.relname, '.+$\d{8}_(.+)'))[0]
           END))
      INTO ln_cnt
      FROM pg_class p
      JOIN pg_inherits h ON p.oid = h.inhparent
      JOIN pg_class c ON h.inhrelid = c.oid
      LEFT JOIN pg_namespace n ON n.oid = c.relnamespace              
     WHERE p.relkind = 'r'
       AND p.relname = LOWER(pv_source_table)
       AND n.nspname = 'rdw';         
    RETURN ln_cnt;     
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION RDW.PARTITION_START_DATE_DIM_ID(pn_date_dim_id SMALLINT, pv_unit VARCHAR)
	RETURNS SMALLINT
   IMMUTABLE LEAKPROOF
	SECURITY DEFINER
AS $$
BEGIN
	RETURN DATE_PART('days', DATE_TRUNC(pv_unit, TIMESTAMP'epoch' + (pn_date_dim_id ||' days')::INTERVAL) - TIMESTAMP'epoch');
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION RDW.PARTITION_END_DATE_DIM_ID(pn_date_dim_id SMALLINT, pv_unit VARCHAR)
	RETURNS SMALLINT
   IMMUTABLE LEAKPROOF
	SECURITY DEFINER
AS $$
BEGIN
	RETURN DATE_PART('days', DATE_TRUNC(pv_unit, TIMESTAMP'epoch' + (pn_date_dim_id ||' days')::INTERVAL) + INTERVAL'1 month' - TIMESTAMP'epoch');
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.FTRBU_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_TS := OLD.CREATED_TS;
	NEW.CREATED_BY := OLD.CREATED_BY;
	NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION RDW.FTRBI_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_TS := STATEMENT_TIMESTAMP();
	NEW.CREATED_BY := SESSION_USER;
	NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END
$$ LANGUAGE plpgsql;


-- ======================================================================

CREATE TABLE RDW._
  (
    REVISION        BIGINT      NOT NULL DEFAULT 1,
    CREATED_TS      TIMESTAMPTZ NOT NULL,
    CREATED_BY      VARCHAR(60) NOT NULL DEFAULT SESSION_USER,
    LAST_UPDATED_TS TIMESTAMPTZ NOT NULL,
    LAST_UPDATED_BY VARCHAR(60) NOT NULL DEFAULT SESSION_USER
  )
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW._ TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW._ TO write_rdw;


CREATE OR REPLACE FUNCTION RDW.FTRBU_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_TS := OLD.CREATED_TS;
	NEW.CREATED_BY := OLD.CREATED_BY;
	NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION RDW.FTRBI_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_TS := STATEMENT_TIMESTAMP();
	NEW.CREATED_BY := SESSION_USER;
	NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS TRBI__ ON RDW._;


CREATE TRIGGER TRBI__
BEFORE INSERT ON RDW._
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU__ ON RDW._;


CREATE TRIGGER TRBU__
BEFORE UPDATE ON RDW._
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX___CREATED_TS ON RDW._(CREATED_TS) TABLESPACE rdw_data_t1;

-- ======================================================================

CREATE TABLE RDW._AUTH_DETAIL_DIM
  (
    AUTH_DETAIL_DIM_ID         BIGSERIAL     NOT NULL,
    SOURCE_AUTH_ID             BIGINT        NOT NULL,
    SOURCE_SYSTEM_CD           VARCHAR(10)   NOT NULL,
    SOURCE_TRAN_ID             BIGINT        NOT NULL,
    SOURCE_TRAN_STATE_CD       CHAR(1)       ,
    AUTH_TS                    TIMESTAMPTZ   ,
    RESPONSE_CD                VARCHAR(60)   ,
    RESPONSE_DESC              VARCHAR(4000) ,
    AUTHORITY_TRAN_CD          VARCHAR(60)   ,
    AUTHORITY_REF_CD           VARCHAR(255)  ,
    AUTHORITY_MISC_DATA        VARCHAR(4000) ,
    TERMINAL_BATCH_NUM         BIGINT        ,
    TERMINAL_BATCH_OPEN_TS     TIMESTAMPTZ   ,
    TERMINAL_BATCH_CLOSE_TS    TIMESTAMPTZ   ,
    SETTLE_AUTH_RESPONSE_CD    VARCHAR(60)   ,
    SETTLE_AUTH_RESPONSE_DESC  VARCHAR(4000) ,
    SETTLE_BATCH_STATE_ID      SMALLINT      ,
    SETTLE_BATCH_STATE_DESC    VARCHAR(60)   ,
    SETTLE_BATCH_TS            TIMESTAMPTZ   ,
    SETTLE_BATCH_RESPONSE_CD   VARCHAR(255)  ,
    SETTLE_BATCH_RESPONSE_DESC VARCHAR(4000) ,
    AUTH_HOLD_USED_FLAG        VARCHAR(10)   NOT NULL,
    TERMINAL_CAPTURE_FLAG      VARCHAR(10)   NOT NULL,
    TRACE_NUMBER               BIGINT        ,
    AUTH_ACTION                VARCHAR(4000) NOT NULL,
    BALANCE_AMOUNT             DECIMAL(12,4) ,
    SETTLE_STATE_ID            SMALLINT      NOT NULL DEFAULT 2,
    SETTLE_STATE               VARCHAR(30)   NOT NULL DEFAULT 'Processed',
    AUTH_APPROVED_AMOUNT       DECIMAL(12,4) ,

    CONSTRAINT PK_AUTH_DETAIL_DIM PRIMARY KEY(AUTH_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_AUTH_DETAIL_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.AUTH_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_AUTH_DETAIL_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_AUTH_DETAIL_DIM', COALESCE(TO_CHAR(OLD.AUTH_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.AUTH_DETAIL_DIM_ID != NEW.AUTH_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_AUTH_ID != NEW.SOURCE_AUTH_ID THEN
			lv_sql := lv_sql || 'SOURCE_AUTH_ID = $2.SOURCE_AUTH_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_AUTH_ID = $1.SOURCE_AUTH_ID OR SOURCE_AUTH_ID = $2.SOURCE_AUTH_ID)';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD != NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD = $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.SOURCE_TRAN_ID != NEW.SOURCE_TRAN_ID THEN
			lv_sql := lv_sql || 'SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_TRAN_ID = $1.SOURCE_TRAN_ID OR SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID)';
		END IF;
		IF OLD.SOURCE_TRAN_STATE_CD IS DISTINCT FROM NEW.SOURCE_TRAN_STATE_CD THEN
			lv_sql := lv_sql || 'SOURCE_TRAN_STATE_CD = $2.SOURCE_TRAN_STATE_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_TRAN_STATE_CD IS NOT DISTINCT FROM $1.SOURCE_TRAN_STATE_CD OR SOURCE_TRAN_STATE_CD IS NOT DISTINCT FROM $2.SOURCE_TRAN_STATE_CD)';
		END IF;
		IF OLD.AUTH_TS IS DISTINCT FROM NEW.AUTH_TS THEN
			lv_sql := lv_sql || 'AUTH_TS = $2.AUTH_TS, ';
			lv_filter := lv_filter || ' AND (AUTH_TS IS NOT DISTINCT FROM $1.AUTH_TS OR AUTH_TS IS NOT DISTINCT FROM $2.AUTH_TS)';
		END IF;
		IF OLD.RESPONSE_CD IS DISTINCT FROM NEW.RESPONSE_CD THEN
			lv_sql := lv_sql || 'RESPONSE_CD = $2.RESPONSE_CD, ';
			lv_filter := lv_filter || ' AND (RESPONSE_CD IS NOT DISTINCT FROM $1.RESPONSE_CD OR RESPONSE_CD IS NOT DISTINCT FROM $2.RESPONSE_CD)';
		END IF;
		IF OLD.RESPONSE_DESC IS DISTINCT FROM NEW.RESPONSE_DESC THEN
			lv_sql := lv_sql || 'RESPONSE_DESC = $2.RESPONSE_DESC, ';
			lv_filter := lv_filter || ' AND (RESPONSE_DESC IS NOT DISTINCT FROM $1.RESPONSE_DESC OR RESPONSE_DESC IS NOT DISTINCT FROM $2.RESPONSE_DESC)';
		END IF;
		IF OLD.AUTHORITY_TRAN_CD IS DISTINCT FROM NEW.AUTHORITY_TRAN_CD THEN
			lv_sql := lv_sql || 'AUTHORITY_TRAN_CD = $2.AUTHORITY_TRAN_CD, ';
			lv_filter := lv_filter || ' AND (AUTHORITY_TRAN_CD IS NOT DISTINCT FROM $1.AUTHORITY_TRAN_CD OR AUTHORITY_TRAN_CD IS NOT DISTINCT FROM $2.AUTHORITY_TRAN_CD)';
		END IF;
		IF OLD.AUTHORITY_REF_CD IS DISTINCT FROM NEW.AUTHORITY_REF_CD THEN
			lv_sql := lv_sql || 'AUTHORITY_REF_CD = $2.AUTHORITY_REF_CD, ';
			lv_filter := lv_filter || ' AND (AUTHORITY_REF_CD IS NOT DISTINCT FROM $1.AUTHORITY_REF_CD OR AUTHORITY_REF_CD IS NOT DISTINCT FROM $2.AUTHORITY_REF_CD)';
		END IF;
		IF OLD.AUTHORITY_MISC_DATA IS DISTINCT FROM NEW.AUTHORITY_MISC_DATA THEN
			lv_sql := lv_sql || 'AUTHORITY_MISC_DATA = $2.AUTHORITY_MISC_DATA, ';
			lv_filter := lv_filter || ' AND (AUTHORITY_MISC_DATA IS NOT DISTINCT FROM $1.AUTHORITY_MISC_DATA OR AUTHORITY_MISC_DATA IS NOT DISTINCT FROM $2.AUTHORITY_MISC_DATA)';
		END IF;
		IF OLD.TERMINAL_BATCH_NUM IS DISTINCT FROM NEW.TERMINAL_BATCH_NUM THEN
			lv_sql := lv_sql || 'TERMINAL_BATCH_NUM = $2.TERMINAL_BATCH_NUM, ';
			lv_filter := lv_filter || ' AND (TERMINAL_BATCH_NUM IS NOT DISTINCT FROM $1.TERMINAL_BATCH_NUM OR TERMINAL_BATCH_NUM IS NOT DISTINCT FROM $2.TERMINAL_BATCH_NUM)';
		END IF;
		IF OLD.TERMINAL_BATCH_OPEN_TS IS DISTINCT FROM NEW.TERMINAL_BATCH_OPEN_TS THEN
			lv_sql := lv_sql || 'TERMINAL_BATCH_OPEN_TS = $2.TERMINAL_BATCH_OPEN_TS, ';
			lv_filter := lv_filter || ' AND (TERMINAL_BATCH_OPEN_TS IS NOT DISTINCT FROM $1.TERMINAL_BATCH_OPEN_TS OR TERMINAL_BATCH_OPEN_TS IS NOT DISTINCT FROM $2.TERMINAL_BATCH_OPEN_TS)';
		END IF;
		IF OLD.TERMINAL_BATCH_CLOSE_TS IS DISTINCT FROM NEW.TERMINAL_BATCH_CLOSE_TS THEN
			lv_sql := lv_sql || 'TERMINAL_BATCH_CLOSE_TS = $2.TERMINAL_BATCH_CLOSE_TS, ';
			lv_filter := lv_filter || ' AND (TERMINAL_BATCH_CLOSE_TS IS NOT DISTINCT FROM $1.TERMINAL_BATCH_CLOSE_TS OR TERMINAL_BATCH_CLOSE_TS IS NOT DISTINCT FROM $2.TERMINAL_BATCH_CLOSE_TS)';
		END IF;
		IF OLD.SETTLE_AUTH_RESPONSE_CD IS DISTINCT FROM NEW.SETTLE_AUTH_RESPONSE_CD THEN
			lv_sql := lv_sql || 'SETTLE_AUTH_RESPONSE_CD = $2.SETTLE_AUTH_RESPONSE_CD, ';
			lv_filter := lv_filter || ' AND (SETTLE_AUTH_RESPONSE_CD IS NOT DISTINCT FROM $1.SETTLE_AUTH_RESPONSE_CD OR SETTLE_AUTH_RESPONSE_CD IS NOT DISTINCT FROM $2.SETTLE_AUTH_RESPONSE_CD)';
		END IF;
		IF OLD.SETTLE_AUTH_RESPONSE_DESC IS DISTINCT FROM NEW.SETTLE_AUTH_RESPONSE_DESC THEN
			lv_sql := lv_sql || 'SETTLE_AUTH_RESPONSE_DESC = $2.SETTLE_AUTH_RESPONSE_DESC, ';
			lv_filter := lv_filter || ' AND (SETTLE_AUTH_RESPONSE_DESC IS NOT DISTINCT FROM $1.SETTLE_AUTH_RESPONSE_DESC OR SETTLE_AUTH_RESPONSE_DESC IS NOT DISTINCT FROM $2.SETTLE_AUTH_RESPONSE_DESC)';
		END IF;
		IF OLD.SETTLE_BATCH_STATE_ID IS DISTINCT FROM NEW.SETTLE_BATCH_STATE_ID THEN
			lv_sql := lv_sql || 'SETTLE_BATCH_STATE_ID = $2.SETTLE_BATCH_STATE_ID, ';
			lv_filter := lv_filter || ' AND (SETTLE_BATCH_STATE_ID IS NOT DISTINCT FROM $1.SETTLE_BATCH_STATE_ID OR SETTLE_BATCH_STATE_ID IS NOT DISTINCT FROM $2.SETTLE_BATCH_STATE_ID)';
		END IF;
		IF OLD.SETTLE_BATCH_STATE_DESC IS DISTINCT FROM NEW.SETTLE_BATCH_STATE_DESC THEN
			lv_sql := lv_sql || 'SETTLE_BATCH_STATE_DESC = $2.SETTLE_BATCH_STATE_DESC, ';
			lv_filter := lv_filter || ' AND (SETTLE_BATCH_STATE_DESC IS NOT DISTINCT FROM $1.SETTLE_BATCH_STATE_DESC OR SETTLE_BATCH_STATE_DESC IS NOT DISTINCT FROM $2.SETTLE_BATCH_STATE_DESC)';
		END IF;
		IF OLD.SETTLE_BATCH_TS IS DISTINCT FROM NEW.SETTLE_BATCH_TS THEN
			lv_sql := lv_sql || 'SETTLE_BATCH_TS = $2.SETTLE_BATCH_TS, ';
			lv_filter := lv_filter || ' AND (SETTLE_BATCH_TS IS NOT DISTINCT FROM $1.SETTLE_BATCH_TS OR SETTLE_BATCH_TS IS NOT DISTINCT FROM $2.SETTLE_BATCH_TS)';
		END IF;
		IF OLD.SETTLE_BATCH_RESPONSE_CD IS DISTINCT FROM NEW.SETTLE_BATCH_RESPONSE_CD THEN
			lv_sql := lv_sql || 'SETTLE_BATCH_RESPONSE_CD = $2.SETTLE_BATCH_RESPONSE_CD, ';
			lv_filter := lv_filter || ' AND (SETTLE_BATCH_RESPONSE_CD IS NOT DISTINCT FROM $1.SETTLE_BATCH_RESPONSE_CD OR SETTLE_BATCH_RESPONSE_CD IS NOT DISTINCT FROM $2.SETTLE_BATCH_RESPONSE_CD)';
		END IF;
		IF OLD.SETTLE_BATCH_RESPONSE_DESC IS DISTINCT FROM NEW.SETTLE_BATCH_RESPONSE_DESC THEN
			lv_sql := lv_sql || 'SETTLE_BATCH_RESPONSE_DESC = $2.SETTLE_BATCH_RESPONSE_DESC, ';
			lv_filter := lv_filter || ' AND (SETTLE_BATCH_RESPONSE_DESC IS NOT DISTINCT FROM $1.SETTLE_BATCH_RESPONSE_DESC OR SETTLE_BATCH_RESPONSE_DESC IS NOT DISTINCT FROM $2.SETTLE_BATCH_RESPONSE_DESC)';
		END IF;
		IF OLD.AUTH_HOLD_USED_FLAG != NEW.AUTH_HOLD_USED_FLAG THEN
			lv_sql := lv_sql || 'AUTH_HOLD_USED_FLAG = $2.AUTH_HOLD_USED_FLAG, ';
			lv_filter := lv_filter || ' AND (AUTH_HOLD_USED_FLAG = $1.AUTH_HOLD_USED_FLAG OR AUTH_HOLD_USED_FLAG = $2.AUTH_HOLD_USED_FLAG)';
		END IF;
		IF OLD.TERMINAL_CAPTURE_FLAG != NEW.TERMINAL_CAPTURE_FLAG THEN
			lv_sql := lv_sql || 'TERMINAL_CAPTURE_FLAG = $2.TERMINAL_CAPTURE_FLAG, ';
			lv_filter := lv_filter || ' AND (TERMINAL_CAPTURE_FLAG = $1.TERMINAL_CAPTURE_FLAG OR TERMINAL_CAPTURE_FLAG = $2.TERMINAL_CAPTURE_FLAG)';
		END IF;
		IF OLD.TRACE_NUMBER IS DISTINCT FROM NEW.TRACE_NUMBER THEN
			lv_sql := lv_sql || 'TRACE_NUMBER = $2.TRACE_NUMBER, ';
			lv_filter := lv_filter || ' AND (TRACE_NUMBER IS NOT DISTINCT FROM $1.TRACE_NUMBER OR TRACE_NUMBER IS NOT DISTINCT FROM $2.TRACE_NUMBER)';
		END IF;
		IF OLD.AUTH_ACTION != NEW.AUTH_ACTION THEN
			lv_sql := lv_sql || 'AUTH_ACTION = $2.AUTH_ACTION, ';
			lv_filter := lv_filter || ' AND (AUTH_ACTION = $1.AUTH_ACTION OR AUTH_ACTION = $2.AUTH_ACTION)';
		END IF;
		IF OLD.BALANCE_AMOUNT IS DISTINCT FROM NEW.BALANCE_AMOUNT THEN
			lv_sql := lv_sql || 'BALANCE_AMOUNT = $2.BALANCE_AMOUNT, ';
			lv_filter := lv_filter || ' AND (BALANCE_AMOUNT IS NOT DISTINCT FROM $1.BALANCE_AMOUNT OR BALANCE_AMOUNT IS NOT DISTINCT FROM $2.BALANCE_AMOUNT)';
		END IF;
		IF OLD.SETTLE_STATE_ID != NEW.SETTLE_STATE_ID THEN
			lv_sql := lv_sql || 'SETTLE_STATE_ID = $2.SETTLE_STATE_ID, ';
			lv_filter := lv_filter || ' AND (SETTLE_STATE_ID = $1.SETTLE_STATE_ID OR SETTLE_STATE_ID = $2.SETTLE_STATE_ID)';
		END IF;
		IF OLD.SETTLE_STATE != NEW.SETTLE_STATE THEN
			lv_sql := lv_sql || 'SETTLE_STATE = $2.SETTLE_STATE, ';
			lv_filter := lv_filter || ' AND (SETTLE_STATE = $1.SETTLE_STATE OR SETTLE_STATE = $2.SETTLE_STATE)';
		END IF;
		IF OLD.AUTH_APPROVED_AMOUNT IS DISTINCT FROM NEW.AUTH_APPROVED_AMOUNT THEN
			lv_sql := lv_sql || 'AUTH_APPROVED_AMOUNT = $2.AUTH_APPROVED_AMOUNT, ';
			lv_filter := lv_filter || ' AND (AUTH_APPROVED_AMOUNT IS NOT DISTINCT FROM $1.AUTH_APPROVED_AMOUNT OR AUTH_APPROVED_AMOUNT IS NOT DISTINCT FROM $2.AUTH_APPROVED_AMOUNT)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE AUTH_DETAIL_DIM_ID = $1.AUTH_DETAIL_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.AUTH_TS IS NULL THEN 'AUTH_TS IS NULL' ELSE 'AUTH_TS >= TIMESTAMP'''||TO_CHAR(NEW.AUTH_TS, 'YYYY-MM-01')||''' AND AUTH_TS < TIMESTAMP'''||TO_CHAR(NEW.AUTH_TS + '1 month', 'YYYY-MM-01')||'''' END  ||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(AUTH_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._AUTH_DETAIL_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_AUTH_DETAIL_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE AUTH_DETAIL_DIM_ID = $1.AUTH_DETAIL_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.AUTH_DETAIL_DIM
	AS SELECT * FROM RDW._AUTH_DETAIL_DIM;

ALTER VIEW RDW.AUTH_DETAIL_DIM ALTER AUTH_DETAIL_DIM_ID SET DEFAULT nextval('RDW._auth_detail_dim_auth_detail_dim_id_seq'::regclass);
ALTER VIEW RDW.AUTH_DETAIL_DIM ALTER SETTLE_STATE_ID SET DEFAULT 2;
ALTER VIEW RDW.AUTH_DETAIL_DIM ALTER SETTLE_STATE SET DEFAULT 'Processed';
ALTER VIEW RDW.AUTH_DETAIL_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.AUTH_DETAIL_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.AUTH_DETAIL_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.AUTH_DETAIL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.AUTH_DETAIL_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._AUTH_DETAIL_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_AUTH_DETAIL_DIM ON RDW.AUTH_DETAIL_DIM;
CREATE TRIGGER TRIIUD_AUTH_DETAIL_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.AUTH_DETAIL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_AUTH_DETAIL_DIM();


CREATE UNIQUE INDEX AK_AUTH_DETAIL_DIM ON RDW._AUTH_DETAIL_DIM(SOURCE_AUTH_ID, SOURCE_SYSTEM_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_SOURCE_TRAN_ID ON RDW._AUTH_DETAIL_DIM(SOURCE_TRAN_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_AUTH_TS ON RDW._AUTH_DETAIL_DIM(AUTH_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_RESPONSE_CD ON RDW._AUTH_DETAIL_DIM(RESPONSE_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_AUTHORITY_TRAN_CD ON RDW._AUTH_DETAIL_DIM(AUTHORITY_TRAN_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_AUTHORITY_REF_CD ON RDW._AUTH_DETAIL_DIM(AUTHORITY_REF_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_SETTLE_BATCH_STATE_ID ON RDW._AUTH_DETAIL_DIM(SETTLE_BATCH_STATE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_AUTH_ACTION ON RDW._AUTH_DETAIL_DIM(AUTH_ACTION) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_SETTLE_STATE_ID ON RDW._AUTH_DETAIL_DIM(SETTLE_STATE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_DETAIL_DIM_CREATED_TS ON RDW._AUTH_DETAIL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.AUTH_DETAIL_DIM ----------------------------------

INSERT INTO RDW.AUTH_DETAIL_DIM(
	AUTH_DETAIL_DIM_ID,
	SOURCE_AUTH_ID,
	SOURCE_SYSTEM_CD,
	SOURCE_TRAN_ID,
	SOURCE_TRAN_STATE_CD,
	AUTH_TS,
	RESPONSE_CD,
	RESPONSE_DESC,
	AUTHORITY_TRAN_CD,
	AUTHORITY_REF_CD,
	AUTHORITY_MISC_DATA,
	TERMINAL_BATCH_NUM,
	TERMINAL_BATCH_OPEN_TS,
	TERMINAL_BATCH_CLOSE_TS,
	SETTLE_AUTH_RESPONSE_CD,
	SETTLE_AUTH_RESPONSE_DESC,
	SETTLE_BATCH_STATE_ID,
	SETTLE_BATCH_STATE_DESC,
	SETTLE_BATCH_TS,
	SETTLE_BATCH_RESPONSE_CD,
	SETTLE_BATCH_RESPONSE_DESC,
	AUTH_HOLD_USED_FLAG,
	TERMINAL_CAPTURE_FLAG,
	TRACE_NUMBER,
	AUTH_ACTION,
	BALANCE_AMOUNT,
	SETTLE_STATE_ID,
	SETTLE_STATE,
	AUTH_APPROVED_AMOUNT,
	REVISION)
VALUES(
	0,
	0,
	'Unknown',
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	NULL,
	2,
	'Processed',
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_AUTH_DETAIL_DIM(
	p_auth_detail_dim_id OUT BIGINT, 
	p_source_auth_id BIGINT, 
	p_source_system_cd VARCHAR(10), 
	p_source_tran_id BIGINT, 
	p_source_tran_state_cd CHAR(1), 
	p_auth_ts TIMESTAMPTZ, 
	p_response_cd VARCHAR(60), 
	p_response_desc VARCHAR(4000), 
	p_authority_tran_cd VARCHAR(60), 
	p_authority_ref_cd VARCHAR(255), 
	p_authority_misc_data VARCHAR(4000), 
	p_terminal_batch_num BIGINT, 
	p_terminal_batch_open_ts TIMESTAMPTZ, 
	p_terminal_batch_close_ts TIMESTAMPTZ, 
	p_settle_auth_response_cd VARCHAR(60), 
	p_settle_auth_response_desc VARCHAR(4000), 
	p_settle_batch_state_id SMALLINT, 
	p_settle_batch_state_desc VARCHAR(60), 
	p_settle_batch_ts TIMESTAMPTZ, 
	p_settle_batch_response_cd VARCHAR(255), 
	p_settle_batch_response_desc VARCHAR(4000), 
	p_auth_hold_used_flag VARCHAR(10), 
	p_terminal_capture_flag VARCHAR(10), 
	p_trace_number BIGINT, 
	p_auth_action VARCHAR(4000), 
	p_balance_amount DECIMAL(12,4), 
	p_settle_state_id SMALLINT, 
	p_settle_state VARCHAR(30), 
	p_auth_approved_amount DECIMAL(12,4), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT AUTH_DETAIL_DIM_ID, REVISION
			  INTO p_auth_detail_dim_id, p_old_revision
			  FROM RDW.AUTH_DETAIL_DIM
			 WHERE SOURCE_AUTH_ID = p_source_auth_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.AUTH_DETAIL_DIM
			   SET SOURCE_TRAN_ID = p_source_tran_id,
			       SOURCE_TRAN_STATE_CD = p_source_tran_state_cd,
			       AUTH_TS = p_auth_ts,
			       RESPONSE_CD = p_response_cd,
			       RESPONSE_DESC = p_response_desc,
			       AUTHORITY_TRAN_CD = p_authority_tran_cd,
			       AUTHORITY_REF_CD = p_authority_ref_cd,
			       AUTHORITY_MISC_DATA = p_authority_misc_data,
			       TERMINAL_BATCH_NUM = p_terminal_batch_num,
			       TERMINAL_BATCH_OPEN_TS = p_terminal_batch_open_ts,
			       TERMINAL_BATCH_CLOSE_TS = p_terminal_batch_close_ts,
			       SETTLE_AUTH_RESPONSE_CD = p_settle_auth_response_cd,
			       SETTLE_AUTH_RESPONSE_DESC = p_settle_auth_response_desc,
			       SETTLE_BATCH_STATE_ID = p_settle_batch_state_id,
			       SETTLE_BATCH_STATE_DESC = p_settle_batch_state_desc,
			       SETTLE_BATCH_TS = p_settle_batch_ts,
			       SETTLE_BATCH_RESPONSE_CD = p_settle_batch_response_cd,
			       SETTLE_BATCH_RESPONSE_DESC = p_settle_batch_response_desc,
			       AUTH_HOLD_USED_FLAG = p_auth_hold_used_flag,
			       TERMINAL_CAPTURE_FLAG = p_terminal_capture_flag,
			       TRACE_NUMBER = p_trace_number,
			       AUTH_ACTION = p_auth_action,
			       BALANCE_AMOUNT = p_balance_amount,
			       SETTLE_STATE_ID = COALESCE(p_settle_state_id, SETTLE_STATE_ID),
			       SETTLE_STATE = COALESCE(p_settle_state, SETTLE_STATE),
			       AUTH_APPROVED_AMOUNT = p_auth_approved_amount,
			       REVISION = p_revision
			 WHERE SOURCE_AUTH_ID = p_source_auth_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.AUTH_DETAIL_DIM(SOURCE_AUTH_ID, SOURCE_SYSTEM_CD, SOURCE_TRAN_ID, SOURCE_TRAN_STATE_CD, AUTH_TS, RESPONSE_CD, RESPONSE_DESC, AUTHORITY_TRAN_CD, AUTHORITY_REF_CD, AUTHORITY_MISC_DATA, TERMINAL_BATCH_NUM, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_CLOSE_TS, SETTLE_AUTH_RESPONSE_CD, SETTLE_AUTH_RESPONSE_DESC, SETTLE_BATCH_STATE_ID, SETTLE_BATCH_STATE_DESC, SETTLE_BATCH_TS, SETTLE_BATCH_RESPONSE_CD, SETTLE_BATCH_RESPONSE_DESC, AUTH_HOLD_USED_FLAG, TERMINAL_CAPTURE_FLAG, TRACE_NUMBER, AUTH_ACTION, BALANCE_AMOUNT, SETTLE_STATE_ID, SETTLE_STATE, AUTH_APPROVED_AMOUNT, REVISION)
					 VALUES(p_source_auth_id, p_source_system_cd, p_source_tran_id, p_source_tran_state_cd, p_auth_ts, p_response_cd, p_response_desc, p_authority_tran_cd, p_authority_ref_cd, p_authority_misc_data, p_terminal_batch_num, p_terminal_batch_open_ts, p_terminal_batch_close_ts, p_settle_auth_response_cd, p_settle_auth_response_desc, p_settle_batch_state_id, p_settle_batch_state_desc, p_settle_batch_ts, p_settle_batch_response_cd, p_settle_batch_response_desc, p_auth_hold_used_flag, p_terminal_capture_flag, p_trace_number, p_auth_action, p_balance_amount, COALESCE(p_settle_state_id, 2), COALESCE(p_settle_state, 'Processed'), p_auth_approved_amount, p_revision)
					 RETURNING AUTH_DETAIL_DIM_ID
					      INTO p_auth_detail_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_AUTH_DETAIL_DIM(
	p_auth_detail_dim_id OUT BIGINT, 
	p_source_auth_id BIGINT, 
	p_source_system_cd VARCHAR(10), 
	p_source_tran_id BIGINT, 
	p_source_tran_state_cd CHAR(1), 
	p_auth_ts TIMESTAMPTZ, 
	p_response_cd VARCHAR(60), 
	p_response_desc VARCHAR(4000), 
	p_authority_tran_cd VARCHAR(60), 
	p_authority_ref_cd VARCHAR(255), 
	p_authority_misc_data VARCHAR(4000), 
	p_terminal_batch_num BIGINT, 
	p_terminal_batch_open_ts TIMESTAMPTZ, 
	p_terminal_batch_close_ts TIMESTAMPTZ, 
	p_settle_auth_response_cd VARCHAR(60), 
	p_settle_auth_response_desc VARCHAR(4000), 
	p_settle_batch_state_id SMALLINT, 
	p_settle_batch_state_desc VARCHAR(60), 
	p_settle_batch_ts TIMESTAMPTZ, 
	p_settle_batch_response_cd VARCHAR(255), 
	p_settle_batch_response_desc VARCHAR(4000), 
	p_auth_hold_used_flag VARCHAR(10), 
	p_terminal_capture_flag VARCHAR(10), 
	p_trace_number BIGINT, 
	p_auth_action VARCHAR(4000), 
	p_balance_amount DECIMAL(12,4), 
	p_settle_state_id SMALLINT, 
	p_settle_state VARCHAR(30), 
	p_auth_approved_amount DECIMAL(12,4), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.AUTH_TYPE_DIM
  (
    AUTH_TYPE_DIM_ID              SERIAL       NOT NULL,
    SOURCE_AUTH_RESULT_CD         CHAR(1)      NOT NULL,
    SOURCE_AUTH_TYPE_CD           CHAR(1)      NOT NULL,
    SOURCE_CLIENT_PAYMENT_TYPE_CD CHAR(1)      NOT NULL,
    SOURCE_ENTRY_METHOD_CD        CHAR(1)      NOT NULL,
    SOURCE_DEVICE_RESULT_TYPE_CD  CHAR(1)      NOT NULL,
    AUTH_STATE_ID                 SMALLINT     NOT NULL,
    AUTH_RESULT                   VARCHAR(255) NOT NULL,
    AUTH_STATE                    VARCHAR(60)  NOT NULL,
    AUTH_TYPE                     VARCHAR(60)  NOT NULL,
    TRAN_TYPE                     VARCHAR(50)  NOT NULL,
    TRAN_TYPE_ID                  SMALLINT     NOT NULL,
    TRAN_TYPE_CODE                CHAR(1)      NOT NULL,
    TRAN_TYPE_ITEM                VARCHAR(50)  NOT NULL,
    ENTRY_METHOD                  VARCHAR(30)  NOT NULL,
    TRAN_RESULT_DESC              VARCHAR(200) NOT NULL,
    DISPLAY_ORDER                 SMALLINT     ,
    REFUNDABLE_FLAG               VARCHAR(10)  NOT NULL,
    CHARGEBACKABLE_FLAG           VARCHAR(10)  NOT NULL,
    CASH_COUNT_FLAG               VARCHAR(10)  NOT NULL,
    CASHLESS_COUNT_FLAG           VARCHAR(10)  NOT NULL,

    CONSTRAINT PK_AUTH_TYPE_DIM PRIMARY KEY(AUTH_TYPE_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.AUTH_TYPE_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.AUTH_TYPE_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_AUTH_TYPE_DIM ON RDW.AUTH_TYPE_DIM;


CREATE TRIGGER TRBI_AUTH_TYPE_DIM
BEFORE INSERT ON RDW.AUTH_TYPE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_AUTH_TYPE_DIM ON RDW.AUTH_TYPE_DIM;


CREATE TRIGGER TRBU_AUTH_TYPE_DIM
BEFORE UPDATE ON RDW.AUTH_TYPE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE UNIQUE INDEX AK_AUTH_TYPE_DIM ON RDW.AUTH_TYPE_DIM(SOURCE_AUTH_RESULT_CD, SOURCE_AUTH_TYPE_CD, SOURCE_CLIENT_PAYMENT_TYPE_CD, SOURCE_ENTRY_METHOD_CD, SOURCE_DEVICE_RESULT_TYPE_CD, TRAN_TYPE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_AUTH_RESULT ON RDW.AUTH_TYPE_DIM(AUTH_RESULT) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_AUTH_STATE ON RDW.AUTH_TYPE_DIM(AUTH_STATE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_AUTH_TYPE ON RDW.AUTH_TYPE_DIM(AUTH_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_TRAN_TYPE ON RDW.AUTH_TYPE_DIM(TRAN_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_ENTRY_METHOD ON RDW.AUTH_TYPE_DIM(ENTRY_METHOD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_REFUNDABLE_FLAG ON RDW.AUTH_TYPE_DIM(REFUNDABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_CHARGEBACKABLE_FLAG ON RDW.AUTH_TYPE_DIM(CHARGEBACKABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_TYPE_DIM_CREATED_TS ON RDW.AUTH_TYPE_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.AUTH_TYPE_DIM ----------------------------------

INSERT INTO RDW.AUTH_TYPE_DIM(
	AUTH_TYPE_DIM_ID,
	SOURCE_AUTH_RESULT_CD,
	SOURCE_AUTH_TYPE_CD,
	SOURCE_CLIENT_PAYMENT_TYPE_CD,
	SOURCE_ENTRY_METHOD_CD,
	SOURCE_DEVICE_RESULT_TYPE_CD,
	AUTH_STATE_ID,
	AUTH_RESULT,
	AUTH_STATE,
	AUTH_TYPE,
	TRAN_TYPE,
	TRAN_TYPE_ID,
	TRAN_TYPE_CODE,
	TRAN_TYPE_ITEM,
	ENTRY_METHOD,
	TRAN_RESULT_DESC,
	DISPLAY_ORDER,
	REFUNDABLE_FLAG,
	CHARGEBACKABLE_FLAG,
	CASH_COUNT_FLAG,
	CASHLESS_COUNT_FLAG,
	REVISION)
VALUES(
	0,
	'-',
	'-',
	'-',
	'-',
	'-',
	0,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	0,
	'-',
	'Unknown',
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_AUTH_TYPE_DIM(
	p_auth_type_dim_id OUT INTEGER, 
	p_source_auth_result_cd CHAR(1), 
	p_source_auth_type_cd CHAR(1), 
	p_source_client_payment_type_cd CHAR(1), 
	p_source_entry_method_cd CHAR(1), 
	p_source_device_result_type_cd CHAR(1), 
	p_auth_state_id SMALLINT, 
	p_auth_result VARCHAR(255), 
	p_auth_state VARCHAR(60), 
	p_auth_type VARCHAR(60), 
	p_tran_type VARCHAR(50), 
	p_tran_type_id SMALLINT, 
	p_tran_type_code CHAR(1), 
	p_tran_type_item VARCHAR(50), 
	p_entry_method VARCHAR(30), 
	p_tran_result_desc VARCHAR(200), 
	p_display_order SMALLINT, 
	p_refundable_flag VARCHAR(10), 
	p_chargebackable_flag VARCHAR(10), 
	p_cash_count_flag VARCHAR(10), 
	p_cashless_count_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT AUTH_TYPE_DIM_ID, REVISION
			  INTO p_auth_type_dim_id, p_old_revision
			  FROM RDW.AUTH_TYPE_DIM
			 WHERE SOURCE_AUTH_RESULT_CD = p_source_auth_result_cd
			   AND SOURCE_AUTH_TYPE_CD = p_source_auth_type_cd
			   AND SOURCE_CLIENT_PAYMENT_TYPE_CD = p_source_client_payment_type_cd
			   AND SOURCE_ENTRY_METHOD_CD = p_source_entry_method_cd
			   AND SOURCE_DEVICE_RESULT_TYPE_CD = p_source_device_result_type_cd
			   AND TRAN_TYPE_ID = p_tran_type_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.AUTH_TYPE_DIM
			   SET AUTH_STATE_ID = p_auth_state_id,
			       AUTH_RESULT = p_auth_result,
			       AUTH_STATE = p_auth_state,
			       AUTH_TYPE = p_auth_type,
			       TRAN_TYPE = p_tran_type,
			       TRAN_TYPE_CODE = p_tran_type_code,
			       TRAN_TYPE_ITEM = p_tran_type_item,
			       ENTRY_METHOD = p_entry_method,
			       TRAN_RESULT_DESC = p_tran_result_desc,
			       DISPLAY_ORDER = p_display_order,
			       REFUNDABLE_FLAG = p_refundable_flag,
			       CHARGEBACKABLE_FLAG = p_chargebackable_flag,
			       CASH_COUNT_FLAG = p_cash_count_flag,
			       CASHLESS_COUNT_FLAG = p_cashless_count_flag,
			       REVISION = p_revision
			 WHERE SOURCE_AUTH_RESULT_CD = p_source_auth_result_cd
			   AND SOURCE_AUTH_TYPE_CD = p_source_auth_type_cd
			   AND SOURCE_CLIENT_PAYMENT_TYPE_CD = p_source_client_payment_type_cd
			   AND SOURCE_ENTRY_METHOD_CD = p_source_entry_method_cd
			   AND SOURCE_DEVICE_RESULT_TYPE_CD = p_source_device_result_type_cd
			   AND TRAN_TYPE_ID = p_tran_type_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.AUTH_TYPE_DIM(SOURCE_AUTH_RESULT_CD, SOURCE_AUTH_TYPE_CD, SOURCE_CLIENT_PAYMENT_TYPE_CD, SOURCE_ENTRY_METHOD_CD, SOURCE_DEVICE_RESULT_TYPE_CD, TRAN_TYPE_ID, AUTH_STATE_ID, AUTH_RESULT, AUTH_STATE, AUTH_TYPE, TRAN_TYPE, TRAN_TYPE_CODE, TRAN_TYPE_ITEM, ENTRY_METHOD, TRAN_RESULT_DESC, DISPLAY_ORDER, REFUNDABLE_FLAG, CHARGEBACKABLE_FLAG, CASH_COUNT_FLAG, CASHLESS_COUNT_FLAG, REVISION)
					 VALUES(p_source_auth_result_cd, p_source_auth_type_cd, p_source_client_payment_type_cd, p_source_entry_method_cd, p_source_device_result_type_cd, p_tran_type_id, p_auth_state_id, p_auth_result, p_auth_state, p_auth_type, p_tran_type, p_tran_type_code, p_tran_type_item, p_entry_method, p_tran_result_desc, p_display_order, p_refundable_flag, p_chargebackable_flag, p_cash_count_flag, p_cashless_count_flag, p_revision)
					 RETURNING AUTH_TYPE_DIM_ID
					      INTO p_auth_type_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_AUTH_TYPE_DIM(
	p_auth_type_dim_id OUT INTEGER, 
	p_source_auth_result_cd CHAR(1), 
	p_source_auth_type_cd CHAR(1), 
	p_source_client_payment_type_cd CHAR(1), 
	p_source_entry_method_cd CHAR(1), 
	p_source_device_result_type_cd CHAR(1), 
	p_auth_state_id SMALLINT, 
	p_auth_result VARCHAR(255), 
	p_auth_state VARCHAR(60), 
	p_auth_type VARCHAR(60), 
	p_tran_type VARCHAR(50), 
	p_tran_type_id SMALLINT, 
	p_tran_type_code CHAR(1), 
	p_tran_type_item VARCHAR(50), 
	p_entry_method VARCHAR(30), 
	p_tran_result_desc VARCHAR(200), 
	p_display_order SMALLINT, 
	p_refundable_flag VARCHAR(10), 
	p_chargebackable_flag VARCHAR(10), 
	p_cash_count_flag VARCHAR(10), 
	p_cashless_count_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.AUTHORITY_TERMINAL_DIM
  (
    AUTHORITY_TERMINAL_DIM_ID   BIGSERIAL    NOT NULL,
    SOURCE_TERMINAL_ID          BIGINT       NOT NULL,
    SOURCE_KEY_NAME             VARCHAR(30)  NOT NULL,
    TERMINAL_CD                 VARCHAR(255) NOT NULL,
    TERMINAL_DESC               VARCHAR(255) ,
    MERCHANT_CD                 VARCHAR(255) NOT NULL,
    MERCHANT_NAME               VARCHAR(255) NOT NULL,
    MERCHANT_DESC               VARCHAR(255) ,
    MERCHANT_BUSINESS_NAME      VARCHAR(255) ,
    AUTHORITY_NAME              VARCHAR(60)  NOT NULL,
    AUTHORITY_TYPE_NAME         VARCHAR(60)  NOT NULL,
    AUTHORITY_TYPE_DESC         VARCHAR(60)  ,
    AUTHORITY_TYPE_HANDLER_NAME VARCHAR(60)  NOT NULL,
    AUTHORITY_SERVICE_NAME      VARCHAR(60)  NOT NULL,
    AUTHORITY_SERVICE_DESC      VARCHAR(255) ,
    AUTHORITY_SERVICE_TYPE_ID   SMALLINT     ,
    AUTHORITY_SERVICE_TYPE_NAME VARCHAR(60)  NOT NULL,
    AUTHORITY_SERVICE_TYPE_DESC VARCHAR(255) ,

    CONSTRAINT PK_AUTHORITY_TERMINAL_DIM PRIMARY KEY(AUTHORITY_TERMINAL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.AUTHORITY_TERMINAL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.AUTHORITY_TERMINAL_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_AUTHORITY_TERMINAL_DIM ON RDW.AUTHORITY_TERMINAL_DIM;


CREATE TRIGGER TRBI_AUTHORITY_TERMINAL_DIM
BEFORE INSERT ON RDW.AUTHORITY_TERMINAL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_AUTHORITY_TERMINAL_DIM ON RDW.AUTHORITY_TERMINAL_DIM;


CREATE TRIGGER TRBU_AUTHORITY_TERMINAL_DIM
BEFORE UPDATE ON RDW.AUTHORITY_TERMINAL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE UNIQUE INDEX AK_AUTHORITY_TERMINAL_DIM ON RDW.AUTHORITY_TERMINAL_DIM(SOURCE_TERMINAL_ID, SOURCE_KEY_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTHORITY_TERMINAL_DIM_AUTHORITY_NAME ON RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTHORITY_TERMINAL_DIM_AUTHORITY_TYPE_HANDLER_NAME ON RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TYPE_HANDLER_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTHORITY_TERMINAL_DIM_CREATED_TS ON RDW.AUTHORITY_TERMINAL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.AUTHORITY_TERMINAL_DIM ----------------------------------

INSERT INTO RDW.AUTHORITY_TERMINAL_DIM(
	AUTHORITY_TERMINAL_DIM_ID,
	SOURCE_TERMINAL_ID,
	SOURCE_KEY_NAME,
	TERMINAL_CD,
	TERMINAL_DESC,
	MERCHANT_CD,
	MERCHANT_NAME,
	MERCHANT_DESC,
	MERCHANT_BUSINESS_NAME,
	AUTHORITY_NAME,
	AUTHORITY_TYPE_NAME,
	AUTHORITY_TYPE_DESC,
	AUTHORITY_TYPE_HANDLER_NAME,
	AUTHORITY_SERVICE_NAME,
	AUTHORITY_SERVICE_DESC,
	AUTHORITY_SERVICE_TYPE_ID,
	AUTHORITY_SERVICE_TYPE_NAME,
	AUTHORITY_SERVICE_TYPE_DESC,
	REVISION)
VALUES(
	0,
	0,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	'Unknown',
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_AUTHORITY_TERMINAL_DIM(
	p_authority_terminal_dim_id OUT BIGINT, 
	p_source_terminal_id BIGINT, 
	p_source_key_name VARCHAR(30), 
	p_terminal_cd VARCHAR(255), 
	p_terminal_desc VARCHAR(255), 
	p_merchant_cd VARCHAR(255), 
	p_merchant_name VARCHAR(255), 
	p_merchant_desc VARCHAR(255), 
	p_merchant_business_name VARCHAR(255), 
	p_authority_name VARCHAR(60), 
	p_authority_type_name VARCHAR(60), 
	p_authority_type_desc VARCHAR(60), 
	p_authority_type_handler_name VARCHAR(60), 
	p_authority_service_name VARCHAR(60), 
	p_authority_service_desc VARCHAR(255), 
	p_authority_service_type_id SMALLINT, 
	p_authority_service_type_name VARCHAR(60), 
	p_authority_service_type_desc VARCHAR(255), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT AUTHORITY_TERMINAL_DIM_ID, REVISION
			  INTO p_authority_terminal_dim_id, p_old_revision
			  FROM RDW.AUTHORITY_TERMINAL_DIM
			 WHERE SOURCE_TERMINAL_ID = p_source_terminal_id
			   AND SOURCE_KEY_NAME = p_source_key_name;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.AUTHORITY_TERMINAL_DIM
			   SET TERMINAL_CD = p_terminal_cd,
			       TERMINAL_DESC = p_terminal_desc,
			       MERCHANT_CD = p_merchant_cd,
			       MERCHANT_NAME = p_merchant_name,
			       MERCHANT_DESC = p_merchant_desc,
			       MERCHANT_BUSINESS_NAME = p_merchant_business_name,
			       AUTHORITY_NAME = p_authority_name,
			       AUTHORITY_TYPE_NAME = p_authority_type_name,
			       AUTHORITY_TYPE_DESC = p_authority_type_desc,
			       AUTHORITY_TYPE_HANDLER_NAME = p_authority_type_handler_name,
			       AUTHORITY_SERVICE_NAME = p_authority_service_name,
			       AUTHORITY_SERVICE_DESC = p_authority_service_desc,
			       AUTHORITY_SERVICE_TYPE_ID = p_authority_service_type_id,
			       AUTHORITY_SERVICE_TYPE_NAME = p_authority_service_type_name,
			       AUTHORITY_SERVICE_TYPE_DESC = p_authority_service_type_desc,
			       REVISION = p_revision
			 WHERE SOURCE_TERMINAL_ID = p_source_terminal_id
			   AND SOURCE_KEY_NAME = p_source_key_name
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.AUTHORITY_TERMINAL_DIM(SOURCE_TERMINAL_ID, SOURCE_KEY_NAME, TERMINAL_CD, TERMINAL_DESC, MERCHANT_CD, MERCHANT_NAME, MERCHANT_DESC, MERCHANT_BUSINESS_NAME, AUTHORITY_NAME, AUTHORITY_TYPE_NAME, AUTHORITY_TYPE_DESC, AUTHORITY_TYPE_HANDLER_NAME, AUTHORITY_SERVICE_NAME, AUTHORITY_SERVICE_DESC, AUTHORITY_SERVICE_TYPE_ID, AUTHORITY_SERVICE_TYPE_NAME, AUTHORITY_SERVICE_TYPE_DESC, REVISION)
					 VALUES(p_source_terminal_id, p_source_key_name, p_terminal_cd, p_terminal_desc, p_merchant_cd, p_merchant_name, p_merchant_desc, p_merchant_business_name, p_authority_name, p_authority_type_name, p_authority_type_desc, p_authority_type_handler_name, p_authority_service_name, p_authority_service_desc, p_authority_service_type_id, p_authority_service_type_name, p_authority_service_type_desc, p_revision)
					 RETURNING AUTHORITY_TERMINAL_DIM_ID
					      INTO p_authority_terminal_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_AUTHORITY_TERMINAL_DIM(
	p_authority_terminal_dim_id OUT BIGINT, 
	p_source_terminal_id BIGINT, 
	p_source_key_name VARCHAR(30), 
	p_terminal_cd VARCHAR(255), 
	p_terminal_desc VARCHAR(255), 
	p_merchant_cd VARCHAR(255), 
	p_merchant_name VARCHAR(255), 
	p_merchant_desc VARCHAR(255), 
	p_merchant_business_name VARCHAR(255), 
	p_authority_name VARCHAR(60), 
	p_authority_type_name VARCHAR(60), 
	p_authority_type_desc VARCHAR(60), 
	p_authority_type_handler_name VARCHAR(60), 
	p_authority_service_name VARCHAR(60), 
	p_authority_service_desc VARCHAR(255), 
	p_authority_service_type_id SMALLINT, 
	p_authority_service_type_name VARCHAR(60), 
	p_authority_service_type_desc VARCHAR(255), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._CONSUMER_DIM
  (
    CONSUMER_DIM_ID          BIGSERIAL    NOT NULL,
    GLOBAL_ACCOUNT_ID        BIGINT       ,
    SOURCE_CONSUMER_ACCT_ID  BIGINT       ,
    CARD_TYPE                VARCHAR(100) NOT NULL,
    SAFE_CARD_NUMBER         VARCHAR(100) NOT NULL,
    CONSUMER_ACCT_IDENTIFIER BIGINT       ,
    CONSUMER_NAME            VARCHAR(100) NOT NULL,
    CONSUMER_IDENTIFIER      VARCHAR(100) ,
    ACCOUNT_STATUS           VARCHAR(50)  NOT NULL,
    LAST_AUTH_TS             TIMESTAMPTZ  ,

    CONSTRAINT PK_CONSUMER_DIM PRIMARY KEY(CONSUMER_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_CONSUMER_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(CASE WHEN NEW.GLOBAL_ACCOUNT_ID IS NULL THEN 'NA' ELSE (NEW.GLOBAL_ACCOUNT_ID % 17)::VARCHAR END,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_CONSUMER_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_CONSUMER_DIM', COALESCE(CASE WHEN OLD.GLOBAL_ACCOUNT_ID IS NULL THEN 'NA' ELSE (OLD.GLOBAL_ACCOUNT_ID % 17)::VARCHAR END, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.CONSUMER_DIM_ID != NEW.CONSUMER_DIM_ID THEN
			lv_sql := lv_sql || 'CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_CONSUMER_ACCT_ID IS DISTINCT FROM NEW.SOURCE_CONSUMER_ACCT_ID THEN
			lv_sql := lv_sql || 'SOURCE_CONSUMER_ACCT_ID = $2.SOURCE_CONSUMER_ACCT_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_CONSUMER_ACCT_ID IS NOT DISTINCT FROM $1.SOURCE_CONSUMER_ACCT_ID OR SOURCE_CONSUMER_ACCT_ID IS NOT DISTINCT FROM $2.SOURCE_CONSUMER_ACCT_ID)';
		END IF;
		IF OLD.CARD_TYPE != NEW.CARD_TYPE THEN
			lv_sql := lv_sql || 'CARD_TYPE = $2.CARD_TYPE, ';
			lv_filter := lv_filter || ' AND (CARD_TYPE = $1.CARD_TYPE OR CARD_TYPE = $2.CARD_TYPE)';
		END IF;
		IF OLD.GLOBAL_ACCOUNT_ID IS DISTINCT FROM NEW.GLOBAL_ACCOUNT_ID THEN
			lv_sql := lv_sql || 'GLOBAL_ACCOUNT_ID = $2.GLOBAL_ACCOUNT_ID, ';
			lv_filter := lv_filter || ' AND (GLOBAL_ACCOUNT_ID IS NOT DISTINCT FROM $1.GLOBAL_ACCOUNT_ID OR GLOBAL_ACCOUNT_ID IS NOT DISTINCT FROM $2.GLOBAL_ACCOUNT_ID)';
		END IF;
		IF OLD.SAFE_CARD_NUMBER != NEW.SAFE_CARD_NUMBER THEN
			lv_sql := lv_sql || 'SAFE_CARD_NUMBER = $2.SAFE_CARD_NUMBER, ';
			lv_filter := lv_filter || ' AND (SAFE_CARD_NUMBER = $1.SAFE_CARD_NUMBER OR SAFE_CARD_NUMBER = $2.SAFE_CARD_NUMBER)';
		END IF;
		IF OLD.CONSUMER_ACCT_IDENTIFIER IS DISTINCT FROM NEW.CONSUMER_ACCT_IDENTIFIER THEN
			lv_sql := lv_sql || 'CONSUMER_ACCT_IDENTIFIER = $2.CONSUMER_ACCT_IDENTIFIER, ';
			lv_filter := lv_filter || ' AND (CONSUMER_ACCT_IDENTIFIER IS NOT DISTINCT FROM $1.CONSUMER_ACCT_IDENTIFIER OR CONSUMER_ACCT_IDENTIFIER IS NOT DISTINCT FROM $2.CONSUMER_ACCT_IDENTIFIER)';
		END IF;
		IF OLD.CONSUMER_NAME != NEW.CONSUMER_NAME THEN
			lv_sql := lv_sql || 'CONSUMER_NAME = $2.CONSUMER_NAME, ';
			lv_filter := lv_filter || ' AND (CONSUMER_NAME = $1.CONSUMER_NAME OR CONSUMER_NAME = $2.CONSUMER_NAME)';
		END IF;
		IF OLD.CONSUMER_IDENTIFIER IS DISTINCT FROM NEW.CONSUMER_IDENTIFIER THEN
			lv_sql := lv_sql || 'CONSUMER_IDENTIFIER = $2.CONSUMER_IDENTIFIER, ';
			lv_filter := lv_filter || ' AND (CONSUMER_IDENTIFIER IS NOT DISTINCT FROM $1.CONSUMER_IDENTIFIER OR CONSUMER_IDENTIFIER IS NOT DISTINCT FROM $2.CONSUMER_IDENTIFIER)';
		END IF;
		IF OLD.ACCOUNT_STATUS != NEW.ACCOUNT_STATUS THEN
			lv_sql := lv_sql || 'ACCOUNT_STATUS = $2.ACCOUNT_STATUS, ';
			lv_filter := lv_filter || ' AND (ACCOUNT_STATUS = $1.ACCOUNT_STATUS OR ACCOUNT_STATUS = $2.ACCOUNT_STATUS)';
		END IF;
		IF OLD.LAST_AUTH_TS IS DISTINCT FROM NEW.LAST_AUTH_TS THEN
			lv_sql := lv_sql || 'LAST_AUTH_TS = $2.LAST_AUTH_TS, ';
			lv_filter := lv_filter || ' AND (LAST_AUTH_TS IS NOT DISTINCT FROM $1.LAST_AUTH_TS OR LAST_AUTH_TS IS NOT DISTINCT FROM $2.LAST_AUTH_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE CONSUMER_DIM_ID = $1.CONSUMER_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.GLOBAL_ACCOUNT_ID IS NULL THEN 'GLOBAL_ACCOUNT_ID IS NULL' ELSE 'GLOBAL_ACCOUNT_ID % 17 = ' || (NEW.GLOBAL_ACCOUNT_ID % 17)::VARCHAR END||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(CONSUMER_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._CONSUMER_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_CONSUMER_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE CONSUMER_DIM_ID = $1.CONSUMER_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.CONSUMER_DIM
	AS SELECT * FROM RDW._CONSUMER_DIM;

ALTER VIEW RDW.CONSUMER_DIM ALTER CONSUMER_DIM_ID SET DEFAULT nextval('RDW._consumer_dim_consumer_dim_id_seq'::regclass);
ALTER VIEW RDW.CONSUMER_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.CONSUMER_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.CONSUMER_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.CONSUMER_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.CONSUMER_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._CONSUMER_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_CONSUMER_DIM ON RDW.CONSUMER_DIM;
CREATE TRIGGER TRIIUD_CONSUMER_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.CONSUMER_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_CONSUMER_DIM();


CREATE INDEX IX_CONSUMER_DIM_GLOBAL_ACCOUNT_ID ON RDW._CONSUMER_DIM(GLOBAL_ACCOUNT_ID) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX AK_CONSUMER_DIM ON RDW._CONSUMER_DIM(SOURCE_CONSUMER_ACCT_ID, CARD_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CONSUMER_DIM_SOURCE_CONSUMER_ACCT_ID ON RDW._CONSUMER_DIM(SOURCE_CONSUMER_ACCT_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CONSUMER_DIM_CARD_TYPE ON RDW._CONSUMER_DIM(CARD_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CONSUMER_DIM_SAFE_CARD_NUMBER ON RDW._CONSUMER_DIM(SAFE_CARD_NUMBER) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CONSUMER_DIM_CONSUMER_ACCT_IDENTIFIER ON RDW._CONSUMER_DIM(CONSUMER_ACCT_IDENTIFIER) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CONSUMER_DIM_CONSUMER_NAME ON RDW._CONSUMER_DIM(CONSUMER_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CONSUMER_DIM_CREATED_TS ON RDW._CONSUMER_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.CONSUMER_DIM ----------------------------------

INSERT INTO RDW.CONSUMER_DIM(
	CONSUMER_DIM_ID,
	GLOBAL_ACCOUNT_ID,
	SOURCE_CONSUMER_ACCT_ID,
	CARD_TYPE,
	SAFE_CARD_NUMBER,
	CONSUMER_ACCT_IDENTIFIER,
	CONSUMER_NAME,
	CONSUMER_IDENTIFIER,
	ACCOUNT_STATUS,
	REVISION)
VALUES(
	0,
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	NULL,
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_CONSUMER_DIM(
	p_consumer_dim_id OUT BIGINT, 
	p_global_account_id BIGINT, 
	p_source_consumer_acct_id BIGINT, 
	p_card_type VARCHAR(100), 
	p_safe_card_number VARCHAR(100), 
	p_consumer_acct_identifier BIGINT, 
	p_consumer_name VARCHAR(100), 
	p_consumer_identifier VARCHAR(100), 
	p_account_status VARCHAR(50), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT CONSUMER_DIM_ID, REVISION
			  INTO p_consumer_dim_id, p_old_revision
			  FROM RDW.CONSUMER_DIM
			 WHERE ((SOURCE_CONSUMER_ACCT_ID IS NOT NULL AND SOURCE_CONSUMER_ACCT_ID = p_source_consumer_acct_id) OR (SOURCE_CONSUMER_ACCT_ID IS NULL AND p_source_consumer_acct_id IS NULL))
			   AND CARD_TYPE = p_card_type;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.CONSUMER_DIM
			   SET GLOBAL_ACCOUNT_ID = p_global_account_id,
			       SAFE_CARD_NUMBER = p_safe_card_number,
			       CONSUMER_ACCT_IDENTIFIER = p_consumer_acct_identifier,
			       CONSUMER_NAME = p_consumer_name,
			       CONSUMER_IDENTIFIER = p_consumer_identifier,
			       ACCOUNT_STATUS = p_account_status,
			       REVISION = p_revision
			 WHERE ((SOURCE_CONSUMER_ACCT_ID IS NOT NULL AND SOURCE_CONSUMER_ACCT_ID = p_source_consumer_acct_id) OR (SOURCE_CONSUMER_ACCT_ID IS NULL AND p_source_consumer_acct_id IS NULL))
			   AND CARD_TYPE = p_card_type
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.CONSUMER_DIM(SOURCE_CONSUMER_ACCT_ID, CARD_TYPE, GLOBAL_ACCOUNT_ID, SAFE_CARD_NUMBER, CONSUMER_ACCT_IDENTIFIER, CONSUMER_NAME, CONSUMER_IDENTIFIER, ACCOUNT_STATUS, REVISION)
					 VALUES(p_source_consumer_acct_id, p_card_type, p_global_account_id, p_safe_card_number, p_consumer_acct_identifier, p_consumer_name, p_consumer_identifier, p_account_status, p_revision)
					 RETURNING CONSUMER_DIM_ID
					      INTO p_consumer_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_CONSUMER_DIM(
	p_consumer_dim_id OUT BIGINT, 
	p_global_account_id BIGINT, 
	p_source_consumer_acct_id BIGINT, 
	p_card_type VARCHAR(100), 
	p_safe_card_number VARCHAR(100), 
	p_consumer_acct_identifier BIGINT, 
	p_consumer_name VARCHAR(100), 
	p_consumer_identifier VARCHAR(100), 
	p_account_status VARCHAR(50), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.CURRENCY_DIM
  (
    CURRENCY_DIM_ID SMALLSERIAL  NOT NULL,
    CURRENCY_CODE   CHAR(3)      NOT NULL,
    CURRENCY_SYMBOL CHAR(1)      NOT NULL,
    CURRENCY_NAME   VARCHAR(100) NOT NULL,

    CONSTRAINT PK_CURRENCY_DIM PRIMARY KEY(CURRENCY_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.CURRENCY_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.CURRENCY_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_CURRENCY_DIM ON RDW.CURRENCY_DIM;


CREATE TRIGGER TRBI_CURRENCY_DIM
BEFORE INSERT ON RDW.CURRENCY_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_CURRENCY_DIM ON RDW.CURRENCY_DIM;


CREATE TRIGGER TRBU_CURRENCY_DIM
BEFORE UPDATE ON RDW.CURRENCY_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE UNIQUE INDEX AK_CURRENCY_DIM ON RDW.CURRENCY_DIM(CURRENCY_CODE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_CURRENCY_DIM_CREATED_TS ON RDW.CURRENCY_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.CURRENCY_DIM ----------------------------------

INSERT INTO RDW.CURRENCY_DIM(
	CURRENCY_DIM_ID,
	CURRENCY_CODE,
	CURRENCY_SYMBOL,
	CURRENCY_NAME,
	REVISION)
VALUES(
	0,
	'---',
	'-',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_CURRENCY_DIM(
	p_currency_dim_id OUT SMALLINT, 
	p_currency_code CHAR(3), 
	p_currency_symbol CHAR(1), 
	p_currency_name VARCHAR(100), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT CURRENCY_DIM_ID, REVISION
			  INTO p_currency_dim_id, p_old_revision
			  FROM RDW.CURRENCY_DIM
			 WHERE CURRENCY_CODE = p_currency_code;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.CURRENCY_DIM
			   SET CURRENCY_SYMBOL = p_currency_symbol,
			       CURRENCY_NAME = p_currency_name,
			       REVISION = p_revision
			 WHERE CURRENCY_CODE = p_currency_code
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.CURRENCY_DIM(CURRENCY_CODE, CURRENCY_SYMBOL, CURRENCY_NAME, REVISION)
					 VALUES(p_currency_code, p_currency_symbol, p_currency_name, p_revision)
					 RETURNING CURRENCY_DIM_ID
					      INTO p_currency_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_CURRENCY_DIM(
	p_currency_dim_id OUT SMALLINT, 
	p_currency_code CHAR(3), 
	p_currency_symbol CHAR(1), 
	p_currency_name VARCHAR(100), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.DATE_DIM
  (
    DATE_DIM_ID     SMALLINT    NOT NULL,
    ACTUAL_DATE     DATE        ,
    ACTUAL_MONTH    DATE        ,
    YEAR_NUM        SMALLINT    ,
    MONTH_NUM       SMALLINT    ,
    MONTH_NAME      VARCHAR(50) NOT NULL,
    DAY_OF_MONTH    SMALLINT    ,
    DAY_OF_WEEK     SMALLINT    ,
    WEEK_DAY        VARCHAR(50) NOT NULL,
    DAY_OF_YEAR     SMALLINT    ,
    WEEK_OF_YEAR    SMALLINT    ,
    WEEK_START_DATE DATE        ,
    WEEK_END_DATE   DATE        ,
    QUARTER         VARCHAR(10) NOT NULL,
    US_HOLIDAY_NAME VARCHAR(50) NOT NULL,

    CONSTRAINT PK_DATE_DIM PRIMARY KEY(DATE_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.DATE_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.DATE_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_DATE_DIM ON RDW.DATE_DIM;


CREATE TRIGGER TRBI_DATE_DIM
BEFORE INSERT ON RDW.DATE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_DATE_DIM ON RDW.DATE_DIM;


CREATE TRIGGER TRBU_DATE_DIM
BEFORE UPDATE ON RDW.DATE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE UNIQUE INDEX AK_DATE_DIM ON RDW.DATE_DIM(ACTUAL_DATE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DATE_DIM_ACTUAL_MONTH ON RDW.DATE_DIM(ACTUAL_MONTH) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DATE_DIM_YEAR_NUM ON RDW.DATE_DIM(YEAR_NUM) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DATE_DIM_WEEK_DAY ON RDW.DATE_DIM(WEEK_DAY) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DATE_DIM_US_HOLIDAY_NAME ON RDW.DATE_DIM(US_HOLIDAY_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DATE_DIM_CREATED_TS ON RDW.DATE_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.DATE_DIM ----------------------------------

INSERT INTO RDW.DATE_DIM(
	DATE_DIM_ID,
	ACTUAL_DATE,
	ACTUAL_MONTH,
	YEAR_NUM,
	MONTH_NUM,
	MONTH_NAME,
	DAY_OF_MONTH,
	DAY_OF_WEEK,
	WEEK_DAY,
	DAY_OF_YEAR,
	WEEK_OF_YEAR,
	WEEK_START_DATE,
	WEEK_END_DATE,
	QUARTER,
	US_HOLIDAY_NAME,
	REVISION)
VALUES(
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	'Unknown',
	NULL,
	NULL,
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	1);


INSERT INTO RDW.DATE_DIM(
	DATE_DIM_ID,
	ACTUAL_DATE,
	ACTUAL_MONTH,
	YEAR_NUM,
	MONTH_NUM,
	MONTH_NAME,
	DAY_OF_MONTH,
	DAY_OF_WEEK,
	WEEK_DAY,
	DAY_OF_YEAR,
	WEEK_OF_YEAR,
	WEEK_START_DATE,
	WEEK_END_DATE,
	QUARTER,
	US_HOLIDAY_NAME,
	REVISION)
SELECT 
	N,
	ACTUAL_DATE,
	CAST(DATE_TRUNC('MONTH', ACTUAL_DATE) AS DATE),
	EXTRACT(YEAR FROM ACTUAL_DATE),
	EXTRACT(MONTH FROM ACTUAL_DATE),
	TO_CHAR(ACTUAL_DATE, 'FMMonth'),
	EXTRACT(DAY FROM ACTUAL_DATE),
	EXTRACT(DOW FROM ACTUAL_DATE) + 1,
	TO_CHAR(ACTUAL_DATE, 'FMDay'),
	EXTRACT(DOY FROM ACTUAL_DATE),
	EXTRACT(WEEK FROM ACTUAL_DATE + 1),
	ACTUAL_DATE - CAST(EXTRACT(DOW FROM ACTUAL_DATE) AS INTEGER),
	ACTUAL_DATE - CAST(EXTRACT(DOW FROM ACTUAL_DATE) AS INTEGER),
	TO_CHAR(ACTUAL_DATE, 'FMQth'),
	CASE WHEN EXTRACT(DOY FROM ACTUAL_DATE) = 1 AND EXTRACT(DOW FROM ACTUAL_DATE) NOT IN(0,6) THEN 'New Year''s Day' WHEN EXTRACT(DOY FROM ACTUAL_DATE) = 2 AND EXTRACT(DOW FROM ACTUAL_DATE) = 1 THEN 'New Year''s Day' WHEN EXTRACT(DOY FROM ACTUAL_DATE + 1) = 1 AND EXTRACT(DOW FROM ACTUAL_DATE) = 5 THEN 'New Year''s Day' WHEN EXTRACT(DOW FROM ACTUAL_DATE) = 1 AND EXTRACT(MONTH FROM ACTUAL_DATE) = 1 AND EXTRACT(DAY FROM ACTUAL_DATE) BETWEEN 15 AND 21 THEN 'Martin Luther King Day' WHEN EXTRACT(DOW FROM ACTUAL_DATE) = 1 AND EXTRACT(MONTH FROM ACTUAL_DATE) = 2 AND EXTRACT(DAY FROM ACTUAL_DATE) BETWEEN 15 AND 21 THEN 'Washington''s Birthday' WHEN EXTRACT(DOW FROM ACTUAL_DATE) = 1 AND EXTRACT(MONTH FROM ACTUAL_DATE) = 5 AND EXTRACT(DAY FROM ACTUAL_DATE) BETWEEN 25 AND 31 THEN 'Memorial Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 7 AND EXTRACT(DAY FROM ACTUAL_DATE) = 4 AND EXTRACT(DOW FROM ACTUAL_DATE) NOT IN(0,6) THEN 'Independence Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 7 AND EXTRACT(DAY FROM ACTUAL_DATE) = 5 AND EXTRACT(DOW FROM ACTUAL_DATE) = 1 THEN 'Independence Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 7 AND EXTRACT(DAY FROM ACTUAL_DATE) = 3 AND EXTRACT(DOW FROM ACTUAL_DATE) = 5 THEN 'Independence Day' WHEN EXTRACT(DOW FROM ACTUAL_DATE) = 1 AND EXTRACT(MONTH FROM ACTUAL_DATE) = 9 AND EXTRACT(DAY FROM ACTUAL_DATE) BETWEEN 1 AND 7 THEN 'Labor Day' WHEN EXTRACT(DOW FROM ACTUAL_DATE) = 1 AND EXTRACT(MONTH FROM ACTUAL_DATE) = 10 AND EXTRACT(DAY FROM ACTUAL_DATE) BETWEEN 8 AND 14 THEN 'Columbus Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 11 AND EXTRACT(DAY FROM ACTUAL_DATE) = 11 AND EXTRACT(DOW FROM ACTUAL_DATE) NOT IN(0,6) THEN 'Veterans Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 11 AND EXTRACT(DAY FROM ACTUAL_DATE) = 12 AND EXTRACT(DOW FROM ACTUAL_DATE) = 1 THEN 'Veterans Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 11 AND EXTRACT(DAY FROM ACTUAL_DATE) = 10 AND EXTRACT(DOW FROM ACTUAL_DATE) = 5 THEN 'Veterans Day' WHEN EXTRACT(DOW FROM ACTUAL_DATE) = 4 AND EXTRACT(MONTH FROM ACTUAL_DATE) = 11 AND EXTRACT(DAY FROM ACTUAL_DATE) BETWEEN 22 AND 28 THEN 'Thanksgiving Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 12 AND EXTRACT(DAY FROM ACTUAL_DATE) = 25 AND EXTRACT(DOW FROM ACTUAL_DATE) NOT IN(0,6) THEN 'Christmas Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 12 AND EXTRACT(DAY FROM ACTUAL_DATE) = 26 AND EXTRACT(DOW FROM ACTUAL_DATE) = 1 THEN 'Christmas Day' WHEN EXTRACT(MONTH FROM ACTUAL_DATE) = 12 AND EXTRACT(DAY FROM ACTUAL_DATE) = 24 AND EXTRACT(DOW FROM ACTUAL_DATE) = 5 THEN 'Christmas Day' ELSE '--' END ,
	1
FROM (SELECT N, DATE'epoch' + N ACTUAL_DATE FROM GENERATE_SERIES(11688, 17166) N) A;


-- ======================================================================

CREATE TABLE RDW._HOST_DIM
  (
    HOST_DIM_ID                 BIGSERIAL    NOT NULL,
    SOURCE_HOST_ID              BIGINT       NOT NULL,
    SOURCE_HOST_TYPE_ID         INTEGER      ,
    SOURCE_HOST_STATUS_CD       CHAR(1)      NOT NULL,
    HOST_SERIAL_CD              VARCHAR(20)  ,
    HOST_LABEL                  VARCHAR(100) NOT NULL,
    HOST_TYPE                   VARCHAR(50)  NOT NULL,
    HOST_POSITION               VARCHAR(50)  NOT NULL,
    HOST_CLASSIFICATION         VARCHAR(200) NOT NULL,
    MANUFACTURER                VARCHAR(50)  NOT NULL,
    MODEL                       VARCHAR(50)  NOT NULL,
    FIRMWARE_VERSION            VARCHAR(255) NOT NULL,
    HOST_STATE                  VARCHAR(30)  NOT NULL,
    LAST_HOST_START_TS          TIMESTAMPTZ  ,
    LAST_FILL_TS                TIMESTAMPTZ  ,
    LAST_TRAN_START_TS          TIMESTAMPTZ  ,
    LAST_TRAN_END_TS            TIMESTAMPTZ  ,
    ACTIVE_FLAG                 VARCHAR(10)  NOT NULL,
    BASE_HOST_FLAG              VARCHAR(10)  NOT NULL,
    ESUDS_WASHER_FLAG           VARCHAR(10)  NOT NULL,
    ESUDS_DRYER_FLAG            VARCHAR(10)  NOT NULL,
    ESUDS_STACKED_DRYER_FLAG    VARCHAR(10)  NOT NULL,
    ESUDS_STACKED_WASH_DRY_FLAG VARCHAR(10)  NOT NULL,

    CONSTRAINT PK_HOST_DIM PRIMARY KEY(HOST_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_HOST_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((NEW.SOURCE_HOST_ID % 17)::VARCHAR,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_HOST_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_HOST_DIM', COALESCE((OLD.SOURCE_HOST_ID % 17)::VARCHAR, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.HOST_DIM_ID != NEW.HOST_DIM_ID THEN
			lv_sql := lv_sql || 'HOST_DIM_ID = $2.HOST_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_HOST_ID != NEW.SOURCE_HOST_ID THEN
			lv_sql := lv_sql || 'SOURCE_HOST_ID = $2.SOURCE_HOST_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_HOST_ID = $1.SOURCE_HOST_ID OR SOURCE_HOST_ID = $2.SOURCE_HOST_ID)';
		END IF;
		IF OLD.SOURCE_HOST_TYPE_ID IS DISTINCT FROM NEW.SOURCE_HOST_TYPE_ID THEN
			lv_sql := lv_sql || 'SOURCE_HOST_TYPE_ID = $2.SOURCE_HOST_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_HOST_TYPE_ID IS NOT DISTINCT FROM $1.SOURCE_HOST_TYPE_ID OR SOURCE_HOST_TYPE_ID IS NOT DISTINCT FROM $2.SOURCE_HOST_TYPE_ID)';
		END IF;
		IF OLD.SOURCE_HOST_STATUS_CD != NEW.SOURCE_HOST_STATUS_CD THEN
			lv_sql := lv_sql || 'SOURCE_HOST_STATUS_CD = $2.SOURCE_HOST_STATUS_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_HOST_STATUS_CD = $1.SOURCE_HOST_STATUS_CD OR SOURCE_HOST_STATUS_CD = $2.SOURCE_HOST_STATUS_CD)';
		END IF;
		IF OLD.HOST_SERIAL_CD IS DISTINCT FROM NEW.HOST_SERIAL_CD THEN
			lv_sql := lv_sql || 'HOST_SERIAL_CD = $2.HOST_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (HOST_SERIAL_CD IS NOT DISTINCT FROM $1.HOST_SERIAL_CD OR HOST_SERIAL_CD IS NOT DISTINCT FROM $2.HOST_SERIAL_CD)';
		END IF;
		IF OLD.HOST_LABEL != NEW.HOST_LABEL THEN
			lv_sql := lv_sql || 'HOST_LABEL = $2.HOST_LABEL, ';
			lv_filter := lv_filter || ' AND (HOST_LABEL = $1.HOST_LABEL OR HOST_LABEL = $2.HOST_LABEL)';
		END IF;
		IF OLD.HOST_TYPE != NEW.HOST_TYPE THEN
			lv_sql := lv_sql || 'HOST_TYPE = $2.HOST_TYPE, ';
			lv_filter := lv_filter || ' AND (HOST_TYPE = $1.HOST_TYPE OR HOST_TYPE = $2.HOST_TYPE)';
		END IF;
		IF OLD.HOST_POSITION != NEW.HOST_POSITION THEN
			lv_sql := lv_sql || 'HOST_POSITION = $2.HOST_POSITION, ';
			lv_filter := lv_filter || ' AND (HOST_POSITION = $1.HOST_POSITION OR HOST_POSITION = $2.HOST_POSITION)';
		END IF;
		IF OLD.HOST_CLASSIFICATION != NEW.HOST_CLASSIFICATION THEN
			lv_sql := lv_sql || 'HOST_CLASSIFICATION = $2.HOST_CLASSIFICATION, ';
			lv_filter := lv_filter || ' AND (HOST_CLASSIFICATION = $1.HOST_CLASSIFICATION OR HOST_CLASSIFICATION = $2.HOST_CLASSIFICATION)';
		END IF;
		IF OLD.MANUFACTURER != NEW.MANUFACTURER THEN
			lv_sql := lv_sql || 'MANUFACTURER = $2.MANUFACTURER, ';
			lv_filter := lv_filter || ' AND (MANUFACTURER = $1.MANUFACTURER OR MANUFACTURER = $2.MANUFACTURER)';
		END IF;
		IF OLD.MODEL != NEW.MODEL THEN
			lv_sql := lv_sql || 'MODEL = $2.MODEL, ';
			lv_filter := lv_filter || ' AND (MODEL = $1.MODEL OR MODEL = $2.MODEL)';
		END IF;
		IF OLD.FIRMWARE_VERSION != NEW.FIRMWARE_VERSION THEN
			lv_sql := lv_sql || 'FIRMWARE_VERSION = $2.FIRMWARE_VERSION, ';
			lv_filter := lv_filter || ' AND (FIRMWARE_VERSION = $1.FIRMWARE_VERSION OR FIRMWARE_VERSION = $2.FIRMWARE_VERSION)';
		END IF;
		IF OLD.HOST_STATE != NEW.HOST_STATE THEN
			lv_sql := lv_sql || 'HOST_STATE = $2.HOST_STATE, ';
			lv_filter := lv_filter || ' AND (HOST_STATE = $1.HOST_STATE OR HOST_STATE = $2.HOST_STATE)';
		END IF;
		IF OLD.LAST_HOST_START_TS IS DISTINCT FROM NEW.LAST_HOST_START_TS THEN
			lv_sql := lv_sql || 'LAST_HOST_START_TS = $2.LAST_HOST_START_TS, ';
			lv_filter := lv_filter || ' AND (LAST_HOST_START_TS IS NOT DISTINCT FROM $1.LAST_HOST_START_TS OR LAST_HOST_START_TS IS NOT DISTINCT FROM $2.LAST_HOST_START_TS)';
		END IF;
		IF OLD.LAST_FILL_TS IS DISTINCT FROM NEW.LAST_FILL_TS THEN
			lv_sql := lv_sql || 'LAST_FILL_TS = $2.LAST_FILL_TS, ';
			lv_filter := lv_filter || ' AND (LAST_FILL_TS IS NOT DISTINCT FROM $1.LAST_FILL_TS OR LAST_FILL_TS IS NOT DISTINCT FROM $2.LAST_FILL_TS)';
		END IF;
		IF OLD.LAST_TRAN_START_TS IS DISTINCT FROM NEW.LAST_TRAN_START_TS THEN
			lv_sql := lv_sql || 'LAST_TRAN_START_TS = $2.LAST_TRAN_START_TS, ';
			lv_filter := lv_filter || ' AND (LAST_TRAN_START_TS IS NOT DISTINCT FROM $1.LAST_TRAN_START_TS OR LAST_TRAN_START_TS IS NOT DISTINCT FROM $2.LAST_TRAN_START_TS)';
		END IF;
		IF OLD.LAST_TRAN_END_TS IS DISTINCT FROM NEW.LAST_TRAN_END_TS THEN
			lv_sql := lv_sql || 'LAST_TRAN_END_TS = $2.LAST_TRAN_END_TS, ';
			lv_filter := lv_filter || ' AND (LAST_TRAN_END_TS IS NOT DISTINCT FROM $1.LAST_TRAN_END_TS OR LAST_TRAN_END_TS IS NOT DISTINCT FROM $2.LAST_TRAN_END_TS)';
		END IF;
		IF OLD.ACTIVE_FLAG != NEW.ACTIVE_FLAG THEN
			lv_sql := lv_sql || 'ACTIVE_FLAG = $2.ACTIVE_FLAG, ';
			lv_filter := lv_filter || ' AND (ACTIVE_FLAG = $1.ACTIVE_FLAG OR ACTIVE_FLAG = $2.ACTIVE_FLAG)';
		END IF;
		IF OLD.BASE_HOST_FLAG != NEW.BASE_HOST_FLAG THEN
			lv_sql := lv_sql || 'BASE_HOST_FLAG = $2.BASE_HOST_FLAG, ';
			lv_filter := lv_filter || ' AND (BASE_HOST_FLAG = $1.BASE_HOST_FLAG OR BASE_HOST_FLAG = $2.BASE_HOST_FLAG)';
		END IF;
		IF OLD.ESUDS_WASHER_FLAG != NEW.ESUDS_WASHER_FLAG THEN
			lv_sql := lv_sql || 'ESUDS_WASHER_FLAG = $2.ESUDS_WASHER_FLAG, ';
			lv_filter := lv_filter || ' AND (ESUDS_WASHER_FLAG = $1.ESUDS_WASHER_FLAG OR ESUDS_WASHER_FLAG = $2.ESUDS_WASHER_FLAG)';
		END IF;
		IF OLD.ESUDS_DRYER_FLAG != NEW.ESUDS_DRYER_FLAG THEN
			lv_sql := lv_sql || 'ESUDS_DRYER_FLAG = $2.ESUDS_DRYER_FLAG, ';
			lv_filter := lv_filter || ' AND (ESUDS_DRYER_FLAG = $1.ESUDS_DRYER_FLAG OR ESUDS_DRYER_FLAG = $2.ESUDS_DRYER_FLAG)';
		END IF;
		IF OLD.ESUDS_STACKED_DRYER_FLAG != NEW.ESUDS_STACKED_DRYER_FLAG THEN
			lv_sql := lv_sql || 'ESUDS_STACKED_DRYER_FLAG = $2.ESUDS_STACKED_DRYER_FLAG, ';
			lv_filter := lv_filter || ' AND (ESUDS_STACKED_DRYER_FLAG = $1.ESUDS_STACKED_DRYER_FLAG OR ESUDS_STACKED_DRYER_FLAG = $2.ESUDS_STACKED_DRYER_FLAG)';
		END IF;
		IF OLD.ESUDS_STACKED_WASH_DRY_FLAG != NEW.ESUDS_STACKED_WASH_DRY_FLAG THEN
			lv_sql := lv_sql || 'ESUDS_STACKED_WASH_DRY_FLAG = $2.ESUDS_STACKED_WASH_DRY_FLAG, ';
			lv_filter := lv_filter || ' AND (ESUDS_STACKED_WASH_DRY_FLAG = $1.ESUDS_STACKED_WASH_DRY_FLAG OR ESUDS_STACKED_WASH_DRY_FLAG = $2.ESUDS_STACKED_WASH_DRY_FLAG)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE HOST_DIM_ID = $1.HOST_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'SOURCE_HOST_ID % 17 = ' || NEW.SOURCE_HOST_ID % 17||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(HOST_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._HOST_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_HOST_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE HOST_DIM_ID = $1.HOST_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.HOST_DIM
	AS SELECT * FROM RDW._HOST_DIM;

ALTER VIEW RDW.HOST_DIM ALTER HOST_DIM_ID SET DEFAULT nextval('RDW._host_dim_host_dim_id_seq'::regclass);
ALTER VIEW RDW.HOST_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.HOST_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.HOST_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.HOST_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.HOST_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._HOST_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_HOST_DIM ON RDW.HOST_DIM;
CREATE TRIGGER TRIIUD_HOST_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.HOST_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_HOST_DIM();


CREATE UNIQUE INDEX AK_HOST_DIM ON RDW._HOST_DIM(SOURCE_HOST_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_HOST_DIM_SOURCE_HOST_TYPE_ID ON RDW._HOST_DIM(SOURCE_HOST_TYPE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_HOST_DIM_MANUFACTURER_MODEL ON RDW._HOST_DIM(MANUFACTURER, MODEL) TABLESPACE rdw_data_t1;

CREATE INDEX IX_HOST_DIM_FIRMWARE_VERSION ON RDW._HOST_DIM(FIRMWARE_VERSION) TABLESPACE rdw_data_t1;

CREATE INDEX IX_HOST_DIM_CREATED_TS ON RDW._HOST_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.HOST_DIM ----------------------------------

INSERT INTO RDW.HOST_DIM(
	HOST_DIM_ID,
	SOURCE_HOST_ID,
	SOURCE_HOST_TYPE_ID,
	SOURCE_HOST_STATUS_CD,
	HOST_SERIAL_CD,
	HOST_LABEL,
	HOST_TYPE,
	HOST_POSITION,
	HOST_CLASSIFICATION,
	MANUFACTURER,
	MODEL,
	FIRMWARE_VERSION,
	HOST_STATE,
	LAST_HOST_START_TS,
	ACTIVE_FLAG,
	BASE_HOST_FLAG,
	ESUDS_WASHER_FLAG,
	ESUDS_DRYER_FLAG,
	ESUDS_STACKED_DRYER_FLAG,
	ESUDS_STACKED_WASH_DRY_FLAG,
	REVISION)
VALUES(
	0,
	0,
	NULL,
	'-',
	NULL,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_HOST_DIM(
	p_host_dim_id OUT BIGINT, 
	p_source_host_id BIGINT, 
	p_source_host_type_id INTEGER, 
	p_source_host_status_cd CHAR(1), 
	p_host_serial_cd VARCHAR(20), 
	p_host_label VARCHAR(100), 
	p_host_type VARCHAR(50), 
	p_host_position VARCHAR(50), 
	p_host_classification VARCHAR(200), 
	p_manufacturer VARCHAR(50), 
	p_model VARCHAR(50), 
	p_firmware_version VARCHAR(255), 
	p_host_state VARCHAR(30), 
	p_last_host_start_ts TIMESTAMPTZ, 
	p_active_flag VARCHAR(10), 
	p_base_host_flag VARCHAR(10), 
	p_esuds_washer_flag VARCHAR(10), 
	p_esuds_dryer_flag VARCHAR(10), 
	p_esuds_stacked_dryer_flag VARCHAR(10), 
	p_esuds_stacked_wash_dry_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT HOST_DIM_ID, REVISION
			  INTO p_host_dim_id, p_old_revision
			  FROM RDW.HOST_DIM
			 WHERE SOURCE_HOST_ID = p_source_host_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.HOST_DIM
			   SET SOURCE_HOST_TYPE_ID = p_source_host_type_id,
			       SOURCE_HOST_STATUS_CD = p_source_host_status_cd,
			       HOST_SERIAL_CD = p_host_serial_cd,
			       HOST_LABEL = p_host_label,
			       HOST_TYPE = p_host_type,
			       HOST_POSITION = p_host_position,
			       HOST_CLASSIFICATION = p_host_classification,
			       MANUFACTURER = p_manufacturer,
			       MODEL = p_model,
			       FIRMWARE_VERSION = p_firmware_version,
			       HOST_STATE = p_host_state,
			       LAST_HOST_START_TS = p_last_host_start_ts,
			       ACTIVE_FLAG = p_active_flag,
			       BASE_HOST_FLAG = p_base_host_flag,
			       ESUDS_WASHER_FLAG = p_esuds_washer_flag,
			       ESUDS_DRYER_FLAG = p_esuds_dryer_flag,
			       ESUDS_STACKED_DRYER_FLAG = p_esuds_stacked_dryer_flag,
			       ESUDS_STACKED_WASH_DRY_FLAG = p_esuds_stacked_wash_dry_flag,
			       REVISION = p_revision
			 WHERE SOURCE_HOST_ID = p_source_host_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.HOST_DIM(SOURCE_HOST_ID, SOURCE_HOST_TYPE_ID, SOURCE_HOST_STATUS_CD, HOST_SERIAL_CD, HOST_LABEL, HOST_TYPE, HOST_POSITION, HOST_CLASSIFICATION, MANUFACTURER, MODEL, FIRMWARE_VERSION, HOST_STATE, LAST_HOST_START_TS, ACTIVE_FLAG, BASE_HOST_FLAG, ESUDS_WASHER_FLAG, ESUDS_DRYER_FLAG, ESUDS_STACKED_DRYER_FLAG, ESUDS_STACKED_WASH_DRY_FLAG, REVISION)
					 VALUES(p_source_host_id, p_source_host_type_id, p_source_host_status_cd, p_host_serial_cd, p_host_label, p_host_type, p_host_position, p_host_classification, p_manufacturer, p_model, p_firmware_version, p_host_state, p_last_host_start_ts, p_active_flag, p_base_host_flag, p_esuds_washer_flag, p_esuds_dryer_flag, p_esuds_stacked_dryer_flag, p_esuds_stacked_wash_dry_flag, p_revision)
					 RETURNING HOST_DIM_ID
					      INTO p_host_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_HOST_DIM(
	p_host_dim_id OUT BIGINT, 
	p_source_host_id BIGINT, 
	p_source_host_type_id INTEGER, 
	p_source_host_status_cd CHAR(1), 
	p_host_serial_cd VARCHAR(20), 
	p_host_label VARCHAR(100), 
	p_host_type VARCHAR(50), 
	p_host_position VARCHAR(50), 
	p_host_classification VARCHAR(200), 
	p_manufacturer VARCHAR(50), 
	p_model VARCHAR(50), 
	p_firmware_version VARCHAR(255), 
	p_host_state VARCHAR(30), 
	p_last_host_start_ts TIMESTAMPTZ, 
	p_active_flag VARCHAR(10), 
	p_base_host_flag VARCHAR(10), 
	p_esuds_washer_flag VARCHAR(10), 
	p_esuds_dryer_flag VARCHAR(10), 
	p_esuds_stacked_dryer_flag VARCHAR(10), 
	p_esuds_stacked_wash_dry_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.TRAN_LINE_ITEM_DIM
  (
    TRAN_LINE_ITEM_DIM_ID   BIGSERIAL     NOT NULL,
    SOURCE_TLI_TYPE_ID      INTEGER       NOT NULL,
    SOURCE_TLI_POSITION_CD  VARCHAR(6)    ,
    SOURCE_TLI_DESC         VARCHAR(60)   ,
    TRAN_LINE_ITEM_TYPE     VARCHAR(60)   NOT NULL,
    TRAN_LINE_ITEM_DESC     VARCHAR(4000) NOT NULL,
    TRAN_LINE_ITEM_PRODUCT  VARCHAR(60)   NOT NULL,
    TRAN_LINE_ITEM_CATEGORY VARCHAR(60)   NOT NULL,
    TRAN_LINE_ITEM_LABEL    VARCHAR(60)   NOT NULL,

    CONSTRAINT PK_TRAN_LINE_ITEM_DIM PRIMARY KEY(TRAN_LINE_ITEM_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.TRAN_LINE_ITEM_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.TRAN_LINE_ITEM_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_TRAN_LINE_ITEM_DIM ON RDW.TRAN_LINE_ITEM_DIM;


CREATE TRIGGER TRBI_TRAN_LINE_ITEM_DIM
BEFORE INSERT ON RDW.TRAN_LINE_ITEM_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_TRAN_LINE_ITEM_DIM ON RDW.TRAN_LINE_ITEM_DIM;


CREATE TRIGGER TRBU_TRAN_LINE_ITEM_DIM
BEFORE UPDATE ON RDW.TRAN_LINE_ITEM_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE UNIQUE INDEX AK_TRAN_LINE_ITEM_DIM ON RDW.TRAN_LINE_ITEM_DIM(SOURCE_TLI_TYPE_ID, SOURCE_TLI_POSITION_CD, SOURCE_TLI_DESC, TRAN_LINE_ITEM_TYPE, TRAN_LINE_ITEM_DESC, TRAN_LINE_ITEM_PRODUCT, TRAN_LINE_ITEM_CATEGORY, TRAN_LINE_ITEM_LABEL) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_LINE_ITEM_DIM_TRAN_LINE_ITEM_TYPE ON RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_LINE_ITEM_DIM_TRAN_LINE_ITEM_DESC ON RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_DESC) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_LINE_ITEM_DIM_TRAN_LINE_ITEM_PRODUCT ON RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_PRODUCT) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_LINE_ITEM_DIM_TRAN_LINE_ITEM_CATEGORY ON RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_CATEGORY) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_LINE_ITEM_DIM_TRAN_LINE_ITEM_LABEL ON RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_LABEL) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_LINE_ITEM_DIM_CREATED_TS ON RDW.TRAN_LINE_ITEM_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.TRAN_LINE_ITEM_DIM ----------------------------------

INSERT INTO RDW.TRAN_LINE_ITEM_DIM(
	TRAN_LINE_ITEM_DIM_ID,
	SOURCE_TLI_TYPE_ID,
	SOURCE_TLI_POSITION_CD,
	SOURCE_TLI_DESC,
	TRAN_LINE_ITEM_TYPE,
	TRAN_LINE_ITEM_DESC,
	TRAN_LINE_ITEM_PRODUCT,
	TRAN_LINE_ITEM_CATEGORY,
	TRAN_LINE_ITEM_LABEL,
	REVISION)
VALUES(
	0,
	0,
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_TRAN_LINE_ITEM_DIM(
	p_tran_line_item_dim_id OUT BIGINT, 
	p_source_tli_type_id INTEGER, 
	p_source_tli_position_cd VARCHAR(6), 
	p_source_tli_desc VARCHAR(60), 
	p_tran_line_item_type VARCHAR(60), 
	p_tran_line_item_desc VARCHAR(4000), 
	p_tran_line_item_product VARCHAR(60), 
	p_tran_line_item_category VARCHAR(60), 
	p_tran_line_item_label VARCHAR(60), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT TRAN_LINE_ITEM_DIM_ID, REVISION
			  INTO p_tran_line_item_dim_id, p_old_revision
			  FROM RDW.TRAN_LINE_ITEM_DIM
			 WHERE SOURCE_TLI_TYPE_ID = p_source_tli_type_id
			   AND ((SOURCE_TLI_POSITION_CD IS NOT NULL AND SOURCE_TLI_POSITION_CD = p_source_tli_position_cd) OR (SOURCE_TLI_POSITION_CD IS NULL AND p_source_tli_position_cd IS NULL))
			   AND ((SOURCE_TLI_DESC IS NOT NULL AND SOURCE_TLI_DESC = p_source_tli_desc) OR (SOURCE_TLI_DESC IS NULL AND p_source_tli_desc IS NULL))
			   AND TRAN_LINE_ITEM_TYPE = p_tran_line_item_type
			   AND TRAN_LINE_ITEM_DESC = p_tran_line_item_desc
			   AND TRAN_LINE_ITEM_PRODUCT = p_tran_line_item_product
			   AND TRAN_LINE_ITEM_CATEGORY = p_tran_line_item_category
			   AND TRAN_LINE_ITEM_LABEL = p_tran_line_item_label;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.TRAN_LINE_ITEM_DIM
			   SET REVISION = p_revision
			 WHERE SOURCE_TLI_TYPE_ID = p_source_tli_type_id
			   AND ((SOURCE_TLI_POSITION_CD IS NOT NULL AND SOURCE_TLI_POSITION_CD = p_source_tli_position_cd) OR (SOURCE_TLI_POSITION_CD IS NULL AND p_source_tli_position_cd IS NULL))
			   AND ((SOURCE_TLI_DESC IS NOT NULL AND SOURCE_TLI_DESC = p_source_tli_desc) OR (SOURCE_TLI_DESC IS NULL AND p_source_tli_desc IS NULL))
			   AND TRAN_LINE_ITEM_TYPE = p_tran_line_item_type
			   AND TRAN_LINE_ITEM_DESC = p_tran_line_item_desc
			   AND TRAN_LINE_ITEM_PRODUCT = p_tran_line_item_product
			   AND TRAN_LINE_ITEM_CATEGORY = p_tran_line_item_category
			   AND TRAN_LINE_ITEM_LABEL = p_tran_line_item_label
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.TRAN_LINE_ITEM_DIM(SOURCE_TLI_TYPE_ID, SOURCE_TLI_POSITION_CD, SOURCE_TLI_DESC, TRAN_LINE_ITEM_TYPE, TRAN_LINE_ITEM_DESC, TRAN_LINE_ITEM_PRODUCT, TRAN_LINE_ITEM_CATEGORY, TRAN_LINE_ITEM_LABEL, REVISION)
					 VALUES(p_source_tli_type_id, p_source_tli_position_cd, p_source_tli_desc, p_tran_line_item_type, p_tran_line_item_desc, p_tran_line_item_product, p_tran_line_item_category, p_tran_line_item_label, p_revision)
					 RETURNING TRAN_LINE_ITEM_DIM_ID
					      INTO p_tran_line_item_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_TRAN_LINE_ITEM_DIM(
	p_tran_line_item_dim_id OUT BIGINT, 
	p_source_tli_type_id INTEGER, 
	p_source_tli_position_cd VARCHAR(6), 
	p_source_tli_desc VARCHAR(60), 
	p_tran_line_item_type VARCHAR(60), 
	p_tran_line_item_desc VARCHAR(4000), 
	p_tran_line_item_product VARCHAR(60), 
	p_tran_line_item_category VARCHAR(60), 
	p_tran_line_item_label VARCHAR(60), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._LOCATION_DIM
  (
    LOCATION_DIM_ID    BIGSERIAL     NOT NULL,
    SOURCE_SYSTEM_CD   VARCHAR(30)   NOT NULL,
    SOURCE_LOCATION_ID BIGINT        NOT NULL,
    SOURCE_CUSTOMER_ID BIGINT        NOT NULL,
    CUSTOMER_NAME      VARCHAR(100)  NOT NULL,
    LOCATION_NAME      VARCHAR(100)  NOT NULL,
    LOCATION_DESC      VARCHAR(4000) ,
    LOCATION_TYPE      VARCHAR(100)  NOT NULL,
    LOCATION_SUB_TYPE  VARCHAR(100)  ,
    HIERARCHY_LEVEL    SMALLINT      NOT NULL,
    TOP_MOST_FLAG      VARCHAR(10)   NOT NULL,
    BOTTOM_MOST_FLAG   VARCHAR(10)   NOT NULL,
    ADDRESS1           VARCHAR(100)  ,
    ADDRESS2           VARCHAR(100)  ,
    CITY               VARCHAR(100)  ,
    STATE_ABBR         VARCHAR(10)   ,
    STATE_NAME         VARCHAR(100)  ,
    POSTAL_CD          VARCHAR(20)   ,
    COUNTY             VARCHAR(100)  ,
    COUNTRY_NAME       VARCHAR(100)  ,
    COUNTRY_CD         VARCHAR(2)    ,
    TIME_ZONE_GUID     VARCHAR(50)   NOT NULL,
    TIME_ZONE_ABBREV   VARCHAR(10)   NOT NULL,
    ACTIVE_FLAG        VARCHAR(10)   NOT NULL,

    CONSTRAINT PK_LOCATION_DIM PRIMARY KEY(LOCATION_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_LOCATION_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((NEW.SOURCE_CUSTOMER_ID % 17)::VARCHAR,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_LOCATION_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_LOCATION_DIM', COALESCE((OLD.SOURCE_CUSTOMER_ID % 17)::VARCHAR, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD != NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD = $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.SOURCE_LOCATION_ID != NEW.SOURCE_LOCATION_ID THEN
			lv_sql := lv_sql || 'SOURCE_LOCATION_ID = $2.SOURCE_LOCATION_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_LOCATION_ID = $1.SOURCE_LOCATION_ID OR SOURCE_LOCATION_ID = $2.SOURCE_LOCATION_ID)';
		END IF;
		IF OLD.SOURCE_CUSTOMER_ID != NEW.SOURCE_CUSTOMER_ID THEN
			lv_sql := lv_sql || 'SOURCE_CUSTOMER_ID = $2.SOURCE_CUSTOMER_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_CUSTOMER_ID = $1.SOURCE_CUSTOMER_ID OR SOURCE_CUSTOMER_ID = $2.SOURCE_CUSTOMER_ID)';
		END IF;
		IF OLD.CUSTOMER_NAME != NEW.CUSTOMER_NAME THEN
			lv_sql := lv_sql || 'CUSTOMER_NAME = $2.CUSTOMER_NAME, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_NAME = $1.CUSTOMER_NAME OR CUSTOMER_NAME = $2.CUSTOMER_NAME)';
		END IF;
		IF OLD.LOCATION_NAME != NEW.LOCATION_NAME THEN
			lv_sql := lv_sql || 'LOCATION_NAME = $2.LOCATION_NAME, ';
			lv_filter := lv_filter || ' AND (LOCATION_NAME = $1.LOCATION_NAME OR LOCATION_NAME = $2.LOCATION_NAME)';
		END IF;
		IF OLD.LOCATION_DESC IS DISTINCT FROM NEW.LOCATION_DESC THEN
			lv_sql := lv_sql || 'LOCATION_DESC = $2.LOCATION_DESC, ';
			lv_filter := lv_filter || ' AND (LOCATION_DESC IS NOT DISTINCT FROM $1.LOCATION_DESC OR LOCATION_DESC IS NOT DISTINCT FROM $2.LOCATION_DESC)';
		END IF;
		IF OLD.LOCATION_TYPE != NEW.LOCATION_TYPE THEN
			lv_sql := lv_sql || 'LOCATION_TYPE = $2.LOCATION_TYPE, ';
			lv_filter := lv_filter || ' AND (LOCATION_TYPE = $1.LOCATION_TYPE OR LOCATION_TYPE = $2.LOCATION_TYPE)';
		END IF;
		IF OLD.LOCATION_SUB_TYPE IS DISTINCT FROM NEW.LOCATION_SUB_TYPE THEN
			lv_sql := lv_sql || 'LOCATION_SUB_TYPE = $2.LOCATION_SUB_TYPE, ';
			lv_filter := lv_filter || ' AND (LOCATION_SUB_TYPE IS NOT DISTINCT FROM $1.LOCATION_SUB_TYPE OR LOCATION_SUB_TYPE IS NOT DISTINCT FROM $2.LOCATION_SUB_TYPE)';
		END IF;
		IF OLD.HIERARCHY_LEVEL != NEW.HIERARCHY_LEVEL THEN
			lv_sql := lv_sql || 'HIERARCHY_LEVEL = $2.HIERARCHY_LEVEL, ';
			lv_filter := lv_filter || ' AND (HIERARCHY_LEVEL = $1.HIERARCHY_LEVEL OR HIERARCHY_LEVEL = $2.HIERARCHY_LEVEL)';
		END IF;
		IF OLD.TOP_MOST_FLAG != NEW.TOP_MOST_FLAG THEN
			lv_sql := lv_sql || 'TOP_MOST_FLAG = $2.TOP_MOST_FLAG, ';
			lv_filter := lv_filter || ' AND (TOP_MOST_FLAG = $1.TOP_MOST_FLAG OR TOP_MOST_FLAG = $2.TOP_MOST_FLAG)';
		END IF;
		IF OLD.BOTTOM_MOST_FLAG != NEW.BOTTOM_MOST_FLAG THEN
			lv_sql := lv_sql || 'BOTTOM_MOST_FLAG = $2.BOTTOM_MOST_FLAG, ';
			lv_filter := lv_filter || ' AND (BOTTOM_MOST_FLAG = $1.BOTTOM_MOST_FLAG OR BOTTOM_MOST_FLAG = $2.BOTTOM_MOST_FLAG)';
		END IF;
		IF OLD.ADDRESS1 IS DISTINCT FROM NEW.ADDRESS1 THEN
			lv_sql := lv_sql || 'ADDRESS1 = $2.ADDRESS1, ';
			lv_filter := lv_filter || ' AND (ADDRESS1 IS NOT DISTINCT FROM $1.ADDRESS1 OR ADDRESS1 IS NOT DISTINCT FROM $2.ADDRESS1)';
		END IF;
		IF OLD.ADDRESS2 IS DISTINCT FROM NEW.ADDRESS2 THEN
			lv_sql := lv_sql || 'ADDRESS2 = $2.ADDRESS2, ';
			lv_filter := lv_filter || ' AND (ADDRESS2 IS NOT DISTINCT FROM $1.ADDRESS2 OR ADDRESS2 IS NOT DISTINCT FROM $2.ADDRESS2)';
		END IF;
		IF OLD.CITY IS DISTINCT FROM NEW.CITY THEN
			lv_sql := lv_sql || 'CITY = $2.CITY, ';
			lv_filter := lv_filter || ' AND (CITY IS NOT DISTINCT FROM $1.CITY OR CITY IS NOT DISTINCT FROM $2.CITY)';
		END IF;
		IF OLD.STATE_ABBR IS DISTINCT FROM NEW.STATE_ABBR THEN
			lv_sql := lv_sql || 'STATE_ABBR = $2.STATE_ABBR, ';
			lv_filter := lv_filter || ' AND (STATE_ABBR IS NOT DISTINCT FROM $1.STATE_ABBR OR STATE_ABBR IS NOT DISTINCT FROM $2.STATE_ABBR)';
		END IF;
		IF OLD.STATE_NAME IS DISTINCT FROM NEW.STATE_NAME THEN
			lv_sql := lv_sql || 'STATE_NAME = $2.STATE_NAME, ';
			lv_filter := lv_filter || ' AND (STATE_NAME IS NOT DISTINCT FROM $1.STATE_NAME OR STATE_NAME IS NOT DISTINCT FROM $2.STATE_NAME)';
		END IF;
		IF OLD.POSTAL_CD IS DISTINCT FROM NEW.POSTAL_CD THEN
			lv_sql := lv_sql || 'POSTAL_CD = $2.POSTAL_CD, ';
			lv_filter := lv_filter || ' AND (POSTAL_CD IS NOT DISTINCT FROM $1.POSTAL_CD OR POSTAL_CD IS NOT DISTINCT FROM $2.POSTAL_CD)';
		END IF;
		IF OLD.COUNTY IS DISTINCT FROM NEW.COUNTY THEN
			lv_sql := lv_sql || 'COUNTY = $2.COUNTY, ';
			lv_filter := lv_filter || ' AND (COUNTY IS NOT DISTINCT FROM $1.COUNTY OR COUNTY IS NOT DISTINCT FROM $2.COUNTY)';
		END IF;
		IF OLD.COUNTRY_NAME IS DISTINCT FROM NEW.COUNTRY_NAME THEN
			lv_sql := lv_sql || 'COUNTRY_NAME = $2.COUNTRY_NAME, ';
			lv_filter := lv_filter || ' AND (COUNTRY_NAME IS NOT DISTINCT FROM $1.COUNTRY_NAME OR COUNTRY_NAME IS NOT DISTINCT FROM $2.COUNTRY_NAME)';
		END IF;
		IF OLD.COUNTRY_CD IS DISTINCT FROM NEW.COUNTRY_CD THEN
			lv_sql := lv_sql || 'COUNTRY_CD = $2.COUNTRY_CD, ';
			lv_filter := lv_filter || ' AND (COUNTRY_CD IS NOT DISTINCT FROM $1.COUNTRY_CD OR COUNTRY_CD IS NOT DISTINCT FROM $2.COUNTRY_CD)';
		END IF;
		IF OLD.TIME_ZONE_GUID != NEW.TIME_ZONE_GUID THEN
			lv_sql := lv_sql || 'TIME_ZONE_GUID = $2.TIME_ZONE_GUID, ';
			lv_filter := lv_filter || ' AND (TIME_ZONE_GUID = $1.TIME_ZONE_GUID OR TIME_ZONE_GUID = $2.TIME_ZONE_GUID)';
		END IF;
		IF OLD.TIME_ZONE_ABBREV != NEW.TIME_ZONE_ABBREV THEN
			lv_sql := lv_sql || 'TIME_ZONE_ABBREV = $2.TIME_ZONE_ABBREV, ';
			lv_filter := lv_filter || ' AND (TIME_ZONE_ABBREV = $1.TIME_ZONE_ABBREV OR TIME_ZONE_ABBREV = $2.TIME_ZONE_ABBREV)';
		END IF;
		IF OLD.ACTIVE_FLAG != NEW.ACTIVE_FLAG THEN
			lv_sql := lv_sql || 'ACTIVE_FLAG = $2.ACTIVE_FLAG, ';
			lv_filter := lv_filter || ' AND (ACTIVE_FLAG = $1.ACTIVE_FLAG OR ACTIVE_FLAG = $2.ACTIVE_FLAG)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE LOCATION_DIM_ID = $1.LOCATION_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'SOURCE_CUSTOMER_ID % 17 = ' || NEW.SOURCE_CUSTOMER_ID % 17||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(LOCATION_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._LOCATION_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_LOCATION_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE LOCATION_DIM_ID = $1.LOCATION_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.LOCATION_DIM
	AS SELECT * FROM RDW._LOCATION_DIM;

ALTER VIEW RDW.LOCATION_DIM ALTER LOCATION_DIM_ID SET DEFAULT nextval('RDW._location_dim_location_dim_id_seq'::regclass);
ALTER VIEW RDW.LOCATION_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.LOCATION_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.LOCATION_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.LOCATION_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.LOCATION_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._LOCATION_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_LOCATION_DIM ON RDW.LOCATION_DIM;
CREATE TRIGGER TRIIUD_LOCATION_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.LOCATION_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_LOCATION_DIM();


CREATE UNIQUE INDEX AK_LOCATION_DIM ON RDW._LOCATION_DIM(SOURCE_SYSTEM_CD, SOURCE_LOCATION_ID, SOURCE_CUSTOMER_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_SOURCE_SYSTEM_CD_SOURCE_CUSTOMER_ID$05410A1F ON RDW._LOCATION_DIM(SOURCE_SYSTEM_CD, SOURCE_CUSTOMER_ID, TOP_MOST_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_CUSTOMER_NAME ON RDW._LOCATION_DIM(CUSTOMER_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_LOCATION_NAME ON RDW._LOCATION_DIM(LOCATION_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_LOCATION_TYPE ON RDW._LOCATION_DIM(LOCATION_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_CITY ON RDW._LOCATION_DIM(CITY) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_STATE_ABBR ON RDW._LOCATION_DIM(STATE_ABBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_POSTAL_CD ON RDW._LOCATION_DIM(POSTAL_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_COUNTY ON RDW._LOCATION_DIM(COUNTY) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_COUNTRY_CD ON RDW._LOCATION_DIM(COUNTRY_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_DIM_CREATED_TS ON RDW._LOCATION_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.LOCATION_DIM ----------------------------------

INSERT INTO RDW.LOCATION_DIM(
	LOCATION_DIM_ID,
	SOURCE_SYSTEM_CD,
	SOURCE_LOCATION_ID,
	SOURCE_CUSTOMER_ID,
	CUSTOMER_NAME,
	LOCATION_NAME,
	LOCATION_DESC,
	LOCATION_TYPE,
	LOCATION_SUB_TYPE,
	HIERARCHY_LEVEL,
	TOP_MOST_FLAG,
	BOTTOM_MOST_FLAG,
	ADDRESS1,
	ADDRESS2,
	CITY,
	STATE_ABBR,
	STATE_NAME,
	POSTAL_CD,
	COUNTY,
	COUNTRY_NAME,
	COUNTRY_CD,
	TIME_ZONE_GUID,
	TIME_ZONE_ABBREV,
	ACTIVE_FLAG,
	REVISION)
VALUES(
	0,
	'Unknown',
	0,
	0,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	NULL,
	0,
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_LOCATION_DIM(
	p_location_dim_id OUT BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_source_location_id BIGINT, 
	p_source_customer_id BIGINT, 
	p_customer_name VARCHAR(100), 
	p_location_name VARCHAR(100), 
	p_location_desc VARCHAR(4000), 
	p_location_type VARCHAR(100), 
	p_location_sub_type VARCHAR(100), 
	p_hierarchy_level SMALLINT, 
	p_top_most_flag VARCHAR(10), 
	p_bottom_most_flag VARCHAR(10), 
	p_address1 VARCHAR(100), 
	p_address2 VARCHAR(100), 
	p_city VARCHAR(100), 
	p_state_abbr VARCHAR(10), 
	p_state_name VARCHAR(100), 
	p_postal_cd VARCHAR(20), 
	p_county VARCHAR(100), 
	p_country_name VARCHAR(100), 
	p_country_cd VARCHAR(2), 
	p_time_zone_guid VARCHAR(50), 
	p_time_zone_abbrev VARCHAR(10), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT LOCATION_DIM_ID, REVISION
			  INTO p_location_dim_id, p_old_revision
			  FROM RDW.LOCATION_DIM
			 WHERE SOURCE_SYSTEM_CD = p_source_system_cd
			   AND SOURCE_LOCATION_ID = p_source_location_id
			   AND SOURCE_CUSTOMER_ID = p_source_customer_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.LOCATION_DIM
			   SET CUSTOMER_NAME = p_customer_name,
			       LOCATION_NAME = p_location_name,
			       LOCATION_DESC = p_location_desc,
			       LOCATION_TYPE = p_location_type,
			       LOCATION_SUB_TYPE = p_location_sub_type,
			       HIERARCHY_LEVEL = p_hierarchy_level,
			       TOP_MOST_FLAG = p_top_most_flag,
			       BOTTOM_MOST_FLAG = p_bottom_most_flag,
			       ADDRESS1 = p_address1,
			       ADDRESS2 = p_address2,
			       CITY = p_city,
			       STATE_ABBR = p_state_abbr,
			       STATE_NAME = p_state_name,
			       POSTAL_CD = p_postal_cd,
			       COUNTY = p_county,
			       COUNTRY_NAME = p_country_name,
			       COUNTRY_CD = p_country_cd,
			       TIME_ZONE_GUID = p_time_zone_guid,
			       TIME_ZONE_ABBREV = p_time_zone_abbrev,
			       ACTIVE_FLAG = p_active_flag,
			       REVISION = p_revision
			 WHERE SOURCE_SYSTEM_CD = p_source_system_cd
			   AND SOURCE_LOCATION_ID = p_source_location_id
			   AND SOURCE_CUSTOMER_ID = p_source_customer_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.LOCATION_DIM(SOURCE_SYSTEM_CD, SOURCE_LOCATION_ID, SOURCE_CUSTOMER_ID, CUSTOMER_NAME, LOCATION_NAME, LOCATION_DESC, LOCATION_TYPE, LOCATION_SUB_TYPE, HIERARCHY_LEVEL, TOP_MOST_FLAG, BOTTOM_MOST_FLAG, ADDRESS1, ADDRESS2, CITY, STATE_ABBR, STATE_NAME, POSTAL_CD, COUNTY, COUNTRY_NAME, COUNTRY_CD, TIME_ZONE_GUID, TIME_ZONE_ABBREV, ACTIVE_FLAG, REVISION)
					 VALUES(p_source_system_cd, p_source_location_id, p_source_customer_id, p_customer_name, p_location_name, p_location_desc, p_location_type, p_location_sub_type, p_hierarchy_level, p_top_most_flag, p_bottom_most_flag, p_address1, p_address2, p_city, p_state_abbr, p_state_name, p_postal_cd, p_county, p_country_name, p_country_cd, p_time_zone_guid, p_time_zone_abbrev, p_active_flag, p_revision)
					 RETURNING LOCATION_DIM_ID
					      INTO p_location_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_LOCATION_DIM(
	p_location_dim_id OUT BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_source_location_id BIGINT, 
	p_source_customer_id BIGINT, 
	p_customer_name VARCHAR(100), 
	p_location_name VARCHAR(100), 
	p_location_desc VARCHAR(4000), 
	p_location_type VARCHAR(100), 
	p_location_sub_type VARCHAR(100), 
	p_hierarchy_level SMALLINT, 
	p_top_most_flag VARCHAR(10), 
	p_bottom_most_flag VARCHAR(10), 
	p_address1 VARCHAR(100), 
	p_address2 VARCHAR(100), 
	p_city VARCHAR(100), 
	p_state_abbr VARCHAR(10), 
	p_state_name VARCHAR(100), 
	p_postal_cd VARCHAR(20), 
	p_county VARCHAR(100), 
	p_country_name VARCHAR(100), 
	p_country_cd VARCHAR(2), 
	p_time_zone_guid VARCHAR(50), 
	p_time_zone_abbrev VARCHAR(10), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._PAYMENT_DIM
  (
    PAYMENT_DIM_ID          BIGSERIAL     NOT NULL,
    SOURCE_DOC_ID           BIGINT        NOT NULL,
    SOURCE_CUSTOMER_BANK_ID BIGINT        ,
    PAYMENT_REF_NUM         VARCHAR(50)   NOT NULL,
    PAID_FLAG               VARCHAR(10)   NOT NULL,
    PAID_TS                 TIMESTAMPTZ   ,
    CREDIT_OR_DEBIT         VARCHAR(20)   NOT NULL,
    PAYMENT_METHOD          VARCHAR(30)   NOT NULL,
    PAYMENT_DESC            VARCHAR(100)  ,
    BUSINESS_UNIT           VARCHAR(50)   NOT NULL,
    BANK_ACCT_NUM           VARCHAR(50)   NOT NULL,
    BANK_ACCT_NAME          VARCHAR(100)  NOT NULL,
    BANK_ROUTING_NUM        VARCHAR(20)   NOT NULL,
    BANK_NAME               VARCHAR(50)   NOT NULL,
    CUSTOMER_NAME           VARCHAR(100)  NOT NULL,
    STATUS                  CHAR(1)       NOT NULL,
    TOTAL_AMOUNT            DECIMAL(17,4) ,

    CONSTRAINT PK_PAYMENT_DIM PRIMARY KEY(PAYMENT_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_PAYMENT_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.PAID_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_PAYMENT_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_PAYMENT_DIM', COALESCE(TO_CHAR(OLD.PAID_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.PAYMENT_DIM_ID != NEW.PAYMENT_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_DOC_ID != NEW.SOURCE_DOC_ID THEN
			lv_sql := lv_sql || 'SOURCE_DOC_ID = $2.SOURCE_DOC_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_DOC_ID = $1.SOURCE_DOC_ID OR SOURCE_DOC_ID = $2.SOURCE_DOC_ID)';
		END IF;
		IF OLD.SOURCE_CUSTOMER_BANK_ID IS DISTINCT FROM NEW.SOURCE_CUSTOMER_BANK_ID THEN
			lv_sql := lv_sql || 'SOURCE_CUSTOMER_BANK_ID = $2.SOURCE_CUSTOMER_BANK_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_CUSTOMER_BANK_ID IS NOT DISTINCT FROM $1.SOURCE_CUSTOMER_BANK_ID OR SOURCE_CUSTOMER_BANK_ID IS NOT DISTINCT FROM $2.SOURCE_CUSTOMER_BANK_ID)';
		END IF;
		IF OLD.PAYMENT_REF_NUM != NEW.PAYMENT_REF_NUM THEN
			lv_sql := lv_sql || 'PAYMENT_REF_NUM = $2.PAYMENT_REF_NUM, ';
			lv_filter := lv_filter || ' AND (PAYMENT_REF_NUM = $1.PAYMENT_REF_NUM OR PAYMENT_REF_NUM = $2.PAYMENT_REF_NUM)';
		END IF;
		IF OLD.PAID_FLAG != NEW.PAID_FLAG THEN
			lv_sql := lv_sql || 'PAID_FLAG = $2.PAID_FLAG, ';
			lv_filter := lv_filter || ' AND (PAID_FLAG = $1.PAID_FLAG OR PAID_FLAG = $2.PAID_FLAG)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.CREDIT_OR_DEBIT != NEW.CREDIT_OR_DEBIT THEN
			lv_sql := lv_sql || 'CREDIT_OR_DEBIT = $2.CREDIT_OR_DEBIT, ';
			lv_filter := lv_filter || ' AND (CREDIT_OR_DEBIT = $1.CREDIT_OR_DEBIT OR CREDIT_OR_DEBIT = $2.CREDIT_OR_DEBIT)';
		END IF;
		IF OLD.PAYMENT_METHOD != NEW.PAYMENT_METHOD THEN
			lv_sql := lv_sql || 'PAYMENT_METHOD = $2.PAYMENT_METHOD, ';
			lv_filter := lv_filter || ' AND (PAYMENT_METHOD = $1.PAYMENT_METHOD OR PAYMENT_METHOD = $2.PAYMENT_METHOD)';
		END IF;
		IF OLD.PAYMENT_DESC IS DISTINCT FROM NEW.PAYMENT_DESC THEN
			lv_sql := lv_sql || 'PAYMENT_DESC = $2.PAYMENT_DESC, ';
			lv_filter := lv_filter || ' AND (PAYMENT_DESC IS NOT DISTINCT FROM $1.PAYMENT_DESC OR PAYMENT_DESC IS NOT DISTINCT FROM $2.PAYMENT_DESC)';
		END IF;
		IF OLD.BUSINESS_UNIT != NEW.BUSINESS_UNIT THEN
			lv_sql := lv_sql || 'BUSINESS_UNIT = $2.BUSINESS_UNIT, ';
			lv_filter := lv_filter || ' AND (BUSINESS_UNIT = $1.BUSINESS_UNIT OR BUSINESS_UNIT = $2.BUSINESS_UNIT)';
		END IF;
		IF OLD.BANK_ACCT_NUM != NEW.BANK_ACCT_NUM THEN
			lv_sql := lv_sql || 'BANK_ACCT_NUM = $2.BANK_ACCT_NUM, ';
			lv_filter := lv_filter || ' AND (BANK_ACCT_NUM = $1.BANK_ACCT_NUM OR BANK_ACCT_NUM = $2.BANK_ACCT_NUM)';
		END IF;
		IF OLD.BANK_ACCT_NAME != NEW.BANK_ACCT_NAME THEN
			lv_sql := lv_sql || 'BANK_ACCT_NAME = $2.BANK_ACCT_NAME, ';
			lv_filter := lv_filter || ' AND (BANK_ACCT_NAME = $1.BANK_ACCT_NAME OR BANK_ACCT_NAME = $2.BANK_ACCT_NAME)';
		END IF;
		IF OLD.BANK_ROUTING_NUM != NEW.BANK_ROUTING_NUM THEN
			lv_sql := lv_sql || 'BANK_ROUTING_NUM = $2.BANK_ROUTING_NUM, ';
			lv_filter := lv_filter || ' AND (BANK_ROUTING_NUM = $1.BANK_ROUTING_NUM OR BANK_ROUTING_NUM = $2.BANK_ROUTING_NUM)';
		END IF;
		IF OLD.BANK_NAME != NEW.BANK_NAME THEN
			lv_sql := lv_sql || 'BANK_NAME = $2.BANK_NAME, ';
			lv_filter := lv_filter || ' AND (BANK_NAME = $1.BANK_NAME OR BANK_NAME = $2.BANK_NAME)';
		END IF;
		IF OLD.CUSTOMER_NAME != NEW.CUSTOMER_NAME THEN
			lv_sql := lv_sql || 'CUSTOMER_NAME = $2.CUSTOMER_NAME, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_NAME = $1.CUSTOMER_NAME OR CUSTOMER_NAME = $2.CUSTOMER_NAME)';
		END IF;
		IF OLD.STATUS != NEW.STATUS THEN
			lv_sql := lv_sql || 'STATUS = $2.STATUS, ';
			lv_filter := lv_filter || ' AND (STATUS = $1.STATUS OR STATUS = $2.STATUS)';
		END IF;
		IF OLD.TOTAL_AMOUNT IS DISTINCT FROM NEW.TOTAL_AMOUNT THEN
			lv_sql := lv_sql || 'TOTAL_AMOUNT = $2.TOTAL_AMOUNT, ';
			lv_filter := lv_filter || ' AND (TOTAL_AMOUNT IS NOT DISTINCT FROM $1.TOTAL_AMOUNT OR TOTAL_AMOUNT IS NOT DISTINCT FROM $2.TOTAL_AMOUNT)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE PAYMENT_DIM_ID = $1.PAYMENT_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.PAID_TS IS NULL THEN 'PAID_TS IS NULL' ELSE 'PAID_TS >= TIMESTAMP'''||TO_CHAR(NEW.PAID_TS, 'YYYY-MM-01')||''' AND PAID_TS < TIMESTAMP'''||TO_CHAR(NEW.PAID_TS + '1 month', 'YYYY-MM-01')||'''' END||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(PAYMENT_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._PAYMENT_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_PAYMENT_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE PAYMENT_DIM_ID = $1.PAYMENT_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.PAYMENT_DIM
	AS SELECT * FROM RDW._PAYMENT_DIM;

ALTER VIEW RDW.PAYMENT_DIM ALTER PAYMENT_DIM_ID SET DEFAULT nextval('RDW._payment_dim_payment_dim_id_seq'::regclass);
ALTER VIEW RDW.PAYMENT_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.PAYMENT_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.PAYMENT_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.PAYMENT_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.PAYMENT_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._PAYMENT_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_PAYMENT_DIM ON RDW.PAYMENT_DIM;
CREATE TRIGGER TRIIUD_PAYMENT_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.PAYMENT_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_PAYMENT_DIM();


CREATE UNIQUE INDEX AK_PAYMENT_DIM ON RDW._PAYMENT_DIM(SOURCE_DOC_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_SOURCE_CUSTOMER_BANK_ID ON RDW._PAYMENT_DIM(SOURCE_CUSTOMER_BANK_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_PAYMENT_REF_NUM ON RDW._PAYMENT_DIM(PAYMENT_REF_NUM) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_PAID_FLAG ON RDW._PAYMENT_DIM(PAID_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_PAID_TS ON RDW._PAYMENT_DIM(PAID_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_CUSTOMER_NAME ON RDW._PAYMENT_DIM(CUSTOMER_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_STATUS ON RDW._PAYMENT_DIM(STATUS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_DIM_CREATED_TS ON RDW._PAYMENT_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.PAYMENT_DIM ----------------------------------

INSERT INTO RDW.PAYMENT_DIM(
	PAYMENT_DIM_ID,
	SOURCE_DOC_ID,
	SOURCE_CUSTOMER_BANK_ID,
	PAYMENT_REF_NUM,
	PAID_FLAG,
	PAID_TS,
	CREDIT_OR_DEBIT,
	PAYMENT_METHOD,
	PAYMENT_DESC,
	BUSINESS_UNIT,
	BANK_ACCT_NUM,
	BANK_ACCT_NAME,
	BANK_ROUTING_NUM,
	BANK_NAME,
	CUSTOMER_NAME,
	STATUS,
	TOTAL_AMOUNT,
	REVISION)
VALUES(
	0,
	0,
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'-',
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_PAYMENT_DIM(
	p_payment_dim_id OUT BIGINT, 
	p_source_doc_id BIGINT, 
	p_source_customer_bank_id BIGINT, 
	p_payment_ref_num VARCHAR(50), 
	p_paid_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_credit_or_debit VARCHAR(20), 
	p_payment_method VARCHAR(30), 
	p_payment_desc VARCHAR(100), 
	p_business_unit VARCHAR(50), 
	p_bank_acct_num VARCHAR(50), 
	p_bank_acct_name VARCHAR(100), 
	p_bank_routing_num VARCHAR(20), 
	p_bank_name VARCHAR(50), 
	p_customer_name VARCHAR(100), 
	p_status CHAR(1), 
	p_total_amount DECIMAL(17,4), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT PAYMENT_DIM_ID, REVISION
			  INTO p_payment_dim_id, p_old_revision
			  FROM RDW.PAYMENT_DIM
			 WHERE SOURCE_DOC_ID = p_source_doc_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.PAYMENT_DIM
			   SET SOURCE_CUSTOMER_BANK_ID = p_source_customer_bank_id,
			       PAYMENT_REF_NUM = p_payment_ref_num,
			       PAID_FLAG = p_paid_flag,
			       PAID_TS = p_paid_ts,
			       CREDIT_OR_DEBIT = p_credit_or_debit,
			       PAYMENT_METHOD = p_payment_method,
			       PAYMENT_DESC = p_payment_desc,
			       BUSINESS_UNIT = p_business_unit,
			       BANK_ACCT_NUM = p_bank_acct_num,
			       BANK_ACCT_NAME = p_bank_acct_name,
			       BANK_ROUTING_NUM = p_bank_routing_num,
			       BANK_NAME = p_bank_name,
			       CUSTOMER_NAME = p_customer_name,
			       STATUS = p_status,
			       TOTAL_AMOUNT = p_total_amount,
			       REVISION = p_revision
			 WHERE SOURCE_DOC_ID = p_source_doc_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.PAYMENT_DIM(SOURCE_DOC_ID, SOURCE_CUSTOMER_BANK_ID, PAYMENT_REF_NUM, PAID_FLAG, PAID_TS, CREDIT_OR_DEBIT, PAYMENT_METHOD, PAYMENT_DESC, BUSINESS_UNIT, BANK_ACCT_NUM, BANK_ACCT_NAME, BANK_ROUTING_NUM, BANK_NAME, CUSTOMER_NAME, STATUS, TOTAL_AMOUNT, REVISION)
					 VALUES(p_source_doc_id, p_source_customer_bank_id, p_payment_ref_num, p_paid_flag, p_paid_ts, p_credit_or_debit, p_payment_method, p_payment_desc, p_business_unit, p_bank_acct_num, p_bank_acct_name, p_bank_routing_num, p_bank_name, p_customer_name, p_status, p_total_amount, p_revision)
					 RETURNING PAYMENT_DIM_ID
					      INTO p_payment_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_PAYMENT_DIM(
	p_payment_dim_id OUT BIGINT, 
	p_source_doc_id BIGINT, 
	p_source_customer_bank_id BIGINT, 
	p_payment_ref_num VARCHAR(50), 
	p_paid_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_credit_or_debit VARCHAR(20), 
	p_payment_method VARCHAR(30), 
	p_payment_desc VARCHAR(100), 
	p_business_unit VARCHAR(50), 
	p_bank_acct_num VARCHAR(50), 
	p_bank_acct_name VARCHAR(100), 
	p_bank_routing_num VARCHAR(20), 
	p_bank_name VARCHAR(50), 
	p_customer_name VARCHAR(100), 
	p_status CHAR(1), 
	p_total_amount DECIMAL(17,4), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.TIME_DIM
  (
    TIME_DIM_ID    SMALLINT    NOT NULL,
    HOUR_OF_DAY    SMALLINT    ,
    MINUTE_OF_HOUR SMALLINT    ,
    MINUTE_OF_DAY  SMALLINT    ,
    TIME_TEXT_24   VARCHAR(10) NOT NULL,
    TIME_TEXT_12   VARCHAR(10) NOT NULL,
    CLOCK_HOUR     SMALLINT    ,
    AM_PM          VARCHAR(2)  NOT NULL,

    CONSTRAINT PK_TIME_DIM PRIMARY KEY(TIME_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.TIME_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.TIME_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_TIME_DIM ON RDW.TIME_DIM;


CREATE TRIGGER TRBI_TIME_DIM
BEFORE INSERT ON RDW.TIME_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_TIME_DIM ON RDW.TIME_DIM;


CREATE TRIGGER TRBU_TIME_DIM
BEFORE UPDATE ON RDW.TIME_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX_TIME_DIM_HOUR_OF_DAY ON RDW.TIME_DIM(HOUR_OF_DAY) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX AK_TIME_DIM ON RDW.TIME_DIM(MINUTE_OF_DAY) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TIME_DIM_CREATED_TS ON RDW.TIME_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.TIME_DIM ----------------------------------

INSERT INTO RDW.TIME_DIM(
	TIME_DIM_ID,
	HOUR_OF_DAY,
	MINUTE_OF_HOUR,
	MINUTE_OF_DAY,
	TIME_TEXT_24,
	TIME_TEXT_12,
	CLOCK_HOUR,
	AM_PM,
	REVISION)
VALUES(
	0,
	NULL,
	NULL,
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	'--',
	1);


INSERT INTO RDW.TIME_DIM(
	TIME_DIM_ID,
	HOUR_OF_DAY,
	MINUTE_OF_HOUR,
	MINUTE_OF_DAY,
	TIME_TEXT_24,
	TIME_TEXT_12,
	CLOCK_HOUR,
	AM_PM,
	REVISION)
SELECT 
	N,
	TRUNC((N - 1) / 60),
	(N - 1) % 60,
	N - 1,
	TO_CHAR(TRUNC((N - 1) / 60),'FM00') || ':' || TO_CHAR((N - 1) % 60,'FM00'),
	TO_CHAR(((CAST((N - 1) / 60  AS INTEGER) + 11) % 12) + 1,'FM90') || ':' || TO_CHAR((N - 1) % 60,'FM00') || ' ' || CASE WHEN N <= 720 THEN 'am' ELSE 'pm' END,
	(CAST((N - 1) / 60 AS INTEGER) % 12) + 1,
	CASE WHEN N <= 720 THEN 'am' ELSE 'pm' END,
	1
FROM GENERATE_SERIES(1,1440) N;


-- ======================================================================

CREATE TABLE RDW._MODEM_HOST_DIM
  (
    HOST_DIM_ID    BIGINT       NOT NULL,
    SOURCE_HOST_ID BIGINT       NOT NULL,
    MEID           VARCHAR(255) NOT NULL,
    ICCID          VARCHAR(255) NOT NULL,
    IMEI           VARCHAR(255) NOT NULL,
    MDN            VARCHAR(255) NOT NULL,
    CIMI           VARCHAR(255) NOT NULL,
    IP             VARCHAR(255) NOT NULL,

    CONSTRAINT PK_MODEM_HOST_DIM PRIMARY KEY(HOST_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_MODEM_HOST_DIM_HOST_DIM_ID FOREIGN KEY(HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_MODEM_HOST_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((NEW.SOURCE_HOST_ID % 17)::VARCHAR,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_MODEM_HOST_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_MODEM_HOST_DIM', COALESCE((OLD.SOURCE_HOST_ID % 17)::VARCHAR, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.HOST_DIM_ID != NEW.HOST_DIM_ID THEN
			lv_sql := lv_sql || 'HOST_DIM_ID = $2.HOST_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_HOST_ID != NEW.SOURCE_HOST_ID THEN
			lv_sql := lv_sql || 'SOURCE_HOST_ID = $2.SOURCE_HOST_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_HOST_ID = $1.SOURCE_HOST_ID OR SOURCE_HOST_ID = $2.SOURCE_HOST_ID)';
		END IF;
		IF OLD.MEID != NEW.MEID THEN
			lv_sql := lv_sql || 'MEID = $2.MEID, ';
			lv_filter := lv_filter || ' AND (MEID = $1.MEID OR MEID = $2.MEID)';
		END IF;
		IF OLD.ICCID != NEW.ICCID THEN
			lv_sql := lv_sql || 'ICCID = $2.ICCID, ';
			lv_filter := lv_filter || ' AND (ICCID = $1.ICCID OR ICCID = $2.ICCID)';
		END IF;
		IF OLD.IMEI != NEW.IMEI THEN
			lv_sql := lv_sql || 'IMEI = $2.IMEI, ';
			lv_filter := lv_filter || ' AND (IMEI = $1.IMEI OR IMEI = $2.IMEI)';
		END IF;
		IF OLD.MDN != NEW.MDN THEN
			lv_sql := lv_sql || 'MDN = $2.MDN, ';
			lv_filter := lv_filter || ' AND (MDN = $1.MDN OR MDN = $2.MDN)';
		END IF;
		IF OLD.CIMI != NEW.CIMI THEN
			lv_sql := lv_sql || 'CIMI = $2.CIMI, ';
			lv_filter := lv_filter || ' AND (CIMI = $1.CIMI OR CIMI = $2.CIMI)';
		END IF;
		IF OLD.IP != NEW.IP THEN
			lv_sql := lv_sql || 'IP = $2.IP, ';
			lv_filter := lv_filter || ' AND (IP = $1.IP OR IP = $2.IP)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE HOST_DIM_ID = $1.HOST_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'SOURCE_HOST_ID % 17 = ' || NEW.SOURCE_HOST_ID % 17||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(HOST_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._MODEM_HOST_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_MODEM_HOST_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE HOST_DIM_ID = $1.HOST_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.MODEM_HOST_DIM
	AS SELECT * FROM RDW._MODEM_HOST_DIM;

ALTER VIEW RDW.MODEM_HOST_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.MODEM_HOST_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.MODEM_HOST_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.MODEM_HOST_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.MODEM_HOST_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._MODEM_HOST_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_MODEM_HOST_DIM ON RDW.MODEM_HOST_DIM;
CREATE TRIGGER TRIIUD_MODEM_HOST_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.MODEM_HOST_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_MODEM_HOST_DIM();


CREATE UNIQUE INDEX AK_MODEM_HOST_DIM ON RDW._MODEM_HOST_DIM(SOURCE_HOST_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_MEID ON RDW._MODEM_HOST_DIM(MEID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_ICCID ON RDW._MODEM_HOST_DIM(ICCID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_IMEI ON RDW._MODEM_HOST_DIM(IMEI) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_MDN ON RDW._MODEM_HOST_DIM(MDN) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_CIMI ON RDW._MODEM_HOST_DIM(CIMI) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_IP ON RDW._MODEM_HOST_DIM(IP) TABLESPACE rdw_data_t1;

CREATE INDEX IX_MODEM_HOST_DIM_CREATED_TS ON RDW._MODEM_HOST_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.MODEM_HOST_DIM ----------------------------------

INSERT INTO RDW.MODEM_HOST_DIM(
	HOST_DIM_ID,
	SOURCE_HOST_ID,
	MEID,
	ICCID,
	IMEI,
	MDN,
	CIMI,
	IP,
	REVISION)
VALUES(
	0,
	0,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_MODEM_HOST_DIM(
	p_host_dim_id BIGINT, 
	p_source_host_id BIGINT, 
	p_meid VARCHAR(255), 
	p_iccid VARCHAR(255), 
	p_imei VARCHAR(255), 
	p_mdn VARCHAR(255), 
	p_cimi VARCHAR(255), 
	p_ip VARCHAR(255), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
	l_host_dim_id BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT HOST_DIM_ID, REVISION
			  INTO l_host_dim_id, p_old_revision
			  FROM RDW.MODEM_HOST_DIM
			 WHERE SOURCE_HOST_ID = p_source_host_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
		IF l_host_dim_id != p_host_dim_id THEN
			RAISE SQLSTATE '22000' USING MESSAGE = 'Row does not match primary key';
		END IF;
			UPDATE RDW.MODEM_HOST_DIM
			   SET MEID = p_meid,
			       ICCID = p_iccid,
			       IMEI = p_imei,
			       MDN = p_mdn,
			       CIMI = p_cimi,
			       IP = p_ip,
			       REVISION = p_revision
			 WHERE SOURCE_HOST_ID = p_source_host_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.MODEM_HOST_DIM(HOST_DIM_ID, SOURCE_HOST_ID, MEID, ICCID, IMEI, MDN, CIMI, IP, REVISION)
					 VALUES(p_host_dim_id, p_source_host_id, p_meid, p_iccid, p_imei, p_mdn, p_cimi, p_ip, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_MODEM_HOST_DIM(
	p_host_dim_id BIGINT, 
	p_source_host_id BIGINT, 
	p_meid VARCHAR(255), 
	p_iccid VARCHAR(255), 
	p_imei VARCHAR(255), 
	p_mdn VARCHAR(255), 
	p_cimi VARCHAR(255), 
	p_ip VARCHAR(255), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.ITEM_TYPE_DIM
  (
    ITEM_TYPE_DIM_ID      SMALLINT    NOT NULL,
    ITEM_TYPE_DESC        VARCHAR(30) NOT NULL,
    ITEM_TABLE_PREFIX     VARCHAR(60) NOT NULL,
    PAYMENT_ITEM_TYPE     VARCHAR(30) ,
    PAYMENT_ENTRY_TYPE_CD CHAR(2)     ,

    CONSTRAINT PK_ITEM_TYPE_DIM PRIMARY KEY(ITEM_TYPE_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.ITEM_TYPE_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.ITEM_TYPE_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_ITEM_TYPE_DIM ON RDW.ITEM_TYPE_DIM;


CREATE TRIGGER TRBI_ITEM_TYPE_DIM
BEFORE INSERT ON RDW.ITEM_TYPE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_ITEM_TYPE_DIM ON RDW.ITEM_TYPE_DIM;


CREATE TRIGGER TRBU_ITEM_TYPE_DIM
BEFORE UPDATE ON RDW.ITEM_TYPE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX_ITEM_TYPE_DIM_ITEM_TYPE_DESC ON RDW.ITEM_TYPE_DIM(ITEM_TYPE_DESC) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_TYPE_DIM_PAYMENT_ITEM_TYPE ON RDW.ITEM_TYPE_DIM(PAYMENT_ITEM_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_TYPE_DIM_PAYMENT_ENTRY_TYPE_CD ON RDW.ITEM_TYPE_DIM(PAYMENT_ENTRY_TYPE_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_TYPE_DIM_CREATED_TS ON RDW.ITEM_TYPE_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.ITEM_TYPE_DIM ----------------------------------

INSERT INTO RDW.ITEM_TYPE_DIM(
	ITEM_TYPE_DIM_ID,
	ITEM_TYPE_DESC,
	ITEM_TABLE_PREFIX,
	PAYMENT_ITEM_TYPE,
	PAYMENT_ENTRY_TYPE_CD,
	REVISION)
VALUES(
	0,
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	1);


INSERT INTO RDW.ITEM_TYPE_DIM(
	ITEM_TYPE_DIM_ID,
	ITEM_TYPE_DESC,
	ITEM_TABLE_PREFIX,
	PAYMENT_ITEM_TYPE,
	PAYMENT_ENTRY_TYPE_CD,
	REVISION)
SELECT 
	N,
	(ARRAY['Fill', 'Credit', 'Cash', 'Other', 'Transaction', 'Process Fee', 'Service Fee', 'Adjustment', 'Chargeback', 'Refund', 'Revenue Share', 'Process Fee'])[N],
	(ARRAY['FILL_', 'CREDIT_', 'CASH_', 'OTHER_', 'LEDGER_', 'LEDGER_', 'LEDGER_', 'LEDGER_', 'OTHER_', 'OTHER_', 'LEDGER_', 'LEDGER_'])[N],
	(ARRAY[NULL, NULL, NULL, NULL, 'CREDIT', 'PROCESS_FEE', 'SERVICE_FEE', 'ADJUSTMENT', 'CHARGEBACK', 'REFUND', 'REVENUE_SHARE', 'PROCESS_FEE'])[N],
	(ARRAY[NULL, NULL, NULL, NULL, 'CC', 'PF', 'SF', 'AD', 'CB', 'RF', 'NR', 'SB'])[N],
	1
FROM GENERATE_SERIES(1, 12) N;


-- ======================================================================

CREATE TABLE RDW._USER_LOCATION_PRIV
  (
    USER_ID         BIGINT      NOT NULL,
    LOCATION_DIM_ID BIGINT      NOT NULL,
    SOURCE          VARCHAR(30) NOT NULL,
    ACTIVE_FLAG     VARCHAR(10) NOT NULL,

    CONSTRAINT PK_USER_LOCATION_PRIV PRIMARY KEY(USER_ID, LOCATION_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_USER_LOCATION_PRIV_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_USER_LOCATION_PRIV()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((NEW.LOCATION_DIM_ID % 11)::VARCHAR,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_USER_LOCATION_PRIV', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_USER_LOCATION_PRIV', COALESCE((OLD.LOCATION_DIM_ID % 11)::VARCHAR, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.USER_ID != NEW.USER_ID THEN
			lv_sql := lv_sql || 'USER_ID = $2.USER_ID, ';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
		END IF;
		IF OLD.SOURCE != NEW.SOURCE THEN
			lv_sql := lv_sql || 'SOURCE = $2.SOURCE, ';
			lv_filter := lv_filter || ' AND (SOURCE = $1.SOURCE OR SOURCE = $2.SOURCE)';
		END IF;
		IF OLD.ACTIVE_FLAG != NEW.ACTIVE_FLAG THEN
			lv_sql := lv_sql || 'ACTIVE_FLAG = $2.ACTIVE_FLAG, ';
			lv_filter := lv_filter || ' AND (ACTIVE_FLAG = $1.ACTIVE_FLAG OR ACTIVE_FLAG = $2.ACTIVE_FLAG)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE USER_ID = $1.USER_ID AND LOCATION_DIM_ID = $1.LOCATION_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'LOCATION_DIM_ID % 11 = ' || NEW.LOCATION_DIM_ID % 11||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(USER_ID, LOCATION_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._USER_LOCATION_PRIV) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_USER_LOCATION_PRIV', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE USER_ID = $1.USER_ID AND LOCATION_DIM_ID = $1.LOCATION_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.USER_LOCATION_PRIV
	AS SELECT * FROM RDW._USER_LOCATION_PRIV;

ALTER VIEW RDW.USER_LOCATION_PRIV ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.USER_LOCATION_PRIV ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.USER_LOCATION_PRIV ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.USER_LOCATION_PRIV TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.USER_LOCATION_PRIV TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._USER_LOCATION_PRIV FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_USER_LOCATION_PRIV ON RDW.USER_LOCATION_PRIV;
CREATE TRIGGER TRIIUD_USER_LOCATION_PRIV
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.USER_LOCATION_PRIV
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_USER_LOCATION_PRIV();


CREATE INDEX IX_USER_LOCATION_PRIV_SOURCE ON RDW._USER_LOCATION_PRIV(SOURCE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_USER_LOCATION_PRIV_ACTIVE_FLAG ON RDW._USER_LOCATION_PRIV(ACTIVE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_USER_LOCATION_PRIV_CREATED_TS ON RDW._USER_LOCATION_PRIV(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_USER_LOCATION_PRIV(
	p_user_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_source VARCHAR(30), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT REVISION
			  INTO p_old_revision
			  FROM RDW.USER_LOCATION_PRIV
			 WHERE USER_ID = p_user_id
			   AND LOCATION_DIM_ID = p_location_dim_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.USER_LOCATION_PRIV
			   SET SOURCE = p_source,
			       ACTIVE_FLAG = p_active_flag,
			       REVISION = p_revision
			 WHERE USER_ID = p_user_id
			   AND LOCATION_DIM_ID = p_location_dim_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.USER_LOCATION_PRIV(USER_ID, LOCATION_DIM_ID, SOURCE, ACTIVE_FLAG, REVISION)
					 VALUES(p_user_id, p_location_dim_id, p_source, p_active_flag, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_USER_LOCATION_PRIV(
	p_user_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_source VARCHAR(30), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.USER_BANK_ACCT_PRIV
  (
    USER_ID                 BIGINT      NOT NULL,
    SOURCE_CUSTOMER_BANK_ID BIGINT      NOT NULL,
    SOURCE                  VARCHAR(30) NOT NULL,
    ACTIVE_FLAG             VARCHAR(10) NOT NULL,

    CONSTRAINT PK_USER_BANK_ACCT_PRIV PRIMARY KEY(USER_ID, SOURCE_CUSTOMER_BANK_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.USER_BANK_ACCT_PRIV TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.USER_BANK_ACCT_PRIV TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_USER_BANK_ACCT_PRIV ON RDW.USER_BANK_ACCT_PRIV;


CREATE TRIGGER TRBI_USER_BANK_ACCT_PRIV
BEFORE INSERT ON RDW.USER_BANK_ACCT_PRIV
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_USER_BANK_ACCT_PRIV ON RDW.USER_BANK_ACCT_PRIV;


CREATE TRIGGER TRBU_USER_BANK_ACCT_PRIV
BEFORE UPDATE ON RDW.USER_BANK_ACCT_PRIV
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX_USER_BANK_ACCT_PRIV_SOURCE ON RDW.USER_BANK_ACCT_PRIV(SOURCE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_USER_BANK_ACCT_PRIV_ACTIVE_FLAG ON RDW.USER_BANK_ACCT_PRIV(ACTIVE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_USER_BANK_ACCT_PRIV_CREATED_TS ON RDW.USER_BANK_ACCT_PRIV(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_USER_BANK_ACCT_PRIV(
	p_user_id BIGINT, 
	p_source_customer_bank_id BIGINT, 
	p_source VARCHAR(30), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT REVISION
			  INTO p_old_revision
			  FROM RDW.USER_BANK_ACCT_PRIV
			 WHERE USER_ID = p_user_id
			   AND SOURCE_CUSTOMER_BANK_ID = p_source_customer_bank_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.USER_BANK_ACCT_PRIV
			   SET SOURCE = p_source,
			       ACTIVE_FLAG = p_active_flag,
			       REVISION = p_revision
			 WHERE USER_ID = p_user_id
			   AND SOURCE_CUSTOMER_BANK_ID = p_source_customer_bank_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.USER_BANK_ACCT_PRIV(USER_ID, SOURCE_CUSTOMER_BANK_ID, SOURCE, ACTIVE_FLAG, REVISION)
					 VALUES(p_user_id, p_source_customer_bank_id, p_source, p_active_flag, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_USER_BANK_ACCT_PRIV(
	p_user_id BIGINT, 
	p_source_customer_bank_id BIGINT, 
	p_source VARCHAR(30), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.DEVICE_CALL_TYPE_DIM
  (
    DEVICE_CALL_TYPE_DIM_ID SERIAL        NOT NULL,
    CALL_STATUS_CD          CHAR(1)       NOT NULL,
    CALL_TYPE_CD            CHAR(1)       NOT NULL,
    DEX_RECEIVED_CD         CHAR(1)       NOT NULL,
    COMM_METHOD_CD          CHAR(1)       NOT NULL,
    CALL_OUTCOME            VARCHAR(30)   NOT NULL,
    CALL_REASON             VARCHAR(30)   NOT NULL,
    DEX_RECEIVED_FLAG       VARCHAR(10)   NOT NULL,
    COMM_METHOD_NAME        VARCHAR(30)   NOT NULL,
    IP_MATCH_ORDER          INTEGER       NOT NULL DEFAULT 999,
    IP_MATCH_REGEX          VARCHAR(4000) NOT NULL DEFAULT '.*',

    CONSTRAINT PK_DEVICE_CALL_TYPE_DIM PRIMARY KEY(DEVICE_CALL_TYPE_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.DEVICE_CALL_TYPE_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.DEVICE_CALL_TYPE_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_DEVICE_CALL_TYPE_DIM ON RDW.DEVICE_CALL_TYPE_DIM;


CREATE TRIGGER TRBI_DEVICE_CALL_TYPE_DIM
BEFORE INSERT ON RDW.DEVICE_CALL_TYPE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_DEVICE_CALL_TYPE_DIM ON RDW.DEVICE_CALL_TYPE_DIM;


CREATE TRIGGER TRBU_DEVICE_CALL_TYPE_DIM
BEFORE UPDATE ON RDW.DEVICE_CALL_TYPE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE UNIQUE INDEX AK_DEVICE_CALL_TYPE_DIM ON RDW.DEVICE_CALL_TYPE_DIM(CALL_STATUS_CD, CALL_TYPE_CD, DEX_RECEIVED_CD, COMM_METHOD_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_TYPE_DIM_CALL_OUTCOME ON RDW.DEVICE_CALL_TYPE_DIM(CALL_OUTCOME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_TYPE_DIM_CALL_REASON ON RDW.DEVICE_CALL_TYPE_DIM(CALL_REASON) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_TYPE_DIM_DEX_RECEIVED_FLAG ON RDW.DEVICE_CALL_TYPE_DIM(DEX_RECEIVED_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_TYPE_DIM_COMM_METHOD_NAME ON RDW.DEVICE_CALL_TYPE_DIM(COMM_METHOD_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_TYPE_DIM_CREATED_TS ON RDW.DEVICE_CALL_TYPE_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.DEVICE_CALL_TYPE_DIM ----------------------------------

INSERT INTO RDW.DEVICE_CALL_TYPE_DIM(
	DEVICE_CALL_TYPE_DIM_ID,
	CALL_STATUS_CD,
	CALL_TYPE_CD,
	DEX_RECEIVED_CD,
	COMM_METHOD_CD,
	CALL_OUTCOME,
	CALL_REASON,
	DEX_RECEIVED_FLAG,
	COMM_METHOD_NAME,
	IP_MATCH_ORDER,
	IP_MATCH_REGEX,
	REVISION)
VALUES(
	0,
	'-',
	'-',
	'-',
	'-',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	999,
	'.*',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_DEVICE_CALL_TYPE_DIM(
	p_device_call_type_dim_id OUT INTEGER, 
	p_call_status_cd CHAR(1), 
	p_call_type_cd CHAR(1), 
	p_dex_received_cd CHAR(1), 
	p_comm_method_cd CHAR(1), 
	p_call_outcome VARCHAR(30), 
	p_call_reason VARCHAR(30), 
	p_dex_received_flag VARCHAR(10), 
	p_comm_method_name VARCHAR(30), 
	p_ip_match_order INTEGER, 
	p_ip_match_regex VARCHAR(4000), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT DEVICE_CALL_TYPE_DIM_ID, REVISION
			  INTO p_device_call_type_dim_id, p_old_revision
			  FROM RDW.DEVICE_CALL_TYPE_DIM
			 WHERE CALL_STATUS_CD = p_call_status_cd
			   AND CALL_TYPE_CD = p_call_type_cd
			   AND DEX_RECEIVED_CD = p_dex_received_cd
			   AND COMM_METHOD_CD = p_comm_method_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.DEVICE_CALL_TYPE_DIM
			   SET CALL_OUTCOME = p_call_outcome,
			       CALL_REASON = p_call_reason,
			       DEX_RECEIVED_FLAG = p_dex_received_flag,
			       COMM_METHOD_NAME = p_comm_method_name,
			       IP_MATCH_ORDER = COALESCE(p_ip_match_order, IP_MATCH_ORDER),
			       IP_MATCH_REGEX = COALESCE(p_ip_match_regex, IP_MATCH_REGEX),
			       REVISION = p_revision
			 WHERE CALL_STATUS_CD = p_call_status_cd
			   AND CALL_TYPE_CD = p_call_type_cd
			   AND DEX_RECEIVED_CD = p_dex_received_cd
			   AND COMM_METHOD_CD = p_comm_method_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.DEVICE_CALL_TYPE_DIM(CALL_STATUS_CD, CALL_TYPE_CD, DEX_RECEIVED_CD, COMM_METHOD_CD, CALL_OUTCOME, CALL_REASON, DEX_RECEIVED_FLAG, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX, REVISION)
					 VALUES(p_call_status_cd, p_call_type_cd, p_dex_received_cd, p_comm_method_cd, p_call_outcome, p_call_reason, p_dex_received_flag, p_comm_method_name, COALESCE(p_ip_match_order, 999), COALESCE(p_ip_match_regex, '.*'), p_revision)
					 RETURNING DEVICE_CALL_TYPE_DIM_ID
					      INTO p_device_call_type_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_DEVICE_CALL_TYPE_DIM(
	p_device_call_type_dim_id OUT INTEGER, 
	p_call_status_cd CHAR(1), 
	p_call_type_cd CHAR(1), 
	p_dex_received_cd CHAR(1), 
	p_comm_method_cd CHAR(1), 
	p_call_outcome VARCHAR(30), 
	p_call_reason VARCHAR(30), 
	p_dex_received_flag VARCHAR(10), 
	p_comm_method_name VARCHAR(30), 
	p_ip_match_order INTEGER, 
	p_ip_match_regex VARCHAR(4000), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._DEVICE_CALL_DIM
  (
    DEVICE_CALL_DIM_ID       BIGSERIAL    NOT NULL,
    SOURCE_GLOBAL_SESSION_CD VARCHAR(100) NOT NULL,
    CALL_START_TS            TIMESTAMPTZ  ,
    CALL_END_TS              TIMESTAMPTZ  ,
    IMPORT_COMPLETED_TS      TIMESTAMPTZ  ,
    CLIENT_IP_ADDR           VARCHAR(20)  NOT NULL,
    CLIENT_PORT              SMALLINT     ,
    NET_LAYER_DESC           VARCHAR(60)  NOT NULL,
    SERVER_NAME              VARCHAR(60)  NOT NULL,
    SERVER_PORT              SMALLINT     ,

    CONSTRAINT PK_DEVICE_CALL_DIM PRIMARY KEY(DEVICE_CALL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_DEVICE_CALL_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.CALL_START_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_DEVICE_CALL_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_DEVICE_CALL_DIM', COALESCE(TO_CHAR(OLD.CALL_START_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.DEVICE_CALL_DIM_ID != NEW.DEVICE_CALL_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_GLOBAL_SESSION_CD != NEW.SOURCE_GLOBAL_SESSION_CD THEN
			lv_sql := lv_sql || 'SOURCE_GLOBAL_SESSION_CD = $2.SOURCE_GLOBAL_SESSION_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_GLOBAL_SESSION_CD = $1.SOURCE_GLOBAL_SESSION_CD OR SOURCE_GLOBAL_SESSION_CD = $2.SOURCE_GLOBAL_SESSION_CD)';
		END IF;
		IF OLD.CALL_START_TS IS DISTINCT FROM NEW.CALL_START_TS THEN
			lv_sql := lv_sql || 'CALL_START_TS = $2.CALL_START_TS, ';
			lv_filter := lv_filter || ' AND (CALL_START_TS IS NOT DISTINCT FROM $1.CALL_START_TS OR CALL_START_TS IS NOT DISTINCT FROM $2.CALL_START_TS)';
		END IF;
		IF OLD.CALL_END_TS IS DISTINCT FROM NEW.CALL_END_TS THEN
			lv_sql := lv_sql || 'CALL_END_TS = $2.CALL_END_TS, ';
			lv_filter := lv_filter || ' AND (CALL_END_TS IS NOT DISTINCT FROM $1.CALL_END_TS OR CALL_END_TS IS NOT DISTINCT FROM $2.CALL_END_TS)';
		END IF;
		IF OLD.IMPORT_COMPLETED_TS IS DISTINCT FROM NEW.IMPORT_COMPLETED_TS THEN
			lv_sql := lv_sql || 'IMPORT_COMPLETED_TS = $2.IMPORT_COMPLETED_TS, ';
			lv_filter := lv_filter || ' AND (IMPORT_COMPLETED_TS IS NOT DISTINCT FROM $1.IMPORT_COMPLETED_TS OR IMPORT_COMPLETED_TS IS NOT DISTINCT FROM $2.IMPORT_COMPLETED_TS)';
		END IF;
		IF OLD.CLIENT_IP_ADDR != NEW.CLIENT_IP_ADDR THEN
			lv_sql := lv_sql || 'CLIENT_IP_ADDR = $2.CLIENT_IP_ADDR, ';
			lv_filter := lv_filter || ' AND (CLIENT_IP_ADDR = $1.CLIENT_IP_ADDR OR CLIENT_IP_ADDR = $2.CLIENT_IP_ADDR)';
		END IF;
		IF OLD.CLIENT_PORT IS DISTINCT FROM NEW.CLIENT_PORT THEN
			lv_sql := lv_sql || 'CLIENT_PORT = $2.CLIENT_PORT, ';
			lv_filter := lv_filter || ' AND (CLIENT_PORT IS NOT DISTINCT FROM $1.CLIENT_PORT OR CLIENT_PORT IS NOT DISTINCT FROM $2.CLIENT_PORT)';
		END IF;
		IF OLD.NET_LAYER_DESC != NEW.NET_LAYER_DESC THEN
			lv_sql := lv_sql || 'NET_LAYER_DESC = $2.NET_LAYER_DESC, ';
			lv_filter := lv_filter || ' AND (NET_LAYER_DESC = $1.NET_LAYER_DESC OR NET_LAYER_DESC = $2.NET_LAYER_DESC)';
		END IF;
		IF OLD.SERVER_NAME != NEW.SERVER_NAME THEN
			lv_sql := lv_sql || 'SERVER_NAME = $2.SERVER_NAME, ';
			lv_filter := lv_filter || ' AND (SERVER_NAME = $1.SERVER_NAME OR SERVER_NAME = $2.SERVER_NAME)';
		END IF;
		IF OLD.SERVER_PORT IS DISTINCT FROM NEW.SERVER_PORT THEN
			lv_sql := lv_sql || 'SERVER_PORT = $2.SERVER_PORT, ';
			lv_filter := lv_filter || ' AND (SERVER_PORT IS NOT DISTINCT FROM $1.SERVER_PORT OR SERVER_PORT IS NOT DISTINCT FROM $2.SERVER_PORT)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.CALL_START_TS IS NULL THEN 'CALL_START_TS IS NULL' ELSE 'CALL_START_TS >= TIMESTAMP'''||TO_CHAR(NEW.CALL_START_TS, 'YYYY-MM-01')||''' AND CALL_START_TS < TIMESTAMP'''||TO_CHAR(NEW.CALL_START_TS + '1 month', 'YYYY-MM-01')||'''' END  ||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(DEVICE_CALL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._DEVICE_CALL_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_DEVICE_CALL_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.DEVICE_CALL_DIM
	AS SELECT * FROM RDW._DEVICE_CALL_DIM;

ALTER VIEW RDW.DEVICE_CALL_DIM ALTER DEVICE_CALL_DIM_ID SET DEFAULT nextval('RDW._device_call_dim_device_call_dim_id_seq'::regclass);
ALTER VIEW RDW.DEVICE_CALL_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.DEVICE_CALL_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.DEVICE_CALL_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.DEVICE_CALL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.DEVICE_CALL_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._DEVICE_CALL_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_DEVICE_CALL_DIM ON RDW.DEVICE_CALL_DIM;
CREATE TRIGGER TRIIUD_DEVICE_CALL_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.DEVICE_CALL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_DEVICE_CALL_DIM();


CREATE UNIQUE INDEX AK_DEVICE_CALL_DIM ON RDW._DEVICE_CALL_DIM(SOURCE_GLOBAL_SESSION_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_CALL_START_TS ON RDW._DEVICE_CALL_DIM(CALL_START_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_CALL_END_TS ON RDW._DEVICE_CALL_DIM(CALL_END_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_IMPORT_COMPLETED_TS ON RDW._DEVICE_CALL_DIM(IMPORT_COMPLETED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_NET_LAYER_DESC ON RDW._DEVICE_CALL_DIM(NET_LAYER_DESC) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_SERVER_NAME ON RDW._DEVICE_CALL_DIM(SERVER_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_SERVER_PORT ON RDW._DEVICE_CALL_DIM(SERVER_PORT) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_DIM_CREATED_TS ON RDW._DEVICE_CALL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.DEVICE_CALL_DIM ----------------------------------

INSERT INTO RDW.DEVICE_CALL_DIM(
	DEVICE_CALL_DIM_ID,
	SOURCE_GLOBAL_SESSION_CD,
	CALL_START_TS,
	CALL_END_TS,
	IMPORT_COMPLETED_TS,
	CLIENT_IP_ADDR,
	CLIENT_PORT,
	NET_LAYER_DESC,
	SERVER_NAME,
	SERVER_PORT,
	REVISION)
VALUES(
	0,
	'Unknown',
	NULL,
	NULL,
	NULL,
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_DEVICE_CALL_DIM(
	p_device_call_dim_id OUT BIGINT, 
	p_source_global_session_cd VARCHAR(100), 
	p_call_start_ts TIMESTAMPTZ, 
	p_call_end_ts TIMESTAMPTZ, 
	p_import_completed_ts TIMESTAMPTZ, 
	p_client_ip_addr VARCHAR(20), 
	p_client_port SMALLINT, 
	p_net_layer_desc VARCHAR(60), 
	p_server_name VARCHAR(60), 
	p_server_port SMALLINT, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT DEVICE_CALL_DIM_ID, REVISION
			  INTO p_device_call_dim_id, p_old_revision
			  FROM RDW.DEVICE_CALL_DIM
			 WHERE SOURCE_GLOBAL_SESSION_CD = p_source_global_session_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.DEVICE_CALL_DIM
			   SET CALL_START_TS = p_call_start_ts,
			       CALL_END_TS = p_call_end_ts,
			       IMPORT_COMPLETED_TS = p_import_completed_ts,
			       CLIENT_IP_ADDR = p_client_ip_addr,
			       CLIENT_PORT = p_client_port,
			       NET_LAYER_DESC = p_net_layer_desc,
			       SERVER_NAME = p_server_name,
			       SERVER_PORT = p_server_port,
			       REVISION = p_revision
			 WHERE SOURCE_GLOBAL_SESSION_CD = p_source_global_session_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.DEVICE_CALL_DIM(SOURCE_GLOBAL_SESSION_CD, CALL_START_TS, CALL_END_TS, IMPORT_COMPLETED_TS, CLIENT_IP_ADDR, CLIENT_PORT, NET_LAYER_DESC, SERVER_NAME, SERVER_PORT, REVISION)
					 VALUES(p_source_global_session_cd, p_call_start_ts, p_call_end_ts, p_import_completed_ts, p_client_ip_addr, p_client_port, p_net_layer_desc, p_server_name, p_server_port, p_revision)
					 RETURNING DEVICE_CALL_DIM_ID
					      INTO p_device_call_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_DEVICE_CALL_DIM(
	p_device_call_dim_id OUT BIGINT, 
	p_source_global_session_cd VARCHAR(100), 
	p_call_start_ts TIMESTAMPTZ, 
	p_call_end_ts TIMESTAMPTZ, 
	p_import_completed_ts TIMESTAMPTZ, 
	p_client_ip_addr VARCHAR(20), 
	p_client_port SMALLINT, 
	p_net_layer_desc VARCHAR(60), 
	p_server_name VARCHAR(60), 
	p_server_port SMALLINT, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._DEVICE_DIM
  (
    DEVICE_DIM_ID           BIGSERIAL     NOT NULL,
    DEVICE_SERIAL_CD        VARCHAR(50)   NOT NULL,
    DEVICE_UNIQUE_NAME      VARCHAR(60)   NOT NULL,
    DEVICE_TYPE_ID          SMALLINT      NOT NULL,
    DEVICE_SUB_TYPE_ID      SMALLINT      NOT NULL,
    DEVICE_TYPE             VARCHAR(60)   NOT NULL,
    ACTIVE_FLAG             VARCHAR(10)   NOT NULL,
    FIRMWARE_VERSION        VARCHAR(256)  NOT NULL,
    DIAGNOSTIC_VERSION      VARCHAR(256)  NOT NULL,
    PTEST_VERSION           VARCHAR(256)  NOT NULL,
    PROPERTY_LIST_VERSION   SMALLINT      ,
    VERSION_REPORTED_UTC_TS TIMESTAMPTZ   ,
    BASE_HOST_DIM_ID        BIGINT        NOT NULL,
    PRIMARY_HOST_DIM_ID     BIGINT        NOT NULL,
    BEZEL_HOST_DIM_ID       BIGINT        NOT NULL,
    MODEM_HOST_DIM_ID       BIGINT        NOT NULL,
    CARDS_ACCEPTED          VARCHAR(4000) NOT NULL,
    DEX_MODE                VARCHAR(30)   NOT NULL,
    VENDS_PER_TRAN          SMALLINT      ,
    CALL_SCHEDULE           VARCHAR(4000) NOT NULL,
    DEX_SCHEDULE            VARCHAR(4000) NOT NULL,
    COMM_METHOD_CD          CHAR(1)       NOT NULL,
    COMM_METHOD_NAME        VARCHAR(30)   NOT NULL,

    CONSTRAINT PK_DEVICE_DIM PRIMARY KEY(DEVICE_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_DEVICE_DIM_BASE_HOST_DIM_ID FOREIGN KEY(BASE_HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID),
    CONSTRAINT FK_DEVICE_DIM_PRIMARY_HOST_DIM_ID FOREIGN KEY(PRIMARY_HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID),
    CONSTRAINT FK_DEVICE_DIM_BEZEL_HOST_DIM_ID FOREIGN KEY(BEZEL_HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID),
    CONSTRAINT FK_DEVICE_DIM_MODEM_HOST_DIM_ID FOREIGN KEY(MODEM_HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_DEVICE_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(UPPER(RIGHT(NEW.DEVICE_SERIAL_CD, 1)),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_DEVICE_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_DEVICE_DIM', COALESCE(UPPER(RIGHT(OLD.DEVICE_SERIAL_CD, 1)), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
		END IF;
		IF OLD.DEVICE_SERIAL_CD != NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD = $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.DEVICE_UNIQUE_NAME != NEW.DEVICE_UNIQUE_NAME THEN
			lv_sql := lv_sql || 'DEVICE_UNIQUE_NAME = $2.DEVICE_UNIQUE_NAME, ';
			lv_filter := lv_filter || ' AND (DEVICE_UNIQUE_NAME = $1.DEVICE_UNIQUE_NAME OR DEVICE_UNIQUE_NAME = $2.DEVICE_UNIQUE_NAME)';
		END IF;
		IF OLD.DEVICE_TYPE_ID != NEW.DEVICE_TYPE_ID THEN
			lv_sql := lv_sql || 'DEVICE_TYPE_ID = $2.DEVICE_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_TYPE_ID = $1.DEVICE_TYPE_ID OR DEVICE_TYPE_ID = $2.DEVICE_TYPE_ID)';
		END IF;
		IF OLD.DEVICE_SUB_TYPE_ID != NEW.DEVICE_SUB_TYPE_ID THEN
			lv_sql := lv_sql || 'DEVICE_SUB_TYPE_ID = $2.DEVICE_SUB_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_SUB_TYPE_ID = $1.DEVICE_SUB_TYPE_ID OR DEVICE_SUB_TYPE_ID = $2.DEVICE_SUB_TYPE_ID)';
		END IF;
		IF OLD.DEVICE_TYPE != NEW.DEVICE_TYPE THEN
			lv_sql := lv_sql || 'DEVICE_TYPE = $2.DEVICE_TYPE, ';
			lv_filter := lv_filter || ' AND (DEVICE_TYPE = $1.DEVICE_TYPE OR DEVICE_TYPE = $2.DEVICE_TYPE)';
		END IF;
		IF OLD.ACTIVE_FLAG != NEW.ACTIVE_FLAG THEN
			lv_sql := lv_sql || 'ACTIVE_FLAG = $2.ACTIVE_FLAG, ';
			lv_filter := lv_filter || ' AND (ACTIVE_FLAG = $1.ACTIVE_FLAG OR ACTIVE_FLAG = $2.ACTIVE_FLAG)';
		END IF;
		IF OLD.FIRMWARE_VERSION != NEW.FIRMWARE_VERSION THEN
			lv_sql := lv_sql || 'FIRMWARE_VERSION = $2.FIRMWARE_VERSION, ';
			lv_filter := lv_filter || ' AND (FIRMWARE_VERSION = $1.FIRMWARE_VERSION OR FIRMWARE_VERSION = $2.FIRMWARE_VERSION)';
		END IF;
		IF OLD.DIAGNOSTIC_VERSION != NEW.DIAGNOSTIC_VERSION THEN
			lv_sql := lv_sql || 'DIAGNOSTIC_VERSION = $2.DIAGNOSTIC_VERSION, ';
			lv_filter := lv_filter || ' AND (DIAGNOSTIC_VERSION = $1.DIAGNOSTIC_VERSION OR DIAGNOSTIC_VERSION = $2.DIAGNOSTIC_VERSION)';
		END IF;
		IF OLD.PTEST_VERSION != NEW.PTEST_VERSION THEN
			lv_sql := lv_sql || 'PTEST_VERSION = $2.PTEST_VERSION, ';
			lv_filter := lv_filter || ' AND (PTEST_VERSION = $1.PTEST_VERSION OR PTEST_VERSION = $2.PTEST_VERSION)';
		END IF;
		IF OLD.PROPERTY_LIST_VERSION IS DISTINCT FROM NEW.PROPERTY_LIST_VERSION THEN
			lv_sql := lv_sql || 'PROPERTY_LIST_VERSION = $2.PROPERTY_LIST_VERSION, ';
			lv_filter := lv_filter || ' AND (PROPERTY_LIST_VERSION IS NOT DISTINCT FROM $1.PROPERTY_LIST_VERSION OR PROPERTY_LIST_VERSION IS NOT DISTINCT FROM $2.PROPERTY_LIST_VERSION)';
		END IF;
		IF OLD.VERSION_REPORTED_UTC_TS IS DISTINCT FROM NEW.VERSION_REPORTED_UTC_TS THEN
			lv_sql := lv_sql || 'VERSION_REPORTED_UTC_TS = $2.VERSION_REPORTED_UTC_TS, ';
			lv_filter := lv_filter || ' AND (VERSION_REPORTED_UTC_TS IS NOT DISTINCT FROM $1.VERSION_REPORTED_UTC_TS OR VERSION_REPORTED_UTC_TS IS NOT DISTINCT FROM $2.VERSION_REPORTED_UTC_TS)';
		END IF;
		IF OLD.BASE_HOST_DIM_ID != NEW.BASE_HOST_DIM_ID THEN
			lv_sql := lv_sql || 'BASE_HOST_DIM_ID = $2.BASE_HOST_DIM_ID, ';
			lv_filter := lv_filter || ' AND (BASE_HOST_DIM_ID = $1.BASE_HOST_DIM_ID OR BASE_HOST_DIM_ID = $2.BASE_HOST_DIM_ID)';
		END IF;
		IF OLD.PRIMARY_HOST_DIM_ID != NEW.PRIMARY_HOST_DIM_ID THEN
			lv_sql := lv_sql || 'PRIMARY_HOST_DIM_ID = $2.PRIMARY_HOST_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PRIMARY_HOST_DIM_ID = $1.PRIMARY_HOST_DIM_ID OR PRIMARY_HOST_DIM_ID = $2.PRIMARY_HOST_DIM_ID)';
		END IF;
		IF OLD.BEZEL_HOST_DIM_ID != NEW.BEZEL_HOST_DIM_ID THEN
			lv_sql := lv_sql || 'BEZEL_HOST_DIM_ID = $2.BEZEL_HOST_DIM_ID, ';
			lv_filter := lv_filter || ' AND (BEZEL_HOST_DIM_ID = $1.BEZEL_HOST_DIM_ID OR BEZEL_HOST_DIM_ID = $2.BEZEL_HOST_DIM_ID)';
		END IF;
		IF OLD.MODEM_HOST_DIM_ID != NEW.MODEM_HOST_DIM_ID THEN
			lv_sql := lv_sql || 'MODEM_HOST_DIM_ID = $2.MODEM_HOST_DIM_ID, ';
			lv_filter := lv_filter || ' AND (MODEM_HOST_DIM_ID = $1.MODEM_HOST_DIM_ID OR MODEM_HOST_DIM_ID = $2.MODEM_HOST_DIM_ID)';
		END IF;
		IF OLD.CARDS_ACCEPTED != NEW.CARDS_ACCEPTED THEN
			lv_sql := lv_sql || 'CARDS_ACCEPTED = $2.CARDS_ACCEPTED, ';
			lv_filter := lv_filter || ' AND (CARDS_ACCEPTED = $1.CARDS_ACCEPTED OR CARDS_ACCEPTED = $2.CARDS_ACCEPTED)';
		END IF;
		IF OLD.DEX_MODE != NEW.DEX_MODE THEN
			lv_sql := lv_sql || 'DEX_MODE = $2.DEX_MODE, ';
			lv_filter := lv_filter || ' AND (DEX_MODE = $1.DEX_MODE OR DEX_MODE = $2.DEX_MODE)';
		END IF;
		IF OLD.VENDS_PER_TRAN IS DISTINCT FROM NEW.VENDS_PER_TRAN THEN
			lv_sql := lv_sql || 'VENDS_PER_TRAN = $2.VENDS_PER_TRAN, ';
			lv_filter := lv_filter || ' AND (VENDS_PER_TRAN IS NOT DISTINCT FROM $1.VENDS_PER_TRAN OR VENDS_PER_TRAN IS NOT DISTINCT FROM $2.VENDS_PER_TRAN)';
		END IF;
		IF OLD.CALL_SCHEDULE != NEW.CALL_SCHEDULE THEN
			lv_sql := lv_sql || 'CALL_SCHEDULE = $2.CALL_SCHEDULE, ';
			lv_filter := lv_filter || ' AND (CALL_SCHEDULE = $1.CALL_SCHEDULE OR CALL_SCHEDULE = $2.CALL_SCHEDULE)';
		END IF;
		IF OLD.DEX_SCHEDULE != NEW.DEX_SCHEDULE THEN
			lv_sql := lv_sql || 'DEX_SCHEDULE = $2.DEX_SCHEDULE, ';
			lv_filter := lv_filter || ' AND (DEX_SCHEDULE = $1.DEX_SCHEDULE OR DEX_SCHEDULE = $2.DEX_SCHEDULE)';
		END IF;
		IF OLD.COMM_METHOD_CD != NEW.COMM_METHOD_CD THEN
			lv_sql := lv_sql || 'COMM_METHOD_CD = $2.COMM_METHOD_CD, ';
			lv_filter := lv_filter || ' AND (COMM_METHOD_CD = $1.COMM_METHOD_CD OR COMM_METHOD_CD = $2.COMM_METHOD_CD)';
		END IF;
		IF OLD.COMM_METHOD_NAME != NEW.COMM_METHOD_NAME THEN
			lv_sql := lv_sql || 'COMM_METHOD_NAME = $2.COMM_METHOD_NAME, ';
			lv_filter := lv_filter || ' AND (COMM_METHOD_NAME = $1.COMM_METHOD_NAME OR COMM_METHOD_NAME = $2.COMM_METHOD_NAME)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE DEVICE_DIM_ID = $1.DEVICE_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'DEVICE_SERIAL_CD ~~* ''%' || UPPER(RIGHT(NEW.DEVICE_SERIAL_CD, 1)) || ''''||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(DEVICE_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._DEVICE_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_DEVICE_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE DEVICE_DIM_ID = $1.DEVICE_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.DEVICE_DIM
	AS SELECT * FROM RDW._DEVICE_DIM;

ALTER VIEW RDW.DEVICE_DIM ALTER DEVICE_DIM_ID SET DEFAULT nextval('RDW._device_dim_device_dim_id_seq'::regclass);
ALTER VIEW RDW.DEVICE_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.DEVICE_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.DEVICE_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.DEVICE_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.DEVICE_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._DEVICE_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_DEVICE_DIM ON RDW.DEVICE_DIM;
CREATE TRIGGER TRIIUD_DEVICE_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.DEVICE_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_DEVICE_DIM();


CREATE UNIQUE INDEX AK_DEVICE_DIM ON RDW._DEVICE_DIM(DEVICE_SERIAL_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DEVICE_SERIAL_CD ON RDW._DEVICE_DIM(DEVICE_SERIAL_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DEVICE_UNIQUE_NAME ON RDW._DEVICE_DIM(DEVICE_UNIQUE_NAME) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DEVICE_TYPE_ID ON RDW._DEVICE_DIM(DEVICE_TYPE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DEVICE_SUB_TYPE_ID ON RDW._DEVICE_DIM(DEVICE_SUB_TYPE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DEVICE_TYPE ON RDW._DEVICE_DIM(DEVICE_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_ACTIVE_FLAG ON RDW._DEVICE_DIM(ACTIVE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_FIRMWARE_VERSION ON RDW._DEVICE_DIM(FIRMWARE_VERSION) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DIAGNOSTIC_VERSION ON RDW._DEVICE_DIM(DIAGNOSTIC_VERSION) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_PTEST_VERSION ON RDW._DEVICE_DIM(PTEST_VERSION) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_PROPERTY_LIST_VERSION ON RDW._DEVICE_DIM(PROPERTY_LIST_VERSION) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_BASE_HOST_DIM_ID ON RDW._DEVICE_DIM(BASE_HOST_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_PRIMARY_HOST_DIM_ID ON RDW._DEVICE_DIM(PRIMARY_HOST_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_BEZEL_HOST_DIM_ID ON RDW._DEVICE_DIM(BEZEL_HOST_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_MODEM_HOST_DIM_ID ON RDW._DEVICE_DIM(MODEM_HOST_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_DEX_MODE ON RDW._DEVICE_DIM(DEX_MODE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_VENDS_PER_TRAN ON RDW._DEVICE_DIM(VENDS_PER_TRAN) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_COMM_METHOD_CD ON RDW._DEVICE_DIM(COMM_METHOD_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_DIM_CREATED_TS ON RDW._DEVICE_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.DEVICE_DIM ----------------------------------

INSERT INTO RDW.DEVICE_DIM(
	DEVICE_DIM_ID,
	DEVICE_SERIAL_CD,
	DEVICE_UNIQUE_NAME,
	DEVICE_TYPE_ID,
	DEVICE_SUB_TYPE_ID,
	DEVICE_TYPE,
	ACTIVE_FLAG,
	FIRMWARE_VERSION,
	DIAGNOSTIC_VERSION,
	PTEST_VERSION,
	PROPERTY_LIST_VERSION,
	VERSION_REPORTED_UTC_TS,
	BASE_HOST_DIM_ID,
	PRIMARY_HOST_DIM_ID,
	BEZEL_HOST_DIM_ID,
	MODEM_HOST_DIM_ID,
	CARDS_ACCEPTED,
	DEX_MODE,
	VENDS_PER_TRAN,
	CALL_SCHEDULE,
	DEX_SCHEDULE,
	COMM_METHOD_CD,
	COMM_METHOD_NAME,
	REVISION)
VALUES(
	0,
	'Unknown',
	'Unknown',
	0,
	0,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	0,
	0,
	0,
	0,
	'Unknown',
	'Unknown',
	NULL,
	'Unknown',
	'Unknown',
	'-',
	'Unknown',
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_DEVICE_DIM(
	p_device_dim_id OUT BIGINT, 
	p_device_serial_cd VARCHAR(50), 
	p_device_unique_name VARCHAR(60), 
	p_device_type_id SMALLINT, 
	p_device_sub_type_id SMALLINT, 
	p_device_type VARCHAR(60), 
	p_active_flag VARCHAR(10), 
	p_firmware_version VARCHAR(256), 
	p_diagnostic_version VARCHAR(256), 
	p_ptest_version VARCHAR(256), 
	p_property_list_version SMALLINT, 
	p_version_reported_utc_ts TIMESTAMPTZ, 
	p_base_host_dim_id BIGINT, 
	p_primary_host_dim_id BIGINT, 
	p_bezel_host_dim_id BIGINT, 
	p_modem_host_dim_id BIGINT, 
	p_cards_accepted VARCHAR(4000), 
	p_dex_mode VARCHAR(30), 
	p_vends_per_tran SMALLINT, 
	p_call_schedule VARCHAR(4000), 
	p_dex_schedule VARCHAR(4000), 
	p_comm_method_cd CHAR(1), 
	p_comm_method_name VARCHAR(30), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT DEVICE_DIM_ID, REVISION
			  INTO p_device_dim_id, p_old_revision
			  FROM RDW.DEVICE_DIM
			 WHERE DEVICE_SERIAL_CD = p_device_serial_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.DEVICE_DIM
			   SET DEVICE_UNIQUE_NAME = p_device_unique_name,
			       DEVICE_TYPE_ID = p_device_type_id,
			       DEVICE_SUB_TYPE_ID = p_device_sub_type_id,
			       DEVICE_TYPE = p_device_type,
			       ACTIVE_FLAG = p_active_flag,
			       FIRMWARE_VERSION = p_firmware_version,
			       DIAGNOSTIC_VERSION = p_diagnostic_version,
			       PTEST_VERSION = p_ptest_version,
			       PROPERTY_LIST_VERSION = p_property_list_version,
			       VERSION_REPORTED_UTC_TS = p_version_reported_utc_ts,
			       BASE_HOST_DIM_ID = p_base_host_dim_id,
			       PRIMARY_HOST_DIM_ID = p_primary_host_dim_id,
			       BEZEL_HOST_DIM_ID = p_bezel_host_dim_id,
			       MODEM_HOST_DIM_ID = p_modem_host_dim_id,
			       CARDS_ACCEPTED = p_cards_accepted,
			       DEX_MODE = p_dex_mode,
			       VENDS_PER_TRAN = p_vends_per_tran,
			       CALL_SCHEDULE = p_call_schedule,
			       DEX_SCHEDULE = p_dex_schedule,
			       COMM_METHOD_CD = p_comm_method_cd,
			       COMM_METHOD_NAME = p_comm_method_name,
			       REVISION = p_revision
			 WHERE DEVICE_SERIAL_CD = p_device_serial_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.DEVICE_DIM(DEVICE_SERIAL_CD, DEVICE_UNIQUE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, DEVICE_TYPE, ACTIVE_FLAG, FIRMWARE_VERSION, DIAGNOSTIC_VERSION, PTEST_VERSION, PROPERTY_LIST_VERSION, VERSION_REPORTED_UTC_TS, BASE_HOST_DIM_ID, PRIMARY_HOST_DIM_ID, BEZEL_HOST_DIM_ID, MODEM_HOST_DIM_ID, CARDS_ACCEPTED, DEX_MODE, VENDS_PER_TRAN, CALL_SCHEDULE, DEX_SCHEDULE, COMM_METHOD_CD, COMM_METHOD_NAME, REVISION)
					 VALUES(p_device_serial_cd, p_device_unique_name, p_device_type_id, p_device_sub_type_id, p_device_type, p_active_flag, p_firmware_version, p_diagnostic_version, p_ptest_version, p_property_list_version, p_version_reported_utc_ts, p_base_host_dim_id, p_primary_host_dim_id, p_bezel_host_dim_id, p_modem_host_dim_id, p_cards_accepted, p_dex_mode, p_vends_per_tran, p_call_schedule, p_dex_schedule, p_comm_method_cd, p_comm_method_name, p_revision)
					 RETURNING DEVICE_DIM_ID
					      INTO p_device_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_DEVICE_DIM(
	p_device_dim_id OUT BIGINT, 
	p_device_serial_cd VARCHAR(50), 
	p_device_unique_name VARCHAR(60), 
	p_device_type_id SMALLINT, 
	p_device_sub_type_id SMALLINT, 
	p_device_type VARCHAR(60), 
	p_active_flag VARCHAR(10), 
	p_firmware_version VARCHAR(256), 
	p_diagnostic_version VARCHAR(256), 
	p_ptest_version VARCHAR(256), 
	p_property_list_version SMALLINT, 
	p_version_reported_utc_ts TIMESTAMPTZ, 
	p_base_host_dim_id BIGINT, 
	p_primary_host_dim_id BIGINT, 
	p_bezel_host_dim_id BIGINT, 
	p_modem_host_dim_id BIGINT, 
	p_cards_accepted VARCHAR(4000), 
	p_dex_mode VARCHAR(30), 
	p_vends_per_tran SMALLINT, 
	p_call_schedule VARCHAR(4000), 
	p_dex_schedule VARCHAR(4000), 
	p_comm_method_cd CHAR(1), 
	p_comm_method_name VARCHAR(30), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._LOCATION_BRIDGE
  (
    ANCESTOR_LOCATION_DIM_ID   BIGINT       NOT NULL,
    DESCENDENT_LOCATION_DIM_ID BIGINT       NOT NULL,
    LEVELS_BETWEEN             SMALLINT     NOT NULL,
    HIERARCHY_TYPE             VARCHAR(100) NOT NULL,
    ACTIVE_FLAG                VARCHAR(10)  NOT NULL,

    CONSTRAINT PK_LOCATION_BRIDGE PRIMARY KEY(ANCESTOR_LOCATION_DIM_ID, DESCENDENT_LOCATION_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_LOCATION_BRIDGE_ANCESTOR_LOCATION_DIM_ID FOREIGN KEY(ANCESTOR_LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_LOCATION_BRIDGE_DESCENDENT_LOCATION_DIM_ID FOREIGN KEY(DESCENDENT_LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_LOCATION_BRIDGE()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((NEW.ANCESTOR_LOCATION_DIM_ID % 11)::VARCHAR,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_LOCATION_BRIDGE', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_LOCATION_BRIDGE', COALESCE((OLD.ANCESTOR_LOCATION_DIM_ID % 11)::VARCHAR, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ANCESTOR_LOCATION_DIM_ID != NEW.ANCESTOR_LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'ANCESTOR_LOCATION_DIM_ID = $2.ANCESTOR_LOCATION_DIM_ID, ';
		END IF;
		IF OLD.DESCENDENT_LOCATION_DIM_ID != NEW.DESCENDENT_LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'DESCENDENT_LOCATION_DIM_ID = $2.DESCENDENT_LOCATION_DIM_ID, ';
		END IF;
		IF OLD.LEVELS_BETWEEN != NEW.LEVELS_BETWEEN THEN
			lv_sql := lv_sql || 'LEVELS_BETWEEN = $2.LEVELS_BETWEEN, ';
			lv_filter := lv_filter || ' AND (LEVELS_BETWEEN = $1.LEVELS_BETWEEN OR LEVELS_BETWEEN = $2.LEVELS_BETWEEN)';
		END IF;
		IF OLD.HIERARCHY_TYPE != NEW.HIERARCHY_TYPE THEN
			lv_sql := lv_sql || 'HIERARCHY_TYPE = $2.HIERARCHY_TYPE, ';
			lv_filter := lv_filter || ' AND (HIERARCHY_TYPE = $1.HIERARCHY_TYPE OR HIERARCHY_TYPE = $2.HIERARCHY_TYPE)';
		END IF;
		IF OLD.ACTIVE_FLAG != NEW.ACTIVE_FLAG THEN
			lv_sql := lv_sql || 'ACTIVE_FLAG = $2.ACTIVE_FLAG, ';
			lv_filter := lv_filter || ' AND (ACTIVE_FLAG = $1.ACTIVE_FLAG OR ACTIVE_FLAG = $2.ACTIVE_FLAG)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ANCESTOR_LOCATION_DIM_ID = $1.ANCESTOR_LOCATION_DIM_ID AND DESCENDENT_LOCATION_DIM_ID = $1.DESCENDENT_LOCATION_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'ANCESTOR_LOCATION_DIM_ID % 11 = ' || NEW.ANCESTOR_LOCATION_DIM_ID % 11||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ANCESTOR_LOCATION_DIM_ID, DESCENDENT_LOCATION_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._LOCATION_BRIDGE) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_LOCATION_BRIDGE', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ANCESTOR_LOCATION_DIM_ID = $1.ANCESTOR_LOCATION_DIM_ID AND DESCENDENT_LOCATION_DIM_ID = $1.DESCENDENT_LOCATION_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.LOCATION_BRIDGE
	AS SELECT * FROM RDW._LOCATION_BRIDGE;

ALTER VIEW RDW.LOCATION_BRIDGE ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.LOCATION_BRIDGE ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.LOCATION_BRIDGE ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.LOCATION_BRIDGE TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.LOCATION_BRIDGE TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._LOCATION_BRIDGE FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_LOCATION_BRIDGE ON RDW.LOCATION_BRIDGE;
CREATE TRIGGER TRIIUD_LOCATION_BRIDGE
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.LOCATION_BRIDGE
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_LOCATION_BRIDGE();


CREATE INDEX IX_LOCATION_BRIDGE_ANCESTOR_LOCATION_DIM_ID ON RDW._LOCATION_BRIDGE(ANCESTOR_LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_BRIDGE_DESCENDENT_LOCATION_DIM_ID ON RDW._LOCATION_BRIDGE(DESCENDENT_LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_BRIDGE_ACTIVE_FLAG ON RDW._LOCATION_BRIDGE(ACTIVE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LOCATION_BRIDGE_CREATED_TS ON RDW._LOCATION_BRIDGE(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_LOCATION_BRIDGE(
	p_ancestor_location_dim_id BIGINT, 
	p_descendent_location_dim_id BIGINT, 
	p_levels_between SMALLINT, 
	p_hierarchy_type VARCHAR(100), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT REVISION
			  INTO p_old_revision
			  FROM RDW.LOCATION_BRIDGE
			 WHERE ANCESTOR_LOCATION_DIM_ID = p_ancestor_location_dim_id
			   AND DESCENDENT_LOCATION_DIM_ID = p_descendent_location_dim_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.LOCATION_BRIDGE
			   SET LEVELS_BETWEEN = p_levels_between,
			       HIERARCHY_TYPE = p_hierarchy_type,
			       ACTIVE_FLAG = p_active_flag,
			       REVISION = p_revision
			 WHERE ANCESTOR_LOCATION_DIM_ID = p_ancestor_location_dim_id
			   AND DESCENDENT_LOCATION_DIM_ID = p_descendent_location_dim_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.LOCATION_BRIDGE(ANCESTOR_LOCATION_DIM_ID, DESCENDENT_LOCATION_DIM_ID, LEVELS_BETWEEN, HIERARCHY_TYPE, ACTIVE_FLAG, REVISION)
					 VALUES(p_ancestor_location_dim_id, p_descendent_location_dim_id, p_levels_between, p_hierarchy_type, p_active_flag, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_LOCATION_BRIDGE(
	p_ancestor_location_dim_id BIGINT, 
	p_descendent_location_dim_id BIGINT, 
	p_levels_between SMALLINT, 
	p_hierarchy_type VARCHAR(100), 
	p_active_flag VARCHAR(10), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._PAYMENT_BATCH_DIM
  (
    PAYMENT_BATCH_DIM_ID BIGSERIAL    NOT NULL,
    SOURCE_BATCH_ID      BIGINT       NOT NULL,
    PAYMENT_DIM_ID       BIGINT       NOT NULL,
    PAYMENT_SCHEDULE_ID  SMALLINT     NOT NULL,
    BATCH_TYPE           VARCHAR(100) NOT NULL,
    BATCH_STATE          VARCHAR(30)  NOT NULL,
    BATCH_START_TS       TIMESTAMPTZ  ,
    BATCH_END_TS         TIMESTAMPTZ  ,
    BATCH_CLOSABLE_FLAG  VARCHAR(10)  NOT NULL,
    PAID_TS              TIMESTAMPTZ  ,
    BATCH_REF_CD         VARCHAR(10)  NOT NULL,
    BATCH_CLOSED_TS      TIMESTAMPTZ  ,
    BATCH_CONFIRMED_TS   TIMESTAMPTZ  ,

    CONSTRAINT PK_PAYMENT_BATCH_DIM PRIMARY KEY(PAYMENT_BATCH_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_PAYMENT_BATCH_DIM_PAYMENT_DIM_ID FOREIGN KEY(PAYMENT_DIM_ID) REFERENCES RDW._PAYMENT_DIM(PAYMENT_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_PAYMENT_BATCH_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.PAID_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_PAYMENT_BATCH_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_PAYMENT_BATCH_DIM', COALESCE(TO_CHAR(OLD.PAID_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.PAYMENT_BATCH_DIM_ID != NEW.PAYMENT_BATCH_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_BATCH_ID != NEW.SOURCE_BATCH_ID THEN
			lv_sql := lv_sql || 'SOURCE_BATCH_ID = $2.SOURCE_BATCH_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_BATCH_ID = $1.SOURCE_BATCH_ID OR SOURCE_BATCH_ID = $2.SOURCE_BATCH_ID)';
		END IF;
		IF OLD.PAYMENT_DIM_ID != NEW.PAYMENT_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_DIM_ID = $1.PAYMENT_DIM_ID OR PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_SCHEDULE_ID != NEW.PAYMENT_SCHEDULE_ID THEN
			lv_sql := lv_sql || 'PAYMENT_SCHEDULE_ID = $2.PAYMENT_SCHEDULE_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_SCHEDULE_ID = $1.PAYMENT_SCHEDULE_ID OR PAYMENT_SCHEDULE_ID = $2.PAYMENT_SCHEDULE_ID)';
		END IF;
		IF OLD.BATCH_TYPE != NEW.BATCH_TYPE THEN
			lv_sql := lv_sql || 'BATCH_TYPE = $2.BATCH_TYPE, ';
			lv_filter := lv_filter || ' AND (BATCH_TYPE = $1.BATCH_TYPE OR BATCH_TYPE = $2.BATCH_TYPE)';
		END IF;
		IF OLD.BATCH_STATE != NEW.BATCH_STATE THEN
			lv_sql := lv_sql || 'BATCH_STATE = $2.BATCH_STATE, ';
			lv_filter := lv_filter || ' AND (BATCH_STATE = $1.BATCH_STATE OR BATCH_STATE = $2.BATCH_STATE)';
		END IF;
		IF OLD.BATCH_START_TS IS DISTINCT FROM NEW.BATCH_START_TS THEN
			lv_sql := lv_sql || 'BATCH_START_TS = $2.BATCH_START_TS, ';
			lv_filter := lv_filter || ' AND (BATCH_START_TS IS NOT DISTINCT FROM $1.BATCH_START_TS OR BATCH_START_TS IS NOT DISTINCT FROM $2.BATCH_START_TS)';
		END IF;
		IF OLD.BATCH_END_TS IS DISTINCT FROM NEW.BATCH_END_TS THEN
			lv_sql := lv_sql || 'BATCH_END_TS = $2.BATCH_END_TS, ';
			lv_filter := lv_filter || ' AND (BATCH_END_TS IS NOT DISTINCT FROM $1.BATCH_END_TS OR BATCH_END_TS IS NOT DISTINCT FROM $2.BATCH_END_TS)';
		END IF;
		IF OLD.BATCH_CLOSABLE_FLAG != NEW.BATCH_CLOSABLE_FLAG THEN
			lv_sql := lv_sql || 'BATCH_CLOSABLE_FLAG = $2.BATCH_CLOSABLE_FLAG, ';
			lv_filter := lv_filter || ' AND (BATCH_CLOSABLE_FLAG = $1.BATCH_CLOSABLE_FLAG OR BATCH_CLOSABLE_FLAG = $2.BATCH_CLOSABLE_FLAG)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.BATCH_REF_CD != NEW.BATCH_REF_CD THEN
			lv_sql := lv_sql || 'BATCH_REF_CD = $2.BATCH_REF_CD, ';
			lv_filter := lv_filter || ' AND (BATCH_REF_CD = $1.BATCH_REF_CD OR BATCH_REF_CD = $2.BATCH_REF_CD)';
		END IF;
		IF OLD.BATCH_CLOSED_TS IS DISTINCT FROM NEW.BATCH_CLOSED_TS THEN
			lv_sql := lv_sql || 'BATCH_CLOSED_TS = $2.BATCH_CLOSED_TS, ';
			lv_filter := lv_filter || ' AND (BATCH_CLOSED_TS IS NOT DISTINCT FROM $1.BATCH_CLOSED_TS OR BATCH_CLOSED_TS IS NOT DISTINCT FROM $2.BATCH_CLOSED_TS)';
		END IF;
		IF OLD.BATCH_CONFIRMED_TS IS DISTINCT FROM NEW.BATCH_CONFIRMED_TS THEN
			lv_sql := lv_sql || 'BATCH_CONFIRMED_TS = $2.BATCH_CONFIRMED_TS, ';
			lv_filter := lv_filter || ' AND (BATCH_CONFIRMED_TS IS NOT DISTINCT FROM $1.BATCH_CONFIRMED_TS OR BATCH_CONFIRMED_TS IS NOT DISTINCT FROM $2.BATCH_CONFIRMED_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE PAYMENT_BATCH_DIM_ID = $1.PAYMENT_BATCH_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.PAID_TS IS NULL THEN 'PAID_TS IS NULL' ELSE 'PAID_TS >= TIMESTAMP'''||TO_CHAR(NEW.PAID_TS, 'YYYY-MM-01')||''' AND PAID_TS < TIMESTAMP'''||TO_CHAR(NEW.PAID_TS + '1 month', 'YYYY-MM-01')||'''' END||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(PAYMENT_BATCH_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._PAYMENT_BATCH_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_PAYMENT_BATCH_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE PAYMENT_BATCH_DIM_ID = $1.PAYMENT_BATCH_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.PAYMENT_BATCH_DIM
	AS SELECT * FROM RDW._PAYMENT_BATCH_DIM;

ALTER VIEW RDW.PAYMENT_BATCH_DIM ALTER PAYMENT_BATCH_DIM_ID SET DEFAULT nextval('RDW._payment_batch_dim_payment_batch_dim_id_seq'::regclass);
ALTER VIEW RDW.PAYMENT_BATCH_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.PAYMENT_BATCH_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.PAYMENT_BATCH_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.PAYMENT_BATCH_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.PAYMENT_BATCH_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._PAYMENT_BATCH_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_PAYMENT_BATCH_DIM ON RDW.PAYMENT_BATCH_DIM;
CREATE TRIGGER TRIIUD_PAYMENT_BATCH_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.PAYMENT_BATCH_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_PAYMENT_BATCH_DIM();


CREATE UNIQUE INDEX AK_PAYMENT_BATCH_DIM ON RDW._PAYMENT_BATCH_DIM(SOURCE_BATCH_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_PAYMENT_DIM_ID ON RDW._PAYMENT_BATCH_DIM(PAYMENT_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_PAYMENT_SCHEDULE_ID ON RDW._PAYMENT_BATCH_DIM(PAYMENT_SCHEDULE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_BATCH_TYPE ON RDW._PAYMENT_BATCH_DIM(BATCH_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_BATCH_START_TS ON RDW._PAYMENT_BATCH_DIM(BATCH_START_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_BATCH_END_TS ON RDW._PAYMENT_BATCH_DIM(BATCH_END_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_BATCH_CLOSABLE_FLAG ON RDW._PAYMENT_BATCH_DIM(BATCH_CLOSABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_PAID_TS ON RDW._PAYMENT_BATCH_DIM(PAID_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_BATCH_REF_CD ON RDW._PAYMENT_BATCH_DIM(BATCH_REF_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_BATCH_DIM_CREATED_TS ON RDW._PAYMENT_BATCH_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.PAYMENT_BATCH_DIM ----------------------------------

INSERT INTO RDW.PAYMENT_BATCH_DIM(
	PAYMENT_BATCH_DIM_ID,
	SOURCE_BATCH_ID,
	PAYMENT_DIM_ID,
	PAYMENT_SCHEDULE_ID,
	BATCH_TYPE,
	BATCH_STATE,
	BATCH_START_TS,
	BATCH_END_TS,
	BATCH_CLOSABLE_FLAG,
	PAID_TS,
	BATCH_REF_CD,
	BATCH_CLOSED_TS,
	BATCH_CONFIRMED_TS,
	REVISION)
VALUES(
	0,
	0,
	0,
	0,
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	'Unknown',
	NULL,
	'Unknown',
	NULL,
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_PAYMENT_BATCH_DIM(
	p_payment_batch_dim_id OUT BIGINT, 
	p_source_batch_id BIGINT, 
	p_payment_dim_id BIGINT, 
	p_payment_schedule_id SMALLINT, 
	p_batch_type VARCHAR(100), 
	p_batch_state VARCHAR(30), 
	p_batch_start_ts TIMESTAMPTZ, 
	p_batch_end_ts TIMESTAMPTZ, 
	p_batch_closable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_batch_ref_cd VARCHAR(10), 
	p_batch_closed_ts TIMESTAMPTZ, 
	p_batch_confirmed_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT PAYMENT_BATCH_DIM_ID, REVISION
			  INTO p_payment_batch_dim_id, p_old_revision
			  FROM RDW.PAYMENT_BATCH_DIM
			 WHERE SOURCE_BATCH_ID = p_source_batch_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.PAYMENT_BATCH_DIM
			   SET PAYMENT_DIM_ID = p_payment_dim_id,
			       PAYMENT_SCHEDULE_ID = p_payment_schedule_id,
			       BATCH_TYPE = p_batch_type,
			       BATCH_STATE = p_batch_state,
			       BATCH_START_TS = p_batch_start_ts,
			       BATCH_END_TS = p_batch_end_ts,
			       BATCH_CLOSABLE_FLAG = p_batch_closable_flag,
			       PAID_TS = p_paid_ts,
			       BATCH_REF_CD = p_batch_ref_cd,
			       BATCH_CLOSED_TS = p_batch_closed_ts,
			       BATCH_CONFIRMED_TS = p_batch_confirmed_ts,
			       REVISION = p_revision
			 WHERE SOURCE_BATCH_ID = p_source_batch_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.PAYMENT_BATCH_DIM(SOURCE_BATCH_ID, PAYMENT_DIM_ID, PAYMENT_SCHEDULE_ID, BATCH_TYPE, BATCH_STATE, BATCH_START_TS, BATCH_END_TS, BATCH_CLOSABLE_FLAG, PAID_TS, BATCH_REF_CD, BATCH_CLOSED_TS, BATCH_CONFIRMED_TS, REVISION)
					 VALUES(p_source_batch_id, p_payment_dim_id, p_payment_schedule_id, p_batch_type, p_batch_state, p_batch_start_ts, p_batch_end_ts, p_batch_closable_flag, p_paid_ts, p_batch_ref_cd, p_batch_closed_ts, p_batch_confirmed_ts, p_revision)
					 RETURNING PAYMENT_BATCH_DIM_ID
					      INTO p_payment_batch_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_PAYMENT_BATCH_DIM(
	p_payment_batch_dim_id OUT BIGINT, 
	p_source_batch_id BIGINT, 
	p_payment_dim_id BIGINT, 
	p_payment_schedule_id SMALLINT, 
	p_batch_type VARCHAR(100), 
	p_batch_state VARCHAR(30), 
	p_batch_start_ts TIMESTAMPTZ, 
	p_batch_end_ts TIMESTAMPTZ, 
	p_batch_closable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_batch_ref_cd VARCHAR(10), 
	p_batch_closed_ts TIMESTAMPTZ, 
	p_batch_confirmed_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._POS_DIM
  (
    POS_DIM_ID                 BIGSERIAL     NOT NULL,
    SOURCE_SYSTEM_CD           VARCHAR(30)   NOT NULL,
    SOURCE_POS_ID              BIGINT        NOT NULL,
    SOURCE_LOCATION_ID         BIGINT        NOT NULL,
    SOURCE_CUSTOMER_ID         BIGINT        NOT NULL,
    SOURCE_BUSINESS_UNIT_ID    SMALLINT      ,
    SOURCE_PAYMENT_SCHEDULE_ID SMALLINT      ,
    LOCATION_DIM_ID            BIGINT        NOT NULL,
    DEVICE_DIM_ID              BIGINT        NOT NULL,
    DEVICE_SERIAL_CD           VARCHAR(50)   NOT NULL,
    BUSINESS_UNIT_NAME         VARCHAR(100)  NOT NULL,
    PAYMENT_SCHEDULE_DESC      VARCHAR(100)  NOT NULL,
    PRODUCT_TYPE               VARCHAR(100)  NOT NULL,
    PRODUCT_SUB_TYPE           VARCHAR(100)  ,
    REGISTERED_TS              TIMESTAMPTZ   ,
    SHIPPED_TS                 TIMESTAMPTZ   ,
    POS_START_TS               TIMESTAMPTZ   NOT NULL DEFAULT TIMESTAMP'-infinity',
    POS_END_TS                 TIMESTAMPTZ   NOT NULL DEFAULT TIMESTAMP'infinity',
    ACTIVE_FLAG                VARCHAR(10)   NOT NULL,
    FIRST_CALL_SUCCESS_TS      TIMESTAMPTZ   ,
    FIRST_CASH_TRAN_TS         TIMESTAMPTZ   ,
    FIRST_CREDIT_TRAN_TS       TIMESTAMPTZ   ,
    FIRST_OTHERCARD_TRAN_TS    TIMESTAMPTZ   ,
    LAST_CALL_SUCCESS_TS       TIMESTAMPTZ   ,
    LAST_CALL_UTC_TS           TIMESTAMPTZ   ,
    LAST_CASH_TRAN_TS          TIMESTAMPTZ   ,
    LAST_CREDIT_TRAN_TS        TIMESTAMPTZ   ,
    LAST_OTHERCARD_TRAN_TS     TIMESTAMPTZ   ,
    LAST_FILL_TS               TIMESTAMPTZ   ,
    LAST_DEX_TS                TIMESTAMPTZ   ,
    ASSET_NBR                  VARCHAR(50)   NOT NULL,
    TERMINAL_NBR               VARCHAR(20)   NOT NULL,
    TERM_LABEL_1               VARCHAR(50)   ,
    TERM_VAR_1                 VARCHAR(4000) ,
    TERM_LABEL_2               VARCHAR(50)   ,
    TERM_VAR_2                 VARCHAR(4000) ,
    TERM_LABEL_3               VARCHAR(50)   ,
    TERM_VAR_3                 VARCHAR(4000) ,
    TERM_LABEL_4               VARCHAR(50)   ,
    TERM_VAR_4                 VARCHAR(4000) ,
    TERM_LABEL_5               VARCHAR(50)   ,
    TERM_VAR_5                 VARCHAR(4000) ,
    TERM_LABEL_6               VARCHAR(50)   ,
    TERM_VAR_6                 VARCHAR(4000) ,
    TERM_LABEL_7               VARCHAR(50)   ,
    TERM_VAR_7                 VARCHAR(4000) ,
    TERM_LABEL_8               VARCHAR(50)   ,
    TERM_VAR_8                 VARCHAR(4000) ,

    CONSTRAINT PK_POS_DIM PRIMARY KEY(POS_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_POS_DIM_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_POS_DIM_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_POS_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((NEW.SOURCE_CUSTOMER_ID % 17)::VARCHAR,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_POS_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_POS_DIM', COALESCE((OLD.SOURCE_CUSTOMER_ID % 17)::VARCHAR, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD != NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD = $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.SOURCE_POS_ID != NEW.SOURCE_POS_ID THEN
			lv_sql := lv_sql || 'SOURCE_POS_ID = $2.SOURCE_POS_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_POS_ID = $1.SOURCE_POS_ID OR SOURCE_POS_ID = $2.SOURCE_POS_ID)';
		END IF;
		IF OLD.SOURCE_LOCATION_ID != NEW.SOURCE_LOCATION_ID THEN
			lv_sql := lv_sql || 'SOURCE_LOCATION_ID = $2.SOURCE_LOCATION_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_LOCATION_ID = $1.SOURCE_LOCATION_ID OR SOURCE_LOCATION_ID = $2.SOURCE_LOCATION_ID)';
		END IF;
		IF OLD.SOURCE_CUSTOMER_ID != NEW.SOURCE_CUSTOMER_ID THEN
			lv_sql := lv_sql || 'SOURCE_CUSTOMER_ID = $2.SOURCE_CUSTOMER_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_CUSTOMER_ID = $1.SOURCE_CUSTOMER_ID OR SOURCE_CUSTOMER_ID = $2.SOURCE_CUSTOMER_ID)';
		END IF;
		IF OLD.SOURCE_BUSINESS_UNIT_ID IS DISTINCT FROM NEW.SOURCE_BUSINESS_UNIT_ID THEN
			lv_sql := lv_sql || 'SOURCE_BUSINESS_UNIT_ID = $2.SOURCE_BUSINESS_UNIT_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_BUSINESS_UNIT_ID IS NOT DISTINCT FROM $1.SOURCE_BUSINESS_UNIT_ID OR SOURCE_BUSINESS_UNIT_ID IS NOT DISTINCT FROM $2.SOURCE_BUSINESS_UNIT_ID)';
		END IF;
		IF OLD.SOURCE_PAYMENT_SCHEDULE_ID IS DISTINCT FROM NEW.SOURCE_PAYMENT_SCHEDULE_ID THEN
			lv_sql := lv_sql || 'SOURCE_PAYMENT_SCHEDULE_ID = $2.SOURCE_PAYMENT_SCHEDULE_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_PAYMENT_SCHEDULE_ID IS NOT DISTINCT FROM $1.SOURCE_PAYMENT_SCHEDULE_ID OR SOURCE_PAYMENT_SCHEDULE_ID IS NOT DISTINCT FROM $2.SOURCE_PAYMENT_SCHEDULE_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD != NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD = $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.BUSINESS_UNIT_NAME != NEW.BUSINESS_UNIT_NAME THEN
			lv_sql := lv_sql || 'BUSINESS_UNIT_NAME = $2.BUSINESS_UNIT_NAME, ';
			lv_filter := lv_filter || ' AND (BUSINESS_UNIT_NAME = $1.BUSINESS_UNIT_NAME OR BUSINESS_UNIT_NAME = $2.BUSINESS_UNIT_NAME)';
		END IF;
		IF OLD.PAYMENT_SCHEDULE_DESC != NEW.PAYMENT_SCHEDULE_DESC THEN
			lv_sql := lv_sql || 'PAYMENT_SCHEDULE_DESC = $2.PAYMENT_SCHEDULE_DESC, ';
			lv_filter := lv_filter || ' AND (PAYMENT_SCHEDULE_DESC = $1.PAYMENT_SCHEDULE_DESC OR PAYMENT_SCHEDULE_DESC = $2.PAYMENT_SCHEDULE_DESC)';
		END IF;
		IF OLD.PRODUCT_TYPE != NEW.PRODUCT_TYPE THEN
			lv_sql := lv_sql || 'PRODUCT_TYPE = $2.PRODUCT_TYPE, ';
			lv_filter := lv_filter || ' AND (PRODUCT_TYPE = $1.PRODUCT_TYPE OR PRODUCT_TYPE = $2.PRODUCT_TYPE)';
		END IF;
		IF OLD.PRODUCT_SUB_TYPE IS DISTINCT FROM NEW.PRODUCT_SUB_TYPE THEN
			lv_sql := lv_sql || 'PRODUCT_SUB_TYPE = $2.PRODUCT_SUB_TYPE, ';
			lv_filter := lv_filter || ' AND (PRODUCT_SUB_TYPE IS NOT DISTINCT FROM $1.PRODUCT_SUB_TYPE OR PRODUCT_SUB_TYPE IS NOT DISTINCT FROM $2.PRODUCT_SUB_TYPE)';
		END IF;
		IF OLD.REGISTERED_TS IS DISTINCT FROM NEW.REGISTERED_TS THEN
			lv_sql := lv_sql || 'REGISTERED_TS = $2.REGISTERED_TS, ';
			lv_filter := lv_filter || ' AND (REGISTERED_TS IS NOT DISTINCT FROM $1.REGISTERED_TS OR REGISTERED_TS IS NOT DISTINCT FROM $2.REGISTERED_TS)';
		END IF;
		IF OLD.SHIPPED_TS IS DISTINCT FROM NEW.SHIPPED_TS THEN
			lv_sql := lv_sql || 'SHIPPED_TS = $2.SHIPPED_TS, ';
			lv_filter := lv_filter || ' AND (SHIPPED_TS IS NOT DISTINCT FROM $1.SHIPPED_TS OR SHIPPED_TS IS NOT DISTINCT FROM $2.SHIPPED_TS)';
		END IF;
		IF OLD.POS_START_TS != NEW.POS_START_TS THEN
			lv_sql := lv_sql || 'POS_START_TS = $2.POS_START_TS, ';
			lv_filter := lv_filter || ' AND (POS_START_TS = $1.POS_START_TS OR POS_START_TS = $2.POS_START_TS)';
		END IF;
		IF OLD.POS_END_TS != NEW.POS_END_TS THEN
			lv_sql := lv_sql || 'POS_END_TS = $2.POS_END_TS, ';
			lv_filter := lv_filter || ' AND (POS_END_TS = $1.POS_END_TS OR POS_END_TS = $2.POS_END_TS)';
		END IF;
		IF OLD.ACTIVE_FLAG != NEW.ACTIVE_FLAG THEN
			lv_sql := lv_sql || 'ACTIVE_FLAG = $2.ACTIVE_FLAG, ';
			lv_filter := lv_filter || ' AND (ACTIVE_FLAG = $1.ACTIVE_FLAG OR ACTIVE_FLAG = $2.ACTIVE_FLAG)';
		END IF;
		IF OLD.FIRST_CALL_SUCCESS_TS IS DISTINCT FROM NEW.FIRST_CALL_SUCCESS_TS THEN
			lv_sql := lv_sql || 'FIRST_CALL_SUCCESS_TS = $2.FIRST_CALL_SUCCESS_TS, ';
			lv_filter := lv_filter || ' AND (FIRST_CALL_SUCCESS_TS IS NOT DISTINCT FROM $1.FIRST_CALL_SUCCESS_TS OR FIRST_CALL_SUCCESS_TS IS NOT DISTINCT FROM $2.FIRST_CALL_SUCCESS_TS)';
		END IF;
		IF OLD.FIRST_CASH_TRAN_TS IS DISTINCT FROM NEW.FIRST_CASH_TRAN_TS THEN
			lv_sql := lv_sql || 'FIRST_CASH_TRAN_TS = $2.FIRST_CASH_TRAN_TS, ';
			lv_filter := lv_filter || ' AND (FIRST_CASH_TRAN_TS IS NOT DISTINCT FROM $1.FIRST_CASH_TRAN_TS OR FIRST_CASH_TRAN_TS IS NOT DISTINCT FROM $2.FIRST_CASH_TRAN_TS)';
		END IF;
		IF OLD.FIRST_CREDIT_TRAN_TS IS DISTINCT FROM NEW.FIRST_CREDIT_TRAN_TS THEN
			lv_sql := lv_sql || 'FIRST_CREDIT_TRAN_TS = $2.FIRST_CREDIT_TRAN_TS, ';
			lv_filter := lv_filter || ' AND (FIRST_CREDIT_TRAN_TS IS NOT DISTINCT FROM $1.FIRST_CREDIT_TRAN_TS OR FIRST_CREDIT_TRAN_TS IS NOT DISTINCT FROM $2.FIRST_CREDIT_TRAN_TS)';
		END IF;
		IF OLD.FIRST_OTHERCARD_TRAN_TS IS DISTINCT FROM NEW.FIRST_OTHERCARD_TRAN_TS THEN
			lv_sql := lv_sql || 'FIRST_OTHERCARD_TRAN_TS = $2.FIRST_OTHERCARD_TRAN_TS, ';
			lv_filter := lv_filter || ' AND (FIRST_OTHERCARD_TRAN_TS IS NOT DISTINCT FROM $1.FIRST_OTHERCARD_TRAN_TS OR FIRST_OTHERCARD_TRAN_TS IS NOT DISTINCT FROM $2.FIRST_OTHERCARD_TRAN_TS)';
		END IF;
		IF OLD.LAST_CALL_SUCCESS_TS IS DISTINCT FROM NEW.LAST_CALL_SUCCESS_TS THEN
			lv_sql := lv_sql || 'LAST_CALL_SUCCESS_TS = $2.LAST_CALL_SUCCESS_TS, ';
			lv_filter := lv_filter || ' AND (LAST_CALL_SUCCESS_TS IS NOT DISTINCT FROM $1.LAST_CALL_SUCCESS_TS OR LAST_CALL_SUCCESS_TS IS NOT DISTINCT FROM $2.LAST_CALL_SUCCESS_TS)';
		END IF;
		IF OLD.LAST_CALL_UTC_TS IS DISTINCT FROM NEW.LAST_CALL_UTC_TS THEN
			lv_sql := lv_sql || 'LAST_CALL_UTC_TS = $2.LAST_CALL_UTC_TS, ';
			lv_filter := lv_filter || ' AND (LAST_CALL_UTC_TS IS NOT DISTINCT FROM $1.LAST_CALL_UTC_TS OR LAST_CALL_UTC_TS IS NOT DISTINCT FROM $2.LAST_CALL_UTC_TS)';
		END IF;
		IF OLD.LAST_CASH_TRAN_TS IS DISTINCT FROM NEW.LAST_CASH_TRAN_TS THEN
			lv_sql := lv_sql || 'LAST_CASH_TRAN_TS = $2.LAST_CASH_TRAN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_CASH_TRAN_TS IS NOT DISTINCT FROM $1.LAST_CASH_TRAN_TS OR LAST_CASH_TRAN_TS IS NOT DISTINCT FROM $2.LAST_CASH_TRAN_TS)';
		END IF;
		IF OLD.LAST_CREDIT_TRAN_TS IS DISTINCT FROM NEW.LAST_CREDIT_TRAN_TS THEN
			lv_sql := lv_sql || 'LAST_CREDIT_TRAN_TS = $2.LAST_CREDIT_TRAN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_CREDIT_TRAN_TS IS NOT DISTINCT FROM $1.LAST_CREDIT_TRAN_TS OR LAST_CREDIT_TRAN_TS IS NOT DISTINCT FROM $2.LAST_CREDIT_TRAN_TS)';
		END IF;
		IF OLD.LAST_OTHERCARD_TRAN_TS IS DISTINCT FROM NEW.LAST_OTHERCARD_TRAN_TS THEN
			lv_sql := lv_sql || 'LAST_OTHERCARD_TRAN_TS = $2.LAST_OTHERCARD_TRAN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_OTHERCARD_TRAN_TS IS NOT DISTINCT FROM $1.LAST_OTHERCARD_TRAN_TS OR LAST_OTHERCARD_TRAN_TS IS NOT DISTINCT FROM $2.LAST_OTHERCARD_TRAN_TS)';
		END IF;
		IF OLD.LAST_FILL_TS IS DISTINCT FROM NEW.LAST_FILL_TS THEN
			lv_sql := lv_sql || 'LAST_FILL_TS = $2.LAST_FILL_TS, ';
			lv_filter := lv_filter || ' AND (LAST_FILL_TS IS NOT DISTINCT FROM $1.LAST_FILL_TS OR LAST_FILL_TS IS NOT DISTINCT FROM $2.LAST_FILL_TS)';
		END IF;
		IF OLD.LAST_DEX_TS IS DISTINCT FROM NEW.LAST_DEX_TS THEN
			lv_sql := lv_sql || 'LAST_DEX_TS = $2.LAST_DEX_TS, ';
			lv_filter := lv_filter || ' AND (LAST_DEX_TS IS NOT DISTINCT FROM $1.LAST_DEX_TS OR LAST_DEX_TS IS NOT DISTINCT FROM $2.LAST_DEX_TS)';
		END IF;
		IF OLD.ASSET_NBR != NEW.ASSET_NBR THEN
			lv_sql := lv_sql || 'ASSET_NBR = $2.ASSET_NBR, ';
			lv_filter := lv_filter || ' AND (ASSET_NBR = $1.ASSET_NBR OR ASSET_NBR = $2.ASSET_NBR)';
		END IF;
		IF OLD.TERMINAL_NBR != NEW.TERMINAL_NBR THEN
			lv_sql := lv_sql || 'TERMINAL_NBR = $2.TERMINAL_NBR, ';
			lv_filter := lv_filter || ' AND (TERMINAL_NBR = $1.TERMINAL_NBR OR TERMINAL_NBR = $2.TERMINAL_NBR)';
		END IF;
		IF OLD.TERM_LABEL_1 IS DISTINCT FROM NEW.TERM_LABEL_1 THEN
			lv_sql := lv_sql || 'TERM_LABEL_1 = $2.TERM_LABEL_1, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_1 IS NOT DISTINCT FROM $1.TERM_LABEL_1 OR TERM_LABEL_1 IS NOT DISTINCT FROM $2.TERM_LABEL_1)';
		END IF;
		IF OLD.TERM_VAR_1 IS DISTINCT FROM NEW.TERM_VAR_1 THEN
			lv_sql := lv_sql || 'TERM_VAR_1 = $2.TERM_VAR_1, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_1 IS NOT DISTINCT FROM $1.TERM_VAR_1 OR TERM_VAR_1 IS NOT DISTINCT FROM $2.TERM_VAR_1)';
		END IF;
		IF OLD.TERM_LABEL_2 IS DISTINCT FROM NEW.TERM_LABEL_2 THEN
			lv_sql := lv_sql || 'TERM_LABEL_2 = $2.TERM_LABEL_2, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_2 IS NOT DISTINCT FROM $1.TERM_LABEL_2 OR TERM_LABEL_2 IS NOT DISTINCT FROM $2.TERM_LABEL_2)';
		END IF;
		IF OLD.TERM_VAR_2 IS DISTINCT FROM NEW.TERM_VAR_2 THEN
			lv_sql := lv_sql || 'TERM_VAR_2 = $2.TERM_VAR_2, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_2 IS NOT DISTINCT FROM $1.TERM_VAR_2 OR TERM_VAR_2 IS NOT DISTINCT FROM $2.TERM_VAR_2)';
		END IF;
		IF OLD.TERM_LABEL_3 IS DISTINCT FROM NEW.TERM_LABEL_3 THEN
			lv_sql := lv_sql || 'TERM_LABEL_3 = $2.TERM_LABEL_3, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_3 IS NOT DISTINCT FROM $1.TERM_LABEL_3 OR TERM_LABEL_3 IS NOT DISTINCT FROM $2.TERM_LABEL_3)';
		END IF;
		IF OLD.TERM_VAR_3 IS DISTINCT FROM NEW.TERM_VAR_3 THEN
			lv_sql := lv_sql || 'TERM_VAR_3 = $2.TERM_VAR_3, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_3 IS NOT DISTINCT FROM $1.TERM_VAR_3 OR TERM_VAR_3 IS NOT DISTINCT FROM $2.TERM_VAR_3)';
		END IF;
		IF OLD.TERM_LABEL_4 IS DISTINCT FROM NEW.TERM_LABEL_4 THEN
			lv_sql := lv_sql || 'TERM_LABEL_4 = $2.TERM_LABEL_4, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_4 IS NOT DISTINCT FROM $1.TERM_LABEL_4 OR TERM_LABEL_4 IS NOT DISTINCT FROM $2.TERM_LABEL_4)';
		END IF;
		IF OLD.TERM_VAR_4 IS DISTINCT FROM NEW.TERM_VAR_4 THEN
			lv_sql := lv_sql || 'TERM_VAR_4 = $2.TERM_VAR_4, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_4 IS NOT DISTINCT FROM $1.TERM_VAR_4 OR TERM_VAR_4 IS NOT DISTINCT FROM $2.TERM_VAR_4)';
		END IF;
		IF OLD.TERM_LABEL_5 IS DISTINCT FROM NEW.TERM_LABEL_5 THEN
			lv_sql := lv_sql || 'TERM_LABEL_5 = $2.TERM_LABEL_5, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_5 IS NOT DISTINCT FROM $1.TERM_LABEL_5 OR TERM_LABEL_5 IS NOT DISTINCT FROM $2.TERM_LABEL_5)';
		END IF;
		IF OLD.TERM_VAR_5 IS DISTINCT FROM NEW.TERM_VAR_5 THEN
			lv_sql := lv_sql || 'TERM_VAR_5 = $2.TERM_VAR_5, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_5 IS NOT DISTINCT FROM $1.TERM_VAR_5 OR TERM_VAR_5 IS NOT DISTINCT FROM $2.TERM_VAR_5)';
		END IF;
		IF OLD.TERM_LABEL_6 IS DISTINCT FROM NEW.TERM_LABEL_6 THEN
			lv_sql := lv_sql || 'TERM_LABEL_6 = $2.TERM_LABEL_6, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_6 IS NOT DISTINCT FROM $1.TERM_LABEL_6 OR TERM_LABEL_6 IS NOT DISTINCT FROM $2.TERM_LABEL_6)';
		END IF;
		IF OLD.TERM_VAR_6 IS DISTINCT FROM NEW.TERM_VAR_6 THEN
			lv_sql := lv_sql || 'TERM_VAR_6 = $2.TERM_VAR_6, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_6 IS NOT DISTINCT FROM $1.TERM_VAR_6 OR TERM_VAR_6 IS NOT DISTINCT FROM $2.TERM_VAR_6)';
		END IF;
		IF OLD.TERM_LABEL_7 IS DISTINCT FROM NEW.TERM_LABEL_7 THEN
			lv_sql := lv_sql || 'TERM_LABEL_7 = $2.TERM_LABEL_7, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_7 IS NOT DISTINCT FROM $1.TERM_LABEL_7 OR TERM_LABEL_7 IS NOT DISTINCT FROM $2.TERM_LABEL_7)';
		END IF;
		IF OLD.TERM_VAR_7 IS DISTINCT FROM NEW.TERM_VAR_7 THEN
			lv_sql := lv_sql || 'TERM_VAR_7 = $2.TERM_VAR_7, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_7 IS NOT DISTINCT FROM $1.TERM_VAR_7 OR TERM_VAR_7 IS NOT DISTINCT FROM $2.TERM_VAR_7)';
		END IF;
		IF OLD.TERM_LABEL_8 IS DISTINCT FROM NEW.TERM_LABEL_8 THEN
			lv_sql := lv_sql || 'TERM_LABEL_8 = $2.TERM_LABEL_8, ';
			lv_filter := lv_filter || ' AND (TERM_LABEL_8 IS NOT DISTINCT FROM $1.TERM_LABEL_8 OR TERM_LABEL_8 IS NOT DISTINCT FROM $2.TERM_LABEL_8)';
		END IF;
		IF OLD.TERM_VAR_8 IS DISTINCT FROM NEW.TERM_VAR_8 THEN
			lv_sql := lv_sql || 'TERM_VAR_8 = $2.TERM_VAR_8, ';
			lv_filter := lv_filter || ' AND (TERM_VAR_8 IS NOT DISTINCT FROM $1.TERM_VAR_8 OR TERM_VAR_8 IS NOT DISTINCT FROM $2.TERM_VAR_8)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE POS_DIM_ID = $1.POS_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'SOURCE_CUSTOMER_ID % 17 = ' || NEW.SOURCE_CUSTOMER_ID % 17||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(POS_DIM_ID) USING INDEX TABLESPACE rdw_data_t1) INHERITS(RDW._POS_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_POS_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE POS_DIM_ID = $1.POS_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.POS_DIM
	AS SELECT * FROM RDW._POS_DIM;

ALTER VIEW RDW.POS_DIM ALTER POS_DIM_ID SET DEFAULT nextval('RDW._pos_dim_pos_dim_id_seq'::regclass);
ALTER VIEW RDW.POS_DIM ALTER POS_START_TS SET DEFAULT TIMESTAMP'-infinity';
ALTER VIEW RDW.POS_DIM ALTER POS_END_TS SET DEFAULT TIMESTAMP'infinity';
ALTER VIEW RDW.POS_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.POS_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.POS_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.POS_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.POS_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._POS_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_POS_DIM ON RDW.POS_DIM;
CREATE TRIGGER TRIIUD_POS_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.POS_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_POS_DIM();


CREATE UNIQUE INDEX AK_POS_DIM ON RDW._POS_DIM(SOURCE_SYSTEM_CD, SOURCE_POS_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_SOURCE_CUSTOMER_ID ON RDW._POS_DIM(SOURCE_CUSTOMER_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_LOCATION_DIM_ID ON RDW._POS_DIM(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_DEVICE_DIM_ID ON RDW._POS_DIM(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_DEVICE_SERIAL_CD_POS_START_TS_POS_END_TS ON RDW._POS_DIM(DEVICE_SERIAL_CD, POS_START_TS, POS_END_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_DEVICE_SERIAL_CD ON RDW._POS_DIM(DEVICE_SERIAL_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_PRODUCT_TYPE ON RDW._POS_DIM(PRODUCT_TYPE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_POS_START_TS ON RDW._POS_DIM(POS_START_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_POS_END_TS ON RDW._POS_DIM(POS_END_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_ASSET_NBR ON RDW._POS_DIM(ASSET_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_TERMINAL_NBR ON RDW._POS_DIM(TERMINAL_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_POS_DIM_CREATED_TS ON RDW._POS_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.POS_DIM ----------------------------------

INSERT INTO RDW.POS_DIM(
	POS_DIM_ID,
	SOURCE_SYSTEM_CD,
	SOURCE_POS_ID,
	SOURCE_LOCATION_ID,
	SOURCE_CUSTOMER_ID,
	SOURCE_BUSINESS_UNIT_ID,
	SOURCE_PAYMENT_SCHEDULE_ID,
	LOCATION_DIM_ID,
	DEVICE_DIM_ID,
	DEVICE_SERIAL_CD,
	BUSINESS_UNIT_NAME,
	PAYMENT_SCHEDULE_DESC,
	PRODUCT_TYPE,
	PRODUCT_SUB_TYPE,
	REGISTERED_TS,
	SHIPPED_TS,
	POS_START_TS,
	POS_END_TS,
	ACTIVE_FLAG,
	ASSET_NBR,
	TERMINAL_NBR,
	TERM_LABEL_1,
	TERM_VAR_1,
	TERM_LABEL_2,
	TERM_VAR_2,
	TERM_LABEL_3,
	TERM_VAR_3,
	TERM_LABEL_4,
	TERM_VAR_4,
	TERM_LABEL_5,
	TERM_VAR_5,
	TERM_LABEL_6,
	TERM_VAR_6,
	TERM_LABEL_7,
	TERM_VAR_7,
	TERM_LABEL_8,
	TERM_VAR_8,
	REVISION)
VALUES(
	0,
	'Unknown',
	0,
	0,
	0,
	NULL,
	NULL,
	0,
	0,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	NULL,
	TIMESTAMP'-infinity',
	TIMESTAMP'infinity',
	'Unknown',
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_POS_DIM(
	p_pos_dim_id OUT BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_source_pos_id BIGINT, 
	p_source_location_id BIGINT, 
	p_source_customer_id BIGINT, 
	p_source_business_unit_id SMALLINT, 
	p_source_payment_schedule_id SMALLINT, 
	p_location_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_device_serial_cd VARCHAR(50), 
	p_business_unit_name VARCHAR(100), 
	p_payment_schedule_desc VARCHAR(100), 
	p_product_type VARCHAR(100), 
	p_product_sub_type VARCHAR(100), 
	p_registered_ts TIMESTAMPTZ, 
	p_shipped_ts TIMESTAMPTZ, 
	p_pos_start_ts TIMESTAMPTZ, 
	p_pos_end_ts TIMESTAMPTZ, 
	p_active_flag VARCHAR(10), 
	p_asset_nbr VARCHAR(50), 
	p_terminal_nbr VARCHAR(20), 
	p_term_label_1 VARCHAR(50), 
	p_term_var_1 VARCHAR(4000), 
	p_term_label_2 VARCHAR(50), 
	p_term_var_2 VARCHAR(4000), 
	p_term_label_3 VARCHAR(50), 
	p_term_var_3 VARCHAR(4000), 
	p_term_label_4 VARCHAR(50), 
	p_term_var_4 VARCHAR(4000), 
	p_term_label_5 VARCHAR(50), 
	p_term_var_5 VARCHAR(4000), 
	p_term_label_6 VARCHAR(50), 
	p_term_var_6 VARCHAR(4000), 
	p_term_label_7 VARCHAR(50), 
	p_term_var_7 VARCHAR(4000), 
	p_term_label_8 VARCHAR(50), 
	p_term_var_8 VARCHAR(4000), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT POS_DIM_ID, REVISION
			  INTO p_pos_dim_id, p_old_revision
			  FROM RDW.POS_DIM
			 WHERE SOURCE_SYSTEM_CD = p_source_system_cd
			   AND SOURCE_POS_ID = p_source_pos_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.POS_DIM
			   SET SOURCE_LOCATION_ID = p_source_location_id,
			       SOURCE_CUSTOMER_ID = p_source_customer_id,
			       SOURCE_BUSINESS_UNIT_ID = p_source_business_unit_id,
			       SOURCE_PAYMENT_SCHEDULE_ID = p_source_payment_schedule_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       DEVICE_SERIAL_CD = p_device_serial_cd,
			       BUSINESS_UNIT_NAME = p_business_unit_name,
			       PAYMENT_SCHEDULE_DESC = p_payment_schedule_desc,
			       PRODUCT_TYPE = p_product_type,
			       PRODUCT_SUB_TYPE = p_product_sub_type,
			       REGISTERED_TS = p_registered_ts,
			       SHIPPED_TS = p_shipped_ts,
			       POS_START_TS = COALESCE(p_pos_start_ts, POS_START_TS),
			       POS_END_TS = COALESCE(p_pos_end_ts, POS_END_TS),
			       ACTIVE_FLAG = p_active_flag,
			       ASSET_NBR = p_asset_nbr,
			       TERMINAL_NBR = p_terminal_nbr,
			       TERM_LABEL_1 = p_term_label_1,
			       TERM_VAR_1 = p_term_var_1,
			       TERM_LABEL_2 = p_term_label_2,
			       TERM_VAR_2 = p_term_var_2,
			       TERM_LABEL_3 = p_term_label_3,
			       TERM_VAR_3 = p_term_var_3,
			       TERM_LABEL_4 = p_term_label_4,
			       TERM_VAR_4 = p_term_var_4,
			       TERM_LABEL_5 = p_term_label_5,
			       TERM_VAR_5 = p_term_var_5,
			       TERM_LABEL_6 = p_term_label_6,
			       TERM_VAR_6 = p_term_var_6,
			       TERM_LABEL_7 = p_term_label_7,
			       TERM_VAR_7 = p_term_var_7,
			       TERM_LABEL_8 = p_term_label_8,
			       TERM_VAR_8 = p_term_var_8,
			       REVISION = p_revision
			 WHERE SOURCE_SYSTEM_CD = p_source_system_cd
			   AND SOURCE_POS_ID = p_source_pos_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.POS_DIM(SOURCE_SYSTEM_CD, SOURCE_POS_ID, SOURCE_LOCATION_ID, SOURCE_CUSTOMER_ID, SOURCE_BUSINESS_UNIT_ID, SOURCE_PAYMENT_SCHEDULE_ID, LOCATION_DIM_ID, DEVICE_DIM_ID, DEVICE_SERIAL_CD, BUSINESS_UNIT_NAME, PAYMENT_SCHEDULE_DESC, PRODUCT_TYPE, PRODUCT_SUB_TYPE, REGISTERED_TS, SHIPPED_TS, POS_START_TS, POS_END_TS, ACTIVE_FLAG, ASSET_NBR, TERMINAL_NBR, TERM_LABEL_1, TERM_VAR_1, TERM_LABEL_2, TERM_VAR_2, TERM_LABEL_3, TERM_VAR_3, TERM_LABEL_4, TERM_VAR_4, TERM_LABEL_5, TERM_VAR_5, TERM_LABEL_6, TERM_VAR_6, TERM_LABEL_7, TERM_VAR_7, TERM_LABEL_8, TERM_VAR_8, REVISION)
					 VALUES(p_source_system_cd, p_source_pos_id, p_source_location_id, p_source_customer_id, p_source_business_unit_id, p_source_payment_schedule_id, p_location_dim_id, p_device_dim_id, p_device_serial_cd, p_business_unit_name, p_payment_schedule_desc, p_product_type, p_product_sub_type, p_registered_ts, p_shipped_ts, COALESCE(p_pos_start_ts, TIMESTAMP'-infinity'), COALESCE(p_pos_end_ts, TIMESTAMP'infinity'), p_active_flag, p_asset_nbr, p_terminal_nbr, p_term_label_1, p_term_var_1, p_term_label_2, p_term_var_2, p_term_label_3, p_term_var_3, p_term_label_4, p_term_var_4, p_term_label_5, p_term_var_5, p_term_label_6, p_term_var_6, p_term_label_7, p_term_var_7, p_term_label_8, p_term_var_8, p_revision)
					 RETURNING POS_DIM_ID
					      INTO p_pos_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_POS_DIM(
	p_pos_dim_id OUT BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_source_pos_id BIGINT, 
	p_source_location_id BIGINT, 
	p_source_customer_id BIGINT, 
	p_source_business_unit_id SMALLINT, 
	p_source_payment_schedule_id SMALLINT, 
	p_location_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_device_serial_cd VARCHAR(50), 
	p_business_unit_name VARCHAR(100), 
	p_payment_schedule_desc VARCHAR(100), 
	p_product_type VARCHAR(100), 
	p_product_sub_type VARCHAR(100), 
	p_registered_ts TIMESTAMPTZ, 
	p_shipped_ts TIMESTAMPTZ, 
	p_pos_start_ts TIMESTAMPTZ, 
	p_pos_end_ts TIMESTAMPTZ, 
	p_active_flag VARCHAR(10), 
	p_asset_nbr VARCHAR(50), 
	p_terminal_nbr VARCHAR(20), 
	p_term_label_1 VARCHAR(50), 
	p_term_var_1 VARCHAR(4000), 
	p_term_label_2 VARCHAR(50), 
	p_term_var_2 VARCHAR(4000), 
	p_term_label_3 VARCHAR(50), 
	p_term_var_3 VARCHAR(4000), 
	p_term_label_4 VARCHAR(50), 
	p_term_var_4 VARCHAR(4000), 
	p_term_label_5 VARCHAR(50), 
	p_term_var_5 VARCHAR(4000), 
	p_term_label_6 VARCHAR(50), 
	p_term_var_6 VARCHAR(4000), 
	p_term_label_7 VARCHAR(50), 
	p_term_var_7 VARCHAR(4000), 
	p_term_label_8 VARCHAR(50), 
	p_term_var_8 VARCHAR(4000), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.ITEM_DETAIL_DIM
  (
    ITEM_DETAIL_DIM_ID BIGINT        NOT NULL DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass),
    ITEM_TYPE_DIM_ID   SMALLINT      NOT NULL,
    DEVICE_SERIAL_CD   VARCHAR(20)   NOT NULL,
    TERMINAL_ID        BIGINT        ,
    CUSTOMER_BANK_ID   BIGINT        ,
    ITEM_TS            TIMESTAMPTZ   ,
    SETTLED_TS         TIMESTAMPTZ   ,
    PROCESSED_TS       TIMESTAMPTZ   ,
    ITEM_DESC          VARCHAR(4000) ,
    EXPORT_BATCH_NUM   BIGINT        ,
    ITEM_PAYABLE_FLAG  VARCHAR(10)   ,
    PAID_TS            TIMESTAMPTZ   ,
    MACHINE_TRANS_NO   VARCHAR(100)  ,
    REF_NBR            VARCHAR(50)   ,
    DEVICE_TRAN_CD     VARCHAR(20)   ,

    CONSTRAINT PK_ITEM_DETAIL_DIM PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.ITEM_DETAIL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.ITEM_DETAIL_DIM TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_ITEM_DETAIL_DIM ON RDW.ITEM_DETAIL_DIM;


CREATE TRIGGER TRBI_ITEM_DETAIL_DIM
BEFORE INSERT ON RDW.ITEM_DETAIL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_ITEM_DETAIL_DIM ON RDW.ITEM_DETAIL_DIM;


CREATE TRIGGER TRBU_ITEM_DETAIL_DIM
BEFORE UPDATE ON RDW.ITEM_DETAIL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_ITEM_TS ON RDW.ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_DEVICE_TRAN_CD ON RDW.ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, DEVICE_TRAN_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_TERMINAL_ID_ITEM_TS ON RDW.ITEM_DETAIL_DIM(TERMINAL_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_PROCESSED_TS ON RDW.ITEM_DETAIL_DIM(PROCESSED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_EXPORT_BATCH_NUM ON RDW.ITEM_DETAIL_DIM(EXPORT_BATCH_NUM) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_ITEM_PAYABLE_FLAG ON RDW.ITEM_DETAIL_DIM(ITEM_PAYABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_PAID_TS ON RDW.ITEM_DETAIL_DIM(PAID_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_MACHINE_TRANS_NO ON RDW.ITEM_DETAIL_DIM(MACHINE_TRANS_NO) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_REF_NBR ON RDW.ITEM_DETAIL_DIM(REF_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_DETAIL_DIM_CREATED_TS ON RDW.ITEM_DETAIL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.ITEM_DETAIL_DIM ----------------------------------

INSERT INTO RDW.ITEM_DETAIL_DIM(
	ITEM_DETAIL_DIM_ID,
	ITEM_TYPE_DIM_ID,
	DEVICE_SERIAL_CD,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	ITEM_TS,
	SETTLED_TS,
	PROCESSED_TS,
	ITEM_DESC,
	EXPORT_BATCH_NUM,
	ITEM_PAYABLE_FLAG,
	PAID_TS,
	MACHINE_TRANS_NO,
	REF_NBR,
	DEVICE_TRAN_CD,
	REVISION)
VALUES(
	0,
	0,
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_ITEM_DETAIL_DIM(
	p_item_detail_dim_id BIGINT, 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT REVISION
			  INTO p_old_revision
			  FROM RDW.ITEM_DETAIL_DIM
			 WHERE ITEM_DETAIL_DIM_ID = p_item_detail_dim_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.ITEM_DETAIL_DIM
			   SET ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_SERIAL_CD = p_device_serial_cd,
			       TERMINAL_ID = p_terminal_id,
			       CUSTOMER_BANK_ID = p_customer_bank_id,
			       ITEM_TS = p_item_ts,
			       SETTLED_TS = p_settled_ts,
			       PROCESSED_TS = p_processed_ts,
			       ITEM_DESC = p_item_desc,
			       EXPORT_BATCH_NUM = p_export_batch_num,
			       ITEM_PAYABLE_FLAG = p_item_payable_flag,
			       PAID_TS = p_paid_ts,
			       MACHINE_TRANS_NO = p_machine_trans_no,
			       REF_NBR = p_ref_nbr,
			       DEVICE_TRAN_CD = p_device_tran_cd,
			       REVISION = p_revision
			 WHERE ITEM_DETAIL_DIM_ID = p_item_detail_dim_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID, ITEM_TYPE_DIM_ID, DEVICE_SERIAL_CD, TERMINAL_ID, CUSTOMER_BANK_ID, ITEM_TS, SETTLED_TS, PROCESSED_TS, ITEM_DESC, EXPORT_BATCH_NUM, ITEM_PAYABLE_FLAG, PAID_TS, MACHINE_TRANS_NO, REF_NBR, DEVICE_TRAN_CD, REVISION)
					 VALUES(COALESCE(p_item_detail_dim_id,  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass)), p_item_type_dim_id, p_device_serial_cd, p_terminal_id, p_customer_bank_id, p_item_ts, p_settled_ts, p_processed_ts, p_item_desc, p_export_batch_num, p_item_payable_flag, p_paid_ts, p_machine_trans_no, p_ref_nbr, p_device_tran_cd, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_ITEM_DETAIL_DIM(
	p_item_detail_dim_id BIGINT, 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._TRAN_ITEM_DETAIL_DIM
  (
    ITEM_DETAIL_DIM_ID     BIGINT        NOT NULL DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass),
    SOURCE_TRAN_ID         BIGINT        NOT NULL,
    REPORT_TRAN_ID         BIGINT        ,
    SOURCE_LEDGER_ID       BIGINT        ,
    SOURCE_SYSTEM_CD       VARCHAR(30)   ,
    TRAN_UPLOAD_TS         TIMESTAMPTZ   ,
    PROCESS_FEE_ID         BIGINT        ,
    DURATION_SECONDS       BIGINT        ,
    ROYALTY_FEE_FLAG       VARCHAR(10)   ,
    TOTAL_AMOUNT           DECIMAL(12,4) ,
    CONVENIENCE_FEE_TOTAL  DECIMAL(12,4) ,
    LOYALTY_DISCOUNT_TOTAL DECIMAL(12,4) ,
    PRODUCT_SERVICE_TOTAL  INTEGER       ,
    ISSUER_NAME            VARCHAR(255)  ,
    ISSUER_COMMENT         VARCHAR(4000) ,
    ISSUE_REASON           VARCHAR(100)  ,

    CONSTRAINT PK_TRAN_ITEM_DETAIL_DIM PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_TRAN_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)
  )
  INHERITS(RDW.ITEM_DETAIL_DIM)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_TRAN_ITEM_DETAIL_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((ARRAY['','','CREDIT_', 'CASH_', 'OTHER_'])[NEW.ITEM_TYPE_DIM_ID + 1] || COALESCE(TO_CHAR(NEW.ITEM_TS, 'YYYY_MM'), ''),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_TRAN_ITEM_DETAIL_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_TRAN_ITEM_DETAIL_DIM', COALESCE((ARRAY['','','CREDIT_', 'CASH_', 'OTHER_'])[OLD.ITEM_TYPE_DIM_ID + 1] || COALESCE(TO_CHAR(OLD.ITEM_TS, 'YYYY_MM'), ''), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ITEM_DETAIL_DIM_ID != NEW.ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_TRAN_ID != NEW.SOURCE_TRAN_ID THEN
			lv_sql := lv_sql || 'SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_TRAN_ID = $1.SOURCE_TRAN_ID OR SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID)';
		END IF;
		IF OLD.REPORT_TRAN_ID IS DISTINCT FROM NEW.REPORT_TRAN_ID THEN
			lv_sql := lv_sql || 'REPORT_TRAN_ID = $2.REPORT_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (REPORT_TRAN_ID IS NOT DISTINCT FROM $1.REPORT_TRAN_ID OR REPORT_TRAN_ID IS NOT DISTINCT FROM $2.REPORT_TRAN_ID)';
		END IF;
		IF OLD.SOURCE_LEDGER_ID IS DISTINCT FROM NEW.SOURCE_LEDGER_ID THEN
			lv_sql := lv_sql || 'SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_LEDGER_ID IS NOT DISTINCT FROM $1.SOURCE_LEDGER_ID OR SOURCE_LEDGER_ID IS NOT DISTINCT FROM $2.SOURCE_LEDGER_ID)';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD IS DISTINCT FROM NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD IS NOT DISTINCT FROM $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD IS NOT DISTINCT FROM $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.TRAN_UPLOAD_TS IS DISTINCT FROM NEW.TRAN_UPLOAD_TS THEN
			lv_sql := lv_sql || 'TRAN_UPLOAD_TS = $2.TRAN_UPLOAD_TS, ';
			lv_filter := lv_filter || ' AND (TRAN_UPLOAD_TS IS NOT DISTINCT FROM $1.TRAN_UPLOAD_TS OR TRAN_UPLOAD_TS IS NOT DISTINCT FROM $2.TRAN_UPLOAD_TS)';
		END IF;
		IF OLD.PROCESS_FEE_ID IS DISTINCT FROM NEW.PROCESS_FEE_ID THEN
			lv_sql := lv_sql || 'PROCESS_FEE_ID = $2.PROCESS_FEE_ID, ';
			lv_filter := lv_filter || ' AND (PROCESS_FEE_ID IS NOT DISTINCT FROM $1.PROCESS_FEE_ID OR PROCESS_FEE_ID IS NOT DISTINCT FROM $2.PROCESS_FEE_ID)';
		END IF;
		IF OLD.DURATION_SECONDS IS DISTINCT FROM NEW.DURATION_SECONDS THEN
			lv_sql := lv_sql || 'DURATION_SECONDS = $2.DURATION_SECONDS, ';
			lv_filter := lv_filter || ' AND (DURATION_SECONDS IS NOT DISTINCT FROM $1.DURATION_SECONDS OR DURATION_SECONDS IS NOT DISTINCT FROM $2.DURATION_SECONDS)';
		END IF;
		IF OLD.ROYALTY_FEE_FLAG IS DISTINCT FROM NEW.ROYALTY_FEE_FLAG THEN
			lv_sql := lv_sql || 'ROYALTY_FEE_FLAG = $2.ROYALTY_FEE_FLAG, ';
			lv_filter := lv_filter || ' AND (ROYALTY_FEE_FLAG IS NOT DISTINCT FROM $1.ROYALTY_FEE_FLAG OR ROYALTY_FEE_FLAG IS NOT DISTINCT FROM $2.ROYALTY_FEE_FLAG)';
		END IF;
		IF OLD.TOTAL_AMOUNT IS DISTINCT FROM NEW.TOTAL_AMOUNT THEN
			lv_sql := lv_sql || 'TOTAL_AMOUNT = $2.TOTAL_AMOUNT, ';
			lv_filter := lv_filter || ' AND (TOTAL_AMOUNT IS NOT DISTINCT FROM $1.TOTAL_AMOUNT OR TOTAL_AMOUNT IS NOT DISTINCT FROM $2.TOTAL_AMOUNT)';
		END IF;
		IF OLD.CONVENIENCE_FEE_TOTAL IS DISTINCT FROM NEW.CONVENIENCE_FEE_TOTAL THEN
			lv_sql := lv_sql || 'CONVENIENCE_FEE_TOTAL = $2.CONVENIENCE_FEE_TOTAL, ';
			lv_filter := lv_filter || ' AND (CONVENIENCE_FEE_TOTAL IS NOT DISTINCT FROM $1.CONVENIENCE_FEE_TOTAL OR CONVENIENCE_FEE_TOTAL IS NOT DISTINCT FROM $2.CONVENIENCE_FEE_TOTAL)';
		END IF;
		IF OLD.LOYALTY_DISCOUNT_TOTAL IS DISTINCT FROM NEW.LOYALTY_DISCOUNT_TOTAL THEN
			lv_sql := lv_sql || 'LOYALTY_DISCOUNT_TOTAL = $2.LOYALTY_DISCOUNT_TOTAL, ';
			lv_filter := lv_filter || ' AND (LOYALTY_DISCOUNT_TOTAL IS NOT DISTINCT FROM $1.LOYALTY_DISCOUNT_TOTAL OR LOYALTY_DISCOUNT_TOTAL IS NOT DISTINCT FROM $2.LOYALTY_DISCOUNT_TOTAL)';
		END IF;
		IF OLD.PRODUCT_SERVICE_TOTAL IS DISTINCT FROM NEW.PRODUCT_SERVICE_TOTAL THEN
			lv_sql := lv_sql || 'PRODUCT_SERVICE_TOTAL = $2.PRODUCT_SERVICE_TOTAL, ';
			lv_filter := lv_filter || ' AND (PRODUCT_SERVICE_TOTAL IS NOT DISTINCT FROM $1.PRODUCT_SERVICE_TOTAL OR PRODUCT_SERVICE_TOTAL IS NOT DISTINCT FROM $2.PRODUCT_SERVICE_TOTAL)';
		END IF;
		IF OLD.ISSUER_NAME IS DISTINCT FROM NEW.ISSUER_NAME THEN
			lv_sql := lv_sql || 'ISSUER_NAME = $2.ISSUER_NAME, ';
			lv_filter := lv_filter || ' AND (ISSUER_NAME IS NOT DISTINCT FROM $1.ISSUER_NAME OR ISSUER_NAME IS NOT DISTINCT FROM $2.ISSUER_NAME)';
		END IF;
		IF OLD.ISSUER_COMMENT IS DISTINCT FROM NEW.ISSUER_COMMENT THEN
			lv_sql := lv_sql || 'ISSUER_COMMENT = $2.ISSUER_COMMENT, ';
			lv_filter := lv_filter || ' AND (ISSUER_COMMENT IS NOT DISTINCT FROM $1.ISSUER_COMMENT OR ISSUER_COMMENT IS NOT DISTINCT FROM $2.ISSUER_COMMENT)';
		END IF;
		IF OLD.ISSUE_REASON IS DISTINCT FROM NEW.ISSUE_REASON THEN
			lv_sql := lv_sql || 'ISSUE_REASON = $2.ISSUE_REASON, ';
			lv_filter := lv_filter || ' AND (ISSUE_REASON IS NOT DISTINCT FROM $1.ISSUE_REASON OR ISSUE_REASON IS NOT DISTINCT FROM $2.ISSUE_REASON)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD != NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD = $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.TERMINAL_ID IS DISTINCT FROM NEW.TERMINAL_ID THEN
			lv_sql := lv_sql || 'TERMINAL_ID = $2.TERMINAL_ID, ';
			lv_filter := lv_filter || ' AND (TERMINAL_ID IS NOT DISTINCT FROM $1.TERMINAL_ID OR TERMINAL_ID IS NOT DISTINCT FROM $2.TERMINAL_ID)';
		END IF;
		IF OLD.CUSTOMER_BANK_ID IS DISTINCT FROM NEW.CUSTOMER_BANK_ID THEN
			lv_sql := lv_sql || 'CUSTOMER_BANK_ID = $2.CUSTOMER_BANK_ID, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_BANK_ID IS NOT DISTINCT FROM $1.CUSTOMER_BANK_ID OR CUSTOMER_BANK_ID IS NOT DISTINCT FROM $2.CUSTOMER_BANK_ID)';
		END IF;
		IF OLD.ITEM_TS IS DISTINCT FROM NEW.ITEM_TS THEN
			lv_sql := lv_sql || 'ITEM_TS = $2.ITEM_TS, ';
			lv_filter := lv_filter || ' AND (ITEM_TS IS NOT DISTINCT FROM $1.ITEM_TS OR ITEM_TS IS NOT DISTINCT FROM $2.ITEM_TS)';
		END IF;
		IF OLD.SETTLED_TS IS DISTINCT FROM NEW.SETTLED_TS THEN
			lv_sql := lv_sql || 'SETTLED_TS = $2.SETTLED_TS, ';
			lv_filter := lv_filter || ' AND (SETTLED_TS IS NOT DISTINCT FROM $1.SETTLED_TS OR SETTLED_TS IS NOT DISTINCT FROM $2.SETTLED_TS)';
		END IF;
		IF OLD.PROCESSED_TS IS DISTINCT FROM NEW.PROCESSED_TS THEN
			lv_sql := lv_sql || 'PROCESSED_TS = $2.PROCESSED_TS, ';
			lv_filter := lv_filter || ' AND (PROCESSED_TS IS NOT DISTINCT FROM $1.PROCESSED_TS OR PROCESSED_TS IS NOT DISTINCT FROM $2.PROCESSED_TS)';
		END IF;
		IF OLD.ITEM_DESC IS DISTINCT FROM NEW.ITEM_DESC THEN
			lv_sql := lv_sql || 'ITEM_DESC = $2.ITEM_DESC, ';
			lv_filter := lv_filter || ' AND (ITEM_DESC IS NOT DISTINCT FROM $1.ITEM_DESC OR ITEM_DESC IS NOT DISTINCT FROM $2.ITEM_DESC)';
		END IF;
		IF OLD.EXPORT_BATCH_NUM IS DISTINCT FROM NEW.EXPORT_BATCH_NUM THEN
			lv_sql := lv_sql || 'EXPORT_BATCH_NUM = $2.EXPORT_BATCH_NUM, ';
			lv_filter := lv_filter || ' AND (EXPORT_BATCH_NUM IS NOT DISTINCT FROM $1.EXPORT_BATCH_NUM OR EXPORT_BATCH_NUM IS NOT DISTINCT FROM $2.EXPORT_BATCH_NUM)';
		END IF;
		IF OLD.ITEM_PAYABLE_FLAG IS DISTINCT FROM NEW.ITEM_PAYABLE_FLAG THEN
			lv_sql := lv_sql || 'ITEM_PAYABLE_FLAG = $2.ITEM_PAYABLE_FLAG, ';
			lv_filter := lv_filter || ' AND (ITEM_PAYABLE_FLAG IS NOT DISTINCT FROM $1.ITEM_PAYABLE_FLAG OR ITEM_PAYABLE_FLAG IS NOT DISTINCT FROM $2.ITEM_PAYABLE_FLAG)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.MACHINE_TRANS_NO IS DISTINCT FROM NEW.MACHINE_TRANS_NO THEN
			lv_sql := lv_sql || 'MACHINE_TRANS_NO = $2.MACHINE_TRANS_NO, ';
			lv_filter := lv_filter || ' AND (MACHINE_TRANS_NO IS NOT DISTINCT FROM $1.MACHINE_TRANS_NO OR MACHINE_TRANS_NO IS NOT DISTINCT FROM $2.MACHINE_TRANS_NO)';
		END IF;
		IF OLD.REF_NBR IS DISTINCT FROM NEW.REF_NBR THEN
			lv_sql := lv_sql || 'REF_NBR = $2.REF_NBR, ';
			lv_filter := lv_filter || ' AND (REF_NBR IS NOT DISTINCT FROM $1.REF_NBR OR REF_NBR IS NOT DISTINCT FROM $2.REF_NBR)';
		END IF;
		IF OLD.DEVICE_TRAN_CD IS DISTINCT FROM NEW.DEVICE_TRAN_CD THEN
			lv_sql := lv_sql || 'DEVICE_TRAN_CD = $2.DEVICE_TRAN_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_TRAN_CD IS NOT DISTINCT FROM $1.DEVICE_TRAN_CD OR DEVICE_TRAN_CD IS NOT DISTINCT FROM $2.DEVICE_TRAN_CD)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.ITEM_TS IS NULL THEN 'ITEM_TS IS NULL' ELSE 'ITEM_TS >= TIMESTAMP'''||TO_CHAR(NEW.ITEM_TS, 'YYYY-MM-01')||''' AND ITEM_TS < TIMESTAMP'''||TO_CHAR(NEW.ITEM_TS + '1 month', 'YYYY-MM-01')||'''' END||'), CONSTRAINT CK_' || lv_new_partition_table || '_1 CHECK(' ||'ITEM_TYPE_DIM_ID=' ||NEW.ITEM_TYPE_DIM_ID||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)) INHERITS(RDW._TRAN_ITEM_DETAIL_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_TRAN_ITEM_DETAIL_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.TRAN_ITEM_DETAIL_DIM
	AS SELECT * FROM RDW._TRAN_ITEM_DETAIL_DIM;

ALTER VIEW RDW.TRAN_ITEM_DETAIL_DIM ALTER ITEM_DETAIL_DIM_ID SET DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass);
ALTER VIEW RDW.TRAN_ITEM_DETAIL_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.TRAN_ITEM_DETAIL_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.TRAN_ITEM_DETAIL_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.TRAN_ITEM_DETAIL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.TRAN_ITEM_DETAIL_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._TRAN_ITEM_DETAIL_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_TRAN_ITEM_DETAIL_DIM ON RDW.TRAN_ITEM_DETAIL_DIM;
CREATE TRIGGER TRIIUD_TRAN_ITEM_DETAIL_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.TRAN_ITEM_DETAIL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_TRAN_ITEM_DETAIL_DIM();


CREATE UNIQUE INDEX AK_TRAN_ITEM_DETAIL_DIM ON RDW._TRAN_ITEM_DETAIL_DIM(SOURCE_TRAN_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_REPORT_TRAN_ID ON RDW._TRAN_ITEM_DETAIL_DIM(REPORT_TRAN_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_SOURCE_LEDGER_ID ON RDW._TRAN_ITEM_DETAIL_DIM(SOURCE_LEDGER_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_TRAN_UPLOAD_TS ON RDW._TRAN_ITEM_DETAIL_DIM(TRAN_UPLOAD_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_PROCESS_FEE_ID ON RDW._TRAN_ITEM_DETAIL_DIM(PROCESS_FEE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_ROYALTY_FEE_FLAG ON RDW._TRAN_ITEM_DETAIL_DIM(ROYALTY_FEE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_ITEM_TS ON RDW._TRAN_ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_DEVICE_TRAN_CD ON RDW._TRAN_ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, DEVICE_TRAN_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_TERMINAL_ID_ITEM_TS ON RDW._TRAN_ITEM_DETAIL_DIM(TERMINAL_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_PROCESSED_TS ON RDW._TRAN_ITEM_DETAIL_DIM(PROCESSED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_EXPORT_BATCH_NUM ON RDW._TRAN_ITEM_DETAIL_DIM(EXPORT_BATCH_NUM) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_ITEM_PAYABLE_FLAG ON RDW._TRAN_ITEM_DETAIL_DIM(ITEM_PAYABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_PAID_TS ON RDW._TRAN_ITEM_DETAIL_DIM(PAID_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_MACHINE_TRANS_NO ON RDW._TRAN_ITEM_DETAIL_DIM(MACHINE_TRANS_NO) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_REF_NBR ON RDW._TRAN_ITEM_DETAIL_DIM(REF_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_DETAIL_DIM_CREATED_TS ON RDW._TRAN_ITEM_DETAIL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.TRAN_ITEM_DETAIL_DIM ----------------------------------

INSERT INTO RDW.TRAN_ITEM_DETAIL_DIM(
	ITEM_DETAIL_DIM_ID,
	SOURCE_TRAN_ID,
	REPORT_TRAN_ID,
	SOURCE_LEDGER_ID,
	SOURCE_SYSTEM_CD,
	TRAN_UPLOAD_TS,
	PROCESS_FEE_ID,
	DURATION_SECONDS,
	ROYALTY_FEE_FLAG,
	TOTAL_AMOUNT,
	CONVENIENCE_FEE_TOTAL,
	LOYALTY_DISCOUNT_TOTAL,
	PRODUCT_SERVICE_TOTAL,
	ISSUER_NAME,
	ISSUER_COMMENT,
	ISSUE_REASON,
	ITEM_TYPE_DIM_ID,
	DEVICE_SERIAL_CD,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	ITEM_TS,
	SETTLED_TS,
	PROCESSED_TS,
	ITEM_DESC,
	EXPORT_BATCH_NUM,
	ITEM_PAYABLE_FLAG,
	PAID_TS,
	MACHINE_TRANS_NO,
	REF_NBR,
	DEVICE_TRAN_CD,
	REVISION)
VALUES(
	0,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	0,
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_TRAN_ITEM_DETAIL_DIM(
	p_item_detail_dim_id OUT BIGINT, 
	p_source_tran_id BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_tran_upload_ts TIMESTAMPTZ, 
	p_process_fee_id BIGINT, 
	p_duration_seconds BIGINT, 
	p_royalty_fee_flag VARCHAR(10), 
	p_total_amount DECIMAL(12,4), 
	p_convenience_fee_total DECIMAL(12,4), 
	p_loyalty_discount_total DECIMAL(12,4), 
	p_product_service_total INTEGER, 
	p_issuer_name VARCHAR(255), 
	p_issuer_comment VARCHAR(4000), 
	p_issue_reason VARCHAR(100), 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_DETAIL_DIM_ID, REVISION
			  INTO p_item_detail_dim_id, p_old_revision
			  FROM RDW.TRAN_ITEM_DETAIL_DIM
			 WHERE SOURCE_TRAN_ID = p_source_tran_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.TRAN_ITEM_DETAIL_DIM
			   SET REPORT_TRAN_ID = p_report_tran_id,
			       SOURCE_LEDGER_ID = p_source_ledger_id,
			       SOURCE_SYSTEM_CD = p_source_system_cd,
			       TRAN_UPLOAD_TS = p_tran_upload_ts,
			       PROCESS_FEE_ID = p_process_fee_id,
			       DURATION_SECONDS = p_duration_seconds,
			       ROYALTY_FEE_FLAG = p_royalty_fee_flag,
			       TOTAL_AMOUNT = p_total_amount,
			       CONVENIENCE_FEE_TOTAL = p_convenience_fee_total,
			       LOYALTY_DISCOUNT_TOTAL = p_loyalty_discount_total,
			       PRODUCT_SERVICE_TOTAL = p_product_service_total,
			       ISSUER_NAME = p_issuer_name,
			       ISSUER_COMMENT = p_issuer_comment,
			       ISSUE_REASON = p_issue_reason,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_SERIAL_CD = p_device_serial_cd,
			       TERMINAL_ID = p_terminal_id,
			       CUSTOMER_BANK_ID = p_customer_bank_id,
			       ITEM_TS = p_item_ts,
			       SETTLED_TS = p_settled_ts,
			       PROCESSED_TS = p_processed_ts,
			       ITEM_DESC = p_item_desc,
			       EXPORT_BATCH_NUM = p_export_batch_num,
			       ITEM_PAYABLE_FLAG = p_item_payable_flag,
			       PAID_TS = p_paid_ts,
			       MACHINE_TRANS_NO = p_machine_trans_no,
			       REF_NBR = p_ref_nbr,
			       DEVICE_TRAN_CD = p_device_tran_cd,
			       REVISION = p_revision
			 WHERE SOURCE_TRAN_ID = p_source_tran_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.TRAN_ITEM_DETAIL_DIM(SOURCE_TRAN_ID, REPORT_TRAN_ID, SOURCE_LEDGER_ID, SOURCE_SYSTEM_CD, TRAN_UPLOAD_TS, PROCESS_FEE_ID, DURATION_SECONDS, ROYALTY_FEE_FLAG, TOTAL_AMOUNT, CONVENIENCE_FEE_TOTAL, LOYALTY_DISCOUNT_TOTAL, PRODUCT_SERVICE_TOTAL, ISSUER_NAME, ISSUER_COMMENT, ISSUE_REASON, ITEM_TYPE_DIM_ID, DEVICE_SERIAL_CD, TERMINAL_ID, CUSTOMER_BANK_ID, ITEM_TS, SETTLED_TS, PROCESSED_TS, ITEM_DESC, EXPORT_BATCH_NUM, ITEM_PAYABLE_FLAG, PAID_TS, MACHINE_TRANS_NO, REF_NBR, DEVICE_TRAN_CD, REVISION)
					 VALUES(p_source_tran_id, p_report_tran_id, p_source_ledger_id, p_source_system_cd, p_tran_upload_ts, p_process_fee_id, p_duration_seconds, p_royalty_fee_flag, p_total_amount, p_convenience_fee_total, p_loyalty_discount_total, p_product_service_total, p_issuer_name, p_issuer_comment, p_issue_reason, p_item_type_dim_id, p_device_serial_cd, p_terminal_id, p_customer_bank_id, p_item_ts, p_settled_ts, p_processed_ts, p_item_desc, p_export_batch_num, p_item_payable_flag, p_paid_ts, p_machine_trans_no, p_ref_nbr, p_device_tran_cd, p_revision)
					 RETURNING ITEM_DETAIL_DIM_ID
					      INTO p_item_detail_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_TRAN_ITEM_DETAIL_DIM(
	p_item_detail_dim_id OUT BIGINT, 
	p_source_tran_id BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_tran_upload_ts TIMESTAMPTZ, 
	p_process_fee_id BIGINT, 
	p_duration_seconds BIGINT, 
	p_royalty_fee_flag VARCHAR(10), 
	p_total_amount DECIMAL(12,4), 
	p_convenience_fee_total DECIMAL(12,4), 
	p_loyalty_discount_total DECIMAL(12,4), 
	p_product_service_total INTEGER, 
	p_issuer_name VARCHAR(255), 
	p_issuer_comment VARCHAR(4000), 
	p_issue_reason VARCHAR(100), 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._LEDGER_ITEM_DETAIL_DIM
  (
    ITEM_DETAIL_DIM_ID  BIGINT        NOT NULL DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass),
    REPORT_TRAN_ID      BIGINT        ,
    SOURCE_LEDGER_ID    BIGINT        NOT NULL,
    DELETED_TS          TIMESTAMPTZ   ,
    PROCESS_FEE_ID      BIGINT        ,
    SERVICE_FEE_ID      BIGINT        ,
    SERVICE_FEE_TYPE_ID BIGINT        ,
    FEE_NAME            VARCHAR(50)   ,
    FEE_PERCENT         DECIMAL(12,4) ,
    FEE_AMOUNT          DECIMAL(17,4) ,
    FEE_FREQUENCY_ID    SMALLINT      ,
    FEE_FREQUENCY       VARCHAR(30)   ,
    TRAN_TYPE_ID        SMALLINT      ,
    ORIG_REF_NBR        VARCHAR(50)   ,

    CONSTRAINT PK_LEDGER_ITEM_DETAIL_DIM PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_LEDGER_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)
  )
  INHERITS(RDW.ITEM_DETAIL_DIM)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_LEDGER_ITEM_DETAIL_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(CASE WHEN NEW.ITEM_TYPE_DIM_ID = 0 THEN '' WHEN NEW.PAID_TS IS NULL THEN 'PENDING' ELSE TO_CHAR(NEW.PAID_TS, 'YYYY_MM') END,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_LEDGER_ITEM_DETAIL_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_LEDGER_ITEM_DETAIL_DIM', COALESCE(CASE WHEN OLD.ITEM_TYPE_DIM_ID = 0 THEN '' WHEN OLD.PAID_TS IS NULL THEN 'PENDING' ELSE TO_CHAR(OLD.PAID_TS, 'YYYY_MM') END, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ITEM_DETAIL_DIM_ID != NEW.ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_LEDGER_ID != NEW.SOURCE_LEDGER_ID THEN
			lv_sql := lv_sql || 'SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_LEDGER_ID = $1.SOURCE_LEDGER_ID OR SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID)';
		END IF;
		IF OLD.REPORT_TRAN_ID IS DISTINCT FROM NEW.REPORT_TRAN_ID THEN
			lv_sql := lv_sql || 'REPORT_TRAN_ID = $2.REPORT_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (REPORT_TRAN_ID IS NOT DISTINCT FROM $1.REPORT_TRAN_ID OR REPORT_TRAN_ID IS NOT DISTINCT FROM $2.REPORT_TRAN_ID)';
		END IF;
		IF OLD.DELETED_TS IS DISTINCT FROM NEW.DELETED_TS THEN
			lv_sql := lv_sql || 'DELETED_TS = $2.DELETED_TS, ';
			lv_filter := lv_filter || ' AND (DELETED_TS IS NOT DISTINCT FROM $1.DELETED_TS OR DELETED_TS IS NOT DISTINCT FROM $2.DELETED_TS)';
		END IF;
		IF OLD.PROCESS_FEE_ID IS DISTINCT FROM NEW.PROCESS_FEE_ID THEN
			lv_sql := lv_sql || 'PROCESS_FEE_ID = $2.PROCESS_FEE_ID, ';
			lv_filter := lv_filter || ' AND (PROCESS_FEE_ID IS NOT DISTINCT FROM $1.PROCESS_FEE_ID OR PROCESS_FEE_ID IS NOT DISTINCT FROM $2.PROCESS_FEE_ID)';
		END IF;
		IF OLD.SERVICE_FEE_ID IS DISTINCT FROM NEW.SERVICE_FEE_ID THEN
			lv_sql := lv_sql || 'SERVICE_FEE_ID = $2.SERVICE_FEE_ID, ';
			lv_filter := lv_filter || ' AND (SERVICE_FEE_ID IS NOT DISTINCT FROM $1.SERVICE_FEE_ID OR SERVICE_FEE_ID IS NOT DISTINCT FROM $2.SERVICE_FEE_ID)';
		END IF;
		IF OLD.SERVICE_FEE_TYPE_ID IS DISTINCT FROM NEW.SERVICE_FEE_TYPE_ID THEN
			lv_sql := lv_sql || 'SERVICE_FEE_TYPE_ID = $2.SERVICE_FEE_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (SERVICE_FEE_TYPE_ID IS NOT DISTINCT FROM $1.SERVICE_FEE_TYPE_ID OR SERVICE_FEE_TYPE_ID IS NOT DISTINCT FROM $2.SERVICE_FEE_TYPE_ID)';
		END IF;
		IF OLD.FEE_NAME IS DISTINCT FROM NEW.FEE_NAME THEN
			lv_sql := lv_sql || 'FEE_NAME = $2.FEE_NAME, ';
			lv_filter := lv_filter || ' AND (FEE_NAME IS NOT DISTINCT FROM $1.FEE_NAME OR FEE_NAME IS NOT DISTINCT FROM $2.FEE_NAME)';
		END IF;
		IF OLD.FEE_PERCENT IS DISTINCT FROM NEW.FEE_PERCENT THEN
			lv_sql := lv_sql || 'FEE_PERCENT = $2.FEE_PERCENT, ';
			lv_filter := lv_filter || ' AND (FEE_PERCENT IS NOT DISTINCT FROM $1.FEE_PERCENT OR FEE_PERCENT IS NOT DISTINCT FROM $2.FEE_PERCENT)';
		END IF;
		IF OLD.FEE_AMOUNT IS DISTINCT FROM NEW.FEE_AMOUNT THEN
			lv_sql := lv_sql || 'FEE_AMOUNT = $2.FEE_AMOUNT, ';
			lv_filter := lv_filter || ' AND (FEE_AMOUNT IS NOT DISTINCT FROM $1.FEE_AMOUNT OR FEE_AMOUNT IS NOT DISTINCT FROM $2.FEE_AMOUNT)';
		END IF;
		IF OLD.FEE_FREQUENCY_ID IS DISTINCT FROM NEW.FEE_FREQUENCY_ID THEN
			lv_sql := lv_sql || 'FEE_FREQUENCY_ID = $2.FEE_FREQUENCY_ID, ';
			lv_filter := lv_filter || ' AND (FEE_FREQUENCY_ID IS NOT DISTINCT FROM $1.FEE_FREQUENCY_ID OR FEE_FREQUENCY_ID IS NOT DISTINCT FROM $2.FEE_FREQUENCY_ID)';
		END IF;
		IF OLD.FEE_FREQUENCY IS DISTINCT FROM NEW.FEE_FREQUENCY THEN
			lv_sql := lv_sql || 'FEE_FREQUENCY = $2.FEE_FREQUENCY, ';
			lv_filter := lv_filter || ' AND (FEE_FREQUENCY IS NOT DISTINCT FROM $1.FEE_FREQUENCY OR FEE_FREQUENCY IS NOT DISTINCT FROM $2.FEE_FREQUENCY)';
		END IF;
		IF OLD.TRAN_TYPE_ID IS DISTINCT FROM NEW.TRAN_TYPE_ID THEN
			lv_sql := lv_sql || 'TRAN_TYPE_ID = $2.TRAN_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (TRAN_TYPE_ID IS NOT DISTINCT FROM $1.TRAN_TYPE_ID OR TRAN_TYPE_ID IS NOT DISTINCT FROM $2.TRAN_TYPE_ID)';
		END IF;
		IF OLD.ORIG_REF_NBR IS DISTINCT FROM NEW.ORIG_REF_NBR THEN
			lv_sql := lv_sql || 'ORIG_REF_NBR = $2.ORIG_REF_NBR, ';
			lv_filter := lv_filter || ' AND (ORIG_REF_NBR IS NOT DISTINCT FROM $1.ORIG_REF_NBR OR ORIG_REF_NBR IS NOT DISTINCT FROM $2.ORIG_REF_NBR)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD != NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD = $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.TERMINAL_ID IS DISTINCT FROM NEW.TERMINAL_ID THEN
			lv_sql := lv_sql || 'TERMINAL_ID = $2.TERMINAL_ID, ';
			lv_filter := lv_filter || ' AND (TERMINAL_ID IS NOT DISTINCT FROM $1.TERMINAL_ID OR TERMINAL_ID IS NOT DISTINCT FROM $2.TERMINAL_ID)';
		END IF;
		IF OLD.CUSTOMER_BANK_ID IS DISTINCT FROM NEW.CUSTOMER_BANK_ID THEN
			lv_sql := lv_sql || 'CUSTOMER_BANK_ID = $2.CUSTOMER_BANK_ID, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_BANK_ID IS NOT DISTINCT FROM $1.CUSTOMER_BANK_ID OR CUSTOMER_BANK_ID IS NOT DISTINCT FROM $2.CUSTOMER_BANK_ID)';
		END IF;
		IF OLD.ITEM_TS IS DISTINCT FROM NEW.ITEM_TS THEN
			lv_sql := lv_sql || 'ITEM_TS = $2.ITEM_TS, ';
			lv_filter := lv_filter || ' AND (ITEM_TS IS NOT DISTINCT FROM $1.ITEM_TS OR ITEM_TS IS NOT DISTINCT FROM $2.ITEM_TS)';
		END IF;
		IF OLD.SETTLED_TS IS DISTINCT FROM NEW.SETTLED_TS THEN
			lv_sql := lv_sql || 'SETTLED_TS = $2.SETTLED_TS, ';
			lv_filter := lv_filter || ' AND (SETTLED_TS IS NOT DISTINCT FROM $1.SETTLED_TS OR SETTLED_TS IS NOT DISTINCT FROM $2.SETTLED_TS)';
		END IF;
		IF OLD.PROCESSED_TS IS DISTINCT FROM NEW.PROCESSED_TS THEN
			lv_sql := lv_sql || 'PROCESSED_TS = $2.PROCESSED_TS, ';
			lv_filter := lv_filter || ' AND (PROCESSED_TS IS NOT DISTINCT FROM $1.PROCESSED_TS OR PROCESSED_TS IS NOT DISTINCT FROM $2.PROCESSED_TS)';
		END IF;
		IF OLD.ITEM_DESC IS DISTINCT FROM NEW.ITEM_DESC THEN
			lv_sql := lv_sql || 'ITEM_DESC = $2.ITEM_DESC, ';
			lv_filter := lv_filter || ' AND (ITEM_DESC IS NOT DISTINCT FROM $1.ITEM_DESC OR ITEM_DESC IS NOT DISTINCT FROM $2.ITEM_DESC)';
		END IF;
		IF OLD.EXPORT_BATCH_NUM IS DISTINCT FROM NEW.EXPORT_BATCH_NUM THEN
			lv_sql := lv_sql || 'EXPORT_BATCH_NUM = $2.EXPORT_BATCH_NUM, ';
			lv_filter := lv_filter || ' AND (EXPORT_BATCH_NUM IS NOT DISTINCT FROM $1.EXPORT_BATCH_NUM OR EXPORT_BATCH_NUM IS NOT DISTINCT FROM $2.EXPORT_BATCH_NUM)';
		END IF;
		IF OLD.ITEM_PAYABLE_FLAG IS DISTINCT FROM NEW.ITEM_PAYABLE_FLAG THEN
			lv_sql := lv_sql || 'ITEM_PAYABLE_FLAG = $2.ITEM_PAYABLE_FLAG, ';
			lv_filter := lv_filter || ' AND (ITEM_PAYABLE_FLAG IS NOT DISTINCT FROM $1.ITEM_PAYABLE_FLAG OR ITEM_PAYABLE_FLAG IS NOT DISTINCT FROM $2.ITEM_PAYABLE_FLAG)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.MACHINE_TRANS_NO IS DISTINCT FROM NEW.MACHINE_TRANS_NO THEN
			lv_sql := lv_sql || 'MACHINE_TRANS_NO = $2.MACHINE_TRANS_NO, ';
			lv_filter := lv_filter || ' AND (MACHINE_TRANS_NO IS NOT DISTINCT FROM $1.MACHINE_TRANS_NO OR MACHINE_TRANS_NO IS NOT DISTINCT FROM $2.MACHINE_TRANS_NO)';
		END IF;
		IF OLD.REF_NBR IS DISTINCT FROM NEW.REF_NBR THEN
			lv_sql := lv_sql || 'REF_NBR = $2.REF_NBR, ';
			lv_filter := lv_filter || ' AND (REF_NBR IS NOT DISTINCT FROM $1.REF_NBR OR REF_NBR IS NOT DISTINCT FROM $2.REF_NBR)';
		END IF;
		IF OLD.DEVICE_TRAN_CD IS DISTINCT FROM NEW.DEVICE_TRAN_CD THEN
			lv_sql := lv_sql || 'DEVICE_TRAN_CD = $2.DEVICE_TRAN_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_TRAN_CD IS NOT DISTINCT FROM $1.DEVICE_TRAN_CD OR DEVICE_TRAN_CD IS NOT DISTINCT FROM $2.DEVICE_TRAN_CD)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.PAID_TS IS NULL THEN 'PAID_TS IS NULL' ELSE 'PAID_TS >= TIMESTAMP'''||TO_CHAR(NEW.PAID_TS, 'YYYY-MM-01')||''' AND PAID_TS < TIMESTAMP'''||TO_CHAR(NEW.PAID_TS + '1 month', 'YYYY-MM-01')||'''' END||'), CONSTRAINT CK_' || lv_new_partition_table || '_1 CHECK(' ||CASE WHEN NEW.ITEM_TYPE_DIM_ID = 0 THEN 'ITEM_TYPE_DIM_ID = 0' ELSE 'ITEM_TYPE_DIM_ID BETWEEN 5 AND 12' END||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)) INHERITS(RDW._LEDGER_ITEM_DETAIL_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_LEDGER_ITEM_DETAIL_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.LEDGER_ITEM_DETAIL_DIM
	AS SELECT * FROM RDW._LEDGER_ITEM_DETAIL_DIM;

ALTER VIEW RDW.LEDGER_ITEM_DETAIL_DIM ALTER ITEM_DETAIL_DIM_ID SET DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass);
ALTER VIEW RDW.LEDGER_ITEM_DETAIL_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.LEDGER_ITEM_DETAIL_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.LEDGER_ITEM_DETAIL_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.LEDGER_ITEM_DETAIL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.LEDGER_ITEM_DETAIL_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._LEDGER_ITEM_DETAIL_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_LEDGER_ITEM_DETAIL_DIM ON RDW.LEDGER_ITEM_DETAIL_DIM;
CREATE TRIGGER TRIIUD_LEDGER_ITEM_DETAIL_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.LEDGER_ITEM_DETAIL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_LEDGER_ITEM_DETAIL_DIM();


CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_REPORT_TRAN_ID ON RDW._LEDGER_ITEM_DETAIL_DIM(REPORT_TRAN_ID) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX AK_LEDGER_ITEM_DETAIL_DIM ON RDW._LEDGER_ITEM_DETAIL_DIM(SOURCE_LEDGER_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_DELETED_TS ON RDW._LEDGER_ITEM_DETAIL_DIM(DELETED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_PROCESS_FEE_ID ON RDW._LEDGER_ITEM_DETAIL_DIM(PROCESS_FEE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_SERVICE_FEE_ID ON RDW._LEDGER_ITEM_DETAIL_DIM(SERVICE_FEE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_SERVICE_FEE_TYPE_ID ON RDW._LEDGER_ITEM_DETAIL_DIM(SERVICE_FEE_TYPE_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_ORIG_REF_NBR ON RDW._LEDGER_ITEM_DETAIL_DIM(ORIG_REF_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_ITEM_TS ON RDW._LEDGER_ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_DEVICE_TRAN_CD ON RDW._LEDGER_ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, DEVICE_TRAN_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_TERMINAL_ID_ITEM_TS ON RDW._LEDGER_ITEM_DETAIL_DIM(TERMINAL_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_PROCESSED_TS ON RDW._LEDGER_ITEM_DETAIL_DIM(PROCESSED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_EXPORT_BATCH_NUM ON RDW._LEDGER_ITEM_DETAIL_DIM(EXPORT_BATCH_NUM) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_ITEM_PAYABLE_FLAG ON RDW._LEDGER_ITEM_DETAIL_DIM(ITEM_PAYABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_PAID_TS ON RDW._LEDGER_ITEM_DETAIL_DIM(PAID_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_MACHINE_TRANS_NO ON RDW._LEDGER_ITEM_DETAIL_DIM(MACHINE_TRANS_NO) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_REF_NBR ON RDW._LEDGER_ITEM_DETAIL_DIM(REF_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_DETAIL_DIM_CREATED_TS ON RDW._LEDGER_ITEM_DETAIL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.LEDGER_ITEM_DETAIL_DIM ----------------------------------

INSERT INTO RDW.LEDGER_ITEM_DETAIL_DIM(
	ITEM_DETAIL_DIM_ID,
	REPORT_TRAN_ID,
	SOURCE_LEDGER_ID,
	DELETED_TS,
	PROCESS_FEE_ID,
	SERVICE_FEE_ID,
	SERVICE_FEE_TYPE_ID,
	FEE_NAME,
	FEE_PERCENT,
	FEE_AMOUNT,
	FEE_FREQUENCY_ID,
	FEE_FREQUENCY,
	TRAN_TYPE_ID,
	ORIG_REF_NBR,
	ITEM_TYPE_DIM_ID,
	DEVICE_SERIAL_CD,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	ITEM_TS,
	SETTLED_TS,
	PROCESSED_TS,
	ITEM_DESC,
	EXPORT_BATCH_NUM,
	ITEM_PAYABLE_FLAG,
	PAID_TS,
	MACHINE_TRANS_NO,
	REF_NBR,
	DEVICE_TRAN_CD,
	REVISION)
VALUES(
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	0,
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_LEDGER_ITEM_DETAIL_DIM(
	p_item_detail_dim_id OUT BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_deleted_ts TIMESTAMPTZ, 
	p_process_fee_id BIGINT, 
	p_service_fee_id BIGINT, 
	p_service_fee_type_id BIGINT, 
	p_fee_name VARCHAR(50), 
	p_fee_percent DECIMAL(12,4), 
	p_fee_amount DECIMAL(17,4), 
	p_fee_frequency_id SMALLINT, 
	p_fee_frequency VARCHAR(30), 
	p_tran_type_id SMALLINT, 
	p_orig_ref_nbr VARCHAR(50), 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_DETAIL_DIM_ID, REVISION
			  INTO p_item_detail_dim_id, p_old_revision
			  FROM RDW.LEDGER_ITEM_DETAIL_DIM
			 WHERE SOURCE_LEDGER_ID = p_source_ledger_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.LEDGER_ITEM_DETAIL_DIM
			   SET REPORT_TRAN_ID = p_report_tran_id,
			       DELETED_TS = p_deleted_ts,
			       PROCESS_FEE_ID = p_process_fee_id,
			       SERVICE_FEE_ID = p_service_fee_id,
			       SERVICE_FEE_TYPE_ID = p_service_fee_type_id,
			       FEE_NAME = p_fee_name,
			       FEE_PERCENT = p_fee_percent,
			       FEE_AMOUNT = p_fee_amount,
			       FEE_FREQUENCY_ID = p_fee_frequency_id,
			       FEE_FREQUENCY = p_fee_frequency,
			       TRAN_TYPE_ID = p_tran_type_id,
			       ORIG_REF_NBR = p_orig_ref_nbr,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_SERIAL_CD = p_device_serial_cd,
			       TERMINAL_ID = p_terminal_id,
			       CUSTOMER_BANK_ID = p_customer_bank_id,
			       ITEM_TS = p_item_ts,
			       SETTLED_TS = p_settled_ts,
			       PROCESSED_TS = p_processed_ts,
			       ITEM_DESC = p_item_desc,
			       EXPORT_BATCH_NUM = p_export_batch_num,
			       ITEM_PAYABLE_FLAG = p_item_payable_flag,
			       PAID_TS = p_paid_ts,
			       MACHINE_TRANS_NO = p_machine_trans_no,
			       REF_NBR = p_ref_nbr,
			       DEVICE_TRAN_CD = p_device_tran_cd,
			       REVISION = p_revision
			 WHERE SOURCE_LEDGER_ID = p_source_ledger_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.LEDGER_ITEM_DETAIL_DIM(SOURCE_LEDGER_ID, REPORT_TRAN_ID, DELETED_TS, PROCESS_FEE_ID, SERVICE_FEE_ID, SERVICE_FEE_TYPE_ID, FEE_NAME, FEE_PERCENT, FEE_AMOUNT, FEE_FREQUENCY_ID, FEE_FREQUENCY, TRAN_TYPE_ID, ORIG_REF_NBR, ITEM_TYPE_DIM_ID, DEVICE_SERIAL_CD, TERMINAL_ID, CUSTOMER_BANK_ID, ITEM_TS, SETTLED_TS, PROCESSED_TS, ITEM_DESC, EXPORT_BATCH_NUM, ITEM_PAYABLE_FLAG, PAID_TS, MACHINE_TRANS_NO, REF_NBR, DEVICE_TRAN_CD, REVISION)
					 VALUES(p_source_ledger_id, p_report_tran_id, p_deleted_ts, p_process_fee_id, p_service_fee_id, p_service_fee_type_id, p_fee_name, p_fee_percent, p_fee_amount, p_fee_frequency_id, p_fee_frequency, p_tran_type_id, p_orig_ref_nbr, p_item_type_dim_id, p_device_serial_cd, p_terminal_id, p_customer_bank_id, p_item_ts, p_settled_ts, p_processed_ts, p_item_desc, p_export_batch_num, p_item_payable_flag, p_paid_ts, p_machine_trans_no, p_ref_nbr, p_device_tran_cd, p_revision)
					 RETURNING ITEM_DETAIL_DIM_ID
					      INTO p_item_detail_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_LEDGER_ITEM_DETAIL_DIM(
	p_item_detail_dim_id OUT BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_deleted_ts TIMESTAMPTZ, 
	p_process_fee_id BIGINT, 
	p_service_fee_id BIGINT, 
	p_service_fee_type_id BIGINT, 
	p_fee_name VARCHAR(50), 
	p_fee_percent DECIMAL(12,4), 
	p_fee_amount DECIMAL(17,4), 
	p_fee_frequency_id SMALLINT, 
	p_fee_frequency VARCHAR(30), 
	p_tran_type_id SMALLINT, 
	p_orig_ref_nbr VARCHAR(50), 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._FILL_ITEM_DETAIL_DIM
  (
    ITEM_DETAIL_DIM_ID      BIGINT      NOT NULL DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass),
    SOURCE_FILL_ID          BIGINT      NOT NULL,
    SOURCE_SYSTEM_CD        VARCHAR(30) NOT NULL,
    COUNTERS_RESET          VARCHAR(20) NOT NULL,
    COUNTERS_DISPLAYED_FLAG VARCHAR(10) NOT NULL,
    COUNTERS_REPORTED_FLAG  VARCHAR(10) NOT NULL,
    FILL_PERIOD_START_TS    TIMESTAMPTZ ,
    FILL_UPLOAD_TS          TIMESTAMPTZ ,
    IMPORT_COMPLETED_TS     TIMESTAMPTZ ,

    CONSTRAINT PK_FILL_ITEM_DETAIL_DIM PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_FILL_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)
  )
  INHERITS(RDW.ITEM_DETAIL_DIM)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_FILL_ITEM_DETAIL_DIM()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.ITEM_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_FILL_ITEM_DETAIL_DIM', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_FILL_ITEM_DETAIL_DIM', COALESCE(TO_CHAR(OLD.ITEM_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ITEM_DETAIL_DIM_ID != NEW.ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID, ';
		END IF;
		IF OLD.SOURCE_FILL_ID != NEW.SOURCE_FILL_ID THEN
			lv_sql := lv_sql || 'SOURCE_FILL_ID = $2.SOURCE_FILL_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_FILL_ID = $1.SOURCE_FILL_ID OR SOURCE_FILL_ID = $2.SOURCE_FILL_ID)';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD != NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD = $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.COUNTERS_RESET != NEW.COUNTERS_RESET THEN
			lv_sql := lv_sql || 'COUNTERS_RESET = $2.COUNTERS_RESET, ';
			lv_filter := lv_filter || ' AND (COUNTERS_RESET = $1.COUNTERS_RESET OR COUNTERS_RESET = $2.COUNTERS_RESET)';
		END IF;
		IF OLD.COUNTERS_DISPLAYED_FLAG != NEW.COUNTERS_DISPLAYED_FLAG THEN
			lv_sql := lv_sql || 'COUNTERS_DISPLAYED_FLAG = $2.COUNTERS_DISPLAYED_FLAG, ';
			lv_filter := lv_filter || ' AND (COUNTERS_DISPLAYED_FLAG = $1.COUNTERS_DISPLAYED_FLAG OR COUNTERS_DISPLAYED_FLAG = $2.COUNTERS_DISPLAYED_FLAG)';
		END IF;
		IF OLD.COUNTERS_REPORTED_FLAG != NEW.COUNTERS_REPORTED_FLAG THEN
			lv_sql := lv_sql || 'COUNTERS_REPORTED_FLAG = $2.COUNTERS_REPORTED_FLAG, ';
			lv_filter := lv_filter || ' AND (COUNTERS_REPORTED_FLAG = $1.COUNTERS_REPORTED_FLAG OR COUNTERS_REPORTED_FLAG = $2.COUNTERS_REPORTED_FLAG)';
		END IF;
		IF OLD.FILL_PERIOD_START_TS IS DISTINCT FROM NEW.FILL_PERIOD_START_TS THEN
			lv_sql := lv_sql || 'FILL_PERIOD_START_TS = $2.FILL_PERIOD_START_TS, ';
			lv_filter := lv_filter || ' AND (FILL_PERIOD_START_TS IS NOT DISTINCT FROM $1.FILL_PERIOD_START_TS OR FILL_PERIOD_START_TS IS NOT DISTINCT FROM $2.FILL_PERIOD_START_TS)';
		END IF;
		IF OLD.FILL_UPLOAD_TS IS DISTINCT FROM NEW.FILL_UPLOAD_TS THEN
			lv_sql := lv_sql || 'FILL_UPLOAD_TS = $2.FILL_UPLOAD_TS, ';
			lv_filter := lv_filter || ' AND (FILL_UPLOAD_TS IS NOT DISTINCT FROM $1.FILL_UPLOAD_TS OR FILL_UPLOAD_TS IS NOT DISTINCT FROM $2.FILL_UPLOAD_TS)';
		END IF;
		IF OLD.IMPORT_COMPLETED_TS IS DISTINCT FROM NEW.IMPORT_COMPLETED_TS THEN
			lv_sql := lv_sql || 'IMPORT_COMPLETED_TS = $2.IMPORT_COMPLETED_TS, ';
			lv_filter := lv_filter || ' AND (IMPORT_COMPLETED_TS IS NOT DISTINCT FROM $1.IMPORT_COMPLETED_TS OR IMPORT_COMPLETED_TS IS NOT DISTINCT FROM $2.IMPORT_COMPLETED_TS)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD != NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD = $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.TERMINAL_ID IS DISTINCT FROM NEW.TERMINAL_ID THEN
			lv_sql := lv_sql || 'TERMINAL_ID = $2.TERMINAL_ID, ';
			lv_filter := lv_filter || ' AND (TERMINAL_ID IS NOT DISTINCT FROM $1.TERMINAL_ID OR TERMINAL_ID IS NOT DISTINCT FROM $2.TERMINAL_ID)';
		END IF;
		IF OLD.CUSTOMER_BANK_ID IS DISTINCT FROM NEW.CUSTOMER_BANK_ID THEN
			lv_sql := lv_sql || 'CUSTOMER_BANK_ID = $2.CUSTOMER_BANK_ID, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_BANK_ID IS NOT DISTINCT FROM $1.CUSTOMER_BANK_ID OR CUSTOMER_BANK_ID IS NOT DISTINCT FROM $2.CUSTOMER_BANK_ID)';
		END IF;
		IF OLD.ITEM_TS IS DISTINCT FROM NEW.ITEM_TS THEN
			lv_sql := lv_sql || 'ITEM_TS = $2.ITEM_TS, ';
			lv_filter := lv_filter || ' AND (ITEM_TS IS NOT DISTINCT FROM $1.ITEM_TS OR ITEM_TS IS NOT DISTINCT FROM $2.ITEM_TS)';
		END IF;
		IF OLD.SETTLED_TS IS DISTINCT FROM NEW.SETTLED_TS THEN
			lv_sql := lv_sql || 'SETTLED_TS = $2.SETTLED_TS, ';
			lv_filter := lv_filter || ' AND (SETTLED_TS IS NOT DISTINCT FROM $1.SETTLED_TS OR SETTLED_TS IS NOT DISTINCT FROM $2.SETTLED_TS)';
		END IF;
		IF OLD.PROCESSED_TS IS DISTINCT FROM NEW.PROCESSED_TS THEN
			lv_sql := lv_sql || 'PROCESSED_TS = $2.PROCESSED_TS, ';
			lv_filter := lv_filter || ' AND (PROCESSED_TS IS NOT DISTINCT FROM $1.PROCESSED_TS OR PROCESSED_TS IS NOT DISTINCT FROM $2.PROCESSED_TS)';
		END IF;
		IF OLD.ITEM_DESC IS DISTINCT FROM NEW.ITEM_DESC THEN
			lv_sql := lv_sql || 'ITEM_DESC = $2.ITEM_DESC, ';
			lv_filter := lv_filter || ' AND (ITEM_DESC IS NOT DISTINCT FROM $1.ITEM_DESC OR ITEM_DESC IS NOT DISTINCT FROM $2.ITEM_DESC)';
		END IF;
		IF OLD.EXPORT_BATCH_NUM IS DISTINCT FROM NEW.EXPORT_BATCH_NUM THEN
			lv_sql := lv_sql || 'EXPORT_BATCH_NUM = $2.EXPORT_BATCH_NUM, ';
			lv_filter := lv_filter || ' AND (EXPORT_BATCH_NUM IS NOT DISTINCT FROM $1.EXPORT_BATCH_NUM OR EXPORT_BATCH_NUM IS NOT DISTINCT FROM $2.EXPORT_BATCH_NUM)';
		END IF;
		IF OLD.ITEM_PAYABLE_FLAG IS DISTINCT FROM NEW.ITEM_PAYABLE_FLAG THEN
			lv_sql := lv_sql || 'ITEM_PAYABLE_FLAG = $2.ITEM_PAYABLE_FLAG, ';
			lv_filter := lv_filter || ' AND (ITEM_PAYABLE_FLAG IS NOT DISTINCT FROM $1.ITEM_PAYABLE_FLAG OR ITEM_PAYABLE_FLAG IS NOT DISTINCT FROM $2.ITEM_PAYABLE_FLAG)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.MACHINE_TRANS_NO IS DISTINCT FROM NEW.MACHINE_TRANS_NO THEN
			lv_sql := lv_sql || 'MACHINE_TRANS_NO = $2.MACHINE_TRANS_NO, ';
			lv_filter := lv_filter || ' AND (MACHINE_TRANS_NO IS NOT DISTINCT FROM $1.MACHINE_TRANS_NO OR MACHINE_TRANS_NO IS NOT DISTINCT FROM $2.MACHINE_TRANS_NO)';
		END IF;
		IF OLD.REF_NBR IS DISTINCT FROM NEW.REF_NBR THEN
			lv_sql := lv_sql || 'REF_NBR = $2.REF_NBR, ';
			lv_filter := lv_filter || ' AND (REF_NBR IS NOT DISTINCT FROM $1.REF_NBR OR REF_NBR IS NOT DISTINCT FROM $2.REF_NBR)';
		END IF;
		IF OLD.DEVICE_TRAN_CD IS DISTINCT FROM NEW.DEVICE_TRAN_CD THEN
			lv_sql := lv_sql || 'DEVICE_TRAN_CD = $2.DEVICE_TRAN_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_TRAN_CD IS NOT DISTINCT FROM $1.DEVICE_TRAN_CD OR DEVICE_TRAN_CD IS NOT DISTINCT FROM $2.DEVICE_TRAN_CD)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.ITEM_TS IS NULL THEN 'ITEM_TS IS NULL' ELSE 'ITEM_TS >= TIMESTAMP'''||TO_CHAR(NEW.ITEM_TS, 'YYYY-MM-01')||''' AND ITEM_TS < TIMESTAMP'''||TO_CHAR(NEW.ITEM_TS + '1 month', 'YYYY-MM-01')||'''' END||'), CONSTRAINT CK_' || lv_new_partition_table || '_1 CHECK(' ||'ITEM_TYPE_DIM_ID =' || NEW.ITEM_TYPE_DIM_ID||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ITEM_DETAIL_DIM_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_ITEM_DETAIL_DIM_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID)) INHERITS(RDW._FILL_ITEM_DETAIL_DIM) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_FILL_ITEM_DETAIL_DIM', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.FILL_ITEM_DETAIL_DIM
	AS SELECT * FROM RDW._FILL_ITEM_DETAIL_DIM;

ALTER VIEW RDW.FILL_ITEM_DETAIL_DIM ALTER ITEM_DETAIL_DIM_ID SET DEFAULT  nextval('rdw.item_fact_item_detail_dim_id_seq'::regclass);
ALTER VIEW RDW.FILL_ITEM_DETAIL_DIM ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.FILL_ITEM_DETAIL_DIM ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.FILL_ITEM_DETAIL_DIM ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.FILL_ITEM_DETAIL_DIM TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.FILL_ITEM_DETAIL_DIM TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._FILL_ITEM_DETAIL_DIM FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_FILL_ITEM_DETAIL_DIM ON RDW.FILL_ITEM_DETAIL_DIM;
CREATE TRIGGER TRIIUD_FILL_ITEM_DETAIL_DIM
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.FILL_ITEM_DETAIL_DIM
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_FILL_ITEM_DETAIL_DIM();


CREATE UNIQUE INDEX AK_FILL_ITEM_DETAIL_DIM ON RDW._FILL_ITEM_DETAIL_DIM(SOURCE_FILL_ID, SOURCE_SYSTEM_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_FILL_PERIOD_START_TS ON RDW._FILL_ITEM_DETAIL_DIM(FILL_PERIOD_START_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_FILL_UPLOAD_TS ON RDW._FILL_ITEM_DETAIL_DIM(FILL_UPLOAD_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_IMPORT_COMPLETED_TS ON RDW._FILL_ITEM_DETAIL_DIM(IMPORT_COMPLETED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_ITEM_TS ON RDW._FILL_ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_DEVICE_SERIAL_CD_DEVICE_TRAN_CD ON RDW._FILL_ITEM_DETAIL_DIM(DEVICE_SERIAL_CD, DEVICE_TRAN_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_TERMINAL_ID_ITEM_TS ON RDW._FILL_ITEM_DETAIL_DIM(TERMINAL_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_PROCESSED_TS ON RDW._FILL_ITEM_DETAIL_DIM(PROCESSED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_EXPORT_BATCH_NUM ON RDW._FILL_ITEM_DETAIL_DIM(EXPORT_BATCH_NUM) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_ITEM_PAYABLE_FLAG ON RDW._FILL_ITEM_DETAIL_DIM(ITEM_PAYABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_PAID_TS ON RDW._FILL_ITEM_DETAIL_DIM(PAID_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_MACHINE_TRANS_NO ON RDW._FILL_ITEM_DETAIL_DIM(MACHINE_TRANS_NO) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_REF_NBR ON RDW._FILL_ITEM_DETAIL_DIM(REF_NBR) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_ITEM_DETAIL_DIM_CREATED_TS ON RDW._FILL_ITEM_DETAIL_DIM(CREATED_TS) TABLESPACE rdw_data_t1;

--------------------- DATA PRE-LOAD for RDW.FILL_ITEM_DETAIL_DIM ----------------------------------

INSERT INTO RDW.FILL_ITEM_DETAIL_DIM(
	ITEM_DETAIL_DIM_ID,
	SOURCE_FILL_ID,
	SOURCE_SYSTEM_CD,
	COUNTERS_RESET,
	COUNTERS_DISPLAYED_FLAG,
	COUNTERS_REPORTED_FLAG,
	FILL_PERIOD_START_TS,
	FILL_UPLOAD_TS,
	IMPORT_COMPLETED_TS,
	ITEM_TYPE_DIM_ID,
	DEVICE_SERIAL_CD,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	ITEM_TS,
	SETTLED_TS,
	PROCESSED_TS,
	ITEM_DESC,
	EXPORT_BATCH_NUM,
	ITEM_PAYABLE_FLAG,
	PAID_TS,
	MACHINE_TRANS_NO,
	REF_NBR,
	DEVICE_TRAN_CD,
	REVISION)
VALUES(
	0,
	0,
	'Unknown',
	'Unknown',
	'Unknown',
	'Unknown',
	NULL,
	NULL,
	NULL,
	0,
	'Unknown',
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	1);



CREATE OR REPLACE FUNCTION RDW.UPSERT_FILL_ITEM_DETAIL_DIM(
	p_item_detail_dim_id OUT BIGINT, 
	p_source_fill_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_counters_reset VARCHAR(20), 
	p_counters_displayed_flag VARCHAR(10), 
	p_counters_reported_flag VARCHAR(10), 
	p_fill_period_start_ts TIMESTAMPTZ, 
	p_fill_upload_ts TIMESTAMPTZ, 
	p_import_completed_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_DETAIL_DIM_ID, REVISION
			  INTO p_item_detail_dim_id, p_old_revision
			  FROM RDW.FILL_ITEM_DETAIL_DIM
			 WHERE SOURCE_FILL_ID = p_source_fill_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.FILL_ITEM_DETAIL_DIM
			   SET COUNTERS_RESET = p_counters_reset,
			       COUNTERS_DISPLAYED_FLAG = p_counters_displayed_flag,
			       COUNTERS_REPORTED_FLAG = p_counters_reported_flag,
			       FILL_PERIOD_START_TS = p_fill_period_start_ts,
			       FILL_UPLOAD_TS = p_fill_upload_ts,
			       IMPORT_COMPLETED_TS = p_import_completed_ts,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_SERIAL_CD = p_device_serial_cd,
			       TERMINAL_ID = p_terminal_id,
			       CUSTOMER_BANK_ID = p_customer_bank_id,
			       ITEM_TS = p_item_ts,
			       SETTLED_TS = p_settled_ts,
			       PROCESSED_TS = p_processed_ts,
			       ITEM_DESC = p_item_desc,
			       EXPORT_BATCH_NUM = p_export_batch_num,
			       ITEM_PAYABLE_FLAG = p_item_payable_flag,
			       PAID_TS = p_paid_ts,
			       MACHINE_TRANS_NO = p_machine_trans_no,
			       REF_NBR = p_ref_nbr,
			       DEVICE_TRAN_CD = p_device_tran_cd,
			       REVISION = p_revision
			 WHERE SOURCE_FILL_ID = p_source_fill_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.FILL_ITEM_DETAIL_DIM(SOURCE_FILL_ID, SOURCE_SYSTEM_CD, COUNTERS_RESET, COUNTERS_DISPLAYED_FLAG, COUNTERS_REPORTED_FLAG, FILL_PERIOD_START_TS, FILL_UPLOAD_TS, IMPORT_COMPLETED_TS, ITEM_TYPE_DIM_ID, DEVICE_SERIAL_CD, TERMINAL_ID, CUSTOMER_BANK_ID, ITEM_TS, SETTLED_TS, PROCESSED_TS, ITEM_DESC, EXPORT_BATCH_NUM, ITEM_PAYABLE_FLAG, PAID_TS, MACHINE_TRANS_NO, REF_NBR, DEVICE_TRAN_CD, REVISION)
					 VALUES(p_source_fill_id, p_source_system_cd, p_counters_reset, p_counters_displayed_flag, p_counters_reported_flag, p_fill_period_start_ts, p_fill_upload_ts, p_import_completed_ts, p_item_type_dim_id, p_device_serial_cd, p_terminal_id, p_customer_bank_id, p_item_ts, p_settled_ts, p_processed_ts, p_item_desc, p_export_batch_num, p_item_payable_flag, p_paid_ts, p_machine_trans_no, p_ref_nbr, p_device_tran_cd, p_revision)
					 RETURNING ITEM_DETAIL_DIM_ID
					      INTO p_item_detail_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_FILL_ITEM_DETAIL_DIM(
	p_item_detail_dim_id OUT BIGINT, 
	p_source_fill_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_counters_reset VARCHAR(20), 
	p_counters_displayed_flag VARCHAR(10), 
	p_counters_reported_flag VARCHAR(10), 
	p_fill_period_start_ts TIMESTAMPTZ, 
	p_fill_upload_ts TIMESTAMPTZ, 
	p_import_completed_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_serial_cd VARCHAR(20), 
	p_terminal_id BIGINT, 
	p_customer_bank_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_processed_ts TIMESTAMPTZ, 
	p_item_desc VARCHAR(4000), 
	p_export_batch_num BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_paid_ts TIMESTAMPTZ, 
	p_machine_trans_no VARCHAR(100), 
	p_ref_nbr VARCHAR(50), 
	p_device_tran_cd VARCHAR(20), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._DEVICE_CALL_FACT
  (
    DEVICE_CALL_FACT_ID      BIGSERIAL     NOT NULL,
    SOURCE_GLOBAL_SESSION_CD VARCHAR(100)  NOT NULL,
    DEVICE_CALL_DIM_ID       BIGINT        NOT NULL,
    DEVICE_DIM_ID            BIGINT        NOT NULL,
    LOCATION_DIM_ID          BIGINT        NOT NULL,
    POS_DIM_ID               BIGINT        NOT NULL,
    START_DATE_DIM_ID        SMALLINT      NOT NULL,
    START_TIME_DIM_ID        SMALLINT      NOT NULL,
    END_DATE_DIM_ID          SMALLINT      NOT NULL,
    END_TIME_DIM_ID          SMALLINT      NOT NULL,
    DEVICE_CALL_TYPE_DIM_ID  INTEGER       NOT NULL,
    CREDIT_TRANS_COUNT       INTEGER       ,
    CREDIT_TRANS_TOTAL       DECIMAL(12,4) ,
    CASH_TRANS_COUNT         INTEGER       ,
    CASH_TRANS_TOTAL         DECIMAL(12,4) ,
    OTHERCARD_TRANS_COUNT    INTEGER       ,
    OTHERCARD_TRANS_TOTAL    DECIMAL(12,4) ,
    EVENT_COUNT              INTEGER       ,
    DEX_FILE_SIZE            BIGINT        ,
    DURATION_MILLISECONDS    BIGINT        ,
    INBOUND_MESSAGE_COUNT    INTEGER       ,
    INBOUND_MESSAGE_BYTES    BIGINT        ,
    OUTBOUND_MESSAGE_COUNT   INTEGER       ,
    OUTBOUND_MESSAGE_BYTES   BIGINT        ,
    CALL_START_TS            TIMESTAMPTZ   ,
    CALL_END_TS              TIMESTAMPTZ   ,

    CONSTRAINT PK_DEVICE_CALL_FACT PRIMARY KEY(DEVICE_CALL_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_DEVICE_CALL_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_START_DATE_DIM_ID FOREIGN KEY(START_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_START_TIME_DIM_ID FOREIGN KEY(START_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_END_DATE_DIM_ID FOREIGN KEY(END_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_END_TIME_DIM_ID FOREIGN KEY(END_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_DEVICE_CALL_FACT_DEVICE_CALL_TYPE_DIM_ID FOREIGN KEY(DEVICE_CALL_TYPE_DIM_ID) REFERENCES RDW.DEVICE_CALL_TYPE_DIM(DEVICE_CALL_TYPE_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_DEVICE_CALL_FACT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(DATE'epoch' + NEW.START_DATE_DIM_ID, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_DEVICE_CALL_FACT', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_DEVICE_CALL_FACT', COALESCE(TO_CHAR(DATE'epoch' + OLD.START_DATE_DIM_ID, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.DEVICE_CALL_FACT_ID != NEW.DEVICE_CALL_FACT_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_FACT_ID = $2.DEVICE_CALL_FACT_ID, ';
		END IF;
		IF OLD.SOURCE_GLOBAL_SESSION_CD != NEW.SOURCE_GLOBAL_SESSION_CD THEN
			lv_sql := lv_sql || 'SOURCE_GLOBAL_SESSION_CD = $2.SOURCE_GLOBAL_SESSION_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_GLOBAL_SESSION_CD = $1.SOURCE_GLOBAL_SESSION_CD OR SOURCE_GLOBAL_SESSION_CD = $2.SOURCE_GLOBAL_SESSION_CD)';
		END IF;
		IF OLD.DEVICE_CALL_DIM_ID != NEW.DEVICE_CALL_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID OR DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
			lv_filter := lv_filter || ' AND (POS_DIM_ID = $1.POS_DIM_ID OR POS_DIM_ID = $2.POS_DIM_ID)';
		END IF;
		IF OLD.START_DATE_DIM_ID != NEW.START_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'START_DATE_DIM_ID = $2.START_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (START_DATE_DIM_ID = $1.START_DATE_DIM_ID OR START_DATE_DIM_ID = $2.START_DATE_DIM_ID)';
		END IF;
		IF OLD.START_TIME_DIM_ID != NEW.START_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'START_TIME_DIM_ID = $2.START_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (START_TIME_DIM_ID = $1.START_TIME_DIM_ID OR START_TIME_DIM_ID = $2.START_TIME_DIM_ID)';
		END IF;
		IF OLD.END_DATE_DIM_ID != NEW.END_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'END_DATE_DIM_ID = $2.END_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (END_DATE_DIM_ID = $1.END_DATE_DIM_ID OR END_DATE_DIM_ID = $2.END_DATE_DIM_ID)';
		END IF;
		IF OLD.END_TIME_DIM_ID != NEW.END_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'END_TIME_DIM_ID = $2.END_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (END_TIME_DIM_ID = $1.END_TIME_DIM_ID OR END_TIME_DIM_ID = $2.END_TIME_DIM_ID)';
		END IF;
		IF OLD.DEVICE_CALL_TYPE_DIM_ID != NEW.DEVICE_CALL_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_TYPE_DIM_ID = $2.DEVICE_CALL_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_CALL_TYPE_DIM_ID = $1.DEVICE_CALL_TYPE_DIM_ID OR DEVICE_CALL_TYPE_DIM_ID = $2.DEVICE_CALL_TYPE_DIM_ID)';
		END IF;
		IF OLD.CREDIT_TRANS_COUNT IS DISTINCT FROM NEW.CREDIT_TRANS_COUNT THEN
			lv_sql := lv_sql || 'CREDIT_TRANS_COUNT = $2.CREDIT_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_TRANS_COUNT IS NOT DISTINCT FROM $1.CREDIT_TRANS_COUNT OR CREDIT_TRANS_COUNT IS NOT DISTINCT FROM $2.CREDIT_TRANS_COUNT)';
		END IF;
		IF OLD.CREDIT_TRANS_TOTAL IS DISTINCT FROM NEW.CREDIT_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'CREDIT_TRANS_TOTAL = $2.CREDIT_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (CREDIT_TRANS_TOTAL IS NOT DISTINCT FROM $1.CREDIT_TRANS_TOTAL OR CREDIT_TRANS_TOTAL IS NOT DISTINCT FROM $2.CREDIT_TRANS_TOTAL)';
		END IF;
		IF OLD.CASH_TRANS_COUNT IS DISTINCT FROM NEW.CASH_TRANS_COUNT THEN
			lv_sql := lv_sql || 'CASH_TRANS_COUNT = $2.CASH_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (CASH_TRANS_COUNT IS NOT DISTINCT FROM $1.CASH_TRANS_COUNT OR CASH_TRANS_COUNT IS NOT DISTINCT FROM $2.CASH_TRANS_COUNT)';
		END IF;
		IF OLD.CASH_TRANS_TOTAL IS DISTINCT FROM NEW.CASH_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'CASH_TRANS_TOTAL = $2.CASH_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (CASH_TRANS_TOTAL IS NOT DISTINCT FROM $1.CASH_TRANS_TOTAL OR CASH_TRANS_TOTAL IS NOT DISTINCT FROM $2.CASH_TRANS_TOTAL)';
		END IF;
		IF OLD.OTHERCARD_TRANS_COUNT IS DISTINCT FROM NEW.OTHERCARD_TRANS_COUNT THEN
			lv_sql := lv_sql || 'OTHERCARD_TRANS_COUNT = $2.OTHERCARD_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (OTHERCARD_TRANS_COUNT IS NOT DISTINCT FROM $1.OTHERCARD_TRANS_COUNT OR OTHERCARD_TRANS_COUNT IS NOT DISTINCT FROM $2.OTHERCARD_TRANS_COUNT)';
		END IF;
		IF OLD.OTHERCARD_TRANS_TOTAL IS DISTINCT FROM NEW.OTHERCARD_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'OTHERCARD_TRANS_TOTAL = $2.OTHERCARD_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (OTHERCARD_TRANS_TOTAL IS NOT DISTINCT FROM $1.OTHERCARD_TRANS_TOTAL OR OTHERCARD_TRANS_TOTAL IS NOT DISTINCT FROM $2.OTHERCARD_TRANS_TOTAL)';
		END IF;
		IF OLD.EVENT_COUNT IS DISTINCT FROM NEW.EVENT_COUNT THEN
			lv_sql := lv_sql || 'EVENT_COUNT = $2.EVENT_COUNT, ';
			lv_filter := lv_filter || ' AND (EVENT_COUNT IS NOT DISTINCT FROM $1.EVENT_COUNT OR EVENT_COUNT IS NOT DISTINCT FROM $2.EVENT_COUNT)';
		END IF;
		IF OLD.DEX_FILE_SIZE IS DISTINCT FROM NEW.DEX_FILE_SIZE THEN
			lv_sql := lv_sql || 'DEX_FILE_SIZE = $2.DEX_FILE_SIZE, ';
			lv_filter := lv_filter || ' AND (DEX_FILE_SIZE IS NOT DISTINCT FROM $1.DEX_FILE_SIZE OR DEX_FILE_SIZE IS NOT DISTINCT FROM $2.DEX_FILE_SIZE)';
		END IF;
		IF OLD.DURATION_MILLISECONDS IS DISTINCT FROM NEW.DURATION_MILLISECONDS THEN
			lv_sql := lv_sql || 'DURATION_MILLISECONDS = $2.DURATION_MILLISECONDS, ';
			lv_filter := lv_filter || ' AND (DURATION_MILLISECONDS IS NOT DISTINCT FROM $1.DURATION_MILLISECONDS OR DURATION_MILLISECONDS IS NOT DISTINCT FROM $2.DURATION_MILLISECONDS)';
		END IF;
		IF OLD.INBOUND_MESSAGE_COUNT IS DISTINCT FROM NEW.INBOUND_MESSAGE_COUNT THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_COUNT = $2.INBOUND_MESSAGE_COUNT, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_COUNT OR INBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_COUNT)';
		END IF;
		IF OLD.INBOUND_MESSAGE_BYTES IS DISTINCT FROM NEW.INBOUND_MESSAGE_BYTES THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_BYTES = $2.INBOUND_MESSAGE_BYTES, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_BYTES IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_BYTES OR INBOUND_MESSAGE_BYTES IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_BYTES)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_COUNT IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_COUNT THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_COUNT = $2.OUTBOUND_MESSAGE_COUNT, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_COUNT OR OUTBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_COUNT)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_BYTES IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_BYTES THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_BYTES = $2.OUTBOUND_MESSAGE_BYTES, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_BYTES IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_BYTES OR OUTBOUND_MESSAGE_BYTES IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_BYTES)';
		END IF;
		IF OLD.CALL_START_TS IS DISTINCT FROM NEW.CALL_START_TS THEN
			lv_sql := lv_sql || 'CALL_START_TS = $2.CALL_START_TS, ';
			lv_filter := lv_filter || ' AND (CALL_START_TS IS NOT DISTINCT FROM $1.CALL_START_TS OR CALL_START_TS IS NOT DISTINCT FROM $2.CALL_START_TS)';
		END IF;
		IF OLD.CALL_END_TS IS DISTINCT FROM NEW.CALL_END_TS THEN
			lv_sql := lv_sql || 'CALL_END_TS = $2.CALL_END_TS, ';
			lv_filter := lv_filter || ' AND (CALL_END_TS IS NOT DISTINCT FROM $1.CALL_END_TS OR CALL_END_TS IS NOT DISTINCT FROM $2.CALL_END_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE DEVICE_CALL_FACT_ID = $1.DEVICE_CALL_FACT_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'START_DATE_DIM_ID >= ' || RDW.PARTITION_START_DATE_DIM_ID(NEW.START_DATE_DIM_ID, 'month') || ' AND START_DATE_DIM_ID < ' || RDW.PARTITION_END_DATE_DIM_ID(NEW.START_DATE_DIM_ID, 'month')||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(DEVICE_CALL_FACT_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_DEVICE_CALL_FACT_START_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(START_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_DEVICE_CALL_FACT_START_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(START_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_DEVICE_CALL_FACT_END_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(END_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_DEVICE_CALL_FACT_END_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(END_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_DEVICE_CALL_FACT_DEVICE_CALL_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(DEVICE_CALL_TYPE_DIM_ID) REFERENCES RDW.DEVICE_CALL_TYPE_DIM(DEVICE_CALL_TYPE_DIM_ID)) INHERITS(RDW._DEVICE_CALL_FACT) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_DEVICE_CALL_FACT', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE DEVICE_CALL_FACT_ID = $1.DEVICE_CALL_FACT_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.DEVICE_CALL_FACT
	AS SELECT * FROM RDW._DEVICE_CALL_FACT;

ALTER VIEW RDW.DEVICE_CALL_FACT ALTER DEVICE_CALL_FACT_ID SET DEFAULT nextval('RDW._device_call_fact_device_call_fact_id_seq'::regclass);
ALTER VIEW RDW.DEVICE_CALL_FACT ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.DEVICE_CALL_FACT ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.DEVICE_CALL_FACT ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.DEVICE_CALL_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.DEVICE_CALL_FACT TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._DEVICE_CALL_FACT FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_DEVICE_CALL_FACT ON RDW.DEVICE_CALL_FACT;
CREATE TRIGGER TRIIUD_DEVICE_CALL_FACT
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.DEVICE_CALL_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_DEVICE_CALL_FACT();


CREATE UNIQUE INDEX AK_DEVICE_CALL_FACT ON RDW._DEVICE_CALL_FACT(SOURCE_GLOBAL_SESSION_CD) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX UX_DEVICE_CALL_FACT_DEVICE_CALL_DIM_ID ON RDW._DEVICE_CALL_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_DEVICE_DIM_ID_CALL_START_TS ON RDW._DEVICE_CALL_FACT(DEVICE_DIM_ID, CALL_START_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_DEVICE_DIM_ID ON RDW._DEVICE_CALL_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_LOCATION_DIM_ID_CALL_START_TS ON RDW._DEVICE_CALL_FACT(LOCATION_DIM_ID, CALL_START_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_LOCATION_DIM_ID ON RDW._DEVICE_CALL_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_POS_DIM_ID ON RDW._DEVICE_CALL_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_START_DATE_DIM_ID ON RDW._DEVICE_CALL_FACT(START_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_START_TIME_DIM_ID ON RDW._DEVICE_CALL_FACT(START_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_END_DATE_DIM_ID ON RDW._DEVICE_CALL_FACT(END_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_END_TIME_DIM_ID ON RDW._DEVICE_CALL_FACT(END_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_DEVICE_CALL_TYPE_DIM_ID ON RDW._DEVICE_CALL_FACT(DEVICE_CALL_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_EVENT_COUNT ON RDW._DEVICE_CALL_FACT(EVENT_COUNT) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_DEX_FILE_SIZE ON RDW._DEVICE_CALL_FACT(DEX_FILE_SIZE) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_DURATION_MILLISECONDS ON RDW._DEVICE_CALL_FACT(DURATION_MILLISECONDS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_DEVICE_CALL_FACT_CREATED_TS ON RDW._DEVICE_CALL_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_DEVICE_CALL_FACT(
	p_device_call_fact_id OUT BIGINT, 
	p_source_global_session_cd VARCHAR(100), 
	p_device_call_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_start_date_dim_id SMALLINT, 
	p_start_time_dim_id SMALLINT, 
	p_end_date_dim_id SMALLINT, 
	p_end_time_dim_id SMALLINT, 
	p_device_call_type_dim_id INTEGER, 
	p_credit_trans_count INTEGER, 
	p_credit_trans_total DECIMAL(12,4), 
	p_cash_trans_count INTEGER, 
	p_cash_trans_total DECIMAL(12,4), 
	p_othercard_trans_count INTEGER, 
	p_othercard_trans_total DECIMAL(12,4), 
	p_event_count INTEGER, 
	p_dex_file_size BIGINT, 
	p_duration_milliseconds BIGINT, 
	p_inbound_message_count INTEGER, 
	p_inbound_message_bytes BIGINT, 
	p_outbound_message_count INTEGER, 
	p_outbound_message_bytes BIGINT, 
	p_call_start_ts TIMESTAMPTZ, 
	p_call_end_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT DEVICE_CALL_FACT_ID, REVISION
			  INTO p_device_call_fact_id, p_old_revision
			  FROM RDW.DEVICE_CALL_FACT
			 WHERE SOURCE_GLOBAL_SESSION_CD = p_source_global_session_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.DEVICE_CALL_FACT
			   SET DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       START_DATE_DIM_ID = p_start_date_dim_id,
			       START_TIME_DIM_ID = p_start_time_dim_id,
			       END_DATE_DIM_ID = p_end_date_dim_id,
			       END_TIME_DIM_ID = p_end_time_dim_id,
			       DEVICE_CALL_TYPE_DIM_ID = p_device_call_type_dim_id,
			       CREDIT_TRANS_COUNT = p_credit_trans_count,
			       CREDIT_TRANS_TOTAL = p_credit_trans_total,
			       CASH_TRANS_COUNT = p_cash_trans_count,
			       CASH_TRANS_TOTAL = p_cash_trans_total,
			       OTHERCARD_TRANS_COUNT = p_othercard_trans_count,
			       OTHERCARD_TRANS_TOTAL = p_othercard_trans_total,
			       EVENT_COUNT = p_event_count,
			       DEX_FILE_SIZE = p_dex_file_size,
			       DURATION_MILLISECONDS = p_duration_milliseconds,
			       INBOUND_MESSAGE_COUNT = p_inbound_message_count,
			       INBOUND_MESSAGE_BYTES = p_inbound_message_bytes,
			       OUTBOUND_MESSAGE_COUNT = p_outbound_message_count,
			       OUTBOUND_MESSAGE_BYTES = p_outbound_message_bytes,
			       CALL_START_TS = p_call_start_ts,
			       CALL_END_TS = p_call_end_ts,
			       REVISION = p_revision
			 WHERE SOURCE_GLOBAL_SESSION_CD = p_source_global_session_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.DEVICE_CALL_FACT(SOURCE_GLOBAL_SESSION_CD, DEVICE_CALL_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, START_DATE_DIM_ID, START_TIME_DIM_ID, END_DATE_DIM_ID, END_TIME_DIM_ID, DEVICE_CALL_TYPE_DIM_ID, CREDIT_TRANS_COUNT, CREDIT_TRANS_TOTAL, CASH_TRANS_COUNT, CASH_TRANS_TOTAL, OTHERCARD_TRANS_COUNT, OTHERCARD_TRANS_TOTAL, EVENT_COUNT, DEX_FILE_SIZE, DURATION_MILLISECONDS, INBOUND_MESSAGE_COUNT, INBOUND_MESSAGE_BYTES, OUTBOUND_MESSAGE_COUNT, OUTBOUND_MESSAGE_BYTES, CALL_START_TS, CALL_END_TS, REVISION)
					 VALUES(p_source_global_session_cd, p_device_call_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_start_date_dim_id, p_start_time_dim_id, p_end_date_dim_id, p_end_time_dim_id, p_device_call_type_dim_id, p_credit_trans_count, p_credit_trans_total, p_cash_trans_count, p_cash_trans_total, p_othercard_trans_count, p_othercard_trans_total, p_event_count, p_dex_file_size, p_duration_milliseconds, p_inbound_message_count, p_inbound_message_bytes, p_outbound_message_count, p_outbound_message_bytes, p_call_start_ts, p_call_end_ts, p_revision)
					 RETURNING DEVICE_CALL_FACT_ID
					      INTO p_device_call_fact_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_DEVICE_CALL_FACT(
	p_device_call_fact_id OUT BIGINT, 
	p_source_global_session_cd VARCHAR(100), 
	p_device_call_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_start_date_dim_id SMALLINT, 
	p_start_time_dim_id SMALLINT, 
	p_end_date_dim_id SMALLINT, 
	p_end_time_dim_id SMALLINT, 
	p_device_call_type_dim_id INTEGER, 
	p_credit_trans_count INTEGER, 
	p_credit_trans_total DECIMAL(12,4), 
	p_cash_trans_count INTEGER, 
	p_cash_trans_total DECIMAL(12,4), 
	p_othercard_trans_count INTEGER, 
	p_othercard_trans_total DECIMAL(12,4), 
	p_event_count INTEGER, 
	p_dex_file_size BIGINT, 
	p_duration_milliseconds BIGINT, 
	p_inbound_message_count INTEGER, 
	p_inbound_message_bytes BIGINT, 
	p_outbound_message_count INTEGER, 
	p_outbound_message_bytes BIGINT, 
	p_call_start_ts TIMESTAMPTZ, 
	p_call_end_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._PAID_ITEM_AGG
  (
    PAID_ITEM_AGG_ID     BIGINT        NOT NULL DEFAULT nextval('rdw.paid_item_agg_paid_item_agg_id_seq'::regclass),
    ITEM_PAYABLE_FLAG    VARCHAR(10)   NOT NULL,
    ITEM_TYPE_DIM_ID     SMALLINT      NOT NULL,
    DEVICE_DIM_ID        BIGINT        NOT NULL,
    LOCATION_DIM_ID      BIGINT        NOT NULL,
    POS_DIM_ID           BIGINT        NOT NULL,
    CURRENCY_DIM_ID      SMALLINT      NOT NULL,
    PAYMENT_DIM_ID       BIGINT        NOT NULL,
    PAYMENT_BATCH_DIM_ID BIGINT        NOT NULL,
    PAID_DATE_DIM_ID     SMALLINT      NOT NULL,
    QUANTITY             INTEGER       ,
    AMOUNT               DECIMAL(12,4) ,
    PAID_TS              TIMESTAMPTZ   ,

    CONSTRAINT PK_PAID_ITEM_AGG PRIMARY KEY(PAID_ITEM_AGG_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_PAID_ITEM_AGG_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_PAYMENT_DIM_ID FOREIGN KEY(PAYMENT_DIM_ID) REFERENCES RDW._PAYMENT_DIM(PAYMENT_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_PAYMENT_BATCH_DIM_ID FOREIGN KEY(PAYMENT_BATCH_DIM_ID) REFERENCES RDW._PAYMENT_BATCH_DIM(PAYMENT_BATCH_DIM_ID),
    CONSTRAINT FK_PAID_ITEM_AGG_PAID_DATE_DIM_ID FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_PAID_ITEM_AGG()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(CASE WHEN NEW.PAID_DATE_DIM_ID = 0 THEN 'PENDING' ELSE TO_CHAR(DATE'epoch' + NEW.PAID_DATE_DIM_ID, 'YYYY_MM') END,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_PAID_ITEM_AGG', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_PAID_ITEM_AGG', COALESCE(CASE WHEN OLD.PAID_DATE_DIM_ID = 0 THEN 'PENDING' ELSE TO_CHAR(DATE'epoch' + OLD.PAID_DATE_DIM_ID, 'YYYY_MM') END, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.PAID_ITEM_AGG_ID != NEW.PAID_ITEM_AGG_ID THEN
			lv_sql := lv_sql || 'PAID_ITEM_AGG_ID = $2.PAID_ITEM_AGG_ID, ';
		END IF;
		IF OLD.ITEM_PAYABLE_FLAG != NEW.ITEM_PAYABLE_FLAG THEN
			lv_sql := lv_sql || 'ITEM_PAYABLE_FLAG = $2.ITEM_PAYABLE_FLAG, ';
			lv_filter := lv_filter || ' AND (ITEM_PAYABLE_FLAG = $1.ITEM_PAYABLE_FLAG OR ITEM_PAYABLE_FLAG = $2.ITEM_PAYABLE_FLAG)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.CURRENCY_DIM_ID != NEW.CURRENCY_DIM_ID THEN
			lv_sql := lv_sql || 'CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CURRENCY_DIM_ID = $1.CURRENCY_DIM_ID OR CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_BATCH_DIM_ID != NEW.PAYMENT_BATCH_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_BATCH_DIM_ID = $1.PAYMENT_BATCH_DIM_ID OR PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
			lv_filter := lv_filter || ' AND (POS_DIM_ID = $1.POS_DIM_ID OR POS_DIM_ID = $2.POS_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_DIM_ID != NEW.PAYMENT_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_DIM_ID = $1.PAYMENT_DIM_ID OR PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID)';
		END IF;
		IF OLD.PAID_DATE_DIM_ID != NEW.PAID_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'PAID_DATE_DIM_ID = $2.PAID_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAID_DATE_DIM_ID = $1.PAID_DATE_DIM_ID OR PAID_DATE_DIM_ID = $2.PAID_DATE_DIM_ID)';
		END IF;
		IF OLD.QUANTITY IS DISTINCT FROM NEW.QUANTITY THEN
			lv_sql := lv_sql || 'QUANTITY = $2.QUANTITY, ';
			lv_filter := lv_filter || ' AND (QUANTITY IS NOT DISTINCT FROM $1.QUANTITY OR QUANTITY IS NOT DISTINCT FROM $2.QUANTITY)';
		END IF;
		IF OLD.AMOUNT IS DISTINCT FROM NEW.AMOUNT THEN
			lv_sql := lv_sql || 'AMOUNT = $2.AMOUNT, ';
			lv_filter := lv_filter || ' AND (AMOUNT IS NOT DISTINCT FROM $1.AMOUNT OR AMOUNT IS NOT DISTINCT FROM $2.AMOUNT)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE PAID_ITEM_AGG_ID = $1.PAID_ITEM_AGG_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.PAID_DATE_DIM_ID = 0 THEN 'PAID_DATE_DIM_ID = 0' ELSE 'PAID_DATE_DIM_ID >= ' || RDW.PARTITION_START_DATE_DIM_ID(NEW.PAID_DATE_DIM_ID, 'month') || ' AND PAID_DATE_DIM_ID < ' || RDW.PARTITION_END_DATE_DIM_ID(NEW.PAID_DATE_DIM_ID, 'month') END||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(PAID_ITEM_AGG_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_PAID_ITEM_AGG_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_PAID_ITEM_AGG_CURRENCY_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_PAID_ITEM_AGG_PAID_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID)) INHERITS(RDW._PAID_ITEM_AGG) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_PAID_ITEM_AGG', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE PAID_ITEM_AGG_ID = $1.PAID_ITEM_AGG_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.PAID_ITEM_AGG
	AS SELECT * FROM RDW._PAID_ITEM_AGG;

ALTER VIEW RDW.PAID_ITEM_AGG ALTER PAID_ITEM_AGG_ID SET DEFAULT nextval('rdw.paid_item_agg_paid_item_agg_id_seq'::regclass);
ALTER VIEW RDW.PAID_ITEM_AGG ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.PAID_ITEM_AGG ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.PAID_ITEM_AGG ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.PAID_ITEM_AGG TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.PAID_ITEM_AGG TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._PAID_ITEM_AGG FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_PAID_ITEM_AGG ON RDW.PAID_ITEM_AGG;
CREATE TRIGGER TRIIUD_PAID_ITEM_AGG
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.PAID_ITEM_AGG
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_PAID_ITEM_AGG();


CREATE UNIQUE INDEX AK_PAID_ITEM_AGG ON RDW._PAID_ITEM_AGG(ITEM_PAYABLE_FLAG, ITEM_TYPE_DIM_ID, CURRENCY_DIM_ID, PAYMENT_BATCH_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_ITEM_PAYABLE_FLAG ON RDW._PAID_ITEM_AGG(ITEM_PAYABLE_FLAG) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_DEVICE_DIM_ID ON RDW._PAID_ITEM_AGG(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_LOCATION_DIM_ID ON RDW._PAID_ITEM_AGG(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_POS_DIM_ID ON RDW._PAID_ITEM_AGG(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_PAYMENT_DIM_ID ON RDW._PAID_ITEM_AGG(PAYMENT_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_PAYMENT_BATCH_DIM_ID ON RDW._PAID_ITEM_AGG(PAYMENT_BATCH_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_PAID_DATE_DIM_ID ON RDW._PAID_ITEM_AGG(PAID_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAID_ITEM_AGG_CREATED_TS ON RDW._PAID_ITEM_AGG(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_PAID_ITEM_AGG(
	p_paid_item_agg_id OUT BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity INTEGER, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT PAID_ITEM_AGG_ID, REVISION
			  INTO p_paid_item_agg_id, p_old_revision
			  FROM RDW.PAID_ITEM_AGG
			 WHERE ITEM_PAYABLE_FLAG = p_item_payable_flag
			   AND ITEM_TYPE_DIM_ID = p_item_type_dim_id
			   AND CURRENCY_DIM_ID = p_currency_dim_id
			   AND PAYMENT_BATCH_DIM_ID = p_payment_batch_dim_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.PAID_ITEM_AGG
			   SET DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       PAYMENT_DIM_ID = p_payment_dim_id,
			       PAID_DATE_DIM_ID = p_paid_date_dim_id,
			       QUANTITY = p_quantity,
			       AMOUNT = p_amount,
			       PAID_TS = p_paid_ts,
			       REVISION = p_revision
			 WHERE ITEM_PAYABLE_FLAG = p_item_payable_flag
			   AND ITEM_TYPE_DIM_ID = p_item_type_dim_id
			   AND CURRENCY_DIM_ID = p_currency_dim_id
			   AND PAYMENT_BATCH_DIM_ID = p_payment_batch_dim_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.PAID_ITEM_AGG(ITEM_PAYABLE_FLAG, ITEM_TYPE_DIM_ID, CURRENCY_DIM_ID, PAYMENT_BATCH_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, PAYMENT_DIM_ID, PAID_DATE_DIM_ID, QUANTITY, AMOUNT, PAID_TS, REVISION)
					 VALUES(p_item_payable_flag, p_item_type_dim_id, p_currency_dim_id, p_payment_batch_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_payment_dim_id, p_paid_date_dim_id, p_quantity, p_amount, p_paid_ts, p_revision)
					 RETURNING PAID_ITEM_AGG_ID
					      INTO p_paid_item_agg_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_PAID_ITEM_AGG(
	p_paid_item_agg_id OUT BIGINT, 
	p_item_payable_flag VARCHAR(10), 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity INTEGER, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._AUTH_FACT
  (
    AUTH_FACT_ID               BIGSERIAL     NOT NULL,
    SOURCE_AUTH_ID             BIGINT        NOT NULL,
    SOURCE_SYSTEM_CD           VARCHAR(10)   NOT NULL,
    SOURCE_TRAN_ID             BIGINT        NOT NULL,
    AUTH_DETAIL_DIM_ID         BIGINT        NOT NULL,
    CONSUMER_DIM_ID            BIGINT        NOT NULL,
    AUTH_TYPE_DIM_ID           INTEGER       NOT NULL,
    AUTHORITY_TERMINAL_DIM_ID  BIGINT        NOT NULL,
    AUTH_DATE_DIM_ID           SMALLINT      NOT NULL,
    AUTH_TIME_DIM_ID           SMALLINT      NOT NULL,
    SERVER_DATE_DIM_ID         SMALLINT      NOT NULL,
    SERVER_TIME_DIM_ID         SMALLINT      NOT NULL,
    DEVICE_DIM_ID              BIGINT        NOT NULL,
    LOCATION_DIM_ID            BIGINT        NOT NULL,
    POS_DIM_ID                 BIGINT        NOT NULL,
    TRAN_ITEM_DETAIL_DIM_ID    BIGINT        NOT NULL,
    FILL_ITEM_DETAIL_DIM_ID    BIGINT        NOT NULL,
    SETTLED_SERVER_DATE_DIM_ID SMALLINT      NOT NULL,
    SETTLED_SERVER_TIME_DIM_ID SMALLINT      NOT NULL,
    CURRENCY_DIM_ID            SMALLINT      NOT NULL,
    DEVICE_CALL_DIM_ID         BIGINT        NOT NULL,
    AMOUNT                     DECIMAL(12,4) NOT NULL,
    APPROVED_AMOUNT            DECIMAL(12,4) ,
    PROCESSOR_MILLISECONDS     BIGINT        ,
    NETWORK_MILLISECONDS       BIGINT        ,
    AUTH_TS                    TIMESTAMPTZ   ,
    SETTLED_TS                 TIMESTAMPTZ   ,

    CONSTRAINT PK_AUTH_FACT PRIMARY KEY(AUTH_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_AUTH_FACT_AUTH_DETAIL_DIM_ID FOREIGN KEY(AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_CONSUMER_DIM_ID FOREIGN KEY(CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_AUTH_TYPE_DIM_ID FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_AUTHORITY_TERMINAL_DIM_ID FOREIGN KEY(AUTHORITY_TERMINAL_DIM_ID) REFERENCES RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TERMINAL_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_AUTH_DATE_DIM_ID FOREIGN KEY(AUTH_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_AUTH_TIME_DIM_ID FOREIGN KEY(AUTH_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_SERVER_DATE_DIM_ID FOREIGN KEY(SERVER_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_SERVER_TIME_DIM_ID FOREIGN KEY(SERVER_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_TRAN_ITEM_DETAIL_DIM_ID FOREIGN KEY(TRAN_ITEM_DETAIL_DIM_ID) REFERENCES RDW._TRAN_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_FILL_ITEM_DETAIL_DIM_ID FOREIGN KEY(FILL_ITEM_DETAIL_DIM_ID) REFERENCES RDW._FILL_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_SETTLED_SERVER_DATE_DIM_ID FOREIGN KEY(SETTLED_SERVER_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_SETTLED_SERVER_TIME_DIM_ID FOREIGN KEY(SETTLED_SERVER_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_AUTH_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_AUTH_FACT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(DATE'epoch' + NEW.AUTH_DATE_DIM_ID, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_AUTH_FACT', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_AUTH_FACT', COALESCE(TO_CHAR(DATE'epoch' + OLD.AUTH_DATE_DIM_ID, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.AUTH_FACT_ID != NEW.AUTH_FACT_ID THEN
			lv_sql := lv_sql || 'AUTH_FACT_ID = $2.AUTH_FACT_ID, ';
		END IF;
		IF OLD.SOURCE_AUTH_ID != NEW.SOURCE_AUTH_ID THEN
			lv_sql := lv_sql || 'SOURCE_AUTH_ID = $2.SOURCE_AUTH_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_AUTH_ID = $1.SOURCE_AUTH_ID OR SOURCE_AUTH_ID = $2.SOURCE_AUTH_ID)';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD != NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD = $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.SOURCE_TRAN_ID != NEW.SOURCE_TRAN_ID THEN
			lv_sql := lv_sql || 'SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_TRAN_ID = $1.SOURCE_TRAN_ID OR SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID)';
		END IF;
		IF OLD.AUTH_DETAIL_DIM_ID != NEW.AUTH_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_DETAIL_DIM_ID = $1.AUTH_DETAIL_DIM_ID OR AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID)';
		END IF;
		IF OLD.CONSUMER_DIM_ID != NEW.CONSUMER_DIM_ID THEN
			lv_sql := lv_sql || 'CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CONSUMER_DIM_ID = $1.CONSUMER_DIM_ID OR CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID)';
		END IF;
		IF OLD.AUTH_TYPE_DIM_ID != NEW.AUTH_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_TYPE_DIM_ID = $1.AUTH_TYPE_DIM_ID OR AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID)';
		END IF;
		IF OLD.AUTHORITY_TERMINAL_DIM_ID != NEW.AUTHORITY_TERMINAL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTHORITY_TERMINAL_DIM_ID = $2.AUTHORITY_TERMINAL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTHORITY_TERMINAL_DIM_ID = $1.AUTHORITY_TERMINAL_DIM_ID OR AUTHORITY_TERMINAL_DIM_ID = $2.AUTHORITY_TERMINAL_DIM_ID)';
		END IF;
		IF OLD.AUTH_DATE_DIM_ID != NEW.AUTH_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_DATE_DIM_ID = $2.AUTH_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_DATE_DIM_ID = $1.AUTH_DATE_DIM_ID OR AUTH_DATE_DIM_ID = $2.AUTH_DATE_DIM_ID)';
		END IF;
		IF OLD.AUTH_TIME_DIM_ID != NEW.AUTH_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_TIME_DIM_ID = $2.AUTH_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_TIME_DIM_ID = $1.AUTH_TIME_DIM_ID OR AUTH_TIME_DIM_ID = $2.AUTH_TIME_DIM_ID)';
		END IF;
		IF OLD.SERVER_DATE_DIM_ID != NEW.SERVER_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'SERVER_DATE_DIM_ID = $2.SERVER_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SERVER_DATE_DIM_ID = $1.SERVER_DATE_DIM_ID OR SERVER_DATE_DIM_ID = $2.SERVER_DATE_DIM_ID)';
		END IF;
		IF OLD.SERVER_TIME_DIM_ID != NEW.SERVER_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'SERVER_TIME_DIM_ID = $2.SERVER_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SERVER_TIME_DIM_ID = $1.SERVER_TIME_DIM_ID OR SERVER_TIME_DIM_ID = $2.SERVER_TIME_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
			lv_filter := lv_filter || ' AND (POS_DIM_ID = $1.POS_DIM_ID OR POS_DIM_ID = $2.POS_DIM_ID)';
		END IF;
		IF OLD.TRAN_ITEM_DETAIL_DIM_ID != NEW.TRAN_ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'TRAN_ITEM_DETAIL_DIM_ID = $2.TRAN_ITEM_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (TRAN_ITEM_DETAIL_DIM_ID = $1.TRAN_ITEM_DETAIL_DIM_ID OR TRAN_ITEM_DETAIL_DIM_ID = $2.TRAN_ITEM_DETAIL_DIM_ID)';
		END IF;
		IF OLD.FILL_ITEM_DETAIL_DIM_ID != NEW.FILL_ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'FILL_ITEM_DETAIL_DIM_ID = $2.FILL_ITEM_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (FILL_ITEM_DETAIL_DIM_ID = $1.FILL_ITEM_DETAIL_DIM_ID OR FILL_ITEM_DETAIL_DIM_ID = $2.FILL_ITEM_DETAIL_DIM_ID)';
		END IF;
		IF OLD.SETTLED_SERVER_DATE_DIM_ID != NEW.SETTLED_SERVER_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'SETTLED_SERVER_DATE_DIM_ID = $2.SETTLED_SERVER_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SETTLED_SERVER_DATE_DIM_ID = $1.SETTLED_SERVER_DATE_DIM_ID OR SETTLED_SERVER_DATE_DIM_ID = $2.SETTLED_SERVER_DATE_DIM_ID)';
		END IF;
		IF OLD.SETTLED_SERVER_TIME_DIM_ID != NEW.SETTLED_SERVER_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'SETTLED_SERVER_TIME_DIM_ID = $2.SETTLED_SERVER_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SETTLED_SERVER_TIME_DIM_ID = $1.SETTLED_SERVER_TIME_DIM_ID OR SETTLED_SERVER_TIME_DIM_ID = $2.SETTLED_SERVER_TIME_DIM_ID)';
		END IF;
		IF OLD.CURRENCY_DIM_ID != NEW.CURRENCY_DIM_ID THEN
			lv_sql := lv_sql || 'CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CURRENCY_DIM_ID = $1.CURRENCY_DIM_ID OR CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID)';
		END IF;
		IF OLD.DEVICE_CALL_DIM_ID != NEW.DEVICE_CALL_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID OR DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID)';
		END IF;
		IF OLD.AMOUNT != NEW.AMOUNT THEN
			lv_sql := lv_sql || 'AMOUNT = $2.AMOUNT, ';
			lv_filter := lv_filter || ' AND (AMOUNT = $1.AMOUNT OR AMOUNT = $2.AMOUNT)';
		END IF;
		IF OLD.APPROVED_AMOUNT IS DISTINCT FROM NEW.APPROVED_AMOUNT THEN
			lv_sql := lv_sql || 'APPROVED_AMOUNT = $2.APPROVED_AMOUNT, ';
			lv_filter := lv_filter || ' AND (APPROVED_AMOUNT IS NOT DISTINCT FROM $1.APPROVED_AMOUNT OR APPROVED_AMOUNT IS NOT DISTINCT FROM $2.APPROVED_AMOUNT)';
		END IF;
		IF OLD.PROCESSOR_MILLISECONDS IS DISTINCT FROM NEW.PROCESSOR_MILLISECONDS THEN
			lv_sql := lv_sql || 'PROCESSOR_MILLISECONDS = $2.PROCESSOR_MILLISECONDS, ';
			lv_filter := lv_filter || ' AND (PROCESSOR_MILLISECONDS IS NOT DISTINCT FROM $1.PROCESSOR_MILLISECONDS OR PROCESSOR_MILLISECONDS IS NOT DISTINCT FROM $2.PROCESSOR_MILLISECONDS)';
		END IF;
		IF OLD.NETWORK_MILLISECONDS IS DISTINCT FROM NEW.NETWORK_MILLISECONDS THEN
			lv_sql := lv_sql || 'NETWORK_MILLISECONDS = $2.NETWORK_MILLISECONDS, ';
			lv_filter := lv_filter || ' AND (NETWORK_MILLISECONDS IS NOT DISTINCT FROM $1.NETWORK_MILLISECONDS OR NETWORK_MILLISECONDS IS NOT DISTINCT FROM $2.NETWORK_MILLISECONDS)';
		END IF;
		IF OLD.AUTH_TS IS DISTINCT FROM NEW.AUTH_TS THEN
			lv_sql := lv_sql || 'AUTH_TS = $2.AUTH_TS, ';
			lv_filter := lv_filter || ' AND (AUTH_TS IS NOT DISTINCT FROM $1.AUTH_TS OR AUTH_TS IS NOT DISTINCT FROM $2.AUTH_TS)';
		END IF;
		IF OLD.SETTLED_TS IS DISTINCT FROM NEW.SETTLED_TS THEN
			lv_sql := lv_sql || 'SETTLED_TS = $2.SETTLED_TS, ';
			lv_filter := lv_filter || ' AND (SETTLED_TS IS NOT DISTINCT FROM $1.SETTLED_TS OR SETTLED_TS IS NOT DISTINCT FROM $2.SETTLED_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE AUTH_FACT_ID = $1.AUTH_FACT_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'AUTH_DATE_DIM_ID >= ' || RDW.PARTITION_START_DATE_DIM_ID(NEW.AUTH_DATE_DIM_ID, 'month') || ' AND AUTH_DATE_DIM_ID < ' || RDW.PARTITION_END_DATE_DIM_ID(NEW.AUTH_DATE_DIM_ID, 'month')||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(AUTH_FACT_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_AUTH_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_AUTHORITY_TERMINAL_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTHORITY_TERMINAL_DIM_ID) REFERENCES RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TERMINAL_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_AUTH_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTH_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_AUTH_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTH_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_SERVER_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SERVER_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_SERVER_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SERVER_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_SETTLED_SERVER_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SETTLED_SERVER_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_SETTLED_SERVER_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SETTLED_SERVER_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_AUTH_FACT_CURRENCY_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID)) INHERITS(RDW._AUTH_FACT) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_AUTH_FACT', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE AUTH_FACT_ID = $1.AUTH_FACT_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.AUTH_FACT
	AS SELECT * FROM RDW._AUTH_FACT;

ALTER VIEW RDW.AUTH_FACT ALTER AUTH_FACT_ID SET DEFAULT nextval('RDW._auth_fact_auth_fact_id_seq'::regclass);
ALTER VIEW RDW.AUTH_FACT ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.AUTH_FACT ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.AUTH_FACT ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.AUTH_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.AUTH_FACT TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._AUTH_FACT FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_AUTH_FACT ON RDW.AUTH_FACT;
CREATE TRIGGER TRIIUD_AUTH_FACT
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.AUTH_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_AUTH_FACT();


CREATE UNIQUE INDEX AK_AUTH_FACT ON RDW._AUTH_FACT(SOURCE_AUTH_ID, SOURCE_SYSTEM_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_SOURCE_TRAN_ID ON RDW._AUTH_FACT(SOURCE_TRAN_ID) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX UX_AUTH_FACT_AUTH_DETAIL_DIM_ID ON RDW._AUTH_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_AUTH_DETAIL_DIM_ID ON RDW._AUTH_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_CONSUMER_DIM_ID ON RDW._AUTH_FACT(CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_AUTH_TYPE_DIM_ID ON RDW._AUTH_FACT(AUTH_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_AUTHORITY_TERMINAL_DIM_ID ON RDW._AUTH_FACT(AUTHORITY_TERMINAL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_AUTH_DATE_DIM_ID ON RDW._AUTH_FACT(AUTH_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_AUTH_TIME_DIM_ID ON RDW._AUTH_FACT(AUTH_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_SERVER_DATE_DIM_ID ON RDW._AUTH_FACT(SERVER_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_SERVER_TIME_DIM_ID ON RDW._AUTH_FACT(SERVER_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_DEVICE_DIM_ID ON RDW._AUTH_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_LOCATION_DIM_ID_AUTH_TS ON RDW._AUTH_FACT(LOCATION_DIM_ID, AUTH_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_LOCATION_DIM_ID ON RDW._AUTH_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_POS_DIM_ID ON RDW._AUTH_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_TRAN_ITEM_DETAIL_DIM_ID ON RDW._AUTH_FACT(TRAN_ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_FILL_ITEM_DETAIL_DIM_ID ON RDW._AUTH_FACT(FILL_ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_SETTLED_SERVER_DATE_DIM_ID ON RDW._AUTH_FACT(SETTLED_SERVER_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_SETTLED_SERVER_TIME_DIM_ID ON RDW._AUTH_FACT(SETTLED_SERVER_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_CURRENCY_DIM_ID ON RDW._AUTH_FACT(CURRENCY_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_DEVICE_CALL_DIM_ID ON RDW._AUTH_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_SETTLED_TS ON RDW._AUTH_FACT(SETTLED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_AUTH_FACT_CREATED_TS ON RDW._AUTH_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_AUTH_FACT(
	p_auth_fact_id OUT BIGINT, 
	p_source_auth_id BIGINT, 
	p_source_system_cd VARCHAR(10), 
	p_source_tran_id BIGINT, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_auth_type_dim_id INTEGER, 
	p_authority_terminal_dim_id BIGINT, 
	p_auth_date_dim_id SMALLINT, 
	p_auth_time_dim_id SMALLINT, 
	p_server_date_dim_id SMALLINT, 
	p_server_time_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_tran_item_detail_dim_id BIGINT, 
	p_fill_item_detail_dim_id BIGINT, 
	p_settled_server_date_dim_id SMALLINT, 
	p_settled_server_time_dim_id SMALLINT, 
	p_currency_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_amount DECIMAL(12,4), 
	p_approved_amount DECIMAL(12,4), 
	p_processor_milliseconds BIGINT, 
	p_network_milliseconds BIGINT, 
	p_auth_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT AUTH_FACT_ID, REVISION
			  INTO p_auth_fact_id, p_old_revision
			  FROM RDW.AUTH_FACT
			 WHERE SOURCE_AUTH_ID = p_source_auth_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.AUTH_FACT
			   SET SOURCE_TRAN_ID = p_source_tran_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTHORITY_TERMINAL_DIM_ID = p_authority_terminal_dim_id,
			       AUTH_DATE_DIM_ID = p_auth_date_dim_id,
			       AUTH_TIME_DIM_ID = p_auth_time_dim_id,
			       SERVER_DATE_DIM_ID = p_server_date_dim_id,
			       SERVER_TIME_DIM_ID = p_server_time_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       TRAN_ITEM_DETAIL_DIM_ID = p_tran_item_detail_dim_id,
			       FILL_ITEM_DETAIL_DIM_ID = p_fill_item_detail_dim_id,
			       SETTLED_SERVER_DATE_DIM_ID = p_settled_server_date_dim_id,
			       SETTLED_SERVER_TIME_DIM_ID = p_settled_server_time_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       AMOUNT = p_amount,
			       APPROVED_AMOUNT = p_approved_amount,
			       PROCESSOR_MILLISECONDS = p_processor_milliseconds,
			       NETWORK_MILLISECONDS = p_network_milliseconds,
			       AUTH_TS = p_auth_ts,
			       SETTLED_TS = p_settled_ts,
			       REVISION = p_revision
			 WHERE SOURCE_AUTH_ID = p_source_auth_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.AUTH_FACT(SOURCE_AUTH_ID, SOURCE_SYSTEM_CD, SOURCE_TRAN_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, AUTH_TYPE_DIM_ID, AUTHORITY_TERMINAL_DIM_ID, AUTH_DATE_DIM_ID, AUTH_TIME_DIM_ID, SERVER_DATE_DIM_ID, SERVER_TIME_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, TRAN_ITEM_DETAIL_DIM_ID, FILL_ITEM_DETAIL_DIM_ID, SETTLED_SERVER_DATE_DIM_ID, SETTLED_SERVER_TIME_DIM_ID, CURRENCY_DIM_ID, DEVICE_CALL_DIM_ID, AMOUNT, APPROVED_AMOUNT, PROCESSOR_MILLISECONDS, NETWORK_MILLISECONDS, AUTH_TS, SETTLED_TS, REVISION)
					 VALUES(p_source_auth_id, p_source_system_cd, p_source_tran_id, p_auth_detail_dim_id, p_consumer_dim_id, p_auth_type_dim_id, p_authority_terminal_dim_id, p_auth_date_dim_id, p_auth_time_dim_id, p_server_date_dim_id, p_server_time_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_tran_item_detail_dim_id, p_fill_item_detail_dim_id, p_settled_server_date_dim_id, p_settled_server_time_dim_id, p_currency_dim_id, p_device_call_dim_id, p_amount, p_approved_amount, p_processor_milliseconds, p_network_milliseconds, p_auth_ts, p_settled_ts, p_revision)
					 RETURNING AUTH_FACT_ID
					      INTO p_auth_fact_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_AUTH_FACT(
	p_auth_fact_id OUT BIGINT, 
	p_source_auth_id BIGINT, 
	p_source_system_cd VARCHAR(10), 
	p_source_tran_id BIGINT, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_auth_type_dim_id INTEGER, 
	p_authority_terminal_dim_id BIGINT, 
	p_auth_date_dim_id SMALLINT, 
	p_auth_time_dim_id SMALLINT, 
	p_server_date_dim_id SMALLINT, 
	p_server_time_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_tran_item_detail_dim_id BIGINT, 
	p_fill_item_detail_dim_id BIGINT, 
	p_settled_server_date_dim_id SMALLINT, 
	p_settled_server_time_dim_id SMALLINT, 
	p_currency_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_amount DECIMAL(12,4), 
	p_approved_amount DECIMAL(12,4), 
	p_processor_milliseconds BIGINT, 
	p_network_milliseconds BIGINT, 
	p_auth_ts TIMESTAMPTZ, 
	p_settled_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW.ITEM_FACT
  (
    ITEM_FACT_ID       BIGINT      NOT NULL DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass),
    ITEM_TYPE_DIM_ID   SMALLINT    NOT NULL,
    ITEM_DETAIL_DIM_ID BIGINT      NOT NULL,
    DEVICE_DIM_ID      BIGINT      NOT NULL,
    LOCATION_DIM_ID    BIGINT      NOT NULL,
    POS_DIM_ID         BIGINT      NOT NULL,
    ITEM_DATE_DIM_ID   SMALLINT    NOT NULL,
    ITEM_TIME_DIM_ID   SMALLINT    NOT NULL,
    UPLOAD_DATE_DIM_ID SMALLINT    NOT NULL,
    UPLOAD_TIME_DIM_ID SMALLINT    NOT NULL,
    DEVICE_CALL_DIM_ID BIGINT      NOT NULL,
    CURRENCY_DIM_ID    SMALLINT    NOT NULL,
    AUTH_TYPE_DIM_ID   INTEGER     NOT NULL,
    AUTH_DETAIL_DIM_ID BIGINT      NOT NULL,
    CONSUMER_DIM_ID    BIGINT      NOT NULL,
    ITEM_TS            TIMESTAMPTZ NOT NULL,

    CONSTRAINT PK_ITEM_FACT PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_ITEM_FACT_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_ITEM_DETAIL_DIM_ID FOREIGN KEY(ITEM_DETAIL_DIM_ID) REFERENCES RDW.ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_ITEM_DATE_DIM_ID FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_ITEM_TIME_DIM_ID FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_UPLOAD_DATE_DIM_ID FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_UPLOAD_TIME_DIM_ID FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_AUTH_TYPE_DIM_ID FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_AUTH_DETAIL_DIM_ID FOREIGN KEY(AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_ITEM_FACT_CONSUMER_DIM_ID FOREIGN KEY(CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID)
  )
  INHERITS(RDW._)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.ITEM_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.ITEM_FACT TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_ITEM_FACT ON RDW.ITEM_FACT;


CREATE TRIGGER TRBI_ITEM_FACT
BEFORE INSERT ON RDW.ITEM_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_ITEM_FACT ON RDW.ITEM_FACT;


CREATE TRIGGER TRBU_ITEM_FACT
BEFORE UPDATE ON RDW.ITEM_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX_ITEM_FACT_ITEM_DETAIL_DIM_ID ON RDW.ITEM_FACT(ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_DEVICE_DIM_ID ON RDW.ITEM_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_LOCATION_DIM_ID_ITEM_TS ON RDW.ITEM_FACT(LOCATION_DIM_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_LOCATION_DIM_ID ON RDW.ITEM_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_POS_DIM_ID ON RDW.ITEM_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_ITEM_DATE_DIM_ID ON RDW.ITEM_FACT(ITEM_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_ITEM_TIME_DIM_ID ON RDW.ITEM_FACT(ITEM_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_UPLOAD_DATE_DIM_ID ON RDW.ITEM_FACT(UPLOAD_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_UPLOAD_TIME_DIM_ID ON RDW.ITEM_FACT(UPLOAD_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_DEVICE_CALL_DIM_ID ON RDW.ITEM_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_AUTH_TYPE_DIM_ID ON RDW.ITEM_FACT(AUTH_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_AUTH_DETAIL_DIM_ID ON RDW.ITEM_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_CONSUMER_DIM_ID ON RDW.ITEM_FACT(CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_ITEM_FACT_CREATED_TS ON RDW.ITEM_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_ITEM_FACT(
	p_item_fact_id BIGINT, 
	p_item_type_dim_id SMALLINT, 
	p_item_detail_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT REVISION
			  INTO p_old_revision
			  FROM RDW.ITEM_FACT
			 WHERE ITEM_FACT_ID = p_item_fact_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.ITEM_FACT
			   SET ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       ITEM_DETAIL_DIM_ID = p_item_detail_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       ITEM_DATE_DIM_ID = p_item_date_dim_id,
			       ITEM_TIME_DIM_ID = p_item_time_dim_id,
			       UPLOAD_DATE_DIM_ID = p_upload_date_dim_id,
			       UPLOAD_TIME_DIM_ID = p_upload_time_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       ITEM_TS = p_item_ts,
			       REVISION = p_revision
			 WHERE ITEM_FACT_ID = p_item_fact_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.ITEM_FACT(ITEM_FACT_ID, ITEM_TYPE_DIM_ID, ITEM_DETAIL_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, ITEM_DATE_DIM_ID, ITEM_TIME_DIM_ID, UPLOAD_DATE_DIM_ID, UPLOAD_TIME_DIM_ID, DEVICE_CALL_DIM_ID, CURRENCY_DIM_ID, AUTH_TYPE_DIM_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, ITEM_TS, REVISION)
					 VALUES(COALESCE(p_item_fact_id,  nextval('rdw.item_fact_item_fact_id_seq'::regclass)), p_item_type_dim_id, p_item_detail_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_item_date_dim_id, p_item_time_dim_id, p_upload_date_dim_id, p_upload_time_dim_id, p_device_call_dim_id, p_currency_dim_id, p_auth_type_dim_id, p_auth_detail_dim_id, p_consumer_dim_id, p_item_ts, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_ITEM_FACT(
	p_item_fact_id BIGINT, 
	p_item_type_dim_id SMALLINT, 
	p_item_detail_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ----------------------------------------------------------------------

ALTER TABLE RDW.ITEM_FACT ADD CONSTRAINT CK_ITEM_FACT_DENY CHECK(false) NO INHERIT;

-- ======================================================================

CREATE TABLE RDW.PAYMENT_ITEM_FACT
  (
    ITEM_FACT_ID         BIGINT        NOT NULL DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass),
    SOURCE_LEDGER_ID     BIGINT        NOT NULL,
    SETTLED_DATE_DIM_ID  SMALLINT      NOT NULL,
    SETTLED_TIME_DIM_ID  SMALLINT      NOT NULL,
    PAYMENT_DIM_ID       BIGINT        NOT NULL,
    PAYMENT_BATCH_DIM_ID BIGINT        NOT NULL,
    PAID_DATE_DIM_ID     SMALLINT      NOT NULL,
    QUANTITY             NUMERIC       ,
    AMOUNT               DECIMAL(12,4) ,
    PAID_TS              TIMESTAMPTZ   ,

    CONSTRAINT PK_PAYMENT_ITEM_FACT PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_PAYMENT_ITEM_FACT_SETTLED_DATE_DIM_ID FOREIGN KEY(SETTLED_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_SETTLED_TIME_DIM_ID FOREIGN KEY(SETTLED_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_PAYMENT_DIM_ID FOREIGN KEY(PAYMENT_DIM_ID) REFERENCES RDW._PAYMENT_DIM(PAYMENT_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_PAYMENT_BATCH_DIM_ID FOREIGN KEY(PAYMENT_BATCH_DIM_ID) REFERENCES RDW._PAYMENT_BATCH_DIM(PAYMENT_BATCH_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_PAID_DATE_DIM_ID FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_ITEM_DETAIL_DIM_ID FOREIGN KEY(ITEM_DETAIL_DIM_ID) REFERENCES RDW.ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_ITEM_DATE_DIM_ID FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_ITEM_TIME_DIM_ID FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_UPLOAD_DATE_DIM_ID FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_UPLOAD_TIME_DIM_ID FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_AUTH_TYPE_DIM_ID FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_AUTH_DETAIL_DIM_ID FOREIGN KEY(AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_PAYMENT_ITEM_FACT_CONSUMER_DIM_ID FOREIGN KEY(CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID)
  )
  INHERITS(RDW.ITEM_FACT)
  TABLESPACE rdw_data_t1;

GRANT SELECT ON RDW.PAYMENT_ITEM_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.PAYMENT_ITEM_FACT TO write_rdw;


DROP TRIGGER IF EXISTS TRBI_PAYMENT_ITEM_FACT ON RDW.PAYMENT_ITEM_FACT;


CREATE TRIGGER TRBI_PAYMENT_ITEM_FACT
BEFORE INSERT ON RDW.PAYMENT_ITEM_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBI_AUDIT();



DROP TRIGGER IF EXISTS TRBU_PAYMENT_ITEM_FACT ON RDW.PAYMENT_ITEM_FACT;


CREATE TRIGGER TRBU_PAYMENT_ITEM_FACT
BEFORE UPDATE ON RDW.PAYMENT_ITEM_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FTRBU_AUDIT();


CREATE INDEX IX_PAYMENT_ITEM_FACT_SETTLED_DATE_DIM_ID ON RDW.PAYMENT_ITEM_FACT(SETTLED_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_SETTLED_TIME_DIM_ID ON RDW.PAYMENT_ITEM_FACT(SETTLED_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_PAYMENT_DIM_ID ON RDW.PAYMENT_ITEM_FACT(PAYMENT_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_PAYMENT_BATCH_DIM_ID ON RDW.PAYMENT_ITEM_FACT(PAYMENT_BATCH_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_PAID_DATE_DIM_ID ON RDW.PAYMENT_ITEM_FACT(PAID_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_ITEM_DETAIL_DIM_ID ON RDW.PAYMENT_ITEM_FACT(ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_DEVICE_DIM_ID ON RDW.PAYMENT_ITEM_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_LOCATION_DIM_ID_ITEM_TS ON RDW.PAYMENT_ITEM_FACT(LOCATION_DIM_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_LOCATION_DIM_ID ON RDW.PAYMENT_ITEM_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_POS_DIM_ID ON RDW.PAYMENT_ITEM_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_ITEM_DATE_DIM_ID ON RDW.PAYMENT_ITEM_FACT(ITEM_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_ITEM_TIME_DIM_ID ON RDW.PAYMENT_ITEM_FACT(ITEM_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_UPLOAD_DATE_DIM_ID ON RDW.PAYMENT_ITEM_FACT(UPLOAD_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_UPLOAD_TIME_DIM_ID ON RDW.PAYMENT_ITEM_FACT(UPLOAD_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_DEVICE_CALL_DIM_ID ON RDW.PAYMENT_ITEM_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_AUTH_TYPE_DIM_ID ON RDW.PAYMENT_ITEM_FACT(AUTH_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_AUTH_DETAIL_DIM_ID ON RDW.PAYMENT_ITEM_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_CONSUMER_DIM_ID ON RDW.PAYMENT_ITEM_FACT(CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_PAYMENT_ITEM_FACT_CREATED_TS ON RDW.PAYMENT_ITEM_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_PAYMENT_ITEM_FACT(
	p_item_fact_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_item_detail_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT REVISION
			  INTO p_old_revision
			  FROM RDW.PAYMENT_ITEM_FACT
			 WHERE ITEM_FACT_ID = p_item_fact_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.PAYMENT_ITEM_FACT
			   SET SOURCE_LEDGER_ID = p_source_ledger_id,
			       SETTLED_DATE_DIM_ID = p_settled_date_dim_id,
			       SETTLED_TIME_DIM_ID = p_settled_time_dim_id,
			       PAYMENT_DIM_ID = p_payment_dim_id,
			       PAYMENT_BATCH_DIM_ID = p_payment_batch_dim_id,
			       PAID_DATE_DIM_ID = p_paid_date_dim_id,
			       QUANTITY = p_quantity,
			       AMOUNT = p_amount,
			       PAID_TS = p_paid_ts,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       ITEM_DETAIL_DIM_ID = p_item_detail_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       ITEM_DATE_DIM_ID = p_item_date_dim_id,
			       ITEM_TIME_DIM_ID = p_item_time_dim_id,
			       UPLOAD_DATE_DIM_ID = p_upload_date_dim_id,
			       UPLOAD_TIME_DIM_ID = p_upload_time_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       ITEM_TS = p_item_ts,
			       REVISION = p_revision
			 WHERE ITEM_FACT_ID = p_item_fact_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.PAYMENT_ITEM_FACT(ITEM_FACT_ID, SOURCE_LEDGER_ID, SETTLED_DATE_DIM_ID, SETTLED_TIME_DIM_ID, PAYMENT_DIM_ID, PAYMENT_BATCH_DIM_ID, PAID_DATE_DIM_ID, QUANTITY, AMOUNT, PAID_TS, ITEM_TYPE_DIM_ID, ITEM_DETAIL_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, ITEM_DATE_DIM_ID, ITEM_TIME_DIM_ID, UPLOAD_DATE_DIM_ID, UPLOAD_TIME_DIM_ID, DEVICE_CALL_DIM_ID, CURRENCY_DIM_ID, AUTH_TYPE_DIM_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, ITEM_TS, REVISION)
					 VALUES(COALESCE(p_item_fact_id,  nextval('rdw.item_fact_item_fact_id_seq'::regclass)), p_source_ledger_id, p_settled_date_dim_id, p_settled_time_dim_id, p_payment_dim_id, p_payment_batch_dim_id, p_paid_date_dim_id, p_quantity, p_amount, p_paid_ts, p_item_type_dim_id, p_item_detail_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_item_date_dim_id, p_item_time_dim_id, p_upload_date_dim_id, p_upload_time_dim_id, p_device_call_dim_id, p_currency_dim_id, p_auth_type_dim_id, p_auth_detail_dim_id, p_consumer_dim_id, p_item_ts, p_revision);
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_PAYMENT_ITEM_FACT(
	p_item_fact_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_item_detail_dim_id BIGINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._TRAN_ITEM_FACT
  (
    ITEM_FACT_ID              BIGINT         NOT NULL DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass),
    ITEM_DETAIL_DIM_ID        BIGINT         NOT NULL,
    SOURCE_TRAN_LINE_ITEM_ID  BIGINT         ,
    SOURCE_TRAN_ID            BIGINT         NOT NULL,
    REPORT_TRAN_ID            BIGINT         ,
    SOURCE_SYSTEM_CD          VARCHAR(30)    ,
    TRAN_LINE_ITEM_DIM_ID     BIGINT         NOT NULL,
    HOST_DIM_ID               BIGINT         NOT NULL,
    FILL_DIM_ID               BIGINT         NOT NULL,
    FILL_DATE_DIM_ID          SMALLINT       NOT NULL,
    FILL_TIME_DIM_ID          SMALLINT       NOT NULL,
    AUTHORITY_TERMINAL_DIM_ID BIGINT         NOT NULL,
    APPLY_TO_CONSUMER_DIM_ID  BIGINT         ,
    ORIG_TRAN_DIM_ID          BIGINT         ,
    ORIG_AUTH_DETAIL_DIM_ID   BIGINT         NOT NULL,
    UNIT_PRICE                DECIMAL(12,4)  ,
    PRODUCT_SERVICE_QUANTITY  INTEGER        ,
    PROCESS_FEE_AMOUNT        DECIMAL(12,4)  ,
    DURATION_SECONDS_FRACTION DECIMAL(22,10) ,
    TRAN_COUNT_FRACTION       DECIMAL(22,21) ,
    CONVENIENCE_FEE_AMOUNT    DECIMAL(12,4)  ,
    LOYALTY_DISCOUNT_AMOUNT   DECIMAL(12,4)  ,
    FILL_TS                   TIMESTAMPTZ    ,

    CONSTRAINT PK_TRAN_ITEM_FACT PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_TRAN_ITEM_FACT_ITEM_DETAIL_DIM_ID FOREIGN KEY(ITEM_DETAIL_DIM_ID) REFERENCES RDW._TRAN_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_TRAN_LINE_ITEM_DIM_ID FOREIGN KEY(TRAN_LINE_ITEM_DIM_ID) REFERENCES RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_HOST_DIM_ID FOREIGN KEY(HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_FILL_DIM_ID FOREIGN KEY(FILL_DIM_ID) REFERENCES RDW._FILL_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_FILL_DATE_DIM_ID FOREIGN KEY(FILL_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_FILL_TIME_DIM_ID FOREIGN KEY(FILL_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_AUTHORITY_TERMINAL_DIM_ID FOREIGN KEY(AUTHORITY_TERMINAL_DIM_ID) REFERENCES RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TERMINAL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_APPLY_TO_CONSUMER_DIM_ID FOREIGN KEY(APPLY_TO_CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_ORIG_TRAN_DIM_ID FOREIGN KEY(ORIG_TRAN_DIM_ID) REFERENCES RDW._TRAN_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_ORIG_AUTH_DETAIL_DIM_ID FOREIGN KEY(ORIG_AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_SETTLED_DATE_DIM_ID FOREIGN KEY(SETTLED_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_SETTLED_TIME_DIM_ID FOREIGN KEY(SETTLED_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_PAYMENT_DIM_ID FOREIGN KEY(PAYMENT_DIM_ID) REFERENCES RDW._PAYMENT_DIM(PAYMENT_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_PAYMENT_BATCH_DIM_ID FOREIGN KEY(PAYMENT_BATCH_DIM_ID) REFERENCES RDW._PAYMENT_BATCH_DIM(PAYMENT_BATCH_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_PAID_DATE_DIM_ID FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_ITEM_DATE_DIM_ID FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_ITEM_TIME_DIM_ID FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_UPLOAD_DATE_DIM_ID FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_UPLOAD_TIME_DIM_ID FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_AUTH_TYPE_DIM_ID FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_AUTH_DETAIL_DIM_ID FOREIGN KEY(AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_TRAN_ITEM_FACT_CONSUMER_DIM_ID FOREIGN KEY(CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID)
  )
  INHERITS(RDW.PAYMENT_ITEM_FACT)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_TRAN_ITEM_FACT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE((ARRAY['CREDIT_', 'CASH_', 'OTHER_'])[NEW.ITEM_TYPE_DIM_ID - 1] || TO_CHAR(DATE'epoch' + NEW.ITEM_DATE_DIM_ID, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_TRAN_ITEM_FACT', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_TRAN_ITEM_FACT', COALESCE((ARRAY['CREDIT_', 'CASH_', 'OTHER_'])[OLD.ITEM_TYPE_DIM_ID - 1] || TO_CHAR(DATE'epoch' + OLD.ITEM_DATE_DIM_ID, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ITEM_FACT_ID != NEW.ITEM_FACT_ID THEN
			lv_sql := lv_sql || 'ITEM_FACT_ID = $2.ITEM_FACT_ID, ';
		END IF;
		IF OLD.SOURCE_TRAN_LINE_ITEM_ID IS DISTINCT FROM NEW.SOURCE_TRAN_LINE_ITEM_ID THEN
			lv_sql := lv_sql || 'SOURCE_TRAN_LINE_ITEM_ID = $2.SOURCE_TRAN_LINE_ITEM_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_TRAN_LINE_ITEM_ID IS NOT DISTINCT FROM $1.SOURCE_TRAN_LINE_ITEM_ID OR SOURCE_TRAN_LINE_ITEM_ID IS NOT DISTINCT FROM $2.SOURCE_TRAN_LINE_ITEM_ID)';
		END IF;
		IF OLD.SOURCE_TRAN_ID != NEW.SOURCE_TRAN_ID THEN
			lv_sql := lv_sql || 'SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_TRAN_ID = $1.SOURCE_TRAN_ID OR SOURCE_TRAN_ID = $2.SOURCE_TRAN_ID)';
		END IF;
		IF OLD.ITEM_DETAIL_DIM_ID != NEW.ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID OR ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID)';
		END IF;
		IF OLD.REPORT_TRAN_ID IS DISTINCT FROM NEW.REPORT_TRAN_ID THEN
			lv_sql := lv_sql || 'REPORT_TRAN_ID = $2.REPORT_TRAN_ID, ';
			lv_filter := lv_filter || ' AND (REPORT_TRAN_ID IS NOT DISTINCT FROM $1.REPORT_TRAN_ID OR REPORT_TRAN_ID IS NOT DISTINCT FROM $2.REPORT_TRAN_ID)';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD IS DISTINCT FROM NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD IS NOT DISTINCT FROM $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD IS NOT DISTINCT FROM $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.TRAN_LINE_ITEM_DIM_ID != NEW.TRAN_LINE_ITEM_DIM_ID THEN
			lv_sql := lv_sql || 'TRAN_LINE_ITEM_DIM_ID = $2.TRAN_LINE_ITEM_DIM_ID, ';
			lv_filter := lv_filter || ' AND (TRAN_LINE_ITEM_DIM_ID = $1.TRAN_LINE_ITEM_DIM_ID OR TRAN_LINE_ITEM_DIM_ID = $2.TRAN_LINE_ITEM_DIM_ID)';
		END IF;
		IF OLD.HOST_DIM_ID != NEW.HOST_DIM_ID THEN
			lv_sql := lv_sql || 'HOST_DIM_ID = $2.HOST_DIM_ID, ';
			lv_filter := lv_filter || ' AND (HOST_DIM_ID = $1.HOST_DIM_ID OR HOST_DIM_ID = $2.HOST_DIM_ID)';
		END IF;
		IF OLD.FILL_DIM_ID != NEW.FILL_DIM_ID THEN
			lv_sql := lv_sql || 'FILL_DIM_ID = $2.FILL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (FILL_DIM_ID = $1.FILL_DIM_ID OR FILL_DIM_ID = $2.FILL_DIM_ID)';
		END IF;
		IF OLD.FILL_DATE_DIM_ID != NEW.FILL_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'FILL_DATE_DIM_ID = $2.FILL_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (FILL_DATE_DIM_ID = $1.FILL_DATE_DIM_ID OR FILL_DATE_DIM_ID = $2.FILL_DATE_DIM_ID)';
		END IF;
		IF OLD.FILL_TIME_DIM_ID != NEW.FILL_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'FILL_TIME_DIM_ID = $2.FILL_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (FILL_TIME_DIM_ID = $1.FILL_TIME_DIM_ID OR FILL_TIME_DIM_ID = $2.FILL_TIME_DIM_ID)';
		END IF;
		IF OLD.AUTHORITY_TERMINAL_DIM_ID != NEW.AUTHORITY_TERMINAL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTHORITY_TERMINAL_DIM_ID = $2.AUTHORITY_TERMINAL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTHORITY_TERMINAL_DIM_ID = $1.AUTHORITY_TERMINAL_DIM_ID OR AUTHORITY_TERMINAL_DIM_ID = $2.AUTHORITY_TERMINAL_DIM_ID)';
		END IF;
		IF OLD.APPLY_TO_CONSUMER_DIM_ID IS DISTINCT FROM NEW.APPLY_TO_CONSUMER_DIM_ID THEN
			lv_sql := lv_sql || 'APPLY_TO_CONSUMER_DIM_ID = $2.APPLY_TO_CONSUMER_DIM_ID, ';
			lv_filter := lv_filter || ' AND (APPLY_TO_CONSUMER_DIM_ID IS NOT DISTINCT FROM $1.APPLY_TO_CONSUMER_DIM_ID OR APPLY_TO_CONSUMER_DIM_ID IS NOT DISTINCT FROM $2.APPLY_TO_CONSUMER_DIM_ID)';
		END IF;
		IF OLD.ORIG_TRAN_DIM_ID IS DISTINCT FROM NEW.ORIG_TRAN_DIM_ID THEN
			lv_sql := lv_sql || 'ORIG_TRAN_DIM_ID = $2.ORIG_TRAN_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ORIG_TRAN_DIM_ID IS NOT DISTINCT FROM $1.ORIG_TRAN_DIM_ID OR ORIG_TRAN_DIM_ID IS NOT DISTINCT FROM $2.ORIG_TRAN_DIM_ID)';
		END IF;
		IF OLD.ORIG_AUTH_DETAIL_DIM_ID != NEW.ORIG_AUTH_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ORIG_AUTH_DETAIL_DIM_ID = $2.ORIG_AUTH_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ORIG_AUTH_DETAIL_DIM_ID = $1.ORIG_AUTH_DETAIL_DIM_ID OR ORIG_AUTH_DETAIL_DIM_ID = $2.ORIG_AUTH_DETAIL_DIM_ID)';
		END IF;
		IF OLD.UNIT_PRICE IS DISTINCT FROM NEW.UNIT_PRICE THEN
			lv_sql := lv_sql || 'UNIT_PRICE = $2.UNIT_PRICE, ';
			lv_filter := lv_filter || ' AND (UNIT_PRICE IS NOT DISTINCT FROM $1.UNIT_PRICE OR UNIT_PRICE IS NOT DISTINCT FROM $2.UNIT_PRICE)';
		END IF;
		IF OLD.PRODUCT_SERVICE_QUANTITY IS DISTINCT FROM NEW.PRODUCT_SERVICE_QUANTITY THEN
			lv_sql := lv_sql || 'PRODUCT_SERVICE_QUANTITY = $2.PRODUCT_SERVICE_QUANTITY, ';
			lv_filter := lv_filter || ' AND (PRODUCT_SERVICE_QUANTITY IS NOT DISTINCT FROM $1.PRODUCT_SERVICE_QUANTITY OR PRODUCT_SERVICE_QUANTITY IS NOT DISTINCT FROM $2.PRODUCT_SERVICE_QUANTITY)';
		END IF;
		IF OLD.PROCESS_FEE_AMOUNT IS DISTINCT FROM NEW.PROCESS_FEE_AMOUNT THEN
			lv_sql := lv_sql || 'PROCESS_FEE_AMOUNT = $2.PROCESS_FEE_AMOUNT, ';
			lv_filter := lv_filter || ' AND (PROCESS_FEE_AMOUNT IS NOT DISTINCT FROM $1.PROCESS_FEE_AMOUNT OR PROCESS_FEE_AMOUNT IS NOT DISTINCT FROM $2.PROCESS_FEE_AMOUNT)';
		END IF;
		IF OLD.DURATION_SECONDS_FRACTION IS DISTINCT FROM NEW.DURATION_SECONDS_FRACTION THEN
			lv_sql := lv_sql || 'DURATION_SECONDS_FRACTION = $2.DURATION_SECONDS_FRACTION, ';
			lv_filter := lv_filter || ' AND (DURATION_SECONDS_FRACTION IS NOT DISTINCT FROM $1.DURATION_SECONDS_FRACTION OR DURATION_SECONDS_FRACTION IS NOT DISTINCT FROM $2.DURATION_SECONDS_FRACTION)';
		END IF;
		IF OLD.TRAN_COUNT_FRACTION IS DISTINCT FROM NEW.TRAN_COUNT_FRACTION THEN
			lv_sql := lv_sql || 'TRAN_COUNT_FRACTION = $2.TRAN_COUNT_FRACTION, ';
			lv_filter := lv_filter || ' AND (TRAN_COUNT_FRACTION IS NOT DISTINCT FROM $1.TRAN_COUNT_FRACTION OR TRAN_COUNT_FRACTION IS NOT DISTINCT FROM $2.TRAN_COUNT_FRACTION)';
		END IF;
		IF OLD.CONVENIENCE_FEE_AMOUNT IS DISTINCT FROM NEW.CONVENIENCE_FEE_AMOUNT THEN
			lv_sql := lv_sql || 'CONVENIENCE_FEE_AMOUNT = $2.CONVENIENCE_FEE_AMOUNT, ';
			lv_filter := lv_filter || ' AND (CONVENIENCE_FEE_AMOUNT IS NOT DISTINCT FROM $1.CONVENIENCE_FEE_AMOUNT OR CONVENIENCE_FEE_AMOUNT IS NOT DISTINCT FROM $2.CONVENIENCE_FEE_AMOUNT)';
		END IF;
		IF OLD.LOYALTY_DISCOUNT_AMOUNT IS DISTINCT FROM NEW.LOYALTY_DISCOUNT_AMOUNT THEN
			lv_sql := lv_sql || 'LOYALTY_DISCOUNT_AMOUNT = $2.LOYALTY_DISCOUNT_AMOUNT, ';
			lv_filter := lv_filter || ' AND (LOYALTY_DISCOUNT_AMOUNT IS NOT DISTINCT FROM $1.LOYALTY_DISCOUNT_AMOUNT OR LOYALTY_DISCOUNT_AMOUNT IS NOT DISTINCT FROM $2.LOYALTY_DISCOUNT_AMOUNT)';
		END IF;
		IF OLD.FILL_TS IS DISTINCT FROM NEW.FILL_TS THEN
			lv_sql := lv_sql || 'FILL_TS = $2.FILL_TS, ';
			lv_filter := lv_filter || ' AND (FILL_TS IS NOT DISTINCT FROM $1.FILL_TS OR FILL_TS IS NOT DISTINCT FROM $2.FILL_TS)';
		END IF;
		IF OLD.SOURCE_LEDGER_ID != NEW.SOURCE_LEDGER_ID THEN
			lv_sql := lv_sql || 'SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_LEDGER_ID = $1.SOURCE_LEDGER_ID OR SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID)';
		END IF;
		IF OLD.SETTLED_DATE_DIM_ID != NEW.SETTLED_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'SETTLED_DATE_DIM_ID = $2.SETTLED_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SETTLED_DATE_DIM_ID = $1.SETTLED_DATE_DIM_ID OR SETTLED_DATE_DIM_ID = $2.SETTLED_DATE_DIM_ID)';
		END IF;
		IF OLD.SETTLED_TIME_DIM_ID != NEW.SETTLED_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'SETTLED_TIME_DIM_ID = $2.SETTLED_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SETTLED_TIME_DIM_ID = $1.SETTLED_TIME_DIM_ID OR SETTLED_TIME_DIM_ID = $2.SETTLED_TIME_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_DIM_ID != NEW.PAYMENT_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_DIM_ID = $1.PAYMENT_DIM_ID OR PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_BATCH_DIM_ID != NEW.PAYMENT_BATCH_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_BATCH_DIM_ID = $1.PAYMENT_BATCH_DIM_ID OR PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID)';
		END IF;
		IF OLD.PAID_DATE_DIM_ID != NEW.PAID_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'PAID_DATE_DIM_ID = $2.PAID_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAID_DATE_DIM_ID = $1.PAID_DATE_DIM_ID OR PAID_DATE_DIM_ID = $2.PAID_DATE_DIM_ID)';
		END IF;
		IF OLD.QUANTITY IS DISTINCT FROM NEW.QUANTITY THEN
			lv_sql := lv_sql || 'QUANTITY = $2.QUANTITY, ';
			lv_filter := lv_filter || ' AND (QUANTITY IS NOT DISTINCT FROM $1.QUANTITY OR QUANTITY IS NOT DISTINCT FROM $2.QUANTITY)';
		END IF;
		IF OLD.AMOUNT IS DISTINCT FROM NEW.AMOUNT THEN
			lv_sql := lv_sql || 'AMOUNT = $2.AMOUNT, ';
			lv_filter := lv_filter || ' AND (AMOUNT IS NOT DISTINCT FROM $1.AMOUNT OR AMOUNT IS NOT DISTINCT FROM $2.AMOUNT)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
			lv_filter := lv_filter || ' AND (POS_DIM_ID = $1.POS_DIM_ID OR POS_DIM_ID = $2.POS_DIM_ID)';
		END IF;
		IF OLD.ITEM_DATE_DIM_ID != NEW.ITEM_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DATE_DIM_ID = $2.ITEM_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_DATE_DIM_ID = $1.ITEM_DATE_DIM_ID OR ITEM_DATE_DIM_ID = $2.ITEM_DATE_DIM_ID)';
		END IF;
		IF OLD.ITEM_TIME_DIM_ID != NEW.ITEM_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TIME_DIM_ID = $2.ITEM_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TIME_DIM_ID = $1.ITEM_TIME_DIM_ID OR ITEM_TIME_DIM_ID = $2.ITEM_TIME_DIM_ID)';
		END IF;
		IF OLD.UPLOAD_DATE_DIM_ID != NEW.UPLOAD_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'UPLOAD_DATE_DIM_ID = $2.UPLOAD_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (UPLOAD_DATE_DIM_ID = $1.UPLOAD_DATE_DIM_ID OR UPLOAD_DATE_DIM_ID = $2.UPLOAD_DATE_DIM_ID)';
		END IF;
		IF OLD.UPLOAD_TIME_DIM_ID != NEW.UPLOAD_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'UPLOAD_TIME_DIM_ID = $2.UPLOAD_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (UPLOAD_TIME_DIM_ID = $1.UPLOAD_TIME_DIM_ID OR UPLOAD_TIME_DIM_ID = $2.UPLOAD_TIME_DIM_ID)';
		END IF;
		IF OLD.DEVICE_CALL_DIM_ID != NEW.DEVICE_CALL_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID OR DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID)';
		END IF;
		IF OLD.CURRENCY_DIM_ID != NEW.CURRENCY_DIM_ID THEN
			lv_sql := lv_sql || 'CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CURRENCY_DIM_ID = $1.CURRENCY_DIM_ID OR CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID)';
		END IF;
		IF OLD.AUTH_TYPE_DIM_ID != NEW.AUTH_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_TYPE_DIM_ID = $1.AUTH_TYPE_DIM_ID OR AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID)';
		END IF;
		IF OLD.AUTH_DETAIL_DIM_ID != NEW.AUTH_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_DETAIL_DIM_ID = $1.AUTH_DETAIL_DIM_ID OR AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID)';
		END IF;
		IF OLD.CONSUMER_DIM_ID != NEW.CONSUMER_DIM_ID THEN
			lv_sql := lv_sql || 'CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CONSUMER_DIM_ID = $1.CONSUMER_DIM_ID OR CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID)';
		END IF;
		IF OLD.ITEM_TS != NEW.ITEM_TS THEN
			lv_sql := lv_sql || 'ITEM_TS = $2.ITEM_TS, ';
			lv_filter := lv_filter || ' AND (ITEM_TS = $1.ITEM_TS OR ITEM_TS = $2.ITEM_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ITEM_FACT_ID = $1.ITEM_FACT_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'ITEM_DATE_DIM_ID >= ' || RDW.PARTITION_START_DATE_DIM_ID(NEW.ITEM_DATE_DIM_ID, 'month') || ' AND ITEM_DATE_DIM_ID < ' || RDW.PARTITION_END_DATE_DIM_ID(NEW.ITEM_DATE_DIM_ID, 'month')||'), CONSTRAINT CK_' || lv_new_partition_table || '_1 CHECK(' ||'ITEM_TYPE_DIM_ID =' || NEW.ITEM_TYPE_DIM_ID||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_TRAN_LINE_ITEM_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(TRAN_LINE_ITEM_DIM_ID) REFERENCES RDW.TRAN_LINE_ITEM_DIM(TRAN_LINE_ITEM_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_FILL_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(FILL_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_FILL_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(FILL_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_AUTHORITY_TERMINAL_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTHORITY_TERMINAL_DIM_ID) REFERENCES RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TERMINAL_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_SETTLED_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SETTLED_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_SETTLED_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SETTLED_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_PAID_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_ITEM_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_ITEM_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_UPLOAD_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_UPLOAD_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_CURRENCY_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_TRAN_ITEM_FACT_AUTH_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID)) INHERITS(RDW._TRAN_ITEM_FACT) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_TRAN_ITEM_FACT', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ITEM_FACT_ID = $1.ITEM_FACT_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.TRAN_ITEM_FACT
	AS SELECT * FROM RDW._TRAN_ITEM_FACT;

ALTER VIEW RDW.TRAN_ITEM_FACT ALTER ITEM_FACT_ID SET DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass);
ALTER VIEW RDW.TRAN_ITEM_FACT ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.TRAN_ITEM_FACT ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.TRAN_ITEM_FACT ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.TRAN_ITEM_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.TRAN_ITEM_FACT TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._TRAN_ITEM_FACT FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_TRAN_ITEM_FACT ON RDW.TRAN_ITEM_FACT;
CREATE TRIGGER TRIIUD_TRAN_ITEM_FACT
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.TRAN_ITEM_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_TRAN_ITEM_FACT();


CREATE INDEX IX_TRAN_ITEM_FACT_ITEM_DETAIL_DIM_ID ON RDW._TRAN_ITEM_FACT(ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX AK_TRAN_ITEM_FACT ON RDW._TRAN_ITEM_FACT(SOURCE_TRAN_ID, SOURCE_TRAN_LINE_ITEM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_REPORT_TRAN_ID ON RDW._TRAN_ITEM_FACT(REPORT_TRAN_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_TRAN_LINE_ITEM_DIM_ID ON RDW._TRAN_ITEM_FACT(TRAN_LINE_ITEM_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_HOST_DIM_ID ON RDW._TRAN_ITEM_FACT(HOST_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_FILL_DIM_ID ON RDW._TRAN_ITEM_FACT(FILL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_FILL_DATE_DIM_ID ON RDW._TRAN_ITEM_FACT(FILL_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_FILL_TIME_DIM_ID ON RDW._TRAN_ITEM_FACT(FILL_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_AUTHORITY_TERMINAL_DIM_ID ON RDW._TRAN_ITEM_FACT(AUTHORITY_TERMINAL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_APPLY_TO_CONSUMER_DIM_ID ON RDW._TRAN_ITEM_FACT(APPLY_TO_CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_ORIG_TRAN_DIM_ID ON RDW._TRAN_ITEM_FACT(ORIG_TRAN_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_ORIG_AUTH_DETAIL_DIM_ID ON RDW._TRAN_ITEM_FACT(ORIG_AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_SETTLED_DATE_DIM_ID ON RDW._TRAN_ITEM_FACT(SETTLED_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_SETTLED_TIME_DIM_ID ON RDW._TRAN_ITEM_FACT(SETTLED_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_PAYMENT_DIM_ID ON RDW._TRAN_ITEM_FACT(PAYMENT_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_PAYMENT_BATCH_DIM_ID ON RDW._TRAN_ITEM_FACT(PAYMENT_BATCH_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_PAID_DATE_DIM_ID ON RDW._TRAN_ITEM_FACT(PAID_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_DEVICE_DIM_ID ON RDW._TRAN_ITEM_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_LOCATION_DIM_ID_ITEM_TS ON RDW._TRAN_ITEM_FACT(LOCATION_DIM_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_LOCATION_DIM_ID ON RDW._TRAN_ITEM_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_POS_DIM_ID ON RDW._TRAN_ITEM_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_ITEM_DATE_DIM_ID ON RDW._TRAN_ITEM_FACT(ITEM_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_ITEM_TIME_DIM_ID ON RDW._TRAN_ITEM_FACT(ITEM_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_UPLOAD_DATE_DIM_ID ON RDW._TRAN_ITEM_FACT(UPLOAD_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_UPLOAD_TIME_DIM_ID ON RDW._TRAN_ITEM_FACT(UPLOAD_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_DEVICE_CALL_DIM_ID ON RDW._TRAN_ITEM_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_AUTH_TYPE_DIM_ID ON RDW._TRAN_ITEM_FACT(AUTH_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_AUTH_DETAIL_DIM_ID ON RDW._TRAN_ITEM_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_CONSUMER_DIM_ID ON RDW._TRAN_ITEM_FACT(CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_TRAN_ITEM_FACT_CREATED_TS ON RDW._TRAN_ITEM_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_TRAN_ITEM_FACT(
	p_item_fact_id OUT BIGINT, 
	p_item_detail_dim_id BIGINT, 
	p_source_tran_line_item_id BIGINT, 
	p_source_tran_id BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_tran_line_item_dim_id BIGINT, 
	p_host_dim_id BIGINT, 
	p_fill_dim_id BIGINT, 
	p_fill_date_dim_id SMALLINT, 
	p_fill_time_dim_id SMALLINT, 
	p_authority_terminal_dim_id BIGINT, 
	p_apply_to_consumer_dim_id BIGINT, 
	p_orig_tran_dim_id BIGINT, 
	p_orig_auth_detail_dim_id BIGINT, 
	p_unit_price DECIMAL(12,4), 
	p_product_service_quantity INTEGER, 
	p_process_fee_amount DECIMAL(12,4), 
	p_duration_seconds_fraction DECIMAL(22,10), 
	p_tran_count_fraction DECIMAL(22,21), 
	p_convenience_fee_amount DECIMAL(12,4), 
	p_loyalty_discount_amount DECIMAL(12,4), 
	p_fill_ts TIMESTAMPTZ, 
	p_source_ledger_id BIGINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_FACT_ID, REVISION
			  INTO p_item_fact_id, p_old_revision
			  FROM RDW.TRAN_ITEM_FACT
			 WHERE ((SOURCE_TRAN_LINE_ITEM_ID IS NOT NULL AND SOURCE_TRAN_LINE_ITEM_ID = p_source_tran_line_item_id) OR (SOURCE_TRAN_LINE_ITEM_ID IS NULL AND p_source_tran_line_item_id IS NULL))
			   AND SOURCE_TRAN_ID = p_source_tran_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.TRAN_ITEM_FACT
			   SET ITEM_DETAIL_DIM_ID = p_item_detail_dim_id,
			       REPORT_TRAN_ID = p_report_tran_id,
			       SOURCE_SYSTEM_CD = p_source_system_cd,
			       TRAN_LINE_ITEM_DIM_ID = p_tran_line_item_dim_id,
			       HOST_DIM_ID = p_host_dim_id,
			       FILL_DIM_ID = p_fill_dim_id,
			       FILL_DATE_DIM_ID = p_fill_date_dim_id,
			       FILL_TIME_DIM_ID = p_fill_time_dim_id,
			       AUTHORITY_TERMINAL_DIM_ID = p_authority_terminal_dim_id,
			       APPLY_TO_CONSUMER_DIM_ID = p_apply_to_consumer_dim_id,
			       ORIG_TRAN_DIM_ID = p_orig_tran_dim_id,
			       ORIG_AUTH_DETAIL_DIM_ID = p_orig_auth_detail_dim_id,
			       UNIT_PRICE = p_unit_price,
			       PRODUCT_SERVICE_QUANTITY = p_product_service_quantity,
			       PROCESS_FEE_AMOUNT = p_process_fee_amount,
			       DURATION_SECONDS_FRACTION = p_duration_seconds_fraction,
			       TRAN_COUNT_FRACTION = p_tran_count_fraction,
			       CONVENIENCE_FEE_AMOUNT = p_convenience_fee_amount,
			       LOYALTY_DISCOUNT_AMOUNT = p_loyalty_discount_amount,
			       FILL_TS = p_fill_ts,
			       SOURCE_LEDGER_ID = p_source_ledger_id,
			       SETTLED_DATE_DIM_ID = p_settled_date_dim_id,
			       SETTLED_TIME_DIM_ID = p_settled_time_dim_id,
			       PAYMENT_DIM_ID = p_payment_dim_id,
			       PAYMENT_BATCH_DIM_ID = p_payment_batch_dim_id,
			       PAID_DATE_DIM_ID = p_paid_date_dim_id,
			       QUANTITY = p_quantity,
			       AMOUNT = p_amount,
			       PAID_TS = p_paid_ts,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       ITEM_DATE_DIM_ID = p_item_date_dim_id,
			       ITEM_TIME_DIM_ID = p_item_time_dim_id,
			       UPLOAD_DATE_DIM_ID = p_upload_date_dim_id,
			       UPLOAD_TIME_DIM_ID = p_upload_time_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       ITEM_TS = p_item_ts,
			       REVISION = p_revision
			 WHERE ((SOURCE_TRAN_LINE_ITEM_ID IS NOT NULL AND SOURCE_TRAN_LINE_ITEM_ID = p_source_tran_line_item_id) OR (SOURCE_TRAN_LINE_ITEM_ID IS NULL AND p_source_tran_line_item_id IS NULL))
			   AND SOURCE_TRAN_ID = p_source_tran_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.TRAN_ITEM_FACT(SOURCE_TRAN_LINE_ITEM_ID, SOURCE_TRAN_ID, ITEM_DETAIL_DIM_ID, REPORT_TRAN_ID, SOURCE_SYSTEM_CD, TRAN_LINE_ITEM_DIM_ID, HOST_DIM_ID, FILL_DIM_ID, FILL_DATE_DIM_ID, FILL_TIME_DIM_ID, AUTHORITY_TERMINAL_DIM_ID, APPLY_TO_CONSUMER_DIM_ID, ORIG_TRAN_DIM_ID, ORIG_AUTH_DETAIL_DIM_ID, UNIT_PRICE, PRODUCT_SERVICE_QUANTITY, PROCESS_FEE_AMOUNT, DURATION_SECONDS_FRACTION, TRAN_COUNT_FRACTION, CONVENIENCE_FEE_AMOUNT, LOYALTY_DISCOUNT_AMOUNT, FILL_TS, SOURCE_LEDGER_ID, SETTLED_DATE_DIM_ID, SETTLED_TIME_DIM_ID, PAYMENT_DIM_ID, PAYMENT_BATCH_DIM_ID, PAID_DATE_DIM_ID, QUANTITY, AMOUNT, PAID_TS, ITEM_TYPE_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, ITEM_DATE_DIM_ID, ITEM_TIME_DIM_ID, UPLOAD_DATE_DIM_ID, UPLOAD_TIME_DIM_ID, DEVICE_CALL_DIM_ID, CURRENCY_DIM_ID, AUTH_TYPE_DIM_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, ITEM_TS, REVISION)
					 VALUES(p_source_tran_line_item_id, p_source_tran_id, p_item_detail_dim_id, p_report_tran_id, p_source_system_cd, p_tran_line_item_dim_id, p_host_dim_id, p_fill_dim_id, p_fill_date_dim_id, p_fill_time_dim_id, p_authority_terminal_dim_id, p_apply_to_consumer_dim_id, p_orig_tran_dim_id, p_orig_auth_detail_dim_id, p_unit_price, p_product_service_quantity, p_process_fee_amount, p_duration_seconds_fraction, p_tran_count_fraction, p_convenience_fee_amount, p_loyalty_discount_amount, p_fill_ts, p_source_ledger_id, p_settled_date_dim_id, p_settled_time_dim_id, p_payment_dim_id, p_payment_batch_dim_id, p_paid_date_dim_id, p_quantity, p_amount, p_paid_ts, p_item_type_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_item_date_dim_id, p_item_time_dim_id, p_upload_date_dim_id, p_upload_time_dim_id, p_device_call_dim_id, p_currency_dim_id, p_auth_type_dim_id, p_auth_detail_dim_id, p_consumer_dim_id, p_item_ts, p_revision)
					 RETURNING ITEM_FACT_ID
					      INTO p_item_fact_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_TRAN_ITEM_FACT(
	p_item_fact_id OUT BIGINT, 
	p_item_detail_dim_id BIGINT, 
	p_source_tran_line_item_id BIGINT, 
	p_source_tran_id BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_tran_line_item_dim_id BIGINT, 
	p_host_dim_id BIGINT, 
	p_fill_dim_id BIGINT, 
	p_fill_date_dim_id SMALLINT, 
	p_fill_time_dim_id SMALLINT, 
	p_authority_terminal_dim_id BIGINT, 
	p_apply_to_consumer_dim_id BIGINT, 
	p_orig_tran_dim_id BIGINT, 
	p_orig_auth_detail_dim_id BIGINT, 
	p_unit_price DECIMAL(12,4), 
	p_product_service_quantity INTEGER, 
	p_process_fee_amount DECIMAL(12,4), 
	p_duration_seconds_fraction DECIMAL(22,10), 
	p_tran_count_fraction DECIMAL(22,21), 
	p_convenience_fee_amount DECIMAL(12,4), 
	p_loyalty_discount_amount DECIMAL(12,4), 
	p_fill_ts TIMESTAMPTZ, 
	p_source_ledger_id BIGINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._LEDGER_ITEM_FACT
  (
    ITEM_FACT_ID              BIGINT      NOT NULL DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass),
    ITEM_DETAIL_DIM_ID        BIGINT      NOT NULL,
    SOURCE_LEDGER_ID          BIGINT      NOT NULL,
    DELETED_TS                TIMESTAMPTZ ,
    TRAN_ITEM_DETAIL_DIM_ID   BIGINT      NOT NULL,
    AUTHORITY_TERMINAL_DIM_ID BIGINT      NOT NULL,
    ACCOUNTING_DATE_DIM_ID    SMALLINT    ,

    CONSTRAINT PK_LEDGER_ITEM_FACT PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_LEDGER_ITEM_FACT_ITEM_DETAIL_DIM_ID FOREIGN KEY(ITEM_DETAIL_DIM_ID) REFERENCES RDW._LEDGER_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_TRAN_ITEM_DETAIL_DIM_ID FOREIGN KEY(TRAN_ITEM_DETAIL_DIM_ID) REFERENCES RDW._TRAN_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_AUTHORITY_TERMINAL_DIM_ID FOREIGN KEY(AUTHORITY_TERMINAL_DIM_ID) REFERENCES RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TERMINAL_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_ACCOUNTING_DATE_DIM_ID FOREIGN KEY(ACCOUNTING_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_SETTLED_DATE_DIM_ID FOREIGN KEY(SETTLED_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_SETTLED_TIME_DIM_ID FOREIGN KEY(SETTLED_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_PAYMENT_DIM_ID FOREIGN KEY(PAYMENT_DIM_ID) REFERENCES RDW._PAYMENT_DIM(PAYMENT_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_PAYMENT_BATCH_DIM_ID FOREIGN KEY(PAYMENT_BATCH_DIM_ID) REFERENCES RDW._PAYMENT_BATCH_DIM(PAYMENT_BATCH_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_PAID_DATE_DIM_ID FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_ITEM_DATE_DIM_ID FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_ITEM_TIME_DIM_ID FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_UPLOAD_DATE_DIM_ID FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_UPLOAD_TIME_DIM_ID FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_AUTH_TYPE_DIM_ID FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_AUTH_DETAIL_DIM_ID FOREIGN KEY(AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_LEDGER_ITEM_FACT_CONSUMER_DIM_ID FOREIGN KEY(CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID)
  )
  INHERITS(RDW.PAYMENT_ITEM_FACT)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_LEDGER_ITEM_FACT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(CASE WHEN NEW.PAID_DATE_DIM_ID = 0 THEN 'PENDING' ELSE TO_CHAR(DATE'epoch' + NEW.PAID_DATE_DIM_ID, 'YYYY_MM') END,'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_LEDGER_ITEM_FACT', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_LEDGER_ITEM_FACT', COALESCE(CASE WHEN OLD.PAID_DATE_DIM_ID = 0 THEN 'PENDING' ELSE TO_CHAR(DATE'epoch' + OLD.PAID_DATE_DIM_ID, 'YYYY_MM') END, '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ITEM_FACT_ID != NEW.ITEM_FACT_ID THEN
			lv_sql := lv_sql || 'ITEM_FACT_ID = $2.ITEM_FACT_ID, ';
		END IF;
		IF OLD.SOURCE_LEDGER_ID != NEW.SOURCE_LEDGER_ID THEN
			lv_sql := lv_sql || 'SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_LEDGER_ID = $1.SOURCE_LEDGER_ID OR SOURCE_LEDGER_ID = $2.SOURCE_LEDGER_ID)';
		END IF;
		IF OLD.ITEM_DETAIL_DIM_ID != NEW.ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID OR ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID)';
		END IF;
		IF OLD.DELETED_TS IS DISTINCT FROM NEW.DELETED_TS THEN
			lv_sql := lv_sql || 'DELETED_TS = $2.DELETED_TS, ';
			lv_filter := lv_filter || ' AND (DELETED_TS IS NOT DISTINCT FROM $1.DELETED_TS OR DELETED_TS IS NOT DISTINCT FROM $2.DELETED_TS)';
		END IF;
		IF OLD.TRAN_ITEM_DETAIL_DIM_ID != NEW.TRAN_ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'TRAN_ITEM_DETAIL_DIM_ID = $2.TRAN_ITEM_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (TRAN_ITEM_DETAIL_DIM_ID = $1.TRAN_ITEM_DETAIL_DIM_ID OR TRAN_ITEM_DETAIL_DIM_ID = $2.TRAN_ITEM_DETAIL_DIM_ID)';
		END IF;
		IF OLD.AUTHORITY_TERMINAL_DIM_ID != NEW.AUTHORITY_TERMINAL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTHORITY_TERMINAL_DIM_ID = $2.AUTHORITY_TERMINAL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTHORITY_TERMINAL_DIM_ID = $1.AUTHORITY_TERMINAL_DIM_ID OR AUTHORITY_TERMINAL_DIM_ID = $2.AUTHORITY_TERMINAL_DIM_ID)';
		END IF;
		IF OLD.ACCOUNTING_DATE_DIM_ID IS DISTINCT FROM NEW.ACCOUNTING_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'ACCOUNTING_DATE_DIM_ID = $2.ACCOUNTING_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ACCOUNTING_DATE_DIM_ID IS NOT DISTINCT FROM $1.ACCOUNTING_DATE_DIM_ID OR ACCOUNTING_DATE_DIM_ID IS NOT DISTINCT FROM $2.ACCOUNTING_DATE_DIM_ID)';
		END IF;
		IF OLD.SETTLED_DATE_DIM_ID != NEW.SETTLED_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'SETTLED_DATE_DIM_ID = $2.SETTLED_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SETTLED_DATE_DIM_ID = $1.SETTLED_DATE_DIM_ID OR SETTLED_DATE_DIM_ID = $2.SETTLED_DATE_DIM_ID)';
		END IF;
		IF OLD.SETTLED_TIME_DIM_ID != NEW.SETTLED_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'SETTLED_TIME_DIM_ID = $2.SETTLED_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (SETTLED_TIME_DIM_ID = $1.SETTLED_TIME_DIM_ID OR SETTLED_TIME_DIM_ID = $2.SETTLED_TIME_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_DIM_ID != NEW.PAYMENT_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_DIM_ID = $1.PAYMENT_DIM_ID OR PAYMENT_DIM_ID = $2.PAYMENT_DIM_ID)';
		END IF;
		IF OLD.PAYMENT_BATCH_DIM_ID != NEW.PAYMENT_BATCH_DIM_ID THEN
			lv_sql := lv_sql || 'PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAYMENT_BATCH_DIM_ID = $1.PAYMENT_BATCH_DIM_ID OR PAYMENT_BATCH_DIM_ID = $2.PAYMENT_BATCH_DIM_ID)';
		END IF;
		IF OLD.PAID_DATE_DIM_ID != NEW.PAID_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'PAID_DATE_DIM_ID = $2.PAID_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PAID_DATE_DIM_ID = $1.PAID_DATE_DIM_ID OR PAID_DATE_DIM_ID = $2.PAID_DATE_DIM_ID)';
		END IF;
		IF OLD.QUANTITY IS DISTINCT FROM NEW.QUANTITY THEN
			lv_sql := lv_sql || 'QUANTITY = $2.QUANTITY, ';
			lv_filter := lv_filter || ' AND (QUANTITY IS NOT DISTINCT FROM $1.QUANTITY OR QUANTITY IS NOT DISTINCT FROM $2.QUANTITY)';
		END IF;
		IF OLD.AMOUNT IS DISTINCT FROM NEW.AMOUNT THEN
			lv_sql := lv_sql || 'AMOUNT = $2.AMOUNT, ';
			lv_filter := lv_filter || ' AND (AMOUNT IS NOT DISTINCT FROM $1.AMOUNT OR AMOUNT IS NOT DISTINCT FROM $2.AMOUNT)';
		END IF;
		IF OLD.PAID_TS IS DISTINCT FROM NEW.PAID_TS THEN
			lv_sql := lv_sql || 'PAID_TS = $2.PAID_TS, ';
			lv_filter := lv_filter || ' AND (PAID_TS IS NOT DISTINCT FROM $1.PAID_TS OR PAID_TS IS NOT DISTINCT FROM $2.PAID_TS)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
			lv_filter := lv_filter || ' AND (POS_DIM_ID = $1.POS_DIM_ID OR POS_DIM_ID = $2.POS_DIM_ID)';
		END IF;
		IF OLD.ITEM_DATE_DIM_ID != NEW.ITEM_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DATE_DIM_ID = $2.ITEM_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_DATE_DIM_ID = $1.ITEM_DATE_DIM_ID OR ITEM_DATE_DIM_ID = $2.ITEM_DATE_DIM_ID)';
		END IF;
		IF OLD.ITEM_TIME_DIM_ID != NEW.ITEM_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TIME_DIM_ID = $2.ITEM_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TIME_DIM_ID = $1.ITEM_TIME_DIM_ID OR ITEM_TIME_DIM_ID = $2.ITEM_TIME_DIM_ID)';
		END IF;
		IF OLD.UPLOAD_DATE_DIM_ID != NEW.UPLOAD_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'UPLOAD_DATE_DIM_ID = $2.UPLOAD_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (UPLOAD_DATE_DIM_ID = $1.UPLOAD_DATE_DIM_ID OR UPLOAD_DATE_DIM_ID = $2.UPLOAD_DATE_DIM_ID)';
		END IF;
		IF OLD.UPLOAD_TIME_DIM_ID != NEW.UPLOAD_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'UPLOAD_TIME_DIM_ID = $2.UPLOAD_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (UPLOAD_TIME_DIM_ID = $1.UPLOAD_TIME_DIM_ID OR UPLOAD_TIME_DIM_ID = $2.UPLOAD_TIME_DIM_ID)';
		END IF;
		IF OLD.DEVICE_CALL_DIM_ID != NEW.DEVICE_CALL_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID OR DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID)';
		END IF;
		IF OLD.CURRENCY_DIM_ID != NEW.CURRENCY_DIM_ID THEN
			lv_sql := lv_sql || 'CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CURRENCY_DIM_ID = $1.CURRENCY_DIM_ID OR CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID)';
		END IF;
		IF OLD.AUTH_TYPE_DIM_ID != NEW.AUTH_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_TYPE_DIM_ID = $1.AUTH_TYPE_DIM_ID OR AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID)';
		END IF;
		IF OLD.AUTH_DETAIL_DIM_ID != NEW.AUTH_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_DETAIL_DIM_ID = $1.AUTH_DETAIL_DIM_ID OR AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID)';
		END IF;
		IF OLD.CONSUMER_DIM_ID != NEW.CONSUMER_DIM_ID THEN
			lv_sql := lv_sql || 'CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CONSUMER_DIM_ID = $1.CONSUMER_DIM_ID OR CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID)';
		END IF;
		IF OLD.ITEM_TS != NEW.ITEM_TS THEN
			lv_sql := lv_sql || 'ITEM_TS = $2.ITEM_TS, ';
			lv_filter := lv_filter || ' AND (ITEM_TS = $1.ITEM_TS OR ITEM_TS = $2.ITEM_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ITEM_FACT_ID = $1.ITEM_FACT_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||CASE WHEN NEW.PAID_DATE_DIM_ID = 0 THEN 'PAID_DATE_DIM_ID = 0' ELSE 'PAID_DATE_DIM_ID >= ' || RDW.PARTITION_START_DATE_DIM_ID(NEW.PAID_DATE_DIM_ID, 'month') || ' AND PAID_DATE_DIM_ID < ' || RDW.PARTITION_END_DATE_DIM_ID(NEW.PAID_DATE_DIM_ID, 'month') END||'), CONSTRAINT CK_' || lv_new_partition_table || '_1 CHECK(' ||'ITEM_TYPE_DIM_ID IN(5,6,7,8,9,10,11,12)'||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_AUTHORITY_TERMINAL_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTHORITY_TERMINAL_DIM_ID) REFERENCES RDW.AUTHORITY_TERMINAL_DIM(AUTHORITY_TERMINAL_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_ACCOUNTING_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ACCOUNTING_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_SETTLED_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SETTLED_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_SETTLED_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(SETTLED_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_PAID_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(PAID_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_ITEM_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_ITEM_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_UPLOAD_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_UPLOAD_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_CURRENCY_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_LEDGER_ITEM_FACT_AUTH_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID)) INHERITS(RDW._LEDGER_ITEM_FACT) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_LEDGER_ITEM_FACT', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ITEM_FACT_ID = $1.ITEM_FACT_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.LEDGER_ITEM_FACT
	AS SELECT * FROM RDW._LEDGER_ITEM_FACT;

ALTER VIEW RDW.LEDGER_ITEM_FACT ALTER ITEM_FACT_ID SET DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass);
ALTER VIEW RDW.LEDGER_ITEM_FACT ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.LEDGER_ITEM_FACT ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.LEDGER_ITEM_FACT ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.LEDGER_ITEM_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.LEDGER_ITEM_FACT TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._LEDGER_ITEM_FACT FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_LEDGER_ITEM_FACT ON RDW.LEDGER_ITEM_FACT;
CREATE TRIGGER TRIIUD_LEDGER_ITEM_FACT
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.LEDGER_ITEM_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_LEDGER_ITEM_FACT();


CREATE INDEX IX_LEDGER_ITEM_FACT_ITEM_DETAIL_DIM_ID ON RDW._LEDGER_ITEM_FACT(ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE UNIQUE INDEX AK_LEDGER_ITEM_FACT ON RDW._LEDGER_ITEM_FACT(SOURCE_LEDGER_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_DELETED_TS ON RDW._LEDGER_ITEM_FACT(DELETED_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_TRAN_ITEM_DETAIL_DIM_ID ON RDW._LEDGER_ITEM_FACT(TRAN_ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_AUTHORITY_TERMINAL_DIM_ID ON RDW._LEDGER_ITEM_FACT(AUTHORITY_TERMINAL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_ACCOUNTING_DATE_DIM_ID ON RDW._LEDGER_ITEM_FACT(ACCOUNTING_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_SETTLED_DATE_DIM_ID ON RDW._LEDGER_ITEM_FACT(SETTLED_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_SETTLED_TIME_DIM_ID ON RDW._LEDGER_ITEM_FACT(SETTLED_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_PAYMENT_DIM_ID ON RDW._LEDGER_ITEM_FACT(PAYMENT_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_PAYMENT_BATCH_DIM_ID ON RDW._LEDGER_ITEM_FACT(PAYMENT_BATCH_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_PAID_DATE_DIM_ID ON RDW._LEDGER_ITEM_FACT(PAID_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_DEVICE_DIM_ID ON RDW._LEDGER_ITEM_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_LOCATION_DIM_ID_ITEM_TS ON RDW._LEDGER_ITEM_FACT(LOCATION_DIM_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_LOCATION_DIM_ID ON RDW._LEDGER_ITEM_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_POS_DIM_ID ON RDW._LEDGER_ITEM_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_ITEM_DATE_DIM_ID ON RDW._LEDGER_ITEM_FACT(ITEM_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_ITEM_TIME_DIM_ID ON RDW._LEDGER_ITEM_FACT(ITEM_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_UPLOAD_DATE_DIM_ID ON RDW._LEDGER_ITEM_FACT(UPLOAD_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_UPLOAD_TIME_DIM_ID ON RDW._LEDGER_ITEM_FACT(UPLOAD_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_DEVICE_CALL_DIM_ID ON RDW._LEDGER_ITEM_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_AUTH_TYPE_DIM_ID ON RDW._LEDGER_ITEM_FACT(AUTH_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_AUTH_DETAIL_DIM_ID ON RDW._LEDGER_ITEM_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_CONSUMER_DIM_ID ON RDW._LEDGER_ITEM_FACT(CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_LEDGER_ITEM_FACT_CREATED_TS ON RDW._LEDGER_ITEM_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_LEDGER_ITEM_FACT(
	p_item_fact_id OUT BIGINT, 
	p_item_detail_dim_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_deleted_ts TIMESTAMPTZ, 
	p_tran_item_detail_dim_id BIGINT, 
	p_authority_terminal_dim_id BIGINT, 
	p_accounting_date_dim_id SMALLINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_FACT_ID, REVISION
			  INTO p_item_fact_id, p_old_revision
			  FROM RDW.LEDGER_ITEM_FACT
			 WHERE SOURCE_LEDGER_ID = p_source_ledger_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.LEDGER_ITEM_FACT
			   SET ITEM_DETAIL_DIM_ID = p_item_detail_dim_id,
			       DELETED_TS = p_deleted_ts,
			       TRAN_ITEM_DETAIL_DIM_ID = p_tran_item_detail_dim_id,
			       AUTHORITY_TERMINAL_DIM_ID = p_authority_terminal_dim_id,
			       ACCOUNTING_DATE_DIM_ID = p_accounting_date_dim_id,
			       SETTLED_DATE_DIM_ID = p_settled_date_dim_id,
			       SETTLED_TIME_DIM_ID = p_settled_time_dim_id,
			       PAYMENT_DIM_ID = p_payment_dim_id,
			       PAYMENT_BATCH_DIM_ID = p_payment_batch_dim_id,
			       PAID_DATE_DIM_ID = p_paid_date_dim_id,
			       QUANTITY = p_quantity,
			       AMOUNT = p_amount,
			       PAID_TS = p_paid_ts,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       ITEM_DATE_DIM_ID = p_item_date_dim_id,
			       ITEM_TIME_DIM_ID = p_item_time_dim_id,
			       UPLOAD_DATE_DIM_ID = p_upload_date_dim_id,
			       UPLOAD_TIME_DIM_ID = p_upload_time_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       ITEM_TS = p_item_ts,
			       REVISION = p_revision
			 WHERE SOURCE_LEDGER_ID = p_source_ledger_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.LEDGER_ITEM_FACT(SOURCE_LEDGER_ID, ITEM_DETAIL_DIM_ID, DELETED_TS, TRAN_ITEM_DETAIL_DIM_ID, AUTHORITY_TERMINAL_DIM_ID, ACCOUNTING_DATE_DIM_ID, SETTLED_DATE_DIM_ID, SETTLED_TIME_DIM_ID, PAYMENT_DIM_ID, PAYMENT_BATCH_DIM_ID, PAID_DATE_DIM_ID, QUANTITY, AMOUNT, PAID_TS, ITEM_TYPE_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, ITEM_DATE_DIM_ID, ITEM_TIME_DIM_ID, UPLOAD_DATE_DIM_ID, UPLOAD_TIME_DIM_ID, DEVICE_CALL_DIM_ID, CURRENCY_DIM_ID, AUTH_TYPE_DIM_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, ITEM_TS, REVISION)
					 VALUES(p_source_ledger_id, p_item_detail_dim_id, p_deleted_ts, p_tran_item_detail_dim_id, p_authority_terminal_dim_id, p_accounting_date_dim_id, p_settled_date_dim_id, p_settled_time_dim_id, p_payment_dim_id, p_payment_batch_dim_id, p_paid_date_dim_id, p_quantity, p_amount, p_paid_ts, p_item_type_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_item_date_dim_id, p_item_time_dim_id, p_upload_date_dim_id, p_upload_time_dim_id, p_device_call_dim_id, p_currency_dim_id, p_auth_type_dim_id, p_auth_detail_dim_id, p_consumer_dim_id, p_item_ts, p_revision)
					 RETURNING ITEM_FACT_ID
					      INTO p_item_fact_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_LEDGER_ITEM_FACT(
	p_item_fact_id OUT BIGINT, 
	p_item_detail_dim_id BIGINT, 
	p_source_ledger_id BIGINT, 
	p_deleted_ts TIMESTAMPTZ, 
	p_tran_item_detail_dim_id BIGINT, 
	p_authority_terminal_dim_id BIGINT, 
	p_accounting_date_dim_id SMALLINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

CREATE TABLE RDW._FILL_FACT
  (
    ITEM_FACT_ID       BIGINT        NOT NULL DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass),
    SOURCE_FILL_ID     BIGINT        NOT NULL,
    SOURCE_SYSTEM_CD   VARCHAR(30)   NOT NULL,
    ITEM_DETAIL_DIM_ID BIGINT        NOT NULL,
    PREV_FILL_DIM_ID   BIGINT        NOT NULL,
    HOST_DIM_ID        BIGINT        NOT NULL,
    CASH_AMOUNT        DECIMAL(12,4) ,
    CREDIT_AMOUNT      DECIMAL(12,4) ,
    OTHERCARD_AMOUNT   DECIMAL(12,4) ,
    CASH_COUNT         BIGINT        ,
    CREDIT_COUNT       BIGINT        ,
    OTHERCARD_COUNT    BIGINT        ,

    CONSTRAINT PK_FILL_FACT PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1,
    CONSTRAINT FK_FILL_FACT_ITEM_DETAIL_DIM_ID FOREIGN KEY(ITEM_DETAIL_DIM_ID) REFERENCES RDW._FILL_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_FILL_FACT_PREV_FILL_DIM_ID FOREIGN KEY(PREV_FILL_DIM_ID) REFERENCES RDW._FILL_ITEM_DETAIL_DIM(ITEM_DETAIL_DIM_ID),
    CONSTRAINT FK_FILL_FACT_HOST_DIM_ID FOREIGN KEY(HOST_DIM_ID) REFERENCES RDW._HOST_DIM(HOST_DIM_ID),
    CONSTRAINT FK_FILL_FACT_ITEM_TYPE_DIM_ID FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID),
    CONSTRAINT FK_FILL_FACT_DEVICE_DIM_ID FOREIGN KEY(DEVICE_DIM_ID) REFERENCES RDW._DEVICE_DIM(DEVICE_DIM_ID),
    CONSTRAINT FK_FILL_FACT_LOCATION_DIM_ID FOREIGN KEY(LOCATION_DIM_ID) REFERENCES RDW._LOCATION_DIM(LOCATION_DIM_ID),
    CONSTRAINT FK_FILL_FACT_POS_DIM_ID FOREIGN KEY(POS_DIM_ID) REFERENCES RDW._POS_DIM(POS_DIM_ID),
    CONSTRAINT FK_FILL_FACT_ITEM_DATE_DIM_ID FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_FILL_FACT_ITEM_TIME_DIM_ID FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_FILL_FACT_UPLOAD_DATE_DIM_ID FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID),
    CONSTRAINT FK_FILL_FACT_UPLOAD_TIME_DIM_ID FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID),
    CONSTRAINT FK_FILL_FACT_DEVICE_CALL_DIM_ID FOREIGN KEY(DEVICE_CALL_DIM_ID) REFERENCES RDW._DEVICE_CALL_DIM(DEVICE_CALL_DIM_ID),
    CONSTRAINT FK_FILL_FACT_CURRENCY_DIM_ID FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID),
    CONSTRAINT FK_FILL_FACT_AUTH_TYPE_DIM_ID FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID),
    CONSTRAINT FK_FILL_FACT_AUTH_DETAIL_DIM_ID FOREIGN KEY(AUTH_DETAIL_DIM_ID) REFERENCES RDW._AUTH_DETAIL_DIM(AUTH_DETAIL_DIM_ID),
    CONSTRAINT FK_FILL_FACT_CONSUMER_DIM_ID FOREIGN KEY(CONSUMER_DIM_ID) REFERENCES RDW._CONSUMER_DIM(CONSUMER_DIM_ID)
  )
  INHERITS(RDW.ITEM_FACT)
  TABLESPACE rdw_data_t1;

CREATE OR REPLACE FUNCTION RDW.FRIIUD_FILL_FACT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_TS := STATEMENT_TIMESTAMP();
			NEW.CREATED_BY := SESSION_USER;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_TS := OLD.CREATED_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_TS := STATEMENT_TIMESTAMP();
			NEW.LAST_UPDATED_BY := SESSION_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(CAST('epoch' AS DATE) + NEW.ITEM_DATE_DIM_ID, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT RDW.CONSTRUCT_NAME('_FILL_FACT', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT RDW.CONSTRUCT_NAME('_FILL_FACT', COALESCE(TO_CHAR(CAST('epoch' AS DATE) + OLD.ITEM_DATE_DIM_ID, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE RDW.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.ITEM_FACT_ID != NEW.ITEM_FACT_ID THEN
			lv_sql := lv_sql || 'ITEM_FACT_ID = $2.ITEM_FACT_ID, ';
		END IF;
		IF OLD.SOURCE_FILL_ID != NEW.SOURCE_FILL_ID THEN
			lv_sql := lv_sql || 'SOURCE_FILL_ID = $2.SOURCE_FILL_ID, ';
			lv_filter := lv_filter || ' AND (SOURCE_FILL_ID = $1.SOURCE_FILL_ID OR SOURCE_FILL_ID = $2.SOURCE_FILL_ID)';
		END IF;
		IF OLD.SOURCE_SYSTEM_CD != NEW.SOURCE_SYSTEM_CD THEN
			lv_sql := lv_sql || 'SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD, ';
			lv_filter := lv_filter || ' AND (SOURCE_SYSTEM_CD = $1.SOURCE_SYSTEM_CD OR SOURCE_SYSTEM_CD = $2.SOURCE_SYSTEM_CD)';
		END IF;
		IF OLD.ITEM_DETAIL_DIM_ID != NEW.ITEM_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_DETAIL_DIM_ID = $1.ITEM_DETAIL_DIM_ID OR ITEM_DETAIL_DIM_ID = $2.ITEM_DETAIL_DIM_ID)';
		END IF;
		IF OLD.PREV_FILL_DIM_ID != NEW.PREV_FILL_DIM_ID THEN
			lv_sql := lv_sql || 'PREV_FILL_DIM_ID = $2.PREV_FILL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (PREV_FILL_DIM_ID = $1.PREV_FILL_DIM_ID OR PREV_FILL_DIM_ID = $2.PREV_FILL_DIM_ID)';
		END IF;
		IF OLD.HOST_DIM_ID != NEW.HOST_DIM_ID THEN
			lv_sql := lv_sql || 'HOST_DIM_ID = $2.HOST_DIM_ID, ';
			lv_filter := lv_filter || ' AND (HOST_DIM_ID = $1.HOST_DIM_ID OR HOST_DIM_ID = $2.HOST_DIM_ID)';
		END IF;
		IF OLD.CASH_AMOUNT IS DISTINCT FROM NEW.CASH_AMOUNT THEN
			lv_sql := lv_sql || 'CASH_AMOUNT = $2.CASH_AMOUNT, ';
			lv_filter := lv_filter || ' AND (CASH_AMOUNT IS NOT DISTINCT FROM $1.CASH_AMOUNT OR CASH_AMOUNT IS NOT DISTINCT FROM $2.CASH_AMOUNT)';
		END IF;
		IF OLD.CREDIT_AMOUNT IS DISTINCT FROM NEW.CREDIT_AMOUNT THEN
			lv_sql := lv_sql || 'CREDIT_AMOUNT = $2.CREDIT_AMOUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_AMOUNT IS NOT DISTINCT FROM $1.CREDIT_AMOUNT OR CREDIT_AMOUNT IS NOT DISTINCT FROM $2.CREDIT_AMOUNT)';
		END IF;
		IF OLD.OTHERCARD_AMOUNT IS DISTINCT FROM NEW.OTHERCARD_AMOUNT THEN
			lv_sql := lv_sql || 'OTHERCARD_AMOUNT = $2.OTHERCARD_AMOUNT, ';
			lv_filter := lv_filter || ' AND (OTHERCARD_AMOUNT IS NOT DISTINCT FROM $1.OTHERCARD_AMOUNT OR OTHERCARD_AMOUNT IS NOT DISTINCT FROM $2.OTHERCARD_AMOUNT)';
		END IF;
		IF OLD.CASH_COUNT IS DISTINCT FROM NEW.CASH_COUNT THEN
			lv_sql := lv_sql || 'CASH_COUNT = $2.CASH_COUNT, ';
			lv_filter := lv_filter || ' AND (CASH_COUNT IS NOT DISTINCT FROM $1.CASH_COUNT OR CASH_COUNT IS NOT DISTINCT FROM $2.CASH_COUNT)';
		END IF;
		IF OLD.CREDIT_COUNT IS DISTINCT FROM NEW.CREDIT_COUNT THEN
			lv_sql := lv_sql || 'CREDIT_COUNT = $2.CREDIT_COUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_COUNT IS NOT DISTINCT FROM $1.CREDIT_COUNT OR CREDIT_COUNT IS NOT DISTINCT FROM $2.CREDIT_COUNT)';
		END IF;
		IF OLD.OTHERCARD_COUNT IS DISTINCT FROM NEW.OTHERCARD_COUNT THEN
			lv_sql := lv_sql || 'OTHERCARD_COUNT = $2.OTHERCARD_COUNT, ';
			lv_filter := lv_filter || ' AND (OTHERCARD_COUNT IS NOT DISTINCT FROM $1.OTHERCARD_COUNT OR OTHERCARD_COUNT IS NOT DISTINCT FROM $2.OTHERCARD_COUNT)';
		END IF;
		IF OLD.ITEM_TYPE_DIM_ID != NEW.ITEM_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TYPE_DIM_ID = $1.ITEM_TYPE_DIM_ID OR ITEM_TYPE_DIM_ID = $2.ITEM_TYPE_DIM_ID)';
		END IF;
		IF OLD.DEVICE_DIM_ID != NEW.DEVICE_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_DIM_ID = $2.DEVICE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_DIM_ID = $1.DEVICE_DIM_ID OR DEVICE_DIM_ID = $2.DEVICE_DIM_ID)';
		END IF;
		IF OLD.LOCATION_DIM_ID != NEW.LOCATION_DIM_ID THEN
			lv_sql := lv_sql || 'LOCATION_DIM_ID = $2.LOCATION_DIM_ID, ';
			lv_filter := lv_filter || ' AND (LOCATION_DIM_ID = $1.LOCATION_DIM_ID OR LOCATION_DIM_ID = $2.LOCATION_DIM_ID)';
		END IF;
		IF OLD.POS_DIM_ID != NEW.POS_DIM_ID THEN
			lv_sql := lv_sql || 'POS_DIM_ID = $2.POS_DIM_ID, ';
			lv_filter := lv_filter || ' AND (POS_DIM_ID = $1.POS_DIM_ID OR POS_DIM_ID = $2.POS_DIM_ID)';
		END IF;
		IF OLD.ITEM_DATE_DIM_ID != NEW.ITEM_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_DATE_DIM_ID = $2.ITEM_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_DATE_DIM_ID = $1.ITEM_DATE_DIM_ID OR ITEM_DATE_DIM_ID = $2.ITEM_DATE_DIM_ID)';
		END IF;
		IF OLD.ITEM_TIME_DIM_ID != NEW.ITEM_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'ITEM_TIME_DIM_ID = $2.ITEM_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (ITEM_TIME_DIM_ID = $1.ITEM_TIME_DIM_ID OR ITEM_TIME_DIM_ID = $2.ITEM_TIME_DIM_ID)';
		END IF;
		IF OLD.UPLOAD_DATE_DIM_ID != NEW.UPLOAD_DATE_DIM_ID THEN
			lv_sql := lv_sql || 'UPLOAD_DATE_DIM_ID = $2.UPLOAD_DATE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (UPLOAD_DATE_DIM_ID = $1.UPLOAD_DATE_DIM_ID OR UPLOAD_DATE_DIM_ID = $2.UPLOAD_DATE_DIM_ID)';
		END IF;
		IF OLD.UPLOAD_TIME_DIM_ID != NEW.UPLOAD_TIME_DIM_ID THEN
			lv_sql := lv_sql || 'UPLOAD_TIME_DIM_ID = $2.UPLOAD_TIME_DIM_ID, ';
			lv_filter := lv_filter || ' AND (UPLOAD_TIME_DIM_ID = $1.UPLOAD_TIME_DIM_ID OR UPLOAD_TIME_DIM_ID = $2.UPLOAD_TIME_DIM_ID)';
		END IF;
		IF OLD.DEVICE_CALL_DIM_ID != NEW.DEVICE_CALL_DIM_ID THEN
			lv_sql := lv_sql || 'DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_CALL_DIM_ID = $1.DEVICE_CALL_DIM_ID OR DEVICE_CALL_DIM_ID = $2.DEVICE_CALL_DIM_ID)';
		END IF;
		IF OLD.CURRENCY_DIM_ID != NEW.CURRENCY_DIM_ID THEN
			lv_sql := lv_sql || 'CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CURRENCY_DIM_ID = $1.CURRENCY_DIM_ID OR CURRENCY_DIM_ID = $2.CURRENCY_DIM_ID)';
		END IF;
		IF OLD.AUTH_TYPE_DIM_ID != NEW.AUTH_TYPE_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_TYPE_DIM_ID = $1.AUTH_TYPE_DIM_ID OR AUTH_TYPE_DIM_ID = $2.AUTH_TYPE_DIM_ID)';
		END IF;
		IF OLD.AUTH_DETAIL_DIM_ID != NEW.AUTH_DETAIL_DIM_ID THEN
			lv_sql := lv_sql || 'AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_DETAIL_DIM_ID = $1.AUTH_DETAIL_DIM_ID OR AUTH_DETAIL_DIM_ID = $2.AUTH_DETAIL_DIM_ID)';
		END IF;
		IF OLD.CONSUMER_DIM_ID != NEW.CONSUMER_DIM_ID THEN
			lv_sql := lv_sql || 'CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID, ';
			lv_filter := lv_filter || ' AND (CONSUMER_DIM_ID = $1.CONSUMER_DIM_ID OR CONSUMER_DIM_ID = $2.CONSUMER_DIM_ID)';
		END IF;
		IF OLD.ITEM_TS != NEW.ITEM_TS THEN
			lv_sql := lv_sql || 'ITEM_TS = $2.ITEM_TS, ';
			lv_filter := lv_filter || ' AND (ITEM_TS = $1.ITEM_TS OR ITEM_TS = $2.ITEM_TS)';
		END IF;
		IF OLD.REVISION != NEW.REVISION THEN
			lv_sql := lv_sql || 'REVISION = $2.REVISION, ';
			lv_filter := lv_filter || ' AND (REVISION = $1.REVISION OR REVISION = $2.REVISION)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_TS = $2.LAST_UPDATED_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE ITEM_FACT_ID = $1.ITEM_FACT_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO RDW.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'ITEM_DATE_DIM_ID >= ' || RDW.PARTITION_START_DATE_DIM_ID(NEW.ITEM_DATE_DIM_ID, 'month') || ' AND ITEM_DATE_DIM_ID < ' || RDW.PARTITION_END_DATE_DIM_ID(NEW.ITEM_DATE_DIM_ID, 'month')||'), CONSTRAINT CK_' || lv_new_partition_table || '_1 CHECK(' ||'ITEM_TYPE_DIM_ID =' || NEW.ITEM_TYPE_DIM_ID||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE RDW.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(ITEM_FACT_ID) USING INDEX TABLESPACE rdw_data_t1, CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_ITEM_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TYPE_DIM_ID) REFERENCES RDW.ITEM_TYPE_DIM(ITEM_TYPE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_ITEM_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_ITEM_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(ITEM_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_UPLOAD_DATE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(UPLOAD_DATE_DIM_ID) REFERENCES RDW.DATE_DIM(DATE_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_UPLOAD_TIME_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(UPLOAD_TIME_DIM_ID) REFERENCES RDW.TIME_DIM(TIME_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_CURRENCY_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(CURRENCY_DIM_ID) REFERENCES RDW.CURRENCY_DIM(CURRENCY_DIM_ID), CONSTRAINT ' || RDW.CONSTRUCT_NAME('_FK_FILL_FACT_AUTH_TYPE_DIM_ID', lv_new_suffix) || ' FOREIGN KEY(AUTH_TYPE_DIM_ID) REFERENCES RDW.AUTH_TYPE_DIM(AUTH_TYPE_DIM_ID)) INHERITS(RDW._FILL_FACT) TABLESPACE rdw_data_t1';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE RDW.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM RDW.COPY_INDEXES('_FILL_FACT', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM RDW.' || lv_old_partition_table || ' WHERE ITEM_FACT_ID = $1.ITEM_FACT_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW RDW.FILL_FACT
	AS SELECT * FROM RDW._FILL_FACT;

ALTER VIEW RDW.FILL_FACT ALTER ITEM_FACT_ID SET DEFAULT  nextval('rdw.item_fact_item_fact_id_seq'::regclass);
ALTER VIEW RDW.FILL_FACT ALTER REVISION SET DEFAULT 1;
ALTER VIEW RDW.FILL_FACT ALTER CREATED_BY SET DEFAULT SESSION_USER;
ALTER VIEW RDW.FILL_FACT ALTER LAST_UPDATED_BY SET DEFAULT SESSION_USER;

GRANT SELECT ON RDW.FILL_FACT TO read_rdw;
GRANT INSERT, UPDATE, DELETE ON RDW.FILL_FACT TO write_rdw;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON RDW._FILL_FACT FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_FILL_FACT ON RDW.FILL_FACT;
CREATE TRIGGER TRIIUD_FILL_FACT
INSTEAD OF INSERT OR UPDATE OR DELETE ON RDW.FILL_FACT
	FOR EACH ROW
	EXECUTE PROCEDURE RDW.FRIIUD_FILL_FACT();


CREATE UNIQUE INDEX AK_FILL_FACT ON RDW._FILL_FACT(SOURCE_FILL_ID, SOURCE_SYSTEM_CD) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_ITEM_DETAIL_DIM_ID ON RDW._FILL_FACT(ITEM_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_PREV_FILL_DIM_ID ON RDW._FILL_FACT(PREV_FILL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_HOST_DIM_ID ON RDW._FILL_FACT(HOST_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_DEVICE_DIM_ID ON RDW._FILL_FACT(DEVICE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_LOCATION_DIM_ID_ITEM_TS ON RDW._FILL_FACT(LOCATION_DIM_ID, ITEM_TS) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_LOCATION_DIM_ID ON RDW._FILL_FACT(LOCATION_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_POS_DIM_ID ON RDW._FILL_FACT(POS_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_ITEM_DATE_DIM_ID ON RDW._FILL_FACT(ITEM_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_ITEM_TIME_DIM_ID ON RDW._FILL_FACT(ITEM_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_UPLOAD_DATE_DIM_ID ON RDW._FILL_FACT(UPLOAD_DATE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_UPLOAD_TIME_DIM_ID ON RDW._FILL_FACT(UPLOAD_TIME_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_DEVICE_CALL_DIM_ID ON RDW._FILL_FACT(DEVICE_CALL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_AUTH_TYPE_DIM_ID ON RDW._FILL_FACT(AUTH_TYPE_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_AUTH_DETAIL_DIM_ID ON RDW._FILL_FACT(AUTH_DETAIL_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_CONSUMER_DIM_ID ON RDW._FILL_FACT(CONSUMER_DIM_ID) TABLESPACE rdw_data_t1;

CREATE INDEX IX_FILL_FACT_CREATED_TS ON RDW._FILL_FACT(CREATED_TS) TABLESPACE rdw_data_t1;


CREATE OR REPLACE FUNCTION RDW.UPSERT_FILL_FACT(
	p_item_fact_id OUT BIGINT, 
	p_source_fill_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_item_detail_dim_id BIGINT, 
	p_prev_fill_dim_id BIGINT, 
	p_host_dim_id BIGINT, 
	p_cash_amount DECIMAL(12,4), 
	p_credit_amount DECIMAL(12,4), 
	p_othercard_amount DECIMAL(12,4), 
	p_cash_count BIGINT, 
	p_credit_count BIGINT, 
	p_othercard_count BIGINT, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_FACT_ID, REVISION
			  INTO p_item_fact_id, p_old_revision
			  FROM RDW.FILL_FACT
			 WHERE SOURCE_FILL_ID = p_source_fill_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.FILL_FACT
			   SET ITEM_DETAIL_DIM_ID = p_item_detail_dim_id,
			       PREV_FILL_DIM_ID = p_prev_fill_dim_id,
			       HOST_DIM_ID = p_host_dim_id,
			       CASH_AMOUNT = p_cash_amount,
			       CREDIT_AMOUNT = p_credit_amount,
			       OTHERCARD_AMOUNT = p_othercard_amount,
			       CASH_COUNT = p_cash_count,
			       CREDIT_COUNT = p_credit_count,
			       OTHERCARD_COUNT = p_othercard_count,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       ITEM_DATE_DIM_ID = p_item_date_dim_id,
			       ITEM_TIME_DIM_ID = p_item_time_dim_id,
			       UPLOAD_DATE_DIM_ID = p_upload_date_dim_id,
			       UPLOAD_TIME_DIM_ID = p_upload_time_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       ITEM_TS = p_item_ts,
			       REVISION = p_revision
			 WHERE SOURCE_FILL_ID = p_source_fill_id
			   AND SOURCE_SYSTEM_CD = p_source_system_cd
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.FILL_FACT(SOURCE_FILL_ID, SOURCE_SYSTEM_CD, ITEM_DETAIL_DIM_ID, PREV_FILL_DIM_ID, HOST_DIM_ID, CASH_AMOUNT, CREDIT_AMOUNT, OTHERCARD_AMOUNT, CASH_COUNT, CREDIT_COUNT, OTHERCARD_COUNT, ITEM_TYPE_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, ITEM_DATE_DIM_ID, ITEM_TIME_DIM_ID, UPLOAD_DATE_DIM_ID, UPLOAD_TIME_DIM_ID, DEVICE_CALL_DIM_ID, CURRENCY_DIM_ID, AUTH_TYPE_DIM_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, ITEM_TS, REVISION)
					 VALUES(p_source_fill_id, p_source_system_cd, p_item_detail_dim_id, p_prev_fill_dim_id, p_host_dim_id, p_cash_amount, p_credit_amount, p_othercard_amount, p_cash_count, p_credit_count, p_othercard_count, p_item_type_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_item_date_dim_id, p_item_time_dim_id, p_upload_date_dim_id, p_upload_time_dim_id, p_device_call_dim_id, p_currency_dim_id, p_auth_type_dim_id, p_auth_detail_dim_id, p_consumer_dim_id, p_item_ts, p_revision)
					 RETURNING ITEM_FACT_ID
					      INTO p_item_fact_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION RDW.UPSERT_FILL_FACT(
	p_item_fact_id OUT BIGINT, 
	p_source_fill_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_item_detail_dim_id BIGINT, 
	p_prev_fill_dim_id BIGINT, 
	p_host_dim_id BIGINT, 
	p_cash_amount DECIMAL(12,4), 
	p_credit_amount DECIMAL(12,4), 
	p_othercard_amount DECIMAL(12,4), 
	p_cash_count BIGINT, 
	p_credit_count BIGINT, 
	p_othercard_count BIGINT, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER) TO write_rdw;


-- ======================================================================

GRANT SELECT ON ALL TABLES IN SCHEMA RDW TO read_rdw;
REVOKE EXECUTE ON ALL FUNCTIONS IN SCHEMA RDW FROM PUBLIC;
GRANT USAGE ON SCHEMA RDW TO read_rdw;
GRANT USAGE ON SCHEMA RDW TO write_rdw;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA RDW TO write_rdw;

CREATE SEQUENCE RDW.SEQ_REVISION NO CYCLE;

GRANT USAGE ON RDW.SEQ_REVISION TO write_rdw;
