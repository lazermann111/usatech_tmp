CREATE OR REPLACE FUNCTION RDW.GET_ADJUST_DESC(pn_doc_id BIGINT)
	RETURNS VARCHAR
	SECURITY DEFINER
AS $$
DECLARE
    str VARCHAR(4000):='';
    r RECORD;
BEGIN
FOR r IN select lidd.ITEM_DESC||'('||round(lif.amount,2)||')' as TEXT from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
where pd.SOURCE_DOC_ID = pn_doc_id and itd.PAYMENT_ENTRY_TYPE_CD ='AD'
    LOOP
    	IF LENGTH(str) > 3997 THEN
            str := SUBSTR(str, 1, 3997) || '...';
            EXIT;
		ELSIF LENGTH(str) > 0 THEN
            str := str || ', ';
    	END IF;
    	IF LENGTH(str) + LENGTH(r.TEXT) > 4000 THEN
            str := substring(str || r.TEXT, 1, 3997) || '...';
            EXIT;
    	ELSE
            str := str || r.TEXT;
    	END IF;
    END LOOP;
    RETURN str;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.GET_HEALTH_CODE(pn_measured_date timestamptz, pn_min_days bigint, pn_max_days bigint)
	RETURNS char(1)
	SECURITY DEFINER
AS $$
DECLARE
	lv_health_code char(1);
BEGIN
	select case when pn_measured_date is null then 'D'
	when pn_min_days is null or pn_min_days <=0 then '-'
	when pn_measured_date < current_timestamp - pn_max_days * interval'1 day' then 'O'
	when pn_measured_date < current_timestamp - pn_min_days * interval'1 day' then 'Y'
	else 'A' end into lv_health_code;
	
	return lv_health_code;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.MASK_STR(pv_str VARCHAR, pn_lead INTEGER, pn_end INTEGER)
	RETURNS VARCHAR
	SECURITY DEFINER
AS $$
DECLARE
BEGIN
	IF pn_lead+pn_end >= char_length(pv_str) THEN
         	RETURN RPAD('*',char_length(pv_str), '*');
     	ELSE
        	RETURN SUBSTR(pv_str, 1, pn_lead)||RPAD('*', char_length(pv_str) - pn_lead-pn_end, '*') || SUBSTR(pv_str, char_length(pv_str) - pn_end+1);
     	END IF; 
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION PUBLIC.INSTR(string varchar, string_to_search varchar, beg_index integer)
RETURNS integer AS $$
DECLARE
    pos integer NOT NULL DEFAULT 0;
    temp_str varchar;
    beg integer;
    length integer;
    ss_length integer;
BEGIN
    IF beg_index > 0 THEN
        temp_str := substring(string FROM beg_index);
        pos := position(string_to_search IN temp_str);

        IF pos = 0 THEN
            RETURN 0;
        ELSE
            RETURN pos + beg_index - 1;
        END IF;
    ELSE
        ss_length := char_length(string_to_search);
        length := char_length(string);
        beg := length + beg_index - ss_length + 2;

        WHILE beg > 0 LOOP
            temp_str := substring(string FROM beg FOR ss_length);
            pos := position(string_to_search IN temp_str);

            IF pos > 0 THEN
                RETURN beg;
            END IF;

            beg := beg - 1;
        END LOOP;

        RETURN 0;
    END IF;
END;
$$ LANGUAGE plpgsql STRICT IMMUTABLE;

CREATE FUNCTION PUBLIC.INSTR(string varchar, string_to_search varchar,
                      beg_index integer, occur_index integer)
RETURNS integer AS $$
DECLARE
    pos integer NOT NULL DEFAULT 0;
    occur_number integer NOT NULL DEFAULT 0;
    temp_str varchar;
    beg integer;
    i integer;
    length integer;
    ss_length integer;
BEGIN
    IF beg_index > 0 THEN
        beg := beg_index;
        temp_str := substring(string FROM beg_index);

        FOR i IN 1..occur_index LOOP
            pos := position(string_to_search IN temp_str);

            IF i = 1 THEN
                beg := beg + pos - 1;
            ELSE
                beg := beg + pos;
            END IF;

            temp_str := substring(string FROM beg + 1);
        END LOOP;

        IF pos = 0 THEN
            RETURN 0;
        ELSE
            RETURN beg;
        END IF;
    ELSE
        ss_length := char_length(string_to_search);
        length := char_length(string);
        beg := length + beg_index - ss_length + 2;

        WHILE beg > 0 LOOP
            temp_str := substring(string FROM beg FOR ss_length);
            pos := position(string_to_search IN temp_str);

            IF pos > 0 THEN
                occur_number := occur_number + 1;

                IF occur_number = occur_index THEN
                    RETURN beg;
                END IF;
            END IF;

            beg := beg - 1;
        END LOOP;

        RETURN 0;
    END IF;
END;
$$ LANGUAGE plpgsql STRICT IMMUTABLE;

CREATE FUNCTION PUBLIC.INSTR(varchar, varchar) RETURNS integer AS $$
DECLARE
    pos integer;
BEGIN
    pos:= instr($1, $2, 1);
    RETURN pos;
END;
$$ LANGUAGE plpgsql STRICT IMMUTABLE;

CREATE OR REPLACE FUNCTION RDW.GET_DELIMITED_FIELD_VALUE(pv_data VARCHAR, pv_field_key VARCHAR)
	RETURNS VARCHAR
	SECURITY DEFINER
AS $$
DECLARE
  lv_fs CHAR(1) NOT NULL DEFAULT CHR(28);
  ln_pos INTEGER;
BEGIN
	IF pv_data IS NULL OR pv_field_key IS NULL THEN
		RETURN NULL;
	END IF;
	ln_pos := INSTR(pv_data, lv_fs || pv_field_key || '=');
	IF ln_pos > 0 THEN
		RETURN SUBSTRING(pv_data, INSTR(pv_data, '=', ln_pos) + 1, INSTR(pv_data, FS, ln_pos + 1) - INSTR(pv_data, '=', ln_pos) - 1);
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.TRUNC(pd_date DATE, pv_field TEXT)
        RETURNS DATE
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        IF UPPER(pv_field) IN('DAY', 'DY', 'D') THEN -- DAY, DY, D truncates to Sunday and requires more work
                RETURN DATE_TRUNC('week', pd_date) - '1 day'::INTERVAL;
        ELSIF UPPER(pv_field) IN('WW') THEN -- WW truncates to same day as jan 1 and requires more work
                RETURN DATE_TRUNC('day', pd_date) - (((EXTRACT(DOY FROM pd_date)::INTEGER - 1) % 7) || ' day')::INTERVAL;
        ELSIF UPPER(pv_field) IN('W') THEN -- W truncates to same day as the first of the month and requires more work
                RETURN DATE_TRUNC('day', pd_date) - (((EXTRACT(DAY FROM pd_date)::INTEGER - 1) % 7) || ' day')::INTERVAL;
        ELSE
                RETURN DATE_TRUNC(CASE UPPER(pv_field)
                        WHEN 'CC' THEN 'century'
                        WHEN 'YYYY' THEN 'year' WHEN 'YYY' THEN 'year' WHEN 'YY' THEN 'year' WHEN 'Y' THEN 'year' WHEN 'YEAR' THEN 'year'
                        WHEN 'Q' THEN 'quarter'
                        WHEN 'MONTH' THEN 'month' WHEN 'MON' THEN 'month' WHEN 'MM' THEN 'month' WHEN 'RM' THEN 'month'
                        WHEN 'IW' THEN 'week'
                        WHEN 'DD' THEN 'day' WHEN 'DDD' THEN 'day' WHEN 'J' THEN 'day'
                        WHEN 'HH' THEN 'hour' WHEN 'HH12' THEN 'hour' WHEN 'HH24' THEN 'hour'
                        WHEN 'MI' THEN 'minute'
                        END, pd_date);
        END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.TRUNC(pd_date TIMESTAMP, pv_field TEXT)
        RETURNS TIMESTAMP
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        IF UPPER(pv_field) IN('DAY', 'DY', 'D') THEN -- DAY, DY, D truncates to Sunday and requires more work
                RETURN DATE_TRUNC('week', pd_date) - '1 day'::INTERVAL;
        ELSIF UPPER(pv_field) IN('WW') THEN -- WW truncates to same day as jan 1 and requires more work
                RETURN DATE_TRUNC('day', pd_date) - (((EXTRACT(DOY FROM pd_date)::INTEGER - 1) % 7) || ' day')::INTERVAL;
        ELSIF UPPER(pv_field) IN('W') THEN -- W truncates to same day as the first of the month and requires more work
                RETURN DATE_TRUNC('day', pd_date) - (((EXTRACT(DAY FROM pd_date)::INTEGER - 1) % 7) || ' day')::INTERVAL;
        ELSE
                RETURN DATE_TRUNC(CASE UPPER(pv_field)
                        WHEN 'CC' THEN 'century'
                        WHEN 'YYYY' THEN 'year' WHEN 'YYY' THEN 'year' WHEN 'YY' THEN 'year' WHEN 'Y' THEN 'year' WHEN 'YEAR' THEN 'year'
                        WHEN 'Q' THEN 'quarter'
                        WHEN 'MONTH' THEN 'month' WHEN 'MON' THEN 'month' WHEN 'MM' THEN 'month' WHEN 'RM' THEN 'month'
                        WHEN 'IW' THEN 'week'
                        WHEN 'DD' THEN 'day' WHEN 'DDD' THEN 'day' WHEN 'J' THEN 'day'
                        WHEN 'HH' THEN 'hour' WHEN 'HH12' THEN 'hour' WHEN 'HH24' THEN 'hour'
                        WHEN 'MI' THEN 'minute'
                        END, pd_date);
        END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.TRUNC(pd_date TIMESTAMPTZ, pv_field TEXT)
        RETURNS TIMESTAMPTZ
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        IF UPPER(pv_field) IN('DAY', 'DY', 'D') THEN -- DAY, DY, D truncates to Sunday and requires more work
                RETURN DATE_TRUNC('week', pd_date) - '1 day'::INTERVAL;
        ELSIF UPPER(pv_field) IN('WW') THEN -- WW truncates to same day as jan 1 and requires more work
                RETURN DATE_TRUNC('day', pd_date) - (((EXTRACT(DOY FROM pd_date)::INTEGER - 1) % 7) || ' day')::INTERVAL;
        ELSIF UPPER(pv_field) IN('W') THEN -- W truncates to same day as the first of the month and requires more work
                RETURN DATE_TRUNC('day', pd_date) - (((EXTRACT(DAY FROM pd_date)::INTEGER - 1) % 7) || ' day')::INTERVAL;
        ELSE
                RETURN DATE_TRUNC(CASE UPPER(pv_field)
                        WHEN 'CC' THEN 'century'
                        WHEN 'YYYY' THEN 'year' WHEN 'YYY' THEN 'year' WHEN 'YY' THEN 'year' WHEN 'Y' THEN 'year' WHEN 'YEAR' THEN 'year'
                        WHEN 'Q' THEN 'quarter'
                        WHEN 'MONTH' THEN 'month' WHEN 'MON' THEN 'month' WHEN 'MM' THEN 'month' WHEN 'RM' THEN 'month'
                        WHEN 'IW' THEN 'week'
                        WHEN 'DD' THEN 'day' WHEN 'DDD' THEN 'day' WHEN 'J' THEN 'day'
                        WHEN 'HH' THEN 'hour' WHEN 'HH12' THEN 'hour' WHEN 'HH24' THEN 'hour'
                        WHEN 'MI' THEN 'minute'
                        END, pd_date);
        END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_date DATE)
        RETURNS SMALLINT
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        RETURN (EXTRACT(EPOCH FROM pd_date) / 24 / 60 / 60)::SMALLINT;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_timestamp timestamp without time zone)
        RETURNS SMALLINT
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        RETURN (EXTRACT(EPOCH FROM date(pd_timestamp)) / 24 / 60 / 60)::SMALLINT;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_timestamp timestamp with time zone)
        RETURNS SMALLINT
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        RETURN (EXTRACT(EPOCH FROM date(pd_timestamp)) / 24 / 60 / 60)::SMALLINT;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.LAST_DAY(pd_date DATE)
        RETURNS DATE
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        RETURN DATE_TRUNC('month', pd_date) + '1 month -1 day'::INTERVAL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.LAST_DAY(pd_date TIMESTAMP)
        RETURNS TIMESTAMP
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        RETURN DATE_TRUNC('month', pd_date) + '1 month -1 day'::INTERVAL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.LAST_DAY(pd_date TIMESTAMPTZ)
        RETURNS TIMESTAMPTZ
   IMMUTABLE LEAKPROOF
AS $$
BEGIN
        RETURN DATE_TRUNC('month', pd_date) + '1 month -1 day'::INTERVAL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MAIN.PARTITION_MAINTENANCE (
   ) RETURNS VOID
AS $$
DECLARE
	L_REC RECORD;
BEGIN
	FOR L_REC IN 
		SELECT PT.SCHEMANAME, PT.TABLENAME
		FROM PG_CATALOG.PG_TABLES PT
		JOIN MAIN.PARTITION_INFO PI ON PT.TABLENAME SIMILAR TO PI.PARTITION_NAME_REGEX
		WHERE PI.PARTITION_TYPE_CD = 'D' AND TO_TIMESTAMP(SUBSTRING(PT.TABLENAME FROM PI.PARTITION_NAME_REGEX), 'YYYY_MM_DD') < CURRENT_TIMESTAMP - CAST(PI.RETENTION + 1 || ' DAYS' AS INTERVAL)
			OR PI.PARTITION_TYPE_CD = 'M' AND TO_TIMESTAMP(SUBSTRING(PT.TABLENAME FROM PI.PARTITION_NAME_REGEX), 'YYYY_MM') < CURRENT_TIMESTAMP - CAST(PI.RETENTION + 1 || ' MONTHS' AS INTERVAL)
	LOOP
		PERFORM DBLINK_CONNECT('main', 'dbname=main');
		PERFORM DBLINK_EXEC('main', 'drop table '||L_REC.SCHEMANAME||'.' || L_REC.TABLENAME || '; commit;');
		PERFORM DBLINK_DISCONNECT('main');
	END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION PUBLIC.FROM_HEX(
       pv_hex VARCHAR)
    RETURNS BIGINT
AS $$
DECLARE
       ln_num BIGINT;
BEGIN
       IF pv_hex IS NULL OR LENGTH(TRIM(pv_hex)) = 0 THEN
              RETURN NULL;
       ELSIF TRIM(pv_hex) !~ '^[0-9A-Fa-f]+$' THEN
              RAISE EXCEPTION 'Input contains non-hex character' USING ERRCODE = 'invalid_binary_representation';
       ELSE
              EXECUTE 'SELECT x''' || TRIM(pv_hex) || '''::BIGINT' INTO ln_num;
              RETURN ln_num;
       END IF;                    
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.UPDATE_PAID_TS(pd_paid_ts timestamptz, pn_doc_id bigint) RETURNS VOID
AS $$
DECLARE
	l_payment_dim_id BIGINT;
BEGIN
select coalesce(max(payment_dim_id),-1) into l_payment_dim_id from rdw.payment_dim where source_doc_id=pn_doc_id;	
IF l_payment_dim_id >-1 THEN

	update rdw.ledger_item_fact set PAID_DATE_DIM_ID=GET_DATE_DIM_ID(pd_paid_ts), PAID_TS=pd_paid_ts 
	where payment_dim_id=l_payment_dim_id and PAID_DATE_DIM_ID=0;

	update rdw.ledger_item_detail_dim set PAID_TS=pd_paid_ts
	where item_detail_dim_id in (select item_detail_dim_id from rdw.ledger_item_fact where payment_dim_id=l_payment_dim_id)
	and PAID_TS IS NULL;
	

	update rdw.tran_item_fact set PAID_DATE_DIM_ID=GET_DATE_DIM_ID(pd_paid_ts), PAID_TS=pd_paid_ts 
	where payment_dim_id=l_payment_dim_id;
	
	update rdw.tran_item_detail_dim set PAID_TS=pd_paid_ts
	where item_detail_dim_id in (select item_detail_dim_id from rdw.tran_item_fact where payment_dim_id=l_payment_dim_id);
	

END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.UPDATE_DOC_ID(pn_batch_id bigint, pn_doc_id bigint) RETURNS VOID
AS $$
DECLARE
	l_payment_batch_dim_id BIGINT;
BEGIN
	select coalesce(max(payment_batch_dim_id),-1) into l_payment_batch_dim_id from rdw.payment_batch_dim where source_batch_id=pn_batch_id;
IF l_payment_batch_dim_id >-1 THEN
	update rdw.ledger_item_fact set payment_dim_id=pd.payment_dim_id
	from (select payment_dim_id from rdw.payment_dim where source_doc_id=pn_doc_id) pd
	where payment_batch_dim_id=l_payment_batch_dim_id;
	
	update rdw.tran_item_fact set payment_dim_id=pd.payment_dim_id
	from (select payment_dim_id from rdw.payment_dim where source_doc_id=pn_doc_id) pd
	where payment_batch_dim_id=l_payment_batch_dim_id;
END IF;
END;
$$ LANGUAGE plpgsql;


GRANT EXECUTE ON FUNCTION RDW.GET_ADJUST_DESC(pn_doc_id BIGINT) to read_rdw;
GRANT EXECUTE ON FUNCTION RDW.GET_HEALTH_CODE(pn_measured_date timestamptz, pn_min_days bigint, pn_max_days bigint) to read_rdw;
GRANT EXECUTE ON FUNCTION RDW.MASK_STR(pv_str VARCHAR, pn_lead INTEGER, pn_end INTEGER) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.INSTR(string varchar, string_to_search varchar, beg_index integer) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.INSTR(string varchar, string_to_search varchar,beg_index integer, occur_index integer) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.INSTR(varchar, varchar) to read_rdw;
GRANT EXECUTE ON FUNCTION RDW.GET_DELIMITED_FIELD_VALUE(pv_data VARCHAR, pv_field_key VARCHAR) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.TRUNC(pd_date DATE, pv_field TEXT) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.TRUNC(pd_date TIMESTAMP, pv_field TEXT) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.TRUNC(pd_date TIMESTAMPTZ, pv_field TEXT) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_date DATE) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_timestamp timestamp without time zone) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_timestamp timestamp with time zone) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.GET_DATE_DIM_ID(pd_timestamp timestamp with time zone) to write_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.LAST_DAY(pd_date DATE) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.LAST_DAY(pd_date TIMESTAMP) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.LAST_DAY(pd_date TIMESTAMPTZ) to read_rdw;
GRANT EXECUTE ON FUNCTION PUBLIC.FROM_HEX(pv_hex VARCHAR) to read_rdw;
GRANT EXECUTE ON FUNCTION RDW.UPDATE_PAID_TS(pd_paid_ts timestamptz, pn_doc_id bigint) to read_rdw;
GRANT EXECUTE ON FUNCTION RDW.UPDATE_PAID_TS(pd_paid_ts timestamptz, pn_doc_id bigint) to write_rdw;
GRANT EXECUTE ON FUNCTION RDW.UPDATE_DOC_ID(pn_batch_id bigint, pn_doc_id bigint) to read_rdw;
GRANT EXECUTE ON FUNCTION RDW.UPDATE_DOC_ID(pn_batch_id bigint, pn_doc_id bigint) to write_rdw;
