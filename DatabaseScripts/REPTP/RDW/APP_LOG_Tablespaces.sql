/*
SELECT * from dba_tablespaces;
SELECT * from dba_data_files;
SELECT * from all_views where view_name like '%FILE%';
-- */
CREATE TABLESPACE APP_LOG_DATA
  DATAFILE '+USARDB_DATA_01/usardb/datafile/app_log_data1.dbf'
  SIZE 30M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/app_log_data1.dbf' AUTOEXTEND ON;
  
CREATE TABLESPACE APP_LOG_INDX
  DATAFILE '+USARDB_DATA_01/usardb/datafile/app_log_idx1.dbf'
  SIZE 10M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/app_log_idx1.dbf' AUTOEXTEND ON;
  

