CREATE OR REPLACE FUNCTION RDW.UPSERT_CONSUMER_DIM(
	p_consumer_dim_id OUT BIGINT, 
	p_global_account_id BIGINT, 
	p_source_consumer_acct_id BIGINT, 
	p_card_type VARCHAR(100), 
	p_safe_card_number VARCHAR(100), 
	p_consumer_acct_identifier BIGINT, 
	p_consumer_name VARCHAR(100), 
	p_consumer_identifier VARCHAR(100), 
	p_account_status VARCHAR(50), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT CONSUMER_DIM_ID, REVISION
			  INTO p_consumer_dim_id, p_old_revision
			  FROM RDW.CONSUMER_DIM
			 WHERE ((SOURCE_CONSUMER_ACCT_ID IS NOT NULL AND SOURCE_CONSUMER_ACCT_ID = p_source_consumer_acct_id) OR (SOURCE_CONSUMER_ACCT_ID IS NULL AND p_source_consumer_acct_id IS NULL))
			   AND CARD_TYPE = p_card_type;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.CONSUMER_DIM
			   SET GLOBAL_ACCOUNT_ID = p_global_account_id,
			       SAFE_CARD_NUMBER = p_safe_card_number,
			       CONSUMER_ACCT_IDENTIFIER = p_consumer_acct_identifier,
			       CONSUMER_NAME = p_consumer_name,
			       CONSUMER_IDENTIFIER = p_consumer_identifier,
			       ACCOUNT_STATUS = p_account_status,
			       REVISION = p_revision
			 WHERE ((SOURCE_CONSUMER_ACCT_ID IS NOT NULL AND SOURCE_CONSUMER_ACCT_ID = p_source_consumer_acct_id) OR (SOURCE_CONSUMER_ACCT_ID IS NULL AND p_source_consumer_acct_id IS NULL))
			   AND CARD_TYPE = p_card_type
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.CONSUMER_DIM(SOURCE_CONSUMER_ACCT_ID, CARD_TYPE, GLOBAL_ACCOUNT_ID, SAFE_CARD_NUMBER, CONSUMER_ACCT_IDENTIFIER, CONSUMER_NAME, CONSUMER_IDENTIFIER, ACCOUNT_STATUS, REVISION)
					 VALUES(p_source_consumer_acct_id, p_card_type, p_global_account_id, p_safe_card_number, p_consumer_acct_identifier, p_consumer_name, p_consumer_identifier, p_account_status, p_revision)
					 RETURNING CONSUMER_DIM_ID
					      INTO p_consumer_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.UPSERT_TRAN_LINE_ITEM_DIM(
	p_tran_line_item_dim_id OUT BIGINT, 
	p_source_tli_type_id INTEGER, 
	p_source_tli_position_cd VARCHAR(6), 
	p_source_tli_desc VARCHAR(60), 
	p_tran_line_item_type VARCHAR(60), 
	p_tran_line_item_desc VARCHAR(4000), 
	p_tran_line_item_product VARCHAR(60), 
	p_tran_line_item_category VARCHAR(60), 
	p_tran_line_item_label VARCHAR(60), 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT TRAN_LINE_ITEM_DIM_ID, REVISION
			  INTO p_tran_line_item_dim_id, p_old_revision
			  FROM RDW.TRAN_LINE_ITEM_DIM
			 WHERE SOURCE_TLI_TYPE_ID = p_source_tli_type_id
			   AND ((SOURCE_TLI_POSITION_CD IS NOT NULL AND SOURCE_TLI_POSITION_CD = p_source_tli_position_cd) OR (SOURCE_TLI_POSITION_CD IS NULL AND p_source_tli_position_cd IS NULL))
			   AND ((SOURCE_TLI_DESC IS NOT NULL AND SOURCE_TLI_DESC = p_source_tli_desc) OR (SOURCE_TLI_DESC IS NULL AND p_source_tli_desc IS NULL))
			   AND TRAN_LINE_ITEM_TYPE = p_tran_line_item_type
			   AND TRAN_LINE_ITEM_DESC = p_tran_line_item_desc
			   AND TRAN_LINE_ITEM_PRODUCT = p_tran_line_item_product
			   AND TRAN_LINE_ITEM_CATEGORY = p_tran_line_item_category
			   AND TRAN_LINE_ITEM_LABEL = p_tran_line_item_label;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.TRAN_LINE_ITEM_DIM
			   SET REVISION = p_revision
			 WHERE SOURCE_TLI_TYPE_ID = p_source_tli_type_id
			   AND ((SOURCE_TLI_POSITION_CD IS NOT NULL AND SOURCE_TLI_POSITION_CD = p_source_tli_position_cd) OR (SOURCE_TLI_POSITION_CD IS NULL AND p_source_tli_position_cd IS NULL))
			   AND ((SOURCE_TLI_DESC IS NOT NULL AND SOURCE_TLI_DESC = p_source_tli_desc) OR (SOURCE_TLI_DESC IS NULL AND p_source_tli_desc IS NULL))
			   AND TRAN_LINE_ITEM_TYPE = p_tran_line_item_type
			   AND TRAN_LINE_ITEM_DESC = p_tran_line_item_desc
			   AND TRAN_LINE_ITEM_PRODUCT = p_tran_line_item_product
			   AND TRAN_LINE_ITEM_CATEGORY = p_tran_line_item_category
			   AND TRAN_LINE_ITEM_LABEL = p_tran_line_item_label
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.TRAN_LINE_ITEM_DIM(SOURCE_TLI_TYPE_ID, SOURCE_TLI_POSITION_CD, SOURCE_TLI_DESC, TRAN_LINE_ITEM_TYPE, TRAN_LINE_ITEM_DESC, TRAN_LINE_ITEM_PRODUCT, TRAN_LINE_ITEM_CATEGORY, TRAN_LINE_ITEM_LABEL, REVISION)
					 VALUES(p_source_tli_type_id, p_source_tli_position_cd, p_source_tli_desc, p_tran_line_item_type, p_tran_line_item_desc, p_tran_line_item_product, p_tran_line_item_category, p_tran_line_item_label, p_revision)
					 RETURNING TRAN_LINE_ITEM_DIM_ID
					      INTO p_tran_line_item_dim_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.UPSERT_TRAN_ITEM_FACT(
	p_item_fact_id OUT BIGINT, 
	p_item_detail_dim_id BIGINT, 
	p_source_tran_line_item_id BIGINT, 
	p_source_tran_id BIGINT, 
	p_report_tran_id BIGINT, 
	p_source_system_cd VARCHAR(30), 
	p_tran_line_item_dim_id BIGINT, 
	p_host_dim_id BIGINT, 
	p_fill_dim_id BIGINT, 
	p_fill_date_dim_id SMALLINT, 
	p_fill_time_dim_id SMALLINT, 
	p_authority_terminal_dim_id BIGINT, 
	p_apply_to_consumer_dim_id BIGINT, 
	p_orig_tran_dim_id BIGINT, 
	p_orig_auth_detail_dim_id BIGINT, 
	p_unit_price DECIMAL(12,4), 
	p_product_service_quantity INTEGER, 
	p_process_fee_amount DECIMAL(12,4), 
	p_duration_seconds_fraction DECIMAL(22,10), 
	p_tran_count_fraction DECIMAL(22,21), 
	p_convenience_fee_amount DECIMAL(12,4), 
	p_loyalty_discount_amount DECIMAL(12,4), 
	p_fill_ts TIMESTAMPTZ, 
	p_source_ledger_id BIGINT, 
	p_settled_date_dim_id SMALLINT, 
	p_settled_time_dim_id SMALLINT, 
	p_payment_dim_id BIGINT, 
	p_payment_batch_dim_id BIGINT, 
	p_paid_date_dim_id SMALLINT, 
	p_quantity NUMERIC, 
	p_amount DECIMAL(12,4), 
	p_paid_ts TIMESTAMPTZ, 
	p_item_type_dim_id SMALLINT, 
	p_device_dim_id BIGINT, 
	p_location_dim_id BIGINT, 
	p_pos_dim_id BIGINT, 
	p_item_date_dim_id SMALLINT, 
	p_item_time_dim_id SMALLINT, 
	p_upload_date_dim_id SMALLINT, 
	p_upload_time_dim_id SMALLINT, 
	p_device_call_dim_id BIGINT, 
	p_currency_dim_id SMALLINT, 
	p_auth_type_dim_id INTEGER, 
	p_auth_detail_dim_id BIGINT, 
	p_consumer_dim_id BIGINT, 
	p_item_ts TIMESTAMPTZ, 
	p_revision INOUT BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
DECLARE
	p_old_revision BIGINT;
BEGIN
	FOR i IN 1..20 LOOP
		BEGIN
			SELECT ITEM_FACT_ID, REVISION
			  INTO p_item_fact_id, p_old_revision
			  FROM RDW.TRAN_ITEM_FACT
			 WHERE ((SOURCE_TRAN_LINE_ITEM_ID IS NOT NULL AND SOURCE_TRAN_LINE_ITEM_ID = p_source_tran_line_item_id) OR (SOURCE_TRAN_LINE_ITEM_ID IS NULL AND p_source_tran_line_item_id IS NULL))
			   AND SOURCE_TRAN_ID = p_source_tran_id;
			IF FOUND THEN
			IF NOT(p_revision > p_old_revision) THEN
				p_row_count := 0;
				p_revision := p_old_revision;
				RETURN;
			END IF;
			UPDATE RDW.TRAN_ITEM_FACT
			   SET ITEM_DETAIL_DIM_ID = p_item_detail_dim_id,
			       REPORT_TRAN_ID = p_report_tran_id,
			       SOURCE_SYSTEM_CD = p_source_system_cd,
			       TRAN_LINE_ITEM_DIM_ID = p_tran_line_item_dim_id,
			       HOST_DIM_ID = p_host_dim_id,
			       FILL_DIM_ID = p_fill_dim_id,
			       FILL_DATE_DIM_ID = p_fill_date_dim_id,
			       FILL_TIME_DIM_ID = p_fill_time_dim_id,
			       AUTHORITY_TERMINAL_DIM_ID = p_authority_terminal_dim_id,
			       APPLY_TO_CONSUMER_DIM_ID = p_apply_to_consumer_dim_id,
			       ORIG_TRAN_DIM_ID = p_orig_tran_dim_id,
			       ORIG_AUTH_DETAIL_DIM_ID = p_orig_auth_detail_dim_id,
			       UNIT_PRICE = p_unit_price,
			       PRODUCT_SERVICE_QUANTITY = p_product_service_quantity,
			       PROCESS_FEE_AMOUNT = p_process_fee_amount,
			       DURATION_SECONDS_FRACTION = p_duration_seconds_fraction,
			       TRAN_COUNT_FRACTION = p_tran_count_fraction,
			       CONVENIENCE_FEE_AMOUNT = p_convenience_fee_amount,
			       LOYALTY_DISCOUNT_AMOUNT = p_loyalty_discount_amount,
			       FILL_TS = p_fill_ts,
			       SOURCE_LEDGER_ID = p_source_ledger_id,
			       SETTLED_DATE_DIM_ID = p_settled_date_dim_id,
			       SETTLED_TIME_DIM_ID = p_settled_time_dim_id,
			       PAYMENT_DIM_ID = p_payment_dim_id,
			       PAYMENT_BATCH_DIM_ID = p_payment_batch_dim_id,
			       PAID_DATE_DIM_ID = p_paid_date_dim_id,
			       QUANTITY = p_quantity,
			       AMOUNT = p_amount,
			       PAID_TS = p_paid_ts,
			       ITEM_TYPE_DIM_ID = p_item_type_dim_id,
			       DEVICE_DIM_ID = p_device_dim_id,
			       LOCATION_DIM_ID = p_location_dim_id,
			       POS_DIM_ID = p_pos_dim_id,
			       ITEM_DATE_DIM_ID = p_item_date_dim_id,
			       ITEM_TIME_DIM_ID = p_item_time_dim_id,
			       UPLOAD_DATE_DIM_ID = p_upload_date_dim_id,
			       UPLOAD_TIME_DIM_ID = p_upload_time_dim_id,
			       DEVICE_CALL_DIM_ID = p_device_call_dim_id,
			       CURRENCY_DIM_ID = p_currency_dim_id,
			       AUTH_TYPE_DIM_ID = p_auth_type_dim_id,
			       AUTH_DETAIL_DIM_ID = p_auth_detail_dim_id,
			       CONSUMER_DIM_ID = p_consumer_dim_id,
			       ITEM_TS = p_item_ts,
			       REVISION = p_revision
			 WHERE ((SOURCE_TRAN_LINE_ITEM_ID IS NOT NULL AND SOURCE_TRAN_LINE_ITEM_ID = p_source_tran_line_item_id) OR (SOURCE_TRAN_LINE_ITEM_ID IS NULL AND p_source_tran_line_item_id IS NULL))
			   AND SOURCE_TRAN_ID = p_source_tran_id
			   AND p_revision > REVISION;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				p_row_count := 0;
				p_revision := p_old_revision;
				END IF;
			ELSE
				BEGIN
					INSERT INTO RDW.TRAN_ITEM_FACT(SOURCE_TRAN_LINE_ITEM_ID, SOURCE_TRAN_ID, ITEM_DETAIL_DIM_ID, REPORT_TRAN_ID, SOURCE_SYSTEM_CD, TRAN_LINE_ITEM_DIM_ID, HOST_DIM_ID, FILL_DIM_ID, FILL_DATE_DIM_ID, FILL_TIME_DIM_ID, AUTHORITY_TERMINAL_DIM_ID, APPLY_TO_CONSUMER_DIM_ID, ORIG_TRAN_DIM_ID, ORIG_AUTH_DETAIL_DIM_ID, UNIT_PRICE, PRODUCT_SERVICE_QUANTITY, PROCESS_FEE_AMOUNT, DURATION_SECONDS_FRACTION, TRAN_COUNT_FRACTION, CONVENIENCE_FEE_AMOUNT, LOYALTY_DISCOUNT_AMOUNT, FILL_TS, SOURCE_LEDGER_ID, SETTLED_DATE_DIM_ID, SETTLED_TIME_DIM_ID, PAYMENT_DIM_ID, PAYMENT_BATCH_DIM_ID, PAID_DATE_DIM_ID, QUANTITY, AMOUNT, PAID_TS, ITEM_TYPE_DIM_ID, DEVICE_DIM_ID, LOCATION_DIM_ID, POS_DIM_ID, ITEM_DATE_DIM_ID, ITEM_TIME_DIM_ID, UPLOAD_DATE_DIM_ID, UPLOAD_TIME_DIM_ID, DEVICE_CALL_DIM_ID, CURRENCY_DIM_ID, AUTH_TYPE_DIM_ID, AUTH_DETAIL_DIM_ID, CONSUMER_DIM_ID, ITEM_TS, REVISION)
					 VALUES(p_source_tran_line_item_id, p_source_tran_id, p_item_detail_dim_id, p_report_tran_id, p_source_system_cd, p_tran_line_item_dim_id, p_host_dim_id, p_fill_dim_id, p_fill_date_dim_id, p_fill_time_dim_id, p_authority_terminal_dim_id, p_apply_to_consumer_dim_id, p_orig_tran_dim_id, p_orig_auth_detail_dim_id, p_unit_price, p_product_service_quantity, p_process_fee_amount, p_duration_seconds_fraction, p_tran_count_fraction, p_convenience_fee_amount, p_loyalty_discount_amount, p_fill_ts, p_source_ledger_id, p_settled_date_dim_id, p_settled_time_dim_id, p_payment_dim_id, p_payment_batch_dim_id, p_paid_date_dim_id, p_quantity, p_amount, p_paid_ts, p_item_type_dim_id, p_device_dim_id, p_location_dim_id, p_pos_dim_id, p_item_date_dim_id, p_item_time_dim_id, p_upload_date_dim_id, p_upload_time_dim_id, p_device_call_dim_id, p_currency_dim_id, p_auth_type_dim_id, p_auth_detail_dim_id, p_consumer_dim_id, p_item_ts, p_revision)
					 RETURNING ITEM_FACT_ID
					      INTO p_item_fact_id;
					GET DIAGNOSTICS p_row_count = ROW_COUNT;
				EXCEPTION
					WHEN unique_violation THEN
						CONTINUE;
				END;
			END IF;
			RETURN;
		EXCEPTION
			WHEN serialization_failure THEN
				CONTINUE;
		END;
	END LOOP;
	RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

