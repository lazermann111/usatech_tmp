@@RDW_Tablespaces.SQL
@@RDW_User.SQL
@@APP_LOG_Tablespaces.sql
@@APP_LOG_User.sql
@@All_Objects.SQL
@@AK_INDEXES.SQL
@@"Create Indexes.sql"
@@InsertUnknownRecords.SQL
--@@RDW_LOADER_user.SQL
@@DATE_DIM_data.SQL
@@TIME_DIM_data.SQL

CREATE OR REPLACE FUNCTION SYSTEM.MIN_DATE   RETURN  DATE DETERMINISTIC
IS
--
-- Returns the minimum oracle date
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
-- B Krug       09-17-04 NEW
    l_min_date CONSTANT DATE := TO_DATE('01/01/-4712', 'MM/DD/SYYYY');
BEGIN 
    RETURN l_min_date;
END;

/

CREATE OR REPLACE FUNCTION SYSTEM.MAX_DATE   RETURN  DATE DETERMINISTIC
IS
--
-- Returns the maximum oracle date
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- B Krug       09-17-04 NEW
    l_max_date CONSTANT DATE := TO_DATE('12/31/9999', 'MM/DD/YYYY');
BEGIN 
    RETURN l_max_date;
END;

/

CREATE PUBLIC SYNONYM MAX_DATE FOR SYSTEM.MAX_DATE;
CREATE PUBLIC SYNONYM MIN_DATE FOR SYSTEM.MIN_DATE;

