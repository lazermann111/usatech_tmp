/*
SELECT 'CREATE TABLESPACE RDW_DATA_' || NVL(to_char(year), 'MISC') || chr(13)
  || 'DATAFILE ''' || data_dir||'/rdw_data_'|| NVL(to_char(year), 'misc') || '.dbf''' || CHR(13)
  || 'SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;'  || CHR(13)
  || 'ALTER DATABASE DATAFILE ''' || data_dir||'/rdw_data_'|| NVL(to_char(year), 'misc') || '.dbf'' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;'  || CHR(13)
  || 'CREATE TABLESPACE RDW_INDX_' || NVL(to_char(year), 'MISC') || chr(13)
  || 'DATAFILE ''' || index_dir||'/rdw_indx_'|| NVL(to_char(year), 'misc') || '.dbf''' || CHR(13)
  || 'SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;'  || CHR(13)
  || 'ALTER DATABASE DATAFILE ''' || index_dir||'/rdw_indx_'|| NVL(to_char(year), 'misc') || '.dbf'' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;'  || CHR(13)

  FROM
(SELECT 2001 + LEVEL year FROM DUAL CONNECT BY LEVEL <= 9 union ALL select null from dual),
(SELECT '+USARDB_DATA_01/usardb/datafile' data_dir, '+USARDB_INDX_01/usardb/datafile' index_dir from dual)
--*/


CREATE TABLESPACE RDW_DATA_2002
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2002.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2002.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2002
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2002.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2002.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2003
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2003.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2003.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2003
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2003.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2003.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2004
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2004.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2004.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2004
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2004.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2004.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2005
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2005.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2005.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2005
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2005.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2005.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2006
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2006.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2006.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2006
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2006.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2006.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2007
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2007.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2007.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2007
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2007.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2007.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2008
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2008.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2008.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2008
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2008.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2008.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2009
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2009.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2009.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2009
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2009.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2009.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_2010
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2010.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_2010.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_2010
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2010.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_2010.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;

CREATE TABLESPACE RDW_DATA_MISC
DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_misc.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_DATA_01/usardb/datafile/rdw_data_misc.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;
CREATE TABLESPACE RDW_INDX_MISC
DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_misc.dbf'
SIZE 50M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '+USARDB_INDX_01/usardb/datafile/rdw_indx_misc.dbf' AUTOEXTEND ON NEXT 1024K MAXSIZE 6144M;



