BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"ENGINE"."DEVICE FLOODING PROTECTION"',
job_type => 'PLSQL_BLOCK',
job_action => 'BEGIN ENGINE.DEVICE_FLOODING_PROTECTION(); END;',
repeat_interval => 'FREQ=MINUTELY',
start_date => to_timestamp_tz('2011-05-06 11:10:00 US/Eastern', 'YYYY-MM-DD HH24:MI:SS TZR'),
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'Detection and prevention of devices flooding the server',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"ENGINE"."DEVICE FLOODING PROTECTION"', attribute => 'job_weight', value => 1); 
sys.dbms_scheduler.set_attribute( name => '"ENGINE"."DEVICE FLOODING PROTECTION"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"ENGINE"."DEVICE FLOODING PROTECTION"' ); 
END;
/


BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"PSS"."CC DATA MASK"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
PSS.USAT_MASKCARDDATA();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=7;BYMINUTE=0;BYSECOND=0',
start_date => to_timestamp_tz('2011-05-04 US/Eastern', 'YYYY-MM-DD TZR'),
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'RFC0000328-PCI Mask Credit Card Data',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"PSS"."CC DATA MASK"', attribute => 'job_weight', value => 1); 
sys.dbms_scheduler.set_attribute( name => '"PSS"."CC DATA MASK"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"PSS"."CC DATA MASK"' ); 
END;
/

			
BEGIN
sys.dbms_scheduler.disable( '"SYSTEM"."DROP OLD PARTITIONS"' );
sys.dbms_scheduler.set_attribute( name => '"SYSTEM"."DROP OLD PARTITIONS"', attribute => 'job_action', value => 'BEGIN
SYSTEM.DROP_PARTITIONS_BEFORE(''RECON'', ''TRAN_COUNT_AUDIT'', SYSDATE - 120);
SYSTEM.DROP_PARTITIONS_BEFORE(''RECON'', ''TRAN_PAYMENT_AUDIT'', SYSDATE - 120);
SYSTEM.DROP_PARTITIONS_BEFORE(''REPORT'', ''WEB_REQUEST'', SYSDATE - 90);
SYSTEM.DROP_PARTITIONS_BEFORE(''REPORT'', ''MSG_LOG'', SYSDATE - 90);
DBADMIN.DROP_PARTITIONS_BEFORE(''ENGINE'', ''MACHINE_CMD_OUTBOUND_HIST'', SYSDATE - 30);
DBADMIN.DROP_PARTITIONS_BEFORE(''ENGINE'', ''MACHINE_CMD_INBOUND_HIST_OLD'', SYSDATE - 30);
DBADMIN.DROP_PARTITIONS_BEFORE(''ENGINE'', ''MACHINE_CMD_PENDING_HIST'', SYSDATE - 180);
DBADMIN.DROP_PARTITIONS_BEFORE(''ENGINE'', ''OB_EMAIL_QUEUE'', SYSDATE - 90);
END;
');
sys.dbms_scheduler.enable( '"SYSTEM"."DROP OLD PARTITIONS"' );
END;
/

BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"SYSTEM"."MV_ESUDS_SD_TRAN_REFRESH_JOB"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
PSS.PKG_REPORTS.UPDATE_MV_ESUDS_SETTLED_TRAN;
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=6;BYMINUTE=15;BYSECOND=0',
start_date => to_timestamp_tz('2008-03-03 EST5EDT', 'YYYY-MM-DD TZR'),
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'MV_ESUDS_SD_TRAN_REFRESH_JOB',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"SYSTEM"."MV_ESUDS_SD_TRAN_REFRESH_JOB"', attribute => 'job_weight', value => 1); 
sys.dbms_scheduler.enable( '"SYSTEM"."MV_ESUDS_SD_TRAN_REFRESH_JOB"' ); 
END;
/

BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"SYSTEM"."CREATE_ID_TAB_PARTS"',
job_type => 'PLSQL_BLOCK',
job_action => 'BEGIN 
dbadmin.usat_check_create_parts_by_id;
END;
',
repeat_interval => 'FREQ=DAILY;BYHOUR=6;BYMINUTE=5;BYSECOND=0',
start_date => to_timestamp_tz('2009-10-28 EST5EDT', 'YYYY-MM-DD TZR'),
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'CREATE_ID_TAB_PARTS',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"SYSTEM"."CREATE_ID_TAB_PARTS"', attribute => 'max_run_duration', value => numtodsinterval(10, 'minute')); 
sys.dbms_scheduler.set_attribute( name => '"SYSTEM"."CREATE_ID_TAB_PARTS"', attribute => 'raise_events', value => dbms_scheduler.job_sch_lim_reached); 
sys.dbms_scheduler.set_attribute( name => '"SYSTEM"."CREATE_ID_TAB_PARTS"', attribute => 'job_weight', value => 1); 
sys.dbms_scheduler.set_attribute( name => '"SYSTEM"."CREATE_ID_TAB_PARTS"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"SYSTEM"."CREATE_ID_TAB_PARTS"' ); 
END;
/


			