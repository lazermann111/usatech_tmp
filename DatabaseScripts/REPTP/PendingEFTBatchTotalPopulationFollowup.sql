DECLARE	
	LN_CNT NUMBER := 0;

	CURSOR cur IS 
		SELECT b.batch_id
		FROM corp.doc d
		JOIN corp.batch b ON d.doc_id = b.doc_id 
		WHERE d.status IN ('O', 'L', 'A')
		ORDER BY b.batch_id DESC;
BEGIN
	DBMS_OUTPUT.put_line('Script is starting...');
	
	FOR rec_cur IN cur LOOP
		--DBMS_OUTPUT.put_line('Processing batch_id ' || rec_cur.batch_id);
			
		UPDATE CORP.BATCH
		SET END_DATE = END_DATE
		WHERE BATCH_ID = rec_cur.batch_id;		
		
		COMMIT;
		LN_CNT := LN_CNT + 1;
	END LOOP;
		
	DBMS_OUTPUT.put_line('Script finished, processed ' || LN_CNT || ' batches.');
END;