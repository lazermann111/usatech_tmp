DECLARE
    CURSOR l_cur(ld_month DATE) IS
        SELECT TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, CURRENCY_ID, COUNT(*) CNT, MIN(X.TRANS_STAT_BY_DAY_ID) MIN_TRANS_STAT_BY_DAY_ID
          FROM REPORT.TRANS_STAT_BY_DAY X
         WHERE TRAN_DATE >= ld_month AND TRAN_DATE < ADD_MONTHS(ld_month, 1)
         GROUP BY TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, CURRENCY_ID
         HAVING COUNT(*) > 1;
    ld_start_month DATE;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT TRUNC(MIN(TRAN_DATE), 'MONTH')
      INTO ld_start_month
      FROM REPORT.TRANS_STAT_BY_DAY;
    WHILE ld_start_month < TRUNC(SYSDATE, 'MONTH') LOOP
        DBMS_OUTPUT.PUT('Checking Month ' || TO_CHAR(ld_start_month, 'MON, YYYY') || '...');
        ln_cnt := 0;
        FOR l_rec IN l_cur(ld_start_month) LOOP
            UPDATE REPORT.EPORT SET EPORT_ID = EPORT_ID WHERE EPORT_ID = l_rec.EPORT_ID; -- hold a lock on eport
            DELETE FROM REPORT.TRANS_STAT_BY_DAY
             WHERE TRANS_STAT_BY_DAY_ID != l_rec.MIN_TRANS_STAT_BY_DAY_ID
               AND TERMINAL_ID = l_rec.TERMINAL_ID
               AND EPORT_ID = l_rec.EPORT_ID
               AND TRAN_DATE = l_rec.TRAN_DATE
               AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
               AND CURRENCY_ID = l_rec.CURRENCY_ID;
            UPDATE REPORT.TRANS_STAT_BY_DAY
               SET (TRAN_COUNT, VEND_COUNT, TRAN_AMOUNT) = (
                    SELECT COUNT(DISTINCT TRAN_ID), SUM(QUANTITY), SUM(TOTAL_AMOUNT)
                      FROM REPORT.ACTIVITY_REF
                     WHERE QUANTITY > 0
                       AND TERMINAL_ID = l_rec.TERMINAL_ID
                       AND EPORT_ID = l_rec.EPORT_ID
                       AND TRUNC(TRAN_DATE, 'DD') = l_rec.TRAN_DATE
                       AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                       AND CURRENCY_ID = l_rec.CURRENCY_ID)
             WHERE TRANS_STAT_BY_DAY_ID = l_rec.MIN_TRANS_STAT_BY_DAY_ID;
            COMMIT; 
            ln_cnt := ln_cnt + 1;
        END LOOP;
        DBMS_OUTPUT.PUT_LINE(ln_cnt);
        ld_start_month := ADD_MONTHS(ld_start_month, 1);
    END LOOP;
END;
/