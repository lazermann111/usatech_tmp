SET DEFINE OFF;

UPDATE REPORT.TERMINAL_ADDR
   SET STATE = STATE || ' - CANADA'
 WHERE COUNTRY_CD = 'CA';
       
COMMIT;
   
ALTER TABLE REPORT.TERMINAL_ADDR MODIFY(COUNTRY_CD NULL);

UPDATE CORP.CUSTOMER_ADDR
   SET STATE = STATE || ' - CANADA'
 WHERE COUNTRY_CD = 'CA';
       
COMMIT;

ALTER TABLE CORP.CUSTOMER_ADDR MODIFY(COUNTRY_CD NULL);

ALTER TABLE REPORT.REPORT_REQUEST MODIFY(COMPRESSED_FLAG NULL);
SET DEFINE OFF;
          

MERGE INTO WEB_CONTENT.LITERAL T
USING (SELECT '' LITERAL_KEY, '' LITERAL_VALUE, 0 SUBDOMAIN_ID FROM DUAL WHERE 1=0
UNION ALL SELECT 'device.update.prompt.eachrow', 'Add a row to your file for each device you wish to update with the appropriate information in each column.', NULL FROM DUAL
UNION ALL SELECT 'device.update.prompt.updatefile', 'Click the Browse button and select the file you just created:', NULL FROM DUAL
UNION ALL SELECT 'main-title', 'Sony PictureStation Reporting', 2 FROM DUAL
UNION ALL SELECT 'app-stylesheets', 'css/usalive-app-style.css,css/sony-app-style.css', 2 FROM DUAL
UNION ALL SELECT 'main-title', 'Sony PictureStation Reporting', 3 FROM DUAL
UNION ALL SELECT 'app-stylesheets', 'css/usalive-app-style.css,css/sony-app-style.css', 3 FROM DUAL
UNION ALL SELECT 'report-please-wait', 'Your report is being generated. Depending on your selection it may take a while to complete. You may view the report later by selecting the "Pick up later" button or cancel the report by selecting the "Cancel" button.', NULL FROM DUAL
UNION ALL SELECT 'device.update.prompt.note', ' ', NULL FROM DUAL
UNION ALL SELECT 'main-stylesheets', 'css/sony-general-style.css', 2 FROM DUAL
UNION ALL SELECT 'main-stylesheets', 'css/sony-general-style.css', 3 FROM DUAL
) S ON (T.LITERAL_KEY = S.LITERAL_KEY AND ((T.SUBDOMAIN_ID IS NULL AND S.SUBDOMAIN_ID IS NULL) OR (S.SUBDOMAIN_ID = T.SUBDOMAIN_ID)))
WHEN MATCHED THEN UPDATE SET T.LITERAL_VALUE = S.LITERAL_VALUE 
    WHERE S.LITERAL_VALUE IS NOT NULL AND T.LITERAL_VALUE != S.LITERAL_VALUE
    DELETE WHERE S.LITERAL_VALUE IS NULL
WHEN NOT MATCHED THEN INSERT(SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
    VALUES(S.SUBDOMAIN_ID, S.LITERAL_KEY, S.LITERAL_VALUE);

DELETE FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WHERE REQUIREMENT_ID IN(23,26,27);
DELETE FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID IN(23,26,27);

UPDATE WEB_CONTENT.WEB_LINK 
   SET WEB_LINK_USAGE = '-'
 WHERE WEB_LINK_USAGE = 'M'
   AND WEB_LINK_URL LIKE '%update_devices_instructs.i?singleBankAcct=false' 
   AND WEB_LINK_LABEL = 'Mass Device Update' 
   AND WEB_LINK_GROUP = 'Administration';
   
UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_USAGE = 'M'
 WHERE WEB_LINK_LABEL = 'Regions'
   AND WEB_LINK_USAGE = 'm';
   
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_login.gif&requestHandler=login' WHERE WEB_LINK_ID = 100;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_menu.gif&requestHandler=my' WHERE WEB_LINK_ID = 110;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=about' WHERE WEB_LINK_ID = 115;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=contact' WHERE WEB_LINK_ID = 120;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_activity.gif&requestHandler=activity_report_list' WHERE WEB_LINK_ID = 125;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_activity.gif&requestHandler=tabs&usage=B' WHERE WEB_LINK_ID = 130;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_diagnostics.gif&requestHandler=diagnostics' WHERE WEB_LINK_ID = 135;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_payment.gif&requestHandler=payments' WHERE WEB_LINK_ID = 140;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_terminals.gif&requestHandler=terminals' WHERE WEB_LINK_ID = 145;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=user_admin.i' WHERE WEB_LINK_ID = 150;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=user_admin.i' WHERE WEB_LINK_ID = 151;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=user_report_frame.i' WHERE WEB_LINK_ID = 155;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=alert_admin' WHERE WEB_LINK_ID = 160;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=regions' WHERE WEB_LINK_ID = 165;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=customer_admin' WHERE WEB_LINK_ID = 170;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_payment.gif&requestHandler=new_bank_acct' WHERE WEB_LINK_ID = 180;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=license_instructions' WHERE WEB_LINK_ID = 185;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=eft_auth_instructions' WHERE WEB_LINK_ID = 190;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=WO_TERMINALS&border=true' WHERE WEB_LINK_ID = 195;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=tabs&usage=N' WHERE WEB_LINK_ID = 200;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=preferences' WHERE WEB_LINK_ID = 205;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=choose_date_range&border=true&_requestHandler=cc_settled_detail' WHERE WEB_LINK_ID = 210;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=choose_date_range&_requestHandler=cc_unsettled_detail&border=true' WHERE WEB_LINK_ID = 215;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?_requestHandler=settlement_batches&requestHandler=choose_date_range&border=true' WHERE WEB_LINK_ID = 225;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=choose_date_range&_requestHandler=revenue_summary&border=true' WHERE WEB_LINK_ID = 230;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=license_instructions' WHERE WEB_LINK_ID = 1174;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=new_terminal' WHERE WEB_LINK_ID = 1175;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=choose_date_range&_requestHandler=refund_report&border=true' WHERE WEB_LINK_ID = 220;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=query&sql=SELECT%20EPORT_SERIAL_NUM%20%22e-Port%22%2C%20LOCATION_NAME%20%22Location%22%2C%20TO_CHAR(LAST_DEX_DATE%2C%20'MM%2FDD%2FYYYY%20HH%3AMI%3ASS%20AM')%20%22Last%20DEX%20Dump%22%20FROM%0D%0AEPORT%20E%2C%20LOCATION%20L%2C%20VW_TERMINAL_EPORT%20TE%2C%20TERMINAL%20T%2C%20%0D%0A(SELECT%20MAX(DEX_DATE)%20LAST_DEX_DATE%2C%20EPORT_ID%20FROM%20G4OP.DEX_FILE%20GROUP%20BY%20EPORT_ID)%20D%0D%0AWHERE%20E.EPORT_ID%20%3D%20D.EPORT_ID%20AND%20E.EPORT_ID%20%3D%20TE.EPORT_ID%20(%2B)%20AND%20TE.TERMINAL_ID%20%3D%20T.TERMINAL_ID%20(%2B)%0D%0AAND%20T.LOCATION_ID%20%3D%20L.LOCATION_ID%20(%2B)%20ORDER%20BY%20EPORT_SERIAL_NUM%2C%20LOCATION_NAME&title=Last%20DEX%20Dump&showSQL=false' WHERE WEB_LINK_ID = 235;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './i?requestHandler=since_last_fill' WHERE WEB_LINK_ID = 89;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=link_menu.i&usage=I' WHERE WEB_LINK_ID = 1315;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=chargeback_search.i&folioId=83' WHERE WEB_LINK_ID = 1316;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=refund_search.i&folioId=81' WHERE WEB_LINK_ID = 1317;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=maintain_folios.i' WHERE WEB_LINK_ID = 143;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=update_devices_instructs.i&singleBankAcct=true' WHERE WEB_LINK_ID = 1341;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&bannerImage=/images/banner_terminals.gif&requestHandler=new_terminal' WHERE WEB_LINK_ID = 175;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=update_devices_instructs.i&singleBankAcct=false' WHERE WEB_LINK_ID = 1342;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=run_report.i&folioId=493' WHERE WEB_LINK_ID = 11467;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=login' WHERE WEB_LINK_ID = 2100;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=logout' WHERE WEB_LINK_ID = 2105;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=my' WHERE WEB_LINK_ID = 2110;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=activity_report_list' WHERE WEB_LINK_ID = 2125;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=tabs&usage=B' WHERE WEB_LINK_ID = 2130;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=sony_alerts_frame.i' WHERE WEB_LINK_ID = 2135;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=payments' WHERE WEB_LINK_ID = 2140;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=terminals' WHERE WEB_LINK_ID = 2145;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=user_admin' WHERE WEB_LINK_ID = 2150;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=i&requestHandler=user_admin' WHERE WEB_LINK_ID = 2151;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=select_date_range_frame.i&folioId=428' WHERE WEB_LINK_ID = 2200;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=sony_printer_info.i' WHERE WEB_LINK_ID = 2205;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=remote_file_update.i' WHERE WEB_LINK_ID = 2210;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=sony_alert_register.i' WHERE WEB_LINK_ID = 2215;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=terminal_customers.i&bannerImage=/images/banner_terminals.gif' WHERE WEB_LINK_ID = 146;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=terminal_customers.i' WHERE WEB_LINK_ID = 2146;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=payment_list.i&folioId=635' WHERE WEB_LINK_ID = 250;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=payment_list.i&bannerImage=/images/banner_payment.gif&folioId=635' WHERE WEB_LINK_ID = 255;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './frame.i?content=report_request_history.i&isByProfile=true' WHERE WEB_LINK_ID = 26117;

UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './activity_parameters.i?fragment=true' WHERE WEB_LINK_ID = 10;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './activity_ext_parameters.i?fragment=true' WHERE WEB_LINK_ID = 11;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './activity_summary_parameters.i?fragment=true' WHERE WEB_LINK_ID = 12;
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_URL = './activity_graph_parameters.i?fragment=true' WHERE WEB_LINK_ID = 13;

DELETE FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WHERE WEB_LINK_ID IN(SELECT WEB_LINK_ID FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_URL = 'new_password.i');
DELETE FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_URL = 'new_password.i';

COMMIT;

SET DEFINE OFF;
UPDATE FOLIO_CONF.FOLIO_PILLAR 
   SET ACTION_FORMAT = REPLACE(ACTION_FORMAT, '&day=', '&Tran_Date_Day=')
 WHERE ACTION_FORMAT LIKE '%activity_detail.i%';

COMMIT;

DELETE FROM REPORT.CCS_TRANSPORT_PROPERTY_TYPE WHERE CCS_TRANSPORT_TYPE_ID = 7 AND CCS_TPT_NAME in('Username', 'Password');

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
  SELECT DISTINCT USER_ID, 10
    FROM REPORT.USER_PRIVS
   WHERE PRIV_ID = 21;
   
DELETE FROM REPORT.USER_PRIVS WHERE PRIV_ID = 21;
DELETE FROM REPORT.PRIV WHERE PRIV_ID = 21;
 
DELETE FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WHERE REQUIREMENT_ID IN(21,22,24,25);
DELETE FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID IN(21,22,24,25);

INSERT INTO WEB_CONTENT.REQUIREMENT (REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_DESCRIPTION, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    VALUES(21, 'REQ_READONLY', null, 'com.usatech.usalive.link.UserReadOnlyRequirement', 'java.lang.Boolean', 'true');
    
INSERT INTO WEB_CONTENT.REQUIREMENT (REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_DESCRIPTION, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    VALUES(22, 'REQ_NOT_READONLY', null, 'com.usatech.usalive.link.UserReadOnlyRequirement', 'java.lang.Boolean', 'false');

INSERT INTO WEB_CONTENT.REQUIREMENT (REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_DESCRIPTION, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    VALUES(24, 'REQ_ORIGINAL_LOGIN', null, 'com.usatech.usalive.link.IsOriginalLoginRequirement', 'java.lang.Boolean', 'true');

INSERT INTO WEB_CONTENT.REQUIREMENT (REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_DESCRIPTION, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    VALUES(25, 'REQ_NOT_ORIGINAL_LOGIN', null, 'com.usatech.usalive.link.IsOriginalLoginRequirement', 'java.lang.Boolean', 'false');

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
     SELECT WEB_LINK_ID, 22 
       FROM WEB_CONTENT.WEB_LINK 
      WHERE WEB_LINK_USAGE = 'M' 
        AND WEB_LINK_LABEL IN('Preferences', 'Mass Device Update', 'Remote File Update', 'New Bank Acct', 'Activate Device');
 
DELETE FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_URL = 'un_be_user.i';