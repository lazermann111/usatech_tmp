SELECT
--ts.state_label settle_state,
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
) in_eft,
TRUNC(lts.entry_date, 'MONTH') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description

FROM (
    SELECT
    led.batch_id,
    led.amount,
    led.entry_type,
    led.settle_state_id,
    led.create_date,
    led.entry_date,
    led.description,
    sfi.fee_name
    FROM corp.ledger led
    LEFT OUTER JOIN report.trans tr
    ON tr.tran_id = led.trans_id
    LEFT OUTER JOIN (
        SELECT 
        sf.service_fee_id,
        f.fee_name
        FROM corp.service_fees sf
        JOIN corp.fees f
        ON f.fee_id = sf.fee_id
    ) sfi
    ON sfi.service_fee_id = led.service_fee_id
    WHERE led.entry_type IN ('SF', 'AD', 'SB') --(led.trans_id IS NULL OR led.entry_type = 'SF') -- this is the filter to get any entry not associated to a transaction
    AND led.deleted = 'N'
    AND led.settle_state_id IN (2, 3)
    AND TRUNC(led.create_date, 'MONTH') = ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'), -1)
)lts

JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct

--WHERE bat.payment_schedule_id != 4 -- is not a montly payment schedule

GROUP BY
--ts.state_label,
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
),
TRUNC(lts.entry_date, 'MONTH'),
lts.description
