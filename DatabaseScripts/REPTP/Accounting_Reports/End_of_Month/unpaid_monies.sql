SELECT
l.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
TRUNC(DECODE (l.entry_type,
    'CC', t.settle_date,
    l.entry_date
), 'MONTH') month_for,
COUNT(1) tran_count,
SUM (l.amount) total_ledger_amount
FROM corp.ledger l
JOIN corp.batch bat
ON bat.batch_id = l.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = l.settle_state_id -- yes, this is non intuitive but correct

LEFT OUTER JOIN report.trans t
ON t.tran_id = l.trans_id

 
WHERE l.deleted = 'N'
AND d.status != 'D'
AND l.settle_state_id IN (2, 3)
AND NVL(d.sent_date, MAX_DATE) >= TRUNC(SYSDATE, 'MONTH')  -- eft sent date (this is the master, shows truly paid or not)
AND DECODE (l.entry_type,
    'CC', t.settle_date,
    l.create_date
) < TRUNC(SYSDATE, 'MONTH')


GROUP BY 
l.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
TRUNC(DECODE (l.entry_type,
    'CC', t.settle_date,
    l.entry_date
), 'MONTH') 



