SELECT
l.entry_type,
fn.fee_name service_fee_name,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
cb.bank_acct_nbr,
cb.bank_routing_nbr,
cb.account_title,
d.doc_id eft_id,
d.sent_date eft_date,
SUM(l.amount) ledger_amount,
d.total_amount eft_total_amount
FROM corp.doc d
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id

JOIN corp.batch bat
ON bat.doc_id = d.doc_id

JOIN corp.ledger l
ON l.batch_id = bat.batch_id

LEFT OUTER JOIN (
	SELECT 
	sf.service_fee_id,
	f.fee_name
	FROM corp.service_fees sf
	JOIN corp.fees f
	ON f.fee_id = sf.fee_id
) fn
ON fn.service_fee_id = l.service_fee_id


WHERE TRUNC(d.sent_date, 'MONTH') = ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'), -1)
AND l.deleted = 'N'
AND l.settle_state_id IN (2, 3)


GROUP BY
l.entry_type,
fn.fee_name,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
cb.bank_acct_nbr,
cb.bank_routing_nbr,
cb.account_title,
d.doc_id,
d.sent_date,
d.total_amount 
