SELECT
e.eport_serial_num,
dt.device_type_desc,
tt.trans_type_name,
ts.state_label settle_state,
NVL2(cbk.customer_bank_id, 'T', 'TB') orphan_type,
NVL(ssy.source_system_name, '~ Legacy') source_system_name,
NVL(cr.currency_code, 'USD') currency_code,
TRUNC(NVL(tr.settle_date, tr.server_date), 'MONTH') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_amount
FROM report.trans tr
JOIN report.eport e
ON e.eport_id = tr.eport_id
JOIN report.device_type dt
ON dt.device_type_id = e.device_type_id

JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id -- yes, this is non intuituve but correct
LEFT OUTER JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
LEFT OUTER JOIN report.terminal ter
ON ter.terminal_id = tr.terminal_id
LEFT OUTER JOIN corp.customer_bank cbk
ON cbk.customer_bank_id = tr.customer_bank_id
LEFT OUTER JOIN report.source_system ssy
ON ssy.source_system_cd = tr.source_system_cd

WHERE NVL(tr.settle_date, tr.server_date) < TRUNC(SYSDATE, 'MONTH')
AND tr.trans_type_id IN (16, 19, 20)
AND tr.settle_state_id IN (2, 3) 
AND ter.terminal_id IS NULL
    
GROUP BY
e.eport_serial_num,
dt.device_type_desc,
tt.trans_type_name,
ts.state_label,
NVL2(cbk.customer_bank_id, 'T', 'TB'),
NVL(ssy.source_system_name, '~ Legacy'),
NVL(cr.currency_code, 'USD'),
TRUNC(NVL(tr.settle_date, tr.server_date), 'MONTH')

ORDER BY 8, 2, 3, 4, 5, 6, 7, 1
