SELECT 
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name,
NVL(dt.monthly_payment, 'UNK') monthly_payment,
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan') customer_name,
TRUNC(DECODE (led.entry_type,
    NULL, tr.settle_date,
    'CC', tr.settle_date,
    led.ledger_date
), 'MONTH') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount


FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id -- yes, this is non intuitive but correct


/* this is where we can potentially pickup orphans */
LEFT OUTER JOIN (
	SELECT
	t.terminal_id,
	c.customer_name,
	bu.business_unit_name,
	DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment
	FROM report.terminal t
	/* the following hard joins DO NOT reduce the result set - dbl chkd already */
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
) dt
ON dt.terminal_id = tr.terminal_id

/* this is where we can potentially pickup orphans */
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'

/* now our contraints */
WHERE tr.trans_type_id IN (16, 19, 20, 21) -- credit, debit, refund, chargeback
AND tr.settle_state_id IN (2, 3) -- we do not want declinded transactions
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF') -- We only want revenue entries in this report
AND TRUNC(DECODE (led.entry_type,
    NULL, tr.settle_date,
    'CC', tr.settle_date,
    led.create_date
), 'MONTH') = ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'), -1)

GROUP BY
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan'),
NVL(dt.monthly_payment, 'UNK'),
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan'),
TRUNC(DECODE (led.entry_type,
    NULL, tr.settle_date,
    'CC', tr.settle_date,
    led.ledger_date
), 'MONTH')

