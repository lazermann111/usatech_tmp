/*
SELECT *
  FROM G4OP.TMP_HURDLE
 WHERE EXPRESS IS NULL
*/
		   
DECLARE
    l_standard_fee_start_date DATE := TO_DATE('12/01/2005','MM/DD/YYYY');
	CURSOR l_cur IS
		SELECT --/*
               REPORT.TERMINAL_SEQ.NEXTVAL TERMINAL_ID,
               REPORT.LOCATION_SEQ.NEXTVAL LOCATION_ID, 
               'T0'|| TO_CHAR(REPORT.TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') TERMINAL_NBR, --*/
               T.name LOCATION_NAME, c.customer_id, c.customer_name, c.user_id,
			   t.amount HURDLE_FEE, t.start_date HURDLE_START_DATE, t.end_date HURDLE_END_DATE, 
               t.NET_REVENUE_PERCENT
	  	  FROM G4OP.TMP_HURDLE t, CORP.CUSTOMER C
		 WHERE c.customer_name LIKE 'Swank%'
           AND t.NAME LIKE 'Swank-Hilton Orl%';
BEGIN
    FOR l_rec IN l_cur LOOP
		INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, LOCATION_TYPE_ID, TERMINAL_ID)
	 	 	VALUES(l_rec.location_id, l_rec.location_name, 8, l_rec.terminal_id);
		 INSERT INTO REPORT.TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, 
	        CUSTOMER_ID, LOCATION_ID, PAYMENT_SCHEDULE_ID, FEE_CURRENCY_ID)
			 VALUES(l_rec.terminal_id, l_rec.terminal_nbr, l_rec.terminal_nbr, l_rec.customer_id, l_rec.location_id, 4, 1);
		 --INSERT INTO USER_TERMINAL
		 INSERT INTO REPORT.USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
			 VALUES(l_rec.user_id, l_rec.terminal_id, 'Y');
		   
		IF NVL(l_rec.HURDLE_FEE,0) > 0 THEN
			INSERT INTO CORP.SERVICE_FEES(
				TERMINAL_ID, 
				FEE_ID, 
				FEE_AMOUNT, 
				FREQUENCY_ID,
	       		LAST_PAYMENT, 
				START_DATE, 
				END_DATE, 
				SERVICE_FEE_ID,
	       		CREATE_DATE, 
				FEE_PERCENT)
			(SELECT
				l_rec.TERMINAL_ID,
				7,
				l_rec.HURDLE_FEE,
				2,
				LAST_DAY(ADD_MONTHS(TRUNC(l_rec.HURDLE_START_DATE), -1)),
				l_rec.HURDLE_START_DATE,
				l_rec.HURDLE_END_DATE,
				CORP.SERVICE_FEE_SEQ.NEXTVAL,
				SYSDATE,
				0
			FROM DUAL);
		END IF;
		/*
		IF NVL(l_rec.NET_REVENUE_PERCENT,0) > 0 THEN
			INSERT INTO CORP.SERVICE_FEES(
				TERMINAL_ID,
				FEE_ID,
				FEE_AMOUNT, 
				FREQUENCY_ID,
	       		LAST_PAYMENT, 
				START_DATE, 
				END_DATE, 
				SERVICE_FEE_ID,
	       		CREATE_DATE, 
				FEE_PERCENT)
			(SELECT
				l_rec.TERMINAL_ID,
				8,
				0,
				6,
				LAST_DAY(ADD_MONTHS(TRUNC(l_rec.SERVICE_FEE_START_DATE), -1)),
			   	l_rec.SERVICE_FEE_START_DATE,
				l_rec.SERVICE_FEE_END_DATE,
				CORP.SERVICE_FEE_SEQ.NEXTVAL,
				SYSDATE,
				l_rec.NET_REVENUE_PERCENT
			FROM DUAL);
        END IF; */
		 INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, 
		 		START_DATE, END_DATE, CUSTOMER_BANK_TERMINAL_ID)
		 		SELECT l_rec.terminal_id, cb_id, NULL, NULL, CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL
		 		  FROM (SELECT MAX(cb.CUSTOMER_BANK_ID) cb_id		   		
		   	 FROM CORP.CUSTOMER_BANK cb
		   	WHERE cb.customer_id = l_rec.customer_id
		   	GROUP BY l_rec.terminal_id
		   HAVING MAX(cb.CUSTOMER_BANK_ID) = MIN(cb.CUSTOMER_BANK_ID));
    END LOOP;
    --COMMIT;
END;
/

SELECT * FROM corp.service_fees sf, report.terminal t, corp.customer c, report.location l
WHERE sf.terminal_id = t.terminal_id AND t.customer_id = c.customer_id
AND t.location_id = l.location_id
AND l.location_name LIKE 'Swank-Hilton Orl%'
AND c.customer_name LIKE 'Swank%';

