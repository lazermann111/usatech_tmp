DECLARE
	CURSOR l_cur IS
		SELECT DISTINCT c.customer_name, c.customer_id 
		FROM corp.customer c, report.terminal t, report.terminal_eport te,
			 report.eport e
		WHERE c.customer_id = t.customer_id
		AND t.terminal_id = te.terminal_id
		AND te.eport_id = e.eport_id
		AND e.device_type_id = 10
		AND NOT EXISTS(SELECT 1 FROM report.user_login u WHERE u.customer_id = c.customer_id)
		AND TRIM(CUSTOMER_NAME) IS NOT NULL AND CUSTOMER_NAME <> 'USA'
    	ORDER BY CUSTOMER_NAME;
    l_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT REPORT.USER_LOGIN_SEQ.NEXTVAL 
          INTO l_user_id 
          FROM DUAL;
			  
	    INSERT INTO REPORT.USER_LOGIN (
				USER_ID, 
				USER_TYPE, 
				USER_NAME, 
				FIRST_NAME, 
				LAST_NAME, 
				USER_PASSWD, 
				EMAIL, 
				ADMIN_ID, 
				CUSTOMER_ID, 
				TELEPHONE, 
				FAX
			) (SELECT 
				l_user_id, 
				8, 
				CUSTOMER_NAME, 
				CUSTOMER_NAME, 
				NULL, 
				DBMS_RANDOM.string('X',8), 
				NULL, 
				0, 
				l_rec.customer_id, 
				NULL, 
				NULL
	        FROM CORP.CUSTOMER
	        WHERE CUSTOMER_ID = l_rec.customer_id);
	
	    INSERT INTO REPORT.USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
	        SELECT l_user_id, TERMINAL_ID, 'Y' FROM REPORT.TERMINAL WHERE CUSTOMER_ID = l_rec.customer_id;
	        
	    UPDATE CORP.CUSTOMER SET USER_ID = l_user_id WHERE CUSTOMER_ID = l_rec.customer_id;
	    
	    INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID) VALUES (l_user_id, 1);--User Administration
	    INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID) VALUES (l_user_id, 2);--Region Administration
	    INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID) VALUES (l_user_id, 3);--Bank Account Creation
	    INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID) VALUES (l_user_id, 7);--Edit all Terminals
	        
	    UPDATE REPORT.TERMINAL SET PAYMENT_SCHEDULE_ID = 4 WHERE CUSTOMER_ID = l_rec.customer_id;
	    
	    COMMIT;
	END LOOP;
END;
