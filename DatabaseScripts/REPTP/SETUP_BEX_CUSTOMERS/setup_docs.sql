/* =============================================================================
This procedure sets up the ledger entries for a new customer to the system

This procedure assumes the following starting conditions:
	- The customer has been set up correctly with a bank account, all
		terminals have been set up with that bank account, and all fees have
		been configured.

	- The sync process has run to create ledger entries for all transactions.

	- NOTE: The docs created by the sync process will be marked as deleted
		(status = 'D'). These docs will contain all old unwanted transactions.
============================================================================= */
DECLARE
    l_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE := -1;
    l_switchover_date DATE := TO_DATE('03/01/2006','MM/DD/YYYY');
	CURSOR l_ledger_cur IS
		SELECT l.LEDGER_ID, t.TERMINAL_ID, t.CURRENCY_ID, l.ENTRY_DATE
        FROM CORP.LEDGER l, REPORT.TRANS t, CORP.BATCH b, CORP.DOC d
        WHERE -- Entries tied to transactions use trans info
        	t.TRAN_ID = l.TRANS_ID AND 
			l.BATCH_ID = b.BATCH_ID AND
        	b.DOC_ID = d.DOC_ID AND
        	d.CUSTOMER_BANK_ID = l_customer_bank_id AND
        	((t.TRANS_TYPE_ID IN (17,18) AND (t.SERVER_DATE >= l_switchover_date)) OR
				(t.SETTLE_DATE >= l_switchover_date))
		UNION
        SELECT l.LEDGER_ID, b.TERMINAL_ID, d.CURRENCY_ID, l.ENTRY_DATE
        FROM CORP.LEDGER l, CORP.BATCH b, CORP.DOC d 
        WHERE -- Entries not tied to transactions use ledger info
        	l.TRANS_ID IS NULL AND
        	l.BATCH_ID = b.BATCH_ID AND
        	b.DOC_ID = d.DOC_ID AND
        	b.TERMINAL_ID IS NOT NULL AND
        	d.CUSTOMER_BANK_ID = l_customer_bank_id AND
			l.ENTRY_DATE >= l_switchover_date;
BEGIN
   	UPDATE CORP.DOC SET STATUS = 'D' WHERE CUSTOMER_BANK_ID = l_customer_bank_id;
       
    FOR l_ledger_rec IN l_ledger_cur LOOP
        UPDATE CORP.LEDGER SET BATCH_ID = CORP.PAYMENTS_PKG.GET_OR_CREATE_BATCH(
			l_ledger_rec.TERMINAL_ID,
			l_customer_bank_id,
			l_ledger_rec.ENTRY_DATE,
			l_ledger_rec.CURRENCY_ID,
			'N')
		WHERE LEDGER_ID = l_ledger_rec.LEDGER_ID;
    END LOOP;
    
    COMMIT;
END;
