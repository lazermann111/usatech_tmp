DECLARE
    CURSOR l_cur IS
        SELECT e.eport_id, dc.express, dc.LINK, dc.location
         FROM REPORT.EPORT E, G4OP.DEVICE_CONFIG@LEGACY DC
        WHERE e.device_type_id = 10
        AND e.eport_serial_num = dc.express
        AND e.eport_serial_num NOT LIKE 'EV%' 
        AND e.eport_serial_num NOT LIKE '10'
        AND NOT EXISTS(SELECT 1 FROM REPORT.TERMINAL_EPORT TE
        WHERE te.eport_id = e.eport_id);
    l_date DATE;
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT MIN(x.close_date)
          INTO l_date
          FROM report.trans x
         WHERE x.eport_id = l_rec.eport_id
           AND x.close_date > SYSDATE - 30;
        IF l_date IS NOT NULL THEN
            REPORT.DATA_IN_PKG.UPDATE_BEX_LOCATION(l_rec.express, l_rec.location, l_rec.LINK, l_date);
            -- add fees & cb
            
            DBMS_OUTPUT.PUT_LINE('Express ' || l_rec.express || ' was configured starting at ' || TO_CHAR(l_date, 'MM/DD/YYYY HH24:MI'));
        ELSE
            DBMS_OUTPUT.PUT_LINE('Express ' || l_rec.express || ' does not have any transactions in the last month');
        END IF;
        COMMIT; 
    END LOOP;
END;
/

