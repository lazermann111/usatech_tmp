DECLARE
	-- INPUTS
    l_license_received_by_user_id REPORT.USER_LOGIN.USER_ID%TYPE := 7; -- Brian: 7
    
    -- VARIABLES
    l_license_id CORP.LICENSE.LICENSE_ID%TYPE;
    l_license_number CORP.LICENSE_NBR.LICENSE_NBR%TYPE;
    l_license_description CORP.LICENSE.DESCRIPTION%TYPE := '';
    l_standard_fee_start_date DATE := TO_DATE('12/01/2005','MM/DD/YYYY');
	CURSOR l_fees_cur IS
		SELECT GREATEST(MIN(te.start_date), l_standard_fee_start_date) SERVICE_FEE_START_DATE,
			   DECODE(MAX(NVL(te.end_date,MAX_DATE)),MAX_DATE,NULL,MAX(NVL(te.end_date,MAX_DATE))) SERVICE_FEE_END_DATE,
			   t.TERMINAL_ID, f.EXPRESS, f.LOCATION, f.CANADA_IND, c.customer_id, c.customer_name,
			   f.CREDIT_PERCENT, f.ACCESS_PERCENT, f.DEBIT_PERCENT, f.INTERNET_FEE, 
			   f.ADMIN_FEE, f.HURDLE_FEE, f.HURDLE_START_DATE, f.HURDLE_END_DATE, 
               f.NET_REVENUE_PERCENT, RANK() OVER (PARTITION BY t.CUSTOMER_ID ORDER BY f.EXPRESS) num
	  	  FROM G4OP.TMP_TRANSACT_FEES f, REPORT.EPORT E, REPORT.TERMINAL_EPORT TE, REPORT.TERMINAL t,
	  	       CORP.CUSTOMER c
		 WHERE e.eport_serial_num = f.EXPRESS
		   AND e.eport_id = te.eport_id
		   AND te.terminal_id = t.terminal_id
		   AND t.CUSTOMER_ID = c.customer_id
		   AND c.customer_name <> 'USA'
		 GROUP BY t.customer_id, t.TERMINAL_ID, f.EXPRESS, f.LOCATION, f.CANADA_IND, 
		 		c.customer_id, c.customer_name,
			   f.CREDIT_PERCENT, f.ACCESS_PERCENT, f.DEBIT_PERCENT, f.INTERNET_FEE, 
			   f.ADMIN_FEE, f.HURDLE_FEE, f.HURDLE_START_DATE, f.HURDLE_END_DATE, 
               f.NET_REVENUE_PERCENT
		 ORDER BY f.EXPRESS;
BEGIN
    FOR l_fees_rec IN l_fees_cur LOOP
		l_license_description := '';
    
    	DBMS_OUTPUT.put_line('Setting fees for ' || l_fees_rec.EXPRESS);
    
    	-- Setup License
        IF l_fees_rec.NUM = 1 THEN
            SELECT CORP.LICENSE_SEQ.NEXTVAL INTO l_license_id FROM DUAL;
            
            INSERT INTO CORP.LICENSE (
                LICENSE_ID, 
                TITLE)
			(SELECT
                l_license_id, 
                'License for ' || NVL(l_fees_rec.CUSTOMER_NAME, 'Customer #' || l_fees_rec.customer_id)
               FROM DUAL);
	           
	        CORP.CREATE_LICENSE (
	           l_license_id,       --in
	           l_fees_rec.customer_id,          --in
	           l_license_number    --out
	        );
        	
        	INSERT INTO CORP.CUSTOMER_LICENSE (
                LICENSE_NBR,
                CUSTOMER_ID,
                RECEIVED,
                RECEIVE_BY)
        	(SELECT 
                l_license_number,
                l_fees_rec.customer_id,
                SYSDATE,
                l_license_received_by_user_id
            FROM DUAL);
        END IF;
    
    	IF NVL(l_fees_rec.CREDIT_PERCENT,1) < 1 THEN
	    	INSERT INTO CORP.PROCESS_FEES (
				TERMINAL_ID, 
				TRANS_TYPE_ID, 
				FEE_PERCENT, 
				START_DATE,
	       		END_DATE, 
				PROCESS_FEE_ID, 
				CREATE_DATE, 
				FEE_AMOUNT,
	       		MIN_AMOUNT)
	       	(SELECT 
				l_fees_rec.TERMINAL_ID,
			   	16, -- Credit
			   	1 - l_fees_rec.CREDIT_PERCENT,
			   	l_standard_fee_start_date,
			   	NULL,
			   	CORP.PROCESS_FEE_SEQ.NEXTVAL,
			   	SYSDATE,
			   	0,
			   	0
			FROM DUAL);
			
			IF (l_fees_rec.NUM = 1) THEN
                INSERT INTO CORP.LICENSE_PROCESS_FEES(
                    LICENSE_ID, 
                    TRANS_TYPE_ID, 
                    FEE_PERCENT, 
                    FEE_AMOUNT, 
                    MIN_AMOUNT
                ) VALUES (
                    l_license_id,
                    16,
                    1 - l_fees_rec.CREDIT_PERCENT,
                    0,
                    0
                );
                
                l_license_description := l_license_description || ' Credit: ' 
					|| TO_CHAR((1 - l_fees_rec.CREDIT_PERCENT)*100,'fm999') || '%';
			END IF;
		END IF;
		
		IF NVL(l_fees_rec.ACCESS_PERCENT,1) < 1 THEN
	    	INSERT INTO CORP.PROCESS_FEES (
				TERMINAL_ID, 
				TRANS_TYPE_ID, 
				FEE_PERCENT, 
				START_DATE,
	       		END_DATE, 
				PROCESS_FEE_ID, 
				CREATE_DATE, 
				FEE_AMOUNT,
	       		MIN_AMOUNT)
	       	(SELECT 
				l_fees_rec.TERMINAL_ID,
			   	17, -- Access
			   	1 - l_fees_rec.ACCESS_PERCENT,
			   	l_standard_fee_start_date,
			   	NULL,
			   	CORP.PROCESS_FEE_SEQ.NEXTVAL,
			   	SYSDATE,
			   	0,
			   	0
			FROM DUAL);
			
			IF (l_fees_rec.NUM = 1) THEN
                INSERT INTO CORP.LICENSE_PROCESS_FEES(
                    LICENSE_ID, 
                    TRANS_TYPE_ID, 
                    FEE_PERCENT, 
                    FEE_AMOUNT, 
                    MIN_AMOUNT
                ) VALUES (
                    l_license_id,
                    17,
                    1 - l_fees_rec.ACCESS_PERCENT,
                    0,
                    0
                );
                
                l_license_description := l_license_description || ' Access: ' 
					|| TO_CHAR((1 - l_fees_rec.ACCESS_PERCENT)*100,'fm999') || '%';
			END IF;
		END IF;
		
		IF NVL(l_fees_rec.DEBIT_PERCENT,1) < 1 THEN
	    	INSERT INTO CORP.PROCESS_FEES (
				TERMINAL_ID, 
				TRANS_TYPE_ID, 
				FEE_PERCENT, 
				START_DATE,
	       		END_DATE, 
				PROCESS_FEE_ID, 
				CREATE_DATE, 
				FEE_AMOUNT,
	       		MIN_AMOUNT)
	       	(SELECT 
				l_fees_rec.TERMINAL_ID,
			   	19, -- Debit
			   	1 - l_fees_rec.DEBIT_PERCENT,
			   	l_standard_fee_start_date,
			   	NULL,
			   	CORP.PROCESS_FEE_SEQ.NEXTVAL,
			   	SYSDATE,
			   	0,
			   	0
			FROM DUAL);
			
			IF (l_fees_rec.NUM = 1) THEN
                INSERT INTO CORP.LICENSE_PROCESS_FEES(
                    LICENSE_ID, 
                    TRANS_TYPE_ID, 
                    FEE_PERCENT, 
                    FEE_AMOUNT, 
                    MIN_AMOUNT
                ) VALUES (
                    l_license_id,
                    19,
                    1 - l_fees_rec.DEBIT_PERCENT,
                    0,
                    0
                );
                
                l_license_description := l_license_description || ' Debit: ' 
					||  TO_CHAR((1 - l_fees_rec.DEBIT_PERCENT)*100,'fm999') || '%';
			END IF;
		END IF;
		
		IF NVL(l_fees_rec.INTERNET_FEE,0) > 0 THEN
			INSERT INTO CORP.SERVICE_FEES(
				TERMINAL_ID, 
				FEE_ID, 
				FEE_AMOUNT, 
				FREQUENCY_ID,
	       		LAST_PAYMENT, 
				START_DATE, 
				END_DATE, 
				SERVICE_FEE_ID,
	       		CREATE_DATE, 
				FEE_PERCENT)
			(SELECT
				l_fees_rec.TERMINAL_ID,
				5,
				l_fees_rec.INTERNET_FEE,
				2,
				LAST_DAY(ADD_MONTHS(TRUNC(l_fees_rec.SERVICE_FEE_START_DATE), -1)),
			   	l_fees_rec.SERVICE_FEE_START_DATE,
				l_fees_rec.SERVICE_FEE_END_DATE,
				CORP.SERVICE_FEE_SEQ.NEXTVAL,
				SYSDATE,
				0
			FROM DUAL);
			
			IF (l_fees_rec.NUM = 1) THEN
                INSERT INTO CORP.LICENSE_SERVICE_FEES (
					LICENSE_ID, 
					FEE_ID, 
					AMOUNT, 
					FREQUENCY_ID
				) VALUES (
					l_license_id, 
					5, 
					l_fees_rec.INTERNET_FEE, 
					2
				);
                
                l_license_description := l_license_description || ' Internet Fee: ' 
					||  TO_CHAR(l_fees_rec.INTERNET_FEE,'fm$9,999.00');
			END IF;
		END IF;
		
		IF NVL(l_fees_rec.ADMIN_FEE,0) > 0 THEN
			INSERT INTO CORP.SERVICE_FEES(
				TERMINAL_ID, 
				FEE_ID, 
				FEE_AMOUNT, 
				FREQUENCY_ID,
	       		LAST_PAYMENT, 
				START_DATE, 
				END_DATE, 
				SERVICE_FEE_ID,
	       		CREATE_DATE, 
				FEE_PERCENT)
			(SELECT
				l_fees_rec.TERMINAL_ID,
				6,
				l_fees_rec.ADMIN_FEE,
				2,
				LAST_DAY(ADD_MONTHS(TRUNC(l_fees_rec.SERVICE_FEE_START_DATE), -1)),
			   	l_fees_rec.SERVICE_FEE_START_DATE,
				l_fees_rec.SERVICE_FEE_END_DATE,
				CORP.SERVICE_FEE_SEQ.NEXTVAL,
				SYSDATE,
				0
			FROM DUAL);
			
			IF (l_fees_rec.NUM = 1) THEN
                INSERT INTO CORP.LICENSE_SERVICE_FEES (
					LICENSE_ID, 
					FEE_ID, 
					AMOUNT, 
					FREQUENCY_ID
				) VALUES (
					l_license_id, 
					6, 
					l_fees_rec.ADMIN_FEE, 
					2
				);
                
                l_license_description := l_license_description || ' Admin Fee: '
					||  TO_CHAR(l_fees_rec.ADMIN_FEE,'fm$9,999.00');
			END IF;
		END IF;
		
		IF NVL(l_fees_rec.HURDLE_FEE,0) > 0 THEN
			INSERT INTO CORP.SERVICE_FEES(
				TERMINAL_ID, 
				FEE_ID, 
				FEE_AMOUNT, 
				FREQUENCY_ID,
	       		LAST_PAYMENT, 
				START_DATE, 
				END_DATE, 
				SERVICE_FEE_ID,
	       		CREATE_DATE, 
				FEE_PERCENT)
			(SELECT
				l_fees_rec.TERMINAL_ID,
				7,
				l_fees_rec.HURDLE_FEE,
				2,
				LAST_DAY(ADD_MONTHS(TRUNC(l_fees_rec.HURDLE_START_DATE), -1)),
				l_fees_rec.HURDLE_START_DATE,
				l_fees_rec.HURDLE_END_DATE,
				CORP.SERVICE_FEE_SEQ.NEXTVAL,
				SYSDATE,
				0
			FROM DUAL);
		END IF;
		
		IF NVL(l_fees_rec.NET_REVENUE_PERCENT,0) > 0 THEN
			INSERT INTO CORP.SERVICE_FEES(
				TERMINAL_ID,
				FEE_ID,
				FEE_AMOUNT, 
				FREQUENCY_ID,
	       		LAST_PAYMENT, 
				START_DATE, 
				END_DATE, 
				SERVICE_FEE_ID,
	       		CREATE_DATE, 
				FEE_PERCENT)
			(SELECT
				l_fees_rec.TERMINAL_ID,
				8,
				0,
				6,
				LAST_DAY(ADD_MONTHS(TRUNC(l_fees_rec.SERVICE_FEE_START_DATE), -1)),
			   	l_fees_rec.SERVICE_FEE_START_DATE,
				l_fees_rec.SERVICE_FEE_END_DATE,
				CORP.SERVICE_FEE_SEQ.NEXTVAL,
				SYSDATE,
				l_fees_rec.NET_REVENUE_PERCENT
			FROM DUAL);
			
			IF (l_fees_rec.NUM = 1) THEN
				INSERT INTO CORP.LICENSE_SERVICE_FEES (
					LICENSE_ID, 
					FEE_ID, 
					AMOUNT, 
					FREQUENCY_ID
				) VALUES (
					l_license_id, 
					8, 
					l_fees_rec.NET_REVENUE_PERCENT, 
					6
				);
                
                l_license_description := l_license_description || ' Net Revenue Fee: ' 
					||  TO_CHAR(l_fees_rec.NET_REVENUE_PERCENT*100,'fm999') || '%';
			END IF;
		END IF;
		
		IF l_fees_rec.NUM = 1 THEN 
            UPDATE CORP.LICENSE 
				SET DESCRIPTION = l_license_description
			WHERE LICENSE_ID = l_license_id;
        END IF;
    END LOOP;
    
    COMMIT;
END;
