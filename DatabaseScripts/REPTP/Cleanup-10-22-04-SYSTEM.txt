DROP TABLE REPORT.G4OLD_DEVICE_COLUMN_MAP
/
DROP TABLE REPORT.G4OLD_LOAD_DATA
/
DROP TABLE CORP.G4OLD_CUSTOMER
/
DROP TABLE CORP.G4OLD_CUSTOMER_BANK
/
DROP TABLE CORP.G4OLD_CUSTOMER_BANK_DEVICE
/
DROP TABLE CORP.G4OLD_EFTS
/
DROP TABLE CORP.G4OLD_EMAIL
/
DROP SEQUENCE CORP.PAYMENT_SEQ
/
DROP SEQUENCE CORP.PAYMENT_SERVICE_FEE_SEQ
/
DROP SEQUENCE CORP.DEVICE_STATS_SEQ
/
DROP SEQUENCE CORP.TERMINAL_RULE_SEQ
/
DROP SEQUENCE CORP.USER_MAIL_SEQ
/
DROP SEQUENCE CORP.LEDGER_PAY_SEQ
/
DROP SEQUENCE CORP.ADJUST_SEQ
/
DROP SEQUENCE CORP.BATCH_ID_SEQ
/
DROP SEQUENCE CORP.DEVICE_SEQ
/
DROP SEQUENCE CORP.PRODUCT_SEQ
/

