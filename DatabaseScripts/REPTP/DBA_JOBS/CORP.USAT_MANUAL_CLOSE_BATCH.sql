grant select on USAT_CUSTOM.FILE_TRNSFR_CCE_FILL to corp;
grant update  on USAT_CUSTOM.CCE_FILL to corp;

CREATE OR REPLACE procedure CORP.USAT_MANUAL_CLOSE_BATCH as 
--Manual delete open fill batches daily as per RFC-000038
    l_cutoff_date_1 DATE := TRUNC(SYSDATE) - 6;
    CURSOR l_close_cur is
      SELECT BATCH_ID
        FROM CORP.BATCH
       WHERE BATCH_STATE_CD = 'O'
         AND END_DATE < l_cutoff_date_1
         AND PAYMENT_SCHEDULE_ID = 2;
    l_cutoff_date_2 DATE := TRUNC(SYSDATE) - 7;
    CURSOR l_match_cur IS
       SELECT B.BATCH_ID, TO_CHAR(l_cutoff_date_2, 'YYYY/MM/DD') CUTOFF_DATE_TEXT
         FROM CORP.BATCH B
         JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
        WHERE B.BATCH_STATE_CD = 'U'
          AND B.PAYMENT_SCHEDULE_ID = 2
          AND T.CUSTOMER_ID IN(2974,9955)
          AND B.END_DATE < l_cutoff_date_2;
BEGIN
    FOR l_close_rec IN l_close_cur LOOP
        INSERT INTO CORP.MANUAL_CLOSE_BATCH(BATCH_ID)
         VALUES(l_close_rec.BATCH_ID);
        UPDATE CORP.BATCH B
           SET B.BATCH_STATE_CD = (
                SELECT DECODE(COUNT(*), 0, 'D', 'U')
                  FROM REPORT.TERMINAL t
                  JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                 WHERE b.TERMINAL_ID = t.TERMINAL_ID
                   AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
               B.BATCH_CLOSED_TS = SYSDATE
          WHERE B.BATCH_ID = l_close_rec.BATCH_ID
            AND B.BATCH_STATE_CD = 'O';
        COMMIT;
    END LOOP;
    FOR l_match_rec IN l_match_cur LOOP
        CORP.SP_CONFIRM_BATCH(l_match_rec.BATCH_ID, 3, l_match_rec.CUTOFF_DATE_TEXT);
    END LOOP;

    UPDATE USAT_CUSTOM.CCE_FILL F SET F.CCE_FILL_STATE_ID = 8
     WHERE F.CCE_FILL_STATE_ID IN(3,6) AND F.BATCH_ID IS NULL
       AND F.CCE_FILL_DATE < l_cutoff_date_2
       AND NOT EXISTS(
          SELECT 1
            FROM USAT_CUSTOM.FILE_TRNSFR_CCE_FILL CFFT
WHERE F.CCE_FILL_ID = CFFT.CCE_FILL_ID
             AND CFFT.FILE_TRNSFR_CCE_FILL_TYPE_ID = 2);
    COMMIT;
END;
/

BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"CORP"."CLOSE_OPEN_FILL_BATCH"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
CORP.USAT_MANUAL_CLOSE_BATCH();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=4;BYMINUTE=0;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'Manual close open fill batches daily',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"CORP"."CLOSE_OPEN_FILL_BATCH"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"CORP"."CLOSE_OPEN_FILL_BATCH"' ); 
END;

/
