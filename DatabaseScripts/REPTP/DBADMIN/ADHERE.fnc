CREATE OR REPLACE FUNCTION DBADMIN.ADHERE (
    l_list IN VARCHAR2_TABLE,
    l_separator  IN VARCHAR2)
 RETURN VARCHAR2 IS
    l_str VARCHAR2(4000) := '';
BEGIN
     IF l_list.FIRST IS NOT NULL THEN
        FOR i IN l_list.FIRST..l_list.LAST LOOP
            IF l_list(i) IS NOT NULL THEN
               IF LENGTH(l_str) > 0 THEN
                  l_str := l_str || l_separator;
               END IF;
               IF LENGTH(l_str) + LENGTH(l_list(i)) + LENGTH(l_separator) > 3997 THEN
                  l_str := l_str || '...';
                  EXIT;
               ELSE
                  l_str := l_str || l_list(i);
               END IF;
            END IF;
        END LOOP;
    END IF;
    RETURN l_str;
END;
/
