CREATE OR REPLACE PROCEDURE DBADMIN.USAT_CHECK_CREATE_MONTHLY_PART AUTHID CURRENT_USER IS
--Created by Marie Njanje on 01/14/08 to check partitions daily
Begin
DECLARE
   CURSOR part_list_cursor IS
  select b.table_owner, b.table_name, partition_name, num_rows, high_value, DECODE(b.table_name, 'STORED_FILE', 50, 14) advance
    from dba_tab_partitions b,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in('SYS', 'SYSTEM','REPORT_ARCH','RDW') and table_name not like 'BIN$%'
    AND (K.COLUMN_NAME like '%DATE%' or  K.COLUMN_NAME like '%TS' or K.COLUMN_NAME like '%DAY')
    AND TABLE_NAME NOT IN ('MACHINE_CMD_OUTBOUND_HIST','MACHINE_CMD_INBOUND_HIST','APP_LOCK') AND T.TABLE_NAME=K.NAME AND TABLE_OWNER=K.OWNER
    group by table_owner, table_name ) a
    where a.part_pos = PARTITION_POSITION
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM');

     part_rec part_list_cursor%ROWTYPE;
     v_highest_value DATE;
BEGIN
     FOR part_rec IN part_list_cursor LOOP
        IF part_rec.HIGH_VALUE = 'MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(part_rec.table_owner)||'.'||upper(part_rec.table_name)||', please review table definition!');
        ELSE
            EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value;
            WHILE v_highest_value < SYSDATE + part_rec.advance LOOP
                DBMS_OUTPUT.PUT_LINE('Adding partition ' || TO_CHAR(v_highest_value, 'YYYY-MM-DD') || ' to table '||upper(part_rec.table_owner)||'.'||upper(part_rec.table_name));
                DBADMIN.ADD_MONTHLY_PARTITION(part_rec.table_owner, part_rec.table_name, v_highest_value - 1);
                v_highest_value := ADD_MONTHS(v_highest_value, 1);
            END LOOP;
       END IF;
    END LOOP;
END;
END;
/