CREATE OR REPLACE FUNCTION DBADMIN.MAX_DATE   RETURN  DATE DETERMINISTIC
IS
--
-- Returns the maximum oracle date
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- B Krug       09-17-04 NEW
    l_max_date CONSTANT DATE := TO_DATE('12/31/9999', 'MM/DD/YYYY');
BEGIN 
    RETURN l_max_date;
END;

/
