CREATE OR REPLACE FUNCTION DBADMIN.REGEXP_SPLIT   ( l_list IN VARCHAR2,
    l_regexp_separator IN VARCHAR2)
  RETURN VARCHAR2_TABLE IS
   l_tab VARCHAR2_TABLE := VARCHAR2_TABLE();
   l_start PLS_INTEGER := 1;
   l_end PLS_INTEGER;
BEGIN 
    LOOP
        l_end := REGEXP_INSTR(l_list, l_regexp_separator, l_start);
        IF l_end < 1 THEN
           l_tab.EXTEND;
           l_tab(l_tab.LAST) := SUBSTR(l_list, l_start);
           EXIT;
        END IF;
        l_tab.EXTEND;
        l_tab(l_tab.LAST) := SUBSTR(l_list, l_start, l_end - l_start);
        l_start := REGEXP_INSTR(l_list, l_regexp_separator, l_start, 1, 1);
    END LOOP;
    RETURN l_tab ;
END;
/
