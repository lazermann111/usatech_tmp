CREATE OR REPLACE FUNCTION DBADMIN.RESTRAIN(
    pd_date DATE,
    pd_min_date DATE,
    pd_max_date DATE)
    RETURN DATE
    DETERMINISTIC
    PARALLEL_ENABLE
IS
BEGIN
    IF pd_date IS NULL THEN
        RETURN pd_date;
    ELSIF pd_min_date IS NOT NULL AND pd_date < pd_min_date THEN
        RETURN NULL;
    ELSIF pd_max_date IS NOT NULL AND pd_date > pd_max_date THEN
        RETURN NULL;
    ELSE
        RETURN pd_date;
    END IF;
END;
/