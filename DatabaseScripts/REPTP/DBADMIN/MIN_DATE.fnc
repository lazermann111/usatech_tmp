CREATE OR REPLACE FUNCTION DBADMIN.MIN_DATE   RETURN  DATE DETERMINISTIC
IS
--
-- Returns the minimum oracle date
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
-- B Krug       09-17-04 NEW
    l_min_date CONSTANT DATE := TO_DATE('01/01/-4712', 'MM/DD/SYYYY');
BEGIN 
    RETURN l_min_date;
END;

/
