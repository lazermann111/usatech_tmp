CREATE OR REPLACE PROCEDURE DBADMIN."ADD_DAILY_PARTITION" (
          v_table_owner VARCHAR2,
          v_table_name VARCHAR2,
          v_until DATE)
AUTHID CURRENT_USER
AS
    v_prefix  VARCHAR2(30);
    v_last_value LONG;
    v_last_part VARCHAR2(30);
    v_sql VARCHAR2(4000);
    v_high_value DATE;
    v_new_value DATE;
      BEGIN
    -- Determine the prefix, the last partition name, and the last partition high value
        SELECT *
      INTO v_prefix, v_last_part, v_last_value
      FROM (
        SELECT TABLE_NAME||'_',PARTITION_NAME,HIGH_VALUE
          FROM ALL_TAB_PARTITIONS
         WHERE TABLE_OWNER = v_table_owner
           AND TABLE_NAME = v_table_name
         ORDER BY PARTITION_POSITION DESC)
     WHERE ROWNUM = 1;

    -- Calculate high value as Date
    EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(v_last_value) || ' FROM DUAL' INTO v_high_value;

    -- Add all need partitions
    WHILE v_until >= v_high_value LOOP
         v_high_value := v_high_value + 1;
        v_new_value := v_high_value -1;
         v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' ADD PARTITION "'
             ||v_prefix||TO_CHAR(v_new_value, 'YYYY_MM_DD')
             ||'" VALUES LESS THAN (TO_DATE ('''
             ||TO_CHAR(v_high_value, 'YYYY/MM/DD')||''', ''YYYY/MM/DD''))';
         DBMS_OUTPUT.PUT_LINE (v_sql);
         EXECUTE IMMEDIATE v_sql;
    END LOOP;
END;
/

CREATE OR REPLACE PROCEDURE DBADMIN."USAT_CHECK_CREATE_DAILY_PART" AUTHID CURRENT_USER is
--Created by Marie Njanje on 05/6/11 to check partitions daily
Begin
DECLARE
   CURSOR part_list_cursor IS
  select b.table_owner, b.table_name, partition_name, num_rows, high_value
    from dba_tab_partitions b,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in('SYS', 'SYSTEM') and table_name not like 'BIN$%'
    AND (K.COLUMN_NAME like '%DATE' or  K.COLUMN_NAME like '%TS' or K.COLUMN_NAME like '%DAY')
    AND TABLE_NAME  IN ('APP_LOCK')  AND T.TABLE_NAME=K.NAME AND TABLE_OWNER=K.OWNER
    group by table_owner, table_name ) a
    where a.part_pos = PARTITION_POSITION
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM');
     part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_sql2 VARCHAR2(4000);
     v_added_value constant VARCHAR2(30):= 'SYSDATE +3';
BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_high_value := part_rec.high_value;
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value;
           IF v_highest_value < SYSDATE +2 THEN
                        v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_DAILY_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                      EXECUTE IMMEDIATE v_sql2;
           END IF;
       END IF;
    END LOOP;
END;
END;
/
