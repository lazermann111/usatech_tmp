CREATE OR REPLACE PROCEDURE DBADMIN.ADD_MONTHLY_PARTITION (
          v_table_owner VARCHAR2,
          v_table_name VARCHAR2,
          v_month DATE)
AUTHID CURRENT_USER
AS
    v_part_name  VARCHAR2(12);
    v_part_value VARCHAR2(10);
    v_count NUMBER(1);
    v_sql VARCHAR2(4000);
BEGIN
    --Construct partition_name for next month with format "YYYY-MM-01";
    SELECT TO_CHAR(TRUNC(ADD_MONTHS(v_month, 1), 'MM'), 'YYYY-MM-DD') INTO v_part_name FROM DUAL;
    SELECT TO_CHAR(TRUNC(ADD_MONTHS(v_month, 2), 'MM'), 'YYYY/MM/DD') INTO v_part_value FROM DUAL;
    
    -- Check if the partition exists
    SELECT COUNT(*) 
      INTO v_count
      FROM ALL_TAB_PARTITIONS
     WHERE TABLE_OWNER = v_table_owner
       AND TABLE_NAME = v_table_name
       AND PARTITION_NAME = v_part_name;
    
    -- Add partition
    IF v_count = 0 THEN
       v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' ADD PARTITION "'||v_part_name||'" VALUES LESS THAN (TO_DATE ('''||v_part_value||''', ''YYYY/MM/DD''))';
       DBMS_OUTPUT.PUT_LINE (v_sql);
       EXECUTE IMMEDIATE v_sql;
    END IF;
END;
/
