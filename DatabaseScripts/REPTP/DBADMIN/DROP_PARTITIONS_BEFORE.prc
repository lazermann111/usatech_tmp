CREATE OR REPLACE PROCEDURE DBADMIN.DROP_PARTITIONS_BEFORE (
    v_table_owner VARCHAR2,
    v_table_name VARCHAR2,
    v_date_before DATE)
AUTHID CURRENT_USER
AS
    v_sql VARCHAR2(4000);
    v_high_value DATE;
    CURSOR part_list_cursor (c_table_owner VARCHAR2, c_table_name VARCHAR2) IS
        SELECT PARTITION_NAME, HIGH_VALUE, PARTITION_POSITION,
               LAST_VALUE(PARTITION_POSITION) 
                 OVER(ORDER BY PARTITION_POSITION 
                 ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) LAST_POSITION
        FROM ALL_TAB_PARTITIONS
        WHERE TABLE_OWNER = c_table_owner
        AND TABLE_NAME = c_table_name
        ORDER BY PARTITION_POSITION;
BEGIN
    FOR part_rec IN part_list_cursor(v_table_owner, v_table_name) LOOP
        IF part_rec.LAST_POSITION > part_rec.PARTITION_POSITION THEN
            EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_high_value;
            IF v_high_value < v_date_before THEN 
                v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' DROP PARTITION "'|| part_rec.partition_name||'" UPDATE INDEXES';
                DBMS_OUTPUT.PUT_LINE (v_sql);
                EXECUTE IMMEDIATE v_sql;
            ELSE
                EXIT;
            END IF;
        ELSE
            EXIT;
        END IF;
    END LOOP;
END;
/
