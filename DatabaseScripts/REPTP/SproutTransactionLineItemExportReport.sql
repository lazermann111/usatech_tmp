DECLARE
    l_report_id NUMBER;
BEGIN
    select report.reports_seq.nextval into l_report_id from dual;
    
	insert into report.reports(report_id, title, generator_id, batch_type_id, report_name, description, usage, user_id)
	values(l_report_id, 'TRANS-LINE-ITEM-{x}-{d}', 6, 1, 'Sprout Transaction Line Item Data Export', 'Comma-Separated Sprout Transaction data. Includes: Customer ID, Customer Name, Transaction ID, Machine ID, Transaction Date/Time, Coil Name, Price, Quantity.', 'E', 0);
    
    insert into report.report_param values(l_report_id, 'folioId', '1021');
    insert into report.report_param values(l_report_id, 'params.BatchId', '{x}');
    
    commit;
END;
