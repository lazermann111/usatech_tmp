alter table report.activity_ref add(SETTLE_STATE VARCHAR2(50));
alter table report.trans_state add(STATE_LABEL VARCHAR2(50));
update report.trans_state set state_label = DECODE(STATE_ID, 1, 'Pending', 2, 'Processed', 3, 'Settled', 4, 'Pending', 5, 'Failed', 6, 'Settled');
commit;
alter table report.trans_state modify(STATE_LABEL NOT NULL);
declare
	cursor l_cur is select tran_id from report.activity_ref ar where ar.settle_state is null;
BEGIN
	for l_rec in l_cur loop		
		update report.activity_ref ar set ar.settle_state = 
		    (select s.state_label from report.trans x, report.trans_state s where x.tran_id = ar.tran_id and x.settle_state_id = s.state_id)
			where ar.tran_id = l_rec.tran_id;
		commit;
	end loop;
END;
/    

