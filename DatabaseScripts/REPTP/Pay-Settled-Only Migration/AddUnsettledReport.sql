select * from report.link_param where link_id = 90


-----
INSERT INTO REPORT.LINK
(LINK_ID,TITLE,DESCRIPTION,CATEGORY,USAGE,SEQ)
VALUES
(90,'Unsettled Transactions (Last 30 Days)','Detailed list of each transaction that is unsettled in the last thirty days','Daily Operations','W',0)
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'requestHandler','activity_detail_ext')
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'sortBy','0, 4, 12, 2')
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'endDate','{0}')
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'beginDate','{-2592000000}')
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'tranType','16,19')
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'settleStates','Declined,Pending,Retry')
/
INSERT INTO report.link_param
(LINK_ID,PARAM_NAME,PARAM_VALUE)
VALUES
(90,'rangeType','ALL')
/

