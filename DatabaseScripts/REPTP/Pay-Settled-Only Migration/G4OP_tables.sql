ALTER TABLE STG_CREDIT 
 ADD (
  SETTLE_DATE DATE
 )
/

ALTER TABLE STG_REFUND
 ADD (
  SETTLE_DATE DATE
 )
/

DROP SYNONYM G4OP.QUICK_PSS;
DROP SYNONYM G4OP.ETPAYMENT;
DROP SYNONYM G4OP.VISANETPAYMENT;

