DROP TABLE FOLIO_CONF.REPORT; --since there IS NO data
CREATE TABLE FOLIO_CONF.REPORT(
    REPORT_ID                       NUMBER                          NOT NULL,
    REPORT_CD                       VARCHAR2 (50)                   NOT NULL,
    REPORT_NAME                     VARCHAR2 (100)                  NOT NULL,
    REPORT_TITLE                    VARCHAR2 (100),
    REPORT_SUBTITLE                 VARCHAR2 (250),                  
    DEFAULT_OUTPUT_TYPE_ID          NUMBER                          DEFAULT 22      NOT NULL,                      
    CREATED_BY                      VARCHAR2 (30),
    CREATED_TS                      DATE,
    LAST_UPDATED_BY                 VARCHAR2 (30),
    LAST_UPDATED_TS                 DATE,
	CONSTRAINT PK_REPORT_ID PRIMARY KEY (REPORT_ID)  USING INDEX TABLESPACE FOLIO_CONF_DATA,
	CONSTRAINT UIX_REPORT_REPORT_CD UNIQUE (REPORT_CD)  USING INDEX TABLESPACE FOLIO_CONF_INDX,
	CONSTRAINT FK_R_DEFAULT_OUTPUT_TYPE_ID FOREIGN KEY(DEFAULT_OUTPUT_TYPE_ID) 
		REFERENCES FOLIO_CONF.OUTPUT_TYPE(OUTPUT_TYPE_ID)
) TABLESPACE FOLIO_CONF_DATA;

CREATE OR REPLACE TRIGGER FOLIO_CONF.TRBI_REPORT
BEFORE INSERT ON FOLIO_CONF.REPORT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        NVL(:NEW.REPORT_ID, SEQ_REPORT_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.REPORT_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER FOLIO_CONF.TRBU_REPORT
BEFORE UPDATE ON FOLIO_CONF.REPORT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_TS,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_TS,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

ALTER TABLE FOLIO_CONF.FOLIO
    ADD (FOLIO_CD VARCHAR2(50),
	     CONSTRAINT UIX_FOLIO_FOLIO_CD 
		     UNIQUE (FOLIO_CD)  
			  USING INDEX TABLESPACE FOLIO_CONF_INDX);
/*
select F.folio_id, f.folio_name, (SELECT DECODE(COUNT(*), 
		   			  1, SUBSTR(MAX(F0.FOLIO_NAME), 1, 50), 
   		              SUBSTR(MAX(F0.FOLIO_NAME), 1, 48) || '-' 
						 || (SUM(CASE WHEN F0.folio_id < f.folio_id then 1 else 0 end) + 1))
		  FROM FOLIO_CONF.FOLIO F0 
		  WHERE F0.folio_name = F.folio_name)
 from FOLIO_CONF.FOLIO F
 order by folio_name, folio_id

*/
UPDATE FOLIO_CONF.FOLIO F
   SET FOLIO_CD = (
   		SELECT DECODE(COUNT(*), 
		   			  1, SUBSTR(MAX(F0.FOLIO_NAME), 1, 50), 
   		              SUBSTR(MAX(F0.FOLIO_NAME), 1, 48) || '-' 
						 || (SUM(CASE WHEN F0.folio_id < f.folio_id then 1 else 0 end) + 1))
		  FROM FOLIO_CONF.FOLIO F0 
		  WHERE F0.folio_name = F.folio_name);

ALTER TABLE FOLIO_CONF.FOLIO
    MODIFY FOLIO_CD NOT NULL;
					
