SELECT * 
FROM folio_conf.folio fo, folio_conf.vw_filter_group_hierarchy h, folio_conf.filter f, folio_conf.filter_param fp
WHERE fo.filter_group_id = h.ancestor_filter_group_id AND h.descendent_filter_group_id = f.filter_group_id
AND f.filter_id = fp.filter_id AND fo.folio_name IN('Pass-Access-Debit Summary', 'Refund Summary', 'Credit Card Summary')
AND f.field_id = 161;

DELETE FROM folio_conf.filter_param fp
WHERE fp.filter_id IN(SELECT f.filter_id
	FROM folio_conf.folio fo, folio_conf.vw_filter_group_hierarchy h, folio_conf.filter f
	WHERE fo.filter_group_id = h.ancestor_filter_group_id 
	AND h.descendent_filter_group_id = f.filter_group_id
	AND fo.folio_name IN('Pass-Access-Debit Summary', 'Refund Summary', 'Credit Card Summary')
	AND f.field_id = 161);
	
DELETE FROM folio_conf.filter
WHERE filter_id IN(SELECT f.filter_id
	FROM folio_conf.folio fo, folio_conf.vw_filter_group_hierarchy h, folio_conf.filter f
	WHERE fo.filter_group_id = h.ancestor_filter_group_id 
	AND h.descendent_filter_group_id = f.filter_group_id
	AND fo.folio_name IN('Pass-Access-Debit Summary', 'Refund Summary', 'Credit Card Summary')
	AND f.field_id = 161);

COMMIT;
