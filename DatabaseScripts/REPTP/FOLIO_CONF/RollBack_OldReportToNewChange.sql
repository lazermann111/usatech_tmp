ALTER TABLE
   FOLIO_CONF.FOLIO_PILLAR
ADD
   PERCENT_FORMAT  VARCHAR2(2000);

delete from folio_conf.filter_operator where filter_operator_id=28;
delete from folio_conf.filter_operator where filter_operator_id=29;
delete from folio_conf.filter_operator where filter_operator_id=30;
-- get previous version of FOLIO_PKG.psk FOLIO_PKG.pbk and run the script
commit;