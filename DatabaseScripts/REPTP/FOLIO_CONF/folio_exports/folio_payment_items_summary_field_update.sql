INSERT INTO FOLIO_CONF.FIELD (FIELD_ID,FIELD_LABEL, FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID)
SELECT 9745,FIELD_LABEL,FIELD_DESC,
  q'[CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE <> 'CC' AND CORP.BATCH_TOTAL_HIST.PAYABLE = 'Y' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END]',
  q'[CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE <> 'CC' AND CORP.BATCH_TOTAL_HIST.PAYABLE = 'Y' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END]',
  DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE, 1042
FROM FOLIO_CONF.FIELD WHERE field_id = 1983 AND NOT EXISTS (SELECT 1 FROM FOLIO_CONF.FIELD WHERE FIELD_ID = 9745);

INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
SELECT c2.column_value, c1.column_value, (CASE WHEN c2.column_value IN (4,5,8) THEN 1 END)
  FROM TABLE(DBADMIN.NUMBER_TABLE(9745)) c1
  CROSS JOIN TABLE(DBADMIN.NUMBER_TABLE(1,2,3,4,5,8)) c2
  WHERE NOT EXISTS (SELECT 1 FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = C1.COLUMN_VALUE); 


COMMIT;