alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN
DELETE FROM tmp_map_filter_group_ids;
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(41);
SELECT 41 INTO l_id0 FROM DUAL;
INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, default_chart_type_id, max_rows_per_section, max_rows) (SELECT l_id0, 'Devices For Recent Call Ins', 'LITERAL:Device Call In', 'LITERAL:Select a device', 22, 1, NULL, -1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 1, 1, 'Region', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 15, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 2, 1, 'Location', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 16, 'ASC', 6, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 3, 1, 'Device', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 649, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 4, 6, '', '', '', 'MESSAGE:run_report.i?folioId=39&DeviceSerialNumber={}', 'LITERAL:Click to view most recent Call ins for this device', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 649, 'ASC', 3, 0 FROM DUAL);
INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) (SELECT seq_folio_directive_id.NEXTVAL, l_id0, 7, 'false' FROM DUAL);
INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) (SELECT seq_folio_directive_id.NEXTVAL, l_id0, 8, 'false' FROM DUAL);
END;

