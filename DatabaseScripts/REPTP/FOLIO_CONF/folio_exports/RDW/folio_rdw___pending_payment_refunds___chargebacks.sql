SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;
DECLARE
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);
BEGIN
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(1456);
SELECT 1456 INTO l_ids(1) FROM DUAL;INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'RDW - Pending Payment Refunds / Chargebacks', '', 'LITERAL:Refunds / Chargebacks', 22, 7, NULL, -1, 1000 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 1, 'LITERAL:Device', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8622, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 1, 'LITERAL:Terminal', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8520, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 1, 'LITERAL:Location', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8514, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 4, 1, 'LITERAL:Asset Nbr', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8516, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 5, 1, 'LITERAL:Tran #', '', '', '', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8615, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 6, 1, 'LITERAL:Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', '', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8604, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 7, 1, 'LITERAL:Type', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8150, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 8, 1, 'LITERAL:Card Type', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 9188, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 9, 1, 'LITERAL:Amount', '', '', 'Currency', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 9046, 'ASC', 1, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 10, 1, 'LITERAL:Description', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 8610, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 2, 'false' FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 172, '10' FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 12, 'No refunds or chargebacks included in this payment' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 9024, 35, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, '', '', 'O,L,A', '', 'ARRAY:CHAR(1)', 'TEXT' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 8607, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'customerBankId', 'Enter the value for customerBankId', '', 'customerBankId', 'BIGINT', 'NUMBER' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 8612, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, '', '', 'Yes', '', 'VARCHAR(10)', 'TEXT' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 8564, 9, 0, l_ids(l_ids.LAST-1) FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT l_ids(l_ids.LAST-1), l_ids(l_ids.LAST), 'OR' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 8650, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, '', '', 'RF', '', 'CHAR(2)', 'TEXT' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 8650, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, '', '', 'CB', '', 'CHAR(2)', 'TEXT' FROM DUAL);
l_ids.TRIM;
l_ids.TRIM;
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);
l_ids.TRIM;
END;
/
COMMIT;
