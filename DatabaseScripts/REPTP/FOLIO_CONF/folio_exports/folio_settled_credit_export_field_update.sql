-- this CCE Fill Discrepancy folioId=673 join is affecting ledger to vw_user_customer_bank so move the order to a large number so ledger to vw_user_customer_bank will use batch and doc table
update FOLIO_CONF.join_filter set join_filter_id=2206 where join_filter_id=210;
commit;