
alter session set current_schema = folio_conf;

--FIELD CATEGORIES
insert into field_category (field_category_id, field_category_label, field_category_desc,parent_field_category_id,active_flag) 
values (437, 'Custom Field Information',null,null,'Y');

insert into field_category (field_category_id, field_category_label, field_category_desc,parent_field_category_id,active_flag) 
values (439, 'Custom Comment Information',null,null,'Y');


--FIELDS
INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2451,'Custom Terminal Field 1', null, 'REPORT.TERMINAL.TERM_VAR_1', 'UPPER(REPORT.TERMINAL.TERM_VAR_1)',        
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2453,'Custom Terminal Field 2', null, 'REPORT.TERMINAL.TERM_VAR_2', 'UPPER(REPORT.TERMINAL.TERM_VAR_2)',       
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');    

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2455, 'Custom Terminal Field 3', null, 'REPORT.TERMINAL.TERM_VAR_3', 'UPPER(REPORT.TERMINAL.TERM_VAR_3)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2457, 'Custom Terminal Field 4', null, 'REPORT.TERMINAL.TERM_VAR_4', 'UPPER(REPORT.TERMINAL.TERM_VAR_4)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2459, 'Custom Terminal Field 5', null, 'REPORT.TERMINAL.TERM_VAR_5', 'UPPER(REPORT.TERMINAL.TERM_VAR_5)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2461, 'Custom Terminal Field 6', null, 'REPORT.TERMINAL.TERM_VAR_6', 'UPPER(REPORT.TERMINAL.TERM_VAR_6)',       
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2463, 'Custom Terminal Field 7', null, 'REPORT.TERMINAL.TERM_VAR_7', 'UPPER(REPORT.TERMINAL.TERM_VAR_7)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2465, 'Custom Terminal Field 8', null, 'REPORT.TERMINAL.TERM_VAR_8', 'UPPER(REPORT.TERMINAL.TERM_VAR_8)', 
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 7,'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2473, 'Custom Terminal Field Label 1', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_1_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_1_LABEL)',
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2475, 'Custom Terminal Field Required Flag 1', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_1_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_1_RQD_FLG)',
null,'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2477, 'Custom Terminal Field Comment 1', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_1_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_1_CMNT)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2479, 'Custom Terminal Field Label 2', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_2_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_2_LABEL) ',
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2481, 'Custom Terminal Field Required Flag 2', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_2_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_2_RQD_FLG)',
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2483, 'Custom Terminal Field Comment 2', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_2_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_2_CMNT)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2487, 'Custom Terminal Field Label 3', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_3_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_3_LABEL)',
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2489, 'Custom Terminal Field Required Flag 3', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_3_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_3_RQD_FLG)',
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2491, 'Custom Terminal Field Comment 3', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_3_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_3_CMNT)',
null,'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2493, 'Custom Terminal Field Label 4', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_4_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_4_LABEL)',
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2495, 'Custom Terminal Field Required Flag 4', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_4_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_4_RQD_FLG)',
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2497, 'Custom Terminal Field Comment 4', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_4_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_4_CMNT)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2499, 'Custom Terminal Field Label 5', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_5_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_5_LABEL)',  
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2501, 'Custom Terminal Field Required Flag 5', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_5_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_5_RQD_FLG)',  
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2503, 'Custom Terminal Field Comment 5', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_5_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_5_CMNT)',
null,'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2505, 'Custom Terminal Field Label 6', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_6_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_6_LABEL)',
null,'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2507, 'Custom Terminal Field Required Flag 6', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_6_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_6_RQD_FLG',    
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2509, 'Custom Terminal Field Comment 6', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_6_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_6_CMNT)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2511, 'Custom Terminal Field Label 7', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_7_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_7_LABEL)',
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2513, 'Custom Terminal Field Required Flag 7', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_7_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_7_RQD_FLG)',
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2515, 'Custom Terminal Field Comment 7', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_7_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_7_CMNT)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2517, 'Custom Terminal Field Label 8', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_8_LABEL', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_8_LABEL)',
null, 'VARCHAR(50)', 'VARCHAR(50)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2519, 'Custom Terminal Field Required Flag 8', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_8_RQD_FLG', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_8_RQD_FLG)',
null, 'VARCHAR(1)', 'VARCHAR(1)', 50, 437, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2521, 'Custom Terminal Field Comment 8', null, 'REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_8_CMNT', 'UPPER(REPORT.VW_TERMINAL_CUSTOM.TERM_VAR_8_CMNT)',
null, 'VARCHAR(4000)', 'VARCHAR(4000)', 50, 437,'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2525, 'Custom Comment Customer Id', null, 'REPORT.CUSTOM_COMMENT_PARAM.CUSTOMER_ID', 'REPORT.CUSTOM_COMMENT_PARAM.CUSTOMER_ID',
null, 'NUMERIC', 'NUMERIC', 50, 439, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2527, 'Custom Comment Text', null, 'REPORT.CUSTOM_COMMENT_PARAM.LINE_TEXT_1','UPPER(REPORT.CUSTOM_COMMENT_PARAM.LINE_TEXT_1)',
null,'VARCHAR(4000)', 'VARCHAR(4000)', 50, 439, 'Y');

INSERT INTO FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
values
(2529, 'Custom Comment HTML Class', null, 'REPORT.CUSTOM_COMMENT_PARAM.COMMENT_HTML_CLASS', 'UPPER(REPORT.CUSTOM_COMMENT_PARAM.COMMENT_HTML_CLASS)',
null,'VARCHAR(100)', 'VARCHAR(100)', 50, 439, 'Y');



--ADD FIELD PRIVILEGES for the 36 NEW FIELDS as listed above with field ids:
--2451,2453,2455,2457,2459,2461,2463,2465,2473,2475,2477,2479,
--2481,2483,2487,2489,2491,2493,2495,2497,2499,2501,2503,2505,2507,2509,
--2511,2513,2515,2517,2519,2521,2525,2527,2529

Declare

cursor fldId is select field_id from field 
where field_id between 2451 and 2529 and field_id != 2471;

l_new_field_id FOLIO_CONF.FIELD.FIELD_ID%TYPE;

Begin

IF fldId%ISOPEN THEN
  close fldId;
END IF;

for field in fldId loop

l_new_field_id := field.field_id;

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1, l_new_field_id,NULL);

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2, l_new_field_id,NULL);

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3, l_new_field_id,NULL);

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4, l_new_field_id,1);

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5, l_new_field_id,1);

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8, l_new_field_id,1);

end loop;


/**COMMIT LINE**/
commit;

EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line(SQLERRM||' '||SQLCODE);
  RAISE;
End;


