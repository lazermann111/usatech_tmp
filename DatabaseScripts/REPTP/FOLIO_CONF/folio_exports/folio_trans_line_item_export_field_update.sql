-- two new fields
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG) VALUES(2263, 'Line Item Price','','REPORT.PURCHASE.PRICE','REPORT.PURCHASE.PRICE','','DECIMAL','DECIMAL',50,62,'Y');
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION, 
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG) VALUES(2265, 'Line Item MDB Number','','REPORT.PURCHASE.MDB_NUMBER','REPORT.PURCHASE.MDB_NUMBER','','DECIMAL','DECIMAL',50,62,'Y');
 
-- field priv for 2263, 2265
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,2263,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,2265,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,2263,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,2263,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,2263,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,2263,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,2263,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,2265,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,2265,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,2265,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,2265,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,2265,1);
 
-- user_group_id 1 already exists ,need to add priv for others
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,488,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,488,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,488,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,488,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,488,1);
 
commit;

 
