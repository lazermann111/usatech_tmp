alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN
DELETE FROM tmp_map_filter_group_ids;
INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 316, seq_filter_group_id.NEXTVAL FROM DUAL);
INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 317, seq_filter_group_id.NEXTVAL FROM DUAL);
SELECT seq_folio_id.NEXTVAL INTO l_id0 FROM DUAL;
INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, default_chart_type_id, max_rows_per_section, max_rows) (SELECT l_id0, 'Credit Card Summary For Customer', 'LITERAL:Credit Card Summary', 'MESSAGE:{StartDate} to {EndDate}', 22, NULL, NULL, 0, 20000 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 1, 1, 'Currency', '', 'MESSAGE:{1}', '', '', '', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 161, '', 0, 0 FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 2, 201, 'ASC', 3, 2 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 2, 1, 'Device', '', '', '', '', '', '', 1, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 60, 'ASC', 3, 1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 3, 1, 'Location', '', '', '', '', '', '', 1, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 16, 'ASC', 3, 1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 4, 1, 'Transaction Date', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', 'DATE:MM/dd/yyyy HH:mm:ss', '', 0, '', 'DATE' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 440, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 5, 1, 'Amount', '', 'MESSAGE:{1} {0,NUMBER,#,##0.00} {2}', '', '', 'MESSAGE:{0,NUMBER,#.###}', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 51, 'ASC', 0, 0 FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 2, 181, 'ASC', 10, 0 FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 3, 182, 'ASC', 10, 3 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 6, 1, 'AP Code', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 25, 'ASC', 3, 1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 7, 1, 'Card Number', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 142, 'ASC', 3, 1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 8, 1, 'Details', '', 'MESSAGE:{0} {1}', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 54, '', 0, 0 FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 2, 58, 'ASC', 3, 2 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 9, 1, 'Quantity', '', 'NUMBER', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 55, 'ASC', 1, 1 FROM DUAL);
INSERT INTO filter_group(parent_group_id, filter_group_id, separator) (SELECT newparent.id, newgroup.id, 'AND' FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=316) newgroup LEFT JOIN (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=NULL) newparent ON 1=1);
INSERT INTO filter_group(parent_group_id, filter_group_id, separator) (SELECT newparent.id, newgroup.id, 'OR' FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=317) newgroup LEFT JOIN (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=316) newparent ON 1=1);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 21, 1, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=316) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type) (SELECT l_id1, 1, 'TransTypeId', 'Enter the value for Trans Type Id', '16', 'TransTypeId', 'NUMERIC' FROM DUAL);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 242, 1, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=317) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type) (SELECT l_id1, 1, 'AllTerminals', 'Enter the value for Include All Constant', '', 'AllTerminals', 'VARCHAR' FROM DUAL);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 18, 21, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=317) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type) (SELECT l_id1, 1, 'TerminalId', 'Enter the value for Terminal Id', '', 'TerminalId', 'ARRAY:NUMERIC_LIST' FROM DUAL);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 440, 22, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=316) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type) (SELECT l_id1, 1, 'StartDate', 'Enter the value for StartDate', '', 'StartDate', 'DATE' FROM DUAL);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type) (SELECT l_id1, 2, 'EndDate', 'Enter the value for EndDate', '', 'EndDate', 'TIMESTAMP' FROM DUAL);
UPDATE FOLIO SET FILTER_GROUP_ID = (SELECT new_group_id FROM tmp_map_filter_group_ids WHERE old_group_id=316) WHERE FOLIO_ID = TO_CHAR(l_id0);
END;

