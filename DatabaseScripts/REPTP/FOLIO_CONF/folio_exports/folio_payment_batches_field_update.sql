INSERT INTO FOLIO_CONF.FIELD (FIELD_ID, FIELD_LABEL, DISPLAY_EXPRESSION, SORT_EXPRESSION, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG) 
VALUES (9743, 'Loyalty Discount', 'CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE = ''LD'' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END', 'CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE = ''LD'' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END', 'NUMERIC', 'NUMERIC', 50, 1042, 'Y');

INSERT INTO FOLIO_CONF.FIELD (FIELD_ID, FIELD_LABEL, DISPLAY_EXPRESSION, SORT_EXPRESSION, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG) 
VALUES (9744, 'Purchase Discount', 'CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE = ''PD'' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END', 'CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE = ''PD'' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END', 'NUMERIC', 'NUMERIC', 50, 1042, 'Y');

INSERT INTO FOLIO_CONF.FIELD (FIELD_ID, FIELD_LABEL, DISPLAY_EXPRESSION, SORT_EXPRESSION, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG) 
VALUES (9746, 'Free Product Discount', 'CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE = ''FP'' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END', 'CASE WHEN CORP.BATCH_TOTAL_HIST.ENTRY_TYPE = ''FP'' THEN CORP.BATCH_TOTAL_HIST.LEDGER_AMOUNT ELSE NULL END', 'NUMERIC', 'NUMERIC', 50, 1042, 'Y');

INSERT INTO FOLIO_CONF.FIELD (FIELD_ID, FIELD_LABEL, DISPLAY_EXPRESSION, SORT_EXPRESSION, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG) 
VALUES (9749, 'Ledger Count', 'CORP.BATCH_TOTAL_HIST.LEDGER_COUNT', 'CORP.BATCH_TOTAL_HIST.LEDGER_COUNT', 'NUMERIC', 'NUMERIC', 50, 1042, 'Y');

INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
SELECT c2.column_value, c1.column_value, (CASE WHEN c2.column_value IN (4,5,8) THEN 1 END)
  FROM TABLE(DBADMIN.NUMBER_TABLE(9743, 9744, 9746, 9749)) c1
  CROSS JOIN TABLE(DBADMIN.NUMBER_TABLE(1,2,3,4,5,8)) c2
  WHERE NOT EXISTS (SELECT 1 FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = C1.COLUMN_VALUE); 

commit;  