
alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN

----DELETE FROM tmp_map_filter_group_ids;

----FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(895);

----INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 895, ----seq_filter_group_id.NEXTVAL FROM DUAL);


SELECT 895 INTO l_id0 FROM DUAL;  


INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, 
default_chart_type_id, max_rows_per_section, max_rows) 
(SELECT l_id0, 'Terminal Details (Custom)', 'LITERAL:Terminal Details', '', 22, 959, NULL, -1, -1 
FROM DUAL);


--PILLARS AND PILLAR FIELDS


---GROUP BY CUSTOMER NAME

SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 1, 1, 'LITERAL:Customer', '', 5, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 101, 'ASC', 0, 0 FROM DUAL);

--DETAIL ROW FIELDS

SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 2, 1, 'LITERAL:Region', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 15, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 3, 1, 'LITERAL:Device', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 649, 'ASC', 0, 0 FROM DUAL); 


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 4, 1, 'LITERAL:Location', '', 2,'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 16, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 5, 1, 'LITERAL:Terminal', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 20, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 6, 1, 'LITERAL:Asset #', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 761, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 7, 1, 'LITERAL:Address', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 873, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 8, 1, 'LITERAL:City', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 875, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 9, 1, 'LITERAL:State', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 877, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 10, 1, 'LITERAL:Zip', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 879, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 11, 1, 'LITERAL:Location Details', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 847, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 12, 1, 'LITERAL:Location Type', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 845, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 13, 1, 'LITERAL:Machine Make', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 708, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 14, 1, 'LITERAL:Machine Model', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 710, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 15, 1, 'LITERAL:Product Type', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 867, 'ASC', 0, 0 FROM DUAL);


SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 16, 1, 'LITERAL:Timezone', '', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 923, 'ASC', 0, 0 FROM DUAL);



SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 17, 1, 'LITERAL:Vends / Swipe', '', 2, 'NUMBER' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 921, 'ASC', 0, 0 FROM DUAL);



SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 18, 1, 'LITERAL:Authorization Mode',        
'MATCH:N=Credit Cards Not Accepted;L=Local Authorization;F=Full Authorization;.*=\\0',
2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 929, 'ASC', 0, 0 FROM DUAL);



SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 19, 1, 'LITERAL:DEX Data Capture',
'MATCH:N=None;D=Complete DEX File;A=Alarm Notification + Complete DEX File;.*=\\0',
2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 925, 'ASC', 0, 0 FROM DUAL);



SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 20, 1, 'LITERAL:Terminal Start Date', 'DATE:MM/dd/yyyy HH:mm:ss', 2, 'DATE' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 736, 'ASC', 0, 0 FROM DUAL);



SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 21, 1, 'LITERAL:Activation Submitted Date', 'DATE:MM/dd/yyyy HH:mm:ss', 2, 'DATE' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 931, 'ASC', 0, 0 FROM DUAL);



--NEW CUSTOM FIELDS

--TERM_VAR_1
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 22, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2451, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2473, 'NONE', 10, 0 FROM DUAL);


--TERM_VAR_2
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 23, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2453, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2479, 'NONE', 10, 0 FROM DUAL);


--TERM_VAR_3
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 24, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2455, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2487, 'NONE', 10, 0 FROM DUAL);


--TERM_VAR_4
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 25, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2457, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2493, 'NONE', 10, 0 FROM DUAL);



--TERM_VAR_5
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 26, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2459, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2499, 'NONE', 10, 0 FROM DUAL);


--TERM_VAR_6
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 27, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2461, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2505, 'NONE', 10, 0 FROM DUAL);


--TERM_VAR_7
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 28, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2463, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2511, 'NONE', 10, 0 FROM DUAL);


--TERM_VAR_8
SELECT folio_conf.seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, display_format, grouping_level,sort_type) 
(SELECT l_id1, l_id0, 29, 1, 'message:{1}', 'message:{0}', 2, 'STRING' FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 1, 2465, 'ASC', 10, 0 FROM DUAL);

INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) 
(SELECT l_id1, 2, 2517, 'NONE', 10, 0 FROM DUAL);





--FOLIO DIRECTIVES


INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) 
(SELECT seq_folio_directive_id.NEXTVAL, l_id0, 14, '2.0' FROM DUAL);

INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) 
(SELECT seq_folio_directive_id.NEXTVAL, l_id0, 13, 'true' FROM DUAL);


/**COMMIT LINE**/
commit;


EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line(SQLERRM||' '||SQLCODE);
  RAISE;
End;




