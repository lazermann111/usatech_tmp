UPDATE folio_pillar p 
SET p.action_format = 'MESSAGE:javascript:var newwin = window.open(''''' || 
    'create_refund.i?transactionId=''''+{},''''refundWindow'''',' || 
    '''''height=480,width=640,location=no,resizable=yes,scrollbars=yes,' || 
    'toolbar=no,menubar=no''''); newwin.focus();'
WHERE p.pillar_label = 'Tran Id' AND 
    p.folio_id IN (SELECT folio_id FROM FOLIO_CONF.FOLIO WHERE folio_name = 'Refund Search Transactions');

UPDATE folio_pillar p 
SET p.action_format = 'MESSAGE:javascript:var newwin = window.open(''''' || 
    'create_chargeback.i?transactionId=''''+{},''''refundWindow'''',' || 
    '''''height=480,width=640,location=no,resizable=yes,scrollbars=yes,' || 
    'toolbar=no,menubar=no''''); newwin.focus();'
WHERE p.pillar_label = 'Tran Id' AND 
    p.folio_id IN (SELECT folio_id FROM FOLIO_CONF.FOLIO WHERE folio_name = 'Chargeback Search Transactions');
        
COMMIT;
        
/*
SELECT p.action_format FROM folio_pillar p
WHERE p.pillar_label = 'Tran Id' AND 
    p.folio_id IN (SELECT folio_id FROM FOLIO_CONF.FOLIO WHERE folio_name IN 
        ('Refund Search Transactions','Chargeback Search Transactions'));
*/
