alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN
DELETE FROM tmp_map_filter_group_ids;
INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 698, seq_filter_group_id.NEXTVAL FROM DUAL);
SELECT 194 INTO l_id0 FROM DUAL;
INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, default_chart_type_id, max_rows_per_section, max_rows) (SELECT l_id0, 'Swank Access Cards', 'LITERAL:Access Card Revenue Report', 'MESSAGE:{''{*MONTH-1}'', DATE, MMMM yyyy}', 27, 2, NULL, -1, -1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 1, 1, 'Customer', '', 'MESSAGE:{1}', '', '', 'MESSAGE:{1}', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 531, '', 0, 0 FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 2, 101, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 2, 1, 'Currency', '', 'MESSAGE:{1}', '', '', 'MESSAGE:{0}', '', 1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 161, '', 0, 0 FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 2, 201, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 3, 1, 'Location', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 16, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 4, 1, 'PC Gross', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 677, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 5, 1, 'PDPA Gross', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 681, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 6, 1, 'PF Gross', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 685, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 7, 1, 'PP Gross', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 689, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 8, 1, 'K1 Gross', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 693, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 9, 1, 'Total Gross', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 51, 'ASC', 1, 0 FROM DUAL);
INSERT INTO filter_group(parent_group_id, filter_group_id, separator) (SELECT newparent.id, newgroup.id, 'AND' FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=698) newgroup LEFT JOIN (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=NULL) newparent ON 1=1);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 21, 1, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=698) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type, param_editor) (SELECT l_id1, 1, 'TransTypeId', 'Enter the value for ', '17', 'TransTypeId', 'NUMERIC', 'NUMBER' FROM DUAL);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 26, 1, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=698) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type, param_editor) (SELECT l_id1, 1, 'TransactionMonth', 'Enter the value for ', '{*MONTH-1}', 'TransactionMonth', 'DATE', 'DATE' FROM DUAL);
UPDATE FOLIO SET FILTER_GROUP_ID = (SELECT new_group_id FROM tmp_map_filter_group_ids WHERE old_group_id=698) WHERE FOLIO_ID = TO_CHAR(l_id0);
END;
