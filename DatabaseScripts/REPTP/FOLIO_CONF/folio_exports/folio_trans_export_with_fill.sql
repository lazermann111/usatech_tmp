alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN
DELETE FROM tmp_map_filter_group_ids;
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(869);
INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 832, seq_filter_group_id.NEXTVAL FROM DUAL);
SELECT 869 INTO l_id0 FROM DUAL;
INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, default_chart_type_id, max_rows_per_section, max_rows) (SELECT l_id0, 'Transaction Export With Fill', 'MESSAGE:TRANS-{BatchId}-{''{*0}'',DATE,yyyyMMdd-HHmmss} WITH FILL', '', 21, 959, NULL, -1, -1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 1, 1, 'LITERAL:Device', '', '', '', '', '', '', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2275, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 2, 1, 'LITERAL:Ref Nbr', '', '', '', '', '', '', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2273, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 3, 1, 'LITERAL:Trans Type Code', '', '', '', '', '', '', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2349, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 4, 1, 'LITERAL:Card Number', '', '', '', '', '', '', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2351, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 5, 1, 'LITERAL:Total Sale Amount', '', '', '', '', '', '', '', 2, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2353, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 6, 1, 'LITERAL:Line Item', '', '', '', '', '', '', '', 2, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2357, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 7, 1, 'LITERAL:Line Item Quantity', '', '', '', '', '', '', '', 2, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2359, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 8, 1, 'LITERAL:Tran Date', '', '', 'DATE:MM/dd/yyyy', '', '', '', '', 2, '', 'DATE' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2361, 'ASC', 0, -1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 9, 1, 'LITERAL:Tran Time', '', '', 'DATE:HH:mm:ss', '', '', '', '', 2, '', 'DATE' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2361, 'ASC', 0, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, pillar_width, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 10, 1, 'LITERAL:Orig Ref Nbr', '', '', '', '', '', '', '', 2, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 2365, 'ASC', 0, 0 FROM DUAL);
INSERT INTO filter_group(parent_group_id, filter_group_id, separator) (SELECT newparent.id, newgroup.id, 'AND' FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=832) newgroup LEFT JOIN (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=NULL) newparent ON 1=1);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id,  aggregate_type_id, filter_group_id) (SELECT l_id1, 2277, 1, 0, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=832) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type, param_editor) (SELECT l_id1, 1, 'BatchId', 'Enter the value for BatchId', '', 'BatchId', 'DECIMAL', 'NUMBER:(20)' FROM DUAL);
INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) (SELECT seq_folio_directive_id.NEXTVAL, l_id0, 14, '2.0' FROM DUAL);
UPDATE FOLIO SET FILTER_GROUP_ID = (SELECT new_group_id FROM tmp_map_filter_group_ids WHERE old_group_id=832) WHERE FOLIO_ID = TO_CHAR(l_id0);
END;

