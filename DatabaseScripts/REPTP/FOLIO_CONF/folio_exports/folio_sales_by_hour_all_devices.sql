alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN
DELETE FROM tmp_map_filter_group_ids;
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(262);
INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 234, seq_filter_group_id.NEXTVAL FROM DUAL);
SELECT 262 INTO l_id0 FROM DUAL;
INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, default_chart_type_id, max_rows_per_section, max_rows) (SELECT l_id0, 'Sales By Hour (All Devices)', 'LITERAL:Sales By Hour', 'MESSAGE:{StartDate,DATE,MMMM d, yyyy} to {EndDate,DATE,MMMM d, yyyy}', 22, 7, NULL, -1, -1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 1, 1, 'Currency', '', '', '', '', '', '', 1, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 201, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 2, 4, 'Hour of Day', '', '', '', '', '', '', 0, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 29, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 3, 5, 'Trans', '', 'NUMBER', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 3, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 4, 3, '', '', '', '', 'LITERAL:$0 transactions have NOT been included in this calculation. Also, any refunds or chargebacks have been deducted from (not added to) the number of transactions in this calculation.', '', '', 0, 'LITERAL:color: #20B020;', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 722, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 5, 5, 'Amount', '', 'CURRENCY', '', '', '', '', -1, '', 'NUMBER' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 51, 'ASC', 1, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 6, 3, 'Unsettled Indicator', '', '', '', 'LITERAL:Contains unsettled transactions', '', '', 0, 'LITERAL:color: #FF6000;', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 102, 'ASC', 1, 0 FROM DUAL);
INSERT INTO filter_group(parent_group_id, filter_group_id, separator) (SELECT newparent.id, newgroup.id, 'AND' FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=234) newgroup LEFT JOIN (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=NULL) newparent ON 1=1);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 440, 22, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=234) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type, param_editor) (SELECT l_id1, 1, 'StartDate', 'Enter the value for StartDate', '{*YEAR}', 'StartDate', 'DATE', 'DATE' FROM DUAL);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type, param_editor) (SELECT l_id1, 2, 'EndDate', 'Enter the value for EndDate', '{*0}', 'EndDate', 'DATE', 'DATE' FROM DUAL);
INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) (SELECT seq_folio_directive_id.NEXTVAL, l_id0, 10, '0' FROM DUAL);
INSERT INTO folio_directive(folio_directive_id, folio_id, directive_id, directive_value) (SELECT seq_folio_directive_id.NEXTVAL, l_id0, 2, 'false' FROM DUAL);
UPDATE FOLIO SET FILTER_GROUP_ID = (SELECT new_group_id FROM tmp_map_filter_group_ids WHERE old_group_id=234) WHERE FOLIO_ID = TO_CHAR(l_id0);
END;

