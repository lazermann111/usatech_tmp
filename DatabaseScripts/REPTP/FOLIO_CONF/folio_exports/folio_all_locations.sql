alter session set current_schema = folio_conf;
DECLARE
  l_id0 NUMBER;
  l_id1 NUMBER;
BEGIN
DELETE FROM tmp_map_filter_group_ids;
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(291);
INSERT INTO tmp_map_filter_group_ids(old_group_id, new_group_id) (SELECT 283, seq_filter_group_id.NEXTVAL FROM DUAL);
SELECT 291 INTO l_id0 FROM DUAL;
INSERT INTO folio(folio_id, folio_name, folio_title, folio_subtitle, default_output_type_id, owner_user_id, default_chart_type_id, max_rows_per_section, max_rows) (SELECT l_id0, 'All Current Locations', 'LITERAL:All Locations', '', 22, 1, NULL, -1, -1 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 1, 1, 'Customer', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 101, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 2, 1, 'Location', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 16, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 3, 1, 'Device', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 649, 'ASC', 3, 0 FROM DUAL);
SELECT seq_folio_pillar_id.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO folio_pillar(folio_pillar_id, folio_id, pillar_index, pillar_type_id, pillar_label, pillar_desc, display_format, action_format, help_format, sort_format, css_style, grouping_level, style_format, sort_type) (SELECT l_id1, l_id0, 4, 1, 'Equipment', '', '', '', '', '', '', 0, '', 'STRING' FROM DUAL);
INSERT INTO folio_pillar_field(folio_pillar_id, field_index, field_id, sort_order, aggregate_type_id, sort_index) (SELECT l_id1, 1, 651, 'ASC', 3, 0 FROM DUAL);
INSERT INTO filter_group(parent_group_id, filter_group_id, separator) (SELECT newparent.id, newgroup.id, 'AND' FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=283) newgroup LEFT JOIN (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=NULL) newparent ON 1=1);
SELECT SEQ_FILTER_ID.NEXTVAL INTO l_id1 FROM DUAL;
INSERT INTO filter(filter_id, field_id, filter_operator_id, filter_group_id) (SELECT l_id1, 613, 21, newgroup.id  FROM (SELECT new_group_id as id FROM tmp_map_filter_group_ids WHERE old_group_id=283) newgroup);
INSERT INTO filter_param(filter_id, param_index, param_name, param_prompt, param_value, param_label, param_sql_type, param_editor) (SELECT l_id1, 1, 'BusinessUnitId', 'Enter the value for BusinessUnitId', '', 'BusinessUnitId', 'ARRAY:NUMERIC_LIST', 'TEXT:;^(\d+[,])*\d+$' FROM DUAL);
UPDATE FOLIO SET FILTER_GROUP_ID = (SELECT new_group_id FROM tmp_map_filter_group_ids WHERE old_group_id=283) WHERE FOLIO_ID = TO_CHAR(l_id0);
END;

