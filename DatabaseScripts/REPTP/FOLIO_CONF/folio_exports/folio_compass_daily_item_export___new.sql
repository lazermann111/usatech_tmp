SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;
DECLARE
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);
BEGIN
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(1554);
SELECT 1554 INTO l_ids(1) FROM DUAL;
INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'Compass Daily Item Export - NEW', 'LITERAL:Daily Item Detail', 'MESSAGE:for Export Group #{params.ExportGroupId}', 22, 7, NULL, -1, -1 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 1, 'LITERAL:Vendor Code', '', '', 'LITERAL:USAT', '', '', 'LITERAL:USAT', '', 2, '', '', 'STRING' FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 1, 'LITERAL:Item Ref #', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3005, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 1, 'LITERAL:Bank Acct Id', '', '', 'NUMBER:#0', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3023, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 4, 1, 'LITERAL:Device', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 60, 'ASC', 8, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 5, 1, 'LITERAL:Asset #', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 761, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 6, 1, 'LITERAL:Item Type', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3007, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 7, 1, 'LITERAL:Orig Ref #', '', '', 'NUMBER:#0', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3008, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 8, 1, 'LITERAL:Item Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', '', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3010, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 9, 1, 'LITERAL:Card Number', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3016, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 10, 1, 'LITERAL:Column(s)', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3017, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 11, 1, 'LITERAL:Amount', '', '', 'Currency', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3012, 'ASC', 1, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 12, 1, 'LITERAL:Quantity', '', '', '', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3013, 'ASC', 1, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 13, 1, 'LITERAL:Fee Rate', '', '', 'MESSAGE:{,CHOICE,0#|0<''{,NUMBER,''$''#0.00}''}', '', '', 'NUMBER:#0.00', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3015, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 14, 1, 'LITERAL:Settle Status', '', '', 'MATCH:Settled=SETTLED;Pending=PENDING;Declined=FAILED;Failed=FAILED;Processed=SETTLED', '', '', 'MATCH:Settled=SETTLED;Pending=PENDING;Declined=FAILED;Failed=FAILED;Processed=SETTLED', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3018, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 15, 1, 'LITERAL:Payment #', '', '', '', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 16, 1, 'LITERAL:Approval Code', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3019, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 17, 1, 'LITERAL:Description', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 3020, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 18, 1, 'LITERAL:Card Id', '', '', '', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 553, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 3022, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'ExportGroupId', 'Enter the value for ExportGroupId', '', 'ExportGroupId', 'NUMERIC', 'NUMBER' FROM DUAL);
l_ids.TRIM;
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);
l_ids.TRIM;
END;
/
COMMIT;
