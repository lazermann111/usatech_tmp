--SELECT * FROM web_content.web_link where web_link_usage = 'W'


INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(91,'Credit Card Summary','./frame.i?content=selection_date_range.i&folioId=25&StartDate={*-604800000}&EndDate={*0}','Summary of All Credit Transactions in the specified Date Range','Transaction Summaries','W',10)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(92,'Pass-Access Card Summary','./frame.i?content=selection_date_range.i&folioId=27&StartDate={*-604800000}&EndDate={*0}','Summary of All Pass or Access Card Transactions in the specified Date Range','Transaction Summaries','W',10)
/
INSERT INTO web_content.web_link
(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER)
VALUES
(93,'Refund Summary','./frame.i?content=selection_date_range.i&folioId=31&StartDate={*-604800000}&EndDate={*0}','Summary of All Refunds created in the specified Date Range','Transaction Summaries','W',10)
/

INSERT INTO web_content.web_link_requirement(web_link_id, requirement_id)
SELECT L.ID, R.ID
FROM (SELECT 1 ID FROM DUAL UNION ALL SELECT 4 FROM DUAL) R,
(SELECT 91 ID FROM DUAL UNION ALL SELECT 92 FROM DUAL UNION ALL SELECT 93 FROM DUAL) L;

COMMIT;
--SELECT * FROM web_content.web_link_requirement

