DECLARE
    l_field_id NUMBER;
BEGIN
    --SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;
    l_field_id := 440;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG)
    (SELECT
        l_field_id,
        'Transaction Date/Time',
        'REPORT.ACTIVITY_REF.TRAN_DATE',
        'REPORT.ACTIVITY_REF.TRAN_DATE',
        'DATE:MM/dd/yyyy HH:mm:ss PM',
        'DATE',
        'DATE',
        50,
        8,
        'Y'
    FROM DUAL);
    
    INSERT INTO FOLIO_CONF.field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO FOLIO_CONF.field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO FOLIO_CONF.field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO FOLIO_CONF.field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO FOLIO_CONF.field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);

	COMMIT;
END;
