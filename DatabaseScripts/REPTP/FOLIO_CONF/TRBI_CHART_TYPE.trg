CREATE OR REPLACE TRIGGER folio_conf.trbi_chart_type
BEFORE INSERT ON folio_conf.chart_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        --NVL(:NEW.CHART_TYPE_ID, SEQ_CHART_TYPE_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        --:NEW.CHART_TYPE_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/