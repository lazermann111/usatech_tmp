CREATE OR REPLACE TRIGGER folio_conf.trbi_filter
BEFORE INSERT ON folio_conf.filter
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        NVL(:NEW.FILTER_ID, SEQ_FILTER_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.FILTER_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/