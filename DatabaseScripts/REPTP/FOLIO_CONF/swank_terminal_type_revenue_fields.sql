DECLARE
	l_field_category_id NUMBER;
    l_field_id NUMBER;
BEGIN
    SELECT FOLIO_CONF.SEQ_FIELD_CATEGORY_ID.NEXTVAL INTO l_field_category_id FROM DUAL;

	INSERT INTO FOLIO_CONF.FIELD_CATEGORY(
		FIELD_CATEGORY_ID,
		FIELD_CATEGORY_LABEL,
		FIELD_CATEGORY_DESC,
		PARENT_FIELD_CATEGORY_ID,
		ACTIVE_FLAG
	) VALUES (
		l_field_category_id,
		'Swank',
		NULL,
		1,
		'Y'
	);

    -- PC
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'PC Gross Credit Card Revenue',
        NULL,
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PCCC'',CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PCCC'',CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- PDPA
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'PDPA Gross Credit Card Revenue',
        NULL,
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PDCC'',CORP.LEDGER.AMOUNT,''PACC'',CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PDCC'',CORP.LEDGER.AMOUNT,''PACC'',CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    --PF
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;
    
    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'PF Gross Credit Card Revenue',
        NULL,
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PFCC'',CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PFCC'',CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- PP
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;
    
    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'PP Gross Credit Card Revenue',
        NULL,
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PPCC'',CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''PPCC'',CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- K1
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;
    
    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'K1 Gross Credit Card Revenue',
        NULL,
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''K1CC'',CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CONCAT(SUBSTR(REPORT.EPORT.EPORT_SERIAL_NUM,1,2),CORP.LEDGER.ENTRY_TYPE),''K1CC'',CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Hurdle
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Monthly Hurdle',
        NULL,
        'SUM(DECODE(CORP.SERVICE_FEES.FEE_ID,7,CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CORP.SERVICE_FEES.FEE_ID,7,CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        15, --Financial/Entry Metrics
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Internet/Admin Fees
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Internet and Admin Fees',
        NULL,
        'SUM(DECODE(CORP.SERVICE_FEES.FEE_ID,5,CORP.LEDGER.AMOUNT,6,CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CORP.SERVICE_FEES.FEE_ID,5,CORP.LEDGER.AMOUNT,6,CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Refund + Chargeback
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Refunds and Chargebacks',
        NULL,
        'SUM(DECODE(CORP.LEDGER.ENTRY_TYPE,''RF'',CORP.LEDGER.AMOUNT,''CB'',CORP.LEDGER.AMOUNT,0))',
        'SUM(DECODE(CORP.LEDGER.ENTRY_TYPE,''RF'',CORP.LEDGER.AMOUNT,''CB'',CORP.LEDGER.AMOUNT,0))',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        l_field_category_id,
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Net Revenue
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Net Revenue',
        NULL,
        'SUM(CORP.LEDGER.AMOUNT)',
        'SUM(CORP.LEDGER.AMOUNT)',
        NULL,
        'DOUBLE',
        'DOUBLE',
        50,
        15, --Financial/Entry Metrics
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Batch Closable
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Batch Closable',
        NULL,
    	'CORP.PAYMENTS_PKG.BATCH_CLOSABLE(CORP.LEDGER.BATCH_ID)',
    	'CORP.PAYMENTS_PKG.BATCH_CLOSABLE(CORP.LEDGER.BATCH_ID)',
        NULL,
        'VARCHAR',
        'VARCHAR',
        50,
        17, --Financial/Entry Details
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Entry Payable
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Entry Payable',
        NULL,
    	'CORP.PAYMENTS_PKG.ENTRY_PAYABLE(CORP.LEDGER.SETTLE_STATE_ID,CORP.LEDGER.ENTRY_TYPE)',
    	'CORP.PAYMENTS_PKG.ENTRY_PAYABLE(CORP.LEDGER.SETTLE_STATE_ID,CORP.LEDGER.ENTRY_TYPE)',
        NULL,
        'VARCHAR',
        'VARCHAR',
        50,
        17, --Financial/Entry Details
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    -- Doc Status
    SELECT FOLIO_CONF.SEQ_FIELD_ID.NEXTVAL INTO l_field_id FROM DUAL;

    INSERT INTO FOLIO_CONF.FIELD(
        FIELD_ID,
        FIELD_LABEL,
        FIELD_DESC,
        DISPLAY_EXPRESSION,
        SORT_EXPRESSION,
        DISPLAY_FORMAT,
        DISPLAY_SQL_TYPE,
        SORT_SQL_TYPE,
        IMPORTANCE,
        FIELD_CATEGORY_ID,
        ACTIVE_FLAG
    ) VALUES (
        l_field_id,
        'Doc Status',
        NULL,
    	'CORP.DOC.STATUS',
    	'CORP.DOC.STATUS',
        NULL,
        'VARCHAR',
        'VARCHAR',
        50,
        17, --Financial/Entry Details
        'Y'
    );
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        1,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        8,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        5,
        l_field_id,
        1,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        2,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
    
    INSERT INTO field_priv(
        FIELD_PRIV_ID,
        USER_GROUP_ID,
        FIELD_ID,
        FILTER_GROUP_ID,
        ACTIVE_FLAG)
    (SELECT
        FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL,
        3,
        l_field_id,
        NULL,
        'Y'
    FROM DUAL);
END;

    
