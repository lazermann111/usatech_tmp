CREATE OR REPLACE TRIGGER folio_conf.trbi_folio_pillar
BEFORE INSERT ON folio_conf.folio_pillar
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        NVL(:NEW.FOLIO_PILLAR_ID, SEQ_FOLIO_PILLAR_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.FOLIO_PILLAR_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/