ALTER TABLE FOLIO_CONF.field ADD created_by_user_id NUMBER;
ALTER TABLE FOLIO_CONF.field ADD last_updated_by_user_id NUMBER;
ALTER TABLE FOLIO_CONF.field ADD active_flag CHAR(1) DEFAULT 'Y' NOT NULL;

ALTER TABLE FOLIO_CONF.field ADD CONSTRAINT FK_field_created_by_user_id FOREIGN KEY (created_by_user_id) REFERENCES REPORT.user_login(user_id);
ALTER TABLE FOLIO_CONF.field ADD CONSTRAINT FK_field_last_updated_by_id FOREIGN KEY (last_updated_by_user_id) REFERENCES REPORT.user_login(user_id);
ALTER TABLE FOLIO_CONF.field ADD CONSTRAINT CH_field_active_flag CHECK (active_flag IN ('Y','N'));
