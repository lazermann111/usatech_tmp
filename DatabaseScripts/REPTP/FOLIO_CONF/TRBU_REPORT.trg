CREATE OR REPLACE TRIGGER FOLIO_CONF.TRBU_REPORT
BEFORE UPDATE ON FOLIO_CONF.REPORT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_TS,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_TS,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/
