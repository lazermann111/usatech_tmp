CREATE OR REPLACE TRIGGER folio_conf.trbu_field_category
BEFORE UPDATE ON folio_conf.field_category
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_TS,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_TS,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/