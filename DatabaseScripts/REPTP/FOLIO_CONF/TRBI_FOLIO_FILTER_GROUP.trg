CREATE OR REPLACE TRIGGER folio_conf.trbi_folio_filter_group
BEFORE INSERT ON folio_conf.folio_filter_group
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        NVL(:NEW.FOLIO_FILTER_GROUP_ID, SEQ_FOLIO_FILTER_GROUP_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.FOLIO_FILTER_GROUP_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/