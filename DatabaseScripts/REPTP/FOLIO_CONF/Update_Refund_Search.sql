SELECT * 
FROM folio_conf.folio fo, folio_conf.vw_filter_group_hierarchy h, folio_conf.filter f, folio_conf.filter_param fp
WHERE fo.filter_group_id = h.ancestor_filter_group_id AND h.descendent_filter_group_id = f.filter_group_id
AND f.filter_id = fp.filter_id AND fo.folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions')
AND f.field_id IN(466,464,25);

DELETE FROM folio_conf.filter_param
WHERE filter_id IN(
SELECT f.filter_id 
FROM folio_conf.folio fo, folio_conf.vw_filter_group_hierarchy h, folio_conf.filter f
WHERE fo.filter_group_id = h.ancestor_filter_group_id AND h.descendent_filter_group_id = f.filter_group_id
AND fo.folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions')
AND f.field_id IN(466,464,25)
);

DELETE FROM folio_conf.filter
WHERE filter_group_id IN(
SELECT h.descendent_filter_group_id 
FROM folio_conf.folio fo, folio_conf.vw_filter_group_hierarchy h
WHERE fo.filter_group_id = h.ancestor_filter_group_id
AND fo.folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions'))
AND field_id IN(466,464,25)
;

COMMIT;

SELECT * 
FROM folio_conf.folio fo, folio_conf.folio_pillar fp
WHERE fo.folio_id = fp.folio_id AND fo.folio_name = 'Refund Search Transactions';

UPDATE folio_conf.folio_pillar SET PILLAR_LABEL = 'Device'
WHERE folio_id IN(SELECT FOLIO_ID FROM folio_conf.folio WHERE folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions'))
AND PILLAR_LABEL = 'Device Serial Num';

UPDATE folio_conf.folio_pillar SET PILLAR_LABEL = 'Location'
WHERE folio_id IN(SELECT FOLIO_ID FROM folio_conf.folio WHERE folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions'))
AND PILLAR_LABEL = 'Location Name';

UPDATE folio_conf.folio_pillar SET PILLAR_LABEL = 'Amount'
WHERE folio_id IN(SELECT FOLIO_ID FROM folio_conf.folio WHERE folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions'))
AND PILLAR_LABEL = 'Currency Symbol';

UPDATE folio_conf.folio_pillar SET PILLAR_LABEL = 'AP Code'
WHERE folio_id IN(SELECT FOLIO_ID FROM folio_conf.folio WHERE folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions'))
AND PILLAR_LABEL = 'Cc Appr Code';

UPDATE folio_conf.folio_pillar SET PILLAR_LABEL = 'Card Number'
WHERE folio_id IN(SELECT FOLIO_ID FROM folio_conf.folio WHERE folio_name IN('Refund Search Transactions', 'Chargeback Search Transactions'))
AND PILLAR_LABEL = 'Credit Card Number';

COMMIT;


SELECT * FROM FOLIO_CONF.DIRECTIVE;

INSERT INTO FOLIO_CONF.DIRECTIVE(DIRECTIVE_ID,DIRECTIVE_NAME,DIRECTIVE_DESCRIPTION)
VALUES(8,'show-run-date','Whether to display the run date on the report (default is true)');

INSERT INTO FOLIO_CONF.DIRECTIVE(DIRECTIVE_ID,DIRECTIVE_NAME,DIRECTIVE_DESCRIPTION)
VALUES(9,'table-width','Dictates the width of each folio table - can be a measurement or a percentage');

COMMIT;

INSERT INTO FOLIO_CONF.FOLIO_DIRECTIVE(FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE)
SELECT FOLIO_ID, 9, '665px'
FROM FOLIO_CONF.FOLIO
WHERE FOLIO_NAME IN('Refund Search Transactions', 'Chargeback Search Transactions');

INSERT INTO FOLIO_CONF.FOLIO_DIRECTIVE(FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE)
SELECT FOLIO_ID, 8, 'false'
FROM FOLIO_CONF.FOLIO
WHERE FOLIO_NAME IN('Refund Search Transactions', 'Chargeback Search Transactions');

COMMIT;



SELECT * FROM FOLIO_CONF.FOLIO_DIRECTIVE;

