INSERT INTO FOLIO_CONF.filter_operator(
    FILTER_OPERATOR_ID,
    OPERATOR_NAME,
    OPERATOR_DESC,
    OPERATOR_PATTERN,
    SQL_TYPES
) VALUES (
    23,
    'LIKE ? || ''%''',
    'like, begins with',
    '{display} LIKE ? || ''''%''''',
    NULL
);

INSERT INTO FOLIO_CONF.filter_operator(
    FILTER_OPERATOR_ID,
    OPERATOR_NAME,
    OPERATOR_DESC,
    OPERATOR_PATTERN,
    SQL_TYPES
) VALUES (
    24,
    'PARAM IS NULL',
    'parameter passed in is null',
    '? IS NULL',
    NULL
);

COMMIT;
