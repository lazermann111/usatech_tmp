INSERT INTO folio_conf.field
(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,
DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,
CREATED_BY_USER_ID,LAST_UPDATED_BY_USER_ID,ACTIVE_FLAG)
VALUES(701,'User Id (for Customer Bank)',NULL,
'REPORT.VW_USER_CUSTOMER_BANK.USER_ID','REPORT.VW_USER_CUSTOMER_BANK.USER_ID',
'LITERAL:***','VARCHAR','NUMERIC',50,12,1,1,'Y');

INSERT INTO folio_conf.filter_group(FILTER_GROUP_ID,SEPARATOR,PARENT_GROUP_ID)
VALUES(100,'AND',NULL);

INSERT INTO folio_conf.filter(FILTER_ID,FIELD_ID,FILTER_OPERATOR_ID,FILTER_GROUP_ID)
VALUES(200,701,1,100);

INSERT INTO folio_conf.filter_param(FILTER_ID,PARAM_INDEX,PARAM_NAME,PARAM_PROMPT,
	PARAM_VALUE,PARAM_LABEL,PARAM_SQL_TYPE,PARAM_EDITOR)
VALUES(200,1,'user.userId',NULL,NULL,NULL,'NUMERIC',NULL);

INSERT INTO folio_conf.join_filter(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES(20,'CORP.DOC','REPORT.VW_USER_CUSTOMER_BANK',
'CORP.DOC.CUSTOMER_BANK_ID = REPORT.VW_USER_CUSTOMER_BANK.CUSTOMER_BANK_ID');

INSERT INTO folio_conf.join_filter(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES(300,'CORP.CUSTOMER_BANK','REPORT.VW_USER_CUSTOMER_BANK',
'CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID = REPORT.VW_USER_CUSTOMER_BANK.CUSTOMER_BANK_ID');

COMMIT;

SELECT * FROM folio_conf.join_filter ORDER BY join_filter_id


