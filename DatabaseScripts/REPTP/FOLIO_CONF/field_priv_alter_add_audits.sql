ALTER TABLE FOLIO_CONF.field_priv ADD created_by_user_id NUMBER;
ALTER TABLE FOLIO_CONF.field_priv ADD last_updated_by_user_id NUMBER;
ALTER TABLE FOLIO_CONF.field_priv ADD active_flag CHAR(1) DEFAULT 'Y' NOT NULL;

ALTER TABLE FOLIO_CONF.field_priv ADD CONSTRAINT FK_field_priv_created_by_id FOREIGN KEY (created_by_user_id) REFERENCES REPORT.user_login(user_id);
ALTER TABLE FOLIO_CONF.field_priv ADD CONSTRAINT FK_field_prv_last_update_by_id FOREIGN KEY (last_updated_by_user_id) REFERENCES REPORT.user_login(user_id);
ALTER TABLE FOLIO_CONF.field_priv ADD CONSTRAINT CH_field_priv_active_flag CHECK (active_flag IN ('Y','N'));
