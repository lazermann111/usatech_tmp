ALTER TABLE folio_conf.folio
DROP CONSTRAINT fk_f_default_chart_type_id;

ALTER TABLE folio_conf.chart_config
DROP CONSTRAINT fk_chart_cfg_chart_type;

ALTER TABLE folio_conf.chart_type RENAME TO chart_type_reorg;
ALTER TABLE folio_conf.chart_type_reorg drop constraint PK_CHART_TYPE;

CREATE TABLE folio_conf.chart_type
    (chart_type_id                  NUMBER NOT NULL,
    chart_type_cd                  VARCHAR2(50) NOT NULL,
    chart_type_name                VARCHAR2(100) NOT NULL,
    chart_type_description         VARCHAR2(4000),
    created_by                     VARCHAR2(30),
    created_ts                     DATE,
    last_updated_by                VARCHAR2(30),
    last_updated_ts                DATE
  ,
  CONSTRAINT PK_CHART_TYPE
  PRIMARY KEY (chart_type_id)
  USING INDEX
  TABLESPACE  folio_conf_data)
  TABLESPACE  folio_conf_data;

INSERT INTO folio_conf.chart_type(chart_type_id, chart_type_cd, chart_type_name, chart_type_description, 
    created_by, created_ts, last_updated_by, last_updated_ts)
SELECT chart_type_id, chart_type_cd, chart_type_name, chart_type_description, 
    created_by, created_ts, last_updated_by, last_updated_ts
FROM folio_conf.chart_type_reorg;
COMMIT;

DROP TABLE folio_conf.chart_type_reorg;

ALTER TABLE folio_conf.folio
ADD CONSTRAINT fk_f_default_chart_type_id FOREIGN KEY (default_chart_type_id)
REFERENCES FOLIO_CONF.chart_type (chart_type_id)
/
ALTER TABLE folio_conf.chart_config
ADD CONSTRAINT fk_chart_cfg_chart_type FOREIGN KEY (chart_type_id)
REFERENCES FOLIO_CONF.chart_type (chart_type_id)
/
CREATE OR REPLACE TRIGGER folio_conf.trbi_chart_type
 BEFORE
  INSERT
 ON folio_conf.chart_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT 
        --NVL(:NEW.CHART_TYPE_ID, SEQ_CHART_TYPE_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        --:NEW.CHART_TYPE_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER folio_conf.trbu_chart_type
 BEFORE
  UPDATE
 ON folio_conf.chart_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_TS,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_TS,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

