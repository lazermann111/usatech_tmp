ALTER TABLE FOLIO_CONF.field_category ADD created_by_user_id NUMBER;
ALTER TABLE FOLIO_CONF.field_category ADD last_updated_by_user_id NUMBER;
ALTER TABLE FOLIO_CONF.field_category ADD active_flag CHAR(1) DEFAULT 'Y' NOT NULL;

ALTER TABLE FOLIO_CONF.field_category ADD CONSTRAINT FK_field_cat_created_by_id FOREIGN KEY (created_by_user_id) REFERENCES REPORT.user_login(user_id);
ALTER TABLE FOLIO_CONF.field_category ADD CONSTRAINT FK_field_cat_last_update_by_id FOREIGN KEY (last_updated_by_user_id) REFERENCES REPORT.user_login(user_id);
ALTER TABLE FOLIO_CONF.field_category ADD CONSTRAINT CH_field_cat_active_flag CHECK (active_flag IN ('Y','N'));
