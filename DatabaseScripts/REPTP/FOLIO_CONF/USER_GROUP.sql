CREATE TABLE FOLIO_CONF.USER_GROUP(
    USER_GROUP_ID   NUMBER NOT NULL,
    DESCRIPTION     VARCHAR2(200),
    CONSTRAINT PK_USER_GROUP
  PRIMARY KEY (USER_GROUP_ID)
  USING INDEX
  TABLESPACE  FOLIO_CONF_DATA)
  TABLESPACE  FOLIO_CONF_DATA
;


INSERT INTO FOLIO_CONF.USER_GROUP (user_group_id,description) VALUES
    (1,'System Administrator (Corresponds to User Type 3)');
INSERT INTO FOLIO_CONF.USER_GROUP (user_group_id,description) VALUES
    (2,'Accounting (Corresponds to User Type 2)');
INSERT INTO FOLIO_CONF.USER_GROUP (user_group_id,description) VALUES
    (3,'Customer Service (Corresponds to User Type 1)');
INSERT INTO FOLIO_CONF.USER_GROUP (user_group_id,description) VALUES
    (4,'Management (Corresponds to User Type 4)');
INSERT INTO FOLIO_CONF.USER_GROUP (user_group_id,description) VALUES
    (5,'Sales (Corresponds to User Type 9)');
INSERT INTO FOLIO_CONF.USER_GROUP (user_group_id,description) VALUES
    (8,'Customer (Corresponds to User Type 8)');
    
COMMIT;
