delete from folio_conf.join_filter;
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(13,'REPORT.ACTIVITY_REF','REPORT.TRANS','REPORT.ACTIVITY_REF.TRAN_ID = REPORT.TRANS.TRAN_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(14,'REPORT.TRANS','CORP.MERCHANT','REPORT.TRANS.MERCHANT_ID = CORP.MERCHANT.MERCHANT_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(46,'CORP.BATCH','REPORT.TERMINAL','CORP.BATCH.TERMINAL_ID = REPORT.TERMINAL.TERMINAL_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(5,'CORP.LEDGER',NULL,'CORP.LEDGER.DELETED = ''N''')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(7,'CORP.LEDGER','CORP.BATCH','CORP.LEDGER.BATCH_ID = CORP.BATCH.BATCH_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(8,'CORP.BATCH','CORP.CUSTOMER_BANK','CORP.BATCH.CUSTOMER_BANK_ID = CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(9,'CORP.LEDGER','REPORT.TRANS','CORP.LEDGER.TRANS_ID = REPORT.TRANS.TRAN_ID (+)')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(50,'CORP.CUSTOMER_BANK','CORP.CUSTOMER','CORP.CUSTOMER_BANK.CUSTOMER_ID = CORP.CUSTOMER.CUSTOMER_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(11,'CORP.BATCH','CORP.DOC','CORP.BATCH.DOC_ID = CORP.DOC.DOC_ID (+)')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(41,'CORP.LEDGER','CORP.SERVICE_FEES','CORP.LEDGER.SERVICE_FEE_ID = CORP.SERVICE_FEES.SERVICE_FEE_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(42,'CORP.SERVICE_FEES','CORP.FEES','CORP.SERVICE_FEES.FEE_ID = CORP.FEES.FEE_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(43,'CORP.LEDGER','CORP.CURRENCY','CORP.LEDGER.CURRENCY_ID = CORP.CURRENCY.CURRENCY_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(45,'REPORT.TERMINAL','REPORT.LOCATION','REPORT.TERMINAL.LOCATION_ID = REPORT.LOCATION.LOCATION_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(40,'REPORT.TERMINAL','REPORT.EPORT','REPORT.TERMINAL.EPORT_ID = REPORT.EPORT.EPORT_ID ')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(39,'REPORT.EPORT','REPORT.DEVICE_TYPE','REPORT.EPORT.DEVICE_TYPE_ID = REPORT.DEVICE_TYPE.DEVICE_TYPE_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(3,'REPORT.ACTIVITY_REF','REPORT.TERMINAL','REPORT.ACTIVITY_REF.TERMINAL_ID = REPORT.TERMINAL.TERMINAL_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(38,'REPORT.TERMINAL','CORP.CUSTOMER','REPORT.TERMINAL.CUSTOMER_ID = CORP.CUSTOMER.CUSTOMER_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(12,'REPORT.ACTIVITY_REF','CORP.CURRENCY','REPORT.ACTIVITY_REF.CURRENCY_ID = CORP.CURRENCY.CURRENCY_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(47,'REPORT.TERMINAL','REPORT.VW_USER_TERMINAL','REPORT.TERMINAL.TERMINAL_ID = REPORT.VW_USER_TERMINAL.TERMINAL_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(48,'REPORT.TERMINAL','REPORT.VW_USER_TERMINAL','REPORT.TERMINAL.TERMINAL_ID = REPORT.VW_USER_TERMINAL.TERMINAL_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(4,'REPORT.ACTIVITY_REF','REPORT.EPORT','REPORT.ACTIVITY_REF.EPORT_ID = REPORT.EPORT.EPORT_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(6,'REPORT.ACTIVITY_REF','REPORT.LOCATION','REPORT.ACTIVITY_REF.LOCATION_ID = REPORT.LOCATION.LOCATION_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(37,'REAL_TIME.DEVICE_CALL_IN_RECORD','REPORT.TERMINAL','REAL_TIME.DEVICE_CALL_IN_RECORD.SERIAL_NUMBER = REPORT.EPORT.EPORT_SERIAL_NUM
AND REPORT.EPORT.EPORT_ID = REPORT.TERMINAL_EPORT.EPORT_ID
AND REPORT.TERMINAL_EPORT.TERMINAL_ID = REPORT.TERMINAL.TERMINAL_ID
AND REAL_TIME.DEVICE_CALL_IN_RECORD.CALL_IN_START_TS BETWEEN NVL(REPORT.TERMINAL_EPORT.START_DATE, MIN_DATE)
AND  NVL(REPORT.TERMINAL_EPORT.END_DATE, MAX_DATE)')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(55,'REPORT.EPORT','REPORT.LOCATION','REPORT.EPORT.EPORT_ID = REPORT.TERMINAL_EPORT.EPORT_ID
AND REPORT.TERMINAL_EPORT.TERMINAL_ID = REPORT.TERMINAL.TERMINAL_ID
AND REPORT.TERMINAL.LOCATION_ID = REPORT.LOCATION.LOCATION_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(56,'REPORT.EPORT','CORP.CUSTOMER','REPORT.EPORT.EPORT_ID = REPORT.TERMINAL_EPORT.EPORT_ID
AND REPORT.TERMINAL_EPORT.TERMINAL_ID = REPORT.TERMINAL.TERMINAL_ID
AND REPORT.TERMINAL.CUSTOMER_ID = CORP.CUSTOMER.CUSTOMER_ID')
/
INSERT INTO folio_conf.join_filter
(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION)
VALUES
(52,'REPORT.LOCATION','CORP.CUSTOMER','REPORT.LOCATION.LOCATION_ID = REPORT.TERMINAL.LOCATION_ID
AND REPORT.TERMINAL.CUSTOMER_ID = CORP.CUSTOMER.CUSTOMER_ID')
/


