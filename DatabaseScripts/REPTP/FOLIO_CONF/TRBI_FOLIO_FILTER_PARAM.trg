CREATE OR REPLACE TRIGGER folio_conf.trbi_folio_filter_param
BEFORE INSERT ON folio_conf.folio_filter_param
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/