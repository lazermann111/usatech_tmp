-- Either delete them
DELETE FROM folio_conf.field_priv WHERE field_priv_id IN (
    SELECT privid FROM (
        SELECT field_id,user_group_id,count(*) num,max(field_priv_id) privid 
            FROM folio_conf.field_priv GROUP BY field_id,user_group_id
    ) WHERE num > 1
);

-- Or switch to user_group 3 which is what they are supposed to be
UPDATE folio_conf.field_priv SET user_group_id=3 WHERE field_priv_id IN (
    SELECT privid FROM (
        SELECT field_id,user_group_id,count(*) num,max(field_priv_id) privid 
            FROM folio_conf.field_priv GROUP BY field_id,user_group_id
    ) WHERE num > 1
);