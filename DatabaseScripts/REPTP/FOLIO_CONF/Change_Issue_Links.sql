UPDATE web_content.web_link SET web_link_url = './frame.i?content=chargeback_search.i&folioId=' 
|| (SELECT to_Char(folio_Id) FROM folio_conf.folio WHERE folio_name = 'Chargeback Search Transactions')
WHERE web_link_usage = 'M' AND web_link_label = 'Issue Chargebacks';
UPDATE web_content.web_link SET web_link_url = './frame.i?content=refund_search.i&folioId=' 
|| (SELECT to_Char(folio_Id) FROM folio_conf.folio WHERE folio_name = 'Refund Search Transactions')
WHERE web_link_usage = 'M' AND web_link_label = 'Issue Refunds';
COMMIT;
/*
SELECT * FROM web_content.web_link WHERE web_link_usage = 'M' AND web_link_label like 'Issue%';
SELECT * FROM folio_conf.folio WHERE folio_name IN('Refund Search Transactions','Chargeback Search Transactions')
*/ 

