CREATE OR REPLACE TRIGGER folio_conf.trbi_field_priv
BEFORE INSERT ON folio_conf.field_priv
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        NVL(:NEW.FIELD_PRIV_ID, SEQ_FIELD_PRIV_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.FIELD_PRIV_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/