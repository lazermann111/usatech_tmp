INSERT INTO folio_conf.output_type
(OUTPUT_TYPE_ID,OUTPUT_TYPE_LABEL,OUTPUT_TYPE_CATEGORY,OUTPUT_TYPE_KEY,CONTENT_TYPE)
VALUES
(21,'CSV Export','Report','csv','text/csv');

INSERT INTO folio_conf.directive
(DIRECTIVE_ID,DIRECTIVE_NAME,DIRECTIVE_DESCRIPTION)
VALUES
(11,'show-header','Whether to display the pillar names for CSV reports');



