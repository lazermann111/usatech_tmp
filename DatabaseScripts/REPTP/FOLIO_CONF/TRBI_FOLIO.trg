CREATE OR REPLACE TRIGGER folio_conf.trbi_folio
BEFORE INSERT ON folio_conf.folio
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        NVL(:NEW.FOLIO_ID, SEQ_FOLIO_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.FOLIO_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/