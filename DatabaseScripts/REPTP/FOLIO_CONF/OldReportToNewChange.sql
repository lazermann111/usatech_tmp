ALTER TABLE
   FOLIO_CONF.FOLIO_PILLAR
ADD
   PERCENT_FORMAT  VARCHAR2(2000);

@@FOLIO_PKG.psk
@@FOLIO_PKG.pbk

-- add rangeType operator
insert into folio_conf.filter_operator (filter_operator_id, operator_name, operator_pattern, SQL_TYPES)
values(28, 'DAY RANGE BETWEEN', '{display} BETWEEN TRUNC(?, ''''DD'''') AND TRUNC(?, ''''DD'''')', 'DATE,DATE');
insert into folio_conf.filter_operator (filter_operator_id, operator_name, operator_pattern, SQL_TYPES)
values(29, 'WEEK RANGE BETWEEN', '{display} BETWEEN TRUNC(?, ''''DAY'''') AND TRUNC(?, ''''DAY'''')', 'DATE,DATE');
insert into folio_conf.filter_operator (filter_operator_id, operator_name, operator_pattern, SQL_TYPES)
values(30, 'MONTH RANGE BETWEEN', '{display} BETWEEN TRUNC(?, ''''MONTH'''') AND TRUNC(?, ''''MONTH'''')', 'DATE,DATE');

commit;