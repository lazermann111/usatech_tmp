CREATE OR REPLACE TRIGGER folio_conf.trbi_output_type
BEFORE INSERT ON folio_conf.output_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        --NVL(:NEW.OUTPUT_TYPE_ID, SEQ_OUTPUT_TYPE_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        --:NEW.OUTPUT_TYPE_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/