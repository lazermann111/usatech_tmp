CREATE OR REPLACE TRIGGER folio_conf.trbi_filter_operator
BEFORE INSERT ON folio_conf.filter_operator
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT 
        --NVL(:NEW.FILTER_OPERATOR_ID, SEQ_FILTER_OPERATOR_ID.NEXTVAL),
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        --:NEW.FILTER_OPERATOR_ID,
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/