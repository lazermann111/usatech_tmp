CREATE GLOBAL TEMPORARY TABLE FOLIO_CONF.tmp_map_filter_group_ids (
    old_group_id INT PRIMARY KEY NOT NULL,
    new_group_id INT NOT NULL
) ;
