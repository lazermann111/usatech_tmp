DELETE FROM folio_conf.field_priv WHERE field_priv_id IN (
    SELECT privid FROM (
        SELECT field_id,user_group_id,count(*) num,max(field_priv_id) privid 
            FROM folio_conf.field_priv GROUP BY field_id,user_group_id
    ) WHERE num > 1
);

ALTER TABLE FOLIO_CONF.FIELD_PRIV 
ADD CONSTRAINT UK_FIELD_PRIV_1 UNIQUE (USER_GROUP_ID, FIELD_ID)
USING INDEX
TABLESPACE FOLIO_CONF_INDX;

