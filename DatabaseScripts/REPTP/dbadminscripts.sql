CREATE OR REPLACE PROCEDURE DBADMIN.GET_LOCK_ID (
    l_object_type VARCHAR2,
    l_object_id   VARCHAR2,
    l_handle OUT VARCHAR2)
AS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DBMS_LOCK.ALLOCATE_UNIQUE(l_object_type || CHR(0) || l_object_id, l_handle);
END;
/

GRANT EXECUTE, DEBUG ON DBADMIN.GET_LOCK_ID TO USAT_ADMIN_ROLE;
CREATE OR REPLACE PROCEDURE DBADMIN.print_any(data IN SYS.AnyData) IS
  tn  VARCHAR2(61);
  str VARCHAR2(4000);
  chr CHAR(255);
  num NUMBER;
  dat DATE;
  rw  RAW(4000);
  res NUMBER;
BEGIN
  IF data IS NULL THEN
    DBMS_OUTPUT.PUT_LINE('NULL value');
    RETURN;
  END IF;
  tn := data.GETTYPENAME();
  IF tn = 'SYS.VARCHAR2' THEN
    res := data.GETVARCHAR2(str);
    DBMS_OUTPUT.PUT_LINE(str);
  ELSIF tn = 'SYS.CHAR' then
    res := data.GETCHAR(chr);
    DBMS_OUTPUT.PUT_LINE(chr);
  ELSIF tn = 'SYS.VARCHAR' THEN
    res := data.GETVARCHAR(chr);
    DBMS_OUTPUT.PUT_LINE(chr);
  ELSIF tn = 'SYS.NUMBER' THEN
    res := data.GETNUMBER(num);
    DBMS_OUTPUT.PUT_LINE(num);
  ELSIF tn = 'SYS.DATE' THEN
    res := data.GETDATE(dat);
    DBMS_OUTPUT.PUT_LINE(dat);
  ELSIF tn = 'SYS.RAW' THEN
    res := data.GETRAW(rw);
    DBMS_OUTPUT.PUT_LINE(RAWTOHEX(rw));
  ELSE
    DBMS_OUTPUT.PUT_LINE('typename is ' || tn);
  END IF;
END print_any;
/

GRANT EXECUTE, DEBUG ON DBADMIN.PRINT_ANY TO USAT_ADMIN_ROLE;

GRANT DEBUG ON DBADMIN.PRINT_ANY TO USAT_DEV_READ_ONLY;
CREATE OR REPLACE PROCEDURE DBADMIN.print_lcr(lcr IN SYS.ANYDATA) IS
  typenm   VARCHAR2(61);
  ddllcr   SYS.LCR$_DDL_RECORD;
  proclcr  SYS.LCR$_PROCEDURE_RECORD;
  rowlcr   SYS.LCR$_ROW_RECORD;
  res      NUMBER;
  newlist  SYS.LCR$_ROW_LIST;
  oldlist  SYS.LCR$_ROW_LIST;
  ddl_text CLOB;
BEGIN
  typenm := lcr.GETTYPENAME();
  DBMS_OUTPUT.PUT_LINE('type name: ' || typenm);
  IF (typenm = 'SYS.LCR$_DDL_RECORD') THEN
    res := lcr.GETOBJECT(ddllcr);
    DBMS_OUTPUT.PUT_LINE('source database: ' ||
                         ddllcr.GET_SOURCE_DATABASE_NAME);
    DBMS_OUTPUT.PUT_LINE('owner: ' || ddllcr.GET_OBJECT_OWNER);
    DBMS_OUTPUT.PUT_LINE('object: ' || ddllcr.GET_OBJECT_NAME);
    DBMS_OUTPUT.PUT_LINE('is tag null: ' || ddllcr.IS_NULL_TAG);
    DBMS_LOB.CREATETEMPORARY(ddl_text, TRUE);
    ddllcr.GET_DDL_TEXT(ddl_text);
    DBMS_OUTPUT.PUT_LINE('ddl: ' || ddl_text);
    DBMS_LOB.FREETEMPORARY(ddl_text);
  ELSIF (typenm = 'SYS.LCR$_ROW_RECORD') THEN
    res := lcr.GETOBJECT(rowlcr);
    DBMS_OUTPUT.PUT_LINE('source database: ' ||
                         rowlcr.GET_SOURCE_DATABASE_NAME);
    DBMS_OUTPUT.PUT_LINE('owner: ' || rowlcr.GET_OBJECT_OWNER);
    DBMS_OUTPUT.PUT_LINE('object: ' || rowlcr.GET_OBJECT_NAME);
    DBMS_OUTPUT.PUT_LINE('is tag null: ' || rowlcr.IS_NULL_TAG);
    DBMS_OUTPUT.PUT_LINE('command_type: ' || rowlcr.GET_COMMAND_TYPE);
    oldlist := rowlcr.GET_VALUES('OLD');
    FOR i IN 1..oldlist.COUNT LOOP
      if oldlist(i) is not null then
        DBMS_OUTPUT.PUT_LINE('old(' || i || '): ' || oldlist(i).column_name);
        print_any(oldlist(i).data);
      END IF;
    END LOOP;
    newlist := rowlcr.GET_VALUES('NEW');
    FOR i in 1..newlist.count LOOP
      IF newlist(i) IS NOT NULL THEN
        DBMS_OUTPUT.PUT_LINE('new(' || i || '): ' || newlist(i).column_name);
        print_any(newlist(i).data);
      END IF;
    END LOOP;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Non-LCR Message with type ' || typenm);
  END IF;
END print_lcr;
/

GRANT EXECUTE, DEBUG ON DBADMIN.PRINT_LCR TO USAT_ADMIN_ROLE;

GRANT DEBUG ON DBADMIN.PRINT_LCR TO USAT_DEV_READ_ONLY;

CREATE OR REPLACE PROCEDURE DBADMIN.USAT_CHECK_PARTITIONS_BY_ID AUTHID CURRENT_USER is
--Created by Marie Njanje on 10/24/08 to check partitions with *ID as key daily
Begin
DECLARE
   CURSOR part_list_cursor IS
 select b.table_owner, b.table_name, partition_name, high_value, p.column_name
    from dba_tab_partitions b,DBA_PART_KEY_COLUMNS p,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos,k.column_name
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in('SYS', 'SYSTEM') and table_name not like 'BIN$%' and  table_name<>'ADMIN_CMD'
    AND K.COLUMN_NAME like '%ID' AND T.TABLE_NAME=K.NAME AND T.TABLE_OWNER=K.OWNER
    group by table_owner, table_name,k.column_name) a
    where b.table_name=p.name and b.TABLE_OWNER=p.OWNER and  a.part_pos = PARTITION_POSITION
    and a.column_name=p.column_name
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM','RDW');

    part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     v_col_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_highest_value_num NUMBER;
     tab_high_value NUMBER;

BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_col_name := part_rec.column_name;
     v_high_value := part_rec.high_value;
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value_num;
            EXECUTE IMMEDIATE 'SELECT max('||v_col_name||') from '||v_table_owner||'.'||v_table_name||' ' INTO tab_high_value;
          IF v_highest_value_num < tab_high_value + 2000000 THEN

             DBMS_OUTPUT.PUT_LINE ('The highest partitioned ID on table '||upper(v_table_owner)||'.'||upper(v_table_name)||' is '||v_highest_value_num||' and

the last partition_name is '||upper(v_part_name)||' and max_value of '||v_col_name||' is '||tab_high_value||' , please create next partition');

           END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE DBADMIN.USAT_CHECK_CREATE_PARTS_ID_DEV AUTHID CURRENT_USER is
--Created by Marie Njanje on 10/24/08 to check partitions with *ID as key daily
Begin
DECLARE
  
   CURSOR part_list_cursor IS 
    select b.table_owner, b.table_name, partition_name, high_value, p.column_name
    from dba_tab_partitions b,DBA_PART_KEY_COLUMNS p,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos,k.column_name
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner in('DEVICE') and table_name not like 'BIN$%'
    AND K.COLUMN_NAME like '%ID' AND T.TABLE_NAME=K.NAME AND T.TABLE_OWNER=K.OWNER
    group by table_owner, table_name,k.column_name) a
    where b.table_name=p.name and b.TABLE_OWNER=p.OWNER and  a.part_pos = PARTITION_POSITION
    and a.column_name=p.column_name
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner in('DEVICE') and b.table_name <>'ADMIN_CMD';
    
           part_rec part_list_cursor%ROWTYPE;  
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     v_col_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_highest_value_num NUMBER;
     tab_high_value NUMBER;
     v_sql2 VARCHAR2(4000);
     v_added_value constant NUMBER (8) :=2000000;
         
            
BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_col_name := part_rec.column_name;
     v_high_value := part_rec.high_value;
                 
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN 
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value_num;
                   EXECUTE IMMEDIATE 'SELECT max('||v_col_name||') from '||v_table_owner||'.'||v_table_name||' ' INTO tab_high_value;
                   
                    IF v_highest_value_num < tab_high_value + 5000000 THEN 
                      v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_RANGEID_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql2;
                      END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE DBADMIN.USAT_CHECK_CREATE_PARTS_ID_ENG AUTHID CURRENT_USER is
--Created by Marie Njanje on 10/24/08 to check partitions with *ID as key daily
Begin
DECLARE
  
   CURSOR part_list_cursor IS 
    select b.table_owner, b.table_name, partition_name, high_value, p.column_name
    from dba_tab_partitions b,DBA_PART_KEY_COLUMNS p,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos,k.column_name
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner in('ENGINE') and table_name not like 'BIN$%'
    AND K.COLUMN_NAME like '%ID' AND T.TABLE_NAME=K.NAME AND T.TABLE_OWNER=K.OWNER
    group by table_owner, table_name,k.column_name) a
    where b.table_name=p.name and b.TABLE_OWNER=p.OWNER and  a.part_pos = PARTITION_POSITION
    and a.column_name=p.column_name
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner in('ENGINE') and b.table_name <>'ADMIN_CMD';
    
           part_rec part_list_cursor%ROWTYPE;  
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     v_col_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_highest_value_num NUMBER;
     tab_high_value NUMBER;
     v_sql2 VARCHAR2(4000);
     v_added_value constant NUMBER (8) :=2000000;
         
            
BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_col_name := part_rec.column_name;
     v_high_value := part_rec.high_value;
                 
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN 
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value_num;
                   EXECUTE IMMEDIATE 'SELECT max('||v_col_name||') from '||v_table_owner||'.'||v_table_name||' ' INTO tab_high_value;
                   
                    IF v_highest_value_num < tab_high_value + 5000000 THEN 
                      v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_RANGEID_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql2;
                      END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE DBADMIN.USAT_CHECK_CREATE_PARTS_BY_ID AUTHID CURRENT_USER is
--Created by Marie Njanje on 10/24/08 to check partitions with *ID as key daily
Begin
DECLARE
  
   CURSOR part_list_cursor IS 
    select b.table_owner, b.table_name, partition_name, high_value, p.column_name
    from dba_tab_partitions b,DBA_PART_KEY_COLUMNS p,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos,k.column_name
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in ('SYS', 'SYSTEM','REPORT_ARCH') and table_name not like 'BIN$%'
    AND K.COLUMN_NAME like '%ID' AND T.TABLE_NAME=K.NAME AND T.TABLE_OWNER=K.OWNER
    group by table_owner, table_name,k.column_name) a
    where b.table_name=p.name and b.TABLE_OWNER=p.OWNER and  a.part_pos = PARTITION_POSITION
    and a.column_name=p.column_name
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM','RDW') and b.table_name <>'ADMIN_CMD';
    
           part_rec part_list_cursor%ROWTYPE;  
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     v_col_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_highest_value_num NUMBER;
     tab_high_value NUMBER;
     v_sql2 VARCHAR2(4000);
     v_added_value constant NUMBER (8) :=5000000;
         
            
BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_col_name := part_rec.column_name;
     v_high_value := part_rec.high_value;
                 
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN 
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value_num;
                   EXECUTE IMMEDIATE 'SELECT max('||v_col_name||') from '||v_table_owner||'.'||v_table_name||' ' INTO tab_high_value;
                   
                    IF v_highest_value_num < tab_high_value + 5000000 THEN 
                      v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_RANGEID_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql2;
                      END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE DBADMIN."USAT_CHECK_CREATE_MONTHLY_PART" AUTHID CURRENT_USER is
--Created by Marie Njanje on 01/14/08 to check partitions daily
Begin
DECLARE
   CURSOR part_list_cursor IS
  select b.table_owner, b.table_name, partition_name, num_rows, high_value
    from dba_tab_partitions b,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in ('SYS', 'SYSTEM','REPORT_ARCH','RDW') and table_name not like 'BIN$%'
    AND (K.COLUMN_NAME like '%DATE' or  K.COLUMN_NAME like '%TS' or K.COLUMN_NAME like '%DAY')
    AND TABLE_NAME NOT IN ('MACHINE_CMD_OUTBOUND_HIST','MACHINE_CMD_INBOUND_HIST','APP_LOCK','DEVICE_CALL_IN_RECORD','HOST_COUNTER') AND T.TABLE_NAME=K.NAME AND TABLE_OWNER=K.OWNER
    group by table_owner, table_name ) a
    where a.part_pos = PARTITION_POSITION
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM');

     part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_sql2 VARCHAR2(4000);
     v_added_value constant VARCHAR2(30):= 'SYSDATE';

BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_high_value := part_rec.high_value;
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value;
           IF v_highest_value < SYSDATE +14 THEN

                        v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_MONTHLY_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';

                      EXECUTE IMMEDIATE v_sql2;

           END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE DBADMIN."USAT_CHECK_CREATE_DAILY_PART" AUTHID CURRENT_USER is
--Created by Marie Njanje on 05/6/11 to check partitions daily
Begin
DECLARE
   CURSOR part_list_cursor IS
  select b.table_owner, b.table_name, partition_name, num_rows, high_value
    from dba_tab_partitions b,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in('SYS', 'SYSTEM') and table_name not like 'BIN$%'
    AND (K.COLUMN_NAME like '%DATE' or  K.COLUMN_NAME like '%TS' or K.COLUMN_NAME like '%DAY')
    AND TABLE_NAME  IN ('APP_LOCK','DEVICE_CALL_IN_RECORD','HOST_COUNTER')  AND T.TABLE_NAME=K.NAME AND TABLE_OWNER=K.OWNER
    group by table_owner, table_name ) a
    where a.part_pos = PARTITION_POSITION
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM');
     part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_sql2 VARCHAR2(4000);
     v_added_value constant VARCHAR2(30):= 'SYSDATE +7';
BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_high_value := part_rec.high_value;
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value;
           IF v_highest_value < SYSDATE +5 THEN
                        v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_DAILY_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                      EXECUTE IMMEDIATE v_sql2;
           END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE DBADMIN."GATHER_PART_STATISTICS" AUTHID CURRENT_USER is
--Created by Marie Njanje on 31/05/11 to gather stats on partitions
Begin
DECLARE
   CURSOR part_list_cursor IS
          SELECT table_owner, table_name, partition_name
          FROM ALL_TAB_PARTITIONS
         WHERE table_owner not in('SYS', 'SYSTEM') and last_analyzed is null and table_name<>'FILE_TRANSFER';
      part_rec part_list_cursor%ROWTYPE;
      v_table_owner VARCHAR2(30);
      v_table_name VARCHAR2(30);
      v_part_name VARCHAR2(30);
      v_sql VARCHAR2(4000);
      BEGIN
      FOR part_rec IN part_list_cursor LOOP
      v_table_owner := part_rec.table_owner;
      v_table_name := part_rec.table_name;
      v_part_name := part_rec.partition_name;
         v_sql := 'begin'||chr(10)||'sys.dbms_stats.gather_table_stats(ownname=>'''||v_table_owner||''',tabname=> '''||v_table_name||''',partname=> ''"'||v_part_name||'"'',estimate_percent=> DBMS_STATS.AUTO_SAMPLE_SIZE,granularity=> ''PARTITION'', cascade=> TRUE);'||chr(10)||'end;';
         DBMS_OUTPUT.PUT_LINE (v_sql);
         EXECUTE IMMEDIATE v_sql;
    END LOOP;
END;
END;
/


