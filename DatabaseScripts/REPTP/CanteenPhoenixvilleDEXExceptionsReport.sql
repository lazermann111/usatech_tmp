DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;	
	LN_USER_ID REPORT.USER_LOGIN.USER_ID%TYPE;
	LN_USER_REPORT_ID REPORT.USER_REPORT.USER_REPORT_ID%TYPE;
	LN_CCS_TRANSPORT_ID REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE;
	LV_EMAIL_ADDRESSES REPORT.CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE := 'dkouznetsov@usatech.com, gharrum@usatech.com, jsimpkins@usatech.com, tcattani@usatech.com';
	LV_DB_NAME VARCHAR2(30);
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Canteen Phoenixville DEX Exceptions';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;
	
	SELECT USER_ID INTO LN_USER_ID FROM REPORT.USER_LOGIN WHERE USER_NAME = 'USATMaster';
	
	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'Canteen Phoenixville DEX Exceptions', 6, 0, 'Canteen Phoenixville DEX Exceptions', 'Canteen Phoenixville devices with DEX exceptions', 'N', LN_USER_ID);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'select d.device_serial_cd, d.firmware_version, df.dex_date, rs.sent_date,
case when df.exception_count > 0 then df.file_content else null end dex_error, 
case when rs.details is null or rs.details like ''%RESULT=Success (0)'' then null else rs.details end upload_error
from report.terminal t 
join report.vw_terminal_eport te on t.terminal_id = te.terminal_id
join report.eport e on te.eport_id = e.eport_id
join device.device d on e.eport_serial_num = d.device_serial_cd and d.device_active_yn_flag = ''Y''
left outer join g4op.dex_file df on e.eport_id = df.eport_id and df.dex_date between trunc(sysdate) - 1 and trunc(sysdate)
left outer join report.report_sent rs on df.dex_file_id = rs.batch_id and rs.user_report_id = 100079467
where t.customer_id = 1973 and e.lastdialin_date > trunc(sysdate) - 1 + 13/24
and (df.exception_count > 0
or e.eport_id not in (
select df.eport_id
from report.report_sent rs
join g4op.dex_file df on rs.batch_id = df.dex_file_id
where rs.sent_date between trunc(sysdate) - 1 + 1/24 and trunc(sysdate) - 1 + 2/24 and rs.user_report_id = 100079467
and rs.details like ''%RESULT=Success (0)''
) or e.eport_id not in (
select df.eport_id
from report.report_sent rs
join g4op.dex_file df on rs.batch_id = df.dex_file_id
where rs.sent_date between trunc(sysdate) - 1 + 13/24 and trunc(sysdate) - 1 + 14/24 and rs.user_report_id = 100079467
and rs.details like ''%RESULT=Success (0)''
))
order by case when df.exception_count > 0 then 1 else 2 end, d.device_serial_cd, df.dex_date');
		
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'header', 'true');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'outputType', '21');	
	
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '') INTO LV_DB_NAME FROM GLOBAL_NAME;
	
	SELECT REPORT.CCS_TRANSPORT_SEQ.NEXTVAL INTO LN_CCS_TRANSPORT_ID FROM DUAL;
	
	INSERT INTO REPORT.CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME, USER_ID)
	SELECT LN_CCS_TRANSPORT_ID, 1, 'Canteen Phoenixville DEX Exceptions', LN_USER_ID
	FROM DUAL;
	
	INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE)
	SELECT REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, LN_CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, LV_EMAIL_ADDRESSES
	FROM REPORT.CCS_TRANSPORT_PROPERTY_TYPE WHERE CCS_TPT_NAME = 'Email';
	
	INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE)
	SELECT REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, LN_CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, NULL
	FROM REPORT.CCS_TRANSPORT_PROPERTY_TYPE WHERE CCS_TPT_NAME = 'CC';
	
	INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE)
	SELECT REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, LN_CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, 'true'
	FROM REPORT.CCS_TRANSPORT_PROPERTY_TYPE WHERE CCS_TPT_NAME = 'As Zip File';
	
	SELECT REPORT.USER_REPORT_SEQ.NEXTVAL INTO LN_USER_REPORT_ID FROM DUAL;
	
	INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, REPORT_ID, USER_ID, CCS_TRANSPORT_ID, FREQUENCY_ID, LATENCY, STATUS)
	SELECT LN_USER_REPORT_ID, LN_REPORT_ID, LN_USER_ID, LN_CCS_TRANSPORT_ID, 4, 0.25, 'A'
	FROM DUAL;	
	
	COMMIT;
END;
/