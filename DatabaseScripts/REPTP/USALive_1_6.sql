SET DEFINE OFF;
GRANT SELECT ON REPORT.TRANS_STAT_BY_DAY TO USALIVE_APP_ROLE;
GRANT EXECUTE ON REPORT.GET_TRANS_DETAILS TO USALIVE_APP_ROLE;

ALTER TABLE REPORT.REPORT_REQUEST ADD(REPORT_NAME VARCHAR2(256));

CREATE BITMAP INDEX REPORT.BIX_RRU_RRU_STATUS_ID ON  REPORT.REPORT_REQUEST_USER(REPORT_REQUEST_USER_STATUS_ID) LOCAL TABLESPACE REPORT_INDX;

CREATE OR REPLACE TYPE DBADMIN.TIMESTAMP_TABLE IS TABLE OF TIMESTAMP
/

GRANT EXECUTE ON DBADMIN.TIMESTAMP_TABLE TO PUBLIC;

INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION)
VALUES(2080, 'REPORT.REPORT_REQUEST', 'REPORT.REPORT_SENT', 'REPORT.REPORT_REQUEST.REPORT_REQUEST_ID = REPORT.REPORT_SENT.REPORT_REQUEST_ID');

INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION)
VALUES(2083, 'REPORT.REPORT_REQUEST', 'REPORT.STORED_FILE', 'REPORT.REPORT_REQUEST.STORED_FILE_ID = REPORT.STORED_FILE.STORED_FILE_ID');

INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION)
VALUES(2085, 'REPORT.REPORT_REQUEST', 'REPORT.REPORT_REQUEST_USER', 'REPORT.REPORT_REQUEST.REPORT_REQUEST_ID = REPORT.REPORT_REQUEST_USER.REPORT_REQUEST_ID');

COMMIT;

INSERT INTO FOLIO_CONF.FILTER_OPERATOR(FILTER_OPERATOR_ID,OPERATOR_NAME,OPERATOR_DESC,OPERATOR_PATTERN,SQL_TYPES) 
    VALUES (29,'WEEK RANGE BETWEEN',NULL,'{display} BETWEEN TRUNC(?, ''''DAY'''') AND TRUNC(?, ''''DAY'''')','DATE,DATE');
INSERT INTO FOLIO_CONF.FILTER_OPERATOR(FILTER_OPERATOR_ID,OPERATOR_NAME,OPERATOR_DESC,OPERATOR_PATTERN,SQL_TYPES) 
    VALUES (30,'MONTH RANGE BETWEEN',NULL,'{display} BETWEEN TRUNC(?, ''''MONTH'''') AND TRUNC(?, ''''MONTH'''')','DATE,DATE');

COMMIT;

-- ADD new folio fields & privs
UPDATE FOLIO_CONF.FIELD 
   SET SORT_SQL_TYPE = 'TIMESTAMP(0)',
       DISPLAY_SQL_TYPE = 'TIMESTAMP(0)'
 WHERE SORT_SQL_TYPE = 'TIMESTAMP'
   AND DISPLAY_SQL_TYPE = 'TIMESTAMP'
   AND FIELD_ID NOT IN(2081,2083);

UPDATE FOLIO_CONF.FIELD 
   SET FIELD_LABEL = 'Tran Amount',
       FIELD_CATEGORY_ID = 6
 WHERE FIELD_ID = 547;

UPDATE FOLIO_CONF.FOLIO_PILLAR_FIELD 
   SET FIELD_ID = 547
 WHERE FIELD_ID = 2589;
  
DELETE FROM FOLIO_CONF.FIELD_PRIV
 WHERE FIELD_ID = 2589;

DELETE FROM FOLIO_CONF.FIELD
 WHERE FIELD_ID = 2589;
 
UPDATE FOLIO_CONF.FIELD 
   SET FIELD_LABEL = 'Terminal Id',
       FIELD_CATEGORY_ID = 11
 WHERE FIELD_ID = 2591;
 
UPDATE FOLIO_CONF.FIELD 
   SET FIELD_LABEL = 'Item'
 WHERE FIELD_ID = 541; 

UPDATE FOLIO_CONF.FIELD 
   SET DISPLAY_EXPRESSION = 'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE,''FMDD'')',
       SORT_EXPRESSION = 'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE,''FMDD'')',
       DISPLAY_SQL_TYPE = 'DATE',
       SORT_SQL_TYPE = 'DATE',
       FIELD_LABEL = 'Tran Date Day'
 WHERE DISPLAY_EXPRESSION = 'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE)';

UPDATE FOLIO_CONF.FIELD 
   SET DISPLAY_EXPRESSION = 'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE,''FMMONTH'')',
       SORT_EXPRESSION = 'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE,''FMMONTH'')',
       FIELD_LABEL = 'Tran Date Month'
 WHERE DISPLAY_EXPRESSION = 'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE, ''MONTH'')';

UPDATE FOLIO_CONF.FIELD 
   SET FIELD_LABEL = 'Tran Timestamp'
 WHERE FIELD_ID = 440;

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,CREATED_BY_USER_ID,LAST_UPDATED_BY_USER_ID,ACTIVE_FLAG) 
    VALUES(31,'Tran Date Week',NULL,'TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE, ''FMDAY'')','TRUNC(REPORT.ACTIVITY_REF.TRAN_DATE, ''FMDAY'')','MESSAGE:{,DATE,MM/dd/yyyy} - {,simple.text.DateAdditionFormat, DATE, 7, MM/dd/yyyy}','DATE','DATE',50,8,null,null,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(709,'Machine Make/Model',null,'REPORT.MACHINE.MAKE || '' ('' || REPORT.MACHINE.MODEL|| '')''','UPPER(REPORT.MACHINE.MAKE || '' ('' || REPORT.MACHINE.MODEL|| '')'')',null,'VARCHAR','VARCHAR',50,121,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(40,'# of Trans',null,'CASE WHEN NVL(REPORT.ACTIVITY_REF.TOTAL_AMOUNT, 0) = 0 AND NVL(REPORT.ACTIVITY_REF.QUANTITY,0) = 0 THEN NULL WHEN NVL(REPORT.ACTIVITY_REF.TOTAL_AMOUNT, 0) < 0 THEN -1 ELSE 1 END','CASE WHEN NVL(REPORT.ACTIVITY_REF.TOTAL_AMOUNT, 0) = 0 AND NVL(REPORT.ACTIVITY_REF.QUANTITY,0) = 0 THEN NULL WHEN NVL(REPORT.ACTIVITY_REF.TOTAL_AMOUNT, 0) < 0 THEN -1 ELSE 1 END',null,'DECIMAL','DECIMAL',50,6,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(32,'Tran Date Day Of Week',null,'TO_NUMBER(TO_CHAR(REPORT.ACTIVITY_REF.TRAN_DATE,''D''))','TO_NUMBER(TO_CHAR(REPORT.ACTIVITY_REF.TRAN_DATE,''D''))',null,'DECIMAL','DECIMAL',50,8,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(441,'Tran Id',null,'REPORT.ACTIVITY_REF.TRAN_ID','REPORT.ACTIVITY_REF.TRAN_ID',null,'NUMERIC','NUMERIC',50,11,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(2532,'User Id (Requested Report)',null,'REPORT.REPORT_REQUEST_USER.USER_ID','REPORT.REPORT_REQUEST_USER.USER_ID',null,'DECIMAL(22)','DECIMAL(22)',50,175,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(2320,'Stored File Id',null,'REPORT.STORED_FILE.STORED_FILE_ID','REPORT.STORED_FILE.STORED_FILE_ID',null,'DECIMAL(22)','DECIMAL(22)',50,175,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(2322,'Stored File Passcode',null,'REPORT.STORED_FILE.STORED_FILE_PASSCODE','REPORT.STORED_FILE.STORED_FILE_PASSCODE',null,'VARCHAR(30)','VARCHAR(30)',50,175,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG)
    VALUES(2318,'Request Id',null,'REPORT.REPORT_REQUEST.REPORT_REQUEST_ID','REPORT.REPORT_REQUEST.REPORT_REQUEST_ID',null,'DECIMAL(22)','DECIMAL(22)',50,175,'Y');

-- add privs for fields
INSERT INTO FOLIO_CONF.FIELD_PRIV(USER_GROUP_ID,FIELD_ID,FILTER_GROUP_ID,ACTIVE_FLAG)
SELECT DISTINCT UG.USER_GROUP_ID, F.FIELD_ID, NULL, 'Y'
FROM FOLIO_CONF.USER_GROUP UG
CROSS JOIN FOLIO_CONF.FIELD F
WHERE UG.USER_GROUP_ID IN(1,2,3)
AND F.FIELD_ID NOT IN(SELECT FIELD_ID FROM FOLIO_CONF.FIELD_PRIV WHERE USER_GROUP_ID = UG.USER_GROUP_ID)
AND (F.FIELD_ID IN(31, 709, 40, 32, 441));

INSERT INTO FOLIO_CONF.FIELD_PRIV(USER_GROUP_ID,FIELD_ID,FILTER_GROUP_ID,ACTIVE_FLAG)
SELECT DISTINCT UG.USER_GROUP_ID, F.FIELD_ID, 1, 'Y'
FROM FOLIO_CONF.USER_GROUP UG
CROSS JOIN FOLIO_CONF.FIELD F
WHERE UG.USER_GROUP_ID IN(4,5,8)
AND F.FIELD_ID NOT IN(SELECT FIELD_ID FROM FOLIO_CONF.FIELD_PRIV WHERE USER_GROUP_ID = UG.USER_GROUP_ID)
AND (F.FIELD_ID IN(31, 709, 40, 32, 441));

COMMIT;

-- Add filters 
INSERT INTO FOLIO_CONF.FILTER_GROUP(FILTER_GROUP_ID, SEPARATOR)
VALUES(90, 'AND');

INSERT INTO FOLIO_CONF.FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, FILTER_GROUP_ID, AGGREGATE_TYPE_ID)
 VALUES(90, 2532, 1, 90, 0);

INSERT INTO FOLIO_CONF.FILTER_PARAM(FILTER_ID,PARAM_INDEX,PARAM_NAME,PARAM_PROMPT,PARAM_VALUE,PARAM_LABEL,PARAM_SQL_TYPE,PARAM_EDITOR) 
    VALUES(90,1,'user.userId',NULL,NULL,NULL,'NUMERIC',NULL);

INSERT INTO FOLIO_CONF.FIELD_PRIV(USER_GROUP_ID,FIELD_ID,FILTER_GROUP_ID,ACTIVE_FLAG)
SELECT DISTINCT UG.USER_GROUP_ID, F.FIELD_ID, 90, 'Y'
FROM FOLIO_CONF.USER_GROUP UG
CROSS JOIN FOLIO_CONF.FIELD F
WHERE UG.USER_GROUP_ID IN(1,2,3,4,5,8)
AND F.FIELD_ID NOT IN(SELECT FIELD_ID FROM FOLIO_CONF.FIELD_PRIV WHERE USER_GROUP_ID = UG.USER_GROUP_ID)
AND (F.FIELD_ID IN(2318, 2320, 2322));

COMMIT;

insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-please-wait', 'Your report is being generated. Depending on your selection it may take a while to complete. You may view the report later by selecting the "Pick up later" button or cancel the report by selecting the "Cancel" button.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-cancel-confirm', 'The report has been successfully cancelled.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-cancel-pending-confirm', 'Your request is cancelled successfully. Another pending request is waiting for the same report.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-cancel-generated-confirm', 'The report could not be cancelled because it has already been built.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-pick-up-later', 'The report is registered for pick-up later. You will see a notification in the upper right side of the page when the report is ready.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-invalid-request', 'This report could not be found. Please try another.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-invalid-profile', 'The current user may not view this report.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-view-error', 'Could not retrieve the requested report.');
        
commit;

DECLARE
   weblinkId INTEGER;
BEGIN
    select web_content.seq_web_link_id.nextval into webLinkId from dual;
    insert into web_content.web_link values(webLinkId,'Recent Reports', './frame.i?content=report_request_history.i&isByProfile=true','Recent Report Request History', 'Reports','M',125);
    insert into web_content.web_link_requirement values(webLinkId,1);
    
    select web_content.seq_web_link_id.nextval into webLinkId from dual;
    insert into web_content.web_link values(webLinkId,'Transaction Trends', './transaction_totals.i?folioId=667','View Line Graph of Transaction Type Trends for a give time period', 'Sales Analysis','W', 0);
    insert into web_content.web_link_requirement values(webLinkId,1);
    COMMIT;
EXCEPTION
	 WHEN OTHERS THEN
	 	 ROLLBACK;
	 	 RAISE;
END;
/

UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = './activity_parameters.i?fragment=true'
 WHERE WEB_LINK_USAGE = 'B' AND WEB_LINK_LABEL = 'Simple Report';

UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = './activity_ext_parameters.i?fragment=true'
 WHERE WEB_LINK_USAGE = 'B' AND WEB_LINK_LABEL = 'Detailed Report';
 
UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = './activity_summary_parameters.i?fragment=true'
 WHERE WEB_LINK_USAGE = 'B' AND WEB_LINK_LABEL = 'Summary Report';
 
UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = './activity_graph_parameters.i?fragment=true'
 WHERE WEB_LINK_USAGE = 'B' AND WEB_LINK_LABEL = 'Bar Graph Report';
 
COMMIT;

-- code to add ccs transport error link in usalive
/*
DECLARE
   weblinkId INTEGER;
BEGIN
    select web_content.seq_web_link_id.nextval into webLinkId from dual;
    insert into web_content.web_link values(webLinkId,'CCS Transport Error', './frame.i?content=ccs_transport_error.i','CCS Transport Error report', 'Customer Service','M',270);
    insert into web_content.web_link_requirement values(webLinkId,14);
    insert into web_content.web_link_requirement values(webLinkId,1);
    COMMIT;
EXCEPTION
     WHEN OTHERS THEN
          ROLLBACK;
          RAISE;
END;
*/
-- create the transport type EmailLink
insert into report.ccs_transport_type values(6, 'Email With Link');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_REGEX)
VALUES( REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, 6, 'Email', 'Y','/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_REGEX)
VALUES( REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, 6, 'CC', 'N','/(^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$)|^$/');

commit;

-- add 'params.' prefix to action_formats in folios
DECLARE
CURSOR l_cur IS
select fp.folio_pillar_id, fp.action_format
from folio_conf.folio_pillar fp
where fp.action_format like 'MESSAGE:run_report.i?%';
 lv_new_action_format folio_conf.folio_pillar.action_format%TYPE;
begin
    for l_rec in l_cur LOOP
        SELECT 'MESSAGE:run_report_async.i?' || dbadmin.ADHERE(cast(collect(CASE WHEN n NOT IN('folioId', 'promptForParams', 'reportId', 'bannerImage')  AND n not like 'params.%' THEN 'params.' END || n || '=' || v) as VARCHAR2_TABLE), '&')
          INTO lv_new_action_format
          FROM (SELECT substr(column_value, 1, instr(column_value, '=') - 1) n, substr(column_value, instr(column_value, '=') + 1, 4000) v
                  FROM TABLE(dbadmin.split(substr(l_rec.action_format, 1 + instr(l_rec.action_format, '?'), 4000), '&')));
        DBMS_OUTPUT.put_line('Replacing:');
        DBMS_OUTPUT.put_line('    ' || l_rec.action_format);
        DBMS_OUTPUT.put_line('With:');
        DBMS_OUTPUT.put_line('    ' || lv_new_action_format);
        UPDATE folio_conf.folio_pillar SET action_format = lv_new_action_format
         WHERE folio_pillar_id = l_rec.folio_pillar_id;
        COMMIT;
    END LOOP;
END;
/

UPDATE REPORT.USER_LINK UL
  SET USAGE = 'u'
 WHERE USAGE = 'C'
 AND LINK_ID IN(
select web_link_id from (
select UTL_URL.UNESCAPE(substr(regexp_substr(wl.web_link_url, '[?&]requestHandler=[^&]*'), 17, 1000)) url_request_handler,
UTL_URL.UNESCAPE(substr(regexp_substr(wl.web_link_url, '[?&]sortBy=[^&]*'), 9, 1000)) url_sort_by,
UTL_URL.UNESCAPE(substr(regexp_substr(wl.web_link_url, '[?&]showValues=[^&]*'), 13, 1000)) url_show_values,
UTL_URL.UNESCAPE(substr(regexp_substr(wl.web_link_url, '[?&]rangeType=[^&]*'), 12, 1000)) url_range_type,
NVL(UTL_URL.UNESCAPE(substr(regexp_substr(wl.web_link_url, '[?&]tranType=[^&]*'), 11, 1000)),
UTL_URL.UNESCAPE(substr(regexp_substr(wl.web_link_url, '[?&]params.tranType=[^&]*'), 11, 1000))) url_tran_type,
wl.* 
from web_content.web_link wl
where wl.web_link_usage = '-'
and substr(regexp_substr(wl.web_link_url, '[?&]requestHandler=[^&]*'), 17, 1000) IN ('activity_ext', 'activity_ext2', 'activity_graph_ext', 'activity_detail_ext'))
where url_sort_by is null or (url_request_handler in('activity_ext', 'activity_graph_ext') and LENGTH(REGEXP_REPLACE(url_sort_by, '[^,]', '')) != 3)
or (url_request_handler in('activity_ext', 'activity_ext2', 'activity_graph_ext') and url_show_values is null) 
or url_range_type is null 
or (url_request_handler in('activity_ext') and url_tran_type is null)
)
;
COMMIT;
-- add 'params.' prefix to links
DECLARE
CURSOR l_cur IS
select wl.web_link_id, wl.web_link_url
from web_content.web_link wl
where wl.web_link_url like '%run_report.i?%';
 lv_new_url web_content.web_link.web_link_url%TYPE;
begin
    for l_rec in l_cur LOOP
        SELECT REPLACE(SUBSTR(l_rec.web_link_url, 1, instr(l_rec.web_link_url, '?')), 'run_report.i?','run_report_async.i?') || dbadmin.ADHERE(cast(collect(CASE WHEN n NOT IN('folioId', 'promptForParams', 'reportId', 'bannerImage')  AND n not like 'params.%' THEN 'params.' END || n || '=' || v) as VARCHAR2_TABLE), '&')
          INTO lv_new_url
          FROM (SELECT substr(column_value, 1, instr(column_value, '=') - 1) n, substr(column_value, instr(column_value, '=') + 1, 4000) v
                  FROM TABLE(dbadmin.split(substr(l_rec.web_link_url, 1 + instr(l_rec.web_link_url, '?'), 4000), '&')));
        DBMS_OUTPUT.put_line('Replacing:');
        DBMS_OUTPUT.put_line('    ' || l_rec.web_link_url);
        DBMS_OUTPUT.put_line('With:');
        DBMS_OUTPUT.put_line('    ' || lv_new_url);
        UPDATE web_content.web_link SET web_link_url = lv_new_url
         WHERE web_link_id = l_rec.web_link_id;
        COMMIT;
    END LOOP;
END;
/
-- add 'params.' prefix to links
DECLARE
CURSOR l_cur IS
select wl.web_link_id, wl.web_link_url
from web_content.web_link wl
where wl.web_link_url like '%select_date_range_frame.i?%';
 lv_new_url web_content.web_link.web_link_url%TYPE;
begin
    for l_rec in l_cur LOOP
        SELECT SUBSTR(l_rec.web_link_url, 1, instr(l_rec.web_link_url, '?')) || dbadmin.ADHERE(cast(collect(CASE WHEN n NOT IN('folioId', 'promptForParams', 'reportId', 'bannerImage')  AND n not like 'params.%' THEN 'params.' END || n || '=' || v) as VARCHAR2_TABLE), '&')
          INTO lv_new_url
          FROM (SELECT substr(column_value, 1, instr(column_value, '=') - 1) n, substr(column_value, instr(column_value, '=') + 1, 4000) v
                  FROM TABLE(dbadmin.split(substr(l_rec.web_link_url, 1 + instr(l_rec.web_link_url, '?'), 4000), '&')));
        DBMS_OUTPUT.put_line('Replacing:');
        DBMS_OUTPUT.put_line('    ' || l_rec.web_link_url);
        DBMS_OUTPUT.put_line('With:');
        DBMS_OUTPUT.put_line('    ' || lv_new_url);
        UPDATE web_content.web_link SET web_link_url = lv_new_url
         WHERE web_link_id = l_rec.web_link_id;
        COMMIT;
    END LOOP;
END;
/

UPDATE FOLIO_CONF.FOLIO
   SET FOLIO_TITLE = 'MESSAGE:Payment Summary #{DocId}'
 WHERE FOLIO_ID = 258;
 
UPDATE FOLIO_CONF.FOLIO
   SET FOLIO_TITLE = 'MESSAGE:Transactions in Payment #{DocId}'
 WHERE FOLIO_ID = 286;
 
COMMIT;

-- update all saved reports
DECLARE
CURSOR l_cur IS
SELECT WL.WEB_LINK_ID, WL.WEB_LINK_URL
FROM WEB_CONTENT.WEB_LINK WL
WHERE REGEXP_LIKE(WL.WEB_LINK_URL, '^([.]/)?i[?](.*[&])?requestHandler=activity(_ext|_ext2|_graph_ext|_detail|_detail_ext)?($|[&])', 'i');
 lv_new_url web_content.web_link.web_link_url%TYPE;
 ln_pos_query PLS_INTEGER;
 ln_pos_param_start PLS_INTEGER;
 ln_pos_param_end PLS_INTEGER;
 lv_orig_rh VARCHAR2(4000);
BEGIN
    FOR l_rec IN l_cur LOOP
        ln_pos_query := INSTR(l_rec.web_link_url, '?');
        ln_pos_param_start := REGEXP_INSTR(l_rec.web_link_url, '[&?]requestHandler=activity(_ext|_ext2|_graph_ext|_detail|_detail_ext)?($|[&])', 1, 1, 0, 'i');
        ln_pos_param_end := REGEXP_INSTR(l_rec.web_link_url, '[&?]requestHandler=activity(_ext|_ext2|_graph_ext|_detail|_detail_ext)?($|[&])', 1, 1, 1, 'i');
        lv_orig_rh := LOWER(SUBSTR(l_rec.web_link_url, ln_pos_param_start + 16, ln_pos_param_end - ln_pos_param_start - 17));
        
        SELECT DECODE(lv_orig_rh, 'activity_graph_ext', 'activity_graph', 'activity_ext2', 'activity_summary', lv_orig_rh) || '.i?'
               || DECODE(lv_orig_rh, 'activity_graph_ext', 'referrer=activity_graph_parameters.i&', 'activity_ext2', 'referrer=activity_summary_parameters.i&',
                    'activity_ext', 'referrer=activity_ext_parameters.i&', 'activity', 'referrer=activity_parameters.i&')
               || REGEXP_REPLACE(REGEXP_REPLACE(SUBSTR(l_rec.web_link_url, ln_pos_query + 1, ln_pos_param_start - ln_pos_query)
               || SUBSTR(l_rec.web_link_url, ln_pos_param_end, 4000), '(^|[&])(beginDate|endDate|tranType)=', '\1params.\2='), '(Date=%7B)(-?[0-9]+)(%7D)', '\1*\2\3')
          INTO lv_new_url
          FROM DUAL;
        DBMS_OUTPUT.put_line('Replacing:');
        DBMS_OUTPUT.put_line('    ' || l_rec.web_link_url);
        DBMS_OUTPUT.put_line('With:');
        DBMS_OUTPUT.put_line('    ' || lv_new_url);
        UPDATE web_content.web_link SET web_link_url = lv_new_url
         WHERE web_link_id = l_rec.web_link_id;
        COMMIT;
    END LOOP;
END;
/

INSERT INTO FOLIO_CONF.USER_GROUP(USER_GROUP_ID, DESCRIPTION)
  VALUES(100, 'User can view data of terminals in their REPORT.USER_TERMINAL table');
INSERT INTO FOLIO_CONF.USER_GROUP(USER_GROUP_ID, DESCRIPTION)
  VALUES(120, 'User can view data of bank accts in their REPORT.USER_CUSTOMER_BANK table');
INSERT INTO FOLIO_CONF.USER_GROUP(USER_GROUP_ID, DESCRIPTION)
  VALUES(101, 'User can view data of all terminals belonging to the customer of the user');
INSERT INTO FOLIO_CONF.USER_GROUP(USER_GROUP_ID, DESCRIPTION)
  VALUES(102, 'User can view data of all terminals');
INSERT INTO FOLIO_CONF.USER_GROUP(USER_GROUP_ID, DESCRIPTION)
  VALUES(121, 'User can view data of all bank accts belonging to the customer of the user');
INSERT INTO FOLIO_CONF.USER_GROUP(USER_GROUP_ID, DESCRIPTION)
  VALUES(122, 'User can view data of all bank accts');
COMMIT; 
INSERT INTO FOLIO_CONF.DIRECTIVE(DIRECTIVE_ID, DIRECTIVE_NAME, DIRECTIVE_DESCRIPTION)
VALUES(FOLIO_CONF.SEQ_DIRECTIVE_ID.NEXTVAL, 'query-optimizer-hint', 'The sql query hint to add to the SELECT statement');
COMMIT;

GRANT SELECT ON REPORT.CCS_TRANSPORT_ERROR TO USALIVE_APP_ROLE;
GRANT UPDATE ON REPORT.CCS_TRANSPORT_ERROR TO USALIVE_APP_ROLE;

CREATE OR REPLACE
FUNCTION         UTC_TO_LOCAL_DATE (
    pt_utc_ts TIMESTAMP)
  RETURN DATE PARALLEL_ENABLE DETERMINISTIC
IS
BEGIN
    RETURN FROM_TZ(pt_utc_ts, 'GMT') AT TIME ZONE 'US/Eastern';
END;
/
GRANT EXECUTE ON DBADMIN.UTC_TO_LOCAL_DATE TO PUBLIC;

UPDATE REPORT.MESSAGE SET MESSAGE_TEXT = 'We at USA Technologies, Inc are continually striving to enhance our customers'' power to run their business effectively. And the USALive Website is loaded with features to help you do just that. Now new in Version 1.6:
<ul><li>Background Report Generation - so you can continue to work on other things while your reports are being built</li>
<li>Retrieve Auto-Generated Reports - you can view the same emailed reports that are automatically sent to you via the Report Register</li>
<li>Upgraded Reports - provide additional column sorting and alternative file types (like PDF or Excel)</li>
<li>Other functional or performance improvements</li></ul>
<p style="color:#444444"><span style="font-weigth:bold">Note:</span>
<span>We recommend you upgrade to the latest versions of
<a href="http://www.mozilla.com/en-US/firefox/" target="_blank">
<span style="color:#3366CC">Firefox</span></a> or 
<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank">
<span style="color:#3366CC">Internet Explorer</span></a> 
web browsers to ensure optimal performance of the USALive reporting system.
</span></p>'
 WHERE MESSAGE_CODE = 'MESSAGE';
 
COMMIT;

UPDATE WEB_CONTENT.WEB_LINK 
   SET WEB_LINK_URL = REPLACE(WEB_LINK_URL, '&showValues=3%2C+4', '&showValues=3')
 WHERE REGEXP_LIKE(WEB_LINK_URL, '^([.]/)?activity_(ext|graph|summary|_ext2|_graph_ext|_detail|_detail_ext).i[?](.*[&])?showValues=4.*', 'i');

UPDATE WEB_CONTENT.WEB_LINK 
   SET WEB_LINK_URL = REPLACE(REPLACE(WEB_LINK_URL, '&showValues=3%2C+4', '&showValues=3'), '&showValues=1%2C+2%2C+3%2C+4', '&showValues=1%2C+2%2C+3')
 WHERE REGEXP_LIKE(WEB_LINK_URL, '^([.]/)?activity_(ext|graph|summary|_ext2|_graph_ext|_detail|_detail_ext).i[?](.*[&])?showValues=[^&]*4.*', 'i');

COMMIT;
