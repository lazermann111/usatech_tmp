SET SCAN OFF;
DECLARE
    l_report_id NUMBER;
	l_web_link_id NUMBER;
BEGIN
    select report.reports_seq.nextval into l_report_id from dual;
    
	insert into report.reports(report_id, title, generator_id, batch_type_id, report_name, description, usage, user_id)
	values(l_report_id, 'Payment Reconciliation for {b} - {e}', 6, 0, 'Payment Reconciliation by Date Range - CSV', 'Payment details', 'E', 0);
    
    insert into report.report_param values(l_report_id, 'folioId', '1039');
    insert into report.report_param values(l_report_id, 'outputType', '21');
	insert into report.report_param values(l_report_id, 'params.StartDate', '{b}');
	insert into report.report_param values(l_report_id, 'params.EndDate', '{e}');
    
	select web_content.seq_web_link_id.nextval into l_web_link_id from dual;
    insert into web_content.web_link values(l_web_link_id, 'Payment Reconciliation', './select_date_range_frame.i?folioId=1039', 'Payment Reconciliation by Date Range', 'Daily Operations', 'W', 0);

	select web_content.seq_web_link_id.nextval into l_web_link_id from dual;
    insert into web_content.web_link values(l_web_link_id, 'Payment Reconciliation (CSV)', './select_date_range_frame.i?folioId=1039&outputType=21', 'Payment Reconciliation by Date Range (CSV)', 'Daily Operations', 'W', 0);	
	
    commit;
END;
