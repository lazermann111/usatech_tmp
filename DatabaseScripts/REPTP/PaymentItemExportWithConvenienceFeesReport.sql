DECLARE
    l_report_id NUMBER;
BEGIN
    select report.reports_seq.nextval into l_report_id from dual;
    
	insert into report.reports(report_id, title, generator_id, batch_type_id, report_name, description, usage, user_id)
	select l_report_id, 'sales_txn_p{x}', 6, 3, 'Payment Item Export With Convenience Fees', 'Payment details with convenience fees', 'N', nvl(max(user_id), 0)
	from report.user_login
	where user_name = 'compassgroup';
    	
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'directiveNames','show-header');
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'directiveValues','false');
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'folioId','1032');
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'is_custom_report','true');
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'outputType','21');
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'params.DocId','{x}');
	insert into report.report_param (REPORT_ID,PARAM_NAME,PARAM_VALUE) values (l_report_id,'params.is_custom_report','true');
	
    commit;
END;
