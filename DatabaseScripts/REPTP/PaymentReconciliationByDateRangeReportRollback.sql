DECLARE
	ln_report_id REPORT.REPORTS.REPORT_ID%TYPE;
BEGIN
	DELETE FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Payment Reconciliation';

	SELECT REPORT_ID
	INTO ln_report_id
	FROM REPORT.REPORTS
	WHERE REPORT_NAME = 'Payment Reconciliation by Date Range - CSV';

	DELETE FROM report.report_param WHERE report_id = ln_report_id;
	DELETE FROM report.reports WHERE report_id = ln_report_id;

	FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(1039);
	
	COMMIT;
END;
