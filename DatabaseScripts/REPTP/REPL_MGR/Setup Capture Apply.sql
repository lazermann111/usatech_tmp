-- try using oracles streams change-capture mechanism
CREATE TABLESPACE REPL_MGR_DATA
  DATAFILE '/u02/oradata/usadbp01/repl_mgr_data1.dbf'
  SIZE 30M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u02/oradata/usadbp01/repl_mgr_data1.dbf' AUTOEXTEND ON;
/*CREATE TABLESPACE REPL_MGR_DATA
  DATAFILE '/u04/oradata/usardb01/repl_mgr_data1.dbf'
  SIZE 30M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u04/oradata/usardb01/repl_mgr_data1.dbf' AUTOEXTEND ON;
/*
CREATE TABLESPACE REPL_MGR_DATA
  DATAFILE '/u04/oradata/usadbt03/repl_mgr_data1.dbf'
  SIZE 30M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u04/oradata/usadbt03/repl_mgr_data1.dbf' AUTOEXTEND ON;
/*
CREATE TABLESPACE REPL_MGR_DATA
  DATAFILE '/u03/oradata/USADBD02/repl_mgr_data1.dbf'
  SIZE 30M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u03/oradata/USADBD02/repl_mgr_data1.dbf' AUTOEXTEND ON;
/*  
CREATE TABLESPACE REP_TEST_INDX
  DATAFILE '/u03/oradata/USADBD02/rep_test_idx1.dbf'
  SIZE 10M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
ALTER DATABASE DATAFILE '/u03/oradata/USADBD02/rep_test_idx1.dbf' AUTOEXTEND ON;
*/  
CREATE USER REPL_MGR
IDENTIFIED BY REPL_MGR
DEFAULT TABLESPACE REPL_MGR_DATA
TEMPORARY TABLESPACE TEMPTS1
/
ALTER USER REPL_MGR QUOTA UNLIMITED ON REPL_MGR_DATA --QUOTA UNLIMITED ON REP_TEST_INDX
/
GRANT CONNECT TO REPL_MGR
/
GRANT RESOURCE TO REPL_MGR
/
GRANT EXECUTE ON DBMS_AQ TO REPL_MGR;
GRANT EXECUTE ON DBMS_STREAMS_ADM TO REPL_MGR;
GRANT EXECUTE ON DBMS_AQADM TO REPL_MGR;
GRANT EXECUTE ON DBMS_FLASHBACK TO REPL_MGR;
GRANT EXECUTE ON DBMS_APPLY_ADM TO REPL_MGR;
GRANT EXECUTE ON DBMS_CAPTURE_ADM TO REPL_MGR;
GRANT EXECUTE ON DBMS_PROPAGATION_ADM TO REPL_MGR;
GRANT EXECUTE ON DBMS_RULE_ADM TO REPL_MGR;
GRANT SELECT ON SYS.V_$DATABASE TO REPL_MGR;

GRANT SELECT ON PSS.TRAN TO REPL_MGR;
GRANT SELECT ON LOCATION.LOCATION TO REPL_MGR;
GRANT SELECT ON LOCATION.CUSTOMER TO REPL_MGR;
GRANT SELECT ON DEVICE.HOST TO REPL_MGR;
GRANT SELECT ON DEVICE.DEVICE_CALL_IN_RECORD TO REPL_MGR;
GRANT SELECT ON DEVICE.DEVICE TO REPL_MGR;

/*
	   SELECT 'GRANT SELECT ON ' || T.OWNER || '.' || T.TABLE_NAME || ' TO REPL_MGR;' --OWNER, TABLE_NAME, CC.COLUMN_NAME, C.*
	     FROM ALL_TABLES T
	   WHERE ((T.OWNER = 'DEVICE' AND T.TABLE_NAME IN('DEVICE_CALL_IN_RECORD', 'DEVICE', 'HOST'))
	      OR (T.OWNER = 'PSS' AND T.TABLE_NAME IN('TRAN')) 
		  OR (T.OWNER = 'LOCATION' AND T.TABLE_NAME IN('LOCATION', 'CUSTOMER')));
*/                 
/*
SELECT * FROM SESSION_PRIVS;
SELECT * FROM SESSION_ROLES;
SELECT * FROM USER_TAB_PRIVS;
SELECT * FROM all_views WHERE view_name LIKE '%DEPEND%';
SELECT * FROM ALL_DEPENDENCIES WHERE NAME LIKE '%APPLY_ALL';

/*
BEGIN
DBMS_CAPTURE_ADM.PREPARE_SCHEMA_INSTANTIATION(
   schema_name  => 'WEI');
END;
*/ 

-- NOTE: must run each of these as the l_rep_schema user
-- 1. Set up queues
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
	l_tablespace VARCHAR2(100) := 'REPL_MGR_DATA'; --l_rep_schema||'_DATA';
BEGIN
DBMS_STREAMS_ADM.SET_UP_QUEUE(
   queue_table     => l_rep_schema||'.QT_CHANGE_CAPTURE',
   storage_clause  => 'TABLESPACE ' || l_tablespace,
   queue_name      => l_rep_schema||'.Q_CHANGE_CAPTURE',
   queue_user      => l_rep_schema,
   comment         => 'Holds capture lcr''s which cannot be dequeued by user procedure');
/* No need for multi-consumer queue
DBMS_STREAMS_ADM.SET_UP_QUEUE(
   queue_table     => l_rep_schema||'.QT_CHANGE_APPLY',
   storage_clause  => 'TABLESPACE '||l_rep_schema||'_DATA',
   queue_name      => l_rep_schema||'.Q_CHANGE_APPLY',
   queue_user      => l_rep_schema,
   comment         => 'Holds lcr''s that can be dequeued by user procedure');
DBMS_AQADM.ALTER_QUEUE (
		queue_name => l_rep_schema||'.Q_CHANGE_APPLY', 
		retry_delay => 0, 
		max_retries => NULL);
DBMS_AQADM.ADD_SUBSCRIBER(
    queue_name          =>  l_rep_schema||'.Q_CHANGE_APPLY',
    subscriber          =>  SYS.AQ$_AGENT('REPLICATE_AGENT', NULL, NULL),
    rule                =>  NULL,
    transformation      =>  NULL
	);
DBMS_AQADM.GRANT_QUEUE_PRIVILEGE(
    privilege  =>  'ALL',
    queue_name => l_rep_schema||'.Q_CHANGE_APPLY',
    grantee    => l_rep_schema);

DBMS_AQADM.ENABLE_DB_ACCESS(
    agent_name  => 'REPLICATE_AGENT',
    db_username => l_rep_schema);
*/
DBMS_AQADM.CREATE_QUEUE_TABLE (
		queue_table     => l_rep_schema||'.QT_CHANGE_APPLY',
        storage_clause  => 'TABLESPACE '||l_tablespace,
   		multiple_consumers => FALSE, 
		queue_payload_type => 'SYS.ANYDATA',
		comment         => 'Holds lcr''s that can be dequeued by user procedure');
DBMS_AQADM.CREATE_QUEUE (
		queue_name => l_rep_schema||'.Q_CHANGE_APPLY', 
		queue_table => l_rep_schema||'.QT_CHANGE_APPLY', 
		retry_delay => 600, 
		max_retries => NULL,
		comment         => 'Holds lcr''s that can be dequeued by user procedure');
DBMS_AQADM.START_QUEUE (
		queue_name => l_rep_schema||'.Q_CHANGE_APPLY');
END;
/

-- 2. Set up objects (can be run as system)
@@./SCHEMA_TABLE_LIST.typ;
@@./LCR_SYNC.typ;
@@./REPLICATE_PKG.psk;
@@./REPLICATE_PKG.pbk;
@@./REPLICATE_QUEUE_PKG.psk;
@@./REPLICATE_QUEUE_PKG.pbk;

SELECT * FROM DATABASE_PROPERTIES WHERE UPPER(PROPERTY_NAME) LIKE '%LOG%';
SELECT * FROM V$SYSTEM_PARAMETER WHERE UPPER(NAME) LIKE '%LOG%';

--3. Add each schema that is to be replicated
-- for some reason this requires ALL PRIVILEGES to run so grant all privileges, then run it then revoke all privilges
GRANT ALL PRIVILEGES TO REPL_MGR;
--/*
-- this doens't work - Capture process remains in paused for flow control state
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
	l_rule VARCHAR2(4000) := 'CHANGE_CAPTURE_RULESET';
	l_scn NUMBER;
    l_db_name VARCHAR2(100);	
BEGIN
-- create rule
DBMS_RULE_ADM.CREATE_RULE_SET(
   rule_set_name       => l_rule,
   evaluation_context  => 'SYS.STREAMS$_EVALUATION_CONTEXT');
DBMS_RULE_ADM.CREATE_RULE(
   rule_name           => 'DEVICE_SCHEMA_CHANGE_RULE',
   condition           => ':dml.get_object_owner() IN(''DEVICE'') and :dml.get_object_name() IN(''DEVICE'',''HOST'',''DEVICE_CALL_IN_RECORD'') and :dml.is_null_tag() = ''Y''',
   evaluation_context  => 'SYS.STREAMS$_EVALUATION_CONTEXT');
    
DBMS_RULE_ADM.ADD_RULE(
   rule_name           => 'DEVICE_SCHEMA_CHANGE_RULE',
   rule_set_name       => l_rule);
   
DBMS_RULE_ADM.CREATE_RULE(
   rule_name           => 'PSS_SCHEMA_CHANGE_RULE',
   condition           => ':dml.get_object_owner() IN(''PSS'') and :dml.get_object_name() IN(''TRAN'') and :dml.is_null_tag() = ''Y''',
   evaluation_context  => 'SYS.STREAMS$_EVALUATION_CONTEXT');
    
DBMS_RULE_ADM.ADD_RULE(
   rule_name           => 'PSS_SCHEMA_CHANGE_RULE',
   rule_set_name       => l_rule);

DBMS_RULE_ADM.CREATE_RULE(
   rule_name           => 'LOCATION_SCHEMA_CHANGE_RULE',
   condition           => ':dml.get_object_owner() IN(''LOCATION'') and :dml.get_object_name() IN(''LOCATION'',''CUSTOMER'') and :dml.is_null_tag() = ''Y''',
   evaluation_context  => 'SYS.STREAMS$_EVALUATION_CONTEXT');
    
DBMS_RULE_ADM.ADD_RULE(
   rule_name           => 'LOCATION_SCHEMA_CHANGE_RULE',
   rule_set_name       => l_rule);
   
/*  
SELECT CURRENT_SCN, DB_UNIQUE_NAME
  INTO l_scn, l_db_name
  FROM SYS.V_$DATABASE;

DBMS_APPLY_ADM.SET_SCHEMA_INSTANTIATION_SCN(
  source_schema_name  => 'DEVICE',
  source_database_name  => l_db_name,
  instantiation_scn     => l_scn,
  recursive             => TRUE);
*/  
DBMS_CAPTURE_ADM.CREATE_CAPTURE(
   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
   capture_name        => 'CHANGE_CAPTURE',
   rule_set_name       => l_rule);
END;
/
--*/
-- This works!
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
    CURSOR l_cur IS
	   SELECT OWNER, TABLE_NAME 
	     FROM ALL_TABLES
	   WHERE (OWNER = 'DEVICE'  AND TABLE_NAME IN('DEVICE_CALL_IN_RECORD', 'DEVICE', 'HOST'))
	      OR (OWNER = 'PSS' AND TABLE_NAME IN('TRAN')) 
		  OR (OWNER = 'LOCATION' AND TABLE_NAME IN('LOCATION', 'CUSTOMER'));
	l_scn NUMBER;
	l_db_name VARCHAR2(100);		
BEGIN
   FOR l_rec IN l_cur LOOP
	  SELECT CURRENT_SCN, DB_UNIQUE_NAME
	    INTO l_scn, l_db_name
	    FROM SYS.V_$DATABASE;
    DBMS_CAPTURE_ADM.PREPARE_SCHEMA_INSTANTIATION(schema_name  => l_rec.OWNER);
    
   	l_scn := REPL_MGR.REPLICATE_PKG.prepare_table(l_rec.OWNER,l_rec.TABLE_NAME);
   	DBMS_OUTPUT.put_line('Prepared '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'; Setting up capture.');
	DBMS_STREAMS_ADM.ADD_TABLE_RULES(
	   table_name          => l_rec.OWNER || '.'||l_rec.TABLE_NAME,
	   streams_type        => 'capture',
	   streams_name        => 'CHANGE_CAPTURE',
	   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
	   include_dml         => TRUE,
	   include_ddl         => FALSE,
	   include_tagged_lcr  => TRUE,
	   --source_database     => l_db_name,
	   --dml_rule_name       OUT  VARCHAR2,
	   --ddl_rule_name       OUT  VARCHAR2,
	   inclusion_rule      => TRUE,
	   and_condition       => NULL);
	 DBMS_OUTPUT.put_line('Capture set up for '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'.');		   
	END LOOP;
END;
/
-- let's try by schema
SELECT * FROM ALL_COLL_TYPEs WHERE elem_type_name LIKE 'VARCHAR%';
-- same issue
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
    CURSOR l_cur IS
	   SELECT OWNER, CAST(COLLECT(TABLE_NAME) AS SYS.RE$NAME_ARRAY) TABLE_LIST
	     FROM ALL_TABLES
	   WHERE (OWNER = 'DEVICE'  AND TABLE_NAME IN('DEVICE_CALL_IN_RECORD', 'DEVICE', 'HOST'))
	      OR (OWNER = 'PSS' AND TABLE_NAME IN('TRAN')) 
		  OR (OWNER = 'LOCATION' AND TABLE_NAME IN('LOCATION', 'CUSTOMER'))
	GROUP BY OWNER;
	l_tmp VARCHAR2(4000);		
BEGIN
   FOR l_rec IN l_cur LOOP
   	l_tmp := '';
   	FOR i IN l_rec.table_list.FIRST .. l_rec.table_list.LAST LOOP
   		IF length(l_tmp) > 0 THEN
   		   l_tmp := l_tmp || ',';
   		END IF;
   		l_tmp := l_tmp || '''' || l_rec.table_list(i) || '''';
   	END LOOP;
   	DBMS_STREAMS_ADM.ADD_SCHEMA_RULES(
	   schema_name         => l_rec.OWNER,
	   streams_type        => 'capture',
	   streams_name        => 'CHANGE_CAPTURE',
	   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
	   include_dml         => TRUE,
	   include_ddl         => FALSE, 
	   include_tagged_lcr  => FALSE,
	   --source_database     => NULL,
	   --dml_rule_name       OUT  VARCHAR2,
	   --ddl_rule_name       OUT  VARCHAR2,
	   inclusion_rule      => TRUE,
	   and_condition       => ':dml.get_object_name() IN('|| l_tmp ||')');
	 DBMS_OUTPUT.put_line('Capture set up for '||l_rec.OWNER|| '.');		   
	END LOOP;
END;
/
-- this ran successfully 
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
	l_rule VARCHAR2(4000);
	l_tmp VARCHAR2(4000);
    CURSOR l_cur IS
	   SELECT DISTINCT OWNER
	     FROM ALL_TABLES
	   WHERE (OWNER = 'DEVICE'  AND TABLE_NAME IN('DEVICE_CALL_IN_RECORD', 'DEVICE', 'HOST'))
	      OR (OWNER = 'PSS' AND TABLE_NAME IN('TRAN')) 
		  OR (OWNER = 'LOCATION' AND TABLE_NAME IN('LOCATION', 'CUSTOMER'));		
BEGIN
   FOR l_rec IN l_cur LOOP
	DBMS_STREAMS_ADM.ADD_SCHEMA_RULES(
	   schema_name         => l_rec.OWNER,
	   streams_type        => 'apply',
	   streams_name        => 'CHANGE_APPLY',
	   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
	   include_dml         => TRUE,
	   include_ddl         => FALSE,
	   include_tagged_lcr  => TRUE,
	   --source_database     => l_db_name,
	   dml_rule_name       => l_rule,
	   ddl_rule_name       => l_tmp,
	   inclusion_rule      => TRUE,
	   and_condition       => NULL);
	DBMS_APPLY_ADM.SET_EXECUTE(
	  rule_name  => l_rule,
	  execute    => FALSE);
	DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
	  rule_name               => l_rule,
	  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY');
	END LOOP;
END;
/
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
	l_rule VARCHAR2(4000);
	l_tmp VARCHAR2(4000);
    l_db_name VARCHAR2(100);		
BEGIN
SELECT DB_UNIQUE_NAME
  INTO l_db_name
  FROM SYS.V_$DATABASE;
  
DBMS_STREAMS_ADM.ADD_GLOBAL_RULES(
   streams_type        => 'apply',
   streams_name        => 'CHANGE_APPLY',
   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
   include_dml         => TRUE,
   include_ddl         => FALSE, --TRUE,
   include_tagged_lcr  => TRUE,
   source_database     => NULL,
   dml_rule_name       => l_rule,
   ddl_rule_name       => l_tmp,
   inclusion_rule      => TRUE,
   and_condition       => NULL);
   --/*
DBMS_RULE_ADM.ALTER_RULE(
   rule_name           => l_rule,
   condition  => '1 = 1');--*/
DBMS_APPLY_ADM.SET_EXECUTE(
  rule_name  => l_rule,
  execute    => FALSE);
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => l_rule,
  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY');
END;
/
/*
DECLARE
	l_rep_schema VARCHAR2(100) := 'REPL_MGR';
	l_dml_rule VARCHAR2(4000);
	l_ddl_rule VARCHAR2(4000);		
BEGIN
DBMS_STREAMS_ADM.ADD_SCHEMA_RULES(
   schema_name         => l_watched_schema,
   streams_type        => 'capture',
   streams_name        => 'CHANGE_CAPTURE',
   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
   include_dml         => TRUE,
   include_ddl         => FALSE, --TRUE,
   include_tagged_lcr  => FALSE,
   source_database     => NULL,
   --dml_rule_name       OUT  VARCHAR2,
   --ddl_rule_name       OUT  VARCHAR2,
   inclusion_rule      => TRUE,
   and_condition       => NULL);
DBMS_APPLY_ADM.CREATE_APPLY(
     queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
     apply_name           => 'CHANGE_APPLY',
     rule_set_name        => l_apply_rule,
     apply_tag            => NULL);
DBMS_APPLY_ADM.SET_EXECUTE(
  rule_name  => l_apply_rule,
  execute    => FALSE);
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => l_apply_rule,
  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY');
DBMS_STREAMS_ADM.ADD_SCHEMA_RULES(
   schema_name         => l_watched_schema,
   streams_type        => 'apply',
   streams_name        => 'CHANGE_APPLY',
   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
   include_dml         => TRUE,
   include_ddl         => FALSE, --TRUE,
   include_tagged_lcr  => FALSE,
   source_database     => NULL,
   dml_rule_name       => l_dml_rule,
   ddl_rule_name       => l_ddl_rule,
   inclusion_rule      => TRUE,
   and_condition       => NULL);
DBMS_APPLY_ADM.SET_EXECUTE(
  rule_name  => l_dml_rule,
  execute    => FALSE);
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => l_dml_rule,
  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY');
/*
DBMS_APPLY_ADM.SET_EXECUTE(
  rule_name  => l_ddl_rule,
  execute    => FALSE);
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => l_ddl_rule,
  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY');

END;
/
*/
REVOKE ALL PRIVILEGES FROM REPL_MGR;

--4. Start up apply & capture processes
BEGIN
DBMS_AQADM.START_QUEUE ('REPL_MGR.Q_CHANGE_APPLY', TRUE, TRUE);
END;
/
BEGIN
DBMS_AQADM.START_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_APPLY_ADM.START_APPLY (
   apply_name      => 'CHANGE_APPLY');
END;
/
BEGIN
DBMS_CAPTURE_ADM.START_CAPTURE(
   capture_name      => 'CHANGE_CAPTURE');
END;
/

-- 5. Add tables to track
DECLARE
  l_scn NUMBER;
BEGIN
  l_scn := REPL_MGR.REPLICATE_PKG.PREPARE_TABLE('DEVICE', 'DEVICE_CALL_IN_RECORD');
  l_scn := REPL_MGR.REPLICATE_PKG.PREPARE_TABLE('PSS', 'TRAN');
  l_scn := REPL_MGR.REPLICATE_PKG.PREPARE_TABLE('LOCATION', 'CUSTOMER');
  l_scn := REPL_MGR.REPLICATE_PKG.PREPARE_TABLE('LOCATION', 'LOCATION');
END;
/
/*
BEGIN
REPL_MGR.REPLICATE_PKG.sync_schema('DEVICE');
COMMIT;
END;*/


-- 6. Check Status
SELECT * FROM V$STREAMS_CAPTURE;
SELECT * FROM V$STREAMS_APPLY_READER;
SELECT * FROM V$STREAMS_APPLY_SERVER;

SELECT * FROM DBA_QUEUES;

--SELECT * FROM V$LOGMNR_CONTENTS;

SELECT * FROM V$LOGMNR_LOGS;

SELECT * FROM V$RULE_SET;
SELECT * FROM V$RULE;

/*
BEGIN
DBMS_AQADM.CREATE_QUEUE_TABLE (
		queue_table => 'REPL_MGR.QT_CHANGE_APPLY_2', 
		multiple_consumers => FALSE, 
		queue_payload_type => 'SYS.ANYDATA');
END;

BEGIN
DBMS_AQADM.CREATE_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_APPLY_2', 
		queue_table => 'REPL_MGR.QT_CHANGE_APPLY_2', 
		retry_delay => 60, 
		max_retries => 10);
END;
BEGIN
DBMS_AQADM.START_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_APPLY');
END;

BEGIN
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => 'REPL_MGR_DATA60', -- DDL
  destination_queue_name  => 'REPL_MGR.Q_CHANGE_APPLY_2');
END;
BEGIN
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => 'REPL_MGR_DATA59', --DML
  destination_queue_name  => 'REPL_MGR.Q_CHANGE_APPLY_2');
END;
*/
/*
begin
DBMS_AQADM.ALTER_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_APPLY', 
		retry_delay => 0, 
		max_retries => NULL);
END;
/
BEGIN
DBMS_APPLY_ADM.ALTER_APPLY(
     apply_name            => 'CHANGE_APPLY',
     ddl_handler           => '');

DBMS_APPLY_ADM.SET_DML_HANDLER(
   object_name          IN  VARCHAR2,
   object_type          IN  VARCHAR2,
   operation_name       IN  VARCHAR2,
   error_handler        IN  BOOLEAN  DEFAULT false,
   user_procedure       IN  VARCHAR2,
   apply_database_link  IN  VARCHAR2  DEFAULT NULL,
   apply_name           IN  VARCHAR2  DEFAULT NULL);
*/
/*
BEGIN
  DBMS_AQADM.REMOVE_SUBSCRIBER(
    queue_name          =>  'REPL_MGR.Q_CHANGE_CAPTURE',
    subscriber          =>  SYS.AQ$_AGENT('REPL_MGR', NULL, NULL)--,
    --rule                =>  NULL,
    --transformation      =>  NULL
	);
END;
/
*/


/*
select * from ALL_CAPTURE;
select * from V$STREAMS_CAPTURE;
-- for testing
DROP TABLE REPL_MGR.TEST_DATA;

CREATE USER REPL_MGR_DATA
IDENTIFIED BY REPL_MGR_DATA
DEFAULT TABLESPACE REPL_MGR_DATA
TEMPORARY TABLESPACE TEMP
/
ALTER USER REPL_MGR_DATA QUOTA UNLIMITED ON REPL_MGR_DATA QUOTA UNLIMITED ON REPL_MGR_INDX
/
CREATE TABLE REPL_MGR_DATA.TEST_DATA
    (TEST_DATA_ID                    NUMBER(20,0) NOT NULL PRIMARY KEY,
    TEST_DATA_TS                        DATE NOT NULL,
    TEST_DATA_MSG                       VARCHAR2(60))
  TABLESPACE  REPL_MGR_DATA
/
BEGIN
DBMS_APPLY_ADM.set_table_instantiation_scn(
	source_object_name => 'REPL_MGR_DATA.TEST_DATA',
	source_database_name => 'KRU02',
	instantiation_scn => 5965625533014 --,
	--apply_database_link
	);
END;

INSERT INTO REPL_MGR_DATA.TEST_DATA
(TEST_DATA_ID,TEST_DATA_TS,TEST_DATA_MSG)
SELECT 14, SYSDATE, 'message #14' FROM dual;


alter table REPL_MGR_DATA.TEST_DATA ADD(TEST_DATA_CLOB CLOB);

SELECT * FROM all_objects WHERE object_id = 56720

--clean up all
BEGIN
DBMS_CAPTURE_ADM.STOP_CAPTURE(
   capture_name      => 'CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_CAPTURE_ADM.DROP_CAPTURE(
   capture_name      => 'CHANGE_CAPTURE');
END;
/

BEGIN
DBMS_STREAMS_ADM.REMOVE_RULE(
   rule_name         => 'REPL_MGR_DATA64',
   streams_type      => 'capture',
   streams_name      => 'CHANGE_CAPTURE',
   drop_unused_rule  => TRUE,
   inclusion_rule    => TRUE);
END;
BEGIN
DBMS_STREAMS_ADM.remove_rule(
rule_name =>'RULESET$_14',
streams_type => 'capture',
streams_name => 'CHANGE_CAPTURE',
drop_unused_rule => true,
inclusion_rule => true);
END;
GRANT ALL ON REPL_MGR.lcr_sync TO PUBLIC;

ALTER DATABASE ADD SUPPLEMENTAL LOG DATA (PRIMARY KEY) COLUMNS;
BEGIN
DBMS_APPLY_ADM.STOP_APPLY('CHANGE_APPLY');
END;
/

begin
DBMS_AQADM.GRANT_QUEUE_PRIVILEGE(
    privilege  =>  'ALL',
    queue_name => 'REPL_MGR.Q_CHANGE_APPLY_2',
    grantee    => 'REPL_MGR');
END;
BEGIN
DBMS_APPLY_ADM.DROP_APPLY('CHANGE_APPLY',false);
END;
/
*/

SELECT CURRENT_SCN, DB_UNIQUE_NAME
          --INTO l_scn, l_db_name
          FROM SYS.V_$DATABASE;

BEGIN
DBMS_APPLY_ADM.set_table_instantiation_scn(
	source_object_name => 'DEVICE.DEVICE_CALL_IN_RECORD',
	source_database_name => 'usadbd02',
	instantiation_scn => 5965625533014 --,
	--apply_database_link
	);
END;
begin
DBMS_RULE_ADM.ALTER_RULE(
   rule_name           => 'REPL_MGR.USADBD0299',
   condition  => '1 = 1');
END;
/

/* CLEAN-UP */
BEGIN
DBMS_CAPTURE_ADM.STOP_CAPTURE(
   capture_name      => 'CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_CAPTURE_ADM.DROP_CAPTURE('CHANGE_CAPTURE',TRUE);
END;
/
BEGIN
DBMS_AQADM.PURGE_QUEUE_TABLE(
   'REPL_MGR.QT_CHANGE_APPLY',
   NULL,
   NULL); -- aq$_purge_options_t);
END;
/
BEGIN
DBMS_AQADM.PURGE_QUEUE_TABLE(
   'REPL_MGR.QT_CHANGE_CAPTURE',
   NULL,
   NULL); -- aq$_purge_options_t);
END;
/
BEGIN
DBMS_AQADM.STOP_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_APPLY');
END;
/
BEGIN
DBMS_AQADM.DROP_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_APPLY');
END;
/
BEGIN
DBMS_AQADM.DROP_QUEUE_TABLE (
		queue_table => 'REPL_MGR.QT_CHANGE_APPLY');
END;
/
BEGIN
DBMS_APPLY_ADM.DELETE_ALL_ERRORS('CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_APPLY_ADM.DELETE_ALL_ERRORS('CHANGE_APPLY');
END;
/

BEGIN
DBMS_APPLY_ADM.STOP_APPLY('CHANGE_APPLY'); --, TRUE);
END;
/
BEGIN
DBMS_APPLY_ADM.DROP_APPLY('CHANGE_APPLY',TRUE);
END;
/
BEGIN
DBMS_AQADM.STOP_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_AQADM.DROP_QUEUE (
		queue_name => 'REPL_MGR.Q_CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_AQADM.DROP_QUEUE_TABLE (
		queue_table => 'REPL_MGR.QT_CHANGE_CAPTURE');
END;
/
BEGIN
DBMS_AQADM.DROP_AQ_AGENT('REPL_MGR');
DBMS_AQADM.DROP_AQ_AGENT('REPLICATE_AGENT');
DBMS_AQADM.DROP_AQ_AGENT('CHANGE_CAPTURE');
DBMS_AQADM.DROP_AQ_AGENT('CHANGE_APPLY');
END;
/
BEGIN
DBMS_RULE_ADM.DROP_RULE_SET(
   rule_set_name                  => 'SYSTEM.CHANGE_RULESET');
END;
/
BEGIN
DBMS_RULE_ADM.DROP_RULE(
   rule_name                  => 'SYSTEM.SCHEMA_CHANGE_RULE');
END;
/
BEGIN
DBMS_RULE_ADM.DROP_RULE_SET(
   rule_set_name                  => 'REPL_MGR.CHANGE_APPLY_RULESET');
END;
/
BEGIN
DBMS_RULE_ADM.DROP_RULE(
   rule_name                  => 'REPL_MGR.APPLY_ALL');
END;
/
purge dba_recyclebin;


-- other stuff
BEGIN
DBMS_APPLY_ADM.ALTER_APPLY(
     apply_name            => 'CHANGE_APPLY',
     ddl_handler           => '');
END;
/

SELECT * FROM DBA_CAPTURE;
SELECT * FROM V$STREAMS_CAPTURE;
SELECT * FROM v$propagation_sender;

SELECT r.SOURCE_DATABASE,
       r.SEQUENCE#, 
       r.NAME, 
       r.DICTIONARY_BEGIN, 
       r.DICTIONARY_END 
  FROM DBA_REGISTERED_ARCHIVED_LOG r, DBA_CAPTURE c
  WHERE c.CAPTURE_NAME = 'STRM05_CAPTURE' AND 
        r.CONSUMER_NAME = c.CAPTURE_NAME;
        
SELECT * FROM V$STREAMS_APPLY_COORDINATOR;
SELECT * FROM V$STREAMS_APPLY_SERVER;
SELECT * FROM V$STREAMS_APPLY_READER;

SELECT r.APPLY_NAME,
       DECODE(ap.APPLY_CAPTURED,
                'YES','Captured LCRS',
                'NO','User-enqueued messages','UNKNOWN') APPLY_CAPTURED,
       SUBSTR(s.PROGRAM,INSTR(S.PROGRAM,'(')+1,4) PROCESS_NAME,
       r.STATE,
       r.TOTAL_MESSAGES_DEQUEUED
       FROM V$STREAMS_APPLY_READER r, V$SESSION s, DBA_APPLY ap 
       WHERE r.SID = s.SID AND 
             r.SERIAL# = s.SERIAL# AND 
             r.APPLY_NAME = ap.APPLY_NAME;

SELECT APPLY_NAME,
     (DEQUEUE_TIME-DEQUEUED_MESSAGE_CREATE_TIME)*86400 LATENCY,
     TO_CHAR(DEQUEUED_MESSAGE_CREATE_TIME,'HH24:MI:SS MM/DD/YY') CREATION,
     TO_CHAR(DEQUEUE_TIME,'HH24:MI:SS MM/DD/YY') LAST_DEQUEUE,
     DEQUEUED_MESSAGE_NUMBER  
  FROM V$STREAMS_APPLY_READER;
  
SELECT * FROM DBA_APPLY;
SELECT * FROM DBA_APPLY_CONFLICT_COLUMNS;
SELECT * FROM DBA_APPLY_DML_HANDLERS;
SELECT * FROM DBA_APPLY_ENQUEUE;
SELECT * FROM DBA_APPLY_ERROR;
SELECT * FROM DBA_APPLY_EXECUTE;
SELECT * FROM DBA_APPLY_INSTANTIATED_GLOBAL;
SELECT * FROM DBA_APPLY_INSTANTIATED_OBJECTS;
SELECT * FROM DBA_APPLY_INSTANTIATED_SCHEMAS;
SELECT * FROM DBA_APPLY_KEY_COLUMNS;
SELECT * FROM DBA_APPLY_PARAMETERS;
SELECT * FROM DBA_APPLY_PROGRESS;
SELECT * FROM DBA_APPLY_TABLE_COLUMNS;
SELECT * FROM V$LOGMNR_PROCESS;
SELECT * FROM V$LOGMNR_SESSION;
SELECT * FROM V$LOGMNR_STATS;
SELECT * FROM DBA_LOGMNR_SESSION;

SELECT * FROM DBA_RULE_SET_RULES;
SELECT * FROM DBA_RULE_SETS;
SELECT RULE_OWNER, RULE_NAME, DBMS_LOB.SUBSTR(RULE_CONDITION, 4000, 1), RULE_EVALUATION_CONTEXT_OWNER, RULE_EVALUATION_CONTEXT_NAME, 
--(SELECT TO_CHAR(rn) || column_value FROM (SELECT ROWNUM rn, COLUMN_VALUE FROM TABLE(r.RULE_ACTION_CONTEXT.GET_ALL_NAMES ())) WHERE rn = 1 ),
--CASE when RULE_ACTION_CONTEXT IS NULL THEN NULL ELSE RULE_ACTION_CONTEXT.GET_ALL_NAMES end,-- ','), 
RULE_COMMENT
,R.RULE_ACTION_CONTEXT.get_value('APPLY$_EXECUTE').AccessVarchar2()
,R.RULE_ACTION_CONTEXT.get_value('APPLY$_ENQUEUE').AccessVarchar2() 
FROM DBA_RULES r;

--SELECT * FROM DBA_STREAMS_ADD_COLUMN;
SELECT * FROM DBA_STREAMS_ADMINISTRATOR;
SELECT * FROM DBA_STREAMS_DELETE_COLUMN;
SELECT * FROM DBA_STREAMS_GLOBAL_RULES;
--SELECT * FROM DBA_STREAMS_MESSAGE_CONSUMERS;
SELECT * FROM DBA_STREAMS_MESSAGE_RULES;
SELECT * FROM DBA_STREAMS_NEWLY_SUPPORTED;
SELECT * FROM DBA_STREAMS_RENAME_COLUMN;
SELECT * FROM DBA_STREAMS_RENAME_SCHEMA;
SELECT * FROM DBA_STREAMS_RENAME_TABLE;
--SELECT * FROM DBA_STREAMS_RULES;
SELECT * FROM DBA_STREAMS_SCHEMA_RULES;
SELECT * FROM DBA_STREAMS_TABLE_RULES;
SELECT * FROM DBA_STREAMS_TRANSFORM_FUNCTION;
--SELECT * FROM DBA_STREAMS_TRANSFORMATIONS;
SELECT * FROM DBA_STREAMS_UNSUPPORTED;

GRANT EXECUTE_ON_EVALUATION_CONTEXT ON SYS TO REPL_MGR;

BEGIN
DBMS_RULE_adm.grant_object_privilege(
SYS.DBMS_RULE_ADM.EXECUTE_ON_EVALUATION_CONTEXT,'SYS.STREAMS$_EVALUATION_CONTEXT','REPL_MGR',false);
END;

BEGIN
DBMS_RULE_ADM.ALTER_RULE(
   rule_name                  => 'REPL_MGR.DEVICE92',
   condition                  => '(:dml.get_object_owner() = ''DEVICE'' and :dml.get_object_name() IN(''DEVICE'',''HOST'',''DEVICE_CALL_IN_RECORD'')) OR (:dml.get_object_owner() = ''PSS'' and :dml.get_object_name() IN(''TRAN'')) OR (:dml.get_object_owner() = ''LOCATION'' and :dml.get_object_name() IN(''LOCATION'', ''CUSTOMER''))');
END;
/
BEGIN
DBMS_RULE_ADM.ALTER_RULE(
   rule_name                  => 'REPL_MGR.DEVICE94',
   condition                  => '1=1');
END;
/

SELECT * FROM session_privs
begin
DBMS_CAPTURE_ADM.PREPARE_SCHEMA_INSTANTIATION(
   		   schema_name  => 'APP_USER');
END;

BEGIN
DBMS_RULE_ADM.DROP_RULE_SET(
   rule_set_name                  => 'WEI.Q_CHANGE_CAPTURE_R');
END;
/
BEGIN
DBMS_RULE_ADM.DROP_RULE(
   rule_name                  => 'SYSTEM.WEI24');
END;
/

GRANT execute ON REPL_MGR.REPLICATE_PKG TO RDW_LOADER;

SELECT a.queue, a.msg_id, a.corr_id, a.msg_priority, a.msg_state,
       a.delay, a.delay_timestamp, a.expiration, a.enq_time,
       a.enq_timestamp, a.enq_user_id, a.enq_txn_id, a.deq_time,
       a.deq_timestamp, a.deq_user_id, a.deq_txn_id, a.retry_count,
       a.exception_queue_owner, a.exception_queue, --a.user_data,
       a.original_queue_name, a.original_queue_owner,
       a.expiration_reason, a.sender_name, a.sender_address,
       a.sender_protocol, a.original_msgid
  FROM repl_mgr.aq$qt_change_apply a;

UPDATE repl_mgr.qt_change_apply SET STATE = 0, EXCEPTION_QUEUE = NULL, q_name = 'Q_CHANGE_APPLY', retry_count = 0 WHERE STATE = 3;

begin
DBMS_AQADM.SCHEDULE_PROPAGATION ( 
   queue_name => 'REPL_MGR.AQ$_QT_CHANGE_APPLY_E', 
   destination => NULL,
   start_time => SYSDATE,
   duration    => 15, 
   next_time => NULL, 
   latency  => 0,
   destination_queue =>'REPL_MGR.Q_CHANGE_APPLY');
END;

SELECT MAX(DEVICE_CALL_IN_RECORD_ID) FROM DEVICE.DEVICE_CALL_IN_RECORD;

DECLARE
  l_scn NUMBER;
BEGIN
 l_scn := REPL_MGR.REPLICATE_PKG.prepare_table('PSS', 'TRAN');
COMMIT;
END;
/
BEGIN
--REPL_MGR.REPLICATE_PKG.sync_table_and_dependees('DEVICE','HOST', null);
REPL_MGR.REPLICATE_PKG.sync_table_in_number_range('DEVICE','DEVICE_CALL_IN_RECORD',NULL,'DEVICE_CALL_IN_RECORD_ID',1849100,1849200);
COMMIT;
END;
/
BEGIN
--REPL_MGR.REPLICATE_PKG.sync_table_and_dependees('DEVICE','HOST', null);
REPL_MGR.REPLICATE_PKG.sync_table_in_number_range('PSS', 'TRAN',NULL,'TRAN_ID',766500,766599);
COMMIT;
END;
BEGIN
--REPL_MGR.REPLICATE_PKG.sync_table_and_dependees('DEVICE','HOST', null);
REPL_MGR.REPLICATE_PKG.sync_table_in_number_range('PSS', 'TRAN',NULL,'TRAN_ID',766400,766499);
COMMIT;
END;
BEGIN
--REPL_MGR.REPLICATE_PKG.sync_table_and_dependees('DEVICE','HOST', null);
REPL_MGR.REPLICATE_PKG.sync_table_in_number_range('PSS', 'TRAN',NULL,'TRAN_ID',766300,766399);
COMMIT;
END;
