-- code to add ccs transport error link in usalive
DECLARE
   weblinkId INTEGER;
BEGIN
    select web_content.seq_web_link_id.nextval into webLinkId from dual;
    insert into web_content.web_link values(webLinkId,'CCS Transport Error', './frame.i?content=ccs_transport_error.i','CCS Transport Error report', 'Customer Service','M',270);
    insert into web_content.web_link_requirement values(webLinkId,14);
    insert into web_content.web_link_requirement values(webLinkId,1);
    COMMIT;
EXCEPTION
     WHEN OTHERS THEN
          ROLLBACK;
          RAISE;
END;

-- create the transport type EmailLink
insert into report.ccs_transport_type values(6, 'Email With Link');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_REGEX)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 6, 'Email', 'Y','/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_REGEX)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 6, 'CC', 'N','/(^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$)|^$/');

commit;
