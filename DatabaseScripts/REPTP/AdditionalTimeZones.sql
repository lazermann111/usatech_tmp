INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 26, -39600000, 'Samoa Standard Time', 'SST', 'Pacific/Samoa' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 26);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 27, -10800000, 'Brasilia Time', 'BET', 'America/Araguaina' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 27);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 28, -7200000, 'Fernando de Noronha Time', 'FNT', 'America/Noronha' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 28);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 29, -3600000, 'Eastern Greenland Time', 'EGT', 'America/Scoresbysund' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 29);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 30, 0, 'Western European Time', 'WET', 'Europe/Lisbon' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 30);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 31, 3600000, 'Central European Time', 'CET', 'Europe/Amsterdam' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 31);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 32, 7200000, 'Eastern European Time', 'EET', 'Europe/Athens' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 32);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 33, 10800000, 'Moscow Standard Time', 'MSK', 'Europe/Moscow' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 33);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 34, 14400000, 'Samara Time', 'SAMT', 'Europe/Samara' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 34);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 35, 18000000, 'Yekaterinburg Time', 'YEKT', 'Asia/Yekaterinburg' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 35);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 36, 19800000, 'India Standard Time', 'IST', 'Asia/Calcutta' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 36);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 37, 21600000, 'Novosibirsk Time', 'NOVT', 'Asia/Novosibirsk' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 37);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 38, 25200000, 'Krasnoyarsk Time', 'KRAT', 'Asia/Krasnoyarsk' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 38);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 39, 28800000, 'China Standard Time', 'CTT', 'Asia/Harbin' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 39);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 40, 32400000, 'Japan Standard Time', 'JST', 'Asia/Tokyo' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 40);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 41, 34200000, 'Australian Central Standard Time', 'ACST', 'Australia/Adelaide' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 41);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 42, 36000000, 'Australian Eastern Standard Time', 'AEST', 'Australia/Brisbane' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 42);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 43, 39600000, 'New Caledonia Time', 'NCT', 'Pacific/Noumea' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 43);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 44, 43200000, 'New Zealand Standard Time', 'NZST', 'Pacific/Auckland' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 44);

INSERT INTO REPORT.TIME_ZONE(TIME_ZONE_ID, OFFSET, NAME, ABBREV, TIME_ZONE_GUID)
SELECT 45, 46800000, 'West Samoa Standard Time', 'WST', 'Pacific/Apia' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TIME_ZONE WHERE TIME_ZONE_ID = 45);

COMMIT;

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'SST', 'Samoa Standard Time', -11, 'Pacific/Samoa' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'SST');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'FNT', 'Fernando de Noronha Time', -2, 'America/Noronha' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'FNT');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'EGT', 'Eastern Greenland Time', -1, 'America/Scoresbysund' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'EGT');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'YEKT', 'Yekaterinburg Time', 5, 'Asia/Yekaterinburg' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'YEKT');

DELETE FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'IST';
INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'IST', 'India Standard Time', 5.5, 'Asia/Calcutta' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'IST');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'NOVT', 'Novosibirsk Time', 6, 'Asia/Novosibirsk' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'NOVT');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'KRAT', 'Krasnoyarsk Time', 7, 'Asia/Krasnoyarsk' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'KRAT');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'JST', 'Japan Standard Time', 9, 'Asia/Tokyo' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'JST');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'NZST', 'New Zealand Standard Time', 12, 'Pacific/Auckland' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'NZST');

INSERT INTO LOCATION.TIME_ZONE(TIME_ZONE_CD, TIME_ZONE_NAME, UTC_OFFSET, TIME_ZONE_GUID)
SELECT 'WST', 'West Samoa Standard Time', 13, 'Pacific/Apia' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM LOCATION.TIME_ZONE WHERE TIME_ZONE_CD = 'WST');

COMMIT;
