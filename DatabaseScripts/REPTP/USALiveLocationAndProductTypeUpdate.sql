SET DEFINE OFF;

delete from report.location_type where location_type_name in ('Kiosk/Parking/Transportation', 'Kiosk/Cash to Card', 'Kiosk/Retail/Mobile', 'Kiosk/Retail/Other', 'Vending/Business/Office Other', 'Vending/Business/Entertainment BOH', 'Vending/Business/Manufacturing Food', 'Vending/Education/Specialized', 'Vending/Entertainment/Gymnasium', 'Vending/Hospitality/Limited Service Hotel');
update report.location_type set location_type_name = 'Car Wash/Air Service' where location_type_name = 'Kiosk/Car Wash/Air Service';
update report.location_type set location_type_name = 'Amusement' where location_type_name = 'Kiosk/Amusement';
update report.location_type set location_type_name = 'Office Equipment' where location_type_name = 'Kiosk/AV/Office';
update report.location_type set location_type_name = 'Kiosk' where location_type_name = 'Kiosk/Health/Sport';
update report.location_type set location_type_name = 'Vending/Education/Technical & Specialized' where location_type_name = 'Vending/Education/Technical School';
update report.location_type set location_type_name = 'Vending/Entertainment/Sport & Gym' where location_type_name = 'Vending/Entertainment/Sport';
update report.location_type set location_type_name = 'Vending/Hospitality/Hotel' where location_type_name = 'Vending/Hospitality/Full Service Hotel';
insert into report.location_type(location_type_id, location_type_name) select (select max(location_type_id) + 1 from report.location_type), 'Laundry' from dual where not exists (select 1 from report.location_type where location_type_name = 'Laundry');

insert into report.product_type(product_type_id, product_type_name, specify) select (select max(product_type_id) + 1 from report.product_type), 'College Laundry', 'N' from dual where not exists (select 1 from report.product_type where product_type_name = 'College Laundry');

commit;
