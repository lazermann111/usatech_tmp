ALTER TABLE REPORT.TERMINAL_COLUMN_MAP ADD MDB_NUMBER_2 VARCHAR2(255);
UPDATE REPORT.TERMINAL_COLUMN_MAP SET MDB_NUMBER_2 = MDB_NUMBER;
COMMIT;
ALTER TABLE REPORT.TERMINAL_COLUMN_MAP MODIFY MDB_NUMBER_2 VARCHAR2(255) NOT NULL;
ALTER TABLE REPORT.TERMINAL_COLUMN_MAP DROP CONSTRAINT TERMINAL_COLU_PK01013462026356;
ALTER TABLE REPORT.TERMINAL_COLUMN_MAP DROP COLUMN MDB_NUMBER;
ALTER TABLE REPORT.TERMINAL_COLUMN_MAP RENAME COLUMN MDB_NUMBER_2 TO MDB_NUMBER;
ALTER TABLE REPORT.TERMINAL_COLUMN_MAP ADD CONSTRAINT PK_TERMINAL_COLUMN_MAP PRIMARY KEY(TERMINAL_ID, MDB_NUMBER);
