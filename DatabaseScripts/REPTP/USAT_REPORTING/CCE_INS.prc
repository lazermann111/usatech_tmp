create or replace procedure USAT_REPORTING.CCE_INS(l_bool_in BOOLEAN) AS

l_bool BOOLEAN;

begin

delete from USAT_REPORTING.rdb_device_list_mc;
commit;
delete from USAT_REPORTING.mc_device_serial_cd_LIST;
commit;
delete from USAT_REPORTING.mc_std_health_report;
commit;
delete from USAT_REPORTING.CCE_device_serial_cd_LIST;
commit;
delete  from USAT_REPORTING.cce_std_health_report;
commit;


/**needs grants
insert into USAT_REPORTING.rdb_device_list_mc
SELECT t.customer_id, e.eport_serial_num, l.location_name, r.region_name, t.asset_nbr,
       x.first_tran_date, x.last_tran_date,
       x.first_credit_tran_date, x.last_credit_tran_date,
       x.first_cash_tran_date, x.last_cash_tran_date
  FROM report.terminal t
            INNER JOIN report.location l         ON (t.location_id = l.location_id)
            INNER JOIN report.terminal_eport te  ON (te.terminal_id = t.terminal_id)
       LEFT OUTER JOIN report.terminal_region tr ON (t.terminal_id = tr.terminal_id) /* OJ: include z-undefined */
/****needs grants
            INNER JOIN report.eport e            ON (te.eport_id = e.eport_id)
       LEFT OUTER JOIN report.region r ON (tr.region_id = r.region_id)
       LEFT OUTER JOIN (select
                    eport_id,
                    MIN(first_tran_date) first_tran_date,
                    MAX(last_tran_date) last_tran_date,
                             MIN(first_credit_tran_date) first_credit_tran_date,
                             MAX(last_credit_tran_date) last_credit_tran_date,
                             MIN(first_cash_tran_date) first_cash_tran_date,
                             MAX(last_cash_tran_date) last_cash_tran_date
                    from USAT_REPORTING.eport_dates
                    group by eport_id) x ON (e.eport_id = x.eport_id)
WHERE  SYSDATE >= NVL (te.start_date, min_date)    /* "current" device view */
/**need grants
   AND SYSDATE < NVL (te.end_ddrop procedure USAT_REPORTING.CCE_DBP_TMP;
commit;


insert into USAT_REPORTING.mc_device_serial_cd_LIST
SELECT
r.customer_id,
p.DEVICE_SERIAL_CD,
r.LOCATION_NAME,
r.REGION_NAME,
r.ASSET_NBR,
p.LAST_ACTIVITY_TS,
r.FIRST_TRAN_DATE,
r.LAST_TRAN_DATE,
r.FIRST_CREDIT_TRAN_DATE,
r.LAST_CREDIT_TRAN_DATE,
r.FIRST_CASH_TRAN_DATE,
r.LAST_CASH_TRAN_DATE,
null as fill_card_config_err
FROM USAT_REPORTING.rdb_device_list_mc r
INNER JOIN USAT_REPORTING.dbp_device_list_mc@usadbp.world p
ON r.EPORT_SERIAL_NUM=p.DEVICE_SERIAL_CD
UNION
SELECT
r.customer_id,
r.EPORT_SERIAL_NUM as device_serial_cd,
r.LOCATION_NAME,
r.REGION_NAME,
r.ASSET_NBR,
null as last_activity_ts,
r.FIRST_TRAN_DATE,
r.LAST_TRAN_DATE,
r.FIRST_CREDIT_TRAN_DATE,
r.LAST_CREDIT_TRAN_DATE,
r.FIRST_CASH_TRAN_DATE,
r.LAST_CASH_TRAN_DATE,
'true' as fill_card_config_err
FROM USAT_REPORTING.rdb_device_list_mc r
LEFT JOIN USAT_REPORTING.dbp_device_list_mc@usadbp.world p
ON r.EPORT_SERIAL_NUM=p.DEVICE_SERIAL_CD
WHERE (p.DEVICE_SERIAL_CD Is Null) AND r.EPORT_SERIAL_NUM IS NOT NULL;

commit;


insert into USAT_REPORTING.mc_std_health_report
SELECT
cdscl.customer_id,
cdscl.REGION_NAME,
case when InStr(cdscl.ASSET_NBR,'TBD',1,1)>0 then Null else cdscl.ASSET_NBR end AS OutletNum,
cdscl.LOCATION_NAME,
cdscl.DEVICE_SERIAL_CD AS ePortNum,
case when cdscl.LOCATION_NAME Is Null then Null
    when InStr(cdscl.LOCATION_NAME,'[',1,1)>0 then
        substr(substr(cdscl.LOCATION_NAME, InStr(cdscl.LOCATION_NAME,'[',1,1)+3,
            length(cdscl.LOCATION_NAME)-InStr(cdscl.LOCATION_NAME,'[',1,1)-2),
        1, length(substr(cdscl.LOCATION_NAME, InStr(cdscl.LOCATION_NAME,'[',1,1)-2,
        length(cdscl.LOCATION_NAME)-InStr(cdscl.LOCATION_NAME,'[',1,1)-2-1)))
    else Null end AS DCnum,
case when cdscl.LOCATION_NAME Is not Null and InStr(cdscl.LOCATION_NAME,'[',1,1)>0
    then substr(cdscl.LOCATION_NAME,1,InStr(cdscl.LOCATION_NAME,'[',1,1)-1-1) else Null end AS OutletNameOrLocation,
cdscl.DEVICE_SERIAL_CD,
(SELECT MAX(last_activity_ts)
    FROM USAT_REPORTING.mc_device_serial_cd_LIST) - cdscl.LAST_ACTIVITY_TS AS NumDaysSinceLastCallin,
cdscl.LAST_ACTIVITY_TS AS DateOfLastCallin,
(SELECT MAX(last_credit_tran_date)
    FROM USAT_REPORTING.mc_device_serial_cd_LIST) - cdscl.LAST_CREDIT_TRAN_DATE AS NumDaysSinceLastCreditTran,
cdscl.LAST_CREDIT_TRAN_DATE AS LastCreditTranDate,
cdscl.LAST_CASH_TRAN_DATE AS LastCashTranDate
FROM USAT_REPORTING.mc_device_serial_cd_LIST cdscl

commit;

insert into USAT_REPORTING.CCE_device_serial_cd_LIST
SELECT
p.DEVICE_SERIAL_CD,
r.LOCATION_NAME,
r.REGION_NAME,
r.ASSET_NBR,
p.LAST_ACTIVITY_TS,
r.FIRST_TRAN_DATE,
r.LAST_TRAN_DATE,
r.FIRST_CREDIT_TRAN_DATE,
r.LAST_CREDIT_TRAN_DATE,
r.FIRST_CASH_TRAN_DATE,
r.LAST_CASH_TRAN_DATE,
null as fill_card_config_err
FROM USAT_REPORTING.rdb_device_list_mc r
INNER JOIN USAT_REPORTING.dbp_device_list_mc@usadbp.world p
ON r.EPORT_SERIAL_NUM=p.DEVICE_SERIAL_CD
where r.customer_id = 2974
UNION
SELECT
r.EPORT_SERIAL_NUM as device_serial_cd,
r.LOCATION_NAME,
r.REGION_NAME,
r.ASSET_NBR,
null as last_activity_ts,
r.FIRST_TRAN_DATE,
r.LAST_TRAN_DATE,
r.FIRST_CREDIT_TRAN_DATE,
r.LAST_CREDIT_TRAN_DATE,
r.FIRST_CASH_TRAN_DATE,
r.LAST_CASH_TRAN_DATE,
'true' as fill_card_config_err
FROM USAT_REPORTING.rdb_device_list_mc r
LEFT JOIN USAT_REPORTING.dbp_device_list_mc@usadbp.world p
ON r.EPORT_SERIAL_NUM=p.DEVICE_SERIAL_CD
WHERE r.customer_id = 2974
and (p.DEVICE_SERIAL_CD Is Null) AND r.EPORT_SERIAL_NUM IS NOT NULL;

commit;

insert into USAT_REPORTING.cce_std_health_report
SELECT
case when InStr(cdscl.REGION_NAME,' P',-1,1)>0 then substr(cdscl.REGION_NAME, 1, 2) else Null end
    AS BASISCompanyDivision,
case when InStr(cdscl.REGION_NAME,' P',-1,1)>0 then substr(substr(cdscl.REGION_NAME,1,4),3,2) else Null end
    AS SalesLocation,
case when InStr(cdscl.REGION_NAME,' P',-1,1)>0 then substr(substr(cdscl.REGION_NAME, 6,length(cdscl.REGION_NAME)-5),
    1, InStr(cdscl.REGION_NAME,' P',-1,1)-5-1) else Null end AS SalesCenterName,
/**/
/****needs grants
case when cdscl.REGION_NAME Is not Null and InStr(cdscl.REGION_NAME,' P',-1,1)>0
    then substr(cdscl.REGION_NAME,InStr(cdscl.REGION_NAME,' P',-1,1)+1,
    length(cdscl.REGION_NAME)-InStr(cdscl.REGION_NAME,' P',-1,1))
    else Null end AS ProfitCtrNum, /**/
/***needs grants
case when InStr(cdscl.ASSET_NBR,'TBD',1,1)>0 then Null else cdscl.ASSET_NBR end AS OutletNum,
case when cdscl.LOCATION_NAME Is Null then Null
    when InStr(cdscl.LOCATION_NAME,'[',1,1)>0 then
        substr(substr(cdscl.LOCATION_NAME, InStr(cdscl.LOCATION_NAME,'[',1,1)+3,
            length(cdscl.LOCATION_NAME)-InStr(cdscl.LOCATION_NAME,'[',1,1)-2),
        1, length(substr(cdscl.LOCATION_NAME, InStr(cdscl.LOCATION_NAME,'[',1,1)-2,
        length(cdscl.LOCATION_NAME)-InStr(cdscl.LOCATION_NAME,'[',1,1)-2-1)))
    else Null end AS DCnum,
case when cdscl.LOCATION_NAME Is not Null and InStr(cdscl.LOCATION_NAME,'[',1,1)>0
    then substr(cdscl.LOCATION_NAME,1,InStr(cdscl.LOCATION_NAME,'[',1,1)-1-1) else Null end AS OutletNameOrLocation,
cdscl.DEVICE_SERIAL_CD,
cdscl.DEVICE_SERIAL_CD AS ePortNum,
(SELECT MAX(last_activity_ts)
    FROM USAT_REPORTING.CCE_device_serial_cd_LIST) - cdscl.LAST_ACTIVITY_TS AS NumDaysSinceLastCallin,
cdscl.LAST_ACTIVITY_TS AS DateOfLastCallin,
(SELECT MAX(last_credit_tran_date)
    FROM USAT_REPORTING.CCE_device_serial_cd_LIST) - cdscl.LAST_CREDIT_TRAN_DATE AS NumDaysSinceLastCreditTran,
cdscl.LAST_CREDIT_TRAN_DATE AS LastCreditTranDate,
cdscl.LAST_CASH_TRAN_DATE AS LastCashTranDate
FROM USAT_REPORTING.CCE_device_serial_cd_LIST cdscl;

commit;

***/


EXCEPTION
  WHEN OTHERS THEN
  RAISE;
  dbms_output.put_line(SQLERRM||' '||SQLCODE);
end; 
/