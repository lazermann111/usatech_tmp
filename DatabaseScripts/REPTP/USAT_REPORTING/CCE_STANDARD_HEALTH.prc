create or replace procedure usat_reporting.cce_standard_health(l_customer varchar2) AS

l_bool BOOLEAN;
l_customer_id varchar2(200);

begin

l_customer_id := 0;
-- the output from this is for Standard Health Report

/*********
select Region_Name,
OutletNum, Location_name, ePortNum, NumDaysSinceLastCallin, DateOfLastCallin,
NumDaysSinceLastCreditTran, LastCreditTranDate, LastCashTranDate,
'=IF(E' || to_char(rownum + 3) || '>$E$2,"Y",IF(G' || to_char(rownum + 3) ||
'="","N",IF(G' || to_char(rownum + 3) || '>$G$2,"Y","N")))' AS show,
last_updated_ts
from
(SELECT cshr.Region_Name,
cshr.OutletNum, cshr.Location_name, cshr.ePortNum, cshr.NumDaysSinceLastCallin, cshr.DateOfLastCallin,
cshr.NumDaysSinceLastCreditTran, cshr.LastCreditTranDate, cshr.LastCashTranDate,
gd2.RSSI,gd2.last_updated_ts
FROM
mc_std_health_report cshr
left join device.device@usadbp.world d
on cshr.EPORTNUM = d.DEVICE_SERIAL_CD and d.device_active_yn_flag = 'Y'
left join (select device_id, max(assigned_ts) as assigned_ts from device.gprs_device@usadbp.world
    where device_id is not null group by device_id) gd
on d.device_id = gd.device_id
left join device.gprs_device@usadbp.world gd2
on gd2.device_id = gd.device_id and gd2.assigned_ts = gd.assigned_ts
where cshr.customer_id = l_customer_id;
order by cshr.ePortNum);
********/


EXCEPTION
  WHEN OTHERS THEN
  RAISE;
  dbms_output.put_line(SQLERRM||' '||SQLCODE);
end;
/