CREATE OR REPLACE TRIGGER sequoia.trbi_controller_lease
BEFORE INSERT ON sequoia.controller_lease
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/