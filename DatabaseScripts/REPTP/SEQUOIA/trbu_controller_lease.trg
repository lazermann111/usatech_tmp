CREATE OR REPLACE TRIGGER sequoia.trbu_controller_lease
BEFORE UPDATE ON sequoia.controller_lease
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT :OLD.CREATED_TS,
           :OLD.CREATED_BY,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/