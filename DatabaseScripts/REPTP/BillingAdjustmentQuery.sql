select 'CORP.PAYMENTS_PKG.ADJUSTMENT_INS(' || cb.customer_id || ', ''USATMaster'', ''' || cur.currency_code || ''', ''Credit for ' 
|| l.description || ' /' || t.terminal_nbr || '/' || l.ledger_id || ''', ' || abs(l.amount) || ', PN_DOC_ID, PN_LEDGER_ID, ''Y''); COMMIT;'
from corp.ledger l join corp.service_fees sf on l.service_fee_id = sf.service_fee_id
join corp.batch b on l.batch_id = b.batch_id join corp.doc d on b.doc_id = d.doc_id
join corp.currency cur on d.currency_id = cur.currency_id 
join corp.customer_bank cb on d.customer_bank_id = cb.customer_bank_id
join report.terminal t on sf.terminal_id = t.terminal_id
where l.create_date > to_date('08/16/2018', 'mm/dd/yyyy') and l.entry_type = 'SF' and sf.frequency_id != 4
and l.entry_date < to_date('01/01/2018', 'mm/dd/yyyy') and l.amount < 0