DECLARE
  ln_cnt PLS_INTEGER;
  ln_updated PLS_INTEGER;
BEGIN
  LOOP
  SELECT COUNT(*)
    INTO ln_cnt
    FROM REPORT.QT_SYNC_MSG Q
   WHERE Q.STATE = 0
     AND Q.USER_DATA.SQL_TO_RUN LIKE 'CALL SYNC_PKG.RECEIVE_PROCESS_FEE_VALUES(%)';
  IF ln_cnt = 0 THEN
    UPDATE REPORT.QT_SYNC_MSG Q 
       SET STATE = 0
     WHERE Q.STATE = 11
       AND EXISTS(SELECT 1 FROM BKRUG.TMP_PFV_SYNC_2010_06_07 S WHERE Q.USER_DATA.SQL_TO_RUN = 'CALL SYNC_PKG.RECEIVE_PROCESS_FEE_VALUES(' || S.PROCESS_FEE_ID || ')')
       AND ROWNUM < 11;
    ln_updated := SQL%ROWCOUNT;
    COMMIT;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || ' - Marked ' || ln_updated || ' rows to be processed');
    IF ln_updated = 0 THEN
      EXIT;
    END IF;
    DBMS_LOCK.SLEEP(ln_updated * 20);
  ELSE
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || ' - Doing nothing as ' || ln_cnt || ' rows are still awaiting processing');
    DBMS_LOCK.SLEEP(ln_cnt * 20);
  END IF;
  END LOOP;
END;
/
