CREATE TABLE UPDATER.LAST_PROCESSED(
    PROCESS_NAME VARCHAR2(50) NOT NULL,
    LAST_PROCESSED_ID NUMBER NOT NULL,
    CREATED_BY                     VARCHAR2(30),
	CREATED_TS                     DATE,
	LAST_UPDATED_BY                VARCHAR2(30),
	LAST_UPDATED_TS                DATE,
	CONSTRAINT PK_LAST_PROCESSED
  PRIMARY KEY (PROCESS_NAME)
  USING INDEX
  TABLESPACE  REPORT_DATA)
  TABLESPACE  REPORT_DATA;


CREATE OR REPLACE TRIGGER UPDATER.TRBI_LAST_PROCESSED
 BEFORE INSERT
 ON UPDATER.LAST_PROCESSED
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT 
        SYSDATE,
        USER,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER UPDATER.TRBU_LAST_PROCESSED
 BEFORE
  UPDATE
 ON UPDATER.LAST_PROCESSED
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_TS,
        SYSDATE,
        USER
      INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_TS,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/


