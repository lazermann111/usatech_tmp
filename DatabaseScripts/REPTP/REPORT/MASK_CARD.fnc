CREATE OR REPLACE FUNCTION REPORT.MASK_CARD (
	l_card IN TRANS.CARD_NUMBER%TYPE,
	l_tt_id IN TRANS.TRANS_TYPE_ID%TYPE
	)
RETURN VARCHAR PARALLEL_ENABLE DETERMINISTIC IS
	lv_card_number VARCHAR(4000);
BEGIN
	lv_card_number := REGEXP_SUBSTR(l_card, '[0-9]{13,}');

	IF lv_card_number IS NULL THEN
		RETURN l_card;
	ELSE
		RETURN RPAD(SUBSTR(lv_card_number, 1, 6), LENGTH(lv_card_number) - 4, '*') || SUBSTR(lv_card_number, -4);
	END IF;
END;
/
