CREATE OR REPLACE PROCEDURE REPORT.USAT_MEI_TERMINALS AS
	LN_USER_ID REPORT.USER_LOGIN.USER_ID%TYPE;
BEGIN
	SELECT MAX(USER_ID)
	INTO LN_USER_ID
	FROM REPORT.USER_LOGIN
	WHERE USER_NAME = 'MEI-Master';

	DELETE FROM REPORT.USER_TERMINAL
	WHERE USER_ID = LN_USER_ID
		AND TERMINAL_ID NOT IN (
			SELECT T.TERMINAL_ID
			FROM REPORT.EPORT E
			JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
			JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
			WHERE E.DEVICE_TYPE_ID = 6
				AND E.EPORT_SERIAL_NUM LIKE 'M1%'
		);
	
	INSERT INTO REPORT.USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
	SELECT DISTINCT LN_USER_ID, T.TERMINAL_ID, 'N'
	FROM REPORT.EPORT E
	JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
	JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
	WHERE E.DEVICE_TYPE_ID = 6
		AND E.EPORT_SERIAL_NUM LIKE 'M1%'
		AND NOT EXISTS (
			SELECT 1
			FROM REPORT.USER_TERMINAL
			WHERE USER_ID = LN_USER_ID
				AND TERMINAL_ID = T.TERMINAL_ID
		);
	
	COMMIT;
END;
/