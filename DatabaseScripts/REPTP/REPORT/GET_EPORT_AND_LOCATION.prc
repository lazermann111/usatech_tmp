CREATE OR REPLACE PROCEDURE REPORT.GET_EPORT_AND_LOCATION (
	   l_serial_nbr IN EPORT.EPORT_SERIAL_NUM%TYPE,
   	   l_device_type IN EPORT.DEVICE_TYPE_ID%TYPE,
   	   l_location_name IN LOCATION.LOCATION_NAME%TYPE,
	   l_effective_date IN DATE,
	   l_eport_id OUT TRANS.EPORT_ID%TYPE,
	   l_location_id OUT TRANS.LOCATION_ID%TYPE,
	   l_terminal_id OUT TRANS.TERMINAL_ID%TYPE)
IS
   l_old_location_id TERMINAL.LOCATION_ID%TYPE;
BEGIN
   BEGIN
   		SELECT EPORT_ID INTO l_eport_id FROM EPORT WHERE EPORT_SERIAL_NUM = l_serial_nbr;
   EXCEPTION
   		WHEN NO_DATA_FOUND THEN
			 SELECT EPORT_SEQ.NEXTVAL INTO l_eport_id FROM DUAL;
			 INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_eport_id, l_serial_nbr, l_effective_date, l_device_type);
		WHEN OTHERS THEN
			 RAISE;
   END;

   BEGIN
       SELECT LOCATION_ID, T.TERMINAL_ID INTO l_old_location_id, l_terminal_id FROM TERMINAL T, TERMINAL_EPORT TE
	      WHERE T.TERMINAL_ID = TE.TERMINAL_ID AND TE.EPORT_ID = l_eport_id
		  AND WITHIN1(l_effective_date, TE.START_DATE, TE.END_DATE) = 1;
   EXCEPTION
	   WHEN NO_DATA_FOUND THEN
		  l_terminal_id := 0;
	   WHEN OTHERS THEN
		  RAISE;
   END;

   BEGIN
   		IF l_terminal_id = 0 THEN
		   SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE TRIM(LOCATION_NAME) = TRIM(l_location_name) AND EPORT_ID = l_eport_id AND TERMINAL_ID = 0;
		ELSE
		   SELECT LOCATION_ID INTO l_location_id FROM (SELECT LOCATION_ID FROM LOCATION WHERE TRIM(LOCATION_NAME) = TRIM(l_location_name) AND TERMINAL_ID = l_terminal_id
		   	  ORDER BY LOCATION_STATUS_RANK(STATUS), CREATE_DATE)  WHERE ROWNUM = 1;
		   --SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE TRIM(LOCATION_NAME) = TRIM(l_location_name) AND TERMINAL_ID = l_terminal_id;
		   IF NVL(l_old_location_id, l_location_id) <> l_location_id THEN
		   	  UPDATE TERMINAL T SET T.LOCATION_ID = l_location_id WHERE T.TERMINAL_ID = l_terminal_id;
		   END IF;
		END IF;
   EXCEPTION
   		WHEN NO_DATA_FOUND THEN
		  	 --NEW LOCATION
		  	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	  		 INSERT INTO LOCATION(LOCATION_ID, TERMINAL_ID, LOCATION_NAME, CREATE_BY, STATUS, EPORT_ID)
	  		     VALUES(l_location_id, l_terminal_id, l_location_name,0,'A', l_eport_id);
			 IF l_terminal_id <> 0 THEN
			 	UPDATE TERMINAL T SET T.LOCATION_ID = l_location_id WHERE T.TERMINAL_ID = l_terminal_id;
			 END IF;
		WHEN OTHERS THEN
			 RAISE;
   END;
   --COMMIT;  THIS WOULD CAUSE AN ERROR SINCE IT HAS AN OUT PARAMETER
END GET_EPORT_AND_LOCATION;
/