CREATE OR REPLACE PROCEDURE REPORT.PROCESS_DATA_EXCHANGE_DI(
    pn_data_exchange_file_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_ID%TYPE,
    pv_foreign_entity_id REPORT.DATA_EXCHANGE_MAPPING.FOREIGN_ENTITY_ID%TYPE,
    pn_customer_id REPORT.DATA_EXCHANGE_MAPPING.CUSTOMER_ID%TYPE,
    pv_device_serial_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
    pn_bank_acct_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    pv_location_name REPORT.LOCATION.LOCATION_NAME%TYPE,
    pv_postal_cd REPORT.TERMINAL_ADDR.ZIP%TYPE,
    pv_city REPORT.TERMINAL_ADDR.CITY%TYPE,
    pv_state REPORT.TERMINAL_ADDR.STATE%TYPE,
    pd_timestamp REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_TS%TYPE,
    pc_status_cd REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_ACTION_CD%TYPE,
    pc_process_status_cd OUT REPORT.DATA_EXCHANGE_ITEM.PROCESS_STATUS_CD%TYPE,
    pt_actions_taken OUT VARCHAR2_TABLE,
    pv_error_message OUT REPORT.DATA_EXCHANGE_ITEM.PROCESS_DETAIL%TYPE)
IS
    ln_data_exchange_type_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_TYPE_ID%TYPE;
    ln_data_exchange_mapping_id REPORT.DATA_EXCHANGE_MAPPING.DATA_EXCHANGE_MAPPING_ID%TYPE;
    lv_curr_foreign_entity_id REPORT.DATA_EXCHANGE_MAPPING.FOREIGN_ENTITY_ID%TYPE;
    ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
    ln_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
    ln_location_id REPORT.LOCATION.LOCATION_ID%TYPE;
    ln_curr_customer_id REPORT.DATA_EXCHANGE_MAPPING.CUSTOMER_ID%TYPE;
    ln_curr_bank_acct_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
    lv_confirmed_state CORP.STATE.STATE_CD%TYPE;
    lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
    lv_country_cd REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE;
    ld_start_date REPORT.DATA_EXCHANGE_ITEM.PROCESS_START_TS%TYPE := SYSDATE;
    lv_lock VARCHAR2(128);
    cn_data_exchange_item_type_id CONSTANT REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_TYPE_ID%TYPE := 3;
    ld_latest_timestamp REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_TS%TYPE;
    ln_device_type_id NUMBER(20,0); --DEVICE.DEVICE_TYPE_ID%TYPE;
    ln_device_sub_type_id NUMBER(3,0); --DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
    lc_initialize_flag VARCHAR2(1);
    lv_app_type VARCHAR2(60); --DEVICE.APP_TYPE.APP_TYPE_VALUE%TYPE;
    ln_pos_pta_tmpl_id NUMBER(20,0); --PSS.POS_PTA_TMPL.POS_PTA_TMPL_ID%TYPE;
    ln_ba_customer_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
    ln_time_zone_id REPORT.TERMINAL.TIME_ZONE_ID%TYPE;
    ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
    ln_new_host_count NUMBER;
    ln_license_id CORP.LICENSE.LICENSE_ID%TYPE;
    lv_machine_make REPORT.MACHINE.MAKE%TYPE := 'Unknown';
    lv_machine_model REPORT.MACHINE.MODEL%TYPE := 'Unknown';
    ln_credential_id NUMBER(20);
    FUNCTION UPDATE_DATA_EXCHANGE_MAPPING(
        pn_data_exchange_type_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_TYPE_ID%TYPE,
        pn_customer_id REPORT.DATA_EXCHANGE_MAPPING.CUSTOMER_ID%TYPE,
        pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_foreign_entity_id REPORT.DATA_EXCHANGE_MAPPING.FOREIGN_ENTITY_ID%TYPE)
    RETURN REPORT.DATA_EXCHANGE_MAPPING.DATA_EXCHANGE_MAPPING_ID%TYPE
    IS
        lv_cust_foreign_entity_id REPORT.DATA_EXCHANGE_MAPPING.FOREIGN_ENTITY_ID%TYPE;
        lv_term_foreign_entity_id REPORT.DATA_EXCHANGE_MAPPING.FOREIGN_ENTITY_ID%TYPE;
        ln_cust_dem_id REPORT.DATA_EXCHANGE_MAPPING.DATA_EXCHANGE_MAPPING_ID%TYPE;
        ln_term_dem_id REPORT.DATA_EXCHANGE_MAPPING.DATA_EXCHANGE_MAPPING_ID%TYPE;  
    BEGIN
        SELECT MAX(CASE WHEN DEM.TERMINAL_ID IS NULL THEN DEM.FOREIGN_ENTITY_ID END),
               MAX(CASE WHEN DEM.TERMINAL_ID IS NULL THEN DEM.DATA_EXCHANGE_MAPPING_ID END),
               MAX(CASE WHEN DEM.TERMINAL_ID = pn_terminal_id THEN DEM.FOREIGN_ENTITY_ID END),
               MAX(CASE WHEN DEM.TERMINAL_ID = pn_terminal_id THEN DEM.DATA_EXCHANGE_MAPPING_ID END)
          INTO lv_cust_foreign_entity_id, ln_cust_dem_id, lv_term_foreign_entity_id, ln_term_dem_id
          FROM REPORT.DATA_EXCHANGE_MAPPING DEM 
         WHERE DEM.DATA_EXCHANGE_TYPE_ID = pn_data_exchange_type_id
           AND DEM.CUSTOMER_ID = pn_customer_id;
        IF pv_foreign_entity_id = lv_cust_foreign_entity_id THEN
            IF lv_term_foreign_entity_id IS NOT NULL THEN
                DELETE
                  FROM REPORT.DATA_EXCHANGE_MAPPING 
                 WHERE DATA_EXCHANGE_MAPPING_ID = ln_term_dem_id;
            END IF;
            RETURN ln_cust_dem_id;
        ELSE
            IF lv_term_foreign_entity_id IS NULL THEN
                SELECT REPORT.SEQ_DATA_EXCHANGE_MAPPING_ID.NEXTVAL
                  INTO ln_term_dem_id
                  FROM DUAL;
                INSERT INTO REPORT.DATA_EXCHANGE_MAPPING(DATA_EXCHANGE_MAPPING_ID, DATA_EXCHANGE_TYPE_ID, CUSTOMER_ID, TERMINAL_ID, FOREIGN_ENTITY_ID)
                    VALUES(ln_term_dem_id, pn_data_exchange_type_id, pn_customer_id, pn_terminal_id, pv_foreign_entity_id); 
                RETURN ln_term_dem_id;
            ELSIF pv_foreign_entity_id = lv_term_foreign_entity_id THEN
                RETURN ln_term_dem_id;
            ELSE 
                UPDATE REPORT.DATA_EXCHANGE_MAPPING
                   SET FOREIGN_ENTITY_ID = pv_foreign_entity_id
                 WHERE DATA_EXCHANGE_MAPPING_ID = ln_term_dem_id;
                RETURN ln_term_dem_id;
            END IF;            
        END IF;
    END;
BEGIN
    SELECT DISTINCT DEF.DATA_EXCHANGE_TYPE_ID, E.EPORT_ID, 
           T.TERMINAL_ID, T.LOCATION_ID, T.CUSTOMER_ID, CBT.CUSTOMER_BANK_ID,
           D.DEVICE_TYPE_ID, D.DEVICE_SUB_TYPE_ID, DS_APP.DEVICE_SETTING_VALUE,
           CASE WHEN D.DEVICE_ID IS NULL THEN 'Y' ELSE 'N' END,
           FIRST_VALUE(DEM.DATA_EXCHANGE_MAPPING_ID) OVER (PARTITION BY T.TERMINAL_ID ORDER BY DEM.TERMINAL_ID ASC NULLS LAST),
           FIRST_VALUE(DEM.FOREIGN_ENTITY_ID) OVER (PARTITION BY T.TERMINAL_ID ORDER BY DEM.TERMINAL_ID ASC NULLS LAST)
      INTO ln_data_exchange_type_id, ln_eport_id, 
           ln_terminal_id, ln_location_id, ln_curr_customer_id, ln_curr_bank_acct_id, 
           ln_device_type_id, ln_device_sub_type_id, lv_app_type, 
           lc_initialize_flag, ln_data_exchange_mapping_id, lv_curr_foreign_entity_id
      FROM REPORT.DATA_EXCHANGE_FILE DEF
      LEFT JOIN REPORT.EPORT E ON E.EPORT_SERIAL_NUM = pv_device_serial_cd
      LEFT JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID AND NVL(TE.START_DATE, MIN_DATE) <= pd_timestamp AND NVL(TE.END_DATE, MAX_DATE) > pd_timestamp
      LEFT JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
      LEFT JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON T.TERMINAL_ID = CBT.TERMINAL_ID AND NVL(CBT.START_DATE, MIN_DATE) <= pd_timestamp AND NVL(CBT.END_DATE, MAX_DATE) > pd_timestamp
      LEFT JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON DLA.DEVICE_SERIAL_CD = pv_device_serial_cd      
      LEFT JOIN DEVICE.DEVICE D ON D.DEVICE_ID = DLA.DEVICE_ID
      LEFT JOIN DEVICE.DEVICE_SETTING DS_APP ON DS_APP.DEVICE_ID = DLA.DEVICE_ID AND DS_APP.DEVICE_SETTING_PARAMETER_CD = 'App Type'
      LEFT JOIN REPORT.DATA_EXCHANGE_MAPPING DEM ON DEF.DATA_EXCHANGE_TYPE_ID = DEM.DATA_EXCHANGE_TYPE_ID AND DEM.CUSTOMER_ID = T.CUSTOMER_ID AND (DEM.TERMINAL_ID IS NULL OR DEM.TERMINAL_ID = T.TERMINAL_ID)
     WHERE DATA_EXCHANGE_FILE_ID = pn_data_exchange_file_id;
    
    IF ln_device_type_id IS NULL THEN
        IF pc_status_cd IN('A', 'F') THEN
            SELECT MAX(DEVICE_TYPE_ID), MAX(DEVICE_SUB_TYPE_ID), MAX(POS_PTA_TMPL_ID)
              INTO ln_device_type_id, ln_device_sub_type_id, ln_pos_pta_tmpl_id
              FROM (
             SELECT DT.DEVICE_TYPE_ID, DST.DEVICE_SUB_TYPE_ID, COALESCE(DST.POS_PTA_TMPL_ID, DT.POS_PTA_TMPL_ID) POS_PTA_TMPL_ID
               FROM DEVICE.DEVICE_TYPE DT
             LEFT JOIN DEVICE.DEVICE_SUB_TYPE DST ON DT.DEVICE_TYPE_ID = DST.DEVICE_TYPE_ID AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
             WHERE REGEXP_LIKE(pv_device_serial_cd, DEVICE_TYPE_SERIAL_CD_REGEX)
             ORDER BY DST.DEVICE_SUB_TYPE_ID DESC, DT.DEVICE_TYPE_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_device_type_id IS NULL THEN
                pv_error_message := 'Device Serial Cd does not match any device type';
                pc_process_status_cd := 'E';
                GOTO DONE;
            END IF;
        ELSIF pc_status_cd = 'D' THEN
            pv_error_message := 'Device is not initialized';
            pc_process_status_cd := 'S';
            GOTO DONE;
        ELSE
            pv_error_message := 'Device is not initialized';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;
    END IF;
    
    IF lv_app_type IS NULL AND ln_device_sub_type_id IS NOT NULL THEN
        SELECT MAX(APP_TYPE_VALUE), NVL(MAX(POS_PTA_TMPL_ID), ln_pos_pta_tmpl_id), MAX(CREDENTIAL_ID)
          INTO lv_app_type, ln_pos_pta_tmpl_id, ln_credential_id
          FROM (SELECT APP_TYPE_VALUE, APP_TYPE_ID, POS_PTA_TMPL_ID, CREDENTIAL_ID
          FROM DEVICE.APP_TYPE
         WHERE DEVICE_TYPE_ID = ln_device_type_id
           AND DEVICE_SUB_TYPE_ID = ln_device_sub_type_id
           AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
         ORDER BY APP_TYPE_ID DESC)
         WHERE ROWNUM = 1;
    END IF;
    
    IF ln_eport_id IS NULL THEN
        IF pc_status_cd IN('A', 'F') THEN
            ln_eport_id := REPORT.DATA_IN_PKG.GET_OR_CREATE_DEVICE(pv_device_serial_cd, 'DE_' || ln_data_exchange_type_id, ln_device_type_id);
        ELSIF pc_status_cd = 'D' THEN
            pv_error_message := 'Device is not registered';
            pc_process_status_cd := 'S';
            GOTO DONE;
        ELSE
            pv_error_message := 'Device is not registered';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;
    END IF;
    pt_actions_taken := VARCHAR2_TABLE();
    
    -- Lock on item for concurrence concerns
    lv_lock := GLOBALS_PKG.REQUEST_LOCK('INBOUND_DATA_EXCHANGE:' || cn_data_exchange_item_type_id, ln_eport_id);
    -- Check timestamp
    SELECT NVL(MAX(DEI.DATA_EXCHANGE_ITEM_TS), MIN_DATE)
      INTO ld_latest_timestamp
      FROM REPORT.DATA_EXCHANGE_ITEM DEI
      JOIN REPORT.DATA_EXCHANGE_FILE DEF ON DEI.DATA_EXCHANGE_FILE_ID = DEF.DATA_EXCHANGE_FILE_ID
     WHERE DEI.DATA_EXCHANGE_ITEM_TYPE_ID = cn_data_exchange_item_type_id
       AND DEI.ENTITY_ID = ln_eport_id
       AND DEF.DATA_EXCHANGE_DIRECTION = 'I'
       AND DEI.PROCESS_STATUS_CD != 'E';
    IF ld_latest_timestamp >= pd_timestamp THEN
        pv_error_message := 'Data is stale';
        pc_process_status_cd := 'S';
        GOTO DONE;
    END IF;
    
    -- Keep from creating REPORT.DATA_EXCHANGE_ITEM while doing this
    DBADMIN.PKG_GLOBAL.SET_TRANSACTION_VARIABLE('SOURCE_DATA_EXCHANGE_TYPE_ID', ln_data_exchange_type_id);
    IF pc_status_cd = 'D' THEN
        -- Do deactive if timestamp is most recent and device is mapped to foreign entity
        IF ln_terminal_id IS NULL THEN
            pv_error_message := 'Device is already deactivated';
            pc_process_status_cd := 'S';
        ELSIF lv_curr_foreign_entity_id IS NULL THEN
            pv_error_message := 'Device is not mapped to external entity';
            pc_process_status_cd := 'E';
        ELSE
            UPDATE REPORT.TERMINAL_EPORT TE
               SET END_DATE = pd_timestamp + (1/24/60/60)
             WHERE TERMINAL_ID = ln_terminal_id
               AND EPORT_ID = ln_eport_id
               AND NVL(START_DATE, MIN_DATE) <= pd_timestamp 
               AND NVL(END_DATE, MAX_DATE) > pd_timestamp;
            IF SQL%ROWCOUNT = 0 THEN
                pv_error_message := 'Device is already deactivated';
                pc_process_status_cd := 'S';
            ELSE
                -- End Fees accordingly
                UPDATE CORP.SERVICE_FEES SF
                   SET END_DATE = SYSDATE
                 WHERE TERMINAL_ID = ln_terminal_id
                   AND END_DATE IS NULL
                   AND FEE_ID IN(
                       SELECT FEE_ID 
                         FROM CORP.FEES
                        WHERE INITIATION_TYPE_CD IN('T', 'R', 'I', 'P', 'A'));
            END IF; 
            -- should we remove the DATA_EXCHANGE_MAPPING for this TERMINAL_ID?, for now we won't
            pt_actions_taken.EXTEND;
            pt_actions_taken(pt_actions_taken.LAST) := 'Deactivated device'; 
            pc_process_status_cd := 'D';
        END IF;
    ELSIF pc_status_cd IN('A', 'F') THEN
        -- Initialize device, Activate device, assign to customer, set location info, set bank info, setup mapping
        IF pc_status_cd = 'A' THEN 
            IF ln_terminal_id IS NOT NULL THEN
                pv_error_message := 'Device is already activated';
                IF ln_curr_bank_acct_id = pn_bank_acct_id THEN
                   pc_process_status_cd := 'S';
                ELSE
                   pc_process_status_cd := 'E';
                END IF;
                GOTO DONE;
            ELSIF pv_location_name IS NULL THEN
                pv_error_message := 'Location Name was not provided';
                pc_process_status_cd := 'E';
                GOTO DONE;
            ELSIF pv_postal_cd IS NULL THEN
                pv_error_message := 'Postal was not provided';
                pc_process_status_cd := 'E';
                GOTO DONE;
            ELSIF pn_bank_acct_id IS NULL THEN
                pv_error_message := 'Bank Account Id was not provided';
                pc_process_status_cd := 'E';
                GOTO DONE;
            END IF;
        END IF;
        IF pv_postal_cd IS NOT NULL OR pv_city IS NOT NULL OR pv_state IS NOT NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(COUNTRY_CD), NVL(MAX(TIME_ZONE_ID), 0)
              INTO lv_city, lv_confirmed_state, lv_country_cd, ln_time_zone_id
              FROM (
            SELECT NVL(P.CITY, pv_city) CITY, RS.STATE_CD, RS.COUNTRY_CD, RTZ.TIME_ZONE_ID
              FROM DUAL
              LEFT JOIN FRONT.POSTAL P ON UPPER(P.POSTAL_CD) = UPPER(pv_postal_cd)
              LEFT JOIN CORP.STATE S ON UPPER(S.STATE_CD) = UPPER(pv_state)
              LEFT JOIN CORP.STATE RS ON RS.STATE_ID IN(P.STATE_ID, S.STATE_ID)
              LEFT JOIN FRONT.TIME_ZONE FTZ ON P.TIME_ZONE_ID = FTZ.TIME_ZONE_ID
              LEFT JOIN REPORT.TIME_ZONE RTZ ON FTZ.STANDARD_ABBR = RTZ.ABBREV
             ORDER BY CASE WHEN S.STATE_ID = P.STATE_ID THEN 1 WHEN RS.STATE_ID = P.STATE_ID THEN 2 WHEN RS.COUNTRY_CD = 'US' THEN 3 ELSE 4 END)
             WHERE ROWNUM = 1;
            IF lv_confirmed_state IS NULL THEN
                pv_error_message := 'Invalid Address (State and Postal)';
                pc_process_status_cd := 'E';
                GOTO DONE;
            END IF;
        END IF;
        SELECT CUSTOMER_ID
          INTO ln_ba_customer_id
          FROM CORP.CUSTOMER_BANK
         WHERE CUSTOMER_BANK_ID = pn_bank_acct_id;
        IF ln_curr_customer_id IS NULL THEN
            IF pn_customer_id IS NOT NULL AND ln_ba_customer_id != pn_customer_id THEN
                pv_error_message := 'USAT Customer Id does not match Bank Account Id';
                pc_process_status_cd := 'E';
                GOTO DONE;
            END IF;
        ELSIF ln_ba_customer_id != ln_curr_customer_id THEN
            pv_error_message := 'Bank Account Id belongs to a different customer';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;
        IF lc_initialize_flag = 'Y' THEN
            DECLARE
                ln_device_id NUMBER(20,0); --DEVICE.DEVICE_ID%TYPE;
                lv_device_name VARCHAR2(60); --DEVICE.DEVICE_NAME%TYPE;
                ln_pos_id PSS.POS.POS_ID%TYPE;
                ln_result_cd NUMBER; 
            BEGIN
                -- Copied relevant parts from PKG_DEVICE_CONFIGURATION.INITIALIZE_DEVICE
                SELECT SEQ_DEVICE_ID.NEXTVAL, SF_GENERATE_DEVICE_NAME(ln_device_type_id, ln_device_sub_type_id), PSS.SEQ_POS_ID.NEXTVAL
                  INTO ln_device_id, lv_device_name, ln_pos_id
                  FROM DUAL;
                INSERT INTO DEVICE.DEVICE(DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, DEVICE_SERIAL_CD, DEVICE_ACTIVE_YN_FLAG, CREDENTIAL_ID)
                    VALUES(ln_device_id, lv_device_name, ln_device_type_id, ln_device_sub_type_id, pv_device_serial_cd, 'Y', ln_credential_id);
                INSERT INTO PSS.POS(POS_ID, DEVICE_ID) 
                    VALUES(ln_pos_id, ln_device_id);
                IF lv_app_type IS NOT NULL THEN 
                    INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
                        VALUES(ln_device_id, 'App Type', lv_app_type); 
                    PKG_DEVICE_CONFIGURATION.ADD_DEFAULT_SETTINGS(ln_device_id, ln_device_type_id, ln_device_sub_type_id, lv_app_type, NULL);
                END IF;
                PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(ln_device_id, ln_new_host_count, ln_result_cd, pv_error_message);
                IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                    pc_process_status_cd := 'E';
                    GOTO DONE;
                END IF;
                PSS.PKG_POS_PTA.SP_IMPORT_POS_PTA_TEMPLATE(ln_device_id, ln_pos_pta_tmpl_id, 'S', 'AE', 'N', ln_result_cd, pv_error_message);
                IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                    pc_process_status_cd := 'E';
                    GOTO DONE;
                END IF;
                -- Assume that device is a Kiosk and don't bother to call PKG_DEVICE_CONFIGURATION.SP_INITIALIZE_CONFIG_FILE
            END;
            pt_actions_taken.EXTEND;
            pt_actions_taken(pt_actions_taken.LAST) := 'Initialized Device';
        END IF;
        IF ln_terminal_id IS NULL THEN
            SELECT MAX(LICENSE_ID)
              INTO ln_license_id
              FROM (
                SELECT L.LICENSE_ID
                  FROM CORP.CUSTOMER_LICENSE CL
                  JOIN CORP.LICENSE_NBR LN ON CL.LICENSE_NBR = LN.LICENSE_NBR 
                  JOIN CORP.LICENSE L ON LN.LICENSE_ID = L.LICENSE_ID
                 WHERE CL.CUSTOMER_ID = ln_ba_customer_id 
                ORDER BY CL.RECEIVED DESC)
              WHERE ROWNUM = 1;
            IF ln_license_id IS NULL THEN
                pv_error_message := 'Customer is not signed up';
                pc_process_status_cd := 'E';
                GOTO DONE;
            END IF;
            SELECT MAX(DEALER_ID)
              INTO ln_dealer_id
              FROM CORP.DEALER_LICENSE DL
             WHERE DL.LICENSE_ID = ln_license_id
               AND SYSDATE >= NVL(DL.START_DATE, MIN_DATE)
               AND SYSDATE < NVL(DL.END_DATE, MAX_DATE);
            SELECT MAX(DEALER_ID)
              INTO ln_dealer_id
              FROM (
            SELECT DISTINCT DLO.DEALER_ID, FIRST_VALUE(DLO.LICENSE_ID) OVER(PARTITION BY DLO.DEALER_ID ORDER BY DLO.START_DATE DESC, DLO.LICENSE_ID DESC) LICENSE_ID
              FROM CORP.DEALER_LICENSE DL
              JOIN CORP.DEALER_LICENSE DLO ON DL.DEALER_ID = DLO.DEALER_ID
             WHERE DL.LICENSE_ID = ln_license_id
               AND SYSDATE >= NVL(DL.START_DATE, MIN_DATE)
               AND SYSDATE < NVL(DL.END_DATE, MAX_DATE)
               AND SYSDATE >= NVL(DLO.START_DATE, MIN_DATE)
               AND SYSDATE < NVL(DLO.END_DATE, MAX_DATE))
             WHERE LICENSE_ID = ln_license_id;
            IF ln_dealer_id IS NULL THEN
                SELECT CORP.DEALER_SEQ.NEXTVAL
                  INTO ln_dealer_id
                  FROM DUAL;
                INSERT INTO CORP.DEALER(DEALER_ID, DEALER_NAME)
                    SELECT ln_dealer_id, SUBSTR('Dealer For ' || TITLE, 1, 50)
                      FROM CORP.LICENSE
                     WHERE LICENSE_ID = ln_license_id;
                INSERT INTO CORP.DEALER_LICENSE(DEALER_ID, LICENSE_ID)
                    VALUES(ln_dealer_id, ln_license_id);
                pt_actions_taken.EXTEND;
                pt_actions_taken(pt_actions_taken.LAST) := 'Created Dealer';
            END IF;
            INSERT INTO CORP.DEALER_EPORT(DEALER_ID, EPORT_ID)
                SELECT ln_dealer_id, ln_eport_id
                  FROM DUAL
                 WHERE NOT EXISTS(SELECT 1 FROM CORP.DEALER_EPORT WHERE DEALER_ID = ln_dealer_id AND EPORT_ID = ln_eport_id);
            IF SQL%ROWCOUNT > 0 THEN
                pt_actions_taken.EXTEND;
                pt_actions_taken(pt_actions_taken.LAST) := 'Registered Device';            
            END IF; 
            IF lv_app_type IS NOT NULL THEN
                lv_machine_make := INITCAP(REPLACE(lv_app_type, '-', ' '));
            END IF;
            REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_TERMINAL_MASS(
                0, --pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
                ln_terminal_id,
                pv_device_serial_cd,
                ln_dealer_id,
                NULL, --pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
                lv_machine_make,
                lv_machine_model,
                NULL, --pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
                NULL, --pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
                NULL, --pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
                pv_location_name,
                NULL, --pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
                NULL, --pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
                lv_city,
                lv_confirmed_state,
                pv_postal_cd,
                lv_country_cd,
                pn_bank_acct_id,
                NULL, --pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
                NULL, --pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
                NULL, --pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
                '- Not Specified -', --pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
                NULL, --pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
                '?', --pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
                0, --pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
                NVL(ln_time_zone_id, 0),
                '?', --pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
                '?', --pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
                0, --pn_vends REPORT.TERMINAL.VENDS%TYPE,
                NULL, --pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
                NULL, --pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
                NULL, --pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
                NULL, --pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
                NULL, --pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
                NULL, --pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
                NULL, --pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
                NULL, --pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
                pd_timestamp);
            IF lv_app_type IS NOT NULL THEN
                UPDATE REPORT.TERMINAL
                  SET BUSINESS_TYPE_ID = NVL((SELECT BUSINESS_TYPE_ID FROM DEVICE.APP_TYPE WHERE APP_TYPE_VALUE = lv_app_type), BUSINESS_TYPE_ID)
                WHERE TERMINAL_ID = ln_terminal_id;
            END IF;
            pt_actions_taken.EXTEND;
            pt_actions_taken(pt_actions_taken.LAST) := 'Activated Device'; 
            ln_curr_customer_id := ln_ba_customer_id;
        ELSE
            -- Do updates
            IF pv_location_name IS NOT NULL THEN
                UPDATE REPORT.LOCATION
                   SET LOCATION_NAME = pv_location_name
                 WHERE LOCATION_ID = ln_location_id
                   AND (LOCATION_NAME IS NULL OR LOCATION_NAME != pv_location_name);
                IF SQL%ROWCOUNT > 0 THEN
                    pt_actions_taken.EXTEND;
                    pt_actions_taken(pt_actions_taken.LAST) := 'Updated Location Name';            
                END IF;                       
            END IF;
            IF pv_postal_cd IS NOT NULL OR pv_city IS NOT NULL OR pv_state IS NOT NULL THEN
                UPDATE REPORT.TERMINAL_ADDR TA
                   SET ZIP = NVL(pv_postal_cd, TA.ZIP),
                       CITY = NVL(lv_city, TA.CITY),
                       STATE = lv_confirmed_state,
                       COUNTRY_CD = lv_country_cd
                 WHERE ADDRESS_ID IN(SELECT ADDRESS_ID FROM REPORT.LOCATION WHERE LOCATION_ID = ln_location_id)
                   AND ((ZIP IS NULL AND pv_postal_cd IS NOT NULL) 
                    OR ZIP != pv_postal_cd
                    OR (CITY IS NULL AND lv_city IS NOT NULL)
                    OR CITY != lv_city
                    OR (STATE IS NULL AND lv_confirmed_state IS NOT NULL)
                    OR STATE != lv_confirmed_state
                    OR (COUNTRY_CD IS NULL AND lv_country_cd IS NOT NULL)
                    OR COUNTRY_CD != lv_country_cd);
                IF SQL%ROWCOUNT > 0 THEN
                    pt_actions_taken.EXTEND;
                    pt_actions_taken(pt_actions_taken.LAST) := 'Updated Address';            
                END IF;
            END IF;
            -- ln_curr_customer_id IS NOT NULL at this point
            IF pn_bank_acct_id IS NOT NULL AND pn_bank_acct_id != ln_curr_bank_acct_id THEN
                IF ln_ba_customer_id != ln_curr_customer_id THEN
                    pv_error_message := 'Bank Account Id belongs to a different customer';
                    pc_process_status_cd := 'E';
                    GOTO DONE;
                END IF;
                UPDATE CORP.CUSTOMER_BANK_TERMINAL
                   SET END_DATE = pd_timestamp
                 WHERE TERMINAL_ID = ln_terminal_id
                   AND NVL(START_DATE, MIN_DATE) <= pd_timestamp
                   AND NVL(END_DATE, MAX_DATE) > pd_timestamp;
                INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_ID, TERMINAL_ID, START_DATE, END_DATE)
                    SELECT pn_bank_acct_id, ln_terminal_id, 
                           MIN(CASE WHEN CBT_B.END_DATE < pd_timestamp THEN CBT_B.END_DATE ELSE pd_timestamp END), 
                           MAX(CBT_A.START_DATE)
                      FROM DUAL
                      LEFT JOIN CORP.CUSTOMER_BANK_TERMINAL CBT_B ON CBT_B.TERMINAL_ID = ln_terminal_id AND NVL(CBT_B.START_DATE, MIN_DATE) <= pd_timestamp
                      LEFT JOIN CORP.CUSTOMER_BANK_TERMINAL CBT_A ON CBT_A.TERMINAL_ID = ln_terminal_id AND CBT_A.START_DATE > pd_timestamp;
                IF SQL%ROWCOUNT > 0 THEN
                    pt_actions_taken.EXTEND;
                    pt_actions_taken(pt_actions_taken.LAST) := 'Updated Bank Account';            
                END IF;
            END IF;
        END IF;
        IF pv_foreign_entity_id IS NOT NULL AND (lv_curr_foreign_entity_id IS NULL OR lv_curr_foreign_entity_id != pv_foreign_entity_id) THEN
            ln_data_exchange_mapping_id := UPDATE_DATA_EXCHANGE_MAPPING(ln_data_exchange_type_id, ln_curr_customer_id, ln_terminal_id, pv_foreign_entity_id);
            pt_actions_taken.EXTEND;
            IF lv_curr_foreign_entity_id IS NULL THEN
                pt_actions_taken(pt_actions_taken.LAST) := 'Added Data Exchange Mapping';            
            ELSE
                pt_actions_taken(pt_actions_taken.LAST) := 'Updated Data Exchange Mapping';  
            END IF;
        END IF;   
        pc_process_status_cd := 'D'; 
    ELSIF pc_status_cd = 'U' THEN
        IF ln_terminal_id IS NULL THEN
            pv_error_message := 'Device is not activated';
            pc_process_status_cd := 'E';
        ELSE
            -- set location info, set bank info, update mapping
            IF pv_location_name IS NOT NULL THEN
                UPDATE REPORT.LOCATION
                   SET LOCATION_NAME = pv_location_name
                 WHERE LOCATION_ID = ln_location_id
                   AND (LOCATION_NAME IS NULL OR LOCATION_NAME != pv_location_name);
                IF SQL%ROWCOUNT > 0 THEN
                    pt_actions_taken.EXTEND;
                    pt_actions_taken(pt_actions_taken.LAST) := 'Updated Location Name';            
                END IF;
            END IF;
            IF pv_postal_cd IS NOT NULL OR pv_city IS NOT NULL OR pv_state IS NOT NULL THEN
                SELECT MAX(CITY), MAX(STATE_CD), MAX(COUNTRY_CD), MAX(TIME_ZONE_ID)
                  INTO lv_city, lv_confirmed_state, lv_country_cd, ln_time_zone_id
                  FROM (
                SELECT NVL(P.CITY, pv_city) CITY, RS.STATE_CD, RS.COUNTRY_CD, RTZ.TIME_ZONE_ID
                  FROM DUAL
                  LEFT JOIN FRONT.POSTAL P ON UPPER(P.POSTAL_CD) = UPPER(pv_postal_cd)
                  LEFT JOIN CORP.STATE S ON UPPER(S.STATE_CD) = UPPER(pv_state)
                  LEFT JOIN CORP.STATE RS ON RS.STATE_ID IN(P.STATE_ID, S.STATE_ID)
                  LEFT JOIN FRONT.TIME_ZONE FTZ ON P.TIME_ZONE_ID = FTZ.TIME_ZONE_ID
                  LEFT JOIN REPORT.TIME_ZONE RTZ ON FTZ.STANDARD_ABBR = RTZ.ABBREV
                 ORDER BY CASE WHEN S.STATE_ID = P.STATE_ID THEN 1 WHEN RS.STATE_ID = P.STATE_ID THEN 2 WHEN RS.COUNTRY_CD = 'US' THEN 3 ELSE 4 END)
                 WHERE ROWNUM = 1;
                IF lv_confirmed_state IS NULL THEN
                    pv_error_message := 'Invalid Address (State and Postal)';
                    pc_process_status_cd := 'E';
                    GOTO DONE;
                ELSE
                    UPDATE REPORT.TERMINAL_ADDR TA
                       SET ZIP = NVL(pv_postal_cd, TA.ZIP),
                           CITY = NVL(lv_city, TA.CITY),
                           STATE = lv_confirmed_state,
                           COUNTRY_CD = lv_country_cd
                     WHERE ADDRESS_ID IN(SELECT ADDRESS_ID FROM REPORT.LOCATION WHERE LOCATION_ID = ln_location_id)
                       AND ((ZIP IS NULL AND pv_postal_cd IS NOT NULL) 
                        OR ZIP != pv_postal_cd
                        OR (CITY IS NULL AND lv_city IS NOT NULL)
                        OR CITY != lv_city
                        OR (STATE IS NULL AND lv_confirmed_state IS NOT NULL)
                        OR STATE != lv_confirmed_state
                        OR (COUNTRY_CD IS NULL AND lv_country_cd IS NOT NULL)
                        OR COUNTRY_CD != lv_country_cd);
                    IF SQL%ROWCOUNT > 0 THEN
                        pt_actions_taken.EXTEND;
                        pt_actions_taken(pt_actions_taken.LAST) := 'Updated Address'; 
                        IF ln_time_zone_id IS NOT NULL THEN
                            UPDATE REPORT.TERMINAL
                               SET TIME_ZONE_ID = ln_time_zone_id
                             WHERE TERMINAL_ID = ln_terminal_id
                               AND (TIME_ZONE_ID IS NULL OR TIME_ZONE_ID != ln_time_zone_id);
                        END IF;
                    END IF;
                END IF;
            END IF;
            IF pn_bank_acct_id IS NOT NULL AND (ln_curr_bank_acct_id IS NULL OR pn_bank_acct_id != ln_curr_bank_acct_id) THEN
                SELECT CUSTOMER_ID
                  INTO ln_ba_customer_id
                  FROM CORP.CUSTOMER_BANK
                 WHERE CUSTOMER_BANK_ID = pn_bank_acct_id;
                IF ln_curr_customer_id IS NULL THEN
                    IF pn_customer_id IS NULL THEN
                        pv_error_message := 'USAT Customer Id was not provided';
                        pc_process_status_cd := 'E';
                        GOTO DONE;
                    ELSIF ln_ba_customer_id != pn_customer_id THEN
                        pv_error_message := 'USAT Customer Id does not match Bank Account Id';
                        pc_process_status_cd := 'E';
                        GOTO DONE;
                    END IF;
                ELSIF ln_ba_customer_id != ln_curr_customer_id THEN
                    pv_error_message := 'Bank Account Id belongs to a different customer';
                    pc_process_status_cd := 'E';
                    GOTO DONE;
                END IF;
                UPDATE CORP.CUSTOMER_BANK_TERMINAL
                   SET END_DATE = pd_timestamp
                 WHERE TERMINAL_ID = ln_terminal_id
                   AND NVL(START_DATE, MIN_DATE) <= pd_timestamp
                   AND NVL(END_DATE, MAX_DATE) > pd_timestamp;
                INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_ID, TERMINAL_ID, START_DATE, END_DATE)
                    SELECT pn_bank_acct_id, ln_terminal_id, 
                           MIN(CASE WHEN CBT_B.END_DATE < pd_timestamp THEN CBT_B.END_DATE ELSE pd_timestamp END), 
                           MAX(CBT_A.START_DATE)
                      FROM DUAL
                      LEFT JOIN CORP.CUSTOMER_BANK_TERMINAL CBT_B ON CBT_B.TERMINAL_ID = ln_terminal_id AND NVL(CBT_B.START_DATE, MIN_DATE) <= pd_timestamp
                      LEFT JOIN CORP.CUSTOMER_BANK_TERMINAL CBT_A ON CBT_A.TERMINAL_ID = ln_terminal_id AND CBT_A.START_DATE > pd_timestamp;
                IF SQL%ROWCOUNT > 0 THEN
                    pt_actions_taken.EXTEND;
                    pt_actions_taken(pt_actions_taken.LAST) := 'Updated Bank Account';            
                END IF;
            END IF;
            IF pv_foreign_entity_id IS NOT NULL AND (lv_curr_foreign_entity_id IS NULL OR lv_curr_foreign_entity_id != pv_foreign_entity_id) THEN
                ln_data_exchange_mapping_id := UPDATE_DATA_EXCHANGE_MAPPING(ln_data_exchange_type_id, ln_curr_customer_id, ln_terminal_id, pv_foreign_entity_id);
                pt_actions_taken.EXTEND;
                IF lv_curr_foreign_entity_id IS NULL THEN
                    pt_actions_taken(pt_actions_taken.LAST) := 'Added Data Exchange Mapping';            
                ELSE
                    pt_actions_taken(pt_actions_taken.LAST) := 'Updated Data Exchange Mapping';  
                END IF;
            END IF;        
            pc_process_status_cd := 'D';
        END IF;
    ELSE
        pv_error_message := 'Invalid Status code';
        pc_process_status_cd := 'E';
    END IF;
    <<DONE>>
    INSERT INTO REPORT.DATA_EXCHANGE_ITEM(
        DATA_EXCHANGE_MAPPING_ID,
        DATA_EXCHANGE_ITEM_TYPE_ID,
        DATA_EXCHANGE_ITEM_ACTION_CD,
        DATA_EXCHANGE_ITEM_TS,
        ENTITY_ID,
        ENTITY_KEY_CD,
        DATA_EXCHANGE_FILE_ID,
        PROCESS_STATUS_CD,
        PROCESS_START_TS,
        PROCESS_COMPLETE_TS,
        PROCESS_DETAIL,
        DATA_EXCHANGE_ITEM_SOURCE)
      SELECT
        ln_data_exchange_mapping_id,
        cn_data_exchange_item_type_id,
        pc_status_cd,
        pd_timestamp,
        ln_eport_id,
        NULL,
        pn_data_exchange_file_id,
        pc_process_status_cd,
        ld_start_date,
        SYSDATE,
        CASE WHEN pc_process_status_cd = 'D' THEN DBADMIN.ADHERE(pt_actions_taken, ', ') ELSE pv_error_message END,
        'PROCESS_DATA_EXCHANGE_DI'
        FROM DUAL;
END;
/