CREATE OR REPLACE PACKAGE BODY REPORT.SYNC_JOB_PKG IS
--
-- All the procedures for controlling the sync job. WARNING: if the sync job
-- is running and you try to compile or change this package it will hang forever
-- waiting for the sync job to finish. Use STOP_SYNC_JOB first. Then, make changes
-- and then restart the job using RUN_SYNC_JOB.
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B KRUG       10-18-04 NEW
-- B KRUG       03-01-05 Added check for invalidated packages

    INVALIDATED_PACKAGES EXCEPTION;
    PRAGMA EXCEPTION_INIT(INVALIDATED_PACKAGES, -4061);

    DEQUEUE_TIMEOUT EXCEPTION;
    PRAGMA EXCEPTION_INIT(DEQUEUE_TIMEOUT, -25228);

    c_filter_in_secondary CONSTANT VARCHAR2(4000) := 'TAB.USER_DATA.SQL_TO_RUN IS NULL OR '||
             'TAB.USER_DATA.SQL_TO_RUN LIKE ''CALL SYNC_PKG.RECEIVE_FILL_INSERT%'' OR ' ||
             'TAB.USER_DATA.SQL_TO_RUN LIKE ''CALL SYNC_PKG.RECEIVE_TERMINAL_INFO%''';
             
    c_filter_out_secondary CONSTANT VARCHAR2(4000) := 'TAB.USER_DATA.SQL_TO_RUN IS NULL OR ('||
             'TAB.USER_DATA.SQL_TO_RUN NOT LIKE ''CALL SYNC_PKG.RECEIVE_FILL_INSERT%'' AND ' ||
             'TAB.USER_DATA.SQL_TO_RUN NOT LIKE ''CALL SYNC_PKG.RECEIVE_TERMINAL_INFO%'')';
             
    PROCEDURE ENQUEUE_NULL(
        l_seq_dev BINARY_INTEGER DEFAULT NULL)
    IS
        l_msg_id RAW(32767);
        l_opts DBMS_AQ.ENQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
        l_msg T_SYNC_MSG;
    BEGIN
        l_opts.SEQUENCE_DEVIATION := l_seq_dev;
        DBMS_AQ.ENQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, l_msg, l_msg_id);
    END;

    FUNCTION DEQUEUE
     RETURN VARCHAR
    IS
        l_msg_id RAW(32767);
        l_payload T_SYNC_MSG;
        l_opts DBMS_AQ.DEQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
    BEGIN
        l_opts.deq_condition := c_filter_out_secondary;
        DBMS_AQ.DEQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, l_payload, l_msg_id);
        RETURN l_payload.sql_to_run;
    END;

    FUNCTION DEQUEUE_SECONDARY
     RETURN VARCHAR
    IS
        l_msg_id RAW(32767);
        l_payload T_SYNC_MSG;
        l_opts DBMS_AQ.DEQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
    BEGIN
        l_opts.deq_condition := c_filter_in_secondary;
        DBMS_AQ.DEQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, l_payload, l_msg_id);
        RETURN l_payload.sql_to_run;
    END;
    
    PROCEDURE PROCESS_SYNCS
    IS
        l_sql VARCHAR(8000);
    BEGIN
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Process is now running...');
        LOOP
            l_sql := DEQUEUE;
            EXIT WHEN l_sql IS NULL;
            BEGIN
                LOG_MSG('TRACE','SYNC_JOB','PROCESSING', 'Executing "'||l_sql||'"');
                EXECUTE IMMEDIATE l_sql;
                COMMIT;
                LOG_MSG('TRACE','SYNC_JOB','PROCESSING', 'Finished "'||l_sql||'"');
            EXCEPTION
                WHEN INVALIDATED_PACKAGES THEN
                    ROLLBACK;
                    LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Re-starting sync job because existing state of packages has been invalidated');
                    DECLARE
                        l_job_id BINARY_INTEGER;
                    BEGIN
                    	DBMS_JOB.SUBMIT (l_job_id, 'SYNC_JOB_PKG.PROCESS_SYNCS;');
                    	COMMIT;
                        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Job Re-started with id = '||TO_CHAR(l_job_id));
                        EXIT;
                    END;
                WHEN OTHERS THEN
                    DECLARE
                        l_msg MSG_LOG.MSG_TEXT%TYPE := SQLERRM;
                        l_err_num MSG_LOG.ERROR_NUM%TYPE := SQLCODE;
                    BEGIN
                        ROLLBACK;
                        LOG_MSG('ERROR','SYNC_JOB','PROCESSING', SUBSTR(l_msg || ' (While executing: "' || l_sql || '")',1, 4000), l_err_num);
                    END;
            END;
        END LOOP;
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Process has ended');
    END;

    PROCEDURE PROCESS_SECONDARY_SYNCS
    IS
        l_sql VARCHAR(8000);
    BEGIN
        LOG_MSG('INFO','SECONDARY_SYNC_JOB','JOB CONTROL', 'Process is now running...');
        LOOP
            l_sql := DEQUEUE_SECONDARY;
            EXIT WHEN l_sql IS NULL;
            BEGIN
                LOG_MSG('TRACE','SECONDARY_SYNC_JOB','PROCESSING', 'Executing "'||l_sql||'"');
                EXECUTE IMMEDIATE l_sql;
                COMMIT;
                LOG_MSG('TRACE','SECONDARY_SYNC_JOB','PROCESSING', 'Finished "'||l_sql||'"');
            EXCEPTION
                WHEN INVALIDATED_PACKAGES THEN
                    ROLLBACK;
                    LOG_MSG('INFO','SECONDARY_SYNC_JOB','JOB CONTROL', 'Re-starting secondary sync job because existing state of packages has been invalidated');
                    DECLARE
                        l_job_id BINARY_INTEGER;
                    BEGIN
                    	DBMS_JOB.SUBMIT (l_job_id, 'SYNC_JOB_PKG.PROCESS_SECONDARY_SYNCS;');
                    	COMMIT;
                        LOG_MSG('INFO','SECONDARY_SYNC_JOB','JOB CONTROL', 'Secondary Sync Job Re-started with id = '||TO_CHAR(l_job_id));
                        EXIT;
                    END;
                WHEN OTHERS THEN
                    DECLARE
                        l_msg MSG_LOG.MSG_TEXT%TYPE := SQLERRM;
                        l_err_num MSG_LOG.ERROR_NUM%TYPE := SQLCODE;
                    BEGIN
                        ROLLBACK;
                        LOG_MSG('ERROR','SECONDARY_SYNC_JOB','PROCESSING', SUBSTR(l_msg || ' (While executing: "' || l_sql || '")',1, 4000), l_err_num);
                    END;
            END;
        END LOOP;
        LOG_MSG('INFO','SECONDARY_SYNC_JOB','JOB CONTROL', 'Process has ended');
    END;
    
    -- Syncs ledger and batch from report's sync queue
    FUNCTION RUN_SYNC_JOB
     RETURN BINARY_INTEGER
    IS
        l_job_id BINARY_INTEGER;
    BEGIN
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Starting Job');
        SELECT MAX(JOB)
          INTO l_job_id
          FROM DBA_JOBS
         WHERE WHAT = 'SYNC_JOB_PKG.PROCESS_SYNCS;'
           AND BROKEN = 'N';
        IF l_job_id IS NOT NULL THEN
            LOG_MSG('ERROR','SYNC_JOB','JOB CONTROL', 'Job is already running with id = '||TO_CHAR(l_job_id));
        ELSE
        	DBMS_JOB.SUBMIT (l_job_id, 'SYNC_JOB_PKG.PROCESS_SYNCS;');
        	COMMIT;
            LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Job Started with id = '||TO_CHAR(l_job_id));
    	END IF;
        RETURN l_job_id;
    END;

    FUNCTION RUN_SECONDARY_SYNC_JOB
     RETURN BINARY_INTEGER
    IS
        l_job_id BINARY_INTEGER;
    BEGIN
        LOG_MSG('INFO','SECONDARY_SYNC_JOB','JOB CONTROL', 'Starting Job');
        SELECT MAX(JOB)
          INTO l_job_id
          FROM DBA_JOBS
         WHERE WHAT = 'SYNC_JOB_PKG.PROCESS_SECONDARY_SYNCS;'
           AND BROKEN = 'N';
        IF l_job_id IS NOT NULL THEN
            LOG_MSG('ERROR','SECONDARY_SYNC_JOB','JOB CONTROL', 'Job is already running with id = '||TO_CHAR(l_job_id));
        ELSE
        	DBMS_JOB.SUBMIT (l_job_id, 'SYNC_JOB_PKG.PROCESS_SECONDARY_SYNCS;');
        	COMMIT;
            LOG_MSG('INFO','SECONDARY_SYNC_JOB','JOB CONTROL', 'Job Started with id = '||TO_CHAR(l_job_id));
    	END IF;
        RETURN l_job_id;
    END;
    
    PROCEDURE STOP_SYNC_JOB
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
    	ENQUEUE_NULL();
    	COMMIT;
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Enqueued NULL to stop the Job');
    END;
    
    PROCEDURE STOP_SYNC_JOB_IMMEDIATELY
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        ENQUEUE_NULL(DBMS_AQ.TOP);
    	COMMIT;
    	LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Enqueued NULL at top of queue to stop the Job right away');
    END;

    FUNCTION DEQUEUE_NO_WAIT
     RETURN VARCHAR
    IS
        l_msg_id RAW(32767);
        l_payload T_SYNC_MSG;
        l_opts DBMS_AQ.DEQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
    BEGIN
        l_opts.wait := DBMS_AQ.NO_WAIT;
        DBMS_AQ.DEQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, l_payload, l_msg_id);
        RETURN l_payload.sql_to_run;
    END;

    PROCEDURE RUN_THRU_SYNCS
    IS
        l_sql VARCHAR(8000);
    BEGIN
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Running through available sync messages...');
        LOOP
            l_sql := DEQUEUE_NO_WAIT;
            EXIT WHEN l_sql IS NULL;
            BEGIN
                LOG_MSG('TRACE','SYNC_JOB','PROCESSING', 'Executing "'||l_sql||'"');
                EXECUTE IMMEDIATE l_sql;
                COMMIT;
                LOG_MSG('TRACE','SYNC_JOB','PROCESSING', 'Finished "'||l_sql||'"');
            EXCEPTION
                WHEN INVALIDATED_PACKAGES THEN
                    ROLLBACK;
                    RAISE;
                WHEN OTHERS THEN
                    DECLARE
                        l_msg MSG_LOG.MSG_TEXT%TYPE := SQLERRM;
                        l_err_num MSG_LOG.ERROR_NUM%TYPE := SQLCODE;
                    BEGIN
                        ROLLBACK;
                        LOG_MSG('ERROR','SYNC_JOB','PROCESSING', SUBSTR(l_msg || ' (While executing: "' || l_sql || '")',1, 4000), l_err_num);
                    END;
            END;
        END LOOP;
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Encountered null message; exitting procedure');
    EXCEPTION
        WHEN DEQUEUE_TIMEOUT THEN
            LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Finished running through available sync messages');
        WHEN OTHERS THEN
            RAISE;
    END;

END;
/
