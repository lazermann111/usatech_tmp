CREATE OR REPLACE TRIGGER REPORT.TRAU_CAMPAIGN AFTER 
UPDATE ON REPORT.CAMPAIGN FOR EACH ROW 
BEGIN
	IF COALESCE(:NEW.start_date, MIN_DATE) != COALESCE(:OLD.start_date, MIN_DATE) OR COALESCE(:NEW.end_date, MAX_DATE) != COALESCE(:OLD.end_date, MAX_DATE) THEN
		IF :NEW.start_date>=:OLD.start_date and :NEW.end_date<=:OLD.start_date THEN
			UPDATE PSS.CAMPAIGN_POS_PTA
			SET start_date=greatest(start_date, NVL(:NEW.start_date, MIN_DATE)),
				end_date=least(end_date, NVL(:NEW.end_date, MAX_DATE))
			where campaign_id=:NEW.campaign_id;
		ELSE
	  		FOR l_cur in (select pt.pos_pta_id, NVL(te.START_DATE, MIN_DATE) l_start_date, NVL(te.END_DATE, MAX_DATE) l_end_date
				from report.terminal_eport te join report.eport e on te.eport_id=e.eport_id
				join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
				join pss.pos p on p.device_id=d.device_id
				join pss.pos_pta pt on p.pos_id=pt.pos_id
				where pt.pos_pta_id in (select pos_pta_id from PSS.CAMPAIGN_POS_PTA where campaign_id=:NEW.campaign_id)
					and  SYSDATE >= NVL(te.START_DATE, MIN_DATE) and SYSDATE    < NVL(te.END_DATE, MAX_DATE)) LOOP
			UPDATE PSS.CAMPAIGN_POS_PTA
			SET start_date=greatest(l_cur.l_start_date, NVL(:NEW.start_date, MIN_DATE)),
			end_date=least(l_cur.l_end_date, NVL(:NEW.end_date, MAX_DATE))
			where campaign_id=:NEW.campaign_id and pos_pta_id=l_cur.pos_pta_id;
			END LOOP;
		END IF;
	END IF;
END; 
/
