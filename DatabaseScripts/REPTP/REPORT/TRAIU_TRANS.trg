CREATE OR REPLACE TRIGGER report.traiu_trans
AFTER INSERT OR UPDATE 
OF
    tran_id
  , eport_id
  , close_date
  , currency_id
  , orig_tran_id
  , total_amount
  , trans_type_id
ON report.trans
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SYNC_PKG.SYNC_TRANS_INSERT(:NEW.TRAN_ID);
END;
/