CREATE OR REPLACE FUNCTION REPORT.GET_DEVICE_SIM_OR_MEID ( l_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
    l_device_type_id IN REPORT.EPORT.DEVICE_TYPE_ID%TYPE)
  RETURN VARCHAR2 IS
    l_sim_meid VARCHAR2(255);
    l_comm_method_cd CHAR(1);
BEGIN 
  IF l_device_type_id = 6 THEN -- for MEI
    select device_setting_value into l_sim_meid from device.device_setting ds join device.device d on ds.device_id=d.device_id and d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y' where device_setting_parameter_cd='28' and REGEXP_LIKE(device_setting_value, '^\d{20}$');
  ELSE
    select COMM_METHOD_CD into l_comm_method_cd from device.device d where d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y';
    IF l_comm_method_cd = 'C' THEN
      select h.host_serial_cd into l_sim_meid from device.host h join device.device d on h.device_id=d.device_id and d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y'
      and h.host_type_id=204;
    ELSIF l_comm_method_cd = 'G' THEN
       select h.host_serial_cd into l_sim_meid from device.host h join device.device d on h.device_id=d.device_id and d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y'
      and h.host_type_id=202;
    END IF;
  END IF;
  return l_sim_meid;
EXCEPTION
    when NO_DATA_FOUND then
     return null;
END;
/
