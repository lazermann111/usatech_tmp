CREATE OR REPLACE PROCEDURE REPORT.GET_OR_CREATE_REGION(
    pn_region_id OUT REPORT.REGION.REGION_ID%TYPE,
    pv_region_name REPORT.REGION.REGION_NAME%TYPE,
    pn_customer_id REPORT.REGION.CUSTOMER_ID%TYPE)
IS
BEGIN
    SELECT MIN(REGION_ID) 
      INTO pn_region_id 
      FROM REPORT.REGION 
     WHERE (REGION_NAME = TRIM(pv_region_name) OR (REGION_NAME IS NULL AND  TRIM(pv_region_name) IS NULL))
       AND CUSTOMER_ID = pn_customer_id;
    IF pn_region_id IS NULL THEN
        SELECT REGION_SEQ.NEXTVAL 
          INTO pn_region_id 
          FROM DUAL;
        BEGIN
            INSERT INTO REPORT.REGION(REGION_ID, REGION_NAME, CUSTOMER_ID)
                VALUES (pn_region_id, TRIM(pv_region_name), pn_customer_id);
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                GET_OR_CREATE_REGION(pn_region_id, pv_region_name, pn_customer_id);
        END;
    END IF;
END;
/    