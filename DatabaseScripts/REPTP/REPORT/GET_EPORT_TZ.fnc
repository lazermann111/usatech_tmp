CREATE OR REPLACE FUNCTION REPORT.GET_EPORT_TZ
 (
  p_EPORT_ID IN NUMBER
 ) RETURN VARCHAR2
IS
 l_rv VARCHAR2(4000);
 BEGIN
  select max(tz.time_zone_guid)
  into l_rv
  from
   report.time_zone tz
   join report.terminal term on tz.TIME_ZONE_ID = term.TIME_ZONE_ID
   join report.vw_terminal_eport vte on vte.TERMINAL_ID = term.TERMINAL_ID
   join report.eport ep on ep.eport_id = vte.eport_id
  where ep.eport_id=p_EPORT_ID;
  RETURN l_rv;
END;
/