CREATE OR REPLACE PROCEDURE REPORT.PROCESS_DATA_EXCHANGE_DS (
    pn_data_exchange_file_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_ID%TYPE,
    pv_device_serial_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
    pd_timestamp REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_TS%TYPE,
    pv_setting_name VARCHAR2, --DEVICE.DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
    pv_setting_value VARCHAR2, --DEVICE.DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
    pc_process_status_cd OUT REPORT.DATA_EXCHANGE_ITEM.PROCESS_STATUS_CD%TYPE,
    pt_actions_taken OUT VARCHAR2_TABLE,
    pv_error_message OUT REPORT.DATA_EXCHANGE_ITEM.PROCESS_DETAIL%TYPE)
IS
    ln_data_exchange_type_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_TYPE_ID%TYPE;
    ln_data_exchange_mapping_id REPORT.DATA_EXCHANGE_MAPPING.DATA_EXCHANGE_MAPPING_ID%TYPE;
    ld_start_date REPORT.DATA_EXCHANGE_ITEM.PROCESS_START_TS%TYPE := SYSDATE;
    lv_lock VARCHAR2(128);
    cn_data_exchange_item_type_id CONSTANT REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_TYPE_ID%TYPE := 4;
    ld_latest_timestamp REPORT.DATA_EXCHANGE_ITEM.DATA_EXCHANGE_ITEM_TS%TYPE;
    ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
    ln_device_id NUMBER(20,0); --DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id NUMBER(20,0); --DEVICE.DEVICE_TYPE_ID%TYPE;
    ln_device_sub_type_id NUMBER(3,0); --DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
    lv_app_type VARCHAR2(60); --DEVICE.APP_TYPE.APP_TYPE_VALUE%TYPE;
    ln_exists NUMBER;
    ln_cents NUMBER;
    lv_translated_value VARCHAR2(200);
    lv_secondary_value VARCHAR2(200);
    lv_time_zone_guid REPORT.TIME_ZONE.TIME_ZONE_GUID%TYPE;
    lv_prev_setting_value VARCHAR2(200);
    ln_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
    lv_vend_or_tran_flag VARCHAR2(1);
BEGIN
    SELECT DISTINCT DEF.DATA_EXCHANGE_TYPE_ID, E.EPORT_ID, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SUB_TYPE_ID, DS_APP.DEVICE_SETTING_VALUE,
           FIRST_VALUE(DEM.DATA_EXCHANGE_MAPPING_ID) OVER (PARTITION BY T.TERMINAL_ID ORDER BY DEM.TERMINAL_ID ASC NULLS LAST), T_TZ.TIME_ZONE_GUID,
           DS.DEVICE_SETTING_VALUE
      INTO ln_data_exchange_type_id, ln_eport_id, ln_device_id, ln_device_type_id, ln_device_sub_type_id, lv_app_type, 
           ln_data_exchange_mapping_id, lv_time_zone_guid, lv_prev_setting_value
      FROM REPORT.DATA_EXCHANGE_FILE DEF
      LEFT JOIN REPORT.EPORT E ON E.EPORT_SERIAL_NUM = pv_device_serial_cd
      LEFT JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID AND NVL(TE.START_DATE, MIN_DATE) <= pd_timestamp AND NVL(TE.END_DATE, MAX_DATE) > pd_timestamp
      LEFT JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
      LEFT JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
      LEFT JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON DLA.DEVICE_SERIAL_CD = pv_device_serial_cd      
      LEFT JOIN DEVICE.DEVICE D ON D.DEVICE_ID = DLA.DEVICE_ID
      LEFT JOIN DEVICE.DEVICE_SETTING DS_APP ON DS_APP.DEVICE_ID = DLA.DEVICE_ID AND DS_APP.DEVICE_SETTING_PARAMETER_CD = 'App Type'
      LEFT JOIN DEVICE.DEVICE_SETTING DS ON DS.DEVICE_ID = DLA.DEVICE_ID AND DS.DEVICE_SETTING_PARAMETER_CD = pv_setting_name
      LEFT JOIN REPORT.DATA_EXCHANGE_MAPPING DEM ON DEF.DATA_EXCHANGE_TYPE_ID = DEM.DATA_EXCHANGE_TYPE_ID AND DEM.CUSTOMER_ID = T.CUSTOMER_ID AND (DEM.TERMINAL_ID IS NULL OR DEM.TERMINAL_ID = T.TERMINAL_ID)
     WHERE DATA_EXCHANGE_FILE_ID = pn_data_exchange_file_id;
    
    IF ln_device_type_id IS NULL THEN
        pv_error_message := 'Device Serial Cd does not match any device type';
        pc_process_status_cd := 'E';
        GOTO DONE;
    ELSIF ln_data_exchange_mapping_id IS NULL THEN
        pv_error_message := 'Device does not have a data exchange mapping';
        pc_process_status_cd := 'S';
        GOTO DONE;
    ELSIF (lv_prev_setting_value IS NULL AND pv_setting_value IS NULL) OR lv_prev_setting_value = pv_setting_value THEN
        pv_error_message := 'Setting has not changed';
        pc_process_status_cd := 'S';
        GOTO DONE;
    END IF;
    
    -- Lock on item for concurrence concerns
    lv_lock := GLOBALS_PKG.REQUEST_LOCK('INBOUND_DATA_EXCHANGE:' || cn_data_exchange_item_type_id, ln_eport_id);
    -- Check timestamp
    SELECT NVL(MAX(DEI.DATA_EXCHANGE_ITEM_TS), MIN_DATE)
      INTO ld_latest_timestamp
      FROM REPORT.DATA_EXCHANGE_ITEM DEI
      JOIN REPORT.DATA_EXCHANGE_FILE DEF ON DEI.DATA_EXCHANGE_FILE_ID = DEF.DATA_EXCHANGE_FILE_ID
     WHERE DEI.DATA_EXCHANGE_ITEM_TYPE_ID = cn_data_exchange_item_type_id
       AND DEI.ENTITY_ID = ln_eport_id
       AND DEI.ENTITY_KEY_CD = pv_setting_name
       AND DEF.DATA_EXCHANGE_DIRECTION = 'I'
       AND DEI.PROCESS_STATUS_CD != 'E';
    IF ld_latest_timestamp >= pd_timestamp THEN
        pv_error_message := 'Data is stale';
        pc_process_status_cd := 'S';
        GOTO DONE;
    END IF;
    
    -- Keep from creating REPORT.DATA_EXCHANGE_ITEM while doing this
    DBADMIN.PKG_GLOBAL.SET_TRANSACTION_VARIABLE('SOURCE_DATA_EXCHANGE_TYPE_ID', ln_data_exchange_type_id);    
    pt_actions_taken := VARCHAR2_TABLE();
    IF pv_setting_name = 'Cash Enabled' THEN
        IF pv_setting_value NOT IN('Y', 'N') THEN
            pv_error_message := 'Invalid Setting Value for "' || pv_setting_name || '" - must be "Y" or "N"';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;    
        IF ln_device_type_id IN(0,1) THEN
            SELECT DECODE(pv_setting_value, 'Y', '55', '00')
              INTO lv_translated_value
              FROM DUAL;
            PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '203', lv_translated_value, ln_exists);
        ELSIF ln_device_type_id = 13 THEN
            SELECT DECODE(pv_setting_value, 'Y', 1, 0)
              INTO lv_translated_value
              FROM DUAL;
            PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '1022', lv_translated_value, ln_exists);
        END IF;
        pt_actions_taken := VARCHAR2_TABLE('Set Property "'|| pv_setting_name || '" to "' || pv_setting_value || '"');
        pc_process_status_cd := 'D';
    ELSIF pv_setting_name = 'Cashless Enabled' THEN
        IF pv_setting_value NOT IN('Y', 'N') THEN
            pv_error_message := 'Invalid Setting Value for "' || pv_setting_name || '" - must be "Y" or "N"';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;    
        -- add / remove cashless
        IF pv_setting_value = 'Y' THEN
            FOR l_rec IN (
                SELECT DISTINCT P.POS_ID, PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID,
                       FIRST_VALUE(PP.PREFERENCE) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) PREFERENCE,
                       FIRST_VALUE(PP.PAYMENT_SUBTYPE_ID) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) PAYMENT_SUBTYPE_ID,
                       FIRST_VALUE(PP.POS_PTA_ENCRYPT_KEY) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_ENCRYPT_KEY,
                       FIRST_VALUE(PP.POS_PTA_ENCRYPT_KEY2) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_ENCRYPT_KEY2,
                       FIRST_VALUE(PP.PAYMENT_SUBTYPE_KEY_ID) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) PAYMENT_SUBTYPE_KEY_ID,
                       FIRST_VALUE(PP.POS_PTA_DEVICE_SERIAL_CD) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_DEVICE_SERIAL_CD,
                       FIRST_VALUE(PP.POS_PTA_PIN_REQ_YN_FLAG) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_PIN_REQ_YN_FLAG,
                       FIRST_VALUE(PP.POS_PTA_REGEX) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_REGEX,
                       FIRST_VALUE(PP.POS_PTA_REGEX_BREF) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_REGEX_BREF,
                       FIRST_VALUE(PP.POS_PTA_PASSTHRU_ALLOW_YN_FLAG) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
                       FIRST_VALUE(PP.POS_PTA_DISABLE_DEBIT_DENIAL) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_DISABLE_DEBIT_DENIAL,
                       FIRST_VALUE(PP.POS_PTA_PREF_AUTH_AMT) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_PREF_AUTH_AMT,
                       FIRST_VALUE(PP.POS_PTA_PREF_AUTH_AMT_MAX) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_PREF_AUTH_AMT_MAX,
                       FIRST_VALUE(PP.CURRENCY_CD) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) CURRENCY_CD,
                       FIRST_VALUE(PP.NO_CONVENIENCE_FEE) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) NO_CONVENIENCE_FEE,
                       FIRST_VALUE(PP.POS_PTA_PRIORITY) OVER (PARTITION BY PP.CLIENT_PAYMENT_TYPE_CD, PP.AUTHORITY_PAYMENT_MASK_ID ORDER BY PP.PREFERENCE, PP.LAST_TS DESC, PP.POS_PTA_PRIORITY) POS_PTA_PRIORITY
                  FROM (
                SELECT CASE WHEN PP.POS_PTA_DEACTIVATION_TS IS NULL THEN 0 ELSE 1 END PREFERENCE, PP.POS_PTA_DEACTIVATION_TS LAST_TS, 
                       PS.CLIENT_PAYMENT_TYPE_CD, PP.PAYMENT_SUBTYPE_ID, PP.POS_PTA_ENCRYPT_KEY,
                       PP.POS_PTA_ENCRYPT_KEY2, PP.PAYMENT_SUBTYPE_KEY_ID, PP.POS_PTA_DEVICE_SERIAL_CD, PP.POS_PTA_PIN_REQ_YN_FLAG,
                       PP.POS_PTA_REGEX, PP.POS_PTA_REGEX_BREF, NVL(PP.AUTHORITY_PAYMENT_MASK_ID, PS.AUTHORITY_PAYMENT_MASK_ID) AUTHORITY_PAYMENT_MASK_ID, 
                       PP.POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
                       PP.POS_PTA_DISABLE_DEBIT_DENIAL, PP.POS_PTA_PREF_AUTH_AMT, PP.POS_PTA_PREF_AUTH_AMT_MAX,
                       PP.CURRENCY_CD, PP.NO_CONVENIENCE_FEE, PP.POS_PTA_PRIORITY
                 FROM PSS.POS_PTA PP
                 JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                 JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
                 JOIN REPORT.TRANS_TYPE TT ON PS.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
                WHERE P.DEVICE_ID = ln_device_id
                  AND PP.POS_PTA_ACTIVATION_TS IS NOT NULL
                  AND TT.CASH_IND = 'N'
                  AND PS.PAYMENT_SUBTYPE_CLASS != 'Authority::NOP'
                UNION ALL
               SELECT 2, NULL, 
                      PS.CLIENT_PAYMENT_TYPE_CD, PPTE.PAYMENT_SUBTYPE_ID, PPTE.POS_PTA_ENCRYPT_KEY,
                      PPTE.POS_PTA_ENCRYPT_KEY2, PPTE.PAYMENT_SUBTYPE_KEY_ID, PPTE.POS_PTA_DEVICE_SERIAL_CD, PPTE.POS_PTA_PIN_REQ_YN_FLAG,
                      NULL, NULL, NVL(PPTE.AUTHORITY_PAYMENT_MASK_ID, PS.AUTHORITY_PAYMENT_MASK_ID), PPTE.POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
                      PPTE.POS_PTA_DISABLE_DEBIT_DENIAL, PPTE.POS_PTA_PREF_AUTH_AMT, PPTE.POS_PTA_PREF_AUTH_AMT_MAX,
                      PPTE.CURRENCY_CD, PPTE.NO_CONVENIENCE_FEE, PPTE.POS_PTA_PRIORITY    
                 FROM PSS.POS_PTA_TMPL_ENTRY PPTE
                 JOIN PSS.PAYMENT_SUBTYPE PS ON PPTE.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
                 JOIN REPORT.TRANS_TYPE TT ON PS.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
                WHERE PPTE.POS_PTA_TMPL_ID = (SELECT COALESCE(APP.POS_PTA_TMPL_ID, DST.POS_PTA_TMPL_ID, DT.POS_PTA_TMPL_ID)
                 FROM DEVICE.DEVICE_TYPE DT
                 LEFT JOIN DEVICE.DEVICE_SUB_TYPE DST ON DST.DEVICE_TYPE_ID = ln_device_type_id AND DST.DEVICE_SUB_TYPE_ID = ln_device_sub_type_id
                 LEFT JOIN DEVICE.APP_TYPE APP ON APP.DEVICE_TYPE_ID = ln_device_type_id AND APP.DEVICE_SUB_TYPE_ID = ln_device_sub_type_id
                  AND APP.APP_TYPE_VALUE = lv_app_type
                WHERE DT.DEVICE_TYPE_ID = ln_device_type_id)
                  AND TT.CASH_IND = 'N'
                  AND PS.PAYMENT_SUBTYPE_CLASS != 'Authority::NOP') PP,
                      PSS.POS P
                WHERE P.DEVICE_ID = ln_device_id
            ) LOOP
                IF l_rec.PREFERENCE > 0 THEN
                    PSS.PKG_POS_PTA.INSERT_POS_PTA(ln_pos_pta_id, l_rec.POS_ID, l_rec.PAYMENT_SUBTYPE_ID, l_rec.POS_PTA_ENCRYPT_KEY,
                        l_rec.POS_PTA_ENCRYPT_KEY2, l_rec.PAYMENT_SUBTYPE_KEY_ID, l_rec.POS_PTA_DEVICE_SERIAL_CD, l_rec.POS_PTA_PIN_REQ_YN_FLAG,
                        l_rec.POS_PTA_REGEX, l_rec.POS_PTA_REGEX_BREF, l_rec.AUTHORITY_PAYMENT_MASK_ID, l_rec.POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
                        l_rec.POS_PTA_DISABLE_DEBIT_DENIAL, l_rec.POS_PTA_PREF_AUTH_AMT, l_rec.POS_PTA_PREF_AUTH_AMT_MAX,
                        l_rec.CURRENCY_CD, SYSDATE, l_rec.NO_CONVENIENCE_FEE);
                    UPDATE PSS.POS_PTA
                       SET POS_PTA_PRIORITY = l_rec.POS_PTA_PRIORITY
                     WHERE POS_PTA_ID = ln_pos_pta_id;
                END IF;
            END LOOP;
        ELSE
            UPDATE PSS.POS_PTA
               SET POS_PTA_DEACTIVATION_TS = SYSDATE
             WHERE POS_ID IN(SELECT POS_ID FROM PSS.POS WHERE DEVICE_ID = ln_device_id)
               AND POS_PTA_ACTIVATION_TS IS NOT NULL
               AND (POS_PTA_DEACTIVATION_TS IS NULL OR POS_PTA_DEACTIVATION_TS > SYSDATE)
               AND PAYMENT_SUBTYPE_ID IN(
                    SELECT PS.PAYMENT_SUBTYPE_ID
                      FROM PSS.PAYMENT_SUBTYPE PS
                      JOIN REPORT.TRANS_TYPE TT ON PS.TRANS_TYPE_ID = TT.TRANS_TYPE_ID 
                     WHERE TT.CASH_IND = 'N'
                       AND PS.PAYMENT_SUBTYPE_CLASS != 'Authority::NOP');
        END IF;
        pt_actions_taken := VARCHAR2_TABLE('Set Property "'|| pv_setting_name || '" to "' || pv_setting_value || '"');
        pc_process_status_cd := 'D';
    ELSIF pv_setting_name = 'Two-Tier Pricing' THEN
        IF ln_device_type_id = 11 THEN
          ln_cents := TO_NUMBER_OR_NULL(substr(pv_setting_value,0,instr(pv_setting_value,'|')-1));
        ELSE
          ln_cents := TO_NUMBER_OR_NULL(substr(pv_setting_value,0,instr(pv_setting_value,'|')-1)) * 100;
        END IF;
        lv_vend_or_tran_flag :=substr(pv_setting_value,instr(pv_setting_value,'|')+1);
        IF ln_cents IS NULL OR ln_cents NOT BETWEEN 0 AND 1000 THEN
            pv_error_message := 'Invalid Setting Value for "' || pv_setting_name || '" - must be a number between 0.00 and 10.00';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;  
        IF lv_vend_or_tran_flag not in ('V','T') THEN
            pv_error_message := 'Invalid Setting Value for "' || pv_setting_name || '" - vend or transaction flag could only be ''V'' or ''T''';
            pc_process_status_cd := 'E';
            GOTO DONE;
        ELSIF lv_vend_or_tran_flag ='T' and ln_device_type_id in (0,1,13,11) and pv_device_serial_cd not like 'K3CMS' THEN
            pv_error_message := 'Invalid Setting Value for "' || pv_setting_name || '" - cannot set vend or transaction flag to ''T'' for USAT devices';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;
        -- set values
        IF ln_device_type_id IN(0,1) THEN        
            PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '362', ln_cents, ln_exists);
        ELSIF ln_device_type_id = 13 THEN
            PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '1202', ln_cents, ln_exists);
        ELSIF ln_device_type_id = 11 AND pv_device_serial_cd like 'K3CMS%' THEN
            PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'Two-Tier Pricing', ln_cents, ln_exists);
            PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'Two-Tier Pricing Vend/Tran Flag', lv_vend_or_tran_flag, ln_exists);
        END IF;
        lv_translated_value := ln_cents;
        pt_actions_taken := VARCHAR2_TABLE('Set Property "'|| pv_setting_name || '" to "' || pv_setting_value || '"');
        pc_process_status_cd := 'D';
        GOTO DONE;
    ELSIF pv_setting_name = 'DEX Schedule' THEN
        IF NOT REGEXP_LIKE(pv_setting_value, '^(D\^[0-2][0-9][0-5][0-9]|W\^[0-2][0-9][0-5][0-9]\^[0-6](\|[0-6])*|I\^[0-2][0-9][0-5][0-9]\^[0-9]{2,4}|E\^[0-2][0-9][0-5][0-9](\|[0-2][0-9][0-5][0-9])*)$') THEN
            pv_error_message := 'Invalid Setting Value for "' || pv_setting_name || '" - must be a valid schedule';
            pc_process_status_cd := 'E';
            GOTO DONE;
        END IF;
        DECLARE
            ln_interval_min NUMBER;
            ln_hours NUMBER;
            ln_mins NUMBER;
        BEGIN          
            IF ln_device_type_id = 0 OR (ln_device_type_id = 1 AND DBADMIN.VERSION_COMPARE(SUBSTR(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(ln_device_id, 'Firmware Version'), 9),'6.0.7') < 0) THEN        
                SELECT REGEXP_REPLACE(pv_setting_value, '^[IDWME]\^([0-2][0-9][0-5][0-9]).*$', '\1'), DECODE(pv_setting_name, NULL, '4E', '59')
                  INTO lv_translated_value, lv_secondary_value
                  FROM DUAL;
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '385', lv_secondary_value, ln_exists);
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '172', lv_translated_value, ln_exists); 
                ln_hours := TO_NUMBER(SUBSTR(lv_translated_value, 1, 2));
                ln_mins := TO_NUMBER(SUBSTR(lv_translated_value, 3, 2));
                IF ln_mins >= 30 THEN
                    ln_mins := ln_mins - 30;
                ELSE
                    ln_mins := ln_mins + 30;
                    IF ln_hours = 0 THEN
                        ln_hours := 23;
                    ELSE
                        ln_hours := ln_hours - 1;
                    END IF;
                END IF;
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'CALL_IN_TIME_WINDOW_START', TO_CHAR(ln_hours, 'FM00') ||  TO_CHAR(ln_mins, 'FM00'), ln_exists);
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'CALL_IN_TIME_WINDOW_HOURS', '1', ln_exists);
            ELSIF ln_device_type_id = 1 THEN
            -- Get interval first, then time
                 SELECT REGEXP_REPLACE(pv_setting_value, '^[IDWME]\^([0-2][0-9][0-5][0-9]).*$', '\1')
                  INTO lv_translated_value
                  FROM DUAL;
                IF pv_setting_value LIKE 'I^%' THEN
                    SELECT TO_NUMBER_OR_NULL(REGEXP_REPLACE(pv_setting_value, '^I\^([0-2][0-9])([0-5][0-9])\^([0-9]{2,4})$', '\3'))
                      INTO ln_interval_min
                      FROM DUAL;  
                ELSIF pv_setting_value LIKE 'E^%' THEN
                    SELECT T1, (TO_NUMBER_OR_NULL(SUBSTR(TN, 1, 2)) * 60 + TO_NUMBER_OR_NULL(SUBSTR(TN, 3, 2)) - TO_NUMBER_OR_NULL(SUBSTR(T1, 1, 2)) * 60 - TO_NUMBER_OR_NULL(SUBSTR(T1, 3, 2))) / COUNT(*)             
                      INTO lv_translated_value, ln_interval_min
                      FROM (
                    SELECT MIN(COLUMN_VALUE) T1, MAX(COLUMN_VALUE) TN, COUNT(*) CNT
                      FROM TABLE(DBADMIN.SPLIT(REGEXP_REPLACE(pv_setting_value, '^E\^([0-2][0-9][0-5][0-9](\|[0-2][0-9][0-5][0-9])*)$', '\1'), '|')));
                END IF;
                ln_hours := TO_NUMBER(SUBSTR(lv_translated_value, 1, 2));
                ln_mins := TO_NUMBER(SUBSTR(lv_translated_value, 3, 2));
                IF ln_mins >= 30 THEN
                    ln_mins := ln_mins - 30;
                ELSE
                    ln_mins := ln_mins + 30;
                    IF ln_hours = 0 THEN
                        ln_hours := 23;
                    ELSE
                        ln_hours := ln_hours - 1;
                    END IF;
                END IF;
    
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '358', lv_translated_value, ln_exists);           
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '244', ln_interval_min, ln_exists);           
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'CALL_IN_TIME_WINDOW_START_DEX', TO_CHAR(ln_hours, 'FM00') ||  TO_CHAR(ln_mins, 'FM00'), ln_exists);
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'CALL_IN_TIME_WINDOW_HOURS_DEX', '1', ln_exists);
                IF ln_interval_min IS NOT NULL AND ln_interval_min != 0 THEN
                    lv_translated_value := lv_translated_value || '^' || ln_interval_min;
                END IF;
            ELSIF ln_device_type_id = 13 THEN
                IF pv_setting_value LIKE 'I^%' THEN
                    IF lv_time_zone_guid IS NULL THEN
                       SELECT COALESCE(TZ.TIME_ZONE_GUID, 'US/Eastern')
                         INTO lv_time_zone_guid
                         FROM DUAL
                         LEFT JOIN PSS.POS P ON P.DEVICE_ID = ln_device_id
                         LEFT JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
                         LEFT JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD;
                    END IF;
                    SELECT 'I^' || CASE WHEN TRANSLATED_OFFSET < 0 THEN TRANSLATED_OFFSET + 86400 ELSE TRANSLATED_OFFSET END || '^' || TRANSLATED_INTERVAL,
                           TO_NUMBER(SUBSTR(LOCAL_TIME, 1, 2)), TO_NUMBER(SUBSTR(LOCAL_TIME, 3, 2))
                      INTO lv_translated_value, ln_hours, ln_mins
                      FROM (
                    SELECT MOD(DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN('GMT', lv_time_zone_guid) * 60 + TO_NUMBER_OR_NULL(REGEXP_REPLACE(pv_setting_value, '^I\^([0-2][0-9])([0-5][0-9])\^([0-9]{2,4})$', '\1')) * 60 * 60 + TO_NUMBER_OR_NULL(REGEXP_REPLACE(pv_setting_value, '^I\^([0-2][0-9])([0-5][0-9])\^([0-9]{2,4})$', '\2')) * 60, 86400) TRANSLATED_OFFSET,
                           TO_NUMBER_OR_NULL(REGEXP_REPLACE(pv_setting_value, '^I\^([0-2][0-9])([0-5][0-9])\^([0-9]{2,4})$', '\3')) * 60 TRANSLATED_INTERVAL,
                           REGEXP_REPLACE(pv_setting_value, '^[IDWME]\^([0-2][0-9][0-5][0-9]).*$', '\1') LOCAL_TIME
                      FROM DUAL);  
                ELSIF pv_setting_value LIKE 'E^%' THEN
                    SELECT CASE WHEN COUNT(*) = 1 THEN 'D^' || MIN(TIME_VALUE) ELSE 'I^'|| MIN(T1) || '^' || ((MAX(TN) - MIN(T1)) / COUNT(*)) END,
                           TO_NUMBER(SUBSTR(MIN(TIME_VALUE), 1, 2)), TO_NUMBER(SUBSTR(MIN(TIME_VALUE), 3, 2))
                      INTO lv_translated_value, ln_hours, ln_mins
                      FROM (
                    SELECT FIRST_VALUE(TIME_VALUE) OVER(ORDER BY TRANSLATED_OFFSET) TIME_VALUE, FIRST_VALUE(TRANSLATED_OFFSET) OVER(ORDER BY TRANSLATED_OFFSET) T1, 
                           FIRST_VALUE(TRANSLATED_OFFSET) OVER(ORDER BY TRANSLATED_OFFSET DESC) TN
                      FROM (
                    SELECT TIME_VALUE, CASE WHEN TRANSLATED_OFFSET < 0 THEN TRANSLATED_OFFSET + 86400 ELSE TRANSLATED_OFFSET END TRANSLATED_OFFSET
                      FROM (
                    SELECT COLUMN_VALUE TIME_VALUE, MOD(DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN('GMT', lv_time_zone_guid) * 60 + TO_NUMBER_OR_NULL(SUBSTR(COLUMN_VALUE, 1, 2)) * 60 * 60 + TO_NUMBER_OR_NULL(SUBSTR(COLUMN_VALUE, 3, 2)) * 60, 86400) TRANSLATED_OFFSET
                      FROM TABLE(DBADMIN.SPLIT(REGEXP_REPLACE(pv_setting_value, '^E\^([0-2][0-9][0-5][0-9](\|[0-2][0-9][0-5][0-9])*)$', '\1'), '|')))));
                ELSE
                    SELECT pv_setting_value, TO_NUMBER(SUBSTR(REGEXP_REPLACE(pv_setting_value, '^[IDWME]\^([0-2][0-9][0-5][0-9]).*$', '\1'), 1, 2)), TO_NUMBER(SUBSTR(REGEXP_REPLACE(pv_setting_value, '^[IDWME]\^([0-2][0-9][0-5][0-9]).*$', '\1'), 3, 2))
                      INTO lv_translated_value, ln_hours, ln_mins
                      FROM DUAL;
                END IF;
                IF ln_mins >= 30 THEN
                    ln_mins := ln_mins - 30;
                ELSE
                    ln_mins := ln_mins + 30;
                    IF ln_hours = 0 THEN
                        ln_hours := 23;
                    ELSE
                        ln_hours := ln_hours - 1;
                    END IF;
                END IF;
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, '1101', lv_translated_value, ln_exists);
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'CALL_IN_TIME_WINDOW_START_DEX', TO_CHAR(ln_hours, 'FM00') ||  TO_CHAR(ln_mins, 'FM00'), ln_exists);
                PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, 'CALL_IN_TIME_WINDOW_HOURS_DEX', '1', ln_exists);
            END IF;
        END;
        
        pt_actions_taken := VARCHAR2_TABLE('Set Property "'|| pv_setting_name || '" to "' || pv_setting_value || '"');
        IF lv_translated_value IS NOT NULL THEN
             pt_actions_taken(pt_actions_taken.LAST) := pt_actions_taken(pt_actions_taken.LAST) || ' (translated="' || lv_translated_value || '")';
        END IF;
        pc_process_status_cd := 'D';
    ELSE
        pv_error_message := 'Device Setting "' || pv_setting_name || '" is not supported';
        pc_process_status_cd := 'E';
        GOTO DONE;
    END IF;
    PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(ln_device_id, pv_setting_name, pv_setting_value, ln_exists);
    
    <<DONE>>
    INSERT INTO REPORT.DATA_EXCHANGE_ITEM(
        DATA_EXCHANGE_MAPPING_ID,
        DATA_EXCHANGE_ITEM_TYPE_ID,
        DATA_EXCHANGE_ITEM_ACTION_CD,
        DATA_EXCHANGE_ITEM_TS,
        ENTITY_ID,
        ENTITY_KEY_CD,
        DATA_EXCHANGE_FILE_ID,
        PROCESS_STATUS_CD,
        PROCESS_START_TS,
        PROCESS_COMPLETE_TS,
        PROCESS_DETAIL,
        DATA_EXCHANGE_ITEM_SOURCE)
      SELECT
        ln_data_exchange_mapping_id,
        cn_data_exchange_item_type_id,
        'U',
        pd_timestamp,
        ln_eport_id,
        pv_setting_name,
        pn_data_exchange_file_id,
        pc_process_status_cd,
        ld_start_date,
        SYSDATE,
        CASE WHEN pc_process_status_cd = 'D' THEN DBADMIN.ADHERE(pt_actions_taken, ', ') ELSE pv_error_message END,
        'PROCESS_DATA_EXCHANGE_DS'
        FROM DUAL;
END;
/
