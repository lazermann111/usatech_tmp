CREATE OR REPLACE FUNCTION REPORT.FIND_BUSINESS_UNIT(
    l_eport_id IN REPORT.EPORT.EPORT_ID%TYPE,
    l_customer_id IN CORP.CUSTOMER.CUSTOMER_ID%TYPE)
  RETURN CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE IS
    l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
BEGIN 
    SELECT DECODE(DEVICE_TYPE_ID,
        0, 1, -- G4 => e-Port
        1, 1, -- G5 => e-Port
        3, 1, -- Brick => e-Port
        6, 1, -- MEI Telemeter => e-Port
        7, 1, -- EZ80 ePort Develoment Module => e-Port
        8, 1, -- EZ80 ePort Production Module => e-Port
        9, 1, -- Legacy G4 => e-Port
        10, 2, -- Transact => BEX
        12, 2, -- T2 => BEX
        5, 3, -- eSuds Room Controller => eSuds
        4, 5, -- Sony PictureStation => Sony
        11, 1, -- Kiosk => e-Port
        13, 1, -- Edge => e-Port
        1) -- default to e-Port business unit
      INTO l_business_unit_id
      FROM REPORT.EPORT
     WHERE EPORT_ID = l_eport_id;
    RETURN l_business_unit_id;
END;
/