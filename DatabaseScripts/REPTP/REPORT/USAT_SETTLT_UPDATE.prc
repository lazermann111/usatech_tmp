CREATE OR REPLACE procedure REPORT.USAT_SETTLT_UPDATE as 
--RFC000136 - Add Settlement Update Clean-up Script 
CURSOR l_cur IS
SELECT x.TRAN_ID, x.SETTLE_STATE_ID, x.SETTLE_DATE, x.CC_APPR_CODE
FROM CORP.LEDGER L
JOIN REPORT.TRANS X ON L.TRANS_ID = X.TRAN_ID
WHERE L.ENTRY_TYPE = 'CC'
AND L.DELETED = 'N'
AND L.PAID_DATE IS NULL
AND L.SETTLE_STATE_ID IN (1,4)
AND X.SETTLE_STATE_ID NOT IN(1,4)
AND X.SERVER_DATE < SYSDATE - 1;
BEGIN
FOR l_rec IN l_cur LOOP
REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(l_rec.TRAN_ID, l_rec.SETTLE_STATE_ID, l_rec.SETTLE_DATE, l_rec.CC_APPR_CODE);
COMMIT;
END LOOP;
END;
/
