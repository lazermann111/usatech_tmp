--REPORT schema
/*
DROP TYPE REPORT.T_SYNC_MSG;

CREATE OR REPLACE type REPORT.T_SYNC_MSG as object (
	sql_to_run VARCHAR(4000)); 
	
*/
EXECUTE DBMS_AQADM.CREATE_QUEUE_TABLE (queue_table => 'REPORT.QT_SYNC_MSG', multiple_consumers => FALSE, queue_payload_type => 'REPORT.T_SYNC_MSG');

EXECUTE DBMS_AQADM.CREATE_QUEUE (queue_name => 'REPORT.Q_SYNC_MSG', queue_table => 'REPORT.QT_SYNC_MSG', retry_delay => 60, max_retries => 10);
/* NOT NEEDED - single consumer
EXECUTE DBMS_AQADM.ADD_SUBSCRIBER (queue_name => 'REPORT.Q_SYNC_MSG', subscriber  =>   CORP.PAYMENTS_PKG.Q_SYNC_COMSUMER,  rule => 'tab.user_data.table_name IN(''TRANS'', ''FILL'')');
EXECUTE DBMS_AQADM.ADD_SUBSCRIBER (queue_name => 'REPORT.Q_SYNC_MSG', subscriber  =>   REPORT.DW_PKG.Q_SYNC_COMSUMER, rule => 'tab.user_data.table_name NOT IN(''ORIG_TRANS'')');
EXECUTE DBMS_AQADM.ADD_SUBSCRIBER (queue_name => 'REPORT.Q_SYNC_MSG', subscriber  =>   REPORT.SYNC_PKG.Q_SYNC_COMSUMER, rule => 'tab.user_data.table_name IN(''ORIG_TRANS'')');
*/
EXECUTE DBMS_AQADM.START_QUEUE (queue_name => 'REPORT.Q_SYNC_MSG');

--EXECUTE DBMS_AQADM.STOP_QUEUE (queue_name => 'REPORT.Q_SYNC_MSG');

--EXECUTE DBMS_AQADM.DROP_QUEUE (queue_name => 'REPORT.Q_SYNC_MSG');

--EXECUTE DBMS_AQADM.DROP_QUEUE_TABLE (queue_table => 'REPORT.QT_SYNC_MSG');

--DROP TYPE T_SYNC_MSG
--------------------------------------------------



