CREATE OR REPLACE TRIGGER REPORT.TRAIUD_TERMINAL_ADDR
AFTER INSERT OR UPDATE OR DELETE ON REPORT.TERMINAL_ADDR
FOR EACH ROW
DECLARE
	LC_STATUS_CD PSS.PAYMENT_SUBTYPE_CLASS.STATUS_CD%TYPE;
BEGIN
    DECLARE
        CURSOR L_CUR IS
			SELECT DISTINCT DLA.DEVICE_ID, DLA.DEVICE_NAME
			FROM DEVICE.DEVICE_LAST_ACTIVE DLA
			JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
			JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
			JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
			JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
			JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
			JOIN DEVICE.DEVICE_SETTING DS ON D.DEVICE_ID = DS.DEVICE_ID AND DS.DEVICE_SETTING_PARAMETER_CD = '2015'
			WHERE L.ADDRESS_ID = COALESCE(:NEW.ADDRESS_ID, :OLD.ADDRESS_ID) AND D.DEVICE_TYPE_ID = 13 AND DS.DEVICE_SETTING_VALUE = '1';
    BEGIN
		SELECT MAX(STATUS_CD) INTO LC_STATUS_CD
		FROM PSS.PAYMENT_SUBTYPE_CLASS WHERE PAYMENT_SUBTYPE_CLASS = 'TNS';
	
		IF LC_STATUS_CD = 'A' THEN
			IF INSERTING
				OR UPDATING AND (
					DBADMIN.PKG_UTL.EQL(:NEW.ADDRESS1, :OLD.ADDRESS1) = 'N'
					OR DBADMIN.PKG_UTL.EQL(:NEW.CITY, :OLD.CITY) = 'N'
					OR DBADMIN.PKG_UTL.EQL(:NEW.STATE, :OLD.STATE) = 'N'
					OR DBADMIN.PKG_UTL.EQL(:NEW.ZIP, :OLD.ZIP) = 'N'
					OR DBADMIN.PKG_UTL.EQL(:NEW.COUNTRY_CD, :OLD.COUNTRY_CD) = 'N'
				) OR DELETING THEN
				FOR L_REC IN L_CUR LOOP
					-- Send Terminal Update messages to TNS for Interac Flash devices
					INSERT INTO ENGINE.AUTHORITY_SYNC(AUTHORITY_SYNC_TYPE_CD, OBJECT_CD, OPERATION_CD, ITEM_ID, ITEM_CD, PAYMENT_SUBTYPE_CLASS)
					VALUES ('TERMINAL', 'REPORT.TERMINAL_ADDR', 'U', L_REC.DEVICE_ID, L_REC.DEVICE_NAME, 'TNS');
				END LOOP;
			END IF;
		END IF;
    END;
END;
/
