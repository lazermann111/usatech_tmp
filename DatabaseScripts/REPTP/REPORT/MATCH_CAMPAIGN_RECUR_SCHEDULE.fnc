CREATE OR REPLACE FUNCTION REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE (
    l_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
    l_time TIMESTAMP)
RETURN CHAR IS
    l_sch CHAR;
    l_sch_exp VARCHAR2(200);
    l_hour_exp VARCHAR2(200);
    l_month_allowed NUMBER_TABLE;
    l_count NUMBER;
    l_match CHAR:='N';
    l_pos	 	NUMBER :=0;
    l_start NUMBER:=3;
    l_temp NUMBER;
    l_delimiter CHAR:=',';
    l_hyphen CHAR:='-';
    l_pipe_pos NUMBER;
    l_start_day NUMBER;
    l_end_day NUMBER;
    l_start_hm DATE;
    l_end_hm DATE;
    l_time_date DATE;
    l_date_var VARCHAR2(10);
BEGIN
  l_sch:=SUBSTR(l_recur_schedule,1,1);
  l_pipe_pos:=INSTR(l_recur_schedule,'|', 1,1);
  l_sch_exp:=SUBSTR(l_recur_schedule,1,l_pipe_pos-1);
  l_hour_exp:=SUBSTR(l_recur_schedule,l_pipe_pos+1);
  --dbms_output.put_line('l_sch_exp:'||l_sch_exp||' l_hour_exp:'||l_hour_exp);
  IF l_sch = 'W' THEN
      l_pos := INSTR(l_sch_exp, to_char(l_time, 'd'), l_start);
      --dbms_output.put_line('W l_pos:'||l_pos);
      IF l_pos = 0 THEN
        l_match :='N';
        return l_match;
      END IF;
  ELSIF l_sch = 'S' THEN
    l_pos := INSTR(l_sch_exp, l_delimiter, l_start);
    --dbms_output.put_line('S l_pos:'||l_pos);
    IF l_pos = 0 THEN
      l_date_var:=SUBSTR(l_sch_exp,l_start);
      --dbms_output.put_line('S l_date_var:'||l_date_var||' l_pos:'||l_pos);
      IF l_date_var != to_char(l_time, 'MM/dd/yyyy') THEN
        l_match :='N';
        return l_match;
      END IF;
    ELSE 
      WHILE ( l_pos != 0) LOOP
        l_date_var:=SUBSTR(l_sch_exp,l_start,l_pos-l_start);
        --dbms_output.put_line('S l_date_var:'||l_date_var);
        IF l_date_var = to_char(l_time, 'MM/dd/yyyy') THEN
          l_match :='Y';
          exit;
        END IF;
        l_start := l_pos + 1; 
        l_pos := INSTR(l_sch_exp, l_delimiter, l_start);
      END LOOP;
       IF l_match = 'N' THEN
        l_date_var:=SUBSTR(l_sch_exp,l_start);
        IF l_date_var != to_char(l_time, 'MM/dd/yyyy') THEN
          return l_match;
        END IF;
        --dbms_output.put_line('S l_date_var:'||l_date_var);
       ELSE
        l_match:='N';
       END IF;
    END IF;
  ELSIF l_sch = 'M' THEN
      l_pos := INSTR(l_sch_exp, l_delimiter, l_start);
      l_month_allowed:=NUMBER_TABLE(31);
      IF l_pos = 0 THEN
         l_temp:=INSTR(l_sch_exp,l_hyphen,l_start,1);
         --dbms_output.put_line('l_temp:'||l_temp);
         IF l_temp = 0 THEN
           l_month_allowed.extend;
           l_month_allowed(l_month_allowed.last):=TO_NUMBER(SUBSTR(l_sch_exp,l_start));
         ELSE
          l_start_day:=TO_NUMBER(SUBSTR(l_sch_exp,l_start,l_temp-l_start));
          l_end_day:=TO_NUMBER(SUBSTR(l_sch_exp,l_temp+1));
          FOR l_index IN l_start_day..l_end_day
          LOOP
              --dbms_output.put_line('l_index:'||l_index);
              l_month_allowed.extend;
              l_month_allowed(l_month_allowed.last):=l_index;
          END LOOP;
         END IF;
      ELSE
        WHILE ( l_pos != 0) LOOP
          l_temp:=INSTR(l_sch_exp,l_hyphen,l_start,1);
          IF l_temp > l_pos THEN
            l_temp:=0;
          END IF;
          l_month_allowed.extend;
          --dbms_output.put_line('l_temp:'||l_temp||' l_start:'||l_start||' l_pos:'||l_pos);
          IF l_temp =0 THEN
             --dbms_output.put_line('l_index_month:'||TO_NUMBER(SUBSTR(l_sch_exp,l_start, l_pos-l_start)));
            l_month_allowed(l_month_allowed.last):=TO_NUMBER(SUBSTR(l_sch_exp,l_start, l_pos-l_start));
          ELSE
            l_start_day:=TO_NUMBER(SUBSTR(l_sch_exp,l_start,l_temp-l_start));
            l_end_day:=TO_NUMBER(SUBSTR(l_sch_exp,l_temp+1, l_pos-l_temp-1));
            FOR l_index IN l_start_day..l_end_day
            LOOP
                --dbms_output.put_line('l_index_month:'||l_index);
                l_month_allowed.extend;
                l_month_allowed(l_month_allowed.last):=l_index;
            END LOOP;
          END IF;
            l_start := l_pos + 1; 
            l_pos := INSTR(l_sch_exp, l_delimiter, l_start);
            --dbms_output.put_line('l_start:'||l_start||' l_pos:'||l_pos);
        END LOOP;
        l_temp:=INSTR(l_sch_exp,l_hyphen,l_start,1);
        IF l_temp =0 THEN
            --dbms_output.put_line('l_index_month:'||TO_NUMBER(SUBSTR(l_sch_exp,l_start)));
            l_month_allowed(l_month_allowed.last):=TO_NUMBER(SUBSTR(l_sch_exp,l_start));
        ELSE
            l_start_day:=TO_NUMBER(SUBSTR(l_sch_exp,l_start,l_temp-l_start));
            l_end_day:=TO_NUMBER(SUBSTR(l_sch_exp,l_temp+1));
            FOR l_index IN l_start_day..l_end_day
            LOOP
                --dbms_output.put_line('l_index_month:'||l_index);
                l_month_allowed.extend;
                l_month_allowed(l_month_allowed.last):=l_index;
            END LOOP;
        END IF;
      END IF;
      select count(1) into l_count from table(l_month_allowed) where column_value=(select TO_NUMBER(to_char(CAST (l_time AS TIMESTAMP), 'dd')) from dual);
      --dbms_output.put_line('l_count:'||l_count);
      IF l_count = 0 THEN
        l_match :='N';
        return l_match;
      END IF;
  END IF;
  -- check hour expression
  IF l_hour_exp is null THEN
    l_match:='Y';
    return l_match;
  ELSE
    l_time_date:=to_date(to_char(l_time, 'HH24:MI'),'HH24:MI');
    l_start:=1;
    l_pos := INSTR(l_hour_exp, l_delimiter, l_start);
    IF l_pos = 0 THEN
      l_temp:=INSTR(l_hour_exp,l_hyphen,l_start,1);
      l_start_hm:=to_date(SUBSTR(l_hour_exp,l_start,l_temp-l_start),'HH24:MI');
      l_end_hm:=to_date(SUBSTR(l_hour_exp,l_temp+1),'HH24:MI');
      --dbms_output.put_line('l_time_date:'||l_time_date||' l_start_hm:'||l_start_hm||' l_end_hm:'||l_end_hm);
      IF l_time_date >= l_start_hm  and l_time_date <= l_end_hm THEN
         l_match:='Y';
         return l_match;
      END IF;
    ELSE
      WHILE ( l_pos != 0) LOOP
          l_temp:=INSTR(l_hour_exp,l_hyphen,l_start,1);         
          l_start_hm:=to_date(SUBSTR(l_hour_exp,l_start,l_temp-l_start),'HH24:MI');
          l_end_hm:=to_date(SUBSTR(l_hour_exp,l_temp+1,l_pos-l_temp-1 ),'HH24:MI');
          --dbms_output.put_line('l_time_date:'||l_time_date||' l_start_hm:'||l_start_hm||' l_end_hm:'||l_end_hm);
          IF l_time_date >= l_start_hm  and l_time_date <= l_end_hm THEN
            l_match:='Y';
            return l_match;
          END IF;
            l_start := l_pos + 1; 
            l_pos := INSTR(l_hour_exp, l_delimiter, l_start);
            --dbms_output.put_line('l_start_hour:'||l_start||' l_pos_hour:'||l_pos);
        END LOOP;
        l_temp:=INSTR(l_hour_exp,l_hyphen,l_start,1);
        l_start_hm:=to_date(SUBSTR(l_hour_exp,l_start,l_temp-l_start),'HH24:MI');
        l_end_hm:=to_date(SUBSTR(l_hour_exp,l_temp+1),'HH24:MI');
        --dbms_output.put_line('l_time_date:'||l_time_date||' l_start_hm:'||l_start_hm||' l_end_hm:'||l_end_hm);
        IF l_time_date >= l_start_hm  and l_time_date <= l_end_hm THEN
            l_match:='Y';
            return l_match;
        END IF;
    END IF;
  END IF;
	return l_match;
END;
/
