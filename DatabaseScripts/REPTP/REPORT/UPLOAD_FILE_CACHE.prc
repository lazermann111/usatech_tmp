CREATE OR REPLACE PROCEDURE REPORT.UPLOAD_FILE_CACHE(
    pv_file_name REPORT.FILE_CACHE.FILE_NAME%TYPE,
    pv_file_source REPORT.FILE_CACHE.FILE_SOURCE%TYPE,
    pn_file_cache_type_id REPORT.FILE_CACHE.FILE_CACHE_TYPE_ID%TYPE,
    pd_file_modified_ts REPORT.FILE_CACHE.FILE_MODIFIED_TS%TYPE,
    pl_file_content REPORT.FILE_CACHE.FILE_CONTENT%TYPE,
    pv_file_comment REPORT.FILE_CACHE.FILE_COMMENT%TYPE,
    pn_file_cache_id OUT REPORT.FILE_CACHE.FILE_CACHE_ID%TYPE)
IS
BEGIN
    LOOP
        SELECT MAX(FILE_CACHE_ID)
          INTO pn_file_cache_id
          FROM REPORT.FILE_CACHE
         WHERE FILE_NAME = pv_file_name
           AND NVL(FILE_SOURCE, '-') = NVL(pv_file_source, '-')
           AND FILE_MODIFIED_TS >= pd_file_modified_ts;
        IF pn_file_cache_id IS NOT NULL THEN
            EXIT;
        END IF;
        SELECT REPORT.SEQ_FILE_CACHE_ID.NEXTVAL
          INTO pn_file_cache_id
          FROM DUAL;
        INSERT INTO REPORT.FILE_CACHE(FILE_CACHE_ID, FILE_NAME, FILE_SOURCE, FILE_CACHE_TYPE_ID, FILE_CONTENT, FILE_COMMENT, FILE_MODIFIED_TS, FILE_CACHE_STATE_ID)
            SELECT pn_file_cache_id, pv_file_name, pv_file_source, pn_file_cache_type_id, pl_file_content, pv_file_comment, pd_file_modified_ts, 1
              FROM DUAL
             WHERE NOT EXISTS(
                SELECT 1
                  FROM REPORT.FILE_CACHE
                 WHERE FILE_NAME = pv_file_name
                   AND NVL(FILE_SOURCE, '-') = NVL(pv_file_source, '-')
                   AND FILE_MODIFIED_TS >= pd_file_modified_ts);
        IF SQL%ROWCOUNT = 1 THEN
            EXIT;
        END IF;
    END LOOP;
END;
/