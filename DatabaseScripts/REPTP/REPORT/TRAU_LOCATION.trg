CREATE OR REPLACE TRIGGER report.trau_location
AFTER UPDATE 
OF
    location_name
ON report.location
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    DECLARE
        l_list ID_LIST;
    BEGIN
        SELECT TERMINAL_ID BULK COLLECT INTO l_list
          FROM TERMINAL
         WHERE LOCATION_ID = :NEW.LOCATION_ID;

        SYNC_PKG.SYNC_TERMINAL_INFO(l_list);
    END;
END;
/