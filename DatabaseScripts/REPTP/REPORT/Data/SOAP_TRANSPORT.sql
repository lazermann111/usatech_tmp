INSERT INTO report.ccs_transport_type VALUES(4, 'SOAP');

INSERT INTO report.ccs_transport_property_type(ccs_transport_property_type_id, ccs_transport_type_id, ccs_tpt_name, ccs_tpt_required_flag) VALUES(13, 4, 'URL', 'Y');
INSERT INTO report.ccs_transport_property_type(ccs_transport_property_type_id, ccs_transport_type_id, ccs_tpt_name, ccs_tpt_required_flag) VALUES(14, 4, 'Username', 'N');
INSERT INTO report.ccs_transport_property_type(ccs_transport_property_type_id, ccs_transport_type_id, ccs_tpt_name, ccs_tpt_required_flag) VALUES(15, 4, 'Password', 'N');