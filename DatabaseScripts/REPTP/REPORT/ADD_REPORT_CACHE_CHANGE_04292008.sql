-- add seq, table and trigger for report request 
@@SEQ_REPORT_REQUEST_ID.seq
@@SEQ_REPORT_REQUEST_USER_ID.seq

@@REPORT_REQUEST.tab
@@REPORT_REQUEST_USER.tab
@@TRBI_REPORT_REQUEST.trg
@@TRBU_REPORT_REQUEST.trg
@@TRBI_REPORT_REQUEST_USER.trg
@@TRBU_REPORT_REQUEST_USER.trg


@@REPORT_REQUEST_STATUS.tab
insert into report.report_request_status values(-1, 'Cancelled');
insert into report.report_request_status values(0, 'New');
insert into report.report_request_status values(1, 'Generated or Viewed');
ALTER TABLE REPORT.report_request ADD CONSTRAINT FK_RR_REPORT_REQUEST_STATUS_ID FOREIGN KEY (REPORT_REQUEST_STATUS_ID) REFERENCES REPORT.REPORT_REQUEST_STATUS(REPORT_REQUEST_STATUS_ID);
ALTER TABLE REPORT.report_request_user ADD CONSTRAINT FK_RRU_STATUS_ID FOREIGN KEY (REPORT_REQUEST_USER_STATUS_ID) REFERENCES REPORT.REPORT_REQUEST_STATUS(REPORT_REQUEST_STATUS_ID);

GRANT select,insert,update on REPORT.REPORT_REQUEST to USALIVE_APP;
GRANT select on report.seq_report_request_id to USALIVE_APP;
GRANT select,insert,update on report.report_request_user to USALIVE_APP;
GRANT select on report.seq_report_request_user_id to USALIVE_APP;

insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-please-wait', 'The report you requested is in the generating process. You can cancel, pick up later or wait. Please wait for the result');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-cancel-confirm', 'The report is cancelled successfully.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-cancel-pending-confirm', 'Your request is cancelled successfully. Another pending request is waiting for the same report.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-cancel-generated-confirm', 'The report you requested to cancel is already generated.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-pick-up-later', 'The report is registered for pick up later. You will see the new report is ready on the upper right side.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
        values(web_content.seq_literal_id.nextval, 'report-view-error', 'There is no report for your request.');

commit;

DECLARE
   weblinkId INTEGER;
BEGIN
    select web_content.seq_web_link_id.nextval into webLinkId from dual;
    insert into web_content.web_link values(webLinkId,'Recent Reports', './frame.i?content=report_request_history.i&isByProfile=true','Recent Report Request History', 'Reports','M',125);
    insert into web_content.web_link_requirement values(webLinkId,1);
    COMMIT;
EXCEPTION
	 WHEN OTHERS THEN
	 	 ROLLBACK;
	 	 RAISE;
END;

