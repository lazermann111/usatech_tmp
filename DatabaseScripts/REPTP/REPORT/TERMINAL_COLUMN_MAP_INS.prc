CREATE OR REPLACE PROCEDURE REPORT.TERMINAL_COLUMN_MAP_INS(
	l_terminal_id IN REPORT.TERMINAL_COLUMN_MAP.TERMINAL_ID%TYPE,
	l_mdb_number IN REPORT.TERMINAL_COLUMN_MAP.MDB_NUMBER%TYPE,
	l_vend_column IN REPORT.TERMINAL_COLUMN_MAP.VEND_COLUMN%TYPE
)
IS
BEGIN
	INSERT INTO REPORT.TERMINAL_COLUMN_MAP (TERMINAL_ID, MDB_NUMBER, VEND_COLUMN)
    SELECT l_terminal_id, l_mdb_number, l_vend_column
	FROM DUAL
	WHERE NOT EXISTS (
		SELECT 1
		FROM REPORT.TERMINAL_COLUMN_MAP
		WHERE TERMINAL_ID = l_terminal_id
			AND MDB_NUMBER = l_mdb_number
	);
END;
/