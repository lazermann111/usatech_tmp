CREATE OR REPLACE PROCEDURE REPORT.ADD_SALES_COMMISSION_EVENT (
	pno_comm_event_id OUT REPORT.SALES_COMM_EVENT.SALES_COMM_EVENT_ID%TYPE,
	pno_sales_rep_id OUT REPORT.SALES_COMM_EVENT.SALES_REP_ID%TYPE,
	pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
	pn_sales_rep_id IN REPORT.SALES_REP.SALES_REP_ID%TYPE,
	pv_comm_event_type_cd IN REPORT.SALES_COMM_EVENT_TYPE.SALES_COMM_EVENT_TYPE_CD%TYPE,
	pd_event_ts IN REPORT.SALES_COMM_EVENT.EVENT_TS%TYPE,
	pv_notes IN REPORT.SALES_COMM_EVENT.NOTES%TYPE,
	pn_fallback_sales_rep_id IN REPORT.SALES_REP.SALES_REP_ID%TYPE
)
IS
	ln_comm_event_id REPORT.SALES_COMM_EVENT.SALES_COMM_EVENT_ID%TYPE;
	ln_device_serial_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE;
	ln_current_sales_rep_id REPORT.SALES_REP.SALES_REP_ID%TYPE;
	ln_new_sales_rep_id REPORT.SALES_REP.SALES_REP_ID%TYPE;
	lv_comm_event_type_cd REPORT.SALES_COMM_EVENT_TYPE.SALES_COMM_EVENT_TYPE_CD%TYPE;
BEGIN
	-- get the current commission event
	begin
		select sales_comm_event_id, sales_rep_id, sales_comm_event_type_cd
		into ln_comm_event_id, ln_current_sales_rep_id, lv_comm_event_type_cd
		from report.vw_current_device_sales_rep
		where device_serial_cd = pv_device_serial_cd;
	exception
		when NO_DATA_FOUND then
			-- there's no assignment event, so if sales_rep is -999, then use the fallback
			select decode(pn_sales_rep_id, -999, pn_fallback_sales_rep_id, null, pn_fallback_sales_rep_id, pn_sales_rep_id) into ln_new_sales_rep_id from dual;
			if nvl(ln_new_sales_rep_id, 0) <= 0 then
				RAISE_APPLICATION_ERROR(-20000, 'No current sales rep and no fallback provided.');
			end if;
			
			-- if there's no assignment event then insert
			insert into report.sales_comm_event(device_serial_cd, sales_rep_id, sales_comm_event_type_cd, event_ts, notes)
			values (pv_device_serial_cd, ln_new_sales_rep_id, pv_comm_event_type_cd, pd_event_ts, pv_notes)
			returning sales_comm_event_id, sales_rep_id into pno_comm_event_id, pno_sales_rep_id;
			return;
		when others then
			 raise;
	end;
	
	-- if sales_rep is -999 the use the current sales rep
	select decode(pn_sales_rep_id, -999, ln_current_sales_rep_id, 0, pn_fallback_sales_rep_id, null, pn_fallback_sales_rep_id, pn_sales_rep_id) into ln_new_sales_rep_id from dual;
	if nvl(ln_new_sales_rep_id, 0) <= 0 then
		RAISE_APPLICATION_ERROR(-20001, 'No sales rep provided.');
	end if;

	-- always insert adjustments
	if pv_comm_event_type_cd in ('POSADJ', 'NEGADJ') then
		insert into report.sales_comm_event(device_serial_cd, sales_rep_id, sales_comm_event_type_cd, event_ts, notes)
		values (pv_device_serial_cd, ln_new_sales_rep_id, pv_comm_event_type_cd, pd_event_ts, pv_notes)
		returning sales_comm_event_id, sales_rep_id into pno_comm_event_id, pno_sales_rep_id;
		return;
	end if;

	-- don't insert if the last event sales rep and event type match this event
	if (ln_current_sales_rep_id = ln_new_sales_rep_id and pv_comm_event_type_cd = lv_comm_event_type_cd) then
		pno_comm_event_id := ln_comm_event_id;
		pno_sales_rep_id := ln_new_sales_rep_id;
		return;
	end if;
	
	-- if the event type matches but sales rep is different, delete the current record and insert the new one
	if (pv_comm_event_type_cd = lv_comm_event_type_cd) then
		update report.sales_comm_event
		set deleted = 'Y'
		where sales_comm_event_id = ln_comm_event_id;
	end if;
	
	-- finally insert the event
	insert into report.sales_comm_event(device_serial_cd, sales_rep_id, sales_comm_event_type_cd, event_ts, notes)
	values (pv_device_serial_cd, ln_new_sales_rep_id, pv_comm_event_type_cd, pd_event_ts, pv_notes)
	returning sales_comm_event_id, sales_rep_id into pno_comm_event_id, pno_sales_rep_id;
END;
/

GRANT EXECUTE ON REPORT.ADD_SALES_COMMISSION_EVENT TO USAT_DMS_ROLE;
