CREATE OR REPLACE PROCEDURE REPORT.START_INBOUND_POLL (
    pn_data_exchange_type_id REPORT.DATA_EXCHANGE_TYPE.DATA_EXCHANGE_TYPE_ID%TYPE,
    pn_poll_interval NUMBER,
    pv_proceed OUT VARCHAR2,
    pn_ccs_transport_id OUT REPORT.DATA_EXCHANGE_TYPE.INBOUND_CCS_TRANSPORT_ID%TYPE,
    pn_ccs_transport_type_id OUT REPORT.CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
    pd_poll_start_ts OUT REPORT.DATA_EXCHANGE_TYPE.INBOUND_POLL_START_TS%TYPE,
    pn_delay OUT NUMBER)
AS
BEGIN
    UPDATE REPORT.DATA_EXCHANGE_TYPE
       SET INBOUND_POLL_START_TS = SYSDATE
     WHERE DATA_EXCHANGE_TYPE_ID = pn_data_exchange_type_id
       AND (INBOUND_POLL_START_TS IS NULL OR INBOUND_POLL_START_TS < SYSDATE - (pn_poll_interval / 24 / 60 / 60 / 1000))
      RETURNING 'Y', INBOUND_CCS_TRANSPORT_ID, pn_poll_interval, SYSDATE
      INTO pv_proceed, pn_ccs_transport_id, pn_delay, pd_poll_start_ts;
    IF pv_proceed IS NULL THEN
        SELECT 'N', NVL(pn_poll_interval + ((INBOUND_POLL_START_TS - SYSDATE) * 24 * 60 * 60 * 1000), 0)
          INTO pv_proceed, pn_delay
          FROM REPORT.DATA_EXCHANGE_TYPE
         WHERE DATA_EXCHANGE_TYPE_ID = pn_data_exchange_type_id;
    ELSIF pn_ccs_transport_id IS NOT NULL THEN
        SELECT CCS_TRANSPORT_TYPE_ID
          INTO pn_ccs_transport_type_id
          FROM REPORT.CCS_TRANSPORT
         WHERE CCS_TRANSPORT_ID = pn_ccs_transport_id;
    END IF;  
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      pv_proceed:='N';
      pn_delay:=pn_poll_interval;
END;
/
