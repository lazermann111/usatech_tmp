CREATE OR REPLACE PROCEDURE REPORT.RESEND_UNSENT_REPORTS(
    pd_min_generated_ts DATE DEFAULT SYSDATE - 2,
    pd_max_generated_ts DATE DEFAULT SYSDATE - (6/24))
AS
    CURSOR l_cur IS
      SELECT RS.*, UR.DESTINATION, RR.GENERATED_TS
        FROM REPORT.REPORT_SENT RS
        JOIN REPORT.REPORT_REQUEST RR ON RR.REPORT_REQUEST_ID = RS.REPORT_REQUEST_ID
        JOIN (SELECT UR.USER_REPORT_ID, DECODE(T.CCS_TRANSPORT_TYPE_ID, 1, 'mailto: ', 2, 'sftp: ', 3, 'ftp: ', 4, '', 5, 'ftps: ', 7, '')
                    ||  MAX(CASE WHEN tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(5, 10, 14, 3866) THEN tp.CCS_TRANSPORT_PROPERTY_VALUE || '@' ELSE NULL END)
                    ||  MAX(CASE WHEN tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(3, 8, 3863 /*HOST*/, 1 /*email to: */) THEN tp.CCS_TRANSPORT_PROPERTY_VALUE WHEN tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(13, 37162 /*url*/) THEN tp.CCS_TRANSPORT_PROPERTY_VALUE || ';%' ELSE NULL END) 
                    ||  MAX(CASE WHEN tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(4,9,3865 /*port*/) THEN ':' || tp.CCS_TRANSPORT_PROPERTY_VALUE || '/%' WHEN tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(2 /*email cc:*/) THEN '; cc: ' || tp.CCS_TRANSPORT_PROPERTY_VALUE ELSE NULL END) destination
                FROM REPORT.USER_REPORT UR
                JOIN REPORT.CCS_TRANSPORT_PROPERTY TP ON UR.CCS_TRANSPORT_ID = TP.CCS_TRANSPORT_ID
                JOIN REPORT.CCS_TRANSPORT T ON TP.CCS_TRANSPORT_ID = T.CCS_TRANSPORT_ID
                JOIN REPORT.CCS_TRANSPORT_PROPERTY_TYPE TPT ON TPT.CCS_TRANSPORT_TYPE_ID = T.CCS_TRANSPORT_TYPE_ID
                AND TPT.CCS_TRANSPORT_PROPERTY_TYPE_ID = TP.CCS_TRANSPORT_PROPERTY_TYPE_ID
                AND tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(3, 8, 3863 /*HOST*/,4,9,3865 /*port*/, 13, 37162 /*url*/, 1 /* email to: */, 2 /* email cc: */, 5, 10, 14, 3866/* user*/)
                and tp.CCS_TRANSPORT_PROPERTY_VALUE IS NOT NULL
                GROUP BY UR.USER_REPORT_ID, T.CCS_TRANSPORT_TYPE_ID) UR ON RS.USER_REPORT_ID = UR.USER_REPORT_ID
      WHERE RS.REPORT_SENT_STATE_ID = 2
      AND RR.REPORT_REQUEST_STATUS_ID = 1
      AND RR.GENERATED_TS BETWEEN pd_min_generated_ts AND pd_max_generated_ts;
  l_cnt PLS_INTEGER;
BEGIN
  FOR l_rec IN l_cur LOOP
    IF l_rec.batch_id IS NOT NULL THEN
      SELECT COUNT(*)
        INTO l_cnt
        FROM REPORT.RESEND_REPORTS
       WHERE USER_REPORT_ID = l_rec.user_report_id
         AND BATCH_ID = l_rec.batch_id;
    ELSE
      SELECT COUNT(*)
        INTO l_cnt
        FROM REPORT.RESEND_REPORTS
       WHERE USER_REPORT_ID = l_rec.user_report_id
         AND BEGIN_DATE = l_rec.begin_date
         AND END_DATE = l_rec.end_date;
    END IF;
    IF l_cnt = 0 THEN
       SELECT COUNT(*)
        INTO l_cnt
        FROM REPORT.REPORT_SENT RS
       WHERE USER_REPORT_ID = l_rec.user_report_id
         AND rs.DETAILS LIKE SUBSTR(l_rec.destination,1, 255)
         AND rs.SENT_DATE > SYSDATE - 7;
      IF l_cnt > 0 THEN
        INSERT INTO REPORT.RESEND_REPORTS(RESEND_REPORT_ID, USER_REPORT_ID, BATCH_ID, BEGIN_DATE, END_DATE)
          VALUES(REPORT.SEQ_RESEND_REPORT_ID.NEXTVAL, l_rec.user_report_id, l_rec.batch_id, l_rec.begin_date, l_rec.end_date);
        UPDATE REPORT.REPORT_SENT
          SET REPORT_SENT_STATE_ID = 4, DETAILS = 'Canceled and re-requested'
         WHERE REPORT_SENT_STATE_ID = 2
           AND REPORT_SENT_ID = l_rec.REPORT_SENT_ID;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Added retry for Report Request #' || l_rec.report_request_id || ' (generated ' || to_char(l_rec.generated_ts, 'MM/DD/YYYY HH24:MI') || ') to resend to ' || l_rec.destination ||'');
      ELSE
        DBMS_OUTPUT.PUT_LINE('Not adding retry for Report Request #' || l_rec.report_request_id || ' (generated ' || to_char(l_rec.generated_ts, 'MM/DD/YYYY HH24:MI') || ') because it has not been successfully sent to ' || l_rec.destination ||' in a week');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Not adding retry for Report Request #' || l_rec.report_request_id || ' (generated ' || to_char(l_rec.generated_ts, 'MM/DD/YYYY HH24:MI') || ') because it is already scheduled for resend');
    END IF;
  END LOOP;
END;
/