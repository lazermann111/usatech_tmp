CREATE OR REPLACE PROCEDURE REPORT.CCE_TERMINAL_UPDATE_PROC (
   out_msg   OUT   VARCHAR2,
   out_rc    OUT   INTEGER
) IS
   l_id          NUMBER;
   l_outlet      usat_custom.cce_outlet_credit_to.outlet%TYPE;
   l_credit_to   usat_custom.cce_outlet_credit_to.credit_to%TYPE;
   l_eport       usat_custom.cce_outlet_credit_to.eport%TYPE;
   l_loc_name    usat_custom.cce_outlet_credit_to.loc_name%TYPE;
   l_loc_details usat_custom.cce_outlet_credit_to.loc_addtl%type;
   l_address     usat_custom.cce_outlet_credit_to.address%TYPE;
   l_city        usat_custom.cce_outlet_credit_to.city%TYPE;
   l_state       usat_custom.cce_outlet_credit_to.state%TYPE;
   l_zip         usat_custom.cce_outlet_credit_to.zip%TYPE;
   l_tz          usat_custom.cce_outlet_credit_to.tz%TYPE;
   l_machine     usat_custom.cce_outlet_credit_to.machine%TYPE;
   l_region      usat_custom.cce_outlet_credit_to.region%TYPE;
   l_processed   usat_custom.cce_outlet_credit_to.processed%TYPE;
   l_rc          INTEGER :=0;
   l_msg         VARCHAR2 (1500);
   l_err_cnt     INTEGER :=0;
   l_row_cnt     INTEGER :=0;

   CURSOR c1 IS
      SELECT   a.ID,
               a.outlet,
               a.credit_to,
               a.eport,
               a.loc_name,
               a.loc_addtl,
               a.address,
               a.city,
               a.state,
               a.zip,
               a.tz,
               a.region,
               a.machine,
               a.processed
          FROM usat_custom.cce_outlet_credit_to a
         WHERE a.eport IS NOT NULL
         and a.region is not null
           AND a.processed = 'N'
      ORDER BY a.division_num,
               a.sales_center_num,
               a.loc_name,
               a.eport;
BEGIN
   OPEN c1;

   LOOP
      FETCH c1
       INTO l_id,
            l_outlet,
            l_credit_to,
            l_eport,
            l_loc_name,
            l_loc_details,
            l_address,
            l_city,
            l_state,
            l_zip,
            l_tz,
            l_region,
            l_machine,
            l_processed;

      EXIT WHEN c1%NOTFOUND;

      BEGIN
         cce_terminal_update (l_eport,
                              2974 /*CCE*/,
                              l_outlet,
                              l_machine,
                              l_region,
                              l_loc_name || ' [DC' || l_credit_to || ']',         -- CCE formatted location name for reporting
                              l_loc_details,
                              l_address,
                              l_city,
                              l_state,
                              l_zip,
                              CASE LOWER (SUBSTR (l_tz,
                                                  1,
                                                  1
                                                 ) )
                                 WHEN 'e' THEN 'EST'
                                 WHEN 'c' THEN 'CST'
                                 WHEN 'm' THEN 'MST'
                                 WHEN 'p' THEN 'PST'
                              END
                             );

                  UPDATE usat_custom.cce_outlet_credit_to a
                     SET a.processed = 'Y'
                   WHERE a.ID = l_id;
            
      EXCEPTION                                              -- trapped from sp call above
         WHEN OTHERS THEN
            BEGIN
               l_msg :=
                     l_msg
                  || '| '
                  || l_eport
                  || ' : '
                  || SQLCODE
                  || ' - '
                  || SUBSTR (SQLERRM,
                             1,
                             100
                            );
               l_err_cnt := l_err_cnt + 1;
            END;

      END;

      IF l_err_cnt > 9 THEN
         EXIT;
      END IF;

      l_row_cnt := l_row_cnt + 1;
   END LOOP;

   IF l_err_cnt > 0 THEN
      BEGIN
         out_rc := l_err_cnt;
         out_msg := l_msg;
      END;
   ELSE
      BEGIN
         out_msg := 'Successfully processed ' || TO_CHAR (l_row_cnt) || ' rows.';
      END;
   END IF;
EXCEPTION
   WHEN OTHERS THEN
      out_msg := 'Unexpected Exception: ' || SQLCODE || ' - ' || SUBSTR (SQLERRM,
                                                                         1,
                                                                         100
                                                                        );
END;
/