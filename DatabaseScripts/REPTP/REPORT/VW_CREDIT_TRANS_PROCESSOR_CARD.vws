CREATE OR REPLACE VIEW REPORT.VW_CREDIT_TRANS_PROCESSOR_CARD(PROCESSOR, TRANMONTH, SERVER_MONTH, MASTERCARD_SETTLED, VISA_SETTLED, DISCOVER_SETTLED, AMEX_SETTLED, MASTERCARD_UNSETTLED, VISA_UNSETTLED, DISCOVER_UNSETTLED, AMEX_UNSETTLED)
AS
    SELECT processor,
        tran_month,
        server_month,
        SUM(mastercard_settled)   AS mastercard_settled,
        SUM(visa_settled)         AS visa_settled,
        SUM(discover_settled)     AS discover_settled,
        SUM(amex_settled)         AS amex_settled,
        SUM(mastercard_unsettled) AS mastercard_unsettled,
        SUM(visa_unsettled)       AS visa_unsettled,
        SUM(discover_unsettled)   AS discover_unsettled,
        SUM(amex_unsettled)       AS amex_unsettled
    FROM
        (SELECT DECODE(a.source_system_cd, 'LG4', 'First Data', 'ReRix', 'First Horizon', 'Unknown')                         AS processor,
            TO_CHAR(a.close_date, 'mon-yyyy')                                                                                AS tran_month,
            TO_CHAR(a.server_date, 'mon-yyyy')                                                                               AS server_month,
            DECODE(a.settle_state_id, 3, DECODE(SUBSTR(a.card_number, 1, 1), '5', a.total_amount, 0))                        AS mastercard_settled,
            DECODE(a.settle_state_id, 3, DECODE(SUBSTR(a.card_number, 1, 1), '4', a.total_amount, 0))                        AS visa_settled,
            DECODE(a.settle_state_id, 3, DECODE(SUBSTR(a.card_number, 1, 4), '6011', a.total_amount, 0))                     AS discover_settled,
            DECODE(a.settle_state_id, 3, DECODE(SUBSTR(a.card_number, 1, 2), '34', a.total_amount, '37', a.total_amount, 0)) AS amex_settled,
            DECODE(a.settle_state_id, 5, DECODE(SUBSTR(a.card_number, 1, 1), '5', a.total_amount, 0))                        AS mastercard_unsettled,
            DECODE(a.settle_state_id, 5, DECODE(SUBSTR(a.card_number, 1, 1), '4', a.total_amount, 0))                        AS visa_unsettled,
            DECODE(a.settle_state_id, 5, DECODE(SUBSTR(a.card_number, 1, 4), '6011', a.total_amount, 0))                     AS discover_unsettled,
            DECODE(a.settle_state_id, 5, DECODE(SUBSTR(a.card_number, 1, 2), '34', a.total_amount, '37', a.total_amount, 0)) AS amex_unsettled
        FROM report.trans a,
            report.eport e,
            report.terminal t
        WHERE t.terminal_id(+) = a.terminal_id
        AND e.eport_id         = a.eport_id
            --AND a.server_date BETWEEN '1-jul-2005' AND '1-aug-2005'
        AND a.server_date  >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'),          - 1)
        AND a.server_date   < LAST_DAY(ADD_MONTHS(TRUNC(SYSDATE, 'MM'), - 1)) + 1 -(1 / 24 / 60 / 60)
        AND a.trans_type_id = 16
        AND(e.eport_serial_num LIKE 'E4%'
         OR e.eport_serial_num LIKE 'G%'
         OR e.eport_serial_num LIKE 'E5%'
         OR e.eport_serial_num LIKE 'M1%')
        )
    GROUP BY processor,
        tran_month,
        server_month;