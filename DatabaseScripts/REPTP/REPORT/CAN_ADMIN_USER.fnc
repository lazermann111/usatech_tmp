CREATE OR REPLACE FUNCTION REPORT.CAN_ADMIN_USER(
  l_user_id USER_LOGIN.USER_ID%TYPE,
	l_admin_id USER_LOGIN.USER_ID%TYPE)
RETURN CHAR IS
	l_new_admin_id USER_LOGIN.USER_ID%TYPE;
	l_admin_type USER_LOGIN.USER_TYPE%TYPE;
	l_user_type USER_LOGIN.USER_TYPE%TYPE;
  l_user_parent_cust_id CORP.CUSTOMER.PARENT_CUSTOMER_ID%TYPE;
	l_user_cust_id USER_LOGIN.CUSTOMER_ID%TYPE;
	l_admin_cust_id USER_LOGIN.CUSTOMER_ID%TYPE;
BEGIN
  IF l_user_id IS NULL THEN
    RETURN 'N';
  ELSIF l_user_id  = 0 THEN
    RETURN 'N';
  ELSIF l_user_id = l_admin_id THEN
    RETURN 'Y';
  ELSE
    SELECT USER_TYPE, CUSTOMER_ID 
    INTO l_admin_type, l_admin_cust_id
    FROM USER_LOGIN
    WHERE USER_ID = l_admin_id;
    SELECT ADMIN_ID, USER_TYPE, CUSTOMER_ID 
    INTO l_new_admin_id, l_user_type, l_user_cust_id 
    FROM USER_LOGIN 
    WHERE USER_ID = l_user_id;
    IF l_admin_type = 8 THEN
      IF l_admin_cust_id = l_user_cust_id AND l_user_cust_id <> 0 THEN
        IF CHECK_PRIV(l_admin_id, 5) = 'Y' OR CHECK_PRIV(l_admin_id, 8) = 'Y' THEN
          RETURN 'Y';
        ELSIF CHECK_PRIV(l_admin_id, 21) = 'Y' THEN
          RETURN 'R'; -- read-only
        END IF;
      ELSIF l_user_cust_id <> 0 AND CHECK_PRIV(l_admin_id, 8) = 'Y' AND CHECK_PRIV(l_admin_id, 29) = 'Y' THEN
        SELECT C.PARENT_CUSTOMER_ID
        INTO l_user_parent_cust_id
        FROM CORP.CUSTOMER C
        WHERE C.CUSTOMER_ID = l_user_cust_id;
        IF l_user_parent_cust_id = l_admin_cust_id THEN
          RETURN 'Y';
        END IF;
      END IF;
    ELSIF (CHECK_PRIV(l_admin_id, 5) = 'Y' OR (l_user_type = 8 AND CHECK_PRIV(l_admin_id, 8) = 'Y')) THEN
      RETURN 'Y';
    ELSIF l_user_type = 8 AND CHECK_PRIV(l_admin_id, 21) = 'Y' THEN
      RETURN 'R'; -- read-only
    END IF;
    IF l_new_admin_id = l_user_id THEN -- if a user's admin is themself
      RETURN 'N';
    ELSE
      RETURN CAN_ADMIN_USER(l_new_admin_id, l_admin_id);
    END IF;
  END IF;
END;
