CREATE OR REPLACE FUNCTION REPORT.CARD_COMPANY (
	pv_card_number VARCHAR2
) RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
	lv_cn REPORT.TRANS.CARD_NUMBER%TYPE;
	lv_default REPORT.TRANS.CARD_NUMBER%TYPE := 'Unknown';
	lv_ch2 VARCHAR2(2);
	lv_ch3 VARCHAR2(3);
	lv_ch6 VARCHAR2(6);
	ln_len NUMBER;
BEGIN
	IF pv_card_number IS NULL THEN
		RETURN lv_default;
	END IF;

	lv_cn := SUBSTR(TRIM(pv_card_number), 1, 50);
	ln_len := INSTR(lv_cn, '=') - 1;
	IF ln_len = -1 THEN
		ln_len := LENGTH(lv_cn);
	END IF;
	
	IF ln_len < 15 THEN
		RETURN lv_default;
	END IF;
	
	IF ln_len = 16 AND lv_cn LIKE '4%' THEN
		RETURN 'Visa';
  ELSIF ln_len = 19 AND lv_cn LIKE '639621%' THEN
    RETURN 'Prepaid';
	END IF;
	
	lv_ch2 := SUBSTR(lv_cn, 1, 2);
	IF ln_len = 16 AND lv_ch2 BETWEEN '51' AND '55' THEN
		RETURN 'MasterCard';
	ELSIF ln_len = 15 AND lv_ch2 IN ('34', '37') THEN
		RETURN 'American Express';
	END IF;
	
	lv_ch3 := SUBSTR(lv_cn, 1, 3);
	lv_ch6 := SUBSTR(lv_cn, 1, 6);
	IF ln_len = 16 AND (lv_cn LIKE '6011%'
		OR lv_ch6 BETWEEN '622126' AND '622925'
		OR lv_ch3 BETWEEN '644' AND '649'
		OR lv_ch2 = '65') THEN
		RETURN 'Discover';
	END IF;
  
	
	RETURN lv_default;
END;
/
