
-- put ccs_transport code to E and insert error
@@CCS_TRANSPORT_ERROR_INS.prc
-- put ccs_transport code to N and update reactivate_ts
@@CCS_TRANSPORT_ERROR_UPD.prc

-- add regex so it can do valiation for property added by usalive 1.5.5
--ALTER TABLE
--   REPORT.CCS_TRANSPORT_PROPERTY_TYPE
--ADD
--   CCS_TPT_REGEX  VARCHAR2(4000);

GRANT execute on REPORT.CCS_TRANSPORT_ERROR_UPD to USALIVE_APP;
GRANT select, update on REPORT.CCS_TRANSPORT_ERROR to USALIVE_APP;

-- add regex for email, port and host
update REPORT.CCS_TRANSPORT_PROPERTY_TYPE set ccs_tpt_regex='/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/' where ccs_tpt_name='Email';
update REPORT.CCS_TRANSPORT_PROPERTY_TYPE set ccs_tpt_regex='/(^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$)|^$/' where ccs_tpt_name='CC';
update REPORT.CCS_TRANSPORT_PROPERTY_TYPE set ccs_tpt_regex='/^(6553[0-5]|655[0-2]\d|65[0-4]\d\d|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}|0)$/' where ccs_tpt_name='Port';
update REPORT.CCS_TRANSPORT_PROPERTY_TYPE set ccs_tpt_regex='/(^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$)|(^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$)/' where ccs_tpt_name='Host';
COMMIT;
-- code to add ccs transport error link in usalive
DECLARE
   weblinkId INTEGER;
BEGIN
    select web_content.seq_web_link_id.nextval into webLinkId from dual;
    insert into web_content.web_link values(webLinkId,'CCS Transport Error', './frame.i?content=ccs_transport_error.i','CCS Transport Error report', 'Customer Service','M',270);
    insert into web_content.web_link_requirement values(webLinkId,14);
    insert into web_content.web_link_requirement values(webLinkId,1);
    COMMIT;
EXCEPTION
	 WHEN OTHERS THEN
	 	 ROLLBACK;
	 	 RAISE;
END;

-- insert message for ccs error activation added by usalive 1.5.5
--insert into web_content.literal (literal_id, literal_key, literal_value) 
--        values(web_content.seq_literal_id.nextval, 'cte-activation-failure', 'The user report activation failed.');
        
--insert into web_content.literal (literal_id, literal_key, literal_value) 
--        values(web_content.seq_literal_id.nextval, 'cte-activation-success', 'The user report activation is successful.');
        
--commit;

-- add generator_id 6 for using runReport
insert into report.generator (generator_id, name) values(6, 'RUNREPORT');
update report.reports set generator_id=6 where report_id in (select report_id from report.report_param where param_name ='HTMLaction' and PARAM_VALUE='run_report.i');
commit;

-- add ccs_transport_error_attribute
@@CCS_TRANSPORT_ERROR_ATTR.tab
@@SEQ_CCS_TRANSPORT_ERROR_ATTR_ID.seq
@@TRBI_CCS_TRANSPORT_ERROR_ATTR.trg
@@TRBU_CCS_TRANSPORT_ERROR_ATTR.trg

ALTER TABLE REPORT.CCS_TRANSPORT_ERROR_ATTR ADD CONSTRAINT FK_CTEA_CCS_TRANSPORT_ERROR_ID FOREIGN KEY (CCS_TRANSPORT_ERROR_ID) REFERENCES REPORT.CCS_TRANSPORT_ERROR(CCS_TRANSPORT_ERROR_ID);
GRANT select on REPORT.CCS_TRANSPORT_ERROR_ATTR to USALIVE_APP;
GRANT select on REPORT.CCS_TRANSPORT to USALIVE_APP;
GRANT select on REPORT.CCS_TRANSPORT_PROPERTY to USALIVE_APP;
GRANT select on REPORT.CCS_TRANSPORT_PROPERTY_TYPE to USALIVE_APP;
GRANT select on REPORT.USER_REPORT to USALIVE_APP;





