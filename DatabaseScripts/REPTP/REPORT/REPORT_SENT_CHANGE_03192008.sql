@@REPORT_SENT_STATE.tab
-- insert the report_state_id with description
insert into report.report_sent_state values(1, 'Report is requested.');
insert into report.report_sent_state values(2, 'Report is generated.');
insert into report.report_sent_state values(3, 'Report is sent.');
insert into report.report_sent_state values(4, 'Report is canceled.');

-- add report_state_id and make foreignkey
ALTER TABLE REPORT.REPORT_SENT ADD REPORT_SENT_STATE_ID NUMBER(1);
ALTER TABLE REPORT.REPORT_SENT ADD CONSTRAINT FK_REPORT_SENT_STATE_ID FOREIGN KEY (REPORT_SENT_STATE_ID) REFERENCES REPORT.REPORT_SENT_STATE(REPORT_SENT_STATE_ID);
UPDATE REPORT.REPORT_SENT SET REPORT_SENT_STATE_ID = CASE WHEN STATUS ='A' THEN 3 ELSE 4 END;
commit;
ALTER TABLE REPORT.REPORT_SENT MODIFY REPORT_SENT_STATE_ID NUMBER(1) NOT NULL;
ALTER TABLE REPORT.REPORT_SENT DROP COLUMN STATUS;
-- make sent_date nullable and add request_date and generate_date
ALTER TABLE REPORT.REPORT_SENT MODIFY SENT_DATE DATE NULL;
ALTER TABLE REPORT.REPORT_SENT ADD (REQUEST_DATE DATE,GENERATE_DATE DATE);

-- add check CCS_TRANSPORT_STATUS_CD to the view so it will not select bad transport report and change check for state_id
@@VW_LAST_SENT.vws

-- change the proc spec and body for adding request inserts
@@CCS_PKG.psk
@@CCS_PKG.pbk