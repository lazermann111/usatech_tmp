CREATE OR REPLACE TRIGGER report.trbu_report_request
BEFORE UPDATE ON report.report_request
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT :OLD.CREATED_TS,
           :OLD.CREATED_BY,
           SYS_EXTRACT_UTC(SYSTIMESTAMP),
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END; 
/