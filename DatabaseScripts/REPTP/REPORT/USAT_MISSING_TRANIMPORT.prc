CREATE OR REPLACE procedure REPORT.USAT_MISSING_TRANIMPORT as 
-- Missing transactions import

    l_cnt PLS_INTEGER;
    CURSOR l_day_cur IS
       SELECT a.SOURCE_SYSTEM_CD, a.DEVICE_TYPE_ID, a.TRAN_TYPE, a.UPLOAD_DAY, NVL(a.TRAN_COUNT, 0) - NVL(b.TRAN_COUNT, 0) MISSING_COUNT
            FROM RECON.TRAN_COUNT_AUDIT a
            JOIN RECON.TRAN_COUNT_AUDIT b 
            ON a.SOURCE_SYSTEM_CD = b.SOURCE_SYSTEM_CD
            AND a.DEVICE_TYPE_ID = b.DEVICE_TYPE_ID
            AND a.TRAN_TYPE = b.TRAN_TYPE
            AND a.UPLOAD_DAY = b.UPLOAD_DAY
            LEFT OUTER JOIN 
            (SELECT NULL SOURCE_SYSTEM_CD, 0 DEVICE_TYPE_ID, '' TRAN_TYPE, NULL UPLOAD_DAY, 0 BAD_TRAN_COUNT, 0 BAD_TRAN_AMOUNT FROM DUAL WHERE 1=0
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('01/21/2008', 'MM/DD/YYYY'), 76, 95 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('01/29/2008', 'MM/DD/YYYY'), 8, 12 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('02/11/2008', 'MM/DD/YYYY'), 1, .75 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('02/13/2008', 'MM/DD/YYYY'), 8, 8 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Credit', TO_DATE('02/13/2008', 'MM/DD/YYYY'), 1, 1 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('02/19/2008', 'MM/DD/YYYY'), 102, 3143.9 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Credit', TO_DATE('02/19/2008', 'MM/DD/YYYY'), 22, 726.9 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('03/04/2008', 'MM/DD/YYYY'), 2, 2.15 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('03/07/2008', 'MM/DD/YYYY'), 12, 11.5 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('04/28/2008', 'MM/DD/YYYY'), 3, 2.75 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('05/19/2008', 'MM/DD/YYYY'), 43, 44.45 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Credit', TO_DATE('05/19/2008', 'MM/DD/YYYY'), 4, 6.5 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('05/30/2008', 'MM/DD/YYYY'), 84, 290.5 FROM DUAL
            ) c 
            ON a.SOURCE_SYSTEM_CD = c.SOURCE_SYSTEM_CD
            AND a.UPLOAD_DAY = c.UPLOAD_DAY 
            AND a.DEVICE_TYPE_ID = c.DEVICE_TYPE_ID
            AND a.TRAN_TYPE = c.TRAN_TYPE
            WHERE a.SYSTEM_NAME = 'USADBP' AND b.SYSTEM_NAME = 'USARDB'
            AND (a.TRAN_COUNT <> b.TRAN_COUNT
            OR a.TRAN_AMOUNT <> b.TRAN_AMOUNT)
            AND (a.DEVICE_TYPE_ID NOT IN(5) OR a.TRAN_TYPE = 'Credit')
            /*custom check for bad tran start dates*/
            AND (c.UPLOAD_DAY IS NULL OR a.TRAN_COUNT <> b.TRAN_COUNT + c.BAD_TRAN_COUNT OR a.TRAN_AMOUNT <> b.TRAN_AMOUNT + c.BAD_TRAN_AMOUNT)
            --AND a.SOURCE_SYSTEM_CD IN('PSS')
            --AND a.TRAN_TYPE IN('Credit')
            AND a.UPLOAD_DAY > SYSDATE -4 
            ;
    l_day_update_cnt PLS_INTEGER;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Discrepancy re-processing beginning for ');
    FOR l_day_rec IN l_day_cur LOOP
        DBMS_OUTPUT.PUT_LINE('Missing ' || l_day_rec.MISSING_COUNT || ' trans for ' || l_day_rec.UPLOAD_DAY);
        l_day_update_cnt := 0;
        IF l_day_rec.MISSING_COUNT < 0 THEN -- more trans in rdb then dbp
            DECLARE
                CURSOR l_hour_cur IS
                    SELECT b.UPLOAD_HOUR 
                      FROM (
                        SELECT 
                            TRUNC(TRAN_UPLOAD_TS, 'HH') UPLOAD_HOUR,
                            COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                          FROM PSS.VW_TRAN_COUNT_BY_DAY
                         WHERE TRAN_UPLOAD_TS >= l_day_rec.UPLOAD_DAY
                           AND TRAN_UPLOAD_TS < l_day_rec.UPLOAD_DAY + 1
                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(TRAN_UPLOAD_TS, 'HH')) a
                           RIGHT OUTER JOIN (
                         SELECT 
                            TRUNC(x.SERVER_DATE, 'HH') UPLOAD_HOUR,
                            COUNT(*) TRAN_COUNT,
                            SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                          FROM REPORT.TRANS x, REPORT.EPORT d
                         WHERE x.EPORT_ID = d.EPORT_ID
                           AND x.SERVER_DATE >= l_day_rec.UPLOAD_DAY
                           AND x.SERVER_DATE < l_day_rec.UPLOAD_DAY + 1
                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND DECODE(x.TRANS_TYPE_ID, 13, 'Credit', 14, 'Credit', 15, 'Special', 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(x.SERVER_DATE, 'HH')) b
                        ON a.UPLOAD_HOUR = b.UPLOAD_HOUR
                     WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                            OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
            BEGIN
            FOR l_hour_rec IN l_hour_cur LOOP
                DECLARE
                    CURSOR l_min_cur IS
                        SELECT b.UPLOAD_MINUTE
                          FROM (
                            SELECT 
                                TRUNC(TRAN_UPLOAD_TS, 'MI') UPLOAD_MINUTE,
                                COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                                SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                              FROM PSS.VW_TRAN_COUNT_BY_DAY
                             WHERE TRAN_UPLOAD_TS >= l_hour_rec.UPLOAD_HOUR
                               AND TRAN_UPLOAD_TS < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                               AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(TRAN_UPLOAD_TS, 'MI')) a
                               RIGHT OUTER JOIN  (
                             SELECT 
                                TRUNC(x.SERVER_DATE, 'MI') UPLOAD_MINUTE,
                                COUNT(*) TRAN_COUNT,
                                SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                              FROM REPORT.TRANS x, REPORT.EPORT d
                             WHERE x.EPORT_ID = d.EPORT_ID
                               AND x.SERVER_DATE >= l_hour_rec.UPLOAD_HOUR
                               AND x.SERVER_DATE < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND DECODE(x.TRANS_TYPE_ID, 13, 'Credit', 14, 'Credit', 15, 'Special', 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(x.SERVER_DATE, 'MI')) b
                            ON a.UPLOAD_MINUTE = b.UPLOAD_MINUTE
                         WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                                OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
                BEGIN
                     FOR l_min_rec IN l_min_cur LOOP
                         DECLARE
                                CURSOR l_tran_cur IS
                                    SELECT 
                                        a.TRAN_ID,
                                        b.MACHINE_TRANS_NO TRAN_GLOBAL_TRANS_CD, 
                                        a.TRAN_AMOUNT TRAN_AMOUNT_A,
                                        b.TOTAL_AMOUNT TRAN_AMOUNT_B
                                      FROM (
                                        SELECT 
                                            TRAN_ID,
                                            TRAN_GLOBAL_TRANS_CD,
                                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                                          FROM PSS.VW_TRAN_COUNT_BY_DAY
                                         WHERE TRAN_UPLOAD_TS >= l_min_rec.UPLOAD_MINUTE
                                           AND TRAN_UPLOAD_TS < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                                           GROUP BY TRAN_ID, TRAN_GLOBAL_TRANS_CD) a
                                         RIGHT OUTER JOIN (
                                         SELECT 
                                            x.MACHINE_TRANS_NO,
                                            x.TOTAL_AMOUNT
                                          FROM REPORT.TRANS x, REPORT.EPORT d
                                         WHERE x.EPORT_ID = d.EPORT_ID
                                           AND x.SERVER_DATE >= l_min_rec.UPLOAD_MINUTE
                                           AND x.SERVER_DATE < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND DECODE(x.TRANS_TYPE_ID, 13, 'Credit', 14, 'Credit', 15, 'Special', 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                                           ) b
                                        ON a.TRAN_GLOBAL_TRANS_CD = b.MACHINE_TRANS_NO
                                     WHERE (a.TRAN_AMOUNT <> b.TOTAL_AMOUNT OR b.MACHINE_TRANS_NO IS NULL);
                        BEGIN
                            /*DBMS_OUTPUT.PUT_LINE('Found Mis-Match at ' || to_char(l_min_rec.UPLOAD_MINUTE, 'MM/DD/YYYY HH24:MI')
                                 || ' (' || l_day_rec.TRAN_TYPE || ',' || l_day_rec.SOURCE_SYSTEM_CD
                                 || ',' || l_day_rec.DEVICE_TYPE_ID || ')');*/
                            FOR l_tran_rec IN l_tran_cur LOOP
                                DBMS_OUTPUT.PUT_LINE('Found Mis-Match for ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD
                                 || ' (' || to_char(l_tran_rec.TRAN_AMOUNT_A) || ' vs ' || to_char(l_tran_rec.TRAN_AMOUNT_B)
                                 ||  ')');
                                IF l_tran_rec.tran_id IS NULL THEN
                                    DBMS_OUTPUT.PUT_LINE('Transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ' is NOT in the transaction processing system!');
                                ELSE
                                    SELECT COUNT(*)
                                      INTO l_cnt
                                      FROM REPORT.TRANS x
                                     WHERE x.MACHINE_TRANS_NO = l_tran_rec.TRAN_GLOBAL_TRANS_CD;
                                    IF l_cnt <> 1 THEN
                                       DBMS_OUTPUT.PUT_LINE('Pulling transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD);
                                       DECLARE
                                          ln_report_tran_id REPORT.TRANS.TRAN_ID%TYPE;
                                       BEGIN
                                         UPDATER.PULL_DATA_PKG.pull_pss_tran_by_id(l_tran_rec.tran_id);
                                         SELECT x.TRAN_ID
                                           INTO ln_report_tran_id
                                           FROM REPORT.TRANS x
                                          WHERE x.MACHINE_TRANS_NO = l_tran_rec.TRAN_GLOBAL_TRANS_CD;                                          
                                         REPORT.SYNC_PKG.RECEIVE_TRANS_INSERT(ln_report_tran_id);
                                         COMMIT;
                                         l_day_update_cnt := l_day_update_cnt + 1;
                                       EXCEPTION
                                         WHEN OTHERS THEN
                                            DBMS_OUTPUT.PUT_LINE('ERROR: Could not pull transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ': ' || SQLERRM);
                                       END;
                                    ELSE
                                       DBMS_OUTPUT.PUT_LINE('Transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ' is already in reporting system');           
                                    END IF;
                                END IF;
                            END LOOP;    
                        END;
                    END LOOP;
                END;
             END LOOP;
            END;
        ELSE
            DECLARE
                CURSOR l_hour_cur IS
                    SELECT a.UPLOAD_HOUR 
                      FROM (
                        SELECT 
                            TRUNC(TRAN_UPLOAD_TS, 'HH') UPLOAD_HOUR,
                            COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                          FROM PSS.VW_TRAN_COUNT_BY_DAY
                         WHERE TRAN_UPLOAD_TS >= l_day_rec.UPLOAD_DAY
                           AND TRAN_UPLOAD_TS < l_day_rec.UPLOAD_DAY + 1
                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(TRAN_UPLOAD_TS, 'HH')) a
                           LEFT OUTER JOIN (
                         SELECT 
                            TRUNC(x.SERVER_DATE, 'HH') UPLOAD_HOUR,
                            COUNT(*) TRAN_COUNT,
                            SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                          FROM REPORT.TRANS x, REPORT.EPORT d
                         WHERE x.EPORT_ID = d.EPORT_ID
                           AND x.SERVER_DATE >= l_day_rec.UPLOAD_DAY
                           AND x.SERVER_DATE < l_day_rec.UPLOAD_DAY + 1
                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND DECODE(x.TRANS_TYPE_ID, 13, 'Credit', 14, 'Credit', 15, 'Special', 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(x.SERVER_DATE, 'HH')) b
                        ON a.UPLOAD_HOUR = b.UPLOAD_HOUR
                     WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                            OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
            BEGIN
            FOR l_hour_rec IN l_hour_cur LOOP
                DECLARE
                    CURSOR l_min_cur IS
                        SELECT a.UPLOAD_MINUTE
                          FROM (
                            SELECT 
                                TRUNC(TRAN_UPLOAD_TS, 'MI') UPLOAD_MINUTE,
                                COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                                SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                              FROM PSS.VW_TRAN_COUNT_BY_DAY
                             WHERE TRAN_UPLOAD_TS >= l_hour_rec.UPLOAD_HOUR
                               AND TRAN_UPLOAD_TS < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                               AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(TRAN_UPLOAD_TS, 'MI')) a
                               LEFT OUTER JOIN  (
                             SELECT 
                                TRUNC(x.SERVER_DATE, 'MI') UPLOAD_MINUTE,
                                COUNT(*) TRAN_COUNT,
                                SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                              FROM REPORT.TRANS x, REPORT.EPORT d
                             WHERE x.EPORT_ID = d.EPORT_ID
                               AND x.SERVER_DATE >= l_hour_rec.UPLOAD_HOUR
                               AND x.SERVER_DATE < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND DECODE(x.TRANS_TYPE_ID, 13, 'Credit', 14, 'Credit', 15, 'Special', 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(x.SERVER_DATE, 'MI')) b
                            ON a.UPLOAD_MINUTE = b.UPLOAD_MINUTE
                         WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                                OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
                BEGIN
                     FOR l_min_rec IN l_min_cur LOOP
                         DECLARE
                                CURSOR l_tran_cur IS
                                    SELECT 
                                        a.TRAN_ID,
                                        a.TRAN_GLOBAL_TRANS_CD, 
                                        a.TRAN_AMOUNT TRAN_AMOUNT_A,
                                        b.TOTAL_AMOUNT TRAN_AMOUNT_B
                                      FROM (
                                        SELECT 
                                            TRAN_ID,
                                            TRAN_GLOBAL_TRANS_CD,
                                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                                          FROM PSS.VW_TRAN_COUNT_BY_DAY
                                         WHERE TRAN_UPLOAD_TS >= l_min_rec.UPLOAD_MINUTE
                                           AND TRAN_UPLOAD_TS < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                                           GROUP BY TRAN_ID, TRAN_GLOBAL_TRANS_CD) a
                                         LEFT OUTER JOIN (
                                         SELECT 
                                            x.MACHINE_TRANS_NO,
                                            x.TOTAL_AMOUNT
                                          FROM REPORT.TRANS x, REPORT.EPORT d
                                         WHERE x.EPORT_ID = d.EPORT_ID
                                           AND x.SERVER_DATE >= l_min_rec.UPLOAD_MINUTE
                                           AND x.SERVER_DATE < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND DECODE(x.TRANS_TYPE_ID, 13, 'Credit', 14, 'Credit', 15, 'Special', 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                                           ) b
                                        ON a.TRAN_GLOBAL_TRANS_CD = b.MACHINE_TRANS_NO
                                     WHERE (a.TRAN_AMOUNT <> b.TOTAL_AMOUNT OR b.MACHINE_TRANS_NO IS NULL);
                        BEGIN
                            /*DBMS_OUTPUT.PUT_LINE('Found Mis-Match at ' || to_char(l_min_rec.UPLOAD_MINUTE, 'MM/DD/YYYY HH24:MI')
                                 || ' (' || l_day_rec.TRAN_TYPE || ',' || l_day_rec.SOURCE_SYSTEM_CD
                                 || ',' || l_day_rec.DEVICE_TYPE_ID || ')');*/
                            FOR l_tran_rec IN l_tran_cur LOOP
                                DBMS_OUTPUT.PUT_LINE('Found Mis-Match for ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD
                                 || ' (' || to_char(l_tran_rec.TRAN_AMOUNT_A) || ' vs ' || to_char(l_tran_rec.TRAN_AMOUNT_B)
                                 ||  ')');
                                 
                                SELECT COUNT(*)
                                  INTO l_cnt
                                  FROM REPORT.TRANS x
                                 WHERE x.MACHINE_TRANS_NO = l_tran_rec.TRAN_GLOBAL_TRANS_CD;
                                IF l_cnt <> 1 THEN
                                   DBMS_OUTPUT.PUT_LINE('Pulling transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD);
                                   DECLARE
                                      ln_report_tran_id REPORT.TRANS.TRAN_ID%TYPE;
                                   BEGIN
                                     UPDATER.PULL_DATA_PKG.pull_pss_tran_by_id(l_tran_rec.tran_id);
                                     SELECT x.TRAN_ID
                                       INTO ln_report_tran_id
                                       FROM REPORT.TRANS x
                                      WHERE x.MACHINE_TRANS_NO = l_tran_rec.TRAN_GLOBAL_TRANS_CD;                                          
                                     REPORT.SYNC_PKG.RECEIVE_TRANS_INSERT(ln_report_tran_id);  
                                     COMMIT;
                                     l_day_update_cnt := l_day_update_cnt + 1;
                                   EXCEPTION
                                     WHEN OTHERS THEN
                                        DBMS_OUTPUT.PUT_LINE('ERROR: Could not pull transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ': ' || SQLERRM);
                                   END;
                                ELSE
                                   DBMS_OUTPUT.PUT_LINE('Transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ' is already in reporting system');           
                                END IF; 
                            END LOOP;    
                        END;
                    END LOOP;
                END;
             END LOOP;
            END;
        END IF;
        IF l_day_update_cnt > 0 THEN
            RECON.AUDIT_PKG.CALC_TRAN_COUNTS(l_day_rec.UPLOAD_DAY);
        END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Discrepancy re-processing complete');
END;
/
