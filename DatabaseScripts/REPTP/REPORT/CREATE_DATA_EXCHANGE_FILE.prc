CREATE OR REPLACE PROCEDURE REPORT.CREATE_DATA_EXCHANGE_FILE(
    pn_data_exchange_type_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_TYPE_ID%TYPE,
    pt_dei_ids NUMBER_TABLE,
    pv_resource_key REPORT.DATA_EXCHANGE_FILE.RESOURCE_KEY%TYPE,
    pn_data_exchange_file_size REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_SIZE%TYPE,
    pv_data_exchange_file_name IN OUT REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_NAME%TYPE,
    pc_process_status_cd REPORT.DATA_EXCHANGE_FILE.PROCESS_STATUS_CD%TYPE,
    pn_data_exchange_file_id OUT REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_ID%TYPE)
IS
    lv_file_name_prefix REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_NAME%TYPE;
    lv_file_name_suffix REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_NAME%TYPE;  
BEGIN
    SELECT REPORT.SEQ_DATA_EXCHANGE_FILE_ID.NEXTVAL, 
           REGEXP_REPLACE(pv_data_exchange_file_name, '^(.*)(\.[^.]*)$', '\1'), 
           REGEXP_SUBSTR(pv_data_exchange_file_name, '(\.[^.]*)$')
      INTO pn_data_exchange_file_id, lv_file_name_prefix, lv_file_name_suffix
      FROM DUAL;
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO REPORT.DATA_EXCHANGE_FILE(
                DATA_EXCHANGE_FILE_ID, 
                DATA_EXCHANGE_FILE_NAME, 
                RESOURCE_KEY, 
                DATA_EXCHANGE_FILE_SIZE, 
                DATA_EXCHANGE_TYPE_ID, 
                DATA_EXCHANGE_DIRECTION,
                PROCESS_STATUS_CD,
                PROCESS_COMPLETE_TS)
              SELECT pn_data_exchange_file_id, 
                     CASE WHEN MAX(DATA_EXCHANGE_FILE_NAME) IS NULL THEN pv_data_exchange_file_name
                          WHEN MAX(DATA_EXCHANGE_FILE_NAME) = pv_data_exchange_file_name THEN lv_file_name_prefix || '_1' || lv_file_name_suffix
                          ELSE lv_file_name_prefix || '_' || (NVL(MAX(TO_NUMBER_OR_NULL(SUBSTR(DATA_EXCHANGE_FILE_NAME, LENGTH(lv_file_name_prefix) + 2, LENGTH(DATA_EXCHANGE_FILE_NAME) - LENGTH(pv_data_exchange_file_name) - 1))), 0) + 1) || lv_file_name_suffix
                     END,
                     pv_resource_key,
                     pn_data_exchange_file_size,
                     pn_data_exchange_type_id,
                     'O',
                     pc_process_status_cd,
                     CASE WHEN pc_process_status_cd = 'P' THEN MIN_DATE ELSE SYSDATE END
                FROM REPORT.DATA_EXCHANGE_FILE
               WHERE DATA_EXCHANGE_TYPE_ID = pn_data_exchange_type_id
                 AND DATA_EXCHANGE_FILE_NAME LIKE REGEXP_REPLACE(lv_file_name_prefix, '([_%\\])', '\\\1') || '%' || REGEXP_REPLACE(lv_file_name_suffix, '([_%\\])', '\\\1') ESCAPE '\';
            SELECT DATA_EXCHANGE_FILE_NAME
              INTO pv_data_exchange_file_name
              FROM REPORT.DATA_EXCHANGE_FILE
             WHERE DATA_EXCHANGE_FILE_ID = pn_data_exchange_file_id;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                CONTINUE;
        END;
        UPDATE REPORT.DATA_EXCHANGE_ITEM
           SET DATA_EXCHANGE_FILE_ID = pn_data_exchange_file_id,
               PROCESS_STATUS_CD = 'D',
               PROCESS_COMPLETE_TS = SYSDATE
         WHERE DATA_EXCHANGE_ITEM_ID MEMBER OF pt_dei_ids;
        EXIT;
   END LOOP;
END;
/