CREATE OR REPLACE TRIGGER report.trbu_ccs_transport_property
BEFORE UPDATE ON report.ccs_transport_property
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    :NEW.LAST_UPDATED_TS := SYSDATE;
    :NEW.LAST_UPDATED_BY := USER;
END;
/