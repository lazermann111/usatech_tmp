CREATE OR REPLACE PROCEDURE REPORT.UPDATE_CUST_USERS IS
BEGIN
	 INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
	 		SELECT USER_ID, TERMINAL_ID, 'Y' FROM TERMINAL T, CUSTOMER C
			WHERE T.CUSTOMER_ID = C.CUSTOMER_ID AND C.USER_ID IS NOT NULL AND
			NOT EXISTS(SELECT * FROM USER_TERMINAL UT WHERE UT.USER_ID = C.USER_ID AND UT.TERMINAL_ID = T.TERMINAL_ID);
END UPDATE_CUST_USERS;
/