CREATE OR REPLACE TRIGGER REPORT.TRBI_USER_REPORT 
BEFORE INSERT ON REPORT.USER_REPORT
FOR EACH ROW
DECLARE
	LV_DATE_FIELD CORP.FREQUENCY.DATE_FIELD%TYPE;
	LN_OFFSET NUMBER;
BEGIN
	IF :NEW.LATENCY IS NULL THEN
		SELECT LATENCY
		INTO :NEW.LATENCY
		FROM REPORT.REPORTS
		WHERE REPORT_ID = :NEW.REPORT_ID;
	END IF;
	
	IF :NEW.FREQUENCY_ID IS NOT NULL THEN
		SELECT DATE_FIELD, OFFSET_MS / 1000 / 60 / 60 / 24
		INTO LV_DATE_FIELD, LN_OFFSET
		FROM CORP.FREQUENCY
		WHERE FREQUENCY_ID = :NEW.FREQUENCY_ID;
	END IF;
	
	SELECT CASE WHEN LV_DATE_FIELD = 'WEEK' THEN
		TRUNC(TRUNC (SYSDATE, 'DY') + LN_OFFSET) - 1
	WHEN LV_DATE_FIELD = 'MONTH' THEN
		TRUNC (ADD_MONTHS (SYSDATE, -1), 'MM') 
	WHEN LV_DATE_FIELD = 'HOUR' THEN
		TRUNC(SYSDATE-2/24, 'HH')
	WHEN LV_DATE_FIELD = 'DAY' THEN
		TRUNC(SYSDATE-1, 'DD') + LN_OFFSET
	WHEN LV_DATE_FIELD = 'YEAR' THEN
		TRUNC(ADD_MONTHS(SYSDATE,-12), 'YY')
	WHEN LV_DATE_FIELD = 'PAY_PERIOD' THEN
		TRUNC(SYSDATE-30, 'DD')
	ELSE
		SYSDATE
	END 
	INTO :NEW.UPD_DT
	FROM DUAL;
	
	SELECT 
        	SYS_EXTRACT_UTC(SYSTIMESTAMP),
        	USER,
        	SYS_EXTRACT_UTC(SYSTIMESTAMP),
        	USER
    	INTO 
        	:NEW.CREATED_UTC_TS,
        	:NEW.CREATED_BY,
        	:NEW.LAST_UPDATED_UTC_TS,
        	:NEW.LAST_UPDATED_BY
    	FROM DUAL;
END;
/