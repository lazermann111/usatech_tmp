CREATE OR REPLACE PROCEDURE REPORT.RESEND_REPORTS_INS (
    l_user_report_id IN RESEND_REPORTS.USER_REPORT_ID%TYPE,
    l_batch_id IN RESEND_REPORTS.BATCH_ID%TYPE
)
   IS
    l_row_num NUMBER;
BEGIN
    SELECT COUNT(1)
    INTO l_row_num
    FROM RESEND_REPORTS
    WHERE USER_REPORT_ID = l_user_report_id AND BATCH_ID = l_batch_id;
    
    IF l_row_num = 0 THEN
        INSERT INTO RESEND_REPORTS(USER_REPORT_ID, BATCH_ID)
        VALUES(l_user_report_id, l_batch_id);
    END IF;
END;
/