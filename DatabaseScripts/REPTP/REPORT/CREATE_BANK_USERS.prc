CREATE OR REPLACE PROCEDURE REPORT.CREATE_BANK_USERS IS
  	CURSOR C IS
	  SELECT CB.CUSTOMER_BANK_ID, USER_ID FROM CUSTOMER_BANK_TERMINAL CBT, CUSTOMER_BANK CB, CUSTOMER C
	  		 WHERE CBT.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND CB.CUSTOMER_ID = C.CUSTOMER_ID;
   	l_user_id USER_LOGIN.USER_ID%TYPE;
	l_cb_id CUSTOMER.CUSTOMER_ID%TYPE;
BEGIN
	OPEN C;
    LOOP
	  FETCH C INTO l_cb_id, l_user_id;
      EXIT WHEN C%NOTFOUND;
	  BEGIN
	  	   INSERT INTO USER_CUSTOMER_BANK(USER_ID, CUSTOMER_BANK_ID)
		   		  VALUES(l_user_id, l_cb_id);
		   COMMIT;
	  EXCEPTION
	  	WHEN DUP_VAL_ON_INDEX THEN
		-- do nothing
		   NULL;
		WHEN OTHERS THEN
			 RAISE;
	  END;
	END LOOP;
END CREATE_BANK_USERS;
/