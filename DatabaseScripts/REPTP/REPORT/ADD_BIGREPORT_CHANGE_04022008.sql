-- insert property type for 1. email, 2. sftp, 3. ftp, 5. ftps to have "As Zip File" option, 4. soap will not have this option
INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 1, 'As Zip File', 'N','CHECKBOX');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 2, 'As Zip File', 'N','CHECKBOX');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 3, 'As Zip File', 'N','CHECKBOX');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 5, 'As Zip File', 'N','CHECKBOX');

commit;

-- create stored_file table and seq
@@STORED_FILE.tab
@@SEQ_STORED_FILE_ID.seq
@@TRBI_STORED_FILE.trg
@@TRBU_STORED_FILE.trg

-- add file_id and file_passcode to report_sent
ALTER TABLE
   REPORT.report_sent
ADD
   (stored_file_id	NUMBER,
    stored_file_passcode	VARCHAR2 (30));

ALTER TABLE REPORT.report_sent ADD CONSTRAINT FK_RS_STORED_FILE_ID FOREIGN KEY (STORED_FILE_ID) REFERENCES REPORT.STORED_FILE(STORED_FILE_ID);

GRANT select on REPORT.STORED_FILE to USALIVE_APP;
GRANT select on REPORT.REPORT_SENT to USALIVE_APP;

-- create the transport type EmailLink
insert into report.ccs_transport_type values(6, 'Email With Link');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_REGEX)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 6, 'Email', 'Y','/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/');

INSERT INTO report.ccs_transport_property_type
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_REGEX)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 6, 'CC', 'N','/(^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$)|^$/');

commit;
