CREATE OR REPLACE PROCEDURE REPORT.RECORD_DATA_EXCHANGE_FILE(
    pn_data_exchange_type_id REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_TYPE_ID%TYPE,
    pv_resource_key REPORT.DATA_EXCHANGE_FILE.RESOURCE_KEY%TYPE,
    pn_data_exchange_file_size REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_SIZE%TYPE,
    pv_data_exchange_file_name REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_NAME%TYPE,
    pn_data_exchange_file_id OUT REPORT.DATA_EXCHANGE_FILE.DATA_EXCHANGE_FILE_ID%TYPE,
    pc_process_status_cd OUT REPORT.DATA_EXCHANGE_FILE.PROCESS_STATUS_CD%TYPE)
IS
BEGIN
    FOR i IN 1..10 LOOP
        SELECT MAX(DATA_EXCHANGE_FILE_ID), MAX(PROCESS_STATUS_CD)
          INTO pn_data_exchange_file_id, pc_process_status_cd
          FROM REPORT.DATA_EXCHANGE_FILE
         WHERE DATA_EXCHANGE_TYPE_ID = pn_data_exchange_type_id
           AND DATA_EXCHANGE_FILE_NAME = pv_data_exchange_file_name
           AND DATA_EXCHANGE_DIRECTION = 'I';
           
        IF pn_data_exchange_file_id IS NOT NULL THEN
            RETURN;
        END IF;
        BEGIN
            SELECT REPORT.SEQ_DATA_EXCHANGE_FILE_ID.NEXTVAL, 'P'
              INTO pn_data_exchange_file_id, pc_process_status_cd
              FROM DUAL;
    
            INSERT INTO REPORT.DATA_EXCHANGE_FILE(
                DATA_EXCHANGE_FILE_ID, 
                DATA_EXCHANGE_FILE_NAME, 
                RESOURCE_KEY, 
                DATA_EXCHANGE_FILE_SIZE, 
                DATA_EXCHANGE_TYPE_ID, 
                DATA_EXCHANGE_DIRECTION) 
              VALUES( 
                pn_data_exchange_file_id, 
                pv_data_exchange_file_name,
                pv_resource_key,
                pn_data_exchange_file_size,
                pn_data_exchange_type_id,
                'I');
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                CONTINUE;
        END;
        EXIT;
   END LOOP;
END;
/