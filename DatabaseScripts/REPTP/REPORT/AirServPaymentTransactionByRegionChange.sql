--add outputtype
insert into folio_conf.output_type (output_type_id, output_type_label, output_type_category, output_type_key, content_type)
values(33, 'AirServ Payment Trans XML', 'Report', 'xsl:resource:simple/falcon/templates/report/custom/airserv-payment-trans.xsl', 'text/xml');

--create mask function to allow flexible lead and ending reserve
CREATE OR REPLACE FUNCTION REPORT.MASK_STR (
    l_str IN VARCHAR,
    l_lead IN NUMBER,
    l_end IN NUMBER
    )
RETURN VARCHAR PARALLEL_ENABLE DETERMINISTIC IS
BEGIN
    IF l_lead+l_end >= LENGTH(l_str) THEN
         RETURN RPAD('*',LENGTH(l_str), '*');
     ELSE
        RETURN SUBSTR(l_str, 1, l_lead)||RPAD('*', LENGTH(l_str) - l_lead-l_end, '*') || SUBSTR(l_str, -l_end);
     END IF;  
END;
/

GRANT EXECUTE ON REPORT.MASK_STR TO USALIVE_APP;

commit;