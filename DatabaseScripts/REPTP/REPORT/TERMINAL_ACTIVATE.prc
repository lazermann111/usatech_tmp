CREATE OR REPLACE PROCEDURE REPORT.TERMINAL_ACTIVATE (L_TERMINAL_ID OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
   l_eport_serial_nbr IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
   l_dealer_id IN NUMBER,
   l_asset IN REPORT.TERMINAL.ASSET_NBR%TYPE,
   l_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
   l_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
   l_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
   l_location IN REPORT.LOCATION.LOCATION_NAME%TYPE,
   l_loc_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
   l_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
   l_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
   l_state IN REPORT.TERMINAL_ADDR.STATE%TYPE,
   l_zip IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
   l_cust_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   l_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
   l_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
   l_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
   l_loc_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
   l_loc_type_spec IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
   l_prod_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
   l_prod_type_spec IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
   l_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
   l_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
   l_tz_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
   l_dex IN REPORT.TERMINAL.DEX_DATA%TYPE,
   l_receipt REPORT.TERMINAL.RECEIPT%TYPE,
   l_vends REPORT.TERMINAL.VENDS%TYPE
   )
   --L_TERMINAL_ID, L_EPORT_SERIAL_NBR, L_DEALER_ID, L_ASSET, L_MACHINE_ID, L_TELEPHONE, L_PREFIX, L_LOCATION,
   --L_ADDRESS1, L_CITY, L_STATE, L_ZIP, L_CUST_BANK_ID, L_USER_ID,
   -- primary contact, secondary contact, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY, PRODUCT_TYPE_ID, PRODUCT_TYPE_ID,
   -- auth mode, avgTransAmt, timeZone, dexData, receipt
IS
  l_eport_id EPORT.EPORT_ID%TYPE;
  l_dev_type_id EPORT.DEVICE_TYPE_ID%TYPE;
  l_location_id TERMINAL.LOCATION_ID%TYPE;
  l_terminal_nbr TERMINAL.TERMINAL_NBR%TYPE;
  l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
  l_license_id NUMBER;
  l_addr_id TERMINAL_ADDR.ADDRESS_ID%TYPE;
  l_activate_date DATE := SYSDATE;
  l_last_deactivate_date DATE;
  l_start_date DATE;
  l_cnt NUMBER;
  l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
  l_lock VARCHAR2(128);
BEGIN
     BEGIN
          SELECT TERMINAL_SEQ.NEXTVAL, EPORT_ID, DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL
             INTO l_terminal_id, l_eport_id, l_dev_type_id, l_addr_id FROM EPORT WHERE EPORT_SERIAL_NUM = l_eport_serial_nbr;
     EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
          WHEN OTHERS THEN
               RAISE;
     END;
	 
	 l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);
	 
     -- CHECK THAT EPORT IS INACTIVE
         SELECT NVL(MAX(NVL(END_DATE, MAX_DATE)), MIN_DATE)
           INTO l_last_deactivate_date
           FROM REPORT.TERMINAL_EPORT 
           WHERE EPORT_ID = l_eport_id;
         /*
     SELECT COUNT(TERMINAL_ID) 
           INTO l_cnt 
           FROM TERMINAL_EPORT 
           WHERE EPORT_ID = l_eport_id 
           AND NVL(END_DATE, MAX_DATE) > l_activate_date;
           */
     IF l_last_deactivate_date > l_activate_date THEN
          SELECT COUNT(TERMINAL_ID) INTO l_cnt FROM USER_TERMINAL WHERE USER_ID = l_user_id AND
             TERMINAL_ID IN(SELECT TERMINAL_ID FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, MAX_DATE) > l_activate_date);
         IF l_cnt > 0 THEN --USER CAN VIEW TERMINAL
              RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
         ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
              RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
         END IF;
     END IF;
     --VERIFY DEALER
     SELECT COUNT(DEALER_ID) INTO l_cnt FROM CORP.DEALER_EPORT WHERE DEALER_ID = l_dealer_id AND EPORT_ID = l_eport_id;
     IF l_cnt = 0 THEN
          RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
     END IF;
     --CREATE TERMINAL_NBR
     SELECT 'T0'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
     
     --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR
     IF l_cust_bank_id <> 0 THEN
         BEGIN
              SELECT CUSTOMER_ID INTO l_customer_id FROM CORP.CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
              WHEN OTHERS THEN
                   RAISE;
         END;
     ELSE
          BEGIN
              SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = l_pc_id AND CUSTOMER_ID <> 0;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
              WHEN OTHERS THEN
                   RAISE;
         END;
     END IF;
     BEGIN
          SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CORP.CUSTOMER_LICENSE CL, CORP.LICENSE_NBR LN
              WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
     EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
          WHEN OTHERS THEN
               RAISE;
     END;
     BEGIN
          SELECT LICENSE_ID 
            INTO l_license_id 
            FROM (SELECT LICENSE_ID 
                    FROM CORP.DEALER_LICENSE 
                   WHERE DEALER_ID = l_dealer_id
                     AND SYSDATE >= NVL(START_DATE, MIN_DATE)
                     AND SYSDATE < NVL(END_DATE, MAX_DATE)
                   ORDER BY START_DATE DESC, LICENSE_ID DESC)
           WHERE ROWNUM = 1;
     EXCEPTION
          WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this dealer');
          WHEN OTHERS THEN
               RAISE;
     END;    
     INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP)
         VALUES(l_addr_id, l_customer_id, l_address1, l_city, l_state, l_zip);
     BEGIN
         SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE EQL(LOCATION_NAME, l_location) = -1 AND EPORT_ID =  l_eport_id AND TERMINAL_ID = 0;
         UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
               (SELECT l_loc_details, l_addr_id, SYSDATE, l_terminal_id, l_loc_type_id,l_loc_type_spec FROM DUAL) WHERE LOCATION_ID = l_location_id;
     EXCEPTION
          WHEN NO_DATA_FOUND THEN
              SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
               INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                      VALUES(l_location_id, l_location, l_loc_details, l_addr_id, l_user_id, l_terminal_id, l_eport_id, l_loc_type_id, l_loc_type_spec);
          WHEN OTHERS THEN
               RAISE;
     END;
     l_business_unit_id := FIND_BUSINESS_UNIT(l_eport_id, l_customer_id);
     INSERT INTO TERMINAL(
            TERMINAL_ID,
            TERMINAL_NBR,
            TERMINAL_NAME,
            EPORT_ID,
            CUSTOMER_ID,
            LOCATION_ID,
            ASSET_NBR,
            MACHINE_ID,
            TELEPHONE,
            PREFIX,
            PRODUCT_TYPE_ID,
            PRODUCT_TYPE_SPECIFY,
            PRIMARY_CONTACT_ID,
            SECONDARY_CONTACT_ID,
            AUTHORIZATION_MODE,
            AVG_TRANS_AMT,
            TIME_ZONE_ID,
            DEX_DATA,
            RECEIPT,
            VENDS,
            BUSINESS_UNIT_ID,
            PAYMENT_SCHEDULE_ID)
         SELECT
            l_terminal_id,
            l_terminal_nbr,
            l_terminal_nbr,
            l_eport_id,
            l_customer_id,
            l_location_id,
            l_asset,
            l_machine_id,
            l_telephone,
            l_prefix,
            l_prod_type_id,
            l_prod_type_spec,
            l_pc_id,
            l_sc_id,
            l_auth_mode,
            l_avg_amt,
            l_tz_id,
            l_dex,
            l_receipt,
            l_vends,
            l_business_unit_id,
            bu.default_payment_schedule_id
          FROM CORP.BUSINESS_UNIT bu
        WHERE bu.BUSINESS_UNIT_ID = l_business_unit_id;
        
        -- Find start date
        SELECT /*+INDEX(x IX_TRANS_EPORT_ID) */
               GREATEST(l_last_deactivate_date, NVL(MIN(x.CLOSE_DATE), MIN_DATE), l_activate_date - 30)
          INTO l_start_date
          FROM REPORT.TRANS x
         WHERE x.EPORT_ID = l_eport_id
           AND x.CLOSE_DATE >= l_last_deactivate_date
           AND x.CLOSE_DATE > l_activate_date - 30; 
          
     --INSERT INTO TERMINAL_EPORT
     /* A much faster way - BSK 04/10/2007
     TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_start_date, NULL, NULL, 'Y');*/
     INSERT INTO REPORT.TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE)
        VALUES(l_eport_id, l_terminal_id, l_start_date, NULL);
    
     --UPDATE DEALER_EPORT
     UPDATE CORP.DEALER_EPORT SET ACTIVATE_DATE = l_activate_date WHERE DEALER_ID = l_dealer_id AND EPORT_ID = l_eport_id;
     --INSERT INTO USER_TERMINAL
     INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
     /* A much faster way - BSK 04/10/2007
          SELECT USER_ID, l_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(l_user_id, USER_ID) = 'Y'; */
         SELECT USER_ID, l_terminal_id, 'Y'
           FROM REPORT.USER_LOGIN u
           CONNECT BY PRIOR u.admin_id = u.user_id
           START WITH u.user_id = l_user_id
         UNION
         SELECT a.USER_ID, l_terminal_id, 'Y'
           FROM REPORT.USER_LOGIN a
          INNER JOIN REPORT.USER_PRIVS up
             ON a.USER_ID = up.user_id
          WHERE a.CUSTOMER_ID = l_customer_id
            AND up.priv_id IN(5,8);

     --INSERT INTO CUSTOMER_BANK_TERMINAL
     IF l_cust_bank_id <> 0 THEN
         INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
           VALUES(l_terminal_id, l_cust_bank_id, NULL); --l_start_date); : Use NULL (or MIN_DATE) instead
     END IF;
     --INSERT INTO SERVICE_FEES
     -- frequency interval so that first active day has a service fee (BSK 09-14-04)
     -- made start dates NULL (or MIN_DATE)
     INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, FEE_PERCENT)
          SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, lsf.FEE_ID,
                 DECODE(lsf.FEE_ID, 8, 0, lsf.AMOUNT), lsf.FREQUENCY_ID,
                 DECODE(lsf.START_IMMEDIATELY_FLAG, 'Y', l_start_date), l_start_date,
                 DECODE(lsf.FEE_ID, 8, lsf.AMOUNT, NULL)
            FROM CORP.LICENSE_SERVICE_FEES lsf, CORP.FREQUENCY f
           WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
             AND lsf.LICENSE_ID = l_license_id
             AND (l_dex != 'N' OR lsf.FEE_ID != 4);
     IF l_dex <> 'N' THEN
         INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
              SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4,
                     4.00, f.FREQUENCY_ID, NULL, l_start_date
                FROM CORP.FREQUENCY f
               WHERE f.FREQUENCY_ID = 2
                 AND NOT EXISTS(SELECT 1 
                                  FROM CORP.LICENSE_SERVICE_FEES lsf 
                                 WHERE lsf.LICENSE_ID = l_license_id
                                   AND lsf.FEE_ID = 4);
     END IF;
     --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
     INSERT INTO CORP.PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
         SELECT CORP.PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, NULL
         FROM CORP.LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
END TERMINAL_ACTIVATE; 
/