CREATE OR REPLACE PROCEDURE REPORT.TERMINAL_UPD_CUST_TERM_NBR (
   --l_term_id IN TERMINAL.TERMINAL_ID%TYPE,
   l_terminal_id IN TERMINAL.CUSTOMER_ID%TYPE,
   l_cust_term_nbr IN TERMINAL.CUST_TERMINAL_NBR%TYPE)
IS

BEGIN

	UPDATE 		   TERMINAL
	SET 		   CUST_TERMINAL_NBR = l_cust_term_nbr
	WHERE 		   terminal_id = l_terminal_id;

	EXCEPTION
		WHEN OTHERS THEN
	      ROLLBACK;
		  RAISE;

END TERMINAL_UPD_CUST_TERM_NBR; /* End of update terminal.cust_terminal_nbr */
/