CREATE OR REPLACE FUNCTION REPORT.MATCH_CAMPAIGN (
    l_campaign_id REPORT.CAMPAIGN.campaign_id%TYPE,
    l_time TIMESTAMP)
RETURN CHAR IS
    l_count NUMBER:=0;
    l_return CHAR;
BEGIN
	select count(1) into l_count from report.campaign where campaign_id=l_campaign_id and cast(l_time as DATE) >=NVL(START_DATE, MIN_DATE) and cast(l_time as DATE)<=NVL(END_DATE, MAX_DATE);
	IF l_count >0 THEN
		select REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(RECUR_SCHEDULE, l_time) into l_return  from report.campaign where campaign_id=l_campaign_id;
		return l_return;
	ELSE
		return 'N';
	END IF;
END;
/
