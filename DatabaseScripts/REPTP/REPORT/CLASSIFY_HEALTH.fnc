CREATE OR REPLACE FUNCTION REPORT.CLASSIFY_HEALTH(
    pd_last_comm_date DATE,
    pd_last_cashless_date DATE,
    pd_last_cash_date DATE,
    pd_last_dex_date DATE,
    pn_terminal_fee_status CHAR,
    pn_rental_fee_status CHAR,    
    pn_no_comm_days_min PLS_INTEGER,
    pn_no_comm_days_max PLS_INTEGER,
    pn_no_cashless_days_min PLS_INTEGER,
    pn_no_cashless_days_max PLS_INTEGER,
    pn_no_cash_days_min PLS_INTEGER,
    pn_no_cash_days_max PLS_INTEGER,
    pn_no_dex_days_min PLS_INTEGER,
    pn_no_dex_days_max PLS_INTEGER)
    RETURN VARCHAR2
    DETERMINISTIC
    PARALLEL_ENABLE
IS
    lv_classification VARCHAR2(20);
BEGIN
    SELECT CASE 
            WHEN TERMINAL_FEE_STATUS != 'A' AND RENTAL_FEE_STATUS != 'A' AND (CALL_IN_HEALTH = 'D' OR (CASHLESS_HEALTH = 'D' AND CASH_HEALTH = 'D' AND DEX_HEALTH = 'D')) THEN 'INVENTORY'
            WHEN CALL_IN_HEALTH = 'Y' THEN 'NO_COMM'
            WHEN CALL_IN_HEALTH = 'A' AND CASHLESS_HEALTH = 'Y' THEN 'NO_CASHLESS'
            WHEN CALL_IN_HEALTH IN('D', 'O') AND RENTAL_FEE_STATUS = 'A' THEN 'INACTIVE_RENTAL'
            WHEN CALL_IN_HEALTH IN('D', 'O') AND TERMINAL_FEE_STATUS = 'A' THEN 'INACTIVE_OWNED'
            WHEN CALL_IN_HEALTH IN('D', 'O') THEN 'INACTIVE'
            ELSE 'ACTIVE'
          END
     INTO lv_classification
     FROM (
    SELECT REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_comm_date, pn_no_comm_days_min, pn_no_comm_days_max) CALL_IN_HEALTH,
           REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_cashless_date, pn_no_cashless_days_min, pn_no_cashless_days_max) CASHLESS_HEALTH,
           REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_cash_date, pn_no_cash_days_min, pn_no_cash_days_max) CASH_HEALTH,
           --REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_fill_date, pn_no_fill_days_min, pn_no_fill_days_max) FILL_HEALTH,
           REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_dex_date, pn_no_dex_days_min, pn_no_dex_days_max) DEX_HEALTH,
           pn_terminal_fee_status TERMINAL_FEE_STATUS,
           pn_rental_fee_status RENTAL_FEE_STATUS
      FROM DUAL);
    RETURN lv_classification;
END;
/