CREATE OR REPLACE PROCEDURE REPORT.CREATE_MISSING_DEX_FOR_FILL
AS
    ld_last_date DATE;
    ld_next_date DATE;
    ld_processed_date DATE;
    ln_processed_count PLS_INTEGER;
    ln_delay FLOAT := 1/24;
    ln_max_items PLS_INTEGER := 100;
    CURSOR l_cur (ld_start_date DATE, ld_end_date DATE) IS
        SELECT * FROM (
        SELECT D.DEVICE_SERIAL_CD, F.FILL_DATE, F.CREATE_DATE
          FROM REPORT.FILL F
          JOIN REPORT.EPORT E ON F.EPORT_ID = E.EPORT_ID
          LEFT JOIN G4OP.DEX_FILE DF ON F.EPORT_ID = DF.EPORT_ID AND DF.DEX_DATE BETWEEN F.FILL_DATE - (5 / (24 * 60)) AND F.FILL_DATE + (5 / (24 * 60))
          JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON E.EPORT_SERIAL_NUM = DLA.DEVICE_SERIAL_CD
          JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
          JOIN DEVICE.DEVICE_SETTING DS_DEX ON D.DEVICE_ID = DS_DEX.DEVICE_ID AND DS_DEX.DEVICE_SETTING_PARAMETER_CD = CASE 
                        WHEN D.DEVICE_TYPE_ID IN(0) THEN '172' 
                        WHEN D.DEVICE_TYPE_ID IN(1) AND DBADMIN.VERSION_COMPARE(SUBSTR(D.FIRMWARE_VERSION, 9),'6.0.7') < 0 THEN '172' 
                        WHEN D.DEVICE_TYPE_ID IN(1) THEN '358'
                        WHEN D.DEVICE_TYPE_ID IN(13) THEN '1101' 
                        WHEN D.DEVICE_TYPE_ID IN(11) AND D.DEVICE_SUB_TYPE_ID IN(3) THEN 'DEX Schedule' END
          LEFT JOIN DEVICE.DEVICE_SETTING DS_385 ON DS_DEX.DEVICE_SETTING_PARAMETER_CD = '172' AND D.DEVICE_ID = DS_385.DEVICE_ID AND DS_385.DEVICE_SETTING_PARAMETER_CD = '385'
         WHERE F.CREATE_DATE BETWEEN ld_start_date AND ld_end_date
           AND DF.DEX_FILE_ID IS NULL
           AND TRIM(DS_DEX.DEVICE_SETTING_VALUE) IS NOT NULL 
           AND TRIM(DS_DEX.DEVICE_SETTING_VALUE) != 'FFFF'
           AND (DS_DEX.DEVICE_SETTING_PARAMETER_CD IN('1101', 'DEX Schedule')
            OR (DS_DEX.DEVICE_SETTING_PARAMETER_CD = '358' AND TO_NUMBER_OR_NULL(TRIM(DS_DEX.DEVICE_SETTING_VALUE)) IS NOT NULL)
            OR (DS_DEX.DEVICE_SETTING_PARAMETER_CD = '172' AND TO_NUMBER_OR_NULL(TRIM(DS_DEX.DEVICE_SETTING_VALUE)) IS NOT NULL AND TO_NUMBER_OR_NULL(DS_385.DEVICE_SETTING_VALUE) = 59))
         ORDER BY F.CREATE_DATE, F.FILL_ID) 
         WHERE ROWNUM <= ln_max_items;
BEGIN
    SELECT TO_DATE(APP_SETTING_VALUE, 'MM/DD/YYYY HH24:MI:SS'), SYSDATE - ln_delay
      INTO ld_last_date, ld_next_date
      FROM CORP.APP_SETTING
     WHERE APP_SETTING_CD = 'MISSING_DEX_FOR_FILL_LAST_TS';
    IF ld_last_date > ld_next_date THEN
        RETURN;
    END IF;
    LOOP
        ln_processed_count := 0;
        FOR l_rec IN l_cur(ld_last_date, ld_next_date) LOOP
            RDW_LOADER.PKG_LOAD_DATA.CREATE_DEX_FOR_FILL(l_rec.DEVICE_SERIAL_CD, l_rec.FILL_DATE);
            ld_processed_date := l_rec.CREATE_DATE;
            ln_processed_count := ln_processed_count + 1;
        END LOOP;
        UPDATE CORP.APP_SETTING
           SET APP_SETTING_VALUE = TO_CHAR(CASE WHEN ln_processed_count < ln_max_items THEN ld_next_date ELSE ld_processed_date END, 'MM/DD/YYYY HH24:MI:SS')
         WHERE APP_SETTING_CD = 'MISSING_DEX_FOR_FILL_LAST_TS'
           AND APP_SETTING_VALUE = TO_CHAR(ld_last_date, 'MM/DD/YYYY HH24:MI:SS');
        COMMIT;        
        IF ln_processed_count < ln_max_items THEN
            EXIT;
        END IF;
        ld_last_date := ld_processed_date;
    END LOOP;
END;
/