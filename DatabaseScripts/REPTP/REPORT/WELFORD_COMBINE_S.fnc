CREATE OR REPLACE FUNCTION REPORT.WELFORD_COMBINE_S (
  a_n NUMBER,
  a_x NUMBER,
  a_s NUMBER,
  b_n NUMBER,
  b_x NUMBER,
  b_s NUMBER
)
RETURN NUMBER IS
  a_m NUMBER;
  b_m NUMBER;
  delta NUMBER;
BEGIN
  a_m := a_x/a_n;
  b_m := b_x/b_n;
  delta := b_m - a_m;
  return (a_s + b_s + (delta*delta) * a_n * b_n / (a_n + b_n));
END;
/
