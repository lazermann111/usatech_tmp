CREATE OR REPLACE PROCEDURE REPORT.CREATE_EXPORT_BATCH_FOR_SPEC (
   --l_term_id IN TERMINAL.TERMINAL_ID%TYPE,
   l_cust_id IN TERMINAL.CUSTOMER_ID%TYPE)
IS
   CURSOR C IS
	SELECT DISTINCT CUSTOMER_ID FROM ACTIVITY_REF A, TERMINAL T WHERE BATCH_ID = 0 AND A.TERMINAL_ID = T.TERMINAL_ID
	AND T.CUSTOMER_ID = l_cust_id;
	--AND (T.CUSTOMER_ID = l_cust_id OR T.TERMINAL_ID = l_term_id);
	--AND T.CUSTOMER_ID = NVL(l_cust_id,T.CUSTOMER_ID) AND T.TERMINAL_ID = NVL(l_term_id,T.TERMINAL_ID);
   l_id TERMINAL.CUSTOMER_ID%TYPE;
   l_bat_id EXPORT_BATCH.BATCH_ID%TYPE;
   min_dt DATE;
   max_dt DATE;
   tot_tran EXPORT_BATCH.TOT_TRAN_ROWS%TYPE;
   tot_amount EXPORT_BATCH.TOT_TRAN_AMOUNT%TYPE;
BEGIN
	BEGIN
	    OPEN C;
	    LOOP
		  FETCH C INTO l_id;
	      EXIT WHEN C%NOTFOUND;
		  BEGIN
		      -- getthe next batch number, create a record in export_batch, and assign all transactions without a batch to this new batch
		      SELECT EXPORT_BATCH_SEQ.NEXTVAL INTO l_bat_id FROM DUAL;
			  INSERT INTO EXPORT_BATCH(BATCH_ID,CUSTOMER_ID,EXPORT_TYPE)
	          		 VALUES (l_bat_id,l_id,1);
	      	  UPDATE ACTIVITY_REF A SET BATCH_ID = l_bat_id WHERE BATCH_ID = 0 AND A.TERMINAL_ID IN (SELECT T.TERMINAL_ID FROM TERMINAL T WHERE CUSTOMER_ID = l_id);
			  -- Generate the stats from the trans table
		      SELECT MIN(TRAN_DATE), MAX(TRAN_DATE), COUNT(*), SUM(TOTAL_AMOUNT) INTO min_dt, max_dt, tot_tran, tot_amount FROM ACTIVITY_REF WHERE BATCH_ID = l_bat_id;
		      -- Update the export_batch details into the table
		      UPDATE EXPORT_BATCH
		       SET TRAN_CREATE_DT_BEG = min_dt,
		           TRAN_CREATE_DT_END = max_dt,
		           TOT_TRAN_ROWS = tot_tran,
		           TOT_TRAN_AMOUNT = tot_amount
		       WHERE
		           BATCH_ID = l_bat_id;
		      COMMIT;
	   	  END;
		END LOOP;
		CLOSE C;
	EXCEPTION
		WHEN OTHERS THEN
	      ROLLBACK;
		  RAISE;
    END;
END CREATE_EXPORT_BATCH_FOR_SPEC; /* End of create_export */
/