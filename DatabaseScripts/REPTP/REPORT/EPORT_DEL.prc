CREATE OR REPLACE PROCEDURE REPORT.EPORT_DEL    (l_eport_id IN EPORT.EPORT_ID%TYPE)
IS
  l_cnt NUMBER;
BEGIN
	 SELECT COUNT(*) INTO l_cnt FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id;
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20241, 'This e-Port has been assigned to a terminal; You can not delete it.');
	 ELSE
	 	 DELETE FROM EPORT WHERE EPORT_ID = l_eport_id;
	 END IF;
END EPORT_DEL;
/