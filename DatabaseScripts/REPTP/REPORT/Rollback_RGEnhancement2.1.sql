-- drop report_request and report_request_user table
drop trigger report.trbi_report_request;
drop trigger report.trbu_report_request;
drop trigger report.trbi_report_request_user;
drop trigger report.trbu_report_request_user;
drop table report.report_request;
drop sequence report.seq_report_request_id;
drop table report.report_request_user;
drop sequence report.seq_report_request_user_id;
drop table report.report_request_status;

delete from web_content.literal where literal_key='report-please-wait';
delete from web_content.literal where literal_key='report-cancel-confirm';
delete from web_content.literal where literal_key='report-cancel-pending-confirm';
delete from web_content.literal where literal_key='report-cancel-generated-confirm';
delete from web_content.literal where literal_key='report-pick-up-later';
delete from web_content.literal where literal_key='report-view-error';

delete from web_content.web_link_requirement 
where web_link_id=(select web_link_id from web_content.web_link 
where web_link_label='Recent Reports');

delete from web_content.web_link 
where web_link_label='Recent Reports';
commit;