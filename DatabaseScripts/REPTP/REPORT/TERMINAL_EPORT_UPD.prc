CREATE OR REPLACE PROCEDURE REPORT.TERMINAL_EPORT_UPD (L_TERMINAL_ID IN TERMINAL_EPORT.TERMINAL_ID%TYPE,
   l_eport_id IN TERMINAL_EPORT.EPORT_ID%TYPE,
   l_start_date IN TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE,
   l_end_date IN TERMINAL_EPORT.END_DATE%TYPE DEFAULT NULL,
   l_terminal_eport_id IN TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE DEFAULT NULL,
   l_override IN CHAR DEFAULT 'N',
   l_activate_detail IN TERMINAL_EPORT.ACTIVATE_DETAIL%TYPE DEFAULT NULL,
   l_deactivate_detail IN TERMINAL_EPORT.DEACTIVATE_DETAIL%TYPE DEFAULT NULL,
   l_activate_detail_id IN TERMINAL_EPORT.ACTIVATE_DETAIL_ID%TYPE DEFAULT NULL,
   l_deactivate_detail_id IN TERMINAL_EPORT.DEACTIVATE_DETAIL_ID%TYPE DEFAULT NULL)
IS
    l_tran_id TRANS.TRAN_ID%TYPE;
	l_lock VARCHAR2(128);
	ln_inactive_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
	ln_device_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE;
	ln_payment_schedule_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
	lv_eport_serial_num REPORT.EPORT.EPORT_SERIAL_NUM%TYPE;
	ld_fill_date REPORT.FILL.FILL_DATE%TYPE;
	l_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
	ln_sales_rep_id REPORT.SALES_REP.SALES_REP_ID%TYPE;
	lv_device_serial_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE;
	ln_comm_event_id NUMBER;
	ln_new_sales_rep_id NUMBER;
BEGIN
    --CHECK if there are paid transactions that will be switched to a different terminal
    IF l_override <> 'Y' THEN
        IF l_terminal_eport_id IS NULL THEN
            SELECT MIN(L.TRANS_ID)
              INTO l_tran_id
              FROM LEDGER L, BATCH B, TRANS T, CORP.DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.TRANS_ID = T.TRAN_ID
               AND T.EPORT_ID = l_eport_id
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND T.CLOSE_DATE >= NVL(l_start_date, MIN_DATE)
               AND T.CLOSE_DATE < NVL(l_end_date, MAX_DATE)
               AND NVL(B.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0);
        ELSE
            SELECT MIN(L.TRANS_ID)
              INTO l_tran_id
              FROM LEDGER L, BATCH B, TRANS T, TERMINAL_EPORT TE, CORP.DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.TRANS_ID = T.TRAN_ID
               AND TE.TERMINAL_EPORT_ID = l_terminal_eport_id
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND ((T.EPORT_ID = l_eport_id
               AND T.CLOSE_DATE >= NVL(l_start_date, MIN_DATE)
               AND T.CLOSE_DATE < NVL(l_end_date, MAX_DATE)
               AND NVL(B.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0))
                    OR (T.EPORT_ID = TE.EPORT_ID
               AND T.CLOSE_DATE >= NVL(TE.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(TE.END_DATE, MAX_DATE))
               AND NVL(B.TERMINAL_ID, 0) <> NVL(TE.TERMINAL_ID, 0));
        END IF;
        IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
             RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which occurred after the end date on the eport you are deactivating, has already been paid.');
        END IF;
    END IF;
	
	l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);
	
    IF l_terminal_id IS NOT NULL THEN
	 	UPDATE LOCATION L SET TERMINAL_ID = l_terminal_id WHERE EPORT_ID = l_eport_id AND TERMINAL_ID = 0
		  AND NOT EXISTS(SELECT 1 FROM LOCATION L1 WHERE L1.LOCATION_ID <> L.LOCATION_ID AND
		  L1.LOCATION_NAME = L.LOCATION_NAME AND L1.TERMINAL_ID = l_terminal_id AND L1.EPORT_ID = l_eport_id);
	 END IF;
	 
    -- transfer the sales rep if possible
    IF l_terminal_id IS NOT NULL AND l_eport_id IS NOT NULL THEN
      SELECT MAX(CE.SALES_REP_ID)
      INTO ln_sales_rep_id
      FROM REPORT.VW_CURRENT_DEVICE_SALES_REP CE
      JOIN (
        SELECT EPORT_SERIAL_NUM 
        FROM (
          SELECT E.EPORT_SERIAL_NUM
          FROM REPORT.EPORT E
          JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
          WHERE TE.TERMINAL_ID = l_terminal_id
          AND NVL(TE.START_DATE, MIN_DATE) < l_start_date
          ORDER BY NVL(TE.END_DATE, MAX_DATE) DESC, NVL(TE.START_DATE, MAX_DATE) DESC
        ) WHERE ROWNUM = 1
      ) E ON E.EPORT_SERIAL_NUM = CE.DEVICE_SERIAL_CD;
      SELECT MAX(E.EPORT_SERIAL_NUM)
      INTO lv_device_serial_cd
      FROM REPORT.EPORT E
      WHERE E.EPORT_ID = l_eport_id;
      IF ln_sales_rep_id IS NOT NULL AND lv_device_serial_cd IS NOT NULL THEN
        REPORT.ADD_SALES_COMMISSION_EVENT(ln_comm_event_id, ln_new_sales_rep_id, lv_device_serial_cd, ln_sales_rep_id, 'ASS', l_start_date, NULL, NULL);
      END IF;
    END IF;

    -- we must update the others
    -- first update end_date
    UPDATE TERMINAL_EPORT SET END_DATE = NVL(l_start_date, MIN_DATE),
    DEACTIVATE_DETAIL=COALESCE (l_deactivate_detail, DEACTIVATE_DETAIL),
    DEACTIVATE_DETAIL_ID=l_deactivate_detail_id
     WHERE EPORT_ID = l_eport_id
       AND TERMINAL_EPORT_ID <> NVL(l_terminal_eport_id, 0)
       AND ((NVL(END_DATE, MAX_DATE) > NVL(l_start_date, MIN_DATE)
       AND NVL(END_DATE, MAX_DATE) <= NVL(l_end_date, MAX_DATE))
       OR (NVL(END_DATE, MAX_DATE) >= NVL(l_end_date, MAX_DATE)
       AND NVL(START_DATE, MIN_DATE) <= NVL(l_start_date, MIN_DATE)))
	RETURNING MAX(TERMINAL_EPORT_ID) INTO ln_inactive_terminal_eport_id;
    -- now update start_date
    UPDATE TERMINAL_EPORT SET START_DATE = NVL(l_end_date, MAX_DATE)
     WHERE EPORT_ID = l_eport_id
       AND TERMINAL_EPORT_ID <> NVL(l_terminal_eport_id, 0)
       AND NVL(END_DATE, MAX_DATE) > NVL(l_start_date, MIN_DATE)
       AND NVL(END_DATE, MAX_DATE) <= NVL(l_end_date, MAX_DATE);
	   
	IF ln_inactive_terminal_eport_id IS NOT NULL AND l_terminal_id IS NULL AND l_terminal_eport_id IS NULL THEN
		SELECT E.EPORT_SERIAL_NUM, E.DEVICE_TYPE_ID, T.PAYMENT_SCHEDULE_ID, TE.END_DATE - 1/86400
		INTO lv_eport_serial_num, ln_device_type_id, ln_payment_schedule_id, ld_fill_date
		FROM REPORT.TERMINAL_EPORT TE
		JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		WHERE TE.TERMINAL_EPORT_ID = ln_inactive_terminal_eport_id;
	
		IF ln_payment_schedule_id = 2 THEN 
			-- Automatically create a manual fill record when a Fill-to-Fill terminal is deactivated
			SELECT PC.MINOR_CURRENCY_FACTOR into l_minor_currency_factor FROM REPORT.TERMINAL T 
			JOIN CORP.CURRENCY C ON T.FEE_CURRENCY_ID=C.CURRENCY_ID
			JOIN PSS.CURRENCY PC ON C.CURRENCY_CODE=PC.CURRENCY_CD
			JOIN REPORT.TERMINAL_EPORT TE ON T.TERMINAL_ID=TE.TERMINAL_ID
			WHERE TE.TERMINAL_EPORT_ID=ln_inactive_terminal_eport_id;
			
			RDW_LOADER.PKG_LOAD_DATA.LOAD_FILL(NULL, NULL, lv_eport_serial_num, ln_device_type_id, ld_fill_date,
				NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, l_minor_currency_factor);
		END IF;
	END IF;
       
	 IF l_terminal_eport_id IS NULL THEN
        IF l_eport_id IS NOT NULL AND l_terminal_id IS NOT NULL THEN
            -- insert new record
            INSERT INTO TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE,ACTIVATE_DETAIL,DEACTIVATE_DETAIL, ACTIVATE_DETAIL_ID, DEACTIVATE_DETAIL_ID)
                VALUES(l_eport_id, l_terminal_id, l_start_date, l_end_date,l_activate_detail,l_deactivate_detail,l_activate_detail_id,l_deactivate_detail_id );
        END IF;
     ELSE
        IF l_eport_id IS NOT NULL AND l_terminal_id IS NOT NULL THEN
            UPDATE TERMINAL_EPORT
            SET START_DATE = l_start_date, END_DATE = l_end_date,
                EPORT_ID = l_eport_id, TERMINAL_ID = l_terminal_id,
                ACTIVATE_DETAIL=COALESCE (l_activate_detail, ACTIVATE_DETAIL),
                DEACTIVATE_DETAIL=COALESCE (l_deactivate_detail, DEACTIVATE_DETAIL),
                ACTIVATE_DETAIL_ID=l_activate_detail_id,
                DEACTIVATE_DETAIL_ID=l_deactivate_detail_id
            WHERE TERMINAL_EPORT_ID = l_terminal_eport_id;
        ELSE
            DELETE FROM TERMINAL_EPORT WHERE TERMINAL_EPORT_ID = l_terminal_eport_id;
        END IF;
	 END IF;
	 IF l_terminal_id IS NULL THEN
		UPDATE REPORT.TERMINAL
		SET EPORT_ID = NULL
		WHERE EPORT_ID = l_eport_id;
	 ELSE
		UPDATE REPORT.TERMINAL T
		SET EPORT_ID = (
			SELECT MAX(TE.EPORT_ID)
			FROM REPORT.VW_TERMINAL_EPORT TE
			WHERE TE.TERMINAL_ID = T.TERMINAL_ID
		) WHERE TERMINAL_ID = l_terminal_id;
	 END IF;

END TERMINAL_EPORT_UPD;
/
