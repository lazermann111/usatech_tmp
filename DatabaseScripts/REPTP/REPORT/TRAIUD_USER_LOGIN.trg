CREATE OR REPLACE TRIGGER REPORT.TRAIUD_USER_LOGIN 
AFTER INSERT OR UPDATE OR DELETE ON REPORT.USER_LOGIN
FOR EACH ROW
BEGIN
	IF INSERTING THEN
		INSERT INTO REPORT.USER_TERMINAL
		VALUES(:NEW.USER_ID, 0, 'N');
	ELSIF (UPDATING AND :NEW.STATUS ='D') OR DELETING THEN
		DELETE FROM REPORT.USER_TERMINAL WHERE USER_ID=:NEW.USER_ID;
		DELETE FROM REPORT.USER_TERMINAL_CUSTOMER WHERE USER_ID=:NEW.USER_ID;
	END IF;
	
	IF INSERTING OR UPDATING THEN
		IF DBADMIN.PKG_UTL.EQL(:NEW.STATUS, :OLD.STATUS) = 'N' THEN
			INSERT INTO REPORT.USER_LOGIN_HIST(USER_ID, FIELD_CD, FIELD_VALUE)
			VALUES(:NEW.USER_ID, 'STATUS', :NEW.STATUS);
		END IF;
		IF :NEW.USER_TYPE != :OLD.USER_TYPE THEN
			INSERT INTO REPORT.USER_LOGIN_HIST(USER_ID, FIELD_CD, FIELD_VALUE)
			VALUES(:NEW.USER_ID, 'USER_TYPE', :NEW.USER_TYPE);
		END IF;
	END IF;
END;
/
