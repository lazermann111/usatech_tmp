-- delete As Zip File
delete from report.CCS_TRANSPORT_PROPERTY where CCS_TRANSPORT_PROPERTY_TYPE_ID in (
select CCS_TRANSPORT_PROPERTY_TYPE_ID from report.CCS_TRANSPORT_PROPERTY_TYPE where CCS_TPT_NAME='As Zip File');

delete from report.CCS_TRANSPORT_PROPERTY_TYPE where CCS_TPT_NAME='As Zip File';

-- delete EmailLink transport type
delete from report.CCS_TRANSPORT_PROPERTY where CCS_TRANSPORT_PROPERTY_TYPE_ID in 
(select CCS_TRANSPORT_PROPERTY_TYPE_ID from report.CCS_TRANSPORT_PROPERTY_TYPE where CCS_TRANSPORT_TYPE_ID =6 );
delete from report.CCS_TRANSPORT_PROPERTY_TYPE where CCS_TRANSPORT_TYPE_ID =6;
update report.ccs_transport set ccs_transport_type_id=1 where ccs_transport_type_id=6;
delete from report.ccs_transport where ccs_transport_type_id=6;
delete from report.ccs_transport_type where CCS_TRANSPORT_TYPE_ID=6; 

commit;

alter table
   report.report_sent
drop
   (stored_file_id, stored_file_passcode);

drop trigger report.trbi_stored_file;
drop trigger report.trbu_stored_file;
drop table report.stored_file;
drop sequence report.seq_stored_file_id;
