CREATE OR REPLACE PROCEDURE REPORT.TERMINAL_COLUMN_MAP_COPY_MODEL    (l_terminal_id IN TERMINAL_COLUMN_MAP.TERMINAL_ID%TYPE,
   l_model_terminal_id IN TERMINAL_COLUMN_MAP.TERMINAL_ID%TYPE)
IS
BEGIN
	 INSERT INTO TERMINAL_COLUMN_MAP(TERMINAL_ID, MDB_NUMBER, VEND_COLUMN)
	 		SELECT l_terminal_id TERMINAL_ID, MDB_NUMBER, VEND_COLUMN FROM TERMINAL_COLUMN_MAP WHERE TERMINAL_ID = l_model_terminal_id;
END TERMINAL_COLUMN_MAP_COPY_MODEL;
/