CREATE OR REPLACE FUNCTION REPORT.VEND_COLUMN_STRING(ATRAN_ID INT)
RETURN VARCHAR2 
IS
BEGIN
  DECLARE
    ln_device_type_id EPORT.DEVICE_TYPE_ID%TYPE;
    lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
    lc_no_price CHAR(1);
    str ACTIVITY_REF.VEND_COLUMN%TYPE := '';
    tmp VARCHAR2(4000);
    
    CURSOR l_cur IS
         SELECT NVL(MAPPED_COLUMN, NVL(DESCRIPTION, NVL(VEND_COLUMN, '#' || MDB_NUMBER))) LABEL, AMOUNT QTY, DECODE(lc_no_price, 'N', PRICE) PRICE
         FROM REPORT.PURCHASE P
         LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON P.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
         WHERE TRAN_ID = atran_id
           AND (IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL OR IT.TRAN_LINE_ITEM_TYPE_GROUP_CD NOT IN('U'))
         ORDER BY PURCHASE_ID;
  BEGIN
    SELECT E.DEVICE_TYPE_ID, NVL(C.CURRENCY_SYMBOL, '$'), CASE WHEN E.DEVICE_TYPE_ID = 0 OR (E.DEVICE_TYPE_ID = 1 AND SUM(P.AMOUNT * P.PRICE) != T.TOTAL_AMOUNT) THEN 'Y' ELSE 'N' END
    INTO ln_device_type_id, lc_currency_symbol, lc_no_price
    FROM REPORT.TRANS T
    JOIN REPORT.EPORT E ON T.EPORT_ID = E.EPORT_ID
    LEFT OUTER JOIN CORP.CURRENCY C ON T.CURRENCY_ID = C.CURRENCY_ID
    LEFT OUTER JOIN REPORT.PURCHASE P ON E.DEVICE_TYPE_ID = 1 AND P.TRAN_ID = T.TRAN_ID
    LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON P.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
   WHERE T.TRAN_ID = atran_id
     AND (IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL OR IT.TRAN_LINE_ITEM_TYPE_GROUP_CD NOT IN('U'))
    GROUP BY E.DEVICE_TYPE_ID, NVL(C.CURRENCY_SYMBOL, '$'), T.TOTAL_AMOUNT;
  
    FOR l_rec IN l_cur LOOP
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      IF l_rec.QTY != 1 OR l_rec.PRICE IS NOT NULL THEN 
          tmp := l_rec.LABEL || '(';
          IF l_rec.QTY != 1 THEN
            tmp := tmp || TO_CHAR(l_rec.QTY);
            IF l_rec.PRICE IS NOT NULL THEN
                 tmp := tmp || ' * ';
            END IF;
          END IF;
          IF l_rec.PRICE IS NOT NULL THEN
            IF l_rec.PRICE < 0 THEN
                tmp := tmp || '-';
            END IF;
            tmp := tmp || lc_currency_symbol || TRIM(TO_CHAR(ABS(l_rec.PRICE), '9,999,999,999,990.00'));
          END IF;
          tmp := tmp || ')';
      ELSE
        tmp := l_rec.LABEL;
      END IF;         
      IF LENGTH(str) + LENGTH(tmp) > 3995 THEN
        str := str || '...';
        EXIT;
      END IF;
      str := str || tmp;
    END LOOP;
    RETURN str;
  END;
END;
/