create or replace
function                                                   INSERT_MODEL(
make_id FRONT.HOST_MAKE.HOST_MAKE_ID%TYPE,
new_model FRONT.HOST_MODEL.HOST_MODEL_CD%TYPE,
prof_id  AAA.PROFILE.PROFILE_ID%TYPE)
return number
is
 pragma autonomous_transaction;
 model_id number;
 counter number;
begin
  SELECT count(*) INTO counter FROM front.HOST_MODEL where host_model_cd = new_model;
  if counter = 0 then
  insert into front.HOST_MODEL (host_make_id,host_model_cd) values (make_id,new_model) returning HOST_MODEL_ID into model_id;
  insert into front.HOST_MODEL_PRODUCT_LINE (PRODUCT_LINE_ID,HOST_MODEL_ID) 
(select distinct l.product_line_id, model_id
  from front.location l, aaa.profile p
  where p.customer_id=l.customer_id
  and p.profile_id=prof_id);
  else
  select host_model_id into model_id from front.HOST_MODEL where host_model_cd = new_model;
  end if;
commit;
return model_id;
end INSERT_MODEL;
