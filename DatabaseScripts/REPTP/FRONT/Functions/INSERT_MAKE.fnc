create or replace
function                 INSERT_MAKE(
new_make FRONT.HOST_MAKE.HOST_MAKE_CD%TYPE )
return number
is
 pragma autonomous_transaction;
 make_id number;
 counter number;
begin
  SELECT count(*) INTO counter FROM front.HOST_MAKE where host_make_cd = new_make;
  if counter = 0 then
  insert into front.HOST_MAKE (host_make_cd) values (new_make) returning HOST_MAKE_ID into make_id;
  else
  select host_make_id into make_id from front.HOST_MAKE where host_make_cd = new_make;
  end if;
commit;
return make_id;
end INSERT_MAKE;