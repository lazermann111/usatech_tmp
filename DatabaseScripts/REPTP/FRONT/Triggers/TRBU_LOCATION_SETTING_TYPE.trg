CREATE OR REPLACE TRIGGER FRONT.TRBU_LOCATION_SETTING_TYPE BEFORE UPDATE ON FRONT.LOCATION_SETTING_TYPE
  FOR EACH ROW
BEGIN
    SELECT :OLD.CREATED_TS,
           :OLD.CREATED_BY,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/