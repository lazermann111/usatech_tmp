CREATE OR REPLACE TRIGGER FRONT.TRBI_TIME_ZONE 
    BEFORE INSERT ON FRONT.TIME_ZONE FOR EACH ROW 
BEGIN 
    IF :NEW.TIME_ZONE_ID IS NULL THEN
        SELECT SEQ_TIME_ZONE_ID.NEXTVAL INTO :NEW.TIME_ZONE_ID FROM DUAL;
    END IF;
    SELECT SYSDATE,
        USER,
        SYSDATE,
        USER
    INTO :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;
ALTER TRIGGER "FRONT"."TRBI_TIME_ZONE" ENABLE
/
