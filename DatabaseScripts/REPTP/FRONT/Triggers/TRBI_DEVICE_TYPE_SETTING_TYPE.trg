CREATE OR REPLACE TRIGGER FRONT.TRBI_DEVICE_TYPE_SETTING_TYPE BEFORE INSERT ON FRONT.DEVICE_TYPE_SETTING_TYPE
  FOR EACH ROW
BEGIN
    SELECT SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;





ALTER TRIGGER "FRONT"."TRBI_DEVICE_TYPE_SETTING_TYPE" ENABLE
/
