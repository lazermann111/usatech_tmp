prompt Rolling back changes for bug # 793
update web_content.web_link
set web_link_usage = 'M'
where web_link_id in (140, 2140)
/

update web_content.web_link
set web_link_usage = 'm'
where web_link_id in (10408, 10409)
/

-- uncomment this and comment out rollback for rollback for bug #793
--commit;
rollback;

prompt Rolling back changes for bug # 529
update web_content.web_link
set web_link_usage = 'm'
where web_link_id in (10410)
/

-- uncomment this and commit rollback for rollback for bug #793
--commit;
rollback;

prompt Rolling back change for folio # 83
update folio_conf.folio_directive
set directive_value = '665px'
where folio_id = 83
and directive_id = 9
/

-- uncomment this and commit rollback for rollback for bug #793
--commit;
rollback;
