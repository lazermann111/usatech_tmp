
ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE SEQUENCE REPORT.MSG_LOG_SEQ
;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TYPE        STRING_ARRAY
 AS VARYING ARRAY (100) OF VARCHAR(100)

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TYPE        STRING_LIST
 AS TABLE OF VARCHAR(1000)

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TYPE T_TRAN_ITEM AS OBJECT(
        machine_item_num NUMBER,
        column_code VARCHAR(2000),
        quantity NUMBER(10),
        price NUMBER(15,2),
        product_desc VARCHAR(2000));

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE FUNCTION card_company   (card_number VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    ch CHAR(1);
    cn VARCHAR2(50);
	len NUMBER;
  BEGIN
    cn := TRIM(card_number);
    ch := SUBSTR(cn,1,1);
	len := INSTR(cn, '=') - 1;
	IF len = -1 THEN
	  len := LENGTH(cn);
	END IF;
    IF ch = '4' AND len = 16 THEN
      RETURN 'VISA';
    ELSIF ch = '5' AND len = 16 THEN
      RETURN 'MasterCard';
    ELSIF ch = '3' AND len = 15 THEN
      RETURN 'American Express';
    ELSIF ch = '6' AND len = 16 THEN
      RETURN 'Discover';
    ELSE
      RETURN 'Unknown';
    END IF;
  END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE FUNCTION get_fill_date   (l_tran_id TRANS.TRAN_ID%TYPE)
RETURN DATE IS
  l_fill_date FILL.FILL_DATE%TYPE;
BEGIN
  SELECT MIN(FILL_DATE) INTO l_fill_date FROM FILL F, TRANS X WHERE TRAN_ID = l_tran_id AND F.EPORT_ID = X.EPORT_ID AND CLOSE_DATE < FILL_DATE;
  RETURN l_fill_date;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE FUNCTION max_date
  RETURN  DATE DETERMINISTIC
IS
--
-- Returns the maximum oracle date
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- B Krug       09-17-04 NEW
    l_max_date CONSTANT DATE := TO_DATE('12/31/9999', 'MM/DD/YYYY');
BEGIN 
    RETURN l_max_date;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE FUNCTION min_date
  RETURN  DATE DETERMINISTIC
IS
--
-- Returns the minimum oracle date
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
-- B Krug       09-17-04 NEW
    l_min_date CONSTANT DATE := TO_DATE('01/01/-4712', 'MM/DD/SYYYY');
BEGIN 
    RETURN l_min_date;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE FUNCTION vend_column_string    (atran_id INT)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(255);
    vc PURCHASE.VEND_COLUMN%TYPE;
    qnt PURCHASE.PURCHASE_ID%TYPE;
    CURSOR cur IS
        SELECT VEND_COLUMN, COUNT(PURCHASE_ID) FROM REPORT.PURCHASE WHERE TRAN_ID = atran_id GROUP BY vend_column;
        /*SELECT VEND_COLUMN, AMOUNT FROM PURCHASE WHERE TRAN_ID = atran_id;*/
  BEGIN
    OPEN cur;
    LOOP
      FETCH cur INTO vc, qnt;
      EXIT WHEN cur%NOTFOUND;
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      str := str || vc;
      IF qnt > 1 THEN
        str := str || '(' || /*TRIM(TO_CHAR(*/qnt/*, '9999'))*/ || ')';
      END IF;
    END LOOP;
    RETURN str;
  END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE FUNCTION GET_ADJUST_DESC(
    l_eft_id DOC.DOC_ID%TYPE)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(4000);
    CURSOR cur IS
        SELECT DESCRIPTION, AMOUNT
          FROM LEDGER L, BATCH B
         WHERE L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_eft_id;
  BEGIN
    FOR rec IN cur LOOP
        IF LENGTH(str) > 0 THEN
            str := str || ', ';
        END IF;
        str := str || rec.DESCRIPTION || ' (' || TO_CHAR(rec.AMOUNT, 'FM$999,999,999,990.00') || ')';
    END LOOP;
    RETURN str;
  END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE FUNCTION last_tran_date
    (l_doc_id INT, l_terminal_id int)
RETURN date

/* This function returns the last tran date 
   for a specific terminal_id for a given EFT 
   
   Tom Shannon   6/6/02
*/

IS
    last_tran_dt DATE;
BEGIN
    SELECT MAX(LEDGER_DATE)
      INTO last_tran_dt
      FROM LEDGER L, BATCH B
     WHERE B.TERMINAL_ID = L_TERMINAL_ID
       AND B.DOC_ID = L_DOC_ID
       AND L.BATCH_ID = B.BATCH_ID;
    RETURN last_tran_dt;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE FUNCTION log_dir           (logtype in varchar2) return varchar2 is
    return_value UTL_DATA.utldata%TYPE := NULL;
begin
  if logtype is null then
    --return default
      return 'No utlname value supplied';
  else
    --look for correct value
      --DECLARE
      BEGIN
      select UTL_DATA.utldata into return_value from corp.UTL_DATA where utlname = logtype;
        EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
            return_value := 'Not Found';
      END;
  end if;
  return return_value;
end;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP FUNCTION CORP.VEND_COLUMN_STRING
/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE SYNONYM REPORT.BATCH
	FOR CORP.BATCH
;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE SYNONYM REPORT.BATCH_SEQ
	FOR CORP.BATCH_SEQ
;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE SYNONYM REPORT.DEVICE
	FOR REPORT.EPORT
;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE SYNONYM REPORT.PAYMENT_SCHEDULE
	FOR CORP.PAYMENT_SCHEDULE
;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE SYNONYM REPORT.SETTLE_STATE
	FOR REPORT.TRANS_STATE
;

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE SYNONYM CORP.FILL
	FOR REPORT.FILL
;

CREATE PUBLIC SYNONYM MAX_DATE
	FOR REPORT.MAX_DATE
;

CREATE PUBLIC SYNONYM MIN_DATE
	FOR REPORT.MIN_DATE
;

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE SYNONYM CORP.SETTLE_STATE
	FOR REPORT.TRANS_STATE
;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE        BATCH_SENT_UPD    (
     UR_ID IN REPORT_SENT.USER_REPORT_ID%TYPE,
	 B_ID IN NUMBER,
   	 SD IN REPORT_SENT.SENT_DATE%TYPE,
   	 E IN REPORT_SENT.EMAIL%TYPE)
IS
  	 T USER_REPORT.BATCH_TYPE_ID%TYPE;
	 N NUMBER;
BEGIN
	 INSERT INTO REPORT_SENT (REPORT_SENT_ID,USER_REPORT_ID,BATCH_ID,EXPORT_TYPE,SENT_DATE,EMAIL)
        VALUES(REPORT_SENT_SEQ.NEXTVAL,UR_ID,B_ID,T,SD,E);
	 DELETE FROM RESEND_REPORTS WHERE USER_REPORT_ID = USER_REPORT_ID AND BATCH_ID = BATCH_ID;
	 SELECT COUNT(*) INTO N FROM VW_UNSENT WHERE BATCH_ID = B_ID AND USER_REPORT_ID  = UR_ID;
	 IF N = 0 THEN
		 SELECT R.BATCH_TYPE_ID INTO T FROM USER_REPORT UR, REPORTS R WHERE UR.REPORT_ID = R.REPORT_ID AND USER_REPORT_ID = UR_ID;
	 	 IF T = 3 THEN -- EFT
		 	UPDATE CORP.DOC SET STATUS = 'S' WHERE DOC_ID = B_ID;
	 	 ELSIF T = 2 THEN -- DEX
		 	UPDATE DEX_FILE SET SENT = 'Y' WHERE DEX_FILE_ID = B_ID;
		 ELSE
		 	UPDATE EXPORT_BATCH SET EXPORT_SENT = SD, STATUS = 'S' WHERE BATCH_ID = B_ID AND EXPORT_TYPE = T;
		 END IF;
	 END IF;
END BATCH_SENT_UPD;

/
/*
ALTER SESSION SET CURRENT_SCHEMA = REPORT;
DROP PROCEDURE REPORT.GETUNSENTDEXFILES
/
*/
ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE LOG_MSG(
     l_type MSG_LOG.MSG_TYPE%TYPE,
     l_source MSG_LOG.MSG_SOURCE%TYPE,
     l_category MSG_LOG.MSG_CATEGORY%TYPE,
     l_text MSG_LOG.MSG_TEXT%TYPE,
     l_err_num MSG_LOG.ERROR_NUM%TYPE DEFAULT NULL)
   AS
PRAGMA AUTONOMOUS_TRANSACTION;
--
-- Adds a record to the MSG_LOG table
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- BKRUG        10/15/2004 NEW
BEGIN
    INSERT INTO MSG_LOG(MSG_TYPE, MSG_SOURCE, MSG_CATEGORY, MSG_TEXT, ERROR_NUM)
        VALUES(l_type, l_source, l_category, l_text, l_err_num);
    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
END; -- Procedure

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE refresh_activity_ref_mdh
IS
BEGIN

    dbms_output.put_line('Start update_trans_data ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

   -- LOAD DATA THAT MAPS EPORT_ID TO TERMINAL_ID
   update_trans_data;

   dbms_output.put_line('End (before commit) update_trans_data ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

   COMMIT;

   dbms_output.put_line('End (after commit) update_trans_data ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

   -- CALCULATE MERCHANT ID IF APPLICABLE (RADISYS ONLY)
   dbms_output.put_line('Start update_merchant_id ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   update_merchant_id;

   dbms_output.put_line('End (before commit) update_merchant_id ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

   COMMIT;

   dbms_output.put_line('End (after commit) update_merchant_id ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

   dbms_output.put_line('Start skip_trans_validate ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   -- CHECK FOR DUPLICATES
   skip_trans_validate; -- commit is inside this procedure
     dbms_output.put_line('End skip_trans_validate ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
     dbms_output.put_line('Start validate_all_trans ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   validate_all_trans; --commit is inside this procedure
   dbms_output.put_line('End validate_all_trans ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
     dbms_output.put_line('Start mark_definite_noncredit_dups ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   mark_definite_noncredit_dups; --commit is inside this procedure
     dbms_output.put_line('End (after commit) mark_definite_noncredit_dups ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

dbms_output.put_line('Start LOAD DATA INTO CORP ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   -- LOAD DATA INTO CORP
   INSERT INTO ledger
               (ledger_id,
                trans_id,
                eport_id,
                terminal_id,
                card_number,
                total_amount,
                cc_appr_code,
                start_date,
                close_date,
                server_date,
                track_data,
                trans_type_id,
                merchant_id)
      SELECT ledger_seq.NEXTVAL,
             t.tran_id,
             eport_id,
             terminal_id,
             card_number,
             total_amount,
             cc_appr_code,
             start_date,
             close_date,
             server_date,
             track_data,
             trans_type_id,
             merchant_id
        FROM trans t
       WHERE status = 'A'
         AND trans_type_id NOT IN (22);


dbms_output.put_line('End  LOAD DATA INTO CORP ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));



dbms_output.put_line('Start LOAD INTO ACTIVITY_REF ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   -- LOAD INTO ACTIVITY_REF
   INSERT INTO activity_ref
               (region_id,
                region_name,
                location_name,
                location_id,
                terminal_id,
                terminal_name,
                terminal_nbr,
                tran_id,
                trans_type_id,
                trans_type_name,
                card_number,
                card_company,
                cc_appr_code,
                tran_date,
                month_range,
                week_range,
                day_range,
                fill_date,
                total_amount,
                orig_tran_id,
                vend_column,
                quantity,
                ref_nbr,
                eport_id,
                description)
      SELECT region_id,
             region_name,
             location_name,
             location_id,
             terminal_id,
             terminal_name,
             terminal_nbr,
             tran_id,
             trans_type_id,
             trans_type_name,
             card_number,
             card_company,
             cc_appr_code,
             tran_date,
             month_range,
             week_range,
             day_range,
             fill_date,
             total_amount,
             orig_tran_id,
             vend_column,
             quantity,
             ref_nbr_seq.NEXTVAL,
             eport_id,
             description
        FROM vw_make_activity
       WHERE status = 'A';

dbms_output.put_line('End  LOAD INTO ACTIVITY_REF ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

dbms_output.put_line('Start UPDATE TRAN ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   UPDATE trans
      SET status = 'R'
    WHERE status = 'A';
dbms_output.put_line('End (Before Commit) UPDATE TRAN ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   COMMIT;
   dbms_output.put_line('End (After Commit) UPDATE TRAN ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

dbms_output.put_line('Start UPDATE CORP ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   -- UPDATE CORP
   UPDATE ledger l
      SET (eport_id, terminal_id, card_number, total_amount, cc_appr_code,
           start_date, close_date, server_date, track_data, trans_type_id,
           merchant_id) =
             (SELECT eport_id,
                     terminal_id,
                     card_number,
                     total_amount,
                     cc_appr_code,
                     start_date,
                     close_date,
                     server_date,
                     track_data,
                     trans_type_id,
                     merchant_id
                FROM trans t
               WHERE status = 'U'
                 AND l.trans_id = t.tran_id)
    WHERE trans_id IN (SELECT tran_id
                         FROM trans
                        WHERE status = 'U');

dbms_output.put_line('End  UPDATE CORP ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));


dbms_output.put_line('Start UPDATE ACTIVITY_REF ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   --UPDATE ACTIVITY_REF
   UPDATE activity_ref a
      SET (region_id, region_name, location_name, location_id, terminal_id,
           terminal_name, terminal_nbr, trans_type_id, trans_type_name,
           card_number, card_company, cc_appr_code, tran_date, month_range,
           week_range, day_range, fill_date, total_amount, orig_tran_id,
           vend_column, quantity, eport_id, description) =
             (SELECT region_id,
                     region_name,
                     location_name,
                     location_id,
                     terminal_id,
                     terminal_name,
                     terminal_nbr,
                     trans_type_id,
                     trans_type_name,
                     card_number,
                     card_company,
                     cc_appr_code,
                     tran_date,
                     month_range,
                     week_range,
                     day_range,
                     fill_date,
                     total_amount,
                     orig_tran_id,
                     vend_column,
                     quantity,
                     eport_id,
                     description
                FROM vw_make_activity m
               WHERE m.tran_id = a.tran_id)
    WHERE tran_id IN (SELECT tran_id
                        FROM trans
                       WHERE status = 'U');

dbms_output.put_line('End UPDATE ACTIVITY_REF ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

dbms_output.put_line('Start UPDATE TRANS ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   UPDATE trans
      SET status = 'R'
    WHERE status = 'U';

dbms_output.put_line('End (before commit) UPDATE TRANS ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
   COMMIT;

   dbms_output.put_line('End (after commit) UPDATE TRANS ' || TO_CHAR(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END refresh_activity_ref_mdh;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE refresh_activity_ref_new
IS

  /*--- Local Fields ------------------------------------------------------- */
		l_region_id 			   vw_make_activity.region_id%TYPE;
		l_region_name 			   vw_make_activity.region_name%TYPE;
		l_location_name			   vw_make_activity.location_name%TYPE;
		l_location_id			   vw_make_activity.location_id%TYPE;
		l_terminal_id			   vw_make_activity.terminal_id%TYPE;
		l_terminal_name			   vw_make_activity.terminal_name%TYPE;
		l_terminal_nbr			   vw_make_activity.terminal_nbr%TYPE;
		l_tran_id				   vw_make_activity.tran_id%TYPE;
		l_trans_type_id			   vw_make_activity.TRANS_TYPE_ID%TYPE;
		l_trans_type_name		   vw_make_activity.TRANS_TYPE_NAME%TYPE;
		l_card_number			   vw_make_activity.CARD_NUMBER%TYPE;
		l_card_company			   vw_make_activity.CARD_COMPANY%TYPE;
		l_cc_appr_code			   vw_make_activity.CC_APPR_CODE%TYPE;
		l_tran_date				   vw_make_activity.TRAN_DATE%TYPE;
		l_month_range			   vw_make_activity.MONTH_RANGE%TYPE;
		l_week_range			   vw_make_activity.WEEK_RANGE%TYPE;
		l_day_range				   vw_make_activity.DAY_RANGE%TYPE;
		l_fill_date				   vw_make_activity.FILL_DATE%TYPE;
		l_total_amount			   vw_make_activity.TOTAL_AMOUNT%TYPE;
		l_orig_tran_id			   vw_make_activity.ORIG_TRAN_ID%TYPE;
		l_vend_column			   vw_make_activity.VEND_COLUMN%TYPE;
		l_quantity				   vw_make_activity.QUANTITY%TYPE;
		l_eport_id				   vw_make_activity.EPORT_ID%TYPE;
		l_description			   vw_make_activity.DESCRIPTION%TYPE;
		l_ref_nbr			   	   activity_ref.REF_NBR%TYPE;

   CURSOR C1 IS
      SELECT region_id,
             region_name,
             location_name,
             location_id,
             terminal_id,
             terminal_name,
             terminal_nbr,
             tran_id,
             trans_type_id,
             trans_type_name,
             card_number,
             card_company,
             cc_appr_code,
             tran_date,
             month_range,
             week_range,
             day_range,
             fill_date,
             total_amount,
             orig_tran_id,
             vend_column,
             quantity,
             eport_id,
             description
        FROM vw_make_activity
       WHERE status = 'A';


   CURSOR C2 IS
	SELECT v.region_id,
	       v.region_name,
	       v.location_name,
	       v.location_id,
	       v.terminal_id,
	       v.terminal_name,
	       v.terminal_nbr,
		   v.tran_id,
	       v.trans_type_id,
	       v.trans_type_name,
	       v.card_number,
	       v.card_company,
	       v.cc_appr_code,
	       v.tran_date,
	       v.month_range,
	       v.week_range,
	       v.day_range,
	       v.fill_date,
	       v.total_amount,
	       v.orig_tran_id,
	       v.vend_column,
	       v.quantity,
	       v.eport_id,
	       v.description
	FROM   vw_make_activity v, trans t
	WHERE  t.tran_id = v.TRAN_ID
	and    t.STATUS= 'U';


BEGIN
   -- LOAD DATA THAT MAPS EPORT_ID TO TERMINAL_ID
   update_trans_data;
   COMMIT;
   -- CALCULATE MERCHANT ID IF APPLICABLE (RADISYS ONLY)
   update_merchant_id;
   COMMIT;
   -- CHECK FOR DUPLICATES
   skip_trans_validate; -- commit is inside this procedure
   validate_all_trans; --commit is inside this procedure
   mark_definite_noncredit_dups; --commit is inside this procedure

   -- LOAD DATA INTO CORP
   INSERT INTO ledger
               (ledger_id,
                trans_id,
                eport_id,
                terminal_id,
                card_number,
                total_amount,
                cc_appr_code,
                start_date,
                close_date,
                server_date,
                track_data,
                trans_type_id,
                merchant_id)
      SELECT ledger_seq.NEXTVAL,
             t.tran_id,
             eport_id,
             terminal_id,
             card_number,
             total_amount,
             cc_appr_code,
             start_date,
             close_date,
             server_date,
             track_data,
             trans_type_id,
             merchant_id
        FROM trans t
       WHERE status = 'A'
         AND trans_type_id NOT IN (22);

	COMMIT;


   -- LOAD INTO ACTIVITY_REF
	BEGIN
	    OPEN C1;
	    LOOP
		  FETCH C1 INTO l_region_id, l_region_name, l_location_name, l_location_id, l_terminal_id,
             	   		l_terminal_name, l_terminal_nbr, l_tran_id, l_trans_type_id, l_trans_type_name,
             			l_card_number, l_card_company, l_cc_appr_code, l_tran_date, l_month_range,
             			l_week_range, l_day_range, l_fill_date, l_total_amount, l_orig_tran_id,
             			l_vend_column, l_quantity, l_eport_id, l_description;

		  EXIT WHEN C1%NOTFOUND;
		  BEGIN
		      -- getthe next batch number, create a record in export_batch, and assign all transactions without a batch to this new batch
				SELECT ref_nbr_seq.NEXTVAL INTO l_ref_nbr FROM DUAL;

				INSERT INTO activity_ref
				            (region_id,
				             region_name,
				             location_name,
				             location_id,
				             terminal_id,
				             terminal_name,
				             terminal_nbr,
				             tran_id,
				             trans_type_id,
				             trans_type_name,
				             card_number,
				             card_company,
				             cc_appr_code,
				             tran_date,
				             month_range,
				             week_range,
				             day_range,
				             fill_date,
				             total_amount,
				             orig_tran_id,
				             vend_column,
				             quantity,
				             ref_nbr,
				             eport_id,
				             description)
						VALUES (l_region_id,
					             l_region_name,
					             l_location_name,
					             l_location_id,
					             l_terminal_id,
					             l_terminal_name,
					             l_terminal_nbr,
					             l_tran_id,
					             l_trans_type_id,
					             l_trans_type_name,
					             l_card_number,
					             l_card_company,
					             l_cc_appr_code,
					             l_tran_date,
					             l_month_range,
					             l_week_range,
					             l_day_range,
					             l_fill_date,
					             l_total_amount,
					             l_orig_tran_id,
					             l_vend_column,
					             l_quantity,
								 l_ref_nbr,
					             l_eport_id,
					             l_description);
		      	COMMIT;
	   	  END;

		END LOOP;

		CLOSE C1;

	EXCEPTION
		WHEN OTHERS THEN
	      ROLLBACK;
    END;

  UPDATE trans
      SET status = 'R'
    WHERE status = 'A';

   COMMIT;

   -- UPDATE CORP
   UPDATE ledger l
      SET (eport_id, terminal_id, card_number, total_amount, cc_appr_code,
           start_date, close_date, server_date, track_data, trans_type_id,
           merchant_id) =
             (SELECT eport_id,
                     terminal_id,
                     card_number,
                     total_amount,
                     cc_appr_code,
                     start_date,
                     close_date,
                     server_date,
                     track_data,
                     trans_type_id,
                     merchant_id
                FROM trans t
               WHERE status = 'U'
                 AND l.trans_id = t.tran_id)
    WHERE trans_id IN (SELECT tran_id
                         FROM trans
                        WHERE status = 'U');

   --UPDATE ACTIVITY_REF
	BEGIN
	    OPEN C2;
	    LOOP
		  FETCH C2 INTO l_region_id, l_region_name, l_location_name, l_location_id, l_terminal_id,
             	   		l_terminal_name, l_terminal_nbr, l_tran_id, l_trans_type_id, l_trans_type_name,
             			l_card_number, l_card_company, l_cc_appr_code, l_tran_date, l_month_range,
             			l_week_range, l_day_range, l_fill_date, l_total_amount, l_orig_tran_id,
             			l_vend_column, l_quantity, l_eport_id, l_description;

		  EXIT WHEN C2%NOTFOUND;
		  BEGIN

				UPDATE activity_ref
					   SET 	region_id = l_region_id,
					   		region_name = l_region_name,
							location_name = l_location_name,
							location_id = l_location_id,
							terminal_id = l_terminal_id,
				            terminal_name = l_terminal_name,
							terminal_nbr = l_terminal_nbr,
							tran_id = l_tran_id,
							trans_type_id = l_trans_type_id,
							trans_type_name = l_trans_type_name,
				            card_number = l_card_number,
							card_company = l_card_company,
							cc_appr_code = l_cc_appr_code,
							tran_date = l_tran_date,
							month_range = l_month_range,
				            week_range = l_week_range,
							day_range = l_day_range,
							fill_date = l_fill_date,
							total_amount = l_total_amount,
							orig_tran_id = l_orig_tran_id,
				            vend_column = l_vend_column,
							quantity = l_quantity,
							ref_nbr = l_ref_nbr,
							eport_id = l_eport_id,
							description = l_description
						WHERE tran_id = l_tran_id;
		      	COMMIT;
	   	  END;

		END LOOP;

		CLOSE C1;

	EXCEPTION
		WHEN OTHERS THEN
	      ROLLBACK;
    END;

   UPDATE trans
      SET status = 'R'
    WHERE status = 'U';

   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
END refresh_activity_ref_new;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE TERMINAL_ACTIVATE
   (l_terminal_id OUT TERMINAL.TERMINAL_ID%TYPE,
   l_eport_serial_nbr IN EPORT.EPORT_SERIAL_NUM%TYPE,
   l_dealer_id IN NUMBER,
   l_asset IN TERMINAL.ASSET_NBR%TYPE,
   l_machine_id IN TERMINAL.MACHINE_ID%TYPE,
   l_telephone IN TERMINAL.TELEPHONE%TYPE,
   l_prefix IN TERMINAL.PREFIX%TYPE,
   l_location IN LOCATION.LOCATION_NAME%TYPE,
   l_loc_details IN LOCATION.DESCRIPTION%TYPE,
   l_address1 IN TERMINAL_ADDR.ADDRESS1%TYPE,
   l_city IN TERMINAL_ADDR.CITY%TYPE,
   l_state IN TERMINAL_ADDR.STATE%TYPE,
   l_zip IN TERMINAL_ADDR.ZIP%TYPE,
   l_cust_bank_id IN USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   l_user_id IN USER_LOGIN.USER_ID%TYPE,
   l_pc_id IN TERMINAL.PRIMARY_CONTACT_ID%TYPE,
   l_sc_id IN TERMINAL.SECONDARY_CONTACT_ID%TYPE,
   l_loc_type_id IN LOCATION.LOCATION_TYPE_ID%TYPE,
   l_loc_type_spec IN LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
   l_prod_type_id IN TERMINAL.PRODUCT_TYPE_ID%TYPE,
   l_prod_type_spec IN TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
   l_auth_mode IN TERMINAL.AUTHORIZATION_MODE%TYPE,
   l_avg_amt IN TERMINAL.AVG_TRANS_AMT%TYPE,
   l_tz_id IN TERMINAL.TIME_ZONE_ID%TYPE,
   l_dex IN TERMINAL.DEX_DATA%TYPE,
   l_receipt TERMINAL.RECEIPT%TYPE,
   l_vends TERMINAL.VENDS%TYPE
   )
   --L_TERMINAL_ID, L_EPORT_SERIAL_NBR, L_DEALER_ID, L_ASSET, L_MACHINE_ID, L_TELEPHONE, L_PREFIX, L_LOCATION,
   --L_ADDRESS1, L_CITY, L_STATE, L_ZIP, L_CUST_BANK_ID, L_USER_ID,
   -- primary contact, secondary contact, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY, PRODUCT_TYPE_ID, PRODUCT_TYPE_ID,
   -- auth mode, avgTransAmt, timeZone, dexData, receipt
IS
  l_eport_id EPORT.EPORT_ID%TYPE;
  l_dev_type_id EPORT.DEVICE_TYPE_ID%TYPE;
  l_location_id TERMINAL.LOCATION_ID%TYPE;
  l_terminal_nbr TERMINAL.TERMINAL_NBR%TYPE;
  l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
  l_license_id NUMBER;
  l_addr_id TERMINAL_ADDR.ADDRESS_ID%TYPE;
  l_date DATE := SYSDATE;
  l_cnt NUMBER;
BEGIN
     BEGIN
	 	 SELECT TERMINAL_SEQ.NEXTVAL, EPORT_ID, DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL
		     INTO l_terminal_id, l_eport_id, l_dev_type_id, l_addr_id FROM EPORT WHERE EPORT_SERIAL_NUM = l_eport_serial_nbr;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 -- CHECK THAT EPORT IS INACTIVE
	 SELECT COUNT(TERMINAL_ID) INTO l_cnt FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, l_date) >= l_date;
	 IF l_cnt > 0 THEN
	 	 SELECT COUNT(TERMINAL_ID) INTO l_cnt FROM USER_TERMINAL WHERE USER_ID = l_user_id AND
		 	TERMINAL_ID IN(SELECT TERMINAL_ID FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, l_date) >= l_date);
		 IF l_cnt > 0 THEN --USER CAN VIEW TERMINAL
	 	 	RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
		 ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
	 	 	RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
		 END IF;
	 END IF;
	 --VERIFY DEALER
	 SELECT COUNT(DEALER_ID) INTO l_cnt FROM DEALER_EPORT WHERE DEALER_ID = l_dealer_id AND EPORT_ID = l_eport_id;
	 IF l_cnt = 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
	 END IF;
	 --CREATE TERMINAL_NBR
	 IF l_dev_type_id = 1 OR l_dev_type_id = 0 THEN --RADISYS, DEFAULT
	     SELECT 'E5'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
	 ELSIF l_dev_type_id = 2 THEN --G4
	     SELECT COUNT(*) INTO l_cnt FROM TERMINAL WHERE TERMINAL_NBR = l_eport_serial_nbr;
	     IF l_cnt > 0 THEN
		     SELECT 'E4'|| TO_CHAR(TERMINAL_NBR_G4_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
		 ELSE
		     l_terminal_nbr := l_eport_serial_nbr;
		 END IF;
	 ELSIF l_dev_type_id = 3 THEN --G5
	     SELECT 'G5'|| TO_CHAR(TERMINAL_NBR_G5_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
     ELSIF l_dev_type_id = 4 THEN                                                       --M1 = MEI
         SELECT COUNT (*) INTO l_cnt FROM terminal WHERE terminal_nbr = l_eport_serial_nbr;
         IF l_cnt > 0 THEN
            SELECT 'M1' || TO_CHAR (TERMINAL_NBR_M1_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
         ELSE
            l_terminal_nbr := l_eport_serial_nbr;
         END IF;
	 END IF;
	 --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR
	 IF l_cust_bank_id <> 0 THEN
		 BEGIN
		 	 SELECT CUSTOMER_ID INTO l_customer_id FROM CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
		 EXCEPTION
		 	 WHEN NO_DATA_FOUND THEN
		 	 	 RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
		 	 WHEN OTHERS THEN
		 	 	 RAISE;
		 END;
	 ELSE
	 	 BEGIN
		 	 SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = l_pc_id AND CUSTOMER_ID <> 0;
		 EXCEPTION
		 	 WHEN NO_DATA_FOUND THEN
		 	 	 RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
		 	 WHEN OTHERS THEN
		 	 	 RAISE;
		 END;
	 END IF;
	 BEGIN
	 	 SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CUSTOMER_LICENSE CL, LICENSE_NBR LN
	     	 WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP)
	     VALUES(l_addr_id, l_customer_id, l_address1, l_city, l_state, l_zip);
	 BEGIN
	 	SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE EQL(LOCATION_NAME, l_location) = -1 AND EPORT_ID =  l_eport_id AND TERMINAL_ID = 0;
	 	UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
			   (SELECT l_loc_details, l_addr_id, SYSDATE, l_terminal_id, l_loc_type_id,l_loc_type_spec FROM DUAL) WHERE LOCATION_ID = l_location_id;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
		 	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	 	 	 INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
	     	 		VALUES(l_location_id, l_location, l_loc_details, l_addr_id, l_user_id, l_terminal_id, l_eport_id, l_loc_type_id, l_loc_type_spec);
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 INSERT INTO TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, EPORT_ID, CUSTOMER_ID, LOCATION_ID, ASSET_NBR,
	     MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY, PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID,
		 AUTHORIZATION_MODE, AVG_TRANS_AMT, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS)
		 VALUES(l_terminal_id, l_terminal_nbr, l_terminal_nbr, l_eport_id, l_customer_id, l_location_id, l_asset,
		 l_machine_id, l_telephone, l_prefix, l_prod_type_id, l_prod_type_spec, l_pc_id, l_sc_id,
		 l_auth_mode, l_avg_amt, l_tz_id, l_dex, l_receipt, l_vends);
	 --INSERT INTO TERMINAL_EPORT
	 TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_date, NULL, NULL);
	 --UPDATE DEALER_EPORT
	 UPDATE DEALER_EPORT SET ACTIVATE_DATE = l_date WHERE DEALER_ID = l_dealer_id AND EPORT_ID = l_eport_id;
	 --INSERT INTO USER_TERMINAL
	 INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
		 SELECT USER_ID, l_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(l_user_id, USER_ID) = 'Y';
	 --INSERT INTO CUSTOMER_BANK_TERMINAL
	 IF l_cust_bank_id <> 0 THEN
	 	INSERT INTO CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
	       VALUES(l_terminal_id, l_cust_bank_id, NULL); --l_date); : Use NULL (or MIN_DATE) instead
	 END IF;
	 --INSERT INTO SERVICE_FEES
     -- frequency interval so that first active day has a service fee (BSK 09-14-04)
     -- made start dates NULL (or MIN_DATE)
	 INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	 SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, lsf.FEE_ID,
                lsf.AMOUNT, lsf.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), NULL
		 FROM LICENSE_SERVICE_FEES lsf, FREQUENCY f
         WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
           AND lsf.LICENSE_ID = l_license_id;
	 IF l_dex <> 'N' THEN
	 	INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	    SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4,
                   4.00, f.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), NULL
		      FROM FREQUENCY f
             WHERE f.FREQUENCY_ID = 2;
	 END IF;
	 --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
	     SELECT PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, NULL
		 FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
END TERMINAL_ACTIVATE;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE terminal_edit
   (l_terminal_id IN TERMINAL.TERMINAL_ID%TYPE,
   l_asset IN TERMINAL.ASSET_NBR%TYPE,
   l_machine_id IN TERMINAL.MACHINE_ID%TYPE,
   l_telephone IN TERMINAL.TELEPHONE%TYPE,
   l_prefix IN TERMINAL.PREFIX%TYPE,
   l_location IN LOCATION.LOCATION_NAME%TYPE,
   l_loc_details IN LOCATION.DESCRIPTION%TYPE,
   l_address_id TERMINAL_ADDR.ADDRESS_ID%TYPE,
   l_address1 IN TERMINAL_ADDR.ADDRESS1%TYPE,
   l_city IN TERMINAL_ADDR.CITY%TYPE,
   l_state IN TERMINAL_ADDR.STATE%TYPE,
   l_zip IN TERMINAL_ADDR.ZIP%TYPE,
   l_cust_bank_id IN USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   l_user_id IN USER_LOGIN.USER_ID%TYPE,
   l_pc_id IN TERMINAL.PRIMARY_CONTACT_ID%TYPE,
   l_sc_id IN TERMINAL.SECONDARY_CONTACT_ID%TYPE,
   l_loc_type_id IN LOCATION.LOCATION_TYPE_ID%TYPE,
   l_loc_type_spec IN LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
   l_prod_type_id IN TERMINAL.PRODUCT_TYPE_ID%TYPE,
   l_prod_type_spec IN TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
   l_auth_mode IN TERMINAL.AUTHORIZATION_MODE%TYPE,
   l_tz_id IN TERMINAL.TIME_ZONE_ID%TYPE,
   l_dex IN TERMINAL.DEX_DATA%TYPE,
   l_receipt TERMINAL.RECEIPT%TYPE,
   l_vends TERMINAL.VENDS%TYPE
   )
IS
  l_location_id TERMINAL.LOCATION_ID%TYPE;
  l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
  l_old_cb_id NUMBER;
  l_old_lname LOCATION.LOCATION_NAME%TYPE;
  l_old_ldesc LOCATION.DESCRIPTION%TYPE;
  l_new_address_id TERMINAL_ADDR.ADDRESS_ID%TYPE;
  l_old_address_id LOCATION.ADDRESS_ID%TYPE;
  l_old_dex TERMINAL.DEX_DATA%TYPE;
  isnew BOOLEAN;
  l_date DATE := SYSDATE;
BEGIN
	 -- if a new location is named than create it
	 SELECT T.LOCATION_ID, T.CUSTOMER_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CUSTOMER_BANK_ID, DEX_DATA
	 INTO l_location_id, l_customer_id, l_old_lname, l_old_ldesc, l_old_address_id, l_old_cb_id, l_old_dex
	 FROM TERMINAL T, LOCATION L, VW_CURRENT_BANK_ACCT CB
	 WHERE T.TERMINAL_ID = l_terminal_id AND T.LOCATION_ID = L.LOCATION_ID (+) AND T.TERMINAL_ID = CB.TERMINAL_ID (+);
	 IF EQL(l_old_lname, l_location) <> -1 THEN  -- location has changed
	 	UPDATE LOCATION SET status = 'D' WHERE LOCATION_ID = l_location_id;
		INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'LOCATION',l_old_lname, l_location);
	    BEGIN
		   SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE LOCATION_NAME = l_location AND TERMINAL_ID = l_terminal_id;
		EXCEPTION
		   WHEN NO_DATA_FOUND THEN
		   	  isnew := true;
		   WHEN OTHERS THEN
		      RAISE;
	    END;
	 END IF;
	 --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
	 DECLARE
	     l_t TERMINAL.TELEPHONE%TYPE;
		 l_p TERMINAL.PREFIX%TYPE;
		 l_a TERMINAL.AUTHORIZATION_MODE%TYPE;
		 l_z TERMINAL.TIME_ZONE_ID%TYPE;
		 l_d TERMINAL.DEX_DATA%TYPE;
		 l_r TERMINAL.RECEIPT%TYPE;
		 l_v TERMINAL.VENDS%TYPE;
		 l_cnt NUMBER;
	 BEGIN
	 	 SELECT TELEPHONE, PREFIX, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS INTO
		 	l_t, l_p, l_a, l_z, l_d, l_r, l_v FROM TERMINAL WHERE TERMINAL_ID = l_terminal_id;
		 IF EQL(l_t, l_telephone) <> -1 THEN
		 	UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_telephone WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'TELEPHONE' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   	  VALUES(l_terminal_id, 'TELEPHONE',l_t, l_telephone);
			END IF;
		 END IF;
		 IF EQL(l_p, l_prefix) <> -1 THEN
	 		UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_prefix WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'PREFIX' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'PREFIX',l_p, l_prefix);
			END IF;
		 END IF;
		 IF EQL(l_a, l_auth_mode) <> -1 THEN
	 		UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_auth_mode WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'AUTHORIZATION_MODE' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'AUTHORIZATION_MODE',l_a, l_auth_mode);
			END IF;
		 END IF;
		 IF EQL(l_z, l_tz_id) <> -1 THEN
	 		UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_tz_id WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'TIME_ZONE',l_z, l_tz_id);
			END IF;
		 END IF;
		 IF EQL(l_d, l_dex) <> -1 THEN
	 		UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_dex WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'DEX_DATA' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'DEX_DATA',l_d, l_dex);
			END IF;
		 END IF;
		 IF EQL(l_r, l_receipt) <> -1 THEN
	 		UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_receipt WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'RECEIPT' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'RECEIPT',l_r, l_receipt);
			END IF;
		 END IF;
		 IF EQL(l_v, l_vends) <> -1 THEN
	 		UPDATE TERMINAL_CHANGES SET NEW_VALUE = l_vends WHERE TERMINAL_ID = l_terminal_id
			   AND ATTRIBUTE = 'VENDS' RETURNING 1 INTO l_cnt;
			IF NVL(l_cnt, 0) = 0 THEN
	 		   INSERT INTO TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
			   VALUES(l_terminal_id, 'VENDS',l_v, l_vends);
			END IF;
		 END IF;
	 END;
	 -- UPDATE ADDRESS
	 IF l_address_id IS NULL THEN	-- none specified
	    l_new_address_id := NULL;
	 ELSIF l_address_id = 0 THEN	--IS NEW
		SELECT TERMINAL_ADDR_SEQ.NEXTVAL INTO l_new_address_id FROM DUAL;
		INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP)
		   	VALUES(l_new_address_id, l_customer_id, l_address1, l_city, l_state, l_zip);
	 ELSE
	 	UPDATE TERMINAL_ADDR SET ADDRESS1 = l_address1, CITY = l_city, STATE = l_state, ZIP = l_zip
			WHERE ADDRESS_ID = l_address_id AND (EQL(ADDRESS1, l_address1) = 0
			OR EQL(CITY, l_city) = 0 OR EQL(STATE, l_state) = 0 OR EQL(ZIP, l_zip) = 0);
		l_new_address_id := l_address_id;
	 END IF;
	 IF isnew THEN
	 	SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	 	INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
        	   VALUES(l_location_id,l_location,l_loc_details,l_new_address_id,l_user_id,l_terminal_id,l_loc_type_id,l_loc_type_spec);
	 ELSE
	 	UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
			   (SELECT l_loc_details,l_new_address_id,l_date,l_loc_type_id,l_loc_type_spec FROM DUAL) WHERE location_id = l_location_id;
	 END IF;
 	 UPDATE TERMINAL SET (LOCATION_ID, ASSET_NBR, MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY,
	 	PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS, STATUS) =
		(SELECT l_location_id, l_asset, l_machine_id, l_telephone, l_prefix, l_prod_type_id, l_prod_type_spec,
		 l_pc_id, l_sc_id, l_auth_mode, l_tz_id, l_dex, l_receipt, l_vends, 'U' FROM DUAL) WHERE TERMINAL_ID = l_terminal_id;
	 --UPDATE BANK ACCOUNT
	 IF NVL(l_old_cb_id, 0) = 0 AND NVL(l_cust_bank_id, 0) <> 0 THEN
	 	DECLARE
		     l_cb_date DATE;
		BEGIN
			 SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = l_terminal_id AND CLOSE_DATE >=
		     	(SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CUSTOMER_BANK_TERMINAL
			 	WHERE TERMINAL_ID = l_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
			 TERMINAL_REASSIGN_BANK_ACCOUNT(l_terminal_id, l_cust_bank_id, l_cb_date);
	    END;
	 ELSIF EQL(l_old_cb_id, l_cust_bank_id) = 0 THEN
	 	TERMINAL_REASSIGN_BANK_ACCOUNT(l_terminal_id, l_cust_bank_id, l_date);
	 END IF;
	 IF l_old_dex = 'N' AND l_dex IN('D','A')  THEN
		 INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
		 	 SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
	 ELSIF l_dex = 'N' AND l_old_dex IN('D','A')  THEN
	 	 UPDATE SERVICE_FEES SET END_DATE = l_date WHERE TERMINAL_ID = l_terminal_id
		 	 AND FEE_ID = 4;
	 END IF;
EXCEPTION
     WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
	 WHEN OTHERS THEN
		 ROLLBACK;
		 RAISE;
END TERMINAL_EDIT;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE terminal_eport_upd
   (l_terminal_id IN TERMINAL_EPORT.TERMINAL_ID%TYPE,
   l_eport_id IN TERMINAL_EPORT.EPORT_ID%TYPE,
   l_start_date IN TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE,
   l_end_date IN TERMINAL_EPORT.END_DATE%TYPE DEFAULT NULL,
   l_terminal_eport_id IN TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE DEFAULT NULL)
IS
    l_tran_id TRANS.TRAN_ID%TYPE;
BEGIN
    --CHECK if there are paid transactions that will be switched to a different terminal
    IF l_terminal_eport_id IS NULL THEN
        SELECT MIN(L.TRANS_ID)
          INTO l_tran_id
          FROM LEDGER L, BATCH B, TRANS T
         WHERE L.BATCH_ID = B.BATCH_ID
           AND L.TRANS_ID = T.TRAN_ID
           AND T.EPORT_ID = l_eport_id
           AND B.CLOSED = 'Y'
           AND T.CLOSE_DATE >= NVL(l_start_date, MIN_DATE)
           AND T.CLOSE_DATE < NVL(l_end_date, MAX_DATE)
           AND NVL(B.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0);
    ELSE
        SELECT MIN(L.TRANS_ID)
          INTO l_tran_id
          FROM LEDGER L, BATCH B, TRANS T, TERMINAL_EPORT TE
         WHERE L.BATCH_ID = B.BATCH_ID
           AND L.TRANS_ID = T.TRAN_ID
           AND TE.TERMINAL_EPORT_ID = l_terminal_eport_id
           AND B.CLOSED = 'Y'
           AND ((T.EPORT_ID = l_eport_id
           AND T.CLOSE_DATE >= NVL(l_start_date, MIN_DATE)
           AND T.CLOSE_DATE < NVL(l_end_date, MAX_DATE)
           AND NVL(B.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0))
                OR (T.EPORT_ID = TE.EPORT_ID
           AND T.CLOSE_DATE >= NVL(TE.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(TE.END_DATE, MAX_DATE))
           AND NVL(B.TERMINAL_ID, 0) <> NVL(TE.TERMINAL_ID, 0));
    END IF;
    IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
         RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which occurred after the end date on the eport you are deactivating, has already been paid.');
    END IF;

    IF l_terminal_id IS NOT NULL THEN
	 	UPDATE LOCATION L SET TERMINAL_ID = l_terminal_id WHERE EPORT_ID = l_eport_id AND TERMINAL_ID = 0
		  AND NOT EXISTS(SELECT 1 FROM LOCATION L1 WHERE L1.LOCATION_ID <> L.LOCATION_ID AND
		  L1.LOCATION_NAME = L.LOCATION_NAME AND L1.TERMINAL_ID = l_terminal_id AND L1.EPORT_ID = l_eport_id);
	 END IF;
	 
    -- we must update the others
    -- first update end_date
    UPDATE TERMINAL_EPORT SET END_DATE = NVL(l_start_date, MIN_DATE)
     WHERE EPORT_ID = l_eport_id
       AND ((NVL(END_DATE, MAX_DATE) > NVL(l_start_date, MIN_DATE)
       AND NVL(END_DATE, MAX_DATE) <= NVL(l_end_date, MAX_DATE))
       OR (NVL(END_DATE, MAX_DATE) >= NVL(l_end_date, MAX_DATE)
       AND NVL(START_DATE, MIN_DATE) <= NVL(l_start_date, MIN_DATE)));
    -- now update start_date
    UPDATE TERMINAL_EPORT SET START_DATE = NVL(l_end_date, MAX_DATE)
     WHERE EPORT_ID = l_eport_id
       AND NVL(END_DATE, MAX_DATE) > NVL(l_start_date, MIN_DATE)
       AND NVL(END_DATE, MAX_DATE) <= NVL(l_end_date, MAX_DATE);
       
	 IF l_terminal_eport_id IS NULL THEN
        IF l_eport_id IS NOT NULL AND l_terminal_id IS NOT NULL THEN
            -- insert new record
            INSERT INTO TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE)
                VALUES(l_eport_id, l_terminal_id, l_start_date, l_end_date);
        END IF;
     ELSE
        IF l_eport_id IS NOT NULL AND l_terminal_id IS NOT NULL THEN
            UPDATE TERMINAL_EPORT
            SET START_DATE = l_start_date, END_DATE = l_end_date,
                EPORT_ID = l_eport_id, TERMINAL_ID = l_terminal_id
            WHERE TERMINAL_EPORT_ID = l_terminal_eport_id;
        ELSE
            DELETE FROM TERMINAL_EPORT WHERE TERMINAL_EPORT_ID = l_terminal_eport_id;
        END IF;
	 END IF;
    -- delete "dead" entries
    DELETE FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND START_DATE >= END_DATE;
    --THE FOLLOWING IS FOR RADISYS REPORTS
    REFRESH_TERMINAL_WITH_EPORT_ID; -- TO SET EPORT_ID IN THE TERMINAL TABLE
END TERMINAL_EPORT_UPD;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE update_settle_state
   IS
--
-- This is a kluge for waiting until transaction is settled because we don't currently get that data
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- B Krug       10-05-04 NEW

BEGIN
    UPDATE TRANS SET SETTLE_STATE_ID = 3
     WHERE SETTLE_STATE_ID = 1 AND TRANS_TYPE_ID IN(16,19) AND CLOSE_DATE <= TRUNC (SYSDATE, 'DD') - 1;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PROCEDURE user_report_upd
   (l_id IN USER_REPORT.USER_REPORT_ID%TYPE,
   l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
   l_email IN USER_REPORT.EMAIL%TYPE,
   l_cc IN USER_REPORT.CC%TYPE )
IS
  l_freq USER_REPORT.FREQUENCY_ID%TYPE;
  l_batch_type_id REPORTS.BATCH_TYPE_ID%TYPE;
BEGIN
	 SELECT R.BATCH_TYPE_ID INTO l_batch_type_id FROM REPORTS R, USER_REPORT UR WHERE R.REPORT_ID = UR.REPORT_ID AND UR.USER_REPORT_ID = l_id;
	 IF l_batch_type_id <> 0 THEN
	 	l_freq := NULL;
	 ELSE
	 	l_freq := l_frequency_id;
	 END IF;
	 UPDATE USER_REPORT SET FREQUENCY_ID = l_freq, EMAIL = l_email, CC = l_cc WHERE USER_REPORT_ID = l_id;
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
	 WHEN OTHERS THEN
	 	 RAISE;
END USER_REPORT_UPD;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.ADJUSTMENT_INS
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.ADJUSTMENT_UPD
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.APPROVE_PAYMENT
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PROCEDURE CREATE_LICENSE
   (l_license_id IN LICENSE_NBR.LICENSE_ID%TYPE,
   l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_license_nbr OUT LICENSE_NBR.LICENSE_NBR%TYPE
)
IS
BEGIN
	 SELECT 'L' || TO_CHAR(l_license_id,'FM99999000') || TO_CHAR(LICENSE_SEQ.NEXTVAL,'FM99990000') INTO l_license_nbr FROM DUAL;
	 INSERT INTO LICENSE_NBR(LICENSE_NBR, LICENSE_ID, CUSTOMER_ID)
	     VALUES (l_license_nbr, l_license_id, l_cust_id);
END CREATE_LICENSE;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.CREATE_PAYMENT_FOR_ACCOUNT
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.DELAY_REFUND
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.DELAY_REFUND_BY_TRAN_ID
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.DELAY_SERVICE_FEE
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.DELETE_SERVICE_FEE
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.LOAD_LICENSE_AGREEMENTS
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.MARK_EFT_PAID
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.PROCESS_FEES_UPD
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.SCAN_FOR_SERVICE_FEES
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.SERVICE_FEES_UPD
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PROCEDURE terminal_earlier_bank_account
   (l_cbt_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_TERMINAL_ID%TYPE,
	l_effective_date IN DATE)
IS
    l_terminal_id CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE;
    l_end_date    CUSTOMER_BANK_TERMINAL.END_DATE%TYPE;
BEGIN
    -- Update mapping
    UPDATE CUSTOMER_BANK_TERMINAL
       SET START_DATE = l_effective_date
     WHERE CUSTOMER_BANK_TERMINAL_ID = l_cbt_id
       RETURNING TERMINAL_ID, END_DATE INTO l_terminal_id, l_end_date;

    -- Update all other Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(START_DATE, MIN_DATE) < NVL(l_end_date, MAX_DATE)
       AND NVL(END_DATE, MAX_DATE) > l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
END TERMINAL_EARLIER_BANK_ACCOUNT;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PROCEDURE terminal_payment_activate
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
   	l_date IN DATE,
	l_dex IN CHAR,
	l_customer_id OUT CUSTOMER.CUSTOMER_ID%TYPE
  )
IS
    l_license_id LICENSE.LICENSE_ID%TYPE;
BEGIN
	 --VERIFY BANK ACCT
	 BEGIN
	 	 SELECT CUSTOMER_ID INTO l_customer_id FROM CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank id');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 BEGIN
	 	 SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CUSTOMER_LICENSE CL, LICENSE_NBR LN
	     	 WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 --INSERT INTO CUSTOMER_BANK_TERMINAL
	 INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
	     VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_date);
	 --INSERT INTO SERVICE_FEES
	 -- Added logic to set last payment to the effective date minus one
     -- frequency interval so that first active day has a service fee (BSK 09-14-04)
	 INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	 SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, lsf.FEE_ID,
                lsf.AMOUNT, lsf.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), l_date
		 FROM LICENSE_SERVICE_FEES lsf, FREQUENCY f
         WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
           AND lsf.LICENSE_ID = l_license_id;
	 IF l_dex <> 'N' THEN
	 	INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	    SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4,
                   4.00, f.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), l_date
		      FROM FREQUENCY f
             WHERE f.FREQUENCY_ID = 2;
	 END IF;
	 --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
	     SELECT PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, l_date
		 FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
END TERMINAL_PAYMENT_ACTIVATE;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PROCEDURE terminal_reassign_bank_account
   (l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
    l_cust_bank_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_ID%TYPE,
	l_effective_date IN DATE)
IS
BEGIN
    -- Update all Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(END_DATE, l_effective_date) >= l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
    -- Insert new mapping if customer bank is not zero
    IF l_cust_bank_id <> 0 THEN
		INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
		    VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_effective_date);
    END IF;
    
    -- Clean up dead dates
    DELETE FROM CUSTOMER_BANK_TERMINAL WHERE START_DATE >= END_DATE;
    
END TERMINAL_REASSIGN_BANK_ACCOUNT;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.UNLOCK_EFT
/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
DROP PROCEDURE CORP.UPDATE_LEDGER_PENDING_TABLES
/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE BODY DATA_IN_PKG
IS
    FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN EPORT.EPORT_ID%TYPE
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_device_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_device_serial;
         -- NOTE: eventually the above should be source system dependent
         RETURN l_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT EPORT_SEQ.NEXTVAL INTO l_device_id FROM DUAL;
            -- What about device type???
            IF l_device_serial LIKE 'E4%' THEN
                l_device_type := 2; -- G4
            ELSIF l_device_serial LIKE 'G5%' THEN
                l_device_type := 3; -- G5
            ELSIF l_device_serial LIKE 'M1%' THEN
                l_device_type := 4; -- MEI
            --ELSIF l_device_serial LIKE '10%' THEN -- esuds
            ELSIF l_device_serial LIKE '10%' THEN
                l_device_type := 1; -- Radisys Brick
            ELSE
                l_device_type := 0;
            END IF;
            INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_device_id, l_device_serial, SYSDATE, l_device_type);
            RETURN l_device_id;
        WHEN OTHERS THEN
            RAISE;
    END;
  
    FUNCTION GET_OR_CREATE_MERCHANT(
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN CORP.MERCHANT.MERCHANT_ID%TYPE
    IS
        l_merchant_id CORP.MERCHANT.MERCHANT_ID%TYPE;
    BEGIN
        SELECT MERCHANT_ID
          INTO l_merchant_id
          FROM CORP.MERCHANT
         WHERE MERCHANT_NBR = l_merchant_num;
         RETURN l_merchant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT CORP.MERCHANT_SEQ.NEXTVAL INTO l_merchant_id FROM DUAL;
            INSERT INTO CORP.MERCHANT(MERCHANT_ID, MERCHANT_NBR, DESCRIPTION)
			  		 VALUES(l_merchant_id, l_merchant_num, 'Created for source system "'||l_source_system_cd||'"');
            RETURN l_merchant_id;
        WHEN OTHERS THEN
            RAISE;
    END;

  /* This allows external systems to add transactions. Credit, debit,
   * pass, access, or maintenance cards or refund, chargeback transaction
   * or cash should be added this way. Refunds
   * and chargebacks should always provide a valid original machine tran number.
   */
  PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_track_data TRANS.TRACK_DATA%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL)
    IS
        l_create_date TRANS.CREATE_DATE%TYPE;
        l_eport_id TRANS.EPORT_ID%TYPE;
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE;
    BEGIN
        -- check for dup
        BEGIN
            SELECT CREATE_DATE
              INTO l_create_date
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            RAISE_APPLICATION_ERROR(-20880, 'A transaction with MACHINE_TRANS_NUM = "'
                ||l_machine_trans_no||'" from the source system "'
                ||l_source_system_cd||'" already exists. It was received '
                ||TO_CHAR(l_create_date, 'MM/DD/YYYY HH24:MI:SS')
                ||'. Transaction was NOT added.');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL; -- CONTINUE, no dup found
            WHEN OTHERS THEN
                RAISE;
        END;
        
        --get needed lookup values
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        
        IF l_orig_machine_trans_no IS NOT NULL THEN
            BEGIN
                SELECT ORIG_TRAN_ID
                  INTO l_orig_tran_id
                  FROM TRANS
                 WHERE MACHINE_TRANS_NO = l_orig_machine_trans_no
                   AND SOURCE_SYSTEM_CD = l_source_system_cd;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with MACHINE_TRANS_NUM = "'||l_orig_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
        
        INSERT INTO TRANS(
            TRAN_ID,
            MACHINE_TRANS_NO,
            SOURCE_SYSTEM_CD,
            EPORT_ID,
            START_DATE,
            CLOSE_DATE,
            TRANS_TYPE_ID,
            TOTAL_AMOUNT,
            TRACK_DATA,
            CARD_NUMBER,
            SERVER_DATE,
            SETTLE_STATE_ID,
            SETTLE_DATE,
            PREAUTH_AMOUNT,
            PREAUTH_DATE,
            CC_APPR_CODE,
            MERCHANT_ID,
            DESCRIPTION,
            ORIG_TRAN_ID)
          SELECT
            TRANS_SEQ.NEXTVAL,
            l_machine_trans_no,
            l_source_system_cd,
            l_eport_id,
            l_start_date,
            l_close_date,
            l_trans_type_id,
            l_total_amount,
            l_track_data,
            l_card_number,
            l_received_date,
            l_settle_state_id,
            l_settle_date,
            l_preauth_amount,
            l_preauth_date,
            l_approval_cd,
            l_merchant_id,
            l_description,
            l_orig_tran_id
          FROM DUAL;
    END;

    PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PLS_INTEGER DEFAULT 1,
        l_price PURCHASE.AMOUNT%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL)
    IS
    BEGIN
        FOR i IN 1..l_quantity LOOP
            INSERT INTO PURCHASE(
                PURCHASE_ID,
                TRAN_ID,
                TRAN_DATE,
                PRICE,
                VEND_COLUMN,
                DESCRIPTION,
                MDB_NUMBER)
              SELECT
                PURCHASE_SEQ.NEXTVAL,
                TRAN_ID,
                CLOSE_DATE,
                l_price,
                l_column_label,
                l_product_desc,
                l_column_num
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            IF SQL%ROWCOUNT = 0 THEN
                RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Item was NOT added.');
            END IF;
        END LOOP;
    END;
    
    /* This procedure allows external systems to update the settlement info
     * of a transaction.
     *
     */
    PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE)
    IS
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
    BEGIN
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        UPDATE TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID) =
            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
                    l_settle_date,
                    NVL(l_approval_cd, T.CC_APPR_CODE),
                    NVL(l_merchant_id, T.MERCHANT_ID)
              FROM DUAL)
         WHERE T.MACHINE_TRANS_NO = l_machine_trans_no
           AND T.SOURCE_SYSTEM_CD = l_source_system_cd;
        IF SQL%ROWCOUNT = 0 THEN
            RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Settle info was NOT updated.');
        END IF;
    END;
    
    /* This procedure allows external systems to update the device info
     * of their devices
     */
    PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_ID = l_device_id;
    END;
    
    PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE)
    IS
       l_prev_fill_date FILL.FILL_DATE%TYPE;
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
  	    INSERT INTO FILL (FILL_ID, EPORT_ID, FILL_DATE)
 	         VALUES(FILL_SEQ.NEXTVAL, l_eport_id, l_fill_date);
    END;
    /* need to change alerts to relate to devices not terminals
    PROCEDURE ADD_ALERT(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_alert_id ALERT.ALERT_ID%TYPE,
       l_alert_date TERMINAL_ALERT.ALERT_DATE%TYPE,
       l_details TERMINAL_ALERT.DETAILS%TYPE)
    IS
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
    	 SELECT TERMINAL_ALERT_SEQ.NEXTVAL INTO l_id FROM DUAL;
    	 INSERT INTO TERMINAL_ALERT (TERMINAL_ALERT_ID, ALERT_ID, TERMINAL_ID, ALERT_DATE, DETAILS, RESPONSE_SENT )
     		VALUES(l_id,l_alert_id,l_terminal_id,l_alert_date,l_details, l_sent);
    END;
    */
    /*
    Right now Legacy G4 and BEX do not have any additional refund information (PROBLEM_DATE is the TRAN_DATE, REFUND_STATUS is always 1)
    */
    
    PROCEDURE UPDATE_POS_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_effective_date TERMINAL_EPORT.START_DATE%TYPE)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        /*UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_SERIAL_NUM = l_device_serial;*/
    END;

END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE BODY DW_PKG
IS
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF SET CC_APPR_CODE = l_appr_cd WHERE TRAN_ID = l_trans_id;
    END;

    PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_ref_nbr ACTIVITY_REF.REF_NBR%TYPE;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_create_dt ACTIVITY_REF.CREATE_DT%TYPE;
        l_update_dt ACTIVITY_REF.UPDATE_DT%TYPE;
    BEGIN
        DELETE FROM activity_ref
         WHERE tran_id = l_trans_id
         RETURNING ref_nbr, batch_id, create_dt, SYSDATE
              INTO l_ref_nbr, l_export_id, l_create_dt, l_update_dt;

        INSERT INTO ACTIVITY_REF(
            REGION_ID,
            REGION_NAME,
            LOCATION_NAME,
            LOCATION_ID,
            TERMINAL_ID,
            TERMINAL_NAME,
            TERMINAL_NBR,
            TRAN_ID,
            TRANS_TYPE_ID,
            TRANS_TYPE_NAME,
            CARD_NUMBER,
            CARD_COMPANY,
            CC_APPR_CODE,
            TRAN_DATE,
            MONTH_RANGE,
            WEEK_RANGE,
            DAY_RANGE,
            FILL_DATE,
            TOTAL_AMOUNT,
            ORIG_TRAN_ID,
            BATCH_ID,
            VEND_COLUMN,
            QUANTITY,
            REF_NBR,
            EPORT_ID,
            DESCRIPTION,
            CREATE_DT,
            UPDATE_DT)
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            T.TERMINAL_ID,
            TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRAN_ID,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            T.CARD_NUMBER,
            CARD_COMPANY(T.CARD_NUMBER),
            T.CC_APPR_CODE,
            T.CLOSE_DATE TRAN_DATE,
            TRUNC(T.CLOSE_DATE, 'MONTH') MONTH_RANGE,
            TRUNC(T.CLOSE_DATE, 'DAY') WEEK_RANGE,
            TRUNC(T.CLOSE_DATE, 'DD') DAY_RANGE,
            GET_FILL_DATE(T.TRAN_ID) FILL_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            l_export_id,
            VEND_COLUMN_STRING(T.TRAN_ID),
            P.CNT,
            NVL(T.MACHINE_TRANS_NO, NVL(l_ref_nbr, REF_NBR_SEQ.NEXTVAL)),
            T.EPORT_ID,
            T.DESCRIPTION,
            l_create_dt,
            l_update_dt
          FROM TRANS_TYPE TT, TRANS T, TERMINAL TERM,
            REGION R, TERMINAL_REGION TR, LOCATION L,
            (SELECT TRAN_ID, COUNT(*) CNT FROM PURCHASE GROUP BY TRAN_ID) P
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND T.TERMINAL_ID = TERM.TERMINAL_ID (+)
            AND T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
            AND T.TRAN_ID = P.TRAN_ID (+)
            AND T.TRAN_ID = l_trans_id;
    END;
    
    PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF A SET (
            region_id,
            region_name,
            location_name,
            location_id,
            terminal_name,
            terminal_nbr) = (
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            TERMINAL_NAME,
            TERM.TERMINAL_NBR
          FROM TERMINAL TERM,  REGION R, TERMINAL_REGION TR, LOCATION L
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND TERM.TERMINAL_ID = A.TERMINAL_ID)
        WHERE A.TERMINAL_ID = l_terminal_id;
    END;

    PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
        l_fill_date FILL.FILL_DATE%TYPE;
        l_prev_fill_date FILL.FILL_DATE%TYPE;
        l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        SELECT FILL_DATE, EPORT_ID
          INTO l_fill_date, l_eport_id
          FROM FILL
         WHERE FILL_ID = l_fill_id;

  	    SELECT NVL(MAX(FILL_DATE), MIN_DATE)
          INTO l_prev_fill_date
          FROM FILL
         WHERE FILL_DATE < l_fill_date
           AND EPORT_ID = l_eport_id;
           
        UPDATE ACTIVITY_REF SET FILL_DATE = GET_FILL_DATE(TRAN_ID)
    	 WHERE EPORT_ID = l_eport_id AND TRAN_DATE BETWEEN l_prev_fill_date AND l_fill_date;
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE BODY SYNC_JOB_PKG
IS
--
-- All the procedures for controlling the sync job. WARNING: if the sync job
-- is running and you try to compile or change this package it will hang forever
-- waiting for the sync job to finish. Use STOP_SYNC_JOB first. Then, make changes
-- and then restart the job using RUN_SYNC_JOB.
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B KRUG       10-18-04 NEW

    PROCEDURE ENQUEUE_NULL
    IS
        l_msg_id RAW(32767);
        l_opts DBMS_AQ.ENQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
        l_msg T_SYNC_MSG;
    BEGIN
        DBMS_AQ.ENQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, l_msg, l_msg_id);
    END;

    FUNCTION DEQUEUE
     RETURN VARCHAR
    IS
        l_msg_id RAW(32767);
        l_payload T_SYNC_MSG;
        l_opts DBMS_AQ.DEQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
    BEGIN
        DBMS_AQ.DEQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, l_payload, l_msg_id);
        RETURN l_payload.sql_to_run;
    END;

    PROCEDURE PROCESS_SYNCS
    IS
        l_sql VARCHAR(8000);
    BEGIN
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Process is now running...');
        LOOP
            l_sql := DEQUEUE;
            EXIT WHEN l_sql IS NULL;
            BEGIN
                LOG_MSG('TRACE','SYNC_JOB','PROCESSING', 'Executing "'||l_sql||'"');
                EXECUTE IMMEDIATE l_sql;
                COMMIT;
                LOG_MSG('TRACE','SYNC_JOB','PROCESSING', 'Finished "'||l_sql||'"');
            EXCEPTION
                WHEN OTHERS THEN
                    DECLARE
                        l_msg MSG_LOG.MSG_TEXT%TYPE := SQLERRM;
                        l_err_num MSG_LOG.ERROR_NUM%TYPE := SQLCODE;
                    BEGIN
                        ROLLBACK;
                        LOG_MSG('ERROR','SYNC_JOB','PROCESSING', SUBSTR(l_msg || ' (While executing: "' || l_sql || '")',1, 4000), l_err_num);
                    END;
            END;
        END LOOP;
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Process has ended');
    END;

    -- Syncs ledger and batch from report's sync queue
    FUNCTION RUN_SYNC_JOB
     RETURN BINARY_INTEGER
    IS
        l_job_id BINARY_INTEGER;
    BEGIN
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Starting Job');
    	DBMS_JOB.SUBMIT (l_job_id, 'SYNC_JOB_PKG.PROCESS_SYNCS;');
    	COMMIT;
    	--l_job_id := 47;
    	--DBMS_JOB.RUN(l_job_id);
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Job Started with id = '||TO_CHAR(l_job_id));
    	RETURN l_job_id;
    END;

    PROCEDURE STOP_SYNC_JOB
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
    	ENQUEUE_NULL();
    	COMMIT;
        LOG_MSG('INFO','SYNC_JOB','JOB CONTROL', 'Enqueued NULL to stop the Job');
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE BODY SYNC_PKG
IS
--
-- All the procedures for updating ledger tables and trans mart tables
-- whenever trans, terminal, location, terminal_device, customer_bank_terminal,
-- process_fees change
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------      
-- B KRUG       10-04-04 NEW
    m_date_format CONSTANT VARCHAR2(50) := 'MM/DD/YYYY HH24:MI:SS';
    
    PROCEDURE ENQUEUE(
        l_sql VARCHAR)
    IS
        l_msg_id RAW(32767);
        l_opts DBMS_AQ.ENQUEUE_OPTIONS_T;
        l_props DBMS_AQ.MESSAGE_PROPERTIES_T;
    BEGIN
        DBMS_AQ.ENQUEUE('REPORT.Q_SYNC_MSG', l_opts, l_props, T_SYNC_MSG(l_sql), l_msg_id);
    END;
    
    FUNCTION LOOKUP_TERMINAL(
        l_eport_id TRANS.EPORT_ID%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE)
    RETURN TRANS.TERMINAL_ID%TYPE
    IS
        l_terminal_id TRANS.TERMINAL_ID%TYPE;
    BEGIN
        SELECT TERMINAL_ID
          INTO l_terminal_id
          FROM TERMINAL_EPORT
         WHERE EPORT_ID = l_eport_id
           AND l_close_date >= NVL(START_DATE, MIN_DATE)
           AND l_close_date < NVL(END_DATE, MAX_DATE);
        RETURN l_terminal_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RAISE;
    END;

    FUNCTION LOOKUP_CUST_BANK(
        l_terminal_id TRANS.TERMINAL_ID%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE)
    RETURN TRANS.CUSTOMER_BANK_ID%TYPE
    IS
        l_cust_bank_id TRANS.CUSTOMER_BANK_ID%TYPE;
    BEGIN
        SELECT CUSTOMER_BANK_ID
          INTO l_cust_bank_id
          FROM CUSTOMER_BANK_TERMINAL
         WHERE TERMINAL_ID = l_terminal_id
           AND l_close_date >= NVL(START_DATE, MIN_DATE)
           AND l_close_date < NVL(END_DATE, MAX_DATE);
        RETURN l_cust_bank_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RAISE;
    END;

    FUNCTION LOOKUP_PROCESS_FEE_ID(
        l_terminal_id TRANS.TERMINAL_ID%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE)
    RETURN TRANS.PROCESS_FEE_ID%TYPE
    IS
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE;
    BEGIN
        SELECT PROCESS_FEE_ID
          INTO l_process_fee_id
          FROM PROCESS_FEES
         WHERE TERMINAL_ID = l_terminal_id
           AND TRANS_TYPE_ID = l_trans_type_id
           AND l_close_date >= NVL(START_DATE, MIN_DATE)
           AND l_close_date < NVL(END_DATE, MAX_DATE);
        RETURN l_process_fee_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        WHEN OTHERS THEN
            RAISE;
    END;

    PROCEDURE LOOKUP_RELATED(
        l_eport_id TRANS.EPORT_ID%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_orig_tran_id TRANS.orig_tran_id%TYPE,
        l_terminal_id IN OUT NOCOPY TRANS.TERMINAL_ID%TYPE,
        l_cust_bank_id IN OUT NOCOPY TRANS.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id IN OUT NOCOPY TRANS.PROCESS_FEE_ID%TYPE,
        l_override_terminal BOOLEAN DEFAULT TRUE)
    IS
    BEGIN
        IF l_override_terminal THEN
            IF l_trans_type_id IN(20,21) AND l_orig_tran_id IS NOT NULL THEN
                SELECT TERMINAL_ID, CUSTOMER_BANK_ID
                  INTO l_terminal_id, l_cust_bank_id
                  FROM TRANS 
                 WHERE TRAN_ID = l_orig_tran_id;
                IF l_terminal_id IS NOT NULL THEN
                    l_process_fee_id := LOOKUP_PROCESS_FEE_ID(l_terminal_id, l_close_date, l_trans_type_id);
                ELSE
                    l_process_fee_id := NULL;
                END IF;
            ELSE
                l_terminal_id := LOOKUP_TERMINAL(l_eport_id, l_close_date);
                LOOKUP_RELATED(l_eport_id, l_close_date, l_trans_type_id, l_orig_tran_id, l_terminal_id, l_cust_bank_id, l_process_fee_id, FALSE);
            END IF;
        --  override is false
        ELSIF l_terminal_id IS NULL THEN
            l_cust_bank_id := NULL;
            l_process_fee_id := NULL;
        ELSE
            l_cust_bank_id := LOOKUP_CUST_BANK(l_terminal_id, l_close_date);
            l_process_fee_id := LOOKUP_PROCESS_FEE_ID(l_terminal_id, l_close_date, l_trans_type_id);
        END IF;
    END;
    
    PROCEDURE RECALC_ALL_TRANS_REDO
    IS
    BEGIN
        --ALTER TRIGGER TRBIU_TRANS DISABLE;
        UPDATE TRANS T SET (TERMINAL_ID, PROCESS_FEE_ID, CUSTOMER_BANK_ID, REFRESH_IND) =
            (SELECT TE.TERMINAL_ID, PF.PROCESS_FEE_ID, CBT.CUSTOMER_BANK_ID, 'Y'
               FROM TERMINAL_EPORT TE, PROCESS_FEES PF, CUSTOMER_BANK_TERMINAL CBT
              WHERE TE.EPORT_ID = T.EPORT_ID
                AND NVL(TE.START_DATE, MIN_DATE) <= T.CLOSE_DATE
                AND NVL(TE.END_DATE, MAX_DATE) > T.CLOSE_DATE
                AND TE.TERMINAL_ID = PF.TERMINAL_ID (+)
                AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID (+)
                AND NVL(PF.START_DATE, MIN_DATE) <= T.CLOSE_DATE
                AND NVL(PF.END_DATE, MAX_DATE) > T.CLOSE_DATE
                AND TE.TERMINAL_ID = CBT.TERMINAL_ID (+)
                AND NVL(CBT.START_DATE, MIN_DATE) <= T.CLOSE_DATE
                AND NVL(CBT.END_DATE, MAX_DATE) > T.CLOSE_DATE
            );
        COMMIT;
        --ALTER TRIGGER TRBIU_TRANS ENABLE;
    END;

-- Actual synchronization procedures (SYNC and RECEIVE procedures for each type of change)
    PROCEDURE UPDATE_CUST_BANK(
        l_trans_rec TRANS%ROWTYPE,
        l_cust_bank_id TRANS.CUSTOMER_BANK_ID%TYPE)
    IS
    BEGIN
        UPDATE TRANS T SET CUSTOMER_BANK_ID = l_cust_bank_id
           WHERE TRAN_ID = l_trans_rec.TRAN_ID;
        CORP.PAYMENTS_PKG.UPDATE_LEDGER(l_trans_rec.tran_id,l_trans_rec.trans_type_id,
            l_trans_rec.close_date,l_trans_rec.settle_date,l_trans_rec.total_amount,l_trans_rec.settle_state_id,
            l_trans_rec.terminal_id,l_cust_bank_id,l_trans_rec.process_fee_id);
        DW_PKG.UPDATE_TRAN_INFO(l_trans_rec.TRAN_ID);
    END;

    PROCEDURE UPDATE_CHILD_CUST_BANK(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_cust_bank_id TRANS.CUSTOMER_BANK_ID%TYPE)
    IS
        CURSOR l_children_cur IS
            SELECT * FROM TRANS
             WHERE ORIG_TRAN_ID = l_trans_id
               AND NVL(CUSTOMER_BANK_ID, 0) <> NVL(l_cust_bank_id, 0);
    BEGIN
        FOR l_children_rec IN l_children_cur LOOP
            UPDATE_CUST_BANK(l_children_rec, l_cust_bank_id);
        END LOOP;
    END;
    
    PROCEDURE UPDATE_TRANS_RELATED(
        l_trans_id    TRANS.TRAN_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_eport_id  TRANS.EPORT_ID%TYPE,
        l_close_date    TRANS.CLOSE_DATE%TYPE,
        l_settle_date    TRANS.SETTLE_DATE%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE)
    IS
        l_terminal_id TRANS.terminal_id%TYPE;
        l_process_fee_id TRANS.process_fee_id%TYPE;
        l_cust_bank_id TRANS.customer_bank_id%TYPE;
    BEGIN
        -- Find terminal_id, customer_bank_id and, process_fee_id
        LOOKUP_RELATED(l_eport_id, l_close_date, l_trans_type_id, l_orig_tran_id,
                l_terminal_id, l_cust_bank_id, l_process_fee_id, TRUE);

        UPDATE TRANS T SET (TERMINAL_ID, PROCESS_FEE_ID, CUSTOMER_BANK_ID, REFRESH_IND, STATUS) =
            (SELECT NVL(l_terminal_id, 0), l_process_fee_id, l_cust_bank_id, 'N', 'R'
                FROM DUAL)
           WHERE TRAN_ID = l_trans_id;
        CORP.PAYMENTS_PKG.UPDATE_LEDGER(l_trans_id,l_trans_type_id,l_close_date,l_settle_date,l_total_amount,l_settle_state_id,l_terminal_id,l_cust_bank_id,l_process_fee_id);
        DW_PKG.UPDATE_TRAN_INFO(l_trans_id);
    END;

    PROCEDURE UPDATE_CHILD_RELATED(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        CURSOR l_children_cur IS
            SELECT * FROM TRANS
             WHERE ORIG_TRAN_ID = l_trans_id;
    BEGIN
        FOR l_children_rec IN l_children_cur LOOP
            UPDATE_TRANS_RELATED(l_children_rec.tran_id, l_children_rec.trans_type_id,
                l_children_rec.eport_id, l_children_rec.close_date, l_children_rec.settle_date,
                l_children_rec.total_amount, l_children_rec.settle_state_id, l_children_rec.orig_tran_id);
        END LOOP;
    END;

    PROCEDURE SYNC_FILL_INSERT(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_FILL_INSERT('|| TO_CHAR(l_fill_id)||')');
    END;

    PROCEDURE RECEIVE_FILL_INSERT(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
    BEGIN
        CORP.PAYMENTS_PKG.UPDATE_FILL_BATCH(l_fill_id);
        DW_PKG.UPDATE_FILL_DATE(l_fill_id);
    END;
    
    PROCEDURE SYNC_TRANS_INSERT(
        l_trans_id TRANS.tran_id%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_TRANS_INSERT('|| TO_CHAR(l_trans_id)||')');
    END;
    
    FUNCTION CHECK_FOR_DUP(
        l_trans_rec TRANS%ROWTYPE)
     RETURN BOOLEAN
    IS
        l_status TRANS.STATUS%TYPE;
        l_terminal_id TRANS.TERMINAL_ID%TYPE;
    BEGIN
        -- if this was marked as active then just accept it
        IF l_trans_rec.STATUS IN('A', 'R') THEN
            RETURN FALSE;
        END IF;
        -- ignore for specific customers
        BEGIN
            l_terminal_id := LOOKUP_TERMINAL(l_trans_rec.eport_id,l_trans_rec.close_date);
            IF l_terminal_id IS NOT NULL THEN
                SELECT 'A'
                  INTO l_status
                  FROM SKIP_VALIDATION SK, TERMINAL T
                  WHERE SK.CUSTOMER_ID = T.CUSTOMER_ID
                    AND T.TERMINAL_ID = l_terminal_id;
                RETURN FALSE;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL; -- continue
            WHEN OTHERS THEN
                RAISE;
        END;
        -- gets all those that are probably dups
        SELECT 'D'
          INTO l_status
          FROM TRANS B
         WHERE TRAN_ID <> l_trans_rec.TRAN_ID
           AND l_trans_rec.CLOSE_DATE = B.CLOSE_DATE
           AND B.STATUS IN('A', 'R')
	       AND l_trans_rec.TRANS_TYPE_ID = B.TRANS_TYPE_ID
           AND l_trans_rec.EPORT_ID = B.EPORT_ID
           AND EQL(VEND_COLUMN_STRING(l_trans_rec.TRAN_ID), VEND_COLUMN_STRING(B.TRAN_ID)) = -1
	       AND l_trans_rec.TOTAL_AMOUNT = B.TOTAL_AMOUNT
           AND EQL(l_trans_rec.CARD_NUMBER,B.CARD_NUMBER) = -1
           AND EQL(l_trans_rec.CC_APPR_CODE, B.CC_APPR_CODE) = -1;
        -- the G4 may not report seconds and we can tell if the hour = minutes = seconds or seconds = 00
        IF l_trans_rec.TRANS_TYPE_ID IN(18, 22)
           AND (TO_CHAR(l_trans_rec.CLOSE_DATE, 'SS') = '00'
                OR (TO_CHAR(l_trans_rec.CLOSE_DATE, 'HH24') = TO_CHAR(l_trans_rec.CLOSE_DATE, 'MI')
                    AND TO_CHAR(l_trans_rec.CLOSE_DATE, 'SS') = TO_CHAR(l_trans_rec.CLOSE_DATE, 'MI'))) THEN
            --this may not be a dup so for cash and pass cards let's let it through
            RETURN FALSE;
        END IF;
        -- It is probably a dup so mark it as such
        UPDATE TRANS T SET (REFRESH_IND, STATUS) =
            (SELECT 'N', 'D' FROM DUAL)
           WHERE TRAN_ID = l_trans_rec.TRAN_ID;
        RETURN TRUE;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN FALSE;
        WHEN OTHERS THEN
            RAISE;
    END;

    PROCEDURE RECEIVE_TRANS_INSERT(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_trans_rec TRANS%ROWTYPE;
    BEGIN
        SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_trans_id;
        -- check for dups
        IF NOT CHECK_FOR_DUP(l_trans_rec)THEN
            UPDATE_TRANS_RELATED(l_trans_rec.tran_id,l_trans_rec.trans_type_id,
                l_trans_rec.eport_id,l_trans_rec.close_date,l_trans_rec.settle_date,
                l_trans_rec.total_amount,l_trans_rec.settle_state_id,l_trans_rec.orig_tran_id);
            --merchant id is updated in the before insert or update trigger on trans
        END IF;
    END;

    PROCEDURE SYNC_TRANS_TERMINAL(
        l_trans_id TRANS.tran_id%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_TRANS_TERMINAL('|| TO_CHAR(l_trans_id)||')');
    END;

    PROCEDURE SYNC_TRANS_TERMINAL(
        l_list_of_ids ID_LIST)
    IS
    BEGIN
        IF l_list_of_ids.FIRST IS NOT NULL THEN
            FOR i IN l_list_of_ids.FIRST..l_list_of_ids.LAST LOOP
                IF l_list_of_ids(i) IS NOT NULL THEN
                    SYNC_TRANS_TERMINAL(l_list_of_ids(i));
                END IF;
            END LOOP;
        END IF;
    END;
    
    PROCEDURE RECEIVE_TRANS_TERMINAL(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_trans_rec TRANS%ROWTYPE;
    BEGIN
        SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_trans_id;
        UPDATE_TRANS_RELATED(l_trans_rec.tran_id,l_trans_rec.trans_type_id,
            l_trans_rec.eport_id,l_trans_rec.close_date,
            l_trans_rec.settle_date,l_trans_rec.total_amount,
            l_trans_rec.settle_state_id,l_trans_rec.orig_tran_id);
        IF l_trans_rec.trans_type_id NOT IN(20,21,22) THEN
            UPDATE_CHILD_RELATED(l_trans_id);
        END IF;
    END;

    PROCEDURE SYNC_TRANS_CUST_BANK(
        l_trans_id TRANS.tran_id%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_TRANS_CUST_BANK('|| TO_CHAR(l_trans_id)||')');
    END;

    PROCEDURE SYNC_TRANS_CUST_BANK(
        l_list_of_ids ID_LIST)
    IS
    BEGIN
        IF l_list_of_ids.FIRST IS NOT NULL THEN
            FOR i IN l_list_of_ids.FIRST..l_list_of_ids.LAST LOOP
                IF l_list_of_ids(i) IS NOT NULL THEN
                    SYNC_TRANS_CUST_BANK(l_list_of_ids(i));
                END IF;
            END LOOP;
        END IF;
    END;

    PROCEDURE RECEIVE_TRANS_CUST_BANK(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_trans_rec TRANS%ROWTYPE;
        l_cust_bank_id TRANS.CUSTOMER_BANK_ID%TYPE;
    BEGIN
        SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_trans_id;
        IF l_trans_rec.TRANS_TYPE_ID IN(20,21) AND l_trans_rec.ORIG_TRAN_ID IS NOT NULL THEN
            SELECT CUSTOMER_BANK_ID
              INTO l_cust_bank_id
              FROM TRANS
             WHERE TRAN_ID = l_trans_rec.ORIG_TRAN_ID;
        ELSE
            l_cust_bank_id := LOOKUP_CUST_BANK(l_trans_rec.terminal_id,l_trans_rec.close_date);
            IF l_trans_rec.TRANS_TYPE_ID NOT IN(22) THEN -- ignore cash
                null;
            END IF;
        END IF;
        
        IF NVL(l_cust_bank_id, 0) <> NVL(l_trans_rec.customer_bank_id, 0) THEN
            UPDATE_CUST_BANK(l_trans_rec, l_cust_bank_id);
            IF l_trans_rec.TRANS_TYPE_ID IN(20,21) AND l_trans_rec.ORIG_TRAN_ID IS NOT NULL THEN
                UPDATE_CHILD_CUST_BANK(l_trans_id,l_cust_bank_id);
            END IF;
        END IF;
    END;

    PROCEDURE SYNC_PROCESS_FEE_VALUES(
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE)
    IS
    BEGIN
        CORP.PAYMENTS_PKG.CHECK_TRANS_WITH_PF_CLOSED(l_process_fee_id);
        ENQUEUE('CALL SYNC_PKG.RECEIVE_PROCESS_FEE_VALUES('|| TO_CHAR(l_process_fee_id)||')');
    END;
    
    PROCEDURE RECEIVE_PROCESS_FEE_VALUES(
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE)
    IS
    BEGIN
        -- must update ledger & trans dim
        CORP.PAYMENTS_PKG.UPDATE_PROCESS_FEE_VALUES(l_process_fee_id);
    END;

    PROCEDURE SYNC_TRANS_PROCESS_FEE(
        l_trans_id TRANS.tran_id%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_TRANS_PROCESS_FEE('|| TO_CHAR(l_trans_id)||')');
    END;

    PROCEDURE SYNC_TRANS_PROCESS_FEE(
        l_list_of_ids ID_LIST)
    IS
    BEGIN
        IF l_list_of_ids.FIRST IS NOT NULL THEN
            FOR i IN l_list_of_ids.FIRST..l_list_of_ids.LAST LOOP
                IF l_list_of_ids(i) IS NOT NULL THEN
                    SYNC_TRANS_PROCESS_FEE(l_list_of_ids(i));
                END IF;
            END LOOP;
        END IF;
    END;

    PROCEDURE RECEIVE_TRANS_PROCESS_FEE(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_trans_rec TRANS%ROWTYPE;
        l_pf_id TRANS.PROCESS_FEE_ID%TYPE;
    BEGIN
        SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_trans_id;
        l_pf_id := LOOKUP_PROCESS_FEE_ID(l_trans_rec.terminal_id,l_trans_rec.close_date,l_trans_rec.trans_type_id);
        IF NVL(l_pf_id, 0) <> NVL(l_trans_rec.process_fee_id, 0) THEN
            UPDATE TRANS T SET PROCESS_FEE_ID = l_pf_id
               WHERE TRAN_ID = l_trans_rec.TRAN_ID;
            CORP.PAYMENTS_PKG.UPDATE_LEDGER(l_trans_rec.tran_id,l_trans_rec.trans_type_id,
                l_trans_rec.close_date,l_trans_rec.settle_date,l_trans_rec.total_amount,l_trans_rec.settle_state_id,
                l_trans_rec.terminal_id,l_trans_rec.customer_bank_id,l_pf_id);
            DW_PKG.UPDATE_TRAN_INFO(l_trans_id);
        END IF;
    END;

    PROCEDURE SYNC_TRANS_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_TRANS_SETTLEMENT('|| TO_CHAR(l_trans_id)
            ||','|| TO_CHAR(l_settle_state_id)
            ||',TO_DATE(''' || TO_CHAR(l_settle_date, m_date_format)
            || ''',''' || m_date_format || '''),'
            ||','''|| REPLACE(l_appr_cd, '''','''''') ||''')');
    END;
    
    PROCEDURE RECEIVE_TRANS_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        CORP.PAYMENTS_PKG.UPDATE_SETTLEMENT(l_trans_id, l_settle_state_id, l_settle_date);
        DW_PKG.UPDATE_SETTLEMENT(l_trans_id, l_settle_state_id, l_settle_date, l_appr_cd);
    END;

    PROCEDURE SYNC_TERMINAL_INFO(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        ENQUEUE('CALL SYNC_PKG.RECEIVE_TERMINAL_INFO('|| TO_CHAR(l_terminal_id) ||')');
    END;

    PROCEDURE SYNC_TERMINAL_INFO(
        l_list_of_ids ID_LIST)
    IS
    BEGIN
        IF l_list_of_ids.FIRST IS NOT NULL THEN
            FOR i IN l_list_of_ids.FIRST..l_list_of_ids.LAST LOOP
                IF l_list_of_ids(i) IS NOT NULL THEN
                    SYNC_TERMINAL_INFO(l_list_of_ids(i));
                END IF;
            END LOOP;
        END IF;
    END;

    PROCEDURE RECEIVE_TERMINAL_INFO(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        DW_PKG.UPDATE_TERMINAL(l_terminal_id);
    END;

    PROCEDURE SYNC_REGION_NAME(
        l_region_id REGION.REGION_ID%TYPE,
        l_region_name REGION.REGION_NAME%TYPE)
    IS
    BEGIN
        ENQUEUE('UPDATE ACTIVITY_REF SET REGION_NAME = '''
            || REPLACE(l_region_name, '''','''''') || ''' WHERE REGION_ID = '
            || TO_CHAR(l_region_id));
    END;
    
-- Utility methods for re-syncing stuff
    PROCEDURE RESYNC_TRANS_BETWEEN(
        l_start_tran_id TRANS.TRAN_ID%TYPE DEFAULT 0,
        l_end_tran_id TRANS.TRAN_ID%TYPE DEFAULT 99999999999999999999999999999999999999)
    IS
        CURSOR c_trans IS SELECT TRAN_ID FROM TRANS
            WHERE TRAN_ID BETWEEN l_start_tran_id AND l_end_tran_id;
    BEGIN
        FOR r_trans IN c_trans LOOP
            SYNC_TRANS_INSERT(r_trans.tran_id);
            COMMIT;
        END LOOP;
    END;

    -- update the terminal_id, process_fee_id and customer_bank_id for each transaction
    PROCEDURE RESYNC_ALL_TRANS
    IS
    BEGIN
        RESYNC_TRANS_BETWEEN(NULL, NULL);
    END;
    
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE BODY WEB_PKG
IS
/*
    FUNCTION GET_REQS_MET
     ( l_user_id IN USER_LOGIN.USER_ID%TYPE )
     RETURN  ID_LIST
    IS
        l_req_ids ID_LIST := ID_LIST();
        CURSOR l_reqs_cur IS
            SELECT 'SELECT ' || REQUIREMENT_ID || ' FROM DUAL WHERE ' || TEST_EXPRESSION SQL_TEXT
              FROM REQUIREMENT;
    BEGIN
        l_req_ids.EXTEND;
        FOR l_reqs_rec IN l_reqs_cur LOOP
            BEGIN
                EXECUTE IMMEDIATE l_reqs_rec.SQL_TEXT
                    INTO l_req_ids(l_req_ids.LAST)
                    USING l_user_id;
                l_req_ids.EXTEND;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;
        END LOOP;
        l_req_ids.TRIM;
        RETURN l_req_ids;
    END;
 */
    FUNCTION GET_LINKS
     ( l_usage IN LINK.USAGE%TYPE,
       l_user_id IN USER_LOGIN.USER_ID%TYPE)
     RETURN  GLOBALS_PKG.REF_CURSOR
    IS
        l_cur GLOBALS_PKG.REF_CURSOR;
        --l_reqs ID_LIST;
    BEGIN
        --l_reqs := GET_REQS_MET(l_user_id);
        OPEN l_cur FOR
            SELECT L.LINK_ID, L.TITLE, l_usage, L.DESCRIPTION, L.CATEGORY, L.SEQ
              FROM LINK L, (
                SELECT * FROM VW_CUSTOM_LINKS
                 WHERE USER_ID = l_user_id AND USAGE = l_usage
                ) AL
              WHERE L.LINK_ID = AL.LINK_ID (+)
                AND l_usage IN(L.USAGE, AL.USAGE)
                AND NVL(AL.INCLUDE, 'Y') = 'Y'
               /*AND TABLE(SELECT LINK_ID FROM LINK_REQUIREMENT WHERE LR.LINK_ID = L.LINK_ID)
                     SUBMULTISET OF TABLE(l_reqs) */
             ORDER BY SEQ ASC, UPPER(CATEGORY) ASC, UPPER(TITLE) ASC;
             
        RETURN l_cur;
    END;
    
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PACKAGE BODY globals_pkg
IS
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_date_list REPORT.DATE_LIST := REPORT.DATE_LIST();
        l_date DATE;
        l_real_end_date DATE;
    BEGIN
        IF (l_days <= 0 AND l_months <= 0) OR l_days + l_months * 30 <= 0 THEN
            RAISE_APPLICATION_ERROR(-20900, 'Invalid days and months specified (Days = '
                || TO_CHAR(l_days) || ', Months = ' || TO_CHAR(l_months)
                || '); would result in endless loop');
        ELSE
            SELECT ADD_MONTHS(TRUNC(l_start_date, l_trunc_to) + (l_days * (1-l_num_before)), l_months * (1-l_num_before)),
                   ADD_MONTHS(l_end_date + (l_days * l_num_after), l_months * l_num_after)
              INTO l_date, l_real_end_date
              FROM DUAL;
            WHILE l_date < l_real_end_date LOOP
                l_date_list.EXTEND;
                l_date_list(l_date_list.LAST) := l_date;
                SELECT ADD_MONTHS(l_date + l_days, l_months)
                  INTO l_date
                  FROM DUAL;
            END LOOP;
        END IF;
        RETURN l_date_list ;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_trunc_to FREQUENCY.INTERVAL%TYPE;
        l_months FREQUENCY.MONTHS%TYPE;
        l_days FREQUENCY.DAYS%TYPE;
    BEGIN
        SELECT INTERVAL, MONTHS, DAYS
          INTO l_trunc_to, l_months, l_days
          FROM FREQUENCY
         WHERE FREQUENCY_ID = l_frequency_id;
         RETURN GET_DATE_LIST(l_start_date,l_end_date,l_trunc_to,l_months,l_days,l_num_before,l_num_after);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20901, 'Invalid frequency id (' || TO_CHAR(l_frequency_id) || ')');
        WHEN OTHERS THEN
            RAISE;
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PACKAGE BODY PAYMENTS_PKG
IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        IF l_always_as_accum IN('Y') THEN
            l_pay_sched := 1;
        ELSIF l_always_as_accum IN('A') THEN
            l_pay_sched := 5;
        ELSE
            SELECT PAYMENT_SCHEDULE_ID
              INTO l_pay_sched
              FROM TERMINAL
             WHERE TERMINAL_ID = l_terminal_id;
        END IF;
        BEGIN
            SELECT B.BATCH_ID
              INTO l_batch_id
              FROM BATCH B
             WHERE B.TERMINAL_ID = l_terminal_id
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
               AND B.CLOSED = 'N'
               -- for "As Accumulated", batch start date does not matter
               AND DECODE(l_pay_sched, 1, MAX_DATE, 5, MAX_DATE, l_entry_date) >= B.START_DATE
               AND l_entry_date < NVL(B.END_DATE, MAX_DATE);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- calc start and end dates
                IF l_pay_sched IN(1, 5) THEN
                    SELECT LEAST(NVL(MAX(B.END_DATE), MIN_DATE), l_entry_date)
                      INTO l_start_date
                      FROM BATCH B
                     WHERE B.TERMINAL_ID = l_terminal_id
                       AND B.CUSTOMER_BANK_ID = l_cust_bank_id
                       AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                    -- end date is null
                ELSIF l_pay_sched = 2 THEN
                    SELECT NVL(MAX(f.fill_date), MIN_DATE)
                      INTO l_start_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE <= l_entry_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                    SELECT MIN(f.fill_date)
                      INTO l_end_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_entry_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                ELSE
                    SELECT TRUNC(l_entry_date, INTERVAL),
                           ADD_MONTHS(TRUNC(l_entry_date, INTERVAL) + DAYS, MONTHS)
                      INTO l_start_date, l_end_date
                      FROM PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                    IF l_end_date <= l_entry_date THEN -- trouble, should not happen
                        l_end_date := l_entry_date + (1.0/(24*60*60));
                    END IF;
                END IF;
                -- create new batch record
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, CUSTOMER_BANK_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE)
                    VALUES(l_batch_id, l_cust_bank_id, l_terminal_id, l_pay_sched, l_start_date, l_end_date);
            WHEN OTHERS THEN
                RAISE;
        END;
        RETURN l_batch_id;
    END;

    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_new_batch_id BATCH.BATCH_ID%TYPE;
        l_terminal_id BATCH.TERMINAL_ID%TYPE;
        l_customer_bank_id BATCH.CUSTOMER_BANK_ID%TYPE;
    BEGIN
        SELECT B.TERMINAL_ID, B.CUSTOMER_BANK_ID
          INTO l_terminal_id, l_customer_bank_id
          FROM BATCH B
          WHERE B.BATCH_ID = l_batch_id;
        l_new_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_entry_date, 'Y');
        RETURN l_new_batch_id;
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE)
     RETURN CHAR
    IS
        l_cnt NUMBER;
    BEGIN
        IF l_pay_sched IN(1, 5) THEN
            RETURN 'Y';
        ELSIF l_end_date IS NULL THEN -- batch is fill-to-fill and not closed yet
            RETURN 'N';
        END IF;
        SELECT COUNT(*)
          INTO l_cnt
          FROM LEDGER l
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
           AND L.DELETED = 'N'
           AND L.BATCH_ID = l_batch_id;
        IF l_cnt = 0 THEN
            RETURN 'Y';
        END IF;
        RETURN 'N';
    END;

    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE
          INTO l_pay_sched, l_end_date
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        RETURN BATCH_CLOSABLE(l_batch_id, l_pay_sched, l_end_date);
    END;

    PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    TRANS.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_check_cur IS
           SELECT L.TRANS_ID
              FROM LEDGER L, BATCH B
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
             GROUP BY L.TRANS_ID
             HAVING COUNT(DECODE(B.CLOSED, 'Y', 1, 0)) > 0
                AND COUNT(DECODE(B.CLOSED, 'N', 1, 0)) = 0;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        FOR l_check_rec IN l_check_cur LOOP
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B
             WHERE L.TRANS_ID = l_check_rec.TRANS_ID
               AND L.BATCH_ID = B.BATCH_ID
               AND B.CLOSED = 'Y';
            IF l_batch_id IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20701, 'PROCESS FEE (process_fee_id='||TO_CHAR(l_process_fee_id)||' is used in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
            END IF;
        END LOOP;
    END;

    -- Checks if the specified transaction is in a closed batch and if so raises an
    -- exception
    PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    TRANS.TRAN_ID%TYPE)
    IS
        l_closed_cnt PLS_INTEGER;
        l_open_cnt PLS_INTEGER;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- get the batch & status
        SELECT COUNT(DECODE(B.CLOSED, 'Y', 1, 0)),
               COUNT(DECODE(B.CLOSED, 'N', 1, 0))
          INTO l_closed_cnt, l_open_cnt
          FROM LEDGER L, BATCH B
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID;
        IF l_open_cnt = 0 AND l_closed_cnt > 0 THEN
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.CLOSED = 'Y';
            RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
        END IF;
    END;
    
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date   TRANS.SETTLE_DATE%TYPE)
    IS
    BEGIN
        CHECK_TRANS_CLOSED(l_trans_id); --just double-check
        UPDATE LEDGER SET SETTLE_STATE_ID = l_settle_state_id,
            LEDGER_DATE = l_settle_date
          WHERE TRANS_ID = l_trans_id;
    EXCEPTION
        WHEN TRANS_BATCH_CLOSED THEN
            DECLARE
                l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                l_close_date TRANS.CLOSE_DATE%TYPE;
                l_total_amount TRANS.TOTAL_AMOUNT%TYPE;
                l_terminal_id TRANS.TERMINAL_ID%TYPE;
                l_customer_bank_id TRANS.CUSTOMER_BANK_ID%TYPE;
                l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE;
            BEGIN
                SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                       CUSTOMER_BANK_ID, PROCESS_FEE_ID
                  INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                       l_customer_bank_id, l_process_fee_id
                  FROM TRANS
                 WHERE TRAN_ID = l_trans_id;

                UPDATE_LEDGER(l_trans_id, l_trans_type_id, l_close_date,
                    l_settle_date, l_total_amount, l_settle_state_id,
                    l_terminal_id, l_customer_bank_id,l_process_fee_id);
            END;
        WHEN OTHERS THEN
            RAISE;
    END;

    PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_tran_cur IS
           SELECT L.TRANS_ID,
                  --COUNT(DECODE(B.CLOSED, 'Y', 1, 0)) CLOSED_CNT,
                  --COUNT(DECODE(B.CLOSED, 'N', 1, 0))OPEN_CNT,
                  MAX(DECODE(B.CLOSED, 'N', B.BATCH_ID, NULL)) LAST_OPEN_BATCH_ID
              FROM LEDGER L, BATCH B
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
               AND L.ENTRY_TYPE = 'PF'
             GROUP BY L.TRANS_ID;
    BEGIN
        FOR l_tran_rec IN l_tran_cur LOOP
            IF l_tran_rec.LAST_OPEN_BATCH_ID IS NOT NULL THEN -- we can do direct update Yippee!
                UPDATE LEDGER L SET L.AMOUNT = (
                    SELECT T.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                      FROM TRANS T, PROCESS_FEES PF
                     WHERE T.TRAN_ID = l_tran_rec.TRANS_ID
                       AND PF.PROCESS_FEE_ID = l_process_fee_id)
                 WHERE L.ENTRY_TYPE = 'PF'
                   AND L.TRANS_ID = l_tran_rec.TRANS_ID
                   AND L.BATCH_ID = l_tran_rec.LAST_OPEN_BATCH_ID
                   AND L.PROCESS_FEE_ID = l_process_fee_id;
            ELSE
                DECLARE
                    l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                    l_close_date TRANS.CLOSE_DATE%TYPE;
                    l_total_amount TRANS.TOTAL_AMOUNT%TYPE;
                    l_terminal_id TRANS.TERMINAL_ID%TYPE;
                    l_customer_bank_id TRANS.CUSTOMER_BANK_ID%TYPE;
                    l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE;
                    l_settle_date TRANS.SETTLE_DATE%TYPE;
                BEGIN
                    SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                           CUSTOMER_BANK_ID, SETTLE_STATE_ID, SETTLE_DATE
                      INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                           l_customer_bank_id, l_settle_state_id, l_settle_date
                      FROM TRANS
                     WHERE TRAN_ID = l_tran_rec.TRANS_ID;

                    UPDATE_LEDGER(l_tran_rec.TRANS_ID, l_trans_type_id, l_close_date,
                        l_settle_date, l_total_amount, l_settle_state_id,
                        l_terminal_id, l_customer_bank_id,l_process_fee_id);
                END;
            END IF;
        END LOOP;
    END;

    -- Puts CC, RF, or CB record into Ledger and also any required PF record
    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_batch_id LEDGER.BATCH_ID%TYPE)
    IS
    BEGIN
        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            TRANS_ID,
            PROCESS_FEE_ID,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE)
        SELECT
            LEDGER_SEQ.NEXTVAL,
            DECODE(l_trans_type_id, 16, 'CC', 19, 'CC', 20, 'RF', 21, 'CB'),--, 'AU'), -- Audit trail of other transactions
            l_trans_id,
            l_process_fee_id,
            DECODE(l_trans_type_id, 20, -ABS(l_amount), 21, -ABS(l_amount), l_amount),
            l_close_date,
            l_batch_id,
            l_settle_state_id,
            l_settle_date
          FROM DUAL
          WHERE l_trans_type_id IN(16,19,20,21);

        IF l_process_fee_id IS NOT NULL THEN
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'PF',
                l_trans_id,
                l_process_fee_id,
                -(ABS(l_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT),
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date
            FROM PROCESS_FEES pf
            WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
        END IF;
    END;

    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_id    TRANS.TRAN_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    TRANS.CLOSE_DATE%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_terminal_id   TRANS.TERMINAL_ID%TYPE,
        l_customer_bank_id  TRANS.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    TRANS.PROCESS_FEE_ID%TYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_batch_closed BATCH.CLOSED%TYPE;
    BEGIN
        IF l_trans_type_id IN(22) THEN -- cash; ignore
            RETURN;
        END IF;
        -- get the batch & status
        SELECT MAX(B.BATCH_ID), MAX(B.CLOSED) -- are there ANY closed batches on this transaction?
          INTO l_batch_id, l_batch_closed
          FROM LEDGER L, BATCH B
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID;
        IF l_batch_id IS NULL THEN -- Create new batch for this ledger record
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                -- if refund or chargeback get the batch that the
                -- original trans is in, if open use it else use "as accumulated"
                IF l_trans_type_id IN(20, 21) THEN
                    BEGIN
                        SELECT B.BATCH_ID, B.CLOSED
                          INTO l_batch_id, l_batch_closed
                          FROM LEDGER L, BATCH B, TRANS T
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_id
                           AND L.BATCH_ID = B.BATCH_ID;
                        IF NVL(l_batch_closed, 'Y') = 'Y' THEN
                            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, 'Y');
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                        -- The following should be removed and the error re-enabled,
                        -- once we enforce orig_tran_id for all refunds/ chargebacks
                        l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, 'Y');
                        --RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                        WHEN OTHERS THEN
                            RAISE;
                    END;
                ELSE
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, 'N');
                END IF;
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
            END IF;
        ELSIF l_batch_closed = 'Y' THEN -- add adjustment for prev paid
            DECLARE
                CURSOR l_prev_cur IS
                    SELECT COUNT(*) PAID_CNT, SUM(AMOUNT) PAID_AMT, B.TERMINAL_ID, B.CUSTOMER_BANK_ID
                  FROM LEDGER L, BATCH B
                 WHERE L.TRANS_ID = l_trans_id
                   AND ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = 'Y'
                   AND L.BATCH_ID = B.BATCH_ID
                 GROUP BY B.TERMINAL_ID, B.CUSTOMER_BANK_ID;
                 
                l_adj_amt LEDGER.AMOUNT%TYPE;
                l_adjusted_new_cb BOOLEAN := FALSE;
            BEGIN
                --figure out what change has occurred
                FOR l_prev_rec IN l_prev_cur LOOP
                    IF NVL(l_customer_bank_id, 0) = l_prev_rec.CUSTOMER_BANK_ID THEN
                        IF NVL(l_prev_rec.PAID_CNT, 0) = 0 THEN
                            -- previous trans must have failed so create normal
                            -- record in a 'Adjustment for Prev Paid' batch
                            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                                l_customer_bank_id, l_close_date, 'A');
                            INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                                l_close_date, l_settle_date, l_total_amount,
                                l_settle_state_id, l_process_fee_id, l_batch_id);
                        ELSE
                            --calc difference and add as adjustment
                            SELECT l_total_amount
                                   - NVL(SUM(NVL(ABS(l_total_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT, 0)), 0)
                                   - l_prev_rec.PAID_AMT
                              INTO l_adj_amt
                              FROM PROCESS_FEES pf
                             WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                             
                             IF l_adj_amt <> 0 THEN
                                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                                        l_customer_bank_id, l_close_date, 'A');
                                -- create a negative adjustment based on this previous payment
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                SELECT
                                    LEDGER_SEQ.NEXTVAL,
                                    'AD',
                                    l_trans_id,
                                    NULL,
                                    l_adj_amt,
                                    l_close_date,
                                    l_batch_id,
                                    l_settle_state_id,
                                    l_settle_date,
                                    'Correction for process fee change or value amount change on a transaction that has already been paid'
                                  FROM DUAL;
                             END IF;
                        END IF;
                        l_adjusted_new_cb := TRUE;
                    --case: bank acct changed
                    ELSIF l_prev_rec.PAID_AMT > 0 THEN
                        -- deduct entire amt from old
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            'AD',
                            l_trans_id,
                            NULL,
                            -l_prev_rec.PAID_AMT,
                            l_close_date,
                            GET_OR_CREATE_BATCH(l_prev_rec.TERMINAL_ID, l_prev_rec.CUSTOMER_BANK_ID, l_close_date, 'A'),
                            l_settle_state_id,
                            l_settle_date,
                            'Adjustment for transaction that was paid to the wrong bank account'
                          FROM DUAL;
                    END IF;
                END LOOP;
                IF NOT l_adjusted_new_cb AND l_customer_bank_id IS NOT NULL THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                        l_customer_bank_id, l_close_date, 'A');
                    INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                        l_close_date, l_settle_date, l_total_amount,
                        l_settle_state_id, l_process_fee_id, l_batch_id);
                END IF;
            END;
        ELSE -- an open batch exists
            -- To make it easy let's just delete what's there and re-insert
            DELETE FROM LEDGER WHERE TRANS_ID = l_trans_id;
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
            END IF;
        END IF;
    END;
    
    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.START_DATE
              FROM BATCH B, TERMINAL_EPORT TE, FILL F
             WHERE B.START_DATE < F.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > F.FILL_DATE
               AND B.CLOSED = 'N'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND B.TERMINAL_ID = TE.TERMINAL_ID
               AND TE.EPORT_ID = F.EPORT_ID
               AND F.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
               AND F.FILL_ID = l_fill_id;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(f.FILL_DATE)
              INTO l_end_date
              FROM FILL F, TERMINAL_EPORT TE
             WHERE F.FILL_DATE > l_batch_rec.START_DATE
               AND F.EPORT_ID = TE.EPORT_ID
               AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE)
               AND TE.TERMINAL_ID = l_batch_rec.TERMINAL_ID;
            -- update batch record
            UPDATE BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
             
            -- also update ledger.batch_id of entries that are after fill_date
            UPDATE LEDGER L SET L.BATCH_ID =
                GET_OR_CREATE_BATCH(l_batch_rec.TERMINAL_ID, l_batch_rec.CUSTOMER_BANK_ID, L.ENTRY_DATE,'N')
             WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
               AND L.ENTRY_DATE >= l_end_date;
         END LOOP;
    END;
    
    -- Creates a document (eft or invoice or check) for the given bank account
    -- doc_type: 'FT' = Funds Transfer (EFT); 'IN' = Invoice; 'CK' = Check
    PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_doc_id        DOC.DOC_ID%TYPE;
        l_batch_ref	    DOC.REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         DOC.TOTAL_AMOUNT%TYPE;
        l_dates         REPORT.DATE_LIST;
        l_now           DATE := SYSDATE;
    BEGIN
        IF l_check_min = 'Y' THEN
            -- ensure that doc amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(L.AMOUNT), 0) INTO l_total
              FROM LEDGER L, BATCH B
             WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE NOT IN('AU')
               AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE) = 'Y';
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'Doc total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        END IF;

        -- Create Doc
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_doc_id, l_batch_ref
          FROM DUAL;
        INSERT INTO DOC(DOC_ID, DOC_TYPE, REF_NBR, DESCRIPTION, CUSTOMER_BANK_ID,
            BANK_ACCT_NBR, BANK_ROUTING_NBR, CREATE_BY)
            SELECT l_doc_id, l_doc_type, l_batch_ref,
                NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, CUSTOMER_BANK_ID,
                BANK_ACCT_NBR, BANK_ROUTING_NBR, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        -- Moves batches into doc
        UPDATE BATCH B SET B.DOC_ID = l_doc_id
         WHERE B.CUSTOMER_BANK_ID = l_cust_bank_id
           AND B.CLOSED = 'N'
           AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE) = 'Y';
        -- remove any unsettled trans from as accumulated batches
        DECLARE
            CURSOR l_unsettled_cur IS
                SELECT L.LEDGER_ID, L.BATCH_ID, L.ENTRY_DATE
                  FROM LEDGER L, BATCH B
                 WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
                   AND L.BATCH_ID = B.BATCH_ID
                   AND B.PAYMENT_SCHEDULE_ID = 1
                   AND B.DOC_ID = l_doc_id;
            l_batch_id LEDGER.BATCH_ID%TYPE;
        BEGIN
            FOR l_unsettled_rec IN l_unsettled_cur LOOP
                l_batch_id := GET_NEW_BATCH(l_unsettled_rec.BATCH_ID, l_unsettled_rec.ENTRY_DATE);
                UPDATE LEDGER L SET L.BATCH_ID = l_batch_id
                 WHERE L.LEDGER_ID = l_unsettled_rec.LEDGER_ID;
            END LOOP;
        END;
    END; -- end of procedure create_doc(...)

/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/

    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
                SELECT CUSTOMER_BANK_ID,
                       SUM(DECODE(ENTRY_TYPE, 'CC', AMOUNT, NULL)) CREDIT_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'RF', AMOUNT, NULL)) REFUND_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'CB', AMOUNT, NULL)) CHARGEBACK_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'PF', AMOUNT, NULL)) PROCESS_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'SF', AMOUNT, NULL)) SERVICE_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'AD', AMOUNT, NULL)) ADJUST_AMOUNT
                  FROM (
                    SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
                      FROM LEDGER L, BATCH B, DOC D
                      WHERE L.BATCH_ID = B.BATCH_ID
                      AND L.DELETED = 'N'
                      AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                      AND B.DOC_ID = D.DOC_ID (+)
                      AND NVL(D.SENT_DATE, MIN_DATE) < l_as_of
                      AND L.LEDGER_DATE < l_as_of
                      GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
                  GROUP BY CUSTOMER_BANK_ID) A
            WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
            AND CB.CUSTOMER_ID = C.CUSTOMER_ID
            ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR;
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        UPDATE LEDGER L
           SET BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.ENTRY_DATE)
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_term_id BATCH.TERMINAL_ID%TYPE;
    BEGIN
        IF l_terminal_id = 0 THEN
            l_term_id := NULL;
        ELSE
            l_term_id := l_terminal_id;
        END IF;
        IF l_doc_id <> 0 THEN
            BEGIN
                SELECT BATCH_ID
                  INTO l_batch_id
                  FROM BATCH
                 WHERE DOC_ID = l_doc_id
                   AND CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(TERMINAL_ID, 0) = NVL(l_terminal_id, 0);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    -- create a batch and set the doc id
                    SELECT BATCH_SEQ.NEXTVAL
                      INTO l_batch_id
                      FROM DUAL;
                    INSERT INTO BATCH(BATCH_ID, DOC_ID, CUSTOMER_BANK_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE)
                        VALUES(l_batch_id, l_doc_id, l_cust_bank_id, l_term_id, 1, SYSDATE, SYSDATE);
                WHEN OTHERS THEN
                    RAISE;
            END;
        ELSE
            l_batch_id := GET_OR_CREATE_BATCH(l_term_id,l_cust_bank_id, SYSDATE, 'Y');
        END IF;
    	SELECT LEDGER_SEQ.NEXTVAL
          INTO l_ledger_id FROM DUAL;
    	INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION,
            CREATE_BY)
           VALUES (
            l_ledger_id,
            'AD',
            l_amt,
            SYSDATE,
            l_batch_id,
            2 /*process-no require*/,
            SYSDATE,
            l_reason,
            l_user_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
    END;
    
    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC D SET APPROVE_BY = l_user_id, UPDATE_DATE= SYSDATE, STATUS = 'A',
            TOTAL_AMOUNT = (SELECT SUM(AMOUNT)
                FROM LEDGER L, BATCH B
               WHERE L.DELETED = 'N'
                 AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.DOC_ID = D.DOC_ID)
  	 		WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
    	 UPDATE DOC D SET APPROVE_BY = NULL, UPDATE_DATE = SYSDATE, STATUS = 'L',
            TOTAL_AMOUNT = NULL
  	 		WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC SET SENT_BY = l_user_id, UPDATE_DATE = SYSDATE, STATUS = 'P',
                SENT_DATE = SYSDATE
          WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_effective_date < SYSDATE THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.CLOSED = 'Y';
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
    	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 DELETE FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(START_DATE, MIN_DATE) >= NVL(END_DATE, MAX_DATE);	
    	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
     	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_effective_date);
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   --FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   SF.END_DATE,
                   F.FEE_NAME
              FROM SERVICE_FEES SF, FREQUENCY Q, CUSTOMER_BANK_TERMINAL CBT, FEES F
             WHERE SF.FREQUENCY_ID = Q.FREQUENCY_ID
               AND SF.TERMINAL_ID = CBT.TERMINAL_ID
               AND SF.FEE_ID = F.FEE_ID
               AND WITHIN1(SYSDATE, CBT.START_DATE, CBT.END_DATE) = 1
               AND (Q.DAYS <> 0 OR Q.MONTHS <> 0)
               AND ADD_MONTHS(SF.LAST_PAYMENT + Q.DAYS, Q.MONTHS) < LEAST(NVL(SF.END_DATE, MAX_DATE), SYSDATE);

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
    BEGIN
        FOR r_fee IN c_fee LOOP
            l_last_payment := r_fee.LAST_PAYMENT;
            IF r_fee.MONTHS >= 12  AND MOD(r_fee.MONTHS, 12.0) = 0 THEN
                l_fmt := 'FMYYYY';
            ELSIF r_fee.MONTHS > 0 THEN
                l_fmt := 'FMMonth, YYYY';
            ELSIF r_fee.DAYS >= 1 AND MOD(r_fee.DAYS, 1.0) = 0 THEN
                l_fmt := 'FMMM/DD/YYYY';
            ELSE
                l_fmt := 'FMMM/DD/YYYY HH:MI AM';
            END IF;
            LOOP
                IF r_fee.MONTHS > 0 THEN
                    SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                      INTO l_last_payment
                      FROM DUAL;
                ELSE
                    SELECT l_last_payment + r_fee.DAYS
                      INTO l_last_payment
                      FROM DUAL;
                END IF;
                EXIT WHEN l_last_payment > LEAST(NVL(r_fee.END_DATE, MAX_DATE), SYSDATE);

                BEGIN -- catch exception here
                    -- skip creation of fee if start date is after fee date
                    IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES (
                            LEDGER_SEQ.NEXTVAL,
                            'SF',
                            r_fee.SERVICE_FEE_ID,
                            -ABS(r_fee.FEE_AMOUNT),
                            l_last_payment,
                            GET_OR_CREATE_BATCH(r_fee.TERMINAL_ID, r_fee.CUSTOMER_BANK_ID, l_last_payment, 'Y'),
                            2,
                            l_last_payment,
                            r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt));
                    END IF;

                    UPDATE SERVICE_FEES
                       SET LAST_PAYMENT = l_last_payment
                     WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;

                    COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                EXCEPTION
    				 WHEN DUP_VAL_ON_INDEX THEN
    				 	 ROLLBACK;
                END;
            END LOOP;
        END LOOP;
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
       l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
    BEGIN
    	 BEGIN
    		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
    	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
    		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
    		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
    		END IF;
    	 EXCEPTION
    	 	WHEN NO_DATA_FOUND THEN
    			 NULL;
    		WHEN OTHERS THEN
    			 RAISE;
    	 END;
    	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 IF l_fee_amt <> 0 THEN
             INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
    		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
    	 END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
    	 SELECT STATUS INTO l_status FROM DOC WHERE DOC_ID = l_doc_id;
    	 IF l_status <> 'L' THEN
    	 	 RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
    	 END IF;
    	 UPDATE BATCH SET DOC_ID = NULL WHERE DOC_ID = l_doc_id;
    	 DELETE FROM DOC WHERE DOC_ID = l_doc_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE DATA_IN_PKG
  IS
--
-- Receives data from external systems
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- BKRUG        10-18-04    NEW

  PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PLS_INTEGER DEFAULT 1,
        l_price PURCHASE.AMOUNT%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL);
        
  PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/);
        
  PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE);
       
  PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_track_data TRANS.TRACK_DATA%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL);
        
  PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE);
END; -- Package spec

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE DW_PKG
  IS
--
-- Holds code to synchronize dw tables
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       


  PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE);
        
  PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE);
        
  PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE);
        
  PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE);
END; -- Package spec

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE SYNC_JOB_PKG
  IS


  PROCEDURE PROCESS_SYNCS;
  
  FUNCTION RUN_SYNC_JOB
     RETURN BINARY_INTEGER;
     
  PROCEDURE STOP_SYNC_JOB;
  
END; -- Package spec

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE PACKAGE SYNC_PKG
  IS
--
-- All the procedures for updating ledger tables and trans mart tables
-- whenever trans, terminal, location, terminal_device, customer_bank_terminal,
-- process_fees change
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B KRUG       10-04-04 NEW
  --Q_SYNC_COMSUMER CONSTANT SYS.AQ$_AGENT := SYS.AQ$_AGENT('SYNC', 'REPORT.Q_SYNC_MSG', 0);

  PROCEDURE RESYNC_ALL_TRANS;
  
  PROCEDURE RESYNC_TRANS_BETWEEN(
        l_start_tran_id TRANS.TRAN_ID%TYPE DEFAULT 0,
        l_end_tran_id TRANS.TRAN_ID%TYPE DEFAULT 99999999999999999999999999999999999999);
        
  PROCEDURE SYNC_FILL_INSERT(
        l_fill_id FILL.FILL_ID%TYPE);

  PROCEDURE SYNC_TRANS_CUST_BANK(
        l_trans_id TRANS.tran_id%TYPE);
        
  PROCEDURE SYNC_TRANS_INSERT(
        l_trans_id TRANS.tran_id%TYPE);
        
  PROCEDURE SYNC_TRANS_PROCESS_FEE(
        l_trans_id TRANS.tran_id%TYPE);
        
  PROCEDURE SYNC_TRANS_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE);
        
  PROCEDURE SYNC_TRANS_TERMINAL(
        l_trans_id TRANS.tran_id%TYPE);
        
  PROCEDURE SYNC_TRANS_PROCESS_FEE(
        l_list_of_ids ID_LIST);
        
  PROCEDURE SYNC_TRANS_CUST_BANK(
        l_list_of_ids ID_LIST);
        
  PROCEDURE SYNC_TRANS_TERMINAL(
        l_list_of_ids ID_LIST);
        
  PROCEDURE SYNC_PROCESS_FEE_VALUES(
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE);

  PROCEDURE SYNC_TERMINAL_INFO(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE);

  PROCEDURE SYNC_TERMINAL_INFO(
        l_list_of_ids ID_LIST);

  PROCEDURE SYNC_REGION_NAME(
        l_region_id REGION.REGION_ID%TYPE,
        l_region_name REGION.REGION_NAME%TYPE);

  -- the following procedures are called by the sync job
  PROCEDURE RECEIVE_FILL_INSERT(
        l_fill_id FILL.FILL_ID%TYPE);
        
  PROCEDURE RECEIVE_PROCESS_FEE_VALUES(
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE);
        
  PROCEDURE RECEIVE_TRANS_CUST_BANK(
        l_trans_id TRANS.TRAN_ID%TYPE);
        
  PROCEDURE RECEIVE_TRANS_INSERT(
        l_trans_id TRANS.TRAN_ID%TYPE);
        
  PROCEDURE RECEIVE_TRANS_PROCESS_FEE(
        l_trans_id TRANS.TRAN_ID%TYPE);
        
  PROCEDURE RECEIVE_TRANS_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE);
        
  PROCEDURE RECEIVE_TRANS_TERMINAL(
        l_trans_id TRANS.TRAN_ID%TYPE);
        
  PROCEDURE RECEIVE_TERMINAL_INFO(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE);

END; -- Package spec

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PACKAGE globals_pkg
  IS
--
-- Holds any global constants or types
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B Krug       09-16-04 NEW
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;

    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST;
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST;

END; -- Package spec

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE PACKAGE PAYMENTS_PKG
  IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW

   /*PROCEDURE AUTO_CREATE_EFTS;
   PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N');

   PROCEDURE REFRESH_PENDING_REVENUE;
   */
   
   TRANS_BATCH_CLOSED EXCEPTION;
   PRAGMA EXCEPTION_INIT(TRANS_BATCH_CLOSED, -20701);

   FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR;
   /*
   FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR;
     */
   FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;

   /*
   FUNCTION GET_NOT_PAYABLE_COUNT(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE,
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_id%TYPE,
        l_close_date LEDGER.CLOSE_DATE%TYPE)
     RETURN NUMBER
     PARALLEL_ENABLE;
*/

  PROCEDURE UPDATE_LEDGER(
        l_trans_id    TRANS.TRAN_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    TRANS.CLOSE_DATE%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_terminal_id   TRANS.TERMINAL_ID%TYPE,
        l_customer_bank_id  TRANS.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    TRANS.PROCESS_FEE_ID%TYPE);

  PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N');
        
  FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE;
     
  PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE);
      
  PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE SCAN_FOR_SERVICE_FEES;
  
  PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE;
     
  FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE;

  PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE);
        
  PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    TRANS.TRAN_ID%TYPE)
  PARALLEL_ENABLE;

  PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date   TRANS.SETTLE_DATE%TYPE);
        
  PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    TRANS.PROCESS_FEE_ID%TYPE)
  PARALLEL_ENABLE;
        
  PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id TRANS.PROCESS_FEE_ID%TYPE);
        
  PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE);
END; -- Package spec

/

CREATE OR REPLACE FORCE VIEW REPORT.VW_ACTIVITY(
	REGION_ID,
	REGION_NAME,
	LOCATION_NAME,
	LOCATION_ID,
	TERMINAL_ID,
	TERMINAL_NAME,
	TERMINAL_NBR,
	TRAN_ID,
	TRANS_TYPE_ID,
	TRANS_TYPE_NAME,
	CARD_NUMBER,
	CARD_COMPANY,
	CC_APPR_CODE,
	TRAN_DATE,
	MONTH_RANGE,
	WEEK_RANGE,
	DAY_RANGE,
	FILL_DATE,
	TOTAL_AMOUNT,
	ORIG_TRAN_ID,
	BATCH_ID,
	VEND_COLUMN,
	QUANTITY,
	REF_NBR,
	DESCRIPTION,
	ASSET_NBR,
	MAKE,
	MODEL,
	EPORT_SERIAL_NUM,
	CUSTOMER_NAME,
	EPORT_ID)
 AS 
SELECT REGION_ID, 
REGION_NAME, LOCATION_NAME, A.LOCATION_ID, A.TERMINAL_ID, 
T.TERMINAL_NAME, T.TERMINAL_NBR, TRAN_ID, TRANS_TYPE_ID, 
TRANS_TYPE_NAME, MASK_CARD(CARD_NUMBER,TRANS_TYPE_ID), CARD_COMPANY, CC_APPR_CODE, 
TRAN_DATE, MONTH_RANGE, WEEK_RANGE, DAY_RANGE, 
FILL_DATE, TOTAL_AMOUNT, ORIG_TRAN_ID, BATCH_ID, VEND_COLUMN, QUANTITY, REF_NBR, 
A.DESCRIPTION, ASSET_NBR, MAKE, MODEL, EPORT_SERIAL_NUM, CUSTOMER_NAME, A.EPORT_ID 
FROM ACTIVITY_REF A, TERMINAL T, MACHINE M, EPORT E, CUSTOMER C 
WHERE A.TERMINAL_ID = T.TERMINAL_ID (+) AND T.MACHINE_ID = M.MACHINE_ID (+) AND A.EPORT_ID = E.EPORT_ID 
AND T.CUSTOMER_ID = C.CUSTOMER_ID (+)

/

CREATE OR REPLACE FORCE VIEW REPORT.VW_ACTIVITY_REF_TEST(
	REGION_ID,
	REGION_NAME,
	LOCATION_NAME,
	LOCATION_ID,
	TERMINAL_ID,
	TERMINAL_NAME,
	TERMINAL_NBR,
	TRAN_ID,
	TRANS_TYPE_ID,
	TRANS_TYPE_NAME,
	CARD_NUMBER,
	CARD_COMPANY,
	CC_APPR_CODE,
	TRAN_DATE,
	MONTH_RANGE,
	WEEK_RANGE,
	DAY_RANGE,
	FILL_DATE,
	TOTAL_AMOUNT,
	ORIG_TRAN_ID,
	BATCH_ID,
	VEND_COLUMN,
	QUANTITY,
	REF_NBR,
	EPORT_ID,
	DESCRIPTION)
 AS 
(select a.*  
from report.activity_ref a  
where a.tran_id IN (SELECT tran_id  
FROM report.trans  
WHERE status = 'U') )

/

CREATE OR REPLACE FORCE VIEW REPORT.VW_PAYMENT_TRANS(
	EFT_ID,
	PAYMENT_ID,
	TRAN_ID,
	REF_NBR)
 AS 
SELECT DISTINCT
	B.DOC_ID, L.BATCH_ID, L.TRANS_ID, A.REF_NBR
FROM BATCH B, LEDGER L, ACTIVITY_REF A
WHERE 
	 L.BATCH_ID = B.BATCH_ID AND L.TRANS_ID = A.TRAN_ID
	 AND CORP.PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'

/


CREATE OR REPLACE FORCE VIEW REPORT.VW_UNSENT_EFT(
	USER_REPORT_ID,
	BATCH_ID)
 AS 
SELECT UR.USER_REPORT_ID, D.DOC_ID
FROM USER_REPORT UR, CORP.DOC D, REPORTS R
WHERE UR.STATUS = 'A' AND D.STATUS = 'P' AND UR.REPORT_ID = R.REPORT_ID
AND R.BATCH_TYPE_ID = 3 AND UR.UPD_DT  < D.SENT_DATE
AND EXISTS(SELECT * FROM VW_USER_CUSTOMER_BANK UCB WHERE UR.USER_ID = UCB.USER_ID AND UCB.CUSTOMER_BANK_ID = D.CUSTOMER_BANK_ID)
AND NOT EXISTS(SELECT * FROM REPORT_SENT S WHERE S.USER_REPORT_ID = UR.USER_REPORT_ID AND S.BATCH_ID = D.DOC_ID)

/

CREATE OR REPLACE FORCE VIEW CORP.VW_ADJUSTMENTS(
	DOC_ID,
	LEDGER_ID,
	AMOUNT,
	REASON,
	ADJUST_DATE,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	BATCH_CLOSED,
	CREATE_BY)
 AS 
SELECT B.DOC_ID, L.LEDGER_ID, L.AMOUNT, L.DESCRIPTION REASON, L.LEDGER_DATE ADJUST_DATE,
B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.CLOSED, L.CREATE_BY
FROM LEDGER L, BATCH B
WHERE L.ENTRY_TYPE = 'AD'
AND L.DELETED = 'N' AND L.BATCH_ID = B.BATCH_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_APPROVED_PAYMENTS(
	EFT_ID,
	BATCH_REF_NBR,
	DESCRIPTION,
	CUSTOMER_BANK_ID,
	CUSTOMER_NAME,
	BANK_ACCT_NBR,
	BANK_ROUTING_NBR,
	EFT_AMOUNT)
 AS 
SELECT D.DOC_ID, D.REF_NBR, D.DESCRIPTION, D.CUSTOMER_BANK_ID, C.CUSTOMER_NAME,
D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, D.TOTAL_AMOUNT FROM DOC D, CUSTOMER C, CUSTOMER_BANK CB
WHERE D.STATUS = 'A' AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND C.CUSTOMER_ID = CB.CUSTOMER_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_CURRENT_LICENSE(
	LICENSE_NBR,
	RECEIVED,
	CUSTOMER_ID)
 AS 
SELECT MAX(LICENSE_NBR), RECEIVED, CL.CUSTOMER_ID FROM CUSTOMER_LICENSE CL, 
(SELECT MAX(RECEIVED) MAX_RECEIVED, CUSTOMER_ID FROM CUSTOMER_LICENSE GROUP BY CUSTOMER_ID) M 
WHERE RECEIVED = MAX_RECEIVED AND CL.CUSTOMER_ID = M.CUSTOMER_ID GROUP BY RECEIVED, CL.CUSTOMER_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_CUSTOMER(
	CUSTOMER_ID,
	CUSTOMER_NAME,
	USER_ID,
	SALES_TERM,
	CREDIT_TERM,
	CREATE_DATE,
	UPD_DATE,
	CREATE_BY,
	UPD_BY,
	CUSTOMER_ALT_NAME,
	ADDRESS_ID,
	ADDRESS_NAME,
	ADDRESS1,
	ADDRESS2,
	CITY,
	STATE,
	ZIP,
	LICENSE_NBR,
	STATUS)
 AS 
SELECT C.CUSTOMER_ID, CUSTOMER_NAME, USER_ID, SALES_TERM, CREDIT_TERM, CREATE_DATE, UPD_DATE, CREATE_BY, UPD_BY, CUSTOMER_ALT_NAME,
ADDRESS_ID, NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, LICENSE_NBR, C.STATUS
FROM CUSTOMER C, (SELECT CUSTOMER_ID, NAME, ADDRESS_ID, ADDRESS1, ADDRESS2, CITY, STATE, ZIP
FROM CUSTOMER_ADDR WHERE STATUS = 'A' AND ADDR_TYPE = 2) CA, VW_CURRENT_LICENSE CL WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND C.CUSTOMER_ID = CL.CUSTOMER_ID (+)

/

CREATE OR REPLACE FORCE VIEW CORP.VW_CUSTOMER_BANK_TERMINAL(
	CUSTOMER_BANK_TERMINAL_ID,
	CUSTOMER_BANK_ID,
	TERMINAL_ID,
	START_DATE,
	END_DATE)
 AS 
SELECT MIN(CUSTOMER_BANK_TERMINAL_ID), CUSTOMER_BANK_ID, TERMINAL_ID, MIN(START_DATE) START_DATE, MAX(END_DATE) END_DATE
FROM (SELECT CUSTOMER_BANK_TERMINAL_ID, CUSTOMER_BANK_ID, TERMINAL_ID,
             (CASE WHEN FIRST = 1 THEN START_DATE ELSE NULL END) START_DATE,
             (CASE WHEN LAST = 1 THEN END_DATE ELSE NULL END) END_DATE,
             SUM(FIRST) OVER (PARTITION BY TERMINAL_ID, CUSTOMER_BANK_ID
                 ORDER BY NVL(START_DATE, MIN_DATE) ASC
                 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) SET_NBR
      FROM (SELECT CUSTOMER_BANK_TERMINAL_ID, A.CUSTOMER_BANK_ID, A.TERMINAL_ID, START_DATE, END_DATE,
                   CASE WHEN CUSTOMER_BANK_ID <>
                        NVL(LAG(CUSTOMER_BANK_ID, 1) OVER
                        (PARTITION BY A.TERMINAL_ID ORDER BY NVL(START_DATE, MIN_DATE) ASC), 0)
                        THEN 1 ELSE 0 END FIRST,
                   CASE WHEN CUSTOMER_BANK_ID <>
                        NVL(LEAD(CUSTOMER_BANK_ID, 1) OVER
                        (PARTITION BY A.TERMINAL_ID ORDER BY NVL(START_DATE, MIN_DATE) ASC), 0)
                        THEN 1 ELSE 0 END LAST
            FROM CUSTOMER_BANK_TERMINAL A))
GROUP BY CUSTOMER_BANK_ID, TERMINAL_ID, SET_NBR

/

CREATE OR REPLACE FORCE VIEW CORP.VW_EFT_EXPORT(
	EFT_ID,
	CUSTOMER_BANK_ID,
	GROSS_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	ADJUST_AMOUNT,
	ADJUST_DESC,
	NET_AMOUNT,
	REFUND_CHARGEBACK_AMOUNT)
 AS 
SELECT DOC_ID, CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT,
           GET_ADJUST_DESC(DOC_ID),
           SUM(CASE WHEN ENTRY_TYPE IN ('CC', 'RF', 'CB', 'PF', 'SF', 'AD') THEN AMOUNT ELSE NULL END) NET_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE IN ('RF', 'CB') THEN AMOUNT ELSE NULL END) REFUND_CHARGEBACK_AMOUNT
FROM (
        SELECT D.DOC_ID, B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND L.DELETED = 'N'
          GROUP BY D.DOC_ID, B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
GROUP BY DOC_ID, CUSTOMER_BANK_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_LOCKED_PAYMENTS(
	EFT_ID,
	CUSTOMER_BANK_ID,
	BANK_ACCT_NBR,
	BANK_ROUTING_NBR,
	BATCH_REF_NBR,
	DESCRIPTION,
	GROSS_AMOUNT,
	REFUND_AMOUNT,
	CHARGEBACK_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	ADJUST_AMOUNT)
 AS 
SELECT DOC_ID, CUSTOMER_BANK_ID, BANK_ACCT_NBR, BANK_ROUTING_NBR, REF_NBR, DESCRIPTION,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT /*+ STAR */ D.DOC_ID, L.ENTRY_TYPE, D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR,
               D.REF_NBR, D.DESCRIPTION, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND L.DELETED = 'N'
          AND D.STATUS = 'L'
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          GROUP BY D.DOC_ID, L.ENTRY_TYPE, D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR,
                   D.REF_NBR, D.DESCRIPTION) M
      GROUP BY DOC_ID, CUSTOMER_BANK_ID, BANK_ACCT_NBR, BANK_ROUTING_NBR, REF_NBR, DESCRIPTION

/

CREATE OR REPLACE FORCE VIEW CORP.VW_MACGRAY_EFT_DETAIL(
	ASSET_NBR,
	LOCATION_NAME,
	TERMINAL_NBR,
	BANK_ACCT_NBR,
	GROSS_AMOUNT,
	TERMINAL_FEE,
	TRANSACTION_FEE,
	ADJUSTMENT,
	EXPLANATION,
	LAST_TRAN_DT,
	EFT_ID,
	EFT_DATE)
 AS 
select    
	   vw.asset_nbr as asset_nbr,   
	   vw.location_name as location_name,   
	   vw.terminal_nbr as terminal_nbr,   
	   cb.BANK_ACCT_NBR as bank_acct_nbr,   
	   pay.GROSS_AMOUNT as gross_amount,   
	   sf.FEE_AMOUNT as terminal_fee,    
	   pf.PROCESS_FEE_AMOUNT as transaction_fee,
	   pa.adjust_AMOUNT as adjustment,
	   pa.description as explanation,
	   last_tran_date(doc.doc_id, cbt.TERMINAL_ID) as last_tran_dt,
	   doc.REF_NBR as eft_id,
	   doc.SENT_DATE
from    
	   customer_bank_terminal cbt,   
	   customer_bank cb,   
	   (SELECT b.doc_id, b.batch_id, b.customer_bank_id, b.terminal_id, SUM(l.AMOUNT) GROSS_AMOUNT
          FROM BATCH B, LEDGER L WHERE B.batch_id = l.batch_id
          AND l.ENTRY_TYPE = 'CC'
          AND l.deleted = 'N'
          AND PAYMENTS_PKG.entry_payable(l.settle_state_id,l.entry_type) = 'Y'
        GROUP BY b.doc_id, b.batch_id, b.customer_bank_id, b.terminal_id)  pay,
	   service_fees  sf,    
	   (SELECT b.doc_id, b.batch_id, b.customer_bank_id, b.terminal_id, SUM(l.AMOUNT) PROCESS_FEE_AMOUNT
          FROM BATCH B, LEDGER L WHERE B.batch_id = l.batch_id
          AND l.ENTRY_TYPE = 'PF'
          AND l.deleted = 'N'
          AND PAYMENTS_PKG.entry_payable(l.settle_state_id,l.entry_type) = 'Y'
        GROUP BY b.doc_id, b.batch_id, b.customer_bank_id, b.terminal_id)  pf,
	   (SELECT b.doc_id, b.batch_id, b.customer_bank_id, b.terminal_id, l.AMOUNT ADJUST_AMOUNT, l.description
          FROM BATCH B, LEDGER L WHERE B.batch_id = l.batch_id
          AND l.ENTRY_TYPE = 'AD'
          AND l.deleted = 'N'
          AND PAYMENTS_PKG.entry_payable(l.settle_state_id,l.entry_type) = 'Y')  pa,
	   doc,
	   report.vw_terminal  vw
where   
	  cbt.terminal_id = vw.terminal_id and   
	  cbt.customer_bank_id = cb.customer_bank_id and   
	  /* this is currently hardcoded but should be parameterized */   
	  cb.customer_id = 57 and   
	  /***********************************************************/   
	  vw.terminal_id = pay.terminal_id and   
	  doc.doc_id = pay.doc_id and
	  pa.doc_id (+)= doc.doc_id and
	  pf.batch_id (+)= pay.batch_id and
	  sf.terminal_id = vw.terminal_id 
order by vw.asset_nbr

/

CREATE OR REPLACE FORCE VIEW CORP.VW_NO_LICENSE(
	CUSTOMER_ID,
	CUSTOMER_NAME)
 AS 
SELECT CUSTOMER_ID, CUSTOMER_NAME FROM CUSTOMER
WHERE CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM VW_CURRENT_LICENSE)

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PAID_DATE(
	LEDGER_ID,
	EFT_DATE)
 AS 
SELECT L.LEDGER_ID, (CASE WHEN D.STATUS = 'D' THEN L.LEDGER_DATE ELSE D.SENT_DATE END)
FROM LEDGER L, BATCH B, DOC D
WHERE L.BATCH_ID = B.BATCH_ID AND B.DOC_ID = D.DOC_ID (+)
AND NVL(D.STATUS, 'N') IN ('P', 'S', 'D', 'N')
AND L.ENTRY_TYPE IN('CC', 'RF', 'CB')

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PAID_EFTS(
	EFT_ID,
	EFT_DATE,
	BATCH_REF_NBR,
	CUSTOMER_BANK_ID,
	BANK_ACCT_NBR,
	BANK_ROUTING_NBR,
	EFT_AMOUNT,
	BANK_NAME,
	CUSTOMER_NAME)
 AS 
SELECT D.DOC_ID, D.SENT_DATE, D.REF_NBR, D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, D.TOTAL_AMOUNT,
CB.BANK_NAME, C.CUSTOMER_NAME
FROM DOC D, CUSTOMER_BANK CB, CUSTOMER C
WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID (+) AND CB.CUSTOMER_ID = C.CUSTOMER_ID (+) AND D.STATUS IN('P', 'S')
AND D.DOC_TYPE = 'FT'

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PAYMENT_PROCESS_FEES(
	DOC_ID,
	BATCH_ID,
	BATCH_CLOSED,
	CUSTOMER_BANK_ID,
	TERMINAL_ID,
	TRANS_TYPE_ID,
	TRANS_TYPE_NAME,
	GROSS_AMOUNT,
	FEE_PERCENT,
	FIXED_FEE_AMOUNT,
	PROCESS_FEE_AMOUNT,
	NET_AMOUNT,
	TRAN_COUNT)
 AS 
SELECT B.DOC_ID, B.BATCH_ID, B.CLOSED, B.CUSTOMER_BANK_ID, B.TERMINAL_ID, T.TRANS_TYPE_ID,
TT.TRANS_TYPE_NAME, SUM(L1.AMOUNT) GROSS_AMOUNT, PF.FEE_PERCENT, PF.FEE_AMOUNT FIXED_FEE_AMOUNT, SUM(L.AMOUNT) PROCESS_FEE_AMOUNT,
SUM(NVL(L1.AMOUNT, 0) + L.AMOUNT) NET_AMOUNT, COUNT(L.LEDGER_ID) TRAN_COUNT
FROM BATCH B, LEDGER L, LEDGER L1, PROCESS_FEES PF, TRANS T, TRANS_TYPE TT
WHERE B.BATCH_ID = L.BATCH_ID
AND L.TRANS_ID = T.TRAN_ID
AND T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
AND L.PROCESS_FEE_ID = PF.PROCESS_FEE_ID
AND L.ENTRY_TYPE = 'PF'
AND L.TRANS_ID = L1.TRANS_ID (+)
AND L.BATCH_ID = L1.BATCH_ID (+)
AND NVL(L1.ENTRY_TYPE, ' ') IN('CC', 'RF', 'CB', ' ')
AND L1.DELETED (+) = 'N'
AND PAYMENTS_PKG.ENTRY_PAYABLE(NVL(L1.SETTLE_STATE_ID, 2), NVL(L1.ENTRY_TYPE, 18)) = 'Y'
AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
AND PAYMENTS_PKG.BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE) = 'Y'
AND L.DELETED = 'N'
GROUP BY B.DOC_ID, B.BATCH_ID, B.CLOSED, B.CUSTOMER_BANK_ID, B.TERMINAL_ID, T.TRANS_TYPE_ID,
TT.TRANS_TYPE_NAME, PF.FEE_PERCENT, PF.FEE_AMOUNT

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PAYMENT_REPORT(
	PAYMENT_ID,
	EFT_ID,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	GROSS_AMOUNT,
	SETTLE_FAILED_AMOUNT,
	TAX_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	REFUND_CHARGEBACK_AMOUNT,
	ADJUST_AMOUNT,
	NET_AMOUNT,
	TERMINAL_NBR,
	LOCATION_NAME,
	TRANSACTIONS,
	PAYMENT_START_DATE,
	PAYMENT_END_DATE,
	REASON)
 AS 
SELECT V.BATCH_ID, V.DOC_ID, V.TERMINAL_ID, V.CUSTOMER_BANK_ID, V.CREDIT_AMOUNT, SETTLE_FAILED_AMOUNT, NULL, V.PROCESS_FEE_AMOUNT, V.SERVICE_FEE_AMOUNT,
CASE WHEN V.REFUND_AMOUNT IS NULL AND V.CHARGEBACK_AMOUNT IS NULL THEN NULL ELSE NVL(V.REFUND_AMOUNT, 0) + NVL(V.CHARGEBACK_AMOUNT, 0) END,
V.ADJUST_AMOUNT,
(NVL(CREDIT_AMOUNT, 0) + NVL(REFUND_AMOUNT, 0) + NVL(CHARGEBACK_AMOUNT, 0) + NVL(PROCESS_FEE_AMOUNT, 0) + NVL(SERVICE_FEE_AMOUNT, 0) + NVL(ADJUST_AMOUNT, 0)),
TERMINAL_NBR, LOCATION_NAME, (SELECT COUNT(DISTINCT X.TRANS_ID)
              FROM LEDGER X
             WHERE X.DELETED = 'N'
               AND V.BATCH_ID = X.BATCH_ID) TRAN_COUNT, V.START_DATE, V.END_DATE, PS.DESCRIPTION
FROM TERMINAL T, LOCATION L, PAYMENT_SCHEDULE PS, (
    SELECT DOC_ID, M.BATCH_ID, TERMINAL_ID, CUSTOMER_BANK_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'N' THEN AMOUNT ELSE NULL END) SETTLE_FAILED_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT D.DOC_ID, B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.PAYMENT_SCHEDULE_ID,
             CASE WHEN B.START_DATE = MIN_DATE THEN NULL ELSE B.START_DATE END START_DATE,
             CASE WHEN B.END_DATE = MAX_DATE THEN NULL ELSE B.END_DATE END END_DATE, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT,
             COUNT(*) ENTRY_COUNT, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) PAYABLE
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) IN('Y', 'N')
          AND L.DELETED = 'N'
          GROUP BY D.DOC_ID, B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.PAYMENT_SCHEDULE_ID,
             B.START_DATE, B.END_DATE, L.ENTRY_TYPE, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE)) M
      GROUP BY DOC_ID, M.BATCH_ID, TERMINAL_ID, CUSTOMER_BANK_ID, PAYMENT_SCHEDULE_ID,
             START_DATE, END_DATE, ENTRY_TYPE, PAYABLE
      ) V
WHERE V.TERMINAL_ID = T.TERMINAL_ID (+) AND T.LOCATION_ID = L.LOCATION_ID (+)
AND V.PAYMENT_SCHEDULE_ID = PS.PAYMENT_SCHEDULE_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PAYMENT_SERVICE_FEES(
	DOC_ID,
	BATCH_ID,
	LEDGER_ID,
	FEE_AMOUNT,
	FEE_NAME,
	FEE_DATE,
	TERMINAL_ID,
	CUSTOMER_BANK_ID,
	BATCH_CLOSED,
	FEE_ID,
	FREQUENCY_NAME)
 AS 
SELECT B.DOC_ID, B.BATCH_ID, L.LEDGER_ID, L.AMOUNT FEE_AMOUNT, FEE_NAME, L.LEDGER_DATE FEE_DATE,
B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.CLOSED, SF.FEE_ID, Q.NAME
FROM LEDGER L, FEES F, SERVICE_FEES SF, BATCH B, FREQUENCY Q
WHERE L.ENTRY_TYPE = 'SF' AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID AND SF.FEE_ID = F.FEE_ID
AND L.DELETED = 'N' AND L.BATCH_ID = B.BATCH_ID AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = 'Y'
--AND PAYMENTS_PKG.batch_closable(l.batch_id) = 'Y'

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PENDING_ADJUSTMENTS(
	CUSTOMER_BANK_ID,
	ADJUST_AMOUNT)
 AS 
SELECT CUSTOMER_BANK_ID, SUM(AMOUNT)
FROM VW_ADJUSTMENTS
WHERE BATCH_CLOSED = 'N'
GROUP BY CUSTOMER_BANK_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PENDING_OR_LOCKED_PAYMENTS(
	CUSTOMER_NAME,
	CUSTOMER_BANK_ID,
	BANK_ACCT_NBR,
	BANK_ROUTING_NBR,
	PAY_MIN_AMOUNT,
	GROSS_AMOUNT,
	REFUND_AMOUNT,
	CHARGEBACK_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	EFT_ID,
	STATUS,
	BATCH_REF_NBR,
	DESCRIPTION,
	NET_AMOUNT,
	ADJUST_AMOUNT)
 AS 
SELECT C.CUSTOMER_NAME, P.CUSTOMER_BANK_ID,
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, P.PAY_MIN_AMOUNT, P.GROSS_AMOUNT,
P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.SERVICE_FEE_AMOUNT,  0, '', '' , '' ,
NVL(P.GROSS_AMOUNT, 0) + NVL(P.REFUND_AMOUNT, 0) + NVL(P.CHARGEBACK_AMOUNT, 0)
 + NVL(P.PROCESS_FEE_AMOUNT, 0) + NVL(P.SERVICE_FEE_AMOUNT, 0) + NVL(P.ADJUST_AMOUNT, 0),
  P.ADJUST_AMOUNT--, REASON
FROM VW_PENDING_PAYMENTS P, CUSTOMER C 
WHERE P.CUSTOMER_ID = C.CUSTOMER_ID
UNION SELECT C.CUSTOMER_NAME, P.CUSTOMER_BANK_ID,
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, CB.PAY_MIN_AMOUNT, P.GROSS_AMOUNT,
P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.SERVICE_FEE_AMOUNT,
P.EFT_ID, 'Locked', P.BATCH_REF_NBR, P.DESCRIPTION,
NVL(P.GROSS_AMOUNT, 0) + NVL(P.REFUND_AMOUNT, 0) + NVL(P.CHARGEBACK_AMOUNT, 0) + NVL(P.PROCESS_FEE_AMOUNT, 0) + NVL(P.SERVICE_FEE_AMOUNT, 0) + NVL(P.ADJUST_AMOUNT, 0),
P.ADJUST_AMOUNT--, REASON
FROM VW_LOCKED_PAYMENTS P, CUSTOMER_BANK CB, CUSTOMER C 
WHERE P.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND CB.CUSTOMER_ID = C.CUSTOMER_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_PENDING_PAYMENTS(
	CUSTOMER_BANK_ID,
	CUSTOMER_ID,
	BANK_ACCT_NBR,
	BANK_ROUTING_NBR,
	PAY_MIN_AMOUNT,
	GROSS_AMOUNT,
	REFUND_AMOUNT,
	CHARGEBACK_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	ADJUST_AMOUNT)
 AS 
SELECT CB.CUSTOMER_BANK_ID, CB.CUSTOMER_ID, CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR, CB.PAY_MIN_AMOUNT, A.CREDIT_AMOUNT,
   A.REFUND_AMOUNT, A.CHARGEBACK_AMOUNT, A.PROCESS_FEE_AMOUNT,
   A.SERVICE_FEE_AMOUNT, A.ADJUST_AMOUNT
FROM CUSTOMER_BANK CB, VW_PENDING_REVENUE A
WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID
/*AND (CB.STATUS = 'A' OR NVL(A.CREDIT_AMOUNT, 0) <> 0 OR NVL(A.REFUND_AMOUNT, 0) <> 0
     OR NVL(A.CHARGEBACK_AMOUNT, 0) <> 0 OR NVL(A.PROCESS_FEE_AMOUNT, 0) <> 0
     OR NVL(A.SERVICE_FEE_AMOUNT, 0) <> 0 OR NVL(A.ADJUST_AMOUNT, 0) <> 0)*/

/

DROP VIEW CORP.VW_PENDING_PAYMENTS_OLD
;

DROP VIEW CORP.VW_PENDING_PROCESS_FEES
;

DROP VIEW CORP.VW_PENDING_PROCESS_FEES_OLD
;

CREATE OR REPLACE FORCE VIEW CORP.VW_PENDING_REVENUE(
	CUSTOMER_BANK_ID,
	CREDIT_AMOUNT,
	REFUND_AMOUNT,
	CHARGEBACK_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	ADJUST_AMOUNT)
 AS 
SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B
          WHERE L.BATCH_ID = B.BATCH_ID
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND L.DELETED = 'N'
          AND B.CLOSED = 'N'
          AND PAYMENTS_PKG.BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE) = 'Y'
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
      GROUP BY CUSTOMER_BANK_ID

/

DROP VIEW CORP.VW_PENDING_REVENUE_OLD
;

CREATE OR REPLACE FORCE VIEW CORP.VW_PENDING_SERVICE_FEES(
	CUSTOMER_BANK_ID,
	SERVICE_FEE_AMOUNT)
 AS 
SELECT B.CUSTOMER_BANK_ID, SUM(L.AMOUNT)
FROM LEDGER L, BATCH B
WHERE L.BATCH_ID = B.BATCH_ID
AND B.CLOSED = 'N'
AND L.DELETED = 'N'
GROUP BY B.CUSTOMER_BANK_ID

/

CREATE OR REPLACE FORCE VIEW CORP.VW_UNPAID_TRANS_AND_FEES(
	CUSTOMER_NAME,
	CUSTOMER_ID,
	CUSTOMER_BANK_ID,
	BANK_ACCT_NBR,
	BANK_ROUTING_NBR,
	PAY_MIN_AMOUNT,
	CREDIT_AMOUNT,
	REFUND_AMOUNT,
	CHARGEBACK_AMOUNT,
	PROCESS_FEE_AMOUNT,
	SERVICE_FEE_AMOUNT,
	ADJUST_AMOUNT)
 AS 
SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
    SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND L.DELETED = 'N'
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND B.DOC_ID = D.DOC_ID (+)
          AND NVL(D.SENT_DATE, MIN_DATE) < SYSDATE
          AND L.LEDGER_DATE < SYSDATE
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
      GROUP BY CUSTOMER_BANK_ID) A
WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
AND CB.CUSTOMER_ID = C.CUSTOMER_ID
ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR

/

CREATE OR REPLACE FORCE VIEW CORP.VW_UNSENT_EFTS(
	EFT_ID,
	TRANSACTION_CODE,
	DFI,
	CHECK_DIGIT,
	BANK_ACCT_NBR,
	AMOUNT,
	TRACE_NUMBER,
	DESCRIPTION,
	COMPANY_ID_NBR,
	COMPANY_NAME)
 AS 
SELECT D.DOC_ID,
(CASE WHEN D.TOTAL_AMOUNT > 0 AND CB.ACCOUNT_TYPE = 'C' THEN 22 WHEN D.TOTAL_AMOUNT < 0 AND CB.ACCOUNT_TYPE = 'C' THEN 27
WHEN D.TOTAL_AMOUNT > 0 AND CB.ACCOUNT_TYPE = 'S' THEN 32 WHEN D.TOTAL_AMOUNT < 0 AND CB.ACCOUNT_TYPE = 'S' THEN 37 END)
/*22=DEPOSIT TO CHECKING, 27=WITHDRAWL FROM CHECKING, 32=DEPOSIT TO SAVINGS, 37=WITHDRAWL FROM SAVINGS*/,      
SUBSTR(TO_CHAR(D.BANK_ROUTING_NBR, 'FM000000000'),1,8),
SUBSTR(TO_CHAR(D.BANK_ROUTING_NBR, 'FM000000000'),9,1),
D.BANK_ACCT_NBR, ABS(D.TOTAL_AMOUNT),
'03100005'/*OUR ROUTING NUMBER*/ || TO_CHAR(D.DOC_ID, 'FM000000'),
D.DESCRIPTION, D.CUSTOMER_BANK_ID, C.CUSTOMER_NAME
FROM DOC D, CUSTOMER_BANK CB, CUSTOMER C			
WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND D.STATUS = 'A' AND CB.CUSTOMER_ID = C.CUSTOMER_ID
AND D.TOTAL_AMOUNT <> 0 AND CB.ACCOUNT_TYPE IN('S','C')

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRAIUD_TERMINAL_EPORT
 AFTER 
 INSERT OR DELETE OR UPDATE
 ON TERMINAL_EPORT
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    DECLARE
        l_list ID_LIST;
    BEGIN
        IF INSERTING OR UPDATING THEN
            SELECT TRAN_ID BULK COLLECT INTO l_list
              FROM TRANS T
             WHERE T.EPORT_ID = :NEW.EPORT_ID
               AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE)
               AND (T.TRANS_TYPE_ID NOT IN(20,21) OR T.ORIG_TRAN_ID IS NULL);
             SYNC_PKG.SYNC_TRANS_TERMINAL(l_list);
        END IF;
        IF (UPDATING OR DELETING) AND :OLD.START_DATE < :OLD.END_DATE THEN
            SELECT TRAN_ID BULK COLLECT INTO l_list
              FROM TRANS T
             WHERE T.EPORT_ID = :OLD.EPORT_ID
                AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE)
                AND (T.TRANS_TYPE_ID NOT IN(20,21) OR T.ORIG_TRAN_ID IS NULL);
             SYNC_PKG.SYNC_TRANS_TERMINAL(l_list);
        END IF;
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRAIUD_TERMINAL_REGION
 AFTER 
 INSERT OR DELETE OR UPDATE
 ON TERMINAL_REGION
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    IF INSERTING OR UPDATING THEN
        SYNC_PKG.SYNC_TERMINAL_INFO(:NEW.TERMINAL_ID);
    END IF;
    IF DELETING OR (UPDATING AND :NEW.TERMINAL_ID <> :OLD.TERMINAL_ID) THEN
        SYNC_PKG.SYNC_TERMINAL_INFO(:OLD.TERMINAL_ID);
    END IF;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRAI_FILL
 AFTER 
 INSERT
 ON FILL
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    SYNC_PKG.SYNC_FILL_INSERT(:NEW.FILL_ID);
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRAIU_TRANS
 AFTER 
 INSERT OR UPDATE OF TRAN_ID, TOTAL_AMOUNT, CLOSE_DATE, EPORT_ID, TRANS_TYPE_ID, ORIG_TRAN_ID
 ON TRANS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    SYNC_PKG.SYNC_TRANS_INSERT(:NEW.TRAN_ID);
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER REPORT.TRAU_REGION
 AFTER 
 UPDATE
 ON REGION
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    SYNC_PKG.SYNC_REGION_NAME(:NEW.REGION_ID, :NEW.REGION_NAME);
END;

/

CREATE OR REPLACE TRIGGER TRAU_TRANS_SETTLE
 AFTER 
 UPDATE OF CC_APPR_CODE, SETTLE_STATE_ID, SETTLE_DATE
 ON TRANS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    SYNC_PKG.SYNC_TRANS_SETTLEMENT(:NEW.tran_id,:NEW.settle_state_id,:NEW.settle_date,:NEW.cc_appr_code);
END;
/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER trau_trans_status
 AFTER
   UPDATE OF status
 ON trans
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.STATUS = 'A' AND :OLD.STATUS = 'P' THEN
        SYNC_PKG.SYNC_TRANS_INSERT(:NEW.TRAN_ID);
    END IF;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRBIU_TRANS
 BEFORE 
 INSERT OR UPDATE
 ON TRANS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
   IF :NEW.CARDTYPE_AUTHORITY_ID IS NOT NULL
        AND ((INSERTING AND :NEW.MERCHANT_ID IS NULL)
            OR (UPDATING AND :NEW.CARDTYPE_AUTHORITY_ID <> NVL(:OLD.CARDTYPE_AUTHORITY_ID, 0))) THEN
        BEGIN
            SELECT M.MERCHANT_ID
              INTO :NEW.MERCHANT_ID
             FROM MERCHANT M, CARDTYPE_AUTHORITY CA, AUTHORITY A
    	    WHERE :NEW.CARDTYPE_AUTHORITY_ID = CA.CARDTYPE_AUTHORITY_ID
              AND CA.AUTHORITY_ID = A.AUTHORITY_ID
              AND M.MERCHANT_NBR  = A.AUTH_MERCHANT_ID||A.AUTH_TERMINAL_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    END IF;
    
    IF :NEW.refresh_ind = 'N' THEN
        :NEW.refresh_ind := NULL;
    ELSE
        :NEW.refresh_ind := 'Y';
    END IF;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRBI_MSG_LOG
 BEFORE 
 INSERT
 ON MSG_LOG
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    IF :NEW.MSG_ID IS NULL THEN
        SELECT MSG_LOG_SEQ.NEXTVAL
          INTO :NEW.MSG_ID
          FROM DUAL;
    END IF;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER trbi_terminal_eport
 BEFORE
  INSERT
 ON terminal_eport
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT TERMINAL_EPORT_SEQ.NEXTVAL
    INTO :NEW.TERMINAL_EPORT_ID
    FROM DUAL;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER T_RESPONSE_SENT_TRIP_ALERT
 AFTER 
 INSERT
 ON RESPONSE_SENT
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    TRIP_ALERT(:NEW.TERMINAL_ID, :NEW.ALERT_ID, :NEW.RESPONSE_ID);
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER T_TERMINAL_ALERT
 AFTER 
 INSERT
 ON TERMINAL_ALERT
 REFERENCING OLD AS OLD NEW AS NEW
BEGIN
    REPORT.RESET_DATE_INTERVAL_ALERTS();
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE TRIGGER TRAIUD_CUSTOMER_BANK_TERMINAL
 AFTER 
 INSERT OR DELETE OR UPDATE
 ON CUSTOMER_BANK_TERMINAL
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    DECLARE
        l_list REPORT.ID_LIST;
    BEGIN
        IF INSERTING OR UPDATING THEN
            SELECT TRAN_ID BULK COLLECT INTO l_list
              FROM TRANS T
             WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
               AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE)
               AND (T.TRANS_TYPE_ID NOT IN(20,21) OR T.ORIG_TRAN_ID IS NULL);
             REPORT.SYNC_PKG.SYNC_TRANS_CUST_BANK(l_list);
        END IF;
        IF (UPDATING OR DELETING) AND :OLD.START_DATE < :OLD.END_DATE THEN
            SELECT TRAN_ID BULK COLLECT INTO l_list
              FROM TRANS T
             WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
               AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE)
               AND (T.TRANS_TYPE_ID NOT IN(20,21) OR T.ORIG_TRAN_ID IS NULL);
             REPORT.SYNC_PKG.SYNC_TRANS_CUST_BANK(l_list);
        END IF;
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE TRIGGER TRAIUD_PROCESS_FEES
 AFTER 
 INSERT OR DELETE OR UPDATE
 ON PROCESS_FEES
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    DECLARE
        l_list REPORT.ID_LIST;
        l_type CHAR(1);
    BEGIN
        IF UPDATING THEN
            IF :OLD.TERMINAL_ID <> :NEW.TERMINAL_ID
                OR :OLD.TRANS_TYPE_ID <> :NEW.TRANS_TYPE_ID
                OR NVL(:OLD.START_DATE, MIN_DATE) <> NVL(:NEW.START_DATE, MIN_DATE)
                OR NVL(:OLD.END_DATE, MAX_DATE) <> NVL(:NEW.END_DATE, MAX_DATE) THEN
                SELECT TRAN_ID BULK COLLECT INTO l_list
                  FROM TRANS T
                 WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
                   AND T.TRANS_TYPE_ID = :NEW.TRANS_TYPE_ID
                   AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                   AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
                 REPORT.SYNC_PKG.SYNC_TRANS_PROCESS_FEE(l_list);
                IF :OLD.START_DATE < :OLD.END_DATE THEN
                    SELECT TRAN_ID BULK COLLECT INTO l_list
                      FROM TRANS T
                     WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
                       AND T.TRANS_TYPE_ID = :OLD.TRANS_TYPE_ID
                       AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                       AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE);
                     REPORT.SYNC_PKG.SYNC_TRANS_PROCESS_FEE(l_list);
                END IF;
            END IF;
            IF :OLD.FEE_PERCENT <> :NEW.FEE_PERCENT OR :OLD.FEE_AMOUNT <> :NEW.FEE_AMOUNT THEN
                -- only update fee_amount and percent
                REPORT.SYNC_PKG.SYNC_PROCESS_FEE_VALUES(:NEW.PROCESS_FEE_ID);
            END IF;
        ELSIF INSERTING THEN
            SELECT TRAN_ID BULK COLLECT INTO l_list
              FROM TRANS T
             WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
               AND T.TRANS_TYPE_ID = :NEW.TRANS_TYPE_ID
               AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
             REPORT.SYNC_PKG.SYNC_TRANS_PROCESS_FEE(l_list);
        ELSIF DELETING AND :OLD.START_DATE < :OLD.END_DATE THEN
            SELECT TRAN_ID BULK COLLECT INTO l_list
              FROM TRANS T
             WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
               AND T.TRANS_TYPE_ID = :OLD.TRANS_TYPE_ID
               AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE);
             REPORT.SYNC_PKG.SYNC_TRANS_PROCESS_FEE(l_list);
        END IF;
    END;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE TRIGGER trbiu_batch
 BEFORE
  INSERT OR UPDATE
 ON batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.DOC_ID IS NOT NULL THEN
        :NEW.CLOSED := 'Y'; -- Closed
        IF :NEW.END_DATE IS NULL THEN
            :NEW.END_DATE := SYSDATE;
        END IF;
    ELSE
        :NEW.CLOSED := 'N'; -- Open
    END IF;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = CORP;
CREATE OR REPLACE TRIGGER TRBI_CUSTOMER_BANK_TERMINAL
 BEFORE 
 INSERT
 ON CUSTOMER_BANK_TERMINAL
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    IF :NEW.CUSTOMER_BANK_TERMINAL_ID IS NULL THEN
        SELECT CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL
          INTO :NEW.CUSTOMER_BANK_TERMINAL_ID
          FROM DUAL;
    END IF;
END;

/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRAU_TERMINAL
 AFTER 
 UPDATE OF LOCATION_ID, TERMINAL_NBR, TERMINAL_NAME
 ON TERMINAL
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    SYNC_PKG.SYNC_TERMINAL_INFO(:NEW.TERMINAL_ID);
END;
/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
CREATE OR REPLACE TRIGGER TRAU_LOCATION
 AFTER 
 UPDATE OF LOCATION_NAME
 ON LOCATION
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
BEGIN
    DECLARE
        l_list ID_LIST;
    BEGIN
        SELECT TERMINAL_ID BULK COLLECT INTO l_list
          FROM TERMINAL
         WHERE LOCATION_ID = :NEW.LOCATION_ID;

        SYNC_PKG.SYNC_TERMINAL_INFO(l_list);
    END;
END;
/

DROP PACKAGE BODY REPORT.GLOBALS_PKG;

DROP PACKAGE REPORT.GLOBALS_PKG;

CREATE PUBLIC SYNONYM GLOBALS_PKG
	FOR CORP.GLOBALS_PKG
;

-- New Index
CREATE INDEX REPORT.IX_TRANS_COMBO ON REPORT.TRANS(
	EPORT_ID,
	CLOSE_DATE,
	TRANS_TYPE_ID,
	TRAN_ID);

-- New Index
CREATE INDEX REPORT.IX_TRANS_TERM_EPORT_STATUS_ORI ON REPORT.TRANS(
	ORIG_TRAN_ID,
	TERMINAL_ID,
	STATUS,
	EPORT_ID,
	CLOSE_DATE);

-- New Index
CREATE INDEX REPORT.UIX_TERMINAL_EPORT_COMBO ON REPORT.TERMINAL_EPORT(
	EPORT_ID,
	TERMINAL_ID,
	START_DATE,
	END_DATE);

-- New Index
CREATE UNIQUE INDEX REPORT.UIX_TERMINAL_PAY_SCHED_ID ON REPORT.TERMINAL(
	PAYMENT_SCHEDULE_ID,
	TERMINAL_ID);
