alter table corp.batch drop (
	BATCH_CLOSABLE,
	TOTAL_LEDGER_AMOUNT,
	TOTAL_LEDGER_COUNT,
	UNPROCESSED_LEDGER_AMOUNT,
	UNPROCESSED_LEDGER_COUNT
);

drop table corp.batch_total purge;
