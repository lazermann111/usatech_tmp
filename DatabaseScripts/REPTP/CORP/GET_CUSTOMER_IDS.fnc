CREATE OR REPLACE FUNCTION CORP.GET_CUSTOMER_IDS (
	pn_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE
) RETURN NUMBER_TABLE IS
	l_customer_ids NUMBER_TABLE;
BEGIN
	select customer_id 
  bulk collect into l_customer_ids
  from (
		select pn_customer_id as customer_id from dual
		union all
		select c.customer_id
		from corp.customer c
		where c.parent_customer_id = pn_customer_id
	);
	return l_customer_ids;
END;
/
