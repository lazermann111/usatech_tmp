CREATE OR REPLACE PACKAGE CORP.GLOBALS_PKG 
  IS
--
-- Holds any global constants or types
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B Krug       09-16-04 NEW
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;

    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST;
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST;

    FUNCTION LIST_TO_STRING(
        l_list IN REPORT.STRING_LIST,
        l_sep  IN VARCHAR)
     RETURN VARCHAR;
     
    FUNCTION REQUEST_LOCK(
        l_object_type VARCHAR2,
        l_object_id   NUMBER)
     RETURN VARCHAR2;
     
    PROCEDURE RELEASE_LOCK(
        l_handle VARCHAR2);
        
    FUNCTION GET_APP_SETTING(
      pv_app_setting_cd CORP.APP_SETTING.APP_SETTING_CD%TYPE
    ) RETURN CORP.APP_SETTING.APP_SETTING_VALUE%TYPE;
  
    PROCEDURE LOCK_PROCESS(
        pv_process_cd CORP.APP_SETTING.APP_SETTING_CD%TYPE,
        pv_process_token_cd CORP.APP_SETTING.APP_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_token_cd OUT CORP.APP_SETTING.APP_SETTING_VALUE%TYPE
    );
    
    PROCEDURE UNLOCK_PROCESS(
        pv_process_cd CORP.APP_SETTING.APP_SETTING_CD%TYPE,
        pv_process_token_cd CORP.APP_SETTING.APP_SETTING_VALUE%TYPE
    );
END; -- Package spec
/
