CREATE OR REPLACE FUNCTION CORP.CARD_COMPANY (CARD_NUMBER VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    ch CHAR(1);
    cn VARCHAR2(50);
  BEGIN
    cn := TRIM(card_number);
    ch := SUBSTR(cn,1,1);
    IF ch = '4' AND LENGTH(cn) = 16 THEN
      RETURN 'VISA';
    ELSIF ch = '5' AND LENGTH(cn) = 16 THEN
      RETURN 'MasterCard';
    ELSIF ch = '3' AND LENGTH(cn) = 15 THEN
      RETURN 'American Express';
    ELSIF ch = '6' AND LENGTH(cn) = 16 THEN
      RETURN 'Discover';
    ELSE
      RETURN 'Unknown';
    END IF;
  END;
END;
/