-- Start of DDL Script for Sequence CORP.BATCH_SEQ
-- Generated 10/5/2004 3:15:32 PM from CORP@USADBD02

-- Drop the old instance of BATCH_SEQ
DROP SEQUENCE batch_seq
/

CREATE SEQUENCE batch_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/

-- Grants for Sequence
GRANT ALTER ON batch_seq TO report
WITH GRANT OPTION
/
GRANT SELECT ON batch_seq TO report
WITH GRANT OPTION
/

-- End of DDL Script for Sequence CORP.BATCH_SEQ

