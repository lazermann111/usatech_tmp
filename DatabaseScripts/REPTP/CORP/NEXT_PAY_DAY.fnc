CREATE OR REPLACE FUNCTION CORP.NEXT_PAY_DAY(
    pd_date DATE,
    pn_pay_cycle_id NUMBER)
    RETURN DATE
    DETERMINISTIC
    PARALLEL_ENABLE
IS
    ld_tmp DATE;
BEGIN
    IF pn_pay_cycle_id = 1 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'MONDAY');
    ELSIF pn_pay_cycle_id = 2 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'THURSDAY');
    ELSIF pn_pay_cycle_id = 3 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'FRIDAY');
    ELSIF pn_pay_cycle_id = 4 THEN
        ld_tmp := NEXT_DAY(TRUNC(pd_date, 'MONTH'), 'FRIDAY');
        IF ld_tmp > TRUNC(pd_date) + 1 THEN
            RETURN ld_tmp;
        ELSE
            RETURN NEXT_DAY(ADD_MONTHS(TRUNC(pd_date, 'MONTH'), 1), 'FRIDAY');
        END IF;
    ELSIF pn_pay_cycle_id = 5 THEN
        IF EXTRACT(DAY FROM pd_date) >= 15 THEN
            RETURN ADD_MONTHS(TRUNC(pd_date, 'MONTH'), 1) + 14;
        ELSE
            RETURN TRUNC(pd_date, 'MONTH') + 14;
        END IF;
    ELSIF pn_pay_cycle_id = 6 THEN
        IF EXTRACT(DAY FROM pd_date) >= 3 THEN
            RETURN ADD_MONTHS(TRUNC(pd_date, 'MONTH'), 1) + 2;
        ELSE
            RETURN TRUNC(pd_date, 'MONTH') + 2;
        END IF;
    ELSIF pn_pay_cycle_id = 7 THEN
        RETURN LEAST(NEXT_DAY(TRUNC(pd_date), 'WEDNESDAY'), NEXT_DAY(TRUNC(pd_date), 'FRIDAY'));
    ELSIF pn_pay_cycle_id = 8 THEN
        RETURN TRUNC(pd_date) + 1;
    ELSIF pn_pay_cycle_id = 9 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'TUESDAY');
    ELSIF pn_pay_cycle_id = 10 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'WEDNESDAY');
    ELSE
        RETURN NULL;
    END IF;
END;
/