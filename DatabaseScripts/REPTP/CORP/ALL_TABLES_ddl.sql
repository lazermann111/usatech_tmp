-- Start of DDL Script for Table CORP.ADDRESS_TYPE
-- Generated 13-Oct-2004 13:39:06 from CORP@USADBD02
/*
CREATE TABLE address_type
    (addr_type                      VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255),
    addr_type_id                   NUMBER NOT NULL
  ,
  PRIMARY KEY (addr_type_id)
  USING INDEX)
/
*/



-- Indexes for ADDRESS_TYPE
/*
CREATE UNIQUE INDEX xpkaddress_type ON address_type
  (
    addr_type_id                    ASC
  )
/
*/


-- End of DDL Script for Table CORP.ADDRESS_TYPE

-- Start of DDL Script for Table CORP.BATCH
-- Generated 13-Oct-2004 13:39:08 from CORP@USADBD02

CREATE TABLE batch
    (batch_id                       NUMBER(*,0) NOT NULL,
    customer_bank_id               NUMBER(*,0) NOT NULL,
    doc_id                         NUMBER(*,0),
    terminal_id                    NUMBER(*,0),
    payment_schedule_id            NUMBER(*,0) NOT NULL,
    start_date                     DATE NOT NULL,
    end_date                       DATE,
    create_date                    DATE DEFAULT SYSDATE

  NOT NULL,
    closed                         CHAR(1) NOT NULL
  ,
  PRIMARY KEY (batch_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON batch TO report
WITH GRANT OPTION
/
GRANT INSERT ON batch TO report
WITH GRANT OPTION
/
GRANT SELECT ON batch TO report
WITH GRANT OPTION
/
GRANT UPDATE ON batch TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON batch TO report
WITH GRANT OPTION
/



-- Indexes for BATCH
/*
CREATE INDEX ix_batch_doc_id ON batch
  (
    doc_id                          ASC
  )
/*/
/*
CREATE INDEX ix_batch_terminal_id ON batch
  (
    terminal_id                     ASC
  )
/
CREATE INDEX ix_batch_payment_schedule_id ON batch
  (
    payment_schedule_id             ASC
  )
/

CREATE INDEX ix_batch_customer_bank_id ON batch
  (
    customer_bank_id                ASC
  )
/

CREATE BITMAP INDEX bix_batch_closed ON batch
  (
    closed                          ASC
  )
/

CREATE INDEX ix_batch_start_end_dates ON batch
  (
    start_date                      ASC,
    end_date                        ASC
  )
COMPRESS  2
/

CREATE UNIQUE INDEX uix_batch_summary ON batch
  (
    closed                          ASC,
    customer_bank_id                ASC,
    batch_id                        ASC
  )
COMPRESS  2
/

CREATE UNIQUE INDEX uix_batch_batch_doc_ids ON batch
  (
    doc_id                          ASC,
    batch_id                        ASC
  )
COMPRESS  1
/



-- Constraints for BATCH


*/




-- Triggers for BATCH

CREATE OR REPLACE TRIGGER trbiu_batch
 BEFORE
  INSERT OR UPDATE
 ON batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.DOC_ID IS NOT NULL THEN
        :NEW.CLOSED := 'Y'; -- Closed
        IF :NEW.END_DATE IS NULL THEN
            :NEW.END_DATE := SYSDATE;
        END IF;
    ELSE
        :NEW.CLOSED := 'N'; -- Open
    END IF;
END;
/


-- Comments for BATCH

COMMENT ON COLUMN batch.closed IS 'Open (O) or Closed (C)'
/
COMMENT ON COLUMN batch.terminal_id IS 'Allow null for non-terminal specific adjustments'
/

-- End of DDL Script for Table CORP.BATCH

-- Start of DDL Script for Table CORP.CATEGORIES
-- Generated 13-Oct-2004 13:39:12 from CORP@USADBD02

CREATE TABLE categories
    (category_id                    NUMBER NOT NULL,
    name                           VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255),
    has_subcategories              NUMBER,
    upd_by                         VARCHAR2(50),
    upd_date                       DATE DEFAULT SYSDATE
 
  ,
  PRIMARY KEY (category_id)
  USING INDEX)
/




-- Indexes for CATEGORIES
/*
CREATE UNIQUE INDEX xpkcategories ON categories
  (
    category_id                     ASC
  )
/
*/


-- Constraints for CATEGORIES



-- End of DDL Script for Table CORP.CATEGORIES

-- Start of DDL Script for Table CORP.CHARGEBACK
-- Generated 13-Oct-2004 13:39:14 from CORP@USADBD02

CREATE TABLE chargeback
    (chargeback_id                  NUMBER NOT NULL,
    ledger_id                      NUMBER,
    cc_appr_code                   VARCHAR2(50) NOT NULL,
    card_number                    VARCHAR2(20),
    tran_date                      DATE NOT NULL,
    problem_date                   DATE,
    location_name                  VARCHAR2(50),
    reason_code_id                 NUMBER,
    name                           VARCHAR2(50),
    phone                          VARCHAR2(20),
    description                    VARCHAR2(255),
    charge_status                  VARCHAR2(20) NOT NULL,
    create_date                    DATE DEFAULT SYSDATE,
    upd_date                       DATE DEFAULT SYSDATE,
    upd_by                         VARCHAR2(50)
  ,
  PRIMARY KEY (chargeback_id)
  USING INDEX)
/




-- Indexes for CHARGEBACK

CREATE INDEX xif56chargeback ON chargeback
  (
    ledger_id                       ASC
  )
/

CREATE INDEX xif62chargeback ON chargeback
  (
    reason_code_id                  ASC
  )
/
/*
CREATE UNIQUE INDEX xpkchargeback ON chargeback
  (
    chargeback_id                   ASC
  )
/
*/


-- Constraints for CHARGEBACK



-- End of DDL Script for Table CORP.CHARGEBACK

-- Start of DDL Script for Table CORP.CONTACT
-- Generated 13-Oct-2004 13:39:17 from CORP@USADBD02

CREATE TABLE contact
    (contact_type                   VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255)
  ,
  PRIMARY KEY (contact_type)
  USING INDEX)
/




-- Indexes for CONTACT
/*
CREATE UNIQUE INDEX xpkcontact_type ON contact
  (
    contact_type                    ASC
  )
/
*/


-- End of DDL Script for Table CORP.CONTACT

-- Start of DDL Script for Table CORP.CUSTOMER
-- Generated 13-Oct-2004 13:39:19 from CORP@USADBD02

CREATE TABLE customer
    (customer_id                    NUMBER NOT NULL,
    customer_name                  VARCHAR2(50) NOT NULL,
    user_id                        NUMBER,
    sales_term                     VARCHAR2(255),
    credit_term                    VARCHAR2(25),
    create_date                    DATE DEFAULT SYSDATE,
    upd_date                       DATE DEFAULT NULL,
    upd_by                         VARCHAR2(50),
    status                         CHAR(1) DEFAULT 'P',
    customer_alt_name              VARCHAR2(50),
    dealer_id                      NUMBER,
    tax_id_nbr                     VARCHAR2(12),
    create_by                      NUMBER NOT NULL
  ,
  PRIMARY KEY (customer_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON customer TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER

CREATE INDEX ix_customer_status ON customer
  (
    status                          ASC
  )
/

CREATE INDEX xif61customer ON customer
  (
    user_id                         ASC
  )
/
/*
CREATE UNIQUE INDEX xpkcustomer ON customer
  (
    customer_id                     ASC
  )
/
*/


-- End of DDL Script for Table CORP.CUSTOMER

-- Start of DDL Script for Table CORP.CUSTOMER_ADDR
-- Generated 13-Oct-2004 13:39:21 from CORP@USADBD02

CREATE TABLE customer_addr
    (address_id                     NUMBER NOT NULL,
    customer_id                    NUMBER NOT NULL,
    addr_type                      NUMBER NOT NULL,
    local_tax                      NUMBER(15,2),
    state_tax                      NUMBER(15,2),
    name                           VARCHAR2(50),
    attn_to                        VARCHAR2(50),
    address1                       VARCHAR2(255),
    address2                       VARCHAR2(255),
    city                           VARCHAR2(50),
    state                          VARCHAR2(50),
    zip                            VARCHAR2(20),
    description                    VARCHAR2(255),
    status                         CHAR(1) DEFAULT 'A'
 
  ,
  PRIMARY KEY (address_id)
  USING INDEX)
/




-- Indexes for CUSTOMER_ADDR

CREATE INDEX xif17customer_addr ON customer_addr
  (
    address_id                      ASC,
    customer_id                     ASC
  )
/

CREATE INDEX xif53customer_addr ON customer_addr
  (
    customer_id                     ASC
  )
/

CREATE INDEX xif71customer_addr ON customer_addr
  (
    addr_type                       ASC
  )
/



-- Constraints for CUSTOMER_ADDR




-- End of DDL Script for Table CORP.CUSTOMER_ADDR

-- Start of DDL Script for Table CORP.CUSTOMER_BANK
-- Generated 13-Oct-2004 13:39:24 from CORP@USADBD02

CREATE TABLE customer_bank
    (customer_id                    NUMBER NOT NULL,
    bank_acct_nbr                  VARCHAR2(20) NOT NULL,
    bank_routing_nbr               VARCHAR2(20) NOT NULL,
    pay_min_amount                 NUMBER DEFAULT 25  NOT NULL,
    schedule_id                    NUMBER,
    account_title                  VARCHAR2(50),
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    upd_date                       DATE DEFAULT SYSDATE  NOT NULL,
    upd_by                         NUMBER,
    status                         CHAR(1) DEFAULT 'P'  NOT NULL,
    customer_bank_id               NUMBER NOT NULL,
    description                    VARCHAR2(255),
    is_eft                         CHAR(1) DEFAULT 'Y'  NOT NULL,
    create_by                      NUMBER NOT NULL,
    eft_prefix                     VARCHAR2(25),
    account_type                   CHAR(1),
    bank_name                      VARCHAR2(50),
    bank_address                   VARCHAR2(255),
    bank_city                      VARCHAR2(50),
    bank_state                     VARCHAR2(10),
    bank_zip                       VARCHAR2(20),
    contact_name                   VARCHAR2(50),
    contact_title                  VARCHAR2(50),
    contact_telephone              VARCHAR2(20),
    contact_fax                    VARCHAR2(20)
  ,
  PRIMARY KEY (customer_bank_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON customer_bank TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_bank TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_bank TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_bank TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_bank TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_BANK

CREATE INDEX ix_customer_bank_status ON customer_bank
  (
    status                          ASC
  )
/

CREATE INDEX ix_cust_bank_customer_id ON customer_bank
  (
    customer_id                     ASC
  )
/
/*
CREATE UNIQUE INDEX pk_customer_bank ON customer_bank
  (
    customer_bank_id                ASC
  )
/
*/


-- Constraints for CUSTOMER_BANK




-- End of DDL Script for Table CORP.CUSTOMER_BANK

-- Start of DDL Script for Table CORP.CUSTOMER_BANK_TERMINAL
-- Generated 13-Oct-2004 13:39:27 from CORP@USADBD02

CREATE TABLE customer_bank_terminal
    (terminal_id                    NUMBER NOT NULL,
    customer_bank_id               NUMBER NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    customer_bank_terminal_id      NUMBER NOT NULL
  ,
  CONSTRAINT PK_CUSTOMER_BANK_TERMINAL
  PRIMARY KEY (customer_bank_terminal_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_bank_terminal TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_BANK_TERMINAL

CREATE UNIQUE INDEX uix_cbt_combo ON customer_bank_terminal
  (
    customer_bank_id                ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC
  )
/

CREATE INDEX ix_cbt_customer_bank_id ON customer_bank_terminal
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_cbt_end_date ON customer_bank_terminal
  (
    end_date                        ASC
  )
/

CREATE INDEX ix_cbt_start_date ON customer_bank_terminal
  (
    start_date                      ASC
  )
/

CREATE INDEX ix_cbt_terminal_id ON customer_bank_terminal
  (
    terminal_id                     ASC
  )
/



-- Constraints for CUSTOMER_BANK_TERMINAL



-- Triggers for CUSTOMER_BANK_TERMINAL

CREATE OR REPLACE TRIGGER traiud_customer_bank_terminal
 AFTER
  INSERT OR DELETE OR UPDATE
 ON customer_bank_terminal
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
            OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE)) -- this should always be null, but to be safe...
             WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
               AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE);
        END IF;
    END IF;
END;
/


-- End of DDL Script for Table CORP.CUSTOMER_BANK_TERMINAL

-- Start of DDL Script for Table CORP.CUSTOMER_LICENSE
-- Generated 13-Oct-2004 13:39:31 from CORP@USADBD02

CREATE TABLE customer_license
    (license_nbr                    VARCHAR2(15) NOT NULL,
    customer_id                    NUMBER NOT NULL,
    received                       DATE DEFAULT SYSDATE  NOT NULL,
    receive_by                     NUMBER NOT NULL)
/

-- Grants for Table
GRANT DELETE ON customer_license TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_license TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_license TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_license TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_license TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_LICENSE

CREATE INDEX ix_cl_customer_id ON customer_license
  (
    customer_id                     ASC
  )
/

CREATE INDEX ix_cl_license_nbr ON customer_license
  (
    license_nbr                     ASC
  )
/

CREATE INDEX ix_cl_received ON customer_license
  (
    received                        ASC
  )
/



-- End of DDL Script for Table CORP.CUSTOMER_LICENSE

-- Start of DDL Script for Table CORP.DEALER
-- Generated 13-Oct-2004 13:39:33 from CORP@USADBD02

CREATE TABLE dealer
    (dealer_id                      NUMBER NOT NULL,
    dealer_name                    VARCHAR2(50) NOT NULL,
    address_id                     NUMBER,
    status                         CHAR(1) DEFAULT 'A'  NOT NULL
  ,
  PRIMARY KEY (dealer_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON dealer TO report
WITH GRANT OPTION
/
GRANT INSERT ON dealer TO report
WITH GRANT OPTION
/
GRANT SELECT ON dealer TO report
WITH GRANT OPTION
/
GRANT UPDATE ON dealer TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON dealer TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.DEALER

-- Start of DDL Script for Table CORP.DEALER_EPORT
-- Generated 13-Oct-2004 13:39:35 from CORP@USADBD02

CREATE TABLE dealer_eport
    (dealer_id                      NUMBER NOT NULL,
    eport_id                       NUMBER NOT NULL,
    activate_date                  DATE,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  PRIMARY KEY (dealer_id, eport_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT INSERT ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT SELECT ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT UPDATE ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON dealer_eport TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.DEALER_EPORT

-- Start of DDL Script for Table CORP.DEALER_LICENSE
-- Generated 13-Oct-2004 13:39:37 from CORP@USADBD02

CREATE TABLE dealer_license
    (license_id                     NUMBER NOT NULL,
    dealer_id                      NUMBER NOT NULL,
    start_date                     DATE,
    end_date                       DATE)
/




-- Indexes for DEALER_LICENSE

CREATE INDEX ix_dl_dealer_id ON dealer_license
  (
    dealer_id                       ASC
  )
/

CREATE INDEX ix_dl_license_id ON dealer_license
  (
    license_id                      ASC
  )
/



-- Constraints for DEALER_LICENSE



-- End of DDL Script for Table CORP.DEALER_LICENSE

-- Start of DDL Script for Table CORP.DOC
-- Generated 13-Oct-2004 13:39:40 from CORP@USADBD02

CREATE TABLE doc
    (doc_id                         NUMBER(*,0) NOT NULL,
    doc_type                       CHAR(2) NOT NULL,
    ref_nbr                        VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255) NOT NULL,
    customer_bank_id               NUMBER(*,0) NOT NULL,
    total_amount                   NUMBER(15,2),
    approve_by                     NUMBER(*,0),
    sent_by                        NUMBER(*,0),
    sent_date                      DATE,
    bank_acct_nbr                  VARCHAR2(20),
    bank_routing_nbr               VARCHAR2(20),
    status                         CHAR(1) DEFAULT 'L'   NOT NULL,
    create_by                      NUMBER(*,0),
    create_date                    DATE DEFAULT SYSDATE

  NOT NULL,
    update_date                    DATE
  ,
  PRIMARY KEY (doc_id)
  USING INDEX)
/




-- Indexes for DOC

CREATE UNIQUE INDEX uix_doc_ref_nbr ON doc
  (
    ref_nbr                         ASC
  )
/

CREATE INDEX doc_customer_bank_id ON doc
  (
    customer_bank_id                ASC
  )
/

CREATE BITMAP INDEX bix_doc_status ON doc
  (
    status                          ASC
  )
/



-- Constraints for DOC






-- Comments for DOC

COMMENT ON COLUMN doc.status IS 'Locked (L),'
/

-- End of DDL Script for Table CORP.DOC

-- Start of DDL Script for Table CORP.EFT
-- Generated 13-Oct-2004 13:39:42 from CORP@USADBD02

CREATE TABLE eft
    (eft_id                         NUMBER NOT NULL,
    eft_date                       DATE,
    batch_ref_nbr                  VARCHAR2(50),
    description                    VARCHAR2(255),
    customer_bank_id               NUMBER,
    bank_acct_nbr                  VARCHAR2(20) NOT NULL,
    bank_routing_nbr               VARCHAR2(20) NOT NULL,
    status                         CHAR(1) DEFAULT 'L'  NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE,
    create_by                      NUMBER,
    approve_by                     NUMBER,
    sent_by                        NUMBER,
    eft_amount                     NUMBER(15,2)
  ,
  PRIMARY KEY (eft_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON eft TO report
WITH GRANT OPTION
/
GRANT INSERT ON eft TO report
WITH GRANT OPTION
/
GRANT SELECT ON eft TO report
WITH GRANT OPTION
/
GRANT UPDATE ON eft TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON eft TO report
WITH GRANT OPTION
/



-- Indexes for EFT

CREATE UNIQUE INDEX ix_eft_combo ON eft
  (
    status                          ASC,
    eft_date                        ASC,
    eft_id                          ASC
  )
COMPRESS  2
/

CREATE INDEX ix_eft_cb_id ON eft
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_eft_status ON eft
  (
    status                          ASC
  )
/

CREATE UNIQUE INDEX xfkeft ON eft
  (
    batch_ref_nbr                   ASC
  )
/



-- End of DDL Script for Table CORP.EFT

-- Start of DDL Script for Table CORP.EPORT_PART
-- Generated 13-Oct-2004 13:39:45 from CORP@USADBD02

CREATE TABLE eport_part
    (eport_part_no                  NUMBER NOT NULL,
    eport_model_no                 VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255)
  ,
  PRIMARY KEY (eport_part_no)
  USING INDEX)
/




-- Indexes for EPORT_PART
/*
CREATE UNIQUE INDEX xpkeport_part ON eport_part
  (
    eport_part_no                   ASC
  )
/
*/


-- End of DDL Script for Table CORP.EPORT_PART

-- Start of DDL Script for Table CORP.ERROR_LOG
-- Generated 13-Oct-2004 13:39:47 from CORP@USADBD02

CREATE TABLE error_log
    (trans_id                       NUMBER NOT NULL,
    error_num                      NUMBER NOT NULL,
    error_msg                      VARCHAR2(4000) NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    error_context                  VARCHAR2(1000) NOT NULL)
/




-- End of DDL Script for Table CORP.ERROR_LOG

-- Start of DDL Script for Table CORP.FEES
-- Generated 13-Oct-2004 13:39:49 from CORP@USADBD02

CREATE TABLE fees
    (fee_id                         NUMBER NOT NULL,
    fee_name                       VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(500)
  ,
  PRIMARY KEY (fee_id)
  USING INDEX)
/




-- End of DDL Script for Table CORP.FEES

-- Start of DDL Script for Table CORP.FREQUENCY
-- Generated 13-Oct-2004 13:39:51 from CORP@USADBD02

CREATE TABLE frequency
    (frequency_id                   NUMBER NOT NULL,
    name                           VARCHAR2(30) NOT NULL,
    interval                       VARCHAR2(2) DEFAULT 'DD'  NOT NULL,
    months                         NUMBER(8,0) DEFAULT 0  NOT NULL,
    days                           NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (frequency_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON frequency TO report
WITH GRANT OPTION
/
GRANT INSERT ON frequency TO report
WITH GRANT OPTION
/
GRANT SELECT ON frequency TO report
WITH GRANT OPTION
/
GRANT UPDATE ON frequency TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON frequency TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.FREQUENCY

-- Start of DDL Script for Table CORP.G4OLD_CUSTOMER
-- Generated 13-Oct-2004 13:39:54 from CORP@USADBD02

CREATE TABLE g4old_customer
    (cid                            NUMBER(10,0) NOT NULL,
    cust_ref                       CHAR(50) NOT NULL,
    cname                          CHAR(50) NOT NULL,
    type_id                        NUMBER(10,0) NOT NULL,
    dex_export                     CHAR(255),
    dex_export_type_id             NUMBER(10,0),
    trans_export                   CHAR(255),
    trans_export_type_id           NUMBER(10,0),
    eft_export                     CHAR(255),
    eft_export_type_id             NUMBER(10,0),
    audit_recip                    CHAR(255),
    caudit                         NUMBER(10,0) NOT NULL,
    zero_trans_rpt                 CHAR(255),
    status                         CHAR(1) NOT NULL,
    create_dt                      DATE NOT NULL,
    upd_by                         NUMBER(10,0),
    upd_dt                         DATE NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_CUSTOMER

-- Start of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK
-- Generated 13-Oct-2004 13:39:56 from CORP@USADBD02

CREATE TABLE g4old_customer_bank
    (id                             NUMBER(10,0) NOT NULL,
    customer_id                    NUMBER(10,0) NOT NULL,
    bank_acct                      CHAR(50) NOT NULL,
    description                    CHAR(255),
    create_dt                      DATE NOT NULL,
    status                         CHAR(1) NOT NULL,
    upd_dt                         DATE NOT NULL,
    upd_by                         NUMBER(10,0) NOT NULL,
    min_eft_amt                    NUMBER(19,4) NOT NULL,
    acct_active                    CHAR(1) NOT NULL,
    process_fee                    NUMBER(5,2) NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK

-- Start of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK_DEVICE
-- Generated 13-Oct-2004 13:39:58 from CORP@USADBD02

CREATE TABLE g4old_customer_bank_device
    (customer_bank_id               NUMBER(10,0) NOT NULL,
    device_id                      NUMBER(10,0) NOT NULL,
    create_dt                      DATE NOT NULL,
    status                         CHAR(1) NOT NULL,
    upd_dt                         DATE NOT NULL,
    upd_by                         NUMBER(10,0) NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK_DEVICE

-- Start of DDL Script for Table CORP.G4OLD_EFTS
-- Generated 13-Oct-2004 13:40:00 from CORP@USADBD02

CREATE TABLE g4old_efts
    (first_date                     DATE NOT NULL,
    customer_bank_id               NUMBER(10,0) NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_EFTS

-- Start of DDL Script for Table CORP.G4OLD_EMAIL
-- Generated 13-Oct-2004 13:40:02 from CORP@USADBD02

CREATE TABLE g4old_email
    (customer_id                    NUMBER(10,0) NOT NULL,
    customer_name                  CHAR(50) NOT NULL,
    email                          CHAR(255))
/




-- End of DDL Script for Table CORP.G4OLD_EMAIL

-- Start of DDL Script for Table CORP.LEDGER
-- Generated 13-Oct-2004 13:40:04 from CORP@USADBD02

CREATE TABLE ledger
    (ledger_id                      NUMBER(*,0) NOT NULL,
    entry_type                     CHAR(2) NOT NULL,
    trans_id                       NUMBER(*,0),
    process_fee_id                 NUMBER(*,0),
    service_fee_id                 NUMBER(*,0),
    amount                         NUMBER(21,8),
    batch_id                       NUMBER(*,0) NOT NULL,
    settle_state_id                NUMBER(*,0) NOT NULL,
    ledger_date                    DATE NOT NULL,
    description                    VARCHAR2(400),
    create_by                      NUMBER,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    deleted                        CHAR(1) DEFAULT 'N'  NOT NULL
  ,
  PRIMARY KEY (ledger_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON ledger TO report
WITH GRANT OPTION
/
GRANT INSERT ON ledger TO report
WITH GRANT OPTION
/
GRANT SELECT ON ledger TO report
WITH GRANT OPTION
/
GRANT UPDATE ON ledger TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON ledger TO report
WITH GRANT OPTION
/



-- Indexes for LEDGER

CREATE BITMAP INDEX bix_ledger_entry_type ON ledger
  (
    entry_type                      ASC
  )
/

CREATE INDEX ledger_trans_id ON ledger
  (
    trans_id                        ASC
  )
/

CREATE INDEX ledger_settle_state_id ON ledger
  (
    settle_state_id                 ASC
  )
/

CREATE INDEX ledger_service_fee_id ON ledger
  (
    service_fee_id                  ASC
  )
/

CREATE INDEX ledger_process_fee_id ON ledger
  (
    process_fee_id                  ASC
  )
/

CREATE INDEX ledger_batch_id ON ledger
  (
    batch_id                        ASC
  )
/

CREATE BITMAP INDEX bix_ledger_deleted ON ledger
  (
    deleted                         ASC
  )
/

CREATE UNIQUE INDEX uix_ledger_batch_combo ON ledger
  (
    batch_id                        ASC,
    deleted                         ASC,
    entry_type                      ASC,
    settle_state_id                 ASC,
    amount                          ASC,
    ledger_id                       ASC
  )
COMPRESS  5
/



-- Constraints for LEDGER








-- Comments for LEDGER

COMMENT ON COLUMN ledger.deleted IS 'Is this record deleted (''Y'' or ''N'')'
/

-- End of DDL Script for Table CORP.LEDGER

-- Start of DDL Script for Table CORP.LEDGER_OLD
-- Generated 13-Oct-2004 13:40:08 from CORP@USADBD02

CREATE TABLE ledger_old
    (ledger_id                      NUMBER NOT NULL,
    trans_id                       NUMBER NOT NULL,
    terminal_id                    NUMBER,
    card_number                    VARCHAR2(50),
    preauth_amount                 NUMBER(15,2),
    total_amount                   NUMBER(15,2),
    cc_appr_code                   VARCHAR2(50),
    start_date                     DATE,
    close_date                     DATE,
    server_date                    DATE,
    track_data                     VARCHAR2(255),
    batch_id                       NUMBER,
    payment_id                     NUMBER DEFAULT NULL

 ,
    eport_id                       NUMBER NOT NULL,
    trans_type_id                  NUMBER,
    create_dt                      DATE DEFAULT SYSDATE,
    merchant_id                    NUMBER,
    customer_bank_id               NUMBER,
    process_fee_id                 NUMBER,
    settle_state_id                NUMBER NOT NULL,
    paid_status                    CHAR(1) NOT NULL,
    fee_percent                    NUMBER(8,4),
    fee_amount                     NUMBER(15,2)
  ,
  CONSTRAINT PK_LEDGER
  PRIMARY KEY (ledger_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON ledger_old TO report
WITH GRANT OPTION
/
GRANT INSERT ON ledger_old TO report
WITH GRANT OPTION
/
GRANT SELECT ON ledger_old TO report
WITH GRANT OPTION
/
GRANT UPDATE ON ledger_old TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON ledger_old TO report
WITH GRANT OPTION
/



-- Indexes for LEDGER_OLD

CREATE UNIQUE INDEX uix_ledger_old_temp ON ledger_old
  (
    payment_id                      ASC,
    trans_type_id                   ASC,
    trans_id                        ASC,
    ledger_id                       ASC
  )
COMPRESS  3
/

CREATE UNIQUE INDEX uix_ledger_for_unpaid2 ON ledger_old
  (
    paid_status                     ASC,
    close_date                      ASC,
    customer_bank_id                ASC,
    trans_type_id                   ASC,
    terminal_id                     ASC,
    total_amount                    ASC,
    fee_percent                     ASC,
    fee_amount                      ASC,
    trans_id                        ASC
  )
COMPRESS  8
/

CREATE UNIQUE INDEX uix_ledger_for_unpaid ON ledger_old
  (
    paid_status                     ASC,
    customer_bank_id                ASC,
    trans_type_id                   ASC,
    fee_percent                     ASC,
    fee_amount                      ASC,
    total_amount                    ASC,
    trans_id                        ASC
  )
COMPRESS  6
/

CREATE INDEX ix_ledger_paymt_id_terminal_id ON ledger_old
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_ledger_payment_id ON ledger_old
  (
    payment_id                      ASC,
    trans_id                        ASC
  )
/

CREATE INDEX ix_ledger_batch_id ON ledger_old
  (
    batch_id                        ASC
  )
/

CREATE INDEX ix_ledger_close_date ON ledger_old
  (
    close_date                      ASC
  )
/

CREATE INDEX ix_ledger_eport_id ON ledger_old
  (
    eport_id                        ASC
  )
/

CREATE INDEX ix_ledger_terminal_id ON ledger_old
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_ledger_trans_type_id ON ledger_old
  (
    trans_type_id                   ASC
  )
/

CREATE UNIQUE INDEX uix_ledger_tran_id ON ledger_old
  (
    trans_id                        ASC
  )
/



-- Triggers for LEDGER_OLD

CREATE OR REPLACE TRIGGER tbiu_ledger
 BEFORE
  INSERT OR UPDATE
 ON ledger_old
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.TRANS_TYPE_ID IN(20,21) AND :NEW.TOTAL_AMOUNT > 0 THEN
        :NEW.TOTAL_AMOUNT := -:NEW.TOTAL_AMOUNT;
    END IF;

    -- Find terminal_id, customer_bank_id and, process_fee_id
    IF INSERTING OR :OLD.EPORT_ID != :NEW.EPORT_ID
            OR :OLD.CLOSE_DATE != :NEW.CLOSE_DATE
            OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID THEN
        BEGIN
            SELECT TERMINAL_ID
              INTO :NEW.TERMINAL_ID
              FROM TERMINAL_EPORT
             WHERE EPORT_ID = :NEW.EPORT_ID
               AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
               AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            BEGIN
                SELECT CUSTOMER_BANK_ID
                  INTO :NEW.CUSTOMER_BANK_ID
                  FROM CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.CUSTOMER_BANK_ID := NULL; -- even if customer bank mapping not found look for process fees
                WHEN OTHERS THEN
                    RAISE;
            END;
            BEGIN
                SELECT PROCESS_FEE_ID, FEE_PERCENT, FEE_AMOUNT
                  INTO :NEW.PROCESS_FEE_ID, :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
                  FROM PROCESS_FEES
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.PROCESS_FEE_ID := NULL;
                    :NEW.FEE_PERCENT := NULL;
                    :NEW.FEE_AMOUNT := NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                :NEW.TERMINAL_ID := NULL;
                :NEW.CUSTOMER_BANK_ID := NULL;
                :NEW.PROCESS_FEE_ID := NULL;
                :NEW.FEE_PERCENT := NULL;
                :NEW.FEE_AMOUNT := NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    ELSIF UPDATING AND NVL(:OLD.TERMINAL_ID, 0) != NVL(:NEW.TERMINAL_ID, 0) THEN
        -- Find customer_bank_id and, process_fee_id
        IF :NEW.TERMINAL_ID IS NULL THEN
            :NEW.CUSTOMER_BANK_ID := NULL;
            :NEW.PROCESS_FEE_ID := NULL;
            :NEW.FEE_PERCENT := NULL;
            :NEW.FEE_AMOUNT := NULL;
        ELSE
            BEGIN
                SELECT CUSTOMER_BANK_ID
                  INTO :NEW.CUSTOMER_BANK_ID
                  FROM CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.CUSTOMER_BANK_ID := NULL; -- even if customer bank mapping not found look for process fees
                WHEN OTHERS THEN
                    RAISE;
            END;
            BEGIN
                SELECT PROCESS_FEE_ID, FEE_PERCENT, FEE_AMOUNT
                  INTO :NEW.PROCESS_FEE_ID, :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
                  FROM PROCESS_FEES
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.PROCESS_FEE_ID := NULL;
                    :NEW.FEE_PERCENT := NULL;
                    :NEW.FEE_AMOUNT := NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    ELSIF UPDATING AND NVL(:OLD.PROCESS_FEE_ID, 0) != NVL(:NEW.PROCESS_FEE_ID, 0) THEN
        BEGIN
            SELECT FEE_PERCENT, FEE_AMOUNT
              INTO :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
              FROM PROCESS_FEES
             WHERE PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                :NEW.FEE_PERCENT := NULL;
                :NEW.FEE_AMOUNT := NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    END IF;
    
    -- Calculate PAID_STATUS
    IF :NEW.PAYMENT_ID IS NOT NULL THEN
        :NEW.PAID_STATUS := 'P'; -- Paid
    ELSIF :NEW.SETTLE_STATE_ID IN(2,3,5,6) THEN
        :NEW.PAID_STATUS := 'U'; -- Unpaid
    ELSE
        :NEW.PAID_STATUS := 'N'; -- Not payable
    END IF;
END;
/


-- Comments for LEDGER_OLD

COMMENT ON COLUMN ledger_old.paid_status IS 'P = Paid; U = Unpaid; N = Not payable'
/

-- End of DDL Script for Table CORP.LEDGER_OLD

-- Start of DDL Script for Table CORP.LICENSE
-- Generated 13-Oct-2004 13:40:13 from CORP@USADBD02

CREATE TABLE license
    (license_id                     NUMBER NOT NULL,
    title                          VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255),
    status                         CHAR(1) DEFAULT 'A'  NOT NULL
  ,
  PRIMARY KEY (license_id)
  USING INDEX)
/




-- Indexes for LICENSE

CREATE INDEX ix_license_status ON license
  (
    status                          ASC
  )
/



-- End of DDL Script for Table CORP.LICENSE

-- Start of DDL Script for Table CORP.LICENSE_NBR
-- Generated 13-Oct-2004 13:40:15 from CORP@USADBD02

CREATE TABLE license_nbr
    (license_nbr                    VARCHAR2(15) NOT NULL,
    license_id                     NUMBER NOT NULL,
    customer_id                    NUMBER NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL
  ,
  PRIMARY KEY (license_nbr)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON license_nbr TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_nbr TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_nbr TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_nbr TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_nbr TO report
WITH GRANT OPTION
/



-- Indexes for LICENSE_NBR

CREATE INDEX ix_license_nbr_customer_id ON license_nbr
  (
    customer_id                     ASC
  )
/

CREATE INDEX ix_license_nbr_license_id ON license_nbr
  (
    license_id                      ASC
  )
/



-- End of DDL Script for Table CORP.LICENSE_NBR

-- Start of DDL Script for Table CORP.LICENSE_PROCESS_FEES
-- Generated 13-Oct-2004 13:40:17 from CORP@USADBD02

CREATE TABLE license_process_fees
    (license_id                     NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    fee_percent                    NUMBER(8,4) NOT NULL,
    fee_amount                     NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (license_id, trans_type_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_process_fees TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.LICENSE_PROCESS_FEES

-- Start of DDL Script for Table CORP.LICENSE_SERVICE_FEES
-- Generated 13-Oct-2004 13:40:20 from CORP@USADBD02

CREATE TABLE license_service_fees
    (license_id                     NUMBER NOT NULL,
    fee_id                         NUMBER NOT NULL,
    amount                         NUMBER(15,2) NOT NULL,
    frequency_id                   NUMBER DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (license_id, fee_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_service_fees TO report
WITH GRANT OPTION
/



-- Indexes for LICENSE_SERVICE_FEES

CREATE INDEX ix_lsf_frequency_id ON license_service_fees
  (
    frequency_id                    ASC
  )
/



-- End of DDL Script for Table CORP.LICENSE_SERVICE_FEES

-- Start of DDL Script for Table CORP.MERCHANT
-- Generated 13-Oct-2004 13:40:22 from CORP@USADBD02

CREATE TABLE merchant
    (merchant_id                    NUMBER NOT NULL,
    merchant_nbr                   VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255),
    status                         VARCHAR2(1) DEFAULT 'A'  NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    upd_by                         NUMBER NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_MERCHANT
  PRIMARY KEY (merchant_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON merchant TO report
WITH GRANT OPTION
/
GRANT INSERT ON merchant TO report
WITH GRANT OPTION
/
GRANT SELECT ON merchant TO report
WITH GRANT OPTION
/
GRANT UPDATE ON merchant TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON merchant TO report
WITH GRANT OPTION
/
/*
GRANT DELETE ON merchant TO g4op
WITH GRANT OPTION
/
GRANT INSERT ON merchant TO g4op
WITH GRANT OPTION
/
GRANT SELECT ON merchant TO g4op
WITH GRANT OPTION
/
GRANT UPDATE ON merchant TO g4op
WITH GRANT OPTION
/
GRANT REFERENCES ON merchant TO g4op
WITH GRANT OPTION
/*/



-- Indexes for MERCHANT

CREATE UNIQUE INDEX uix_merchant_nbr ON merchant
  (
    merchant_nbr                    ASC
  )
/



-- End of DDL Script for Table CORP.MERCHANT

-- Start of DDL Script for Table CORP.OPER_EPORT
-- Generated 13-Oct-2004 13:40:24 from CORP@USADBD02

CREATE TABLE oper_eport
    (eport_id                       NUMBER NOT NULL,
    eport_serial_nbr               VARCHAR2(50),
    activation_date                DATE,
    termination_date               DATE,
    dialing_date                   DATE
  ,
  PRIMARY KEY (eport_id)
  USING INDEX)
/




-- Indexes for OPER_EPORT
/*
CREATE UNIQUE INDEX xpkeport ON oper_eport
  (
    eport_id                        ASC
  )
/
*/


-- End of DDL Script for Table CORP.OPER_EPORT

-- Start of DDL Script for Table CORP.PAYMENT_ADJUSTMENT
-- Generated 13-Oct-2004 13:40:26 from CORP@USADBD02

CREATE TABLE payment_adjustment
    (adjust_id                      NUMBER NOT NULL,
    eft_id                         NUMBER NOT NULL,
    terminal_id                    NUMBER DEFAULT 0  NOT NULL,
    reason                         VARCHAR2(255) NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    create_by                      NUMBER NOT NULL,
    amount                         NUMBER(15,2) NOT NULL,
    customer_bank_id               NUMBER DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (adjust_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT INSERT ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT SELECT ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_adjustment TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENT_ADJUSTMENT

CREATE INDEX ix_payment_adjust_eft_terminal ON payment_adjustment
  (
    eft_id                          ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pay_adj_eft_id_cust_bank_id ON payment_adjustment
  (
    eft_id                          ASC,
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_adjust_create_date ON payment_adjustment
  (
    create_date                     ASC
  )
/

CREATE INDEX ix_adjust_cust_bank_id ON payment_adjustment
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_adjust_eft_id ON payment_adjustment
  (
    eft_id                          ASC
  )
/

CREATE INDEX ix_adjust_terminal_id ON payment_adjustment
  (
    terminal_id                     ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_ADJUSTMENT

-- Start of DDL Script for Table CORP.PAYMENT_PROCESS_FEE
-- Generated 13-Oct-2004 13:40:29 from CORP@USADBD02

CREATE TABLE payment_process_fee
    (payment_id                     NUMBER NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    gross_amount                   NUMBER(15,2),
    fee_percent                    NUMBER(8,4) NOT NULL,
    fee_amount                     NUMBER(15,2),
    net_amount                     NUMBER(15,2),
    customer_bank_id               NUMBER NOT NULL,
    tran_fee_amount                NUMBER(15,2),
    tran_count                     NUMBER
  ,
  CONSTRAINT PK_PAYMENT_PROCESS_FEE
  PRIMARY KEY (payment_id, terminal_id, trans_type_id, fee_percent, customer_bank_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT INSERT ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT SELECT ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_process_fee TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENT_PROCESS_FEE

CREATE INDEX ix_pay_proc_fee_pay_id_term_id ON payment_process_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pay_process_fee_payment_id ON payment_process_fee
  (
    payment_id                      ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_PROCESS_FEE

-- Start of DDL Script for Table CORP.PAYMENT_SCHEDULE
-- Generated 13-Oct-2004 13:40:32 from CORP@USADBD02

CREATE TABLE payment_schedule
    (payment_schedule_id            NUMBER NOT NULL,
    description                    VARCHAR2(100) NOT NULL,
    interval                       CHAR(2),
    months                         NUMBER,
    days                           NUMBER
  ,
  PRIMARY KEY (payment_schedule_id)
  USING INDEX)
/

-- Grants for Table
GRANT SELECT ON payment_schedule TO public
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_schedule TO public
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.PAYMENT_SCHEDULE

-- Start of DDL Script for Table CORP.PAYMENT_SERVICE_FEE
-- Generated 13-Oct-2004 13:40:34 from CORP@USADBD02

CREATE TABLE payment_service_fee
    (payment_id                     NUMBER DEFAULT 0  NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    fee_id                         NUMBER NOT NULL,
    fee_amount                     NUMBER(15,2) NOT NULL,
    fee_date                       DATE NOT NULL,
    status                         CHAR(1) DEFAULT 'A'  NOT NULL,
    payment_service_fee_id         NUMBER NOT NULL,
    service_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_PAYMENT_SERVICE_FEE
  PRIMARY KEY (payment_service_fee_id)
  USING INDEX)
/




-- Indexes for PAYMENT_SERVICE_FEE

CREATE INDEX ix_pay_serv_fee_pay_term_stat ON payment_service_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC,
    status                          ASC
  )
/

CREATE INDEX ix_pay_service_fee_payment_id ON payment_service_fee
  (
    payment_id                      ASC
  )
/

CREATE INDEX ix_pay_serv_fee_combo ON payment_service_fee
  (
    payment_id                      ASC,
    status                          ASC,
    terminal_id                     ASC,
    fee_date                        ASC,
    fee_amount                      ASC
  )
/

CREATE UNIQUE INDEX uix_payment_service_fee ON payment_service_fee
  (
    fee_id                          ASC,
    terminal_id                     ASC,
    fee_date                        ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_SERVICE_FEE

-- Start of DDL Script for Table CORP.PAYMENTS
-- Generated 13-Oct-2004 13:40:36 from CORP@USADBD02

CREATE TABLE payments
    (payment_id                     NUMBER NOT NULL,
    eft_id                         NUMBER NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    gross_amount                   NUMBER(15,2),
    tax_amount                     NUMBER(15,2),
    process_fee_amount             NUMBER(15,2),
    service_fee_amount             NUMBER(15,2),
    refund_amount                  NUMBER(15,2),
    chargeback_amount              NUMBER(15,2),
    adjust_amount                  NUMBER(15,2),
    net_amount                     NUMBER(15,2),
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    payment_schedule_id            NUMBER,
    payment_start_date             DATE,
    payment_end_date               DATE
  ,
  CONSTRAINT PAYMENTS_PK21014403827934
  PRIMARY KEY (payment_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON payments TO report
WITH GRANT OPTION
/
GRANT INSERT ON payments TO report
WITH GRANT OPTION
/
GRANT SELECT ON payments TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payments TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payments TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENTS

CREATE INDEX ix_payments_eft_id ON payments
  (
    eft_id                          ASC
  )
/

CREATE INDEX ix_payments_terminal_id ON payments
  (
    terminal_id                     ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENTS

-- Start of DDL Script for Table CORP.PROCESS_FEES
-- Generated 13-Oct-2004 13:40:39 from CORP@USADBD02

CREATE TABLE process_fees
    (terminal_id                    NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    fee_percent                    NUMBER(8,4) NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    process_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    fee_amount                     NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  CONSTRAINT PK_PROCESS_FEES
  PRIMARY KEY (process_fee_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON process_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON process_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON process_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON process_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON process_fees TO report
WITH GRANT OPTION
/



-- Indexes for PROCESS_FEES

CREATE UNIQUE INDEX uix_process_fees_combo1 ON process_fees
  (
    trans_type_id                   ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC,
    process_fee_id                  ASC
  )
/

CREATE UNIQUE INDEX uix_process_fees_combo2 ON process_fees
  (
    process_fee_id                  ASC,
    fee_percent                     ASC,
    fee_amount                      ASC
  )
/

CREATE INDEX ix_pf_end_date ON process_fees
  (
    end_date                        ASC
  )
/

CREATE INDEX ix_pf_start_date ON process_fees
  (
    start_date                      ASC
  )
/

CREATE INDEX ix_pf_terminal_id ON process_fees
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pf_trans_type_id ON process_fees
  (
    trans_type_id                   ASC
  )
/



-- Triggers for PROCESS_FEES

CREATE OR REPLACE TRIGGER traiud_process_fees
 AFTER
  INSERT OR DELETE OR UPDATE
 ON process_fees
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        -- must update ledger & trans dim
        UPDATE LEDGER L SET L.AMOUNT = (
            SELECT T.TOTAL_AMOUNT * :NEW.FEE_PERCENT + :NEW.FEE_AMOUNT
              FROM TRANS T
             WHERE T.TRAN_ID = L.TRANS_ID)
         WHERE L.ENTRY_TYPE = 'PF'
           AND L.PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
           
        IF :OLD.PROCESS_FEE_ID != :NEW.PROCESS_FEE_ID OR :OLD.TERMINAL_ID != :NEW.TERMINAL_ID
                OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID
                OR NVL(:OLD.START_DATE, MIN_DATE) !=  NVL(:NEW.START_DATE, MIN_DATE)
                OR NVL(:OLD.END_DATE, MAX_DATE) !=  NVL(:NEW.END_DATE, MAX_DATE) THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
                   FROM PROCESS_FEES PF
                   WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                     AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                     AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                     AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
             WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
                OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
        END IF;
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE)) -- should be null always
             WHERE PROCESS_FEE_ID = :OLD.PROCESS_FEE_ID;
        END IF;
   END IF;
END;
/


-- End of DDL Script for Table CORP.PROCESS_FEES

-- Start of DDL Script for Table CORP.PURCHASE
-- Generated 13-Oct-2004 13:40:43 from CORP@USADBD02

CREATE TABLE purchase
    (purchase_id                    NUMBER NOT NULL,
    ledger_id                      NUMBER NOT NULL,
    trans_id                       NUMBER,
    trans_date                     DATE,
    amount                         NUMBER(15,2),
    vend_column                    VARCHAR2(50)
  ,
  PRIMARY KEY (purchase_id)
  USING INDEX)
/




-- Indexes for PURCHASE

CREATE INDEX xif30purchase ON purchase
  (
    ledger_id                       ASC
  )
/
/*
CREATE UNIQUE INDEX xpkpurchase ON purchase
  (
    purchase_id                     ASC
  )
/
*/


-- End of DDL Script for Table CORP.PURCHASE

-- Start of DDL Script for Table CORP.REASON_CODE
-- Generated 13-Oct-2004 13:40:45 from CORP@USADBD02

CREATE TABLE reason_code
    (reason_code_id                 NUMBER NOT NULL,
    reason                         VARCHAR2(50) NOT NULL,
    reason_desc                    VARCHAR2(50)
  ,
  PRIMARY KEY (reason_code_id)
  USING INDEX)
/




-- Indexes for REASON_CODE
/*
CREATE UNIQUE INDEX xpkreason_code ON reason_code
  (
    reason_code_id                  ASC
  )
/
*/


-- End of DDL Script for Table CORP.REASON_CODE

-- Start of DDL Script for Table CORP.REFUND
-- Generated 13-Oct-2004 13:40:48 from CORP@USADBD02

CREATE TABLE refund
    (refund_id                      NUMBER NOT NULL,
    ledger_id                      NUMBER,
    cc_appr_code                   VARCHAR2(50) NOT NULL,
    card_number                    VARCHAR2(50) NOT NULL,
    tran_date                      DATE NOT NULL,
    problem_date                   DATE NOT NULL,
    location_name                  VARCHAR2(50),
    reason_code_id                 NUMBER,
    name                           VARCHAR2(50),
    phone                          VARCHAR2(20),
    description                    VARCHAR2(255),
    refund_status                  VARCHAR2(20) NOT NULL,
    upd_by                         VARCHAR2(50),
    upd_date                       DATE DEFAULT SYSDATE
 
  ,
  PRIMARY KEY (refund_id)
  USING INDEX)
/

-- Grants for Table
/*
GRANT DELETE ON refund TO g4op
WITH GRANT OPTION
/
GRANT INSERT ON refund TO g4op
WITH GRANT OPTION
/
GRANT SELECT ON refund TO g4op
WITH GRANT OPTION
/
GRANT UPDATE ON refund TO g4op
WITH GRANT OPTION
/
GRANT REFERENCES ON refund TO g4op
WITH GRANT OPTION
/
*/


-- Indexes for REFUND

CREATE INDEX xif50refund ON refund
  (
    ledger_id                       ASC
  )
/

CREATE INDEX xif51refund ON refund
  (
    reason_code_id                  ASC
  )
/
/*
CREATE UNIQUE INDEX xpkrefund ON refund
  (
    refund_id                       ASC
  )
/
*/


-- Constraints for REFUND



-- End of DDL Script for Table CORP.REFUND

-- Start of DDL Script for Table CORP.SCHEDULE
-- Generated 13-Oct-2004 13:40:50 from CORP@USADBD02

CREATE TABLE schedule
    (schedule_id                    NUMBER NOT NULL,
    schedule_type                  NUMBER NOT NULL,
    schedule_days                  VARCHAR2(20) NOT NULL,
    schedule_name                  VARCHAR2(255) NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    create_by                      NUMBER NOT NULL,
    upd_by                         NUMBER NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE  NOT NULL,
    status                         CHAR(1) DEFAULT 'A'  NOT NULL
  ,
  PRIMARY KEY (schedule_id)
  USING INDEX)
/




-- Indexes for SCHEDULE
/*
CREATE UNIQUE INDEX xpkschedule ON schedule
  (
    schedule_id                     ASC
  )
/
*/


-- Constraints for SCHEDULE



-- End of DDL Script for Table CORP.SCHEDULE

-- Start of DDL Script for Table CORP.SCHEDULE_TYPE
-- Generated 13-Oct-2004 13:40:53 from CORP@USADBD02

CREATE TABLE schedule_type
    (schedule_type_id               NUMBER NOT NULL,
    schedule_type                  VARCHAR2(25) NOT NULL,
    description                    VARCHAR2(255),
    status                         CHAR(1) DEFAULT 'A'  NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    create_by                      NUMBER NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE  NOT NULL,
    upd_by                         NUMBER NOT NULL
  ,
  PRIMARY KEY (schedule_type_id)
  USING INDEX)
/




-- Indexes for SCHEDULE_TYPE
/*
CREATE UNIQUE INDEX xpkschedule_type ON schedule_type
  (
    schedule_type_id                ASC
  )
/
*/


-- End of DDL Script for Table CORP.SCHEDULE_TYPE

-- Start of DDL Script for Table CORP.SERVICE_FEES
-- Generated 13-Oct-2004 13:40:55 from CORP@USADBD02

CREATE TABLE service_fees
    (terminal_id                    NUMBER DEFAULT 0  NOT NULL,
    fee_id                         NUMBER NOT NULL,
    fee_amount                     NUMBER(15,2) NOT NULL,
    frequency_id                   NUMBER NOT NULL,
    last_payment                   DATE DEFAULT SYSDATE  NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    service_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_SERVICE_FEE
  PRIMARY KEY (service_fee_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON service_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON service_fees TO report
WITH GRANT OPTION
/



-- Indexes for SERVICE_FEES

CREATE INDEX ix_sf_fee_id ON service_fees
  (
    fee_id                          ASC
  )
/

CREATE INDEX ix_sf_frequency_id ON service_fees
  (
    frequency_id                    ASC
  )
/

CREATE INDEX ix_sf_terminal_id ON service_fees
  (
    terminal_id                     ASC
  )
/



-- End of DDL Script for Table CORP.SERVICE_FEES

-- Start of DDL Script for Table CORP.SQLN_EXPLAIN_PLAN
-- Generated 13-Oct-2004 13:40:57 from CORP@USADBD02

CREATE TABLE sqln_explain_plan
    (statement_id                   VARCHAR2(30),
    timestamp                      DATE,
    remarks                        VARCHAR2(80),
    operation                      VARCHAR2(30),
    options                        VARCHAR2(30),
    object_node                    VARCHAR2(128),
    object_owner                   VARCHAR2(30),
    object_name                    VARCHAR2(30),
    object_instance                NUMBER(*,0),
    object_type                    VARCHAR2(30),
    optimizer                      VARCHAR2(255),
    search_columns                 NUMBER(*,0),
    id                             NUMBER(*,0),
    parent_id                      NUMBER(*,0),
    position                       NUMBER(*,0),
    cost                           NUMBER(*,0),
    cardinality                    NUMBER(*,0),
    bytes                          NUMBER(*,0),
    other_tag                      VARCHAR2(255),
    partition_start                VARCHAR2(255),
    partition_stop                 VARCHAR2(255),
    partition_id                   NUMBER(*,0),
    other                          LONG,
    distribution                   VARCHAR2(30))
/

-- Grants for Table
GRANT DELETE ON sqln_explain_plan TO public
/
GRANT INSERT ON sqln_explain_plan TO public
/
GRANT SELECT ON sqln_explain_plan TO public
/
GRANT UPDATE ON sqln_explain_plan TO public
/



-- End of DDL Script for Table CORP.SQLN_EXPLAIN_PLAN

-- Start of DDL Script for Table CORP.UTL_DATA
-- Generated 13-Oct-2004 13:41:00 from CORP@USADBD02

CREATE TABLE utl_data
    (utl_data_id                    NUMBER NOT NULL,
    utldata                        VARCHAR2(500),
    utlname                        VARCHAR2(50)
  ,
  PRIMARY KEY (utl_data_id)
  USING INDEX)
/




-- Indexes for UTL_DATA
/*
CREATE UNIQUE INDEX pk_utl_data ON utl_data
  (
    utl_data_id                     ASC
  )
/
*/
CREATE UNIQUE INDEX sys_utl_name ON utl_data
  (
    utlname                         ASC
  )
/



-- End of DDL Script for Table CORP.UTL_DATA

-- Foreign Key
ALTER TABLE batch
ADD FOREIGN KEY (doc_id)
REFERENCES doc (doc_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (terminal_id)
REFERENCES report.terminal (terminal_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (payment_schedule_id)
REFERENCES payment_schedule (payment_schedule_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- Foreign Key
ALTER TABLE categories
ADD FOREIGN KEY (category_id)
REFERENCES categories (category_id)
/
-- Foreign Key
ALTER TABLE chargeback
ADD FOREIGN KEY (reason_code_id)
REFERENCES reason_code (reason_code_id)
/
-- Foreign Key
ALTER TABLE customer_addr
ADD FOREIGN KEY (customer_id)
REFERENCES customer (customer_id)
/
ALTER TABLE customer_addr
ADD FOREIGN KEY (addr_type)
REFERENCES address_type (addr_type_id)
/
-- Foreign Key
ALTER TABLE customer_bank
ADD FOREIGN KEY (customer_id)
REFERENCES customer (customer_id)
/
ALTER TABLE customer_bank
ADD FOREIGN KEY (schedule_id)
REFERENCES schedule (schedule_id)
/
-- Foreign Key
ALTER TABLE customer_bank_terminal
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- Foreign Key
ALTER TABLE dealer_license
ADD CONSTRAINT fk_dl_dealer_id FOREIGN KEY (dealer_id)
REFERENCES dealer (dealer_id)
/
-- Foreign Key
ALTER TABLE doc
ADD CONSTRAINT fk_doc_approve_user FOREIGN KEY (approve_by)
REFERENCES report.user_login (user_id)
/
ALTER TABLE doc
ADD CONSTRAINT fk_doc_create_user FOREIGN KEY (create_by)
REFERENCES report.user_login (user_id)
/
ALTER TABLE doc
ADD CONSTRAINT fk_doc_sent_user FOREIGN KEY (sent_by)
REFERENCES report.user_login (user_id)
/
ALTER TABLE doc
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- Foreign Key
ALTER TABLE ledger
ADD CONSTRAINT fk_ledger_user FOREIGN KEY (create_by)
REFERENCES report.user_login (user_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (trans_id)
REFERENCES report.trans (tran_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (settle_state_id)
REFERENCES report.trans_state (state_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (service_fee_id)
REFERENCES service_fees (service_fee_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (process_fee_id)
REFERENCES process_fees (process_fee_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (batch_id)
REFERENCES batch (batch_id)
/
-- Foreign Key
ALTER TABLE refund
ADD FOREIGN KEY (reason_code_id)
REFERENCES reason_code (reason_code_id)
/
-- Foreign Key
ALTER TABLE schedule
ADD FOREIGN KEY (schedule_type)
REFERENCES schedule_type (schedule_type_id)
/
-- End of DDL script for Foreign Key(s)
