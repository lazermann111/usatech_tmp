-- Start of DDL Script for Table CORP.PAYMENT_PROCESS_FEE
-- Generated 9/16/2004 9:38:12 AM from CORP@REPTP

CREATE TABLE payment_process_fee
    (payment_id                     NUMBER NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    gross_amount                   NUMBER(15,2),
    fee_percent                    NUMBER(8,4) NOT NULL,
    fee_amount                     NUMBER(15,2),
    net_amount                     NUMBER(15,2),
    customer_bank_id               NUMBER NOT NULL
  ,
  CONSTRAINT PK_PAYMENT_PROCESS_FEE
  PRIMARY KEY (payment_id, terminal_id, trans_type_id, fee_percent, customer_bank_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT INSERT ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT SELECT ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_process_fee TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENT_PROCESS_FEE

CREATE INDEX ix_pay_proc_fee_pay_id_term_id ON payment_process_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pay_process_fee_payment_id ON payment_process_fee
  (
    payment_id                      ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_PROCESS_FEE

