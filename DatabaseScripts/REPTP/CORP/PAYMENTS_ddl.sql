-- Start of DDL Script for Table CORP.PAYMENTS
-- Generated 9/16/2004 9:46:57 AM from CORP@REPTP

CREATE TABLE payments
    (payment_id                     NUMBER NOT NULL,
    eft_id                         NUMBER NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    gross_amount                   NUMBER(15,2),
    tax_amount                     NUMBER(15,2),
    process_fee_amount             NUMBER(15,2),
    service_fee_amount             NUMBER(15,2),
    refund_amount                  NUMBER(15,2),
    chargeback_amount              NUMBER(15,2),
    adjust_amount                  NUMBER(15,2),
    net_amount                     NUMBER(15,2),
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PAYMENTS_PK21014403827934
  PRIMARY KEY (payment_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON payments TO report
WITH GRANT OPTION
/
GRANT INSERT ON payments TO report
WITH GRANT OPTION
/
GRANT SELECT ON payments TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payments TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payments TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENTS

CREATE INDEX ix_payments_eft_id ON payments
  (
    eft_id                          ASC
  )
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX ix_payments_terminal_id ON payments
  (
    terminal_id                     ASC
  )
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- End of DDL Script for Table CORP.PAYMENTS

