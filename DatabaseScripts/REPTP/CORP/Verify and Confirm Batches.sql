--Verify Fill Batches 
DECLARE
 CURSOR l_cur IS
     SELECT b.batch_id
       FROM CORP.BATCH b
     WHERE B.PAYMENT_SCHEDULE_ID IN(2)
       AND B.END_DATE IS NOT NULL
       AND B.BATCH_STATE_CD = 'O';
BEGIN
    FOR l_rec IN l_cur LOOP
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSTIMESTAMP, 'MM/DD/YYYY HH24:MI:SS.FF3') || ' - Starting Check Fill Batch Complete for ' || l_rec.batch_id);
        CORP.PAYMENTS_PKG.check_fill_batch_complete(l_rec.batch_id);
        COMMIT;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSTIMESTAMP, 'MM/DD/YYYY HH24:MI:SS.FF3') || ' - Finished Check Fill Batch Complete for ' || l_rec.batch_id);             
    END LOOP;
END;          
/

-- Confirm all batches that were confirmed by CCE FILL
UPDATE CORP.BATCH B
   SET BATCH_STATE_CD = 'C',
       BATCH_CONFIRM_TS = SYSDATE,
       BATCH_CONFIRM_TYPE_ID = 2,
       BATCH_CONFIRM_KEY = (SELECT f.CCE_FILL_ID FROM USAT_CUSTOM.CCE_FILL f WHERE f.BATCH_ID =  B.BATCH_ID AND F.CCE_FILL_STATE_ID IN(4,5,8))
 WHERE BATCH_STATE_CD IN('D', 'U')
   AND PAYMENT_SCHEDULE_ID = 2
   AND BATCH_ID IN(SELECT f.BATCH_ID
                     FROM USAT_CUSTOM.CCE_FILL f
                    WHERE f.BATCH_ID IS NOT NULL 
                      AND F.CCE_FILL_STATE_ID IN(4,5,8)
                 GROUP BY f.BATCH_ID
                 HAVING COUNT(*) = 1);
COMMIT;
