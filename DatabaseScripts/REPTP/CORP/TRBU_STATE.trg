CREATE OR REPLACE TRIGGER corp.trbu_state
BEFORE UPDATE ON corp.state
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT :OLD.CREATED_BY,
           :OLD.CREATED_TS,
           SYSDATE,
           USER
      INTO :NEW.CREATED_BY,
           :NEW.CREATED_TS,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/