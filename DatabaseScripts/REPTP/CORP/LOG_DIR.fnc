CREATE OR REPLACE FUNCTION CORP.LOG_DIR (LOGTYPE IN VARCHAR2) RETURN VARCHAR2 IS
    return_value UTL_DATA.utldata%TYPE := NULL;
begin
  if logtype is null then
    --return default
      return 'No utlname value supplied';
  else
    --look for correct value
      --DECLARE
      BEGIN
      select UTL_DATA.utldata into return_value from corp.UTL_DATA where utlname = logtype;
        EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
            return_value := 'Not Found';
      END;
  end if;
  return return_value;
end;

/