CREATE OR REPLACE PROCEDURE SERVICE_FEES_UPD
   (l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
    l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
    l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
    l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
    l_effective_dt IN SERVICE_FEES.END_DATE%TYPE )
IS
   l_effective_date DATE;
   l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
BEGIN
	 IF l_effective_dt IS NULL THEN
	     l_effective_date := SYSDATE;
	 ELSE
	 	 l_effective_date := l_effective_dt;
	 END IF;
	 BEGIN
		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
		END IF;
	 EXCEPTION
	 	WHEN NO_DATA_FOUND THEN
			 NULL;
		WHEN OTHERS THEN
			 RAISE;
	 END;
	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
	 IF l_fee_amt <> 0 THEN
         INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
	 END IF;
END SERVICE_FEES_UPD;
