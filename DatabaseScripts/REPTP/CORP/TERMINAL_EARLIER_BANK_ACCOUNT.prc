CREATE OR REPLACE PROCEDURE CORP.TERMINAL_EARLIER_BANK_ACCOUNT 
   (l_cbt_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_TERMINAL_ID%TYPE,
	l_effective_date IN DATE)
IS
    l_terminal_id CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE;
    l_end_date    CUSTOMER_BANK_TERMINAL.END_DATE%TYPE;
BEGIN
    -- Update mapping
    UPDATE CUSTOMER_BANK_TERMINAL
       SET START_DATE = l_effective_date
     WHERE CUSTOMER_BANK_TERMINAL_ID = l_cbt_id
       RETURNING TERMINAL_ID, END_DATE INTO l_terminal_id, l_end_date;

    -- Update all other Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(START_DATE, MIN_DATE) < NVL(l_end_date, MAX_DATE)
       AND NVL(END_DATE, MAX_DATE) > l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
END TERMINAL_EARLIER_BANK_ACCOUNT;
/
