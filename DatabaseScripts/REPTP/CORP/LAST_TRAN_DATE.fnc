CREATE OR REPLACE FUNCTION CORP.LAST_TRAN_DATE     (l_doc_id INT, l_terminal_id int)
RETURN date

/* This function returns the last tran date 
   for a specific terminal_id for a given EFT 
   
   Tom Shannon   6/6/02
*/

IS
    last_tran_dt DATE;
BEGIN
    SELECT MAX(LEDGER_DATE)
      INTO last_tran_dt
      FROM LEDGER L, BATCH B
     WHERE B.TERMINAL_ID = L_TERMINAL_ID
       AND B.DOC_ID = L_DOC_ID
       AND L.BATCH_ID = B.BATCH_ID;
    RETURN last_tran_dt;
END;

/