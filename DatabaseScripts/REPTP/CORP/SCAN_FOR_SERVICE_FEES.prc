CREATE OR REPLACE PROCEDURE SCAN_FOR_SERVICE_FEES
IS
    CURSOR c_fee
    IS
        SELECT service_fee_id,
               terminal_id,
               fee_id,
               fee_amount,
               months,
               days,
               last_payment,
               start_date,
               end_date
          FROM service_fees sf, frequency q
         WHERE sf.frequency_id = q.frequency_id
           AND (   days <> 0
                OR months <> 0)
           AND ADD_MONTHS (last_payment + days, months) <
                                      LEAST (NVL (end_date, SYSDATE), SYSDATE);
                                      
    cb_DEBUG_MODE                      CONSTANT BOOLEAN := FALSE;

    l_service_fee_id                   service_fees.service_fee_id%TYPE;
    l_terminal_id                      service_fees.terminal_id%TYPE;
    l_fee_id                           service_fees.fee_id%TYPE;
    l_fee_amount                       service_fees.fee_amount%TYPE;
    l_months                           frequency.months%TYPE;
    l_days                             frequency.days%TYPE;
    l_last_payment                     service_fees.last_payment%TYPE;
    l_start_date                       service_fees.start_date%TYPE;
    l_end_date                         service_fees.end_date%TYPE;
BEGIN
    OPEN c_fee;

    LOOP
        FETCH c_fee INTO l_service_fee_id,
         l_terminal_id,
         l_fee_id,
         l_fee_amount,
         l_months,
         l_days,
         l_last_payment,
         l_start_date,
         l_end_date;
        EXIT WHEN c_fee%NOTFOUND;

        LOOP
            IF l_months > 0 THEN
                SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, l_months)), 'DD')
                  INTO l_last_payment
                  FROM DUAL;
            ELSE
                SELECT l_last_payment + l_days
                  INTO l_last_payment
                  FROM DUAL;
            END IF;

            EXIT WHEN l_last_payment > SYSDATE
                  OR l_last_payment > NVL (l_end_date, SYSDATE);

            IF cb_DEBUG_MODE = TRUE Then
            
                DBMS_OUTPUT.PUT_LINE('payment_service_fee: terminal_id = ''' || l_terminal_id ||
                                    ''', fee_id = ''' || l_fee_id ||
                                    ''', fee_amount = ''' || l_fee_amount ||
                                    ''', last_payment = ''' || l_last_payment ||
                                    ''', service_fee_id = ''' || l_service_fee_id ||
                                    ''', end_date = ''' || l_end_date || '''');
            END IF;
            
            BEGIN -- catch exception here
                -- skip creation of fee if start date is after fee date
                IF l_last_payment >= NVL(l_start_date, l_last_payment) THEN
                    INSERT INTO payment_service_fee
                                (payment_service_fee_id,
                                 terminal_id,
                                 fee_id,
                                 fee_amount,
                                 fee_date,
                                 service_fee_id)
                         VALUES (payment_service_fee_seq.NEXTVAL,
                                 l_terminal_id,
                                 l_fee_id,
                                 l_fee_amount,
                                 l_last_payment,
                                 l_service_fee_id);
                END IF;

                UPDATE service_fees
                   SET last_payment = l_last_payment
                 WHERE service_fee_id = l_service_fee_id;

                COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
            EXCEPTION
				 WHEN DUP_VAL_ON_INDEX THEN
				 	 ROLLBACK;
            END;
        END LOOP;
    END LOOP;

    CLOSE c_fee;
    
END scan_for_service_fees;
