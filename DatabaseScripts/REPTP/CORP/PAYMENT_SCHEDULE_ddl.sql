-- Start of DDL Script for Table CORP.PAYMENT_SCHEDULE
-- Generated 9/16/2004 9:44:40 AM from CORP@USADBD02

CREATE TABLE payment_schedule
    (payment_schedule_id            NUMBER NOT NULL,
    description                    VARCHAR2(100) NOT NULL,
    interval                       CHAR(2),
    months                         NUMBER,
    days                           NUMBER
  ,
  PRIMARY KEY (payment_schedule_id)
  USING INDEX)
/

-- Grants for Table
GRANT SELECT ON payment_schedule TO public
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_schedule TO public
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.PAYMENT_SCHEDULE

