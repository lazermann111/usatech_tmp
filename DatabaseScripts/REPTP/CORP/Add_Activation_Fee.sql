INSERT INTO report.frequency(FREQUENCY_ID,NAME,INTERVAL,MONTHS,DAYS)
VALUES(7,'One Time',' ',0,0);

SELECT * FROM report.frequency;
SELECT * FROM corp.frequency;

INSERT INTO corp.fees(FEE_ID,FEE_NAME,DESCRIPTION)
VALUES(10,'Activation Fee','A one-time fee for activating a device');

SELECT * FROM corp.fees;

COMMIT;
