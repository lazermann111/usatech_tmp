CREATE OR REPLACE PROCEDURE CORP.USAT_MANUAL_CLOSE_BATCH AS
--Manual delete open fill batches daily as per RFC-000038
    l_cutoff_date_1 DATE := SYSDATE - (2/24);
    CURSOR l_close_cur is
      SELECT B.BATCH_ID
        FROM CORP.BATCH B
        JOIN CORP.BATCH_FILL BF ON B.BATCH_ID = BF.BATCH_ID
        JOIN REPORT.FILL F ON BF.END_FILL_ID = F.FILL_ID
       WHERE B.BATCH_STATE_CD = 'O'
         AND B.END_DATE < (l_cutoff_date_1 + 1) /*accomodate timezone)*/
         AND F.CREATE_DATE < l_cutoff_date_1
         AND B.PAYMENT_SCHEDULE_ID = 2;
    l_cutoff_date_2 DATE := TRUNC(SYSDATE) - 7;
    CURSOR l_match_cur IS
       SELECT B.BATCH_ID, TO_CHAR(l_cutoff_date_2, 'YYYY/MM/DD') CUTOFF_DATE_TEXT
         FROM CORP.BATCH B
         JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
        WHERE B.BATCH_STATE_CD = 'U'
          AND B.PAYMENT_SCHEDULE_ID = 2
          AND T.CUSTOMER_ID IN(2974,9955)
          AND B.END_DATE < l_cutoff_date_2;
BEGIN
    FOR l_close_rec IN l_close_cur LOOP
        INSERT INTO CORP.MANUAL_CLOSE_BATCH(BATCH_ID)
         VALUES(l_close_rec.BATCH_ID);
        UPDATE CORP.BATCH B
           SET B.BATCH_STATE_CD = (
                SELECT DECODE(COUNT(*), 0, 'D', 'U')
                  FROM REPORT.TERMINAL t
                  JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                 WHERE b.TERMINAL_ID = t.TERMINAL_ID
                   AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
               B.BATCH_CLOSED_TS = SYSDATE
          WHERE B.BATCH_ID = l_close_rec.BATCH_ID
            AND B.BATCH_STATE_CD = 'O';
        COMMIT;
    END LOOP;
    FOR l_match_rec IN l_match_cur LOOP
        CORP.SP_CONFIRM_BATCH(l_match_rec.BATCH_ID, 3, l_match_rec.CUTOFF_DATE_TEXT);
    END LOOP;

    UPDATE USAT_CUSTOM.CCE_FILL F SET F.CCE_FILL_STATE_ID = 8
     WHERE F.CCE_FILL_STATE_ID IN(3,6) AND F.BATCH_ID IS NULL
       AND F.CCE_FILL_DATE < l_cutoff_date_2
       AND NOT EXISTS(
          SELECT 1
            FROM USAT_CUSTOM.FILE_TRNSFR_CCE_FILL CFFT
WHERE F.CCE_FILL_ID = CFFT.CCE_FILL_ID
             AND CFFT.FILE_TRNSFR_CCE_FILL_TYPE_ID = 2);
    COMMIT;
END;
 
