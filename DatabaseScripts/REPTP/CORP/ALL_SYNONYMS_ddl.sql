-- Start of DDL Script for Synonym CORP.CREATE_CUST_USER_ADMIN
-- Generated 13-Oct-2004 13:43:40 from CORP@USADBD02

CREATE SYNONYM create_cust_user_admin
  FOR report.create_cust_user_admin
/


-- End of DDL Script FOR report.Synonym CORP.CREATE_CUST_USER_ADMIN

-- Start of DDL Script FOR report.Synonym CORP.DATE_LIST
-- Generated 13-Oct-2004 13:43:40 from CORP@USADBD02

CREATE SYNONYM date_list
  FOR report.date_list
/


-- End of DDL Script FOR report.Synonym CORP.DATE_LIST

-- Start of DDL Script FOR report.Synonym CORP.DEVICE
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM device
  FOR report.eport
/


-- End of DDL Script FOR report.Synonym CORP.DEVICE

-- Start of DDL Script FOR report.Synonym CORP.DEVICE_TYPE
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM device_type
  FOR report.device_type
/


-- End of DDL Script FOR report.Synonym CORP.DEVICE_TYPE

-- Start of DDL Script FOR report.Synonym CORP.EPORT
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM eport
  FOR report.eport
/


-- End of DDL Script FOR report.Synonym CORP.EPORT

-- Start of DDL Script FOR report.Synonym CORP.FILL
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM fill
  FOR report.fill
/


-- End of DDL Script FOR report.Synonym CORP.FILL

-- Start of DDL Script FOR report.Synonym CORP.LOCATION
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM location
  FOR report.location
/


-- End of DDL Script FOR report.Synonym CORP.LOCATION

-- Start of DDL Script FOR report.Synonym CORP.MAX_DATE
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM max_date
  FOR report.max_date
/


-- End of DDL Script FOR report.Synonym CORP.MAX_DATE

-- Start of DDL Script FOR report.Synonym CORP.MIN_DATE
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM min_date
  FOR report.min_date
/


-- End of DDL Script FOR report.Synonym CORP.MIN_DATE

-- Start of DDL Script FOR report.Synonym CORP.SETTLE_STATE
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM settle_state
  FOR report.trans_state
/


-- End of DDL Script FOR report.Synonym CORP.SETTLE_STATE

-- Start of DDL Script FOR report.Synonym CORP.SETTLEMENT
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM settlement
  FOR report.settlement
/


-- End of DDL Script FOR report.Synonym CORP.SETTLEMENT

-- Start of DDL Script FOR report.Synonym CORP.TERMINAL
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM terminal
  FOR report.terminal
/


-- End of DDL Script FOR report.Synonym CORP.TERMINAL

-- Start of DDL Script FOR report.Synonym CORP.TERMINAL_EPORT
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM terminal_eport
  FOR report.terminal_eport
/


-- End of DDL Script FOR report.Synonym CORP.TERMINAL_EPORT

-- Start of DDL Script FOR report.Synonym CORP.TRANS
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM trans
  FOR report.trans
/


-- End of DDL Script FOR report.Synonym CORP.TRANS

-- Start of DDL Script FOR report.Synonym CORP.TRANS_TYPE
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM trans_type
  FOR report.trans_type
/


-- End of DDL Script FOR report.Synonym CORP.TRANS_TYPE

-- Start of DDL Script FOR report.Synonym CORP.USER_CUSTOMER_BANK
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM user_customer_bank
  FOR report.user_customer_bank
/


-- End of DDL Script FOR report.Synonym CORP.USER_CUSTOMER_BANK

-- Start of DDL Script FOR report.Synonym CORP.USER_LOGIN
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM user_login
  FOR report.user_login
/


-- End of DDL Script FOR report.Synonym CORP.USER_LOGIN

-- Start of DDL Script FOR report.Synonym CORP.VW_TERMINAL
-- Generated 13-Oct-2004 13:43:41 from CORP@USADBD02

CREATE SYNONYM vw_terminal
  FOR report.vw_terminal
/


-- End of DDL Script FOR report.Synonym CORP.VW_TERMINAL

