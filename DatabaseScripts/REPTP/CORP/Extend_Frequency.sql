ALTER TABLE corp.frequency ADD (DATE_FIELD VARCHAR2(30), AMOUNT NUMBER DEFAULT 1 NOT NULL, OFFSET_MS NUMBER DEFAULT 0 NOT NULL);
UPDATE corp.frequency set DATE_FIELD = 'WEEK', amount=1,offset_ms=0 where frequency_id =1;
UPDATE corp.frequency set DATE_FIELD = 'MONTH', amount=1,offset_ms=0 where frequency_id =2;
UPDATE corp.frequency set DATE_FIELD = 'HOUR', amount=1,offset_ms=0 where frequency_id =3;
UPDATE corp.frequency set DATE_FIELD = 'DAY', amount=1,offset_ms=0 where frequency_id =4;
UPDATE corp.frequency set DATE_FIELD = 'YEAR', amount=1,offset_ms=0 where frequency_id =5;
INSERT INTO corp.frequency
(FREQUENCY_ID,NAME,INTERVAL,MONTHS,DAYS,DATE_FIELD,AMOUNT,OFFSET_MS)
VALUES
(8,'Daily (5 am)','DD',0,0,'DAY',1,18000000)
;
COMMIT;

--update
SELECT f.* /*, 'UPDATE corp.frequency SET DATE_FIELD = ''' || date_field 
|| ''', amount='||amount||',offset_ms='||offset_ms|| ' where frequency_id ='||frequency_id|| ';'*/
FROM corp.frequency f
WHERE f.date_field IS NOT null;

--ALTER TABLE corp.frequency MODIFY (DATE_FIELD NOT NULL);

