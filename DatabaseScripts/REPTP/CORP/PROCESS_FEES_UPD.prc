CREATE OR REPLACE PROCEDURE PROCESS_FEES_UPD
   (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
    l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
    l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
    l_effective_dt IN PROCESS_FEES.END_DATE%TYPE )
IS
   l_effective_date DATE;
BEGIN
	 IF l_effective_dt IS NULL THEN
	     l_effective_date := SYSDATE;
	 ELSE
	 	 l_effective_date := l_effective_dt;
	 END IF;
	 --CHECK IF WE NEED already paid on transactions before after the effective date
	 IF l_effective_date < SYSDATE THEN
	 	 DECLARE
		     l_tran_id LEDGER.TRANS_ID%TYPE;
			BEGIN
		 	 SELECT MIN(TRANS_ID) INTO l_tran_id FROM LEDGER WHERE TERMINAL_ID = l_terminal_id
			     AND CLOSE_DATE > l_effective_date AND PAYMENT_ID <> 0;
			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
			 END IF;
		 END;
	 END IF;
	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, START_DATE)
 	    VALUES(PROCESS_FEE_SEQ.NEXTVAL,l_terminal_id,l_trans_type_id,l_fee_percent,l_effective_date);
END PROCESS_FEES_UPD;
