-- Start of DDL Script for Function CORP.CARD_COMPANY
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
FUNCTION card_company   (card_number VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    ch CHAR(1);
    cn VARCHAR2(50);
  BEGIN
    cn := TRIM(card_number);
    ch := SUBSTR(cn,1,1);
    IF ch = '4' AND LENGTH(cn) = 16 THEN
      RETURN 'VISA';
    ELSIF ch = '5' AND LENGTH(cn) = 16 THEN
      RETURN 'MasterCard';
    ELSIF ch = '3' AND LENGTH(cn) = 15 THEN
      RETURN 'American Express';
    ELSIF ch = '6' AND LENGTH(cn) = 16 THEN
      RETURN 'Discover';
    ELSE
      RETURN 'Unknown';
    END IF;
  END;
END;
/



-- End of DDL Script for Function CORP.CARD_COMPANY

-- Start of DDL Script for Function CORP.EQL
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
FUNCTION eql   (s1 VARCHAR2, s2 VARCHAR2)
RETURN NUMBER IS
BEGIN
  DECLARE
  	ret BOOLEAN;
  BEGIN
  	IF s1 IS NULL THEN
	   ret := s2 IS NULL;
	ELSIF s2 IS NULL THEN
	   ret := FALSE;
	ELSE
	   ret := ( s1 = s2);
	END IF;
	IF ret THEN
	   RETURN -1;
	ELSE
	   RETURN 0;
	END IF;
  END;
END;
/



-- End of DDL Script for Function CORP.EQL

-- Start of DDL Script for Function CORP.GET_ADJUST_DESC
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
FUNCTION get_adjust_desc(
    l_eft_id EFT.EFT_ID%TYPE)

RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(4000);
    reason PAYMENT_ADJUSTMENT.REASON%TYPE;
    amt PAYMENT_ADJUSTMENT.AMOUNT%TYPE;
    CURSOR cur IS
        SELECT REASON, AMOUNT FROM PAYMENT_ADJUSTMENT WHERE EFT_ID = l_eft_id;
  BEGIN
    OPEN cur;
    LOOP
      FETCH cur INTO reason, amt;
      EXIT WHEN cur%NOTFOUND;
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      str := str || reason || ' (' || TO_CHAR(amt, 'FM$999,999,999,990.00') || ')';
    END LOOP;
    RETURN str;
  END;
END;
/



-- End of DDL Script for Function CORP.GET_ADJUST_DESC

-- Start of DDL Script for Function CORP.LAST_TRAN_DATE
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
FUNCTION last_tran_date
    (l_doc_id INT, l_terminal_id int)
RETURN date

/* This function returns the last tran date 
   for a specific terminal_id for a given EFT 
   
   Tom Shannon   6/6/02
*/

IS
    last_tran_dt DATE;
BEGIN
    SELECT MAX(LEDGER_DATE)
      INTO last_tran_dt
      FROM LEDGER L, BATCH B
     WHERE B.TERMINAL_ID = L_TERMINAL_ID
       AND B.DOC_ID = L_DOC_ID
       AND L.BATCH_ID = B.BATCH_ID;
    RETURN last_tran_dt;
END;
/



-- End of DDL Script for Function CORP.LAST_TRAN_DATE

-- Start of DDL Script for Function CORP.LOG_DIR
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
FUNCTION log_dir           (logtype in varchar2) return varchar2 is
    return_value UTL_DATA.utldata%TYPE := NULL;
begin
  if logtype is null then
    --return default
      return 'No utlname value supplied';
  else
    --look for correct value
      --DECLARE
      BEGIN
      select UTL_DATA.utldata into return_value from corp.UTL_DATA where utlname = logtype;
        EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
            return_value := 'Not Found';
      END;
  end if;
  return return_value;
end;
/



-- End of DDL Script for Function CORP.LOG_DIR

-- Start of DDL Script for Function CORP.VEND_COLUMN_STRING
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
function vend_column_string
    (atran_id INT)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(255);
    vc PURCHASE.VEND_COLUMN%TYPE;
    qnt PURCHASE.PURCHASE_ID%TYPE;
    CURSOR cur IS
        SELECT VEND_COLUMN, COUNT(PURCHASE_ID) FROM PURCHASE WHERE TRANS_ID = atran_id GROUP BY vend_column;
        /*SELECT VEND_COLUMN, AMOUNT FROM PURCHASE WHERE TRAN_ID = atran_id;*/
  BEGIN
    OPEN cur;
    LOOP
      FETCH cur INTO vc, qnt;
      EXIT WHEN cur%NOTFOUND;
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      str := str || vc;
      IF qnt > 1 THEN
        str := str || '(' || /*TRIM(TO_CHAR(*/qnt/*, '9999'))*/ || ')';
      END IF;
    END LOOP;
    RETURN str;
  END;
END;
/



-- End of DDL Script for Function CORP.VEND_COLUMN_STRING

-- Start of DDL Script for Function CORP.WITHIN1
-- Generated 13-Oct-2004 13:42:31 from CORP@USADBD02

CREATE OR REPLACE 
FUNCTION within1(
	l_date IN DATE,
	l_start IN DATE,
	l_end IN DATE
	)
RETURN NUMBER PARALLEL_ENABLE DETERMINISTIC IS
BEGIN
	 IF l_start IS NOT NULL THEN
	 	IF l_date < l_start THEN
		   RETURN 0;
		END IF;
	 END IF;
	 IF l_end IS NOT NULL THEN
	 	IF l_date >= l_end THEN
		   RETURN 0;
	 	END IF;
	 END IF;
	 RETURN 1;
END;
/



-- End of DDL Script for Function CORP.WITHIN1

