CREATE OR REPLACE PROCEDURE CORP.SETUP_ROUTING_NUM_REFRESH(
    pn_last_modified NUMBER,
    pv_country_cd CORP.COUNTRY.COUNTRY_CD%TYPE,
    pc_proceed OUT VARCHAR2)
AS
    ln_prev_modified NUMBER;
BEGIN
    BEGIN
        SELECT TO_NUMBER_OR_NULL(APP_SETTING_VALUE)
          INTO ln_prev_modified
          FROM CORP.APP_SETTING
         WHERE APP_SETTING_CD = 'ROUTING_NUM_' || pv_country_cd ||'_LAST_MODIFIED'
          FOR UPDATE;
        IF ln_prev_modified >= pn_last_modified THEN
            pc_proceed := 'N';
        ELSE
            UPDATE CORP.APP_SETTING
               SET APP_SETTING_VALUE = TO_CHAR(pn_last_modified)
             WHERE APP_SETTING_CD = 'ROUTING_NUM_' || pv_country_cd ||'_LAST_MODIFIED';	
            pc_proceed := 'Y';
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO CORP.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
                VALUES('ROUTING_NUM_' || pv_country_cd ||'_LAST_MODIFIED', TO_CHAR(pn_last_modified), 'Last modified time of the routing number file for ' || pv_country_cd);
            SELECT TO_NUMBER_OR_NULL(APP_SETTING_VALUE)
              INTO ln_prev_modified
              FROM CORP.APP_SETTING
             WHERE APP_SETTING_CD = 'ROUTING_NUM_' || pv_country_cd ||'_LAST_MODIFIED'
               AND TO_NUMBER_OR_NULL(APP_SETTING_VALUE) = pn_last_modified
               FOR UPDATE;  
            pc_proceed := 'Y';
    END;                                
    IF pc_proceed = 'Y' THEN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE CORP.ROUTING_NUM_' || pv_country_cd || '_NEW';            
    END IF;
END;
/