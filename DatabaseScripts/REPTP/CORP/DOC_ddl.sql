-- Start of DDL Script for Table CORP.DOC
-- Generated 7-Oct-2004 9:26:10 from CORP@USADBD02

-- Drop the old instance of DOC
DROP TABLE doc
/

CREATE TABLE doc
    (doc_id                         NUMBER(*,0) NOT NULL,
    doc_type                       CHAR(2) NOT NULL,
    ref_nbr                        VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255) NOT NULL,
    customer_bank_id               NUMBER(*,0) NOT NULL,
    total_amount                   NUMBER(15,2),
    approve_by                     NUMBER(*,0),
    sent_by                        NUMBER(*,0),
    sent_date                      DATE,
    bank_acct_nbr                  VARCHAR2(20),
    bank_routing_nbr               VARCHAR2(20),
    status                         CHAR(1) DEFAULT 'L'   NOT NULL,
    create_by                      NUMBER(*,0),
    create_date                    DATE DEFAULT SYSDATE

  NOT NULL,
    update_date                    DATE
  ,
  PRIMARY KEY (doc_id)
  USING INDEX)
/




-- Indexes for DOC

CREATE UNIQUE INDEX uix_doc_ref_nbr ON doc
  (
    ref_nbr                         ASC
  )
/

CREATE INDEX doc_customer_bank_id ON doc
  (
    customer_bank_id                ASC
  )
/



-- Constraints for DOC






-- Comments for DOC

COMMENT ON COLUMN doc.status IS 'Locked (L),'
/

-- End of DDL Script for Table CORP.DOC

-- Foreign Key
ALTER TABLE doc
ADD CONSTRAINT fk_doc_approve_user FOREIGN KEY (approve_by)
REFERENCES user_login (user_id)
/
ALTER TABLE doc
ADD CONSTRAINT fk_doc_create_user FOREIGN KEY (create_by)
REFERENCES user_login (user_id)
/
ALTER TABLE doc
ADD CONSTRAINT fk_doc_sent_user FOREIGN KEY (sent_by)
REFERENCES user_login (user_id)
/
ALTER TABLE doc
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- End of DDL script for Foreign Key(s)
