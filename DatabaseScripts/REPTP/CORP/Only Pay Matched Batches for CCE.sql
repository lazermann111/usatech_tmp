GRANT SELECT ON USAT_CUSTOM.CCE_FILL TO CORP;

CREATE OR REPLACE VIEW CORP.VW_MATCHED_BATCH
AS 
SELECT B.BATCH_ID 
FROM CORP.BATCH B 
INNER JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = t.TERMINAL_ID
WHERE T.CUSTOMER_ID NOT IN(2974) OR B.PAYMENT_SCHEDULE_ID NOT IN(2)
UNION ALL SELECT BATCH_ID
FROM USAT_CUSTOM.CCE_FILL F
WHERE F.CCE_FILL_STATE_ID IN(4,5,8)
/

@@PAYMENTS_PKG.pbk;

