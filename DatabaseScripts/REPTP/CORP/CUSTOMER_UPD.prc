CREATE OR REPLACE PROCEDURE CORP.CUSTOMER_UPD 
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_cust_name IN CUSTOMER.CUSTOMER_NAME%TYPE,
   l_cust_alt_name IN CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
   l_addr_name IN CUSTOMER_ADDR.NAME%TYPE,
   l_addr1 IN CUSTOMER_ADDR.ADDRESS1%TYPE,
   l_addr2 IN CUSTOMER_ADDR.ADDRESS2%TYPE,
   l_city IN CUSTOMER_ADDR.CITY%TYPE,
   l_state IN CUSTOMER_ADDR.STATE%TYPE,
   l_zip IN CUSTOMER_ADDR.ZIP%TYPE,
   l_user_id IN USER_LOGIN.USER_ID%TYPE,
   pv_country_cd IN CUSTOMER_ADDR.COUNTRY_CD%TYPE DEFAULT NULL)
IS
   l_addr_id CUSTOMER_ADDR.ADDRESS_ID%TYPE;
   lv_country_cd CUSTOMER_ADDR.COUNTRY_CD%TYPE := pv_country_cd;
BEGIN
	IF lv_country_cd IS NULL THEN
		SELECT NVL(MAX(COUNTRY_CD), 'US')
		INTO lv_country_cd
		FROM CORP.STATE
		WHERE STATE_CD = l_state;
	END IF;

	 SELECT ADDRESS_ID INTO l_addr_id FROM VW_CUSTOMER WHERE CUSTOMER_ID = l_cust_id;
	 UPDATE CUSTOMER SET CUSTOMER_NAME = l_cust_name, CUSTOMER_ALT_NAME = l_cust_alt_name, UPD_BY = l_user_id, UPD_DATE = SYSDATE
		 WHERE CUSTOMER_ID = l_cust_id;
	 IF l_addr_id IS NULL THEN
	 	SELECT CUSTOMER_ADDR_SEQ.NEXTVAL INTO l_addr_id FROM DUAL;
	 	INSERT INTO CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, COUNTRY_CD)
	     	VALUES (l_addr_id, l_cust_id, 2, l_addr_name, l_addr1, l_addr2, l_city, l_state, l_zip, lv_country_cd);
	 ELSE
	 	 UPDATE CUSTOMER_ADDR SET NAME = l_addr_name, ADDRESS1 = l_addr1, ADDRESS2 = l_addr2, CITY = l_city, STATE = l_state, ZIP = l_zip, COUNTRY_CD = lv_country_cd
		 		WHERE ADDRESS_ID = l_addr_id;
	 END IF;
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Customer not found');
	 WHEN OTHERS THEN
	 	 RAISE;
END CUSTOMER_UPD;
/
