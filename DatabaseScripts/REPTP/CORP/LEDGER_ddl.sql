-- Start of DDL Script for Table CORP.LEDGER
-- Generated 10/7/2004 1:37:31 PM from CORP@USADBD02

-- Drop the old instance of LEDGER
DROP TABLE ledger
/

CREATE TABLE ledger
    (ledger_id                      NUMBER(*,0) NOT NULL,
    entry_type                     CHAR(2) NOT NULL,
    trans_id                       NUMBER(*,0),
    process_fee_id                 NUMBER(*,0),
    service_fee_id                 NUMBER(*,0),
    amount                         NUMBER(15,2),
    batch_id                       NUMBER(*,0) NOT NULL,
    settle_state_id                NUMBER(*,0) NOT NULL,
    ledger_date                    DATE NOT NULL,
    description                    VARCHAR2(400),
    create_by                      NUMBER,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    deleted                        CHAR(1) DEFAULT 'N'  NOT NULL
  ,
  PRIMARY KEY (ledger_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON ledger TO report
WITH GRANT OPTION
/
GRANT INSERT ON ledger TO report
WITH GRANT OPTION
/
GRANT SELECT ON ledger TO report
WITH GRANT OPTION
/
GRANT UPDATE ON ledger TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON ledger TO report
WITH GRANT OPTION
/



-- Indexes for LEDGER

CREATE BITMAP INDEX bix_ledger_entry_type ON ledger
  (
    entry_type                      ASC
  )
/

CREATE INDEX ledger_trans_id ON ledger
  (
    trans_id                        ASC
  )
/

CREATE INDEX ledger_settle_state_id ON ledger
  (
    settle_state_id                 ASC
  )
/

CREATE INDEX ledger_service_fee_id ON ledger
  (
    service_fee_id                  ASC
  )
/

CREATE INDEX ledger_process_fee_id ON ledger
  (
    process_fee_id                  ASC
  )
/

CREATE INDEX ledger_batch_id ON ledger
  (
    batch_id                        ASC
  )
/



-- Constraints for LEDGER








-- Comments for LEDGER

COMMENT ON COLUMN ledger.deleted IS 'Is this record deleted (''Y'' or ''N'')'
/

-- End of DDL Script for Table CORP.LEDGER

-- Foreign Key
ALTER TABLE ledger
ADD CONSTRAINT fk_ledger_user FOREIGN KEY (create_by)
REFERENCES user_login (user_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (trans_id)
REFERENCES trans (tran_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (settle_state_id)
REFERENCES trans_state (state_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (service_fee_id)
REFERENCES service_fees (service_fee_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (process_fee_id)
REFERENCES process_fees (process_fee_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (batch_id)
REFERENCES batch (batch_id)
/
-- End of DDL script for Foreign Key(s)
