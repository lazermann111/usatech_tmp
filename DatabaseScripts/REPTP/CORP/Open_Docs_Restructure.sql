/*--Move Customer bank id to doc
select * from corp.batch b, corp.doc d
where b.doc_id = d.doc_id
and b.customer_bank_id <> d.customer_bank_id;
-- in prod: 0 rows (GOOD!)

select * from corp.doc;
*/
-- Already done (remove create_by non-null constraint)
--ALTER TABLE CORP.DOC MODIFY COLUMN CREATE_BY NULL;

INSERT INTO  corp.payment_schedule(payment_schedule_id, description, selectable, offset_hours)
VALUES(7, 'Payment Adjustments', 'N', 0);
COMMIT;

-- add ledger.paid_date
ALTER TABLE CORP.LEDGER ADD (PAID_DATE DATE NULL);

-- compile package
@@PAYMENTS_PKG.psk
@@PAYMENTS_PKG.pbk

--drop trigger
DROP TRIGGER CORP.TRBIU_BATCH;

-- add all open batches to a document
UPDATE CORP.BATCH B 
   SET DOC_ID = CORP.PAYMENTS_PKG.GET_OR_CREATE_DOC(B.CUSTOMER_BANK_ID, B.CURRENCY_ID)
 WHERE B.CLOSED = 'N';
COMMIT;

-- drop batch.customer_bank_id and batch.currency_id
DROP INDEX SYSTEM.BIX_BATCH_CLOSED;
ALTER TABLE CORP.BATCH DROP COLUMN CUSTOMER_BANK_ID;
ALTER TABLE CORP.BATCH DROP COLUMN CURRENCY_ID;
ALTER TABLE CORP.BATCH DROP COLUMN CLOSED;

-- update paid_date
UPDATE CORP.LEDGER L
   SET PAID_DATE = (
    SELECT D.SENT_DATE
      FROM CORP.DOC D, CORP.BATCH B
     WHERE D.DOC_ID = B.DOC_ID
       AND B.BATCH_ID = L.BATCH_ID
       AND D.STATUS IN('S', 'P'))
 WHERE L.BATCH_ID IN(
    SELECT B.BATCH_ID
      FROM CORP.DOC D, CORP.BATCH B
     WHERE D.DOC_ID = B.DOC_ID
       AND B.BATCH_ID = L.BATCH_ID
       AND D.STATUS IN('S', 'P'));
COMMIT;

ALTER TABLE CORP.DOC 
 MODIFY (  STATUS DEFAULT 'O' );

COMMENT ON COLUMN CORP.DOC.STATUS IS 'Open (O), Locked (L), Approved (A), Paid (P), Sent (S)';

@@../CORP/VW_ADJUSTMENTS.vws
@@../CORP/VW_PENDING_REVENUE.vws
@@../CORP/VW_PENDING_OR_LOCKED_PAYMENTS.vws
@@../CORP/VW_PENDING_SERVICE_FEES.vws
@@../CORP/VW_PENDING_ADJUSTMENTS.vws
@@../CORP/VW_BATCH_SUMMARY.vws
@@../CORP/VW_EFT_EXPORT.vws
@@../CORP/VW_MACGRAY_EFT_DETAIL.vws
@@../CORP/VW_PAYMENT_PROCESS_FEES.vws
@@../CORP/VW_PAYMENT_REPORT.vws
@@../CORP/VW_PAYMENT_SERVICE_FEES.vws --*
@@../CORP/VW_UNPAID_TRANS_AND_FEES.vws
@@../CORP/VW_PAID_DATE.vws
@@../REPORT/TERMINAL_EPORT_UPD.prc

CREATE UNIQUE INDEX CORP.UIX_LEDGER_BATCH_COMBO2 ON CORP.LEDGER
  (
    BATCH_ID                        ASC,
    DELETED                         ASC,
    ENTRY_TYPE                      ASC,
    SETTLE_STATE_ID                 ASC,
    AMOUNT                          ASC,
    SERVICE_FEE_ID                  ASC,
    LEDGER_ID                       ASC
  )
  TABLESPACE  CORP_INDX
COMPRESS  6
;
ANALYZE INDEX CORP.UIX_LEDGER_BATCH_COMBO2 ESTIMATE STATISTICS SAMPLE 30 PERCENT;

CREATE UNIQUE INDEX CORP.UIX_BATCH_COMBO ON CORP.BATCH
  (
    DOC_ID                          ASC,
    PAYMENT_SCHEDULE_ID             ASC,
    END_DATE                        ASC,
    BATCH_ID                        ASC
  )
  TABLESPACE  CORP_INDX
COMPRESS  3
;
ANALYZE INDEX CORP.UIX_BATCH_COMBO ESTIMATE STATISTICS SAMPLE 30 PERCENT;

