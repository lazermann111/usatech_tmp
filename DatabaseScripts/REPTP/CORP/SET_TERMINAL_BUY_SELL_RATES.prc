CREATE OR REPLACE PROCEDURE CORP.SET_TERMINAL_BUY_SELL_RATES (
  pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
  pn_commission_bank_id IN CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
  pn_pf_br_cp_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_br_cp_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_br_cp_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_pf_br_me_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_br_me_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_br_me_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_pf_sr_cp_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_sr_cp_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_sr_cp_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_pf_sr_me_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_sr_me_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_sr_me_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_sf_br_mm_f IN CORP.SERVICE_FEES.FEE_AMOUNT%TYPE,
  pn_sf_sr_mm_f IN CORP.SERVICE_FEES.FEE_AMOUNT%TYPE,
  pn_sf_frequency_id IN CORP.SERVICE_FEES.FREQUENCY_ID%TYPE DEFAULT NULL)
IS
  -- pn = param numeric, ln = local numeric, cln = constant local numeric
  -- pf = process fee, sf = service fee
  -- br = buy rate, sr = sell rate
  -- cp = card present, me = manual entry
  -- p = percent, f = flat, m = minimum
  -- mm = monthly
  -- See com.usatech.layers.common.fees.Fees for correspond Java object
  cln_pf_cp_tran_type_id CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 16;
  cln_pf_me_tran_type_id CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 13;
  cln_sf_fee_id CORP.SERVICE_FEES.FEE_ID%TYPE := 1;
  ld_effective_date DATE := sysdate;
  ln_commission_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE := pn_commission_bank_id;
  ln_sf_frequency_id CORP.SERVICE_FEES.FREQUENCY_ID%TYPE := pn_sf_frequency_id;
BEGIN
  IF ln_commission_bank_id IS NULL THEN
     select max(customer_bank_id) 
     into ln_commission_bank_id
     from (
       select sfc.customer_bank_id
       from corp.service_fees sf
       left outer join corp.service_fee_commission sfc on sf.service_fee_id = sfc.service_fee_id
       where sf.terminal_id = pn_terminal_id
       and sf.fee_id = cln_sf_fee_id
       order by nvl(end_date, MAX_DATE) desc
     ) where rownum = 1;
     IF ln_commission_bank_id IS NULL THEN
       RAISE_APPLICATION_ERROR(-20001, 'commission_bank_id is required');
     END IF;
  END IF;
  IF ln_sf_frequency_id IS NULL THEN
    select max(frequency_id) 
    into ln_sf_frequency_id
    from (
      select sf.frequency_id
      from corp.service_fees sf
      where sf.terminal_id = pn_terminal_id
      and sf.fee_id = cln_sf_fee_id
      order by nvl(end_date, MAX_DATE) desc
    ) where rownum = 1;
    IF ln_sf_frequency_id IS NULL THEN
      ln_sf_frequency_id := 2;
    END IF;
  END IF;
  -- pass the sell rate to the proc and calc the commission using (sell rate - buy rate)
  CORP.PAYMENTS_PKG.PROCESS_FEES_UPD(pn_terminal_id, cln_pf_cp_tran_type_id, pn_pf_sr_cp_p, pn_pf_sr_cp_f, pn_pf_sr_cp_m, ld_effective_date, 'N', (pn_pf_sr_cp_p - pn_pf_br_cp_p), (pn_pf_sr_cp_f - pn_pf_br_cp_f), (pn_pf_sr_cp_m - pn_pf_br_cp_m), ln_commission_bank_id);
  CORP.PAYMENTS_PKG.PROCESS_FEES_UPD(pn_terminal_id, cln_pf_me_tran_type_id, pn_pf_sr_me_p, pn_pf_sr_me_f, pn_pf_sr_me_m, ld_effective_date, 'N', (pn_pf_sr_me_p - pn_pf_br_me_p), (pn_pf_sr_me_f - pn_pf_br_me_f), (pn_pf_sr_me_m - pn_pf_br_me_m), ln_commission_bank_id);
  CORP.PAYMENTS_PKG.SERVICE_FEES_UPD(pn_terminal_id, cln_sf_fee_id, ln_sf_frequency_id, pn_sf_sr_mm_f, 0, ld_effective_date, null, 'N', 60, null, null, null, (pn_sf_sr_mm_f - pn_sf_br_mm_f), ln_commission_bank_id);
END;
/
