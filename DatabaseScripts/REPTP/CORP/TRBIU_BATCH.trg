CREATE OR REPLACE TRIGGER CORP.TRBIU_BATCH
BEFORE INSERT OR UPDATE
OF PAID_DATE
ON CORP.BATCH
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.PAID_DATE IS NULL THEN
		:NEW.PARTITION_TS := DBADMIN.EPOCH_DATE;
	ELSE
		:NEW.PARTITION_TS := :NEW.PAID_DATE;
		
		INSERT INTO CORP.BATCH_TOTAL_HIST
		SELECT * FROM CORP.BATCH_TOTAL
		WHERE BATCH_ID = :NEW.BATCH_ID;
		
		DELETE FROM CORP.BATCH_TOTAL
		WHERE BATCH_ID = :NEW.BATCH_ID;
	END IF;
END;
/