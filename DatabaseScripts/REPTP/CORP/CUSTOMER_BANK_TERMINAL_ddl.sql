-- Start of DDL Script for Table CORP.CUSTOMER_BANK_TERMINAL
-- Generated 7-Oct-2004 9:25:23 from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_TERMINAL
DROP TABLE customer_bank_terminal
/

CREATE TABLE customer_bank_terminal
    (terminal_id                    NUMBER NOT NULL,
    customer_bank_id               NUMBER NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    customer_bank_terminal_id      NUMBER NOT NULL
  ,
  CONSTRAINT PK_CUSTOMER_BANK_TERMINAL
  PRIMARY KEY (customer_bank_terminal_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_bank_terminal TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_BANK_TERMINAL

CREATE UNIQUE INDEX uix_cbt_combo ON customer_bank_terminal
  (
    customer_bank_id                ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC
  )
/

CREATE INDEX ix_cbt_customer_bank_id ON customer_bank_terminal
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_cbt_end_date ON customer_bank_terminal
  (
    end_date                        ASC
  )
/

CREATE INDEX ix_cbt_start_date ON customer_bank_terminal
  (
    start_date                      ASC
  )
/

CREATE INDEX ix_cbt_terminal_id ON customer_bank_terminal
  (
    terminal_id                     ASC
  )
/



-- Constraints for CUSTOMER_BANK_TERMINAL



-- Triggers for CUSTOMER_BANK_TERMINAL

CREATE OR REPLACE TRIGGER traiud_customer_bank_terminal
 AFTER
  INSERT OR DELETE OR UPDATE
 ON customer_bank_terminal
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
            OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE)) -- this should always be null, but to be safe...
             WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
               AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE);
        END IF;
    END IF;
END;
/


-- End of DDL Script for Table CORP.CUSTOMER_BANK_TERMINAL

-- Foreign Key
ALTER TABLE customer_bank_terminal
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- End of DDL script for Foreign Key(s)
