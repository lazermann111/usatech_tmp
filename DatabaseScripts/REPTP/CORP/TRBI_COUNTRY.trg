CREATE OR REPLACE TRIGGER corp.trbi_country
BEFORE INSERT ON corp.country
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT UPPER(:NEW.COUNTRY_CD),
           SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :NEW.COUNTRY_CD,
           :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY 
      FROM DUAL;
END;
/