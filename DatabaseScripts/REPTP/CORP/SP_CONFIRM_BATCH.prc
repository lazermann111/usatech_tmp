CREATE OR REPLACE PROCEDURE CORP.SP_CONFIRM_BATCH (
    pn_batch_id CORP.BATCH.BATCH_ID%TYPE,
    pn_batch_confirm_type_id CORP.BATCH.BATCH_CONFIRM_TYPE_ID%TYPE,
    pv_batch_confirm_key CORP.BATCH.BATCH_CONFIRM_KEY%TYPE    
) IS
/******************************************************************************
   NAME:       CONFIRM_BATCH
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/23/2008          1. Created this procedure.

   NOTES:
        Sets a batch as confirmed by an external process

******************************************************************************/
BEGIN
   UPDATE CORP.BATCH B
      SET BATCH_STATE_CD = 'C',
          BATCH_CONFIRM_TS = SYSDATE,
          BATCH_CONFIRM_TYPE_ID = pn_batch_confirm_type_id,
          BATCH_CONFIRM_KEY = pv_batch_confirm_key
    WHERE BATCH_STATE_CD IN('D', 'U')
      AND BATCH_ID = pn_batch_id;
END;
/
