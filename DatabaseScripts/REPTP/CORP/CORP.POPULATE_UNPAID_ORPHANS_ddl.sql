-- Start of DDL Script for Procedure CORP.POPULATE_UNPAID_ORPHANS

CREATE OR REPLACE 
PROCEDURE corp.populate_unpaid_orphans
   (inDate IN DATE)
   IS
--
-- Purpose: This will populate the unpaid_monies_rpt table with
--          with orphan data for a given date
--
-- MODIFICATION HISTORY
-- Person      Date        Comments
-- ---------   ---------   ------------------------------------------
-- J Bradley   15-MAR-05   Inital version
-- J Bradley   16-MAR-05   Added grouping for orphan data by device type 

    tDate DATE;
    orpahnCustBankId corp.customer_bank.customer_bank_id%TYPE;

BEGIN
    -- ensure tDate is truncated to a day (i.e. time = 00:00:00)
    tDate := TRUNC(inDate);
	
	/* truncate the scratch table */
	EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
	
	-- loop through each orphan terminal where terminal_id != 0 in trans
	/* this is faster than doing it through standard SQL */
	FOR tot IN (
	    SELECT DISTINCT tr.terminal_id orph_ter, tr.eport_id eprt_id
        FROM trans tr, terminal ter
        WHERE tr.terminal_id = ter.terminal_id (+)
        AND ter.terminal_id IS NULL
        AND tr.terminal_id != 0
	)		
    LOOP
        
        /* get the orphanCustBankId for the Orphan Transactions customer */
        SELECT cbk.customer_bank_id
        INTO orphanCustBankId
        FROM customer_bank cbk, orphan_cust_dev ocd, eport ept 
        WHERE cbk.customer_id = ocd.customer_id
        AND ocd.device_type_id = ept.device_type_id
        AND ept.eport_id = tot.eprt_id
        AND ROWNUM = 1;
        
        
		-- Insert credit_amount and process_fee_amount
        INSERT INTO scr_unpaid_monies
		SELECT orphanCustBankId customer_bank_id,
		SUM(tr.total_amount) credit_amount,
		0, 0,
		(SUM(tr.total_amount) * -0.05) process_fee_amount,
		0, 0
		FROM trans tr
		WHERE tr.terminal_id = tot.orph_ter
		AND tr.trans_type_id = 16
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
		-- Insert refund_amount
		INSERT INTO scr_unpaid_monies
        SELECT orphanCustBankId customer_bank_id,
		0,
		SUM(tr.total_amount) refund_amount,
		0, 0, 0, 0
		FROM trans tr
		WHERE tr.terminal_id = tot.orph_ter
		AND tr.trans_type_id = 20
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
		-- Insert chargeback_amount
		INSERT INTO scr_unpaid_monies
        SELECT orphanCustBankId customer_bank_id,
        0, 0,
		SUM(tr.total_amount) chargeback_amount,
		0, 0, 0
		FROM trans tr
		WHERE tr.terminal_id = tot.orph_ter
		AND tr.trans_type_id = 21
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;	
		
    END LOOP;
    
    
    /* get the rest of the trans terminal orphans (terminal_id = 0) */
    
    -- Insert credit_amount and process_fee_amount      
    INSERT INTO scr_unpaid_monies
	SELECT cbk.customer_bank_id,
    SUM(tr.total_amount) credit_amount,
    0, 0,
    (SUM(tr.total_amount) * -0.05) process_fee_amount,
    0, 0
    FROM trans tr, eport ept, orphan_cust_dev ocd, customer_bank cbk
    WHERE tr.eport_id = ept.eport_id
    AND ept.device_type_id = ocd.device_type_id
    AND ocd.customer_id = cbk.customer_id
    AND tr.terminal_id = 0
    AND tr.trans_type_id = 16
    AND tr.status != 'D'
    AND tr.status != 'E'
    AND tr.server_date < tDate
    GROUP BY cbk.customer_bank_id;
	
	-- Insert refund_amount
	INSERT INTO scr_unpaid_monies
	SELECT cbk.customer_bank_id,
	0,
	SUM(tr.total_amount) refund_amount,
	0, 0, 0, 0
	FROM trans tr, eport ept, orphan_cust_dev ocd, customer_bank cbk
    WHERE tr.eport_id = ept.eport_id
    AND ept.device_type_id = ocd.device_type_id
    AND ocd.customer_id = cbk.customer_id
    AND tr.terminal_id = 0
    AND tr.trans_type_id = 20
    AND tr.status != 'D'
    AND tr.status != 'E'
    AND tr.server_date < tDate
    GROUP BY cbk.customer_bank_id;
	
	-- Insert chargeback_amount
	INSERT INTO scr_unpaid_monies
	SELECT cbk.customer_bank_id,
    0, 0,
	SUM(tr.total_amount) chargeback_amount,
	0, 0, 0
	FROM trans tr, eport ept, orphan_cust_dev ocd, customer_bank cbk
    WHERE tr.eport_id = ept.eport_id
    AND ept.device_type_id = ocd.device_type_id
    AND ocd.customer_id = cbk.customer_id
    AND tr.terminal_id = 0
    AND tr.trans_type_id = 21
    AND tr.status != 'D'
    AND tr.status != 'E'
    AND tr.server_date < tDate
    GROUP BY cbk.customer_bank_id;
    
    
    /* get the orphanCustBankId for the generic Orphan Transactions customer (for catch all) */
    SELECT cbk.customer_bank_id
    INTO orphanCustBankId
    FROM customer cus, customer_bank cbk
    WHERE cbk.customer_id = cus.customer_id
    AND cus.customer_name = '~~ Unknown Device Orphan Transactions'
    AND ROWNUM = 1;
    
    
    /* check for any eports without an orphan customer device link (the catch all) */
    
    FOR orph_ept IN (
        SELECT ept.eport_id eprt_id
        FROM eport ept, orphan_cust_dev ocd
        WHERE ept.device_type_id = ocd.device_type_id (+)
        AND ocd.device_type_id IS NULL
	)		
    LOOP
        
        /* check for and insert any orphans that are found for these eports */
        
        -- Insert credit_amount and process_fee_amount
        INSERT INTO scr_unpaid_monies
		SELECT orphanCustBankId customer_bank_id,
		SUM(tr.total_amount) credit_amount,
		0, 0,
		(SUM(tr.total_amount) * -0.05) process_fee_amount,
		0, 0
		FROM trans tr, terminal ter
		WHERE tr.eport_id = orph_ept.eprt_id
        AND tr.terminal_id = ter.terminal_id (+)
        AND ter.terminal_id IS NULL
		AND tr.trans_type_id = 16
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
		-- Insert refund_amount
		INSERT INTO scr_unpaid_monies
        SELECT orphanCustBankId customer_bank_id,
		0,
		SUM(tr.total_amount) refund_amount,
		0, 0, 0, 0
		FROM trans tr, terminal ter
		WHERE tr.eport_id = orph_ept.eprt_id
        AND tr.terminal_id = ter.terminal_id (+)
        AND ter.terminal_id IS NULL
		AND tr.trans_type_id = 20
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
		-- Insert chargeback_amount
		INSERT INTO scr_unpaid_monies
        SELECT orphanCustBankId customer_bank_id,
        0, 0,
		SUM(tr.total_amount) chargeback_amount,
		0, 0, 0
		FROM trans tr, terminal ter
		WHERE tr.eport_id = orph_ept.eprt_id
        AND tr.terminal_id = ter.terminal_id (+)
        AND ter.terminal_id IS NULL
		AND tr.trans_type_id = 21
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
    END LOOP;
	
	
	-- Insert zeros for orphan transaction customers without unpaid monies for date
    INSERT INTO scr_unpaid_monies
	SELECT cbk.customer_bank_id,
	0 credit_amount,
	0 refund_amount,
	0 chargeback_amount,
	0 process_fee_amount,
	0 service_fee_amount,
	0 adjust_amount
	FROM scr_unpaid_monies tum, customer_bank cbk
	WHERE cbk.customer_bank_id = tum.customer_bank_id (+)
	AND tum.customer_bank_id IS NULL
	  -- only include Orphan Transaction customers
    AND cbk.customer_bank_id IN (
		SELECT cbk.customer_bank_id
	    FROM customer cus, customer_bank cbk
	    WHERE cbk.customer_id = cus.customer_id
	    AND cus.customer_name LIKE '%Orphan Transactions'
	);
	
	
	-- Insert the the calulated data into the UNPAID_MONIES_RPT table
	INSERT INTO unpaid_monies_rpt (
        customer_bank_id,
        server_date,
        credit_amount,
        refund_amount,
        chargeback_amount,
        process_fee_amount,
        service_fee_amount,
        adjust_amount,
        net_payment_amount
    )		
	SELECT tum.customer_bank_id,
    tDate server_date,
    SUM(tum.credit_amount) credit_amount,
    SUM(tum.refund_amount) refund_amount,
    SUM(tum.chargeback_amount) chargeback_amount,
    SUM(tum.process_fee_amount) process_fee_amount,
    SUM(tum.service_fee_amount) service_fee_amount,
    SUM(tum.adjust_amount) adjust_amount,
    (
    	(SUM(tum.credit_amount) + SUM(tum.refund_amount)
    	+ SUM(tum.chargeback_amount) + SUM(tum.process_fee_amount)
    	+ SUM(tum.service_fee_amount) + SUM(tum.adjust_amount))
    ) net_payment_amount
    FROM scr_unpaid_monies tum
    GROUP BY tum.customer_bank_id;
    
    -- truncate the scratch table 
    EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
    
END; -- Procedure
/



-- End of DDL Script for Procedure CORP.POPULATE_UNPAID_ORPHANS

