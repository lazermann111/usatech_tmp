drop trigger corp.trbiu_doc;
drop trigger corp.trbiu_batch;
drop trigger corp.trbiu_ledger;

ALTER TABLE CORP.BATCH
DROP (PAID_DATE, PARTITION_TS);

ALTER TABLE CORP.DOC
DROP COLUMN PARTITION_TS;

ALTER TABLE CORP.LEDGER
DROP COLUMN PARTITION_TS;