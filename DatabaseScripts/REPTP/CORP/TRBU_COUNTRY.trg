CREATE OR REPLACE TRIGGER corp.trbu_country
BEFORE UPDATE ON corp.country
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT UPPER(:NEW.COUNTRY_CD),
           :OLD.CREATED_BY,
           :OLD.CREATED_TS,
           SYSDATE,
           USER
      INTO :NEW.COUNTRY_CD,
           :NEW.CREATED_BY,
           :NEW.CREATED_TS,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/