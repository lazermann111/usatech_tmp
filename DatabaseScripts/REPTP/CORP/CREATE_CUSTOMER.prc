CREATE OR REPLACE PROCEDURE CORP.CREATE_CUSTOMER 
   (l_user_id OUT CUSTOMER.USER_ID%TYPE,
   l_cust_id OUT CUSTOMER.CUSTOMER_ID%TYPE,
   l_user_name IN VARCHAR,
   l_first IN VARCHAR,
   l_last IN VARCHAR,
   l_email IN VARCHAR,
   l_pwd IN VARCHAR,
   l_cust_name IN CUSTOMER.CUSTOMER_NAME%TYPE,
   l_addr1 IN CUSTOMER_ADDR.ADDRESS1%TYPE,
   l_city IN CUSTOMER_ADDR.CITY%TYPE,
   l_state IN CUSTOMER_ADDR.STATE%TYPE,
   l_zip IN CUSTOMER_ADDR.ZIP%TYPE,
   l_telephone IN VARCHAR,
   l_fax IN VARCHAR,
   l_dealer_id IN CUSTOMER.DEALER_ID%TYPE,
   l_tax_id_nbr IN CUSTOMER.TAX_ID_NBR%TYPE)
IS
   l_addr_id CUSTOMER_ADDR.ADDRESS_ID%TYPE;
   l_lic_nbr LICENSE_NBR.LICENSE_NBR%TYPE;
   l_lic_id LICENSE_NBR.LICENSE_ID%TYPE;
BEGIN
	 SELECT LICENSE_ID INTO l_lic_id FROM VW_DEALER_LICENSE WHERE DEALER_ID = l_dealer_id;
	 SELECT CUSTOMER_SEQ.NEXTVAL, CUSTOMER_ADDR_SEQ.NEXTVAL INTO l_cust_id, l_addr_id FROM DUAL;
	 CREATE_CUST_USER_ADMIN(l_user_id,l_user_name,l_first,l_last,l_email,l_cust_id,l_pwd,l_telephone,l_fax);
	 INSERT INTO CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, USER_ID, CREATE_BY, DEALER_ID, TAX_ID_NBR)
	     VALUES(l_cust_id, l_cust_name, l_user_id, l_user_id, l_dealer_id, l_tax_id_nbr);
	 INSERT INTO CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, CITY, STATE, ZIP)
	     VALUES (l_addr_id, l_cust_id, 2, l_first || ' ' || l_last, l_addr1, l_city, l_state, l_zip);
	 CREATE_LICENSE(l_lic_id, l_cust_id, l_lic_nbr);
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find license agreement for this dealer');
	 WHEN OTHERS THEN
	 	 RAISE;
END CREATE_CUSTOMER;
/
