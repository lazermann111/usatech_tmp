CREATE OR REPLACE FUNCTION CORP.GET_SERVICE_FEE_DATE 
    (pn_customer_id IN NUMBER, pd_month_date DATE)
RETURN date
IS
    service_fee_date DATE;
    l_fee_schedule_cd VARCHAR(50);
BEGIN
    SELECT MAX(fee_schedule_cd)
      INTO l_fee_schedule_cd
      FROM CORP.CUSTOMER C JOIN CORP.FEE_SCHEDULE FS on C.FEE_SCHEDULE_ID=FS.FEE_SCHEDULE_ID
      WHERE CUSTOMER_ID=pn_customer_id;
      
    IF l_fee_schedule_cd = 'EOM' THEN
    	select TRUNC(LAST_DAY(pd_month_date), 'DD') INTO service_fee_date FROM DUAL;
   	ELSE
   		select TRUNC(pd_month_date, 'MM')+to_number(l_fee_schedule_cd)-1 INTO service_fee_date FROM DUAL;
   	END IF;
    RETURN service_fee_date;
END;
/
