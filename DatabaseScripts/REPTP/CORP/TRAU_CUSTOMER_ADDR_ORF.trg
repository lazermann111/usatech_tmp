CREATE OR REPLACE TRIGGER corp.trau_customer_addr_orf
AFTER UPDATE 
OF
    zip
  , city
  , state
  , address1
  , address2
  , customer_id
ON corp.customer_addr
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
     REPORT.SYNC_PKG.SYNC_ORF_VENDOR(:NEW.CUSTOMER_ID);
END;
/