create or replace TRIGGER CORP.TRBI_SERVICE_FEES BEFORE INSERT ON CORP.SERVICE_FEES
  FOR EACH ROW
DECLARE
	l_fee_schedule_id NUMBER;
  l_customer_id NUMBER;
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
    IF :NEW.FREQUENCY_ID = 4 OR :NEW.FREQUENCY_ID = 2 THEN
    	IF :NEW.FREQUENCY_ID = 2 THEN
    			select c.fee_schedule_id, c.customer_id into  l_fee_schedule_id, l_customer_id from REPORT.TERMINAL t join corp.customer c on t.customer_id=c.customer_id 
    			where terminal_id=:NEW.TERMINAL_ID;
    	END IF;
    	IF :NEW.START_DATE IS NOT NULL THEN
    		IF NOT(:NEW.FREQUENCY_ID = 2 AND l_fee_schedule_id = 2) THEN
    			--This is temporary to restart daily fees when they are turned on by job
				  IF :NEW.AUTO_TURNED_ON_DATE IS NULL THEN
    				:NEW.START_DATE:=TRUNC(LAST_DAY(:NEW.START_DATE), 'DD');
    			END IF;
			ELSE
				:NEW.START_DATE:= CORP.GET_SERVICE_FEE_DATE(l_customer_id,:NEW.START_DATE); 
     		END IF;
    	END IF;

    	IF :NEW.END_DATE IS NOT NULL THEN
    		IF :NEW.FREQUENCY_ID = 2 THEN
    			:NEW.END_DATE:=CORP.GET_SERVICE_FEE_DATE(l_customer_id,:NEW.END_DATE);
    		ELSE
    			:NEW.END_DATE:=TRUNC(LAST_DAY(:NEW.END_DATE), 'DD');
    		END IF;
    	END IF;
    END IF;
END; 
/