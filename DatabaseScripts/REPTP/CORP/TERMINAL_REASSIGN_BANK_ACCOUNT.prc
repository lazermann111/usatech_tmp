CREATE OR REPLACE PROCEDURE CORP.TERMINAL_REASSIGN_BANK_ACCOUNT 
   (l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
    l_cust_bank_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_ID%TYPE,
	l_effective_date IN DATE)
IS
BEGIN
    -- Update all Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(END_DATE, l_effective_date) >= l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
    -- Insert new mapping if customer bank is not zero
    IF l_cust_bank_id <> 0 THEN
		INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
		    VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_effective_date);
    END IF;
    
    -- Clean up dead dates
    --DELETE FROM CUSTOMER_BANK_TERMINAL WHERE START_DATE >= END_DATE;
    
END TERMINAL_REASSIGN_BANK_ACCOUNT;
/
