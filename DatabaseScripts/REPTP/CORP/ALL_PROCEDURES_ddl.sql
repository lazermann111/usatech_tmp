-- Start of DDL Script for Procedure CORP.ACCEPT_CUSTOMER
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE accept_customer
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   	l_user_id IN CUSTOMER.UPD_BY%TYPE)
IS
BEGIN
	 UPDATE CUSTOMER SET STATUS = 'A', UPD_DATE = SYSDATE, UPD_BY = l_user_id WHERE CUSTOMER_ID = l_cust_id;
END ACCEPT_CUSTOMER;
/



-- End of DDL Script for Procedure CORP.ACCEPT_CUSTOMER

-- Start of DDL Script for Procedure CORP.ACCEPT_CUSTOMER_BANK
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE accept_customer_bank
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   	l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE)
IS
BEGIN
	 UPDATE CUSTOMER_BANK SET STATUS = 'A', UPD_DATE = SYSDATE, UPD_BY = l_user_id WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
END ACCEPT_CUSTOMER_BANK;
/



-- End of DDL Script for Procedure CORP.ACCEPT_CUSTOMER_BANK

-- Start of DDL Script for Procedure CORP.ADJUSTMENT_INS
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE adjustment_ins
   (l_adjust_id OUT PAYMENT_ADJUSTMENT.ADJUST_ID%TYPE,
   	l_eft_id IN PAYMENT_ADJUSTMENT.EFT_ID%TYPE,
	l_cust_bank_id IN PAYMENT_ADJUSTMENT.CUSTOMER_BANK_ID%TYPE,
    l_terminal_id IN PAYMENT_ADJUSTMENT.TERMINAL_ID%TYPE,
	l_reason IN PAYMENT_ADJUSTMENT.REASON%TYPE,
	l_amt IN PAYMENT_ADJUSTMENT.AMOUNT%TYPE,
	l_user_id IN PAYMENT_ADJUSTMENT.CREATE_BY%TYPE
)
IS
BEGIN
	 SELECT ADJUST_SEQ.NEXTVAL INTO l_adjust_id FROM DUAL;
	 INSERT INTO PAYMENT_ADJUSTMENT(ADJUST_ID, EFT_ID, TERMINAL_ID, REASON, CREATE_DATE, CREATE_BY, AMOUNT, CUSTOMER_BANK_ID)
	     VALUES (l_adjust_id, l_eft_id, l_terminal_id, l_reason, SYSDATE, l_user_id, l_amt, l_cust_bank_id);
END ADJUSTMENT_INS;
/



-- End of DDL Script for Procedure CORP.ADJUSTMENT_INS

-- Start of DDL Script for Procedure CORP.ADJUSTMENT_UPD
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE adjustment_upd
   (l_adjust_id IN PAYMENT_ADJUSTMENT.ADJUST_ID%TYPE,
   	l_reason IN PAYMENT_ADJUSTMENT.REASON%TYPE,
   	l_amt IN PAYMENT_ADJUSTMENT.AMOUNT%TYPE,
	l_user_id IN PAYMENT_ADJUSTMENT.CREATE_BY%TYPE
)
IS
BEGIN
	 UPDATE PAYMENT_ADJUSTMENT SET REASON = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id WHERE ADJUST_ID = l_adjust_id;
END ADJUSTMENT_UPD;
/



-- End of DDL Script for Procedure CORP.ADJUSTMENT_UPD

-- Start of DDL Script for Procedure CORP.APPROVE_PAYMENT
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE approve_payment (
  l_eft_id IN EFT.EFT_ID%TYPE,
  l_user_id IN EFT.APPROVE_BY%TYPE
  )
IS
   l_adjust        PAYMENTS.ADJUST_AMOUNT%TYPE;
   l_net		   PAYMENTS.NET_AMOUNT%TYPE;
BEGIN
	 UPDATE PAYMENTS P SET ADJUST_AMOUNT = (SELECT SUM(AMOUNT) FROM PAYMENT_ADJUSTMENT PA
	    WHERE PA.TERMINAL_ID = P.TERMINAL_ID AND EFT_ID = l_eft_id) WHERE EFT_ID = l_eft_id;
	 UPDATE PAYMENTS P SET NET_AMOUNT = NET_AMOUNT + ADJUST_AMOUNT WHERE EFT_ID = l_eft_id AND NVL(ADJUST_AMOUNT, 0) <> 0;
	 SELECT SUM(AMOUNT) INTO l_adjust FROM PAYMENT_ADJUSTMENT WHERE EFT_ID = l_eft_id AND TERMINAL_ID = 0;
	 SELECT SUM(NET_AMOUNT) INTO l_net FROM PAYMENTS WHERE EFT_ID = l_eft_id;
	 UPDATE EFT SET APPROVE_BY = l_user_id, UPD_DT = SYSDATE, STATUS = 'A', EFT_AMOUNT = NVL(l_net, 0) + NVL(l_adjust, 0)
	 		WHERE EFT_ID = l_eft_id;
END APPROVE_PAYMENT;
/



-- End of DDL Script for Procedure CORP.APPROVE_PAYMENT

-- Start of DDL Script for Procedure CORP.CLEAN_UP_DEAD_DATES
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE clean_up_dead_dates
AS
BEGIN
DELETE FROM SERVICE_FEES WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM PROCESS_FEES WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM CUSTOMER_BANK_TERMINAL WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM DEALER_LICENSE WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM TERMINAL_EPORT WHERE START_DATE >= END_DATE;
COMMIT;
END;
/



-- End of DDL Script for Procedure CORP.CLEAN_UP_DEAD_DATES

-- Start of DDL Script for Procedure CORP.CREATE_CUSTOMER
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE create_customer
   (l_user_id OUT CUSTOMER.USER_ID%TYPE,
   l_cust_id OUT CUSTOMER.CUSTOMER_ID%TYPE,
   l_user_name IN VARCHAR,
   l_first IN VARCHAR,
   l_last IN VARCHAR,
   l_email IN VARCHAR,
   l_pwd IN VARCHAR,
   l_cust_name IN CUSTOMER.CUSTOMER_NAME%TYPE,
   l_addr1 IN CUSTOMER_ADDR.ADDRESS1%TYPE,
   l_city IN CUSTOMER_ADDR.CITY%TYPE,
   l_state IN CUSTOMER_ADDR.STATE%TYPE,
   l_zip IN CUSTOMER_ADDR.ZIP%TYPE,
   l_telephone IN VARCHAR,
   l_fax IN VARCHAR,
   l_dealer_id IN CUSTOMER.DEALER_ID%TYPE,
   l_tax_id_nbr IN CUSTOMER.TAX_ID_NBR%TYPE)
IS
   l_addr_id CUSTOMER_ADDR.ADDRESS_ID%TYPE;
   l_lic_nbr LICENSE_NBR.LICENSE_NBR%TYPE;
   l_lic_id LICENSE_NBR.LICENSE_ID%TYPE;
BEGIN
	 SELECT LICENSE_ID INTO l_lic_id FROM VW_DEALER_LICENSE WHERE DEALER_ID = l_dealer_id;
	 SELECT CUSTOMER_SEQ.NEXTVAL, CUSTOMER_ADDR_SEQ.NEXTVAL INTO l_cust_id, l_addr_id FROM DUAL;
	 CREATE_CUST_USER_ADMIN(l_user_id,l_user_name,l_first,l_last,l_email,l_cust_id,l_pwd,l_telephone,l_fax);
	 INSERT INTO CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, USER_ID, CREATE_BY, DEALER_ID, TAX_ID_NBR)
	     VALUES(l_cust_id, l_cust_name, l_user_id, l_user_id, l_dealer_id, l_tax_id_nbr);
	 INSERT INTO CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, CITY, STATE, ZIP)
	     VALUES (l_addr_id, l_cust_id, 2, l_first || ' ' || l_last, l_addr1, l_city, l_state, l_zip);
	 CREATE_LICENSE(l_lic_id, l_cust_id, l_lic_nbr);
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find license agreement for this dealer');
	 WHEN OTHERS THEN
	 	 RAISE;
END CREATE_CUSTOMER;
/

-- Grants for Procedure
GRANT EXECUTE ON create_customer TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.CREATE_CUSTOMER

-- Start of DDL Script for Procedure CORP.CREATE_CUSTOMER_BANK
-- Generated 13-Oct-2004 13:42:03 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE create_customer_bank
   (l_cust_bank_id OUT CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_cust_id IN CUSTOMER_BANK.CUSTOMER_ID%TYPE,
	l_user_id IN CUSTOMER.USER_ID%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_address1 IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_city IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_state IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_zip IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_telephone IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_fax IN CUSTOMER_BANK.BANK_NAME%TYPE)
IS
BEGIN
	 SELECT CUSTOMER_BANK_SEQ.NEXTVAL INTO l_cust_bank_id FROM DUAL;
	 INSERT INTO CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, CREATE_BY,
	 	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX)
	     VALUES (l_cust_bank_id, l_cust_id, l_bank_name,l_bank_address1,l_bank_city,l_bank_state,l_bank_zip,l_user_id,
		 l_title,l_account_type,l_aba,l_account_number,l_contact_name,l_contact_title,l_contact_telephone,l_contact_fax);
	 INSERT INTO USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
	 	 VALUES (l_cust_bank_id, l_user_id, 'Y');
END CREATE_CUSTOMER_BANK;
/

-- Grants for Procedure
GRANT EXECUTE ON create_customer_bank TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.CREATE_CUSTOMER_BANK

-- Start of DDL Script for Procedure CORP.CREATE_LICENSE
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE create_license
   (l_license_id IN LICENSE_NBR.LICENSE_ID%TYPE,
   l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_license_nbr OUT LICENSE_NBR.LICENSE_NBR%TYPE
)
IS
BEGIN
	 SELECT 'L' || TO_CHAR(l_license_id,'FM000') || TO_CHAR(LICENSE_SEQ.NEXTVAL,'FM99990000') INTO l_license_nbr FROM DUAL;
	 INSERT INTO LICENSE_NBR(LICENSE_NBR, LICENSE_ID, CUSTOMER_ID)
	     VALUES (l_license_nbr, l_license_id, l_cust_id);
END CREATE_LICENSE;
/



-- End of DDL Script for Procedure CORP.CREATE_LICENSE

-- Start of DDL Script for Procedure CORP.CUSTOMER_BANK_DEL
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE customer_bank_del
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE)
IS
BEGIN
     UPDATE CUSTOMER_BANK SET STATUS = 'D', UPD_BY = l_user_id WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
END CUSTOMER_BANK_DEL;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_BANK_DEL

-- Start of DDL Script for Procedure CORP.CUSTOMER_BANK_INS
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE customer_bank_ins
   (l_cust_bank_id OUT CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_cust_id IN CUSTOMER_BANK.CUSTOMER_ID%TYPE,
	l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_desc IN CUSTOMER_BANK.DESCRIPTION%TYPE,
	l_prefix IN CUSTOMER_BANK.EFT_PREFIX%TYPE,
	l_min_amt IN CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE,
	l_is_eft IN CUSTOMER_BANK.IS_EFT%TYPE,
	l_user_id IN CUSTOMER_BANK.CREATE_BY%TYPE
)
IS
BEGIN
	 SELECT CUSTOMER_BANK_SEQ.NEXTVAL INTO l_cust_bank_id FROM DUAL;
	 INSERT INTO CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, CREATE_BY, DESCRIPTION, EFT_PREFIX, PAY_MIN_AMOUNT,	 	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, IS_EFT)
	     VALUES (l_cust_bank_id, l_cust_id, l_bank_name,l_user_id, l_desc, l_prefix, l_min_amt,
		 l_title,l_account_type,l_aba,l_account_number, l_is_eft);
END CUSTOMER_BANK_INS;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_BANK_INS

-- Start of DDL Script for Procedure CORP.CUSTOMER_BANK_UPD
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE customer_bank_upd
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_desc IN CUSTOMER_BANK.DESCRIPTION%TYPE,
	l_prefix IN CUSTOMER_BANK.EFT_PREFIX%TYPE,
	l_min_amt IN CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE,
	l_is_eft IN CUSTOMER_BANK.IS_EFT%TYPE,
	l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE
)
IS
BEGIN
	 UPDATE CUSTOMER_BANK SET (BANK_NAME, CREATE_BY, DESCRIPTION, EFT_PREFIX, PAY_MIN_AMOUNT,
	  	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, IS_EFT)
	     = (SELECT l_bank_name,l_user_id, l_desc, l_prefix, l_min_amt,
		 l_title,l_account_type,l_aba,l_account_number, l_is_eft FROM DUAL) WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
END CUSTOMER_BANK_UPD;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_BANK_UPD

-- Start of DDL Script for Procedure CORP.CUSTOMER_DEL
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE customer_del
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE)
IS
    l_cnt NUMBER;
BEGIN
     -- select to validate if any terminal are active
	 DECLARE
  	    NO_TRANS EXCEPTION;
	    PRAGMA EXCEPTION_INIT(NO_TRANS, -2041);
     BEGIN
     	SELECT COUNT(*) INTO l_cnt FROM TERMINAL WHERE CUSTOMER_ID = l_cust_id AND STATUS <> 'D';
	 EXCEPTION
	    WHEN NO_TRANS THEN  -- THIS IS FOR VB WHICH CAN'T DO DISTRIBUTED QUERIES OR UPDATES
			 l_cnt := 0;
		WHEN OTHERS THEN
			 RAISE;
	 END;
     IF l_cnt = 0  THEN
     	UPDATE CUSTOMER SET STATUS = 'D' WHERE  CUSTOMER_ID = l_cust_id;
      	UPDATE CUSTOMER_ADDR SET STATUS ='D' WHERE  CUSTOMER_ID = l_cust_id;
     ELSE
	 	RAISE_APPLICATION_ERROR(-20100, 'Customer has active terminals');
	 END IF;
END CUSTOMER_DEL;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_DEL

-- Start of DDL Script for Procedure CORP.CUSTOMER_INS
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE customer_ins
   (custname IN CUSTOMER.CUSTOMER_NAME%TYPE,
    custaltname IN CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
    userid IN CUSTOMER.USER_ID%TYPE,
    salesterm IN CUSTOMER.SALES_TERM%TYPE,
    creditterm IN CUSTOMER.CREDIT_TERM%TYPE,
    updby IN CUSTOMER.UPD_BY%TYPE,
    Varout OUT NUMBER )
IS
    BEGIN
             -- Insert the details into the customer
            INSERT into CUSTOMER (customer_id,customer_name,user_id,sales_term,credit_term,
                                  create_by,customer_alt_name) VALUES
                                  (CUSTOMER_SEQ.NEXTVAL,custname,userid,salesterm,creditterm,
                                   updby,custaltname);
             Varout:= 0;
        EXCEPTION
          When OTHERS THEN
              Varout := SQLCODE;
END customer_ins;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_INS

-- Start of DDL Script for Procedure CORP.CUSTOMER_UPD
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE customer_upd
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_cust_name IN CUSTOMER.CUSTOMER_NAME%TYPE,
   l_cust_alt_name IN CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
   l_addr_name IN CUSTOMER_ADDR.NAME%TYPE,
   l_addr1 IN CUSTOMER_ADDR.ADDRESS1%TYPE,
   l_addr2 IN CUSTOMER_ADDR.ADDRESS2%TYPE,
   l_city IN CUSTOMER_ADDR.CITY%TYPE,
   l_state IN CUSTOMER_ADDR.STATE%TYPE,
   l_zip IN CUSTOMER_ADDR.ZIP%TYPE,
   l_user_id IN USER_LOGIN.USER_ID%TYPE)
IS
   l_addr_id CUSTOMER_ADDR.ADDRESS_ID%TYPE;
BEGIN
	 SELECT ADDRESS_ID INTO l_addr_id FROM VW_CUSTOMER WHERE CUSTOMER_ID = l_cust_id;
	 UPDATE CUSTOMER SET CUSTOMER_NAME = l_cust_name, CUSTOMER_ALT_NAME = l_cust_alt_name, UPD_BY= l_user_id, UPD_DATE = SYSDATE
		 WHERE CUSTOMER_ID = l_cust_id;
	 IF l_addr_id IS NULL THEN
	 	SELECT CUSTOMER_ADDR_SEQ.NEXTVAL INTO l_addr_id FROM DUAL;
	 	INSERT INTO CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP)
	     	VALUES (l_addr_id, l_cust_id, 2, l_addr_name, l_addr1, l_addr2, l_city, l_state, l_zip);
	 ELSE
	 	 UPDATE CUSTOMER_ADDR SET NAME = l_addr_name, ADDRESS1 = l_addr1, ADDRESS2 = l_addr2, CITY =  l_city, STATE = l_state, ZIP = l_zip
		 		WHERE ADDRESS_ID = l_addr_id;
	 END IF;
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Customer not found');
	 WHEN OTHERS THEN
	 	 RAISE;
END CUSTOMER_UPD;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_UPD

-- Start of DDL Script for Procedure CORP.DEALER_DEL
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_del
   (l_dealer_id IN DEALER.DEALER_ID%TYPE
)
IS
BEGIN
	 UPDATE DEALER SET STATUS = 'D' WHERE DEALER_ID = l_dealer_id;
END DEALER_DEL;
/



-- End of DDL Script for Procedure CORP.DEALER_DEL

-- Start of DDL Script for Procedure CORP.DEALER_EPORT_INS
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_eport_ins
   (l_dealer_id IN DEALER_EPORT.DEALER_ID%TYPE,
    l_eport_id IN DEALER_EPORT.EPORT_ID%TYPE
)
IS
BEGIN
	 INSERT INTO DEALER_EPORT(DEALER_ID, EPORT_ID)
	     VALUES(l_dealer_id, l_eport_id);
END DEALER_EPORT_INS;
/



-- End of DDL Script for Procedure CORP.DEALER_EPORT_INS

-- Start of DDL Script for Procedure CORP.DEALER_EPORT_UPD
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_eport_upd
   (l_dealer_id IN DEALER_EPORT.DEALER_ID%TYPE,
    l_eport_id IN DEALER_EPORT.EPORT_ID%TYPE
)
IS
BEGIN
	 DELETE FROM DEALER_EPORT WHERE EPORT_ID = l_eport_id;
	 INSERT INTO DEALER_EPORT(DEALER_ID, EPORT_ID)
	     VALUES(l_dealer_id, l_eport_id);
END DEALER_EPORT_UPD;
/



-- End of DDL Script for Procedure CORP.DEALER_EPORT_UPD

-- Start of DDL Script for Procedure CORP.DEALER_INS
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_ins
   (l_dealer_id OUT DEALER.DEALER_ID%TYPE,
    l_name IN DEALER.DEALER_NAME%TYPE
)
IS
BEGIN
	 SELECT DEALER_SEQ.NEXTVAL INTO l_dealer_id FROM DUAL;
	 INSERT INTO DEALER(DEALER_ID, DEALER_NAME)
	     VALUES (l_dealer_id, l_name);
END DEALER_INS;
/



-- End of DDL Script for Procedure CORP.DEALER_INS

-- Start of DDL Script for Procedure CORP.DEALER_LICENSE_DEL
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_license_del
   (l_dealer_id IN DEALER_LICENSE.DEALER_ID%TYPE,
   l_license_id IN DEALER_LICENSE.LICENSE_ID%TYPE
)
IS
BEGIN
	 DELETE FROM DEALER_LICENSE WHERE DEALER_ID = l_dealer_id AND LICENSE_ID = l_license_id;
END DEALER_LICENSE_DEL;
/



-- End of DDL Script for Procedure CORP.DEALER_LICENSE_DEL

-- Start of DDL Script for Procedure CORP.DEALER_LICENSE_INS
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_license_ins
   (l_dealer_id IN DEALER_LICENSE.DEALER_ID%TYPE,
   l_license_id IN DEALER_LICENSE.LICENSE_ID%TYPE
)
IS
BEGIN
	 INSERT INTO DEALER_LICENSE(DEALER_ID, LICENSE_ID) VALUES (l_dealer_id, l_license_id);
END DEALER_LICENSE_INS;
/



-- End of DDL Script for Procedure CORP.DEALER_LICENSE_INS

-- Start of DDL Script for Procedure CORP.DEALER_UPD
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE dealer_upd
   (l_dealer_id IN DEALER.DEALER_ID%TYPE,
    l_name IN DEALER.DEALER_NAME%TYPE
)
IS
BEGIN
	 UPDATE DEALER SET DEALER_NAME = l_name WHERE DEALER_ID = l_dealer_id;
END DEALER_UPD;
/



-- End of DDL Script for Procedure CORP.DEALER_UPD

-- Start of DDL Script for Procedure CORP.DELAY_REFUND
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE delay_refund (
  l_id IN LEDGER.LEDGER_ID%TYPE
  )
IS
  l_amt LEDGER.TOTAL_AMOUNT%TYPE;
  l_payment_id LEDGER.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT TOTAL_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM LEDGER WHERE LEDGER_ID = l_id AND TRANS_TYPE_ID = 20;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE LEDGER SET PAYMENT_ID = 0 WHERE LEDGER_ID = l_id;
     UPDATE PAYMENTS P SET REFUND_AMOUNT = REFUND_AMOUNT - l_amt, NET_AMOUNT = NET_AMOUNT - l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Refund not found');
	WHEN OTHERS THEN
		 RAISE;
END DELAY_REFUND;
/



-- End of DDL Script for Procedure CORP.DELAY_REFUND

-- Start of DDL Script for Procedure CORP.DELAY_REFUND_BY_TRAN_ID
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE delay_refund_by_tran_id (
  l_id IN LEDGER.TRANS_ID%TYPE
  )
IS
  l_amt LEDGER.TOTAL_AMOUNT%TYPE;
  l_payment_id LEDGER.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT TOTAL_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM LEDGER WHERE TRANS_ID = l_id AND TRANS_TYPE_ID = 20;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE LEDGER SET PAYMENT_ID = 0 WHERE TRANS_ID = l_id;
     UPDATE PAYMENTS P SET REFUND_AMOUNT = REFUND_AMOUNT - l_amt, NET_AMOUNT = NET_AMOUNT - l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Refund not found');
	WHEN OTHERS THEN
		 RAISE;
END DELAY_REFUND_BY_TRAN_ID;
/



-- End of DDL Script for Procedure CORP.DELAY_REFUND_BY_TRAN_ID

-- Start of DDL Script for Procedure CORP.DELAY_SERVICE_FEE
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE delay_service_fee (
  l_id IN PAYMENT_SERVICE_FEE.PAYMENT_SERVICE_FEE_ID%TYPE
  )
IS
  l_amt PAYMENT_SERVICE_FEE.FEE_AMOUNT%TYPE;
  l_payment_id PAYMENT_SERVICE_FEE.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT FEE_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_SERVICE_FEE_ID = l_id;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = 0 WHERE PAYMENT_SERVICE_FEE_ID = l_id;
     UPDATE PAYMENTS P SET SERVICE_FEE_AMOUNT = SERVICE_FEE_AMOUNT + l_amt, NET_AMOUNT = NET_AMOUNT + l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Service Fee not found');
	WHEN OTHERS THEN
		 RAISE;
END DELAY_SERVICE_FEE;
/



-- End of DDL Script for Procedure CORP.DELAY_SERVICE_FEE

-- Start of DDL Script for Procedure CORP.DELETE_SERVICE_FEE
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE delete_service_fee (
  l_id IN PAYMENT_SERVICE_FEE.PAYMENT_SERVICE_FEE_ID%TYPE
  )
IS
  l_amt PAYMENT_SERVICE_FEE.FEE_AMOUNT%TYPE;
  l_payment_id PAYMENT_SERVICE_FEE.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT FEE_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_SERVICE_FEE_ID = l_id;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = 0, STATUS = 'D' WHERE PAYMENT_SERVICE_FEE_ID = l_id;
     UPDATE PAYMENTS P SET SERVICE_FEE_AMOUNT = SERVICE_FEE_AMOUNT + l_amt, NET_AMOUNT = NET_AMOUNT - l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Service Fee not found');
	WHEN OTHERS THEN
		 RAISE;
END DELETE_SERVICE_FEE;
/



-- End of DDL Script for Procedure CORP.DELETE_SERVICE_FEE

-- Start of DDL Script for Procedure CORP.LICENSE_DEL
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE license_del
   (l_license_id IN LICENSE.LICENSE_ID%TYPE
)
IS
BEGIN
	 UPDATE LICENSE SET STATUS = 'D' WHERE LICENSE_ID = l_license_id;
END LICENSE_DEL;
/



-- End of DDL Script for Procedure CORP.LICENSE_DEL

-- Start of DDL Script for Procedure CORP.LICENSE_INS
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE license_ins
   (l_license_id OUT LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE,
	l_credit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE, --as decimal (i.e. - for 5% use 0.05)
	l_debit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_pass_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_access_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_maint_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_cash_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_refund_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_chargeback_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_service_fee IN LICENSE_SERVICE_FEES.AMOUNT%TYPE
)
IS
BEGIN
	 SELECT LICENSE_SEQ.NEXTVAL INTO l_license_id FROM DUAL;
	 INSERT INTO LICENSE(LICENSE_ID, TITLE, DESCRIPTION)
	     VALUES(l_license_id, l_title, l_desc);
	 IF l_credit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 16, l_credit_fee);
	 END IF;
	 IF l_debit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 19, l_debit_fee);
	 END IF;
	 IF l_pass_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 18, l_pass_fee);
	 END IF;
	 IF l_access_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 17, l_access_fee);
	 END IF;
	 IF l_maint_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 23, l_maint_fee);
	 END IF;
	 IF l_cash_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 22, l_cash_fee);
	 END IF;
	 IF l_refund_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 20, l_refund_fee);
	 END IF;
	 IF l_chargeback_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 21, l_chargeback_fee);
	 END IF;
 	 INSERT INTO LICENSE_SERVICE_FEES(LICENSE_ID, FEE_ID, AMOUNT, FREQUENCY_ID)
	     VALUES(l_license_id, 1, l_service_fee, 2);
END LICENSE_INS;
/



-- End of DDL Script for Procedure CORP.LICENSE_INS

-- Start of DDL Script for Procedure CORP.LICENSE_UPD
-- Generated 13-Oct-2004 13:42:04 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE license_upd
   (l_license_id IN LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE,
	l_credit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE, --as decimal (i.e. - for 5% use 0.05)
	l_debit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_pass_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_access_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_maint_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_cash_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_refund_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_chargeback_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_service_fee IN LICENSE_SERVICE_FEES.AMOUNT%TYPE
)
IS
BEGIN
	 UPDATE LICENSE SET (TITLE, DESCRIPTION) =
	     (SELECT l_title, l_desc FROM DUAL) WHERE LICENSE_ID = l_license_id;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 16;
	 IF l_credit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 16, l_credit_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 19;
	 IF l_debit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 19, l_debit_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 18;
	 IF l_pass_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 18, l_pass_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 17;
	 IF l_access_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 17, l_access_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 23;
	 IF l_maint_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 23, l_maint_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 22;
	 IF l_cash_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 22, l_cash_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 20;
	 IF l_refund_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 20, l_refund_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 21;
	 IF l_chargeback_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 21, l_chargeback_fee);
	 END IF;
	 DELETE FROM LICENSE_SERVICE_FEES WHERE LICENSE_ID = l_license_id AND FEE_ID = 1;
	 INSERT INTO LICENSE_SERVICE_FEES(LICENSE_ID, FEE_ID, AMOUNT, FREQUENCY_ID)
	     VALUES(l_license_id, 1, l_service_fee, 2);
END LICENSE_UPD;
/



-- End of DDL Script for Procedure CORP.LICENSE_UPD

-- Start of DDL Script for Procedure CORP.LOAD_LICENSE_AGREEMENTS
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE load_license_agreements IS
    CURSOR C IS
        SELECT CUSTOMER_ID, 1 - CUSTOMER_CUT, FEES, CUSTOMER FROM XLICENSE_FEE_DATA X WHERE LICENSE_AGREEMENT = 'Y';
    l_customer_id CUSTOMER.CUSTOMER_ID%TYPE;
    l_license_id LICENSE.LICENSE_ID%TYPE;
    l_customer XLICENSE_FEE_DATA.CUSTOMER%TYPE;
    l_credit_fee LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE;
    l_service_fee LICENSE_SERVICE_FEES.AMOUNT%TYPE;
BEGIN
   OPEN C;
   LOOP
	  FETCH C INTO l_customer_id, l_credit_fee, l_service_fee, l_customer;
      EXIT WHEN C%NOTFOUND;
	  LICENSE_INS(l_license_id, SUBSTR('License for ' || l_customer,1,50),'', l_credit_fee, 0, 0, 0, 0, 0, 0, 0, l_service_fee);
	  RECEIVED_CUSTOM_LICENSE(l_license_id, l_customer_id, 7);
	  COMMIT;
   END LOOP;
END;
/



-- End of DDL Script for Procedure CORP.LOAD_LICENSE_AGREEMENTS

-- Start of DDL Script for Procedure CORP.MARK_EFT_PAID
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE mark_eft_paid (
  l_eft_id IN EFT.EFT_ID%TYPE,
  l_user_id IN EFT.APPROVE_BY%TYPE
  )
IS
BEGIN
	 UPDATE EFT SET SENT_BY = l_user_id, UPD_DT = SYSDATE, STATUS = 'P', EFT_DATE = SYSDATE
	 		WHERE EFT_ID = l_eft_id;
END MARK_EFT_PAID;
/



-- End of DDL Script for Procedure CORP.MARK_EFT_PAID

-- Start of DDL Script for Procedure CORP.PROCESS_FEES_UPD
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE process_fees_upd
   (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
    l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
    l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
    l_effective_dt IN PROCESS_FEES.END_DATE%TYPE )
IS
   l_effective_date DATE;
BEGIN
	 IF l_effective_dt IS NULL THEN
	     l_effective_date := SYSDATE;
	 ELSE
	 	 l_effective_date := l_effective_dt;
	 END IF;
	 --CHECK IF WE NEED already paid on transactions before after the effective date
	 IF l_effective_date < SYSDATE THEN
	 	 DECLARE
		     l_tran_id LEDGER.TRANS_ID%TYPE;
			BEGIN
		 	 SELECT MIN(TRANS_ID) INTO l_tran_id FROM LEDGER WHERE TERMINAL_ID = l_terminal_id
			     AND CLOSE_DATE > l_effective_date AND PAYMENT_ID <> 0;
			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
			 END IF;
		 END;
	 END IF;
	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, START_DATE)
 	    VALUES(PROCESS_FEE_SEQ.NEXTVAL,l_terminal_id,l_trans_type_id,l_fee_percent,l_effective_date);
END PROCESS_FEES_UPD;
/



-- End of DDL Script for Procedure CORP.PROCESS_FEES_UPD

-- Start of DDL Script for Procedure CORP.RECEIVED_CUSTOM_LICENSE
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE received_custom_license
   (l_license_id IN LICENSE_NBR.LICENSE_ID%TYPE,
   l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_user_id IN CUSTOMER_LICENSE.RECEIVE_BY%TYPE)
IS
   l_lic_nbr LICENSE_NBR.LICENSE_NBR%TYPE;
BEGIN
	 CREATE_LICENSE(l_license_id, l_cust_id, l_lic_nbr);
	 RECEIVED_LICENSE(l_cust_id, l_lic_nbr, l_user_id);
END RECEIVED_CUSTOM_LICENSE;
/



-- End of DDL Script for Procedure CORP.RECEIVED_CUSTOM_LICENSE

-- Start of DDL Script for Procedure CORP.RECEIVED_LICENSE
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE received_license
   (l_cust_id IN CUSTOMER_LICENSE.CUSTOMER_ID%TYPE,
   	l_license_nbr IN CUSTOMER_LICENSE.LICENSE_NBR%TYPE,
   	l_user_id IN CUSTOMER_LICENSE.RECEIVE_BY%TYPE)
IS
  	l_match_cust_id CUSTOMER_LICENSE.CUSTOMER_ID%TYPE;
  	l_match_license_id LICENSE_NBR.LICENSE_ID%TYPE;
	l_cnt NUMBER;
BEGIN
	 SELECT CUSTOMER_ID, LICENSE_ID INTO l_match_cust_id, l_match_license_id FROM LICENSE_NBR WHERE LICENSE_NBR = l_license_nbr;
	 IF l_match_cust_id <> l_cust_id THEN -- CUSTOMER USED A DIFFERENT LICENSE NBR, MAKE SURE LICENSE ID IS THE SAME
	 	SELECT COUNT(*) INTO l_cnt FROM LICENSE_NBR WHERE CUSTOMER_ID = l_cust_id AND LICENSE_ID = l_match_license_id;
	 	IF l_cnt = 0 THEN --INVALID LICENSE ID
		   RAISE_APPLICATION_ERROR(-20112, 'License Nbr matches the wrong contract for this customer');
		END IF;
	 END IF;
	 INSERT INTO CUSTOMER_LICENSE(LICENSE_NBR, CUSTOMER_ID, RECEIVE_BY) VALUES (l_license_nbr, l_cust_id, l_user_id);
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20110, 'Invalid License Nbr specified');
	 WHEN OTHERS THEN
	 	 RAISE;
END RECEIVED_LICENSE;
/



-- End of DDL Script for Procedure CORP.RECEIVED_LICENSE

-- Start of DDL Script for Procedure CORP.SCAN_FOR_SERVICE_FEES
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE scan_for_service_fees
IS
    CURSOR c_fee
    IS
        SELECT service_fee_id,
               terminal_id,
               fee_id,
               fee_amount,
               months,
               days,
               last_payment,
               start_date,
               end_date
          FROM service_fees sf, frequency q
         WHERE sf.frequency_id = q.frequency_id
           AND (   days <> 0
                OR months <> 0)
           AND ADD_MONTHS (last_payment + days, months) <
                                      LEAST (NVL (end_date, SYSDATE), SYSDATE);
                                      
    cb_DEBUG_MODE                      CONSTANT BOOLEAN := FALSE;

    l_service_fee_id                   service_fees.service_fee_id%TYPE;
    l_terminal_id                      service_fees.terminal_id%TYPE;
    l_fee_id                           service_fees.fee_id%TYPE;
    l_fee_amount                       service_fees.fee_amount%TYPE;
    l_months                           frequency.months%TYPE;
    l_days                             frequency.days%TYPE;
    l_last_payment                     service_fees.last_payment%TYPE;
    l_start_date                       service_fees.start_date%TYPE;
    l_end_date                         service_fees.end_date%TYPE;
BEGIN
    OPEN c_fee;

    LOOP
        FETCH c_fee INTO l_service_fee_id,
         l_terminal_id,
         l_fee_id,
         l_fee_amount,
         l_months,
         l_days,
         l_last_payment,
         l_start_date,
         l_end_date;
        EXIT WHEN c_fee%NOTFOUND;

        LOOP
            IF l_months > 0 THEN
                SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, l_months)), 'DD')
                  INTO l_last_payment
                  FROM DUAL;
            ELSE
                SELECT l_last_payment + l_days
                  INTO l_last_payment
                  FROM DUAL;
            END IF;

            EXIT WHEN l_last_payment > SYSDATE
                  OR l_last_payment > NVL (l_end_date, SYSDATE);

            IF cb_DEBUG_MODE = TRUE Then
            
                DBMS_OUTPUT.PUT_LINE('payment_service_fee: terminal_id = ''' || l_terminal_id ||
                                    ''', fee_id = ''' || l_fee_id ||
                                    ''', fee_amount = ''' || l_fee_amount ||
                                    ''', last_payment = ''' || l_last_payment ||
                                    ''', service_fee_id = ''' || l_service_fee_id ||
                                    ''', end_date = ''' || l_end_date || '''');
            END IF;
            
            BEGIN -- catch exception here
                -- skip creation of fee if start date is after fee date
                IF l_last_payment >= NVL(l_start_date, l_last_payment) THEN
                    INSERT INTO payment_service_fee
                                (payment_service_fee_id,
                                 terminal_id,
                                 fee_id,
                                 fee_amount,
                                 fee_date,
                                 service_fee_id)
                         VALUES (payment_service_fee_seq.NEXTVAL,
                                 l_terminal_id,
                                 l_fee_id,
                                 l_fee_amount,
                                 l_last_payment,
                                 l_service_fee_id);
                END IF;

                UPDATE service_fees
                   SET last_payment = l_last_payment
                 WHERE service_fee_id = l_service_fee_id;

                COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
            EXCEPTION
				 WHEN DUP_VAL_ON_INDEX THEN
				 	 ROLLBACK;
            END;
        END LOOP;
    END LOOP;

    CLOSE c_fee;
    
END scan_for_service_fees;
/

-- Grants for Procedure
GRANT EXECUTE ON scan_for_service_fees TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.SCAN_FOR_SERVICE_FEES

-- Start of DDL Script for Procedure CORP.SERVICE_FEES_UPD
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE service_fees_upd
   (l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
    l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
    l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
    l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
    l_effective_dt IN SERVICE_FEES.END_DATE%TYPE )
IS
   l_effective_date DATE;
   l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
BEGIN
	 IF l_effective_dt IS NULL THEN
	     l_effective_date := SYSDATE;
	 ELSE
	 	 l_effective_date := l_effective_dt;
	 END IF;
	 BEGIN
		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
		END IF;
	 EXCEPTION
	 	WHEN NO_DATA_FOUND THEN
			 NULL;
		WHEN OTHERS THEN
			 RAISE;
	 END;
	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
	 IF l_fee_amt <> 0 THEN
         INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
	 END IF;
END SERVICE_FEES_UPD;
/



-- End of DDL Script for Procedure CORP.SERVICE_FEES_UPD

-- Start of DDL Script for Procedure CORP.TERMINAL_EARLIER_BANK_ACCOUNT
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE terminal_earlier_bank_account
   (l_cbt_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_TERMINAL_ID%TYPE,
	l_effective_date IN DATE)
IS
    l_terminal_id CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE;
    l_end_date    CUSTOMER_BANK_TERMINAL.END_DATE%TYPE;
BEGIN
    -- Update mapping
    UPDATE CUSTOMER_BANK_TERMINAL
       SET START_DATE = l_effective_date
     WHERE CUSTOMER_BANK_TERMINAL_ID = l_cbt_id
       RETURNING TERMINAL_ID, END_DATE INTO l_terminal_id, l_end_date;

    -- Update all other Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(START_DATE, MIN_DATE) < NVL(l_end_date, MAX_DATE)
       AND NVL(END_DATE, MAX_DATE) > l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
END TERMINAL_EARLIER_BANK_ACCOUNT;
/



-- End of DDL Script for Procedure CORP.TERMINAL_EARLIER_BANK_ACCOUNT

-- Start of DDL Script for Procedure CORP.TERMINAL_PAYMENT_ACTIVATE
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE terminal_payment_activate
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
   	l_date IN DATE,
	l_dex IN CHAR,
	l_customer_id OUT CUSTOMER.CUSTOMER_ID%TYPE
  )
IS
    l_license_id LICENSE.LICENSE_ID%TYPE;
BEGIN
	 --VERIFY BANK ACCT
	 BEGIN
	 	 SELECT CUSTOMER_ID INTO l_customer_id FROM CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank id');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 BEGIN
	 	 SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CUSTOMER_LICENSE CL, LICENSE_NBR LN
	     	 WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 --INSERT INTO CUSTOMER_BANK_TERMINAL
	 INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
	     VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_date);
	 --INSERT INTO SERVICE_FEES
	 -- Added logic to set last payment to the effective date minus one
     -- frequency interval so that first active day has a service fee (BSK 09-14-04)
	 INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	 SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, lsf.FEE_ID,
                lsf.AMOUNT, lsf.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), l_date
		 FROM LICENSE_SERVICE_FEES lsf, FREQUENCY f
         WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
           AND lsf.LICENSE_ID = l_license_id;
	 IF l_dex <> 'N' THEN
	 	INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	    SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4,
                   4.00, f.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), l_date
		      FROM FREQUENCY f
             WHERE f.FREQUENCY_ID = 2;
	 END IF;
	 --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
	     SELECT PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, l_date
		 FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
END TERMINAL_PAYMENT_ACTIVATE;
/



-- End of DDL Script for Procedure CORP.TERMINAL_PAYMENT_ACTIVATE

-- Start of DDL Script for Procedure CORP.TERMINAL_REASSIGN_BANK_ACCOUNT
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE terminal_reassign_bank_account
   (l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
    l_cust_bank_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_ID%TYPE,
	l_effective_date IN DATE)
IS
BEGIN
    -- Update all Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(END_DATE, l_effective_date) >= l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
    -- Insert new mapping if customer bank is not zero
    IF l_cust_bank_id <> 0 THEN
		INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
		    VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_effective_date);
    END IF;
    
    -- Clean up dead dates
    DELETE FROM CUSTOMER_BANK_TERMINAL WHERE START_DATE >= END_DATE;
    
END TERMINAL_REASSIGN_BANK_ACCOUNT;
/

-- Grants for Procedure
GRANT EXECUTE ON terminal_reassign_bank_account TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.TERMINAL_REASSIGN_BANK_ACCOUNT

-- Start of DDL Script for Procedure CORP.UNLOCK_EFT
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE unlock_eft (
  l_eft_id IN EFT.EFT_ID%TYPE
  )
IS
  l_status EFT.STATUS%TYPE;
BEGIN
	 SELECT STATUS INTO l_status FROM EFT WHERE EFT_ID = l_eft_id;
	 IF l_status <> 'L' THEN
	 	 RAISE_APPLICATION_ERROR(-20400, 'This EFT has already been approved; you can not unlock it.');
	 END IF;
	 UPDATE LEDGER SET PAYMENT_ID = 0 WHERE PAYMENT_ID IN(SELECT PAYMENT_ID FROM PAYMENTS WHERE EFT_ID = l_eft_id);
	 UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = 0 WHERE PAYMENT_ID IN(SELECT PAYMENT_ID FROM PAYMENTS WHERE EFT_ID = l_eft_id);
	 DELETE FROM PAYMENT_PROCESS_FEE WHERE PAYMENT_ID IN(SELECT PAYMENT_ID FROM PAYMENTS WHERE EFT_ID = l_eft_id);
	 DELETE FROM PAYMENTS WHERE EFT_ID = l_eft_id;
	 UPDATE PAYMENT_ADJUSTMENT SET EFT_ID = 0 WHERE EFT_ID = l_eft_id;
	 DELETE FROM EFT WHERE EFT_ID = l_eft_id;
	 COMMIT;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	   RAISE_APPLICATION_ERROR(-20401, 'This EFT does not exist.');
    WHEN OTHERS THEN
	   ROLLBACK;
	   RAISE;
END UNLOCK_EFT;
/



-- End of DDL Script for Procedure CORP.UNLOCK_EFT

-- Start of DDL Script for Procedure CORP.UPDATE_LEDGER_PENDING_TABLES
-- Generated 13-Oct-2004 13:42:05 from CORP@USADBD02

CREATE OR REPLACE 
PROCEDURE update_ledger_pending_tables
IS
BEGIN
execute immediate 'truncate table ledger_pending_revenue';

insert into ledger_pending_revenue(ledger_id, customer_bank_id, close_date, trans_type_id, total_amount)
select l.ledger_id, cbt.customer_bank_id, l.close_date, l.trans_type_id, l.total_amount
FROM ledger l, customer_bank_terminal cbt
   WHERE payment_id = 0
     AND l.terminal_id = cbt.terminal_id
     AND within1 (close_date,
                  cbt.start_date,
                  cbt.end_date
                 ) = 1;

execute immediate 'truncate table ledger_pending_process_fees';

insert into ledger_pending_process_fees(ledger_id, customer_bank_id, close_date, trans_type_id, total_amount, fee_percent)
select l.ledger_id, pf.customer_bank_id, l.close_date, l.trans_type_id, l.total_amount, pf.fee_percent
FROM ledger l, process_fees pf
   WHERE payment_id = 0
     AND l.trans_type_id = pf.trans_type_id
     AND l.terminal_id = pf.terminal_id
     AND within1 (l.close_date,
                  pf.start_date,
                  pf.end_date
                 ) = 1;
END;
/



-- End of DDL Script for Procedure CORP.UPDATE_LEDGER_PENDING_TABLES

