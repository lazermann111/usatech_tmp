-- Start of DDL Script for Table CORP.UNPAID_MONIES_RPT
-- Generated 3/15/2005 10:31:35 AM from CORP@USADBD02

CREATE TABLE corp.unpaid_monies_rpt
    (unpaid_monies_rpt_id           NUMBER(20,0) NOT NULL,
    customer_bank_id               NUMBER(20,0) NOT NULL,
    server_date                    DATE NOT NULL,
    credit_amount                  NUMBER(21,8) DEFAULT 0  NOT NULL,
    refund_amount                  NUMBER(21,8) DEFAULT 0  NOT NULL,
    chargeback_amount              NUMBER(21,8) DEFAULT 0  NOT NULL,
    process_fee_amount             NUMBER(21,8) DEFAULT 0  NOT NULL,
    service_fee_amount             NUMBER(21,8) DEFAULT 0  NOT NULL,
    adjust_amount                  NUMBER(21,8) DEFAULT 0  NOT NULL,
    net_payment_amount             NUMBER(21,8) DEFAULT 0  NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  corp_data
/




-- Indexes for CORP.UNPAID_MONIES_RPT

CREATE INDEX corp.ix_unpaid_monies_rpt_1 ON corp.unpaid_monies_rpt
  (
    customer_bank_id                ASC
  )
  TABLESPACE  corp_idx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX corp.ix_unpaid_monies_rpt_2 ON corp.unpaid_monies_rpt
  (
    server_date                     ASC
  )
  TABLESPACE  corp_idx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE UNIQUE INDEX corp.uix_unpaid_monies_rpt_1 ON corp.unpaid_monies_rpt
  (
    customer_bank_id                ASC,
    server_date                     ASC
  )
  TABLESPACE  corp_idx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- Constraints for CORP.UNPAID_MONIES_RPT

ALTER TABLE corp.unpaid_monies_rpt
ADD CONSTRAINT pk_unpaid_monies_rpt_id PRIMARY KEY (unpaid_monies_rpt_id)
USING INDEX
  TABLESPACE  corp_idx
/



-- Triggers for CORP.UNPAID_MONIES_RPT

CREATE OR REPLACE TRIGGER corp.trbi_unpaid_monies_rpt
 BEFORE
  INSERT
 ON corp.unpaid_monies_rpt
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.UNPAID_MONIES_RPT_ID IS NULL THEN

      SELECT SEQ_UNPAID_MONIES_RPT_ID.nextval
        into :new.UNPAID_MONIES_RPT_ID
        FROM dual;

    END IF;
    
    IF :new.server_date IS NOT NULL THEN

      :new.server_date := TRUNC(:new.server_date);

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER corp.trbu_unpaid_monies_rpt
 BEFORE
  UPDATE
 ON corp.unpaid_monies_rpt
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.server_date IS NOT NULL THEN

      :new.server_date := TRUNC(:new.server_date);

    END IF;
    
  SELECT
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table CORP.UNPAID_MONIES_RPT

-- Foreign Key
ALTER TABLE corp.unpaid_monies_rpt
ADD CONSTRAINT fk_unpaid_monies_customer_bank FOREIGN KEY (customer_bank_id)
REFERENCES CORP.customer_bank (customer_bank_id)
/
-- End of DDL script for Foreign Key(s)




-- Start of DDL Script for Sequence CORP.SEQ_UNPAID_MONIES_RPT_ID
-- Generated 3/15/2005 4:36:01 PM from CORP@USADBD02

CREATE SEQUENCE corp.seq_unpaid_monies_rpt_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence CORP.SEQ_UNPAID_MONIES_RPT_ID

