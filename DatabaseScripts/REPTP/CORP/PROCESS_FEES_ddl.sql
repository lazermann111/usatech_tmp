-- Start of DDL Script for Table CORP.PROCESS_FEES
-- Generated 7-Oct-2004 9:27:34 from CORP@USADBD02

-- Drop the old instance of PROCESS_FEES
DROP TABLE process_fees
/

CREATE TABLE process_fees
    (terminal_id                    NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    fee_percent                    NUMBER(8,4) NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    process_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    fee_amount                     NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  CONSTRAINT PK_PROCESS_FEES
  PRIMARY KEY (process_fee_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON process_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON process_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON process_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON process_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON process_fees TO report
WITH GRANT OPTION
/



-- Indexes for PROCESS_FEES

CREATE UNIQUE INDEX uix_process_fees_combo1 ON process_fees
  (
    trans_type_id                   ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC,
    process_fee_id                  ASC
  )
/

CREATE UNIQUE INDEX uix_process_fees_combo2 ON process_fees
  (
    process_fee_id                  ASC,
    fee_percent                     ASC,
    fee_amount                      ASC
  )
/

CREATE INDEX ix_pf_end_date ON process_fees
  (
    end_date                        ASC
  )
/

CREATE INDEX ix_pf_start_date ON process_fees
  (
    start_date                      ASC
  )
/

CREATE INDEX ix_pf_terminal_id ON process_fees
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pf_trans_type_id ON process_fees
  (
    trans_type_id                   ASC
  )
/



-- Triggers for PROCESS_FEES

CREATE OR REPLACE TRIGGER traiud_process_fees
 AFTER
  INSERT OR DELETE OR UPDATE
 ON process_fees
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        -- must update ledger & trans dim
        UPDATE LEDGER L SET L.AMOUNT = (
            SELECT T.TOTAL_AMOUNT * :NEW.FEE_PERCENT + :NEW.FEE_AMOUNT
              FROM TRANS T
             WHERE T.TRAN_ID = L.TRANS_ID)
         WHERE L.ENTRY_TYPE = 'PF'
           AND L.PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
           
        IF :OLD.PROCESS_FEE_ID != :NEW.PROCESS_FEE_ID OR :OLD.TERMINAL_ID != :NEW.TERMINAL_ID
                OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID
                OR NVL(:OLD.START_DATE, MIN_DATE) !=  NVL(:NEW.START_DATE, MIN_DATE)
                OR NVL(:OLD.END_DATE, MAX_DATE) !=  NVL(:NEW.END_DATE, MAX_DATE) THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
                   FROM PROCESS_FEES PF
                   WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                     AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                     AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                     AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
             WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
                OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
        END IF;
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE)) -- should be null always
             WHERE PROCESS_FEE_ID = :OLD.PROCESS_FEE_ID;
        END IF;
   END IF;
END;
/


-- End of DDL Script for Table CORP.PROCESS_FEES

