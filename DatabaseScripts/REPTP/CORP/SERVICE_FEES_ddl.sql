-- Start of DDL Script for Table CORP.SERVICE_FEES
-- Generated 9/16/2004 9:39:06 AM from CORP@REPTP

CREATE TABLE service_fees
    (customer_bank_id               NUMBER NOT NULL,
    terminal_id                    NUMBER DEFAULT 0  NOT NULL,
    fee_id                         NUMBER NOT NULL,
    fee_amount                     NUMBER(15,2) NOT NULL,
    frequency_id                   NUMBER NOT NULL,
    last_payment                   DATE DEFAULT SYSDATE  NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    service_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_SERVICE_FEE
  PRIMARY KEY (service_fee_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON service_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON service_fees TO report
WITH GRANT OPTION
/



-- Indexes for SERVICE_FEES

CREATE INDEX ix_sf_customer_bank_id ON service_fees
  (
    customer_bank_id                ASC
  )
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX ix_sf_fee_id ON service_fees
  (
    fee_id                          ASC
  )
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX ix_sf_frequency_id ON service_fees
  (
    frequency_id                    ASC
  )
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX ix_sf_terminal_id ON service_fees
  (
    terminal_id                     ASC
  )
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- End of DDL Script for Table CORP.SERVICE_FEES

