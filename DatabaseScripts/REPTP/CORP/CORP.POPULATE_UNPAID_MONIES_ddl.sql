-- Start of DDL Script for Procedure CORP.POPULATE_UNPAID_MONIES

CREATE OR REPLACE 
PROCEDURE corp.populate_unpaid_monies
   (inDate IN DATE)
   IS
--
-- Purpose: This will populate the unpaid_monies_rpt table with
--          with data for a given date
--
-- MODIFICATION HISTORY
-- Person      Date        Comments
-- ---------   ---------   ------------------------------------------
-- J Bradley   09-MAR-05   Inital version
-- J Bradley   15-MAR-05   Added filter to not populate data for Orphan Transactions (faux) customer
-- J Bradley   16-MAR-05   Altered filter for Orphan Transactions (faux) customers (there are now more than one)

    tDate DATE;

BEGIN
    -- ensure tDate is truncated to a day (i.e. time = 00:00:00)
    tDate := TRUNC(inDate);

    -- truncate the scratch table
	EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
	
	-- loop through each customer id and insert amounts
	/* this is faster than doing it through standard SQL */
	FOR cbc IN (
	    SELECT DISTINCT bat.customer_bank_id cbid
		FROM doc eft, batch bat
		WHERE bat.doc_id = eft.doc_id (+)
		AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
	)		
    LOOP
        
		-- Insert credit_amount       
        INSERT INTO scr_unpaid_monies
        SELECT bat.customer_bank_id,
		SUM(led.amount) credit_amount,
		0, 0, 0, 0, 0
		FROM doc eft, batch bat, ledger led, report.trans rtr
		WHERE led.trans_id = rtr.tran_id
		AND bat.batch_id = led.batch_id
		AND bat.doc_id = eft.doc_id (+)
		AND bat.customer_bank_id = cbc.cbid
		AND led.entry_type = 'CC'
		AND led.settle_state_id IN (2,3,6)
		AND led.deleted = 'N'
		AND rtr.server_date < tDate
        AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
		GROUP BY bat.customer_bank_id;
		
		-- Insert refund_amount
		INSERT INTO scr_unpaid_monies
        SELECT bat.customer_bank_id,
        0,
		SUM(led.amount) refund_amount,
		0, 0, 0, 0
		FROM doc eft, batch bat, ledger led, report.trans rtr
		WHERE led.trans_id = rtr.tran_id
		AND bat.batch_id = led.batch_id
		AND bat.doc_id = eft.doc_id (+)
		AND bat.customer_bank_id = cbc.cbid
		AND led.entry_type = 'RF'
		AND led.settle_state_id IN (2,3,6)
		AND led.deleted = 'N'
		AND rtr.server_date < tDate
        AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
		GROUP BY bat.customer_bank_id;
		
		-- Insert chargeback_amount
		INSERT INTO scr_unpaid_monies
        SELECT bat.customer_bank_id,
        0, 0,
		SUM(led.amount) chargeback_amount,
		0, 0, 0
		FROM doc eft, batch bat, ledger led, report.trans rtr
		WHERE led.trans_id = rtr.tran_id
		AND bat.batch_id = led.batch_id
		AND bat.doc_id = eft.doc_id (+)
		AND bat.customer_bank_id = cbc.cbid
		AND led.entry_type = 'CB'
		AND led.settle_state_id IN (2,3,6)
		AND led.deleted = 'N'
		AND rtr.server_date < tDate
        AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
		GROUP BY bat.customer_bank_id;
		
		-- Insert process_fee_amount
		INSERT INTO scr_unpaid_monies
        SELECT bat.customer_bank_id,
        0, 0, 0,
		SUM(led.amount) process_fee_amount,
		0, 0
		FROM doc eft, batch bat, ledger led, report.trans rtr
		WHERE led.trans_id = rtr.tran_id
		AND bat.batch_id = led.batch_id
		AND bat.doc_id = eft.doc_id (+)
		AND bat.customer_bank_id = cbc.cbid
		AND led.entry_type = 'PF'
		AND led.settle_state_id IN (2,3,6)
		AND led.deleted = 'N'
		AND rtr.server_date < tDate
        AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
		GROUP BY bat.customer_bank_id;
		
		-- Insert service_fee_amount
		INSERT INTO scr_unpaid_monies
        SELECT bat.customer_bank_id,
        0, 0, 0, 0,
		SUM(led.amount) service_fee_amount,
		0
		FROM doc eft, batch bat, ledger led
		WHERE bat.batch_id = led.batch_id
		AND bat.doc_id = eft.doc_id (+)
		AND bat.customer_bank_id = cbc.cbid
		AND led.entry_type = 'SF'
		AND led.settle_state_id IN (2,3,6)
		AND led.deleted = 'N'
		AND led.ledger_date < tDate
        AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
		GROUP BY bat.customer_bank_id;
		
		-- Insert adjust_amount
		INSERT INTO scr_unpaid_monies
        SELECT bat.customer_bank_id,
        0, 0, 0, 0, 0,
		SUM(led.amount) adjust_amount
		FROM doc eft, batch bat, ledger led
		WHERE bat.batch_id = led.batch_id
		AND bat.doc_id = eft.doc_id (+)
		AND bat.customer_bank_id = cbc.cbid
		AND led.entry_type = 'AD'
		AND led.settle_state_id IN (2,3,6)
		AND led.deleted = 'N'
		AND led.ledger_date < tDate
        AND bat.create_date < tDate
		AND (eft.sent_date >= tDate OR bat.doc_id IS NULL)
		GROUP BY bat.customer_bank_id;		
		
    END LOOP;
    
    -- Insert zeros for custermers without unpaid monies for date
    INSERT INTO scr_unpaid_monies
	SELECT cbk.customer_bank_id,
	0 credit_amount,
	0 refund_amount,
	0 chargeback_amount,
	0 process_fee_amount,
	0 service_fee_amount,
	0 adjust_amount
	FROM scr_unpaid_monies tum, customer_bank cbk
	WHERE cbk.customer_bank_id = tum.customer_bank_id (+)
	AND tum.customer_bank_id IS NULL
	  -- exclude Orphan Transactions customers
    AND cbk.customer_bank_id NOT IN (
		SELECT cbk.customer_bank_id
	    FROM customer cus, customer_bank cbk
	    WHERE cbk.customer_id = cus.customer_id
	    AND cus.customer_name LIKE '%Orphan Transactions'
	);
	
	
	-- Insert the the calulated data into the UNPAID_MONIES_RPT table
	INSERT INTO unpaid_monies_rpt (
        customer_bank_id,
        server_date,
        credit_amount,
        refund_amount,
        chargeback_amount,
        process_fee_amount,
        service_fee_amount,
        adjust_amount,
        net_payment_amount
    )		
	SELECT tum.customer_bank_id,
    tDate server_date,
    SUM(tum.credit_amount) credit_amount,
    SUM(tum.refund_amount) refund_amount,
    SUM(tum.chargeback_amount) chargeback_amount,
    SUM(tum.process_fee_amount) process_fee_amount,
    SUM(tum.service_fee_amount) service_fee_amount,
    SUM(tum.adjust_amount) adjust_amount,
    (
    	(SUM(tum.credit_amount) + SUM(tum.refund_amount)
    	+ SUM(tum.chargeback_amount) + SUM(tum.process_fee_amount)
    	+ SUM(tum.service_fee_amount) + SUM(tum.adjust_amount))
    ) net_payment_amount
    FROM scr_unpaid_monies tum
    GROUP BY tum.customer_bank_id;
    
    -- truncate the scratch table
    EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
    
END; -- Procedure
/



-- End of DDL Script for Procedure CORP.POPULATE_UNPAID_MONIES

