CREATE OR REPLACE PACKAGE GLOBALS_PKG
  IS
--
-- Holds any global constants or types
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B Krug       09-16-04 NEW
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;

    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;

END; -- Package spec

/
CREATE OR REPLACE PACKAGE BODY GLOBALS_PKG
IS
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_date_list DATE_LIST := DATE_LIST();
        l_date DATE;
        l_real_end_date DATE;
    BEGIN
        IF (l_days <= 0 AND l_months <= 0) OR l_days + l_months * 30 <= 0 THEN
            RAISE_APPLICATION_ERROR(-20900, 'Invalid days and months specified (Days = '
                || TO_CHAR(l_days) || ', Months = ' || TO_CHAR(l_months)
                || '); would result in endless loop');
        ELSE
            SELECT ADD_MONTHS(TRUNC(l_start_date, l_trunc_to) + (l_days * (1-l_num_before)), l_months * (1-l_num_before)),
                   ADD_MONTHS(l_end_date + (l_days * l_num_after), l_months * l_num_after)
              INTO l_date, l_real_end_date
              FROM DUAL;
            WHILE l_date < l_real_end_date LOOP
                l_date_list.EXTEND;
                l_date_list(l_date_list.LAST) := l_date;
                SELECT ADD_MONTHS(l_date + l_days, l_months)
                  INTO l_date
                  FROM DUAL;
            END LOOP;
        END IF;
        RETURN l_date_list ;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_trunc_to FREQUENCY.INTERVAL%TYPE;
        l_months FREQUENCY.MONTHS%TYPE;
        l_days FREQUENCY.DAYS%TYPE;
    BEGIN
        SELECT INTERVAL, MONTHS, DAYS
          INTO l_trunc_to, l_months, l_days
          FROM FREQUENCY
         WHERE FREQUENCY_ID = l_frequency_id;
         RETURN GET_DATE_LIST(l_start_date,l_end_date,l_trunc_to,l_months,l_days,l_num_before,l_num_after);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20901, 'Invalid frequency id (' || TO_CHAR(l_frequency_id) || ')');
        WHEN OTHERS THEN
            RAISE;
    END;
END;
