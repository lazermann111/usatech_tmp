CREATE OR REPLACE PROCEDURE CORP.LICENSE_INSERT_WITH_FEES (
  po_dealer_id OUT DEALER.DEALER_ID%TYPE,
  po_license_id OUT LICENSE.LICENSE_ID%TYPE,
  pi_dealer_name IN DEALER.DEALER_NAME%TYPE,
  pi_title IN LICENSE.TITLE%TYPE,
  pi_desc IN LICENSE.DESCRIPTION%TYPE,
  pi_license_type IN LICENSE.LICENSE_TYPE%TYPE,
  pi_parent_license_id IN LICENSE.PARENT_LICENSE_ID%TYPE,
  pi_card_pres_pct IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
  pi_card_pres_amt IN LICENSE_PROCESS_FEES.FEE_AMOUNT%TYPE,
  pi_card_not_pres_pct IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
  pi_card_not_pres_amt IN LICENSE_PROCESS_FEES.FEE_AMOUNT%TYPE,
  pi_service_fee_amt IN LICENSE_SERVICE_FEES.AMOUNT%TYPE,
  pi_card_pres_min_amt IN LICENSE_PROCESS_FEES.MIN_AMOUNT%TYPE DEFAULT 0,
  pi_card_not_pres_min_amt IN LICENSE_PROCESS_FEES.MIN_AMOUNT%TYPE DEFAULT 0)
IS
  CARD_PRESENT_TRANS_TYPE_ID CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 16;
  CARD_NOT_PRESENT_TRANS_TYPE_ID CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 13;
  MONTHLY_SERVICE_FEE_ID CORP.SERVICE_FEES.FEE_ID%TYPE := 1;
  ln_frequency_id CORP.FREQUENCY.FREQUENCY_ID%TYPE;
BEGIN
  select max(lsf.frequency_id)
  into ln_frequency_id
  from corp.license_service_fees lsf
  where lsf.license_id = pi_parent_license_id
  and lsf.fee_id = MONTHLY_SERVICE_FEE_ID;
  license_insert(po_license_id, pi_title, pi_desc, pi_license_type, pi_parent_license_id);
  license_pf_update(po_license_id, CARD_PRESENT_TRANS_TYPE_ID, pi_card_pres_pct, pi_card_pres_amt, pi_card_pres_min_amt);
  license_pf_update(po_license_id, CARD_NOT_PRESENT_TRANS_TYPE_ID, pi_card_not_pres_pct, pi_card_not_pres_amt, pi_card_not_pres_min_amt);
  license_sf_update(po_license_id, MONTHLY_SERVICE_FEE_ID, pi_service_fee_amt, null, ln_frequency_id);
  dealer_ins(po_dealer_id, pi_dealer_name);
  dealer_license_ins(po_dealer_id, po_license_id);
END;
/
