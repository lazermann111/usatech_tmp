CREATE OR REPLACE PROCEDURE CORP.SET_TERM_FEES_WITH_COMMISSION (
  pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
  pn_license_id IN CORP.LICENSE.LICENSE_ID%TYPE,
  pn_commission_bank_id IN CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
  pn_card_pres_pf_pct IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_card_pres_pf_amt IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_card_pres_pf_min IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_card_not_pres_pf_pct IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_card_not_pres_pf_amt IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_card_not_pres_pf_min IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_monthly_sf_amt IN CORP.SERVICE_FEES.FEE_AMOUNT%TYPE)
IS
  cln_card_pres_tran_type_id CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 16;
  cln_card_not_pres_tran_type_id CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 13;
  cln_monthly_service_fee_id CORP.SERVICE_FEES.FEE_ID%TYPE := 1;
  cln_monthly_frequency_id CORP.FREQUENCY.FREQUENCY_ID%TYPE := 2;
  ld_effective_date DATE := sysdate;
  lc_license_type CHAR(1);
  ln_parent_license_id CORP.LICENSE.LICENSE_ID%TYPE;
  ln_card_pres_pfc_pct CORP.PROCESS_FEES.FEE_PERCENT%TYPE;
  ln_card_pres_pfc_amt CORP.PROCESS_FEES.FEE_AMOUNT%TYPE;
  ln_card_pres_pfc_min CORP.PROCESS_FEES.MIN_AMOUNT%TYPE;
  ln_card_not_pres_pfc_pct CORP.PROCESS_FEES.FEE_PERCENT%TYPE;
  ln_card_not_pres_pfc_amt CORP.PROCESS_FEES.FEE_AMOUNT%TYPE;
  ln_card_not_pres_pfc_min CORP.PROCESS_FEES.MIN_AMOUNT%TYPE;
  ln_sfc_amt CORP.SERVICE_FEES.FEE_AMOUNT%TYPE;
BEGIN
  IF pn_commission_bank_id is null OR pn_license_id is null THEN
    RAISE_APPLICATION_ERROR(-20001, 'commission_bank_id and license_id are required');
  END IF;
  
  SELECT license_type
  INTO lc_license_type
  FROM corp.license
  WHERE license_id = pn_license_id;
  IF lc_license_type != 'C' THEN
    RAISE_APPLICATION_ERROR(-20002, 'license ' || pn_license_id || ' is not a commissioned license');
  END IF;
  
  SELECT (nvl(pn_card_pres_pf_pct,0) - nvl(ppf.fee_percent,0)),
         (nvl(pn_card_pres_pf_amt,0) - nvl(ppf.fee_amount,0)),
         (nvl(pn_card_pres_pf_min,0) - nvl(ppf.min_amount,0))
    INTO ln_card_pres_pfc_pct, ln_card_pres_pfc_amt, ln_card_pres_pfc_min
    FROM corp.license c
    JOIN corp.license_process_fees cpf ON c.license_id = cpf.license_id
    JOIN corp.license p ON c.parent_license_id = p.license_id
    JOIN corp.license_process_fees ppf ON p.license_id = ppf.license_id
   WHERE cpf.trans_type_id = ppf.trans_type_id
     AND cpf.trans_type_id = cln_card_pres_tran_type_id
     AND c.license_id = pn_license_id;
  CORP.PAYMENTS_PKG.PROCESS_FEES_UPD(pn_terminal_id, cln_card_pres_tran_type_id, pn_card_pres_pf_pct, pn_card_pres_pf_amt, pn_card_pres_pf_min, ld_effective_date, 'N', ln_card_pres_pfc_pct, ln_card_pres_pfc_amt, ln_card_pres_pfc_min, pn_commission_bank_id);
  
  SELECT (nvl(pn_card_not_pres_pf_pct,0) - nvl(ppf.fee_percent,0)),
         (nvl(pn_card_not_pres_pf_amt,0) - nvl(ppf.fee_amount,0)),
         (nvl(pn_card_not_pres_pf_min,0) - nvl(ppf.min_amount,0))
    INTO ln_card_not_pres_pfc_pct, ln_card_not_pres_pfc_amt, ln_card_not_pres_pfc_min
    FROM corp.license c
    JOIN corp.license_process_fees cpf ON c.license_id = cpf.license_id
    JOIN corp.license p ON c.parent_license_id = p.license_id
    JOIN corp.license_process_fees ppf ON p.license_id = ppf.license_id
   WHERE cpf.trans_type_id = ppf.trans_type_id
     AND cpf.trans_type_id = cln_card_not_pres_tran_type_id
     AND c.license_id = pn_license_id;
  CORP.PAYMENTS_PKG.PROCESS_FEES_UPD(pn_terminal_id, cln_card_not_pres_tran_type_id, pn_card_not_pres_pf_pct, pn_card_not_pres_pf_amt, pn_card_not_pres_pf_min, ld_effective_date, 'N', ln_card_not_pres_pfc_pct, ln_card_not_pres_pfc_amt, ln_card_not_pres_pfc_min, pn_commission_bank_id);
  
  SELECT (nvl(pn_monthly_sf_amt,0) - nvl(psf.amount,0))
    INTO ln_sfc_amt
    FROM corp.license c
    JOIN corp.license_service_fees csf ON c.license_id = csf.license_id
    JOIN corp.license p ON c.parent_license_id = p.license_id
    JOIN corp.license_service_fees psf ON p.license_id = psf.license_id
   WHERE csf.fee_id = psf.fee_id
     AND csf.fee_id = cln_monthly_service_fee_id
     AND c.license_id = pn_license_id;
  CORP.PAYMENTS_PKG.SERVICE_FEES_UPD(pn_terminal_id, cln_monthly_service_fee_id, cln_monthly_frequency_id, pn_monthly_sf_amt, 0, ld_effective_date, null, 'N', 60, null, null, null, ln_sfc_amt, pn_commission_bank_id);
END;
/
