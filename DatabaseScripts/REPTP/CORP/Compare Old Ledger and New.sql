alter session set CURRENT_SCHEMA = CORP

select * from VW_PENDING_OR_LOCKED_PAYMENTS
select * from VW_UNPAID_TRANS_AND_FEES
select * from VW_PENDING_REVENUE
select * from VW_LOCKED_PAYMENTS

-- to get old pending or locked payments
SELECT CUSTOMER_NAME, P.CUSTOMER_BANK_ID, 
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, P.PAY_MIN_AMOUNT, CREDIT_AMOUNT, 
REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT, SERVICE_FEE_AMOUNT,  0, '', '' , '' ,
NVL(CREDIT_AMOUNT, 0) + NVL(REFUND_AMOUNT, 0) + NVL(CHARGEBACK_AMOUNT, 0)
 + NVL(PROCESS_FEE_AMOUNT, 0) + NVL(SERVICE_FEE_AMOUNT, 0) + NVL(ADJUST_AMOUNT, 0),
  ADJUST_AMOUNT--, REASON
FROM ( 
--
SELECT CB.CUSTOMER_BANK_ID, BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,     
   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,     
   SERVICE_FEE_AMOUNT,     
   ADJUST_AMOUNT--, PAYMENTS_PKG.FORMAT_EFT_REASON(1, SYSDATE, SYSDATE)
FROM CUSTOMER_BANK CB, (
SELECT customer_bank_id,
       SUM(CASE
              WHEN trans_type_id IN (16, 19) THEN total_amount
              ELSE NULL
           END) CREDIT_AMOUNT,
       SUM(CASE
              WHEN trans_type_id IN (20) THEN total_amount
              ELSE NULL
           END) REFUND_AMOUNT,
       SUM(CASE
               WHEN trans_type_id IN (21) THEN total_amount
               ELSE NULL
           END) CHARGEBACK_AMOUNT,
       -SUM (lp.process_fee) PROCESS_FEE_AMOUNT
FROM (SELECT cbt.customer_bank_id, l.trans_type_id, SUM(l.total_amount) total_amount, SUM(l.total_amount * pf.fee_percent) process_fee
        FROM ledger_old l, customer_bank_terminal cbt, process_fees pf, report.terminal t
       WHERE l.payment_id = 0
		 AND l.trans_type_id IN(16,19,20,21)
	     AND (close_date <= TRUNC (SYSDATE, 'DD') - 1 OR l.trans_type_id IN (20, 21))
         AND l.trans_type_id = pf.trans_type_id (+)
         AND l.terminal_id = pf.terminal_id (+)
         AND within1 (l.close_date, pf.start_date (+), pf.end_date (+)) = 1
         AND l.terminal_id = cbt.terminal_id
         AND within1 (l.close_date, cbt.start_date, cbt.end_date) = 1
         AND cbt.terminal_id = t.terminal_id
         AND t.payment_schedule_id = 1
       GROUP BY cbt.customer_bank_id, l.trans_type_id) lp
GROUP BY customer_bank_id
) /*VW_PENDING_REVENUE*/ PR, (SELECT CBT.CUSTOMER_BANK_ID, -SUM(FEE_AMOUNT) SERVICE_FEE_AMOUNT
FROM PAYMENT_SERVICE_FEE S, CUSTOMER_BANK_TERMINAL CBT  
WHERE PAYMENT_ID = 0 AND CBT.TERMINAL_ID = S.TERMINAL_ID AND STATUS = 'A'  
AND WITHIN1(FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1 
GROUP BY CBT.CUSTOMER_BANK_ID) /*VW_PENDING_SERVICE_FEES*/ PS, (SELECT NVL(PA.CUSTOMER_BANK_ID, CBT.CUSTOMER_BANK_ID) CUSTOMER_BANK_ID, SUM(AMOUNT)  ADJUST_AMOUNT
FROM PAYMENT_ADJUSTMENT PA, CUSTOMER_BANK_TERMINAL CBT  
WHERE PA.EFT_ID = 0 AND PA.TERMINAL_ID = CBT.TERMINAL_ID (+) AND WITHIN1(CREATE_DATE, CBT.START_DATE, CBT.END_DATE) = 1 
GROUP BY NVL(PA.CUSTOMER_BANK_ID, CBT.CUSTOMER_BANK_ID)) /*VW_PENDING_ADJUSTMENTS*/ PA     
WHERE CB.CUSTOMER_BANK_ID = PR.CUSTOMER_BANK_ID (+) AND CB.CUSTOMER_BANK_ID = PS.CUSTOMER_BANK_ID (+)    
AND CB.CUSTOMER_BANK_ID = PA.CUSTOMER_BANK_ID (+)   
AND (CB.STATUS = 'A' OR NVL(CREDIT_AMOUNT, 0) <> 0 OR NVL(REFUND_AMOUNT, 0) <> 0 OR NVL(CHARGEBACK_AMOUNT, 0) <> 0 OR NVL(PROCESS_FEE_AMOUNT, 0) <> 0
OR NVL(SERVICE_FEE_AMOUNT, 0) <> 0 OR NVL(ADJUST_AMOUNT, 0)  <> 0)
--
) /*VW_PENDING_PAYMENTS*/ P, CUSTOMER_BANK CB, CUSTOMER C 
WHERE P.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND CB.CUSTOMER_ID = C.CUSTOMER_ID 
UNION SELECT CUSTOMER_NAME, P.CUSTOMER_BANK_ID, 
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR,  CB.PAY_MIN_AMOUNT, GROSS_AMOUNT, 
REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT, SERVICE_FEE_AMOUNT, 
EFT_ID, 'Locked', BATCH_REF_NBR, P.DESCRIPTION, 
NVL(GROSS_AMOUNT, 0) + NVL(REFUND_AMOUNT, 0) + NVL(CHARGEBACK_AMOUNT, 0) + NVL(PROCESS_FEE_AMOUNT, 0) + NVL(SERVICE_FEE_AMOUNT, 0) + NVL(ADJUST_AMOUNT, 0), 
ADJUST_AMOUNT--, REASON
FROM (SELECT E.CUSTOMER_BANK_ID, E.BANK_ACCT_NBR, E.BANK_ROUTING_NBR, SUM(GROSS_AMOUNT) GROSS_AMOUNT,  
SUM(REFUND_AMOUNT) REFUND_AMOUNT, SUM(CHARGEBACK_AMOUNT) CHARGEBACK_AMOUNT, SUM(PROCESS_FEE_AMOUNT) PROCESS_FEE_AMOUNT, SUM(SERVICE_FEE_AMOUNT) SERVICE_FEE_AMOUNT, E.EFT_ID, BATCH_REF_NBR, DESCRIPTION, PA.AMOUNT ADJUST_AMOUNT  
FROM EFT E, PAYMENTS P, (SELECT EFT_ID, SUM(AMOUNT) AMOUNT FROM PAYMENT_ADJUSTMENT GROUP BY EFT_ID)  PA  
WHERE P.EFT_ID = E.EFT_ID AND E.STATUS = 'L' AND E.EFT_ID = PA.EFT_ID (+)  
GROUP BY E.CUSTOMER_BANK_ID, E.BANK_ACCT_NBR, E.BANK_ROUTING_NBR, E.EFT_ID, BATCH_REF_NBR, DESCRIPTION, PA.AMOUNT) /*VW_LOCKED_PAYMENTS*/ P, CUSTOMER_BANK CB, CUSTOMER C 
WHERE P.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND CB.CUSTOMER_ID = C.CUSTOMER_ID



----------
SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT,
           SUM(CASE WHEN PAYABLE = 'N' THEN AMOUNT ELSE NULL END) FAILED_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) PAYABLE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B
          WHERE L.BATCH_ID = B.BATCH_ID
          AND L.DELETED = 'N'
          AND B.CLOSED = 'N'
          AND PAYMENTS_PKG.BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE) = 'Y'
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE)) M
      GROUP BY CUSTOMER_BANK_ID
