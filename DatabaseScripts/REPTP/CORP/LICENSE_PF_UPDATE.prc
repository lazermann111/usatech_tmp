CREATE OR REPLACE PROCEDURE CORP.LICENSE_PF_UPDATE 
(
  pn_license_id CORP.LICENSE_PROCESS_FEES.LICENSE_ID%TYPE,
  pn_trans_type_id CORP.LICENSE_PROCESS_FEES.TRANS_TYPE_ID%TYPE,
  pn_fee_percent CORP.LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_fee_amount CORP.LICENSE_PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_min_amount CORP.LICENSE_PROCESS_FEES.MIN_AMOUNT%TYPE
) AS 
BEGIN
    DELETE FROM CORP.LICENSE_PROCESS_FEES
     WHERE LICENSE_ID = pn_license_id
       AND TRANS_TYPE_ID = pn_trans_type_id;
    IF NVL(pn_fee_percent, 0) != 0 OR NVL(pn_fee_amount, 0) != 0 OR NVL(pn_min_amount, 0) > 0 THEN
        INSERT INTO CORP.LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT)
          VALUES(pn_license_id, pn_trans_type_id, NVL(pn_fee_percent, 0), NVL(pn_fee_amount, 0),  NVL(pn_min_amount, 0));
    END IF;
END;
/
