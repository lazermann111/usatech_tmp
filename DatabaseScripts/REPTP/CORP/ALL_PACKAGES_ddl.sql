-- Start of DDL Script for Package CORP.GLOBALS_PKG
-- Generated 13-Oct-2004 13:42:54 from CORP@USADBD02

CREATE OR REPLACE 
PACKAGE globals_pkg
  IS
--
-- Holds any global constants or types
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B Krug       09-16-04 NEW
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;

    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;

END; -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON globals_pkg TO public
WITH GRANT OPTION
/

CREATE OR REPLACE 
PACKAGE BODY globals_pkg
IS
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_date_list DATE_LIST := DATE_LIST();
        l_date DATE;
        l_real_end_date DATE;
    BEGIN
        IF (l_days <= 0 AND l_months <= 0) OR l_days + l_months * 30 <= 0 THEN
            RAISE_APPLICATION_ERROR(-20900, 'Invalid days and months specified (Days = '
                || TO_CHAR(l_days) || ', Months = ' || TO_CHAR(l_months)
                || '); would result in endless loop');
        ELSE
            SELECT ADD_MONTHS(TRUNC(l_start_date, l_trunc_to) + (l_days * (1-l_num_before)), l_months * (1-l_num_before)),
                   ADD_MONTHS(l_end_date + (l_days * l_num_after), l_months * l_num_after)
              INTO l_date, l_real_end_date
              FROM DUAL;
            WHILE l_date < l_real_end_date LOOP
                l_date_list.EXTEND;
                l_date_list(l_date_list.LAST) := l_date;
                SELECT ADD_MONTHS(l_date + l_days, l_months)
                  INTO l_date
                  FROM DUAL;
            END LOOP;
        END IF;
        RETURN l_date_list ;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_trunc_to FREQUENCY.INTERVAL%TYPE;
        l_months FREQUENCY.MONTHS%TYPE;
        l_days FREQUENCY.DAYS%TYPE;
    BEGIN
        SELECT INTERVAL, MONTHS, DAYS
          INTO l_trunc_to, l_months, l_days
          FROM FREQUENCY
         WHERE FREQUENCY_ID = l_frequency_id;
         RETURN GET_DATE_LIST(l_start_date,l_end_date,l_trunc_to,l_months,l_days,l_num_before,l_num_after);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20901, 'Invalid frequency id (' || TO_CHAR(l_frequency_id) || ')');
        WHEN OTHERS THEN
            RAISE;
    END;
END;
/


-- End of DDL Script for Package CORP.GLOBALS_PKG

-- Start of DDL Script for Package CORP.PAYMENTS_PKG
-- Generated 13-Oct-2004 13:42:55 from CORP@USADBD02

CREATE OR REPLACE 
PACKAGE payments_pkg
  IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW

   Q_SYNC_COMSUMER CONSTANT SYS.AQ$_AGENT := SYS.AQ$_AGENT('LEDGER', 'REPORT.Q_SYNC_MSG', 0);
    
   /*PROCEDURE AUTO_CREATE_EFTS;
   PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N');

   PROCEDURE REFRESH_PENDING_REVENUE;
   */

   FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR;
   /*
   FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR;
     */
   FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_max_pay_date PAYMENTS.PAYMENT_END_DATE%TYPE
     ) RETURN VARCHAR;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;
   /*
   FUNCTION GET_NOT_PAYABLE_COUNT(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE,
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_id%TYPE,
        l_close_date LEDGER.CLOSE_DATE%TYPE)
     RETURN NUMBER
     PARALLEL_ENABLE;
*/

  PROCEDURE UPDATE_LEDGER(
        l_trans_rec    TRANS%ROWTYPE);
        
  PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N');
        
  FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_ledger_date LEDGER.LEDGER_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE;
     
  PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE);
      
  PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT LEDGER.LEDGER_ID%TYPE,
       	l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN EFT.APPROVE_BY%TYPE);
      
  PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE SCAN_FOR_SERVICE_FEES;
  
  PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_ledger_date LEDGER.ledger_date%TYPE)
     RETURN BATCH.BATCH_ID%TYPE;
     
  FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE;

  FUNCTION RUN_SYNC_LEDGER_JOB
     RETURN BINARY_INTEGER;
END; -- Package spec
/


CREATE OR REPLACE 
PACKAGE BODY payments_pkg
IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_ledger_date LEDGER.LEDGER_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        IF l_always_as_accum IN('Y') THEN
            l_pay_sched := 1;
        ELSE
            SELECT PAYMENT_SCHEDULE_ID
              INTO l_pay_sched
              FROM TERMINAL
             WHERE TERMINAL_ID = l_terminal_id;
        END IF;
        BEGIN
            SELECT B.BATCH_ID
              INTO l_batch_id
              FROM BATCH B
             WHERE B.TERMINAL_ID = l_terminal_id
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
               AND B.CLOSED = 'N'
               AND CASE
                    WHEN l_pay_sched = 1 THEN MAX_DATE -- for "As Accumulated", batch start date does not matter
                    ELSE l_ledger_date
                END >= B.START_DATE
               AND l_ledger_date < NVL(B.END_DATE, MAX_DATE);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- calc start and end dates
                IF l_pay_sched = 1 THEN
                    SELECT LEAST(NVL(MAX(B.END_DATE), MIN_DATE), l_ledger_date)
                      INTO l_start_date
                      FROM BATCH B
                     WHERE B.TERMINAL_ID = l_terminal_id
                       AND B.CUSTOMER_BANK_ID = l_cust_bank_id
                       AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                    -- end date is null
                ELSIF l_pay_sched = 2 THEN
                    SELECT NVL(MAX(f.fill_date), MIN_DATE)
                      INTO l_start_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE <= l_ledger_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                    SELECT MIN(f.fill_date)
                      INTO l_end_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_ledger_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                ELSE
                    SELECT TRUNC(l_ledger_date, INTERVAL),
                           ADD_MONTHS(TRUNC(l_ledger_date, INTERVAL) + DAYS, MONTHS)
                      INTO l_start_date, l_end_date
                      FROM PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                    IF l_end_date <= l_ledger_date THEN -- trouble, should not happen
                        l_end_date := l_ledger_date + (1.0/(24*60*60));
                    END IF;
                END IF;
                -- create new batch record
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, CUSTOMER_BANK_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE)
                    VALUES(l_batch_id, l_cust_bank_id, l_terminal_id, l_pay_sched, l_start_date, l_end_date);
            WHEN OTHERS THEN
                RAISE;
        END;
        RETURN l_batch_id;
    END;

    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_ledger_date LEDGER.ledger_date%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_new_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        SELECT GET_OR_CREATE_BATCH(B.TERMINAL_ID, B.CUSTOMER_BANK_ID, l_ledger_date, 'Y')
          INTO l_new_batch_id
          FROM BATCH B
          WHERE B.BATCH_ID = l_batch_id;
        RETURN l_new_batch_id;
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_cnt NUMBER;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE
          INTO l_pay_sched, l_end_date
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        IF l_pay_sched = 1 THEN
            RETURN 'Y';
        ELSIF l_end_date IS NULL THEN -- batch is fill-to-fill and not closed yet
            RETURN 'N';
        END IF;
        SELECT COUNT(*)
          INTO l_cnt
          FROM LEDGER l
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
           AND L.DELETED = 'N'
           AND L.BATCH_ID = l_batch_id;
        IF l_cnt = 0 THEN
            RETURN 'Y';
        END IF;
        RETURN 'N';
    END;

/*
            IF NVL(l_mn_rec.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0) OR
               NVL(l_mn_rec.CUSTOMER_BANK_ID, 0) <> NVL(l_cust_bank_id, 0) THEN
                -- batch has changed so let's delete and start again
            ELSIF NVL(l_mn_rec.PROCESS_FEE_ID, 0) <> NVL(l_process_fee_id, 0) THEN
                IF l_pf_rec IS NOT NULL THEN
                    --update pf rec
                    UPDATE LEDGER SET PROCESS_FEE_ID = l_process_fee_id,
                        AMOUNT = (SELECT l_trans_rec.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                                    FROM PROCESS_FEES PF
                                    WHERE PF.PROCESS_FEE_ID = l_process_fee_id
                        ),
                        LEDGER_DATE = l_trans_rec.CLOSE_DATE,
                        SETTLE_STATE_ID = l_trans_rec.SETTLE_STATE_ID
                     WHERE LEDGER_ID = l_pf_rec.LEDGER_ID;
                ELSE -- insert pf
                    INSERT INTO LEDGER(LEDGER_ID, TERMINAL_ID, ENTRY_TYPE, TRANS_ID,
                        PROCESS_FEE_ID, SERVICE_FEE_ID, CUSTOMER_BANK_ID, AMOUNT,
                        BATCH_ID, SETTLE_STATE_ID, LEDGER_DATE)
                        SELECT LEDGER_SEQ.NEXTVAL, l_terminal_id, 'PF', l_trans_rec.TRAN_ID,
                            l_process_fee_id, NULL, l_cust_bank_id, l_trans_rec.TOTAL_AMOUNT * pf.FEE_PERCENT + pf.FEE_AMOUNT,
                            l_mn_rec.BATCH_ID, l_trans_rec.SETTLE_STATE_ID, l_trans_rec.CLOSE_DATE
                          FROM PROCESS_FEES pf
                          WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                END IF;
            ELSIF NVL(l_old_pf_id, 0) <> NVL(l_process_fee_id, 0) THEN
                IF l_pf_rec IS NOT NULL THEN
                    --update pf rec
                    UPDATE LEDGER SET PROCESS_FEE_ID = l_process_fee_id,
                        AMOUNT = (SELECT l_trans_rec.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                                    FROM PROCESS_FEES PF
                                    WHERE PF.PROCESS_FEE_ID = l_process_fee_id
                        )
                     WHERE LEDGER_ID = l_pf_rec.LEDGER_ID;
             END IF;
*/
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_rec    TRANS%ROWTYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_batch_closed BATCH.CLOSED%TYPE;
    BEGIN
        IF l_trans_rec.TRANS_TYPE_ID IN(22) THEN -- cash; ignore
            RETURN;
        END IF;
        -- get the batch & status
        BEGIN
            SELECT DISTINCT B.BATCH_ID, B.CLOSED
              INTO l_batch_id, l_batch_closed
              FROM LEDGER L, BATCH B
             WHERE L.TRANS_ID = l_trans_rec.TRAN_ID
               AND L.BATCH_ID = B.BATCH_ID;
            IF l_batch_closed = 'Y' THEN -- trouble
                RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_rec.TRAN_ID)||' is in a batch that was already closed - cannot update ledger table!');
            END IF;
            -- To make it easy let's just delete what's there and re-insert
            DELETE FROM LEDGER WHERE TRANS_ID = l_trans_rec.TRAN_ID;
            -- If terminal is null then no need to do anything more
            IF l_trans_rec.TERMINAL_ID IS NULL THEN
                RETURN;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- Create new batch for this ledger record
                -- If terminal is null then no need to do anything more
                IF l_trans_rec.TERMINAL_ID IS NULL THEN
                    RETURN;
                END IF;
                -- if refund or chargeback get the batch that the
                -- original trans is in, if open use it else use "as accumulated"
                IF l_trans_rec.TRANS_TYPE_ID IN(20, 21) THEN
                    BEGIN
                        SELECT B.BATCH_ID, B.CLOSED
                          INTO l_batch_id, l_batch_closed
                          FROM LEDGER L, BATCH B, TRANS T
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_rec.TRAN_ID
                           AND L.BATCH_ID = B.BATCH_ID;
                        IF NVL(l_batch_closed, 'Y') = 'Y' THEN
                            l_batch_id := GET_OR_CREATE_BATCH(l_trans_rec.TERMINAL_ID, l_trans_rec.CUSTOMER_BANK_ID, l_trans_rec.CLOSE_DATE, 'Y');
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                            RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_rec.TRAN_ID)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                        WHEN OTHERS THEN
                            RAISE;
                    END;
                ELSE
                    l_batch_id := GET_OR_CREATE_BATCH(l_trans_rec.TERMINAL_ID, l_trans_rec.CUSTOMER_BANK_ID, l_trans_rec.CLOSE_DATE, 'N');
                END IF;
            WHEN OTHERS THEN
                RAISE;
        END;

        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            TRANS_ID,
            PROCESS_FEE_ID,
            AMOUNT,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE)
        SELECT
            LEDGER_SEQ.NEXTVAL,
            CASE
                WHEN l_trans_rec.TRANS_TYPE_ID IN(16,19) THEN 'CC'
                WHEN l_trans_rec.TRANS_TYPE_ID = 20 THEN 'RF'
                WHEN l_trans_rec.TRANS_TYPE_ID = 21 THEN 'CB'
                --ELSE 'AU' -- Audit trail of other transactions
            END,
            l_trans_rec.TRAN_ID,
            l_trans_rec.PROCESS_FEE_ID,
            CASE
                WHEN l_trans_rec.TRANS_TYPE_ID IN(20,21) THEN -ABS(l_trans_rec.TOTAL_AMOUNT)
                ELSE l_trans_rec.TOTAL_AMOUNT END,
            l_batch_id,
            l_trans_rec.SETTLE_STATE_ID,
            l_trans_rec.CLOSE_DATE
          FROM DUAL
          WHERE l_trans_rec.TRANS_TYPE_ID IN(16,19,20,21);

        IF l_trans_rec.PROCESS_FEE_ID IS NOT NULL THEN
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'PF',
                l_trans_rec.TRAN_ID,
                l_trans_rec.PROCESS_FEE_ID,
                -(ABS(l_trans_rec.TOTAL_AMOUNT) * pf.FEE_PERCENT + pf.FEE_AMOUNT),
                l_batch_id,
                l_trans_rec.SETTLE_STATE_ID,
                l_trans_rec.CLOSE_DATE
            FROM PROCESS_FEES pf
            WHERE pf.PROCESS_FEE_ID = l_trans_rec.PROCESS_FEE_ID;
        END IF;
    END;

    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_rec    FILL%ROWTYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.START_DATE
              FROM BATCH B, TERMINAL_EPORT TE
             WHERE B.START_DATE < l_fill_rec.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > l_fill_rec.FILL_DATE
               AND B.CLOSED = 'N'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND B.TERMINAL_ID = TE.TERMINAL_ID
               AND TE.EPORT_ID = l_fill_rec.EPORT_ID
               AND l_fill_rec.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND l_fill_rec.FILL_DATE < NVL(te.END_DATE, MAX_DATE);
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(f.FILL_DATE)
              INTO l_end_date
              FROM FILL F, TERMINAL_EPORT TE
             WHERE F.FILL_DATE > l_batch_rec.START_DATE
               AND F.EPORT_ID = TE.EPORT_ID
               AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE)
               AND TE.TERMINAL_ID = l_batch_rec.TERMINAL_ID;
            -- update batch record
            UPDATE BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
             
            -- also update ledger.batch_id of entries that are after fill_date
            UPDATE LEDGER L SET L.BATCH_ID =
                GET_OR_CREATE_BATCH(l_batch_rec.TERMINAL_ID, l_batch_rec.CUSTOMER_BANK_ID, L.LEDGER_DATE,'N')
             WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
               AND L.LEDGER_DATE >= l_end_date;
         END LOOP;
    END;
    
    -- Creates a document (eft or invoice or check) for the given bank account
    -- doc_type: 'FT' = Funds Transfer (EFT); 'IN' = Invoice; 'CK' = Check
    PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_doc_id        DOC.DOC_ID%TYPE;
        l_batch_ref	    DOC.REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         DOC.TOTAL_AMOUNT%TYPE;
        l_dates         DATE_LIST;
        l_now           DATE := SYSDATE;
    BEGIN
        IF l_check_min = 'Y' THEN
            -- ensure that doc amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(L.AMOUNT), 0) INTO l_total
              FROM LEDGER L, BATCH B
             WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE NOT IN('AU')
               AND BATCH_CLOSABLE(B.BATCH_ID) = 'Y';
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'Doc total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        END IF;

        -- Create Doc
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_doc_id, l_batch_ref
          FROM DUAL;
        INSERT INTO DOC(DOC_ID, DOC_TYPE, REF_NBR, DESCRIPTION, CUSTOMER_BANK_ID,
            BANK_ACCT_NBR, BANK_ROUTING_NBR, CREATE_BY)
            SELECT l_doc_id, l_doc_type, l_batch_ref,
                NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, CUSTOMER_BANK_ID,
                BANK_ACCT_NBR, BANK_ROUTING_NBR, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        -- Moves batches into doc
        UPDATE BATCH B SET B.DOC_ID = l_doc_id
         WHERE B.CUSTOMER_BANK_ID = l_cust_bank_id
           AND B.CLOSED = 'N'
           AND BATCH_CLOSABLE(B.BATCH_ID) = 'Y';
        -- remove any unsettled trans from as accumulated batches
        UPDATE LEDGER L SET L.BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.LEDGER_DATE)
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
           AND L.BATCH_ID IN(SELECT B.BATCH_ID
                FROM BATCH B
                WHERE B.PAYMENT_SCHEDULE_ID = 1
                  AND B.DOC_ID = l_doc_id);
    END; -- end of procedure create_doc(...)

/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/
/* No longer need to do this
    PROCEDURE CREATE_PAYMENT(
        l_eft_id        EFT.EFT_ID%TYPE,
        l_cust_bank_id  CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id   PAYMENTS.TERMINAL_ID%TYPE,
        l_pay_sched     PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_start_date    PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_end_date      PAYMENTS.PAYMENT_END_DATE%TYPE)
    IS
       l_payment_id    PAYMENTS.PAYMENT_ID%TYPE;
       l_gross         PAYMENTS.GROSS_AMOUNT%TYPE;
       l_process       PAYMENTS.PROCESS_FEE_AMOUNT%TYPE;
       l_refund        PAYMENTS.REFUND_AMOUNT%TYPE;
       l_chargeback    PAYMENTS.CHARGEBACK_AMOUNT%TYPE;
       l_service       PAYMENTS.SERVICE_FEE_AMOUNT%TYPE;
       l_net           PAYMENTS.NET_AMOUNT%TYPE;
       l_tran_cnt      NUMBER;
       l_unproc_cnt    NUMBER;
       l_fees_cnt      NUMBER;
    BEGIN
        IF l_end_date IS NULL OR NVL(l_start_date, MIN_DATE) >= NVL(l_end_date, MAX_DATE) THEN
            RETURN;
        END IF;
            
        -- check that a transaction exists and that no unprocessed transactions exist in this period
        SELECT COUNT(*)
          INTO l_fees_cnt
          FROM PAYMENT_SERVICE_FEE
         WHERE PAYMENT_ID = 0
 		   AND TERMINAL_ID = l_terminal_id
           AND STATUS = 'A'
		   AND FEE_DATE >= NVL(l_start_date, MIN_DATE) AND FEE_DATE < l_end_date;
		IF l_pay_sched <> 1 THEN
            -- Check if transaction will be included
            SELECT COUNT(*)
              INTO l_tran_cnt
              FROM LEDGER l
             WHERE l.PAID_STATUS = 'U' -- unpaid
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT COUNT(*)
              INTO l_unproc_cnt
              FROM LEDGER l
             WHERE l.PAID_STATUS = 'N' -- not payable
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        
        IF (l_pay_sched = 1 AND l_fees_cnt > 0) OR (l_unproc_cnt = 0 AND l_tran_cnt > 0) THEN                		
           -- Create payment record
            SELECT PAYMENT_SEQ.NEXTVAL INTO l_payment_id FROM DUAL;
	        INSERT INTO PAYMENTS(EFT_ID, PAYMENT_ID, TERMINAL_ID,
                   PAYMENT_SCHEDULE_ID, PAYMENT_START_DATE, PAYMENT_END_DATE, CREATE_DATE)
                SELECT l_eft_id, l_payment_id, l_terminal_id,
                   l_pay_sched, l_start_date, l_end_date, SYSDATE FROM DUAL;
                   
            -- Add in service fees
            IF l_fees_cnt > 0 THEN
                UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = l_payment_id
                 WHERE PAYMENT_ID = 0
    	 		   AND TERMINAL_ID = l_terminal_id
                   AND STATUS = 'A'
    			   AND FEE_DATE >= NVL(l_start_date, MIN_DATE) AND FEE_DATE < l_end_date;
    		END IF;

            UPDATE LEDGER l SET PAYMENT_ID = l_payment_id
             WHERE l.PAID_STATUS = 'U'
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date;
               
	        INSERT INTO PAYMENT_PROCESS_FEE(PAYMENT_ID, CUSTOMER_BANK_ID, TERMINAL_ID,
                   TRANS_TYPE_ID, GROSS_AMOUNT, FEE_PERCENT, TRAN_COUNT, TRAN_FEE_AMOUNT,
                   FEE_AMOUNT, NET_AMOUNT)
	 		    SELECT l_payment_id, l_cust_bank_id, l_terminal_id,
                       TRANS_TYPE_ID, GROSS_AMOUNT, FEE_PERCENT, TRAN_COUNT, FEE_AMOUNT,
                       NVL(GROSS_AMOUNT * FEE_PERCENT, 0) + NVL(TRAN_COUNT * FEE_AMOUNT, 0),
                       GROSS_AMOUNT - NVL(GROSS_AMOUNT * FEE_PERCENT, 0) + NVL(TRAN_COUNT * FEE_AMOUNT, 0)
			      FROM (SELECT l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) GROSS_AMOUNT, NVL(l.FEE_PERCENT, 0) FEE_PERCENT,
                       COUNT(*) TRAN_COUNT, NVL(l.FEE_AMOUNT, 0) FEE_AMOUNT
			      FROM LEDGER l
                 WHERE l.payment_id = l_payment_id
                   AND l.terminal_id = l_terminal_id
                   AND l.customer_bank_id = l_cust_bank_id
			     GROUP BY l.trans_type_id, l.fee_percent, l.fee_amount);
            			
    		 --CALCULATE TOTAL AMOUNTS
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_gross FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID IN(16,19); --ONLY CREDIT AND DEBIT
    		 SELECT -SUM(FEE_AMOUNT) INTO l_process FROM PAYMENT_PROCESS_FEE WHERE PAYMENT_ID = l_payment_id;
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_refund FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID = 20;
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_chargeback FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID = 21;
    		 SELECT -SUM(FEE_AMOUNT) INTO l_service FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_ID = l_payment_id;
    		 -- Calculate Net Amount
    		 l_net := NVL(l_gross, 0) + NVL(l_process, 0) + NVL(l_refund, 0) + NVL(l_chargeback, 0) + NVL(l_service, 0);
    		 -- Insert the records into payments
    		UPDATE PAYMENTS SET (GROSS_AMOUNT, PROCESS_FEE_AMOUNT, REFUND_AMOUNT,
                    CHARGEBACK_AMOUNT, SERVICE_FEE_AMOUNT, NET_AMOUNT) =
                    (SELECT l_gross, l_process, l_refund,
                     l_chargeback, l_service, l_net FROM DUAL)
             WHERE PAYMENT_ID = l_payment_id;
   	    END IF;
    END; -- end of create_payment proc
*/
/*    PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_eft_id        EFT.EFT_ID%TYPE;
        l_batch_ref	    EFT.BATCH_REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         EFT.EFT_AMOUNT%TYPE;
        l_dates         DATE_LIST;
        l_now           DATE := SYSDATE;
        CURSOR c_payments IS
                SELECT t.TERMINAL_ID, t.PAYMENT_SCHEDULE_ID,
                       MIN(l.CLOSE_DATE) MIN_DATE, MAX(l.CLOSE_DATE) MAX_DATE
                  FROM LEDGER l, TERMINAL t
                 WHERE l.PAID_STATUS = 'U'
                   AND l.TERMINAL_ID = t.TERMINAL_ID
                   AND l.CUSTOMER_BANK_ID = l_cust_bank_id
                 GROUP BY t.TERMINAL_ID, t.PAYMENT_SCHEDULE_ID;
    BEGIN
        -- Create EFT
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_eft_id, l_batch_ref
          FROM DUAL;
        INSERT INTO EFT(EFT_ID, CUSTOMER_BANK_ID, BATCH_REF_NBR, BANK_ACCT_NBR, BANK_ROUTING_NBR, DESCRIPTION, CREATE_BY)
            SELECT l_eft_id, CUSTOMER_BANK_ID, l_batch_ref, BANK_ACCT_NBR,
                   BANK_ROUTING_NBR, NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;

        -- Create Payments for each customer bank / terminal / pay schedule combination
        FOR r_payments IN c_payments LOOP
            -- Load all dates
            IF r_payments.payment_schedule_id = 1 THEN -- for as accumulated
                l_dates := DATE_LIST(r_payments.min_date, l_now);
            ELSIF r_payments.payment_schedule_id = 2 THEN -- for fill to fill
                l_dates := DATE_LIST();
                l_dates.EXTEND;
                SELECT MAX(f.fill_date)
                  INTO l_dates(l_dates.LAST)
                  FROM FILL f, TERMINAL_EPORT te
                 WHERE f.FILL_DATE < r_payments.min_date
                   AND f.EPORT_ID = te.EPORT_ID
                   AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                   AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                   AND te.TERMINAL_ID = r_payments.terminal_id;
                DECLARE
                    CURSOR c_dates IS
                        SELECT f.fill_date
                          FROM FILL f, TERMINAL_EPORT te
                         WHERE f.fill_date BETWEEN r_payments.min_date AND r_payments.max_date
                           AND f.eport_id = te.eport_id
                           AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                           AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                           AND te.terminal_id = r_payments.terminal_id
                         ORDER BY f.fill_date;
                BEGIN
                    FOR r_dates IN c_dates LOOP
                        l_dates.EXTEND;
                        l_dates(l_dates.LAST) := r_dates.fill_date;
                    END LOOP;
                END;
                l_dates.EXTEND;
                SELECT MIN(fill_date)
                  INTO l_dates(l_dates.LAST)
                  FROM FILL f, TERMINAL_EPORT te
                 WHERE f.fill_date > r_payments.max_date
                   AND f.eport_id = te.eport_id
                   AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                   AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                   AND te.terminal_id = r_payments.terminal_id;
            ELSE
                SELECT GLOBALS_PKG.GET_DATE_LIST(r_payments.min_date, r_payments.max_date,
                       INTERVAL, MONTHS, DAYS, 1, 0)
                  INTO l_dates
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = r_payments.payment_schedule_id;
            END IF;

            DECLARE
                CURSOR c_periods IS
                    SELECT DISTINCT COLUMN_VALUE FROM TABLE(CAST(l_dates AS DATE_LIST)) WHERE COLUMN_VALUE IS NOT NULL ORDER BY 1;
                l_start_date DATE;
                l_end_date DATE;
            BEGIN
                OPEN c_periods;
                FETCH c_periods INTO l_start_date;
                IF NOT c_periods%NOTFOUND THEN
                    LOOP
                        FETCH c_periods INTO l_end_date;                   	
                        EXIT WHEN c_periods%NOTFOUND;
                   	    CREATE_PAYMENT(l_eft_id, l_cust_bank_id, r_payments.terminal_id,
                            r_payments.payment_schedule_id, l_start_date, l_end_date);
                        l_start_date := l_end_date;
                    END LOOP;
                END IF;
                CLOSE c_periods;
            EXCEPTION
                WHEN OTHERS THEN
                   CLOSE c_periods;
                   RAISE;
            END;
        END LOOP;

        IF l_check_min = 'Y' THEN
            -- ensure that eft amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(NET_AMOUNT), 0) + NVL(SUM(pa.AMOUNT), 0) INTO l_total
              FROM PAYMENTS p, PAYMENT_ADJUSTMENT pa
             WHERE p.EFT_ID(+) = l_eft_id
               AND pa.CUSTOMER_BANK_ID(+) = l_cust_bank_id
               AND pa.EFT_ID(+) = 0;
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'EFT total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        ELSE
            UPDATE PAYMENT_ADJUSTMENT SET EFT_ID = l_eft_id
             WHERE EFT_ID = 0 AND CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
    END; -- end of procedure create_eft(...)
            
    PROCEDURE AUTO_CREATE_EFTS
    IS
       CURSOR c_efts IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_efts IN c_efts LOOP
            BEGIN
                CREATE_EFT(r_efts.customer_bank_id, NULL, 'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

    PROCEDURE REFRESH_PENDING_REVENUE
    AS
    BEGIN
        DELETE FROM V$PENDING_REVENUE;
        INSERT INTO V$PENDING_REVENUE(CUSTOMER_BANK_ID, CREDIT_AMOUNT, REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT)
            SELECT CUSTOMER_BANK_ID, CREDIT_AMOUNT, REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT
            FROM VW_PENDING_REVENUE;
    END;
*/
    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       /* OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   (SELECT - SUM(S.FEE_AMOUNT) SERVICE_FEE_AMOUNT
                      FROM PAYMENT_SERVICE_FEE S, CUSTOMER_BANK_TERMINAL CBT, EFT E, PAYMENTS P
                     WHERE CBT.TERMINAL_ID = S.TERMINAL_ID
                       AND CBT.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
                       AND S.STATUS = 'A'
                       AND WITHIN1(FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                       AND FEE_DATE < l_as_of
                       AND S.PAYMENT_ID = P.PAYMENT_ID (+)
                       AND P.EFT_ID = E.EFT_ID (+)
                       AND NVL(E.STATUS, '') NOT IN ('P', 'S', 'D')
                       AND NVL(E.EFT_DATE, MAX_DATE) >= l_as_of
                   ) AS SERVICE_FEE_AMOUNT,
                   (SELECT SUM(PA.AMOUNT)
                      FROM PAYMENT_ADJUSTMENT PA, EFT E
                     WHERE PA.EFT_ID = E.EFT_ID (+)
                       AND NVL(E.STATUS, '') NOT IN ('P', 'S', 'D')
                       AND NVL(E.EFT_DATE, MAX_DATE) >= l_as_of
                       AND PA.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
                   ) AS ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB,
                        (SELECT CUSTOMER_BANK_ID,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (16, 19) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) CREDIT_AMOUNT,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (20) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) REFUND_AMOUNT,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (21) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) CHARGEBACK_AMOUNT,
                             -SUM(PROCESS_FEE_AMOUNT) PROCESS_FEE_AMOUNT
                         FROM (SELECT CUSTOMER_BANK_ID, TRANS_TYPE_ID, SUM(TOTAL_AMOUNT) TOTAL_AMOUNT, SUM(TOTAL_AMOUNT * FEE_PERCENT + FEE_AMOUNT) PROCESS_FEE_AMOUNT
                                FROM (SELECT L.CUSTOMER_BANK_ID, L.TRANS_TYPE_ID, L.TOTAL_AMOUNT, L.FEE_PERCENT, L.FEE_AMOUNT
                                          FROM LEDGER L, EFT E, PAYMENTS P
                                         WHERE L.PAYMENT_ID = P.PAYMENT_ID
                                           AND P.EFT_ID = E.EFT_ID
                                           AND E.STATUS NOT IN ('P', 'S', 'D')
                                           AND E.EFT_DATE >= l_as_of
                                           AND L.LEDGER_DATE < l_as_of
                                       UNION ALL
                                       SELECT L.CUSTOMER_BANK_ID, L.TRANS_TYPE_ID, L.TOTAL_AMOUNT, L.FEE_PERCENT, L.FEE_AMOUNT
                                       FROM LEDGER L
                                       WHERE L.PAID_STATUS = 'U' AND L.LEDGER_DATE < l_as_of
                                       ) L
                               GROUP BY CUSTOMER_BANK_ID, TRANS_TYPE_ID
                               ) L
                           GROUP BY CUSTOMER_BANK_ID
                          ) V
                  WHERE CB.CUSTOMER_BANK_ID = V.CUSTOMER_BANK_ID (+)
                    AND CB.CUSTOMER_ID = C.CUSTOMER_ID
                  ORDER BY CUSTOMER_NAME, BANK_ACCT_NBR;*/
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_max_pay_date PAYMENTS.PAYMENT_END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        UPDATE LEDGER L
           SET BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.LEDGER_DATE)
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT LEDGER.LEDGER_ID%TYPE,
       	l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 SELECT LEDGER_SEQ.NEXTVAL INTO l_ledger_id FROM DUAL;
    	 INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            AMOUNT,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION,
            CREATE_BY)
           VALUES (
            l_ledger_id,
            'AD',
            l_amt,
            GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id, SYSDATE, 'Y'),
            2 /*process-no require*/,
            SYSDATE,
            l_reason,
            l_user_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
    END;
    
    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC D SET APPROVE_BY = l_user_id, UPDATE_DATE= SYSDATE, STATUS = 'A',
            TOTAL_AMOUNT = (SELECT SUM(AMOUNT)
                FROM LEDGER L, BATCH B
               WHERE L.DELETED = 'N'
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.DOC_ID = D.DOC_ID)
  	 		WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN EFT.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC SET SENT_BY = l_user_id, UPDATE_DATE = SYSDATE, STATUS = 'P',
                SENT_DATE = SYSDATE
          WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_effective_date < SYSDATE THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    --AND L.DELETED = 'N'
 			        AND L.LEDGER_DATE >= l_effective_date
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.CLOSED = 'Y';
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
    	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
     	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_effective_date);
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   --FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   SF.END_DATE,
                   F.FEE_NAME
              FROM SERVICE_FEES SF, FREQUENCY Q, CUSTOMER_BANK_TERMINAL CBT, FEES F
             WHERE SF.FREQUENCY_ID = Q.FREQUENCY_ID
               AND SF.TERMINAL_ID = CBT.TERMINAL_ID
               AND SF.FEE_ID = F.FEE_ID
               AND WITHIN1(SYSDATE, CBT.START_DATE, CBT.END_DATE) = 1
               AND (Q.DAYS <> 0 OR Q.MONTHS <> 0)
               AND ADD_MONTHS(SF.LAST_PAYMENT + Q.DAYS, Q.MONTHS) < LEAST(NVL(SF.END_DATE, MAX_DATE), SYSDATE);

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
    BEGIN
        FOR r_fee IN c_fee LOOP
            l_last_payment := r_fee.LAST_PAYMENT;
            IF r_fee.MONTHS >= 12  AND MOD(r_fee.MONTHS, 12.0) = 0 THEN
                l_fmt := 'FMYYYY';
            ELSIF r_fee.MONTHS > 0 THEN
                l_fmt := 'FMMonth, YYYY';
            ELSIF r_fee.DAYS >= 1 AND MOD(r_fee.DAYS, 1.0) = 0 THEN
                l_fmt := 'FMMM/DD/YYYY';
            ELSE
                l_fmt := 'FMMM/DD/YYYY HH:MI AM';
            END IF;
            LOOP
                IF r_fee.MONTHS > 0 THEN
                    SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                      INTO l_last_payment
                      FROM DUAL;
                ELSE
                    SELECT l_last_payment + r_fee.DAYS
                      INTO l_last_payment
                      FROM DUAL;
                END IF;
                EXIT WHEN l_last_payment > LEAST(NVL(r_fee.END_DATE, MAX_DATE), SYSDATE);

                BEGIN -- catch exception here
                    -- skip creation of fee if start date is after fee date
                    IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES (
                            LEDGER_SEQ.NEXTVAL,
                            'SF',
                            r_fee.SERVICE_FEE_ID,
                            -ABS(r_fee.FEE_AMOUNT),
                            GET_OR_CREATE_BATCH(r_fee.TERMINAL_ID, r_fee.CUSTOMER_BANK_ID, l_last_payment, 'Y'),
                            2,
                            l_last_payment,
                            r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt));
                    END IF;

                    UPDATE SERVICE_FEES
                       SET LAST_PAYMENT = l_last_payment
                     WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;

                    COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                EXCEPTION
    				 WHEN DUP_VAL_ON_INDEX THEN
    				 	 ROLLBACK;
                END;
            END LOOP;
        END LOOP;
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
       l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
    BEGIN
    	 BEGIN
    		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
    	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
    		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
    		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
    		END IF;
    	 EXCEPTION
    	 	WHEN NO_DATA_FOUND THEN
    			 NULL;
    		WHEN OTHERS THEN
    			 RAISE;
    	 END;
    	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 IF l_fee_amt <> 0 THEN
             INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
    		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
    	 END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
    	 SELECT STATUS INTO l_status FROM DOC WHERE DOC_ID = l_doc_id;
    	 IF l_status <> 'L' THEN
    	 	 RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
    	 END IF;
    	 UPDATE BATCH SET DOC_ID = NULL WHERE DOC_ID = l_doc_id;
    	 DELETE FROM DOC WHERE DOC_ID = l_doc_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    -- Syncs ledger and batch from report's sync queue
    PROCEDURE SYNC_LEDGER_JOB
    IS
        l_sync_msg REPORT.T_SYNC_MSG;
        l_trans_rec TRANS%ROWTYPE;
        l_fill_rec FILL%ROWTYPE;
    BEGIN
        LOOP
            l_sync_msg := REPORT.SYNC_PKG.dequeue(Q_SYNC_COMSUMER);
            EXIT WHEN l_sync_msg IS NULL OR l_sync_msg.pk_id IS NULL;
            BEGIN
                IF l_sync_msg.table_name = 'TRANS' THEN
                    SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_sync_msg.pk_id;
                    UPDATE_LEDGER(l_trans_rec);
                ELSIF l_sync_msg.table_name = 'FILL' AND l_sync_msg.dml_type = 'I' THEN
                    SELECT * INTO l_fill_rec FROM FILL WHERE FILL_ID = l_sync_msg.pk_id;
                    UPDATE_FILL_BATCH(l_fill_rec);
                END IF;
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    --log error somewhere
                    ROLLBACK;
            END;
        END LOOP;
    END;
    
    FUNCTION RUN_SYNC_LEDGER_JOB
     RETURN BINARY_INTEGER
    IS
        l_job_id BINARY_INTEGER;
    BEGIN
    	DBMS_JOB.SUBMIT (l_job_id, 'PAYMENTS_PKG.SYNC_LEDGER_JOB');
    	RETURN l_job_id;
    END;
END;
/


-- End of DDL Script for Package CORP.PAYMENTS_PKG

