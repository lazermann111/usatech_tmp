CREATE OR REPLACE PROCEDURE CORP.CUSTOMER_BANK_UPD 
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_desc IN CUSTOMER_BANK.DESCRIPTION%TYPE,
	l_prefix IN CUSTOMER_BANK.EFT_PREFIX%TYPE,
	l_min_amt IN CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE,
	l_is_eft IN CUSTOMER_BANK.IS_EFT%TYPE,
	l_pay_cycle_id IN CUSTOMER_BANK.PAY_CYCLE_ID%TYPE,
    l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE,
    l_tax_id_nbr IN CUSTOMER_BANK.TAX_ID_NBR%TYPE DEFAULT NULL,
    l_address1 IN CUSTOMER_ADDR.ADDRESS1%TYPE DEFAULT NULL,
    l_address2 IN CUSTOMER_ADDR.ADDRESS2%TYPE DEFAULT NULL,
    l_zip IN CUSTOMER_ADDR.ZIP%TYPE DEFAULT NULL,
    l_city IN CUSTOMER_ADDR.CITY%TYPE DEFAULT NULL,
    l_state IN CUSTOMER_ADDR.STATE%TYPE DEFAULT NULL,
    l_country_cd IN CUSTOMER_ADDR.COUNTRY_CD%TYPE DEFAULT NULL,
    l_use_tax_id_for_all IN VARCHAR DEFAULT NULL
)
IS
  l_customer_id NUMBER;
BEGIN
	 UPDATE CUSTOMER_BANK SET (BANK_NAME, CREATE_BY, DESCRIPTION, EFT_PREFIX, PAY_MIN_AMOUNT,
	  	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, IS_EFT, PAY_CYCLE_ID, TAX_ID_NBR)
	     = (SELECT l_bank_name,l_user_id, l_desc, l_prefix, l_min_amt,
		 l_title,l_account_type,l_aba,l_account_number, l_is_eft, l_pay_cycle_id,l_tax_id_nbr FROM DUAL)
     WHERE CUSTOMER_BANK_ID = l_cust_bank_id
     return CUSTOMER_ID into l_customer_id;
     
   IF l_address1 IS NOT NULL THEN
    UPDATE CORP.CUSTOMER_ADDR set ADDRESS1=l_address1,ADDRESS2=l_address2,ZIP=l_zip,CITY=l_city,STATE=l_state, COUNTRY_CD=l_country_cd
    WHERE addr_type=4 and CUSTOMER_BANK_ID=l_cust_bank_id;
    IF SQL%ROWCOUNT = 0 THEN
      INSERT INTO CORP.CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, ADDRESS1, ADDRESS2,CITY, STATE, ZIP, COUNTRY_CD, customer_bank_id)
      VALUES (CORP.CUSTOMER_ADDR_SEQ.NEXTVAL, l_customer_id, 4, l_address1, l_address2, l_city, l_state,l_zip,l_country_cd,l_cust_bank_id);
    END IF;
   END IF;
   
   IF l_use_tax_id_for_all = 'Y' THEN
          update CORP.CUSTOMER_BANK set tax_id_nbr=l_tax_id_nbr
          where customer_id=l_customer_id and status<>'D' and customer_bank_id<>l_cust_bank_id;
   END IF;
END CUSTOMER_BANK_UPD;
/
