-- Start of DDL Script for Table CORP.ORPHAN_CUST_DEV
-- Generated 3/16/2005 4:44:49 PM from CORP@USADBD02

CREATE TABLE corp.orphan_cust_dev
    (orphan_cust_dev_id             NUMBER(20,0) NOT NULL,
    customer_id                    NUMBER(20,0) NOT NULL,
    device_type_id                 NUMBER(20,0) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  corp_data
/




-- Indexes for CORP.ORPHAN_CUST_DEV

CREATE INDEX corp.ix_orphan_cust_dev_1 ON corp.orphan_cust_dev
  (
    customer_id                     ASC
  )
  TABLESPACE  corp_data
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX corp.ix_orphan_cust_dev_2 ON corp.orphan_cust_dev
  (
    device_type_id                  ASC
  )
  TABLESPACE  corp_idx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE UNIQUE INDEX corp.uix_orphan_cust_dev_1 ON corp.orphan_cust_dev
  (
    customer_id                     ASC,
    device_type_id                  ASC
  )
  TABLESPACE  corp_idx
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- Constraints for CORP.ORPHAN_CUST_DEV

ALTER TABLE corp.orphan_cust_dev
ADD CONSTRAINT pk_orphan_cust_dev_id PRIMARY KEY (orphan_cust_dev_id)
USING INDEX
  TABLESPACE  corp_idx
/




-- Triggers for CORP.ORPHAN_CUST_DEV

CREATE OR REPLACE TRIGGER corp.trbu_orphan_cust_dev
 BEFORE
  UPDATE
 ON corp.orphan_cust_dev
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER corp.trbi_orphan_cust_dev
 BEFORE
  INSERT
 ON corp.orphan_cust_dev
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.orphan_cust_dev_id IS NULL THEN

      SELECT seq_orphan_cust_dev_id.NEXTVAL
        INTO :new.orphan_cust_dev_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table CORP.ORPHAN_CUST_DEV

-- Foreign Key
ALTER TABLE corp.orphan_cust_dev
ADD CONSTRAINT fk_orphan_cust_dev_customer FOREIGN KEY (customer_id)
REFERENCES CORP.customer (customer_id)
/
ALTER TABLE corp.orphan_cust_dev
ADD CONSTRAINT fk_orphan_cust_dev_device FOREIGN KEY (device_type_id)
REFERENCES REPORT.device_type (device_type_id)
/
-- End of DDL script for Foreign Key(s)
