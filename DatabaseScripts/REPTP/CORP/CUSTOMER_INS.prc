CREATE OR REPLACE PROCEDURE CORP.CUSTOMER_INS 
   (custname IN CUSTOMER.CUSTOMER_NAME%TYPE,
    custaltname IN CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
    userid IN CUSTOMER.USER_ID%TYPE,
    salesterm IN CUSTOMER.SALES_TERM%TYPE,
    creditterm IN CUSTOMER.CREDIT_TERM%TYPE,
    updby IN CUSTOMER.UPD_BY%TYPE,
    Varout OUT NUMBER )
IS
    BEGIN
             -- Insert the details into the customer
            INSERT into CUSTOMER (customer_id,customer_name,user_id,sales_term,credit_term,
                                  create_by,customer_alt_name) VALUES
                                  (CUSTOMER_SEQ.NEXTVAL,custname,userid,salesterm,creditterm,
                                   updby,custaltname);
             Varout:= 0;
        EXCEPTION
          When OTHERS THEN
              Varout := SQLCODE;
END customer_ins;
/
