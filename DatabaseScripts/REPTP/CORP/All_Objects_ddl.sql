-- Start of DDL Script for Table CORP.ADDRESS_TYPE
-- Generated 10/13/2004 9:22:33 AM from CORP@USADBD02

-- Drop the old instance of ADDRESS_TYPE
DROP TABLE address_type
/

CREATE TABLE address_type
    (addr_type                      VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255),
    addr_type_id                   NUMBER NOT NULL
  ,
  PRIMARY KEY (addr_type_id)
  USING INDEX)
/




-- Indexes for ADDRESS_TYPE

CREATE UNIQUE INDEX xpkaddress_type ON address_type
  (
    addr_type_id                    ASC
  )
/



-- End of DDL Script for Table CORP.ADDRESS_TYPE

-- Start of DDL Script for Table CORP.BATCH
-- Generated 10/13/2004 9:22:36 AM from CORP@USADBD02

-- Drop the old instance of BATCH
DROP TABLE batch
/

CREATE TABLE batch
    (batch_id                       NUMBER(*,0) NOT NULL,
    customer_bank_id               NUMBER(*,0) NOT NULL,
    doc_id                         NUMBER(*,0),
    terminal_id                    NUMBER(*,0),
    payment_schedule_id            NUMBER(*,0) NOT NULL,
    start_date                     DATE NOT NULL,
    end_date                       DATE,
    create_date                    DATE DEFAULT SYSDATE

  NOT NULL,
    closed                         CHAR(1) NOT NULL
  ,
  PRIMARY KEY (batch_id)
  USING INDEX)
/

-- Drop the old synonym BATCH
DROP SYNONYM batch
/

-- Create synonym BATCH
CREATE SYNONYM batch
  FOR batch
/

-- Grants for Table
GRANT DELETE ON batch TO report
WITH GRANT OPTION
/
GRANT INSERT ON batch TO report
WITH GRANT OPTION
/
GRANT SELECT ON batch TO report
WITH GRANT OPTION
/
GRANT UPDATE ON batch TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON batch TO report
WITH GRANT OPTION
/



-- Indexes for BATCH

CREATE INDEX ix_batch_doc_id ON batch
  (
    doc_id                          ASC
  )
/

CREATE INDEX ix_batch_terminal_id ON batch
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_batch_payment_schedule_id ON batch
  (
    payment_schedule_id             ASC
  )
/

CREATE INDEX ix_batch_customer_bank_id ON batch
  (
    customer_bank_id                ASC
  )
/

CREATE BITMAP INDEX bix_batch_closed ON batch
  (
    closed                          ASC
  )
/

CREATE INDEX ix_batch_start_end_dates ON batch
  (
    start_date                      ASC,
    end_date                        ASC
  )
COMPRESS  2
/

CREATE UNIQUE INDEX uix_batch_summary ON batch
  (
    closed                          ASC,
    customer_bank_id                ASC,
    batch_id                        ASC
  )
COMPRESS  2
/

CREATE UNIQUE INDEX uix_batch_batch_doc_ids ON batch
  (
    doc_id                          ASC,
    batch_id                        ASC
  )
COMPRESS  1
/



-- Constraints for BATCH






-- Triggers for BATCH

CREATE OR REPLACE TRIGGER trbiu_batch
 BEFORE
  INSERT OR UPDATE
 ON batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.DOC_ID IS NOT NULL THEN
        :NEW.CLOSED := 'Y'; -- Closed
        IF :NEW.END_DATE IS NULL THEN
            :NEW.END_DATE := SYSDATE;
        END IF;
    ELSE
        :NEW.CLOSED := 'N'; -- Open
    END IF;
END;
/


-- Comments for BATCH

COMMENT ON COLUMN batch.closed IS 'Open (O) or Closed (C)'
/
COMMENT ON COLUMN batch.terminal_id IS 'Allow null for non-terminal specific adjustments'
/

-- End of DDL Script for Table CORP.BATCH

-- Start of DDL Script for Table CORP.CATEGORIES
-- Generated 10/13/2004 9:22:41 AM from CORP@USADBD02

-- Drop the old instance of CATEGORIES
DROP TABLE categories
/

CREATE TABLE categories
    (category_id                    NUMBER NOT NULL,
    name                           VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255),
    has_subcategories              NUMBER,
    upd_by                         VARCHAR2(50),
    upd_date                       DATE DEFAULT SYSDATE
 
  ,
  PRIMARY KEY (category_id)
  USING INDEX)
/




-- Indexes for CATEGORIES

CREATE UNIQUE INDEX xpkcategories ON categories
  (
    category_id                     ASC
  )
/



-- Constraints for CATEGORIES



-- End of DDL Script for Table CORP.CATEGORIES

-- Start of DDL Script for Table CORP.CHARGEBACK
-- Generated 10/13/2004 9:22:44 AM from CORP@USADBD02

-- Drop the old instance of CHARGEBACK
DROP TABLE chargeback
/

CREATE TABLE chargeback
    (chargeback_id                  NUMBER NOT NULL,
    ledger_id                      NUMBER,
    cc_appr_code                   VARCHAR2(50) NOT NULL,
    card_number                    VARCHAR2(20),
    tran_date                      DATE NOT NULL,
    problem_date                   DATE,
    location_name                  VARCHAR2(50),
    reason_code_id                 NUMBER,
    name                           VARCHAR2(50),
    phone                          VARCHAR2(20),
    description                    VARCHAR2(255),
    charge_status                  VARCHAR2(20) NOT NULL,
    create_date                    DATE DEFAULT SYSDATE,
    upd_date                       DATE DEFAULT SYSDATE,
    upd_by                         VARCHAR2(50)
  ,
  PRIMARY KEY (chargeback_id)
  USING INDEX)
/




-- Indexes for CHARGEBACK

CREATE INDEX xif56chargeback ON chargeback
  (
    ledger_id                       ASC
  )
/

CREATE INDEX xif62chargeback ON chargeback
  (
    reason_code_id                  ASC
  )
/

CREATE UNIQUE INDEX xpkchargeback ON chargeback
  (
    chargeback_id                   ASC
  )
/



-- Constraints for CHARGEBACK



-- End of DDL Script for Table CORP.CHARGEBACK

-- Start of DDL Script for Table CORP.CONTACT
-- Generated 10/13/2004 9:22:47 AM from CORP@USADBD02

-- Drop the old instance of CONTACT
DROP TABLE contact
/

CREATE TABLE contact
    (contact_type                   VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255)
  ,
  PRIMARY KEY (contact_type)
  USING INDEX)
/




-- Indexes for CONTACT

CREATE UNIQUE INDEX xpkcontact_type ON contact
  (
    contact_type                    ASC
  )
/



-- End of DDL Script for Table CORP.CONTACT

-- Start of DDL Script for Table CORP.CUSTOMER
-- Generated 10/13/2004 9:22:50 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER
DROP TABLE customer
/

CREATE TABLE customer
    (customer_id                    NUMBER NOT NULL,
    customer_name                  VARCHAR2(50) NOT NULL,
    user_id                        NUMBER,
    sales_term                     VARCHAR2(255),
    credit_term                    VARCHAR2(25),
    create_date                    DATE DEFAULT SYSDATE,
    upd_date                       DATE DEFAULT NULL,
    upd_by                         VARCHAR2(50),
    status                         CHAR(1) DEFAULT 'P',
    customer_alt_name              VARCHAR2(50),
    dealer_id                      NUMBER,
    tax_id_nbr                     VARCHAR2(12),
    create_by                      NUMBER NOT NULL
  ,
  PRIMARY KEY (customer_id)
  USING INDEX)
/

-- Drop the old synonym CUSTOMER
DROP SYNONYM customer
/

-- Create synonym CUSTOMER
CREATE SYNONYM customer
  FOR customer
/

-- Grants for Table
GRANT DELETE ON customer TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER

CREATE INDEX ix_customer_status ON customer
  (
    status                          ASC
  )
/

CREATE INDEX xif61customer ON customer
  (
    user_id                         ASC
  )
/

CREATE UNIQUE INDEX xpkcustomer ON customer
  (
    customer_id                     ASC
  )
/



-- End of DDL Script for Table CORP.CUSTOMER

-- Start of DDL Script for Table CORP.CUSTOMER_ADDR
-- Generated 10/13/2004 9:22:53 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_ADDR
DROP TABLE customer_addr
/

CREATE TABLE customer_addr
    (address_id                     NUMBER NOT NULL,
    customer_id                    NUMBER NOT NULL,
    addr_type                      NUMBER NOT NULL,
    local_tax                      NUMBER(15,2),
    state_tax                      NUMBER(15,2),
    name                           VARCHAR2(50),
    attn_to                        VARCHAR2(50),
    address1                       VARCHAR2(255),
    address2                       VARCHAR2(255),
    city                           VARCHAR2(50),
    state                          VARCHAR2(50),
    zip                            VARCHAR2(20),
    description                    VARCHAR2(255),
    status                         CHAR(1) DEFAULT 'A'
 
  ,
  PRIMARY KEY (address_id)
  USING INDEX)
/




-- Indexes for CUSTOMER_ADDR

CREATE INDEX xif17customer_addr ON customer_addr
  (
    address_id                      ASC,
    customer_id                     ASC
  )
/

CREATE INDEX xif53customer_addr ON customer_addr
  (
    customer_id                     ASC
  )
/

CREATE INDEX xif71customer_addr ON customer_addr
  (
    addr_type                       ASC
  )
/



-- Constraints for CUSTOMER_ADDR




-- End of DDL Script for Table CORP.CUSTOMER_ADDR

-- Start of DDL Script for Table CORP.CUSTOMER_BANK
-- Generated 10/13/2004 9:22:56 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK
DROP TABLE customer_bank
/

CREATE TABLE customer_bank
    (customer_id                    NUMBER NOT NULL,
    bank_acct_nbr                  VARCHAR2(20) NOT NULL,
    bank_routing_nbr               VARCHAR2(20) NOT NULL,
    pay_min_amount                 NUMBER DEFAULT 25  NOT NULL,
    schedule_id                    NUMBER,
    account_title                  VARCHAR2(50),
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    upd_date                       DATE DEFAULT SYSDATE  NOT NULL,
    upd_by                         NUMBER,
    status                         CHAR(1) DEFAULT 'P'  NOT NULL,
    customer_bank_id               NUMBER NOT NULL,
    description                    VARCHAR2(255),
    is_eft                         CHAR(1) DEFAULT 'Y'  NOT NULL,
    create_by                      NUMBER NOT NULL,
    eft_prefix                     VARCHAR2(25),
    account_type                   CHAR(1),
    bank_name                      VARCHAR2(50),
    bank_address                   VARCHAR2(255),
    bank_city                      VARCHAR2(50),
    bank_state                     VARCHAR2(10),
    bank_zip                       VARCHAR2(20),
    contact_name                   VARCHAR2(50),
    contact_title                  VARCHAR2(50),
    contact_telephone              VARCHAR2(20),
    contact_fax                    VARCHAR2(20)
  ,
  PRIMARY KEY (customer_bank_id)
  USING INDEX)
/

-- Drop the old synonym CUSTOMER_BANK
DROP SYNONYM customer_bank
/

-- Create synonym CUSTOMER_BANK
CREATE SYNONYM customer_bank
  FOR customer_bank
/

-- Grants for Table
GRANT DELETE ON customer_bank TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_bank TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_bank TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_bank TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_bank TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_BANK

CREATE INDEX ix_customer_bank_status ON customer_bank
  (
    status                          ASC
  )
/

CREATE INDEX ix_cust_bank_customer_id ON customer_bank
  (
    customer_id                     ASC
  )
/

CREATE UNIQUE INDEX pk_customer_bank ON customer_bank
  (
    customer_bank_id                ASC
  )
/



-- Constraints for CUSTOMER_BANK




-- End of DDL Script for Table CORP.CUSTOMER_BANK

-- Start of DDL Script for Table CORP.CUSTOMER_BANK_TERMINAL
-- Generated 10/13/2004 9:22:59 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_TERMINAL
DROP TABLE customer_bank_terminal
/

CREATE TABLE customer_bank_terminal
    (terminal_id                    NUMBER NOT NULL,
    customer_bank_id               NUMBER NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    customer_bank_terminal_id      NUMBER NOT NULL
  ,
  CONSTRAINT PK_CUSTOMER_BANK_TERMINAL
  PRIMARY KEY (customer_bank_terminal_id)
  USING INDEX)
/

-- Drop the old synonym CUSTOMER_BANK_TERMINAL
DROP SYNONYM customer_bank_terminal
/

-- Create synonym CUSTOMER_BANK_TERMINAL
CREATE SYNONYM customer_bank_terminal
  FOR customer_bank_terminal
/

-- Grants for Table
GRANT DELETE ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_bank_terminal TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_bank_terminal TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_BANK_TERMINAL

CREATE UNIQUE INDEX uix_cbt_combo ON customer_bank_terminal
  (
    customer_bank_id                ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC
  )
/

CREATE INDEX ix_cbt_customer_bank_id ON customer_bank_terminal
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_cbt_end_date ON customer_bank_terminal
  (
    end_date                        ASC
  )
/

CREATE INDEX ix_cbt_start_date ON customer_bank_terminal
  (
    start_date                      ASC
  )
/

CREATE INDEX ix_cbt_terminal_id ON customer_bank_terminal
  (
    terminal_id                     ASC
  )
/



-- Constraints for CUSTOMER_BANK_TERMINAL



-- Triggers for CUSTOMER_BANK_TERMINAL

CREATE OR REPLACE TRIGGER traiud_customer_bank_terminal
 AFTER
  INSERT OR DELETE OR UPDATE
 ON customer_bank_terminal
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
            OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE)) -- this should always be null, but to be safe...
             WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
               AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE);
        END IF;
    END IF;
END;
/


-- End of DDL Script for Table CORP.CUSTOMER_BANK_TERMINAL

-- Start of DDL Script for Table CORP.CUSTOMER_LICENSE
-- Generated 10/13/2004 9:23:04 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_LICENSE
DROP TABLE customer_license
/

CREATE TABLE customer_license
    (license_nbr                    VARCHAR2(15) NOT NULL,
    customer_id                    NUMBER NOT NULL,
    received                       DATE DEFAULT SYSDATE  NOT NULL,
    receive_by                     NUMBER NOT NULL)
/

-- Drop the old synonym CUSTOMER_LICENSE
DROP SYNONYM customer_license
/

-- Create synonym CUSTOMER_LICENSE
CREATE SYNONYM customer_license
  FOR customer_license
/

-- Grants for Table
GRANT DELETE ON customer_license TO report
WITH GRANT OPTION
/
GRANT INSERT ON customer_license TO report
WITH GRANT OPTION
/
GRANT SELECT ON customer_license TO report
WITH GRANT OPTION
/
GRANT UPDATE ON customer_license TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON customer_license TO report
WITH GRANT OPTION
/



-- Indexes for CUSTOMER_LICENSE

CREATE INDEX ix_cl_customer_id ON customer_license
  (
    customer_id                     ASC
  )
/

CREATE INDEX ix_cl_license_nbr ON customer_license
  (
    license_nbr                     ASC
  )
/

CREATE INDEX ix_cl_received ON customer_license
  (
    received                        ASC
  )
/



-- End of DDL Script for Table CORP.CUSTOMER_LICENSE

-- Start of DDL Script for Table CORP.DEALER
-- Generated 10/13/2004 9:23:07 AM from CORP@USADBD02

-- Drop the old instance of DEALER
DROP TABLE dealer
/

CREATE TABLE dealer
    (dealer_id                      NUMBER NOT NULL,
    dealer_name                    VARCHAR2(50) NOT NULL,
    address_id                     NUMBER,
    status                         CHAR(1) DEFAULT 'A'  NOT NULL
  ,
  PRIMARY KEY (dealer_id)
  USING INDEX)
/

-- Drop the old synonym DEALER
DROP SYNONYM dealer
/

-- Create synonym DEALER
CREATE SYNONYM dealer
  FOR dealer
/

-- Grants for Table
GRANT DELETE ON dealer TO report
WITH GRANT OPTION
/
GRANT INSERT ON dealer TO report
WITH GRANT OPTION
/
GRANT SELECT ON dealer TO report
WITH GRANT OPTION
/
GRANT UPDATE ON dealer TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON dealer TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.DEALER

-- Start of DDL Script for Table CORP.DEALER_EPORT
-- Generated 10/13/2004 9:23:10 AM from CORP@USADBD02

-- Drop the old instance of DEALER_EPORT
DROP TABLE dealer_eport
/

CREATE TABLE dealer_eport
    (dealer_id                      NUMBER NOT NULL,
    eport_id                       NUMBER NOT NULL,
    activate_date                  DATE,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  PRIMARY KEY (dealer_id, eport_id)
  USING INDEX)
/

-- Drop the old synonym DEALER_EPORT
DROP SYNONYM dealer_eport
/

-- Create synonym DEALER_EPORT
CREATE SYNONYM dealer_eport
  FOR dealer_eport
/

-- Grants for Table
GRANT DELETE ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT INSERT ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT SELECT ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT UPDATE ON dealer_eport TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON dealer_eport TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.DEALER_EPORT

-- Start of DDL Script for Table CORP.DEALER_LICENSE
-- Generated 10/13/2004 9:23:12 AM from CORP@USADBD02

-- Drop the old instance of DEALER_LICENSE
DROP TABLE dealer_license
/

CREATE TABLE dealer_license
    (license_id                     NUMBER NOT NULL,
    dealer_id                      NUMBER NOT NULL,
    start_date                     DATE,
    end_date                       DATE)
/




-- Indexes for DEALER_LICENSE

CREATE INDEX ix_dl_dealer_id ON dealer_license
  (
    dealer_id                       ASC
  )
/

CREATE INDEX ix_dl_license_id ON dealer_license
  (
    license_id                      ASC
  )
/



-- Constraints for DEALER_LICENSE



-- End of DDL Script for Table CORP.DEALER_LICENSE

-- Start of DDL Script for Table CORP.DOC
-- Generated 10/13/2004 9:23:15 AM from CORP@USADBD02

-- Drop the old instance of DOC
DROP TABLE doc
/

CREATE TABLE doc
    (doc_id                         NUMBER(*,0) NOT NULL,
    doc_type                       CHAR(2) NOT NULL,
    ref_nbr                        VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255) NOT NULL,
    customer_bank_id               NUMBER(*,0) NOT NULL,
    total_amount                   NUMBER(15,2),
    approve_by                     NUMBER(*,0),
    sent_by                        NUMBER(*,0),
    sent_date                      DATE,
    bank_acct_nbr                  VARCHAR2(20),
    bank_routing_nbr               VARCHAR2(20),
    status                         CHAR(1) DEFAULT 'L'   NOT NULL,
    create_by                      NUMBER(*,0),
    create_date                    DATE DEFAULT SYSDATE

  NOT NULL,
    update_date                    DATE
  ,
  PRIMARY KEY (doc_id)
  USING INDEX)
/




-- Indexes for DOC

CREATE UNIQUE INDEX uix_doc_ref_nbr ON doc
  (
    ref_nbr                         ASC
  )
/

CREATE INDEX doc_customer_bank_id ON doc
  (
    customer_bank_id                ASC
  )
/

CREATE BITMAP INDEX bix_doc_status ON doc
  (
    status                          ASC
  )
/



-- Constraints for DOC






-- Comments for DOC

COMMENT ON COLUMN doc.status IS 'Locked (L),'
/

-- End of DDL Script for Table CORP.DOC

-- Start of DDL Script for Table CORP.EFT
-- Generated 10/13/2004 9:23:19 AM from CORP@USADBD02

-- Drop the old instance of EFT
DROP TABLE eft
/

CREATE TABLE eft
    (eft_id                         NUMBER NOT NULL,
    eft_date                       DATE,
    batch_ref_nbr                  VARCHAR2(50),
    description                    VARCHAR2(255),
    customer_bank_id               NUMBER,
    bank_acct_nbr                  VARCHAR2(20) NOT NULL,
    bank_routing_nbr               VARCHAR2(20) NOT NULL,
    status                         CHAR(1) DEFAULT 'L'  NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE,
    create_by                      NUMBER,
    approve_by                     NUMBER,
    sent_by                        NUMBER,
    eft_amount                     NUMBER(15,2)
  ,
  PRIMARY KEY (eft_id)
  USING INDEX)
/

-- Drop the old synonym EFT
DROP SYNONYM eft
/

-- Create synonym EFT
CREATE SYNONYM eft
  FOR eft
/

-- Grants for Table
GRANT DELETE ON eft TO report
WITH GRANT OPTION
/
GRANT INSERT ON eft TO report
WITH GRANT OPTION
/
GRANT SELECT ON eft TO report
WITH GRANT OPTION
/
GRANT UPDATE ON eft TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON eft TO report
WITH GRANT OPTION
/



-- Indexes for EFT

CREATE UNIQUE INDEX ix_eft_combo ON eft
  (
    status                          ASC,
    eft_date                        ASC,
    eft_id                          ASC
  )
COMPRESS  2
/

CREATE INDEX ix_eft_cb_id ON eft
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_eft_status ON eft
  (
    status                          ASC
  )
/

CREATE UNIQUE INDEX xfkeft ON eft
  (
    batch_ref_nbr                   ASC
  )
/



-- End of DDL Script for Table CORP.EFT

-- Start of DDL Script for Table CORP.EPORT_PART
-- Generated 10/13/2004 9:23:22 AM from CORP@USADBD02

-- Drop the old instance of EPORT_PART
DROP TABLE eport_part
/

CREATE TABLE eport_part
    (eport_part_no                  NUMBER NOT NULL,
    eport_model_no                 VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255)
  ,
  PRIMARY KEY (eport_part_no)
  USING INDEX)
/




-- Indexes for EPORT_PART

CREATE UNIQUE INDEX xpkeport_part ON eport_part
  (
    eport_part_no                   ASC
  )
/



-- End of DDL Script for Table CORP.EPORT_PART

-- Start of DDL Script for Table CORP.ERROR_LOG
-- Generated 10/13/2004 9:23:25 AM from CORP@USADBD02

-- Drop the old instance of ERROR_LOG
DROP TABLE error_log
/

CREATE TABLE error_log
    (trans_id                       NUMBER NOT NULL,
    error_num                      NUMBER NOT NULL,
    error_msg                      VARCHAR2(4000) NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    error_context                  VARCHAR2(1000) NOT NULL)
/




-- End of DDL Script for Table CORP.ERROR_LOG

-- Start of DDL Script for Table CORP.FEES
-- Generated 10/13/2004 9:23:27 AM from CORP@USADBD02

-- Drop the old instance of FEES
DROP TABLE fees
/

CREATE TABLE fees
    (fee_id                         NUMBER NOT NULL,
    fee_name                       VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(500)
  ,
  PRIMARY KEY (fee_id)
  USING INDEX)
/




-- End of DDL Script for Table CORP.FEES

-- Start of DDL Script for Table CORP.FREQUENCY
-- Generated 10/13/2004 9:23:30 AM from CORP@USADBD02

-- Drop the old instance of FREQUENCY
DROP TABLE frequency
/

CREATE TABLE frequency
    (frequency_id                   NUMBER NOT NULL,
    name                           VARCHAR2(30) NOT NULL,
    interval                       VARCHAR2(2) DEFAULT 'DD'  NOT NULL,
    months                         NUMBER(8,0) DEFAULT 0  NOT NULL,
    days                           NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (frequency_id)
  USING INDEX)
/

-- Drop the old synonym FREQUENCY
DROP SYNONYM frequency
/

-- Create synonym FREQUENCY
CREATE SYNONYM frequency
  FOR frequency
/

-- Grants for Table
GRANT DELETE ON frequency TO report
WITH GRANT OPTION
/
GRANT INSERT ON frequency TO report
WITH GRANT OPTION
/
GRANT SELECT ON frequency TO report
WITH GRANT OPTION
/
GRANT UPDATE ON frequency TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON frequency TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.FREQUENCY

-- Start of DDL Script for Table CORP.G4OLD_CUSTOMER
-- Generated 10/13/2004 9:23:33 AM from CORP@USADBD02

-- Drop the old instance of G4OLD_CUSTOMER
DROP TABLE g4old_customer
/

CREATE TABLE g4old_customer
    (cid                            NUMBER(10,0) NOT NULL,
    cust_ref                       CHAR(50) NOT NULL,
    cname                          CHAR(50) NOT NULL,
    type_id                        NUMBER(10,0) NOT NULL,
    dex_export                     CHAR(255),
    dex_export_type_id             NUMBER(10,0),
    trans_export                   CHAR(255),
    trans_export_type_id           NUMBER(10,0),
    eft_export                     CHAR(255),
    eft_export_type_id             NUMBER(10,0),
    audit_recip                    CHAR(255),
    caudit                         NUMBER(10,0) NOT NULL,
    zero_trans_rpt                 CHAR(255),
    status                         CHAR(1) NOT NULL,
    create_dt                      DATE NOT NULL,
    upd_by                         NUMBER(10,0),
    upd_dt                         DATE NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_CUSTOMER

-- Start of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK
-- Generated 10/13/2004 9:23:36 AM from CORP@USADBD02

-- Drop the old instance of G4OLD_CUSTOMER_BANK
DROP TABLE g4old_customer_bank
/

CREATE TABLE g4old_customer_bank
    (id                             NUMBER(10,0) NOT NULL,
    customer_id                    NUMBER(10,0) NOT NULL,
    bank_acct                      CHAR(50) NOT NULL,
    description                    CHAR(255),
    create_dt                      DATE NOT NULL,
    status                         CHAR(1) NOT NULL,
    upd_dt                         DATE NOT NULL,
    upd_by                         NUMBER(10,0) NOT NULL,
    min_eft_amt                    NUMBER(19,4) NOT NULL,
    acct_active                    CHAR(1) NOT NULL,
    process_fee                    NUMBER(5,2) NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK

-- Start of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK_DEVICE
-- Generated 10/13/2004 9:23:39 AM from CORP@USADBD02

-- Drop the old instance of G4OLD_CUSTOMER_BANK_DEVICE
DROP TABLE g4old_customer_bank_device
/

CREATE TABLE g4old_customer_bank_device
    (customer_bank_id               NUMBER(10,0) NOT NULL,
    device_id                      NUMBER(10,0) NOT NULL,
    create_dt                      DATE NOT NULL,
    status                         CHAR(1) NOT NULL,
    upd_dt                         DATE NOT NULL,
    upd_by                         NUMBER(10,0) NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_CUSTOMER_BANK_DEVICE

-- Start of DDL Script for Table CORP.G4OLD_EFTS
-- Generated 10/13/2004 9:23:41 AM from CORP@USADBD02

-- Drop the old instance of G4OLD_EFTS
DROP TABLE g4old_efts
/

CREATE TABLE g4old_efts
    (first_date                     DATE NOT NULL,
    customer_bank_id               NUMBER(10,0) NOT NULL)
/




-- End of DDL Script for Table CORP.G4OLD_EFTS

-- Start of DDL Script for Table CORP.G4OLD_EMAIL
-- Generated 10/13/2004 9:23:44 AM from CORP@USADBD02

-- Drop the old instance of G4OLD_EMAIL
DROP TABLE g4old_email
/

CREATE TABLE g4old_email
    (customer_id                    NUMBER(10,0) NOT NULL,
    customer_name                  CHAR(50) NOT NULL,
    email                          CHAR(255))
/




-- End of DDL Script for Table CORP.G4OLD_EMAIL

-- Start of DDL Script for Table CORP.LEDGER
-- Generated 10/13/2004 9:23:47 AM from CORP@USADBD02

-- Drop the old instance of LEDGER
DROP TABLE ledger
/

CREATE TABLE ledger
    (ledger_id                      NUMBER(*,0) NOT NULL,
    entry_type                     CHAR(2) NOT NULL,
    trans_id                       NUMBER(*,0),
    process_fee_id                 NUMBER(*,0),
    service_fee_id                 NUMBER(*,0),
    amount                         NUMBER(21,8),
    batch_id                       NUMBER(*,0) NOT NULL,
    settle_state_id                NUMBER(*,0) NOT NULL,
    ledger_date                    DATE NOT NULL,
    description                    VARCHAR2(400),
    create_by                      NUMBER,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    deleted                        CHAR(1) DEFAULT 'N'  NOT NULL
  ,
  PRIMARY KEY (ledger_id)
  USING INDEX)
/

-- Drop the old synonym LEDGER
DROP SYNONYM ledger
/

-- Create synonym LEDGER
CREATE SYNONYM ledger
  FOR ledger
/

-- Grants for Table
GRANT DELETE ON ledger TO report
WITH GRANT OPTION
/
GRANT INSERT ON ledger TO report
WITH GRANT OPTION
/
GRANT SELECT ON ledger TO report
WITH GRANT OPTION
/
GRANT UPDATE ON ledger TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON ledger TO report
WITH GRANT OPTION
/



-- Indexes for LEDGER

CREATE BITMAP INDEX bix_ledger_entry_type ON ledger
  (
    entry_type                      ASC
  )
/

CREATE INDEX ledger_trans_id ON ledger
  (
    trans_id                        ASC
  )
/

CREATE INDEX ledger_settle_state_id ON ledger
  (
    settle_state_id                 ASC
  )
/

CREATE INDEX ledger_service_fee_id ON ledger
  (
    service_fee_id                  ASC
  )
/

CREATE INDEX ledger_process_fee_id ON ledger
  (
    process_fee_id                  ASC
  )
/

CREATE INDEX ledger_batch_id ON ledger
  (
    batch_id                        ASC
  )
/

CREATE BITMAP INDEX bix_ledger_deleted ON ledger
  (
    deleted                         ASC
  )
/

CREATE UNIQUE INDEX uix_ledger_batch_combo ON ledger
  (
    batch_id                        ASC,
    deleted                         ASC,
    entry_type                      ASC,
    settle_state_id                 ASC,
    amount                          ASC,
    ledger_id                       ASC
  )
COMPRESS  5
/



-- Constraints for LEDGER








-- Comments for LEDGER

COMMENT ON COLUMN ledger.deleted IS 'Is this record deleted (''Y'' or ''N'')'
/

-- End of DDL Script for Table CORP.LEDGER

-- Start of DDL Script for Table CORP.LEDGER_OLD
-- Generated 10/13/2004 9:23:51 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_OLD
DROP TABLE ledger_old
/

CREATE TABLE ledger_old
    (ledger_id                      NUMBER NOT NULL,
    trans_id                       NUMBER NOT NULL,
    terminal_id                    NUMBER,
    card_number                    VARCHAR2(50),
    preauth_amount                 NUMBER(15,2),
    total_amount                   NUMBER(15,2),
    cc_appr_code                   VARCHAR2(50),
    start_date                     DATE,
    close_date                     DATE,
    server_date                    DATE,
    track_data                     VARCHAR2(255),
    batch_id                       NUMBER,
    payment_id                     NUMBER DEFAULT NULL

 ,
    eport_id                       NUMBER NOT NULL,
    trans_type_id                  NUMBER,
    create_dt                      DATE DEFAULT SYSDATE,
    merchant_id                    NUMBER,
    customer_bank_id               NUMBER,
    process_fee_id                 NUMBER,
    settle_state_id                NUMBER NOT NULL,
    paid_status                    CHAR(1) NOT NULL,
    fee_percent                    NUMBER(8,4),
    fee_amount                     NUMBER(15,2)
  ,
  CONSTRAINT PK_LEDGER
  PRIMARY KEY (ledger_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON ledger_old TO report
WITH GRANT OPTION
/
GRANT INSERT ON ledger_old TO report
WITH GRANT OPTION
/
GRANT SELECT ON ledger_old TO report
WITH GRANT OPTION
/
GRANT UPDATE ON ledger_old TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON ledger_old TO report
WITH GRANT OPTION
/



-- Indexes for LEDGER_OLD

CREATE UNIQUE INDEX uix_ledger_old_temp ON ledger_old
  (
    payment_id                      ASC,
    trans_type_id                   ASC,
    trans_id                        ASC,
    ledger_id                       ASC
  )
COMPRESS  3
/

CREATE UNIQUE INDEX uix_ledger_for_unpaid2 ON ledger_old
  (
    paid_status                     ASC,
    close_date                      ASC,
    customer_bank_id                ASC,
    trans_type_id                   ASC,
    terminal_id                     ASC,
    total_amount                    ASC,
    fee_percent                     ASC,
    fee_amount                      ASC,
    trans_id                        ASC
  )
COMPRESS  8
/

CREATE UNIQUE INDEX uix_ledger_for_unpaid ON ledger_old
  (
    paid_status                     ASC,
    customer_bank_id                ASC,
    trans_type_id                   ASC,
    fee_percent                     ASC,
    fee_amount                      ASC,
    total_amount                    ASC,
    trans_id                        ASC
  )
COMPRESS  6
/

CREATE INDEX ix_ledger_paymt_id_terminal_id ON ledger_old
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_ledger_payment_id ON ledger_old
  (
    payment_id                      ASC,
    trans_id                        ASC
  )
/

CREATE INDEX ix_ledger_batch_id ON ledger_old
  (
    batch_id                        ASC
  )
/

CREATE INDEX ix_ledger_close_date ON ledger_old
  (
    close_date                      ASC
  )
/

CREATE INDEX ix_ledger_eport_id ON ledger_old
  (
    eport_id                        ASC
  )
/

CREATE INDEX ix_ledger_terminal_id ON ledger_old
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_ledger_trans_type_id ON ledger_old
  (
    trans_type_id                   ASC
  )
/

CREATE UNIQUE INDEX uix_ledger_tran_id ON ledger_old
  (
    trans_id                        ASC
  )
/



-- Triggers for LEDGER_OLD

CREATE OR REPLACE TRIGGER tbiu_ledger
 BEFORE
  INSERT OR UPDATE
 ON ledger_old
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.TRANS_TYPE_ID IN(20,21) AND :NEW.TOTAL_AMOUNT > 0 THEN
        :NEW.TOTAL_AMOUNT := -:NEW.TOTAL_AMOUNT;
    END IF;

    -- Find terminal_id, customer_bank_id and, process_fee_id
    IF INSERTING OR :OLD.EPORT_ID != :NEW.EPORT_ID
            OR :OLD.CLOSE_DATE != :NEW.CLOSE_DATE
            OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID THEN
        BEGIN
            SELECT TERMINAL_ID
              INTO :NEW.TERMINAL_ID
              FROM TERMINAL_EPORT
             WHERE EPORT_ID = :NEW.EPORT_ID
               AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
               AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            BEGIN
                SELECT CUSTOMER_BANK_ID
                  INTO :NEW.CUSTOMER_BANK_ID
                  FROM CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.CUSTOMER_BANK_ID := NULL; -- even if customer bank mapping not found look for process fees
                WHEN OTHERS THEN
                    RAISE;
            END;
            BEGIN
                SELECT PROCESS_FEE_ID, FEE_PERCENT, FEE_AMOUNT
                  INTO :NEW.PROCESS_FEE_ID, :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
                  FROM PROCESS_FEES
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.PROCESS_FEE_ID := NULL;
                    :NEW.FEE_PERCENT := NULL;
                    :NEW.FEE_AMOUNT := NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                :NEW.TERMINAL_ID := NULL;
                :NEW.CUSTOMER_BANK_ID := NULL;
                :NEW.PROCESS_FEE_ID := NULL;
                :NEW.FEE_PERCENT := NULL;
                :NEW.FEE_AMOUNT := NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    ELSIF UPDATING AND NVL(:OLD.TERMINAL_ID, 0) != NVL(:NEW.TERMINAL_ID, 0) THEN
        -- Find customer_bank_id and, process_fee_id
        IF :NEW.TERMINAL_ID IS NULL THEN
            :NEW.CUSTOMER_BANK_ID := NULL;
            :NEW.PROCESS_FEE_ID := NULL;
            :NEW.FEE_PERCENT := NULL;
            :NEW.FEE_AMOUNT := NULL;
        ELSE
            BEGIN
                SELECT CUSTOMER_BANK_ID
                  INTO :NEW.CUSTOMER_BANK_ID
                  FROM CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.CUSTOMER_BANK_ID := NULL; -- even if customer bank mapping not found look for process fees
                WHEN OTHERS THEN
                    RAISE;
            END;
            BEGIN
                SELECT PROCESS_FEE_ID, FEE_PERCENT, FEE_AMOUNT
                  INTO :NEW.PROCESS_FEE_ID, :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
                  FROM PROCESS_FEES
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.PROCESS_FEE_ID := NULL;
                    :NEW.FEE_PERCENT := NULL;
                    :NEW.FEE_AMOUNT := NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    ELSIF UPDATING AND NVL(:OLD.PROCESS_FEE_ID, 0) != NVL(:NEW.PROCESS_FEE_ID, 0) THEN
        BEGIN
            SELECT FEE_PERCENT, FEE_AMOUNT
              INTO :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
              FROM PROCESS_FEES
             WHERE PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                :NEW.FEE_PERCENT := NULL;
                :NEW.FEE_AMOUNT := NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    END IF;
    
    -- Calculate PAID_STATUS
    IF :NEW.PAYMENT_ID IS NOT NULL THEN
        :NEW.PAID_STATUS := 'P'; -- Paid
    ELSIF :NEW.SETTLE_STATE_ID IN(2,3,5,6) THEN
        :NEW.PAID_STATUS := 'U'; -- Unpaid
    ELSE
        :NEW.PAID_STATUS := 'N'; -- Not payable
    END IF;
END;
/


-- Comments for LEDGER_OLD

COMMENT ON COLUMN ledger_old.paid_status IS 'P = Paid; U = Unpaid; N = Not payable'
/

-- End of DDL Script for Table CORP.LEDGER_OLD

-- Start of DDL Script for Table CORP.LICENSE
-- Generated 10/13/2004 9:23:56 AM from CORP@USADBD02

-- Drop the old instance of LICENSE
DROP TABLE license
/

CREATE TABLE license
    (license_id                     NUMBER NOT NULL,
    title                          VARCHAR2(50) NOT NULL,
    description                    VARCHAR2(255),
    status                         CHAR(1) DEFAULT 'A'  NOT NULL
  ,
  PRIMARY KEY (license_id)
  USING INDEX)
/




-- Indexes for LICENSE

CREATE INDEX ix_license_status ON license
  (
    status                          ASC
  )
/



-- End of DDL Script for Table CORP.LICENSE

-- Start of DDL Script for Table CORP.LICENSE_NBR
-- Generated 10/13/2004 9:23:59 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_NBR
DROP TABLE license_nbr
/

CREATE TABLE license_nbr
    (license_nbr                    VARCHAR2(15) NOT NULL,
    license_id                     NUMBER NOT NULL,
    customer_id                    NUMBER NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL
  ,
  PRIMARY KEY (license_nbr)
  USING INDEX)
/

-- Drop the old synonym LICENSE_NBR
DROP SYNONYM license_nbr
/

-- Create synonym LICENSE_NBR
CREATE SYNONYM license_nbr
  FOR license_nbr
/

-- Grants for Table
GRANT DELETE ON license_nbr TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_nbr TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_nbr TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_nbr TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_nbr TO report
WITH GRANT OPTION
/



-- Indexes for LICENSE_NBR

CREATE INDEX ix_license_nbr_customer_id ON license_nbr
  (
    customer_id                     ASC
  )
/

CREATE INDEX ix_license_nbr_license_id ON license_nbr
  (
    license_id                      ASC
  )
/



-- End of DDL Script for Table CORP.LICENSE_NBR

-- Start of DDL Script for Table CORP.LICENSE_PROCESS_FEES
-- Generated 10/13/2004 9:24:02 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_PROCESS_FEES
DROP TABLE license_process_fees
/

CREATE TABLE license_process_fees
    (license_id                     NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    fee_percent                    NUMBER(8,4) NOT NULL,
    fee_amount                     NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (license_id, trans_type_id)
  USING INDEX)
/

-- Drop the old synonym LICENSE_PROCESS_FEES
DROP SYNONYM license_process_fees
/

-- Create synonym LICENSE_PROCESS_FEES
CREATE SYNONYM license_process_fees
  FOR license_process_fees
/

-- Grants for Table
GRANT DELETE ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_process_fees TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.LICENSE_PROCESS_FEES

-- Start of DDL Script for Table CORP.LICENSE_SERVICE_FEES
-- Generated 10/13/2004 9:24:05 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_SERVICE_FEES
DROP TABLE license_service_fees
/

CREATE TABLE license_service_fees
    (license_id                     NUMBER NOT NULL,
    fee_id                         NUMBER NOT NULL,
    amount                         NUMBER(15,2) NOT NULL,
    frequency_id                   NUMBER DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (license_id, fee_id)
  USING INDEX)
/

-- Drop the old synonym LICENSE_SERVICE_FEES
DROP SYNONYM license_service_fees
/

-- Create synonym LICENSE_SERVICE_FEES
CREATE SYNONYM license_service_fees
  FOR license_service_fees
/

-- Grants for Table
GRANT DELETE ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_service_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_service_fees TO report
WITH GRANT OPTION
/



-- Indexes for LICENSE_SERVICE_FEES

CREATE INDEX ix_lsf_frequency_id ON license_service_fees
  (
    frequency_id                    ASC
  )
/



-- End of DDL Script for Table CORP.LICENSE_SERVICE_FEES

-- Start of DDL Script for Table CORP.MERCHANT
-- Generated 10/13/2004 9:24:08 AM from CORP@USADBD02

-- Drop the old instance of MERCHANT
DROP TABLE merchant
/

CREATE TABLE merchant
    (merchant_id                    NUMBER NOT NULL,
    merchant_nbr                   VARCHAR2(20) NOT NULL,
    description                    VARCHAR2(255),
    status                         VARCHAR2(1) DEFAULT 'A'  NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    upd_by                         NUMBER NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_MERCHANT
  PRIMARY KEY (merchant_id)
  USING INDEX)
/

-- Drop the old synonym MERCHANT
DROP SYNONYM merchant
/

-- Create synonym MERCHANT
CREATE SYNONYM merchant
  FOR merchant
/

-- Drop the old synonym MERCHANT
DROP SYNONYM merchant
/

-- Create synonym MERCHANT
CREATE SYNONYM merchant
  FOR merchant
/

-- Grants for Table
GRANT DELETE ON merchant TO report
WITH GRANT OPTION
/
GRANT INSERT ON merchant TO report
WITH GRANT OPTION
/
GRANT SELECT ON merchant TO report
WITH GRANT OPTION
/
GRANT UPDATE ON merchant TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON merchant TO report
WITH GRANT OPTION
/
GRANT DELETE ON merchant TO g4op
WITH GRANT OPTION
/
GRANT INSERT ON merchant TO g4op
WITH GRANT OPTION
/
GRANT SELECT ON merchant TO g4op
WITH GRANT OPTION
/
GRANT UPDATE ON merchant TO g4op
WITH GRANT OPTION
/
GRANT REFERENCES ON merchant TO g4op
WITH GRANT OPTION
/



-- Indexes for MERCHANT

CREATE UNIQUE INDEX uix_merchant_nbr ON merchant
  (
    merchant_nbr                    ASC
  )
/



-- End of DDL Script for Table CORP.MERCHANT

-- Start of DDL Script for Table CORP.OPER_EPORT
-- Generated 10/13/2004 9:24:11 AM from CORP@USADBD02

-- Drop the old instance of OPER_EPORT
DROP TABLE oper_eport
/

CREATE TABLE oper_eport
    (eport_id                       NUMBER NOT NULL,
    eport_serial_nbr               VARCHAR2(50),
    activation_date                DATE,
    termination_date               DATE,
    dialing_date                   DATE
  ,
  PRIMARY KEY (eport_id)
  USING INDEX)
/




-- Indexes for OPER_EPORT

CREATE UNIQUE INDEX xpkeport ON oper_eport
  (
    eport_id                        ASC
  )
/



-- End of DDL Script for Table CORP.OPER_EPORT

-- Start of DDL Script for Table CORP.PAYMENT_ADJUSTMENT
-- Generated 10/13/2004 9:24:14 AM from CORP@USADBD02

-- Drop the old instance of PAYMENT_ADJUSTMENT
DROP TABLE payment_adjustment
/

CREATE TABLE payment_adjustment
    (adjust_id                      NUMBER NOT NULL,
    eft_id                         NUMBER NOT NULL,
    terminal_id                    NUMBER DEFAULT 0  NOT NULL,
    reason                         VARCHAR2(255) NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    create_by                      NUMBER NOT NULL,
    amount                         NUMBER(15,2) NOT NULL,
    customer_bank_id               NUMBER DEFAULT 0  NOT NULL
  ,
  PRIMARY KEY (adjust_id)
  USING INDEX)
/

-- Drop the old synonym PAYMENT_ADJUSTMENT
DROP SYNONYM payment_adjustment
/

-- Create synonym PAYMENT_ADJUSTMENT
CREATE SYNONYM payment_adjustment
  FOR payment_adjustment
/

-- Grants for Table
GRANT DELETE ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT INSERT ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT SELECT ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payment_adjustment TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_adjustment TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENT_ADJUSTMENT

CREATE INDEX ix_payment_adjust_eft_terminal ON payment_adjustment
  (
    eft_id                          ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pay_adj_eft_id_cust_bank_id ON payment_adjustment
  (
    eft_id                          ASC,
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_adjust_create_date ON payment_adjustment
  (
    create_date                     ASC
  )
/

CREATE INDEX ix_adjust_cust_bank_id ON payment_adjustment
  (
    customer_bank_id                ASC
  )
/

CREATE INDEX ix_adjust_eft_id ON payment_adjustment
  (
    eft_id                          ASC
  )
/

CREATE INDEX ix_adjust_terminal_id ON payment_adjustment
  (
    terminal_id                     ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_ADJUSTMENT

-- Start of DDL Script for Table CORP.PAYMENT_PROCESS_FEE
-- Generated 10/13/2004 9:24:17 AM from CORP@USADBD02

-- Drop the old instance of PAYMENT_PROCESS_FEE
DROP TABLE payment_process_fee
/

CREATE TABLE payment_process_fee
    (payment_id                     NUMBER NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    gross_amount                   NUMBER(15,2),
    fee_percent                    NUMBER(8,4) NOT NULL,
    fee_amount                     NUMBER(15,2),
    net_amount                     NUMBER(15,2),
    customer_bank_id               NUMBER NOT NULL,
    tran_fee_amount                NUMBER(15,2),
    tran_count                     NUMBER
  ,
  CONSTRAINT PK_PAYMENT_PROCESS_FEE
  PRIMARY KEY (payment_id, terminal_id, trans_type_id, fee_percent, customer_bank_id)
  USING INDEX)
/

-- Drop the old synonym PAYMENT_PROCESS_FEE
DROP SYNONYM payment_process_fee
/

-- Create synonym PAYMENT_PROCESS_FEE
CREATE SYNONYM payment_process_fee
  FOR payment_process_fee
/

-- Grants for Table
GRANT DELETE ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT INSERT ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT SELECT ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payment_process_fee TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_process_fee TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENT_PROCESS_FEE

CREATE INDEX ix_pay_proc_fee_pay_id_term_id ON payment_process_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pay_process_fee_payment_id ON payment_process_fee
  (
    payment_id                      ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_PROCESS_FEE

-- Start of DDL Script for Table CORP.PAYMENT_SCHEDULE
-- Generated 10/13/2004 9:24:20 AM from CORP@USADBD02

-- Drop the old instance of PAYMENT_SCHEDULE
DROP TABLE payment_schedule
/

CREATE TABLE payment_schedule
    (payment_schedule_id            NUMBER NOT NULL,
    description                    VARCHAR2(100) NOT NULL,
    interval                       CHAR(2),
    months                         NUMBER,
    days                           NUMBER
  ,
  PRIMARY KEY (payment_schedule_id)
  USING INDEX)
/

-- Drop the old synonym PAYMENT_SCHEDULE
DROP SYNONYM payment_schedule
/

-- Create synonym PAYMENT_SCHEDULE
CREATE SYNONYM payment_schedule
  FOR payment_schedule
/

-- Grants for Table
GRANT SELECT ON payment_schedule TO public
WITH GRANT OPTION
/
GRANT REFERENCES ON payment_schedule TO public
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.PAYMENT_SCHEDULE

-- Start of DDL Script for Table CORP.PAYMENT_SERVICE_FEE
-- Generated 10/13/2004 9:24:23 AM from CORP@USADBD02

-- Drop the old instance of PAYMENT_SERVICE_FEE
DROP TABLE payment_service_fee
/

CREATE TABLE payment_service_fee
    (payment_id                     NUMBER DEFAULT 0  NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    fee_id                         NUMBER NOT NULL,
    fee_amount                     NUMBER(15,2) NOT NULL,
    fee_date                       DATE NOT NULL,
    status                         CHAR(1) DEFAULT 'A'  NOT NULL,
    payment_service_fee_id         NUMBER NOT NULL,
    service_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_PAYMENT_SERVICE_FEE
  PRIMARY KEY (payment_service_fee_id)
  USING INDEX)
/




-- Indexes for PAYMENT_SERVICE_FEE

CREATE INDEX ix_pay_serv_fee_pay_term_stat ON payment_service_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC,
    status                          ASC
  )
/

CREATE INDEX ix_pay_service_fee_payment_id ON payment_service_fee
  (
    payment_id                      ASC
  )
/

CREATE INDEX ix_pay_serv_fee_combo ON payment_service_fee
  (
    payment_id                      ASC,
    status                          ASC,
    terminal_id                     ASC,
    fee_date                        ASC,
    fee_amount                      ASC
  )
/

CREATE UNIQUE INDEX uix_payment_service_fee ON payment_service_fee
  (
    fee_id                          ASC,
    terminal_id                     ASC,
    fee_date                        ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENT_SERVICE_FEE

-- Start of DDL Script for Table CORP.PAYMENTS
-- Generated 10/13/2004 9:24:26 AM from CORP@USADBD02

-- Drop the old instance of PAYMENTS
DROP TABLE payments
/

CREATE TABLE payments
    (payment_id                     NUMBER NOT NULL,
    eft_id                         NUMBER NOT NULL,
    terminal_id                    NUMBER NOT NULL,
    gross_amount                   NUMBER(15,2),
    tax_amount                     NUMBER(15,2),
    process_fee_amount             NUMBER(15,2),
    service_fee_amount             NUMBER(15,2),
    refund_amount                  NUMBER(15,2),
    chargeback_amount              NUMBER(15,2),
    adjust_amount                  NUMBER(15,2),
    net_amount                     NUMBER(15,2),
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    payment_schedule_id            NUMBER,
    payment_start_date             DATE,
    payment_end_date               DATE
  ,
  CONSTRAINT PAYMENTS_PK21014403827934
  PRIMARY KEY (payment_id)
  USING INDEX)
/

-- Drop the old synonym PAYMENTS
DROP SYNONYM payments
/

-- Create synonym PAYMENTS
CREATE SYNONYM payments
  FOR payments
/

-- Grants for Table
GRANT DELETE ON payments TO report
WITH GRANT OPTION
/
GRANT INSERT ON payments TO report
WITH GRANT OPTION
/
GRANT SELECT ON payments TO report
WITH GRANT OPTION
/
GRANT UPDATE ON payments TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON payments TO report
WITH GRANT OPTION
/



-- Indexes for PAYMENTS

CREATE INDEX ix_payments_eft_id ON payments
  (
    eft_id                          ASC
  )
/

CREATE INDEX ix_payments_terminal_id ON payments
  (
    terminal_id                     ASC
  )
/



-- End of DDL Script for Table CORP.PAYMENTS

-- Start of DDL Script for Table CORP.PROCESS_FEES
-- Generated 10/13/2004 9:24:29 AM from CORP@USADBD02

-- Drop the old instance of PROCESS_FEES
DROP TABLE process_fees
/

CREATE TABLE process_fees
    (terminal_id                    NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    fee_percent                    NUMBER(8,4) NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    process_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL,
    fee_amount                     NUMBER(15,2) DEFAULT 0  NOT NULL
  ,
  CONSTRAINT PK_PROCESS_FEES
  PRIMARY KEY (process_fee_id)
  USING INDEX)
/

-- Drop the old synonym PROCESS_FEES
DROP SYNONYM process_fees
/

-- Create synonym PROCESS_FEES
CREATE SYNONYM process_fees
  FOR process_fees
/

-- Grants for Table
GRANT DELETE ON process_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON process_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON process_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON process_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON process_fees TO report
WITH GRANT OPTION
/



-- Indexes for PROCESS_FEES

CREATE UNIQUE INDEX uix_process_fees_combo1 ON process_fees
  (
    trans_type_id                   ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC,
    process_fee_id                  ASC
  )
/

CREATE UNIQUE INDEX uix_process_fees_combo2 ON process_fees
  (
    process_fee_id                  ASC,
    fee_percent                     ASC,
    fee_amount                      ASC
  )
/

CREATE INDEX ix_pf_end_date ON process_fees
  (
    end_date                        ASC
  )
/

CREATE INDEX ix_pf_start_date ON process_fees
  (
    start_date                      ASC
  )
/

CREATE INDEX ix_pf_terminal_id ON process_fees
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_pf_trans_type_id ON process_fees
  (
    trans_type_id                   ASC
  )
/



-- Triggers for PROCESS_FEES

CREATE OR REPLACE TRIGGER traiud_process_fees
 AFTER
  INSERT OR DELETE OR UPDATE
 ON process_fees
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        -- must update ledger & trans dim
        UPDATE LEDGER L SET L.AMOUNT = (
            SELECT T.TOTAL_AMOUNT * :NEW.FEE_PERCENT + :NEW.FEE_AMOUNT
              FROM TRANS T
             WHERE T.TRAN_ID = L.TRANS_ID)
         WHERE L.ENTRY_TYPE = 'PF'
           AND L.PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
           
        IF :OLD.PROCESS_FEE_ID != :NEW.PROCESS_FEE_ID OR :OLD.TERMINAL_ID != :NEW.TERMINAL_ID
                OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID
                OR NVL(:OLD.START_DATE, MIN_DATE) !=  NVL(:NEW.START_DATE, MIN_DATE)
                OR NVL(:OLD.END_DATE, MAX_DATE) !=  NVL(:NEW.END_DATE, MAX_DATE) THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
                   FROM PROCESS_FEES PF
                   WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                     AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                     AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                     AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
             WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
                OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
        END IF;
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE)) -- should be null always
             WHERE PROCESS_FEE_ID = :OLD.PROCESS_FEE_ID;
        END IF;
   END IF;
END;
/


-- End of DDL Script for Table CORP.PROCESS_FEES

-- Start of DDL Script for Table CORP.PURCHASE
-- Generated 10/13/2004 9:24:34 AM from CORP@USADBD02

-- Drop the old instance of PURCHASE
DROP TABLE purchase
/

CREATE TABLE purchase
    (purchase_id                    NUMBER NOT NULL,
    ledger_id                      NUMBER NOT NULL,
    trans_id                       NUMBER,
    trans_date                     DATE,
    amount                         NUMBER(15,2),
    vend_column                    VARCHAR2(50)
  ,
  PRIMARY KEY (purchase_id)
  USING INDEX)
/




-- Indexes for PURCHASE

CREATE INDEX xif30purchase ON purchase
  (
    ledger_id                       ASC
  )
/

CREATE UNIQUE INDEX xpkpurchase ON purchase
  (
    purchase_id                     ASC
  )
/



-- End of DDL Script for Table CORP.PURCHASE

-- Start of DDL Script for Table CORP.REASON_CODE
-- Generated 10/13/2004 9:24:37 AM from CORP@USADBD02

-- Drop the old instance of REASON_CODE
DROP TABLE reason_code
/

CREATE TABLE reason_code
    (reason_code_id                 NUMBER NOT NULL,
    reason                         VARCHAR2(50) NOT NULL,
    reason_desc                    VARCHAR2(50)
  ,
  PRIMARY KEY (reason_code_id)
  USING INDEX)
/




-- Indexes for REASON_CODE

CREATE UNIQUE INDEX xpkreason_code ON reason_code
  (
    reason_code_id                  ASC
  )
/



-- End of DDL Script for Table CORP.REASON_CODE

-- Start of DDL Script for Table CORP.REFUND
-- Generated 10/13/2004 9:24:40 AM from CORP@USADBD02

-- Drop the old instance of REFUND
DROP TABLE refund
/

CREATE TABLE refund
    (refund_id                      NUMBER NOT NULL,
    ledger_id                      NUMBER,
    cc_appr_code                   VARCHAR2(50) NOT NULL,
    card_number                    VARCHAR2(50) NOT NULL,
    tran_date                      DATE NOT NULL,
    problem_date                   DATE NOT NULL,
    location_name                  VARCHAR2(50),
    reason_code_id                 NUMBER,
    name                           VARCHAR2(50),
    phone                          VARCHAR2(20),
    description                    VARCHAR2(255),
    refund_status                  VARCHAR2(20) NOT NULL,
    upd_by                         VARCHAR2(50),
    upd_date                       DATE DEFAULT SYSDATE
 
  ,
  PRIMARY KEY (refund_id)
  USING INDEX)
/

-- Drop the old synonym REFUND
DROP SYNONYM refund
/

-- Create synonym REFUND
CREATE SYNONYM refund
  FOR refund
/

-- Grants for Table
GRANT DELETE ON refund TO g4op
WITH GRANT OPTION
/
GRANT INSERT ON refund TO g4op
WITH GRANT OPTION
/
GRANT SELECT ON refund TO g4op
WITH GRANT OPTION
/
GRANT UPDATE ON refund TO g4op
WITH GRANT OPTION
/
GRANT REFERENCES ON refund TO g4op
WITH GRANT OPTION
/



-- Indexes for REFUND

CREATE INDEX xif50refund ON refund
  (
    ledger_id                       ASC
  )
/

CREATE INDEX xif51refund ON refund
  (
    reason_code_id                  ASC
  )
/

CREATE UNIQUE INDEX xpkrefund ON refund
  (
    refund_id                       ASC
  )
/



-- Constraints for REFUND



-- End of DDL Script for Table CORP.REFUND

-- Start of DDL Script for Table CORP.SCHEDULE
-- Generated 10/13/2004 9:24:43 AM from CORP@USADBD02

-- Drop the old instance of SCHEDULE
DROP TABLE schedule
/

CREATE TABLE schedule
    (schedule_id                    NUMBER NOT NULL,
    schedule_type                  NUMBER NOT NULL,
    schedule_days                  VARCHAR2(20) NOT NULL,
    schedule_name                  VARCHAR2(255) NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    create_by                      NUMBER NOT NULL,
    upd_by                         NUMBER NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE  NOT NULL,
    status                         CHAR(1) DEFAULT 'A'  NOT NULL
  ,
  PRIMARY KEY (schedule_id)
  USING INDEX)
/




-- Indexes for SCHEDULE

CREATE UNIQUE INDEX xpkschedule ON schedule
  (
    schedule_id                     ASC
  )
/



-- Constraints for SCHEDULE



-- End of DDL Script for Table CORP.SCHEDULE

-- Start of DDL Script for Table CORP.SCHEDULE_TYPE
-- Generated 10/13/2004 9:24:46 AM from CORP@USADBD02

-- Drop the old instance of SCHEDULE_TYPE
DROP TABLE schedule_type
/

CREATE TABLE schedule_type
    (schedule_type_id               NUMBER NOT NULL,
    schedule_type                  VARCHAR2(25) NOT NULL,
    description                    VARCHAR2(255),
    status                         CHAR(1) DEFAULT 'A'  NOT NULL,
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    create_by                      NUMBER NOT NULL,
    upd_dt                         DATE DEFAULT SYSDATE  NOT NULL,
    upd_by                         NUMBER NOT NULL
  ,
  PRIMARY KEY (schedule_type_id)
  USING INDEX)
/




-- Indexes for SCHEDULE_TYPE

CREATE UNIQUE INDEX xpkschedule_type ON schedule_type
  (
    schedule_type_id                ASC
  )
/



-- End of DDL Script for Table CORP.SCHEDULE_TYPE

-- Start of DDL Script for Table CORP.SERVICE_FEES
-- Generated 10/13/2004 9:24:49 AM from CORP@USADBD02

-- Drop the old instance of SERVICE_FEES
DROP TABLE service_fees
/

CREATE TABLE service_fees
    (terminal_id                    NUMBER DEFAULT 0  NOT NULL,
    fee_id                         NUMBER NOT NULL,
    fee_amount                     NUMBER(15,2) NOT NULL,
    frequency_id                   NUMBER NOT NULL,
    last_payment                   DATE DEFAULT SYSDATE  NOT NULL,
    start_date                     DATE,
    end_date                       DATE,
    service_fee_id                 NUMBER NOT NULL,
    create_date                    DATE DEFAULT SYSDATE  NOT NULL
  ,
  CONSTRAINT PK_SERVICE_FEE
  PRIMARY KEY (service_fee_id)
  USING INDEX)
/

-- Drop the old synonym SERVICE_FEES
DROP SYNONYM service_fees
/

-- Create synonym SERVICE_FEES
CREATE SYNONYM service_fees
  FOR service_fees
/

-- Grants for Table
GRANT DELETE ON service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON service_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON service_fees TO report
WITH GRANT OPTION
/



-- Indexes for SERVICE_FEES

CREATE INDEX ix_sf_fee_id ON service_fees
  (
    fee_id                          ASC
  )
/

CREATE INDEX ix_sf_frequency_id ON service_fees
  (
    frequency_id                    ASC
  )
/

CREATE INDEX ix_sf_terminal_id ON service_fees
  (
    terminal_id                     ASC
  )
/



-- End of DDL Script for Table CORP.SERVICE_FEES

-- Start of DDL Script for Table CORP.SQLN_EXPLAIN_PLAN
-- Generated 10/13/2004 9:24:52 AM from CORP@USADBD02

-- Drop the old instance of SQLN_EXPLAIN_PLAN
DROP TABLE sqln_explain_plan
/

CREATE TABLE sqln_explain_plan
    (statement_id                   VARCHAR2(30),
    timestamp                      DATE,
    remarks                        VARCHAR2(80),
    operation                      VARCHAR2(30),
    options                        VARCHAR2(30),
    object_node                    VARCHAR2(128),
    object_owner                   VARCHAR2(30),
    object_name                    VARCHAR2(30),
    object_instance                NUMBER(*,0),
    object_type                    VARCHAR2(30),
    optimizer                      VARCHAR2(255),
    search_columns                 NUMBER(*,0),
    id                             NUMBER(*,0),
    parent_id                      NUMBER(*,0),
    position                       NUMBER(*,0),
    cost                           NUMBER(*,0),
    cardinality                    NUMBER(*,0),
    bytes                          NUMBER(*,0),
    other_tag                      VARCHAR2(255),
    partition_start                VARCHAR2(255),
    partition_stop                 VARCHAR2(255),
    partition_id                   NUMBER(*,0),
    other                          LONG,
    distribution                   VARCHAR2(30))
/

-- Grants for Table
GRANT DELETE ON sqln_explain_plan TO public
/
GRANT INSERT ON sqln_explain_plan TO public
/
GRANT SELECT ON sqln_explain_plan TO public
/
GRANT UPDATE ON sqln_explain_plan TO public
/



-- End of DDL Script for Table CORP.SQLN_EXPLAIN_PLAN

-- Start of DDL Script for Table CORP.UTL_DATA
-- Generated 10/13/2004 9:24:55 AM from CORP@USADBD02

-- Drop the old instance of UTL_DATA
DROP TABLE utl_data
/

CREATE TABLE utl_data
    (utl_data_id                    NUMBER NOT NULL,
    utldata                        VARCHAR2(500),
    utlname                        VARCHAR2(50)
  ,
  PRIMARY KEY (utl_data_id)
  USING INDEX)
/




-- Indexes for UTL_DATA

CREATE UNIQUE INDEX pk_utl_data ON utl_data
  (
    utl_data_id                     ASC
  )
/

CREATE UNIQUE INDEX sys_utl_name ON utl_data
  (
    utlname                         ASC
  )
/



-- End of DDL Script for Table CORP.UTL_DATA

-- Start of DDL Script for View CORP.VW_ADJUSTMENTS
-- Generated 10/13/2004 9:24:57 AM from CORP@USADBD02

-- Drop the old instance of VW_ADJUSTMENTS
DROP VIEW vw_adjustments
/

CREATE OR REPLACE VIEW vw_adjustments (
   doc_id,
   ledger_id,
   amount,
   reason,
   adjust_date,
   terminal_id,
   customer_bank_id,
   batch_closed,
   create_by )
AS
SELECT B.DOC_ID, L.LEDGER_ID, L.AMOUNT, L.DESCRIPTION REASON, L.LEDGER_DATE ADJUST_DATE,
B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.CLOSED, L.CREATE_BY
FROM LEDGER L, BATCH B
WHERE L.ENTRY_TYPE = 'AD'
AND L.DELETED = 'N' AND L.BATCH_ID = B.BATCH_ID
/


-- End of DDL Script for View CORP.VW_ADJUSTMENTS

-- Start of DDL Script for View CORP.VW_APPROVED_PAYMENTS
-- Generated 10/13/2004 9:24:58 AM from CORP@USADBD02

-- Drop the old instance of VW_APPROVED_PAYMENTS
DROP VIEW vw_approved_payments
/

CREATE OR REPLACE VIEW vw_approved_payments (
   eft_id,
   batch_ref_nbr,
   description,
   customer_bank_id,
   customer_name,
   bank_acct_nbr,
   bank_routing_nbr,
   eft_amount )
AS
SELECT D.DOC_ID, D.REF_NBR, D.DESCRIPTION, D.CUSTOMER_BANK_ID, C.CUSTOMER_NAME,
D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, D.TOTAL_AMOUNT FROM DOC D, CUSTOMER C, CUSTOMER_BANK CB
WHERE D.STATUS = 'A' AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND C.CUSTOMER_ID = CB.CUSTOMER_ID
/


-- End of DDL Script for View CORP.VW_APPROVED_PAYMENTS

-- Start of DDL Script for View CORP.VW_CHANGED_CUSTOMER_BANK
-- Generated 10/13/2004 9:24:59 AM from CORP@USADBD02

-- Drop the old instance of VW_CHANGED_CUSTOMER_BANK
DROP VIEW vw_changed_customer_bank
/

CREATE OR REPLACE VIEW vw_changed_customer_bank (
   bank_acct_nbr,
   bank_routing_nbr,
   account_title,
   create_date,
   customer_bank_id,
   customer_id,
   status,
   status_text,
   account_type_text,
   bank_name,
   bank_address,
   bank_city,
   bank_state,
   bank_zip,
   contact_name,
   contact_title,
   contact_telephone,
   contact_fax,
   customer_name,
   create_by )
AS
SELECT CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR, CB.ACCOUNT_TITLE, CB.CREATE_DATE, CB.CUSTOMER_BANK_ID, CB.CUSTOMER_ID, CB.STATUS,
(CASE WHEN CB.STATUS = 'P' THEN 'PENDING' WHEN CB.STATUS = 'U' THEN 'UPDATED' WHEN CB.STATUS = 'A' THEN 'ACTIVE' ELSE CB.STATUS END) STATUS_TEXT,
(CASE WHEN CB.ACCOUNT_TYPE = 'C' THEN 'Checking' WHEN CB.ACCOUNT_TYPE = 'S' THEN 'Saving' ELSE 'Unknown' END) ACCOUNT_TYPE_TEXT,
CB.BANK_NAME, CB.BANK_ADDRESS, CB.BANK_CITY, CB.BANK_STATE, CB.BANK_ZIP, CB.CONTACT_NAME, CB.CONTACT_TITLE,
CB.CONTACT_TELEPHONE, CB.CONTACT_FAX, C.CUSTOMER_NAME, CB.CREATE_BY
FROM CUSTOMER_BANK CB, CUSTOMER C WHERE CB.CUSTOMER_ID = C.CUSTOMER_ID
AND CB.STATUS IN ('U', 'P')
/


-- End of DDL Script for View CORP.VW_CHANGED_CUSTOMER_BANK

-- Start of DDL Script for View CORP.VW_CURRENT_BANK_ACCT
-- Generated 10/13/2004 9:24:59 AM from CORP@USADBD02

-- Drop the old instance of VW_CURRENT_BANK_ACCT
DROP VIEW vw_current_bank_acct
/

CREATE OR REPLACE VIEW vw_current_bank_acct (
   terminal_id,
   customer_bank_id )
AS
SELECT TERMINAL_ID, CUSTOMER_BANK_ID FROM CUSTOMER_BANK_TERMINAL CBT
WHERE SYSDATE BETWEEN NVL(START_DATE, SYSDATE) AND NVL(END_DATE, SYSDATE)
/

-- Drop the old synonym VW_CURRENT_BANK_ACCT
DROP SYNONYM vw_current_bank_acct
/

-- Create synonym VW_CURRENT_BANK_ACCT
CREATE SYNONYM vw_current_bank_acct
  FOR vw_current_bank_acct
/

-- Grants for View
GRANT DELETE ON vw_current_bank_acct TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_current_bank_acct TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_current_bank_acct TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_current_bank_acct TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_CURRENT_BANK_ACCT

-- Start of DDL Script for View CORP.VW_CURRENT_LICENSE
-- Generated 10/13/2004 9:25:00 AM from CORP@USADBD02

-- Drop the old instance of VW_CURRENT_LICENSE
DROP VIEW vw_current_license
/

CREATE OR REPLACE VIEW vw_current_license (
   license_nbr,
   received,
   customer_id )
AS
SELECT MAX(LICENSE_NBR), RECEIVED, CL.CUSTOMER_ID FROM CUSTOMER_LICENSE CL, 
(SELECT MAX(RECEIVED) MAX_RECEIVED, CUSTOMER_ID FROM CUSTOMER_LICENSE GROUP BY CUSTOMER_ID) M 
WHERE RECEIVED = MAX_RECEIVED AND CL.CUSTOMER_ID = M.CUSTOMER_ID GROUP BY RECEIVED, CL.CUSTOMER_ID
/


-- End of DDL Script for View CORP.VW_CURRENT_LICENSE

-- Start of DDL Script for View CORP.VW_CUSTOMER
-- Generated 10/13/2004 9:25:01 AM from CORP@USADBD02

-- Drop the old instance of VW_CUSTOMER
DROP VIEW vw_customer
/

CREATE OR REPLACE VIEW vw_customer (
   customer_id,
   customer_name,
   user_id,
   sales_term,
   credit_term,
   create_date,
   upd_date,
   create_by,
   upd_by,
   customer_alt_name,
   address_id,
   address_name,
   address1,
   address2,
   city,
   state,
   zip,
   license_nbr,
   status )
AS
SELECT C.CUSTOMER_ID, CUSTOMER_NAME, USER_ID, SALES_TERM, CREDIT_TERM, CREATE_DATE, UPD_DATE, CREATE_BY, UPD_BY, CUSTOMER_ALT_NAME,
ADDRESS_ID, NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, LICENSE_NBR, C.STATUS
FROM CUSTOMER C, (SELECT CUSTOMER_ID, NAME, ADDRESS_ID, ADDRESS1, ADDRESS2, CITY, STATE, ZIP
FROM CUSTOMER_ADDR WHERE STATUS = 'A' AND ADDR_TYPE = 2) CA, VW_CURRENT_LICENSE CL WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND C.CUSTOMER_ID = CL.CUSTOMER_ID (+)
/


-- End of DDL Script for View CORP.VW_CUSTOMER

-- Start of DDL Script for View CORP.VW_CUSTOMER_BANK_TERMINAL
-- Generated 10/13/2004 9:25:01 AM from CORP@USADBD02

-- Drop the old instance of VW_CUSTOMER_BANK_TERMINAL
DROP VIEW vw_customer_bank_terminal
/

CREATE OR REPLACE VIEW vw_customer_bank_terminal (
   customer_bank_terminal_id,
   customer_bank_id,
   terminal_id,
   start_date,
   end_date )
AS
SELECT MIN(CUSTOMER_BANK_TERMINAL_ID), CUSTOMER_BANK_ID, TERMINAL_ID, MIN(START_DATE) START_DATE, MAX(END_DATE) END_DATE
FROM (SELECT CUSTOMER_BANK_TERMINAL_ID, CUSTOMER_BANK_ID, TERMINAL_ID,
             (CASE WHEN FIRST = 1 THEN START_DATE ELSE NULL END) START_DATE,
             (CASE WHEN LAST = 1 THEN END_DATE ELSE NULL END) END_DATE,
             SUM(FIRST) OVER (PARTITION BY TERMINAL_ID, CUSTOMER_BANK_ID
                 ORDER BY NVL(START_DATE, MIN_DATE) ASC
                 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) SET_NBR
      FROM (SELECT CUSTOMER_BANK_TERMINAL_ID, A.CUSTOMER_BANK_ID, A.TERMINAL_ID, START_DATE, END_DATE,
                   CASE WHEN CUSTOMER_BANK_ID <>
                        NVL(LAG(CUSTOMER_BANK_ID, 1) OVER
                        (PARTITION BY A.TERMINAL_ID ORDER BY NVL(START_DATE, MIN_DATE) ASC), 0)
                        THEN 1 ELSE 0 END FIRST,
                   CASE WHEN CUSTOMER_BANK_ID <>
                        NVL(LEAD(CUSTOMER_BANK_ID, 1) OVER
                        (PARTITION BY A.TERMINAL_ID ORDER BY NVL(START_DATE, MIN_DATE) ASC), 0)
                        THEN 1 ELSE 0 END LAST
            FROM CUSTOMER_BANK_TERMINAL A))
GROUP BY CUSTOMER_BANK_ID, TERMINAL_ID, SET_NBR
/


-- End of DDL Script for View CORP.VW_CUSTOMER_BANK_TERMINAL

-- Start of DDL Script for View CORP.VW_DEALER_LICENSE
-- Generated 10/13/2004 9:25:02 AM from CORP@USADBD02

-- Drop the old instance of VW_DEALER_LICENSE
DROP VIEW vw_dealer_license
/

CREATE OR REPLACE VIEW vw_dealer_license (
   license_id,
   dealer_id )
AS
SELECT MAX(LICENSE_ID), DL.DEALER_ID FROM DEALER_LICENSE DL,
(SELECT MAX(START_DATE) MAX_START_DATE, DEALER_ID FROM DEALER_LICENSE GROUP BY DEALER_ID) M
WHERE NVL(START_DATE, SYSDATE) = NVL(MAX_START_DATE, SYSDATE) AND DL.DEALER_ID = M.DEALER_ID
GROUP BY DL.DEALER_ID
/


-- End of DDL Script for View CORP.VW_DEALER_LICENSE

-- Start of DDL Script for View CORP.VW_EFT_AUTH_INFO
-- Generated 10/13/2004 9:25:02 AM from CORP@USADBD02

-- Drop the old instance of VW_EFT_AUTH_INFO
DROP VIEW vw_eft_auth_info
/

CREATE OR REPLACE VIEW vw_eft_auth_info (
   customer_bank_id,
   customer_name,
   address1,
   city,
   state,
   zip,
   tax_id_nbr,
   bank_name,
   bank_address1,
   bank_city,
   bank_state,
   bank_zip,
   account_title,
   account_type,
   bank_acct_nbr,
   bank_routing_nbr,
   contact_name,
   contact_title,
   contact_telephone,
   contact_fax,
   telephone,
   fax )
AS
SELECT CB.CUSTOMER_BANK_ID, CUSTOMER_NAME, ADDRESS1, CITY, STATE, ZIP, TAX_ID_NBR, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE,
BANK_ZIP, ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ACCT_NBR, BANK_ROUTING_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE,
CONTACT_FAX, TELEPHONE, FAX
FROM CUSTOMER C, CUSTOMER_BANK CB, CUSTOMER_ADDR CA, USER_LOGIN U
WHERE C.CUSTOMER_ID = CB.CUSTOMER_ID AND
C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND NVL(CA.ADDR_TYPE, 2) = 2 AND U.USER_ID = C.USER_ID
/

-- Drop the old synonym VW_EFT_AUTH_INFO
DROP SYNONYM vw_eft_auth_info
/

-- Create synonym VW_EFT_AUTH_INFO
CREATE SYNONYM vw_eft_auth_info
  FOR vw_eft_auth_info
/

-- Grants for View
GRANT DELETE ON vw_eft_auth_info TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_eft_auth_info TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_eft_auth_info TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_eft_auth_info TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_EFT_AUTH_INFO

-- Start of DDL Script for View CORP.VW_EFT_EXPORT
-- Generated 10/13/2004 9:25:03 AM from CORP@USADBD02

-- Drop the old instance of VW_EFT_EXPORT
DROP VIEW vw_eft_export
/

CREATE OR REPLACE VIEW vw_eft_export (
   eft_id,
   customer_bank_id,
   gross_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount,
   adjust_desc,
   net_amount,
   refund_chargeback_amount )
AS
SELECT E.EFT_ID, CUSTOMER_BANK_ID, SUM(GROSS_AMOUNT), SUM(PROCESS_FEE_AMOUNT), SUM(SERVICE_FEE_AMOUNT), SUM(ADJUST_AMOUNT), GET_ADJUST_DESC(E.EFT_ID), SUM(NET_AMOUNT), NVL(SUM(REFUND_AMOUNT),0) + NVL(SUM(CHARGEBACK_AMOUNT),0)
FROM EFT E, PAYMENTS P WHERE E.EFT_ID = P.EFT_ID
GROUP BY E.EFT_ID, CUSTOMER_BANK_ID
/

-- Grants for View
GRANT DELETE ON vw_eft_export TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_eft_export TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_eft_export TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_eft_export TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_EFT_EXPORT

-- Start of DDL Script for View CORP.VW_LICENSE
-- Generated 10/13/2004 9:25:04 AM from CORP@USADBD02

-- Drop the old instance of VW_LICENSE
DROP VIEW vw_license
/

CREATE OR REPLACE VIEW vw_license (
   license_id,
   title,
   description,
   credit_fee,
   debit_fee,
   pass_fee,
   access_fee,
   maint_fee,
   cash_fee,
   refund_fee,
   chargeback_fee,
   service_fee )
AS
SELECT LICENSE_ID, TITLE, DESCRIPTION,
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF1 WHERE LPF1.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 16),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF2 WHERE LPF2.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 19),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF3 WHERE LPF3.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 18),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF4 WHERE LPF4.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 17),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF5 WHERE LPF5.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 23),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF6 WHERE LPF6.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 22),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF7 WHERE LPF7.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 20),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF8 WHERE LPF8.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 21),
(SELECT AMOUNT FROM LICENSE_SERVICE_FEES LSF1 WHERE LSF1.LICENSE_ID = L.LICENSE_ID AND FEE_ID = 1 AND FREQUENCY_ID = 2)
FROM LICENSE L
/


-- End of DDL Script for View CORP.VW_LICENSE

-- Start of DDL Script for View CORP.VW_LICENSE_INFO
-- Generated 10/13/2004 9:25:04 AM from CORP@USADBD02

-- Drop the old instance of VW_LICENSE_INFO
DROP VIEW vw_license_info
/

CREATE OR REPLACE VIEW vw_license_info (
   customer_id,
   customer_name,
   address1,
   city,
   state,
   zip,
   license_nbr,
   telephone,
   fax,
   dealer_name,
   credit_process_fee,
   monthly_service_fee )
AS
SELECT C.CUSTOMER_ID, CUSTOMER_NAME, ADDRESS1, CITY, STATE, ZIP, LICENSE_NBR, TELEPHONE, FAX, DEALER_NAME, 
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF WHERE L.LICENSE_ID = LPF.LICENSE_ID AND LPF.TRANS_TYPE_ID = 16), 
(SELECT AMOUNT FROM LICENSE_SERVICE_FEES LSF WHERE L.LICENSE_ID = LSF.LICENSE_ID AND FREQUENCY_ID = 2 AND FEE_ID = 1) 
FROM CUSTOMER C, CUSTOMER_ADDR CA, USER_LOGIN U, DEALER D, LICENSE_NBR L 
WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND NVL(CA.ADDR_TYPE, 2) = 2 AND U.USER_ID = C.USER_ID 
AND C.DEALER_ID = D.DEALER_ID (+) AND C.CUSTOMER_ID = L.CUSTOMER_ID (+)
/

-- Drop the old synonym VW_LICENSE_INFO
DROP SYNONYM vw_license_info
/

-- Create synonym VW_LICENSE_INFO
CREATE SYNONYM vw_license_info
  FOR vw_license_info
/

-- Grants for View
GRANT DELETE ON vw_license_info TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_license_info TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_license_info TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_license_info TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_LICENSE_INFO

-- Start of DDL Script for View CORP.VW_LOCATION
-- Generated 10/13/2004 9:25:05 AM from CORP@USADBD02

-- Drop the old instance of VW_LOCATION
DROP VIEW vw_location
/

CREATE OR REPLACE VIEW vw_location (
   location_id,
   location_name,
   location_description,
   address1,
   city,
   state,
   zip )
AS
SELECT L.LOCATION_ID, L.LOCATION_NAME, L.DESCRIPTION, A.ADDRESS1, A.CITY, A.STATE, A.ZIP
       FROM CUSTOMER_ADDR A, LOCATION L
       WHERE L.ADDRESS_ID =  A.ADDRESS_ID
/


-- End of DDL Script for View CORP.VW_LOCATION

-- Start of DDL Script for View CORP.VW_LOCKED_PAYMENTS
-- Generated 10/13/2004 9:25:06 AM from CORP@USADBD02

-- Drop the old instance of VW_LOCKED_PAYMENTS
DROP VIEW vw_locked_payments
/

CREATE OR REPLACE VIEW vw_locked_payments (
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   gross_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   eft_id,
   batch_ref_nbr,
   description,
   adjust_amount )
AS
SELECT D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, A.CREDIT_AMOUNT,
A.REFUND_AMOUNT, A.CHARGEBACK_AMOUNT, A.PROCESS_FEE_AMOUNT, A.SERVICE_FEE_AMOUNT,
 D.DOC_ID, D.REF_NBR, D.DESCRIPTION, A.ADJUST_AMOUNT
FROM DOC D, (
    SELECT DOC_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT D.DOC_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND L.DELETED = 'N'
          AND D.STATUS = 'L'
          GROUP BY D.DOC_ID, L.ENTRY_TYPE) M
      GROUP BY DOC_ID) A
WHERE D.DOC_ID = A.DOC_ID
/


-- End of DDL Script for View CORP.VW_LOCKED_PAYMENTS

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL
-- Generated 10/13/2004 9:25:06 AM from CORP@USADBD02

-- Drop the old instance of VW_MACGRAY_EFT_DETAIL
DROP VIEW vw_macgray_eft_detail
/

CREATE OR REPLACE VIEW vw_macgray_eft_detail (
   asset_nbr,
   location_name,
   terminal_nbr,
   bank_acct_nbr,
   gross_amount,
   terminal_fee,
   transaction_fee,
   adjustment,
   explanation,
   last_tran_dt,
   eft_id,
   eft_date )
AS
select    
	   vw.asset_nbr as asset_nbr,   
	   vw.location_name as location_name,   
	   vw.terminal_nbr as terminal_nbr,   
	   cb.BANK_ACCT_NBR as bank_acct_nbr,   
	   pay.GROSS_AMOUNT as gross_amount,   
	   sf.FEE_AMOUNT as terminal_fee,    
	   pf.FEE_PERCENT * pay.GROSS_AMOUNT as transaction_fee,   
	   pa.AMOUNT as adjustment,   
	   pa.REASON as explanation,   
	   last_tran_date(eft.eft_id, cbt.TERMINAL_ID) as last_tran_dt,    
	   eft.BATCH_REF_NBR as eft_id,   
	   eft.EFT_DATE   
from    
	   customer_bank_terminal cbt,   
	   customer_bank cb,   
	   payments  pay,   
	   service_fees  sf,    
	   payment_process_fee  pf,   
	   payment_adjustment  pa,    
	   eft,   
	   vw_terminal  vw   
where   
	  cbt.terminal_id = vw.terminal_id and   
	  cbt.customer_bank_id = cb.customer_bank_id and   
	  /* this is currently hardcoded but should be parameterized */   
	  cb.customer_id = 57 and   
	  /***********************************************************/   
	  vw.terminal_id = pay.terminal_id and   
	  eft.eft_id = pay.eft_id and    
	  pa.eft_id (+)= eft.eft_id and   
	  pf.payment_id (+)= pay.payment_id and   
	  sf.terminal_id = vw.terminal_id 
order by vw.asset_nbr
/


-- End of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL_OLD
-- Generated 10/13/2004 9:25:07 AM from CORP@USADBD02

-- Drop the old instance of VW_MACGRAY_EFT_DETAIL_OLD
DROP VIEW vw_macgray_eft_detail_old
/

CREATE OR REPLACE VIEW vw_macgray_eft_detail_old (
   asset_nbr,
   location_name,
   terminal_nbr,
   bank_acct_nbr,
   gross_amount,
   terminal_fee,
   transaction_fee,
   adjustment,
   explanation,
   last_tran_dt,
   eft_id,
   eft_date )
AS
select    
	   vw.asset_nbr as asset_nbr,   
	   vw.location_name as location_name,   
	   vw.terminal_nbr as terminal_nbr,   
	   cb.BANK_ACCT_NBR as bank_acct_nbr,   
	   pay.GROSS_AMOUNT as gross_amount,   
	   sf.FEE_AMOUNT as terminal_fee,    
	   pf.FEE_PERCENT * pay.GROSS_AMOUNT as transaction_fee,   
	   pa.AMOUNT as adjustment,   
	   pa.REASON as explanation,   
	   last_tran_date(eft.eft_id, cbt.TERMINAL_ID) as last_tran_dt,    
	   eft.BATCH_REF_NBR as eft_id,   
	   eft.EFT_DATE   
from    
	   customer_bank_terminal cbt,   
	   customer_bank cb,   
	   payments  pay,   
	   service_fees  sf,    
	   payment_process_fee  pf,   
	   payment_adjustment  pa,    
	   eft,   
	   vw_terminal  vw   
where   
	  cbt.terminal_id = vw.terminal_id and   
	  cbt.customer_bank_id = cb.customer_bank_id and   
	  /* this is currently hardcoded but should be parameterized */   
	  cb.customer_id = 57 and   
	  /***********************************************************/   
	  vw.terminal_id = pay.terminal_id and   
	  eft.eft_id = pay.eft_id and    
	  pa.eft_id (+)= eft.eft_id and   
	  pf.payment_id (+)= pay.payment_id and   
	  sf.terminal_id = vw.terminal_id	and   
	  eft.eft_date between to_date('8/1/2002','mm/dd/yyyy') and to_date('8/31/2002','mm/dd/yyyy')   
order by vw.asset_nbr
/


-- End of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL_OLD

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_DETAIL
-- Generated 10/13/2004 9:25:08 AM from CORP@USADBD02

-- Drop the old instance of VW_MACGRAY_EFT_MONTHLY_DETAIL
DROP VIEW vw_macgray_eft_monthly_detail
/

CREATE OR REPLACE VIEW vw_macgray_eft_monthly_detail (
   bank_acct_nbr,
   gross_amount,
   terminal_fee,
   transaction_fee,
   last_tran_dt,
   location_name,
   eft_id,
   adjustment,
   explanation,
   eft_date )
AS
SELECT	trim(bank_acct_nbr), Gross_Amount, 
		Terminal_Fee, Transaction_Fee,
		Last_Tran_Dt, location_name,
		eft_ID, Adjustment, Explanation, eft_date
FROM    vw_MacGray_EFT_detail
/

-- Grants for View
GRANT DELETE ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_DETAIL

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUM_SUB
-- Generated 10/13/2004 9:25:08 AM from CORP@USADBD02

-- Drop the old instance of VW_MACGRAY_EFT_MONTHLY_SUM_SUB
DROP VIEW vw_macgray_eft_monthly_sum_sub
/

CREATE OR REPLACE VIEW vw_macgray_eft_monthly_sum_sub (
   tagid,
   grossamt,
   termfee,
   tranfee,
   lasttrandt,
   location,
   eft_id,
   eft_date )
AS
SELECT     asset_nbr as TagID, 
		   SUM(Gross_Amount) AS GrossAmt, 
		   MAX(Terminal_Fee) AS TermFee, 
		   SUM(Transaction_Fee) AS TranFee, 
		   MAX(Last_Tran_Dt) AS LastTranDt, 
		   location_name as location, 
           eft_ID,
		   eft_date
FROM         vw_MacGray_EFT_detail 
GROUP BY asset_nbr, location_Name, eft_ID, eft_date
/


-- End of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUM_SUB

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUMMARY
-- Generated 10/13/2004 9:25:09 AM from CORP@USADBD02

-- Drop the old instance of VW_MACGRAY_EFT_MONTHLY_SUMMARY
DROP VIEW vw_macgray_eft_monthly_summary
/

CREATE OR REPLACE VIEW vw_macgray_eft_monthly_summary (
   tagid,
   grossamt,
   termfee,
   tranfee,
   lasttrandt,
   name,
   eft_id,
   eft_date )
AS
SELECT     TagID, 
		   GrossAmt, 
		   TermFee, 
		   TranFee, 
		   LastTranDt, 
		   location, 
           eft_ID,
		   eft_date
FROM       VW_MACGRAY_EFT_MONTHLY_SUM_sub
/

-- Grants for View
GRANT DELETE ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUMMARY

-- Start of DDL Script for View CORP.VW_NO_LICENSE
-- Generated 10/13/2004 9:25:10 AM from CORP@USADBD02

-- Drop the old instance of VW_NO_LICENSE
DROP VIEW vw_no_license
/

CREATE OR REPLACE VIEW vw_no_license (
   customer_id,
   customer_name )
AS
SELECT CUSTOMER_ID, CUSTOMER_NAME FROM CUSTOMER
WHERE CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM VW_CURRENT_LICENSE)
/


-- End of DDL Script for View CORP.VW_NO_LICENSE

-- Start of DDL Script for View CORP.VW_PAID_DATE
-- Generated 10/13/2004 9:25:10 AM from CORP@USADBD02

-- Drop the old instance of VW_PAID_DATE
DROP VIEW vw_paid_date
/

CREATE OR REPLACE VIEW vw_paid_date (
   ledger_id,
   eft_date )
AS
SELECT L.LEDGER_ID, (CASE WHEN D.STATUS = 'D' THEN L.LEDGER_DATE ELSE D.SENT_DATE END)
FROM LEDGER L, BATCH B, DOC D
WHERE L.BATCH_ID = B.BATCH_ID AND B.DOC_ID = D.DOC_ID (+)
AND NVL(D.STATUS, 'N') IN ('P', 'S', 'D', 'N')
AND L.ENTRY_TYPE IN('CC', 'RF', 'CB')
/


-- End of DDL Script for View CORP.VW_PAID_DATE

-- Start of DDL Script for View CORP.VW_PAID_EFTS
-- Generated 10/13/2004 9:25:11 AM from CORP@USADBD02

-- Drop the old instance of VW_PAID_EFTS
DROP VIEW vw_paid_efts
/

CREATE OR REPLACE VIEW vw_paid_efts (
   eft_id,
   eft_date,
   batch_ref_nbr,
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   eft_amount,
   bank_name,
   customer_name )
AS
SELECT D.DOC_ID, D.SENT_DATE, D.REF_NBR, D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, D.TOTAL_AMOUNT,
CB.BANK_NAME, C.CUSTOMER_NAME
FROM DOC D, CUSTOMER_BANK CB, CUSTOMER C
WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID (+) AND CB.CUSTOMER_ID = C.CUSTOMER_ID (+) AND D.STATUS IN('P', 'S')
AND D.DOC_TYPE = 'FT'
/

-- Drop the old synonym VW_PAID_EFTS
DROP SYNONYM vw_paid_efts
/

-- Create synonym VW_PAID_EFTS
CREATE SYNONYM vw_paid_efts
  FOR vw_paid_efts
/

-- Grants for View
GRANT DELETE ON vw_paid_efts TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_paid_efts TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_paid_efts TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_paid_efts TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_PAID_EFTS

-- Start of DDL Script for View CORP.VW_PAYMENT_PROCESS_FEES
-- Generated 10/13/2004 9:25:11 AM from CORP@USADBD02

-- Drop the old instance of VW_PAYMENT_PROCESS_FEES
DROP VIEW vw_payment_process_fees
/

CREATE OR REPLACE VIEW vw_payment_process_fees (
   doc_id,
   batch_id,
   batch_closed,
   customer_bank_id,
   terminal_id,
   trans_type_id,
   trans_type_name,
   gross_amount,
   fee_percent,
   fixed_fee_amount,
   process_fee_amount,
   net_amount )
AS
SELECT B.DOC_ID, B.BATCH_ID, B.CLOSED, B.CUSTOMER_BANK_ID, B.TERMINAL_ID, T.TRANS_TYPE_ID,
TT.TRANS_TYPE_NAME, SUM(L1.AMOUNT) GROSS_AMOUNT, PF.FEE_PERCENT, PF.FEE_AMOUNT FIXED_FEE_AMOUNT, SUM(L.AMOUNT) PROCESS_FEE_AMOUNT,
SUM(NVL(L1.AMOUNT, 0) + L.AMOUNT) NET_AMOUNT
FROM BATCH B, LEDGER L, LEDGER L1, PROCESS_FEES PF, TRANS T, TRANS_TYPE TT
WHERE B.BATCH_ID = L.BATCH_ID
AND L.TRANS_ID = T.TRAN_ID
AND T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
AND L.PROCESS_FEE_ID = PF.PROCESS_FEE_ID
AND L.ENTRY_TYPE = 'PF'
AND L.TRANS_ID = L1.TRANS_ID (+)
AND NVL(L1.ENTRY_TYPE, '') IN('CC', 'RF', 'CB', '')
GROUP BY B.DOC_ID, B.BATCH_ID, B.CLOSED, B.CUSTOMER_BANK_ID, B.TERMINAL_ID, T.TRANS_TYPE_ID,
TT.TRANS_TYPE_NAME, PF.FEE_PERCENT, PF.FEE_AMOUNT
/


-- End of DDL Script for View CORP.VW_PAYMENT_PROCESS_FEES

-- Start of DDL Script for View CORP.VW_PAYMENT_REPORT
-- Generated 10/13/2004 9:25:12 AM from CORP@USADBD02

-- Drop the old instance of VW_PAYMENT_REPORT
DROP VIEW vw_payment_report
/

CREATE OR REPLACE VIEW vw_payment_report (
   payment_id,
   eft_id,
   terminal_id,
   customer_bank_id,
   gross_amount,
   settle_failed_amount,
   tax_amount,
   process_fee_amount,
   service_fee_amount,
   refund_chargeback_amount,
   adjust_amount,
   net_amount,
   terminal_nbr,
   location_name,
   transactions,
   payment_start_date,
   payment_end_date,
   reason )
AS
SELECT V.BATCH_ID, V.DOC_ID, V.TERMINAL_ID, V.CUSTOMER_BANK_ID, V.CREDIT_AMOUNT, SETTLE_FAILED_AMOUNT, NULL, V.PROCESS_FEE_AMOUNT, V.SERVICE_FEE_AMOUNT,
CASE WHEN V.REFUND_AMOUNT IS NULL AND V.CHARGEBACK_AMOUNT IS NULL THEN NULL ELSE NVL(V.REFUND_AMOUNT, 0) + NVL(V.CHARGEBACK_AMOUNT, 0) END,
V.ADJUST_AMOUNT,
(NVL(CREDIT_AMOUNT, 0) + NVL(REFUND_AMOUNT, 0) + NVL(CHARGEBACK_AMOUNT, 0) + NVL(PROCESS_FEE_AMOUNT, 0) + NVL(SERVICE_FEE_AMOUNT, 0) + NVL(ADJUST_AMOUNT, 0)),
TERMINAL_NBR, LOCATION_NAME, V.TRAN_COUNT, V.START_DATE, V.END_DATE, PS.DESCRIPTION
FROM TERMINAL T, LOCATION L, PAYMENT_SCHEDULE PS, (
    SELECT DOC_ID, BATCH_ID, TERMINAL_ID, CUSTOMER_BANK_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'N' THEN AMOUNT ELSE NULL END) SETTLE_FAILED_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT,
           SUM(ENTRY_COUNT) TRAN_COUNT
      FROM (
        SELECT D.DOC_ID, B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.PAYMENT_SCHEDULE_ID,
             B.START_DATE, B.END_DATE, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT,
             COUNT(*) ENTRY_COUNT, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) PAYABLE
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND L.DELETED = 'N'
          GROUP BY D.DOC_ID, B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.PAYMENT_SCHEDULE_ID,
             B.START_DATE, B.END_DATE, L.ENTRY_TYPE, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE)) M
      GROUP BY DOC_ID, BATCH_ID, TERMINAL_ID, CUSTOMER_BANK_ID, PAYMENT_SCHEDULE_ID,
             START_DATE, END_DATE
      ) V
WHERE V.TERMINAL_ID = T.TERMINAL_ID (+) AND T.LOCATION_ID = L.LOCATION_ID (+)
AND V.PAYMENT_SCHEDULE_ID = PS.PAYMENT_SCHEDULE_ID
/

-- Drop the old synonym VW_PAYMENT_REPORT
DROP SYNONYM vw_payment_report
/

-- Create synonym VW_PAYMENT_REPORT
CREATE SYNONYM vw_payment_report
  FOR vw_payment_report
/

-- Grants for View
GRANT DELETE ON vw_payment_report TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_payment_report TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_payment_report TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_payment_report TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_PAYMENT_REPORT

-- Start of DDL Script for View CORP.VW_PAYMENT_SERVICE_FEES
-- Generated 10/13/2004 9:25:13 AM from CORP@USADBD02

-- Drop the old instance of VW_PAYMENT_SERVICE_FEES
DROP VIEW vw_payment_service_fees
/

CREATE OR REPLACE VIEW vw_payment_service_fees (
   doc_id,
   batch_id,
   ledger_id,
   fee_amount,
   fee_name,
   fee_date,
   terminal_id,
   customer_bank_id,
   batch_closed,
   fee_id,
   frequency_name )
AS
SELECT B.DOC_ID, B.BATCH_ID, L.LEDGER_ID, L.AMOUNT FEE_AMOUNT, FEE_NAME, L.LEDGER_DATE FEE_DATE,
B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.CLOSED, SF.FEE_ID, Q.NAME
FROM LEDGER L, FEES F, SERVICE_FEES SF, BATCH B, FREQUENCY Q
WHERE L.ENTRY_TYPE = 'SF' AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID AND SF.FEE_ID = F.FEE_ID
AND L.DELETED = 'N' AND L.BATCH_ID = B.BATCH_ID AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
/

-- Drop the old synonym VW_PAYMENT_SERVICE_FEES
DROP SYNONYM vw_payment_service_fees
/

-- Create synonym VW_PAYMENT_SERVICE_FEES
CREATE SYNONYM vw_payment_service_fees
  FOR vw_payment_service_fees
/

-- Grants for View
GRANT DELETE ON vw_payment_service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_payment_service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_payment_service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_payment_service_fees TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_PAYMENT_SERVICE_FEES

-- Start of DDL Script for View CORP.VW_PENDING_ADJUSTMENTS
-- Generated 10/13/2004 9:25:13 AM from CORP@USADBD02

-- Drop the old instance of VW_PENDING_ADJUSTMENTS
DROP VIEW vw_pending_adjustments
/

CREATE OR REPLACE VIEW vw_pending_adjustments (
   customer_bank_id,
   adjust_amount )
AS
SELECT CUSTOMER_BANK_ID, SUM(AMOUNT)
FROM VW_ADJUSTMENTS
WHERE BATCH_CLOSED = 'N'
GROUP BY CUSTOMER_BANK_ID
/


-- End of DDL Script for View CORP.VW_PENDING_ADJUSTMENTS

-- Start of DDL Script for View CORP.VW_PENDING_OR_LOCKED_PAYMENTS
-- Generated 10/13/2004 9:25:14 AM from CORP@USADBD02

-- Drop the old instance of VW_PENDING_OR_LOCKED_PAYMENTS
DROP VIEW vw_pending_or_locked_payments
/

CREATE OR REPLACE VIEW vw_pending_or_locked_payments (
   customer_name,
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   pay_min_amount,
   gross_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   eft_id,
   status,
   batch_ref_nbr,
   description,
   net_amount,
   adjust_amount )
AS
SELECT C.CUSTOMER_NAME, P.CUSTOMER_BANK_ID,
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, P.PAY_MIN_AMOUNT, P.GROSS_AMOUNT,
P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.SERVICE_FEE_AMOUNT,  0, '', '' , '' ,
NVL(P.GROSS_AMOUNT, 0) + NVL(P.REFUND_AMOUNT, 0) + NVL(P.CHARGEBACK_AMOUNT, 0)
 + NVL(P.PROCESS_FEE_AMOUNT, 0) + NVL(P.SERVICE_FEE_AMOUNT, 0) + NVL(P.ADJUST_AMOUNT, 0),
  P.ADJUST_AMOUNT--, REASON
FROM VW_PENDING_PAYMENTS P, CUSTOMER C 
WHERE P.CUSTOMER_ID = C.CUSTOMER_ID
UNION SELECT C.CUSTOMER_NAME, P.CUSTOMER_BANK_ID,
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, CB.PAY_MIN_AMOUNT, P.GROSS_AMOUNT,
P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.SERVICE_FEE_AMOUNT,
P.EFT_ID, 'Locked', P.BATCH_REF_NBR, P.DESCRIPTION,
NVL(P.GROSS_AMOUNT, 0) + NVL(P.REFUND_AMOUNT, 0) + NVL(P.CHARGEBACK_AMOUNT, 0) + NVL(P.PROCESS_FEE_AMOUNT, 0) + NVL(P.SERVICE_FEE_AMOUNT, 0) + NVL(P.ADJUST_AMOUNT, 0),
P.ADJUST_AMOUNT--, REASON
FROM VW_LOCKED_PAYMENTS P, CUSTOMER_BANK CB, CUSTOMER C 
WHERE P.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND CB.CUSTOMER_ID = C.CUSTOMER_ID
/


-- End of DDL Script for View CORP.VW_PENDING_OR_LOCKED_PAYMENTS

-- Start of DDL Script for View CORP.VW_PENDING_PAYMENTS
-- Generated 10/13/2004 9:25:15 AM from CORP@USADBD02

-- Drop the old instance of VW_PENDING_PAYMENTS
DROP VIEW vw_pending_payments
/

CREATE OR REPLACE VIEW vw_pending_payments (
   customer_bank_id,
   customer_id,
   bank_acct_nbr,
   bank_routing_nbr,
   pay_min_amount,
   gross_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount )
AS
SELECT CB.CUSTOMER_BANK_ID, CB.CUSTOMER_ID, CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR, CB.PAY_MIN_AMOUNT, A.CREDIT_AMOUNT,
   A.REFUND_AMOUNT, A.CHARGEBACK_AMOUNT, A.PROCESS_FEE_AMOUNT,
   A.SERVICE_FEE_AMOUNT, A.ADJUST_AMOUNT
FROM CUSTOMER_BANK CB, VW_PENDING_REVENUE A
WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID
/*AND (CB.STATUS = 'A' OR NVL(A.CREDIT_AMOUNT, 0) <> 0 OR NVL(A.REFUND_AMOUNT, 0) <> 0
     OR NVL(A.CHARGEBACK_AMOUNT, 0) <> 0 OR NVL(A.PROCESS_FEE_AMOUNT, 0) <> 0
     OR NVL(A.SERVICE_FEE_AMOUNT, 0) <> 0 OR NVL(A.ADJUST_AMOUNT, 0) <> 0)*/
/


-- End of DDL Script for View CORP.VW_PENDING_PAYMENTS

-- Start of DDL Script for View CORP.VW_PENDING_REVENUE
-- Generated 10/13/2004 9:25:15 AM from CORP@USADBD02

-- Drop the old instance of VW_PENDING_REVENUE
DROP VIEW vw_pending_revenue
/

CREATE OR REPLACE VIEW vw_pending_revenue (
   customer_bank_id,
   credit_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount )
AS
SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B
          WHERE L.BATCH_ID = B.BATCH_ID
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND L.DELETED = 'N'
          AND B.CLOSED = 'N'
          AND PAYMENTS_PKG.BATCH_CLOSABLE(B.BATCH_ID) = 'Y'
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
      GROUP BY CUSTOMER_BANK_ID
/


-- End of DDL Script for View CORP.VW_PENDING_REVENUE

-- Start of DDL Script for View CORP.VW_PENDING_SERVICE_FEES
-- Generated 10/13/2004 9:25:16 AM from CORP@USADBD02

-- Drop the old instance of VW_PENDING_SERVICE_FEES
DROP VIEW vw_pending_service_fees
/

CREATE OR REPLACE VIEW vw_pending_service_fees (
   customer_bank_id,
   service_fee_amount )
AS
SELECT B.CUSTOMER_BANK_ID, SUM(L.AMOUNT)
FROM LEDGER L, BATCH B
WHERE L.BATCH_ID = B.BATCH_ID
AND B.CLOSED = 'N'
AND L.DELETED = 'N'
GROUP BY B.CUSTOMER_BANK_ID
/


-- End of DDL Script for View CORP.VW_PENDING_SERVICE_FEES

-- Start of DDL Script for View CORP.VW_UNPAID_TRANS_AND_FEES
-- Generated 10/13/2004 9:25:17 AM from CORP@USADBD02

-- Drop the old instance of VW_UNPAID_TRANS_AND_FEES
DROP VIEW vw_unpaid_trans_and_fees
/

CREATE OR REPLACE VIEW vw_unpaid_trans_and_fees (
   customer_name,
   customer_id,
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   pay_min_amount,
   credit_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount )
AS
SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
    SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND L.DELETED = 'N'
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND B.DOC_ID = D.DOC_ID (+)
          AND NVL(D.SENT_DATE, MIN_DATE) < SYSDATE
          AND L.LEDGER_DATE < SYSDATE
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
      GROUP BY CUSTOMER_BANK_ID) A
WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
AND CB.CUSTOMER_ID = C.CUSTOMER_ID
ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR
/


-- End of DDL Script for View CORP.VW_UNPAID_TRANS_AND_FEES

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS
-- Generated 10/13/2004 9:25:17 AM from CORP@USADBD02

-- Drop the old instance of VW_UNSENT_EFTS
DROP VIEW vw_unsent_efts
/

CREATE OR REPLACE VIEW vw_unsent_efts (
   eft_id,
   transaction_code,
   dfi,
   check_digit,
   bank_acct_nbr,
   amount,
   trace_number,
   description,
   company_id_nbr,
   company_name )
AS
SELECT D.DOC_ID,
(CASE WHEN D.TOTAL_AMOUNT > 0 AND CB.ACCOUNT_TYPE = 'C' THEN 22 WHEN D.TOTAL_AMOUNT < 0 AND CB.ACCOUNT_TYPE = 'C' THEN 27
WHEN D.TOTAL_AMOUNT > 0 AND CB.ACCOUNT_TYPE = 'S' THEN 32 WHEN D.TOTAL_AMOUNT < 0 AND CB.ACCOUNT_TYPE = 'S' THEN 37 END)
/*22=DEPOSIT TO CHECKING, 27=WITHDRAWL FROM CHECKING, 32=DEPOSIT TO SAVINGS, 37=WITHDRAWL FROM SAVINGS*/,      
SUBSTR(TO_CHAR(D.BANK_ROUTING_NBR, 'FM000000000'),1,8),
SUBSTR(TO_CHAR(D.BANK_ROUTING_NBR, 'FM000000000'),9,1),
D.BANK_ACCT_NBR, ABS(D.TOTAL_AMOUNT),
'03100005'/*OUR ROUTING NUMBER*/ || TO_CHAR(D.DOC_ID, 'FM000000'),
D.DESCRIPTION, D.CUSTOMER_BANK_ID, C.CUSTOMER_NAME
FROM DOC D, CUSTOMER_BANK CB, CUSTOMER C			
WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND D.STATUS = 'A' AND CB.CUSTOMER_ID = C.CUSTOMER_ID
AND D.TOTAL_AMOUNT <> 0 AND CB.ACCOUNT_TYPE IN('S','C')
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS_TEST
-- Generated 10/13/2004 9:25:18 AM from CORP@USADBD02

-- Drop the old instance of VW_UNSENT_EFTS_TEST
DROP VIEW vw_unsent_efts_test
/

CREATE OR REPLACE VIEW vw_unsent_efts_test (
   eft_id,
   transaction_code,
   dfi,
   check_digit,
   bank_acct_nbr,
   amount,
   trace_number,
   description,
   company_id_nbr,
   company_name )
AS
SELECT EFT_ID,        
(CASE WHEN ACCOUNT_TYPE = 'C' THEN 22 WHEN ACCOUNT_TYPE = 'C' THEN 27        
--WHEN EFT_AMOUNT >= 0 AND ACCOUNT_TYPE = 'S' THEN 32 WHEN EFT_AMOUNT < 0 AND ACCOUNT_TYPE = 'S' THEN 37  
END)       
/*22=DEPOSIT TO CHECKING, 27=WITHDRAWL FROM CHECKING, 32=DEPOSIT TO SAVINGS, 37=WITHDRAWL FROM SAVINGS*/,       
SUBSTR(TO_CHAR(CB.BANK_ROUTING_NBR, 'FM000000000'),1,8),       
SUBSTR(TO_CHAR(CB.BANK_ROUTING_NBR, 'FM000000000'),9,1),       
CB.BANK_ACCT_NBR, 0,       
'03100005'/*OUR ROUTING NUMBER*/ || TO_CHAR(EFT_ID, 'FM000000'),       
E.DESCRIPTION, CB.CUSTOMER_BANK_ID, CUSTOMER_NAME       
FROM EFT E, CUSTOMER_BANK CB, CUSTOMER C			       
WHERE E.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND E.STATUS = 'S' AND CB.CUSTOMER_ID = C.CUSTOMER_ID     
AND ACCOUNT_TYPE IN('S','C')  
AND CB.CUSTOMER_BANK_ID IN(33,25) AND EFT_ID > 180
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS_TEST

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS
-- Generated 10/13/2004 9:25:18 AM from CORP@USADBD02

-- Drop the old instance of VW_UNSENT_EFTS_TOTALS
DROP VIEW vw_unsent_efts_totals
/

CREATE OR REPLACE VIEW vw_unsent_efts_totals (
   entry_count,
   entry_hash,
   total_debit,
   total_credit,
   block_count )
AS
SELECT COUNT(EFT_ID) * 2, SUM(TO_NUMBER(DFI)),        
NVL(SUM(CASE WHEN TRANSACTION_CODE = 27 OR TRANSACTION_CODE = 37 THEN AMOUNT ELSE NULL END), 0),       
NVL(SUM(CASE WHEN TRANSACTION_CODE = 22 OR TRANSACTION_CODE = 32 THEN AMOUNT ELSE NULL END), 0),      
CEIL((4 + (2 * COUNT(EFT_ID))) / 10)        
FROM VW_UNSENT_EFTS
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS_TEST
-- Generated 10/13/2004 9:25:19 AM from CORP@USADBD02

-- Drop the old instance of VW_UNSENT_EFTS_TOTALS_TEST
DROP VIEW vw_unsent_efts_totals_test
/

CREATE OR REPLACE VIEW vw_unsent_efts_totals_test (
   entry_count,
   entry_hash,
   total_debit,
   total_credit,
   block_count )
AS
SELECT COUNT(EFT_ID) * 2, SUM(TO_NUMBER(DFI)),         
NVL(SUM(CASE WHEN TRANSACTION_CODE = 27 OR TRANSACTION_CODE = 37 THEN AMOUNT ELSE NULL END), 0),        
NVL(SUM(CASE WHEN TRANSACTION_CODE = 22 OR TRANSACTION_CODE = 32 THEN AMOUNT ELSE NULL END), 0),       
CEIL((4 + (2 * COUNT(EFT_ID))) / 10)         
FROM VW_UNSENT_EFTS_TEST
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS_TEST

-- Start of DDL Script for View CORP.VW_USER
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of VW_USER
DROP VIEW vw_user
/

CREATE OR REPLACE VIEW vw_user (
   user_id,
   customer_id,
   user_name,
   first_name,
   last_name,
   password,
   email,
   is_primary,
   telephone,
   fax )
AS
SELECT USER_ID, CUSTOMER_ID, USER_NAME, FIRST_NAME, LAST_NAME, USER_PASSWD, EMAIL,
NVL((SELECT 'Y' FROM CUSTOMER C WHERE C.USER_ID = U.USER_ID AND C.CUSTOMER_ID = U.CUSTOMER_ID), 'N'), TELEPHONE, FAX
FROM USER_LOGIN U WHERE STATUS = 'A'
/


-- End of DDL Script for View CORP.VW_USER

-- Start of DDL Script for Index CORP.BIX_BATCH_CLOSED
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of BIX_BATCH_CLOSED
DROP INDEX bix_batch_closed
/

CREATE BITMAP INDEX bix_batch_closed ON batch
  (
    closed                          ASC
  )
/


-- End of DDL Script for Index CORP.BIX_BATCH_CLOSED

-- Start of DDL Script for Index CORP.BIX_DOC_STATUS
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of BIX_DOC_STATUS
DROP INDEX bix_doc_status
/

CREATE BITMAP INDEX bix_doc_status ON doc
  (
    status                          ASC
  )
/


-- End of DDL Script for Index CORP.BIX_DOC_STATUS

-- Start of DDL Script for Index CORP.BIX_LEDGER_DELETED
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of BIX_LEDGER_DELETED
DROP INDEX bix_ledger_deleted
/

CREATE BITMAP INDEX bix_ledger_deleted ON ledger
  (
    deleted                         ASC
  )
/


-- End of DDL Script for Index CORP.BIX_LEDGER_DELETED

-- Start of DDL Script for Index CORP.BIX_LEDGER_ENTRY_TYPE
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of BIX_LEDGER_ENTRY_TYPE
DROP INDEX bix_ledger_entry_type
/

CREATE BITMAP INDEX bix_ledger_entry_type ON ledger
  (
    entry_type                      ASC
  )
/


-- End of DDL Script for Index CORP.BIX_LEDGER_ENTRY_TYPE

-- Start of DDL Script for Index CORP.DOC_CUSTOMER_BANK_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of DOC_CUSTOMER_BANK_ID
DROP INDEX doc_customer_bank_id
/

CREATE INDEX doc_customer_bank_id ON doc
  (
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.DOC_CUSTOMER_BANK_ID

-- Start of DDL Script for Index CORP.IX_ADJUST_CREATE_DATE
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_ADJUST_CREATE_DATE
DROP INDEX ix_adjust_create_date
/

CREATE INDEX ix_adjust_create_date ON payment_adjustment
  (
    create_date                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_ADJUST_CREATE_DATE

-- Start of DDL Script for Index CORP.IX_ADJUST_CUST_BANK_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_ADJUST_CUST_BANK_ID
DROP INDEX ix_adjust_cust_bank_id
/

CREATE INDEX ix_adjust_cust_bank_id ON payment_adjustment
  (
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.IX_ADJUST_CUST_BANK_ID

-- Start of DDL Script for Index CORP.IX_ADJUST_EFT_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_ADJUST_EFT_ID
DROP INDEX ix_adjust_eft_id
/

CREATE INDEX ix_adjust_eft_id ON payment_adjustment
  (
    eft_id                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_ADJUST_EFT_ID

-- Start of DDL Script for Index CORP.IX_ADJUST_TERMINAL_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_ADJUST_TERMINAL_ID
DROP INDEX ix_adjust_terminal_id
/

CREATE INDEX ix_adjust_terminal_id ON payment_adjustment
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_ADJUST_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_BATCH_CUSTOMER_BANK_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_BATCH_CUSTOMER_BANK_ID
DROP INDEX ix_batch_customer_bank_id
/

CREATE INDEX ix_batch_customer_bank_id ON batch
  (
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.IX_BATCH_CUSTOMER_BANK_ID

-- Start of DDL Script for Index CORP.IX_BATCH_DOC_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_BATCH_DOC_ID
DROP INDEX ix_batch_doc_id
/

CREATE INDEX ix_batch_doc_id ON batch
  (
    doc_id                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_BATCH_DOC_ID

-- Start of DDL Script for Index CORP.IX_BATCH_PAYMENT_SCHEDULE_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_BATCH_PAYMENT_SCHEDULE_ID
DROP INDEX ix_batch_payment_schedule_id
/

CREATE INDEX ix_batch_payment_schedule_id ON batch
  (
    payment_schedule_id             ASC
  )
/


-- End of DDL Script for Index CORP.IX_BATCH_PAYMENT_SCHEDULE_ID

-- Start of DDL Script for Index CORP.IX_BATCH_START_END_DATES
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_BATCH_START_END_DATES
DROP INDEX ix_batch_start_end_dates
/

CREATE INDEX ix_batch_start_end_dates ON batch
  (
    start_date                      ASC,
    end_date                        ASC
  )
COMPRESS  2
/


-- End of DDL Script for Index CORP.IX_BATCH_START_END_DATES

-- Start of DDL Script for Index CORP.IX_BATCH_TERMINAL_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_BATCH_TERMINAL_ID
DROP INDEX ix_batch_terminal_id
/

CREATE INDEX ix_batch_terminal_id ON batch
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_BATCH_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_CBT_CUSTOMER_BANK_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CBT_CUSTOMER_BANK_ID
DROP INDEX ix_cbt_customer_bank_id
/

CREATE INDEX ix_cbt_customer_bank_id ON customer_bank_terminal
  (
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.IX_CBT_CUSTOMER_BANK_ID

-- Start of DDL Script for Index CORP.IX_CBT_END_DATE
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CBT_END_DATE
DROP INDEX ix_cbt_end_date
/

CREATE INDEX ix_cbt_end_date ON customer_bank_terminal
  (
    end_date                        ASC
  )
/


-- End of DDL Script for Index CORP.IX_CBT_END_DATE

-- Start of DDL Script for Index CORP.IX_CBT_START_DATE
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CBT_START_DATE
DROP INDEX ix_cbt_start_date
/

CREATE INDEX ix_cbt_start_date ON customer_bank_terminal
  (
    start_date                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_CBT_START_DATE

-- Start of DDL Script for Index CORP.IX_CBT_TERMINAL_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CBT_TERMINAL_ID
DROP INDEX ix_cbt_terminal_id
/

CREATE INDEX ix_cbt_terminal_id ON customer_bank_terminal
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_CBT_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_CL_CUSTOMER_ID
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CL_CUSTOMER_ID
DROP INDEX ix_cl_customer_id
/

CREATE INDEX ix_cl_customer_id ON customer_license
  (
    customer_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_CL_CUSTOMER_ID

-- Start of DDL Script for Index CORP.IX_CL_LICENSE_NBR
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CL_LICENSE_NBR
DROP INDEX ix_cl_license_nbr
/

CREATE INDEX ix_cl_license_nbr ON customer_license
  (
    license_nbr                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_CL_LICENSE_NBR

-- Start of DDL Script for Index CORP.IX_CL_RECEIVED
-- Generated 10/13/2004 9:25:20 AM from CORP@USADBD02

-- Drop the old instance of IX_CL_RECEIVED
DROP INDEX ix_cl_received
/

CREATE INDEX ix_cl_received ON customer_license
  (
    received                        ASC
  )
/


-- End of DDL Script for Index CORP.IX_CL_RECEIVED

-- Start of DDL Script for Index CORP.IX_CUST_BANK_CUSTOMER_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_CUST_BANK_CUSTOMER_ID
DROP INDEX ix_cust_bank_customer_id
/

CREATE INDEX ix_cust_bank_customer_id ON customer_bank
  (
    customer_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_CUST_BANK_CUSTOMER_ID

-- Start of DDL Script for Index CORP.IX_CUSTOMER_BANK_STATUS
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_CUSTOMER_BANK_STATUS
DROP INDEX ix_customer_bank_status
/

CREATE INDEX ix_customer_bank_status ON customer_bank
  (
    status                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_CUSTOMER_BANK_STATUS

-- Start of DDL Script for Index CORP.IX_CUSTOMER_STATUS
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_CUSTOMER_STATUS
DROP INDEX ix_customer_status
/

CREATE INDEX ix_customer_status ON customer
  (
    status                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_CUSTOMER_STATUS

-- Start of DDL Script for Index CORP.IX_DL_DEALER_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_DL_DEALER_ID
DROP INDEX ix_dl_dealer_id
/

CREATE INDEX ix_dl_dealer_id ON dealer_license
  (
    dealer_id                       ASC
  )
/


-- End of DDL Script for Index CORP.IX_DL_DEALER_ID

-- Start of DDL Script for Index CORP.IX_DL_LICENSE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_DL_LICENSE_ID
DROP INDEX ix_dl_license_id
/

CREATE INDEX ix_dl_license_id ON dealer_license
  (
    license_id                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_DL_LICENSE_ID

-- Start of DDL Script for Index CORP.IX_EFT_CB_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_EFT_CB_ID
DROP INDEX ix_eft_cb_id
/

CREATE INDEX ix_eft_cb_id ON eft
  (
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.IX_EFT_CB_ID

-- Start of DDL Script for Index CORP.IX_EFT_COMBO
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_EFT_COMBO
DROP INDEX ix_eft_combo
/

CREATE UNIQUE INDEX ix_eft_combo ON eft
  (
    status                          ASC,
    eft_date                        ASC,
    eft_id                          ASC
  )
COMPRESS  2
/


-- End of DDL Script for Index CORP.IX_EFT_COMBO

-- Start of DDL Script for Index CORP.IX_EFT_STATUS
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_EFT_STATUS
DROP INDEX ix_eft_status
/

CREATE INDEX ix_eft_status ON eft
  (
    status                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_EFT_STATUS

-- Start of DDL Script for Index CORP.IX_LEDGER_BATCH_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_BATCH_ID
DROP INDEX ix_ledger_batch_id
/

CREATE INDEX ix_ledger_batch_id ON ledger_old
  (
    batch_id                        ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_BATCH_ID

-- Start of DDL Script for Index CORP.IX_LEDGER_CLOSE_DATE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_CLOSE_DATE
DROP INDEX ix_ledger_close_date
/

CREATE INDEX ix_ledger_close_date ON ledger_old
  (
    close_date                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_CLOSE_DATE

-- Start of DDL Script for Index CORP.IX_LEDGER_EPORT_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_EPORT_ID
DROP INDEX ix_ledger_eport_id
/

CREATE INDEX ix_ledger_eport_id ON ledger_old
  (
    eport_id                        ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_EPORT_ID

-- Start of DDL Script for Index CORP.IX_LEDGER_PAYMENT_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_PAYMENT_ID
DROP INDEX ix_ledger_payment_id
/

CREATE INDEX ix_ledger_payment_id ON ledger_old
  (
    payment_id                      ASC,
    trans_id                        ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_PAYMENT_ID

-- Start of DDL Script for Index CORP.IX_LEDGER_PAYMT_ID_TERMINAL_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_PAYMT_ID_TERMINAL_ID
DROP INDEX ix_ledger_paymt_id_terminal_id
/

CREATE INDEX ix_ledger_paymt_id_terminal_id ON ledger_old
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_PAYMT_ID_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_LEDGER_TERMINAL_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_TERMINAL_ID
DROP INDEX ix_ledger_terminal_id
/

CREATE INDEX ix_ledger_terminal_id ON ledger_old
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_LEDGER_TRANS_TYPE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LEDGER_TRANS_TYPE_ID
DROP INDEX ix_ledger_trans_type_id
/

CREATE INDEX ix_ledger_trans_type_id ON ledger_old
  (
    trans_type_id                   ASC
  )
/


-- End of DDL Script for Index CORP.IX_LEDGER_TRANS_TYPE_ID

-- Start of DDL Script for Index CORP.IX_LICENSE_NBR_CUSTOMER_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LICENSE_NBR_CUSTOMER_ID
DROP INDEX ix_license_nbr_customer_id
/

CREATE INDEX ix_license_nbr_customer_id ON license_nbr
  (
    customer_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_LICENSE_NBR_CUSTOMER_ID

-- Start of DDL Script for Index CORP.IX_LICENSE_NBR_LICENSE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LICENSE_NBR_LICENSE_ID
DROP INDEX ix_license_nbr_license_id
/

CREATE INDEX ix_license_nbr_license_id ON license_nbr
  (
    license_id                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_LICENSE_NBR_LICENSE_ID

-- Start of DDL Script for Index CORP.IX_LICENSE_STATUS
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LICENSE_STATUS
DROP INDEX ix_license_status
/

CREATE INDEX ix_license_status ON license
  (
    status                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_LICENSE_STATUS

-- Start of DDL Script for Index CORP.IX_LSF_FREQUENCY_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_LSF_FREQUENCY_ID
DROP INDEX ix_lsf_frequency_id
/

CREATE INDEX ix_lsf_frequency_id ON license_service_fees
  (
    frequency_id                    ASC
  )
/


-- End of DDL Script for Index CORP.IX_LSF_FREQUENCY_ID

-- Start of DDL Script for Index CORP.IX_PAY_ADJ_EFT_ID_CUST_BANK_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAY_ADJ_EFT_ID_CUST_BANK_ID
DROP INDEX ix_pay_adj_eft_id_cust_bank_id
/

CREATE INDEX ix_pay_adj_eft_id_cust_bank_id ON payment_adjustment
  (
    eft_id                          ASC,
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAY_ADJ_EFT_ID_CUST_BANK_ID

-- Start of DDL Script for Index CORP.IX_PAY_PROC_FEE_PAY_ID_TERM_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAY_PROC_FEE_PAY_ID_TERM_ID
DROP INDEX ix_pay_proc_fee_pay_id_term_id
/

CREATE INDEX ix_pay_proc_fee_pay_id_term_id ON payment_process_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAY_PROC_FEE_PAY_ID_TERM_ID

-- Start of DDL Script for Index CORP.IX_PAY_PROCESS_FEE_PAYMENT_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAY_PROCESS_FEE_PAYMENT_ID
DROP INDEX ix_pay_process_fee_payment_id
/

CREATE INDEX ix_pay_process_fee_payment_id ON payment_process_fee
  (
    payment_id                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAY_PROCESS_FEE_PAYMENT_ID

-- Start of DDL Script for Index CORP.IX_PAY_SERV_FEE_COMBO
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAY_SERV_FEE_COMBO
DROP INDEX ix_pay_serv_fee_combo
/

CREATE INDEX ix_pay_serv_fee_combo ON payment_service_fee
  (
    payment_id                      ASC,
    status                          ASC,
    terminal_id                     ASC,
    fee_date                        ASC,
    fee_amount                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAY_SERV_FEE_COMBO

-- Start of DDL Script for Index CORP.IX_PAY_SERV_FEE_PAY_TERM_STAT
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAY_SERV_FEE_PAY_TERM_STAT
DROP INDEX ix_pay_serv_fee_pay_term_stat
/

CREATE INDEX ix_pay_serv_fee_pay_term_stat ON payment_service_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC,
    status                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAY_SERV_FEE_PAY_TERM_STAT

-- Start of DDL Script for Index CORP.IX_PAY_SERVICE_FEE_PAYMENT_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAY_SERVICE_FEE_PAYMENT_ID
DROP INDEX ix_pay_service_fee_payment_id
/

CREATE INDEX ix_pay_service_fee_payment_id ON payment_service_fee
  (
    payment_id                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAY_SERVICE_FEE_PAYMENT_ID

-- Start of DDL Script for Index CORP.IX_PAYMENT_ADJUST_EFT_TERMINAL
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAYMENT_ADJUST_EFT_TERMINAL
DROP INDEX ix_payment_adjust_eft_terminal
/

CREATE INDEX ix_payment_adjust_eft_terminal ON payment_adjustment
  (
    eft_id                          ASC,
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAYMENT_ADJUST_EFT_TERMINAL

-- Start of DDL Script for Index CORP.IX_PAYMENTS_EFT_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAYMENTS_EFT_ID
DROP INDEX ix_payments_eft_id
/

CREATE INDEX ix_payments_eft_id ON payments
  (
    eft_id                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAYMENTS_EFT_ID

-- Start of DDL Script for Index CORP.IX_PAYMENTS_TERMINAL_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PAYMENTS_TERMINAL_ID
DROP INDEX ix_payments_terminal_id
/

CREATE INDEX ix_payments_terminal_id ON payments
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_PAYMENTS_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_PF_END_DATE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PF_END_DATE
DROP INDEX ix_pf_end_date
/

CREATE INDEX ix_pf_end_date ON process_fees
  (
    end_date                        ASC
  )
/


-- End of DDL Script for Index CORP.IX_PF_END_DATE

-- Start of DDL Script for Index CORP.IX_PF_START_DATE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PF_START_DATE
DROP INDEX ix_pf_start_date
/

CREATE INDEX ix_pf_start_date ON process_fees
  (
    start_date                      ASC
  )
/


-- End of DDL Script for Index CORP.IX_PF_START_DATE

-- Start of DDL Script for Index CORP.IX_PF_TERMINAL_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PF_TERMINAL_ID
DROP INDEX ix_pf_terminal_id
/

CREATE INDEX ix_pf_terminal_id ON process_fees
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_PF_TERMINAL_ID

-- Start of DDL Script for Index CORP.IX_PF_TRANS_TYPE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_PF_TRANS_TYPE_ID
DROP INDEX ix_pf_trans_type_id
/

CREATE INDEX ix_pf_trans_type_id ON process_fees
  (
    trans_type_id                   ASC
  )
/


-- End of DDL Script for Index CORP.IX_PF_TRANS_TYPE_ID

-- Start of DDL Script for Index CORP.IX_SF_FEE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_SF_FEE_ID
DROP INDEX ix_sf_fee_id
/

CREATE INDEX ix_sf_fee_id ON service_fees
  (
    fee_id                          ASC
  )
/


-- End of DDL Script for Index CORP.IX_SF_FEE_ID

-- Start of DDL Script for Index CORP.IX_SF_FREQUENCY_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_SF_FREQUENCY_ID
DROP INDEX ix_sf_frequency_id
/

CREATE INDEX ix_sf_frequency_id ON service_fees
  (
    frequency_id                    ASC
  )
/


-- End of DDL Script for Index CORP.IX_SF_FREQUENCY_ID

-- Start of DDL Script for Index CORP.IX_SF_TERMINAL_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of IX_SF_TERMINAL_ID
DROP INDEX ix_sf_terminal_id
/

CREATE INDEX ix_sf_terminal_id ON service_fees
  (
    terminal_id                     ASC
  )
/


-- End of DDL Script for Index CORP.IX_SF_TERMINAL_ID

-- Start of DDL Script for Index CORP.LEDGER_BATCH_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_BATCH_ID
DROP INDEX ledger_batch_id
/

CREATE INDEX ledger_batch_id ON ledger
  (
    batch_id                        ASC
  )
/


-- End of DDL Script for Index CORP.LEDGER_BATCH_ID

-- Start of DDL Script for Index CORP.LEDGER_PROCESS_FEE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_PROCESS_FEE_ID
DROP INDEX ledger_process_fee_id
/

CREATE INDEX ledger_process_fee_id ON ledger
  (
    process_fee_id                  ASC
  )
/


-- End of DDL Script for Index CORP.LEDGER_PROCESS_FEE_ID

-- Start of DDL Script for Index CORP.LEDGER_SERVICE_FEE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_SERVICE_FEE_ID
DROP INDEX ledger_service_fee_id
/

CREATE INDEX ledger_service_fee_id ON ledger
  (
    service_fee_id                  ASC
  )
/


-- End of DDL Script for Index CORP.LEDGER_SERVICE_FEE_ID

-- Start of DDL Script for Index CORP.LEDGER_SETTLE_STATE_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_SETTLE_STATE_ID
DROP INDEX ledger_settle_state_id
/

CREATE INDEX ledger_settle_state_id ON ledger
  (
    settle_state_id                 ASC
  )
/


-- End of DDL Script for Index CORP.LEDGER_SETTLE_STATE_ID

-- Start of DDL Script for Index CORP.LEDGER_TRANS_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_TRANS_ID
DROP INDEX ledger_trans_id
/

CREATE INDEX ledger_trans_id ON ledger
  (
    trans_id                        ASC
  )
/


-- End of DDL Script for Index CORP.LEDGER_TRANS_ID

-- Start of DDL Script for Index CORP.PAYMENTS_PK21014403827934
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PAYMENTS_PK21014403827934
DROP INDEX payments_pk21014403827934
/

CREATE UNIQUE INDEX payments_pk21014403827934 ON payments
  (
    payment_id                      ASC
  )
/


-- End of DDL Script for Index CORP.PAYMENTS_PK21014403827934

-- Start of DDL Script for Index CORP.PK_CUSTOMER_BANK
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_CUSTOMER_BANK
DROP INDEX pk_customer_bank
/

CREATE UNIQUE INDEX pk_customer_bank ON customer_bank
  (
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.PK_CUSTOMER_BANK

-- Start of DDL Script for Index CORP.PK_CUSTOMER_BANK_TERMINAL
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_CUSTOMER_BANK_TERMINAL
DROP INDEX pk_customer_bank_terminal
/

CREATE UNIQUE INDEX pk_customer_bank_terminal ON customer_bank_terminal
  (
    customer_bank_terminal_id       ASC
  )
/


-- End of DDL Script for Index CORP.PK_CUSTOMER_BANK_TERMINAL

-- Start of DDL Script for Index CORP.PK_LEDGER
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_LEDGER
DROP INDEX pk_ledger
/

CREATE UNIQUE INDEX pk_ledger ON ledger_old
  (
    ledger_id                       ASC
  )
/


-- End of DDL Script for Index CORP.PK_LEDGER

-- Start of DDL Script for Index CORP.PK_MERCHANT
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_MERCHANT
DROP INDEX pk_merchant
/

CREATE UNIQUE INDEX pk_merchant ON merchant
  (
    merchant_id                     ASC
  )
/


-- End of DDL Script for Index CORP.PK_MERCHANT

-- Start of DDL Script for Index CORP.PK_PAYMENT_PROCESS_FEE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_PAYMENT_PROCESS_FEE
DROP INDEX pk_payment_process_fee
/

CREATE UNIQUE INDEX pk_payment_process_fee ON payment_process_fee
  (
    payment_id                      ASC,
    terminal_id                     ASC,
    trans_type_id                   ASC,
    fee_percent                     ASC,
    customer_bank_id                ASC
  )
/


-- End of DDL Script for Index CORP.PK_PAYMENT_PROCESS_FEE

-- Start of DDL Script for Index CORP.PK_PAYMENT_SERVICE_FEE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_PAYMENT_SERVICE_FEE
DROP INDEX pk_payment_service_fee
/

CREATE UNIQUE INDEX pk_payment_service_fee ON payment_service_fee
  (
    payment_service_fee_id          ASC
  )
/


-- End of DDL Script for Index CORP.PK_PAYMENT_SERVICE_FEE

-- Start of DDL Script for Index CORP.PK_PROCESS_FEES
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_PROCESS_FEES
DROP INDEX pk_process_fees
/

CREATE UNIQUE INDEX pk_process_fees ON process_fees
  (
    process_fee_id                  ASC
  )
/


-- End of DDL Script for Index CORP.PK_PROCESS_FEES

-- Start of DDL Script for Index CORP.PK_SERVICE_FEE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_SERVICE_FEE
DROP INDEX pk_service_fee
/

CREATE UNIQUE INDEX pk_service_fee ON service_fees
  (
    service_fee_id                  ASC
  )
/


-- End of DDL Script for Index CORP.PK_SERVICE_FEE

-- Start of DDL Script for Index CORP.PK_UTL_DATA
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of PK_UTL_DATA
DROP INDEX pk_utl_data
/

CREATE UNIQUE INDEX pk_utl_data ON utl_data
  (
    utl_data_id                     ASC
  )
/


-- End of DDL Script for Index CORP.PK_UTL_DATA

-- Start of DDL Script for Index CORP.SYS_C003813
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003813
DROP INDEX sys_c003813
/

CREATE UNIQUE INDEX sys_c003813 ON dealer
  (
    dealer_id                       ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003813

-- Start of DDL Script for Index CORP.SYS_C003815
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003815
DROP INDEX sys_c003815
/

CREATE UNIQUE INDEX sys_c003815 ON dealer_eport
  (
    dealer_id                       ASC,
    eport_id                        ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003815

-- Start of DDL Script for Index CORP.SYS_C003817
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003817
DROP INDEX sys_c003817
/

CREATE UNIQUE INDEX sys_c003817 ON eft
  (
    eft_id                          ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003817

-- Start of DDL Script for Index CORP.SYS_C003820
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003820
DROP INDEX sys_c003820
/

CREATE UNIQUE INDEX sys_c003820 ON fees
  (
    fee_id                          ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003820

-- Start of DDL Script for Index CORP.SYS_C003822
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003822
DROP INDEX sys_c003822
/

CREATE UNIQUE INDEX sys_c003822 ON frequency
  (
    frequency_id                    ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003822

-- Start of DDL Script for Index CORP.SYS_C003826
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003826
DROP INDEX sys_c003826
/

CREATE UNIQUE INDEX sys_c003826 ON license
  (
    license_id                      ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003826

-- Start of DDL Script for Index CORP.SYS_C003828
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003828
DROP INDEX sys_c003828
/

CREATE UNIQUE INDEX sys_c003828 ON license_nbr
  (
    license_nbr                     ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003828

-- Start of DDL Script for Index CORP.SYS_C003830
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003830
DROP INDEX sys_c003830
/

CREATE UNIQUE INDEX sys_c003830 ON license_process_fees
  (
    license_id                      ASC,
    trans_type_id                   ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003830

-- Start of DDL Script for Index CORP.SYS_C003832
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003832
DROP INDEX sys_c003832
/

CREATE UNIQUE INDEX sys_c003832 ON license_service_fees
  (
    license_id                      ASC,
    fee_id                          ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003832

-- Start of DDL Script for Index CORP.SYS_C003839
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C003839
DROP INDEX sys_c003839
/

CREATE UNIQUE INDEX sys_c003839 ON payment_adjustment
  (
    adjust_id                       ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C003839

-- Start of DDL Script for Index CORP.SYS_C007113
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C007113
DROP INDEX sys_c007113
/

CREATE UNIQUE INDEX sys_c007113 ON payment_schedule
  (
    payment_schedule_id             ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C007113

-- Start of DDL Script for Index CORP.SYS_C007188
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C007188
DROP INDEX sys_c007188
/

CREATE UNIQUE INDEX sys_c007188 ON doc
  (
    doc_id                          ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C007188

-- Start of DDL Script for Index CORP.SYS_C007197
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C007197
DROP INDEX sys_c007197
/

CREATE UNIQUE INDEX sys_c007197 ON batch
  (
    batch_id                        ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C007197

-- Start of DDL Script for Index CORP.SYS_C007208
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_C007208
DROP INDEX sys_c007208
/

CREATE UNIQUE INDEX sys_c007208 ON ledger
  (
    ledger_id                       ASC
  )
/


-- End of DDL Script for Index CORP.SYS_C007208

-- Start of DDL Script for Index CORP.SYS_UTL_NAME
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of SYS_UTL_NAME
DROP INDEX sys_utl_name
/

CREATE UNIQUE INDEX sys_utl_name ON utl_data
  (
    utlname                         ASC
  )
/


-- End of DDL Script for Index CORP.SYS_UTL_NAME

-- Start of DDL Script for Index CORP.UIX_BATCH_BATCH_DOC_IDS
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_BATCH_BATCH_DOC_IDS
DROP INDEX uix_batch_batch_doc_ids
/

CREATE UNIQUE INDEX uix_batch_batch_doc_ids ON batch
  (
    doc_id                          ASC,
    batch_id                        ASC
  )
COMPRESS  1
/


-- End of DDL Script for Index CORP.UIX_BATCH_BATCH_DOC_IDS

-- Start of DDL Script for Index CORP.UIX_BATCH_SUMMARY
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_BATCH_SUMMARY
DROP INDEX uix_batch_summary
/

CREATE UNIQUE INDEX uix_batch_summary ON batch
  (
    closed                          ASC,
    customer_bank_id                ASC,
    batch_id                        ASC
  )
COMPRESS  2
/


-- End of DDL Script for Index CORP.UIX_BATCH_SUMMARY

-- Start of DDL Script for Index CORP.UIX_CBT_COMBO
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_CBT_COMBO
DROP INDEX uix_cbt_combo
/

CREATE UNIQUE INDEX uix_cbt_combo ON customer_bank_terminal
  (
    customer_bank_id                ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC
  )
/


-- End of DDL Script for Index CORP.UIX_CBT_COMBO

-- Start of DDL Script for Index CORP.UIX_DOC_REF_NBR
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_DOC_REF_NBR
DROP INDEX uix_doc_ref_nbr
/

CREATE UNIQUE INDEX uix_doc_ref_nbr ON doc
  (
    ref_nbr                         ASC
  )
/


-- End of DDL Script for Index CORP.UIX_DOC_REF_NBR

-- Start of DDL Script for Index CORP.UIX_LEDGER_BATCH_COMBO
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_LEDGER_BATCH_COMBO
DROP INDEX uix_ledger_batch_combo
/

CREATE UNIQUE INDEX uix_ledger_batch_combo ON ledger
  (
    batch_id                        ASC,
    deleted                         ASC,
    entry_type                      ASC,
    settle_state_id                 ASC,
    amount                          ASC,
    ledger_id                       ASC
  )
COMPRESS  5
/


-- End of DDL Script for Index CORP.UIX_LEDGER_BATCH_COMBO

-- Start of DDL Script for Index CORP.UIX_LEDGER_FOR_UNPAID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_LEDGER_FOR_UNPAID
DROP INDEX uix_ledger_for_unpaid
/

CREATE UNIQUE INDEX uix_ledger_for_unpaid ON ledger_old
  (
    paid_status                     ASC,
    customer_bank_id                ASC,
    trans_type_id                   ASC,
    fee_percent                     ASC,
    fee_amount                      ASC,
    total_amount                    ASC,
    trans_id                        ASC
  )
COMPRESS  6
/


-- End of DDL Script for Index CORP.UIX_LEDGER_FOR_UNPAID

-- Start of DDL Script for Index CORP.UIX_LEDGER_FOR_UNPAID2
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_LEDGER_FOR_UNPAID2
DROP INDEX uix_ledger_for_unpaid2
/

CREATE UNIQUE INDEX uix_ledger_for_unpaid2 ON ledger_old
  (
    paid_status                     ASC,
    close_date                      ASC,
    customer_bank_id                ASC,
    trans_type_id                   ASC,
    terminal_id                     ASC,
    total_amount                    ASC,
    fee_percent                     ASC,
    fee_amount                      ASC,
    trans_id                        ASC
  )
COMPRESS  8
/


-- End of DDL Script for Index CORP.UIX_LEDGER_FOR_UNPAID2

-- Start of DDL Script for Index CORP.UIX_LEDGER_OLD_TEMP
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_LEDGER_OLD_TEMP
DROP INDEX uix_ledger_old_temp
/

CREATE UNIQUE INDEX uix_ledger_old_temp ON ledger_old
  (
    payment_id                      ASC,
    trans_type_id                   ASC,
    trans_id                        ASC,
    ledger_id                       ASC
  )
COMPRESS  3
/


-- End of DDL Script for Index CORP.UIX_LEDGER_OLD_TEMP

-- Start of DDL Script for Index CORP.UIX_LEDGER_TRAN_ID
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_LEDGER_TRAN_ID
DROP INDEX uix_ledger_tran_id
/

CREATE UNIQUE INDEX uix_ledger_tran_id ON ledger_old
  (
    trans_id                        ASC
  )
/


-- End of DDL Script for Index CORP.UIX_LEDGER_TRAN_ID

-- Start of DDL Script for Index CORP.UIX_MERCHANT_NBR
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_MERCHANT_NBR
DROP INDEX uix_merchant_nbr
/

CREATE UNIQUE INDEX uix_merchant_nbr ON merchant
  (
    merchant_nbr                    ASC
  )
/


-- End of DDL Script for Index CORP.UIX_MERCHANT_NBR

-- Start of DDL Script for Index CORP.UIX_PAYMENT_SERVICE_FEE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_PAYMENT_SERVICE_FEE
DROP INDEX uix_payment_service_fee
/

CREATE UNIQUE INDEX uix_payment_service_fee ON payment_service_fee
  (
    fee_id                          ASC,
    terminal_id                     ASC,
    fee_date                        ASC
  )
/


-- End of DDL Script for Index CORP.UIX_PAYMENT_SERVICE_FEE

-- Start of DDL Script for Index CORP.UIX_PROCESS_FEES_COMBO1
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_PROCESS_FEES_COMBO1
DROP INDEX uix_process_fees_combo1
/

CREATE UNIQUE INDEX uix_process_fees_combo1 ON process_fees
  (
    trans_type_id                   ASC,
    terminal_id                     ASC,
    start_date                      ASC,
    end_date                        ASC,
    process_fee_id                  ASC
  )
/


-- End of DDL Script for Index CORP.UIX_PROCESS_FEES_COMBO1

-- Start of DDL Script for Index CORP.UIX_PROCESS_FEES_COMBO2
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of UIX_PROCESS_FEES_COMBO2
DROP INDEX uix_process_fees_combo2
/

CREATE UNIQUE INDEX uix_process_fees_combo2 ON process_fees
  (
    process_fee_id                  ASC,
    fee_percent                     ASC,
    fee_amount                      ASC
  )
/


-- End of DDL Script for Index CORP.UIX_PROCESS_FEES_COMBO2

-- Start of DDL Script for Index CORP.XFKEFT
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XFKEFT
DROP INDEX xfkeft
/

CREATE UNIQUE INDEX xfkeft ON eft
  (
    batch_ref_nbr                   ASC
  )
/


-- End of DDL Script for Index CORP.XFKEFT

-- Start of DDL Script for Index CORP.XIF17CUSTOMER_ADDR
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF17CUSTOMER_ADDR
DROP INDEX xif17customer_addr
/

CREATE INDEX xif17customer_addr ON customer_addr
  (
    address_id                      ASC,
    customer_id                     ASC
  )
/


-- End of DDL Script for Index CORP.XIF17CUSTOMER_ADDR

-- Start of DDL Script for Index CORP.XIF30PURCHASE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF30PURCHASE
DROP INDEX xif30purchase
/

CREATE INDEX xif30purchase ON purchase
  (
    ledger_id                       ASC
  )
/


-- End of DDL Script for Index CORP.XIF30PURCHASE

-- Start of DDL Script for Index CORP.XIF50REFUND
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF50REFUND
DROP INDEX xif50refund
/

CREATE INDEX xif50refund ON refund
  (
    ledger_id                       ASC
  )
/


-- End of DDL Script for Index CORP.XIF50REFUND

-- Start of DDL Script for Index CORP.XIF51REFUND
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF51REFUND
DROP INDEX xif51refund
/

CREATE INDEX xif51refund ON refund
  (
    reason_code_id                  ASC
  )
/


-- End of DDL Script for Index CORP.XIF51REFUND

-- Start of DDL Script for Index CORP.XIF53CUSTOMER_ADDR
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF53CUSTOMER_ADDR
DROP INDEX xif53customer_addr
/

CREATE INDEX xif53customer_addr ON customer_addr
  (
    customer_id                     ASC
  )
/


-- End of DDL Script for Index CORP.XIF53CUSTOMER_ADDR

-- Start of DDL Script for Index CORP.XIF56CHARGEBACK
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF56CHARGEBACK
DROP INDEX xif56chargeback
/

CREATE INDEX xif56chargeback ON chargeback
  (
    ledger_id                       ASC
  )
/


-- End of DDL Script for Index CORP.XIF56CHARGEBACK

-- Start of DDL Script for Index CORP.XIF61CUSTOMER
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF61CUSTOMER
DROP INDEX xif61customer
/

CREATE INDEX xif61customer ON customer
  (
    user_id                         ASC
  )
/


-- End of DDL Script for Index CORP.XIF61CUSTOMER

-- Start of DDL Script for Index CORP.XIF62CHARGEBACK
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF62CHARGEBACK
DROP INDEX xif62chargeback
/

CREATE INDEX xif62chargeback ON chargeback
  (
    reason_code_id                  ASC
  )
/


-- End of DDL Script for Index CORP.XIF62CHARGEBACK

-- Start of DDL Script for Index CORP.XIF71CUSTOMER_ADDR
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XIF71CUSTOMER_ADDR
DROP INDEX xif71customer_addr
/

CREATE INDEX xif71customer_addr ON customer_addr
  (
    addr_type                       ASC
  )
/


-- End of DDL Script for Index CORP.XIF71CUSTOMER_ADDR

-- Start of DDL Script for Index CORP.XPKADDRESS_TYPE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKADDRESS_TYPE
DROP INDEX xpkaddress_type
/

CREATE UNIQUE INDEX xpkaddress_type ON address_type
  (
    addr_type_id                    ASC
  )
/


-- End of DDL Script for Index CORP.XPKADDRESS_TYPE

-- Start of DDL Script for Index CORP.XPKCATEGORIES
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKCATEGORIES
DROP INDEX xpkcategories
/

CREATE UNIQUE INDEX xpkcategories ON categories
  (
    category_id                     ASC
  )
/


-- End of DDL Script for Index CORP.XPKCATEGORIES

-- Start of DDL Script for Index CORP.XPKCHARGEBACK
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKCHARGEBACK
DROP INDEX xpkchargeback
/

CREATE UNIQUE INDEX xpkchargeback ON chargeback
  (
    chargeback_id                   ASC
  )
/


-- End of DDL Script for Index CORP.XPKCHARGEBACK

-- Start of DDL Script for Index CORP.XPKCONTACT_TYPE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKCONTACT_TYPE
DROP INDEX xpkcontact_type
/

CREATE UNIQUE INDEX xpkcontact_type ON contact
  (
    contact_type                    ASC
  )
/


-- End of DDL Script for Index CORP.XPKCONTACT_TYPE

-- Start of DDL Script for Index CORP.XPKCUSTOMER
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKCUSTOMER
DROP INDEX xpkcustomer
/

CREATE UNIQUE INDEX xpkcustomer ON customer
  (
    customer_id                     ASC
  )
/


-- End of DDL Script for Index CORP.XPKCUSTOMER

-- Start of DDL Script for Index CORP.XPKEPORT
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKEPORT
DROP INDEX xpkeport
/

CREATE UNIQUE INDEX xpkeport ON oper_eport
  (
    eport_id                        ASC
  )
/


-- End of DDL Script for Index CORP.XPKEPORT

-- Start of DDL Script for Index CORP.XPKEPORT_PART
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKEPORT_PART
DROP INDEX xpkeport_part
/

CREATE UNIQUE INDEX xpkeport_part ON eport_part
  (
    eport_part_no                   ASC
  )
/


-- End of DDL Script for Index CORP.XPKEPORT_PART

-- Start of DDL Script for Index CORP.XPKPURCHASE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKPURCHASE
DROP INDEX xpkpurchase
/

CREATE UNIQUE INDEX xpkpurchase ON purchase
  (
    purchase_id                     ASC
  )
/


-- End of DDL Script for Index CORP.XPKPURCHASE

-- Start of DDL Script for Index CORP.XPKREASON_CODE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKREASON_CODE
DROP INDEX xpkreason_code
/

CREATE UNIQUE INDEX xpkreason_code ON reason_code
  (
    reason_code_id                  ASC
  )
/


-- End of DDL Script for Index CORP.XPKREASON_CODE

-- Start of DDL Script for Index CORP.XPKREFUND
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKREFUND
DROP INDEX xpkrefund
/

CREATE UNIQUE INDEX xpkrefund ON refund
  (
    refund_id                       ASC
  )
/


-- End of DDL Script for Index CORP.XPKREFUND

-- Start of DDL Script for Index CORP.XPKSCHEDULE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKSCHEDULE
DROP INDEX xpkschedule
/

CREATE UNIQUE INDEX xpkschedule ON schedule
  (
    schedule_id                     ASC
  )
/


-- End of DDL Script for Index CORP.XPKSCHEDULE

-- Start of DDL Script for Index CORP.XPKSCHEDULE_TYPE
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of XPKSCHEDULE_TYPE
DROP INDEX xpkschedule_type
/

CREATE UNIQUE INDEX xpkschedule_type ON schedule_type
  (
    schedule_type_id                ASC
  )
/


-- End of DDL Script for Index CORP.XPKSCHEDULE_TYPE

-- Start of DDL Script for Procedure CORP.ACCEPT_CUSTOMER
-- Generated 10/13/2004 9:25:21 AM from CORP@USADBD02

-- Drop the old instance of ACCEPT_CUSTOMER
DROP PROCEDURE accept_customer
/

CREATE OR REPLACE 
PROCEDURE accept_customer
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   	l_user_id IN CUSTOMER.UPD_BY%TYPE)
IS
BEGIN
	 UPDATE CUSTOMER SET STATUS = 'A', UPD_DATE = SYSDATE, UPD_BY = l_user_id WHERE CUSTOMER_ID = l_cust_id;
END ACCEPT_CUSTOMER;
/



-- End of DDL Script for Procedure CORP.ACCEPT_CUSTOMER

-- Start of DDL Script for Procedure CORP.ACCEPT_CUSTOMER_BANK
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of ACCEPT_CUSTOMER_BANK
DROP PROCEDURE accept_customer_bank
/

CREATE OR REPLACE 
PROCEDURE accept_customer_bank
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
   	l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE)
IS
BEGIN
	 UPDATE CUSTOMER_BANK SET STATUS = 'A', UPD_DATE = SYSDATE, UPD_BY = l_user_id WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
END ACCEPT_CUSTOMER_BANK;
/



-- End of DDL Script for Procedure CORP.ACCEPT_CUSTOMER_BANK

-- Start of DDL Script for Procedure CORP.ADJUSTMENT_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of ADJUSTMENT_INS
DROP PROCEDURE adjustment_ins
/

CREATE OR REPLACE 
PROCEDURE adjustment_ins
   (l_adjust_id OUT PAYMENT_ADJUSTMENT.ADJUST_ID%TYPE,
   	l_eft_id IN PAYMENT_ADJUSTMENT.EFT_ID%TYPE,
	l_cust_bank_id IN PAYMENT_ADJUSTMENT.CUSTOMER_BANK_ID%TYPE,
    l_terminal_id IN PAYMENT_ADJUSTMENT.TERMINAL_ID%TYPE,
	l_reason IN PAYMENT_ADJUSTMENT.REASON%TYPE,
	l_amt IN PAYMENT_ADJUSTMENT.AMOUNT%TYPE,
	l_user_id IN PAYMENT_ADJUSTMENT.CREATE_BY%TYPE
)
IS
BEGIN
	 SELECT ADJUST_SEQ.NEXTVAL INTO l_adjust_id FROM DUAL;
	 INSERT INTO PAYMENT_ADJUSTMENT(ADJUST_ID, EFT_ID, TERMINAL_ID, REASON, CREATE_DATE, CREATE_BY, AMOUNT, CUSTOMER_BANK_ID)
	     VALUES (l_adjust_id, l_eft_id, l_terminal_id, l_reason, SYSDATE, l_user_id, l_amt, l_cust_bank_id);
END ADJUSTMENT_INS;
/



-- End of DDL Script for Procedure CORP.ADJUSTMENT_INS

-- Start of DDL Script for Procedure CORP.ADJUSTMENT_UPD
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of ADJUSTMENT_UPD
DROP PROCEDURE adjustment_upd
/

CREATE OR REPLACE 
PROCEDURE adjustment_upd
   (l_adjust_id IN PAYMENT_ADJUSTMENT.ADJUST_ID%TYPE,
   	l_reason IN PAYMENT_ADJUSTMENT.REASON%TYPE,
   	l_amt IN PAYMENT_ADJUSTMENT.AMOUNT%TYPE,
	l_user_id IN PAYMENT_ADJUSTMENT.CREATE_BY%TYPE
)
IS
BEGIN
	 UPDATE PAYMENT_ADJUSTMENT SET REASON = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id WHERE ADJUST_ID = l_adjust_id;
END ADJUSTMENT_UPD;
/



-- End of DDL Script for Procedure CORP.ADJUSTMENT_UPD

-- Start of DDL Script for Procedure CORP.APPROVE_PAYMENT
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of APPROVE_PAYMENT
DROP PROCEDURE approve_payment
/

CREATE OR REPLACE 
PROCEDURE approve_payment (
  l_eft_id IN EFT.EFT_ID%TYPE,
  l_user_id IN EFT.APPROVE_BY%TYPE
  )
IS
   l_adjust        PAYMENTS.ADJUST_AMOUNT%TYPE;
   l_net		   PAYMENTS.NET_AMOUNT%TYPE;
BEGIN
	 UPDATE PAYMENTS P SET ADJUST_AMOUNT = (SELECT SUM(AMOUNT) FROM PAYMENT_ADJUSTMENT PA
	    WHERE PA.TERMINAL_ID = P.TERMINAL_ID AND EFT_ID = l_eft_id) WHERE EFT_ID = l_eft_id;
	 UPDATE PAYMENTS P SET NET_AMOUNT = NET_AMOUNT + ADJUST_AMOUNT WHERE EFT_ID = l_eft_id AND NVL(ADJUST_AMOUNT, 0) <> 0;
	 SELECT SUM(AMOUNT) INTO l_adjust FROM PAYMENT_ADJUSTMENT WHERE EFT_ID = l_eft_id AND TERMINAL_ID = 0;
	 SELECT SUM(NET_AMOUNT) INTO l_net FROM PAYMENTS WHERE EFT_ID = l_eft_id;
	 UPDATE EFT SET APPROVE_BY = l_user_id, UPD_DT = SYSDATE, STATUS = 'A', EFT_AMOUNT = NVL(l_net, 0) + NVL(l_adjust, 0)
	 		WHERE EFT_ID = l_eft_id;
END APPROVE_PAYMENT;
/



-- End of DDL Script for Procedure CORP.APPROVE_PAYMENT

-- Start of DDL Script for Procedure CORP.CLEAN_UP_DEAD_DATES
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CLEAN_UP_DEAD_DATES
DROP PROCEDURE clean_up_dead_dates
/

CREATE OR REPLACE 
PROCEDURE clean_up_dead_dates
AS
BEGIN
DELETE FROM SERVICE_FEES WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM PROCESS_FEES WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM CUSTOMER_BANK_TERMINAL WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM DEALER_LICENSE WHERE START_DATE >= END_DATE;
COMMIT;
DELETE FROM TERMINAL_EPORT WHERE START_DATE >= END_DATE;
COMMIT;
END;
/



-- End of DDL Script for Procedure CORP.CLEAN_UP_DEAD_DATES

-- Start of DDL Script for Procedure CORP.CREATE_CUSTOMER
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CREATE_CUSTOMER
DROP PROCEDURE create_customer
/

CREATE OR REPLACE 
PROCEDURE create_customer
   (l_user_id OUT CUSTOMER.USER_ID%TYPE,
   l_cust_id OUT CUSTOMER.CUSTOMER_ID%TYPE,
   l_user_name IN VARCHAR,
   l_first IN VARCHAR,
   l_last IN VARCHAR,
   l_email IN VARCHAR,
   l_pwd IN VARCHAR,
   l_cust_name IN CUSTOMER.CUSTOMER_NAME%TYPE,
   l_addr1 IN CUSTOMER_ADDR.ADDRESS1%TYPE,
   l_city IN CUSTOMER_ADDR.CITY%TYPE,
   l_state IN CUSTOMER_ADDR.STATE%TYPE,
   l_zip IN CUSTOMER_ADDR.ZIP%TYPE,
   l_telephone IN VARCHAR,
   l_fax IN VARCHAR,
   l_dealer_id IN CUSTOMER.DEALER_ID%TYPE,
   l_tax_id_nbr IN CUSTOMER.TAX_ID_NBR%TYPE)
IS
   l_addr_id CUSTOMER_ADDR.ADDRESS_ID%TYPE;
   l_lic_nbr LICENSE_NBR.LICENSE_NBR%TYPE;
   l_lic_id LICENSE_NBR.LICENSE_ID%TYPE;
BEGIN
	 SELECT LICENSE_ID INTO l_lic_id FROM VW_DEALER_LICENSE WHERE DEALER_ID = l_dealer_id;
	 SELECT CUSTOMER_SEQ.NEXTVAL, CUSTOMER_ADDR_SEQ.NEXTVAL INTO l_cust_id, l_addr_id FROM DUAL;
	 CREATE_CUST_USER_ADMIN(l_user_id,l_user_name,l_first,l_last,l_email,l_cust_id,l_pwd,l_telephone,l_fax);
	 INSERT INTO CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, USER_ID, CREATE_BY, DEALER_ID, TAX_ID_NBR)
	     VALUES(l_cust_id, l_cust_name, l_user_id, l_user_id, l_dealer_id, l_tax_id_nbr);
	 INSERT INTO CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, CITY, STATE, ZIP)
	     VALUES (l_addr_id, l_cust_id, 2, l_first || ' ' || l_last, l_addr1, l_city, l_state, l_zip);
	 CREATE_LICENSE(l_lic_id, l_cust_id, l_lic_nbr);
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find license agreement for this dealer');
	 WHEN OTHERS THEN
	 	 RAISE;
END CREATE_CUSTOMER;
/

-- Grants for Procedure
GRANT EXECUTE ON create_customer TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.CREATE_CUSTOMER

-- Start of DDL Script for Procedure CORP.CREATE_CUSTOMER_BANK
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CREATE_CUSTOMER_BANK
DROP PROCEDURE create_customer_bank
/

CREATE OR REPLACE 
PROCEDURE create_customer_bank
   (l_cust_bank_id OUT CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_cust_id IN CUSTOMER_BANK.CUSTOMER_ID%TYPE,
	l_user_id IN CUSTOMER.USER_ID%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_address1 IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_city IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_state IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_zip IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_telephone IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_contact_fax IN CUSTOMER_BANK.BANK_NAME%TYPE)
IS
BEGIN
	 SELECT CUSTOMER_BANK_SEQ.NEXTVAL INTO l_cust_bank_id FROM DUAL;
	 INSERT INTO CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, CREATE_BY,
	 	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX)
	     VALUES (l_cust_bank_id, l_cust_id, l_bank_name,l_bank_address1,l_bank_city,l_bank_state,l_bank_zip,l_user_id,
		 l_title,l_account_type,l_aba,l_account_number,l_contact_name,l_contact_title,l_contact_telephone,l_contact_fax);
	 INSERT INTO USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
	 	 VALUES (l_cust_bank_id, l_user_id, 'Y');
END CREATE_CUSTOMER_BANK;
/

-- Grants for Procedure
GRANT EXECUTE ON create_customer_bank TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.CREATE_CUSTOMER_BANK

-- Start of DDL Script for Procedure CORP.CREATE_LICENSE
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CREATE_LICENSE
DROP PROCEDURE create_license
/

CREATE OR REPLACE 
PROCEDURE create_license
   (l_license_id IN LICENSE_NBR.LICENSE_ID%TYPE,
   l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_license_nbr OUT LICENSE_NBR.LICENSE_NBR%TYPE
)
IS
BEGIN
	 SELECT 'L' || TO_CHAR(l_license_id,'FM000') || TO_CHAR(LICENSE_SEQ.NEXTVAL,'FM99990000') INTO l_license_nbr FROM DUAL;
	 INSERT INTO LICENSE_NBR(LICENSE_NBR, LICENSE_ID, CUSTOMER_ID)
	     VALUES (l_license_nbr, l_license_id, l_cust_id);
END CREATE_LICENSE;
/



-- End of DDL Script for Procedure CORP.CREATE_LICENSE

-- Start of DDL Script for Procedure CORP.CUSTOMER_BANK_DEL
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_DEL
DROP PROCEDURE customer_bank_del
/

CREATE OR REPLACE 
PROCEDURE customer_bank_del
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE)
IS
BEGIN
     UPDATE CUSTOMER_BANK SET STATUS = 'D', UPD_BY = l_user_id WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
END CUSTOMER_BANK_DEL;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_BANK_DEL

-- Start of DDL Script for Procedure CORP.CUSTOMER_BANK_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_INS
DROP PROCEDURE customer_bank_ins
/

CREATE OR REPLACE 
PROCEDURE customer_bank_ins
   (l_cust_bank_id OUT CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_cust_id IN CUSTOMER_BANK.CUSTOMER_ID%TYPE,
	l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_desc IN CUSTOMER_BANK.DESCRIPTION%TYPE,
	l_prefix IN CUSTOMER_BANK.EFT_PREFIX%TYPE,
	l_min_amt IN CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE,
	l_is_eft IN CUSTOMER_BANK.IS_EFT%TYPE,
	l_user_id IN CUSTOMER_BANK.CREATE_BY%TYPE
)
IS
BEGIN
	 SELECT CUSTOMER_BANK_SEQ.NEXTVAL INTO l_cust_bank_id FROM DUAL;
	 INSERT INTO CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, CREATE_BY, DESCRIPTION, EFT_PREFIX, PAY_MIN_AMOUNT,	 	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, IS_EFT)
	     VALUES (l_cust_bank_id, l_cust_id, l_bank_name,l_user_id, l_desc, l_prefix, l_min_amt,
		 l_title,l_account_type,l_aba,l_account_number, l_is_eft);
END CUSTOMER_BANK_INS;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_BANK_INS

-- Start of DDL Script for Procedure CORP.CUSTOMER_BANK_UPD
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_UPD
DROP PROCEDURE customer_bank_upd
/

CREATE OR REPLACE 
PROCEDURE customer_bank_upd
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_account_number IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_aba IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_title IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_bank_name IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_account_type IN CUSTOMER_BANK.BANK_NAME%TYPE,
	l_desc IN CUSTOMER_BANK.DESCRIPTION%TYPE,
	l_prefix IN CUSTOMER_BANK.EFT_PREFIX%TYPE,
	l_min_amt IN CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE,
	l_is_eft IN CUSTOMER_BANK.IS_EFT%TYPE,
	l_user_id IN CUSTOMER_BANK.UPD_BY%TYPE
)
IS
BEGIN
	 UPDATE CUSTOMER_BANK SET (BANK_NAME, CREATE_BY, DESCRIPTION, EFT_PREFIX, PAY_MIN_AMOUNT,
	  	 ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, IS_EFT)
	     = (SELECT l_bank_name,l_user_id, l_desc, l_prefix, l_min_amt,
		 l_title,l_account_type,l_aba,l_account_number, l_is_eft FROM DUAL) WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
END CUSTOMER_BANK_UPD;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_BANK_UPD

-- Start of DDL Script for Procedure CORP.CUSTOMER_DEL
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_DEL
DROP PROCEDURE customer_del
/

CREATE OR REPLACE 
PROCEDURE customer_del
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE)
IS
    l_cnt NUMBER;
BEGIN
     -- select to validate if any terminal are active
	 DECLARE
  	    NO_TRANS EXCEPTION;
	    PRAGMA EXCEPTION_INIT(NO_TRANS, -2041);
     BEGIN
     	SELECT COUNT(*) INTO l_cnt FROM TERMINAL WHERE CUSTOMER_ID = l_cust_id AND STATUS <> 'D';
	 EXCEPTION
	    WHEN NO_TRANS THEN  -- THIS IS FOR VB WHICH CAN'T DO DISTRIBUTED QUERIES OR UPDATES
			 l_cnt := 0;
		WHEN OTHERS THEN
			 RAISE;
	 END;
     IF l_cnt = 0  THEN
     	UPDATE CUSTOMER SET STATUS = 'D' WHERE  CUSTOMER_ID = l_cust_id;
      	UPDATE CUSTOMER_ADDR SET STATUS ='D' WHERE  CUSTOMER_ID = l_cust_id;
     ELSE
	 	RAISE_APPLICATION_ERROR(-20100, 'Customer has active terminals');
	 END IF;
END CUSTOMER_DEL;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_DEL

-- Start of DDL Script for Procedure CORP.CUSTOMER_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_INS
DROP PROCEDURE customer_ins
/

CREATE OR REPLACE 
PROCEDURE customer_ins
   (custname IN CUSTOMER.CUSTOMER_NAME%TYPE,
    custaltname IN CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
    userid IN CUSTOMER.USER_ID%TYPE,
    salesterm IN CUSTOMER.SALES_TERM%TYPE,
    creditterm IN CUSTOMER.CREDIT_TERM%TYPE,
    updby IN CUSTOMER.UPD_BY%TYPE,
    Varout OUT NUMBER )
IS
    BEGIN
             -- Insert the details into the customer
            INSERT into CUSTOMER (customer_id,customer_name,user_id,sales_term,credit_term,
                                  create_by,customer_alt_name) VALUES
                                  (CUSTOMER_SEQ.NEXTVAL,custname,userid,salesterm,creditterm,
                                   updby,custaltname);
             Varout:= 0;
        EXCEPTION
          When OTHERS THEN
              Varout := SQLCODE;
END customer_ins;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_INS

-- Start of DDL Script for Procedure CORP.CUSTOMER_UPD
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_UPD
DROP PROCEDURE customer_upd
/

CREATE OR REPLACE 
PROCEDURE customer_upd
   (l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_cust_name IN CUSTOMER.CUSTOMER_NAME%TYPE,
   l_cust_alt_name IN CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
   l_addr_name IN CUSTOMER_ADDR.NAME%TYPE,
   l_addr1 IN CUSTOMER_ADDR.ADDRESS1%TYPE,
   l_addr2 IN CUSTOMER_ADDR.ADDRESS2%TYPE,
   l_city IN CUSTOMER_ADDR.CITY%TYPE,
   l_state IN CUSTOMER_ADDR.STATE%TYPE,
   l_zip IN CUSTOMER_ADDR.ZIP%TYPE,
   l_user_id IN USER_LOGIN.USER_ID%TYPE)
IS
   l_addr_id CUSTOMER_ADDR.ADDRESS_ID%TYPE;
BEGIN
	 SELECT ADDRESS_ID INTO l_addr_id FROM VW_CUSTOMER WHERE CUSTOMER_ID = l_cust_id;
	 UPDATE CUSTOMER SET CUSTOMER_NAME = l_cust_name, CUSTOMER_ALT_NAME = l_cust_alt_name, UPD_BY= l_user_id, UPD_DATE = SYSDATE
		 WHERE CUSTOMER_ID = l_cust_id;
	 IF l_addr_id IS NULL THEN
	 	SELECT CUSTOMER_ADDR_SEQ.NEXTVAL INTO l_addr_id FROM DUAL;
	 	INSERT INTO CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP)
	     	VALUES (l_addr_id, l_cust_id, 2, l_addr_name, l_addr1, l_addr2, l_city, l_state, l_zip);
	 ELSE
	 	 UPDATE CUSTOMER_ADDR SET NAME = l_addr_name, ADDRESS1 = l_addr1, ADDRESS2 = l_addr2, CITY =  l_city, STATE = l_state, ZIP = l_zip
		 		WHERE ADDRESS_ID = l_addr_id;
	 END IF;
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20100, 'Customer not found');
	 WHEN OTHERS THEN
	 	 RAISE;
END CUSTOMER_UPD;
/



-- End of DDL Script for Procedure CORP.CUSTOMER_UPD

-- Start of DDL Script for Procedure CORP.DEALER_DEL
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_DEL
DROP PROCEDURE dealer_del
/

CREATE OR REPLACE 
PROCEDURE dealer_del
   (l_dealer_id IN DEALER.DEALER_ID%TYPE
)
IS
BEGIN
	 UPDATE DEALER SET STATUS = 'D' WHERE DEALER_ID = l_dealer_id;
END DEALER_DEL;
/



-- End of DDL Script for Procedure CORP.DEALER_DEL

-- Start of DDL Script for Procedure CORP.DEALER_EPORT_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_EPORT_INS
DROP PROCEDURE dealer_eport_ins
/

CREATE OR REPLACE 
PROCEDURE dealer_eport_ins
   (l_dealer_id IN DEALER_EPORT.DEALER_ID%TYPE,
    l_eport_id IN DEALER_EPORT.EPORT_ID%TYPE
)
IS
BEGIN
	 INSERT INTO DEALER_EPORT(DEALER_ID, EPORT_ID)
	     VALUES(l_dealer_id, l_eport_id);
END DEALER_EPORT_INS;
/



-- End of DDL Script for Procedure CORP.DEALER_EPORT_INS

-- Start of DDL Script for Procedure CORP.DEALER_EPORT_UPD
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_EPORT_UPD
DROP PROCEDURE dealer_eport_upd
/

CREATE OR REPLACE 
PROCEDURE dealer_eport_upd
   (l_dealer_id IN DEALER_EPORT.DEALER_ID%TYPE,
    l_eport_id IN DEALER_EPORT.EPORT_ID%TYPE
)
IS
BEGIN
	 DELETE FROM DEALER_EPORT WHERE EPORT_ID = l_eport_id;
	 INSERT INTO DEALER_EPORT(DEALER_ID, EPORT_ID)
	     VALUES(l_dealer_id, l_eport_id);
END DEALER_EPORT_UPD;
/



-- End of DDL Script for Procedure CORP.DEALER_EPORT_UPD

-- Start of DDL Script for Procedure CORP.DEALER_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_INS
DROP PROCEDURE dealer_ins
/

CREATE OR REPLACE 
PROCEDURE dealer_ins
   (l_dealer_id OUT DEALER.DEALER_ID%TYPE,
    l_name IN DEALER.DEALER_NAME%TYPE
)
IS
BEGIN
	 SELECT DEALER_SEQ.NEXTVAL INTO l_dealer_id FROM DUAL;
	 INSERT INTO DEALER(DEALER_ID, DEALER_NAME)
	     VALUES (l_dealer_id, l_name);
END DEALER_INS;
/



-- End of DDL Script for Procedure CORP.DEALER_INS

-- Start of DDL Script for Procedure CORP.DEALER_LICENSE_DEL
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_LICENSE_DEL
DROP PROCEDURE dealer_license_del
/

CREATE OR REPLACE 
PROCEDURE dealer_license_del
   (l_dealer_id IN DEALER_LICENSE.DEALER_ID%TYPE,
   l_license_id IN DEALER_LICENSE.LICENSE_ID%TYPE
)
IS
BEGIN
	 DELETE FROM DEALER_LICENSE WHERE DEALER_ID = l_dealer_id AND LICENSE_ID = l_license_id;
END DEALER_LICENSE_DEL;
/



-- End of DDL Script for Procedure CORP.DEALER_LICENSE_DEL

-- Start of DDL Script for Procedure CORP.DEALER_LICENSE_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_LICENSE_INS
DROP PROCEDURE dealer_license_ins
/

CREATE OR REPLACE 
PROCEDURE dealer_license_ins
   (l_dealer_id IN DEALER_LICENSE.DEALER_ID%TYPE,
   l_license_id IN DEALER_LICENSE.LICENSE_ID%TYPE
)
IS
BEGIN
	 INSERT INTO DEALER_LICENSE(DEALER_ID, LICENSE_ID) VALUES (l_dealer_id, l_license_id);
END DEALER_LICENSE_INS;
/



-- End of DDL Script for Procedure CORP.DEALER_LICENSE_INS

-- Start of DDL Script for Procedure CORP.DEALER_UPD
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DEALER_UPD
DROP PROCEDURE dealer_upd
/

CREATE OR REPLACE 
PROCEDURE dealer_upd
   (l_dealer_id IN DEALER.DEALER_ID%TYPE,
    l_name IN DEALER.DEALER_NAME%TYPE
)
IS
BEGIN
	 UPDATE DEALER SET DEALER_NAME = l_name WHERE DEALER_ID = l_dealer_id;
END DEALER_UPD;
/



-- End of DDL Script for Procedure CORP.DEALER_UPD

-- Start of DDL Script for Procedure CORP.DELAY_REFUND
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DELAY_REFUND
DROP PROCEDURE delay_refund
/

CREATE OR REPLACE 
PROCEDURE delay_refund (
  l_id IN LEDGER.LEDGER_ID%TYPE
  )
IS
  l_amt LEDGER.TOTAL_AMOUNT%TYPE;
  l_payment_id LEDGER.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT TOTAL_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM LEDGER WHERE LEDGER_ID = l_id AND TRANS_TYPE_ID = 20;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE LEDGER SET PAYMENT_ID = 0 WHERE LEDGER_ID = l_id;
     UPDATE PAYMENTS P SET REFUND_AMOUNT = REFUND_AMOUNT - l_amt, NET_AMOUNT = NET_AMOUNT - l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Refund not found');
	WHEN OTHERS THEN
		 RAISE;
END DELAY_REFUND;
/



-- End of DDL Script for Procedure CORP.DELAY_REFUND

-- Start of DDL Script for Procedure CORP.DELAY_REFUND_BY_TRAN_ID
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DELAY_REFUND_BY_TRAN_ID
DROP PROCEDURE delay_refund_by_tran_id
/

CREATE OR REPLACE 
PROCEDURE delay_refund_by_tran_id (
  l_id IN LEDGER.TRANS_ID%TYPE
  )
IS
  l_amt LEDGER.TOTAL_AMOUNT%TYPE;
  l_payment_id LEDGER.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT TOTAL_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM LEDGER WHERE TRANS_ID = l_id AND TRANS_TYPE_ID = 20;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE LEDGER SET PAYMENT_ID = 0 WHERE TRANS_ID = l_id;
     UPDATE PAYMENTS P SET REFUND_AMOUNT = REFUND_AMOUNT - l_amt, NET_AMOUNT = NET_AMOUNT - l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Refund not found');
	WHEN OTHERS THEN
		 RAISE;
END DELAY_REFUND_BY_TRAN_ID;
/



-- End of DDL Script for Procedure CORP.DELAY_REFUND_BY_TRAN_ID

-- Start of DDL Script for Procedure CORP.DELAY_SERVICE_FEE
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DELAY_SERVICE_FEE
DROP PROCEDURE delay_service_fee
/

CREATE OR REPLACE 
PROCEDURE delay_service_fee (
  l_id IN PAYMENT_SERVICE_FEE.PAYMENT_SERVICE_FEE_ID%TYPE
  )
IS
  l_amt PAYMENT_SERVICE_FEE.FEE_AMOUNT%TYPE;
  l_payment_id PAYMENT_SERVICE_FEE.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT FEE_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_SERVICE_FEE_ID = l_id;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = 0 WHERE PAYMENT_SERVICE_FEE_ID = l_id;
     UPDATE PAYMENTS P SET SERVICE_FEE_AMOUNT = SERVICE_FEE_AMOUNT + l_amt, NET_AMOUNT = NET_AMOUNT + l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Service Fee not found');
	WHEN OTHERS THEN
		 RAISE;
END DELAY_SERVICE_FEE;
/



-- End of DDL Script for Procedure CORP.DELAY_SERVICE_FEE

-- Start of DDL Script for Procedure CORP.DELETE_SERVICE_FEE
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of DELETE_SERVICE_FEE
DROP PROCEDURE delete_service_fee
/

CREATE OR REPLACE 
PROCEDURE delete_service_fee (
  l_id IN PAYMENT_SERVICE_FEE.PAYMENT_SERVICE_FEE_ID%TYPE
  )
IS
  l_amt PAYMENT_SERVICE_FEE.FEE_AMOUNT%TYPE;
  l_payment_id PAYMENT_SERVICE_FEE.PAYMENT_ID%TYPE;
  l_cnt NUMBER;
BEGIN
	 SELECT FEE_AMOUNT, PAYMENT_ID INTO l_amt, l_payment_id
	 		FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_SERVICE_FEE_ID = l_id;
	 SELECT COUNT(*) INTO l_cnt FROM PAYMENTS P, EFT E WHERE PAYMENT_ID = l_payment_id AND E.EFT_ID = P.EFT_ID
	 		AND E.STATUS NOT IN('L');
	 IF l_cnt > 0 THEN
	 	 RAISE_APPLICATION_ERROR(-20142, 'This EFT is already approved.');
	 END IF;
	 UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = 0, STATUS = 'D' WHERE PAYMENT_SERVICE_FEE_ID = l_id;
     UPDATE PAYMENTS P SET SERVICE_FEE_AMOUNT = SERVICE_FEE_AMOUNT + l_amt, NET_AMOUNT = NET_AMOUNT - l_amt
	 		WHERE PAYMENT_ID = l_payment_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
		 RAISE_APPLICATION_ERROR(-20100, 'Service Fee not found');
	WHEN OTHERS THEN
		 RAISE;
END DELETE_SERVICE_FEE;
/



-- End of DDL Script for Procedure CORP.DELETE_SERVICE_FEE

-- Start of DDL Script for Procedure CORP.LICENSE_DEL
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_DEL
DROP PROCEDURE license_del
/

CREATE OR REPLACE 
PROCEDURE license_del
   (l_license_id IN LICENSE.LICENSE_ID%TYPE
)
IS
BEGIN
	 UPDATE LICENSE SET STATUS = 'D' WHERE LICENSE_ID = l_license_id;
END LICENSE_DEL;
/



-- End of DDL Script for Procedure CORP.LICENSE_DEL

-- Start of DDL Script for Procedure CORP.LICENSE_INS
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_INS
DROP PROCEDURE license_ins
/

CREATE OR REPLACE 
PROCEDURE license_ins
   (l_license_id OUT LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE,
	l_credit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE, --as decimal (i.e. - for 5% use 0.05)
	l_debit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_pass_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_access_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_maint_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_cash_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_refund_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_chargeback_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_service_fee IN LICENSE_SERVICE_FEES.AMOUNT%TYPE
)
IS
BEGIN
	 SELECT LICENSE_SEQ.NEXTVAL INTO l_license_id FROM DUAL;
	 INSERT INTO LICENSE(LICENSE_ID, TITLE, DESCRIPTION)
	     VALUES(l_license_id, l_title, l_desc);
	 IF l_credit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 16, l_credit_fee);
	 END IF;
	 IF l_debit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 19, l_debit_fee);
	 END IF;
	 IF l_pass_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 18, l_pass_fee);
	 END IF;
	 IF l_access_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 17, l_access_fee);
	 END IF;
	 IF l_maint_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 23, l_maint_fee);
	 END IF;
	 IF l_cash_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 22, l_cash_fee);
	 END IF;
	 IF l_refund_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 20, l_refund_fee);
	 END IF;
	 IF l_chargeback_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 21, l_chargeback_fee);
	 END IF;
 	 INSERT INTO LICENSE_SERVICE_FEES(LICENSE_ID, FEE_ID, AMOUNT, FREQUENCY_ID)
	     VALUES(l_license_id, 1, l_service_fee, 2);
END LICENSE_INS;
/



-- End of DDL Script for Procedure CORP.LICENSE_INS

-- Start of DDL Script for Procedure CORP.LICENSE_UPD
-- Generated 10/13/2004 9:25:22 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_UPD
DROP PROCEDURE license_upd
/

CREATE OR REPLACE 
PROCEDURE license_upd
   (l_license_id IN LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE,
	l_credit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE, --as decimal (i.e. - for 5% use 0.05)
	l_debit_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_pass_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_access_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_maint_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_cash_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_refund_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_chargeback_fee IN LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE,
	l_service_fee IN LICENSE_SERVICE_FEES.AMOUNT%TYPE
)
IS
BEGIN
	 UPDATE LICENSE SET (TITLE, DESCRIPTION) =
	     (SELECT l_title, l_desc FROM DUAL) WHERE LICENSE_ID = l_license_id;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 16;
	 IF l_credit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 16, l_credit_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 19;
	 IF l_debit_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 19, l_debit_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 18;
	 IF l_pass_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 18, l_pass_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 17;
	 IF l_access_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 17, l_access_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 23;
	 IF l_maint_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 23, l_maint_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 22;
	 IF l_cash_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 22, l_cash_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 20;
	 IF l_refund_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 20, l_refund_fee);
	 END IF;
	 DELETE FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id AND TRANS_TYPE_ID = 21;
	 IF l_chargeback_fee <> 0 THEN
	 	INSERT INTO LICENSE_PROCESS_FEES(LICENSE_ID, TRANS_TYPE_ID, FEE_PERCENT)
		    VALUES(l_license_id, 21, l_chargeback_fee);
	 END IF;
	 DELETE FROM LICENSE_SERVICE_FEES WHERE LICENSE_ID = l_license_id AND FEE_ID = 1;
	 INSERT INTO LICENSE_SERVICE_FEES(LICENSE_ID, FEE_ID, AMOUNT, FREQUENCY_ID)
	     VALUES(l_license_id, 1, l_service_fee, 2);
END LICENSE_UPD;
/



-- End of DDL Script for Procedure CORP.LICENSE_UPD

-- Start of DDL Script for Procedure CORP.LOAD_LICENSE_AGREEMENTS
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of LOAD_LICENSE_AGREEMENTS
DROP PROCEDURE load_license_agreements
/

CREATE OR REPLACE 
PROCEDURE load_license_agreements IS
    CURSOR C IS
        SELECT CUSTOMER_ID, 1 - CUSTOMER_CUT, FEES, CUSTOMER FROM XLICENSE_FEE_DATA X WHERE LICENSE_AGREEMENT = 'Y';
    l_customer_id CUSTOMER.CUSTOMER_ID%TYPE;
    l_license_id LICENSE.LICENSE_ID%TYPE;
    l_customer XLICENSE_FEE_DATA.CUSTOMER%TYPE;
    l_credit_fee LICENSE_PROCESS_FEES.FEE_PERCENT%TYPE;
    l_service_fee LICENSE_SERVICE_FEES.AMOUNT%TYPE;
BEGIN
   OPEN C;
   LOOP
	  FETCH C INTO l_customer_id, l_credit_fee, l_service_fee, l_customer;
      EXIT WHEN C%NOTFOUND;
	  LICENSE_INS(l_license_id, SUBSTR('License for ' || l_customer,1,50),'', l_credit_fee, 0, 0, 0, 0, 0, 0, 0, l_service_fee);
	  RECEIVED_CUSTOM_LICENSE(l_license_id, l_customer_id, 7);
	  COMMIT;
   END LOOP;
END;
/



-- End of DDL Script for Procedure CORP.LOAD_LICENSE_AGREEMENTS

-- Start of DDL Script for Procedure CORP.MARK_EFT_PAID
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of MARK_EFT_PAID
DROP PROCEDURE mark_eft_paid
/

CREATE OR REPLACE 
PROCEDURE mark_eft_paid (
  l_eft_id IN EFT.EFT_ID%TYPE,
  l_user_id IN EFT.APPROVE_BY%TYPE
  )
IS
BEGIN
	 UPDATE EFT SET SENT_BY = l_user_id, UPD_DT = SYSDATE, STATUS = 'P', EFT_DATE = SYSDATE
	 		WHERE EFT_ID = l_eft_id;
END MARK_EFT_PAID;
/



-- End of DDL Script for Procedure CORP.MARK_EFT_PAID

-- Start of DDL Script for Procedure CORP.PROCESS_FEES_UPD
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of PROCESS_FEES_UPD
DROP PROCEDURE process_fees_upd
/

CREATE OR REPLACE 
PROCEDURE process_fees_upd
   (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
    l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
    l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
    l_effective_dt IN PROCESS_FEES.END_DATE%TYPE )
IS
   l_effective_date DATE;
BEGIN
	 IF l_effective_dt IS NULL THEN
	     l_effective_date := SYSDATE;
	 ELSE
	 	 l_effective_date := l_effective_dt;
	 END IF;
	 --CHECK IF WE NEED already paid on transactions before after the effective date
	 IF l_effective_date < SYSDATE THEN
	 	 DECLARE
		     l_tran_id LEDGER.TRANS_ID%TYPE;
			BEGIN
		 	 SELECT MIN(TRANS_ID) INTO l_tran_id FROM LEDGER WHERE TERMINAL_ID = l_terminal_id
			     AND CLOSE_DATE > l_effective_date AND PAYMENT_ID <> 0;
			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
			 END IF;
		 END;
	 END IF;
	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, START_DATE)
 	    VALUES(PROCESS_FEE_SEQ.NEXTVAL,l_terminal_id,l_trans_type_id,l_fee_percent,l_effective_date);
END PROCESS_FEES_UPD;
/



-- End of DDL Script for Procedure CORP.PROCESS_FEES_UPD

-- Start of DDL Script for Procedure CORP.RECEIVED_CUSTOM_LICENSE
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of RECEIVED_CUSTOM_LICENSE
DROP PROCEDURE received_custom_license
/

CREATE OR REPLACE 
PROCEDURE received_custom_license
   (l_license_id IN LICENSE_NBR.LICENSE_ID%TYPE,
   l_cust_id IN CUSTOMER.CUSTOMER_ID%TYPE,
   l_user_id IN CUSTOMER_LICENSE.RECEIVE_BY%TYPE)
IS
   l_lic_nbr LICENSE_NBR.LICENSE_NBR%TYPE;
BEGIN
	 CREATE_LICENSE(l_license_id, l_cust_id, l_lic_nbr);
	 RECEIVED_LICENSE(l_cust_id, l_lic_nbr, l_user_id);
END RECEIVED_CUSTOM_LICENSE;
/



-- End of DDL Script for Procedure CORP.RECEIVED_CUSTOM_LICENSE

-- Start of DDL Script for Procedure CORP.RECEIVED_LICENSE
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of RECEIVED_LICENSE
DROP PROCEDURE received_license
/

CREATE OR REPLACE 
PROCEDURE received_license
   (l_cust_id IN CUSTOMER_LICENSE.CUSTOMER_ID%TYPE,
   	l_license_nbr IN CUSTOMER_LICENSE.LICENSE_NBR%TYPE,
   	l_user_id IN CUSTOMER_LICENSE.RECEIVE_BY%TYPE)
IS
  	l_match_cust_id CUSTOMER_LICENSE.CUSTOMER_ID%TYPE;
  	l_match_license_id LICENSE_NBR.LICENSE_ID%TYPE;
	l_cnt NUMBER;
BEGIN
	 SELECT CUSTOMER_ID, LICENSE_ID INTO l_match_cust_id, l_match_license_id FROM LICENSE_NBR WHERE LICENSE_NBR = l_license_nbr;
	 IF l_match_cust_id <> l_cust_id THEN -- CUSTOMER USED A DIFFERENT LICENSE NBR, MAKE SURE LICENSE ID IS THE SAME
	 	SELECT COUNT(*) INTO l_cnt FROM LICENSE_NBR WHERE CUSTOMER_ID = l_cust_id AND LICENSE_ID = l_match_license_id;
	 	IF l_cnt = 0 THEN --INVALID LICENSE ID
		   RAISE_APPLICATION_ERROR(-20112, 'License Nbr matches the wrong contract for this customer');
		END IF;
	 END IF;
	 INSERT INTO CUSTOMER_LICENSE(LICENSE_NBR, CUSTOMER_ID, RECEIVE_BY) VALUES (l_license_nbr, l_cust_id, l_user_id);
EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	 	 RAISE_APPLICATION_ERROR(-20110, 'Invalid License Nbr specified');
	 WHEN OTHERS THEN
	 	 RAISE;
END RECEIVED_LICENSE;
/



-- End of DDL Script for Procedure CORP.RECEIVED_LICENSE

-- Start of DDL Script for Procedure CORP.SCAN_FOR_SERVICE_FEES
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of SCAN_FOR_SERVICE_FEES
DROP PROCEDURE scan_for_service_fees
/

CREATE OR REPLACE 
PROCEDURE scan_for_service_fees
IS
    CURSOR c_fee
    IS
        SELECT service_fee_id,
               terminal_id,
               fee_id,
               fee_amount,
               months,
               days,
               last_payment,
               start_date,
               end_date
          FROM service_fees sf, frequency q
         WHERE sf.frequency_id = q.frequency_id
           AND (   days <> 0
                OR months <> 0)
           AND ADD_MONTHS (last_payment + days, months) <
                                      LEAST (NVL (end_date, SYSDATE), SYSDATE);
                                      
    cb_DEBUG_MODE                      CONSTANT BOOLEAN := FALSE;

    l_service_fee_id                   service_fees.service_fee_id%TYPE;
    l_terminal_id                      service_fees.terminal_id%TYPE;
    l_fee_id                           service_fees.fee_id%TYPE;
    l_fee_amount                       service_fees.fee_amount%TYPE;
    l_months                           frequency.months%TYPE;
    l_days                             frequency.days%TYPE;
    l_last_payment                     service_fees.last_payment%TYPE;
    l_start_date                       service_fees.start_date%TYPE;
    l_end_date                         service_fees.end_date%TYPE;
BEGIN
    OPEN c_fee;

    LOOP
        FETCH c_fee INTO l_service_fee_id,
         l_terminal_id,
         l_fee_id,
         l_fee_amount,
         l_months,
         l_days,
         l_last_payment,
         l_start_date,
         l_end_date;
        EXIT WHEN c_fee%NOTFOUND;

        LOOP
            IF l_months > 0 THEN
                SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, l_months)), 'DD')
                  INTO l_last_payment
                  FROM DUAL;
            ELSE
                SELECT l_last_payment + l_days
                  INTO l_last_payment
                  FROM DUAL;
            END IF;

            EXIT WHEN l_last_payment > SYSDATE
                  OR l_last_payment > NVL (l_end_date, SYSDATE);

            IF cb_DEBUG_MODE = TRUE Then
            
                DBMS_OUTPUT.PUT_LINE('payment_service_fee: terminal_id = ''' || l_terminal_id ||
                                    ''', fee_id = ''' || l_fee_id ||
                                    ''', fee_amount = ''' || l_fee_amount ||
                                    ''', last_payment = ''' || l_last_payment ||
                                    ''', service_fee_id = ''' || l_service_fee_id ||
                                    ''', end_date = ''' || l_end_date || '''');
            END IF;
            
            BEGIN -- catch exception here
                -- skip creation of fee if start date is after fee date
                IF l_last_payment >= NVL(l_start_date, l_last_payment) THEN
                    INSERT INTO payment_service_fee
                                (payment_service_fee_id,
                                 terminal_id,
                                 fee_id,
                                 fee_amount,
                                 fee_date,
                                 service_fee_id)
                         VALUES (payment_service_fee_seq.NEXTVAL,
                                 l_terminal_id,
                                 l_fee_id,
                                 l_fee_amount,
                                 l_last_payment,
                                 l_service_fee_id);
                END IF;

                UPDATE service_fees
                   SET last_payment = l_last_payment
                 WHERE service_fee_id = l_service_fee_id;

                COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
            EXCEPTION
				 WHEN DUP_VAL_ON_INDEX THEN
				 	 ROLLBACK;
            END;
        END LOOP;
    END LOOP;

    CLOSE c_fee;
    
END scan_for_service_fees;
/

-- Grants for Procedure
GRANT EXECUTE ON scan_for_service_fees TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.SCAN_FOR_SERVICE_FEES

-- Start of DDL Script for Procedure CORP.SERVICE_FEES_UPD
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of SERVICE_FEES_UPD
DROP PROCEDURE service_fees_upd
/

CREATE OR REPLACE 
PROCEDURE service_fees_upd
   (l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
    l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
    l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
    l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
    l_effective_dt IN SERVICE_FEES.END_DATE%TYPE )
IS
   l_effective_date DATE;
   l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
BEGIN
	 IF l_effective_dt IS NULL THEN
	     l_effective_date := SYSDATE;
	 ELSE
	 	 l_effective_date := l_effective_dt;
	 END IF;
	 BEGIN
		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
		END IF;
	 EXCEPTION
	 	WHEN NO_DATA_FOUND THEN
			 NULL;
		WHEN OTHERS THEN
			 RAISE;
	 END;
	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
	 IF l_fee_amt <> 0 THEN
         INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
	 END IF;
END SERVICE_FEES_UPD;
/



-- End of DDL Script for Procedure CORP.SERVICE_FEES_UPD

-- Start of DDL Script for Procedure CORP.TERMINAL_EARLIER_BANK_ACCOUNT
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of TERMINAL_EARLIER_BANK_ACCOUNT
DROP PROCEDURE terminal_earlier_bank_account
/

CREATE OR REPLACE 
PROCEDURE terminal_earlier_bank_account
   (l_cbt_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_TERMINAL_ID%TYPE,
	l_effective_date IN DATE)
IS
    l_terminal_id CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE;
    l_end_date    CUSTOMER_BANK_TERMINAL.END_DATE%TYPE;
BEGIN
    -- Update mapping
    UPDATE CUSTOMER_BANK_TERMINAL
       SET START_DATE = l_effective_date
     WHERE CUSTOMER_BANK_TERMINAL_ID = l_cbt_id
       RETURNING TERMINAL_ID, END_DATE INTO l_terminal_id, l_end_date;

    -- Update all other Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(START_DATE, MIN_DATE) < NVL(l_end_date, MAX_DATE)
       AND NVL(END_DATE, MAX_DATE) > l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
END TERMINAL_EARLIER_BANK_ACCOUNT;
/



-- End of DDL Script for Procedure CORP.TERMINAL_EARLIER_BANK_ACCOUNT

-- Start of DDL Script for Procedure CORP.TERMINAL_PAYMENT_ACTIVATE
-- Generated 10/13/2004 9:25:23 AM from CORP@USADBD02

-- Drop the old instance of TERMINAL_PAYMENT_ACTIVATE
DROP PROCEDURE terminal_payment_activate
/

CREATE OR REPLACE 
PROCEDURE terminal_payment_activate
   (l_cust_bank_id IN CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
   	l_date IN DATE,
	l_dex IN CHAR,
	l_customer_id OUT CUSTOMER.CUSTOMER_ID%TYPE
  )
IS
    l_license_id LICENSE.LICENSE_ID%TYPE;
BEGIN
	 --VERIFY BANK ACCT
	 BEGIN
	 	 SELECT CUSTOMER_ID INTO l_customer_id FROM CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank id');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 BEGIN
	 	 SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CUSTOMER_LICENSE CL, LICENSE_NBR LN
	     	 WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
	 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	 	 	 RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
	 	 WHEN OTHERS THEN
	 	 	 RAISE;
	 END;
	 --INSERT INTO CUSTOMER_BANK_TERMINAL
	 INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
	     VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_date);
	 --INSERT INTO SERVICE_FEES
	 -- Added logic to set last payment to the effective date minus one
     -- frequency interval so that first active day has a service fee (BSK 09-14-04)
	 INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	 SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, lsf.FEE_ID,
                lsf.AMOUNT, lsf.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), l_date
		 FROM LICENSE_SERVICE_FEES lsf, FREQUENCY f
         WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
           AND lsf.LICENSE_ID = l_license_id;
	 IF l_dex <> 'N' THEN
	 	INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
	 	    SELECT SERVICE_FEE_SEQ.NEXTVAL, l_terminal_id, 4,
                   4.00, f.FREQUENCY_ID, ADD_MONTHS(l_date - f.DAYS, -f.MONTHS), l_date
		      FROM FREQUENCY f
             WHERE f.FREQUENCY_ID = 2;
	 END IF;
	 --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
	     SELECT PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, l_date
		 FROM LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
END TERMINAL_PAYMENT_ACTIVATE;
/



-- End of DDL Script for Procedure CORP.TERMINAL_PAYMENT_ACTIVATE

-- Start of DDL Script for Procedure CORP.TERMINAL_REASSIGN_BANK_ACCOUNT
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of TERMINAL_REASSIGN_BANK_ACCOUNT
DROP PROCEDURE terminal_reassign_bank_account
/

CREATE OR REPLACE 
PROCEDURE terminal_reassign_bank_account
   (l_terminal_id IN CUSTOMER_BANK_TERMINAL.TERMINAL_ID%TYPE,
    l_cust_bank_id IN CUSTOMER_BANK_TERMINAL.CUSTOMER_BANK_ID%TYPE,
	l_effective_date IN DATE)
IS
BEGIN
    -- Update all Customer Bank Terminal Mappings for this terminal
    UPDATE CUSTOMER_BANK_TERMINAL
       SET END_DATE = l_effective_date
     WHERE NVL(END_DATE, l_effective_date) >= l_effective_date
       AND TERMINAL_ID = l_terminal_id;
       
    -- Insert new mapping if customer bank is not zero
    IF l_cust_bank_id <> 0 THEN
		INSERT INTO CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
		    VALUES(CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, l_terminal_id, l_cust_bank_id, l_effective_date);
    END IF;
    
    -- Clean up dead dates
    DELETE FROM CUSTOMER_BANK_TERMINAL WHERE START_DATE >= END_DATE;
    
END TERMINAL_REASSIGN_BANK_ACCOUNT;
/

-- Grants for Procedure
GRANT EXECUTE ON terminal_reassign_bank_account TO report
WITH GRANT OPTION
/


-- End of DDL Script for Procedure CORP.TERMINAL_REASSIGN_BANK_ACCOUNT

-- Start of DDL Script for Procedure CORP.UNLOCK_EFT
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of UNLOCK_EFT
DROP PROCEDURE unlock_eft
/

CREATE OR REPLACE 
PROCEDURE unlock_eft (
  l_eft_id IN EFT.EFT_ID%TYPE
  )
IS
  l_status EFT.STATUS%TYPE;
BEGIN
	 SELECT STATUS INTO l_status FROM EFT WHERE EFT_ID = l_eft_id;
	 IF l_status <> 'L' THEN
	 	 RAISE_APPLICATION_ERROR(-20400, 'This EFT has already been approved; you can not unlock it.');
	 END IF;
	 UPDATE LEDGER SET PAYMENT_ID = 0 WHERE PAYMENT_ID IN(SELECT PAYMENT_ID FROM PAYMENTS WHERE EFT_ID = l_eft_id);
	 UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = 0 WHERE PAYMENT_ID IN(SELECT PAYMENT_ID FROM PAYMENTS WHERE EFT_ID = l_eft_id);
	 DELETE FROM PAYMENT_PROCESS_FEE WHERE PAYMENT_ID IN(SELECT PAYMENT_ID FROM PAYMENTS WHERE EFT_ID = l_eft_id);
	 DELETE FROM PAYMENTS WHERE EFT_ID = l_eft_id;
	 UPDATE PAYMENT_ADJUSTMENT SET EFT_ID = 0 WHERE EFT_ID = l_eft_id;
	 DELETE FROM EFT WHERE EFT_ID = l_eft_id;
	 COMMIT;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	   RAISE_APPLICATION_ERROR(-20401, 'This EFT does not exist.');
    WHEN OTHERS THEN
	   ROLLBACK;
	   RAISE;
END UNLOCK_EFT;
/



-- End of DDL Script for Procedure CORP.UNLOCK_EFT

-- Start of DDL Script for Procedure CORP.UPDATE_LEDGER_PENDING_TABLES
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of UPDATE_LEDGER_PENDING_TABLES
DROP PROCEDURE update_ledger_pending_tables
/

CREATE OR REPLACE 
PROCEDURE update_ledger_pending_tables
IS
BEGIN
execute immediate 'truncate table ledger_pending_revenue';

insert into ledger_pending_revenue(ledger_id, customer_bank_id, close_date, trans_type_id, total_amount)
select l.ledger_id, cbt.customer_bank_id, l.close_date, l.trans_type_id, l.total_amount
FROM ledger l, customer_bank_terminal cbt
   WHERE payment_id = 0
     AND l.terminal_id = cbt.terminal_id
     AND within1 (close_date,
                  cbt.start_date,
                  cbt.end_date
                 ) = 1;

execute immediate 'truncate table ledger_pending_process_fees';

insert into ledger_pending_process_fees(ledger_id, customer_bank_id, close_date, trans_type_id, total_amount, fee_percent)
select l.ledger_id, pf.customer_bank_id, l.close_date, l.trans_type_id, l.total_amount, pf.fee_percent
FROM ledger l, process_fees pf
   WHERE payment_id = 0
     AND l.trans_type_id = pf.trans_type_id
     AND l.terminal_id = pf.terminal_id
     AND within1 (l.close_date,
                  pf.start_date,
                  pf.end_date
                 ) = 1;
END;
/



-- End of DDL Script for Procedure CORP.UPDATE_LEDGER_PENDING_TABLES

-- Start of DDL Script for Function CORP.CARD_COMPANY
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of CARD_COMPANY
DROP FUNCTION card_company
/

CREATE OR REPLACE 
FUNCTION card_company   (card_number VARCHAR2)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    ch CHAR(1);
    cn VARCHAR2(50);
  BEGIN
    cn := TRIM(card_number);
    ch := SUBSTR(cn,1,1);
    IF ch = '4' AND LENGTH(cn) = 16 THEN
      RETURN 'VISA';
    ELSIF ch = '5' AND LENGTH(cn) = 16 THEN
      RETURN 'MasterCard';
    ELSIF ch = '3' AND LENGTH(cn) = 15 THEN
      RETURN 'American Express';
    ELSIF ch = '6' AND LENGTH(cn) = 16 THEN
      RETURN 'Discover';
    ELSE
      RETURN 'Unknown';
    END IF;
  END;
END;
/



-- End of DDL Script for Function CORP.CARD_COMPANY

-- Start of DDL Script for Function CORP.EQL
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of EQL
DROP FUNCTION eql
/

CREATE OR REPLACE 
FUNCTION eql   (s1 VARCHAR2, s2 VARCHAR2)
RETURN NUMBER IS
BEGIN
  DECLARE
  	ret BOOLEAN;
  BEGIN
  	IF s1 IS NULL THEN
	   ret := s2 IS NULL;
	ELSIF s2 IS NULL THEN
	   ret := FALSE;
	ELSE
	   ret := ( s1 = s2);
	END IF;
	IF ret THEN
	   RETURN -1;
	ELSE
	   RETURN 0;
	END IF;
  END;
END;
/



-- End of DDL Script for Function CORP.EQL

-- Start of DDL Script for Function CORP.GET_ADJUST_DESC
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of GET_ADJUST_DESC
DROP FUNCTION get_adjust_desc
/

CREATE OR REPLACE 
FUNCTION get_adjust_desc(
    l_eft_id EFT.EFT_ID%TYPE)

RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(4000);
    reason PAYMENT_ADJUSTMENT.REASON%TYPE;
    amt PAYMENT_ADJUSTMENT.AMOUNT%TYPE;
    CURSOR cur IS
        SELECT REASON, AMOUNT FROM PAYMENT_ADJUSTMENT WHERE EFT_ID = l_eft_id;
  BEGIN
    OPEN cur;
    LOOP
      FETCH cur INTO reason, amt;
      EXIT WHEN cur%NOTFOUND;
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      str := str || reason || ' (' || TO_CHAR(amt, 'FM$999,999,999,990.00') || ')';
    END LOOP;
    RETURN str;
  END;
END;
/



-- End of DDL Script for Function CORP.GET_ADJUST_DESC

-- Start of DDL Script for Function CORP.LAST_TRAN_DATE
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of LAST_TRAN_DATE
DROP FUNCTION last_tran_date
/

CREATE OR REPLACE 
FUNCTION last_tran_date
    (l_doc_id INT, l_terminal_id int)
RETURN date

/* This function returns the last tran date 
   for a specific terminal_id for a given EFT 
   
   Tom Shannon   6/6/02
*/

IS
    last_tran_dt DATE;
BEGIN
    SELECT MAX(LEDGER_DATE)
      INTO last_tran_dt
      FROM LEDGER L, BATCH B
     WHERE B.TERMINAL_ID = L_TERMINAL_ID
       AND B.DOC_ID = L_DOC_ID
       AND L.BATCH_ID = B.BATCH_ID;
    RETURN last_tran_dt;
END;
/



-- End of DDL Script for Function CORP.LAST_TRAN_DATE

-- Start of DDL Script for Function CORP.LOG_DIR
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of LOG_DIR
DROP FUNCTION log_dir
/

CREATE OR REPLACE 
FUNCTION log_dir           (logtype in varchar2) return varchar2 is
    return_value UTL_DATA.utldata%TYPE := NULL;
begin
  if logtype is null then
    --return default
      return 'No utlname value supplied';
  else
    --look for correct value
      --DECLARE
      BEGIN
      select UTL_DATA.utldata into return_value from corp.UTL_DATA where utlname = logtype;
        EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
            return_value := 'Not Found';
      END;
  end if;
  return return_value;
end;
/



-- End of DDL Script for Function CORP.LOG_DIR

-- Start of DDL Script for Function CORP.VEND_COLUMN_STRING
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of VEND_COLUMN_STRING
DROP FUNCTION vend_column_string
/

CREATE OR REPLACE 
function vend_column_string
    (atran_id INT)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(255);
    vc PURCHASE.VEND_COLUMN%TYPE;
    qnt PURCHASE.PURCHASE_ID%TYPE;
    CURSOR cur IS
        SELECT VEND_COLUMN, COUNT(PURCHASE_ID) FROM PURCHASE WHERE TRANS_ID = atran_id GROUP BY vend_column;
        /*SELECT VEND_COLUMN, AMOUNT FROM PURCHASE WHERE TRAN_ID = atran_id;*/
  BEGIN
    OPEN cur;
    LOOP
      FETCH cur INTO vc, qnt;
      EXIT WHEN cur%NOTFOUND;
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      str := str || vc;
      IF qnt > 1 THEN
        str := str || '(' || /*TRIM(TO_CHAR(*/qnt/*, '9999'))*/ || ')';
      END IF;
    END LOOP;
    RETURN str;
  END;
END;
/



-- End of DDL Script for Function CORP.VEND_COLUMN_STRING

-- Start of DDL Script for Function CORP.WITHIN1
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of WITHIN1
DROP FUNCTION within1
/

CREATE OR REPLACE 
FUNCTION within1(
	l_date IN DATE,
	l_start IN DATE,
	l_end IN DATE
	)
RETURN NUMBER PARALLEL_ENABLE DETERMINISTIC IS
BEGIN
	 IF l_start IS NOT NULL THEN
	 	IF l_date < l_start THEN
		   RETURN 0;
		END IF;
	 END IF;
	 IF l_end IS NOT NULL THEN
	 	IF l_date >= l_end THEN
		   RETURN 0;
	 	END IF;
	 END IF;
	 RETURN 1;
END;
/



-- End of DDL Script for Function CORP.WITHIN1

-- Start of DDL Script for Package CORP.GLOBALS_PKG
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of GLOBALS_PKG
DROP PACKAGE globals_pkg
/

CREATE OR REPLACE 
PACKAGE globals_pkg
  IS
--
-- Holds any global constants or types
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B Krug       09-16-04 NEW
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;

    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;

END; -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON globals_pkg TO public
WITH GRANT OPTION
/

CREATE OR REPLACE 
PACKAGE BODY globals_pkg
IS
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_date_list DATE_LIST := DATE_LIST();
        l_date DATE;
        l_real_end_date DATE;
    BEGIN
        IF (l_days <= 0 AND l_months <= 0) OR l_days + l_months * 30 <= 0 THEN
            RAISE_APPLICATION_ERROR(-20900, 'Invalid days and months specified (Days = '
                || TO_CHAR(l_days) || ', Months = ' || TO_CHAR(l_months)
                || '); would result in endless loop');
        ELSE
            SELECT ADD_MONTHS(TRUNC(l_start_date, l_trunc_to) + (l_days * (1-l_num_before)), l_months * (1-l_num_before)),
                   ADD_MONTHS(l_end_date + (l_days * l_num_after), l_months * l_num_after)
              INTO l_date, l_real_end_date
              FROM DUAL;
            WHILE l_date < l_real_end_date LOOP
                l_date_list.EXTEND;
                l_date_list(l_date_list.LAST) := l_date;
                SELECT ADD_MONTHS(l_date + l_days, l_months)
                  INTO l_date
                  FROM DUAL;
            END LOOP;
        END IF;
        RETURN l_date_list ;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_trunc_to FREQUENCY.INTERVAL%TYPE;
        l_months FREQUENCY.MONTHS%TYPE;
        l_days FREQUENCY.DAYS%TYPE;
    BEGIN
        SELECT INTERVAL, MONTHS, DAYS
          INTO l_trunc_to, l_months, l_days
          FROM FREQUENCY
         WHERE FREQUENCY_ID = l_frequency_id;
         RETURN GET_DATE_LIST(l_start_date,l_end_date,l_trunc_to,l_months,l_days,l_num_before,l_num_after);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20901, 'Invalid frequency id (' || TO_CHAR(l_frequency_id) || ')');
        WHEN OTHERS THEN
            RAISE;
    END;
END;
/


-- End of DDL Script for Package CORP.GLOBALS_PKG

-- Start of DDL Script for Package CORP.PAYMENTS_PKG
-- Generated 10/13/2004 9:25:24 AM from CORP@USADBD02

-- Drop the old instance of PAYMENTS_PKG
DROP PACKAGE payments_pkg
/

CREATE OR REPLACE 
PACKAGE payments_pkg
  IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW

   Q_SYNC_COMSUMER CONSTANT SYS.AQ$_AGENT := SYS.AQ$_AGENT('LEDGER', 'REPORT.Q_SYNC_MSG', 0);
    
   /*PROCEDURE AUTO_CREATE_EFTS;
   PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N');

   PROCEDURE REFRESH_PENDING_REVENUE;
   */

   FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR;
   /*
   FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR;
     */
   FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_max_pay_date PAYMENTS.PAYMENT_END_DATE%TYPE
     ) RETURN VARCHAR;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;
   /*
   FUNCTION GET_NOT_PAYABLE_COUNT(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE,
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_id%TYPE,
        l_close_date LEDGER.CLOSE_DATE%TYPE)
     RETURN NUMBER
     PARALLEL_ENABLE;
*/

  PROCEDURE UPDATE_LEDGER(
        l_trans_rec    TRANS%ROWTYPE);
        
  PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N');
        
  FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_ledger_date LEDGER.LEDGER_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE;
     
  PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE);
      
  PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT LEDGER.LEDGER_ID%TYPE,
       	l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN EFT.APPROVE_BY%TYPE);
      
  PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE SCAN_FOR_SERVICE_FEES;
  
  PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_ledger_date LEDGER.ledger_date%TYPE)
     RETURN BATCH.BATCH_ID%TYPE;
     
  FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE;

  FUNCTION RUN_SYNC_LEDGER_JOB
     RETURN BINARY_INTEGER;
END; -- Package spec
/


CREATE OR REPLACE 
PACKAGE BODY payments_pkg
IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_ledger_date LEDGER.LEDGER_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        IF l_always_as_accum IN('Y') THEN
            l_pay_sched := 1;
        ELSE
            SELECT PAYMENT_SCHEDULE_ID
              INTO l_pay_sched
              FROM TERMINAL
             WHERE TERMINAL_ID = l_terminal_id;
        END IF;
        BEGIN
            SELECT B.BATCH_ID
              INTO l_batch_id
              FROM BATCH B
             WHERE B.TERMINAL_ID = l_terminal_id
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
               AND B.CLOSED = 'N'
               AND CASE
                    WHEN l_pay_sched = 1 THEN MAX_DATE -- for "As Accumulated", batch start date does not matter
                    ELSE l_ledger_date
                END >= B.START_DATE
               AND l_ledger_date < NVL(B.END_DATE, MAX_DATE);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- calc start and end dates
                IF l_pay_sched = 1 THEN
                    SELECT LEAST(NVL(MAX(B.END_DATE), MIN_DATE), l_ledger_date)
                      INTO l_start_date
                      FROM BATCH B
                     WHERE B.TERMINAL_ID = l_terminal_id
                       AND B.CUSTOMER_BANK_ID = l_cust_bank_id
                       AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                    -- end date is null
                ELSIF l_pay_sched = 2 THEN
                    SELECT NVL(MAX(f.fill_date), MIN_DATE)
                      INTO l_start_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE <= l_ledger_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                    SELECT MIN(f.fill_date)
                      INTO l_end_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_ledger_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                ELSE
                    SELECT TRUNC(l_ledger_date, INTERVAL),
                           ADD_MONTHS(TRUNC(l_ledger_date, INTERVAL) + DAYS, MONTHS)
                      INTO l_start_date, l_end_date
                      FROM PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                    IF l_end_date <= l_ledger_date THEN -- trouble, should not happen
                        l_end_date := l_ledger_date + (1.0/(24*60*60));
                    END IF;
                END IF;
                -- create new batch record
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, CUSTOMER_BANK_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE)
                    VALUES(l_batch_id, l_cust_bank_id, l_terminal_id, l_pay_sched, l_start_date, l_end_date);
            WHEN OTHERS THEN
                RAISE;
        END;
        RETURN l_batch_id;
    END;

    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_ledger_date LEDGER.ledger_date%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_new_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        SELECT GET_OR_CREATE_BATCH(B.TERMINAL_ID, B.CUSTOMER_BANK_ID, l_ledger_date, 'Y')
          INTO l_new_batch_id
          FROM BATCH B
          WHERE B.BATCH_ID = l_batch_id;
        RETURN l_new_batch_id;
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_cnt NUMBER;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE
          INTO l_pay_sched, l_end_date
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        IF l_pay_sched = 1 THEN
            RETURN 'Y';
        ELSIF l_end_date IS NULL THEN -- batch is fill-to-fill and not closed yet
            RETURN 'N';
        END IF;
        SELECT COUNT(*)
          INTO l_cnt
          FROM LEDGER l
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
           AND L.DELETED = 'N'
           AND L.BATCH_ID = l_batch_id;
        IF l_cnt = 0 THEN
            RETURN 'Y';
        END IF;
        RETURN 'N';
    END;

/*
            IF NVL(l_mn_rec.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0) OR
               NVL(l_mn_rec.CUSTOMER_BANK_ID, 0) <> NVL(l_cust_bank_id, 0) THEN
                -- batch has changed so let's delete and start again
            ELSIF NVL(l_mn_rec.PROCESS_FEE_ID, 0) <> NVL(l_process_fee_id, 0) THEN
                IF l_pf_rec IS NOT NULL THEN
                    --update pf rec
                    UPDATE LEDGER SET PROCESS_FEE_ID = l_process_fee_id,
                        AMOUNT = (SELECT l_trans_rec.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                                    FROM PROCESS_FEES PF
                                    WHERE PF.PROCESS_FEE_ID = l_process_fee_id
                        ),
                        LEDGER_DATE = l_trans_rec.CLOSE_DATE,
                        SETTLE_STATE_ID = l_trans_rec.SETTLE_STATE_ID
                     WHERE LEDGER_ID = l_pf_rec.LEDGER_ID;
                ELSE -- insert pf
                    INSERT INTO LEDGER(LEDGER_ID, TERMINAL_ID, ENTRY_TYPE, TRANS_ID,
                        PROCESS_FEE_ID, SERVICE_FEE_ID, CUSTOMER_BANK_ID, AMOUNT,
                        BATCH_ID, SETTLE_STATE_ID, LEDGER_DATE)
                        SELECT LEDGER_SEQ.NEXTVAL, l_terminal_id, 'PF', l_trans_rec.TRAN_ID,
                            l_process_fee_id, NULL, l_cust_bank_id, l_trans_rec.TOTAL_AMOUNT * pf.FEE_PERCENT + pf.FEE_AMOUNT,
                            l_mn_rec.BATCH_ID, l_trans_rec.SETTLE_STATE_ID, l_trans_rec.CLOSE_DATE
                          FROM PROCESS_FEES pf
                          WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                END IF;
            ELSIF NVL(l_old_pf_id, 0) <> NVL(l_process_fee_id, 0) THEN
                IF l_pf_rec IS NOT NULL THEN
                    --update pf rec
                    UPDATE LEDGER SET PROCESS_FEE_ID = l_process_fee_id,
                        AMOUNT = (SELECT l_trans_rec.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                                    FROM PROCESS_FEES PF
                                    WHERE PF.PROCESS_FEE_ID = l_process_fee_id
                        )
                     WHERE LEDGER_ID = l_pf_rec.LEDGER_ID;
             END IF;
*/
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_rec    TRANS%ROWTYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_batch_closed BATCH.CLOSED%TYPE;
    BEGIN
        IF l_trans_rec.TRANS_TYPE_ID IN(22) THEN -- cash; ignore
            RETURN;
        END IF;
        -- get the batch & status
        BEGIN
            SELECT DISTINCT B.BATCH_ID, B.CLOSED
              INTO l_batch_id, l_batch_closed
              FROM LEDGER L, BATCH B
             WHERE L.TRANS_ID = l_trans_rec.TRAN_ID
               AND L.BATCH_ID = B.BATCH_ID;
            IF l_batch_closed = 'Y' THEN -- trouble
                RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_rec.TRAN_ID)||' is in a batch that was already closed - cannot update ledger table!');
            END IF;
            -- To make it easy let's just delete what's there and re-insert
            DELETE FROM LEDGER WHERE TRANS_ID = l_trans_rec.TRAN_ID;
            -- If terminal is null then no need to do anything more
            IF l_trans_rec.TERMINAL_ID IS NULL THEN
                RETURN;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- Create new batch for this ledger record
                -- If terminal is null then no need to do anything more
                IF l_trans_rec.TERMINAL_ID IS NULL THEN
                    RETURN;
                END IF;
                -- if refund or chargeback get the batch that the
                -- original trans is in, if open use it else use "as accumulated"
                IF l_trans_rec.TRANS_TYPE_ID IN(20, 21) THEN
                    BEGIN
                        SELECT B.BATCH_ID, B.CLOSED
                          INTO l_batch_id, l_batch_closed
                          FROM LEDGER L, BATCH B, TRANS T
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_rec.TRAN_ID
                           AND L.BATCH_ID = B.BATCH_ID;
                        IF NVL(l_batch_closed, 'Y') = 'Y' THEN
                            l_batch_id := GET_OR_CREATE_BATCH(l_trans_rec.TERMINAL_ID, l_trans_rec.CUSTOMER_BANK_ID, l_trans_rec.CLOSE_DATE, 'Y');
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                            RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_rec.TRAN_ID)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                        WHEN OTHERS THEN
                            RAISE;
                    END;
                ELSE
                    l_batch_id := GET_OR_CREATE_BATCH(l_trans_rec.TERMINAL_ID, l_trans_rec.CUSTOMER_BANK_ID, l_trans_rec.CLOSE_DATE, 'N');
                END IF;
            WHEN OTHERS THEN
                RAISE;
        END;

        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            TRANS_ID,
            PROCESS_FEE_ID,
            AMOUNT,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE)
        SELECT
            LEDGER_SEQ.NEXTVAL,
            CASE
                WHEN l_trans_rec.TRANS_TYPE_ID IN(16,19) THEN 'CC'
                WHEN l_trans_rec.TRANS_TYPE_ID = 20 THEN 'RF'
                WHEN l_trans_rec.TRANS_TYPE_ID = 21 THEN 'CB'
                --ELSE 'AU' -- Audit trail of other transactions
            END,
            l_trans_rec.TRAN_ID,
            l_trans_rec.PROCESS_FEE_ID,
            CASE
                WHEN l_trans_rec.TRANS_TYPE_ID IN(20,21) THEN -ABS(l_trans_rec.TOTAL_AMOUNT)
                ELSE l_trans_rec.TOTAL_AMOUNT END,
            l_batch_id,
            l_trans_rec.SETTLE_STATE_ID,
            l_trans_rec.CLOSE_DATE
          FROM DUAL
          WHERE l_trans_rec.TRANS_TYPE_ID IN(16,19,20,21);

        IF l_trans_rec.PROCESS_FEE_ID IS NOT NULL THEN
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'PF',
                l_trans_rec.TRAN_ID,
                l_trans_rec.PROCESS_FEE_ID,
                -(ABS(l_trans_rec.TOTAL_AMOUNT) * pf.FEE_PERCENT + pf.FEE_AMOUNT),
                l_batch_id,
                l_trans_rec.SETTLE_STATE_ID,
                l_trans_rec.CLOSE_DATE
            FROM PROCESS_FEES pf
            WHERE pf.PROCESS_FEE_ID = l_trans_rec.PROCESS_FEE_ID;
        END IF;
    END;

    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_rec    FILL%ROWTYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.START_DATE
              FROM BATCH B, TERMINAL_EPORT TE
             WHERE B.START_DATE < l_fill_rec.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > l_fill_rec.FILL_DATE
               AND B.CLOSED = 'N'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND B.TERMINAL_ID = TE.TERMINAL_ID
               AND TE.EPORT_ID = l_fill_rec.EPORT_ID
               AND l_fill_rec.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND l_fill_rec.FILL_DATE < NVL(te.END_DATE, MAX_DATE);
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(f.FILL_DATE)
              INTO l_end_date
              FROM FILL F, TERMINAL_EPORT TE
             WHERE F.FILL_DATE > l_batch_rec.START_DATE
               AND F.EPORT_ID = TE.EPORT_ID
               AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE)
               AND TE.TERMINAL_ID = l_batch_rec.TERMINAL_ID;
            -- update batch record
            UPDATE BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
             
            -- also update ledger.batch_id of entries that are after fill_date
            UPDATE LEDGER L SET L.BATCH_ID =
                GET_OR_CREATE_BATCH(l_batch_rec.TERMINAL_ID, l_batch_rec.CUSTOMER_BANK_ID, L.LEDGER_DATE,'N')
             WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
               AND L.LEDGER_DATE >= l_end_date;
         END LOOP;
    END;
    
    -- Creates a document (eft or invoice or check) for the given bank account
    -- doc_type: 'FT' = Funds Transfer (EFT); 'IN' = Invoice; 'CK' = Check
    PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_doc_id        DOC.DOC_ID%TYPE;
        l_batch_ref	    DOC.REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         DOC.TOTAL_AMOUNT%TYPE;
        l_dates         DATE_LIST;
        l_now           DATE := SYSDATE;
    BEGIN
        IF l_check_min = 'Y' THEN
            -- ensure that doc amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(L.AMOUNT), 0) INTO l_total
              FROM LEDGER L, BATCH B
             WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE NOT IN('AU')
               AND BATCH_CLOSABLE(B.BATCH_ID) = 'Y';
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'Doc total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        END IF;

        -- Create Doc
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_doc_id, l_batch_ref
          FROM DUAL;
        INSERT INTO DOC(DOC_ID, DOC_TYPE, REF_NBR, DESCRIPTION, CUSTOMER_BANK_ID,
            BANK_ACCT_NBR, BANK_ROUTING_NBR, CREATE_BY)
            SELECT l_doc_id, l_doc_type, l_batch_ref,
                NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, CUSTOMER_BANK_ID,
                BANK_ACCT_NBR, BANK_ROUTING_NBR, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        -- Moves batches into doc
        UPDATE BATCH B SET B.DOC_ID = l_doc_id
         WHERE B.CUSTOMER_BANK_ID = l_cust_bank_id
           AND B.CLOSED = 'N'
           AND BATCH_CLOSABLE(B.BATCH_ID) = 'Y';
        -- remove any unsettled trans from as accumulated batches
        UPDATE LEDGER L SET L.BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.LEDGER_DATE)
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
           AND L.BATCH_ID IN(SELECT B.BATCH_ID
                FROM BATCH B
                WHERE B.PAYMENT_SCHEDULE_ID = 1
                  AND B.DOC_ID = l_doc_id);
    END; -- end of procedure create_doc(...)

/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/
/* No longer need to do this
    PROCEDURE CREATE_PAYMENT(
        l_eft_id        EFT.EFT_ID%TYPE,
        l_cust_bank_id  CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id   PAYMENTS.TERMINAL_ID%TYPE,
        l_pay_sched     PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_start_date    PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_end_date      PAYMENTS.PAYMENT_END_DATE%TYPE)
    IS
       l_payment_id    PAYMENTS.PAYMENT_ID%TYPE;
       l_gross         PAYMENTS.GROSS_AMOUNT%TYPE;
       l_process       PAYMENTS.PROCESS_FEE_AMOUNT%TYPE;
       l_refund        PAYMENTS.REFUND_AMOUNT%TYPE;
       l_chargeback    PAYMENTS.CHARGEBACK_AMOUNT%TYPE;
       l_service       PAYMENTS.SERVICE_FEE_AMOUNT%TYPE;
       l_net           PAYMENTS.NET_AMOUNT%TYPE;
       l_tran_cnt      NUMBER;
       l_unproc_cnt    NUMBER;
       l_fees_cnt      NUMBER;
    BEGIN
        IF l_end_date IS NULL OR NVL(l_start_date, MIN_DATE) >= NVL(l_end_date, MAX_DATE) THEN
            RETURN;
        END IF;
            
        -- check that a transaction exists and that no unprocessed transactions exist in this period
        SELECT COUNT(*)
          INTO l_fees_cnt
          FROM PAYMENT_SERVICE_FEE
         WHERE PAYMENT_ID = 0
 		   AND TERMINAL_ID = l_terminal_id
           AND STATUS = 'A'
		   AND FEE_DATE >= NVL(l_start_date, MIN_DATE) AND FEE_DATE < l_end_date;
		IF l_pay_sched <> 1 THEN
            -- Check if transaction will be included
            SELECT COUNT(*)
              INTO l_tran_cnt
              FROM LEDGER l
             WHERE l.PAID_STATUS = 'U' -- unpaid
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT COUNT(*)
              INTO l_unproc_cnt
              FROM LEDGER l
             WHERE l.PAID_STATUS = 'N' -- not payable
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        
        IF (l_pay_sched = 1 AND l_fees_cnt > 0) OR (l_unproc_cnt = 0 AND l_tran_cnt > 0) THEN                		
           -- Create payment record
            SELECT PAYMENT_SEQ.NEXTVAL INTO l_payment_id FROM DUAL;
	        INSERT INTO PAYMENTS(EFT_ID, PAYMENT_ID, TERMINAL_ID,
                   PAYMENT_SCHEDULE_ID, PAYMENT_START_DATE, PAYMENT_END_DATE, CREATE_DATE)
                SELECT l_eft_id, l_payment_id, l_terminal_id,
                   l_pay_sched, l_start_date, l_end_date, SYSDATE FROM DUAL;
                   
            -- Add in service fees
            IF l_fees_cnt > 0 THEN
                UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = l_payment_id
                 WHERE PAYMENT_ID = 0
    	 		   AND TERMINAL_ID = l_terminal_id
                   AND STATUS = 'A'
    			   AND FEE_DATE >= NVL(l_start_date, MIN_DATE) AND FEE_DATE < l_end_date;
    		END IF;

            UPDATE LEDGER l SET PAYMENT_ID = l_payment_id
             WHERE l.PAID_STATUS = 'U'
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date;
               
	        INSERT INTO PAYMENT_PROCESS_FEE(PAYMENT_ID, CUSTOMER_BANK_ID, TERMINAL_ID,
                   TRANS_TYPE_ID, GROSS_AMOUNT, FEE_PERCENT, TRAN_COUNT, TRAN_FEE_AMOUNT,
                   FEE_AMOUNT, NET_AMOUNT)
	 		    SELECT l_payment_id, l_cust_bank_id, l_terminal_id,
                       TRANS_TYPE_ID, GROSS_AMOUNT, FEE_PERCENT, TRAN_COUNT, FEE_AMOUNT,
                       NVL(GROSS_AMOUNT * FEE_PERCENT, 0) + NVL(TRAN_COUNT * FEE_AMOUNT, 0),
                       GROSS_AMOUNT - NVL(GROSS_AMOUNT * FEE_PERCENT, 0) + NVL(TRAN_COUNT * FEE_AMOUNT, 0)
			      FROM (SELECT l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) GROSS_AMOUNT, NVL(l.FEE_PERCENT, 0) FEE_PERCENT,
                       COUNT(*) TRAN_COUNT, NVL(l.FEE_AMOUNT, 0) FEE_AMOUNT
			      FROM LEDGER l
                 WHERE l.payment_id = l_payment_id
                   AND l.terminal_id = l_terminal_id
                   AND l.customer_bank_id = l_cust_bank_id
			     GROUP BY l.trans_type_id, l.fee_percent, l.fee_amount);
            			
    		 --CALCULATE TOTAL AMOUNTS
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_gross FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID IN(16,19); --ONLY CREDIT AND DEBIT
    		 SELECT -SUM(FEE_AMOUNT) INTO l_process FROM PAYMENT_PROCESS_FEE WHERE PAYMENT_ID = l_payment_id;
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_refund FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID = 20;
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_chargeback FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID = 21;
    		 SELECT -SUM(FEE_AMOUNT) INTO l_service FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_ID = l_payment_id;
    		 -- Calculate Net Amount
    		 l_net := NVL(l_gross, 0) + NVL(l_process, 0) + NVL(l_refund, 0) + NVL(l_chargeback, 0) + NVL(l_service, 0);
    		 -- Insert the records into payments
    		UPDATE PAYMENTS SET (GROSS_AMOUNT, PROCESS_FEE_AMOUNT, REFUND_AMOUNT,
                    CHARGEBACK_AMOUNT, SERVICE_FEE_AMOUNT, NET_AMOUNT) =
                    (SELECT l_gross, l_process, l_refund,
                     l_chargeback, l_service, l_net FROM DUAL)
             WHERE PAYMENT_ID = l_payment_id;
   	    END IF;
    END; -- end of create_payment proc
*/
/*    PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_eft_id        EFT.EFT_ID%TYPE;
        l_batch_ref	    EFT.BATCH_REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         EFT.EFT_AMOUNT%TYPE;
        l_dates         DATE_LIST;
        l_now           DATE := SYSDATE;
        CURSOR c_payments IS
                SELECT t.TERMINAL_ID, t.PAYMENT_SCHEDULE_ID,
                       MIN(l.CLOSE_DATE) MIN_DATE, MAX(l.CLOSE_DATE) MAX_DATE
                  FROM LEDGER l, TERMINAL t
                 WHERE l.PAID_STATUS = 'U'
                   AND l.TERMINAL_ID = t.TERMINAL_ID
                   AND l.CUSTOMER_BANK_ID = l_cust_bank_id
                 GROUP BY t.TERMINAL_ID, t.PAYMENT_SCHEDULE_ID;
    BEGIN
        -- Create EFT
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_eft_id, l_batch_ref
          FROM DUAL;
        INSERT INTO EFT(EFT_ID, CUSTOMER_BANK_ID, BATCH_REF_NBR, BANK_ACCT_NBR, BANK_ROUTING_NBR, DESCRIPTION, CREATE_BY)
            SELECT l_eft_id, CUSTOMER_BANK_ID, l_batch_ref, BANK_ACCT_NBR,
                   BANK_ROUTING_NBR, NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;

        -- Create Payments for each customer bank / terminal / pay schedule combination
        FOR r_payments IN c_payments LOOP
            -- Load all dates
            IF r_payments.payment_schedule_id = 1 THEN -- for as accumulated
                l_dates := DATE_LIST(r_payments.min_date, l_now);
            ELSIF r_payments.payment_schedule_id = 2 THEN -- for fill to fill
                l_dates := DATE_LIST();
                l_dates.EXTEND;
                SELECT MAX(f.fill_date)
                  INTO l_dates(l_dates.LAST)
                  FROM FILL f, TERMINAL_EPORT te
                 WHERE f.FILL_DATE < r_payments.min_date
                   AND f.EPORT_ID = te.EPORT_ID
                   AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                   AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                   AND te.TERMINAL_ID = r_payments.terminal_id;
                DECLARE
                    CURSOR c_dates IS
                        SELECT f.fill_date
                          FROM FILL f, TERMINAL_EPORT te
                         WHERE f.fill_date BETWEEN r_payments.min_date AND r_payments.max_date
                           AND f.eport_id = te.eport_id
                           AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                           AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                           AND te.terminal_id = r_payments.terminal_id
                         ORDER BY f.fill_date;
                BEGIN
                    FOR r_dates IN c_dates LOOP
                        l_dates.EXTEND;
                        l_dates(l_dates.LAST) := r_dates.fill_date;
                    END LOOP;
                END;
                l_dates.EXTEND;
                SELECT MIN(fill_date)
                  INTO l_dates(l_dates.LAST)
                  FROM FILL f, TERMINAL_EPORT te
                 WHERE f.fill_date > r_payments.max_date
                   AND f.eport_id = te.eport_id
                   AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                   AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                   AND te.terminal_id = r_payments.terminal_id;
            ELSE
                SELECT GLOBALS_PKG.GET_DATE_LIST(r_payments.min_date, r_payments.max_date,
                       INTERVAL, MONTHS, DAYS, 1, 0)
                  INTO l_dates
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = r_payments.payment_schedule_id;
            END IF;

            DECLARE
                CURSOR c_periods IS
                    SELECT DISTINCT COLUMN_VALUE FROM TABLE(CAST(l_dates AS DATE_LIST)) WHERE COLUMN_VALUE IS NOT NULL ORDER BY 1;
                l_start_date DATE;
                l_end_date DATE;
            BEGIN
                OPEN c_periods;
                FETCH c_periods INTO l_start_date;
                IF NOT c_periods%NOTFOUND THEN
                    LOOP
                        FETCH c_periods INTO l_end_date;                   	
                        EXIT WHEN c_periods%NOTFOUND;
                   	    CREATE_PAYMENT(l_eft_id, l_cust_bank_id, r_payments.terminal_id,
                            r_payments.payment_schedule_id, l_start_date, l_end_date);
                        l_start_date := l_end_date;
                    END LOOP;
                END IF;
                CLOSE c_periods;
            EXCEPTION
                WHEN OTHERS THEN
                   CLOSE c_periods;
                   RAISE;
            END;
        END LOOP;

        IF l_check_min = 'Y' THEN
            -- ensure that eft amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(NET_AMOUNT), 0) + NVL(SUM(pa.AMOUNT), 0) INTO l_total
              FROM PAYMENTS p, PAYMENT_ADJUSTMENT pa
             WHERE p.EFT_ID(+) = l_eft_id
               AND pa.CUSTOMER_BANK_ID(+) = l_cust_bank_id
               AND pa.EFT_ID(+) = 0;
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'EFT total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        ELSE
            UPDATE PAYMENT_ADJUSTMENT SET EFT_ID = l_eft_id
             WHERE EFT_ID = 0 AND CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
    END; -- end of procedure create_eft(...)
            
    PROCEDURE AUTO_CREATE_EFTS
    IS
       CURSOR c_efts IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_efts IN c_efts LOOP
            BEGIN
                CREATE_EFT(r_efts.customer_bank_id, NULL, 'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

    PROCEDURE REFRESH_PENDING_REVENUE
    AS
    BEGIN
        DELETE FROM V$PENDING_REVENUE;
        INSERT INTO V$PENDING_REVENUE(CUSTOMER_BANK_ID, CREDIT_AMOUNT, REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT)
            SELECT CUSTOMER_BANK_ID, CREDIT_AMOUNT, REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT
            FROM VW_PENDING_REVENUE;
    END;
*/
    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       /* OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   (SELECT - SUM(S.FEE_AMOUNT) SERVICE_FEE_AMOUNT
                      FROM PAYMENT_SERVICE_FEE S, CUSTOMER_BANK_TERMINAL CBT, EFT E, PAYMENTS P
                     WHERE CBT.TERMINAL_ID = S.TERMINAL_ID
                       AND CBT.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
                       AND S.STATUS = 'A'
                       AND WITHIN1(FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                       AND FEE_DATE < l_as_of
                       AND S.PAYMENT_ID = P.PAYMENT_ID (+)
                       AND P.EFT_ID = E.EFT_ID (+)
                       AND NVL(E.STATUS, '') NOT IN ('P', 'S', 'D')
                       AND NVL(E.EFT_DATE, MAX_DATE) >= l_as_of
                   ) AS SERVICE_FEE_AMOUNT,
                   (SELECT SUM(PA.AMOUNT)
                      FROM PAYMENT_ADJUSTMENT PA, EFT E
                     WHERE PA.EFT_ID = E.EFT_ID (+)
                       AND NVL(E.STATUS, '') NOT IN ('P', 'S', 'D')
                       AND NVL(E.EFT_DATE, MAX_DATE) >= l_as_of
                       AND PA.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
                   ) AS ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB,
                        (SELECT CUSTOMER_BANK_ID,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (16, 19) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) CREDIT_AMOUNT,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (20) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) REFUND_AMOUNT,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (21) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) CHARGEBACK_AMOUNT,
                             -SUM(PROCESS_FEE_AMOUNT) PROCESS_FEE_AMOUNT
                         FROM (SELECT CUSTOMER_BANK_ID, TRANS_TYPE_ID, SUM(TOTAL_AMOUNT) TOTAL_AMOUNT, SUM(TOTAL_AMOUNT * FEE_PERCENT + FEE_AMOUNT) PROCESS_FEE_AMOUNT
                                FROM (SELECT L.CUSTOMER_BANK_ID, L.TRANS_TYPE_ID, L.TOTAL_AMOUNT, L.FEE_PERCENT, L.FEE_AMOUNT
                                          FROM LEDGER L, EFT E, PAYMENTS P
                                         WHERE L.PAYMENT_ID = P.PAYMENT_ID
                                           AND P.EFT_ID = E.EFT_ID
                                           AND E.STATUS NOT IN ('P', 'S', 'D')
                                           AND E.EFT_DATE >= l_as_of
                                           AND L.LEDGER_DATE < l_as_of
                                       UNION ALL
                                       SELECT L.CUSTOMER_BANK_ID, L.TRANS_TYPE_ID, L.TOTAL_AMOUNT, L.FEE_PERCENT, L.FEE_AMOUNT
                                       FROM LEDGER L
                                       WHERE L.PAID_STATUS = 'U' AND L.LEDGER_DATE < l_as_of
                                       ) L
                               GROUP BY CUSTOMER_BANK_ID, TRANS_TYPE_ID
                               ) L
                           GROUP BY CUSTOMER_BANK_ID
                          ) V
                  WHERE CB.CUSTOMER_BANK_ID = V.CUSTOMER_BANK_ID (+)
                    AND CB.CUSTOMER_ID = C.CUSTOMER_ID
                  ORDER BY CUSTOMER_NAME, BANK_ACCT_NBR;*/
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_max_pay_date PAYMENTS.PAYMENT_END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        UPDATE LEDGER L
           SET BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.LEDGER_DATE)
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT LEDGER.LEDGER_ID%TYPE,
       	l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 SELECT LEDGER_SEQ.NEXTVAL INTO l_ledger_id FROM DUAL;
    	 INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            AMOUNT,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION,
            CREATE_BY)
           VALUES (
            l_ledger_id,
            'AD',
            l_amt,
            GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id, SYSDATE, 'Y'),
            2 /*process-no require*/,
            SYSDATE,
            l_reason,
            l_user_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
    END;
    
    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC D SET APPROVE_BY = l_user_id, UPDATE_DATE= SYSDATE, STATUS = 'A',
            TOTAL_AMOUNT = (SELECT SUM(AMOUNT)
                FROM LEDGER L, BATCH B
               WHERE L.DELETED = 'N'
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.DOC_ID = D.DOC_ID)
  	 		WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN EFT.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC SET SENT_BY = l_user_id, UPDATE_DATE = SYSDATE, STATUS = 'P',
                SENT_DATE = SYSDATE
          WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_effective_date < SYSDATE THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    --AND L.DELETED = 'N'
 			        AND L.LEDGER_DATE >= l_effective_date
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.CLOSED = 'Y';
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
    	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
     	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_effective_date);
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   --FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   SF.END_DATE,
                   F.FEE_NAME
              FROM SERVICE_FEES SF, FREQUENCY Q, CUSTOMER_BANK_TERMINAL CBT, FEES F
             WHERE SF.FREQUENCY_ID = Q.FREQUENCY_ID
               AND SF.TERMINAL_ID = CBT.TERMINAL_ID
               AND SF.FEE_ID = F.FEE_ID
               AND WITHIN1(SYSDATE, CBT.START_DATE, CBT.END_DATE) = 1
               AND (Q.DAYS <> 0 OR Q.MONTHS <> 0)
               AND ADD_MONTHS(SF.LAST_PAYMENT + Q.DAYS, Q.MONTHS) < LEAST(NVL(SF.END_DATE, MAX_DATE), SYSDATE);

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
    BEGIN
        FOR r_fee IN c_fee LOOP
            l_last_payment := r_fee.LAST_PAYMENT;
            IF r_fee.MONTHS >= 12  AND MOD(r_fee.MONTHS, 12.0) = 0 THEN
                l_fmt := 'FMYYYY';
            ELSIF r_fee.MONTHS > 0 THEN
                l_fmt := 'FMMonth, YYYY';
            ELSIF r_fee.DAYS >= 1 AND MOD(r_fee.DAYS, 1.0) = 0 THEN
                l_fmt := 'FMMM/DD/YYYY';
            ELSE
                l_fmt := 'FMMM/DD/YYYY HH:MI AM';
            END IF;
            LOOP
                IF r_fee.MONTHS > 0 THEN
                    SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                      INTO l_last_payment
                      FROM DUAL;
                ELSE
                    SELECT l_last_payment + r_fee.DAYS
                      INTO l_last_payment
                      FROM DUAL;
                END IF;
                EXIT WHEN l_last_payment > LEAST(NVL(r_fee.END_DATE, MAX_DATE), SYSDATE);

                BEGIN -- catch exception here
                    -- skip creation of fee if start date is after fee date
                    IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES (
                            LEDGER_SEQ.NEXTVAL,
                            'SF',
                            r_fee.SERVICE_FEE_ID,
                            -ABS(r_fee.FEE_AMOUNT),
                            GET_OR_CREATE_BATCH(r_fee.TERMINAL_ID, r_fee.CUSTOMER_BANK_ID, l_last_payment, 'Y'),
                            2,
                            l_last_payment,
                            r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt));
                    END IF;

                    UPDATE SERVICE_FEES
                       SET LAST_PAYMENT = l_last_payment
                     WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;

                    COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                EXCEPTION
    				 WHEN DUP_VAL_ON_INDEX THEN
    				 	 ROLLBACK;
                END;
            END LOOP;
        END LOOP;
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
       l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
    BEGIN
    	 BEGIN
    		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
    	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
    		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
    		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
    		END IF;
    	 EXCEPTION
    	 	WHEN NO_DATA_FOUND THEN
    			 NULL;
    		WHEN OTHERS THEN
    			 RAISE;
    	 END;
    	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 IF l_fee_amt <> 0 THEN
             INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
    		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
    	 END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
    	 SELECT STATUS INTO l_status FROM DOC WHERE DOC_ID = l_doc_id;
    	 IF l_status <> 'L' THEN
    	 	 RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
    	 END IF;
    	 UPDATE BATCH SET DOC_ID = NULL WHERE DOC_ID = l_doc_id;
    	 DELETE FROM DOC WHERE DOC_ID = l_doc_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    -- Syncs ledger and batch from report's sync queue
    PROCEDURE SYNC_LEDGER_JOB
    IS
        l_sync_msg REPORT.T_SYNC_MSG;
        l_trans_rec TRANS%ROWTYPE;
        l_fill_rec FILL%ROWTYPE;
    BEGIN
        LOOP
            l_sync_msg := REPORT.SYNC_PKG.dequeue(Q_SYNC_COMSUMER);
            EXIT WHEN l_sync_msg IS NULL OR l_sync_msg.pk_id IS NULL;
            BEGIN
                IF l_sync_msg.table_name = 'TRANS' THEN
                    SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_sync_msg.pk_id;
                    UPDATE_LEDGER(l_trans_rec);
                ELSIF l_sync_msg.table_name = 'FILL' AND l_sync_msg.dml_type = 'I' THEN
                    SELECT * INTO l_fill_rec FROM FILL WHERE FILL_ID = l_sync_msg.pk_id;
                    UPDATE_FILL_BATCH(l_fill_rec);
                END IF;
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    --log error somewhere
                    ROLLBACK;
            END;
        END LOOP;
    END;
    
    FUNCTION RUN_SYNC_LEDGER_JOB
     RETURN BINARY_INTEGER
    IS
        l_job_id BINARY_INTEGER;
    BEGIN
    	DBMS_JOB.SUBMIT (l_job_id, 'PAYMENTS_PKG.SYNC_LEDGER_JOB');
    	RETURN l_job_id;
    END;
END;
/


-- End of DDL Script for Package CORP.PAYMENTS_PKG

-- Start of DDL Script for Package Body CORP.GLOBALS_PKG
-- Generated 10/13/2004 9:25:26 AM from CORP@USADBD02

-- Drop the old instance of GLOBALS_PKG
DROP PACKAGE globals_pkg
/

CREATE OR REPLACE 
PACKAGE globals_pkg
  IS
--
-- Holds any global constants or types
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B Krug       09-16-04 NEW
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;

    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST;

END; -- Package spec
/

-- Grants for Package
GRANT EXECUTE ON globals_pkg TO public
WITH GRANT OPTION
/

CREATE OR REPLACE 
PACKAGE BODY globals_pkg
IS
      
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_date_list DATE_LIST := DATE_LIST();
        l_date DATE;
        l_real_end_date DATE;
    BEGIN
        IF (l_days <= 0 AND l_months <= 0) OR l_days + l_months * 30 <= 0 THEN
            RAISE_APPLICATION_ERROR(-20900, 'Invalid days and months specified (Days = '
                || TO_CHAR(l_days) || ', Months = ' || TO_CHAR(l_months)
                || '); would result in endless loop');
        ELSE
            SELECT ADD_MONTHS(TRUNC(l_start_date, l_trunc_to) + (l_days * (1-l_num_before)), l_months * (1-l_num_before)),
                   ADD_MONTHS(l_end_date + (l_days * l_num_after), l_months * l_num_after)
              INTO l_date, l_real_end_date
              FROM DUAL;
            WHILE l_date < l_real_end_date LOOP
                l_date_list.EXTEND;
                l_date_list(l_date_list.LAST) := l_date;
                SELECT ADD_MONTHS(l_date + l_days, l_months)
                  INTO l_date
                  FROM DUAL;
            END LOOP;
        END IF;
        RETURN l_date_list ;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_trunc_to FREQUENCY.INTERVAL%TYPE;
        l_months FREQUENCY.MONTHS%TYPE;
        l_days FREQUENCY.DAYS%TYPE;
    BEGIN
        SELECT INTERVAL, MONTHS, DAYS
          INTO l_trunc_to, l_months, l_days
          FROM FREQUENCY
         WHERE FREQUENCY_ID = l_frequency_id;
         RETURN GET_DATE_LIST(l_start_date,l_end_date,l_trunc_to,l_months,l_days,l_num_before,l_num_after);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20901, 'Invalid frequency id (' || TO_CHAR(l_frequency_id) || ')');
        WHEN OTHERS THEN
            RAISE;
    END;
END;
/


-- End of DDL Script for Package Body CORP.GLOBALS_PKG

-- Start of DDL Script for Package Body CORP.PAYMENTS_PKG
-- Generated 10/13/2004 9:25:27 AM from CORP@USADBD02

-- Drop the old instance of PAYMENTS_PKG
DROP PACKAGE payments_pkg
/

CREATE OR REPLACE 
PACKAGE payments_pkg
  IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW

   Q_SYNC_COMSUMER CONSTANT SYS.AQ$_AGENT := SYS.AQ$_AGENT('LEDGER', 'REPORT.Q_SYNC_MSG', 0);
    
   /*PROCEDURE AUTO_CREATE_EFTS;
   PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N');

   PROCEDURE REFRESH_PENDING_REVENUE;
   */

   FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR;
   /*
   FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR;
     */
   FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_max_pay_date PAYMENTS.PAYMENT_END_DATE%TYPE
     ) RETURN VARCHAR;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;
   /*
   FUNCTION GET_NOT_PAYABLE_COUNT(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE,
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_id%TYPE,
        l_close_date LEDGER.CLOSE_DATE%TYPE)
     RETURN NUMBER
     PARALLEL_ENABLE;
*/

  PROCEDURE UPDATE_LEDGER(
        l_trans_rec    TRANS%ROWTYPE);
        
  PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N');
        
  FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_ledger_date LEDGER.LEDGER_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE;
     
  PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE);
      
  PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT LEDGER.LEDGER_ID%TYPE,
       	l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN EFT.APPROVE_BY%TYPE);
      
  PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE SCAN_FOR_SERVICE_FEES;
  
  PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE);
        
  PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_ledger_date LEDGER.ledger_date%TYPE)
     RETURN BATCH.BATCH_ID%TYPE;
     
  FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE;

  FUNCTION RUN_SYNC_LEDGER_JOB
     RETURN BINARY_INTEGER;
END; -- Package spec
/


CREATE OR REPLACE 
PACKAGE BODY payments_pkg
IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id BATCH.CUSTOMER_BANK_ID%TYPE,
        l_ledger_date LEDGER.LEDGER_DATE%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        IF l_always_as_accum IN('Y') THEN
            l_pay_sched := 1;
        ELSE
            SELECT PAYMENT_SCHEDULE_ID
              INTO l_pay_sched
              FROM TERMINAL
             WHERE TERMINAL_ID = l_terminal_id;
        END IF;
        BEGIN
            SELECT B.BATCH_ID
              INTO l_batch_id
              FROM BATCH B
             WHERE B.TERMINAL_ID = l_terminal_id
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
               AND B.CLOSED = 'N'
               AND CASE
                    WHEN l_pay_sched = 1 THEN MAX_DATE -- for "As Accumulated", batch start date does not matter
                    ELSE l_ledger_date
                END >= B.START_DATE
               AND l_ledger_date < NVL(B.END_DATE, MAX_DATE);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- calc start and end dates
                IF l_pay_sched = 1 THEN
                    SELECT LEAST(NVL(MAX(B.END_DATE), MIN_DATE), l_ledger_date)
                      INTO l_start_date
                      FROM BATCH B
                     WHERE B.TERMINAL_ID = l_terminal_id
                       AND B.CUSTOMER_BANK_ID = l_cust_bank_id
                       AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                    -- end date is null
                ELSIF l_pay_sched = 2 THEN
                    SELECT NVL(MAX(f.fill_date), MIN_DATE)
                      INTO l_start_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE <= l_ledger_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                    SELECT MIN(f.fill_date)
                      INTO l_end_date
                      FROM FILL f, TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_ledger_date
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_terminal_id;
                ELSE
                    SELECT TRUNC(l_ledger_date, INTERVAL),
                           ADD_MONTHS(TRUNC(l_ledger_date, INTERVAL) + DAYS, MONTHS)
                      INTO l_start_date, l_end_date
                      FROM PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                    IF l_end_date <= l_ledger_date THEN -- trouble, should not happen
                        l_end_date := l_ledger_date + (1.0/(24*60*60));
                    END IF;
                END IF;
                -- create new batch record
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, CUSTOMER_BANK_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE)
                    VALUES(l_batch_id, l_cust_bank_id, l_terminal_id, l_pay_sched, l_start_date, l_end_date);
            WHEN OTHERS THEN
                RAISE;
        END;
        RETURN l_batch_id;
    END;

    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_ledger_date LEDGER.ledger_date%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_new_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        SELECT GET_OR_CREATE_BATCH(B.TERMINAL_ID, B.CUSTOMER_BANK_ID, l_ledger_date, 'Y')
          INTO l_new_batch_id
          FROM BATCH B
          WHERE B.BATCH_ID = l_batch_id;
        RETURN l_new_batch_id;
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_cnt NUMBER;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE
          INTO l_pay_sched, l_end_date
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        IF l_pay_sched = 1 THEN
            RETURN 'Y';
        ELSIF l_end_date IS NULL THEN -- batch is fill-to-fill and not closed yet
            RETURN 'N';
        END IF;
        SELECT COUNT(*)
          INTO l_cnt
          FROM LEDGER l
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
           AND L.DELETED = 'N'
           AND L.BATCH_ID = l_batch_id;
        IF l_cnt = 0 THEN
            RETURN 'Y';
        END IF;
        RETURN 'N';
    END;

/*
            IF NVL(l_mn_rec.TERMINAL_ID, 0) <> NVL(l_terminal_id, 0) OR
               NVL(l_mn_rec.CUSTOMER_BANK_ID, 0) <> NVL(l_cust_bank_id, 0) THEN
                -- batch has changed so let's delete and start again
            ELSIF NVL(l_mn_rec.PROCESS_FEE_ID, 0) <> NVL(l_process_fee_id, 0) THEN
                IF l_pf_rec IS NOT NULL THEN
                    --update pf rec
                    UPDATE LEDGER SET PROCESS_FEE_ID = l_process_fee_id,
                        AMOUNT = (SELECT l_trans_rec.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                                    FROM PROCESS_FEES PF
                                    WHERE PF.PROCESS_FEE_ID = l_process_fee_id
                        ),
                        LEDGER_DATE = l_trans_rec.CLOSE_DATE,
                        SETTLE_STATE_ID = l_trans_rec.SETTLE_STATE_ID
                     WHERE LEDGER_ID = l_pf_rec.LEDGER_ID;
                ELSE -- insert pf
                    INSERT INTO LEDGER(LEDGER_ID, TERMINAL_ID, ENTRY_TYPE, TRANS_ID,
                        PROCESS_FEE_ID, SERVICE_FEE_ID, CUSTOMER_BANK_ID, AMOUNT,
                        BATCH_ID, SETTLE_STATE_ID, LEDGER_DATE)
                        SELECT LEDGER_SEQ.NEXTVAL, l_terminal_id, 'PF', l_trans_rec.TRAN_ID,
                            l_process_fee_id, NULL, l_cust_bank_id, l_trans_rec.TOTAL_AMOUNT * pf.FEE_PERCENT + pf.FEE_AMOUNT,
                            l_mn_rec.BATCH_ID, l_trans_rec.SETTLE_STATE_ID, l_trans_rec.CLOSE_DATE
                          FROM PROCESS_FEES pf
                          WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                END IF;
            ELSIF NVL(l_old_pf_id, 0) <> NVL(l_process_fee_id, 0) THEN
                IF l_pf_rec IS NOT NULL THEN
                    --update pf rec
                    UPDATE LEDGER SET PROCESS_FEE_ID = l_process_fee_id,
                        AMOUNT = (SELECT l_trans_rec.TOTAL_AMOUNT * PF.FEE_PERCENT + PF.FEE_AMOUNT
                                    FROM PROCESS_FEES PF
                                    WHERE PF.PROCESS_FEE_ID = l_process_fee_id
                        )
                     WHERE LEDGER_ID = l_pf_rec.LEDGER_ID;
             END IF;
*/
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_rec    TRANS%ROWTYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_batch_closed BATCH.CLOSED%TYPE;
    BEGIN
        IF l_trans_rec.TRANS_TYPE_ID IN(22) THEN -- cash; ignore
            RETURN;
        END IF;
        -- get the batch & status
        BEGIN
            SELECT DISTINCT B.BATCH_ID, B.CLOSED
              INTO l_batch_id, l_batch_closed
              FROM LEDGER L, BATCH B
             WHERE L.TRANS_ID = l_trans_rec.TRAN_ID
               AND L.BATCH_ID = B.BATCH_ID;
            IF l_batch_closed = 'Y' THEN -- trouble
                RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_rec.TRAN_ID)||' is in a batch that was already closed - cannot update ledger table!');
            END IF;
            -- To make it easy let's just delete what's there and re-insert
            DELETE FROM LEDGER WHERE TRANS_ID = l_trans_rec.TRAN_ID;
            -- If terminal is null then no need to do anything more
            IF l_trans_rec.TERMINAL_ID IS NULL THEN
                RETURN;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- Create new batch for this ledger record
                -- If terminal is null then no need to do anything more
                IF l_trans_rec.TERMINAL_ID IS NULL THEN
                    RETURN;
                END IF;
                -- if refund or chargeback get the batch that the
                -- original trans is in, if open use it else use "as accumulated"
                IF l_trans_rec.TRANS_TYPE_ID IN(20, 21) THEN
                    BEGIN
                        SELECT B.BATCH_ID, B.CLOSED
                          INTO l_batch_id, l_batch_closed
                          FROM LEDGER L, BATCH B, TRANS T
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_rec.TRAN_ID
                           AND L.BATCH_ID = B.BATCH_ID;
                        IF NVL(l_batch_closed, 'Y') = 'Y' THEN
                            l_batch_id := GET_OR_CREATE_BATCH(l_trans_rec.TERMINAL_ID, l_trans_rec.CUSTOMER_BANK_ID, l_trans_rec.CLOSE_DATE, 'Y');
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                            RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_rec.TRAN_ID)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                        WHEN OTHERS THEN
                            RAISE;
                    END;
                ELSE
                    l_batch_id := GET_OR_CREATE_BATCH(l_trans_rec.TERMINAL_ID, l_trans_rec.CUSTOMER_BANK_ID, l_trans_rec.CLOSE_DATE, 'N');
                END IF;
            WHEN OTHERS THEN
                RAISE;
        END;

        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            TRANS_ID,
            PROCESS_FEE_ID,
            AMOUNT,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE)
        SELECT
            LEDGER_SEQ.NEXTVAL,
            CASE
                WHEN l_trans_rec.TRANS_TYPE_ID IN(16,19) THEN 'CC'
                WHEN l_trans_rec.TRANS_TYPE_ID = 20 THEN 'RF'
                WHEN l_trans_rec.TRANS_TYPE_ID = 21 THEN 'CB'
                --ELSE 'AU' -- Audit trail of other transactions
            END,
            l_trans_rec.TRAN_ID,
            l_trans_rec.PROCESS_FEE_ID,
            CASE
                WHEN l_trans_rec.TRANS_TYPE_ID IN(20,21) THEN -ABS(l_trans_rec.TOTAL_AMOUNT)
                ELSE l_trans_rec.TOTAL_AMOUNT END,
            l_batch_id,
            l_trans_rec.SETTLE_STATE_ID,
            l_trans_rec.CLOSE_DATE
          FROM DUAL
          WHERE l_trans_rec.TRANS_TYPE_ID IN(16,19,20,21);

        IF l_trans_rec.PROCESS_FEE_ID IS NOT NULL THEN
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'PF',
                l_trans_rec.TRAN_ID,
                l_trans_rec.PROCESS_FEE_ID,
                -(ABS(l_trans_rec.TOTAL_AMOUNT) * pf.FEE_PERCENT + pf.FEE_AMOUNT),
                l_batch_id,
                l_trans_rec.SETTLE_STATE_ID,
                l_trans_rec.CLOSE_DATE
            FROM PROCESS_FEES pf
            WHERE pf.PROCESS_FEE_ID = l_trans_rec.PROCESS_FEE_ID;
        END IF;
    END;

    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_rec    FILL%ROWTYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.START_DATE
              FROM BATCH B, TERMINAL_EPORT TE
             WHERE B.START_DATE < l_fill_rec.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > l_fill_rec.FILL_DATE
               AND B.CLOSED = 'N'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND B.TERMINAL_ID = TE.TERMINAL_ID
               AND TE.EPORT_ID = l_fill_rec.EPORT_ID
               AND l_fill_rec.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND l_fill_rec.FILL_DATE < NVL(te.END_DATE, MAX_DATE);
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(f.FILL_DATE)
              INTO l_end_date
              FROM FILL F, TERMINAL_EPORT TE
             WHERE F.FILL_DATE > l_batch_rec.START_DATE
               AND F.EPORT_ID = TE.EPORT_ID
               AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE)
               AND TE.TERMINAL_ID = l_batch_rec.TERMINAL_ID;
            -- update batch record
            UPDATE BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
             
            -- also update ledger.batch_id of entries that are after fill_date
            UPDATE LEDGER L SET L.BATCH_ID =
                GET_OR_CREATE_BATCH(l_batch_rec.TERMINAL_ID, l_batch_rec.CUSTOMER_BANK_ID, L.LEDGER_DATE,'N')
             WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
               AND L.LEDGER_DATE >= l_end_date;
         END LOOP;
    END;
    
    -- Creates a document (eft or invoice or check) for the given bank account
    -- doc_type: 'FT' = Funds Transfer (EFT); 'IN' = Invoice; 'CK' = Check
    PROCEDURE CREATE_DOC(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE,
        l_doc_type IN DOC.DOC_TYPE%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_doc_id        DOC.DOC_ID%TYPE;
        l_batch_ref	    DOC.REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         DOC.TOTAL_AMOUNT%TYPE;
        l_dates         DATE_LIST;
        l_now           DATE := SYSDATE;
    BEGIN
        IF l_check_min = 'Y' THEN
            -- ensure that doc amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(L.AMOUNT), 0) INTO l_total
              FROM LEDGER L, BATCH B
             WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.CUSTOMER_BANK_ID = l_cust_bank_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE NOT IN('AU')
               AND BATCH_CLOSABLE(B.BATCH_ID) = 'Y';
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'Doc total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        END IF;

        -- Create Doc
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_doc_id, l_batch_ref
          FROM DUAL;
        INSERT INTO DOC(DOC_ID, DOC_TYPE, REF_NBR, DESCRIPTION, CUSTOMER_BANK_ID,
            BANK_ACCT_NBR, BANK_ROUTING_NBR, CREATE_BY)
            SELECT l_doc_id, l_doc_type, l_batch_ref,
                NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, CUSTOMER_BANK_ID,
                BANK_ACCT_NBR, BANK_ROUTING_NBR, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        -- Moves batches into doc
        UPDATE BATCH B SET B.DOC_ID = l_doc_id
         WHERE B.CUSTOMER_BANK_ID = l_cust_bank_id
           AND B.CLOSED = 'N'
           AND BATCH_CLOSABLE(B.BATCH_ID) = 'Y';
        -- remove any unsettled trans from as accumulated batches
        UPDATE LEDGER L SET L.BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.LEDGER_DATE)
         WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
           AND L.BATCH_ID IN(SELECT B.BATCH_ID
                FROM BATCH B
                WHERE B.PAYMENT_SCHEDULE_ID = 1
                  AND B.DOC_ID = l_doc_id);
    END; -- end of procedure create_doc(...)

/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/
/* No longer need to do this
    PROCEDURE CREATE_PAYMENT(
        l_eft_id        EFT.EFT_ID%TYPE,
        l_cust_bank_id  CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id   PAYMENTS.TERMINAL_ID%TYPE,
        l_pay_sched     PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_start_date    PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_end_date      PAYMENTS.PAYMENT_END_DATE%TYPE)
    IS
       l_payment_id    PAYMENTS.PAYMENT_ID%TYPE;
       l_gross         PAYMENTS.GROSS_AMOUNT%TYPE;
       l_process       PAYMENTS.PROCESS_FEE_AMOUNT%TYPE;
       l_refund        PAYMENTS.REFUND_AMOUNT%TYPE;
       l_chargeback    PAYMENTS.CHARGEBACK_AMOUNT%TYPE;
       l_service       PAYMENTS.SERVICE_FEE_AMOUNT%TYPE;
       l_net           PAYMENTS.NET_AMOUNT%TYPE;
       l_tran_cnt      NUMBER;
       l_unproc_cnt    NUMBER;
       l_fees_cnt      NUMBER;
    BEGIN
        IF l_end_date IS NULL OR NVL(l_start_date, MIN_DATE) >= NVL(l_end_date, MAX_DATE) THEN
            RETURN;
        END IF;
            
        -- check that a transaction exists and that no unprocessed transactions exist in this period
        SELECT COUNT(*)
          INTO l_fees_cnt
          FROM PAYMENT_SERVICE_FEE
         WHERE PAYMENT_ID = 0
 		   AND TERMINAL_ID = l_terminal_id
           AND STATUS = 'A'
		   AND FEE_DATE >= NVL(l_start_date, MIN_DATE) AND FEE_DATE < l_end_date;
		IF l_pay_sched <> 1 THEN
            -- Check if transaction will be included
            SELECT COUNT(*)
              INTO l_tran_cnt
              FROM LEDGER l
             WHERE l.PAID_STATUS = 'U' -- unpaid
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT COUNT(*)
              INTO l_unproc_cnt
              FROM LEDGER l
             WHERE l.PAID_STATUS = 'N' -- not payable
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        
        IF (l_pay_sched = 1 AND l_fees_cnt > 0) OR (l_unproc_cnt = 0 AND l_tran_cnt > 0) THEN                		
           -- Create payment record
            SELECT PAYMENT_SEQ.NEXTVAL INTO l_payment_id FROM DUAL;
	        INSERT INTO PAYMENTS(EFT_ID, PAYMENT_ID, TERMINAL_ID,
                   PAYMENT_SCHEDULE_ID, PAYMENT_START_DATE, PAYMENT_END_DATE, CREATE_DATE)
                SELECT l_eft_id, l_payment_id, l_terminal_id,
                   l_pay_sched, l_start_date, l_end_date, SYSDATE FROM DUAL;
                   
            -- Add in service fees
            IF l_fees_cnt > 0 THEN
                UPDATE PAYMENT_SERVICE_FEE SET PAYMENT_ID = l_payment_id
                 WHERE PAYMENT_ID = 0
    	 		   AND TERMINAL_ID = l_terminal_id
                   AND STATUS = 'A'
    			   AND FEE_DATE >= NVL(l_start_date, MIN_DATE) AND FEE_DATE < l_end_date;
    		END IF;

            UPDATE LEDGER l SET PAYMENT_ID = l_payment_id
             WHERE l.PAID_STATUS = 'U'
               AND l.TERMINAL_ID = l_terminal_id
               AND l.CUSTOMER_BANK_ID = l_cust_bank_id
               AND l.CLOSE_DATE >= NVL(l_start_date, MIN_DATE) AND l.CLOSE_DATE < l_end_date;
               
	        INSERT INTO PAYMENT_PROCESS_FEE(PAYMENT_ID, CUSTOMER_BANK_ID, TERMINAL_ID,
                   TRANS_TYPE_ID, GROSS_AMOUNT, FEE_PERCENT, TRAN_COUNT, TRAN_FEE_AMOUNT,
                   FEE_AMOUNT, NET_AMOUNT)
	 		    SELECT l_payment_id, l_cust_bank_id, l_terminal_id,
                       TRANS_TYPE_ID, GROSS_AMOUNT, FEE_PERCENT, TRAN_COUNT, FEE_AMOUNT,
                       NVL(GROSS_AMOUNT * FEE_PERCENT, 0) + NVL(TRAN_COUNT * FEE_AMOUNT, 0),
                       GROSS_AMOUNT - NVL(GROSS_AMOUNT * FEE_PERCENT, 0) + NVL(TRAN_COUNT * FEE_AMOUNT, 0)
			      FROM (SELECT l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) GROSS_AMOUNT, NVL(l.FEE_PERCENT, 0) FEE_PERCENT,
                       COUNT(*) TRAN_COUNT, NVL(l.FEE_AMOUNT, 0) FEE_AMOUNT
			      FROM LEDGER l
                 WHERE l.payment_id = l_payment_id
                   AND l.terminal_id = l_terminal_id
                   AND l.customer_bank_id = l_cust_bank_id
			     GROUP BY l.trans_type_id, l.fee_percent, l.fee_amount);
            			
    		 --CALCULATE TOTAL AMOUNTS
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_gross FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID IN(16,19); --ONLY CREDIT AND DEBIT
    		 SELECT -SUM(FEE_AMOUNT) INTO l_process FROM PAYMENT_PROCESS_FEE WHERE PAYMENT_ID = l_payment_id;
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_refund FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID = 20;
    		 SELECT SUM(TOTAL_AMOUNT) INTO l_chargeback FROM LEDGER WHERE PAYMENT_ID = l_payment_id AND TRANS_TYPE_ID = 21;
    		 SELECT -SUM(FEE_AMOUNT) INTO l_service FROM PAYMENT_SERVICE_FEE WHERE PAYMENT_ID = l_payment_id;
    		 -- Calculate Net Amount
    		 l_net := NVL(l_gross, 0) + NVL(l_process, 0) + NVL(l_refund, 0) + NVL(l_chargeback, 0) + NVL(l_service, 0);
    		 -- Insert the records into payments
    		UPDATE PAYMENTS SET (GROSS_AMOUNT, PROCESS_FEE_AMOUNT, REFUND_AMOUNT,
                    CHARGEBACK_AMOUNT, SERVICE_FEE_AMOUNT, NET_AMOUNT) =
                    (SELECT l_gross, l_process, l_refund,
                     l_chargeback, l_service, l_net FROM DUAL)
             WHERE PAYMENT_ID = l_payment_id;
   	    END IF;
    END; -- end of create_payment proc
*/
/*    PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N')
    IS
        l_eft_id        EFT.EFT_ID%TYPE;
        l_batch_ref	    EFT.BATCH_REF_NBR%TYPE;
        l_pay_min       CUSTOMER_BANK.PAY_MIN_AMOUNT%TYPE;
        l_total         EFT.EFT_AMOUNT%TYPE;
        l_dates         DATE_LIST;
        l_now           DATE := SYSDATE;
        CURSOR c_payments IS
                SELECT t.TERMINAL_ID, t.PAYMENT_SCHEDULE_ID,
                       MIN(l.CLOSE_DATE) MIN_DATE, MAX(l.CLOSE_DATE) MAX_DATE
                  FROM LEDGER l, TERMINAL t
                 WHERE l.PAID_STATUS = 'U'
                   AND l.TERMINAL_ID = t.TERMINAL_ID
                   AND l.CUSTOMER_BANK_ID = l_cust_bank_id
                 GROUP BY t.TERMINAL_ID, t.PAYMENT_SCHEDULE_ID;
    BEGIN
        -- Create EFT
        SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
          INTO l_eft_id, l_batch_ref
          FROM DUAL;
        INSERT INTO EFT(EFT_ID, CUSTOMER_BANK_ID, BATCH_REF_NBR, BANK_ACCT_NBR, BANK_ROUTING_NBR, DESCRIPTION, CREATE_BY)
            SELECT l_eft_id, CUSTOMER_BANK_ID, l_batch_ref, BANK_ACCT_NBR,
                   BANK_ROUTING_NBR, NVL(EFT_PREFIX, 'Vending: ') || l_batch_ref, l_user_id
              FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;

        -- Create Payments for each customer bank / terminal / pay schedule combination
        FOR r_payments IN c_payments LOOP
            -- Load all dates
            IF r_payments.payment_schedule_id = 1 THEN -- for as accumulated
                l_dates := DATE_LIST(r_payments.min_date, l_now);
            ELSIF r_payments.payment_schedule_id = 2 THEN -- for fill to fill
                l_dates := DATE_LIST();
                l_dates.EXTEND;
                SELECT MAX(f.fill_date)
                  INTO l_dates(l_dates.LAST)
                  FROM FILL f, TERMINAL_EPORT te
                 WHERE f.FILL_DATE < r_payments.min_date
                   AND f.EPORT_ID = te.EPORT_ID
                   AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                   AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                   AND te.TERMINAL_ID = r_payments.terminal_id;
                DECLARE
                    CURSOR c_dates IS
                        SELECT f.fill_date
                          FROM FILL f, TERMINAL_EPORT te
                         WHERE f.fill_date BETWEEN r_payments.min_date AND r_payments.max_date
                           AND f.eport_id = te.eport_id
                           AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                           AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                           AND te.terminal_id = r_payments.terminal_id
                         ORDER BY f.fill_date;
                BEGIN
                    FOR r_dates IN c_dates LOOP
                        l_dates.EXTEND;
                        l_dates(l_dates.LAST) := r_dates.fill_date;
                    END LOOP;
                END;
                l_dates.EXTEND;
                SELECT MIN(fill_date)
                  INTO l_dates(l_dates.LAST)
                  FROM FILL f, TERMINAL_EPORT te
                 WHERE f.fill_date > r_payments.max_date
                   AND f.eport_id = te.eport_id
                   AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                   AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                   AND te.terminal_id = r_payments.terminal_id;
            ELSE
                SELECT GLOBALS_PKG.GET_DATE_LIST(r_payments.min_date, r_payments.max_date,
                       INTERVAL, MONTHS, DAYS, 1, 0)
                  INTO l_dates
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = r_payments.payment_schedule_id;
            END IF;

            DECLARE
                CURSOR c_periods IS
                    SELECT DISTINCT COLUMN_VALUE FROM TABLE(CAST(l_dates AS DATE_LIST)) WHERE COLUMN_VALUE IS NOT NULL ORDER BY 1;
                l_start_date DATE;
                l_end_date DATE;
            BEGIN
                OPEN c_periods;
                FETCH c_periods INTO l_start_date;
                IF NOT c_periods%NOTFOUND THEN
                    LOOP
                        FETCH c_periods INTO l_end_date;                   	
                        EXIT WHEN c_periods%NOTFOUND;
                   	    CREATE_PAYMENT(l_eft_id, l_cust_bank_id, r_payments.terminal_id,
                            r_payments.payment_schedule_id, l_start_date, l_end_date);
                        l_start_date := l_end_date;
                    END LOOP;
                END IF;
                CLOSE c_periods;
            EXCEPTION
                WHEN OTHERS THEN
                   CLOSE c_periods;
                   RAISE;
            END;
        END LOOP;

        IF l_check_min = 'Y' THEN
            -- ensure that eft amount is greater than minimum
            SELECT PAY_MIN_AMOUNT INTO l_pay_min FROM CUSTOMER_BANK
             WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
            SELECT NVL(SUM(NET_AMOUNT), 0) + NVL(SUM(pa.AMOUNT), 0) INTO l_total
              FROM PAYMENTS p, PAYMENT_ADJUSTMENT pa
             WHERE p.EFT_ID(+) = l_eft_id
               AND pa.CUSTOMER_BANK_ID(+) = l_cust_bank_id
               AND pa.EFT_ID(+) = 0;
            IF l_total < l_pay_min THEN
                RAISE_APPLICATION_ERROR(-20910, 'EFT total ('||TO_CHAR(l_total, '$999G999G990.00')||') is less than mininum ('||TO_CHAR(l_pay_min, '$999G999G990.00')||')');
            END IF;
        ELSE
            UPDATE PAYMENT_ADJUSTMENT SET EFT_ID = l_eft_id
             WHERE EFT_ID = 0 AND CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
    END; -- end of procedure create_eft(...)
            
    PROCEDURE AUTO_CREATE_EFTS
    IS
       CURSOR c_efts IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_efts IN c_efts LOOP
            BEGIN
                CREATE_EFT(r_efts.customer_bank_id, NULL, 'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

    PROCEDURE REFRESH_PENDING_REVENUE
    AS
    BEGIN
        DELETE FROM V$PENDING_REVENUE;
        INSERT INTO V$PENDING_REVENUE(CUSTOMER_BANK_ID, CREDIT_AMOUNT, REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT)
            SELECT CUSTOMER_BANK_ID, CREDIT_AMOUNT, REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT
            FROM VW_PENDING_REVENUE;
    END;
*/
    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       /* OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   (SELECT - SUM(S.FEE_AMOUNT) SERVICE_FEE_AMOUNT
                      FROM PAYMENT_SERVICE_FEE S, CUSTOMER_BANK_TERMINAL CBT, EFT E, PAYMENTS P
                     WHERE CBT.TERMINAL_ID = S.TERMINAL_ID
                       AND CBT.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
                       AND S.STATUS = 'A'
                       AND WITHIN1(FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                       AND FEE_DATE < l_as_of
                       AND S.PAYMENT_ID = P.PAYMENT_ID (+)
                       AND P.EFT_ID = E.EFT_ID (+)
                       AND NVL(E.STATUS, '') NOT IN ('P', 'S', 'D')
                       AND NVL(E.EFT_DATE, MAX_DATE) >= l_as_of
                   ) AS SERVICE_FEE_AMOUNT,
                   (SELECT SUM(PA.AMOUNT)
                      FROM PAYMENT_ADJUSTMENT PA, EFT E
                     WHERE PA.EFT_ID = E.EFT_ID (+)
                       AND NVL(E.STATUS, '') NOT IN ('P', 'S', 'D')
                       AND NVL(E.EFT_DATE, MAX_DATE) >= l_as_of
                       AND PA.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
                   ) AS ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB,
                        (SELECT CUSTOMER_BANK_ID,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (16, 19) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) CREDIT_AMOUNT,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (20) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) REFUND_AMOUNT,
                             SUM (CASE
                                     WHEN TRANS_TYPE_ID IN (21) THEN TOTAL_AMOUNT
                                     ELSE NULL
                                  END) CHARGEBACK_AMOUNT,
                             -SUM(PROCESS_FEE_AMOUNT) PROCESS_FEE_AMOUNT
                         FROM (SELECT CUSTOMER_BANK_ID, TRANS_TYPE_ID, SUM(TOTAL_AMOUNT) TOTAL_AMOUNT, SUM(TOTAL_AMOUNT * FEE_PERCENT + FEE_AMOUNT) PROCESS_FEE_AMOUNT
                                FROM (SELECT L.CUSTOMER_BANK_ID, L.TRANS_TYPE_ID, L.TOTAL_AMOUNT, L.FEE_PERCENT, L.FEE_AMOUNT
                                          FROM LEDGER L, EFT E, PAYMENTS P
                                         WHERE L.PAYMENT_ID = P.PAYMENT_ID
                                           AND P.EFT_ID = E.EFT_ID
                                           AND E.STATUS NOT IN ('P', 'S', 'D')
                                           AND E.EFT_DATE >= l_as_of
                                           AND L.LEDGER_DATE < l_as_of
                                       UNION ALL
                                       SELECT L.CUSTOMER_BANK_ID, L.TRANS_TYPE_ID, L.TOTAL_AMOUNT, L.FEE_PERCENT, L.FEE_AMOUNT
                                       FROM LEDGER L
                                       WHERE L.PAID_STATUS = 'U' AND L.LEDGER_DATE < l_as_of
                                       ) L
                               GROUP BY CUSTOMER_BANK_ID, TRANS_TYPE_ID
                               ) L
                           GROUP BY CUSTOMER_BANK_ID
                          ) V
                  WHERE CB.CUSTOMER_BANK_ID = V.CUSTOMER_BANK_ID (+)
                    AND CB.CUSTOMER_ID = C.CUSTOMER_ID
                  ORDER BY CUSTOMER_NAME, BANK_ACCT_NBR;*/
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id PAYMENTS.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date PAYMENTS.PAYMENT_START_DATE%TYPE,
        l_max_pay_date PAYMENTS.PAYMENT_END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        UPDATE LEDGER L
           SET BATCH_ID = GET_NEW_BATCH(L.BATCH_ID, L.LEDGER_DATE)
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT D.STATUS
          INTO l_status
          FROM DOC D, BATCH B, LEDGER L
         WHERE L.LEDGER_ID = l_ledger_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID (+);
        IF l_status IS NULL THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT LEDGER.LEDGER_ID%TYPE,
       	l_cust_bank_id IN BATCH.CUSTOMER_BANK_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 SELECT LEDGER_SEQ.NEXTVAL INTO l_ledger_id FROM DUAL;
    	 INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            AMOUNT,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION,
            CREATE_BY)
           VALUES (
            l_ledger_id,
            'AD',
            l_amt,
            GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id, SYSDATE, 'Y'),
            2 /*process-no require*/,
            SYSDATE,
            l_reason,
            l_user_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
    END;
    
    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC D SET APPROVE_BY = l_user_id, UPDATE_DATE= SYSDATE, STATUS = 'A',
            TOTAL_AMOUNT = (SELECT SUM(AMOUNT)
                FROM LEDGER L, BATCH B
               WHERE L.DELETED = 'N'
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.DOC_ID = D.DOC_ID)
  	 		WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN EFT.APPROVE_BY%TYPE)
    IS
    BEGIN
    	 UPDATE DOC SET SENT_BY = l_user_id, UPDATE_DATE = SYSDATE, STATUS = 'P',
                SENT_DATE = SYSDATE
          WHERE DOC_ID = l_doc_id;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_effective_date < SYSDATE THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    --AND L.DELETED = 'N'
 			        AND L.LEDGER_DATE >= l_effective_date
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.CLOSED = 'Y';
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
    	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, START_DATE)
     	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_effective_date);
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   --FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   SF.END_DATE,
                   F.FEE_NAME
              FROM SERVICE_FEES SF, FREQUENCY Q, CUSTOMER_BANK_TERMINAL CBT, FEES F
             WHERE SF.FREQUENCY_ID = Q.FREQUENCY_ID
               AND SF.TERMINAL_ID = CBT.TERMINAL_ID
               AND SF.FEE_ID = F.FEE_ID
               AND WITHIN1(SYSDATE, CBT.START_DATE, CBT.END_DATE) = 1
               AND (Q.DAYS <> 0 OR Q.MONTHS <> 0)
               AND ADD_MONTHS(SF.LAST_PAYMENT + Q.DAYS, Q.MONTHS) < LEAST(NVL(SF.END_DATE, MAX_DATE), SYSDATE);

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
    BEGIN
        FOR r_fee IN c_fee LOOP
            l_last_payment := r_fee.LAST_PAYMENT;
            IF r_fee.MONTHS >= 12  AND MOD(r_fee.MONTHS, 12.0) = 0 THEN
                l_fmt := 'FMYYYY';
            ELSIF r_fee.MONTHS > 0 THEN
                l_fmt := 'FMMonth, YYYY';
            ELSIF r_fee.DAYS >= 1 AND MOD(r_fee.DAYS, 1.0) = 0 THEN
                l_fmt := 'FMMM/DD/YYYY';
            ELSE
                l_fmt := 'FMMM/DD/YYYY HH:MI AM';
            END IF;
            LOOP
                IF r_fee.MONTHS > 0 THEN
                    SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                      INTO l_last_payment
                      FROM DUAL;
                ELSE
                    SELECT l_last_payment + r_fee.DAYS
                      INTO l_last_payment
                      FROM DUAL;
                END IF;
                EXIT WHEN l_last_payment > LEAST(NVL(r_fee.END_DATE, MAX_DATE), SYSDATE);

                BEGIN -- catch exception here
                    -- skip creation of fee if start date is after fee date
                    IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES (
                            LEDGER_SEQ.NEXTVAL,
                            'SF',
                            r_fee.SERVICE_FEE_ID,
                            -ABS(r_fee.FEE_AMOUNT),
                            GET_OR_CREATE_BATCH(r_fee.TERMINAL_ID, r_fee.CUSTOMER_BANK_ID, l_last_payment, 'Y'),
                            2,
                            l_last_payment,
                            r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt));
                    END IF;

                    UPDATE SERVICE_FEES
                       SET LAST_PAYMENT = l_last_payment
                     WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;

                    COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                EXCEPTION
    				 WHEN DUP_VAL_ON_INDEX THEN
    				 	 ROLLBACK;
                END;
            END LOOP;
        END LOOP;
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE)
    IS
       l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
    BEGIN
    	 BEGIN
    		SELECT MAX(LAST_PAYMENT) INTO l_last_payment FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
    	 		 AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id;
    		IF NVL(l_last_payment,l_effective_date) > l_effective_date THEN  --BIG PROBLEM
    		 	 RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
    		END IF;
    	 EXCEPTION
    	 	WHEN NO_DATA_FOUND THEN
    			 NULL;
    		WHEN OTHERS THEN
    			 RAISE;
    	 END;
    	 UPDATE SERVICE_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND FEE_ID = l_fee_id AND FREQUENCY_ID = l_freq_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 IF l_fee_amt <> 0 THEN
             INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FREQUENCY_ID, FEE_AMOUNT, START_DATE, LAST_PAYMENT)
    		 	VALUES(SERVICE_FEE_SEQ.NEXTVAL,l_terminal_id,l_fee_id,l_freq_id,l_fee_amt,l_effective_date, NVL(l_last_payment, SYSDATE));
    	 END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
    	 SELECT STATUS INTO l_status FROM DOC WHERE DOC_ID = l_doc_id;
    	 IF l_status <> 'L' THEN
    	 	 RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
    	 END IF;
    	 UPDATE BATCH SET DOC_ID = NULL WHERE DOC_ID = l_doc_id;
    	 DELETE FROM DOC WHERE DOC_ID = l_doc_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    -- Syncs ledger and batch from report's sync queue
    PROCEDURE SYNC_LEDGER_JOB
    IS
        l_sync_msg REPORT.T_SYNC_MSG;
        l_trans_rec TRANS%ROWTYPE;
        l_fill_rec FILL%ROWTYPE;
    BEGIN
        LOOP
            l_sync_msg := REPORT.SYNC_PKG.dequeue(Q_SYNC_COMSUMER);
            EXIT WHEN l_sync_msg IS NULL OR l_sync_msg.pk_id IS NULL;
            BEGIN
                IF l_sync_msg.table_name = 'TRANS' THEN
                    SELECT * INTO l_trans_rec FROM TRANS WHERE TRAN_ID = l_sync_msg.pk_id;
                    UPDATE_LEDGER(l_trans_rec);
                ELSIF l_sync_msg.table_name = 'FILL' AND l_sync_msg.dml_type = 'I' THEN
                    SELECT * INTO l_fill_rec FROM FILL WHERE FILL_ID = l_sync_msg.pk_id;
                    UPDATE_FILL_BATCH(l_fill_rec);
                END IF;
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    --log error somewhere
                    ROLLBACK;
            END;
        END LOOP;
    END;
    
    FUNCTION RUN_SYNC_LEDGER_JOB
     RETURN BINARY_INTEGER
    IS
        l_job_id BINARY_INTEGER;
    BEGIN
    	DBMS_JOB.SUBMIT (l_job_id, 'PAYMENTS_PKG.SYNC_LEDGER_JOB');
    	RETURN l_job_id;
    END;
END;
/


-- End of DDL Script for Package Body CORP.PAYMENTS_PKG

-- Start of DDL Script for Trigger CORP.TBIU_LEDGER
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of TBIU_LEDGER
DROP TRIGGER tbiu_ledger
/

CREATE OR REPLACE TRIGGER tbiu_ledger
 BEFORE
  INSERT OR UPDATE
 ON ledger_old
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.TRANS_TYPE_ID IN(20,21) AND :NEW.TOTAL_AMOUNT > 0 THEN
        :NEW.TOTAL_AMOUNT := -:NEW.TOTAL_AMOUNT;
    END IF;

    -- Find terminal_id, customer_bank_id and, process_fee_id
    IF INSERTING OR :OLD.EPORT_ID != :NEW.EPORT_ID
            OR :OLD.CLOSE_DATE != :NEW.CLOSE_DATE
            OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID THEN
        BEGIN
            SELECT TERMINAL_ID
              INTO :NEW.TERMINAL_ID
              FROM TERMINAL_EPORT
             WHERE EPORT_ID = :NEW.EPORT_ID
               AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
               AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            BEGIN
                SELECT CUSTOMER_BANK_ID
                  INTO :NEW.CUSTOMER_BANK_ID
                  FROM CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.CUSTOMER_BANK_ID := NULL; -- even if customer bank mapping not found look for process fees
                WHEN OTHERS THEN
                    RAISE;
            END;
            BEGIN
                SELECT PROCESS_FEE_ID, FEE_PERCENT, FEE_AMOUNT
                  INTO :NEW.PROCESS_FEE_ID, :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
                  FROM PROCESS_FEES
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.PROCESS_FEE_ID := NULL;
                    :NEW.FEE_PERCENT := NULL;
                    :NEW.FEE_AMOUNT := NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                :NEW.TERMINAL_ID := NULL;
                :NEW.CUSTOMER_BANK_ID := NULL;
                :NEW.PROCESS_FEE_ID := NULL;
                :NEW.FEE_PERCENT := NULL;
                :NEW.FEE_AMOUNT := NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    ELSIF UPDATING AND NVL(:OLD.TERMINAL_ID, 0) != NVL(:NEW.TERMINAL_ID, 0) THEN
        -- Find customer_bank_id and, process_fee_id
        IF :NEW.TERMINAL_ID IS NULL THEN
            :NEW.CUSTOMER_BANK_ID := NULL;
            :NEW.PROCESS_FEE_ID := NULL;
            :NEW.FEE_PERCENT := NULL;
            :NEW.FEE_AMOUNT := NULL;
        ELSE
            BEGIN
                SELECT CUSTOMER_BANK_ID
                  INTO :NEW.CUSTOMER_BANK_ID
                  FROM CUSTOMER_BANK_TERMINAL
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.CUSTOMER_BANK_ID := NULL; -- even if customer bank mapping not found look for process fees
                WHEN OTHERS THEN
                    RAISE;
            END;
            BEGIN
                SELECT PROCESS_FEE_ID, FEE_PERCENT, FEE_AMOUNT
                  INTO :NEW.PROCESS_FEE_ID, :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
                  FROM PROCESS_FEES
                 WHERE TERMINAL_ID = :NEW.TERMINAL_ID
                   AND :NEW.CLOSE_DATE >= NVL(START_DATE, MIN_DATE)
                   AND :NEW.CLOSE_DATE < NVL(END_DATE, MAX_DATE);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    :NEW.PROCESS_FEE_ID := NULL;
                    :NEW.FEE_PERCENT := NULL;
                    :NEW.FEE_AMOUNT := NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    ELSIF UPDATING AND NVL(:OLD.PROCESS_FEE_ID, 0) != NVL(:NEW.PROCESS_FEE_ID, 0) THEN
        BEGIN
            SELECT FEE_PERCENT, FEE_AMOUNT
              INTO :NEW.FEE_PERCENT, :NEW.FEE_AMOUNT
              FROM PROCESS_FEES
             WHERE PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                :NEW.FEE_PERCENT := NULL;
                :NEW.FEE_AMOUNT := NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
    END IF;
    
    -- Calculate PAID_STATUS
    IF :NEW.PAYMENT_ID IS NOT NULL THEN
        :NEW.PAID_STATUS := 'P'; -- Paid
    ELSIF :NEW.SETTLE_STATE_ID IN(2,3,5,6) THEN
        :NEW.PAID_STATUS := 'U'; -- Unpaid
    ELSE
        :NEW.PAID_STATUS := 'N'; -- Not payable
    END IF;
END;
/


-- End of DDL Script for Trigger CORP.TBIU_LEDGER

-- Start of DDL Script for Trigger CORP.TRAIUD_CUSTOMER_BANK_TERMINAL
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of TRAIUD_CUSTOMER_BANK_TERMINAL
DROP TRIGGER traiud_customer_bank_terminal
/

CREATE OR REPLACE TRIGGER traiud_customer_bank_terminal
 AFTER
  INSERT OR DELETE OR UPDATE
 ON customer_bank_terminal
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE))
         WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
            OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.CUSTOMER_BANK_ID = (SELECT CBT.CUSTOMER_BANK_ID
               FROM CUSTOMER_BANK_TERMINAL CBT
               WHERE T.TERMINAL_ID = CBT.TERMINAL_ID
                 AND T.CLOSE_DATE >= NVL(CBT.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(CBT.END_DATE, MAX_DATE)) -- this should always be null, but to be safe...
             WHERE T.TERMINAL_ID = :OLD.TERMINAL_ID
               AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
               AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE);
        END IF;
    END IF;
END;
/


-- End of DDL Script for Trigger CORP.TRAIUD_CUSTOMER_BANK_TERMINAL

-- Start of DDL Script for Trigger CORP.TRAIUD_PROCESS_FEES
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of TRAIUD_PROCESS_FEES
DROP TRIGGER traiud_process_fees
/

CREATE OR REPLACE TRIGGER traiud_process_fees
 AFTER
  INSERT OR DELETE OR UPDATE
 ON process_fees
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
         WHERE T.TERMINAL_ID = :NEW.TERMINAL_ID
           AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
           AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE);
    ELSIF UPDATING THEN
        -- must update ledger & trans dim
        UPDATE LEDGER L SET L.AMOUNT = (
            SELECT T.TOTAL_AMOUNT * :NEW.FEE_PERCENT + :NEW.FEE_AMOUNT
              FROM TRANS T
             WHERE T.TRAN_ID = L.TRANS_ID)
         WHERE L.ENTRY_TYPE = 'PF'
           AND L.PROCESS_FEE_ID = :NEW.PROCESS_FEE_ID;
           
        IF :OLD.PROCESS_FEE_ID != :NEW.PROCESS_FEE_ID OR :OLD.TERMINAL_ID != :NEW.TERMINAL_ID
                OR :OLD.TRANS_TYPE_ID != :NEW.TRANS_TYPE_ID
                OR NVL(:OLD.START_DATE, MIN_DATE) !=  NVL(:NEW.START_DATE, MIN_DATE)
                OR NVL(:OLD.END_DATE, MAX_DATE) !=  NVL(:NEW.END_DATE, MAX_DATE) THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
                   FROM PROCESS_FEES PF
                   WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                     AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                     AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                     AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE))
             WHERE (T.TERMINAL_ID = :OLD.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:OLD.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:OLD.END_DATE, MAX_DATE))
                OR (T.TERMINAL_ID = :NEW.TERMINAL_ID
                    AND T.CLOSE_DATE >= NVL(:NEW.START_DATE, MIN_DATE)
                    AND T.CLOSE_DATE < NVL(:NEW.END_DATE, MAX_DATE));
        END IF;
    ELSIF DELETING THEN
        IF :OLD.START_DATE < :OLD.END_DATE THEN
            UPDATE TRANS T SET T.PROCESS_FEE_ID = (SELECT PF.PROCESS_FEE_ID
               FROM PROCESS_FEES PF
               WHERE T.TERMINAL_ID = PF.TERMINAL_ID
                 AND T.TRANS_TYPE_ID = PF.TRANS_TYPE_ID
                 AND T.CLOSE_DATE >= NVL(PF.START_DATE, MIN_DATE)
                 AND T.CLOSE_DATE < NVL(PF.END_DATE, MAX_DATE)) -- should be null always
             WHERE PROCESS_FEE_ID = :OLD.PROCESS_FEE_ID;
        END IF;
   END IF;
END;
/


-- End of DDL Script for Trigger CORP.TRAIUD_PROCESS_FEES

-- Start of DDL Script for Trigger CORP.TRBIU_BATCH
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of TRBIU_BATCH
DROP TRIGGER trbiu_batch
/

CREATE OR REPLACE TRIGGER trbiu_batch
 BEFORE
  INSERT OR UPDATE
 ON batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.DOC_ID IS NOT NULL THEN
        :NEW.CLOSED := 'Y'; -- Closed
        IF :NEW.END_DATE IS NULL THEN
            :NEW.END_DATE := SYSDATE;
        END IF;
    ELSE
        :NEW.CLOSED := 'N'; -- Open
    END IF;
END;
/


-- End of DDL Script for Trigger CORP.TRBIU_BATCH

-- Start of DDL Script for Sequence CORP.ADJUST_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of ADJUST_SEQ
DROP SEQUENCE adjust_seq
/

CREATE SEQUENCE adjust_seq
  INCREMENT BY 1
  START WITH 1022
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.ADJUST_SEQ

-- Start of DDL Script for Sequence CORP.BATCH_ID_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of BATCH_ID_SEQ
DROP SEQUENCE batch_id_seq
/

CREATE SEQUENCE batch_id_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.BATCH_ID_SEQ

-- Start of DDL Script for Sequence CORP.BATCH_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of BATCH_SEQ
DROP SEQUENCE batch_seq
/

CREATE SEQUENCE batch_seq
  INCREMENT BY 1
  START WITH 10384
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.BATCH_SEQ

-- Start of DDL Script for Sequence CORP.CARD_TYPE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CARD_TYPE_SEQ
DROP SEQUENCE card_type_seq
/

CREATE SEQUENCE card_type_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CARD_TYPE_SEQ

-- Start of DDL Script for Sequence CORP.CATEGORIES_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CATEGORIES_SEQ
DROP SEQUENCE categories_seq
/

CREATE SEQUENCE categories_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CATEGORIES_SEQ

-- Start of DDL Script for Sequence CORP.CHARGEBACK_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CHARGEBACK_SEQ
DROP SEQUENCE chargeback_seq
/

CREATE SEQUENCE chargeback_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CHARGEBACK_SEQ

-- Start of DDL Script for Sequence CORP.CUSTOMER_ADDR_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_ADDR_SEQ
DROP SEQUENCE customer_addr_seq
/

CREATE SEQUENCE customer_addr_seq
  INCREMENT BY 1
  START WITH 1564
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CUSTOMER_ADDR_SEQ

-- Start of DDL Script for Sequence CORP.CUSTOMER_BANK_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_SEQ
DROP SEQUENCE customer_bank_seq
/

CREATE SEQUENCE customer_bank_seq
  INCREMENT BY 1
  START WITH 1485
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CUSTOMER_BANK_SEQ

-- Start of DDL Script for Sequence CORP.CUSTOMER_BANK_TERMINAL_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_BANK_TERMINAL_SEQ
DROP SEQUENCE customer_bank_terminal_seq
/

CREATE SEQUENCE customer_bank_terminal_seq
  INCREMENT BY 1
  START WITH 801
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CUSTOMER_BANK_TERMINAL_SEQ

-- Start of DDL Script for Sequence CORP.CUSTOMER_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CUSTOMER_SEQ
DROP SEQUENCE customer_seq
/

CREATE SEQUENCE customer_seq
  INCREMENT BY 1
  START WITH 1465
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.CUSTOMER_SEQ

-- Start of DDL Script for Sequence CORP.DEALER_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DEALER_SEQ
DROP SEQUENCE dealer_seq
/

CREATE SEQUENCE dealer_seq
  INCREMENT BY 1
  START WITH 281
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.DEALER_SEQ

-- Start of DDL Script for Sequence CORP.DEVICE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DEVICE_SEQ
DROP SEQUENCE device_seq
/

CREATE SEQUENCE device_seq
  INCREMENT BY 1
  START WITH 1221
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.DEVICE_SEQ

-- Start of DDL Script for Sequence CORP.DEVICE_STATS_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DEVICE_STATS_SEQ
DROP SEQUENCE device_stats_seq
/

CREATE SEQUENCE device_stats_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.DEVICE_STATS_SEQ

-- Start of DDL Script for Sequence CORP.DEVICE_TYPE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DEVICE_TYPE_SEQ
DROP SEQUENCE device_type_seq
/

CREATE SEQUENCE device_type_seq
  INCREMENT BY 1
  START WITH 21
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.DEVICE_TYPE_SEQ

-- Start of DDL Script for Sequence CORP.EFT_BATCH_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of EFT_BATCH_SEQ
DROP SEQUENCE eft_batch_seq
/

CREATE SEQUENCE eft_batch_seq
  INCREMENT BY 1
  START WITH 1484
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/


-- End of DDL Script for Sequence CORP.EFT_BATCH_SEQ

-- Start of DDL Script for Sequence CORP.EFT_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of EFT_SEQ
DROP SEQUENCE eft_seq
/

CREATE SEQUENCE eft_seq
  INCREMENT BY 1
  START WITH 2365
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.EFT_SEQ

-- Start of DDL Script for Sequence CORP.EPORT_PART_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of EPORT_PART_SEQ
DROP SEQUENCE eport_part_seq
/

CREATE SEQUENCE eport_part_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.EPORT_PART_SEQ

-- Start of DDL Script for Sequence CORP.LEDGER_PAY_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_PAY_SEQ
DROP SEQUENCE ledger_pay_seq
/

CREATE SEQUENCE ledger_pay_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.LEDGER_PAY_SEQ

-- Start of DDL Script for Sequence CORP.LEDGER_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of LEDGER_SEQ
DROP SEQUENCE ledger_seq
/

CREATE SEQUENCE ledger_seq
  INCREMENT BY 1
  START WITH 3396983
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.LEDGER_SEQ

-- Start of DDL Script for Sequence CORP.LICENSE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of LICENSE_SEQ
DROP SEQUENCE license_seq
/

CREATE SEQUENCE license_seq
  INCREMENT BY 1
  START WITH 1570
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.LICENSE_SEQ

-- Start of DDL Script for Sequence CORP.PAYMENT_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of PAYMENT_SEQ
DROP SEQUENCE payment_seq
/

CREATE SEQUENCE payment_seq
  INCREMENT BY 1
  START WITH 9704
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.PAYMENT_SEQ

-- Start of DDL Script for Sequence CORP.PAYMENT_SERVICE_FEE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of PAYMENT_SERVICE_FEE_SEQ
DROP SEQUENCE payment_service_fee_seq
/

CREATE SEQUENCE payment_service_fee_seq
  INCREMENT BY 1
  START WITH 15901
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.PAYMENT_SERVICE_FEE_SEQ

-- Start of DDL Script for Sequence CORP.PROCESS_FEE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of PROCESS_FEE_SEQ
DROP SEQUENCE process_fee_seq
/

CREATE SEQUENCE process_fee_seq
  INCREMENT BY 1
  START WITH 1172
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- Grants for Sequence
GRANT SELECT ON process_fee_seq TO report
WITH GRANT OPTION
/

-- End of DDL Script for Sequence CORP.PROCESS_FEE_SEQ

-- Start of DDL Script for Sequence CORP.PRODUCT_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of PRODUCT_SEQ
DROP SEQUENCE product_seq
/

CREATE SEQUENCE product_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.PRODUCT_SEQ

-- Start of DDL Script for Sequence CORP.REASON_ CODE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of REASON_ CODE_SEQ
DROP SEQUENCE "REASON_ CODE_SEQ"
/

CREATE SEQUENCE "REASON_ CODE_SEQ"
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.REASON_ CODE_SEQ

-- Start of DDL Script for Sequence CORP.REFUND_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of REFUND_SEQ
DROP SEQUENCE refund_seq
/

CREATE SEQUENCE refund_seq
  INCREMENT BY 1
  START WITH 3375
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/

-- Grants for Sequence
GRANT SELECT ON refund_seq TO g4op
WITH GRANT OPTION
/

-- End of DDL Script for Sequence CORP.REFUND_SEQ

-- Start of DDL Script for Sequence CORP.SCHEDULE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of SCHEDULE_SEQ
DROP SEQUENCE schedule_seq
/

CREATE SEQUENCE schedule_seq
  INCREMENT BY 1
  START WITH 21
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.SCHEDULE_SEQ

-- Start of DDL Script for Sequence CORP.SERVICE_FEE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of SERVICE_FEE_SEQ
DROP SEQUENCE service_fee_seq
/

CREATE SEQUENCE service_fee_seq
  INCREMENT BY 1
  START WITH 1832
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

-- Grants for Sequence
GRANT SELECT ON service_fee_seq TO report
WITH GRANT OPTION
/

-- End of DDL Script for Sequence CORP.SERVICE_FEE_SEQ

-- Start of DDL Script for Sequence CORP.TERMINAL_RULE_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of TERMINAL_RULE_SEQ
DROP SEQUENCE terminal_rule_seq
/

CREATE SEQUENCE terminal_rule_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.TERMINAL_RULE_SEQ

-- Start of DDL Script for Sequence CORP.USER_MAIL_SEQ
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of USER_MAIL_SEQ
DROP SEQUENCE user_mail_seq
/

CREATE SEQUENCE user_mail_seq
  INCREMENT BY 1
  START WITH 101
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/


-- End of DDL Script for Sequence CORP.USER_MAIL_SEQ

-- Start of DDL Script for Synonym CORP.CREATE_CUST_USER_ADMIN
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of CREATE_CUST_USER_ADMIN
DROP SYNONYM create_cust_user_admin
/

CREATE SYNONYM create_cust_user_admin
  FOR create_cust_user_admin
/


-- End of DDL Script for Synonym CORP.CREATE_CUST_USER_ADMIN

-- Start of DDL Script for Synonym CORP.DATE_LIST
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DATE_LIST
DROP SYNONYM date_list
/

CREATE SYNONYM date_list
  FOR date_list
/


-- End of DDL Script for Synonym CORP.DATE_LIST

-- Start of DDL Script for Synonym CORP.DEVICE
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DEVICE
DROP SYNONYM device
/

CREATE SYNONYM device
  FOR eport
/


-- End of DDL Script for Synonym CORP.DEVICE

-- Start of DDL Script for Synonym CORP.DEVICE_TYPE
-- Generated 10/13/2004 9:25:29 AM from CORP@USADBD02

-- Drop the old instance of DEVICE_TYPE
DROP SYNONYM device_type
/

CREATE SYNONYM device_type
  FOR device_type
/


-- End of DDL Script for Synonym CORP.DEVICE_TYPE

-- Start of DDL Script for Synonym CORP.EPORT
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of EPORT
DROP SYNONYM eport
/

CREATE SYNONYM eport
  FOR eport
/


-- End of DDL Script for Synonym CORP.EPORT

-- Start of DDL Script for Synonym CORP.FILL
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of FILL
DROP SYNONYM fill
/

CREATE SYNONYM fill
  FOR fill
/


-- End of DDL Script for Synonym CORP.FILL

-- Start of DDL Script for Synonym CORP.LOCATION
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of LOCATION
DROP SYNONYM location
/

CREATE SYNONYM location
  FOR location
/


-- End of DDL Script for Synonym CORP.LOCATION

-- Start of DDL Script for Synonym CORP.MAX_DATE
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of MAX_DATE
DROP SYNONYM max_date
/

CREATE SYNONYM max_date
  FOR max_date
/


-- End of DDL Script for Synonym CORP.MAX_DATE

-- Start of DDL Script for Synonym CORP.MIN_DATE
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of MIN_DATE
DROP SYNONYM min_date
/

CREATE SYNONYM min_date
  FOR min_date
/


-- End of DDL Script for Synonym CORP.MIN_DATE

-- Start of DDL Script for Synonym CORP.SETTLE_STATE
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of SETTLE_STATE
DROP SYNONYM settle_state
/

CREATE SYNONYM settle_state
  FOR trans_state
/


-- End of DDL Script for Synonym CORP.SETTLE_STATE

-- Start of DDL Script for Synonym CORP.SETTLEMENT
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of SETTLEMENT
DROP SYNONYM settlement
/

CREATE SYNONYM settlement
  FOR settlement
/


-- End of DDL Script for Synonym CORP.SETTLEMENT

-- Start of DDL Script for Synonym CORP.TERMINAL
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of TERMINAL
DROP SYNONYM terminal
/

CREATE SYNONYM terminal
  FOR terminal
/


-- End of DDL Script for Synonym CORP.TERMINAL

-- Start of DDL Script for Synonym CORP.TERMINAL_EPORT
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of TERMINAL_EPORT
DROP SYNONYM terminal_eport
/

CREATE SYNONYM terminal_eport
  FOR terminal_eport
/


-- End of DDL Script for Synonym CORP.TERMINAL_EPORT

-- Start of DDL Script for Synonym CORP.TRANS
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of TRANS
DROP SYNONYM trans
/

CREATE SYNONYM trans
  FOR trans
/


-- End of DDL Script for Synonym CORP.TRANS

-- Start of DDL Script for Synonym CORP.TRANS_TYPE
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of TRANS_TYPE
DROP SYNONYM trans_type
/

CREATE SYNONYM trans_type
  FOR trans_type
/


-- End of DDL Script for Synonym CORP.TRANS_TYPE

-- Start of DDL Script for Synonym CORP.USER_CUSTOMER_BANK
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of USER_CUSTOMER_BANK
DROP SYNONYM user_customer_bank
/

CREATE SYNONYM user_customer_bank
  FOR user_customer_bank
/


-- End of DDL Script for Synonym CORP.USER_CUSTOMER_BANK

-- Start of DDL Script for Synonym CORP.USER_LOGIN
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of USER_LOGIN
DROP SYNONYM user_login
/

CREATE SYNONYM user_login
  FOR user_login
/


-- End of DDL Script for Synonym CORP.USER_LOGIN

-- Start of DDL Script for Synonym CORP.VW_TERMINAL
-- Generated 10/13/2004 9:25:30 AM from CORP@USADBD02

-- Drop the old instance of VW_TERMINAL
DROP SYNONYM vw_terminal
/

CREATE SYNONYM vw_terminal
  FOR vw_terminal
/


-- End of DDL Script for Synonym CORP.VW_TERMINAL

-- Foreign Key
ALTER TABLE batch
ADD FOREIGN KEY (doc_id)
REFERENCES doc (doc_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (terminal_id)
REFERENCES terminal (terminal_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (payment_schedule_id)
REFERENCES payment_schedule (payment_schedule_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- Foreign Key
ALTER TABLE categories
ADD FOREIGN KEY (category_id)
REFERENCES categories (category_id)
/
-- Foreign Key
ALTER TABLE chargeback
ADD FOREIGN KEY (reason_code_id)
REFERENCES reason_code (reason_code_id)
/
-- Foreign Key
ALTER TABLE customer_addr
ADD FOREIGN KEY (customer_id)
REFERENCES customer (customer_id)
/
ALTER TABLE customer_addr
ADD FOREIGN KEY (addr_type)
REFERENCES address_type (addr_type_id)
/
-- Foreign Key
ALTER TABLE customer_bank
ADD FOREIGN KEY (customer_id)
REFERENCES customer (customer_id)
/
ALTER TABLE customer_bank
ADD FOREIGN KEY (schedule_id)
REFERENCES schedule (schedule_id)
/
-- Foreign Key
ALTER TABLE customer_bank_terminal
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- Foreign Key
ALTER TABLE dealer_license
ADD CONSTRAINT fk_dl_dealer_id FOREIGN KEY (dealer_id)
REFERENCES dealer (dealer_id)
/
-- Foreign Key
ALTER TABLE doc
ADD CONSTRAINT fk_doc_approve_user FOREIGN KEY (approve_by)
REFERENCES user_login (user_id)
/
ALTER TABLE doc
ADD CONSTRAINT fk_doc_create_user FOREIGN KEY (create_by)
REFERENCES user_login (user_id)
/
ALTER TABLE doc
ADD CONSTRAINT fk_doc_sent_user FOREIGN KEY (sent_by)
REFERENCES user_login (user_id)
/
ALTER TABLE doc
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- Foreign Key
ALTER TABLE ledger
ADD CONSTRAINT fk_ledger_user FOREIGN KEY (create_by)
REFERENCES user_login (user_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (trans_id)
REFERENCES trans (tran_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (settle_state_id)
REFERENCES trans_state (state_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (service_fee_id)
REFERENCES service_fees (service_fee_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (process_fee_id)
REFERENCES process_fees (process_fee_id)
/
ALTER TABLE ledger
ADD FOREIGN KEY (batch_id)
REFERENCES batch (batch_id)
/
-- Foreign Key
ALTER TABLE refund
ADD FOREIGN KEY (reason_code_id)
REFERENCES reason_code (reason_code_id)
/
-- Foreign Key
ALTER TABLE schedule
ADD FOREIGN KEY (schedule_type)
REFERENCES schedule_type (schedule_type_id)
/
-- End of DDL script for Foreign Key(s)
