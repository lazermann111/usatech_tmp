CREATE OR REPLACE FUNCTION             CORP.GET_ADJUST_DESC (
    l_eft_id DOC.DOC_ID%TYPE)
RETURN VARCHAR2 IS
BEGIN
  DECLARE
    str VARCHAR2(4000);
    CURSOR cur IS
        SELECT DESCRIPTION || ' (' || TO_CHAR(AMOUNT, 'FM$999,999,999,990.00') || ')' TEXT
          FROM LEDGER L, BATCH B
         WHERE L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_eft_id
           AND L.ENTRY_TYPE = 'AD'
           AND L.DELETED = 'N';
  BEGIN
    FOR rec IN cur LOOP
        IF LENGTH(str) > 3997 THEN
            str := SUBSTR(str, 1, 3997) || '...';
            EXIT;
        ELSIF LENGTH(str) > 0 THEN
            str := str || ', ';
        END IF;
        IF LENGTH(str) + LENGTH(rec.TEXT) > 4000 THEN
            str := SUBSTR(str || rec.TEXT, 1, 3997) || '...';
            EXIT;
        ELSE
            str := str || rec.TEXT;
        END IF;
    END LOOP;
    RETURN str;
  END;
END;
/