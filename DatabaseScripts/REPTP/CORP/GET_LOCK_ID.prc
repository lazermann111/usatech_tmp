CREATE OR REPLACE PROCEDURE CORP.GET_LOCK_ID (
    l_object_type VARCHAR2,
    l_object_id   NUMBER,
    l_handle OUT VARCHAR2)
AS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DBMS_LOCK.ALLOCATE_UNIQUE(l_object_type || CHR(0) || TO_CHAR(l_object_id), l_handle);
END;
/
