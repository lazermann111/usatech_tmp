CREATE OR REPLACE PROCEDURE CORP.POPULATE_UNPAID_ORPHANS    (inDate IN DATE)
   IS
--
-- Purpose: This will populate the unpaid_monies_rpt table with
--          with data for a given date
--
-- MODIFICATION HISTORY
-- Person      Date        Comments
-- ---------   ---------   ------------------------------------------
-- J Bradley   15-MAR-05   Inital version

    tDate DATE;
    orpahnCustBankId corp.customer_bank.customer_bank_id%TYPE;

BEGIN
    -- ensure tDate is truncated to a day (i.e. time = 00:00:00)
    tDate := TRUNC(inDate);

    /* get the orpahnCustBankId for the Orphan Transactions customer */
    SELECT cbk.customer_bank_id
    INTO orpahnCustBankId
    FROM customer cus, customer_bank cbk
    WHERE cbk.customer_id = cus.customer_id
    AND cus.customer_name = '~ Orphan Transactions'
    AND ROWNUM = 1;
	
	-- truncate the scratch table
	EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
	
	-- loop through each orphan terminal where terminal_id != 0 in trans
	/* this is faster than doing it through standard SQL */
	FOR tot IN (
	    SELECT DISTINCT tr.terminal_id orph_ter
		FROM trans tr, terminal ter
		WHERE tr.terminal_id = ter.terminal_id (+)
		AND ter.terminal_id IS NULL
		AND tr.terminal_id != 0
	)		
    LOOP
        
		-- Insert credit_amount and process_fee_amount
        INSERT INTO scr_unpaid_monies
		SELECT orpahnCustBankId customer_bank_id,
		SUM(tr.total_amount) credit_amount,
		0, 0,
		(SUM(tr.total_amount) * -0.05) process_fee_amount,
		0, 0
		FROM trans tr
		WHERE tr.terminal_id = tot.orph_ter
		AND tr.trans_type_id = 16
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
		-- Insert refund_amount
		INSERT INTO scr_unpaid_monies
        SELECT orpahnCustBankId customer_bank_id,
		0,
		SUM(tr.total_amount) refund_amount,
		0, 0, 0, 0
		FROM trans tr
		WHERE tr.terminal_id = tot.orph_ter
		AND tr.trans_type_id = 20
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;
		
		-- Insert chargeback_amount
		INSERT INTO scr_unpaid_monies
        SELECT orpahnCustBankId customer_bank_id,
        0, 0,
		SUM(tr.total_amount) chargeback_amount,
		0, 0, 0
		FROM trans tr
		WHERE tr.terminal_id = tot.orph_ter
		AND tr.trans_type_id = 21
		AND tr.status != 'D'
		AND tr.status != 'E'
		AND tr.server_date < tDate;	
		
    END LOOP;
    
    
    /* get the rest of the trans terminal orphans (terminal_id = 0) */
    
    -- Insert credit_amount and process_fee_amount      
    INSERT INTO scr_unpaid_monies
	SELECT orpahnCustBankId customer_bank_id,
	SUM(tr.total_amount) credit_amount,
	0, 0,
	(SUM(tr.total_amount) * -0.05) process_fee_amount,
	0, 0
	FROM trans tr
	WHERE tr.terminal_id = 0
	AND tr.trans_type_id = 16
	AND tr.status != 'D'
	AND tr.status != 'E'
	AND tr.server_date < tDate;
	
	-- Insert refund_amount
	INSERT INTO scr_unpaid_monies
    SELECT orpahnCustBankId customer_bank_id,
	0,
	SUM(tr.total_amount) refund_amount,
	0, 0, 0, 0
	FROM trans tr
	WHERE tr.terminal_id = 0
	AND tr.trans_type_id = 20
	AND tr.status != 'D'
	AND tr.status != 'E'
	AND tr.server_date < tDate;
	
	-- Insert chargeback_amount
	INSERT INTO scr_unpaid_monies
    SELECT orpahnCustBankId customer_bank_id,
    0, 0,
	SUM(tr.total_amount) chargeback_amount,
	0, 0, 0
	FROM trans tr
	WHERE tr.terminal_id = 0
	AND tr.trans_type_id = 21
	AND tr.status != 'D'
	AND tr.status != 'E'
	AND tr.server_date < tDate;
	
	
	-- Insert the the calulated data into the UNPAID_MONIES_RPT table
	INSERT INTO unpaid_monies_rpt (
        customer_bank_id,
        server_date,
        credit_amount,
        refund_amount,
        chargeback_amount,
        process_fee_amount,
        service_fee_amount,
        adjust_amount,
        net_payment_amount
    )		
	SELECT tum.customer_bank_id,
    tDate server_date,
    SUM(tum.credit_amount) credit_amount,
    SUM(tum.refund_amount) refund_amount,
    SUM(tum.chargeback_amount) chargeback_amount,
    SUM(tum.process_fee_amount) process_fee_amount,
    SUM(tum.service_fee_amount) service_fee_amount,
    SUM(tum.adjust_amount) adjust_amount,
    (
    	(SUM(tum.credit_amount) + SUM(tum.refund_amount)
    	+ SUM(tum.chargeback_amount) + SUM(tum.process_fee_amount)
    	+ SUM(tum.service_fee_amount) + SUM(tum.adjust_amount))
    ) net_payment_amount
    FROM scr_unpaid_monies tum
    GROUP BY tum.customer_bank_id;
    
    -- truncate the scratch table 
    EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
    
END; -- Procedure
/