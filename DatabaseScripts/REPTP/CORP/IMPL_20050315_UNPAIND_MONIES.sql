/* 

Description:

This is an update for the Unpaid Monies Reporting functionality.
It includes daily data generation scripts and a new report table 
(the table will contain summarized data). Additionally, we are
adding unpaid Orphan Transactions to this report.

Created By:
J Bradley

Date:
2005-03-15

Grant Privileges (run as):
CORP

*/



-- summarized data table
@CORP.UNPAID_MONIES_RPT_ddl.sql;

-- scratch table for data population
@CORP.SCR_UNPAID_MONIES_ddl.sql;

-- standard unpaid data generating proc
@CORP.POPULATE_UNPAID_MONIES_ddl.sql;

-- unpaid orphans data generating proc 
@CORP.POPULATE_UNPAID_ORPHANS_ddl.sql; 