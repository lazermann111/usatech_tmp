-- Start of DDL Script for Table CORP.SCR_UNPAID_MONIES
-- Generated 3/15/2005 10:32:50 AM from CORP@USADBD02

CREATE TABLE corp.scr_unpaid_monies
    (customer_bank_id               NUMBER(20,0) NOT NULL,
    credit_amount                  NUMBER(21,8),
    refund_amount                  NUMBER(21,8),
    chargeback_amount              NUMBER(21,8),
    process_fee_amount             NUMBER(21,8),
    service_fee_amount             NUMBER(21,8),
    adjust_amount                  NUMBER(21,8))
  TABLESPACE  corp_data
/




-- End of DDL Script for Table CORP.SCR_UNPAID_MONIES

