SELECT * FROM corp.fees

INSERT INTO corp.fees(FEE_ID,FEE_NAME,DESCRIPTION)
VALUES
(9,'Host Fee','An amount charged for each host attached to a device');

COMMIT;

