-- Start of DDL Script for Table CORP.LICENSE_PROCESS_FEES
-- Generated 9/17/2004 11:34:50 AM from CORP@USADBD02

CREATE TABLE license_process_fees
    (license_id                     NUMBER NOT NULL,
    trans_type_id                  NUMBER NOT NULL,
    fee_percent                    NUMBER(8,4) NOT NULL
  ,
  PRIMARY KEY (license_id, trans_type_id)
  USING INDEX)
/

-- Grants for Table
GRANT DELETE ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON license_process_fees TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON license_process_fees TO report
WITH GRANT OPTION
/



-- End of DDL Script for Table CORP.LICENSE_PROCESS_FEES

