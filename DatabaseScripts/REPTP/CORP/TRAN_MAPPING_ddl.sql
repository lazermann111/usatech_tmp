-- Start of DDL Script for Table CORP.TRAN_MAPPING
-- Generated 16-Sep-2004 11:03:53 from CORP@usadbd02

CREATE TABLE tran_mapping
    (trans_id                       NUMBER NOT NULL,
    terminal_id                    NUMBER,
    customer_bank_id               NUMBER,
    process_fee_id                 NUMBER)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  small_reptp_datafile
  STORAGE   (
    INITIAL     131072
    NEXT        131072
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/




-- Constraints for TRAN_MAPPING




-- End of DDL Script for Table CORP.TRAN_MAPPING

-- Foreign Key
ALTER TABLE tran_mapping
ADD CONSTRAINT fk_tran_mapping_customer_bank FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id) ON DELETE CASCADE
/
ALTER TABLE tran_mapping
ADD CONSTRAINT fk_tran_mapping_process_fees FOREIGN KEY (process_fee_id)
REFERENCES process_fees (process_fee_id) ON DELETE CASCADE
/
-- End of DDL script for Foreign Key(s)
