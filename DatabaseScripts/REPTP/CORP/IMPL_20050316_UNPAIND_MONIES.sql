/* 

Description:

This is an update for the Unpaid Monies Reporting functionality.
It includes updates to the daily data generation scripts and a 
new orphan customer to device type linkage table (the table will 
contain a record for each device type). 

Created By:
J Bradley

Date:
2005-03-16

Grant Privileges (run as):
CORP

*/


-- orphan customer to device type linkage table
@CORP.ORPHAN_CUST_DEV_ddl.sql;

-- ORPHAN_CUST_DEV sequence
@CORP.SEQ_ORPHAN_CUST_DEV_ID_ddl.sql;

-- standard unpaid data generating proc update
@CORP.POPULATE_UNPAID_MONIES_ddl.sql;

-- unpaid orphans data generating proc update
@CORP.POPULATE_UNPAID_ORPHANS_ddl.sql; 