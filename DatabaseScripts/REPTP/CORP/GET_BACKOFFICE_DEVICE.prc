CREATE OR REPLACE PROCEDURE CORP.GET_BACKOFFICE_DEVICE(
    pn_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    pn_device_id OUT NUMBER,
    pv_device_serial_cd OUT VARCHAR2,
    pv_bank_country_cd OUT CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE)
IS
    lv_device_name VARCHAR2(60);
    lv_device_type_desc VARCHAR2(60);
    ln_device_sub_type_id NUMBER(3);
    lc_prev_device_active_yn_flag VARCHAR(1);
    lc_master_id_always_inc VARCHAR(1);
    ln_key_gen_time NUMBER;
    lc_legacy_safe_key VARCHAR(1);
    lv_time_zone_guid REPORT.TIME_ZONE.TIME_ZONE_GUID%TYPE; 
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(1000);
    ln_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
    ln_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
    ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
    ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
    lv_customer_address1 CORP.CUSTOMER_ADDR.ADDRESS1%TYPE;                                                                                                                                                               
    lv_customer_city CORP.CUSTOMER_ADDR.CITY%TYPE;                                                                                                                                                                              
    lv_customer_state_cd CORP.CUSTOMER_ADDR.STATE%TYPE;                                                                                                                                                                              
    lv_customer_postal CORP.CUSTOMER_ADDR.ZIP%TYPE;                                                                                                                                                                              
    lv_customer_country_cd CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE;
    lv_location_name REPORT.LOCATION.LOCATION_NAME%TYPE;
BEGIN
    SELECT D.DEVICE_ID, 'V2-' || CB.CUSTOMER_BANK_ID || '-' || CU.CURRENCY_CODE, CB.BANK_COUNTRY_CD, CB.CUSTOMER_ID, SUBSTR('Back Office - ' || CB.BANK_NAME || ' - #' ||CB.BANK_ACCT_NBR, 1, 50)
      INTO pn_device_id, pv_device_serial_cd, pv_bank_country_cd, ln_customer_id, lv_location_name
      FROM CORP.CUSTOMER_BANK CB
      JOIN CORP.COUNTRY CO ON CB.BANK_COUNTRY_CD = CO.COUNTRY_CD
      JOIN CORP.CURRENCY CU ON CO.CURRENCY_ID = CU.CURRENCY_ID
      LEFT OUTER JOIN DEVICE.DEVICE D ON D.DEVICE_SERIAL_CD = 'V2-' || CB.CUSTOMER_BANK_ID || '-' || CU.CURRENCY_CODE
     WHERE CB.CUSTOMER_BANK_ID = pn_customer_bank_id
       AND CB.STATUS = 'A'
       AND CO.ALLOW_BANKS = 'Y';
     
    IF pn_device_id IS NOT NULL THEN
        RETURN;
    END IF;
    
    -- insert device
    PKG_DEVICE_CONFIGURATION.INITIALIZE_DEVICE(pv_device_serial_cd, 14, 'Y',
        pn_device_id,
        lv_device_name,
        lv_device_type_desc,
        ln_device_sub_type_id,
        lc_prev_device_active_yn_flag,
        lc_master_id_always_inc,
        ln_key_gen_time,
        lc_legacy_safe_key,
        lv_time_zone_guid,
        ln_new_host_count,
        ln_result_cd,
        lv_error_message		
    );
    
	IF pn_device_id <= 0 THEN
        RAISE_APPLICATION_ERROR(-20010, lv_error_message);
    END IF;
    
    SELECT MAX(DEALER_ID)
      INTO ln_dealer_id
      FROM (
        SELECT CD.DEALER_ID
        FROM CORP.DEALER D
        JOIN CORP.DEALER_LICENSE DL ON D.DEALER_ID = DL.DEALER_ID
        JOIN CORP.LICENSE L ON DL.LICENSE_ID = L.LICENSE_ID
        LEFT OUTER JOIN CORP.LICENSE_DEVICE_TYPE LDT ON DL.LICENSE_ID = LDT.LICENSE_ID AND LDT.DEVICE_TYPE_ID = 14 AND LDT.DEVICE_SUB_TYPE_ID = ln_device_sub_type_id
        JOIN (SELECT DEALER_ID, 1 PRIORITY, CREATE_DATE CREATED_TS
                FROM CORP.CUSTOMER
               WHERE CUSTOMER_ID = ln_customer_id
              UNION ALL
              SELECT DEALER_ID, 2 PRIORITY, CREATED_TS
                FROM CORP.CUSTOMER_DEALER
               WHERE CUSTOMER_ID = ln_customer_id) CD ON DL.DEALER_ID = CD.DEALER_ID
        WHERE L.STATUS = 'A'
          AND D.STATUS = 'A'
          AND SYSDATE >= NVL(DL.START_DATE, MIN_DATE)
          AND SYSDATE < NVL(DL.END_DATE, MAX_DATE)          
        ORDER BY CASE WHEN LDT.LICENSE_ID IS NOT NULL THEN 1 ELSE 2 END, CD.PRIORITY, CD.CREATED_TS DESC, DL.START_DATE DESC, CD.DEALER_ID DESC
    ) WHERE ROWNUM = 1;
    
    IF ln_dealer_id IS NULL THEN
        SELECT DEALER_ID
        INTO ln_dealer_id
        FROM (
            SELECT DEALER_ID
            FROM CORP.DEALER
            WHERE DEALER_NAME = 'USA Technologies'
            ORDER BY DEALER_ID
        ) WHERE ROWNUM = 1;
    END IF;

    SELECT MAX(ADDRESS1), NVL(MAX(CITY), 'Malvern'), NVL(MAX(STATE), 'PA'), NVL(MAX(ZIP), '19355'), NVL(MAX(COUNTRY_CD), 'US')                                                                                                                                                                     
      INTO lv_customer_address1, lv_customer_city, lv_customer_state_cd, lv_customer_postal, lv_customer_country_cd 
      FROM CORP.CUSTOMER_ADDR
     WHERE CUSTOMER_ID = ln_customer_id and CUSTOMER_BANK_ID=pn_customer_bank_id;
     
    REPORT.GET_OR_CREATE_EPORT(ln_eport_id, pv_device_serial_cd, 14);
    CORP.DEALER_EPORT_UPD(ln_dealer_id, ln_eport_id);
    REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_TERMINAL_MASS(
        0, /* pn_user_id */
        ln_terminal_id, /* pn_terminal_id */
        pv_device_serial_cd, /* pv_device_serial_cd */
        ln_dealer_id, /* pn_dealer_id */
        'TBD', /* pv_asset_nbr */
        'TBD', /* pv_machine_make */
        'To Be Determined', /* pv_machine_model */
        NULL, /* pv_telephone */
        NULL, /* pv_prefix */
        NULL, /* pv_region_name */
        lv_location_name, /* pv_location_name */
        NULL, /* pv_location_details */
        lv_customer_address1, /* pv_address1 */
        lv_customer_city, /* pv_city */
        lv_customer_state_cd, /* pv_state_cd */
        lv_customer_postal, /* pv_postal */
        lv_customer_country_cd, /* pv_country_cd */
        pn_customer_bank_id, /* pn_customer_bank_id */
        1, /* pn_pay_sched_id */
        '- Not Specified -', /* pv_location_type_name */
        NULL, /* pn_location_type_specific */
        '- Not Assigned -', /* pv_product_type_name */
        NULL, /* pn_product_type_specific */
        '?', /* pc_auth_mode */
        0, /* pn_avg_amt */
        0, /* pn_time_zone_id */
        '?', /* pc_dex_data */
        '?', /* pc_receipt */
        0, /* pn_vends */
        NULL, /* pv_term_var_1 */
        NULL, /* pv_term_var_2 */
        NULL, /* pv_term_var_3 */
        NULL, /* pv_term_var_4 */
        NULL, /* pv_term_var_5 */
        NULL, /* pv_term_var_6 */
        NULL, /* pv_term_var_7 */
        NULL, /* pv_term_var_8 */
        TRUNC(SYSDATE - 30), /* pd_activate_date */
        0, /* pn_fee_grace_days */
        'N', /* pc_keep_existing_data */
        'N' /* pc_override_payment_schedule */,
        NULL /* pv_doing_business_as */,
        NULL /* pc_fee_no_trigger_event_flag */,
        NULL /* pv_sales_order_number */,
        NULL /* pv_purchase_order_number */,
        NULL /* pc_pos_environment_cd */,
        NULL /* pv_entry_capability_cds */,
        NULL /* pc_pin_capability_flag */,
        NULL /* pn_commission_bank_id */,
        'ePort Online' /* pv_business_type_name */,
        NULL /* pv_customer_service_phone */,
        NULL /* pv_customer_service_email */  
    );
    
    UPDATE REPORT.TERMINAL
       SET STATUS = 'A'
     WHERE TERMINAL_ID = ln_terminal_id 
       AND STATUS != 'A';
END;
/