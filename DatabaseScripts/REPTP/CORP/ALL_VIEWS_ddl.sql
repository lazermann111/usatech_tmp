-- Start of DDL Script for View CORP.VW_ADJUSTMENTS
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_adjustments (
   doc_id,
   ledger_id,
   amount,
   reason,
   adjust_date,
   terminal_id,
   customer_bank_id,
   batch_closed,
   create_by )
AS
SELECT B.DOC_ID, L.LEDGER_ID, L.AMOUNT, L.DESCRIPTION REASON, L.LEDGER_DATE ADJUST_DATE,
B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.CLOSED, L.CREATE_BY
FROM LEDGER L, BATCH B
WHERE L.ENTRY_TYPE = 'AD'
AND L.DELETED = 'N' AND L.BATCH_ID = B.BATCH_ID
/


-- End of DDL Script for View CORP.VW_ADJUSTMENTS

-- Start of DDL Script for View CORP.VW_APPROVED_PAYMENTS
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_approved_payments (
   eft_id,
   batch_ref_nbr,
   description,
   customer_bank_id,
   customer_name,
   bank_acct_nbr,
   bank_routing_nbr,
   eft_amount )
AS
SELECT D.DOC_ID, D.REF_NBR, D.DESCRIPTION, D.CUSTOMER_BANK_ID, C.CUSTOMER_NAME,
D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, D.TOTAL_AMOUNT FROM DOC D, CUSTOMER C, CUSTOMER_BANK CB
WHERE D.STATUS = 'A' AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND C.CUSTOMER_ID = CB.CUSTOMER_ID
/


-- End of DDL Script for View CORP.VW_APPROVED_PAYMENTS

-- Start of DDL Script for View CORP.VW_CHANGED_CUSTOMER_BANK
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_changed_customer_bank (
   bank_acct_nbr,
   bank_routing_nbr,
   account_title,
   create_date,
   customer_bank_id,
   customer_id,
   status,
   status_text,
   account_type_text,
   bank_name,
   bank_address,
   bank_city,
   bank_state,
   bank_zip,
   contact_name,
   contact_title,
   contact_telephone,
   contact_fax,
   customer_name,
   create_by )
AS
SELECT CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR, CB.ACCOUNT_TITLE, CB.CREATE_DATE, CB.CUSTOMER_BANK_ID, CB.CUSTOMER_ID, CB.STATUS,
(CASE WHEN CB.STATUS = 'P' THEN 'PENDING' WHEN CB.STATUS = 'U' THEN 'UPDATED' WHEN CB.STATUS = 'A' THEN 'ACTIVE' ELSE CB.STATUS END) STATUS_TEXT,
(CASE WHEN CB.ACCOUNT_TYPE = 'C' THEN 'Checking' WHEN CB.ACCOUNT_TYPE = 'S' THEN 'Saving' ELSE 'Unknown' END) ACCOUNT_TYPE_TEXT,
CB.BANK_NAME, CB.BANK_ADDRESS, CB.BANK_CITY, CB.BANK_STATE, CB.BANK_ZIP, CB.CONTACT_NAME, CB.CONTACT_TITLE,
CB.CONTACT_TELEPHONE, CB.CONTACT_FAX, C.CUSTOMER_NAME, CB.CREATE_BY
FROM CUSTOMER_BANK CB, CUSTOMER C WHERE CB.CUSTOMER_ID = C.CUSTOMER_ID
AND CB.STATUS IN ('U', 'P')
/


-- End of DDL Script for View CORP.VW_CHANGED_CUSTOMER_BANK

-- Start of DDL Script for View CORP.VW_CURRENT_BANK_ACCT
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_current_bank_acct (
   terminal_id,
   customer_bank_id )
AS
SELECT TERMINAL_ID, CUSTOMER_BANK_ID FROM CUSTOMER_BANK_TERMINAL CBT
WHERE SYSDATE BETWEEN NVL(START_DATE, SYSDATE) AND NVL(END_DATE, SYSDATE)
/

-- Grants for View
GRANT DELETE ON vw_current_bank_acct TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_current_bank_acct TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_current_bank_acct TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_current_bank_acct TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_CURRENT_BANK_ACCT

-- Start of DDL Script for View CORP.VW_CURRENT_LICENSE
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_current_license (
   license_nbr,
   received,
   customer_id )
AS
SELECT MAX(LICENSE_NBR), RECEIVED, CL.CUSTOMER_ID FROM CUSTOMER_LICENSE CL, 
(SELECT MAX(RECEIVED) MAX_RECEIVED, CUSTOMER_ID FROM CUSTOMER_LICENSE GROUP BY CUSTOMER_ID) M 
WHERE RECEIVED = MAX_RECEIVED AND CL.CUSTOMER_ID = M.CUSTOMER_ID GROUP BY RECEIVED, CL.CUSTOMER_ID
/


-- End of DDL Script for View CORP.VW_CURRENT_LICENSE

-- Start of DDL Script for View CORP.VW_CUSTOMER
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_customer (
   customer_id,
   customer_name,
   user_id,
   sales_term,
   credit_term,
   create_date,
   upd_date,
   create_by,
   upd_by,
   customer_alt_name,
   address_id,
   address_name,
   address1,
   address2,
   city,
   state,
   zip,
   license_nbr,
   status )
AS
SELECT C.CUSTOMER_ID, CUSTOMER_NAME, USER_ID, SALES_TERM, CREDIT_TERM, CREATE_DATE, UPD_DATE, CREATE_BY, UPD_BY, CUSTOMER_ALT_NAME,
ADDRESS_ID, NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP, LICENSE_NBR, C.STATUS
FROM CUSTOMER C, (SELECT CUSTOMER_ID, NAME, ADDRESS_ID, ADDRESS1, ADDRESS2, CITY, STATE, ZIP
FROM CUSTOMER_ADDR WHERE STATUS = 'A' AND ADDR_TYPE = 2) CA, VW_CURRENT_LICENSE CL WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND C.CUSTOMER_ID = CL.CUSTOMER_ID (+)
/


-- End of DDL Script for View CORP.VW_CUSTOMER

-- Start of DDL Script for View CORP.VW_CUSTOMER_BANK_TERMINAL
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_customer_bank_terminal (
   customer_bank_terminal_id,
   customer_bank_id,
   terminal_id,
   start_date,
   end_date )
AS
SELECT MIN(CUSTOMER_BANK_TERMINAL_ID), CUSTOMER_BANK_ID, TERMINAL_ID, MIN(START_DATE) START_DATE, MAX(END_DATE) END_DATE
FROM (SELECT CUSTOMER_BANK_TERMINAL_ID, CUSTOMER_BANK_ID, TERMINAL_ID,
             (CASE WHEN FIRST = 1 THEN START_DATE ELSE NULL END) START_DATE,
             (CASE WHEN LAST = 1 THEN END_DATE ELSE NULL END) END_DATE,
             SUM(FIRST) OVER (PARTITION BY TERMINAL_ID, CUSTOMER_BANK_ID
                 ORDER BY NVL(START_DATE, MIN_DATE) ASC
                 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) SET_NBR
      FROM (SELECT CUSTOMER_BANK_TERMINAL_ID, A.CUSTOMER_BANK_ID, A.TERMINAL_ID, START_DATE, END_DATE,
                   CASE WHEN CUSTOMER_BANK_ID <>
                        NVL(LAG(CUSTOMER_BANK_ID, 1) OVER
                        (PARTITION BY A.TERMINAL_ID ORDER BY NVL(START_DATE, MIN_DATE) ASC), 0)
                        THEN 1 ELSE 0 END FIRST,
                   CASE WHEN CUSTOMER_BANK_ID <>
                        NVL(LEAD(CUSTOMER_BANK_ID, 1) OVER
                        (PARTITION BY A.TERMINAL_ID ORDER BY NVL(START_DATE, MIN_DATE) ASC), 0)
                        THEN 1 ELSE 0 END LAST
            FROM CUSTOMER_BANK_TERMINAL A))
GROUP BY CUSTOMER_BANK_ID, TERMINAL_ID, SET_NBR
/


-- End of DDL Script for View CORP.VW_CUSTOMER_BANK_TERMINAL

-- Start of DDL Script for View CORP.VW_DEALER_LICENSE
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_dealer_license (
   license_id,
   dealer_id )
AS
SELECT MAX(LICENSE_ID), DL.DEALER_ID FROM DEALER_LICENSE DL,
(SELECT MAX(START_DATE) MAX_START_DATE, DEALER_ID FROM DEALER_LICENSE GROUP BY DEALER_ID) M
WHERE NVL(START_DATE, SYSDATE) = NVL(MAX_START_DATE, SYSDATE) AND DL.DEALER_ID = M.DEALER_ID
GROUP BY DL.DEALER_ID
/


-- End of DDL Script for View CORP.VW_DEALER_LICENSE

-- Start of DDL Script for View CORP.VW_EFT_AUTH_INFO
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_eft_auth_info (
   customer_bank_id,
   customer_name,
   address1,
   city,
   state,
   zip,
   tax_id_nbr,
   bank_name,
   bank_address1,
   bank_city,
   bank_state,
   bank_zip,
   account_title,
   account_type,
   bank_acct_nbr,
   bank_routing_nbr,
   contact_name,
   contact_title,
   contact_telephone,
   contact_fax,
   telephone,
   fax )
AS
SELECT CB.CUSTOMER_BANK_ID, CUSTOMER_NAME, ADDRESS1, CITY, STATE, ZIP, TAX_ID_NBR, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE,
BANK_ZIP, ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ACCT_NBR, BANK_ROUTING_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE,
CONTACT_FAX, TELEPHONE, FAX
FROM CUSTOMER C, CUSTOMER_BANK CB, CUSTOMER_ADDR CA, USER_LOGIN U
WHERE C.CUSTOMER_ID = CB.CUSTOMER_ID AND
C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND NVL(CA.ADDR_TYPE, 2) = 2 AND U.USER_ID = C.USER_ID
/

-- Grants for View
GRANT DELETE ON vw_eft_auth_info TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_eft_auth_info TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_eft_auth_info TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_eft_auth_info TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_EFT_AUTH_INFO

-- Start of DDL Script for View CORP.VW_EFT_EXPORT
-- Generated 13-Oct-2004 13:41:36 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_eft_export (
   eft_id,
   customer_bank_id,
   gross_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount,
   adjust_desc,
   net_amount,
   refund_chargeback_amount )
AS
SELECT E.EFT_ID, CUSTOMER_BANK_ID, SUM(GROSS_AMOUNT), SUM(PROCESS_FEE_AMOUNT), SUM(SERVICE_FEE_AMOUNT), SUM(ADJUST_AMOUNT), GET_ADJUST_DESC(E.EFT_ID), SUM(NET_AMOUNT), NVL(SUM(REFUND_AMOUNT),0) + NVL(SUM(CHARGEBACK_AMOUNT),0)
FROM EFT E, PAYMENTS P WHERE E.EFT_ID = P.EFT_ID
GROUP BY E.EFT_ID, CUSTOMER_BANK_ID
/

-- Grants for View
GRANT DELETE ON vw_eft_export TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_eft_export TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_eft_export TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_eft_export TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_EFT_EXPORT

-- Start of DDL Script for View CORP.VW_LICENSE
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_license (
   license_id,
   title,
   description,
   credit_fee,
   debit_fee,
   pass_fee,
   access_fee,
   maint_fee,
   cash_fee,
   refund_fee,
   chargeback_fee,
   service_fee )
AS
SELECT LICENSE_ID, TITLE, DESCRIPTION,
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF1 WHERE LPF1.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 16),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF2 WHERE LPF2.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 19),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF3 WHERE LPF3.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 18),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF4 WHERE LPF4.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 17),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF5 WHERE LPF5.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 23),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF6 WHERE LPF6.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 22),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF7 WHERE LPF7.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 20),
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF8 WHERE LPF8.LICENSE_ID = L.LICENSE_ID AND TRANS_TYPE_ID = 21),
(SELECT AMOUNT FROM LICENSE_SERVICE_FEES LSF1 WHERE LSF1.LICENSE_ID = L.LICENSE_ID AND FEE_ID = 1 AND FREQUENCY_ID = 2)
FROM LICENSE L
/


-- End of DDL Script for View CORP.VW_LICENSE

-- Start of DDL Script for View CORP.VW_LICENSE_INFO
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_license_info (
   customer_id,
   customer_name,
   address1,
   city,
   state,
   zip,
   license_nbr,
   telephone,
   fax,
   dealer_name,
   credit_process_fee,
   monthly_service_fee )
AS
SELECT C.CUSTOMER_ID, CUSTOMER_NAME, ADDRESS1, CITY, STATE, ZIP, LICENSE_NBR, TELEPHONE, FAX, DEALER_NAME, 
(SELECT FEE_PERCENT FROM LICENSE_PROCESS_FEES LPF WHERE L.LICENSE_ID = LPF.LICENSE_ID AND LPF.TRANS_TYPE_ID = 16), 
(SELECT AMOUNT FROM LICENSE_SERVICE_FEES LSF WHERE L.LICENSE_ID = LSF.LICENSE_ID AND FREQUENCY_ID = 2 AND FEE_ID = 1) 
FROM CUSTOMER C, CUSTOMER_ADDR CA, USER_LOGIN U, DEALER D, LICENSE_NBR L 
WHERE C.CUSTOMER_ID = CA.CUSTOMER_ID (+) AND NVL(CA.ADDR_TYPE, 2) = 2 AND U.USER_ID = C.USER_ID 
AND C.DEALER_ID = D.DEALER_ID (+) AND C.CUSTOMER_ID = L.CUSTOMER_ID (+)
/

-- Grants for View
GRANT DELETE ON vw_license_info TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_license_info TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_license_info TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_license_info TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_LICENSE_INFO

-- Start of DDL Script for View CORP.VW_LOCATION
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_location (
   location_id,
   location_name,
   location_description,
   address1,
   city,
   state,
   zip )
AS
SELECT L.LOCATION_ID, L.LOCATION_NAME, L.DESCRIPTION, A.ADDRESS1, A.CITY, A.STATE, A.ZIP
       FROM CUSTOMER_ADDR A, LOCATION L
       WHERE L.ADDRESS_ID =  A.ADDRESS_ID
/


-- End of DDL Script for View CORP.VW_LOCATION

-- Start of DDL Script for View CORP.VW_LOCKED_PAYMENTS
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_locked_payments (
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   gross_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   eft_id,
   batch_ref_nbr,
   description,
   adjust_amount )
AS
SELECT D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, A.CREDIT_AMOUNT,
A.REFUND_AMOUNT, A.CHARGEBACK_AMOUNT, A.PROCESS_FEE_AMOUNT, A.SERVICE_FEE_AMOUNT,
 D.DOC_ID, D.REF_NBR, D.DESCRIPTION, A.ADJUST_AMOUNT
FROM DOC D, (
    SELECT DOC_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT D.DOC_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND L.DELETED = 'N'
          AND D.STATUS = 'L'
          GROUP BY D.DOC_ID, L.ENTRY_TYPE) M
      GROUP BY DOC_ID) A
WHERE D.DOC_ID = A.DOC_ID
/


-- End of DDL Script for View CORP.VW_LOCKED_PAYMENTS

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_macgray_eft_detail (
   asset_nbr,
   location_name,
   terminal_nbr,
   bank_acct_nbr,
   gross_amount,
   terminal_fee,
   transaction_fee,
   adjustment,
   explanation,
   last_tran_dt,
   eft_id,
   eft_date )
AS
select    
	   vw.asset_nbr as asset_nbr,   
	   vw.location_name as location_name,   
	   vw.terminal_nbr as terminal_nbr,   
	   cb.BANK_ACCT_NBR as bank_acct_nbr,   
	   pay.GROSS_AMOUNT as gross_amount,   
	   sf.FEE_AMOUNT as terminal_fee,    
	   pf.FEE_PERCENT * pay.GROSS_AMOUNT as transaction_fee,   
	   pa.AMOUNT as adjustment,   
	   pa.REASON as explanation,   
	   last_tran_date(eft.eft_id, cbt.TERMINAL_ID) as last_tran_dt,    
	   eft.BATCH_REF_NBR as eft_id,   
	   eft.EFT_DATE   
from    
	   customer_bank_terminal cbt,   
	   customer_bank cb,   
	   payments  pay,   
	   service_fees  sf,    
	   payment_process_fee  pf,   
	   payment_adjustment  pa,    
	   eft,   
	   vw_terminal  vw   
where   
	  cbt.terminal_id = vw.terminal_id and   
	  cbt.customer_bank_id = cb.customer_bank_id and   
	  /* this is currently hardcoded but should be parameterized */   
	  cb.customer_id = 57 and   
	  /***********************************************************/   
	  vw.terminal_id = pay.terminal_id and   
	  eft.eft_id = pay.eft_id and    
	  pa.eft_id (+)= eft.eft_id and   
	  pf.payment_id (+)= pay.payment_id and   
	  sf.terminal_id = vw.terminal_id 
order by vw.asset_nbr
/


-- End of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL_OLD
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_macgray_eft_detail_old (
   asset_nbr,
   location_name,
   terminal_nbr,
   bank_acct_nbr,
   gross_amount,
   terminal_fee,
   transaction_fee,
   adjustment,
   explanation,
   last_tran_dt,
   eft_id,
   eft_date )
AS
select    
	   vw.asset_nbr as asset_nbr,   
	   vw.location_name as location_name,   
	   vw.terminal_nbr as terminal_nbr,   
	   cb.BANK_ACCT_NBR as bank_acct_nbr,   
	   pay.GROSS_AMOUNT as gross_amount,   
	   sf.FEE_AMOUNT as terminal_fee,    
	   pf.FEE_PERCENT * pay.GROSS_AMOUNT as transaction_fee,   
	   pa.AMOUNT as adjustment,   
	   pa.REASON as explanation,   
	   last_tran_date(eft.eft_id, cbt.TERMINAL_ID) as last_tran_dt,    
	   eft.BATCH_REF_NBR as eft_id,   
	   eft.EFT_DATE   
from    
	   customer_bank_terminal cbt,   
	   customer_bank cb,   
	   payments  pay,   
	   service_fees  sf,    
	   payment_process_fee  pf,   
	   payment_adjustment  pa,    
	   eft,   
	   vw_terminal  vw   
where   
	  cbt.terminal_id = vw.terminal_id and   
	  cbt.customer_bank_id = cb.customer_bank_id and   
	  /* this is currently hardcoded but should be parameterized */   
	  cb.customer_id = 57 and   
	  /***********************************************************/   
	  vw.terminal_id = pay.terminal_id and   
	  eft.eft_id = pay.eft_id and    
	  pa.eft_id (+)= eft.eft_id and   
	  pf.payment_id (+)= pay.payment_id and   
	  sf.terminal_id = vw.terminal_id	and   
	  eft.eft_date between to_date('8/1/2002','mm/dd/yyyy') and to_date('8/31/2002','mm/dd/yyyy')   
order by vw.asset_nbr
/


-- End of DDL Script for View CORP.VW_MACGRAY_EFT_DETAIL_OLD

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_DETAIL
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_macgray_eft_monthly_detail (
   bank_acct_nbr,
   gross_amount,
   terminal_fee,
   transaction_fee,
   last_tran_dt,
   location_name,
   eft_id,
   adjustment,
   explanation,
   eft_date )
AS
SELECT	trim(bank_acct_nbr), Gross_Amount, 
		Terminal_Fee, Transaction_Fee,
		Last_Tran_Dt, location_name,
		eft_ID, Adjustment, Explanation, eft_date
FROM    vw_MacGray_EFT_detail
/

-- Grants for View
GRANT DELETE ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_macgray_eft_monthly_detail TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_DETAIL

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUM_SUB
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_macgray_eft_monthly_sum_sub (
   tagid,
   grossamt,
   termfee,
   tranfee,
   lasttrandt,
   location,
   eft_id,
   eft_date )
AS
SELECT     asset_nbr as TagID, 
		   SUM(Gross_Amount) AS GrossAmt, 
		   MAX(Terminal_Fee) AS TermFee, 
		   SUM(Transaction_Fee) AS TranFee, 
		   MAX(Last_Tran_Dt) AS LastTranDt, 
		   location_name as location, 
           eft_ID,
		   eft_date
FROM         vw_MacGray_EFT_detail 
GROUP BY asset_nbr, location_Name, eft_ID, eft_date
/


-- End of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUM_SUB

-- Start of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUMMARY
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_macgray_eft_monthly_summary (
   tagid,
   grossamt,
   termfee,
   tranfee,
   lasttrandt,
   name,
   eft_id,
   eft_date )
AS
SELECT     TagID, 
		   GrossAmt, 
		   TermFee, 
		   TranFee, 
		   LastTranDt, 
		   location, 
           eft_ID,
		   eft_date
FROM       VW_MACGRAY_EFT_MONTHLY_SUM_sub
/

-- Grants for View
GRANT DELETE ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_macgray_eft_monthly_summary TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_MACGRAY_EFT_MONTHLY_SUMMARY

-- Start of DDL Script for View CORP.VW_NO_LICENSE
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_no_license (
   customer_id,
   customer_name )
AS
SELECT CUSTOMER_ID, CUSTOMER_NAME FROM CUSTOMER
WHERE CUSTOMER_ID NOT IN(SELECT CUSTOMER_ID FROM VW_CURRENT_LICENSE)
/


-- End of DDL Script for View CORP.VW_NO_LICENSE

-- Start of DDL Script for View CORP.VW_PAID_DATE
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_paid_date (
   ledger_id,
   eft_date )
AS
SELECT L.LEDGER_ID, (CASE WHEN D.STATUS = 'D' THEN L.LEDGER_DATE ELSE D.SENT_DATE END)
FROM LEDGER L, BATCH B, DOC D
WHERE L.BATCH_ID = B.BATCH_ID AND B.DOC_ID = D.DOC_ID (+)
AND NVL(D.STATUS, 'N') IN ('P', 'S', 'D', 'N')
AND L.ENTRY_TYPE IN('CC', 'RF', 'CB')
/


-- End of DDL Script for View CORP.VW_PAID_DATE

-- Start of DDL Script for View CORP.VW_PAID_EFTS
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_paid_efts (
   eft_id,
   eft_date,
   batch_ref_nbr,
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   eft_amount,
   bank_name,
   customer_name )
AS
SELECT D.DOC_ID, D.SENT_DATE, D.REF_NBR, D.CUSTOMER_BANK_ID, D.BANK_ACCT_NBR, D.BANK_ROUTING_NBR, D.TOTAL_AMOUNT,
CB.BANK_NAME, C.CUSTOMER_NAME
FROM DOC D, CUSTOMER_BANK CB, CUSTOMER C
WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID (+) AND CB.CUSTOMER_ID = C.CUSTOMER_ID (+) AND D.STATUS IN('P', 'S')
AND D.DOC_TYPE = 'FT'
/

-- Grants for View
GRANT DELETE ON vw_paid_efts TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_paid_efts TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_paid_efts TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_paid_efts TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_PAID_EFTS

-- Start of DDL Script for View CORP.VW_PAYMENT_PROCESS_FEES
-- Generated 13-Oct-2004 13:41:37 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_payment_process_fees (
   doc_id,
   batch_id,
   batch_closed,
   customer_bank_id,
   terminal_id,
   trans_type_id,
   trans_type_name,
   gross_amount,
   fee_percent,
   fixed_fee_amount,
   process_fee_amount,
   net_amount )
AS
SELECT B.DOC_ID, B.BATCH_ID, B.CLOSED, B.CUSTOMER_BANK_ID, B.TERMINAL_ID, T.TRANS_TYPE_ID,
TT.TRANS_TYPE_NAME, SUM(L1.AMOUNT) GROSS_AMOUNT, PF.FEE_PERCENT, PF.FEE_AMOUNT FIXED_FEE_AMOUNT, SUM(L.AMOUNT) PROCESS_FEE_AMOUNT,
SUM(NVL(L1.AMOUNT, 0) + L.AMOUNT) NET_AMOUNT
FROM BATCH B, LEDGER L, LEDGER L1, PROCESS_FEES PF, TRANS T, TRANS_TYPE TT
WHERE B.BATCH_ID = L.BATCH_ID
AND L.TRANS_ID = T.TRAN_ID
AND T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
AND L.PROCESS_FEE_ID = PF.PROCESS_FEE_ID
AND L.ENTRY_TYPE = 'PF'
AND L.TRANS_ID = L1.TRANS_ID (+)
AND NVL(L1.ENTRY_TYPE, '') IN('CC', 'RF', 'CB', '')
GROUP BY B.DOC_ID, B.BATCH_ID, B.CLOSED, B.CUSTOMER_BANK_ID, B.TERMINAL_ID, T.TRANS_TYPE_ID,
TT.TRANS_TYPE_NAME, PF.FEE_PERCENT, PF.FEE_AMOUNT
/


-- End of DDL Script for View CORP.VW_PAYMENT_PROCESS_FEES

-- Start of DDL Script for View CORP.VW_PAYMENT_REPORT
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_payment_report (
   payment_id,
   eft_id,
   terminal_id,
   customer_bank_id,
   gross_amount,
   settle_failed_amount,
   tax_amount,
   process_fee_amount,
   service_fee_amount,
   refund_chargeback_amount,
   adjust_amount,
   net_amount,
   terminal_nbr,
   location_name,
   transactions,
   payment_start_date,
   payment_end_date,
   reason )
AS
SELECT V.BATCH_ID, V.DOC_ID, V.TERMINAL_ID, V.CUSTOMER_BANK_ID, V.CREDIT_AMOUNT, SETTLE_FAILED_AMOUNT, NULL, V.PROCESS_FEE_AMOUNT, V.SERVICE_FEE_AMOUNT,
CASE WHEN V.REFUND_AMOUNT IS NULL AND V.CHARGEBACK_AMOUNT IS NULL THEN NULL ELSE NVL(V.REFUND_AMOUNT, 0) + NVL(V.CHARGEBACK_AMOUNT, 0) END,
V.ADJUST_AMOUNT,
(NVL(CREDIT_AMOUNT, 0) + NVL(REFUND_AMOUNT, 0) + NVL(CHARGEBACK_AMOUNT, 0) + NVL(PROCESS_FEE_AMOUNT, 0) + NVL(SERVICE_FEE_AMOUNT, 0) + NVL(ADJUST_AMOUNT, 0)),
TERMINAL_NBR, LOCATION_NAME, V.TRAN_COUNT, V.START_DATE, V.END_DATE, PS.DESCRIPTION
FROM TERMINAL T, LOCATION L, PAYMENT_SCHEDULE PS, (
    SELECT DOC_ID, BATCH_ID, TERMINAL_ID, CUSTOMER_BANK_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' AND PAYABLE = 'N' THEN AMOUNT ELSE NULL END) SETTLE_FAILED_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' AND PAYABLE = 'Y' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT,
           SUM(ENTRY_COUNT) TRAN_COUNT
      FROM (
        SELECT D.DOC_ID, B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.PAYMENT_SCHEDULE_ID,
             B.START_DATE, B.END_DATE, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT,
             COUNT(*) ENTRY_COUNT, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) PAYABLE
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND B.DOC_ID = D.DOC_ID
          AND L.DELETED = 'N'
          GROUP BY D.DOC_ID, B.BATCH_ID, B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.PAYMENT_SCHEDULE_ID,
             B.START_DATE, B.END_DATE, L.ENTRY_TYPE, PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE)) M
      GROUP BY DOC_ID, BATCH_ID, TERMINAL_ID, CUSTOMER_BANK_ID, PAYMENT_SCHEDULE_ID,
             START_DATE, END_DATE
      ) V
WHERE V.TERMINAL_ID = T.TERMINAL_ID (+) AND T.LOCATION_ID = L.LOCATION_ID (+)
AND V.PAYMENT_SCHEDULE_ID = PS.PAYMENT_SCHEDULE_ID
/

-- Grants for View
GRANT DELETE ON vw_payment_report TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_payment_report TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_payment_report TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_payment_report TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_PAYMENT_REPORT

-- Start of DDL Script for View CORP.VW_PAYMENT_SERVICE_FEES
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_payment_service_fees (
   doc_id,
   batch_id,
   ledger_id,
   fee_amount,
   fee_name,
   fee_date,
   terminal_id,
   customer_bank_id,
   batch_closed,
   fee_id,
   frequency_name )
AS
SELECT B.DOC_ID, B.BATCH_ID, L.LEDGER_ID, L.AMOUNT FEE_AMOUNT, FEE_NAME, L.LEDGER_DATE FEE_DATE,
B.TERMINAL_ID, B.CUSTOMER_BANK_ID, B.CLOSED, SF.FEE_ID, Q.NAME
FROM LEDGER L, FEES F, SERVICE_FEES SF, BATCH B, FREQUENCY Q
WHERE L.ENTRY_TYPE = 'SF' AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID AND SF.FEE_ID = F.FEE_ID
AND L.DELETED = 'N' AND L.BATCH_ID = B.BATCH_ID AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
/

-- Grants for View
GRANT DELETE ON vw_payment_service_fees TO report
WITH GRANT OPTION
/
GRANT INSERT ON vw_payment_service_fees TO report
WITH GRANT OPTION
/
GRANT SELECT ON vw_payment_service_fees TO report
WITH GRANT OPTION
/
GRANT UPDATE ON vw_payment_service_fees TO report
WITH GRANT OPTION
/

-- End of DDL Script for View CORP.VW_PAYMENT_SERVICE_FEES

-- Start of DDL Script for View CORP.VW_PENDING_ADJUSTMENTS
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_pending_adjustments (
   customer_bank_id,
   adjust_amount )
AS
SELECT CUSTOMER_BANK_ID, SUM(AMOUNT)
FROM VW_ADJUSTMENTS
WHERE BATCH_CLOSED = 'N'
GROUP BY CUSTOMER_BANK_ID
/


-- End of DDL Script for View CORP.VW_PENDING_ADJUSTMENTS

-- Start of DDL Script for View CORP.VW_PENDING_OR_LOCKED_PAYMENTS
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_pending_or_locked_payments (
   customer_name,
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   pay_min_amount,
   gross_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   eft_id,
   status,
   batch_ref_nbr,
   description,
   net_amount,
   adjust_amount )
AS
SELECT C.CUSTOMER_NAME, P.CUSTOMER_BANK_ID,
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, P.PAY_MIN_AMOUNT, P.GROSS_AMOUNT,
P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.SERVICE_FEE_AMOUNT,  0, '', '' , '' ,
NVL(P.GROSS_AMOUNT, 0) + NVL(P.REFUND_AMOUNT, 0) + NVL(P.CHARGEBACK_AMOUNT, 0)
 + NVL(P.PROCESS_FEE_AMOUNT, 0) + NVL(P.SERVICE_FEE_AMOUNT, 0) + NVL(P.ADJUST_AMOUNT, 0),
  P.ADJUST_AMOUNT--, REASON
FROM VW_PENDING_PAYMENTS P, CUSTOMER C 
WHERE P.CUSTOMER_ID = C.CUSTOMER_ID
UNION SELECT C.CUSTOMER_NAME, P.CUSTOMER_BANK_ID,
P.BANK_ACCT_NBR, P.BANK_ROUTING_NBR, CB.PAY_MIN_AMOUNT, P.GROSS_AMOUNT,
P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.SERVICE_FEE_AMOUNT,
P.EFT_ID, 'Locked', P.BATCH_REF_NBR, P.DESCRIPTION,
NVL(P.GROSS_AMOUNT, 0) + NVL(P.REFUND_AMOUNT, 0) + NVL(P.CHARGEBACK_AMOUNT, 0) + NVL(P.PROCESS_FEE_AMOUNT, 0) + NVL(P.SERVICE_FEE_AMOUNT, 0) + NVL(P.ADJUST_AMOUNT, 0),
P.ADJUST_AMOUNT--, REASON
FROM VW_LOCKED_PAYMENTS P, CUSTOMER_BANK CB, CUSTOMER C 
WHERE P.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND CB.CUSTOMER_ID = C.CUSTOMER_ID
/


-- End of DDL Script for View CORP.VW_PENDING_OR_LOCKED_PAYMENTS

-- Start of DDL Script for View CORP.VW_PENDING_PAYMENTS
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_pending_payments (
   customer_bank_id,
   customer_id,
   bank_acct_nbr,
   bank_routing_nbr,
   pay_min_amount,
   gross_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount )
AS
SELECT CB.CUSTOMER_BANK_ID, CB.CUSTOMER_ID, CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR, CB.PAY_MIN_AMOUNT, A.CREDIT_AMOUNT,
   A.REFUND_AMOUNT, A.CHARGEBACK_AMOUNT, A.PROCESS_FEE_AMOUNT,
   A.SERVICE_FEE_AMOUNT, A.ADJUST_AMOUNT
FROM CUSTOMER_BANK CB, VW_PENDING_REVENUE A
WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID
/*AND (CB.STATUS = 'A' OR NVL(A.CREDIT_AMOUNT, 0) <> 0 OR NVL(A.REFUND_AMOUNT, 0) <> 0
     OR NVL(A.CHARGEBACK_AMOUNT, 0) <> 0 OR NVL(A.PROCESS_FEE_AMOUNT, 0) <> 0
     OR NVL(A.SERVICE_FEE_AMOUNT, 0) <> 0 OR NVL(A.ADJUST_AMOUNT, 0) <> 0)*/
/


-- End of DDL Script for View CORP.VW_PENDING_PAYMENTS

-- Start of DDL Script for View CORP.VW_PENDING_REVENUE
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_pending_revenue (
   customer_bank_id,
   credit_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount )
AS
SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B
          WHERE L.BATCH_ID = B.BATCH_ID
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND L.DELETED = 'N'
          AND B.CLOSED = 'N'
          AND PAYMENTS_PKG.BATCH_CLOSABLE(B.BATCH_ID) = 'Y'
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
      GROUP BY CUSTOMER_BANK_ID
/


-- End of DDL Script for View CORP.VW_PENDING_REVENUE

-- Start of DDL Script for View CORP.VW_PENDING_SERVICE_FEES
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_pending_service_fees (
   customer_bank_id,
   service_fee_amount )
AS
SELECT B.CUSTOMER_BANK_ID, SUM(L.AMOUNT)
FROM LEDGER L, BATCH B
WHERE L.BATCH_ID = B.BATCH_ID
AND B.CLOSED = 'N'
AND L.DELETED = 'N'
GROUP BY B.CUSTOMER_BANK_ID
/


-- End of DDL Script for View CORP.VW_PENDING_SERVICE_FEES

-- Start of DDL Script for View CORP.VW_UNPAID_TRANS_AND_FEES
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_unpaid_trans_and_fees (
   customer_name,
   customer_id,
   customer_bank_id,
   bank_acct_nbr,
   bank_routing_nbr,
   pay_min_amount,
   credit_amount,
   refund_amount,
   chargeback_amount,
   process_fee_amount,
   service_fee_amount,
   adjust_amount )
AS
SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
    SELECT CUSTOMER_BANK_ID,
           SUM(CASE WHEN ENTRY_TYPE = 'CC' THEN AMOUNT ELSE NULL END) CREDIT_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'RF' THEN AMOUNT ELSE NULL END) REFUND_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'CB' THEN AMOUNT ELSE NULL END) CHARGEBACK_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'PF' THEN AMOUNT ELSE NULL END) PROCESS_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'SF' THEN AMOUNT ELSE NULL END) SERVICE_FEE_AMOUNT,
           SUM(CASE WHEN ENTRY_TYPE = 'AD' THEN AMOUNT ELSE NULL END) ADJUST_AMOUNT
      FROM (
        SELECT B.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
          FROM LEDGER L, BATCH B, DOC D
          WHERE L.BATCH_ID = B.BATCH_ID
          AND L.DELETED = 'N'
          AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
          AND B.DOC_ID = D.DOC_ID (+)
          AND NVL(D.SENT_DATE, MIN_DATE) < SYSDATE
          AND L.LEDGER_DATE < SYSDATE
          GROUP BY B.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
      GROUP BY CUSTOMER_BANK_ID) A
WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
AND CB.CUSTOMER_ID = C.CUSTOMER_ID
ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR
/


-- End of DDL Script for View CORP.VW_UNPAID_TRANS_AND_FEES

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_unsent_efts (
   eft_id,
   transaction_code,
   dfi,
   check_digit,
   bank_acct_nbr,
   amount,
   trace_number,
   description,
   company_id_nbr,
   company_name )
AS
SELECT D.DOC_ID,
(CASE WHEN D.TOTAL_AMOUNT > 0 AND CB.ACCOUNT_TYPE = 'C' THEN 22 WHEN D.TOTAL_AMOUNT < 0 AND CB.ACCOUNT_TYPE = 'C' THEN 27
WHEN D.TOTAL_AMOUNT > 0 AND CB.ACCOUNT_TYPE = 'S' THEN 32 WHEN D.TOTAL_AMOUNT < 0 AND CB.ACCOUNT_TYPE = 'S' THEN 37 END)
/*22=DEPOSIT TO CHECKING, 27=WITHDRAWL FROM CHECKING, 32=DEPOSIT TO SAVINGS, 37=WITHDRAWL FROM SAVINGS*/,      
SUBSTR(TO_CHAR(D.BANK_ROUTING_NBR, 'FM000000000'),1,8),
SUBSTR(TO_CHAR(D.BANK_ROUTING_NBR, 'FM000000000'),9,1),
D.BANK_ACCT_NBR, ABS(D.TOTAL_AMOUNT),
'03100005'/*OUR ROUTING NUMBER*/ || TO_CHAR(D.DOC_ID, 'FM000000'),
D.DESCRIPTION, D.CUSTOMER_BANK_ID, C.CUSTOMER_NAME
FROM DOC D, CUSTOMER_BANK CB, CUSTOMER C			
WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND D.STATUS = 'A' AND CB.CUSTOMER_ID = C.CUSTOMER_ID
AND D.TOTAL_AMOUNT <> 0 AND CB.ACCOUNT_TYPE IN('S','C')
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS_TEST
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_unsent_efts_test (
   eft_id,
   transaction_code,
   dfi,
   check_digit,
   bank_acct_nbr,
   amount,
   trace_number,
   description,
   company_id_nbr,
   company_name )
AS
SELECT EFT_ID,        
(CASE WHEN ACCOUNT_TYPE = 'C' THEN 22 WHEN ACCOUNT_TYPE = 'C' THEN 27        
--WHEN EFT_AMOUNT >= 0 AND ACCOUNT_TYPE = 'S' THEN 32 WHEN EFT_AMOUNT < 0 AND ACCOUNT_TYPE = 'S' THEN 37  
END)       
/*22=DEPOSIT TO CHECKING, 27=WITHDRAWL FROM CHECKING, 32=DEPOSIT TO SAVINGS, 37=WITHDRAWL FROM SAVINGS*/,       
SUBSTR(TO_CHAR(CB.BANK_ROUTING_NBR, 'FM000000000'),1,8),       
SUBSTR(TO_CHAR(CB.BANK_ROUTING_NBR, 'FM000000000'),9,1),       
CB.BANK_ACCT_NBR, 0,       
'03100005'/*OUR ROUTING NUMBER*/ || TO_CHAR(EFT_ID, 'FM000000'),       
E.DESCRIPTION, CB.CUSTOMER_BANK_ID, CUSTOMER_NAME       
FROM EFT E, CUSTOMER_BANK CB, CUSTOMER C			       
WHERE E.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID AND E.STATUS = 'S' AND CB.CUSTOMER_ID = C.CUSTOMER_ID     
AND ACCOUNT_TYPE IN('S','C')  
AND CB.CUSTOMER_BANK_ID IN(33,25) AND EFT_ID > 180
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS_TEST

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_unsent_efts_totals (
   entry_count,
   entry_hash,
   total_debit,
   total_credit,
   block_count )
AS
SELECT COUNT(EFT_ID) * 2, SUM(TO_NUMBER(DFI)),        
NVL(SUM(CASE WHEN TRANSACTION_CODE = 27 OR TRANSACTION_CODE = 37 THEN AMOUNT ELSE NULL END), 0),       
NVL(SUM(CASE WHEN TRANSACTION_CODE = 22 OR TRANSACTION_CODE = 32 THEN AMOUNT ELSE NULL END), 0),      
CEIL((4 + (2 * COUNT(EFT_ID))) / 10)        
FROM VW_UNSENT_EFTS
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS

-- Start of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS_TEST
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_unsent_efts_totals_test (
   entry_count,
   entry_hash,
   total_debit,
   total_credit,
   block_count )
AS
SELECT COUNT(EFT_ID) * 2, SUM(TO_NUMBER(DFI)),         
NVL(SUM(CASE WHEN TRANSACTION_CODE = 27 OR TRANSACTION_CODE = 37 THEN AMOUNT ELSE NULL END), 0),        
NVL(SUM(CASE WHEN TRANSACTION_CODE = 22 OR TRANSACTION_CODE = 32 THEN AMOUNT ELSE NULL END), 0),       
CEIL((4 + (2 * COUNT(EFT_ID))) / 10)         
FROM VW_UNSENT_EFTS_TEST
/


-- End of DDL Script for View CORP.VW_UNSENT_EFTS_TOTALS_TEST

-- Start of DDL Script for View CORP.VW_USER
-- Generated 13-Oct-2004 13:41:38 from CORP@USADBD02

CREATE OR REPLACE VIEW vw_user (
   user_id,
   customer_id,
   user_name,
   first_name,
   last_name,
   password,
   email,
   is_primary,
   telephone,
   fax )
AS
SELECT USER_ID, CUSTOMER_ID, USER_NAME, FIRST_NAME, LAST_NAME, USER_PASSWD, EMAIL,
NVL((SELECT 'Y' FROM CUSTOMER C WHERE C.USER_ID = U.USER_ID AND C.CUSTOMER_ID = U.CUSTOMER_ID), 'N'), TELEPHONE, FAX
FROM USER_LOGIN U WHERE STATUS = 'A'
/


-- End of DDL Script for View CORP.VW_USER

