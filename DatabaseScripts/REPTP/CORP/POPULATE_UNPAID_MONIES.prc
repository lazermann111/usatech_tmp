CREATE OR REPLACE PROCEDURE CORP.POPULATE_UNPAID_MONIES 
   (inDate IN DATE)
   IS
--
-- Purpose: This will populate the unpaid_monies_rpt table with
--          with data for a given date
--
-- MODIFICATION HISTORY
-- Person      Date        Comments
-- ---------   ---------   ------------------------------------------
-- J Bradley   09-MAR-05   Inital version
-- J Bradley   15-MAR-05   Added filter to not populate data for Orphan Transactions (faux) customer
-- J Bradley   16-MAR-05   Altered filter for Orphan Transactions (faux) customers (there are now more than one)
-- J Bradley   20-DEC-05   Altered linkage to customer_bank_id from batch table to doc table.
-- J Bradley   22-DEC-05   Corrected bug from previous alteration.
-- J Bradley   28-DEC-05   Corrected bug from 20-DEC-05 (UNION ALL)
-- J Bradley   28-DEC-05   Corrected bug from 20-DEC-05 removed UNION ALL (corrected with unique id) and fixed issues with service_fee_amount and adjust_amount.
-- J Bradley   12-JUN-06   Completely altered with new methodology


    tDate DATE;

BEGIN
    -- ensure tDate is truncated to a day (i.e. time = 00:00:00)
    tDate := TRUNC(inDate);

    -- truncate the scratch table
	EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';
	
	-- Insert unpaid legder ids
    INSERT INTO corp.scr_unpaid_monies
    SELECT upl.ledger_id
    FROM (
        SELECT ldg.ledger_id
        FROM corp.doc eft, corp.batch bat, corp.ledger ldg
        WHERE bat.batch_id = ldg.batch_id
        AND bat.doc_id = eft.doc_id
        AND ldg.settle_state_id IN (2,3,6)
        AND ldg.deleted = 'N'
        AND eft.status != 'D'
    	AND bat.create_date < tDate
    	AND ldg.paid_date IS NULL
        UNION
        SELECT ldg.ledger_id
        FROM corp.doc eft, corp.batch bat, corp.ledger ldg
        WHERE bat.batch_id = ldg.batch_id
        AND bat.doc_id = eft.doc_id
        AND ldg.settle_state_id IN (2,3,6)
        AND ldg.deleted = 'N'
        AND eft.status != 'D'
    	AND bat.create_date < tDate
    	AND ldg.paid_date >= tDate
        AND ldg.paid_date IS NULL
    ) upl;
	
	
	-- Insert the the calulated data into the UNPAID_MONIES_RPT table
	INSERT INTO unpaid_monies_rpt (
        customer_id,
        customer_bank_id,
        business_unit_id,
        credit_amount,
        refund_amount,
        chargeback_amount,
        process_fee_amount,
        service_fee_amount,
        adjust_amount,
        net_payment_amount,
        unpaid_ts
    )		
	SELECT
    cus.customer_id,
    cbk.customer_bank_id,
    bu.business_unit_id,
    SUM(CASE WHEN led.entry_type = 'CC' THEN led.amount ELSE 0 END) credit_amount,
    SUM(CASE WHEN led.entry_type = 'RF' THEN led.amount ELSE 0 END) refund_amount,
    SUM(CASE WHEN led.entry_type = 'CB' THEN led.amount ELSE 0 END) chargeback_amount,
    SUM(CASE WHEN led.entry_type = 'PF' THEN led.amount ELSE 0 END) process_fee_amount,
    SUM(CASE WHEN led.entry_type = 'SF' THEN led.amount ELSE 0 END) service_fee_amount,
    SUM(CASE WHEN led.entry_type = 'AD' THEN led.amount ELSE 0 END) adjust_amount,
    SUM(led.amount) net_payment_amount,
    tDate unpaid_ts
    FROM corp.scr_unpaid_monies upm, corp.ledger led, 
    corp.batch bat, corp.doc eft, corp.business_unit bu,
    corp.customer_bank cbk, corp.customer cus
    WHERE led.ledger_id = upm.ledger_id
    AND bat.batch_id = led.batch_id
    AND eft.doc_id = bat.doc_id
    AND bu.business_unit_id = eft.business_unit_id
    AND cbk.customer_bank_id = eft.customer_bank_id
    AND cus.customer_id = cbk.customer_id
    AND cbk.customer_id NOT IN (5, 6, 1951, 1959, 1960, 1968)
    GROUP BY cus.customer_id, cbk.customer_bank_id, bu.business_unit_id;

    -- truncate the scratch table
    EXECUTE IMMEDIATE 'TRUNCATE TABLE scr_unpaid_monies';

END; -- Procedure
/
