-- Start of DDL Script for Table CORP.BATCH
-- Generated 7-Oct-2004 9:23:58 from CORP@USADBD02

-- Drop the old instance of BATCH
DROP TABLE batch
/

CREATE TABLE batch
    (batch_id                       NUMBER(*,0) NOT NULL,
    customer_bank_id               NUMBER(*,0) NOT NULL,
    doc_id                         NUMBER(*,0),
    terminal_id                    NUMBER(*,0),
    payment_schedule_id            NUMBER(*,0) NOT NULL,
    start_date                     DATE NOT NULL,
    end_date                       DATE,
    create_date                    DATE DEFAULT SYSDATE

  NOT NULL,
    closed                         CHAR(1) NOT NULL
  ,
  PRIMARY KEY (batch_id)
  USING INDEX)
/

-- Grants for Table
GRANT SELECT ON batch TO report
WITH GRANT OPTION
/
GRANT REFERENCES ON batch TO report
WITH GRANT OPTION
/



-- Indexes for BATCH

CREATE INDEX ix_batch_doc_id ON batch
  (
    doc_id                          ASC
  )
/

CREATE INDEX ix_batch_terminal_id ON batch
  (
    terminal_id                     ASC
  )
/

CREATE INDEX ix_batch_payment_schedule_id ON batch
  (
    payment_schedule_id             ASC
  )
/

CREATE INDEX ix_batch_customer_bank_id ON batch
  (
    customer_bank_id                ASC
  )
/



-- Constraints for BATCH






-- Triggers for BATCH

CREATE OR REPLACE TRIGGER trbiu_batch
 BEFORE
  INSERT OR UPDATE
 ON batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    IF :NEW.DOC_ID IS NOT NULL THEN
        :NEW.CLOSED := 'Y'; -- Closed
        IF :NEW.END_DATE IS NULL THEN
            :NEW.END_DATE := SYSDATE;
        END IF;
    ELSE
        :NEW.CLOSED := 'N'; -- Open
    END IF;
END;
/


-- Comments for BATCH

COMMENT ON COLUMN batch.closed IS 'Open (O) or Closed (C)'
/
COMMENT ON COLUMN batch.terminal_id IS 'Allow null for non-terminal specific adjustments'
/

-- End of DDL Script for Table CORP.BATCH

-- Foreign Key
ALTER TABLE batch
ADD FOREIGN KEY (doc_id)
REFERENCES doc (doc_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (terminal_id)
REFERENCES terminal (terminal_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (payment_schedule_id)
REFERENCES payment_schedule (payment_schedule_id)
/
ALTER TABLE batch
ADD FOREIGN KEY (customer_bank_id)
REFERENCES customer_bank (customer_bank_id)
/
-- End of DDL script for Foreign Key(s)
