CREATE OR REPLACE TRIGGER g4op.trbi_stg_batch_history
BEFORE INSERT ON g4op.stg_batch_history
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT STG_BATCH_HISTORY_SEQ.NEXTVAL
    INTO   :NEW.ID
    FROM   DUAL;
END;
/