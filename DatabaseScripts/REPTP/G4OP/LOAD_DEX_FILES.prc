CREATE OR REPLACE PROCEDURE G4OP.LOAD_DEX_FILES
    IS
        CURSOR l_dex_cur IS
            SELECT DF.DEX_FILE_ID, TE.TERMINAL_ID, DF.DEX_DATE, DF.DEX_TYPE,
                   DECODE(DF.DEX_TYPE, 4, 'P', 'N') SENT
              FROM DEX_FILE DF, REPORT.TERMINAL_EPORT TE
             WHERE DF.PROCESSED = 'N'
               AND DF.EPORT_ID = TE.EPORT_ID
               AND WITHIN1(DF.DEX_DATE, TE.START_DATE,TE.END_DATE) = 1;
    BEGIN
    	FOR l_dex_rec IN l_dex_cur LOOP
    	    DECLARE
                CURSOR l_line_cur IS
                    SELECT DCA.ALERT_ID, DFL.DEX_CODE, DFL.DETAILS
                      FROM DEX_FILE_LINE DFL, DEX_CODE_ALERT DCA
                     WHERE DFL.DEX_FILE_ID = l_dex_rec.DEX_FILE_ID
                       AND DFL.DEX_CODE = DCA.DEX_CODE
                     GROUP BY DCA.ALERT_ID, DFL.DEX_CODE, DFL.DETAILS
                     HAVING COUNT(*) > (SELECT NVL(COUNT(*), 0)
                        FROM DEX_FILE_LINE DFL1
                        WHERE PREVIOUS_FILE_ID(l_dex_rec.DEX_FILE_ID) = DFL1.DEX_FILE_ID
                        AND DFL.DEX_CODE = DFL1.DEX_CODE
                        AND NVL(DFL.DETAILS, ' ') = NVL(DFL1.DETAILS, ' '));
            BEGIN
                FOR l_line_rec IN l_line_cur LOOP
                    REPORT.TERMINAL_ALERT_INS(
                        l_line_rec.ALERT_ID,
                        l_dex_rec.TERMINAL_ID,
                        l_dex_rec.DEX_DATE,
                        l_line_rec.DETAILS,
                        l_dex_rec.SENT);
    	        END LOOP;
    	        UPDATE DEX_FILE SET PROCESSED = 'Y' WHERE DEX_FILE_ID = l_dex_rec.DEX_FILE_ID;
    	        COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    DECLARE
                        l_msg VARCHAR2(4000) := SQLERRM;
                        l_err_num NUMBER := SQLCODE;
                    BEGIN
                        ROLLBACK;
                        INSERT INTO STG_PROCESSING_ERRORS(STG_TABLE_ID, TRANS_TYPE, ERROR_COLUMN, ERROR_MSG, ERROR_CODE)
                            VALUES(l_dex_rec.DEX_FILE_ID, 50, NULL, l_msg, l_err_num);
                        UPDATE DEX_FILE SET PROCESSED = 'E' WHERE DEX_FILE_ID = l_dex_rec.DEX_FILE_ID;
                        COMMIT;
                    END;
            END;
         END LOOP;
     END;

