CREATE OR REPLACE TRIGGER g4op.trbu_history
BEFORE UPDATE ON g4op.stg_history
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;
    
    IF REPLACE(:NEW.SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.SALE) THEN
        :NEW.SALE := '00000';
    END IF;
END;
/