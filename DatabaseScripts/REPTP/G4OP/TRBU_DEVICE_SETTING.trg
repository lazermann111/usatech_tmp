CREATE OR REPLACE TRIGGER g4op.trbu_device_setting
BEFORE UPDATE ON g4op.device_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

     SELECT
           sysdate,
           user
      into
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/