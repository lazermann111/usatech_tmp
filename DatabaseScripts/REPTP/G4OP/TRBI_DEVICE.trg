CREATE OR REPLACE TRIGGER g4op.trbi_device
BEFORE INSERT ON g4op.device
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.DEVICE_ID IS NULL THEN

      SELECT SEQ_DEVICE_ID.nextval
        into :new.DEVICE_ID
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/