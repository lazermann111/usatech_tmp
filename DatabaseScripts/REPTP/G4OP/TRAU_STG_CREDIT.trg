CREATE OR REPLACE TRIGGER g4op.trau_stg_credit
AFTER UPDATE ON g4op.stg_credit
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF NVL(:NEW.BATCH_NUMBER, 0) != NVL(:OLD.BATCH_NUMBER, 0) THEN
        INSERT INTO STG_BATCH_HISTORY
        (
            TRAN_ID,
            BATCH_NUMBER,
            ERR_CD,
            OLD_ERR_CD,
            TRAN_STATE_CD,
            OLD_TRAN_STATE_CD,
            FAILURE_COUNT,
            TRANTYPE
        )
        VALUES
        (
            :NEW.ID,
            :NEW.BATCH_NUMBER,
            :NEW.ERR_CD,
            :OLD.ERR_CD,
            :NEW.TRAN_STATE_CD,
            :OLD.TRAN_STATE_CD,
            :NEW.FAILURE_COUNT,
            'CREDIT'
        );
    END IF;
END;
/