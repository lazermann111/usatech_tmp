CREATE OR REPLACE TRIGGER g4op.trbi_device_type
BEFORE INSERT ON g4op.device_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

    IF :new.device_type_id IS NULL THEN

      SELECT seq_device_type_id.nextval
        into :new.device_type_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/