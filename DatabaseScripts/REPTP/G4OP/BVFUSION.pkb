create or replace
package body g4op.bvfusion as

  procedure ACCUMULATE_DEX_OLD
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    ) as
    totalRow DEX_DELTA%ROWTYPE;
  begin

      begin 
        select t.* into totalRow from DEX_DELTA t 
          where t.EPORT_ID = pn_eportId and t.TERMINAL_ID = pn_terminalId 
          for update of SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS;
        
          totalRow.SALES_GROSS := totalRow.SALES_GROSS + pn_salesGross;
          totalRow.SALES_NET := totalRow.SALES_NET + pn_salesNet;
          totalRow.SALES_CASH := totalRow.SALES_CASH + pn_salesCash;
          totalRow.SALES_CASHLESS := totalRow.SALES_CASHLESS + pn_salesCashless;
          totalRow.QTY := totalRow.QTY + pn_qty;
          totalRow.QTY_CASH := totalRow.QTY_CASH + pn_qtyCash;
          totalRow.QTY_CASHLESS := totalRow.QTY_CASHLESS + pn_qtyCashless;
          
          pn_dexTotalId := totalRow.DEX_DELTA_ID;
        
          update DEX_DELTA set row = totalRow;
          
        exception 
          when NO_DATA_FOUND then
          
            select DEX_DELTA_SEQ.NEXTVAL into pn_dexTotalId from DUAL;
            
            insert into DEX_DELTA columns (DEX_DELTA_ID,EPORT_ID, TERMINAL_ID, SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS,DEX_FILE_ID)
              select pn_dexTotalId, 
                      pn_eportId, pn_terminalId, 
                      pn_salesGross, pn_salesNet, pn_salesCash, pn_salesCashless, 
                      pn_qty, pn_qtyCash, pn_qtyCashless,
                      DEX_FILE_ID 
              from DEX_FILE
              where FILE_NAME = pv_fileName and FILE_TRANSFER_ID = pn_fileTransferId; 
              
      end;     
  end ACCUMULATE_DEX_OLD;

/**
  * This procedures works with single rows of dex totals, by looking for the previous row and storing deltas
  */
  procedure ACCUMULATE_DEX_DELTA
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    ) as
    totalRow DEX_DELTA%ROWTYPE;
  begin
   
    select pn_eportId as EPORT_ID,
           SUM(t.SALES_GROSS) as SALES_GROSS,
           SUM(t.SALES_NET) as SALES_NET,
           SUM(t.SALES_CASH) as SALES_CASH,
           SUM(t.SALES_CASHLESS) as SALES_CASHLESS,
           SUM(t.QTY) as QTY,
           SUM(t.QTY_CASH) as QTY_CASH,
           SUM(t.QTY_CASHLESS) as QTY_CASHLESS,
           f.DEX_FILE_ID as DEX_FILE_ID,
           pn_terminalId as TERMINAL_ID,
           COUNT(1) as DEX_DELTA_ID
      into totalRow 
      from DEX_DELTA t, DEX_FILE f -- these are not a direct join, the second table is used for the following insert
      where t.EPORT_ID = pn_eportId and t.TERMINAL_ID = pn_terminalId
        and f.FILE_NAME = pv_fileName and f.FILE_TRANSFER_ID = pn_fileTransferId
      GROUP BY pn_eportId, f.DEX_FILE_ID, pn_terminalId;

      select DEX_DELTA_SEQ.NEXTVAL into pn_dexTotalId from DUAL;
      
      -- if no rows found, insert baseline as is 
      -- otherwise insert delta
      if totalRow.DEX_DELTA_ID > 0 THEN          
		
          totalRow.DEX_DELTA_ID := pn_dexTotalId;
          totalRow.SALES_GROSS := pn_salesGross - totalRow.SALES_GROSS;
          totalRow.SALES_NET := pn_salesNet - totalRow.SALES_NET;
          totalRow.SALES_CASH := pn_salesCash - totalRow.SALES_CASH;
          totalRow.SALES_CASHLESS := pn_salesCashless - totalRow.SALES_CASHLESS;
          totalRow.QTY := pn_qty - totalRow.QTY;
          totalRow.QTY_CASH := pn_qtyCash - totalRow.QTY_CASH;
          totalRow.QTY_CASHLESS := pn_qtyCashless - totalRow.QTY_CASHLESS;
              
      END IF;               
      
      totalRow.DEX_DELTA_ID := pn_dexTotalId;
      DBMS_OUTPUT.PUT_LINE(' new sequence number: ' || totalRow.DEX_DELTA_ID);
      insert into DEX_DELTA values totalRow;

  end ACCUMULATE_DEX_DELTA;

/**
  * This procedures works with two tables - it checks the counter table,
  * and subtracts that from the current dex, storing it in the delta table
  */
  procedure ACCUMULATE_DEX
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    ) as
    lr_counterRow DEX_COUNTER%ROWTYPE;
    ld_thisDexDate DATE;
  begin

	begin 
      select DEX_DELTA_SEQ.NEXTVAL into pn_dexTotalId from DUAL;

      select f.DEX_DATE into ld_thisDexDate 
        from G4OP.DEX_FILE f 
        where f.FILE_TRANSFER_ID = pn_fileTransferId;
        
	    select c.* into lr_counterRow from DEX_COUNTER c 
          where c.EPORT_ID = pn_eportId and c.TERMINAL_ID = pn_terminalId 
          for update of SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS,AS_OF_DEX_DATE;
                    
        if ld_thisDexDate > lr_counterRow.AS_OF_DEX_DATE then        

			insert into DEX_DELTA (
				DEX_DELTA_ID, EPORT_ID, TERMINAL_ID, DEX_FILE_ID,
				SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS
			)  select
				pn_dexTotalId, pn_eportId, pn_terminalId, f.DEX_FILE_ID,
				pn_salesGross - lr_counterRow.SALES_GROSS,
				pn_salesNet - lr_counterRow.SALES_NET,
				pn_salesCash - lr_counterRow.SALES_CASH,
				pn_salesCashless - lr_counterRow.SALES_CASHLESS,
				pn_qty - lr_counterRow.QTY,
				pn_qtyCash - lr_counterRow.QTY_CASH,
				pn_qtyCashless - lr_counterRow.QTY_CASHLESS			
	          from 	G4OP.DEX_FILE f 
	          where f.FILE_TRANSFER_ID = pn_fileTransferId;
		       
		       -- update values in counter table
	          lr_counterRow.SALES_GROSS := pn_salesGross;
	          lr_counterRow.SALES_NET := pn_salesNet;
	          lr_counterRow.SALES_CASH := pn_salesCash;
	          lr_counterRow.SALES_CASHLESS := pn_salesCashless;
	          lr_counterRow.QTY := pn_qty;
	          lr_counterRow.QTY_CASH := pn_qtyCash;
	          lr_counterRow.QTY_CASHLESS := pn_qtyCashless;
	          lr_counterRow.AS_OF_DEX_DATE := ld_thisDexDate; 
	                    
		      update DEX_COUNTER SET row = lr_counterRow WHERE DEX_COUNTER_ID = lr_counterRow.DEX_COUNTER_ID;
		       
			end if;	    
        exception 
          when NO_DATA_FOUND then
        
        	-- as before, but we insert into the counter table and put absolutes in both tables

			insert into DEX_DELTA (
				DEX_DELTA_ID, EPORT_ID, TERMINAL_ID, DEX_FILE_ID,
				SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS
			)  select pn_dexTotalId, pn_eportId, pn_terminalId, f.DEX_FILE_ID,
				pn_salesGross,
				pn_salesNet,
				pn_salesCash,
				pn_salesCashless,
				pn_qty,
				pn_qtyCash,
				pn_qtyCashless			
	          from 	G4OP.DEX_FILE f 
	          where f.FILE_TRANSFER_ID = pn_fileTransferId;
		       
		    -- insert values in counter table, too
			insert into DEX_COUNTER (
				DEX_COUNTER_ID, EPORT_ID, TERMINAL_ID, AS_OF_DEX_DATE,
				SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS
			)  values (
				DEX_COUNTER_SEQ.NEXTVAL, pn_eportId, pn_terminalId, ld_thisDexDate,
				pn_salesGross,
				pn_salesNet,
				pn_salesCash,
				pn_salesCashless,
				pn_qty,
				pn_qtyCash,
				pn_qtyCashless);	
	                    
    end;

  end ACCUMULATE_DEX;

   /*
      Retrieves data from DEX_DELTA by terminal and eport 
      and locks the rows in the table. Caller should reset.
    */
    procedure GET_TOTALS_FOR_UPDATE
    (
        prec_totalRow OUT totalCurType,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE
    ) as
  begin
  
    open prec_totalRow for
      select * from DEX_DELTA 
      where TERMINAL_ID = pn_terminalId and EPORT_ID = pn_eportId 
      for update of SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS,DEX_FILE_ID;
      
  end GET_TOTALS_FOR_UPDATE;

end bvfusion;
/