CREATE OR REPLACE TRIGGER g4op.trbu_refund
BEFORE UPDATE ON g4op.stg_refund
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;
    
    IF :new.failure_count > 2 OR (:new.err_cd IS NOT NULL AND UPPER(:new.err_cd) <> 'ERR') THEN
        :new.tran_state_cd := '5';
    END IF;

    IF :new.PROCESSED = 'Y' AND (:new.tran_state_cd <> :old.tran_state_cd OR :new.failure_count <> :old.failure_count) THEN
        :new.PROCESSED := 'U';
    END IF;
END;
/