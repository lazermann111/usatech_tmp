CREATE OR REPLACE TRIGGER g4op.trbi_device_config
BEFORE INSERT ON g4op.device_config
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF REPLACE(:new.month_charge, ' ', '') IS NULL OR NOT IS_NUMBER(REPLACE(:new.month_charge, '.', '')) THEN
        :new.month_charge := '00000';
    END IF;

    IF REPLACE(:new.month_charge_2, ' ', '') IS NULL OR NOT IS_NUMBER(REPLACE(:new.month_charge_2, '.', '')) THEN
        :new.month_charge_2 := '00000';
    END IF;

    IF REPLACE(:new.month_charge_3, ' ', '') IS NULL OR NOT IS_NUMBER(REPLACE(:new.month_charge_3, '.', '')) THEN
        :new.month_charge_3 := '00000';
    END IF;
    
    IF NVL(:new.batch, ' ') = ' ' THEN
        :new.batch := 'U';
    END IF;

    CHECK_DEVICE(L_DEVICE_SERIAL_CD => :NEW.EXPRESS);
EXCEPTION
    WHEN OTHERS THEN NULL;
END;
/