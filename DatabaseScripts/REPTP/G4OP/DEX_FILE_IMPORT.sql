ALTER TABLE g4op.dex_file ADD 
(
	file_transfer_id NUMBER(20, 0),
	file_content CLOB
);

CREATE INDEX ix_dex_file_file_transfer_id ON g4op.dex_file
(
    file_transfer_id ASC
)
TABLESPACE  g4op_indx;

CREATE OR REPLACE 
PROCEDURE dex_file_ins
(
    l_id                OUT DEX_FILE.DEX_FILE_ID%TYPE,
    l_filename          IN DEX_FILE.FILE_NAME%TYPE,
    l_filepath          IN DEX_FILE.FILE_PATH%TYPE,
    l_eport_id          IN DEX_FILE.EPORT_ID%TYPE,
    l_dex_date          IN DEX_FILE.DEX_DATE%TYPE,
    l_dex_type          IN DEX_FILE.DEX_TYPE%TYPE,
    l_file_transfer_id  IN DEX_FILE.FILE_TRANSFER_ID%TYPE DEFAULT NULL,
    l_file_content      IN DEX_FILE.FILE_CONTENT%TYPE DEFAULT NULL
)
IS
BEGIN
    SELECT DEX_FILE_SEQ.NEXTVAL INTO l_id FROM DUAL;
    
    INSERT INTO DEX_FILE
    (
        DEX_FILE_ID,
        FILE_NAME,
        FILE_PATH,
        EPORT_ID,
        DEX_DATE,
        DEX_TYPE,
        FILE_TRANSFER_ID,
        FILE_CONTENT
    )
    VALUES
    (
        l_id,
        l_filename,
        l_filepath,
        l_eport_id,
        l_dex_date,
        l_dex_type,
        l_file_transfer_id,
        l_file_content
    );
END DEX_FILE_INS;
/