CREATE OR REPLACE TRIGGER g4op.trbi_device_setting
BEFORE INSERT ON g4op.device_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Begin

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/