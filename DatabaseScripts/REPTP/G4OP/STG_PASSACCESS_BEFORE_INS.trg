CREATE OR REPLACE TRIGGER STG_PASSACCESS_BEFORE_INS
 BEFORE 
 INSERT
 ON STG_PASSACCESS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
DECLARE
    L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
    L_CHAR CHAR(1);
    L_ROW_NUM NUMBER;
    L_SS VARCHAR2(2);
BEGIN
    SELECT STG_PASSACCESS_SEQ.NEXTVAL
    INTO   :NEW.ID
    FROM   DUAL;
    
    UPDATE_DEVICE_INFO(l_express => :NEW.EXPRESS, l_canada_ind => :NEW.CANADA_IND, l_import_file => :NEW.IMPORT_FILE, l_location => :NEW.LOCATION);

    IF :NEW.MACHINE_TRANS_NO IS NULL OR :NEW.MACHINE_TRANS_NO = ' ' THEN
        :NEW.MACHINE_TRANS_NO := 'LP:' || :NEW.EXPRESS || ':' || TO_CHAR(:NEW.ID);
    END IF;
    
    IF :NEW.IMPORT_FILE = 'RERIX_IMPORT' THEN
        :NEW.FILE_EXPORT_IND := 'Y';
    END IF;

    IF REPLACE(:NEW.SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.SALE) THEN
        :NEW.SALE := '00000';
    END IF;
    
    BEGIN
        L_SS := '00';
        
        IF SUBSTR(:NEW.STOPTIME, 3, 1) = ':' THEN
            BEGIN
                L_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(:NEW.STOPTIME, 4, 2)), 60)), 2, '0');
            EXCEPTION
                WHEN OTHERS THEN NULL;
            END;
        END IF;

        IF LENGTH(:NEW.IMPORT_FILE) = 6 THEN
            :NEW.CREATE_DT := TO_DATE(:NEW.IMPORT_FILE, 'MMDDYY');
        ELSIF :NEW.CREATE_DT IS NULL THEN
            :NEW.CREATE_DT := SYSDATE;
        END IF;
        
        :NEW.TRAN_DT := TO_DATE(:NEW.TRANDATE || '/' || TO_CHAR(:NEW.CREATE_DT, 'YY') || ' ' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(:NEW.TRANTIME, 1, INSTR(:NEW.TRANTIME, ':') - 1)), 24)), 2, '0') || ':' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(:NEW.TRANTIME, INSTR(:NEW.TRANTIME, ':') + 1)), 60)), 2, '0') || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');

        IF :NEW.TRAN_DT > :NEW.CREATE_DT + 1 THEN
            :NEW.TRAN_DT := ADD_MONTHS(:NEW.TRAN_DT, -12);
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
        :NEW.TRAN_STATE_CD := 'I';
    END;
    
    IF :NEW.RECEIVED_DT IS NULL THEN
        :NEW.RECEIVED_DT := :NEW.CREATE_DT;
    END IF;

    IF NVL(LENGTH(:NEW.IMPORT_FILE), 0) = 6 THEN
        :NEW.PROCESSED := 'P';
        :NEW.TRAN_STATE_CD := 'S';
    ELSIF :NEW.TRAN_STATE_CD = '9' THEN
		:NEW.PROCESSED := 'D';
	ELSE
        :NEW.PROCESSED := 'N';
    END IF;

    BEGIN
        IF :NEW.LINK IS NULL OR :NEW.LINK = ' ' THEN
            SELECT LINK
            INTO :NEW.LINK
            FROM DEVICE_CONFIG
            WHERE EXPRESS = :NEW.EXPRESS;
        END IF;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF :NEW.LINK IS NULL THEN
                :NEW.LINK := ' ';
            END IF;
    END;

    IF :NEW.TRANTYPE IS NULL OR :NEW.TRANTYPE = ' ' THEN
        IF UPPER(SUBSTR(TRIM(:NEW.CARD), 1, 5)) = 'DEBIT' THEN
            :NEW.TRANTYPE := 'F-DEBIT';
        ELSE
          L_CHAR := SUBSTR(:NEW.CARD, LENGTH(:NEW.CARD), 1);

          IF ASCII(L_CHAR) BETWEEN 48 AND 57 THEN
              IF TO_NUMBER(L_CHAR) < 5 THEN
                :NEW.TRANTYPE := 'D-ACCESS';
              ELSE
                :NEW.TRANTYPE := 'E-PASS';
              END IF;
          END IF;
        END IF;

        IF :NEW.TRANTYPE IS NULL THEN
            :NEW.TRANTYPE := ' ';
        END IF;
    END IF;

    IF :NEW.RAW_DATA IS NULL OR :NEW.RAW_DATA = ' ' THEN
        IF SUBSTR(:NEW.EXPRESS, 1, 1) IN ('E', 'P', 'G', 'M') THEN
            :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.CARD || ',' || :NEW.SALE || ',' || :NEW.DATA3 || ',' || :NEW.DATA4 || ',' || :NEW.STOPDATE || ',' || :NEW.STOPTIME || ',' || :NEW.DATA5 || ',' || :NEW.DATA6 || ',' || :NEW.SRV1 || ',' || :NEW.CNT1 || ',' || :NEW.SRV2 || ',' || :NEW.CNT2 || ',' || :NEW.SRV3 || ',' || :NEW.CNT3 || ',' || :NEW.SRV4 || ',' || :NEW.CNT4 || ',' || :NEW.SRV5 || ',' || :NEW.CNT5 || ',' || :NEW.SRV6 || ',' || :NEW.CNT6;
        ELSE
            :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.CARD || ',' || :NEW.SALE || ',' || :NEW.DATA3;
        END IF;
    END IF;

    SELECT COUNT(1)
    INTO L_ROW_NUM
    FROM STG_PASSACCESS
    WHERE EXPRESS = :NEW.EXPRESS AND RAW_DATA = :NEW.RAW_DATA;

    IF L_ROW_NUM > 0 THEN
        :NEW.TRAN_STATE_CD := '9';
        :NEW.NO_REPORT_IND := 'Y';
        :NEW.PROCESSED := 'D';
    END IF;
END;
/
