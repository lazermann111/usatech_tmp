create or replace
package g4op.bvfusion as 

    type totalCurType is ref cursor return DEX_DELTA%ROWTYPE;
    
    /*
      Inserts or updates a row in the DEX_DELTA table
      Sums the existing values with the incoming values
      Sets dex file id to zero if null
    */
    procedure ACCUMULATE_DEX
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    );  
    
    /*
      Retrieves data from DEX_DELTA by terminal and eport 
      and locks the rows in the table. Caller should reset.
    */
    procedure GET_TOTALS_FOR_UPDATE
    (
        prec_totalRow OUT totalCurType,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE
    );

end bvfusion;
/