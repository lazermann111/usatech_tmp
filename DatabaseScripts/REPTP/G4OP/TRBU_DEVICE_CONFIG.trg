CREATE OR REPLACE TRIGGER g4op.trbu_device_config
BEFORE UPDATE ON g4op.device_config
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;

    IF REPLACE(:new.month_charge, ' ', '') IS NULL OR NOT IS_NUMBER(REPLACE(:new.month_charge, '.', '')) THEN
        :new.month_charge := '00000';
    END IF;

    IF REPLACE(:new.month_charge_2, ' ', '') IS NULL OR NOT IS_NUMBER(REPLACE(:new.month_charge_2, '.', '')) THEN
        :new.month_charge_2 := '00000';
    END IF;

    IF REPLACE(:new.month_charge_3, ' ', '') IS NULL OR NOT IS_NUMBER(REPLACE(:new.month_charge_3, '.', '')) THEN
        :new.month_charge_3 := '00000';
    END IF;
EXCEPTION
    WHEN OTHERS THEN NULL;
END;
/