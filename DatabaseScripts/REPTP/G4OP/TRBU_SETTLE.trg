CREATE OR REPLACE TRIGGER g4op.trbu_settle
BEFORE UPDATE ON g4op.stg_settle
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;
    
    IF REPLACE(:NEW.TOTAL_SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.TOTAL_SALE) THEN
        :NEW.TOTAL_SALE := '00000';
    END IF;
END;
/