CREATE OR REPLACE TRIGGER g4op.trbu_error
BEFORE UPDATE ON g4op.stg_error
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;
END;
/