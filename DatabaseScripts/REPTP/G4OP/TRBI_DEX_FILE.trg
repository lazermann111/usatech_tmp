CREATE OR REPLACE TRIGGER G4OP.TRBI_DEX_FILE
BEFORE INSERT ON G4OP.DEX_FILE
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF :NEW.DEX_FILE_ID IS NULL THEN
		SELECT DEX_FILE_SEQ.NEXTVAL
		INTO :NEW.DEX_FILE_ID
		FROM DUAL;
    END IF;
	SELECT 
		SYSDATE,
		SYSDATE
	INTO 
		:NEW.CREATE_DT,
		:NEW.UPD_DT
	FROM DUAL;
	UPDATE REPORT.EPORT
	SET LAST_DEX_DATE = GREATEST(:NEW.DEX_DATE, NVL(LAST_DEX_DATE, MIN_DATE))
	WHERE EPORT_ID = :NEW.EPORT_ID;
END;
/