CREATE OR REPLACE TRIGGER g4op.trbu_call_in
BEFORE UPDATE ON g4op.stg_call_in
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;
END;
/