CREATE OR REPLACE PROCEDURE LOG_MSG(
     l_type MSG_LOG.MSG_TYPE%TYPE,
     l_source MSG_LOG.MSG_SOURCE%TYPE,
     l_category MSG_LOG.MSG_CATEGORY%TYPE,
     l_text MSG_LOG.MSG_TEXT%TYPE,
     l_err_num MSG_LOG.ERROR_NUM%TYPE DEFAULT NULL)
   AS
PRAGMA AUTONOMOUS_TRANSACTION;
--
-- Adds a record to the MSG_LOG table
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------       
-- BKRUG        10/15/2004 NEW
BEGIN
    INSERT INTO MSG_LOG(MSG_TYPE, MSG_SOURCE, MSG_CATEGORY, MSG_TEXT, ERROR_NUM)
        VALUES(l_type, l_source, l_category, l_text, l_err_num);
    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
END; -- Procedure
