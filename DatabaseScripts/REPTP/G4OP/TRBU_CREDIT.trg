CREATE OR REPLACE TRIGGER g4op.trbu_credit
BEFORE UPDATE ON g4op.stg_credit
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    SELECT
        :old.create_dt,
        sysdate
    INTO
        :new.create_dt,
        :new.upd_dt
    FROM dual;

    IF REPLACE(:NEW.SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.SALE) THEN
        :NEW.SALE := '00000';
    END IF;

    IF :new.tran_state_cd != '5' THEN
        IF :new.failure_count > 2 THEN
            :new.tran_state_cd := '5';
        ELSIF :new.err_cd IS NOT NULL THEN
            IF :new.canada_ind = 'N' THEN
                IF UPPER(:new.err_cd) <> 'ERR' THEN
                    :new.tran_state_cd := '5';
                END IF;
            ELSE
                IF INSTR(UPPER(:new.err_cd), 'ERROR - NO MATCH') > 0 THEN
                    :new.tran_state_cd := '5';
                END IF;
            END IF;
        END IF;
    END IF;
    
    IF :new.PROCESSED = 'Y' AND (:new.tran_state_cd <> :old.tran_state_cd OR :new.failure_count <> :old.failure_count) THEN
        :new.PROCESSED := 'U';
    END IF;
END;
/