CREATE OR REPLACE TRIGGER g4op.dex_file_before_upd
BEFORE UPDATE 
OF
    sent
ON g4op.dex_file
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN

 :new.upd_dt := SYSDATE;

END;

/