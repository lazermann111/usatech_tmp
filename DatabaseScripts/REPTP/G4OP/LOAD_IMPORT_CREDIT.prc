CREATE OR REPLACE PROCEDURE G4OP.LOAD_IMPORT_CREDIT (
	   l_express IN STG_CREDIT.EXPRESS%TYPE,
	   l_location IN STG_CREDIT.LOCATION%TYPE,
	   l_trandate IN STG_CREDIT.TRANDATE%TYPE,
	   l_trantime IN STG_CREDIT.TRANTIME%TYPE,
	   l_apcode IN STG_CREDIT.APCODE%TYPE,
	   l_sale IN STG_CREDIT.SALE%TYPE,
	   l_data1 IN STG_CREDIT.DATA1%TYPE,
	   l_merch IN STG_CREDIT.MERCH%TYPE,
	   l_card IN STG_CREDIT.CARD%TYPE,
	   l_data2 IN STG_CREDIT.DATA2%TYPE,
	   l_startdate IN STG_CREDIT.STARTDATE%TYPE,
	   l_starttime IN STG_CREDIT.STARTTIME%TYPE,
	   l_data3 IN STG_CREDIT.DATA3%TYPE,
	   l_data4 IN STG_CREDIT.DATA4%TYPE,
	   l_srv1 IN STG_CREDIT.SRV1%TYPE,
	   l_cnt1 IN STG_CREDIT.CNT1%TYPE,
	   l_srv2 IN STG_CREDIT.SRV2%TYPE,
	   l_cnt2 IN STG_CREDIT.CNT2%TYPE,
	   l_srv3 IN STG_CREDIT.SRV3%TYPE,
	   l_cnt3 IN STG_CREDIT.CNT3%TYPE,
	   l_srv4 IN STG_CREDIT.SRV4%TYPE,
	   l_cnt4 IN STG_CREDIT.CNT4%TYPE,
	   l_srv5 IN STG_CREDIT.SRV5%TYPE,
	   l_cnt5 IN STG_CREDIT.CNT5%TYPE,
	   l_srv6 IN STG_CREDIT.SRV6%TYPE,
	   l_cnt6 IN STG_CREDIT.CNT6%TYPE,
	   l_machine_trans_no IN STG_CREDIT.MACHINE_TRANS_NO%TYPE,
	   l_prm_received_dt VARCHAR2,
	   l_import_file IN STG_CREDIT.IMPORT_FILE%TYPE,
       l_canada_ind IN STG_CREDIT.CANADA_IND%TYPE)
IS
    l_received_dt STG_CREDIT.RECEIVED_DT%TYPE;
    l_tran_state_cd STG_CREDIT.TRAN_STATE_CD%TYPE;
BEGIN
    BEGIN
        l_received_dt := TO_DATE(l_prm_received_dt, 'MM/DD/YYYY HH24:MI:SS');
    EXCEPTION
        WHEN OTHERS THEN NULL;
    END;
    IF LOAD_DATA_PKG.GET_SOURCE_SYSTEM_CD(l_machine_trans_no) = 'ReRix' THEN
        IF l_sale = '00000' THEN
	       l_tran_state_cd := 'S';
        ELSE
	       l_tran_state_cd := 'N';
        END IF;
    ELSE
        l_tran_state_cd := 'S';
    END IF;
    INSERT INTO STG_CREDIT(EXPRESS, LOCATION, TRANDATE, TRANTIME, APCODE, SALE, DATA1, MERCH, CARD, DATA2, STARTDATE, STARTTIME, DATA3, DATA4, SRV1, CNT1, SRV2, CNT2, SRV3, CNT3, SRV4, CNT4, SRV5, CNT5, SRV6, CNT6, CANADA_IND, IMPORT_FILE, TRAN_STATE_CD, MACHINE_TRANS_NO, RECEIVED_DT)
    VALUES(l_express, l_location, l_trandate, l_trantime, l_apcode, l_sale, l_data1, l_merch, l_card, l_data2, l_startdate, l_starttime, l_data3, l_data4, l_srv1, l_cnt1, l_srv2, l_cnt2, l_srv3, l_cnt3, l_srv4, l_cnt4, l_srv5, l_cnt5, l_srv6, l_cnt6, l_canada_ind, l_import_file, l_tran_state_cd, l_machine_trans_no, l_received_dt);
END LOAD_IMPORT_CREDIT;
/