CREATE OR REPLACE TRIGGER G4OP.STG_CREDIT_BEFORE_INS
 BEFORE 
 INSERT
 ON G4OP.STG_CREDIT
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
DECLARE
    L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
    L_ROW_NUM NUMBER;
    L_SS VARCHAR2(2);
BEGIN
    SELECT STG_CREDIT_SEQ.NEXTVAL
    INTO   :NEW.ID
    FROM   DUAL;

    UPDATE_DEVICE_INFO(l_express => :NEW.EXPRESS, l_canada_ind => :NEW.CANADA_IND, l_import_file => :NEW.IMPORT_FILE, l_location => :NEW.LOCATION);
    
    IF :NEW.MACHINE_TRANS_NO IS NULL OR :NEW.MACHINE_TRANS_NO = ' ' THEN
        :NEW.MACHINE_TRANS_NO := 'LC:' || :NEW.EXPRESS || ':' || TO_CHAR(:NEW.ID);
    END IF;
    
    IF :NEW.IMPORT_FILE = 'RERIX_IMPORT' THEN
        :NEW.FILE_EXPORT_IND := 'Y';
    END IF;

    IF INSTR(:NEW.APCODE, 'APG4') > 0 THEN
        :NEW.TRAN_AUTH_TYPE_CD := 'L'; -- LOCAL AUTH
    ELSIF INSTR(:NEW.APCODE, 'APGReRix') > 0 THEN
        :NEW.TRAN_AUTH_TYPE_CD := 'R'; -- RERIX
        :NEW.AUTHORITY_CD := 'H';
        IF :NEW.TRAN_STATE_CD IS NULL THEN
            :NEW.TRAN_STATE_CD := 'S';
        END IF;
    ELSE
        :NEW.TRAN_AUTH_TYPE_CD := 'N'; -- TRANLOG
    END IF;

    :NEW.ORIG_TRAN_AUTH_TYPE_CD := :NEW.TRAN_AUTH_TYPE_CD;
    
    IF :NEW.TRAN_AUTH_TYPE_CD <> 'R' THEN
        SELECT DECODE(SUBSTR(TRIM(:NEW.APCODE), 3, 1), 'h', 'H', DECODE(:NEW.CANADA_IND, 'Y', 'S', 'D'))
        INTO :NEW.AUTHORITY_CD
        FROM DUAL;
    END IF;

    IF REPLACE(:NEW.SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.SALE) THEN
        :NEW.SALE := '00000';
    END IF;
    
    BEGIN
        L_SS := '00';
    
        IF SUBSTR(:NEW.STARTTIME, 3, 1) = ':' THEN
            BEGIN
                L_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(:NEW.STARTTIME, 4, 2)), 60)), 2, '0');
            EXCEPTION
                WHEN OTHERS THEN NULL;
            END;
        END IF;

        IF LENGTH(:NEW.IMPORT_FILE) = 6 THEN
            :NEW.CREATE_DT := TO_DATE(:NEW.IMPORT_FILE, 'MMDDYY');
        ELSIF :NEW.CREATE_DT IS NULL THEN
            :NEW.CREATE_DT := SYSDATE;
        END IF;
        
        :NEW.TRAN_DT := TO_DATE(:NEW.TRANDATE || '/' || TO_CHAR(:NEW.CREATE_DT, 'YY') || ' ' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(:NEW.TRANTIME, 1, INSTR(:NEW.TRANTIME, ':') - 1)), 24)), 2, '0') || ':' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(:NEW.TRANTIME, INSTR(:NEW.TRANTIME, ':') + 1)), 60)), 2, '0') || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');
        
        IF :NEW.TRAN_DT > :NEW.CREATE_DT + 1 THEN
            :NEW.TRAN_DT := ADD_MONTHS(:NEW.TRAN_DT, -12);
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
        :NEW.TRAN_STATE_CD := 'I';
    END;
    
    IF :NEW.RECEIVED_DT IS NULL THEN
        :NEW.RECEIVED_DT := :NEW.CREATE_DT;
    END IF;
    
    IF NVL(LENGTH(:NEW.IMPORT_FILE), 0) = 6 THEN
        :NEW.PROCESSED := 'P';
        :NEW.TRAN_STATE_CD := 'S';
    ELSIF :NEW.TRAN_STATE_CD = '9' THEN
		:NEW.PROCESSED := 'D';
	ELSE
        :NEW.PROCESSED := 'N';
    END IF;

    IF NVL(LENGTH(TRANSLATE(TRIM(:new.cnt1) || TRIM(:new.cnt2) || TRIM(:new.cnt3) || TRIM(:new.cnt4) || TRIM(:new.cnt5) || TRIM(:new.cnt6),' 0123456789',' ')), 0) > 0 THEN
        :NEW.DATA_FORMAT_ERR_IND := 'Y';
    END IF;

    IF :NEW.RAW_DATA IS NULL OR :NEW.RAW_DATA = ' ' THEN
        IF SUBSTR(:NEW.EXPRESS, 1, 1) IN ('E', 'P', 'G', 'M') THEN
            :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.APCODE || ',' || :NEW.SALE || ',' || :NEW.DATA1 || ',' || :NEW.MERCH || ',' || :NEW.CARD || ',' || :NEW.DATA2 || ',' || :NEW.STARTDATE || ',' || :NEW.STARTTIME || ',' || :NEW.DATA3 || ',' || :NEW.DATA4 || ',' || :NEW.SRV1 || ',' || :NEW.CNT1 || ',' || :NEW.SRV2 || ',' || :NEW.CNT2 || ',' || :NEW.SRV3 || ',' || :NEW.CNT3 || ',' || :NEW.SRV4 || ',' || :NEW.CNT4 || ',' || :NEW.SRV5 || ',' || :NEW.CNT5 || ',' || :NEW.SRV6 || ',' || :NEW.CNT6;
        ELSE
            :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.APCODE || ',' || :NEW.SALE || ',' || :NEW.DATA1 || ',' || :NEW.MERCH || ',' || :NEW.CARD;
        END IF;
    END IF;

    SELECT COUNT(1)
    INTO L_ROW_NUM
    FROM STG_CREDIT
    WHERE EXPRESS = :NEW.EXPRESS AND RAW_DATA = :NEW.RAW_DATA;

    IF L_ROW_NUM > 0 THEN
        :NEW.TRAN_STATE_CD := '9';
        :NEW.NO_REPORT_IND := 'Y';
        :NEW.PROCESSED := 'D';
	END IF;

    IF :NEW.LINK IS NULL OR :NEW.LINK = ' ' THEN
        BEGIN
            SELECT LINK
            INTO :NEW.LINK
            FROM DEVICE_CONFIG
            WHERE EXPRESS = :NEW.EXPRESS;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF :NEW.LINK IS NULL THEN
                    :NEW.LINK := ' ';
                END IF;
        END;
    END IF;
END;
/
