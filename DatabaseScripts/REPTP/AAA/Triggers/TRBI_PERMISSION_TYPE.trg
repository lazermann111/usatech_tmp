CREATE OR REPLACE TRIGGER AAA.TRBI_PERMISSION_TYPE BEFORE INSERT ON AAA.PERMISSION_TYPE
  FOR EACH ROW
BEGIN
    IF :NEW.PERMISSION_TYPE_ID IS NULL THEN
        SELECT SEQ_PERMISSION_TYPE_ID.NEXTVAL
          INTO :NEW.PERMISSION_TYPE_ID
          FROM DUAL;
    END IF;
    SELECT SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/