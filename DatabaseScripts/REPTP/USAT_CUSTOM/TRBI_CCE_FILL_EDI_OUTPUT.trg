CREATE OR REPLACE TRIGGER usat_custom.trbi_cce_fill_edi_output
BEFORE INSERT ON usat_custom.cce_fill_edi_output
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.CCE_FILL_EDI_OUTPUT_ID IS NULL
	THEN
		SELECT SEQ_CCE_FILL_EDI_OUTPUT_ID.NEXTVAL
		INTO :NEW.CCE_FILL_EDI_OUTPUT_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/