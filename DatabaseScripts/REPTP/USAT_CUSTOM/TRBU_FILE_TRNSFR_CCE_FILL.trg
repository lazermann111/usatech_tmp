CREATE OR REPLACE TRIGGER usat_custom.trbu_file_trnsfr_cce_fill
BEFORE UPDATE ON usat_custom.file_trnsfr_cce_fill
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/