CREATE OR REPLACE TRIGGER usat_custom.trbi_mime_type
BEFORE INSERT ON usat_custom.mime_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by
	FROM DUAL;
END;
/