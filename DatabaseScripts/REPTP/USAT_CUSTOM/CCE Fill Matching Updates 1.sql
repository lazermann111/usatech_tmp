ALTER TABLE USAT_CUSTOM.CCE_FILL_LOG MODIFY(CCE_FILL_ID NULL);
INSERT INTO USAT_CUSTOM.CCE_FILL_LOG_TYPE ( CCE_FILL_LOG_TYPE_ID, CCE_FILL_LOG_TYPE_NAME, CCE_FILL_LOG_TYPE_DESC ) 
VALUES (6, 'No matching cce fill found yet', 'For given fill date and outlet number, no matching cce_fill_id was found yet'); 
COMMIT;

