CREATE OR REPLACE TRIGGER usat_custom.trbi_file_trnsfr_cce_fill
BEFORE INSERT ON usat_custom.file_trnsfr_cce_fill
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/