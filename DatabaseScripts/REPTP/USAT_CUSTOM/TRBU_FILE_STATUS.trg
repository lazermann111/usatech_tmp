CREATE OR REPLACE TRIGGER usat_custom.trbu_file_status
BEFORE UPDATE ON usat_custom.z_file_status
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT
		:OLD.CREATED_BY,
		:OLD.CREATED_TS,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/