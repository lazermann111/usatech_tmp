CREATE OR REPLACE TRIGGER usat_custom.trbu_mime_type
BEFORE UPDATE ON usat_custom.mime_type
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts
	INTO
		:NEW.created_by,
		:NEW.created_ts
	FROM DUAL;
END;
/