CREATE OR REPLACE TRIGGER usat_custom.trbi_file_content
BEFORE INSERT ON usat_custom.file_content
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.FILE_CONTENT_ID IS NULL
	THEN
		SELECT SEQ_FILE_CONTENT_ID.NEXTVAL
		INTO :NEW.FILE_CONTENT_ID
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/