set echo off verify off timing off

prompt ************************************************************************
prompt Updating and inserting for Bug # 793 - Payment Detail report
prompt ************************************************************************

prompt ==> turning off old web links
update web_content.web_link
set web_link_usage = 'm'
where web_link_id in (140, 2140)
/

prompt ==> creating new web links
insert into web_content.web_link (
    web_link_id, web_link_label, web_link_url, web_link_desc,
    web_link_group, web_link_usage, web_link_order
) values (
    10408, 'Payment Detail', './frame.i?content=payment_list.i' || chr(38) ||
    'folioId=635', 'View Details of Payment(s) by EFT or Check',
    'Reports', 'M', 140
)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10408, 1)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10408, 6)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10408, 30)
/

insert into web_content.web_link (
    web_link_id, web_link_label, web_link_url, web_link_desc,
    web_link_group, web_link_usage, web_link_order
) values (
    10409, 'Payment Detail', './frame.i?content=payment_list.i' || chr(38) ||
    'bannerImage=/images/banner_payment.gif' || chr(38) || 'folioId=635',
    'View Details of Payment(s) by EFT or Check', 'Reports', 'M', 140
)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10409, 1)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10409, 6)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10409, 31)
/

prompt ==> creating new folio field
insert into folio_conf.field (
    field_id, field_label, field_desc, display_expression, sort_expression,
    display_format, display_sql_type, sort_sql_type, importance,
    field_category_id, created_by, created_ts, last_updated_by, last_updated_ts,
    created_by_user_id, active_flag
) values (
    2003 'Doc Amount', 'CORP.DOC.TOTAL_AMOUNT', 'CORP.DOC.TOTAL_AMOUNT', 'CURRENCY'
    'DECIMAL', 'DECIMAL', 50, 15, 'FOLIO_CONF', sysdate, 'FOLIO_CONF', sysdate,
    7, 929, 'Y'
)
/

prompt ==> creating new folio # 635

-- TODO: web links are subject to change
prompt ************************************************************************
prompt Updating and inserting for Bug # 529 - Cash vs Credit Trend
prompt ************************************************************************

prompt ==> creating new web link
insert into web_content.web_link (
    web_link_id, web_link_label, web_link_url, web_link_desc,
    web_link_group, web_link_usage, web_link_order
) values (
    10410, 'Transaction Trends', './frame.i?content=transaction_totals.i' || chr(38) ||
    'folioId=667', 'View Line Graph of Transaction Type Trends for a give time period',
    'Reports', 'M', 141
)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10410, 1)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10410, 6)
/

insert into web_content.web_link_requirement (web_link_id, requirement_id)
values (10410, 31)
/

prompt ==> creating new line chart configuration
insert into folio_conf.chart_config (
    chart_type_id, cluster_one_name, cluster_two_name,
    value_one_name, value_two_name, value_three_name,
    cluster_one_description, cluster_two_description,
    value_one_description, value_two_description, value_three_description
) values (
    2, 'Category', 'Series', 'Value', 'Value', 'Value', 'Categories along the X axis',
    'Colored line in each category', 'Values along the Y axis', null, null
)
/

prompt ************************************************************************
prompt Minor change to Folio # 83s directives
prompt ************************************************************************

update folio_conf.folio_directive
set directive_value = '100%'
where folio_id = 83
and directive_id = 9
/
