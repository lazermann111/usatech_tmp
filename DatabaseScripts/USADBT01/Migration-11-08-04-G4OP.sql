CREATE OR REPLACE 
PROCEDURE load_import_credit (
	   l_express IN STG_CREDIT.EXPRESS%TYPE,
	   l_location IN STG_CREDIT.LOCATION%TYPE,
	   l_trandate IN STG_CREDIT.TRANDATE%TYPE,
	   l_trantime IN STG_CREDIT.TRANTIME%TYPE,
	   l_apcode IN STG_CREDIT.APCODE%TYPE,
	   l_sale IN STG_CREDIT.SALE%TYPE,
	   l_data1 IN STG_CREDIT.DATA1%TYPE,
	   l_merch IN STG_CREDIT.MERCH%TYPE,
	   l_card IN STG_CREDIT.CARD%TYPE,
	   l_data2 IN STG_CREDIT.DATA2%TYPE,
	   l_startdate IN STG_CREDIT.STARTDATE%TYPE,
	   l_starttime IN STG_CREDIT.STARTTIME%TYPE,
	   l_data3 IN STG_CREDIT.DATA3%TYPE,
	   l_data4 IN STG_CREDIT.DATA4%TYPE,
	   l_srv1 IN STG_CREDIT.SRV1%TYPE,
	   l_cnt1 IN STG_CREDIT.CNT1%TYPE,
	   l_srv2 IN STG_CREDIT.SRV2%TYPE,
	   l_cnt2 IN STG_CREDIT.CNT2%TYPE,
	   l_srv3 IN STG_CREDIT.SRV3%TYPE,
	   l_cnt3 IN STG_CREDIT.CNT3%TYPE,
	   l_srv4 IN STG_CREDIT.SRV4%TYPE,
	   l_cnt4 IN STG_CREDIT.CNT4%TYPE,
	   l_srv5 IN STG_CREDIT.SRV5%TYPE,
	   l_cnt5 IN STG_CREDIT.CNT5%TYPE,
	   l_srv6 IN STG_CREDIT.SRV6%TYPE,
	   l_cnt6 IN STG_CREDIT.CNT6%TYPE,
	   l_machine_trans_no IN STG_CREDIT.MACHINE_TRANS_NO%TYPE,
	   l_import_file IN STG_CREDIT.IMPORT_FILE%TYPE,
       l_canada_ind IN STG_CREDIT.CANADA_IND%TYPE)
IS
    L_TRAN_STATE_CD STG_CREDIT.TRAN_STATE_CD%TYPE;
    L_TRAN_DT STG_CREDIT.TRAN_DT%TYPE;
    L_SS VARCHAR2(2);
    L_CREATE_DT STG_CREDIT.CREATE_DT%TYPE;
BEGIN
    CHECK_DEVICE(L_DEVICE_SERIAL_CD => L_EXPRESS);

    BEGIN
        IF SUBSTR(L_STARTTIME, 3, 1) = ':' THEN
            L_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_STARTTIME, 4, 2)), 60)), 2, '0');
        ELSE
            L_SS := '00';
        END IF;

        IF LENGTH(l_import_file) = 6 THEN
            l_create_dt := TO_DATE(l_import_file, 'MMDDYY');

            L_TRAN_DT := TO_DATE(L_TRANDATE || '/' || SUBSTR(L_IMPORT_FILE, 5, 2) || ' ' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, 1, INSTR(L_TRANTIME, ':') - 1)), 24)), 2, '0') || ':' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, INSTR(L_TRANTIME, ':') + 1)), 60)), 2, '0') || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');
            IF L_TRAN_DT > TO_DATE(L_IMPORT_FILE || ' 23:59:59', 'MMDDYY HH24:MI:SS') THEN
                L_TRAN_DT := ADD_MONTHS(L_TRAN_DT, -12);
            END IF;
        ELSE
            l_create_dt := SYSDATE;

            L_TRAN_DT := TO_DATE(L_TRANDATE || '/' || TO_CHAR(L_CREATE_DT, 'YY') || ' ' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, 1, INSTR(L_TRANTIME, ':') - 1)), 24)), 2, '0') || ':' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, INSTR(L_TRANTIME, ':') + 1)), 60)), 2, '0') || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');
            IF L_TRAN_DT > TO_DATE(TO_CHAR(L_CREATE_DT, 'MMDDYY') || ' 23:59:59', 'MMDDYY HH24:MI:SS') THEN
                L_TRAN_DT := ADD_MONTHS(L_TRAN_DT, -12);
            END IF;
        END IF;

        L_TRAN_STATE_CD := 'S';
    EXCEPTION
        WHEN OTHERS THEN
        L_TRAN_STATE_CD := 'I';
    END;

    INSERT INTO STG_CREDIT(EXPRESS, LOCATION, TRANDATE, TRANTIME, APCODE, SALE, DATA1, MERCH, CARD, DATA2, STARTDATE, STARTTIME, DATA3, DATA4, SRV1, CNT1, SRV2, CNT2, SRV3, CNT3, SRV4, CNT4, SRV5, CNT5, SRV6, CNT6, CANADA_IND, IMPORT_FILE, CREATE_DT, TRAN_STATE_CD, TRAN_DT, MACHINE_TRANS_NO)
    VALUES(l_express, l_location, l_trandate, l_trantime, l_apcode, l_sale, l_data1, l_merch, l_card, l_data2, l_startdate, l_starttime, l_data3, l_data4, l_srv1, l_cnt1, l_srv2, l_cnt2, l_srv3, l_cnt3, l_srv4, l_cnt4, l_srv5, l_cnt5, l_srv6, l_cnt6, l_canada_ind, l_import_file, L_CREATE_DT, L_TRAN_STATE_CD, L_TRAN_DT, L_MACHINE_TRANS_NO);
END LOAD_IMPORT_CREDIT;

/

CREATE OR REPLACE 
PROCEDURE load_import_error (
	   l_express IN STG_ERROR.EXPRESS%TYPE,
	   l_location IN STG_ERROR.LOCATION%TYPE ,
	   l_errdate IN STG_ERROR.ERRDATE%TYPE,
	   l_errtime IN STG_ERROR.ERRTIME%TYPE,
	   l_msg IN STG_ERROR.MSG%TYPE,
	   l_machine_trans_no IN STG_ERROR.MACHINE_TRANS_NO%TYPE,
	   l_import_file IN STG_ERROR.IMPORT_FILE%TYPE,
       l_canada_ind IN STG_ERROR.CANADA_IND%TYPE )
IS
    l_create_dt STG_ERROR.CREATE_DT%TYPE;
BEGIN
     CHECK_DEVICE(L_DEVICE_SERIAL_CD => L_EXPRESS);
     
     IF LENGTH(l_import_file) = 6 THEN
        l_create_dt := TO_DATE(l_import_file, 'MMDDYY');
     ELSE
        l_create_dt := SYSDATE;
     END IF;

	 INSERT INTO STG_ERROR(EXPRESS, LOCATION, ERRDATE, ERRTIME, MSG, CANADA_IND, IMPORT_FILE, CREATE_DT, MACHINE_TRANS_NO)
        VALUES(l_express, l_location, l_errdate, l_errtime, l_msg, l_canada_ind, l_import_file, l_create_dt, l_machine_trans_no);
END LOAD_IMPORT_ERROR;

/

CREATE OR REPLACE 
PROCEDURE load_import_passaccess (
	   l_express IN STG_PASSACCESS.EXPRESS%TYPE,
	   l_location IN STG_PASSACCESS.LOCATION%TYPE ,
	   l_trandate IN STG_PASSACCESS.TRANDATE%TYPE,
	   l_trantime IN STG_PASSACCESS.TRANTIME%TYPE,
	   l_card IN STG_PASSACCESS.CARD%TYPE,
	   l_sale IN STG_PASSACCESS.SALE%TYPE,
	   l_data3 IN STG_PASSACCESS.DATA3%TYPE,
	   l_data4 IN STG_PASSACCESS.DATA4%TYPE,
	   l_stopdate IN STG_PASSACCESS.STOPDATE%TYPE,
	   l_stoptime IN STG_PASSACCESS.STOPTIME%TYPE,
	   l_data5 IN STG_PASSACCESS.DATA5%TYPE,
	   l_data6 IN STG_PASSACCESS.DATA6%TYPE,
	   l_srv1 IN STG_PASSACCESS.SRV1%TYPE,
	   l_cnt1 IN STG_PASSACCESS.CNT1%TYPE,
	   l_srv2 IN STG_PASSACCESS.SRV2%TYPE,
	   l_cnt2 IN STG_PASSACCESS.CNT2%TYPE,
	   l_srv3 IN STG_PASSACCESS.SRV3%TYPE,
	   l_cnt3 IN STG_PASSACCESS.CNT3%TYPE,
	   l_srv4 IN STG_PASSACCESS.SRV4%TYPE,
	   l_cnt4 IN STG_PASSACCESS.CNT4%TYPE,
	   l_srv5 IN STG_PASSACCESS.SRV5%TYPE,
	   l_cnt5 IN STG_PASSACCESS.CNT5%TYPE,
	   l_srv6 IN STG_PASSACCESS.SRV6%TYPE,
	   l_cnt6 IN STG_PASSACCESS.CNT6%TYPE,
	   l_machine_trans_no IN STG_PASSACCESS.MACHINE_TRANS_NO%TYPE,
	   l_import_file IN STG_PASSACCESS.IMPORT_FILE%TYPE,
       l_canada_ind IN STG_PASSACCESS.CANADA_IND%TYPE )
IS
    L_TRAN_STATE_CD STG_PASSACCESS.TRAN_STATE_CD%TYPE;
    L_TRAN_DT STG_PASSACCESS.TRAN_DT%TYPE;
    L_SS VARCHAR2(2);
    L_CREATE_DT STG_PASSACCESS.CREATE_DT%TYPE;
BEGIN
    CHECK_DEVICE(L_DEVICE_SERIAL_CD => L_EXPRESS);

    BEGIN
        IF SUBSTR(L_STOPTIME, 3, 1) = ':' THEN
            L_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_STOPTIME, 4, 2)), 60)), 2, '0');
        ELSE
            L_SS := '00';
        END IF;

        IF LENGTH(l_import_file) = 6 THEN
            l_create_dt := TO_DATE(l_import_file, 'MMDDYY');

            L_TRAN_DT := TO_DATE(L_TRANDATE || '/' || SUBSTR(L_IMPORT_FILE, 5, 2) || ' ' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, 1, INSTR(L_TRANTIME, ':') - 1)), 24)), 2, '0') || ':' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, INSTR(L_TRANTIME, ':') + 1)), 60)), 2, '0') || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');
            IF L_TRAN_DT > TO_DATE(L_IMPORT_FILE || ' 23:59:59', 'MMDDYY HH24:MI:SS') THEN
                L_TRAN_DT := ADD_MONTHS(L_TRAN_DT, -12);
            END IF;
        ELSE
            l_create_dt := SYSDATE;

            L_TRAN_DT := TO_DATE(L_TRANDATE || '/' || TO_CHAR(L_CREATE_DT, 'YY') || ' ' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, 1, INSTR(L_TRANTIME, ':') - 1)), 24)), 2, '0') || ':' || LPAD(TO_CHAR(MOD(TO_NUMBER(SUBSTR(L_TRANTIME, INSTR(L_TRANTIME, ':') + 1)), 60)), 2, '0') || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');
            IF L_TRAN_DT > TO_DATE(TO_CHAR(L_CREATE_DT, 'MMDDYY') || ' 23:59:59', 'MMDDYY HH24:MI:SS') THEN
                L_TRAN_DT := ADD_MONTHS(L_TRAN_DT, -12);
            END IF;
        END IF;

        L_TRAN_STATE_CD := 'S';
    EXCEPTION
        WHEN OTHERS THEN
        L_TRAN_STATE_CD := 'I';
    END;

	 INSERT INTO STG_PASSACCESS (EXPRESS, LOCATION, TRANDATE, TRANTIME, CARD, SALE, DATA3, DATA4, STOPDATE, STOPTIME, DATA5, DATA6, SRV1, CNT1, SRV2, CNT2, SRV3, CNT3, SRV4, CNT4, SRV5, CNT5, SRV6, CNT6, CANADA_IND, IMPORT_FILE, CREATE_DT, TRAN_STATE_CD, TRAN_DT, MACHINE_TRANS_NO)
     		VALUES(l_express, l_location, l_trandate, l_trantime, l_card, l_sale, l_data3, l_data4, l_stopdate, l_stoptime, l_data5, l_data6, l_srv1, l_cnt1, l_srv2, l_cnt2, l_srv3, l_cnt3, l_srv4, l_cnt4, l_srv5, l_cnt5, l_srv6, l_cnt6, l_canada_ind, l_import_file, l_create_dt, l_tran_state_cd, l_tran_dt, l_machine_trans_no);
END LOAD_IMPORT_PASSACCESS;

/

CREATE OR REPLACE 
PROCEDURE process_batch 
   IS
BEGIN
    INSERT INTO G4OP.STG_CREDIT@REPTP_G4OP
    (
        id,
        express,
        location,
        trandate,
        trantime,
        apcode,
        sale,
        data1,
        merch,
        card,
        data2,
        startdate,
        starttime,
        data3,
        data4,
        srv1,
        cnt1,
        srv2,
        cnt2,
        srv3,
        cnt3,
        srv4,
        cnt4,
        srv5,
        cnt5,
        srv6,
        cnt6,
        processed,
        orig_tran_id,
        machine_trans_no
    )
    SELECT
        G4OP.stg_credit_seq.nextval@REPTP_G4OP,
        express,
        location,
        trandate,
        trantime,
        apcode,
        sale,
        data1,
        merch,
        card,
        data2,
        startdate,
        starttime,
        data3,
        data4,
        srv1,
        cnt1,
        srv2,
        cnt2,
        srv3,
        cnt3,
        srv4,
        cnt4,
        srv5,
        cnt5,
        srv6,
        cnt6,
        processed,
        id,
        machine_trans_no
    FROM STG_CREDIT
    WHERE ID IN
    (
        SELECT TRAN_ID
        FROM EXPORT_FAILURES
        WHERE STATUS = 'N' AND TABLE_NAME = 'STG_CREDIT'
    );

    UPDATE EXPORT_FAILURES
    SET STATUS = 'P'
    WHERE STATUS = 'N' AND TABLE_NAME = 'STG_CREDIT';
    COMMIT;
    
    INSERT INTO G4OP.STG_PASSACCESS@REPTP_G4OP
    (
        id,
        express,
        location,
        trandate,
        trantime,
        card,
        sale,
        data3,
        data4,
        stopdate,
        stoptime,
        data5,
        data6,
        srv1,
        cnt1,
        srv2,
        cnt2,
        srv3,
        cnt3,
        srv4,
        cnt4,
        srv5,
        cnt5,
        srv6,
        cnt6,
        processed,
        orig_tran_id,
        machine_trans_no
    )
    SELECT
        G4OP.stg_passaccess_seq.nextval@REPTP_G4OP,
        express,
        location,
        trandate,
        trantime,
        card,
        sale,
        data3,
        data4,
        stopdate,
        stoptime,
        data5,
        data6,
        srv1,
        cnt1,
        srv2,
        cnt2,
        srv3,
        cnt3,
        srv4,
        cnt4,
        srv5,
        cnt5,
        srv6,
        cnt6,
        processed,
        id,
        machine_trans_no
    FROM STG_PASSACCESS
    WHERE ID IN
    (
        SELECT TRAN_ID
        FROM EXPORT_FAILURES
        WHERE STATUS = 'N' AND TABLE_NAME = 'STG_PASSACCESS'
    );

    UPDATE EXPORT_FAILURES
    SET STATUS = 'P'
    WHERE STATUS = 'N' AND TABLE_NAME = 'STG_PASSACCESS';
    COMMIT;
    
    INSERT INTO G4OP.STG_REFUND@REPTP_G4OP
    (
        id,
        express,
        location,
        data1,
        data2,
        data3,
        data4,
        data5,
        data6,
        data7,
        data8,
        data9,
        data10,
        processed,
        machine_trans_no
    )
    SELECT
        G4OP.stg_refund_seq.nextval@REPTP_G4OP,
        express,
        location,
        data1,
        data2,
        data3,
        data4,
        data5,
        data6,
        data7,
        data8,
        data9,
        data10,
        processed,
        machine_trans_no
    FROM STG_REFUND
    WHERE ID IN
    (
        SELECT TRAN_ID
        FROM EXPORT_FAILURES
        WHERE STATUS = 'N' AND TABLE_NAME = 'STG_REFUND'
    );

    UPDATE EXPORT_FAILURES
    SET STATUS = 'P'
    WHERE STATUS = 'N' AND TABLE_NAME = 'STG_REFUND';
    COMMIT;
    
    INSERT INTO G4OP.STG_ERROR@REPTP_G4OP
    (
        id,
        express,
        location,
        errdate,
        errtime,
        msg,
        processed,
        machine_trans_no
    )
    SELECT
        G4OP.stg_error_seq.nextval@REPTP_G4OP,
        express,
        location,
        errdate,
        errtime,
        msg,
        processed,
        machine_trans_no
    FROM STG_ERROR
    WHERE ID IN
    (
        SELECT TRAN_ID
        FROM EXPORT_FAILURES
        WHERE STATUS = 'N' AND TABLE_NAME = 'STG_ERROR'
    );

    UPDATE EXPORT_FAILURES
    SET STATUS = 'P'
    WHERE STATUS = 'N' AND TABLE_NAME = 'STG_ERROR';
    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
END;

/

CREATE OR REPLACE 
PROCEDURE process_import_credit
IS
    l_export_batch_id NUMBER;
    l_tran_total NUMBER(10);
    l_sale_total NUMBER(10, 2);

    CURSOR l_tran_cur IS SELECT
        express,
        location,
        trandate,
        trantime,
        apcode,
        sale,
        data1,
        merch,
        card,
        data2,
        startdate,
        starttime,
        data3,
        data4,
        srv1,
        cnt1,
        srv2,
        cnt2,
        srv3,
        cnt3,
        srv4,
        cnt4,
        srv5,
        cnt5,
        srv6,
        cnt6,
        machine_trans_no
    FROM EXPORT_CREDIT@USADBP01_TAZDBA
    WHERE EXPORT_LOCK_IND = 'Y'
    ORDER BY ID;
    
    l_tran_rec l_tran_cur%ROWTYPE;
BEGIN
    UPDATE EXPORT_CREDIT@USADBP01_TAZDBA
    SET EXPORT_LOCK_IND = 'Y'
    WHERE EXPORT_BATCH_ID = 0;
    
    IF SQL%FOUND THEN
        FOR l_tran_rec IN l_tran_cur
        LOOP
            LOAD_IMPORT_CREDIT
            (
                l_tran_rec.express,
                l_tran_rec.location,
                l_tran_rec.trandate,
                l_tran_rec.trantime,
                l_tran_rec.apcode,
                l_tran_rec.sale,
                l_tran_rec.data1,
                l_tran_rec.merch,
                l_tran_rec.card,
                l_tran_rec.data2,
                l_tran_rec.startdate,
                l_tran_rec.starttime,
                l_tran_rec.data3,
                l_tran_rec.data4,
                l_tran_rec.srv1,
                l_tran_rec.cnt1,
                l_tran_rec.srv2,
                l_tran_rec.cnt2,
                l_tran_rec.srv3,
                l_tran_rec.cnt3,
                l_tran_rec.srv4,
                l_tran_rec.cnt4,
                l_tran_rec.srv5,
                l_tran_rec.cnt5,
                l_tran_rec.srv6,
                l_tran_rec.cnt6,
                l_tran_rec.machine_trans_no,
                'RERIX_IMPORT',
                'N'
            );
        END LOOP;

        SELECT EXPORT_CREDIT_BATCH_SEQ.NEXTVAL@USADBP01_TAZDBA
        INTO l_export_batch_id
        FROM DUAL;
        
        SELECT COUNT(1), SUM(TO_NUMBER(SALE)) / 100
        INTO l_tran_total, l_sale_total
        FROM EXPORT_CREDIT@USADBP01_TAZDBA
        WHERE EXPORT_LOCK_IND = 'Y';
        
        INSERT INTO EXPORT_CREDIT_HISTORY@USADBP01_TAZDBA(EXPORT_BATCH_ID, TRAN_TOTAL, SALE_TOTAL)
        VALUES(l_export_batch_id, l_tran_total, l_sale_total);
        
        UPDATE EXPORT_CREDIT@USADBP01_TAZDBA
        SET EXPORT_LOCK_IND = NULL, EXPORT_BATCH_ID = l_export_batch_id
        WHERE EXPORT_LOCK_IND = 'Y';
        
        COMMIT;
    END IF;
EXCEPTION
   WHEN OTHERS THEN
       ROLLBACK;
       RAISE;
END;

/

CREATE OR REPLACE 
PROCEDURE process_import_error
IS
    l_export_batch_id NUMBER;
    l_tran_total NUMBER(10);

    CURSOR l_tran_cur IS SELECT
        express,
        location,
        errdate,
        errtime,
        msg,
        machine_trans_no
    FROM EXPORT_ERROR@USADBP01_TAZDBA
    WHERE EXPORT_LOCK_IND = 'Y'
    ORDER BY ID;

    l_tran_rec l_tran_cur%ROWTYPE;
BEGIN
    UPDATE EXPORT_ERROR@USADBP01_TAZDBA
    SET EXPORT_LOCK_IND = 'Y'
    WHERE EXPORT_BATCH_ID = 0;

    IF SQL%FOUND THEN
        FOR l_tran_rec IN l_tran_cur
        LOOP
            LOAD_IMPORT_ERROR
            (
                l_tran_rec.express,
                l_tran_rec.location,
                l_tran_rec.errdate,
                l_tran_rec.errtime,
                l_tran_rec.msg,
                l_tran_rec.machine_trans_no,
                'RERIX_IMPORT',
                'N'
            );
        END LOOP;

        SELECT EXPORT_ERROR_BATCH_SEQ.NEXTVAL@USADBP01_TAZDBA
        INTO l_export_batch_id
        FROM DUAL;

        SELECT COUNT(1)
        INTO l_tran_total
        FROM EXPORT_ERROR@USADBP01_TAZDBA
        WHERE EXPORT_LOCK_IND = 'Y';

        INSERT INTO EXPORT_ERROR_HISTORY@USADBP01_TAZDBA(EXPORT_BATCH_ID, TRAN_TOTAL)
        VALUES(l_export_batch_id, l_tran_total);

        UPDATE EXPORT_ERROR@USADBP01_TAZDBA
        SET EXPORT_LOCK_IND = NULL, EXPORT_BATCH_ID = l_export_batch_id
        WHERE EXPORT_LOCK_IND = 'Y';

        COMMIT;
    END IF;
EXCEPTION
   WHEN OTHERS THEN
       ROLLBACK;
       RAISE;
END;

/

CREATE OR REPLACE 
PROCEDURE process_import_passaccess
IS
    l_export_batch_id NUMBER;
    l_tran_total NUMBER(10);
    l_sale_total NUMBER(10, 2);

    CURSOR l_tran_cur IS SELECT
        express,
        location,
        trandate,
        trantime,
        card,
        sale,
        data3,
        data4,
        stopdate,
        stoptime,
        data5,
        data6,
        srv1,
        cnt1,
        srv2,
        cnt2,
        srv3,
        cnt3,
        srv4,
        cnt4,
        srv5,
        cnt5,
        srv6,
        cnt6,
        machine_trans_no
    FROM EXPORT_PASSACCESS@USADBP01_TAZDBA
    WHERE EXPORT_LOCK_IND = 'Y'
    ORDER BY ID;
    
    tran_rec l_tran_cur%ROWTYPE;
BEGIN
    UPDATE EXPORT_PASSACCESS@USADBP01_TAZDBA
    SET EXPORT_LOCK_IND = 'Y'
    WHERE EXPORT_BATCH_ID = 0;

    IF SQL%FOUND THEN
        FOR l_tran_rec IN l_tran_cur
        LOOP
            LOAD_IMPORT_PASSACCESS
            (
                l_tran_rec.express,
                l_tran_rec.location,
                l_tran_rec.trandate,
                l_tran_rec.trantime,
                l_tran_rec.card,
                l_tran_rec.sale,
                l_tran_rec.data3,
                l_tran_rec.data4,
                l_tran_rec.stopdate,
                l_tran_rec.stoptime,
                l_tran_rec.data5,
                l_tran_rec.data6,
                l_tran_rec.srv1,
                l_tran_rec.cnt1,
                l_tran_rec.srv2,
                l_tran_rec.cnt2,
                l_tran_rec.srv3,
                l_tran_rec.cnt3,
                l_tran_rec.srv4,
                l_tran_rec.cnt4,
                l_tran_rec.srv5,
                l_tran_rec.cnt5,
                l_tran_rec.srv6,
                l_tran_rec.cnt6,
                l_tran_rec.machine_trans_no,
                'RERIX_IMPORT',
                'N'
            );
        END LOOP;

        SELECT EXPORT_PASSACCESS_BATCH_SEQ.NEXTVAL@USADBP01_TAZDBA
        INTO l_export_batch_id
        FROM DUAL;
        
        SELECT COUNT(1), SUM(TO_NUMBER(SALE)) / 100
        INTO l_tran_total, l_sale_total
        FROM EXPORT_PASSACCESS@USADBP01_TAZDBA
        WHERE EXPORT_LOCK_IND = 'Y';

        INSERT INTO EXPORT_PASSACCESS_HISTORY@USADBP01_TAZDBA(EXPORT_BATCH_ID, TRAN_TOTAL, SALE_TOTAL)
        VALUES(l_export_batch_id, l_tran_total, l_sale_total);

        UPDATE EXPORT_PASSACCESS@USADBP01_TAZDBA
        SET EXPORT_LOCK_IND = NULL, EXPORT_BATCH_ID = l_export_batch_id
        WHERE EXPORT_LOCK_IND = 'Y';

        COMMIT;
    END IF;
EXCEPTION
   WHEN OTHERS THEN
       ROLLBACK;
       RAISE;
END;

/

CREATE OR REPLACE TRIGGER stg_credit_before_ins
 BEFORE
  INSERT
 ON stg_credit
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
    L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
    L_ROW_NUM NUMBER;
BEGIN
    SELECT STG_CREDIT_SEQ.NEXTVAL
    INTO   :NEW.ID
    FROM   DUAL;
    
    IF :NEW.MACHINE_TRANS_NO IS NULL OR :NEW.MACHINE_TRANS_NO = ' ' THEN
        :NEW.MACHINE_TRANS_NO := 'LC:' || :NEW.EXPRESS || ':' || TO_CHAR(:NEW.ID);
    END IF;

    IF INSTR(:NEW.APCODE, 'APG4') > 0 THEN
        :NEW.TRAN_AUTH_TYPE_CD := 'L'; -- LOCAL AUTH
    ELSIF INSTR(:NEW.APCODE, 'APGReRix') > 0 THEN
        :NEW.TRAN_AUTH_TYPE_CD := 'R'; -- RERIX
        :NEW.AUTHORITY_CD := 'H';
        IF :NEW.TRAN_STATE_CD IS NULL THEN
            :NEW.TRAN_STATE_CD := 'S';
        END IF;
    ELSE
        :NEW.TRAN_AUTH_TYPE_CD := 'N'; -- TRANLOG
    END IF;

    :NEW.ORIG_TRAN_AUTH_TYPE_CD := :NEW.TRAN_AUTH_TYPE_CD;
    
    IF :NEW.TRAN_AUTH_TYPE_CD <> 'R' THEN
        SELECT DECODE(SUBSTR(TRIM(:NEW.APCODE), 3, 1), 'h', 'H', DECODE(:NEW.CANADA_IND, 'Y', 'S', 'D'))
        INTO :NEW.AUTHORITY_CD
        FROM DUAL;
    END IF;

    IF REPLACE(:NEW.SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.SALE) THEN
        :NEW.SALE := '00000';
    END IF;

    BEGIN
        SELECT DEVICE_TYPE_ID
        INTO L_DEVICE_TYPE_ID
        FROM DEVICE
        WHERE DEVICE_SERIAL_CD = :NEW.EXPRESS AND DEVICE_ACTIVE_YN_FLAG = 'Y' AND ROWNUM < 2;

        IF L_DEVICE_TYPE_ID = 10 THEN --Transact
            :NEW.PROCESSED := 'X';
        ELSE
            IF NVL(LENGTH(:NEW.IMPORT_FILE), 0) = 6 THEN
                :NEW.PROCESSED := 'P';
            ELSE
                :NEW.PROCESSED := 'N';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.PROCESSED := 'X';
    END;

    IF NVL(LENGTH(TRANSLATE(TRIM(:new.cnt1) || TRIM(:new.cnt2) || TRIM(:new.cnt3) || TRIM(:new.cnt4) || TRIM(:new.cnt5) || TRIM(:new.cnt6),' 0123456789',' ')), 0) > 0 THEN
        :NEW.DATA_FORMAT_ERR_IND := 'Y';
    END IF;

    IF :NEW.RAW_DATA IS NULL OR :NEW.RAW_DATA = ' ' THEN
        IF SUBSTR(:NEW.EXPRESS, 1, 2) IN ('DC', 'DP', 'DF') THEN
            :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.APCODE || ',' || :NEW.SALE || ',' || :NEW.DATA1 || ',' || :NEW.MERCH || ',' || :NEW.CARD;
        ELSE
            IF SUBSTR(:NEW.EXPRESS, 1, 1) IN ('E', 'P') THEN
                :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.APCODE || ',' || :NEW.SALE || ',' || :NEW.DATA1 || ',' || :NEW.MERCH || ',' || :NEW.CARD || ',' || :NEW.DATA2 || ',' || :NEW.STARTDATE || ',' || :NEW.STARTTIME || ',' || :NEW.DATA3 || ',' || :NEW.DATA4 || ',' || :NEW.SRV1 || ',' || :NEW.CNT1 || ',' || :NEW.SRV2 || ',' || :NEW.CNT2 || ',' || :NEW.SRV3 || ',' || :NEW.CNT3 || ',' || :NEW.SRV4 || ',' || :NEW.CNT4 || ',' || :NEW.SRV5 || ',' || :NEW.CNT5 || ',' || :NEW.SRV6 || ',' || :NEW.CNT6;
            ELSE
                :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.APCODE || ',' || :NEW.SALE || ',' || :NEW.DATA1 || ',' || :NEW.MERCH || ',' || :NEW.CARD;
            END IF;
        END IF;
    END IF;

    SELECT COUNT(1)
    INTO L_ROW_NUM
    FROM STG_CREDIT
    WHERE EXPRESS = :NEW.EXPRESS AND RAW_DATA = :NEW.RAW_DATA;

    IF L_ROW_NUM > 0 THEN
        :NEW.TRAN_STATE_CD := '9';
        :NEW.NO_REPORT_IND := 'Y';
    END IF;

    IF :NEW.LINK IS NULL OR :NEW.LINK = ' ' THEN
        BEGIN
            SELECT LINK
            INTO :NEW.LINK
            FROM DEVICE_CONFIG
            WHERE EXPRESS = :NEW.EXPRESS;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF :NEW.LINK IS NULL THEN
                    :NEW.LINK := ' ';
                END IF;
        END;
    END IF;
END;

/

CREATE OR REPLACE TRIGGER stg_error_before_ins
 BEFORE
  INSERT
 ON stg_error
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT STG_ERROR_SEQ.NEXTVAL
    INTO   :NEW.ID
    FROM   DUAL;
    
    IF :NEW.MACHINE_TRANS_NO IS NULL OR :NEW.MACHINE_TRANS_NO = ' ' THEN
        :NEW.MACHINE_TRANS_NO := 'LE:' || :NEW.EXPRESS || ':' || TO_CHAR(:NEW.ID);
    END IF;

    BEGIN
        SELECT DEVICE_TYPE_ID
        INTO L_DEVICE_TYPE_ID
        FROM DEVICE
        WHERE DEVICE_SERIAL_CD = :NEW.EXPRESS AND DEVICE_ACTIVE_YN_FLAG = 'Y' AND ROWNUM < 2;

        IF L_DEVICE_TYPE_ID = 10 THEN --Transact
            :NEW.PROCESSED := 'X';
        ELSE
            IF NVL(LENGTH(:NEW.IMPORT_FILE), 0) = 6 THEN
                :NEW.PROCESSED := 'P';
            ELSE
                :NEW.PROCESSED := 'N';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.PROCESSED := 'X';
    END;

    BEGIN
        IF :NEW.LINK IS NULL OR :NEW.LINK = ' ' THEN
            SELECT LINK
            INTO :NEW.LINK
            FROM DEVICE_CONFIG
            WHERE EXPRESS = :NEW.EXPRESS;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF :NEW.LINK IS NULL THEN
                :NEW.LINK := ' ';
            END IF;
    END;
END;

/

CREATE OR REPLACE TRIGGER stg_passaccess_before_ins
 BEFORE
  INSERT
 ON stg_passaccess
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
    L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
    L_CHAR CHAR(1);
    L_ROW_NUM NUMBER;
BEGIN
    SELECT STG_PASSACCESS_SEQ.NEXTVAL
    INTO   :NEW.ID
    FROM   DUAL;

    IF :NEW.MACHINE_TRANS_NO IS NULL OR :NEW.MACHINE_TRANS_NO = ' ' THEN
        :NEW.MACHINE_TRANS_NO := 'LP:' || :NEW.EXPRESS || ':' || TO_CHAR(:NEW.ID);
    END IF;

    IF REPLACE(:NEW.SALE, ' ', '') IS NULL OR NOT IS_NUMBER(:NEW.SALE) THEN
        :NEW.SALE := '00000';
    END IF;

    BEGIN
        SELECT DEVICE_TYPE_ID
        INTO L_DEVICE_TYPE_ID
        FROM DEVICE
        WHERE DEVICE_SERIAL_CD = :NEW.EXPRESS AND DEVICE_ACTIVE_YN_FLAG = 'Y' AND ROWNUM < 2;

        IF L_DEVICE_TYPE_ID = 10 THEN --Transact
            :NEW.PROCESSED := 'X';
        ELSE
            IF NVL(LENGTH(:NEW.IMPORT_FILE), 0) = 6 THEN
                :NEW.PROCESSED := 'P';
            ELSE
                :NEW.PROCESSED := 'N';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.PROCESSED := 'X';
    END;

    BEGIN
        IF :NEW.LINK IS NULL OR :NEW.LINK = ' ' THEN
            SELECT LINK
            INTO :NEW.LINK
            FROM DEVICE_CONFIG
            WHERE EXPRESS = :NEW.EXPRESS;
        END IF;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF :NEW.LINK IS NULL THEN
                :NEW.LINK := ' ';
            END IF;
    END;

    IF :NEW.TRANTYPE IS NULL OR :NEW.TRANTYPE = ' ' THEN
        IF UPPER(SUBSTR(TRIM(:NEW.CARD), 1, 5)) = 'DEBIT' THEN
            :NEW.TRANTYPE := 'F-DEBIT';
        ELSE
          L_CHAR := SUBSTR(:NEW.CARD, LENGTH(:NEW.CARD), 1);

          IF ASCII(L_CHAR) BETWEEN 48 AND 57 THEN
              IF TO_NUMBER(L_CHAR) < 5 THEN
                :NEW.TRANTYPE := 'D-ACCESS';
              ELSE
                :NEW.TRANTYPE := 'E-PASS';
              END IF;
          END IF;
        END IF;

        IF :NEW.TRANTYPE IS NULL THEN
            :NEW.TRANTYPE := ' ';
        END IF;
    END IF;

    IF :NEW.RAW_DATA IS NULL OR :NEW.RAW_DATA = ' ' THEN
        IF SUBSTR(:NEW.EXPRESS, 1, 2) IN ('DC', 'DP', 'DF') THEN
            :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.CARD || ',' || :NEW.SALE || ',' || :NEW.DATA3;
        ELSE
            IF SUBSTR(:NEW.EXPRESS, 1, 1) IN ('E', 'P') THEN
                :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.CARD || ',' || :NEW.SALE || ',' || :NEW.DATA3 || ',' || :NEW.DATA4 || ',' || :NEW.STOPDATE || ',' || :NEW.STOPTIME || ',' || :NEW.DATA5 || ',' || :NEW.DATA6 || ',' || :NEW.SRV1 || ',' || :NEW.CNT1 || ',' || :NEW.SRV2 || ',' || :NEW.CNT2 || ',' || :NEW.SRV3 || ',' || :NEW.CNT3 || ',' || :NEW.SRV4 || ',' || :NEW.CNT4 || ',' || :NEW.SRV5 || ',' || :NEW.CNT5 || ',' || :NEW.SRV6 || ',' || :NEW.CNT6;
            ELSE
                :NEW.RAW_DATA := :NEW.EXPRESS || ',' || :NEW.LOCATION || ',' || :NEW.TRANDATE || ',' || :NEW.TRANTIME || ',' || :NEW.CARD || ',' || :NEW.SALE || ',' || :NEW.DATA3;
            END IF;
        END IF;
    END IF;

    SELECT COUNT(1)
    INTO L_ROW_NUM
    FROM STG_PASSACCESS
    WHERE EXPRESS = :NEW.EXPRESS AND RAW_DATA = :NEW.RAW_DATA;

    IF L_ROW_NUM > 0 THEN
        :NEW.TRAN_STATE_CD := '9';
        :NEW.NO_REPORT_IND := 'Y';
    END IF;
END;

/

CREATE OR REPLACE TRIGGER stg_refund_before_ins
 BEFORE
  INSERT
 ON stg_refund
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT
        STG_REFUND_SEQ.NEXTVAL,
        DECODE(UPPER(SUBSTR(TRIM(:NEW.DATA8), 3, 1)), 'h', 'H', DECODE(:NEW.CANADA_IND, 'Y', 'S', 'D'))
    INTO
        :NEW.ID,
        :NEW.AUTHORITY_CD
    FROM DUAL;
    
    IF :NEW.MACHINE_TRANS_NO IS NULL OR :NEW.MACHINE_TRANS_NO = ' ' THEN
        :NEW.MACHINE_TRANS_NO := 'LR:' || :NEW.EXPRESS || ':' || TO_CHAR(:NEW.ID);
    END IF;

    BEGIN
        SELECT DEVICE_TYPE_ID
        INTO L_DEVICE_TYPE_ID
        FROM DEVICE
        WHERE DEVICE_SERIAL_CD = :NEW.EXPRESS AND DEVICE_ACTIVE_YN_FLAG = 'Y' AND ROWNUM < 2;

        IF L_DEVICE_TYPE_ID = 10 THEN --Transact
            :NEW.PROCESSED := 'X';
        ELSE
            IF NVL(LENGTH(:NEW.IMPORT_FILE), 0) = 6 THEN
                :NEW.PROCESSED := 'P';
            ELSE
                :NEW.PROCESSED := 'N';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.PROCESSED := 'X';
    END;

    BEGIN
        IF :NEW.LINK IS NULL OR :NEW.LINK = ' ' THEN
            SELECT LINK
            INTO :NEW.LINK
            FROM DEVICE_CONFIG
            WHERE EXPRESS = :NEW.EXPRESS;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF :NEW.LINK IS NULL THEN
                :NEW.LINK := ' ';
            END IF;
    END;
END;

/

CREATE OR REPLACE TRIGGER trai_stg_error
 AFTER
  INSERT
 ON stg_error
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
IF NVL(LENGTH(:new.import_file), 0) NOT IN (0, 6) THEN
    UPDATE DEVICE_CONFIG
    SET CALLIN_SITE = 'Sterling', DATETIME = TO_CHAR(SYSDATE, 'MMDDYYHH24MISS')
    WHERE EXPRESS = :NEW.EXPRESS;
END IF;

IF NVL(LENGTH(:new.import_file), 0) != 6 THEN
    BEGIN
    INSERT INTO G4OP.STG_ERROR@REPTP_G4OP
    (
        id,
        express,
        location,
        errdate,
        errtime,
        msg,
        processed,
        machine_trans_no
    )
    VALUES
    (
        G4OP.stg_error_seq.nextval@REPTP_G4OP,
        :new.express,
        :new.location,
        :new.errdate,
        :new.errtime,
        :new.msg,
        :new.processed,
        :new.machine_trans_no
    );
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO EXPORT_FAILURES(TABLE_NAME, TRAN_ID)
            VALUES('STG_ERROR', :NEW.ID);
    END;
END IF;
END;