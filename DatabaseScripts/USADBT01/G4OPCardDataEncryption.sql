DECLARE
    ln_increment PLS_INTEGER := 10000;
    ln_id NUMBER;
    ln_max_id NUMBER;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT MIN(ID), MAX(ID)
    INTO ln_id, ln_max_id
    FROM G4OP.STG_CREDIT;
	
	DBMS_OUTPUT.PUT_LINE('Starting STG_CREDIT update...');
    WHILE ln_id < ln_max_id LOOP
		DBMS_OUTPUT.PUT_LINE('Updating STG_CREDIT table from ID ' || ln_id || ' to ' || (ln_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));

		UPDATE G4OP.STG_CREDIT
		SET CARD = DBADMIN.USAT_GET_ENC_VAL(CARD),
			RAW_DATA = DBADMIN.USAT_GET_ENC_VAL(RAW_DATA),
			TRAN_DATA = DBADMIN.USAT_GET_ENC_VAL(TRAN_DATA)
		WHERE ID >= ln_id AND ID < ln_id + ln_increment
			AND (INSTR(CARD, '*') > 0 OR LENGTH(CARD) < 32);
		
        ln_cnt := SQL%ROWCOUNT;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows in STG_CREDIT table from ID ' || ln_id || ' to ' || (ln_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
		ln_id := ln_id + ln_increment;
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('STG_CREDIT update complete...');
	
    SELECT MIN(ID), MAX(ID)
    INTO ln_id, ln_max_id
    FROM G4OP.STG_PASSACCESS;
	
	DBMS_OUTPUT.PUT_LINE('Starting STG_PASSACCESS update...');
    WHILE ln_id < ln_max_id LOOP
		DBMS_OUTPUT.PUT_LINE('Updating STG_PASSACCESS table from ID ' || ln_id || ' to ' || (ln_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));

		UPDATE G4OP.STG_PASSACCESS
		SET CARD = DBADMIN.USAT_GET_ENC_VAL(CARD),
			RAW_DATA = DBADMIN.USAT_GET_ENC_VAL(RAW_DATA),
			TRAN_DATA = DBADMIN.USAT_GET_ENC_VAL(TRAN_DATA)
		WHERE ID >= ln_id AND ID < ln_id + ln_increment
			AND (INSTR(CARD, '*') > 0 OR LENGTH(CARD) < 32);
		
        ln_cnt := SQL%ROWCOUNT;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows in STG_PASSACCESS table from ID ' || ln_id || ' to ' || (ln_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
		ln_id := ln_id + ln_increment;
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('STG_PASSACCESS update complete...');

    SELECT MIN(ID), MAX(ID)
    INTO ln_id, ln_max_id
    FROM G4OP.STG_REFUND;
	
	DBMS_OUTPUT.PUT_LINE('Starting STG_REFUND update...');
    WHILE ln_id < ln_max_id LOOP
		DBMS_OUTPUT.PUT_LINE('Updating STG_REFUND table from ID ' || ln_id || ' to ' || (ln_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));

		UPDATE G4OP.STG_REFUND
		SET DATA4 = DBADMIN.USAT_GET_ENC_VAL(DATA4)
		WHERE ID >= ln_id AND ID < ln_id + ln_increment
			AND (INSTR(DATA4, '*') > 0 OR LENGTH(DATA4) < 32);
		
        ln_cnt := SQL%ROWCOUNT;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Completed updating ' || ln_cnt || ' rows in STG_REFUND table from ID ' || ln_id || ' to ' || (ln_id + ln_increment - 1) || ' at ' || TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS'));
		ln_id := ln_id + ln_increment;
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('STG_REFUND update complete...');	
END;
/
