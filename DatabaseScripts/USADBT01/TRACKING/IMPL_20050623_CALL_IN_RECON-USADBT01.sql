/* 
Description:


This is a new implementation for daily call in record to transaction exceptions and device counters to call in record exception tracking. It includes a package to generate the exception data. The data is pulled from USADBP and inserted on REPTP for reporting.

This package will be deprecated after the implementation of USARDB01 is completed (replacement DB for REPTP).


Created By:
J Bradley 

Date:
2005-06-23 

Grant Privileges (run as):
TRACKING 

*/ 


-- reconciliation package (contains stored procedures for data generation)
@TRACKING.PKG_RECONCILIATION_ddl.sql;


