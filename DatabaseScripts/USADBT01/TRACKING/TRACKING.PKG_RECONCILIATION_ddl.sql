-- Start of DDL Script for Package TRACKING.PKG_RECONCILIATION
-- Generated 6/24/2005 3:22:50 PM from TRACKING@USADBT01

CREATE OR REPLACE 
PACKAGE tracking.pkg_reconciliation IS
   PROCEDURE sp_gen_call_in_trans_excpt (
      inDate IN DATE
   );
   PROCEDURE sp_gen_counter_call_in_excpt (
      inDate IN DATE
   );
END;
/


CREATE OR REPLACE 
PACKAGE BODY tracking.pkg_reconciliation IS
   
   PROCEDURE sp_gen_call_in_trans_excpt (inDate IN DATE) AS
      tDate DATE;
   BEGIN
		-- ensure tDate is truncated to a day (i.e. time = 00:00:00)
		tDate := TRUNC(inDate);
		
		-- migrate the exception data to reporting
		INSERT INTO report.call_in_trans_except@reptp_report (
    		device_call_in_record_id,
            device_id,
            machine_id,
            device_serial_cd,
            device_type,
            firmware_version,
            server_date,
            call_in_start_ts,
            call_in_finish_ts,
            call_in_credit_count,
            trans_credit_count,
            call_in_credit_amount,
            trans_credit_amount,
            call_in_cash_count,
            trans_cash_count,
            call_in_cash_amount,
            trans_cash_amount,
            call_in_pass_count,
            trans_pass_count,
            call_in_pass_amount,
            trans_pass_amount
		)
		SELECT citr.device_call_in_record_id,
        citr.device_id,
        citr.machine_id,
        citr.device_serial_cd,
        citr.device_type,
        citr.firmware_version,
        citr.server_date,
        dci.call_in_start_ts,
        dci.call_in_finish_ts,
        citr.call_in_credit_count,
        citr.trans_credit_count,
        citr.call_in_credit_amount,
        citr.trans_credit_amount,
        citr.call_in_cash_count,
        citr.trans_cash_count,
        citr.call_in_cash_amount,
        citr.trans_cash_amount,
        citr.call_in_pass_count,
        citr.trans_pass_count,
        citr.call_in_pass_amount,
        citr.trans_pass_amount
        FROM tracking.call_in_trans_rec@usadbp_tracking citr
        JOIN device.device_call_in_record@usadbp_tracking dci
        ON citr.device_call_in_record_id = dci.device_call_in_record_id
        WHERE citr.server_date = (tDate - 1)
        AND (citr.call_in_credit_count != citr.trans_credit_count
        	OR citr.call_in_credit_amount != citr.trans_credit_amount
        	OR citr.call_in_cash_count != citr.trans_cash_count
        	OR citr.call_in_cash_amount != citr.trans_cash_amount
        	OR citr.call_in_pass_count != citr.trans_pass_count
        	OR citr.call_in_pass_amount != citr.trans_pass_amount);		
		
		COMMIT;
      
   --EXCEPTION
      
   END;


   PROCEDURE sp_gen_counter_call_in_excpt (inDate IN DATE) AS
      tDate DATE;
   BEGIN
		-- ensure tDate is truncated to a day (i.e. time = 00:00:00)
		tDate := TRUNC(inDate);
		
		INSERT INTO report.counter_call_in_except@reptp_report (
            device_call_in_record_id,
            device_id, machine_id,
            device_serial_cd,
            device_type,
            firmware_version,
            server_date,
            call_in_start_ts,
            call_in_finish_ts,
            trans_cred_vend_count,
            counter_cred_vend_count,
            call_in_credit_amount,
            counter_credit_amount,
            trans_cash_vend_count,
            counter_cash_vend_count,
            call_in_cash_amount,
            counter_cash_amount,
            trans_pass_vend_count,
            counter_pass_vend_count,
            call_in_pass_amount,
            counter_pass_amount
		)
		SELECT cc.device_call_in_record_id,
        cc.device_id, machine_id,
        cc.device_serial_cd,
        cc.device_type,
        cc.firmware_version,
        cc.server_date,
        cc.call_in_start_ts,
        cc.call_in_finish_ts,
        cc.trans_cred_vend_count,
        cc.counter_cred_vend_count,
        cc.call_in_credit_amount,
        cc.counter_credit_amount,
        cc.trans_cash_vend_count,
        cc.counter_cash_vend_count,
        cc.call_in_cash_amount,
        cc.counter_cash_amount,
        cc.trans_pass_vend_count,
        cc.counter_pass_vend_count,
        cc.call_in_pass_amount,
        cc.counter_pass_amount
        FROM tracking.counter_call_in_excpt@usadbp_tracking cc
        WHERE cc.server_date = (tDate - 1);
        
        COMMIT;
      
   --EXCEPTION
      
   END;

   
END;
/


-- End of DDL Script for Package TRACKING.PKG_RECONCILIATION
