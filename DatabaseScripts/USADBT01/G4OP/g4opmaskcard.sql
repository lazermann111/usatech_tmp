CREATE OR REPLACE FUNCTION G4OP.USAT_MASK_CARD_DATA
(
    l_track_data IN VARCHAR2
)
RETURN VARCHAR2 IS
   l_mask VARCHAR2(40);
   l_clean_card stg_credit.card%TYPE;
   l_pos NUMBER;
BEGIN
   l_clean_card := TRIM(l_track_data);
 l_mask := '****************************************';
    l_pos := NVL(INSTR(l_clean_card, '='), 0);
    IF l_pos > 13 THEN
        IF INSTR(UPPER(l_clean_card), '=PIN') = 0 THEN
RETURN SUBSTR(l_clean_card, 1, 2) || SUBSTR(l_mask,1,LENGTH(l_clean_card) - 6) ||SUBSTR(l_clean_card, l_pos - 4, 4);
   END IF;
    END IF;

    RETURN NVL(l_clean_card, ' ');
END;
/