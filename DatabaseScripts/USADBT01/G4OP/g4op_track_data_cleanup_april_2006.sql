update stg_credit set card = clean_track_data(card),
	raw_data = clean_raw_data(raw_data), 
	tran_data = clean_raw_data(tran_data)
where create_dt > '1-apr-2006'
	and (tran_state_cd <> 'N' or tran_auth_type_cd <> 'L' or authority_cd <> 'D')
/
exec clean_passaccess_track_data('1-apr-2006', sysdate);
/
update stg_refund set data4 = clean_track_data(data4) where create_dt > '1-apr-2006'
/
