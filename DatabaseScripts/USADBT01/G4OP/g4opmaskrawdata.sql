CREATE OR REPLACE FUNCTION G4OP.USAT_MASK_RAW_DATA
(
    l_raw_data IN stg_credit.raw_data%TYPE
)
RETURN VARCHAR2 IS
    l_track VARCHAR2(60);
    l_card VARCHAR2(40);
    ll_card VARCHAR2(40);
    l_mask VARCHAR2(40);
    l_clean_data stg_credit.raw_data%TYPE;
BEGIN
    l_mask := '****************************************';

    -- Extract the track data (9th position in CSV)
    l_track := REGEXP_SUBSTR(l_raw_data, '[^,]+', 1, 9);

    -- Extract the card # from the track data (split at equals)
    l_card := REGEXP_SUBSTR(l_track, '[^=]+', 1, 1);
    ll_card := REGEXP_SUBSTR(l_track, '[^=]+', 1, 2);

    -- Mask the card # (easier to do with SUBSTR than REGEX)
    l_card := SUBSTR(l_card, 1, 2) || SUBSTR(l_mask, 1, LENGTH(l_card) - 6) || SUBSTR(l_card, -4, 4);
    ll_card := SUBSTR(l_mask, 1, LENGTH(ll_card)) || SUBSTR(ll_card, -4, 4);
    -- Put masked card # back into track data
    l_track := REGEXP_REPLACE(l_track, '([^=]+)(=.*)?', l_card ||ll_card);

    -- Put the masked track data back into the raw data (CSV)
    l_clean_data := REGEXP_REPLACE(l_raw_data, '(([^,]+,){8})([^,]+)(,.*)', '\1' || l_track ||'\4');

    RETURN NVL(l_clean_data, ' ');
END;
/