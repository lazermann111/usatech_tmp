CREATE OR REPLACE FUNCTION REPORT.USAT_MASK_PASS_DATA
(
    l_track_data IN VARCHAR2
)
RETURN VARCHAR2 IS
   l_clean_card trans.card_number%TYPE;
   l_tt_id TRANS.TRANS_TYPE_ID%TYPE;
   l_mask VARCHAR2(40);
   l_pos NUMBER;
 BEGIN
   l_clean_card := TRIM(l_track_data);
   l_mask := '****************************************';
IF  l_tt_id = 22 OR l_tt_id = 18 OR l_tt_id = 23 OR LENGTH(l_clean_card) < 12 THEN
         RETURN l_clean_card;
         ELSE
         l_pos := NVL(INSTR(l_clean_card, '='), 0);
             IF l_pos > 13 AND INSTR(UPPER(l_clean_card), '=PIN') = 0 THEN
RETURN SUBSTR(l_clean_card, 1, 2) || SUBSTR(l_mask,1,LENGTH(l_clean_card) - 6) ||SUBSTR(l_clean_card, l_pos - 4, 4);
      END IF;
    END IF;
    RETURN NVL(l_clean_card, ' ');
END;
/