CREATE OR REPLACE PROCEDURE G4OP.process_import_credit
IS
    /*l_export_batch_id NUMBER;
    l_tran_total NUMBER(10);
    l_sale_total NUMBER(10, 2);

    CURSOR l_tran_cur IS SELECT
        e.express,
        e.location,
        e.trandate,
        e.trantime,
        e.apcode,
        e.sale,
        e.data1,
        e.merch,
        e.card,
        e.data2,
        e.startdate,
        e.starttime,
        e.data3,
        e.data4,
        e.srv1,
        e.cnt1,
        e.srv2,
        e.cnt2,
        e.srv3,
        e.cnt3,
        e.srv4,
        e.cnt4,
        e.srv5,
        e.cnt5,
        e.srv6,
        e.cnt6,
        e.machine_trans_no,
        NVL(h.created_ts, e.create_dt) AS received_dt
    FROM EXPORT_CREDIT@USADBP01_TAZDBA e, TRANSACTION_RECORD_HIST@USADBP01_TAZDBA h
    WHERE e.EXPORT_LOCK_IND = 'Y' AND e.machine_trans_no = h.machine_trans_no (+)
    ORDER BY e.ID;

    l_tran_rec l_tran_cur%ROWTYPE;*/
BEGIN
    /*UPDATE EXPORT_CREDIT@USADBP01_TAZDBA
    SET EXPORT_LOCK_IND = 'Y'
    WHERE EXPORT_BATCH_ID = 0;

    IF SQL%FOUND THEN
        FOR l_tran_rec IN l_tran_cur
        LOOP
            LOAD_IMPORT_CREDIT
            (
                l_tran_rec.express,
                l_tran_rec.location,
                l_tran_rec.trandate,
                l_tran_rec.trantime,
                l_tran_rec.apcode,
                l_tran_rec.sale,
                l_tran_rec.data1,
                l_tran_rec.merch,
                l_tran_rec.card,
                l_tran_rec.data2,
                l_tran_rec.startdate,
                l_tran_rec.starttime,
                l_tran_rec.data3,
                l_tran_rec.data4,
                l_tran_rec.srv1,
                l_tran_rec.cnt1,
                l_tran_rec.srv2,
                l_tran_rec.cnt2,
                l_tran_rec.srv3,
                l_tran_rec.cnt3,
                l_tran_rec.srv4,
                l_tran_rec.cnt4,
                l_tran_rec.srv5,
                l_tran_rec.cnt5,
                l_tran_rec.srv6,
                l_tran_rec.cnt6,
                l_tran_rec.machine_trans_no,
                TO_CHAR(l_tran_rec.received_dt, 'MM/DD/YYYY HH24:MI:SS'),
                'RERIX_IMPORT',
                'N'
            );
        END LOOP;

        SELECT EXPORT_CREDIT_BATCH_SEQ.NEXTVAL@USADBP01_TAZDBA
        INTO l_export_batch_id
        FROM DUAL;

        SELECT COUNT(1), SUM(TO_NUMBER(SALE)) / 100
        INTO l_tran_total, l_sale_total
        FROM EXPORT_CREDIT@USADBP01_TAZDBA
        WHERE EXPORT_LOCK_IND = 'Y';

        INSERT INTO EXPORT_CREDIT_HISTORY@USADBP01_TAZDBA(EXPORT_BATCH_ID, TRAN_TOTAL, SALE_TOTAL)
        VALUES(l_export_batch_id, l_tran_total, l_sale_total);

        UPDATE EXPORT_CREDIT@USADBP01_TAZDBA
        SET EXPORT_LOCK_IND = NULL, EXPORT_BATCH_ID = l_export_batch_id
        WHERE EXPORT_LOCK_IND = 'Y';

        COMMIT;
    END IF;*/
	NULL;
EXCEPTION
   WHEN OTHERS THEN
       ROLLBACK;
       RAISE;
END;
/
