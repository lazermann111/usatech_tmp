CREATE OR REPLACE FUNCTION G4OP.USAT_MASK_TRAN_DATA
(
    l_track_data IN VARCHAR2
)
RETURN VARCHAR2 IS
      l_clean_card stg_credit.tran_data%TYPE;
   l_card VARCHAR2(1000);
   l_track VARCHAR2(1000);
   ll_track VARCHAR2(1000);
   l_pos NUMBER;
   l_mask VARCHAR2(40);
BEGIN
    l_mask := '****************************************';
    l_clean_card := l_track_data;
    ll_track := REGEXP_SUBSTR(l_clean_card, '[^=]+', 1, 1);
    l_track := REGEXP_SUBSTR(l_clean_card, '[^=]+', 1, 2);
    l_card:= SUBSTR(l_mask, 1, 4)|| SUBSTR(l_track, 5, LENGTH(l_track));
    l_pos := NVL(INSTR(l_clean_card, '='), 0);
    IF l_pos > 13 THEN
        IF INSTR(UPPER(l_clean_card), '=PIN') = 0 THEN
RETURN SUBSTR(ll_track, 1, 2) || SUBSTR(l_mask,1,LENGTH(ll_track) - 6) ||SUBSTR(ll_track, l_pos - 4, 4)||l_card;
   END IF;
    END IF;

    RETURN NVL(l_clean_card, ' ');
END;
/