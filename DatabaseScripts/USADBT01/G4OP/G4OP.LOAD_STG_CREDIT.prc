CREATE OR REPLACE PROCEDURE LOAD_STG_CREDIT
(
    L_EXPRESS IN STG_CREDIT.EXPRESS%TYPE,
    L_LOCATION IN STG_CREDIT.LOCATION%TYPE,
    L_TRANDATE IN STG_CREDIT.TRANDATE%TYPE,
    L_TRANTIME IN STG_CREDIT.TRANTIME%TYPE,
    L_APCODE IN STG_CREDIT.APCODE%TYPE,
    L_SALE IN STG_CREDIT.SALE%TYPE,
    L_DATA1 IN STG_CREDIT.DATA1%TYPE,
    L_MERCH IN STG_CREDIT.MERCH%TYPE,
    L_CARD IN STG_CREDIT.CARD%TYPE,
    L_DATA2 IN STG_CREDIT.DATA2%TYPE,
    L_STARTDATE IN STG_CREDIT.STARTDATE%TYPE,
    L_STARTTIME IN STG_CREDIT.STARTTIME%TYPE,
    L_DATA3 IN STG_CREDIT.DATA3%TYPE,
    L_DATA4 IN STG_CREDIT.DATA4%TYPE,
    L_SRV1 IN STG_CREDIT.SRV1%TYPE,
    L_CNT1 IN STG_CREDIT.CNT1%TYPE,
    L_SRV2 IN STG_CREDIT.SRV2%TYPE,
    L_CNT2 IN STG_CREDIT.CNT2%TYPE,
    L_SRV3 IN STG_CREDIT.SRV3%TYPE,
    L_CNT3 IN STG_CREDIT.CNT3%TYPE,
    L_SRV4 IN STG_CREDIT.SRV4%TYPE,
    L_CNT4 IN STG_CREDIT.CNT4%TYPE,
    L_SRV5 IN STG_CREDIT.SRV5%TYPE,
    L_CNT5 IN STG_CREDIT.CNT5%TYPE,
    L_SRV6 IN STG_CREDIT.SRV6%TYPE,
    L_CNT6 IN STG_CREDIT.CNT6%TYPE,
    L_PREAUTH IN STG_CREDIT.PREAUTH%TYPE,
    L_TRANYEAR IN VARCHAR2,
    L_RAW_DATA IN STG_CREDIT.RAW_DATA%TYPE,
    L_TRAN_DATA IN STG_CREDIT.TRAN_DATA%TYPE,
    L_CANADA_IND IN STG_CREDIT.CANADA_IND%TYPE,
    L_LINK IN STG_CREDIT.LINK%TYPE,
    L_TRAN_STATE_CD IN STG_CREDIT.TRAN_STATE_CD%TYPE,
    L_CREATE_DT IN VARCHAR2
)
IS
    L_DUP_IND CHAR(1);
    L_MM VARCHAR(2);
    L_DD VARCHAR(2);
    L_HH24 VARCHAR(2);
    L_MI VARCHAR(2);
    L_SS VARCHAR(2);
    L_STARTTIME_MI VARCHAR(2);
    L_STARTTIME_SS VARCHAR(2);
    L_CORRECT_STARTTIME STG_CREDIT.STARTTIME%TYPE;
    L_CORRECT_TRANYEAR VARCHAR2(2);
    L_CORRECT_CREATE_DT STG_CREDIT.CREATE_DT%TYPE;
    L_TRAN_DT STG_CREDIT.TRAN_DT%TYPE;
    L_SECONDS_IND DEVICE_CONFIG.SECONDS_IND%TYPE;
    L_END_DT_IND DEVICE_CONFIG.END_DT_IND%TYPE;
    --L_ROW_COUNT NUMBER;
    L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
    L_NEW_TRAN_STATE_CD STG_CREDIT.TRAN_STATE_CD%TYPE;
BEGIN
    CHECK_DEVICE(L_DEVICE_SERIAL_CD => L_EXPRESS);

    IF L_TRAN_STATE_CD IS NULL OR L_TRAN_STATE_CD = ' ' THEN
        L_NEW_TRAN_STATE_CD := 'I';
    ELSE
        L_NEW_TRAN_STATE_CD := L_TRAN_STATE_CD;
    END IF;

    L_DUP_IND := 'N';
    L_MM := SUBSTR(L_TRANDATE, 1, INSTR(L_TRANDATE, '/') - 1);
    L_DD := SUBSTR(L_TRANDATE, INSTR(L_TRANDATE, '/') + 1);
    L_HH24 := SUBSTR(L_TRANTIME, 1, INSTR(L_TRANTIME, ':') - 1);
    L_MI := SUBSTR(L_TRANTIME, INSTR(L_TRANTIME, ':') + 1);
    L_CORRECT_STARTTIME := L_STARTTIME;
    IF L_TRANYEAR IS NULL OR L_TRANYEAR = ' ' THEN
        L_CORRECT_TRANYEAR := SUBSTR(L_CNT6, 3, 2);
    ELSE
        L_CORRECT_TRANYEAR := L_TRANYEAR;
    END IF;

    IF IS_NUMBER(L_HH24) THEN
        L_HH24 := LPAD(TO_CHAR(MOD(TO_NUMBER(L_HH24), 24)), 2, '0');
    ELSE
        L_HH24 := '00';
    END IF;

    IF IS_NUMBER(L_MI) THEN
        L_MI := LPAD(TO_CHAR(MOD(TO_NUMBER(L_MI), 60)), 2, '0');
    ELSE
        L_MI := '00';
    END IF;

    BEGIN
        IF L_CREATE_DT IS NULL OR L_CREATE_DT = ' ' THEN
            L_CORRECT_CREATE_DT := SYSDATE;
        ELSE
            IF LENGTH(L_CREATE_DT) = 10 THEN
                L_CORRECT_CREATE_DT := TO_DATE(SUBSTR(L_CREATE_DT, 1, 6), 'MMDDYY');
            ELSE
                L_CORRECT_CREATE_DT := TO_DATE(L_CREATE_DT, 'MMDDYYYYHH24MISS');
            END IF;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            L_CORRECT_CREATE_DT := SYSDATE;
    END;

    BEGIN
        SELECT
            D.DEVICE_TYPE_ID,
            NVL(DC.SECONDS_IND, 'Y'),
            NVL(DC.END_DT_IND, 'Y')
        INTO
            L_DEVICE_TYPE_ID,
            L_SECONDS_IND,
            L_END_DT_IND
        FROM DEVICE D
        LEFT OUTER JOIN DEVICE_CONFIG DC
        ON D.DEVICE_SERIAL_CD = DC.EXPRESS
        WHERE D.DEVICE_SERIAL_CD = L_EXPRESS AND D.DEVICE_ACTIVE_YN_FLAG = 'Y' AND ROWNUM < 2;

        IF L_CORRECT_TRANYEAR IS NULL OR L_CORRECT_TRANYEAR = ' ' OR L_DEVICE_TYPE_ID = 10 THEN --Transact
            IF TO_NUMBER(L_MM) > TO_NUMBER(TO_CHAR(SYSDATE, 'MM')) THEN -- transaction month > current month
                L_CORRECT_TRANYEAR := TO_CHAR(ADD_MONTHS(SYSDATE, -12), 'YY'); -- previous year
            ELSE
                L_CORRECT_TRANYEAR := TO_CHAR(SYSDATE, 'YY'); -- current year
            END IF;
        END IF;

        IF L_DEVICE_TYPE_ID = 10 THEN --Transact
            L_SS := '00';
        ELSE
            IF INSTR(L_STARTTIME, ':') > 0 THEN
                L_STARTTIME_MI := SUBSTR(L_STARTTIME, 1, INSTR(L_STARTTIME, ':') - 1);
                IF IS_NUMBER(L_STARTTIME_MI) THEN
                    L_STARTTIME_MI := LPAD(TO_CHAR(MOD(TO_NUMBER(L_STARTTIME_MI), 60)), 2, '0');
                ELSE
                    L_STARTTIME_MI := '00';
                END IF;

                L_STARTTIME_SS := SUBSTR(L_STARTTIME, INSTR(L_STARTTIME, ':') + 1);
                IF IS_NUMBER(L_STARTTIME_SS) THEN
                    L_STARTTIME_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(L_STARTTIME_SS), 60)), 2, '0');
                ELSE
                    L_STARTTIME_SS := '00';
                END IF;

                L_CORRECT_STARTTIME := L_STARTTIME_MI || ':' || L_STARTTIME_SS;

                SELECT DECODE(L_SECONDS_IND, 'Y', SUBSTR(L_STARTTIME, 4, 2), '00') INTO L_SS FROM DUAL;

                IF IS_NUMBER(L_SS) THEN
                    L_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(L_SS), 60)), 2, '0');
                ELSE
                    L_SS := '00';
                END IF;
            END IF;
        END IF;

        IF IS_NUMBER(L_MM)
            AND IS_NUMBER(L_DD)
            AND IS_NUMBER(L_CORRECT_TRANYEAR)
            AND TO_NUMBER(L_MM) <= 12
            AND TO_NUMBER(L_DD) <= MONTH_NUM_DAYS(L_MONTH => L_MM, L_YEAR => L_CORRECT_TRANYEAR) THEN

            IF L_NEW_TRAN_STATE_CD <> 'S' THEN
                L_NEW_TRAN_STATE_CD := 'N';
            END IF;

            L_TRAN_DT := TO_DATE(L_MM || '/' || L_DD || '/' || L_CORRECT_TRANYEAR || ' ' || L_HH24 || ':' || L_MI || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');

            /*
            IF L_DEVICE_TYPE_ID = 10 THEN --Transact
                IF L_END_DT_IND = 'Y' THEN
                    SELECT COUNT(ID)
                    INTO L_ROW_COUNT
                    FROM STG_CREDIT
                    WHERE EXPRESS = L_EXPRESS
                        AND TRANDATE = L_TRANDATE
                        AND TRANTIME = L_TRANTIME
                        AND STARTDATE = L_STARTDATE
                        AND STARTTIME = L_STARTTIME;
                ELSE
                    SELECT COUNT(ID)
                    INTO L_ROW_COUNT
                    FROM STG_CREDIT
                    WHERE EXPRESS = L_EXPRESS
                        AND TRANDATE = L_TRANDATE
                        AND TRANTIME = L_TRANTIME;
                END IF;

                IF L_ROW_COUNT > 0 THEN
                    UPDATE STG_CREDIT
                    SET DUP_COUNT = DUP_COUNT + 1
                    WHERE EXPRESS = L_EXPRESS AND RAW_DATA = L_RAW_DATA;

                    IF SQL%FOUND THEN
                        L_DUP_IND := 'Y';
                    END IF;
                END IF;
            ELSE
                IF L_SECONDS_IND = 'Y' THEN
                    UPDATE STG_CREDIT
                    SET DUP_COUNT = DUP_COUNT + 1
                    WHERE EXPRESS = L_EXPRESS AND TRAN_DT = L_TRAN_DT;

                    IF SQL%FOUND THEN
                        L_DUP_IND := 'Y';
                    END IF;
                ELSE
                    SELECT COUNT(ID)
                    INTO L_ROW_COUNT
                    FROM STG_CREDIT
                    WHERE EXPRESS = L_EXPRESS
                        AND TRAN_DT = L_TRAN_DT
                        AND SALE = L_SALE
                        AND CARD = L_CARD;

                    IF L_ROW_COUNT > 0 THEN
                        UPDATE STG_CREDIT
                        SET DUP_COUNT = DUP_COUNT + 1
                        WHERE EXPRESS = L_EXPRESS AND RAW_DATA = L_RAW_DATA;

                        IF SQL%FOUND THEN
                            L_DUP_IND := 'Y';
                        END IF;
                    END IF;
                END IF;
            END IF;
            */
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
    END;

    UPDATE STG_CREDIT
    SET DUP_COUNT = DUP_COUNT + 1
    WHERE EXPRESS = L_EXPRESS AND RAW_DATA = L_RAW_DATA;

    IF SQL%FOUND THEN
        L_DUP_IND := 'Y';
    END IF;

    IF L_DUP_IND = 'N' THEN
        INSERT INTO STG_CREDIT(
            EXPRESS,
            LOCATION,
            TRANDATE,
            TRANTIME,
            APCODE,
            SALE,
            DATA1,
            MERCH,
            CARD,
            DATA2,
            STARTDATE,
            STARTTIME,
            DATA3,
            DATA4,
            SRV1,
            CNT1,
            SRV2,
            CNT2,
            SRV3,
            CNT3,
            SRV4,
            CNT4,
            SRV5,
            CNT5,
            SRV6,
            CNT6,
            RAW_DATA,
            TRAN_DATA,
            TRAN_DT,
            CANADA_IND,
            TRAN_STATE_CD,
            LINK,
            CREATE_DT,
            PREAUTH
        )
        VALUES(
            L_EXPRESS,
            L_LOCATION,
            L_MM || '/' || L_DD,
            L_HH24 || ':' || L_MI,
            L_APCODE,
            L_SALE,
            L_DATA1,
            L_MERCH,
            L_CARD,
            L_DATA2,
            L_STARTDATE,
            L_CORRECT_STARTTIME,
            L_DATA3,
            L_DATA4,
            L_SRV1,
            L_CNT1,
            L_SRV2,
            L_CNT2,
            L_SRV3,
            L_CNT3,
            L_SRV4,
            L_CNT4,
            L_SRV5,
            L_CNT5,
            L_SRV6,
            L_CNT6,
            L_RAW_DATA,
            L_TRAN_DATA,
            L_TRAN_DT,
            L_CANADA_IND,
            L_NEW_TRAN_STATE_CD,
            L_LINK,
            L_CORRECT_CREATE_DT,
            L_PREAUTH
        );
    END IF;
END LOAD_STG_CREDIT;
