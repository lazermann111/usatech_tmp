-- Start of DDL Script for Sequence G4OP.STG_BATCH_MERCHANT_SEQ
-- Generated 1-Feb-2005 11:23:47 from G4OP@USADBD03

CREATE SEQUENCE g4op.stg_batch_merchant_seq
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  CACHE 20
/

-- Start of DDL Script for Table G4OP.STG_BATCH_MERCHANT
-- Generated 1-Feb-2005 11:29:15 from G4OP@USADBD03

CREATE TABLE g4op.stg_batch_merchant
    (batch_number                   NUMBER,
    merch                          VARCHAR2(30),
    authority_cd                   CHAR(1),
    tran_auth_type_cd              VARCHAR2(1),
    create_dt                      DATE DEFAULT SYSDATE  NOT NULL,
    id                             NUMBER NOT NULL)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  g4op_data
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/




-- Indexes for STG_BATCH_MERCHANT

CREATE INDEX ix_batch_merch_create_auth_typ ON g4op.stg_batch_merchant
  (
    create_dt                       ASC,
    authority_cd                    ASC,
    tran_auth_type_cd               ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  g4op_indx
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/



-- Constraints for STG_BATCH_MERCHANT

ALTER TABLE g4op.stg_batch_merchant
ADD CONSTRAINT pk_stg_batch_merchant PRIMARY KEY (id)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  g4op_data
  STORAGE   (
    INITIAL     65536
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/


-- Triggers for STG_BATCH_MERCHANT

CREATE OR REPLACE TRIGGER trbi_stg_batch_merchant
 BEFORE
  INSERT
 ON g4op.stg_batch_merchant
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT STG_BATCH_MERCHANT_SEQ.NEXTVAL
    INTO :NEW.ID
    FROM DUAL;
END;
/


-- End of DDL Script for Table G4OP.STG_BATCH_MERCHANT

