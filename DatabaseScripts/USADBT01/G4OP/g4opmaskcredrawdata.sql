CREATE OR REPLACE FUNCTION G4OP.USAT_MASK_CREDRAW_DATA
(
    l_track_data IN VARCHAR2
)
RETURN VARCHAR2 IS
   l_clean_card stg_CREDIT.raw_data%TYPE;
   l_mask VARCHAR2(40);
   l_card VARCHAR2(400);
   ll_card VARCHAR2(400);
   l_pos NUMBER;
 BEGIN
   l_clean_card := trim(l_track_data);
   ll_card := REGEXP_SUBSTR(l_track_data, '[^,]+', 8, 9);
   l_mask := '****************************************';
l_card := REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 10)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 11)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 12)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 13)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 14)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 15)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 16)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 17)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 18)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 19)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 20)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 21)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 22)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 23)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 24)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 25)||
REGEXP_SUBSTR(l_track_data, '[^,]+,', 1, 26)||
REGEXP_SUBSTR(l_track_data, '[^,]+', 1, 27);
IF LENGTH(ll_card) < 12 THEN
         RETURN l_clean_card;
         ELSE
         l_pos := NVL(INSTR(ll_card, '='), 0);
             IF l_pos > 13 AND INSTR(UPPER(ll_card), '=PIN') = 0 THEN
RETURN  REGEXP_REPLACE(l_track_data, '(([^,]+,){8})([^,]+)(,.*)', '\1')||SUBSTR(ll_card, 1, 2) || SUBSTR(l_mask,1,LENGTH(ll_card) - 6) ||SUBSTR(ll_card, l_pos - 4, 4)||' '||l_card;
ELSE
 RETURN  REGEXP_REPLACE(l_track_data, '(([^,]+,){8})([^,]+)(,.*)', '\1')||SUBSTR(ll_card, 1, 2) || SUBSTR(l_mask,1,LENGTH(ll_card) - 6) || SUBSTR(ll_card, l_pos - 4, 4)||' '||l_card;
      END IF;
    END IF;
    RETURN NVL(l_clean_card, ' ');
END;
/