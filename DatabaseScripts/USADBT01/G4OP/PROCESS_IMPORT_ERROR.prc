CREATE OR REPLACE PROCEDURE G4OP.process_import_error
IS
    /*l_export_batch_id NUMBER;
    l_tran_total NUMBER(10);

    CURSOR l_tran_cur IS SELECT
        express,
        location,
        errdate,
        errtime,
        msg,
        machine_trans_no,
        create_dt AS received_dt
    FROM EXPORT_ERROR@USADBP01_TAZDBA
    WHERE EXPORT_LOCK_IND = 'Y'
    ORDER BY ID;

    l_tran_rec l_tran_cur%ROWTYPE;*/
BEGIN
    /*UPDATE EXPORT_ERROR@USADBP01_TAZDBA
    SET EXPORT_LOCK_IND = 'Y'
    WHERE EXPORT_BATCH_ID = 0;

    IF SQL%FOUND THEN
        FOR l_tran_rec IN l_tran_cur
        LOOP
            LOAD_IMPORT_ERROR
            (
                l_tran_rec.express,
                l_tran_rec.location,
                l_tran_rec.errdate,
                l_tran_rec.errtime,
                l_tran_rec.msg,
                l_tran_rec.machine_trans_no,
                TO_CHAR(l_tran_rec.received_dt, 'MM/DD/YYYY HH24:MI:SS'),
                'RERIX_IMPORT',
                'N'
            );
        END LOOP;

        SELECT EXPORT_ERROR_BATCH_SEQ.NEXTVAL@USADBP01_TAZDBA
        INTO l_export_batch_id
        FROM DUAL;

        SELECT COUNT(1)
        INTO l_tran_total
        FROM EXPORT_ERROR@USADBP01_TAZDBA
        WHERE EXPORT_LOCK_IND = 'Y';

        INSERT INTO EXPORT_ERROR_HISTORY@USADBP01_TAZDBA(EXPORT_BATCH_ID, TRAN_TOTAL)
        VALUES(l_export_batch_id, l_tran_total);

        UPDATE EXPORT_ERROR@USADBP01_TAZDBA
        SET EXPORT_LOCK_IND = NULL, EXPORT_BATCH_ID = l_export_batch_id
        WHERE EXPORT_LOCK_IND = 'Y';

        COMMIT;
    END IF;*/
	NULL;
EXCEPTION
   WHEN OTHERS THEN
       ROLLBACK;
       RAISE;
END;
/
