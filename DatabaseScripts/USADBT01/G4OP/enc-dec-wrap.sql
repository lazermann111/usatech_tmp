 /u01/app/oracle/product/10.2.0/usatwork > 

wrap iname=/u01/app/oracle/product/10.2.0/usatwork/usatenc.sql

PL/SQL Wrapper: Release 10.2.0.2.0- 64bit Production on Tue Sep 15 10:14:27 2009

Copyright (c) 1993, 2004, Oracle.  All rights reserved.

Processing /u01/app/oracle/product/10.2.0/usatwork/usatenc.sql to usatenc.plb

/u01/app/oracle/product/10.2.0/usatwork > 

wrap iname=/u01/app/oracle/product/10.2.0/usatwork/usatDenc.sql

PL/SQL Wrapper: Release 10.2.0.2.0- 64bit Production on Tue Sep 15 10:14:40 2009

Copyright (c) 1993, 2004, Oracle.  All rights reserved.

Processing /u01/app/oracle/product/10.2.0/usatwork/usatDenc.sql to usatDenc.plb
[522:oracle.usadbt01] /u01/app/oracle/product/10.2.0/usatwork >

SQL> @/u01/app/oracle/product/10.2.0/usatwork/usatenc.plb

Function created.

SQL> @/u01/app/oracle/product/10.2.0/usatwork/usatDenc.plb

Function created.

SQL>

grant execute on dbadmin.usat_get_enc_val to g4op;
grant execute on dbadmin.usat_get_Denc_val to g4op;


CREATE OR REPLACE function DBADMIN.usat_get_enc_val
    (
       input_string    in varchar2

    )
    return raw is
    encrypted_raw RAW(2000);
    encrypted_string VARCHAR2(2000);
    raw_key RAW(32) := '-------------------';
       encryption_type    PLS_INTEGER :=          -- total encryption type
                            DBMS_CRYPTO.ENCRYPT_AES128
                          + DBMS_CRYPTO.CHAIN_CBC
                          + DBMS_CRYPTO.PAD_PKCS5;
   begin
      encrypted_raw := dbms_crypto.encrypt
        (
            UTL_I18N.STRING_TO_RAW
               (input_string, 'US7ASCII'),
            encryption_type,
           raw_key
         );
     return encrypted_raw;
  end;
/


CREATE OR REPLACE function DBADMIN.usat_get_Denc_val
    (
       input_string in raw

    )
    return varchar2 is
    decrypted_raw RAW(2000);
    decrypted_string VARCHAR2(2000);
    raw_key RAW(32):='-----------------------';
       encryption_type    PLS_INTEGER :=          -- total encryption type
                            DBMS_CRYPTO.ENCRYPT_AES128
                          + DBMS_CRYPTO.CHAIN_CBC
                          + DBMS_CRYPTO.PAD_PKCS5;
   begin
  -- select usat_EK into raw_key from g4op.usat_tab_sec;
      decrypted_raw := dbms_crypto.DECRYPT
        (
            input_string,
            encryption_type,
           raw_key
         );
         decrypted_string:= UTL_I18N.RAW_TO_CHAR
               (decrypted_raw, 'US7ASCII');
     return decrypted_string;
  end;
/
