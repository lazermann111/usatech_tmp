--USADBT
prompt for stg_credit

update g4op.stg_credit set card = G4OP.USAT_MASK_CARD_DATA(card)
 where create_dt < sysdate-90
/
commit;

update g4op.stg_credit set tran_data = G4OP.USAT_MASK_TRAN_DATA(tran_data)
 where create_dt < sysdate-90
/
commit;
update g4op.stg_credit set raw_data= G4OP.USAT_MASK_CREDRAW_DATA(raw_data)
 where create_dt < sysdate-90
/
commit;


promt for stg_refund

update g4op.stg_refund set data4 = G4OP.USAT_MASK_CARD_DATA(data4)
 where create_dt < sysdate-30
/
commit;

prompt G4OP.STG_PASSACCESS


update g4op.stg_passaccess set card = G4OP.USAT_MASK_PASS_DATA(card)
 where create_dt < sysdate-30;
commit;

update g4op.stg_passaccess set tran_data= G4OP.USAT_MASK_PASSTRAN_DATA(tran_data)
 where create_dt < sysdate-30;
commit;

update g4op.stg_passaccess set raw_data= G4OP.USAT_MASK_PASSRAW_DATA(raw_data)
 where create_dt < sysdate-30;
commit
/

prompt --- dropping useless tables

drop table G4OP.STG_PASSACCESS_DUP cascade constraints;
drop table G4OP.STG_REFUND_TEMP cascade constraints;