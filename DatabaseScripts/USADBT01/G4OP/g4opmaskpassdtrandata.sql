CREATE OR REPLACE FUNCTION G4OP.USAT_MASK_PASSTRAN_DATA
(
    l_track_data IN VARCHAR2
)
RETURN VARCHAR2 IS
   l_clean_card stg_passaccess.tran_data%TYPE;
   l_mask VARCHAR2(40);
   ll_card VARCHAR2(100);
   l_card VARCHAR2(100);
   l_pos NUMBER;
 BEGIN
    l_clean_card := trim(l_track_data);
   ll_card := REGEXP_SUBSTR(l_track_data, '[^ ]+', 1, 1);
   l_mask := '****************************************';
 l_card := REGEXP_SUBSTR(l_track_data, '[^ ]+ ', 1, 2)||REGEXP_SUBSTR(l_track_data, '[^ ]+', 2, 3)|| REGEXP_SUBSTR(l_track_data, '[^ ]+', 3, 4);
IF LENGTH(ll_card) < 12 THEN
         RETURN l_clean_card;
         ELSE
         l_pos := NVL(INSTR(ll_card, '='), 0);
             IF l_pos > 13 AND INSTR(UPPER(ll_card), '=PIN') = 0 THEN
RETURN SUBSTR(ll_card, 1, 2) || SUBSTR(l_mask,1,LENGTH(ll_card) - 6) ||SUBSTR(ll_card, l_pos - 4, 4)||' '||l_card;
ELSE
 RETURN SUBSTR(ll_card, 1, 2) || SUBSTR(l_mask,1,LENGTH(ll_card) - 6) || SUBSTR(ll_card, l_pos - 4, 4)||' '||l_card;
      END IF;
    END IF;
    RETURN NVL(l_clean_card, ' ');
END;
/