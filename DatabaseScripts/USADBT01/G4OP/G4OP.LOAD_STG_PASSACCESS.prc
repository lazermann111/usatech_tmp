CREATE OR REPLACE PROCEDURE LOAD_STG_PASSACCESS
(
    L_EXPRESS IN STG_PASSACCESS.EXPRESS%TYPE,
    L_LOCATION IN STG_PASSACCESS.LOCATION%TYPE,
    L_TRANDATE IN STG_PASSACCESS.TRANDATE%TYPE,
    L_TRANTIME IN STG_PASSACCESS.TRANTIME%TYPE,
    L_CARD IN STG_PASSACCESS.CARD%TYPE,
    L_SALE IN STG_PASSACCESS.SALE%TYPE,
    L_DATA3 IN STG_PASSACCESS.DATA3%TYPE,
    L_DATA4 IN STG_PASSACCESS.DATA4%TYPE,
    L_STOPDATE IN STG_PASSACCESS.STOPDATE%TYPE,
    L_STOPTIME IN STG_PASSACCESS.STOPTIME%TYPE,
    L_DATA5 IN STG_PASSACCESS.DATA5%TYPE,
    L_DATA6 IN STG_PASSACCESS.DATA6%TYPE,
    L_SRV1 IN STG_PASSACCESS.SRV1%TYPE,
    L_CNT1 IN STG_PASSACCESS.CNT1%TYPE,
    L_SRV2 IN STG_PASSACCESS.SRV2%TYPE,
    L_CNT2 IN STG_PASSACCESS.CNT2%TYPE,
    L_SRV3 IN STG_PASSACCESS.SRV3%TYPE,
    L_CNT3 IN STG_PASSACCESS.CNT3%TYPE,
    L_SRV4 IN STG_PASSACCESS.SRV4%TYPE,
    L_CNT4 IN STG_PASSACCESS.CNT4%TYPE,
    L_SRV5 IN STG_PASSACCESS.SRV5%TYPE,
    L_CNT5 IN STG_PASSACCESS.CNT5%TYPE,
    L_SRV6 IN STG_PASSACCESS.SRV6%TYPE,
    L_CNT6 IN STG_PASSACCESS.CNT6%TYPE,
    L_TRANYEAR IN VARCHAR2,
    L_RAW_DATA IN STG_PASSACCESS.RAW_DATA%TYPE,
    L_TRAN_DATA IN STG_PASSACCESS.TRAN_DATA%TYPE,
    L_CANADA_IND IN STG_PASSACCESS.CANADA_IND%TYPE,
    L_LINK IN STG_PASSACCESS.LINK%TYPE,
    L_CREATE_DT IN VARCHAR2
)
IS
    L_TRAN_STATE_CD STG_PASSACCESS.TRAN_STATE_CD%TYPE;
    L_DUP_IND CHAR(1);
    L_MM VARCHAR(2);
    L_DD VARCHAR(2);
    L_HH24 VARCHAR(2);
    L_MI VARCHAR(2);
    L_SS VARCHAR(2);
    L_STOPTIME_MI VARCHAR(2);
    L_STOPTIME_SS VARCHAR(2);
    L_CORRECT_STOPTIME STG_PASSACCESS.STOPTIME%TYPE;
    L_CORRECT_TRANYEAR VARCHAR2(2);
    L_TRAN_DT STG_PASSACCESS.TRAN_DT%TYPE;
    L_SECONDS_IND DEVICE_CONFIG.SECONDS_IND%TYPE;
    L_END_DT_IND DEVICE_CONFIG.END_DT_IND%TYPE;
    --L_ROW_COUNT NUMBER;
    L_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
    L_CORRECT_CREATE_DT STG_PASSACCESS.CREATE_DT%TYPE;
BEGIN
    CHECK_DEVICE(L_DEVICE_SERIAL_CD => L_EXPRESS);

    L_TRAN_STATE_CD := 'I';
    L_DUP_IND := 'N';
    L_MM := SUBSTR(L_TRANDATE, 1, INSTR(L_TRANDATE, '/') - 1);
    L_DD := SUBSTR(L_TRANDATE, INSTR(L_TRANDATE, '/') + 1);
    L_HH24 := SUBSTR(L_TRANTIME, 1, INSTR(L_TRANTIME, ':') - 1);
    L_MI := SUBSTR(L_TRANTIME, INSTR(L_TRANTIME, ':') + 1);
    L_CORRECT_STOPTIME := L_STOPTIME;
    IF L_TRANYEAR IS NULL OR L_TRANYEAR = ' ' THEN
        L_CORRECT_TRANYEAR := SUBSTR(L_CNT6, 3, 2);
    ELSE
        L_CORRECT_TRANYEAR := L_TRANYEAR;
    END IF;

    IF IS_NUMBER(L_HH24) THEN
        L_HH24 := LPAD(TO_CHAR(MOD(TO_NUMBER(L_HH24), 24)), 2, '0');
    ELSE
        L_HH24 := '00';
    END IF;

    IF IS_NUMBER(L_MI) THEN
        L_MI := LPAD(TO_CHAR(MOD(TO_NUMBER(L_MI), 60)), 2, '0');
    ELSE
        L_MI := '00';
    END IF;

    BEGIN
        IF L_CREATE_DT IS NULL OR L_CREATE_DT = ' ' THEN
            L_CORRECT_CREATE_DT := SYSDATE;
        ELSE
            IF LENGTH(L_CREATE_DT) = 10 THEN
                L_CORRECT_CREATE_DT := TO_DATE(SUBSTR(L_CREATE_DT, 1, 6), 'MMDDYY');
            ELSE
                L_CORRECT_CREATE_DT := TO_DATE(L_CREATE_DT, 'MMDDYYYYHH24MISS');
            END IF;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            L_CORRECT_CREATE_DT := SYSDATE;
    END;

    BEGIN
        SELECT
            D.DEVICE_TYPE_ID,
            NVL(DC.SECONDS_IND, 'Y'),
            NVL(DC.END_DT_IND, 'Y')
        INTO
            L_DEVICE_TYPE_ID,
            L_SECONDS_IND,
            L_END_DT_IND
        FROM DEVICE D
        LEFT OUTER JOIN DEVICE_CONFIG DC
        ON D.DEVICE_SERIAL_CD = DC.EXPRESS
        WHERE D.DEVICE_SERIAL_CD = L_EXPRESS AND D.DEVICE_ACTIVE_YN_FLAG = 'Y' AND ROWNUM < 2;

        IF L_CORRECT_TRANYEAR = ' ' OR L_DEVICE_TYPE_ID = 10 THEN --Transact
            IF TO_NUMBER(L_MM) > TO_NUMBER(TO_CHAR(SYSDATE, 'MM')) THEN -- transaction month > current month
                L_CORRECT_TRANYEAR := TO_CHAR(ADD_MONTHS(SYSDATE, -12), 'YY'); -- previous year
            ELSE
                L_CORRECT_TRANYEAR := TO_CHAR(SYSDATE, 'YY'); -- current year
            END IF;
        END IF;

        IF L_DEVICE_TYPE_ID = 10 THEN --Transact
            L_SS := '00';
        ELSE
            IF INSTR(L_STOPTIME, ':') > 0 THEN
                L_STOPTIME_MI := SUBSTR(L_STOPTIME, 1, INSTR(L_STOPTIME, ':') - 1);
                IF IS_NUMBER(L_STOPTIME_MI) THEN
                    L_STOPTIME_MI := LPAD(TO_CHAR(MOD(TO_NUMBER(L_STOPTIME_MI), 60)), 2, '0');
                ELSE
                    L_STOPTIME_MI := '00';
                END IF;

                L_STOPTIME_SS := SUBSTR(L_STOPTIME, INSTR(L_STOPTIME, ':') + 1);
                IF IS_NUMBER(L_STOPTIME_SS) THEN
                    L_STOPTIME_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(L_STOPTIME_SS), 60)), 2, '0');
                ELSE
                    L_STOPTIME_SS := '00';
                END IF;

                L_CORRECT_STOPTIME := L_STOPTIME_MI || ':' || L_STOPTIME_SS;

                SELECT DECODE(L_SECONDS_IND, 'Y', SUBSTR(L_CORRECT_STOPTIME, 4, 2), '00') INTO L_SS FROM DUAL;

                IF IS_NUMBER(L_SS) THEN
                    L_SS := LPAD(TO_CHAR(MOD(TO_NUMBER(L_SS), 60)), 2, '0');
                ELSE
                    L_SS := '00';
                END IF;
            END IF;
        END IF;

        IF IS_NUMBER(L_MM)
            AND IS_NUMBER(L_DD)
            AND IS_NUMBER(L_CORRECT_TRANYEAR)
            AND TO_NUMBER(L_MM) <= 12
            AND TO_NUMBER(L_DD) <= MONTH_NUM_DAYS(L_MONTH => L_MM, L_YEAR => L_CORRECT_TRANYEAR) THEN

            L_TRAN_STATE_CD := 'N';

            L_TRAN_DT := TO_DATE(L_MM || '/' || L_DD || '/' || L_CORRECT_TRANYEAR || ' ' || L_HH24 || ':' || L_MI || ':' || L_SS, 'MM/DD/YY HH24:MI:SS');

            /*
            IF L_DEVICE_TYPE_ID = 10 THEN --Transact
                IF L_END_DT_IND = 'Y' THEN
                    SELECT COUNT(ID)
                    INTO L_ROW_COUNT
                    FROM STG_PASSACCESS
                    WHERE EXPRESS = L_EXPRESS
                        AND TRANDATE = L_TRANDATE
                        AND TRANTIME = L_TRANTIME
                        AND STOPDATE = L_STOPDATE
                        AND STOPTIME = L_STOPTIME;
                ELSE
                    SELECT COUNT(ID)
                    INTO L_ROW_COUNT
                    FROM STG_PASSACCESS
                    WHERE EXPRESS = L_EXPRESS
                        AND TRANDATE = L_TRANDATE
                        AND TRANTIME = L_TRANTIME;
                END IF;

                IF L_ROW_COUNT > 0 THEN
                    UPDATE STG_PASSACCESS
                    SET DUP_COUNT = DUP_COUNT + 1
                    WHERE EXPRESS = L_EXPRESS AND RAW_DATA = L_RAW_DATA;

                    IF SQL%FOUND THEN
                        L_DUP_IND := 'Y';
                    END IF;
                END IF;
            ELSE
                IF L_SECONDS_IND = 'Y' THEN
                    UPDATE STG_PASSACCESS
                    SET DUP_COUNT = DUP_COUNT + 1
                    WHERE EXPRESS = L_EXPRESS AND TRAN_DT = L_TRAN_DT;

                    IF SQL%FOUND THEN
                        L_DUP_IND := 'Y';
                    END IF;
                ELSE
                    SELECT COUNT(ID)
                    INTO L_ROW_COUNT
                    FROM STG_PASSACCESS
                    WHERE EXPRESS = L_EXPRESS
                        AND TRAN_DT = L_TRAN_DT
                        AND SALE = L_SALE
                        AND CARD = L_CARD;

                    IF L_ROW_COUNT > 0 THEN
                        UPDATE STG_PASSACCESS
                        SET DUP_COUNT = DUP_COUNT + 1
                        WHERE EXPRESS = L_EXPRESS AND RAW_DATA = L_RAW_DATA;

                        IF SQL%FOUND THEN
                            L_DUP_IND := 'Y';
                        END IF;
                    END IF;
                END IF;
            END IF;
            */
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
    END;

    UPDATE STG_PASSACCESS
    SET DUP_COUNT = DUP_COUNT + 1
    WHERE EXPRESS = L_EXPRESS AND RAW_DATA = L_RAW_DATA;

    IF SQL%FOUND THEN
        L_DUP_IND := 'Y';
    END IF;

    IF L_DUP_IND = 'N' THEN
        INSERT INTO STG_PASSACCESS(
            EXPRESS,
            LOCATION,
            TRANDATE,
            TRANTIME,
            SALE,
            CARD,
            STOPDATE,
            STOPTIME,
            DATA3,
            DATA4,
            DATA5,
            DATA6,
            SRV1,
            CNT1,
            SRV2,
            CNT2,
            SRV3,
            CNT3,
            SRV4,
            CNT4,
            SRV5,
            CNT5,
            SRV6,
            CNT6,
            RAW_DATA,
            TRAN_DATA,
            TRAN_DT,
            TRAN_STATE_CD,
            CANADA_IND,
            LINK,
            CREATE_DT
        )
        VALUES(
            L_EXPRESS,
            L_LOCATION,
            L_MM || '/' || L_DD,
            L_HH24 || ':' || L_MI,
            L_SALE,
            L_CARD,
            L_STOPDATE,
            L_CORRECT_STOPTIME,
            L_DATA3,
            L_DATA4,
            L_DATA5,
            L_DATA6,
            L_SRV1,
            L_CNT1,
            L_SRV2,
            L_CNT2,
            L_SRV3,
            L_CNT3,
            L_SRV4,
            L_CNT4,
            L_SRV5,
            L_CNT5,
            L_SRV6,
            L_CNT6,
            L_RAW_DATA,
            L_TRAN_DATA,
            L_TRAN_DT,
            L_TRAN_STATE_CD,
            L_CANADA_IND,
            L_LINK,
            L_CORRECT_CREATE_DT
        );
    END IF;
END LOAD_STG_PASSACCESS;
