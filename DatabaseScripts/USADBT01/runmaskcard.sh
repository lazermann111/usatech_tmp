#  Created by           Marie Njanje
#
#  # ------------------------------------------------------------------

. /export/home/oracle/.profile

sqlplus -s <<EOF > /export/home/oracle/log/dailyrunmaskpass.out
/ as sysdba
set echo on;
set timing on;
set feedback on;

prompt for stg_passaccess
update /*+ index(p USA_IX_SP_CREATE_DT) */ g4op.stg_passaccess set card = DBADMIN.USAT_GET_ENC_VAL(DBADMIN.MASK_CREDIT_CARD(DBADMIN.USAT_GET_DENC_VAL(card))), raw_data = null, tran_data = null where create_dt between sysdate - 21 and sysdate - 14;
commit;

prompt for stg_refund
update g4op.stg_refund set data4 = DBADMIN.USAT_GET_ENC_VAL(DBADMIN.MASK_CREDIT_CARD(DBADMIN.USAT_GET_DENC_VAL(data4))) where tran_state_cd <> 'N' and create_dt > sysdate - 7;
commit;

prompt for stg_credit
update /*+ index (t IX_STG_CREDIT_CREATE_DT) */ g4op.stg_credit t set card = DBADMIN.USAT_GET_ENC_VAL(DBADMIN.MASK_CREDIT_CARD(DBADMIN.USAT_GET_DENC_VAL(card))), raw_data = null, tran_data = null where create_dt between sysdate - 97 and sysdate - 90 and tran_state_cd = 'S';
update /*+ index (t IX_STG_CREDIT_CREATE_DT) */ g4op.stg_credit t set card = DBADMIN.USAT_GET_ENC_VAL(DBADMIN.MASK_CREDIT_CARD(DBADMIN.USAT_GET_DENC_VAL(card))), raw_data = null, tran_data = null where create_dt between sysdate - 21 and sysdate - 14 and tran_state_cd not in ('S','N');
commit;

exit;
EOF


