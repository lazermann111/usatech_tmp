CREATE ROLE quartz_user LOGIN PASSWORD 'quartz_user';

CREATE TABLESPACE quartz_data LOCATION '/opt/USAT/postgres/data/tblspace/quartz_data';

GRANT CREATE ON TABLESPACE quartz_data TO quartz_user;
