CREATE UNIQUE INDEX device.udx_device_def_active_record ON device.device_default (DECODE(device_default_active_yn_flag, 'Y', device_default_name, NULL)) TABLESPACE DEVICE_INDX;

DECLARE
 l_us_mid VARCHAR2(30) := '000000020030003';
 l_us_tid VARCHAR2(30) := '00010850';
 --l_ca_mid VARCHAR2(30) := '000000020030003';
 --l_ca_tid VARCHAR2(30) := '00010850';
 --l_ppt_id PSS.POS_PTA_TMPL.POS_PTA_TMPL_ID%TYPE;
BEGIN
UPDATE PSS.POS_PTA_TMPL SET POS_PTA_TMPL_NAME = 'DEFAULT: Edge Initialization - US', POS_PTA_TMPL_DESC = 'Default template for Edge in US during initialization'
WHERE POS_PTA_TMPL_ID = 13;

DELETE FROM PSS.POS_PTA_TMPL_ENTRY WHERE POS_PTA_TMPL_ID = 13;

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(
       pos_pta_tmpl_id, payment_subtype_id,
       pos_pta_encrypt_key, pos_pta_activation_oset_hr,
       pos_pta_deactivation_oset_hr, payment_subtype_key_id,
       pos_pta_pin_req_yn_flag, pos_pta_device_serial_cd,
       pos_pta_encrypt_key2, authority_payment_mask_id,
       pos_pta_priority, terminal_id, merchant_bank_acct_id,
       currency_cd, pos_pta_passthru_allow_yn_flag,
       pos_pta_pref_auth_amt, pos_pta_pref_auth_amt_max)
SELECT 13, 
       pst.payment_subtype_id,
       NULL pos_pta_encrypt_key, 
       0 pos_pta_activation_oset_hr,
       NULL pos_pta_deactivation_oset_hr, 
       t.terminal_id payment_subtype_key_id,
       'N' pos_pta_pin_req_yn_flag, 
       NULL pos_pta_device_serial_cd,
       NULL pos_pta_encrypt_key2, 
       NULL authority_payment_mask_id, -- use payment_subtype's
       DECODE(aa.authority_assn_name, 'Visa', 25, 'MasterCard', 30, 'American Express', 35, 'Discover Card', 40, 'Diners Club International', 45, 'Diners Club Carte Blanche', 50) pos_pta_priority, -- order like this: VISA, MC, AMEX, DISCOVER
       NULL terminal_id, 
       NULL merchant_bank_acct_id,
       'USD' currency_cd, 
       'N' pos_pta_passthru_allow_yn_flag,
       1 pos_pta_pref_auth_amt, 
       10 pos_pta_pref_auth_amt_max
FROM PSS.PAYMENT_SUBTYPE pst
join PSS.AUTHORITY_PAYMENT_MASK apm on pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
join AUTHORITY.AUTHORITY_ASSN aa on apm.AUTHORITY_ASSN_ID = aa.AUTHORITY_ASSN_ID
cross join pss.merchant m
join PSS.TERMINAL T on t.MERCHANT_ID = m.merchant_id
WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Authority::ISO8583::FHMS'
and pst.PAYMENT_SUBTYPE_NAME like '%(Track 2) - USA'
and pst.CLIENT_PAYMENT_TYPE_CD in('C', 'R')
AND aa.authority_assn_name != 'Diners Club International'
and m.merchant_cd = l_us_mid
and t.terminal_cd = l_us_tid
UNION ALL
SELECT 13, 
       pst.payment_subtype_id,
       NULL pos_pta_encrypt_key, 
       0 pos_pta_activation_oset_hr,
       NULL pos_pta_deactivation_oset_hr, 
       ipt.INTERNAL_PAYMENT_TYPE_id payment_subtype_key_id,
       'N' pos_pta_pin_req_yn_flag, 
       NULL pos_pta_device_serial_cd,
       NULL pos_pta_encrypt_key2, 
       NULL authority_payment_mask_id, -- use payment_subtype's
       60 pos_pta_priority, 
       NULL terminal_id, 
       NULL merchant_bank_acct_id,
       'USD' currency_cd, 
       'N' pos_pta_passthru_allow_yn_flag,
       NULL pos_pta_pref_auth_amt, 
       NULL pos_pta_pref_auth_amt_max
FROM PSS.PAYMENT_SUBTYPE pst
JOIN PSS.AUTHORITY_PAYMENT_MASK APM ON PST.AUTHORITY_PAYMENT_MASK_ID = APM.AUTHORITY_PAYMENT_MASK_ID
CROSS JOIN PSS.INTERNAL_PAYMENT_TYPE ipt
JOIN PSS.INTERNAL_AUTHORITY ia on ipt.INTERNAL_AUTHORITY_ID = ia.INTERNAL_AUTHORITY_ID
WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Internal'
AND pst.PAYMENT_SUBTYPE_NAME LIKE 'USAT ISO Card - Gift/Stored Value Card'
AND pst.CLIENT_PAYMENT_TYPE_CD IN('S')
AND ia.INTERNAL_AUTHORITY_NAME LIKE 'USA Technologies%'
AND ia.AUTHORITY_SERVICE_TYPE_ID = 1
UNION ALL
SELECT 13, 
       pst.payment_subtype_id,
       NULL pos_pta_encrypt_key, 
       0 pos_pta_activation_oset_hr,
       NULL pos_pta_deactivation_oset_hr, 
       NULL payment_subtype_key_id,
       'N' pos_pta_pin_req_yn_flag, 
       NULL pos_pta_device_serial_cd,
       NULL pos_pta_encrypt_key2, 
       NULL authority_payment_mask_id,
       5 pos_pta_priority, 
       NULL terminal_id, 
       NULL merchant_bank_acct_id,
       'USD' currency_cd, 
       'N' pos_pta_passthru_allow_yn_flag,
       NULL pos_pta_pref_auth_amt, 
       NULL pos_pta_pref_auth_amt_max
FROM PSS.PAYMENT_SUBTYPE pst
WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Cash'
and pst.CLIENT_PAYMENT_TYPE_CD in('M')
UNION ALL
SELECT 13, 
       pst.payment_subtype_id,
       NULL pos_pta_encrypt_key, 
       0 pos_pta_activation_oset_hr,
       NULL pos_pta_deactivation_oset_hr, 
       t.terminal_id payment_subtype_key_id,
       'N' pos_pta_pin_req_yn_flag, 
       NULL pos_pta_device_serial_cd,
       NULL pos_pta_encrypt_key2, 
       NULL authority_payment_mask_id,
       99 pos_pta_priority, 
       NULL terminal_id, 
       NULL merchant_bank_acct_id,
       'USD' currency_cd, 
       'N' pos_pta_passthru_allow_yn_flag,
       NULL pos_pta_pref_auth_amt, 
       NULL pos_pta_pref_auth_amt_max
FROM PSS.PAYMENT_SUBTYPE pst
cross join pss.merchant m
join PSS.TERMINAL T on t.MERCHANT_ID = m.merchant_id
WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Authority::NOP'
AND pst.PAYMENT_SUBTYPE_NAME like '%Error Bin'
and pst.CLIENT_PAYMENT_TYPE_CD in('C','R')
and m.merchant_cd = '0'
and t.terminal_cd = '0'
and t.terminal_desc = 'Error Bin Terminal';

COMMIT;
/*
    SELECT PSS.SEQ_POS_PTA_TMPL_ID.NEXTVAL
      INTO l_ppt_id
      FROM DUAL;
      
    INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_ID, POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
    VALUES(l_ppt_id, 'DEFAULT: Edge Initialization - CAN', 'Default template for Edge in Canada during initialization');

    INSERT INTO PSS.POS_PTA_TMPL_ENTRY(
           pos_pta_tmpl_id, payment_subtype_id,
           pos_pta_encrypt_key, pos_pta_activation_oset_hr,
           pos_pta_deactivation_oset_hr, payment_subtype_key_id,
           pos_pta_pin_req_yn_flag, pos_pta_device_serial_cd,
           pos_pta_encrypt_key2, authority_payment_mask_id,
           pos_pta_priority, terminal_id, merchant_bank_acct_id,
           currency_cd, pos_pta_passthru_allow_yn_flag,
           pos_pta_pref_auth_amt, pos_pta_pref_auth_amt_max)
    SELECT l_ppt_id, 
           pst.payment_subtype_id,
           NULL pos_pta_encrypt_key, 
           0 pos_pta_activation_oset_hr,
           NULL pos_pta_deactivation_oset_hr, 
           t.terminal_id payment_subtype_key_id,
           'N' pos_pta_pin_req_yn_flag, 
           NULL pos_pta_device_serial_cd,
           NULL pos_pta_encrypt_key2, 
           NULL authority_payment_mask_id, -- use payment_subtype's
           DECODE(aa.authority_assn_name, 'Visa', 25, 'MasterCard', 30, 'American Express', 35, 'Discover Card', 40, 'Diners Club International', 45, 'Diners Club Carte Blanche', 50) pos_pta_priority, -- order like this: VISA, MC, AMEX, DISCOVER
           NULL terminal_id, 
           NULL merchant_bank_acct_id,
           'CAD' currency_cd, 
           'N' pos_pta_passthru_allow_yn_flag,
           1 pos_pta_pref_auth_amt, 
           10 pos_pta_pref_auth_amt_max
    FROM PSS.PAYMENT_SUBTYPE pst
    join PSS.AUTHORITY_PAYMENT_MASK apm on pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
    join AUTHORITY.AUTHORITY_ASSN aa on apm.AUTHORITY_ASSN_ID = aa.AUTHORITY_ASSN_ID
    cross join pss.merchant m
    join PSS.TERMINAL T on t.MERCHANT_ID = m.merchant_id
    WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Authority::ISO8583::FHMS::Paymentech'
    and pst.PAYMENT_SUBTYPE_NAME like '%(Track 2) - Canada'
    and pst.CLIENT_PAYMENT_TYPE_CD in('C', 'R')
    AND aa.authority_assn_name != 'Diners Club International'
    and m.merchant_cd = l_ca_mid
    and t.terminal_cd = l_ca_tid
    UNION ALL
    SELECT l_ppt_id, 
       pst.payment_subtype_id,
       NULL pos_pta_encrypt_key, 
       0 pos_pta_activation_oset_hr,
       NULL pos_pta_deactivation_oset_hr, 
       ipt.INTERNAL_PAYMENT_TYPE_id payment_subtype_key_id,
       'N' pos_pta_pin_req_yn_flag, 
       NULL pos_pta_device_serial_cd,
       NULL pos_pta_encrypt_key2, 
       NULL authority_payment_mask_id, -- use payment_subtype's
       60 pos_pta_priority, 
       NULL terminal_id, 
       NULL merchant_bank_acct_id,
       'CAD' currency_cd, 
       'N' pos_pta_passthru_allow_yn_flag,
       NULL pos_pta_pref_auth_amt, 
       NULL pos_pta_pref_auth_amt_max
    FROM PSS.PAYMENT_SUBTYPE pst
    JOIN PSS.AUTHORITY_PAYMENT_MASK APM ON PST.AUTHORITY_PAYMENT_MASK_ID = APM.AUTHORITY_PAYMENT_MASK_ID
    CROSS JOIN PSS.INTERNAL_PAYMENT_TYPE ipt
    JOIN PSS.INTERNAL_AUTHORITY ia on ipt.INTERNAL_AUTHORITY_ID = ia.INTERNAL_AUTHORITY_ID
    WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Internal'
    AND pst.PAYMENT_SUBTYPE_NAME LIKE 'USAT ISO Card - Gift/Stored Value Card'
    AND pst.CLIENT_PAYMENT_TYPE_CD IN('S')
    AND ia.INTERNAL_AUTHORITY_NAME LIKE 'USA Technologies%'
    AND ia.AUTHORITY_SERVICE_TYPE_ID = 1
    UNION ALL
    SELECT l_ppt_id, 
           pst.payment_subtype_id,
           NULL pos_pta_encrypt_key, 
           0 pos_pta_activation_oset_hr,
           NULL pos_pta_deactivation_oset_hr, 
           NULL payment_subtype_key_id,
           'N' pos_pta_pin_req_yn_flag, 
           NULL pos_pta_device_serial_cd,
           NULL pos_pta_encrypt_key2, 
           NULL authority_payment_mask_id,
           5 pos_pta_priority, 
           NULL terminal_id, 
           NULL merchant_bank_acct_id,
           'CAD' currency_cd, 
           'N' pos_pta_passthru_allow_yn_flag,
           NULL pos_pta_pref_auth_amt, 
           NULL pos_pta_pref_auth_amt_max
    FROM PSS.PAYMENT_SUBTYPE pst
    WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Cash'
    and pst.CLIENT_PAYMENT_TYPE_CD in('M')
    UNION ALL
    SELECT l_ppt_id, 
           pst.payment_subtype_id,
           NULL pos_pta_encrypt_key, 
           0 pos_pta_activation_oset_hr,
           NULL pos_pta_deactivation_oset_hr, 
           t.terminal_id payment_subtype_key_id,
           'N' pos_pta_pin_req_yn_flag, 
           NULL pos_pta_device_serial_cd,
           NULL pos_pta_encrypt_key2, 
           NULL authority_payment_mask_id,
           99 pos_pta_priority, 
           NULL terminal_id, 
           NULL merchant_bank_acct_id,
           'CAD' currency_cd, 
           'N' pos_pta_passthru_allow_yn_flag,
           NULL pos_pta_pref_auth_amt, 
           NULL pos_pta_pref_auth_amt_max
    FROM PSS.PAYMENT_SUBTYPE pst
    cross join pss.merchant m
    join PSS.TERMINAL T on t.MERCHANT_ID = m.merchant_id
    WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Authority::NOP'
    AND pst.PAYMENT_SUBTYPE_NAME like '%Error Bin'
    and pst.CLIENT_PAYMENT_TYPE_CD in('C','R')
    and m.merchant_cd = '0'
    and t.terminal_cd = '0'
    and t.terminal_desc = 'Error Bin Terminal';
    
    COMMIT;
    */
END;
/
