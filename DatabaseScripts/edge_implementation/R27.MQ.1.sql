CREATE OR REPLACE FUNCTION MQ.CREATE_QUEUE(
	pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE,
	pb_primary BOOLEAN
) 
	RETURNS VOID 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'CREATE TABLE MQ.' || pv_queue_table || '('
		|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
		|| 'CONSTRAINT CK_' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
		|| 'PRIMARY KEY(MESSAGE_ID)'
		|| ') INHERITS(MQ.Q_)';
	EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_queue_table || ' TO use_mq';
	EXECUTE 'ALTER TABLE MQ.'|| pv_queue_table || ' OWNER TO use_mq';
	EXECUTE 'CREATE UNIQUE INDEX UX_' || pv_queue_table || '_ORDER ON MQ.' || pv_queue_table || '(AVAILABLE_TIME, PRIORITY DESC, POSTED_TIME, MESSAGE_ID)';
	EXECUTE 'CREATE INDEX IX_' || pv_queue_table || '_EXPIRE_TIME ON MQ.' || pv_queue_table || '(EXPIRE_TIME)';
	IF pb_primary THEN
		EXECUTE 'CREATE TABLE MQ.I' || pv_queue_table || '('
			|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
			|| 'CONSTRAINT CK_I' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
			|| 'PRIMARY KEY(MESSAGE_ID)'
			|| ') INHERITS(MQ.IQ_)';
		EXECUTE 'ALTER TABLE MQ.I'|| pv_queue_table || ' OWNER TO use_mq';	
	END IF;
END;
$$ LANGUAGE plpgsql;

