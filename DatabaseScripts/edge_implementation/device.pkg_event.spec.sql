CREATE OR REPLACE PACKAGE DEVICE.PKG_EVENT
  IS

FUNCTION SF_GET_GLOBAL_EVENT_CD
(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN event.event_device_tran_cd%TYPE
) RETURN VARCHAR2;

PROCEDURE SP_GET_OR_CREATE_EVENT
(
    pc_global_event_cd_prefix IN CHAR,
    pn_session_id IN NUMBER,
    pv_device_name IN device.device_name%TYPE,
    pn_host_id IN host.host_id%TYPE,
    pn_host_type_id IN host_type.host_type_id%TYPE,
    pn_event_type_id IN event_type.event_type_id%TYPE,
    pn_event_type_host_type_num IN event_type_host_type.event_type_host_type_num%TYPE,
    pn_event_state_id IN event.event_state_id%TYPE,
    pv_event_device_tran_cd IN event.event_device_tran_cd%TYPE,
    pn_event_end_flag IN NUMBER,
    pn_event_upload_complete_flag IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_id	OUT event.event_id%TYPE,
    pn_exists OUT NUMBER,
    pd_event_start_ts IN event.event_start_ts%TYPE DEFAULT SYSDATE,
    pd_event_end_ts IN event.event_end_ts%TYPE DEFAULT SYSDATE
);

PROCEDURE SP_GET_OR_CREATE_EVENT_DETAIL
(
    pn_event_id IN event.event_id%TYPE,
    pn_event_detail_type_id IN event_detail.event_detail_type_id%TYPE,
    pv_event_detail_value IN event_detail.event_detail_value%TYPE,
    pv_event_detail_value_ts_ms IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_detail_id	OUT event_detail.event_detail_id%TYPE,
    pn_exists OUT NUMBER
);

PROCEDURE SP_GET_OR_CREATE_STTLMT_EVENT
(
    pn_event_id IN event.event_id%TYPE,
    pn_device_batch_id IN settlement_event.device_batch_id%TYPE,
    pn_device_new_batch_id IN settlement_event.device_new_batch_id%TYPE,
    pn_device_event_utc_ts_ms IN NUMBER,
    pn_device_event_utc_offset_min IN settlement_event.device_event_utc_offset_min%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_exists OUT NUMBER
);

PROCEDURE SP_GET_OR_CREATE_HOST_COUNTER
(
    pn_host_id IN host.host_id%TYPE,
    pn_event_id IN event.event_id%TYPE,
    pv_host_counter_parameter IN host_counter.host_counter_parameter%TYPE,
    pv_host_counter_value IN host_counter.host_counter_value%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_counter_id OUT host_counter.host_counter_id%TYPE,
    pn_exists OUT NUMBER
);

END;
/

GRANT EXECUTE ON DEVICE.PKG_EVENT TO PSS, USAT_WEB_ROLE;

CREATE PUBLIC SYNONYM PKG_EVENT FOR DEVICE.PKG_EVENT;
