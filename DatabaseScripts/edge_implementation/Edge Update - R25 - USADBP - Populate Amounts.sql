DECLARE
    ln_min_auth_id PSS.AUTH.AUTH_ID%TYPE;
    ln_max_auth_id PSS.AUTH.AUTH_ID%TYPE; 
    ln_interval PLS_INTEGER := 1000;
    lt_auth_ids NUMBER_TABLE;
    lt_tran_ids NUMBER_TABLE;
    ln_cnt_1 PLS_INTEGER;
    ln_cnt_2 PLS_INTEGER;
    ln_cnt_3 PLS_INTEGER;    
BEGIN
    SELECT GREATEST(NVL(ln_min_auth_id, 0), MIN(AUTH_ID)), MAX(AUTH_ID)
      INTO ln_min_auth_id, ln_max_auth_id
      FROM PSS.AUTH;
    WHILE ln_min_auth_id < ln_max_auth_id LOOP
        UPDATE PSS.AUTH
           SET AUTH_AMT_APPROVED = AUTH_AMT
         WHERE AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
           AND AUTH_AMT_APPROVED IS NULL
           AND AUTH_RESULT_CD = 'Y'
           AND AUTH_ID BETWEEN ln_min_auth_id AND ln_min_auth_id + ln_interval
          RETURNING AUTH_ID, TRAN_ID BULK COLLECT INTO lt_auth_ids, lt_tran_ids;
       ln_cnt_1 := SQL%ROWCOUNT;
       UPDATE PSS.TRAN_SETTLEMENT_BATCH ASB
          SET TRAN_SETTLEMENT_B_AMT = (SELECT A.AUTH_AMT_APPROVED FROM PSS.AUTH A WHERE A.AUTH_ID = ASB.AUTH_ID)
        WHERE TRAN_SETTLEMENT_B_AMT IS NULL
          AND AUTH_ID IN(SELECT COLUMN_VALUE FROM TABLE(lt_auth_ids))
          AND 1 = (SELECT SB.SETTLEMENT_BATCH_STATE_ID FROM PSS.SETTLEMENT_BATCH SB WHERE ASB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID);
      ln_cnt_2 := SQL%ROWCOUNT;
      UPDATE PSS.MV_ESUDS_SETTLED_TRAN E
         SET TRAN_SETTLEMENT_AMOUNT = (SELECT TRAN_SETTLEMENT_B_AMT FROM PSS.TRAN_SETTLEMENT_BATCH ASB WHERE ASB.TRAN_ID = E.TRAN_ID AND ASB.SETTLEMENT_BATCH_ID = E.SETTLEMENT_BATCH_ID)
       WHERE TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(lt_tran_ids))
         AND TRAN_SETTLEMENT_AMOUNT IS NULL;       
      ln_cnt_3 := SQL%ROWCOUNT;
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('UPDATED ' || ln_cnt_1 || ' auth rows, ' || ln_cnt_2 || ' tran settlement rows, and '||ln_cnt_3 ||' esuds settled rows between AUTH_IDs ' || ln_min_auth_id || ' and ' || (ln_min_auth_id + ln_interval));
      ln_min_auth_id := ln_min_auth_id + ln_interval + 1;
    END LOOP;
END;
/
DECLARE
    ln_min_refund_id PSS.REFUND.REFUND_ID%TYPE;
    ln_max_refund_id PSS.REFUND.REFUND_ID%TYPE; 
    ln_interval PLS_INTEGER := 1000;
    lt_refund_ids NUMBER_TABLE;
    ln_cnt_1 PLS_INTEGER;
    ln_cnt_2 PLS_INTEGER;
BEGIN
    SELECT GREATEST(NVL(ln_min_refund_id, 0), MIN(REFUND_ID)), MAX(REFUND_ID)
      INTO ln_min_refund_id, ln_max_refund_id
      FROM PSS.REFUND;
    WHILE ln_min_refund_id < ln_max_refund_id LOOP
       UPDATE PSS.REFUND_SETTLEMENT_BATCH RSB
          SET REFUND_SETTLEMENT_B_AMT = (SELECT R.REFUND_AMT FROM PSS.REFUND R WHERE R.REFUND_ID = RSB.REFUND_ID)
        WHERE REFUND_SETTLEMENT_B_AMT IS NULL
          AND REFUND_ID BETWEEN ln_min_refund_id AND ln_min_refund_id + ln_interval
          AND 1 = (SELECT SB.SETTLEMENT_BATCH_STATE_ID FROM PSS.SETTLEMENT_BATCH SB WHERE RSB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID)
          AND 'Y' = (SELECT R.REFUND_RESP_CD FROM PSS.REFUND R WHERE R.REFUND_ID = RSB.REFUND_ID)
        RETURNING REFUND_ID BULK COLLECT INTO lt_refund_ids;
      ln_cnt_1 := SQL%ROWCOUNT;
      UPDATE PSS.MV_ESUDS_SETTLED_REFUND E
         SET REFUND_SETTLEMENT_B_AMT = (SELECT REFUND_SETTLEMENT_B_AMT FROM PSS.REFUND_SETTLEMENT_BATCH RSB WHERE RSB.TRAN_ID = E.TRAN_ID AND RSB.SETTLEMENT_BATCH_ID = E.SETTLEMENT_BATCH_ID)
       WHERE REFUND_ID IN(SELECT COLUMN_VALUE FROM TABLE(lt_refund_ids))
         AND REFUND_SETTLEMENT_B_AMT IS NULL;            
      ln_cnt_2 := SQL%ROWCOUNT;
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('UPDATED ' || ln_cnt_1 || ' refund rows, and ' || ln_cnt_2 || ' esuds settled rows between REFUND_IDs ' || ln_min_refund_id || ' and ' || (ln_min_refund_id + ln_interval));
      ln_min_refund_id := ln_min_refund_id + ln_interval + 1;
    END LOOP;
END;
/
         