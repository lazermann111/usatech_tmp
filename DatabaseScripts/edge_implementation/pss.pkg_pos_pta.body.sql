CREATE OR REPLACE PACKAGE BODY PSS.PKG_POS_PTA
IS

/* payment template import mode codes
 	S = Safe Mode
		- Create new pos_pta records based on the given template where no active
		  pos_pta already exists per client_payment_type_cd
	MS = Merge Safe Mode
		- Create new pos_pta records based on the given template where no active
		  pos_pta already exists per payment_subtype_id
	MO = Merge Overwrite Mode
		- Deactivate any existing pos_pta records where a new pos_pta record is defined
		  in the template per payment_subtype_id
		- Create all new pos_pta records based on the given template
	O = Overwrite Mode
		- Deactivate all existing pos_pta records where a new pos_pta record is
		  being imported for the client_payment_type_cd
		- Create all new pos_pta records based on the given template */
IMPORT_MODE__SAFE                            CONSTANT VARCHAR2(1)            := 'S';
IMPORT_MODE__MERGE_SAFE                      CONSTANT VARCHAR2(2)            := 'MS';
IMPORT_MODE__MERGE_OVERWRITE                 CONSTANT VARCHAR2(2)            := 'MO';
IMPORT_MODE__OVERWRITE                       CONSTANT VARCHAR2(1)            := 'O';

PROCEDURE SP_GET_DEVICE_DATA
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_pos_id                               OUT pss.pos.pos_id%TYPE,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SELECT p.pos_id
    INTO pn_pos_id
    FROM pss.pos p, device.device d
	WHERE p.device_id = d.device_id
	   AND d.device_id = pn_device_id;
	
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;


PROCEDURE SP_DISABLE_POS_PTA
(
    pn_pos_pta_id       IN       pss.pos_pta.pos_pta_id%TYPE,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
)
IS
    ln_sys_date                  DATE                           := SYSDATE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    /* update activation timestamp to 1 second ago to prevent active and inactive records from
    having exactly the same activation timestamp */
    UPDATE pss.pos_pta
    SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
        pos_pta_deactivation_ts = ln_sys_date
	WHERE pos_pta_id = pn_pos_pta_id;
	
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_IMPORT_POS_PTA_TEMPLATE
(
    pn_device_id        IN       device.device_id%TYPE,
    pn_pos_pta_tmpl_id      IN       pss.pos_pta_tmpl.pos_pta_tmpl_id%TYPE,
    pv_mode_cd          IN       VARCHAR2,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
)
IS
    ln_pos_id                    pss.pos.pos_id%TYPE;
    ln_sys_date                  DATE                           := SYSDATE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    IF pv_mode_cd NOT IN (IMPORT_MODE__SAFE,
        IMPORT_MODE__MERGE_SAFE,
        IMPORT_MODE__MERGE_OVERWRITE,
        IMPORT_MODE__OVERWRITE) THEN
        pv_error_message := 'Unrecognized import mode_cd: ' || pv_mode_cd;
        RETURN;
    END IF;

    SP_GET_DEVICE_DATA(pn_device_id, ln_pos_id, pn_result_cd, pv_error_message);

    IF pv_mode_cd = IMPORT_MODE__OVERWRITE THEN
        -- deactivate any active or inactive pos_pta that have a client_payment_type_cd in our template
        UPDATE pss.pos_pta
        SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
            pos_pta_deactivation_ts = ln_sys_date
    	WHERE pos_pta_id IN
    	(
           	SELECT pp.pos_pta_id
            FROM pss.pos_pta pp, pss.payment_subtype ps, pss.pos_pta_tmpl_entry tmpl_ppte, pss.payment_subtype tmpl_ps
            WHERE pp.pos_id = ln_pos_id
                AND (pp.pos_pta_activation_ts <= ln_sys_date OR pp.pos_pta_activation_ts IS NULL)
                AND (pp.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pp.pos_pta_deactivation_ts)
                AND pp.payment_subtype_id = ps.payment_subtype_id
                AND tmpl_ppte.pos_pta_tmpl_id = pn_pos_pta_tmpl_id
                AND tmpl_ppte.payment_subtype_id = tmpl_ps.payment_subtype_id
                AND ps.client_payment_type_cd = tmpl_ps.client_payment_type_cd
        );    	
    ELSIF pv_mode_cd = IMPORT_MODE__MERGE_OVERWRITE THEN
        -- deactivate any active active or inactive pos_pta that have a payment_type_id in our template
        UPDATE pss.pos_pta
        SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
            pos_pta_deactivation_ts = ln_sys_date
    	WHERE pos_pta_id IN
    	(
           	SELECT pp.pos_pta_id
            FROM pss.pos_pta pp, pss.pos_pta_tmpl_entry tmpl_ppte
            WHERE pp.pos_id = ln_pos_id
                AND (pp.pos_pta_activation_ts <= ln_sys_date OR pp.pos_pta_activation_ts IS NULL)
                AND (pp.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pp.pos_pta_deactivation_ts)
                AND tmpl_ppte.pos_pta_tmpl_id = pn_pos_pta_tmpl_id
                AND pp.payment_subtype_id = tmpl_ppte.payment_subtype_id
        );
    END IF;

	INSERT INTO pss.pos_pta
    (
    	pos_id,
		terminal_id,
        payment_subtype_key_id,
        pos_pta_device_serial_cd,
        pos_pta_activation_ts,
        pos_pta_deactivation_ts,
        pos_pta_encrypt_key,
        pos_pta_encrypt_key2,
        authority_payment_mask_id,
        pos_pta_pin_req_yn_flag,
        pos_pta_priority,
        merchant_bank_acct_id,
        currency_cd,
        payment_subtype_id,
        pos_pta_passthru_allow_yn_flag,
		pos_pta_pref_auth_amt,
		pos_pta_pref_auth_amt_max
    )
	SELECT ln_pos_id,
		ppte.terminal_id,
		ppte.payment_subtype_key_id,
		ppte.pos_pta_device_serial_cd,
		CASE WHEN ppte.pos_pta_activation_oset_hr IS NULL THEN NULL
            ELSE ln_sys_date + ppte.pos_pta_activation_oset_hr * 24 END,
		CASE WHEN ppte.pos_pta_deactivation_oset_hr IS NULL THEN NULL
            ELSE ln_sys_date + ppte.pos_pta_deactivation_oset_hr * 24 END,
		ppte.pos_pta_encrypt_key,
		ppte.pos_pta_encrypt_key2,		
        /* if the template specifies an authority payment mask id, and it is different than the payment subtype's
        default mask id, then set it in the pos_pta, otherwise leave it null to use the default */
        CASE WHEN ppte.authority_payment_mask_id IS NOT NULL
		  AND ppte.authority_payment_mask_id <> ps.authority_payment_mask_id
            THEN ppte.authority_payment_mask_id
        ELSE NULL END,
		ppte.pos_pta_pin_req_yn_flag,
		-- query for the max priority values per client_payment_type_cd so we don't violate the unique constraint
		NVL((SELECT MAX(pty_pp.pos_pta_priority)
                FROM pss.pos_pta pty_pp, pss.payment_subtype pty_ps
                WHERE pty_pp.pos_id = ln_pos_id
                    AND pty_pp.payment_subtype_id = pty_ps.payment_subtype_id
                    AND pty_ps.client_payment_type_cd = ps.client_payment_type_cd), 1)
            + ppte.pos_pta_priority,
		ppte.merchant_bank_acct_id,
		ppte.currency_cd,
		ps.payment_subtype_id,
		ppte.pos_pta_passthru_allow_yn_flag,
		ppte.pos_pta_pref_auth_amt,
		ppte.pos_pta_pref_auth_amt_max
    FROM pss.pos_pta_tmpl_entry ppte, pss.payment_subtype ps, client_payment_type cpt
    WHERE ps.payment_subtype_id = ppte.payment_subtype_id
        AND ps.client_payment_type_cd = cpt.client_payment_type_cd
        AND ppte.pos_pta_tmpl_id = pn_pos_pta_tmpl_id
        AND
        (
            -- create all the template entries
            pv_mode_cd IN (IMPORT_MODE__MERGE_OVERWRITE, IMPORT_MODE__OVERWRITE)
            -- create the template entries unless the payment_subtype_id already exists
            OR pv_mode_cd = IMPORT_MODE__MERGE_SAFE AND ps.payment_subtype_id NOT IN
            (
                SELECT payment_subtype_id
                FROM pss.pos_pta
                WHERE pos_id = ln_pos_id
                    AND (pos_pta_activation_ts <= ln_sys_date OR pos_pta_activation_ts IS NULL)
                    AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts)
            )
            -- create the template entries unless the client_payment_type_cd already exists
            OR pv_mode_cd = IMPORT_MODE__SAFE AND ps.client_payment_type_cd NOT IN
            (
                SELECT s_ps.client_payment_type_cd
                FROM pss.pos_pta s_pp, pss.payment_subtype s_ps
                WHERE s_pp.pos_id = ln_pos_id
                    AND (pos_pta_activation_ts <= ln_sys_date OR pos_pta_activation_ts IS NULL)
                    AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts)
                    AND s_pp.payment_subtype_id = s_ps.payment_subtype_id
            )
        );

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;


PROCEDURE SP_GET_POS_PTA
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    BEGIN
        SELECT pos_pta_id INTO pn_pos_pta_id FROM
        (
            SELECT pp.pos_pta_id        
            FROM pss.pos p, pss.pos_pta pp, pss.payment_subtype ps
            WHERE p.pos_id = pp.pos_id
            AND ps.payment_subtype_id = pp.payment_subtype_id
            AND p.pos_id = (
                SELECT pos_id FROM (
                    SELECT pos_id
                    FROM pss.pos
                    WHERE device_id = pn_device_id
                        AND SYS_EXTRACT_UTC(CAST(pos_activation_ts AS TIMESTAMP)) <= pt_utc_ts
                    ORDER BY pos_activation_ts DESC, pos_id DESC
                )
                WHERE ROWNUM = 1
            )
            AND ps.client_payment_type_cd = pc_client_payment_type_cd
            AND SYS_EXTRACT_UTC(CAST(pp.pos_pta_activation_ts AS TIMESTAMP)) <= pt_utc_ts
            AND (pp.pos_pta_deactivation_ts IS NULL
                OR SYS_EXTRACT_UTC(CAST(pp.pos_pta_deactivation_ts AS TIMESTAMP)) > pt_utc_ts)
            ORDER BY pp.pos_pta_priority, pp.pos_pta_activation_ts DESC, pp.pos_pta_id DESC
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_result_cd := PKG_CONST.RESULT__PAYMENT_NOT_ACCEPTED;
            pv_error_message := 'Payment method is not accepted';
            RETURN;    
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;


PROCEDURE SP_GET_OR_CREATE_POS_PTA
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
)
IS
    ln_pos_id pss.pos.pos_id%TYPE;
    ln_payment_subtype_id pss.payment_subtype.payment_subtype_id%TYPE;
    ln_payment_subtype_key_id pss.pos_pta.payment_subtype_key_id%TYPE := NULL;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SP_GET_POS_PTA(pn_device_id, pv_device_name, pc_client_payment_type_cd, pt_utc_ts, pn_result_cd, pv_error_message, pn_pos_pta_id);
    IF pn_result_cd = PKG_CONST.RESULT__PAYMENT_NOT_ACCEPTED THEN
        BEGIN
            SELECT pos_pta_id INTO pn_pos_pta_id
            FROM (
                SELECT pp.pos_pta_id     
                FROM device.device d, pss.pos p, pss.pos_pta pp, pss.payment_subtype ps
                WHERE d.device_name = pv_device_name
                    AND d.device_id = p.device_id 
                    AND p.pos_id = pp.pos_id
                    AND pp.payment_subtype_id = ps.payment_subtype_id                            
                    AND ps.client_payment_type_cd = CASE
                        WHEN pc_client_payment_type_cd = PKG_CONST.CLNT_PMNT_TYPE__CASH THEN PKG_CONST.CLNT_PMNT_TYPE__CASH
                        WHEN pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__RFID_CREDIT,
                            PKG_CONST.CLNT_PMNT_TYPE__RFID_SPECIAL) THEN PKG_CONST.CLNT_PMNT_TYPE__RFID_CREDIT
                        ELSE PKG_CONST.CLNT_PMNT_TYPE__CREDIT
                    END
                    AND ps.payment_subtype_class = CASE pc_client_payment_type_cd
                        WHEN PKG_CONST.CLNT_PMNT_TYPE__CASH THEN ps.payment_subtype_class
                        ELSE 'Authority::NOP'
                    END
                    AND UPPER(ps.payment_subtype_name) LIKE CASE pc_client_payment_type_cd
                        WHEN PKG_CONST.CLNT_PMNT_TYPE__CASH THEN UPPER(ps.payment_subtype_name)
                        ELSE '%ERROR BIN'
                    END
                ORDER BY pp.pos_pta_activation_ts DESC, pp.pos_pta_id DESC
            ) WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                    SELECT pos_id INTO ln_pos_id
                    FROM (                
                        SELECT p.pos_id     
                        FROM device.device d, pss.pos p
                        WHERE d.device_name = pv_device_name
                            AND d.device_id = p.device_id 
                        ORDER BY p.pos_activation_ts DESC, p.pos_id DESC
                    ) WHERE ROWNUM = 1;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        SELECT pss.seq_pos_id.NEXTVAL INTO ln_pos_id FROM DUAL;
                    
                        INSERT INTO pss.pos (
                            pos_id,
                            location_id,
                            customer_id,
                            device_id
                        ) VALUES (
                            ln_pos_id,
                            1,
                            1,
                            pn_device_id
                        );
                END;
                
                IF pc_client_payment_type_cd = PKG_CONST.CLNT_PMNT_TYPE__CASH THEN
                    SELECT MIN(payment_subtype_id) INTO ln_payment_subtype_id
                    FROM pss.payment_subtype
                    WHERE client_payment_type_cd = PKG_CONST.CLNT_PMNT_TYPE__CASH;
                ELSE
                    SELECT MIN(payment_subtype_id) INTO ln_payment_subtype_id
                    FROM pss.payment_subtype
                    WHERE client_payment_type_cd = CASE
                            WHEN pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__RFID_CREDIT,
                                PKG_CONST.CLNT_PMNT_TYPE__RFID_SPECIAL) THEN PKG_CONST.CLNT_PMNT_TYPE__RFID_CREDIT
                            ELSE PKG_CONST.CLNT_PMNT_TYPE__CREDIT
                        END
                        AND payment_subtype_class = 'Authority::NOP'
                        AND UPPER(payment_subtype_name) LIKE '%ERROR BIN';
                        
                    SELECT MIN(terminal_id) INTO ln_payment_subtype_key_id
                    FROM authority.authority a
                    INNER JOIN pss.merchant m ON a.authority_id = m.authority_id
                    INNER JOIN pss.terminal t ON m.merchant_id = t.merchant_id
                    WHERE UPPER(a.authority_name) = 'ERROR BIN';
                END IF;
                
                SELECT pss.seq_pos_pta_id.NEXTVAL INTO pn_pos_pta_id FROM DUAL;
                
                INSERT INTO pss.pos_pta (
                    pos_pta_id,
                    pos_id,
                    payment_subtype_id,
                    payment_subtype_key_id,
                    pos_pta_activation_ts,
                    pos_pta_priority
                ) VALUES (
                    pn_pos_pta_id,
                    ln_pos_id,
                    ln_payment_subtype_id,
                    ln_payment_subtype_key_id,
                    '1-jan-1970',
                    100
                );
        END;
    END IF;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;


END;
/
