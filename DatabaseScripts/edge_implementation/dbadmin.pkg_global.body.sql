CREATE OR REPLACE PACKAGE BODY DBADMIN.pkg_global
IS
    FUNCTION REQUEST_LOCK(
        l_object_type VARCHAR2,
        l_object_id   VARCHAR2)
     RETURN VARCHAR2
    IS
        l_handle VARCHAR2(128);
        l_ret INTEGER;
    BEGIN
        DBMS_LOCK.ALLOCATE_UNIQUE(l_object_type || CHR(0) || l_object_id, l_handle);
        l_ret := DBMS_LOCK.REQUEST(l_handle, 6 /*Exclusive*/, DBMS_LOCK.MAXWAIT, TRUE);
        IF l_ret IN(0,4) THEN
            RETURN l_handle;
        ELSE
            RAISE_APPLICATION_ERROR(-20801, 'Could not get lock on ' || l_object_type || ' ' ||  l_object_id || ': ' || TO_CHAR(l_ret));
        END IF;
    END;

    PROCEDURE RELEASE_LOCK(
        l_handle VARCHAR2)
    IS
        l_ret INTEGER;
    BEGIN
        l_ret := DBMS_LOCK.RELEASE(l_handle);
        IF l_ret NOT IN(0,4) THEN
            RAISE_APPLICATION_ERROR(-20801, 'Could not release lock ' || l_handle || ': ' || TO_CHAR(l_ret));
        END IF;
    END;
END;
/
