CREATE OR REPLACE PACKAGE BODY PSS."PKG_TRAN" 
IS

PROCEDURE sp_create_sale
(
    pc_global_event_cd_prefix IN CHAR,
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN pss.sale.sale_amount%TYPE,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
        RESULT__ILLEGAL_STATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
    lv_lock VARCHAR2(128);
    lv_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_tran_id := 0;
    
    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;
    
    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
    
    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    
    lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
    
    BEGIN
        SELECT tran_id, tran_state_cd, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
        INTO pn_tran_id, lv_tran_state_cd, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
        FROM
        (
            SELECT tx.tran_id, tx.tran_state_cd, tx.tran_upload_ts,
                CASE WHEN s.tran_line_item_hash IS NULL THEN PKG_CONST.BOOLEAN__FALSE 
                    ELSE PKG_CONST.BOOLEAN__TRUE END AS tli_hash_match
            FROM
            (
                -- we have to use a UNION here because Oracle wants to do a full table scan if two LIKEs below are in the same WHERE clause
                SELECT tran_id, tran_state_cd, tran_upload_ts, created_ts    
                FROM pss.tran
                WHERE tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
                    OR tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
                UNION
                SELECT tran_id, tran_state_cd, tran_upload_ts, created_ts  
                FROM pss.tran
                WHERE tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
            ) tx
            LEFT OUTER JOIN pss.sale s ON tx.tran_id = s.tran_id
                AND s.hash_type_cd = pv_hash_type_cd
                AND s.tran_line_item_hash = pv_tran_line_item_hash 
                AND s.sale_type_cd = pc_sale_type_cd
            ORDER BY
                CASE WHEN s.tran_line_item_hash IS NULL THEN 0 
                    ELSE 1 END DESC,
                NVL(tx.tran_upload_ts, tx.created_ts) DESC
        )
        WHERE ROWNUM = 1;
        
        ln_original_tran_id := pn_tran_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR ld_tran_upload_ts IS NOT NULL) THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;
            
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;
        
        ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;
    
    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;
        
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
        ELSE
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSE
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;                
        END IF;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;

        PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(pn_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
            
        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd
        ) VALUES (
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            lv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd
        );
        
        IF lv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
        END IF;
    ELSIF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF lv_tran_state_cd IN (
            PKG_CONST.TRAN_STATE__AUTH_DECLINE,
            PKG_CONST.TRAN_STATE__AUTH_FAILURE
            ) THEN
                pn_result_cd := PKG_CONST.RESULT__ILLEGAL_STATE;
                pv_error_message := 'Illegal tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || lv_tran_state_cd;
                RETURN;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
            AND lv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN 
            lv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL 
            AND lv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                PKG_CONST.TRAN_STATE__PROCESSING_BATCH,            
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL)
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN                        
            lv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND lv_tran_state_cd = PKG_CONST.TRAN_STATE__BATCH_INTENDED THEN
            lv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND lv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
            lv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
        ELSIF lv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
             pn_result_cd := PKG_CONST.RESULT__ILLEGAL_STATE;
             pv_error_message := 'Illegal tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || lv_tran_state_cd;
             RETURN;
        END IF;
                 
        UPDATE pss.tran
        SET tran_state_cd = lv_tran_state_cd,
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id;
             
        DELETE FROM pss.tran_line_item
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
            
        -- Remove Auth Holds
        DELETE FROM PSS.CONSUMER_ACCT_AUTH_HOLD
        WHERE AUTH_ID IN(SELECT AUTH_ID FROM PSS.AUTH A WHERE A.TRAN_ID = pn_tran_id);
    END IF;
     
    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash
    WHERE tran_id = pn_tran_id;
             
    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash
        );
    END IF;

    IF pn_result_cd != PKG_CONST.RESULT__DUPLICATE THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END IF;
END;


PROCEDURE sp_create_tran_line_item
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,    
    pn_tli_amount IN pss.tran_line_item.tran_line_item_amount%TYPE,
    pn_tli_tax IN pss.tran_line_item.tran_line_item_tax%TYPE,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,    
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
BEGIN
    pkg_device_configuration.sp_get_host(pn_device_id, pv_device_name, pn_host_port_num, DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms), ln_result_cd, lv_error_message, ln_host_id);
    IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
        RETURN;
    END IF;    

    INSERT INTO pss.tran_line_item (
        tran_id,        
        tran_line_item_amount,
        tran_line_item_position_cd,
        tran_line_item_tax,
        tran_line_item_type_id,
        tran_line_item_quantity,
        tran_line_item_desc,
        host_id,
        tran_line_item_batch_type_cd,
        tran_line_item_ts
    )
    SELECT
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END,
        pn_tli_type_id,
        pn_tli_quantity,
        pv_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE)
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = pn_tli_type_id;
END;


PROCEDURE sp_finalize_sale
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE, 
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN pss.sale.sale_amount%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_settlement_batch.tran_settlement_b_amt%TYPE;
    ln_tli_count pss.tran_line_item.tran_line_item_quantity%TYPE;
    ln_adj_amt tran_settlement_batch.tran_settlement_b_amt%TYPE;    
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    
    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
        
        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;
    
    SELECT NVL(SUM((NVL(tran_line_item_amount, 0) + NVL(tran_line_item_tax, 0)) * NVL(tran_line_item_quantity, 0)), 0),
        SUM(NVL(tran_line_item_quantity, 0))
    INTO ln_tli_total, ln_tli_count
    FROM pss.tran_line_item
    WHERE tran_id = pn_tran_id
        AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS 
        OR NVL(pn_sale_amount, 0) = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := NVL(pn_sale_amount, 0) - ln_tli_total;
        IF ln_adj_amt > 0 THEN
            ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;

    IF ln_adj_tli > -1 THEN
        -- use the base host for adjustments
        pkg_device_configuration.sp_get_host(pn_device_id, pv_device_name, 0, DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms), pn_result_cd, pv_error_message, ln_base_host_id);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
    
        INSERT INTO pss.tran_line_item
        (
            tran_id,
            host_id,
            tran_line_item_type_id,
            tran_line_item_amount,
            tran_line_item_quantity,
            tran_line_item_desc,
            tran_line_item_batch_type_cd,
            tran_line_item_tax
        )
        SELECT
            pn_tran_id,
            ln_base_host_id,
            ln_adj_tli,
            ln_adj_amt,
            1,
            tran_line_item_type_desc,
            pc_tran_batch_type_cd,
            0
        FROM pss.tran_line_item_type
        WHERE tran_line_item_type_id = ln_adj_tli;
    END IF;
    
    SELECT ps.client_payment_type_cd INTO lc_client_payment_type_cd
    FROM pss.tran t
    JOIN pss.pos_pta pp ON t.pos_pta_id = pp.pos_pta_id
    JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
    WHERE t.tran_id = pn_tran_id;
    
    pkg_call_in_record.sp_add_trans(pn_session_id, lc_client_payment_type_cd, pn_sale_amount, ln_tli_count);
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;


PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pc_global_event_cd_prefix IN CHAR,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE, 
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_approved_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_auth_hold CHAR,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lv_lock VARCHAR2(128);
   lv_global_trans_cd_app_layer PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
   lv_global_trans_cd_legacy PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
   lc_uploaded_flag CHAR(1);
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
   lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
BEGIN
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_event_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_event_cd);
    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        RAISE_APPLICATION_ERROR(-20377,'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix);
    END IF;
    lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
    SELECT MAX(TRAN_ID), MAX(SALE_TYPE_CD), MAX(TRAN_STATE_CD)
      INTO pn_tran_id, lc_sale_type_cd, lc_tran_state_cd
      FROM (SELECT X.TRAN_ID, S.SALE_TYPE_CD, X.TRAN_STATE_CD
      FROM PSS.TRAN X
      LEFT OUTER JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
     WHERE TRAN_GLOBAL_TRANS_CD IN(pv_global_event_cd, lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
     ORDER BY DECODE(TRAN_STATE_CD, 
        PKG_CONST.TRAN_STATE__WAITING_FOR_AUTH, 1,
        PKG_CONST.TRAN_STATE__COMPLETE_ERROR, 5, 
        100), X.TRAN_ID)
     WHERE ROWNUM = 1;
    IF pn_tran_id IS NOT NULL THEN  
        IF lc_tran_state_cd NOT IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
            RAISE_APPLICATION_ERROR(-20378,'Invalid TRAN_STATE_CD (' || lc_tran_state_cd || ') for TRAN_ID ' || pn_tran_id);
        END IF;
        IF pc_auth_result_cd IN('Y', 'P') THEN
            IF lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__INTENDED) THEN
                lc_new_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
            ELSE
                lc_new_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
            END IF;
        ELSE
            IF lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__INTENDED) THEN
                lc_new_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
            ELSE
                lc_new_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            END IF;
        END IF;
        UPDATE PSS.TRAN
           SET (TRAN_START_TS,
                TRAN_END_TS,
                TRAN_STATE_CD,
                TRAN_ACCOUNT_PIN,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                TRAN_PARSED_ACCT_NAME,
                TRAN_PARSED_ACCT_EXP_DATE,
                TRAN_PARSED_ACCT_NUM,
                TRAN_REPORTABLE_ACCT_NUM,
                POS_PTA_ID,
                CONSUMER_ACCT_ID) =
            (SELECT 
                CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE),  /* TRAN_START_TS */
                TRAN_END_TS - TRAN_START_TS + CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE),  /* TRAN_END_TS */
                lc_new_tran_state_cd,  /* TRAN_STATE_CD */
                pv_pin,  /* TRAN_ACCOUNT_PIN */
                pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                pv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
                pv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
                pv_pan,  /* TRAN_PARSED_ACCT_NUM */
                SUBSTR(pv_pan, LENGTH(pv_pan) - 3, LENGTH(pv_pan)),  /* TRAN_REPORTABLE_ACCT_NUM */
                pn_pos_pta_id, /* POS_PTA_ID */
                pn_consumer_acct_id  /* CONSUMER_ACCT_ID */
           FROM DUAL)
            WHERE TRAN_ID = pn_tran_id;
        lc_uploaded_flag := 'Y';
    ELSE
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL, 'N'
          INTO pn_tran_id, lc_uploaded_flag
          FROM DUAL;
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_REPORTABLE_ACCT_NUM,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID)
         VALUES(
            pn_tran_id, /* TRAN_ID */
            CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE),  /* TRAN_START_TS */
            DECODE(pc_auth_result_cd, 'Y', '6', 'N', '7', 'P', '0', 'O', '7', 'F', '5'),  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            pv_pin,  /* TRAN_ACCOUNT_PIN */
            pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            pv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
            pv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
            pv_pan,  /* TRAN_PARSED_ACCT_NUM */
            SUBSTR(pv_pan, LENGTH(pv_pan) - 3, LENGTH(pv_pan)),  /* TRAN_REPORTABLE_ACCT_NUM */
            pn_pos_pta_id, /* POS_PTA_ID */
            pv_global_event_cd, /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */);
    END IF;
    
    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO ln_auth_id
      FROM DUAL;
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        AUTH_PARSED_ACCT_DATA,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        TRACE_NUMBER)
     VALUES(
        ln_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        DECODE(pc_auth_result_cd, 'Y', pv_pan, 'P', pv_pan), /* AUTH_PARSED_ACCT_DATA */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_approved_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_trace_number); /* TRACE_NUMBER */
    IF lc_uploaded_flag = 'N' AND pc_auth_hold IN('t', 'T') THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID)
          VALUES(pn_consumer_acct_id, ln_auth_id, pn_tran_id);
    END IF;
    UPDATE DEVICE.DEVICE_CALL_IN_RECORD
       SET AUTH_AMOUNT = NVL(pn_approved_amt / pn_minor_currency_factor, 0),
           AUTH_APPROVED_FLAG = DECODE(pc_auth_result_cd, 'Y', 'Y', 'N', 'N', 'P', 'Y', 'O', 'N', 'F', 'N'),
           AUTH_CARD_TYPE = pc_payment_type,
           LAST_AUTH_IN_TS = GREATEST(pd_auth_ts, NVL(LAST_AUTH_IN_TS, MIN_DATE)),
           CALL_IN_TYPE = DECODE(CALL_IN_TYPE, 'B', 'C', 'C', 'C', 'A')
     WHERE SESSION_ID = pn_session_id;
END;

PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN

    INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      VALUES(pn_tran_id, 2 /* live auth "POSM" time*/, (pn_applayer_end_time - pn_applayer_start_time) / 1000);
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      VALUES(pn_tran_id, 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000);
    IF pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL THEN
        INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
          VALUES(pn_tran_id, 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000);
    END IF;
END;

PROCEDURE SP_FINALIZE_AUTH(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER,
   pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE
)
IS
   ln_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE;
BEGIN
    SELECT DEVICE_SESSION_ID INTO ln_session_id
    FROM ENGINE.DEVICE_SESSION
    WHERE GLOBAL_SESSION_CD = pv_global_session_cd;

    UPDATE DEVICE.DEVICE_CALL_IN_RECORD
       SET AUTH_AMOUNT = NVL(pn_auth_amt / pn_minor_currency_factor, 0),
           AUTH_APPROVED_FLAG = DECODE(pc_auth_result_cd, 'Y', 'Y', 'N', 'N', 'P', 'Y', 'O', 'N', 'F', 'N'),
           AUTH_CARD_TYPE = pc_payment_type,
           LAST_AUTH_IN_TS = GREATEST(pd_auth_ts, NVL(LAST_AUTH_IN_TS, MIN_DATE)),
           CALL_IN_TYPE = DECODE(CALL_IN_TYPE, 'B', 'C', 'C', 'C', 'A')
     WHERE SESSION_ID = ln_session_id;
     
     SP_INSERT_AUTH_STATS(pn_tran_id,pn_request_time,pn_applayer_start_time,pn_authority_start_time,pn_authority_end_time,pn_applayer_end_time,pn_response_time);
END;
   
END;
/
