CREATE OR REPLACE PACKAGE PSS."PKG_TRAN" 
IS

PROCEDURE sp_create_sale
(
    pc_global_event_cd_prefix IN CHAR,
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN pss.sale.sale_amount%TYPE,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE
);


PROCEDURE sp_create_tran_line_item
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,    
    pn_tli_amount IN pss.tran_line_item.tran_line_item_amount%TYPE,
    pn_tli_tax IN pss.tran_line_item.tran_line_item_tax%TYPE,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,    
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE
);


PROCEDURE sp_finalize_sale
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN pss.sale.sale_amount%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2
);


PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pc_global_event_cd_prefix IN CHAR,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE, 
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_approved_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_auth_hold CHAR,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE
);

PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
);

PROCEDURE SP_FINALIZE_AUTH(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER,
   pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE
);

END;
/
