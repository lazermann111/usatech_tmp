declare
	ln_file_transfer_id file_transfer.file_transfer_id%type;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_setting_count NUMBER;
begin
	SELECT file_transfer_id INTO ln_file_transfer_id
	FROM device.file_transfer
	WHERE file_transfer_name = 'DEFAULT-CFG-13-0';

	UPDATE device.file_transfer SET file_transfer_type_cd = 22 WHERE file_transfer_id = ln_file_transfer_id;
	
    pkg_device_configuration.sp_update_cfg_tmpl_settings(ln_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
end;
