DECLARE
    CURSOR l_cur IS
        SELECT '' new_cond, '' table_owner, '' table_name from dual where 0=1
UNION ALL SELECT '(:dml.get_object_owner() = ''DEVICE'' and :dml.get_object_name() = ''DEVICE'' and (
        (:dml.get_command_type() = ''INSERT'' and NVL(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'', ''N''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() NOT LIKE ''APP_LAYER%'' and NVL(:dml.GET_VALUE(''new'', ''DEVICE_ACTIVE_YN_FLAG''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = ''Y'')
     or (:dml.get_command_type() = ''UPDATE''
        and (
                LNNVL(NVL(:dml.GET_VALUE(''new'', ''DEVICE_ACTIVE_YN_FLAG''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = NVL(:dml.GET_VALUE(''old'', ''DEVICE_ACTIVE_YN_FLAG''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2())
             or (
                    NVL(:dml.GET_VALUE(''new'', ''DEVICE_ACTIVE_YN_FLAG''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = ''Y'' 
                    and NVL(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'', ''N''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() NOT LIKE ''APP_LAYER%''
                    and (
                        LNNVL(NVL(:dml.GET_VALUE(''new'', ''DEVICE_NAME''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = NVL(:dml.GET_VALUE(''old'', ''DEVICE_NAME''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2())
                     or LNNVL(NVL(:dml.GET_VALUE(''new'', ''DEVICE_TYPE_ID''), ANYDATA.ConvertNumber(null)).AccessNumber() =  NVL(:dml.GET_VALUE(''old'', ''DEVICE_TYPE_ID''), ANYDATA.ConvertNumber(null)).AccessNumber())
                     or LNNVL(NVL(:dml.GET_VALUE(''new'', ''ENCRYPTION_KEY''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = NVL(:dml.GET_VALUE(''old'', ''ENCRYPTION_KEY''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2())
                     or LNNVL(NVL(:dml.GET_VALUE(''new'', ''PREVIOUS_ENCRYPTION_KEY''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = NVL(:dml.GET_VALUE(''old'', ''PREVIOUS_ENCRYPTION_KEY''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2())
                    )
                 )
             )
        )
    or (:dml.get_command_type() = ''DELETE'' and NVL(:dml.GET_VALUE(''old'', ''DEVICE_ACTIVE_YN_FLAG''), ANYDATA.ConvertVarchar2(null)).AccessVarchar2() = ''Y'')
    )
)', 'DEVICE', 'DEVICE' FROM DUAL
;
    lv_rule_owner ALL_RULES.RULE_OWNER%TYPE;
    lv_rule_name ALL_RULES.RULE_NAME%TYPE;
    lv_rule_condition VARCHAR2(4000);
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT r.RULE_OWNER, r.RULE_NAME, DBMS_LOB.SUBSTR(R.RULE_CONDITION, 4000, 1)
          INTO lv_rule_owner, lv_rule_name, lv_rule_condition
          FROM ALL_RULES r
          JOIN ALL_RULE_SET_RULES RSR ON R.RULE_OWNER = RSR.RULE_OWNER AND R.RULE_NAME = RSR.RULE_NAME
          JOIN ALL_CAPTURE D ON D.RULE_SET_OWNER = RSR.RULE_SET_OWNER AND D.RULE_SET_NAME = RSR.RULE_SET_NAME
         WHERE D.CAPTURE_NAME = 'CHANGE_CAPTURE'
           AND REGEXP_LIKE(r.RULE_NAME, l_rec.TABLE_NAME || '[0-9]+','xn')
           AND R.RULE_OWNER = 'REPL_MGR' 
           AND REGEXP_LIKE(DBMS_LOB.SUBSTR(R.RULE_CONDITION, 4000, 1), '^(\s*\()+\s*:dml\.get_object_owner\(\s*\)\s*=\s*''' || l_rec.TABLE_OWNER || '''\s+and\s+:dml\.get_object_name\(\s*\)\s*=\s*''' || l_rec.TABLE_NAME || '''', 'xin');
        DBMS_RULE_ADM.ALTER_RULE(rule_name => lv_rule_owner || '.' || lv_rule_name, condition => l_rec.new_cond);
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Updated rule "' || lv_rule_owner || '.' || lv_rule_name || '":');
        DBMS_OUTPUT.PUT_LINE('from: ' || lv_rule_condition);
        DBMS_OUTPUT.PUT_LINE('to  : ' || l_rec.new_cond);
    END LOOP;
END;
/

