CREATE OR REPLACE PACKAGE DBADMIN.pkg_global
  IS
    TYPE REF_CURSOR IS REF CURSOR;
    TYPE ID_LIST_NDX IS TABLE OF BINARY_INTEGER INDEX BY BINARY_INTEGER;
     
    FUNCTION REQUEST_LOCK(
        l_object_type VARCHAR2,
        l_object_id   VARCHAR2)
     RETURN VARCHAR2;
     
    PROCEDURE RELEASE_LOCK(
        l_handle VARCHAR2);
END;
/

GRANT EXECUTE ON dbadmin.pkg_global TO public;

CREATE PUBLIC SYNONYM PKG_GLOBAL FOR DBADMIN.PKG_GLOBAL;
