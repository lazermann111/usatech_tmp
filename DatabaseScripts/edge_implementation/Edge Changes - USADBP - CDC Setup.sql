-- Stop processes AS REPL_MGR

BEGIN
DBMS_AQADM.stop_QUEUE ('REPL_MGR.Q_CHANGE_APPLY', TRUE, TRUE);
--DBMS_AQADM.stop_QUEUE ('REPL_MGR.Q_CHANGE_APPLY_LAYERS', TRUE, TRUE);
DBMS_APPLY_ADM.STOP_APPLY (apply_name      => 'CHANGE_APPLY');
DBMS_CAPTURE_ADM.STOP_CAPTURE(capture_name      => 'CHANGE_CAPTURE');
END;
/

--create new apply queue
DECLARE
    l_rep_schema VARCHAR2(100) := 'REPL_MGR';
    l_tablespace VARCHAR2(100) := 'REPL_MGR_DATA'; --l_rep_schema||'_DATA';
BEGIN
DBMS_AQADM.CREATE_QUEUE_TABLE (
        queue_table     => l_rep_schema||'.QT_CHANGE_APPLY_LAYERS',
        storage_clause  => 'TABLESPACE '||l_tablespace,
           multiple_consumers => FALSE, 
        queue_payload_type => 'SYS.ANYDATA',
        comment         => 'Holds Edge lcr''s that can be dequeued by user procedure');
DBMS_AQADM.CREATE_QUEUE (
        queue_name => l_rep_schema||'.Q_CHANGE_APPLY_LAYERS', 
        queue_table => l_rep_schema||'.QT_CHANGE_APPLY_LAYERS', 
        retry_delay => 600, 
        max_retries => 10,
        comment         => 'Holds Edge lcr''s that can be dequeued by user procedure');
DBMS_AQADM.START_QUEUE (
        queue_name => l_rep_schema||'.Q_CHANGE_APPLY_LAYERS');
END;
/

--Drop existing global_rule
-- Get the capture rule name

select r.rule_name, r.rule_owner
from dba_rule_set_rules rs, dba_capture c, dba_rules r
where c.rule_set_name=rs.rule_set_name and
      c.rule_set_owner=rs.rule_set_owner and
      rs.rule_name=r.rule_name and
      rs.rule_owner=r.rule_owner and
      upper(r.rule_condition) like '%:DML%';

-- select 'exec dbms_rule_adm.drop_rule('''||r.rule_name||''',force=>true);' 
from dba_rule_set_rules rs, dba_capture c, dba_rules r
where c.rule_set_name=rs.rule_set_name and
      c.rule_set_owner=rs.rule_set_owner and
      rs.rule_name=r.rule_name and
      rs.rule_owner=r.rule_owner and
      upper(r.rule_condition) like '%:DML%';



exec dbms_rule_adm.drop_rule('<capture rule_name',force=>true);

-- get the global apply rule name

select r.rule_name, r.rule_owner
from dba_rule_set_rules rs, dba_apply c, dba_rules r
where c.rule_set_name=rs.rule_set_name and
      c.rule_set_owner=rs.rule_set_owner and
      rs.rule_name=r.rule_name and
      rs.rule_owner=r.rule_owner ;

-- select 'exec dbms_rule_adm.drop_rule('''||r.rule_name||''',force=>true);' 
from dba_rule_set_rules rs, dba_apply c, dba_rules r
where c.rule_set_name=rs.rule_set_name and
      c.rule_set_owner=rs.rule_set_owner and
      rs.rule_name=r.rule_name and
      rs.rule_owner=r.rule_owner ;



exec dbms_rule_adm.drop_rule('<global rule name>',force=>true);

/

--Add Table rule for load fill


DECLARE
    l_rep_schema VARCHAR2(100) := 'REPL_MGR';
    CURSOR l_cur IS
       SELECT OWNER, TABLE_NAME 
         FROM ALL_TABLES
       WHERE OWNER = 'DEVICE' AND TABLE_NAME = 'EVENT';
    l_scn NUMBER;
    l_db_name VARCHAR2(100);    
        l_rule VARCHAR2(4000);
    l_tmp VARCHAR2(4000);    
BEGIN
  SELECT CURRENT_SCN, DB_UNIQUE_NAME
    INTO l_scn, l_db_name
    FROM SYS.V_$DATABASE;
    
   FOR l_rec IN l_cur LOOP
    DBMS_CAPTURE_ADM.PREPARE_SCHEMA_INSTANTIATION(schema_name  => l_rec.OWNER);
    
       l_scn := REPL_MGR.REPLICATE_PKG.prepare_table(l_rec.OWNER,l_rec.TABLE_NAME);
       DBMS_OUTPUT.put_line('Prepared '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'; Setting up capture.');
    DBMS_STREAMS_ADM.ADD_TABLE_RULES(
       table_name          => l_rec.OWNER || '.'||l_rec.TABLE_NAME,
       streams_type        => 'capture',
       streams_name        => 'CHANGE_CAPTURE',
       queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
       include_dml         => TRUE,
       include_ddl         => FALSE,
       include_tagged_lcr  => TRUE,
       --source_database     => l_db_name,
       --dml_rule_name       OUT  VARCHAR2,
       --ddl_rule_name       OUT  VARCHAR2,
       inclusion_rule      => TRUE,
       and_condition       => NULL);
     DBMS_OUTPUT.put_line('Capture set up for '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'.');           
    END LOOP;
DBMS_STREAMS_ADM.ADD_GLOBAL_RULES(
   streams_type        => 'apply',
   streams_name        => 'CHANGE_APPLY',
   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
   include_dml         => TRUE,
   include_ddl         => FALSE,
   include_tagged_lcr  => TRUE,
   source_database     => NULL,
   dml_rule_name       => l_rule,
   ddl_rule_name       => l_tmp,
   inclusion_rule      => TRUE,
   and_condition       => NULL);
DBMS_RULE_ADM.ALTER_RULE(
   rule_name           => l_rule,
   condition  => '(:dml.get_object_owner() =''DEVICE'' and :dml.get_object_name() = ''EVENT'')');
DBMS_APPLY_ADM.SET_EXECUTE(
  rule_name  => l_rule,
  execute    => FALSE);
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => l_rule,
  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY');
END;


-- For Edge

ALTER TABLE PSS.POS_PTA ADD SUPPLEMENTAL LOG GROUP POS_PTA_DEACTIVATION_TS (POS_PTA_DEACTIVATION_TS) ALWAYS;
ALTER TABLE DEVICE.DEVICE ADD SUPPLEMENTAL LOG GROUP DEVICE_NAME_ACTIVE_FLAG (DEVICE_NAME, DEVICE_ACTIVE_YN_FLAG) ALWAYS;
ALTER TABLE DEVICE.DEVICE_SETTING ADD SUPPLEMENTAL LOG GROUP DEVICE_SETTING_PARAMETER_CD (DEVICE_SETTING_PARAMETER_CD) ALWAYS;
ALTER TABLE DEVICE.DEVICE_DEFAULT ADD SUPPLEMENTAL LOG GROUP DEVICE_DFT_NAME_ACTIVE_FLAG (DEVICE_DEFAULT_NAME, DEVICE_DEFAULT_ACTIVE_YN_FLAG) ALWAYS;

GRANT ALL PRIVILEGES TO REPL_MGR; -- Needed for DBMS_STREAMS_ADM.ADD_TABLE_RULES

-- PSS.POS changes we will ignore as they shouldn't occur with out being accompanied by changes to device or pos_pta
DECLARE
    l_rep_schema VARCHAR2(100) := 'REPL_MGR';
    CURSOR l_cur IS
       SELECT OWNER, TABLE_NAME 
         FROM ALL_TABLES
       WHERE (OWNER = 'DEVICE' AND TABLE_NAME IN('DEVICE', 'DEVICE_DEFAULT', 'DEVICE_SETTING'))
          OR (OWNER = 'PSS' AND TABLE_NAME IN('POS_PTA', 'PAYMENT_SUBTYPE', 'CURRENCY', 'AUTHORITY_PAYMENT_MASK', 
                'TERMINAL', 'MERCHANT', 'INTERNAL_PAYMENT_TYPE', 'INTERNAL_AUTHORITY', 'ARAMARK_PAYMENT_TYPE', 'ARAMARK_AUTHORITY',
                'BLACKBRD_TENDER', 'BLACKBRD_AUTHORITY', 'BANORTE_TERMINAL', 'BANORTE_AUTHORITY', 'WEBPOS_TERMINAL', 'WEBPOS_AUTHORITY'))
          OR (OWNER = 'AUTHORITY' AND TABLE_NAME IN('AUTHORITY', 'AUTHORITY_SERVICE'))
          OR (OWNER = 'LOCATION' AND TABLE_NAME IN('LOCATION'));
    l_scn NUMBER;
    l_db_name VARCHAR2(100);    
        l_rule VARCHAR2(4000);
    l_tmp VARCHAR2(4000);    
BEGIN
  SELECT CURRENT_SCN, DB_UNIQUE_NAME
    INTO l_scn, l_db_name
    FROM SYS.V_$DATABASE;
    
   FOR l_rec IN l_cur LOOP
    DBMS_CAPTURE_ADM.PREPARE_SCHEMA_INSTANTIATION(schema_name  => l_rec.OWNER);
    
       l_scn := REPL_MGR.REPLICATE_PKG.prepare_table(l_rec.OWNER,l_rec.TABLE_NAME);
       DBMS_OUTPUT.put_line('Prepared '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'; Setting up capture.');
    DBMS_STREAMS_ADM.ADD_TABLE_RULES(
       table_name          => l_rec.OWNER || '.'||l_rec.TABLE_NAME,
       streams_type        => 'capture',
       streams_name        => 'CHANGE_CAPTURE',
       queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
       include_dml         => TRUE,
       include_ddl         => FALSE,
       include_tagged_lcr  => TRUE,
       --source_database     => l_db_name,
       --dml_rule_name       OUT  VARCHAR2,
       --ddl_rule_name       OUT  VARCHAR2,
       inclusion_rule      => TRUE,
       and_condition       => NULL);
     DBMS_OUTPUT.put_line('Capture set up for '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'.');   
END LOOP;
DBMS_STREAMS_ADM.ADD_GLOBAL_RULES(
   streams_type        => 'apply',
   streams_name        => 'CHANGE_APPLY',
   queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
   include_dml         => TRUE,
   include_ddl         => FALSE,
   include_tagged_lcr  => TRUE,
   source_database     => NULL,
   dml_rule_name       => l_rule,
   ddl_rule_name       => l_tmp,
   inclusion_rule      => TRUE,
   and_condition       => NULL);
DBMS_RULE_ADM.ALTER_RULE(
   rule_name           => l_rule,
   condition  => '(:dml.get_object_owner() !=''DEVICE'' or :dml.get_object_name() != ''EVENT'')');
DBMS_APPLY_ADM.SET_EXECUTE(
  rule_name  => l_rule,
  execute    => FALSE);
DBMS_APPLY_ADM.SET_ENQUEUE_DESTINATION(
  rule_name               => l_rule,
  destination_queue_name  => l_rep_schema||'.Q_CHANGE_APPLY_LAYERS');
END;
/

  
REVOKE ALL PRIVILEGES FROM REPL_MGR;

-- start all queues as repl_mgr
BEGIN
DBMS_AQADM.START_QUEUE ('REPL_MGR.Q_CHANGE_APPLY', TRUE, TRUE);
DBMS_AQADM.START_QUEUE ('REPL_MGR.Q_CHANGE_APPLY_LAYERS', TRUE, TRUE);
DBMS_APPLY_ADM.START_APPLY (apply_name      => 'CHANGE_APPLY');
DBMS_CAPTURE_ADM.START_CAPTURE(capture_name      => 'CHANGE_CAPTURE');
END;
/
