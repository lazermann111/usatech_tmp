CREATE OR REPLACE PACKAGE PSS.PKG_POS_PTA
  IS

PROCEDURE SP_GET_DEVICE_DATA
(
    pn_device_id        IN       device.device_id%TYPE,
    pn_pos_id           OUT      pss.pos.pos_id%TYPE,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
);

PROCEDURE SP_DISABLE_POS_PTA
(
    pn_pos_pta_id       IN       pss.pos_pta.pos_pta_id%TYPE,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
);

PROCEDURE SP_IMPORT_POS_PTA_TEMPLATE
(
    pn_device_id        IN       device.device_id%TYPE,
    pn_pos_pta_tmpl_id  IN       pss.pos_pta_tmpl.pos_pta_tmpl_id%TYPE,
    pv_mode_cd          IN       VARCHAR2,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
);

PROCEDURE SP_GET_POS_PTA
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
);

PROCEDURE SP_GET_OR_CREATE_POS_PTA
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
);

END;
/
