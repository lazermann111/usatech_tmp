WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/edge_implementation/R26.USARDB.User.Creation.sql?rev=HEAD
CREATE ROLE USAT_APP_LAYER_ROLE;
CREATE ROLE USAT_POSM_ROLE;

DECLARE
	lv_app_layer_pwd VARCHAR2(30) := 'APP_LAYER_1';
	lv_posm_layer_pwd VARCHAR2(30) := 'POSM_LAYER_1';
	ln_i NUMBER;
BEGIN
	FOR ln_i IN 1..4 LOOP
		BEGIN
            EXECUTE IMMEDIATE 'CREATE USER APP_LAYER_' || ln_i || ' IDENTIFIED BY ' || lv_app_layer_pwd || ' DEFAULT TABLESPACE USATAPPS_DEFAULT_TBS TEMPORARY TABLESPACE TEMPTS1';
        EXCEPTION
            WHEN OTHERS THEN
                IF SQLCODE != -1920 THEN
                    RAISE;
                END IF;
        END;
		EXECUTE IMMEDIATE 'ALTER USER APP_LAYER_' || ln_i || ' PROFILE USAT_APPS';
		EXECUTE IMMEDIATE 'GRANT CONNECT TO APP_LAYER_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT CREATE SESSION TO APP_LAYER_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT USAT_APP_LAYER_ROLE TO APP_LAYER_' || ln_i;
		EXECUTE IMMEDIATE 'ALTER USER APP_LAYER_' || ln_i || ' DEFAULT ROLE USAT_APP_LAYER_ROLE';
	END LOOP;
	
	FOR ln_i IN 1..4 LOOP
		BEGIN
			EXECUTE IMMEDIATE 'CREATE USER POSM_LAYER_' || ln_i || ' IDENTIFIED BY ' || lv_posm_layer_pwd || ' DEFAULT TABLESPACE USATAPPS_DEFAULT_TBS TEMPORARY TABLESPACE TEMPTS1';
        EXCEPTION
            WHEN OTHERS THEN
                IF SQLCODE != -1920 THEN
                    RAISE;
                END IF;
        END;		
		EXECUTE IMMEDIATE 'ALTER USER POSM_LAYER_' || ln_i || ' PROFILE USAT_APPS';
		EXECUTE IMMEDIATE 'GRANT CONNECT TO POSM_LAYER_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT CREATE SESSION TO POSM_LAYER_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT USAT_POSM_ROLE TO POSM_LAYER_' || ln_i;
		EXECUTE IMMEDIATE 'ALTER USER POSM_LAYER_' || ln_i || ' DEFAULT ROLE USAT_POSM_ROLE';
	END LOOP;	
END;
/

GRANT SELECT ON G4OP.DEX_CODE_ALERT TO USAT_APP_LAYER_ROLE;
GRANT SELECT ON G4OP.DEX_CODE_CONTEXT TO USAT_APP_LAYER_ROLE;
GRANT SELECT ON G4OP.DEX_FILE TO USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON G4OP.DEX_FILE_INS TO USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON G4OP.DEX_FILE_LINE_INS TO USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON REPORT.DATA_IN_PKG TO USAT_APP_LAYER_ROLE;
GRANT SELECT, UPDATE ON REPORT.EPORT TO USAT_APP_LAYER_ROLE;

GRANT SELECT ON CORP.CURRENCY TO USAT_POSM_ROLE;
GRANT SELECT, UPDATE ON CORP.REFUND TO USAT_POSM_ROLE;
GRANT SELECT ON CORP.REFUND_REASON TO USAT_POSM_ROLE;
GRANT SELECT, UPDATE ON REPORT.TRANS TO USAT_POSM_ROLE;
GRANT SELECT ON REPORT.USER_LOGIN TO USAT_POSM_ROLE;

/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBADMIN/MILLIS_TO_TIMESTAMP.fnc?rev=HEAD
CREATE OR REPLACE FUNCTION DBADMIN.MILLIS_TO_TIMESTAMP (
    l_millis NUMBER)
   RETURN TIMESTAMP DETERMINISTIC PARALLEL_ENABLE
IS
    c_epoch_ts CONSTANT TIMESTAMP := TO_TIMESTAMP('01/01/1970', 'MM/DD/YYYY');
BEGIN
    RETURN c_epoch_ts + NUMTODSINTERVAL(l_millis / 1000, 'SECOND');
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CARD_NAME.fnc?rev=HEAD
CREATE OR REPLACE FUNCTION REPORT.CARD_NAME ( 
	pn_cardtype_authority_id REPORT.TRANS.CARDTYPE_AUTHORITY_ID%TYPE,
	pv_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE,
	pv_card_number REPORT.TRANS.CARD_NUMBER%TYPE
) RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
	lv_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE;
BEGIN
	IF pn_cardtype_authority_id IS NOT NULL THEN
		SELECT MAX(CT.CARD_NAME)
		INTO lv_card_name
		FROM REPORT.CARDTYPE_AUTHORITY CA
		JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
		WHERE CA.CARDTYPE_AUTHORITY_ID = pn_cardtype_authority_id;
		
		IF lv_card_name IS NOT NULL THEN
			RETURN lv_card_name;
		END IF;
	END IF;
		
	SELECT MAX(CT.CARD_NAME)
	INTO lv_card_name
	FROM REPORT.CARD_TYPE CT
	JOIN REPORT.TRANS_TYPE TT ON CT.CARD_NAME = TT.TRANS_TYPE_NAME
	WHERE TT.TRANS_TYPE_ID = pv_trans_type_id;
	
	IF lv_card_name IS NOT NULL THEN
		RETURN lv_card_name;
	END IF;

	RETURN REPORT.CARD_COMPANY(pv_card_number);
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/edge_implementation/R26.USARDB.1.sql?rev=HEAD
GRANT EXECUTE ON REPORT.CARD_COMPANY TO CORP;
GRANT EXECUTE ON REPORT.CARD_NAME TO CORP;
GRANT SELECT, UPDATE ON REPORT.EPORT TO G4OP;
GRANT UPDATE ON REPORT.EPORT TO RDW_LOADER;
GRANT UPDATE ON REPORT.ACTIVITY_REF TO RDW_LOADER;
GRANT EXECUTE ON REPORT.GET_FILL_DATE TO RDW_LOADER;
GRANT EXECUTE ON DBADMIN.MILLIS_TO_TIMESTAMP TO PUBLIC;
grant execute on G4OP.RETRY_NONZERO_ERRORS to report;
grant execute on G4OP.LOAD_DEX_FILES to report;
grant select on RECON.TRAN_COUNT_AUDIT to report;
grant execute on UPDATER.PULL_DATA_PKG to report;
grant execute on RECON.AUDIT_PKG to report;

ALTER TABLE REPORT.EPORT ADD (
	LAST_DEX_DATE DATE
);

ALTER TABLE REPORT.CARD_TYPE MODIFY (
	CARD_NAME VARCHAR2(50) NOT NULL
);

DECLARE
	LN_MAX_ID NUMBER;
	LN_I NUMBER;
	LN_VALUE NUMBER;
	LN_AUTHORITY_ID REPORT.AUTHORITY.AUTHORITY_ID%TYPE;
BEGIN
	SELECT MAX(CARD_TYPE_ID)
	INTO LN_MAX_ID
	FROM REPORT.CARD_TYPE;
	
	FOR LN_I IN 1..LN_MAX_ID LOOP
		SELECT CORP.CARD_TYPE_SEQ.NEXTVAL INTO LN_VALUE FROM DUAL;
	END LOOP;
	
	SELECT MAX(AUTHORITY_ID) + 1
	INTO LN_AUTHORITY_ID
	FROM REPORT.AUTHORITY;
	
	INSERT INTO REPORT.AUTHORITY(AUTHORITY_ID, ALIASNAME, AUTH_MERCHANT_ID, AUTH_TERMINAL_ID)
	VALUES(LN_AUTHORITY_ID, 'Generic Authority', '-- null --', '-- null --');
	
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'Diners Club', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Diners Club'), LN_AUTHORITY_ID);
	
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'USAT Stored Value', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USAT Stored Value'), LN_AUTHORITY_ID);	
	
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'USAT Operator', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USAT Operator'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'Sprout', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Sprout'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'Aramark', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Aramark'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'BlackBoard', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'BlackBoard'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'AquaFill', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'AquaFill'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'Pass', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Pass'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'Cash', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Cash'), LN_AUTHORITY_ID);	
		
	INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) VALUES(CORP.CARD_TYPE_SEQ.NEXTVAL, 'Access', '*');
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Access'), LN_AUTHORITY_ID);
		
	INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
	VALUES((SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
		(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'Maintenance'), LN_AUTHORITY_ID);

	COMMIT;	
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/RDW_LOADER/PKG_LOAD_DATA.psk?rev=HEAD
CREATE OR REPLACE PACKAGE RDW_LOADER.PKG_LOAD_DATA AS 

PROCEDURE LOAD_FILL(
    pn_source_fill_id REPORT.FILL.SOURCE_FILL_ID%TYPE,
    pv_source_system_cd REPORT.FILL.SOURCE_SYSTEM_CD%TYPE,
    pv_device_serial_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
    pv_device_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE,
    pd_fill_date REPORT.FILL.FILL_DATE%TYPE,
    pn_cash_amount REPORT.FILL.CASH_AMOUNT%TYPE,
    pn_credit_amount REPORT.FILL.CREDIT_AMOUNT%TYPE,
    pn_other_amount REPORT.FILL.OTHERCARD_AMOUNT%TYPE,
    pn_cash_count REPORT.FILL.CASH_COUNT%TYPE,
    pn_credit_count REPORT.FILL.CREDIT_COUNT%TYPE,
    pn_other_count REPORT.FILL.OTHERCARD_COUNT%TYPE,
    pc_counters_displayed_flag REPORT.FILL.COUNTERS_DISPLAYED_FLAG%TYPE,
    pc_counters_reset_flag REPORT.FILL.COUNTERS_RESET_FLAG%TYPE,
    pc_counters_reported_flag REPORT.FILL.COUNTERS_REPORTED_FLAG%TYPE) ;
    
END PKG_LOAD_DATA;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/RDW_LOADER/PKG_LOAD_DATA.pbk?rev=HEAD
CREATE OR REPLACE PACKAGE BODY RDW_LOADER.PKG_LOAD_DATA AS

    PROCEDURE LOAD_FILL(
        pn_source_fill_id REPORT.FILL.SOURCE_FILL_ID%TYPE,
        pv_source_system_cd REPORT.FILL.SOURCE_SYSTEM_CD%TYPE,
        pv_device_serial_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pv_device_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE,
        pd_fill_date REPORT.FILL.FILL_DATE%TYPE,
        pn_cash_amount REPORT.FILL.CASH_AMOUNT%TYPE,
        pn_credit_amount REPORT.FILL.CREDIT_AMOUNT%TYPE,
        pn_other_amount REPORT.FILL.OTHERCARD_AMOUNT%TYPE,
        pn_cash_count REPORT.FILL.CASH_COUNT%TYPE,
        pn_credit_count REPORT.FILL.CREDIT_COUNT%TYPE,
        pn_other_count REPORT.FILL.OTHERCARD_COUNT%TYPE,
        pc_counters_displayed_flag REPORT.FILL.COUNTERS_DISPLAYED_FLAG%TYPE,
        pc_counters_reset_flag REPORT.FILL.COUNTERS_RESET_FLAG%TYPE,
        pc_counters_reported_flag REPORT.FILL.COUNTERS_REPORTED_FLAG%TYPE) 
    AS
        ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
        ln_prev_fill_id REPORT.FILL.PREV_FILL_ID%TYPE;
        ln_prev_source_fill_id REPORT.FILL.SOURCE_FILL_ID%TYPE;
        ld_prev_fill_date REPORT.FILL.FILL_DATE%TYPE;
        ln_fill_id REPORT.FILL.FILL_ID%TYPE;
        CURSOR l_cur IS
            SELECT *
              FROM REPORT.FILL
             WHERE EPORT_ID = ln_eport_id
               AND FILL_DATE > pd_fill_date;  
        lc_device_different CHAR(1);
        lc_date_different CHAR(1);
        lc_amts_different CHAR(1);  
        ld_old_prev_fill_date REPORT.FILL.FILL_DATE%TYPE;    
    BEGIN
        ln_eport_id := REPORT.DATA_IN_PKG.GET_OR_CREATE_DEVICE(pv_device_serial_cd, pv_source_system_cd, pv_device_type_id);
        SELECT MAX(F.FILL_ID), MAX(DECODE(F.EPORT_ID, ln_eport_id, 'N', 'Y')), MAX(DECODE(F.FILL_DATE, pd_fill_date, 'N', 'Y')),
               MAX(GREATEST(
                    DECODE(F.CASH_AMOUNT, pn_cash_amount, 'N', 'Y'), DECODE(F.CASH_COUNT, pn_cash_count, 'N', 'Y'),
                    DECODE(F.CREDIT_AMOUNT, pn_credit_amount, 'N', 'Y'), DECODE(F.CREDIT_COUNT, pn_credit_count, 'N', 'Y'),
                    DECODE(F.OTHERCARD_AMOUNT, pn_other_amount, 'N', 'Y'), DECODE(F.OTHERCARD_COUNT, pn_other_count, 'N', 'Y'))
               ), MAX(PF.FILL_DATE)
          INTO ln_fill_id, lc_device_different, lc_date_different, lc_amts_different, ld_old_prev_fill_date
          FROM REPORT.FILL F
          LEFT OUTER JOIN REPORT.FILL PF ON F.PREV_FILL_ID = PF.FILL_ID
         WHERE F.SOURCE_SYSTEM_CD = pv_source_system_cd
           AND F.SOURCE_FILL_ID = pn_source_fill_id;
        SELECT MAX(FILL_ID), MAX(SOURCE_FILL_ID), MAX(FILL_DATE)
          INTO ln_prev_fill_id, ln_prev_source_fill_id, ld_prev_fill_date
          FROM (
              SELECT FILL_ID, SOURCE_FILL_ID, FILL_DATE
              FROM REPORT.FILL
            WHERE EPORT_ID = ln_eport_id
              AND FILL_DATE < pd_fill_date
            ORDER BY FILL_DATE DESC)
         WHERE ROWNUM = 1;
        IF ln_fill_id IS NOT NULL THEN
            IF lc_device_different = 'Y' OR lc_date_different = 'Y' OR lc_amts_different = 'Y' THEN
                UPDATE REPORT.FILL SET (
                       PREV_FILL_ID,
                       PREV_SOURCE_FILL_ID, 
                       EPORT_ID, 
                       FILL_DATE, 
                       CASH_AMOUNT, 
                       CREDIT_AMOUNT, 
                       OTHERCARD_AMOUNT, 
                       CASH_COUNT, 
                       CREDIT_COUNT, 
                       OTHERCARD_COUNT, 
                       COUNTERS_DISPLAYED_FLAG, 
                       COUNTERS_RESET_FLAG, 
                       COUNTERS_REPORTED_FLAG
                  ) = (SELECT 
                       ln_prev_fill_id,
                       ln_prev_source_fill_id,
                       ln_eport_id,
                       pd_fill_date,
                       pn_cash_amount,
                       pn_credit_amount,
                       pn_other_amount,
                       pn_cash_count,
                       pn_credit_count,
                       pn_other_count,
                       pc_counters_displayed_flag,
                       pc_counters_reset_flag,
                       pc_counters_reported_flag
                  FROM DUAL)
                 WHERE FILL_ID = ln_fill_id;                
            END IF;
        ELSE
            SELECT REPORT.FILL_SEQ.NEXTVAL, 'Y', 'Y', 'Y'
              INTO ln_fill_id, lc_device_different, lc_date_different, lc_amts_different
              FROM DUAL;
            INSERT INTO REPORT.FILL(
                FILL_ID,
                SOURCE_FILL_ID,
                SOURCE_SYSTEM_CD, 
                PREV_FILL_ID,
                PREV_SOURCE_FILL_ID, 
                EPORT_ID, 
                FILL_DATE, 
                CASH_AMOUNT, 
                CREDIT_AMOUNT, 
                OTHERCARD_AMOUNT, 
                CASH_COUNT, 
                CREDIT_COUNT, 
                OTHERCARD_COUNT, 
                COUNTERS_DISPLAYED_FLAG, 
                COUNTERS_RESET_FLAG, 
                COUNTERS_REPORTED_FLAG)
              VALUES(
                ln_fill_id,
                pn_source_fill_id,
                pv_source_system_cd,
                ln_prev_fill_id,
                ln_prev_source_fill_id,
                ln_eport_id,
                pd_fill_date,
                pn_cash_amount,
                pn_credit_amount,
                pn_other_amount,
                pn_cash_count,
                pn_credit_count,
                pn_other_count,
                pc_counters_displayed_flag,
                pc_counters_reset_flag,
                pc_counters_reported_flag);    
        END IF;
                
        IF lc_device_different = 'Y' OR lc_date_different = 'Y' THEN
            FOR l_rec IN l_cur LOOP
                SELECT MAX(FILL_ID), MAX(SOURCE_FILL_ID)
                  INTO ln_prev_fill_id, ln_prev_source_fill_id
                  FROM (
                      SELECT FILL_ID, SOURCE_FILL_ID, FILL_DATE
                      FROM REPORT.FILL
                    WHERE EPORT_ID = l_rec.EPORT_ID
                      AND FILL_DATE < l_rec.FILL_DATE
                    ORDER BY FILL_DATE DESC)
                 WHERE ROWNUM = 1;
              
                 UPDATE REPORT.FILL
                    SET PREV_FILL_ID = ln_prev_fill_id, 
                        PREV_SOURCE_FILL_ID = ln_prev_source_fill_id
                  WHERE FILL_ID = l_rec.FILL_ID;      
            END LOOP;
            -- add update last fill date for condition report
            UPDATE REPORT.EPORT
               SET LAST_FILL_DATE = pd_fill_date
             WHERE EPORT_ID = ln_eport_id
               AND NVL(LAST_FILL_DATE, MIN_DATE) < pd_fill_date;
               
            REPORT.DW_PKG.UPDATE_FILL_INFO(ln_fill_id);
        END IF;
        
        IF NVL(ld_old_prev_fill_date, MIN_DATE) != NVL(ld_prev_fill_date, MIN_DATE) OR lc_device_different = 'Y' OR lc_date_different = 'Y' OR lc_amts_different = 'Y' THEN
            CORP.PAYMENTS_PKG.UPDATE_FILL_BATCH(ln_fill_id);
        END IF;
        
        IF NVL(ld_old_prev_fill_date, MIN_DATE) != NVL(ld_prev_fill_date, MIN_DATE) OR lc_device_different = 'Y' OR lc_date_different = 'Y' THEN
            -- Added to capture first fills (03-05-2007 BSK)
            IF ld_prev_fill_date IS NULL THEN
                SELECT MAX(START_DATE)
                  INTO ld_prev_fill_date
                  FROM REPORT.TERMINAL_EPORT
                 WHERE START_DATE <= pd_fill_date
                   AND EPORT_ID = ln_eport_id;
            END IF;
            
            -- Ignore first fill period (added June 8th for performance reasons - BSK)
            IF ld_prev_fill_date IS NOT NULL THEN
                UPDATE REPORT.ACTIVITY_REF 
                   SET FILL_DATE = REPORT.GET_FILL_DATE(TRAN_ID)
                 WHERE EPORT_ID = ln_eport_id 
                   AND TRAN_DATE BETWEEN ld_prev_fill_date AND pd_fill_date;
            END IF;
        END IF;
        
    END LOAD_FILL;

END PKG_LOAD_DATA;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CARD_COMPANY.fnc?rev=1.4
CREATE OR REPLACE FUNCTION REPORT.CARD_COMPANY (
	pv_card_number VARCHAR2
) RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
	lv_cn REPORT.TRANS.CARD_NUMBER%TYPE;
	lv_default REPORT.TRANS.CARD_NUMBER%TYPE := 'Unknown';
	lv_ch2 VARCHAR2(2);
	lv_ch3 VARCHAR2(3);
	lv_ch6 VARCHAR2(6);
	ln_len NUMBER;
BEGIN
	IF pv_card_number IS NULL THEN
		RETURN lv_default;
	END IF;

	lv_cn := SUBSTR(TRIM(pv_card_number), 1, 50);
	ln_len := INSTR(lv_cn, '=') - 1;
	IF ln_len = -1 THEN
		ln_len := LENGTH(lv_cn);
	END IF;
	
	IF ln_len < 15 THEN
		RETURN lv_default;
	END IF;
	
	IF ln_len = 16 AND lv_cn LIKE '4%' THEN
		RETURN 'Visa';
	END IF;
	
	lv_ch2 := SUBSTR(lv_cn, 1, 2);
	IF ln_len = 16 AND lv_ch2 BETWEEN '51' AND '55' THEN
		RETURN 'MasterCard';
	ELSIF ln_len = 15 AND lv_ch2 IN ('34', '37') THEN
		RETURN 'American Express';
	END IF;
	
	lv_ch3 := SUBSTR(lv_cn, 1, 3);
	lv_ch6 := SUBSTR(lv_cn, 1, 6);
	IF ln_len = 16 AND (lv_cn LIKE '6011%'
		OR lv_ch6 BETWEEN '622126' AND '622925'
		OR lv_ch3 BETWEEN '644' AND '649'
		OR lv_ch2 = '65') THEN
		RETURN 'Discover';
	END IF;
	
	RETURN lv_default;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/MASK_CARD.fnc?rev=1.10
CREATE OR REPLACE FUNCTION REPORT.MASK_CARD (
	l_card IN TRANS.CARD_NUMBER%TYPE,
	l_tt_id IN TRANS.TRANS_TYPE_ID%TYPE
	)
RETURN VARCHAR PARALLEL_ENABLE DETERMINISTIC IS
	lv_card_number VARCHAR(4000);
BEGIN
	lv_card_number := REGEXP_SUBSTR(l_card, '[0-9]{13,}');

	IF lv_card_number IS NULL THEN
		RETURN l_card;
	ELSE
		RETURN RPAD(SUBSTR(lv_card_number, 1, 6), LENGTH(lv_card_number) - 4, '*') || SUBSTR(lv_card_number, -4);
	END IF;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/USAT_MASK_PASS_DATA.fnc?rev=1.2
CREATE OR REPLACE FUNCTION REPORT.USAT_MASK_PASS_DATA (
    l_track_data IN VARCHAR2
)
RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
BEGIN
	RETURN REPORT.MASK_CARD(l_track_data, -1);
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/TRBIU_TRANS.trg?rev=1.3
CREATE OR REPLACE TRIGGER report.trbiu_trans
BEFORE INSERT OR UPDATE ON report.trans
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN    
    IF :NEW.refresh_ind = 'N' THEN
        :NEW.refresh_ind := NULL;
    ELSE
        :NEW.refresh_ind := 'Y';
    END IF;
END;

/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DW_PKG.pbk?rev=1.20
CREATE OR REPLACE PACKAGE BODY REPORT.DW_PKG IS
    FUNCTION GET_OR_CREATE_EXPORT_BATCH(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
     RETURN ACTIVITY_REF.BATCH_ID%TYPE
    IS
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        SELECT eb.batch_id
          INTO l_export_id
          FROM REPORT.EXPORT_BATCH eb
         WHERE eb.CUSTOMER_ID = l_customer_id
           AND eb.EXPORT_TYPE = 1
           AND eb.STATUS = 'O';
        RETURN l_export_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT REPORT.EXPORT_BATCH_SEQ.NEXTVAL
              INTO l_export_id
              FROM DUAL;
            
            INSERT INTO REPORT.EXPORT_BATCH(
                   BATCH_ID,
                   CUSTOMER_ID,
                   EXPORT_TYPE,
                   STATUS)
               SELECT
                   l_export_id,
                   l_customer_id,
                   1,
                   'O'
                 FROM DUAL
                WHERE NOT EXISTS(
                    SELECT 1
                      FROM REPORT.EXPORT_BATCH eb
                     WHERE eb.CUSTOMER_ID = l_customer_id
                       AND eb.EXPORT_TYPE = 1
                       AND eb.STATUS = 'O');
            IF SQL%FOUND THEN
                RETURN l_export_id;
            ELSE
                RETURN GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
            END IF;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH(
        l_export_id REPORT.EXPORT_BATCH.BATCH_ID%TYPE,
        l_customer_id REPORT.EXPORT_BATCH.CUSTOMER_ID%TYPE)
    IS
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        UPDATE REPORT.EXPORT_BATCH EB SET (
                STATUS,
                TRAN_CREATE_DT_BEG,
                TRAN_CREATE_DT_END,
                TOT_TRAN_ROWS,
                TOT_TRAN_AMOUNT
              ) = (SELECT
                'A',
                MIN(A.TRAN_DATE),
                MAX(A.TRAN_DATE),
                COUNT(*),
                SUM(A.TOTAL_AMOUNT)
              FROM REPORT.ACTIVITY_REF A
             WHERE A.BATCH_ID = eb.BATCH_ID)
         WHERE EB.BATCH_ID = l_export_id
           AND EB.STATUS = 'O';
         CORP.PAYMENTS_PKG.UPDATE_EXPORT_BATCH(l_export_id);
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCHES
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID, CUSTOMER_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CRD_DATE < TRUNC(SYSDATE - (5/24)) + (5/24);
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_rec.CUSTOMER_ID);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH_FOR(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CUSTOMER_ID = l_customer_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_customer_id);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF SET CC_APPR_CODE = l_appr_cd,
            SETTLE_STATE = (SELECT STATE_LABEL FROM TRANS_STATE S
                WHERE l_settle_state_id = S.STATE_ID)
         WHERE TRAN_ID = l_trans_id;
    END;

    PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_ref_nbr ACTIVITY_REF.REF_NBR%TYPE;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_create_dt ACTIVITY_REF.CREATE_DT%TYPE;
        l_update_dt ACTIVITY_REF.UPDATE_DT%TYPE;
        l_qty ACTIVITY_REF.QUANTITY%TYPE;
        l_terminal_id ACTIVITY_REF.TERMINAL_ID%TYPE;
        l_eport_id ACTIVITY_REF.EPORT_ID%TYPE;
        l_tran_date ACTIVITY_REF.TRAN_DATE%TYPE;
        l_trans_type_id ACTIVITY_REF.TRANS_TYPE_ID%TYPE;
        l_total_amount ACTIVITY_REF.TOTAL_AMOUNT%TYPE;
        l_currency_id ACTIVITY_REF.CURRENCY_ID%TYPE;
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        CURSOR l_cur IS
         SELECT
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
			T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID
          FROM TRANS_TYPE TT, TRANS T, TERMINAL TERM,
            REGION R, TERMINAL_REGION TR, LOCATION L,
            TRANS_STATE S
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND T.TERMINAL_ID = TERM.TERMINAL_ID (+)
            AND T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
            AND T.TRAN_ID = l_trans_id
            AND T.SETTLE_STATE_ID = S.STATE_ID;
    BEGIN
        DELETE FROM activity_ref
         WHERE tran_id = l_trans_id
         RETURNING ref_nbr, batch_id, create_dt, SYSDATE, terminal_id, eport_id, tran_date, trans_type_id, total_amount, quantity, currency_id
              INTO l_ref_nbr, l_export_id, l_create_dt, l_update_dt, l_terminal_id, l_eport_id, l_tran_date, l_trans_type_id, l_total_amount, l_qty, l_currency_id ;
        -- if exists, then update the trans_stat_by_day
        IF NVL(l_qty, 0) != 0 AND l_create_dt is NOT NULL THEN
          update trans_stat_by_day 
          set tran_count = NVL(tran_count, 0) - 1,
          vend_count = NVL(vend_count, 0) - l_qty,
          tran_amount = NVL(tran_amount, 0) - NVL(l_total_amount, 0)
          where terminal_id = l_terminal_id
          and eport_id=l_eport_id
          and tran_date = trunc(l_tran_date, 'DD')
          and trans_type_id=l_trans_type_id
          and currency_id=l_currency_id;
        END IF;
        FOR l_rec IN l_cur LOOP
            IF NVL(l_export_id, 0) = 0 THEN
                IF l_rec.customer_id IS NULL THEN
                    l_export_id := 0;
                ELSE
                    l_export_id := GET_OR_CREATE_EXPORT_BATCH(l_rec.customer_id);
                END IF;
            END IF;
            IF l_ref_nbr IS NULL THEN
                SELECT REF_NBR_SEQ.NEXTVAL
                  INTO l_ref_nbr 
                  FROM DUAL;
            END IF;
            SELECT SUM(AMOUNT)
              INTO l_qty
              FROM REPORT.PURCHASE
             WHERE TRAN_ID = l_trans_id;

            INSERT INTO ACTIVITY_REF(
                REGION_ID,
                REGION_NAME,
                LOCATION_NAME,
                LOCATION_ID,
                TERMINAL_ID,
                TERMINAL_NAME,
                TERMINAL_NBR,
                TRAN_ID,
                TRANS_TYPE_ID,
                TRANS_TYPE_NAME,
                SETTLE_STATE,
                CARD_NUMBER,
                CARD_COMPANY,
                CC_APPR_CODE,
                TRAN_DATE,
                FILL_DATE,
                TOTAL_AMOUNT,
                ORIG_TRAN_ID,
                BATCH_ID,
                VEND_COLUMN,
                QUANTITY,
                REF_NBR,
                EPORT_ID,
                DESCRIPTION,
                CREATE_DT,
                UPDATE_DT,
                CURRENCY_ID)
            SELECT
                l_rec.REGION_ID,
                l_rec.REGION_NAME,
                NVL(l_rec.LOCATION_NAME, 'Unknown'),
                NVL(l_rec.LOCATION_ID, 0),
                l_rec.TERMINAL_ID,
                l_rec.TERMINAL_NAME,
                l_rec.TERMINAL_NBR,
                l_trans_id,
                l_rec.TRANS_TYPE_ID,
                l_rec.TRANS_TYPE_NAME,
                l_rec.STATE_LABEL,
                l_rec.CARD_NUMBER,
                REPORT.CARD_NAME(l_rec.CARDTYPE_AUTHORITY_ID, l_rec.TRANS_TYPE_ID, l_rec.CARD_NUMBER),
                l_rec.CC_APPR_CODE,
                l_rec.CLOSE_DATE TRAN_DATE,
                GET_FILL_DATE(l_trans_id) FILL_DATE,
                l_rec.TOTAL_AMOUNT,
                l_rec.ORIG_TRAN_ID,
                l_export_id,
                VEND_COLUMN_STRING(l_trans_id),
                l_qty,
                l_ref_nbr,
                l_rec.EPORT_ID,
                l_rec.DESCRIPTION,
                NVL(l_create_dt, SYSDATE),
                l_update_dt,
                l_rec.currency_id
              FROM DUAL;
              -- update trans_stat_by_day
              IF NVL(l_qty, 0) != 0 THEN
              MERGE INTO REPORT.trans_stat_by_day TGT
              USING (SELECT l_rec.TERMINAL_ID AS terminal_id, l_rec.EPORT_ID AS eport_id, trunc(l_rec.CLOSE_DATE, 'DD') AS tran_date, l_rec.TRANS_TYPE_ID as trans_type_id, l_rec.currency_id as currency_id FROM DUAL) SRC
              ON (SRC.terminal_id = TGT.terminal_id and SRC.eport_id = TGT.eport_id and SRC.tran_date=TGT.tran_date and SRC.trans_type_id=TGT.trans_type_id and SRC.currency_id=TGT.currency_id)
              WHEN MATCHED
              THEN
                UPDATE 
                SET TGT.tran_count = NVL(TGT.tran_count, 0) + 1,
                TGT.vend_count = NVL(TGT.vend_count, 0) + l_qty,
                TGT.tran_amount = NVL(TGT.tran_amount, 0) + NVL(l_rec.TOTAL_AMOUNT, 0)
              WHEN NOT MATCHED 
              THEN
                INSERT ( terminal_id,
                  eport_id,
                  tran_date, 
                  trans_type_id,
                  tran_count,
                  vend_count, 
                  tran_amount,
                  currency_id) 
                VALUES( l_rec.TERMINAL_ID,
                  l_rec.EPORT_ID,
                  trunc(l_rec.CLOSE_DATE, 'DD'),
                  l_rec.TRANS_TYPE_ID, 
                  1,
                  l_qty,
                  NVL(l_rec.TOTAL_AMOUNT, 0),
                  l_rec.currency_id);
                  
              --logic to update report.eport column for condition report
                     IF l_rec.TRANS_TYPE_ID IN (16, 19) THEN
                       -- credit, debit
                       UPDATE REPORT.EPORT SET
                           FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                           FIRST_CREDIT_TRAN_DATE=LEAST(NVL(FIRST_CREDIT_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                           LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                           LAST_CREDIT_TRAN_DATE=GREATEST(NVL(LAST_CREDIT_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                       WHERE EPORT_ID=l_rec.EPORT_ID;
                     ELSIF l_rec.TRANS_TYPE_ID = 22 THEN
                       -- cash
                       UPDATE REPORT.EPORT SET
                           FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                           FIRST_CASH_TRAN_DATE=LEAST(NVL(FIRST_CASH_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                           LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                           LAST_CASH_TRAN_DATE=GREATEST(NVL(LAST_CASH_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                       WHERE EPORT_ID=l_rec.EPORT_ID;
                     ELSE
                       UPDATE REPORT.EPORT SET
                           FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                           LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                       WHERE EPORT_ID=l_rec.EPORT_ID;
                     END IF;
              END IF;
          END LOOP;
    END;
    
    PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF A SET (
            region_id,
            region_name,
            location_name,
            location_id,
            terminal_name,
            terminal_nbr) = (
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            TERMINAL_NAME,
            TERM.TERMINAL_NBR
          FROM TERMINAL TERM,  REGION R, TERMINAL_REGION TR, LOCATION L
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND TERM.TERMINAL_ID = A.TERMINAL_ID)
        WHERE A.TERMINAL_ID = l_terminal_id
          AND A.TRAN_DATE > SYSDATE - 90;
    END;

    PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
        l_fill_date FILL.FILL_DATE%TYPE;
        l_prev_fill_date FILL.FILL_DATE%TYPE;
        l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        SELECT FILL_DATE, EPORT_ID
          INTO l_fill_date, l_eport_id
          FROM FILL
         WHERE FILL_ID = l_fill_id;
         
         -- add update last fill date for condition report
         UPDATE REPORT.EPORT
         SET LAST_FILL_DATE=l_fill_date
         WHERE EPORT_ID=l_eport_id
         AND NVL(LAST_FILL_DATE, min_date) < l_fill_date;

          SELECT MAX(FILL_DATE)
          INTO l_prev_fill_date
          FROM FILL
         WHERE FILL_DATE < l_fill_date
           AND EPORT_ID = l_eport_id;
           
        -- Added to capture first fills (03-05-2007 BSK)
        IF l_prev_fill_date IS NULL THEN
            SELECT MAX(START_DATE)
              INTO l_prev_fill_date
              FROM REPORT.TERMINAL_EPORT
             WHERE START_DATE <= l_fill_date
               AND EPORT_ID = l_eport_id;
        END IF;
        
        -- Ignore first fill period (added June 8th for performance reasons - BSK)
        IF l_prev_fill_date IS NOT NULL THEN
            UPDATE ACTIVITY_REF SET FILL_DATE = GET_FILL_DATE(TRAN_ID)
             WHERE EPORT_ID = l_eport_id AND TRAN_DATE BETWEEN l_prev_fill_date AND l_fill_date;
          END IF;
    END;
    
    PROCEDURE UPDATE_FILL_INFO(
            l_fill_id   FILL.FILL_ID%TYPE) 
    IS
        l_export_batch_id FILL.BATCH_ID%TYPE;
        l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
        l_terminal_id FILL.TERMINAL_ID%TYPE;
    
    BEGIN
        SELECT T.CUSTOMER_ID, T.TERMINAL_ID INTO l_customer_id, l_terminal_id
        FROM REPORT.TERMINAL T 
        JOIN REPORT.TERMINAL_EPORT TE ON T.TERMINAL_ID=TE.TERMINAL_ID
        JOIN REPORT.FILL F ON F.EPORT_ID=TE.EPORT_ID
        WHERE F.FILL_ID=l_fill_id
        AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE) AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE);
        
        l_export_batch_id := GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
        UPDATE REPORT.FILL SET BATCH_ID = l_export_batch_id, TERMINAL_ID = l_terminal_id,
        REF_NBR = NVL(REF_NBR, REF_NBR_SEQ.nextval)
        WHERE FILL_ID = l_fill_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN;  
    END;
    
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.psk?rev=1.12
CREATE OR REPLACE PACKAGE REPORT.DATA_IN_PKG IS
--
-- Receives data from external systems
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- BKRUG        10-18-04    NEW
  PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PLS_INTEGER DEFAULT 1,
        l_price PURCHASE.AMOUNT%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL);
  PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/);
  PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE);
  PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_masked_card_number TRANS_C.MASKED_CARD_NUMBER%TYPE,
        l_hashed_card_number TRANS_C.HASHED_CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
		l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
		l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL);
  PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE);
  PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE);
  FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE;
END; -- Package spec
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.pbk?rev=1.19
CREATE OR REPLACE PACKAGE BODY REPORT.DATA_IN_PKG IS
   FK_NOT_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(FK_NOT_FOUND, -2291);

    FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_device_id EPORT.EPORT_ID%TYPE;
        l_use_device_type EPORT.DEVICE_TYPE_ID%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_device_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_device_serial;
         -- NOTE: eventually the above should be source system dependent
         RETURN l_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT EPORT_SEQ.NEXTVAL INTO l_device_id FROM DUAL;
            IF l_device_type IS NOT NULL THEN
                l_use_device_type := l_device_type;
            ELSIF l_device_serial LIKE 'E4%' THEN
                l_use_device_type := 0; -- G4
			ELSIF l_device_serial LIKE 'EE%' THEN
                l_use_device_type := 13; -- Edge
            ELSIF l_device_serial LIKE 'G%' THEN
                l_use_device_type := 1; -- G5-G8
			ELSIF l_device_serial LIKE 'K%' THEN
                l_use_device_type := 11; -- Kiosk
            ELSIF l_device_serial LIKE 'M1%' THEN
                l_use_device_type := 6; -- MEI
            --ELSIF l_device_serial LIKE '10%' THEN -- esuds
            ELSIF l_device_serial LIKE '10%' THEN
                l_use_device_type := 3; -- Radisys Brick
            ELSE
                l_use_device_type := 10;
            END IF;
            BEGIN
                INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_device_id, l_device_serial, SYSDATE, l_use_device_type);
 		        COMMIT;
            EXCEPTION
                WHEN FK_NOT_FOUND THEN
                    ROLLBACK;
                    RAISE_APPLICATION_ERROR(-20889, 'Device Type ''' || l_use_device_type || ''' does not exist');
                WHEN DUP_VAL_ON_INDEX THEN
                    ROLLBACK;
                    IF l_tries > 0 THEN
                        RETURN GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type, l_tries - 1);
                    ELSE
                        RAISE;
                    END IF;
                WHEN OTHERS THEN
                    ROLLBACK;
                    RAISE;
            END;
            RETURN l_device_id;
        WHEN OTHERS THEN
            RAISE;
    END;
  
    FUNCTION GET_OR_CREATE_MERCHANT(
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN CORP.MERCHANT.MERCHANT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_merchant_id CORP.MERCHANT.MERCHANT_ID%TYPE;
    BEGIN
        SELECT MERCHANT_ID
          INTO l_merchant_id
          FROM CORP.MERCHANT
         WHERE MERCHANT_NBR = l_merchant_num;
         RETURN l_merchant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT CORP.MERCHANT_SEQ.NEXTVAL INTO l_merchant_id FROM DUAL;
            INSERT INTO CORP.MERCHANT(MERCHANT_ID, MERCHANT_NBR, DESCRIPTION, UPD_BY)
			  		 VALUES(l_merchant_id, l_merchant_num, 'Created for source system "'||l_source_system_cd||'"', 0);
            COMMIT;
            RETURN l_merchant_id;
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END;

  /* This allows external systems to add transactions. Credit, debit,
   * pass, access, or maintenance cards or refund, chargeback transaction
   * or cash should be added this way. Refunds
   * and chargebacks should always provide a valid original machine tran number.
   */
  PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_masked_card_number TRANS_C.MASKED_CARD_NUMBER%TYPE,
        l_hashed_card_number TRANS_C.HASHED_CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
		l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
		l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL)
    IS
        l_create_date TRANS.CREATE_DATE%TYPE;
        l_eport_id TRANS.EPORT_ID%TYPE;
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE;
        l_real_amount TRANS.TOTAL_AMOUNT%TYPE;
        l_currency_id TRANS.CURRENCY_ID%TYPE;
        l_tran_id TRANS.TRAN_ID%TYPE;
		l_cardtype_authority_id TRANS.CARDTYPE_AUTHORITY_ID%TYPE := NULL;
    BEGIN
        -- check for dup
        BEGIN
            SELECT CREATE_DATE
              INTO l_create_date
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            RAISE_APPLICATION_ERROR(-20880, 'A transaction with MACHINE_TRANS_NUM = "'
                ||l_machine_trans_no||'" from the source system "'
                ||l_source_system_cd||'" already exists. It was received '
                ||TO_CHAR(l_create_date, 'MM/DD/YYYY HH24:MI:SS')
                ||'. Transaction was NOT added.');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL; -- CONTINUE, no dup found
            WHEN OTHERS THEN
                RAISE;
        END;
        
        --get needed lookup values
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type);
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        
        IF l_orig_machine_trans_no IS NOT NULL THEN
            BEGIN
                SELECT TRAN_ID
                  INTO l_orig_tran_id
                  FROM TRANS
                 WHERE MACHINE_TRANS_NO = l_orig_machine_trans_no
                   AND SOURCE_SYSTEM_CD = NVL(l_orig_source_system_cd, l_source_system_cd);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with MACHINE_TRANS_NUM = "'||l_orig_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
        
        --get currency id from currency code
        IF l_currency_code IS NOT NULL THEN
        	BEGIN
        		SELECT currency_id
        		  INTO l_currency_id
        		  FROM CORP.currency
        		 WHERE currency_code = l_currency_code;
        	EXCEPTION
        		WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'No currency is mapped in the'
						|| ' system for currency code ''' || l_currency_code
						|| '''. A transaction can not be added with no currency'
						|| ' identified. Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
        	END;
        ELSE
        	RAISE_APPLICATION_ERROR(-20880, 'A transaction can not be entered '
				|| 'without a currency identified. Transaction was NOT added.');
        END IF;
		
		IF l_card_name IS NOT NULL THEN
			SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
			INTO l_cardtype_authority_id
			FROM REPORT.CARDTYPE_AUTHORITY CA
			JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
			WHERE CT.CARD_NAME = l_card_name;
			
			IF l_cardtype_authority_id IS NULL THEN
				SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
				INTO l_cardtype_authority_id
				FROM REPORT.CARDTYPE_AUTHORITY CA
				JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
				JOIN REPORT.TRANS_TYPE TT ON CT.CARD_NAME = TT.TRANS_TYPE_NAME
				WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
			END IF;
		END IF;
        
        IF l_trans_type_id IN(20,21) THEN -- make it negative
            l_real_amount := -ABS(l_total_amount);
        ELSE
            l_real_amount := ABS(l_total_amount);
        END IF;
        SELECT TRANS_SEQ.NEXTVAL
          INTO l_tran_id
          FROM DUAL;
        INSERT INTO TRANS(
            TRAN_ID,
            MACHINE_TRANS_NO,
            SOURCE_SYSTEM_CD,
            EPORT_ID,
			CARDTYPE_AUTHORITY_ID,
            START_DATE,
            CLOSE_DATE,
            TRANS_TYPE_ID,
            TOTAL_AMOUNT,
            CARD_NUMBER,
            SERVER_DATE,
            SETTLE_STATE_ID,
            SETTLE_DATE,
            PREAUTH_AMOUNT,
            PREAUTH_DATE,
            CC_APPR_CODE,
            MERCHANT_ID,
            DESCRIPTION,
            ORIG_TRAN_ID,
			CURRENCY_ID)
          SELECT
            l_tran_id,
            l_machine_trans_no,
            l_source_system_cd,
            l_eport_id,
			l_cardtype_authority_id,
            l_start_date,
            l_close_date,
            l_trans_type_id,
            l_real_amount,
            l_masked_card_number,
            l_received_date,
            l_settle_state_id,
            l_settle_date,
            l_preauth_amount,
            l_preauth_date,
            l_approval_cd,
            l_merchant_id,
            l_description,
            l_orig_tran_id,
            l_currency_id
          FROM DUAL;
          IF l_trans_type_id <> 22 AND l_hashed_card_number IS NOT NULL THEN
              INSERT INTO TRANS_C(
                TRAN_ID,
                MASKED_CARD_NUMBER,
                HASHED_CARD_NUMBER,
                CLOSE_DATE)
              VALUES(
                l_tran_id,
                l_masked_card_number,
                l_hashed_card_number,
                l_close_date);
          END IF;
    END;

    PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PLS_INTEGER DEFAULT 1,
        l_price PURCHASE.AMOUNT%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL)
    IS
    BEGIN
        --FOR i IN 1..l_quantity LOOP
            INSERT INTO PURCHASE(
                PURCHASE_ID,
                TRAN_ID,
                TRAN_DATE,
                AMOUNT,
                PRICE,
                VEND_COLUMN,
                DESCRIPTION,
                MDB_NUMBER)
              SELECT
                PURCHASE_SEQ.NEXTVAL,
                TRAN_ID,
                CLOSE_DATE,
                l_quantity,
                l_price,
                l_column_label,
                l_product_desc,
                l_column_num
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            IF SQL%ROWCOUNT = 0 THEN
                RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Item was NOT added.');
            END IF;
--        END LOOP;
    END;
    
    /* This procedure allows external systems to update the settlement info
     * of a transaction.
     *
     */
    PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE)
    IS
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
    BEGIN
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        UPDATE TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID) =
            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
                    l_settle_date,
                    NVL(l_approval_cd, T.CC_APPR_CODE),
                    NVL(l_merchant_id, T.MERCHANT_ID)
              FROM DUAL)
         WHERE T.MACHINE_TRANS_NO = l_machine_trans_no
           AND T.SOURCE_SYSTEM_CD = l_source_system_cd;
        IF SQL%ROWCOUNT = 0 THEN
            RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Settle info was NOT updated.');
        END IF;
    END;
    
    /* This procedure allows external systems to update the device info
     * of their devices
     */
    PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_ID = l_device_id;
    END;
    
    PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE)
    IS
       l_prev_fill_date FILL.FILL_DATE%TYPE;
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
  	    INSERT INTO FILL (FILL_ID, EPORT_ID, FILL_DATE)
 	         VALUES(FILL_SEQ.NEXTVAL, l_eport_id, l_fill_date);
    END;
    /* need to change alerts to relate to devices not terminals
    PROCEDURE ADD_ALERT(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_alert_id ALERT.ALERT_ID%TYPE,
       l_alert_date TERMINAL_ALERT.ALERT_DATE%TYPE,
       l_details TERMINAL_ALERT.DETAILS%TYPE)
    IS
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
    	 SELECT TERMINAL_ALERT_SEQ.NEXTVAL INTO l_id FROM DUAL;
    	 INSERT INTO TERMINAL_ALERT (TERMINAL_ALERT_ID, ALERT_ID, TERMINAL_ID, ALERT_DATE, DETAILS, RESPONSE_SENT )
     		VALUES(l_id,l_alert_id,l_terminal_id,l_alert_date,l_details, l_sent);
    END;
    */
    /*
    Right now Legacy G4 and BEX do not have any additional refund information (PROBLEM_DATE is the TRAN_DATE, REFUND_STATUS is always 1)
    */
    
    PROCEDURE UPDATE_POS_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_effective_date TERMINAL_EPORT.START_DATE%TYPE)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        /*UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_SERIAL_NUM = l_device_serial;*/
    END;

    /* Creates a customer, location and terminal, if necessary for the given device
     * This should only be used by BEX machines because they are currently not
     * configured in the Customer Reporting System.
     */
    PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE)
    IS
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_location_id REPORT.LOCATION.LOCATION_ID%TYPE;
        l_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
        l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
        l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
        l_te_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
        l_old_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_eport_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_serial_num;

        SELECT MAX(CUSTOMER_ID)
          INTO l_customer_id
          FROM CORP.CUSTOMER
         WHERE CUSTOMER_ALT_NAME = l_customer_abbr;

        IF l_customer_id IS NULL THEN
            SELECT CORP.CUSTOMER_SEQ.NEXTVAL
              INTO l_customer_id
              FROM DUAL;

            INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_ALT_NAME, STATUS, CREATE_BY)
	           VALUES(l_customer_id, l_customer_abbr, l_customer_abbr, 'A', 0);	
        END IF;

        SELECT MAX(LOCATION_ID)
          INTO l_location_id
          FROM LOCATION
         WHERE LOCATION_NAME = l_location_name
           AND EPORT_ID = l_eport_id;

        IF l_location_id IS NULL THEN
		  	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	  		 INSERT INTO LOCATION(LOCATION_ID, TERMINAL_ID, LOCATION_NAME, CREATE_BY, STATUS, EPORT_ID)
	  		     VALUES(l_location_id, 0, l_location_name, 0, 'A', l_eport_id);
        END IF;

        SELECT MAX(TE.TERMINAL_ID)
          INTO l_terminal_id
          FROM TERMINAL_EPORT TE, TERMINAL T
         WHERE TE.TERMINAL_ID = T.TERMINAL_ID
           AND TE.EPORT_ID = l_eport_id
           AND T.CUSTOMER_ID = l_customer_id
           AND T.LOCATION_ID = l_location_id;

        IF l_terminal_id IS NULL THEN
            --customer_id or location_id changed, or its new
            SELECT MAX(T.TERMINAL_NBR)
              INTO l_terminal_nbr
              FROM TERMINAL_EPORT TE, TERMINAL T
             WHERE TE.TERMINAL_ID = T.TERMINAL_ID
               AND TE.EPORT_ID = l_eport_id;
            IF l_terminal_nbr IS NULL THEN
                l_terminal_nbr := l_serial_num;
            ELSIF l_terminal_nbr = l_serial_num THEN
                l_terminal_nbr := l_serial_num || '-1';
            ELSIF l_terminal_nbr LIKE l_serial_num || '-%' THEN
                l_terminal_nbr := l_serial_num || '-' || TO_CHAR(TO_NUMBER(SUBSTR(l_terminal_nbr, INSTR(l_terminal_nbr, '-', -1) + 1)) + 1);
            ELSE
                l_terminal_nbr := l_serial_num;
            END IF;
            SELECT TERMINAL_SEQ.NEXTVAL INTO l_terminal_id FROM DUAL;
            INSERT INTO TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, EPORT_ID, CUSTOMER_ID, LOCATION_ID, BUSINESS_UNIT_ID, PAYMENT_SCHEDULE_ID)
                VALUES(l_terminal_id, l_terminal_nbr, l_serial_num, l_eport_id, l_customer_id, l_location_id, 2, 4);
            TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
        ELSE
            -- ensure terminal_eport start date is early enough
            BEGIN
                SELECT TERMINAL_EPORT_ID, START_DATE
                  INTO l_te_id, l_old_start_date
                  FROM (SELECT TERMINAL_EPORT_ID, START_DATE
                          FROM TERMINAL_EPORT
                         WHERE TERMINAL_ID = l_terminal_id
                           AND EPORT_ID = l_eport_id
                           AND NVL(END_DATE, MAX_DATE) > l_effective_date
                           ORDER BY START_DATE ASC)
                 WHERE ROWNUM = 1;
                IF l_old_start_date > l_effective_date THEN -- we need to adjust
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, l_te_id);
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- Need to add new entry
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    END;
END; 
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/TRBI_EPORT.trg?rev=1.3
CREATE OR REPLACE TRIGGER REPORT.TRBI_EPORT
BEFORE INSERT ON REPORT.EPORT
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	LD_SYSDATE DATE := SYSDATE;
	LD_MAX_CURRENT_LOCAL_DATE DATE := CAST(SYS_EXTRACT_UTC(SYSTIMESTAMP) AS DATE) + 14/24;
BEGIN
    IF :NEW.EPORT_ID IS NULL THEN
        SELECT EPORT_SEQ.NEXTVAL
          INTO :NEW.EPORT_ID
          FROM DUAL;
    END IF;
    SELECT SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
	IF :NEW.LASTDIALIN_DATE > LD_SYSDATE THEN
		:NEW.LASTDIALIN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.FIRST_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.FIRST_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.FIRST_CREDIT_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.FIRST_CREDIT_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.FIRST_CASH_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.FIRST_CASH_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_CREDIT_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_CREDIT_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_CASH_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_CASH_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_FILL_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_FILL_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_DEX_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_DEX_DATE := LD_SYSDATE;
	END IF;
	:NEW.LASTDIALIN_DATE := LEAST(LD_SYSDATE, NULLIF(GREATEST(NVL(:NEW.LASTDIALIN_DATE, MIN_DATE), NVL(:NEW.LAST_TRAN_DATE, MIN_DATE), 
		NVL(:NEW.LAST_FILL_DATE, MIN_DATE), NVL(:NEW.LAST_DEX_DATE, MIN_DATE)), MIN_DATE));
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/TRBU_EPORT.trg?rev=1.3
CREATE OR REPLACE TRIGGER REPORT.TRBU_EPORT
BEFORE UPDATE ON REPORT.EPORT
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	LD_SYSDATE DATE := SYSDATE;
	LD_MAX_CURRENT_LOCAL_DATE DATE := CAST(SYS_EXTRACT_UTC(SYSTIMESTAMP) AS DATE) + 14/24;
BEGIN
    SELECT :OLD.CREATED_TS,
           :OLD.CREATED_BY,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
	IF :NEW.LASTDIALIN_DATE > LD_SYSDATE THEN
		:NEW.LASTDIALIN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.FIRST_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.FIRST_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.FIRST_CREDIT_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.FIRST_CREDIT_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.FIRST_CASH_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.FIRST_CASH_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_CREDIT_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_CREDIT_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_CASH_TRAN_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_CASH_TRAN_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_FILL_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_FILL_DATE := LD_SYSDATE;
	END IF;
	IF :NEW.LAST_DEX_DATE > LD_MAX_CURRENT_LOCAL_DATE THEN
		:NEW.LAST_DEX_DATE := LD_SYSDATE;
	END IF;
	:NEW.LASTDIALIN_DATE := LEAST(LD_SYSDATE, NULLIF(GREATEST(NVL(:NEW.LASTDIALIN_DATE, MIN_DATE), NVL(:NEW.LAST_TRAN_DATE, MIN_DATE), 
		NVL(:NEW.LAST_FILL_DATE, MIN_DATE), NVL(:NEW.LAST_DEX_DATE, MIN_DATE)), MIN_DATE));
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/USAT_SETTLT_UPDATE.prc?rev=HEAD
CREATE OR REPLACE procedure REPORT.USAT_SETTLT_UPDATE as 
--RFC000136 - Add Settlement Update Clean-up Script 
CURSOR l_cur IS
SELECT x.TRAN_ID, x.SETTLE_STATE_ID, x.SETTLE_DATE, x.CC_APPR_CODE
FROM CORP.LEDGER L
JOIN REPORT.TRANS X ON L.TRANS_ID = X.TRAN_ID
WHERE L.ENTRY_TYPE = 'CC'
AND L.DELETED = 'N'
AND L.PAID_DATE IS NULL
AND L.SETTLE_STATE_ID IN (1,4)
AND X.SETTLE_STATE_ID NOT IN(1,4)
AND X.SERVER_DATE < SYSDATE - 1;
BEGIN
FOR l_rec IN l_cur LOOP
REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(l_rec.TRAN_ID, l_rec.SETTLE_STATE_ID, l_rec.SETTLE_DATE, l_rec.CC_APPR_CODE);
COMMIT;
END LOOP;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/USAT_FILL_BATCH_COMPLETE.prc?rev=HEAD
CREATE OR REPLACE procedure REPORT.USAT_FILL_BATCH_COMPLETE as 
--RFC000081 - Recheck Fill is Complete 
CURSOR l_cur IS
    SELECT BATCH_ID
    FROM CORP.BATCH
    WHERE BATCH_STATE_CD = 'O'
    AND END_DATE < SYSDATE - 0.25
    AND PAYMENT_SCHEDULE_ID = 2;
BEGIN
    FOR l_rec IN l_cur LOOP
        CORP.PAYMENTS_PKG.CHECK_FILL_BATCH_COMPLETE(l_rec.BATCH_ID);
        COMMIT;
    END LOOP;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/USAT_BV_TERMINALS.prc?rev=HEAD
CREATE OR REPLACE procedure REPORT.USAT_BV_TERMINALS as 
-- RFC000423 - Schedule Best Vendors Terminals Automated Refresh
begin
DELETE FROM REPORT.USER_TERMINAL
  WHERE USER_ID IN(
    SELECT U.USER_ID
      FROM REPORT.USER_LOGIN U
     WHERE U.USER_NAME = 'bestvendorsgroup');

INSERT INTO REPORT.USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
SELECT DISTINCT U.USER_ID, T.TERMINAL_ID, 'N'
  FROM REPORT.CUSTOM_FIELD_PARAM cfp
  JOIN REPORT.TERMINAL T ON cfp.CUSTOMER_ID = T.CUSTOMER_ID
 CROSS JOIN REPORT.USER_LOGIN U
 WHERE U.USER_NAME = 'bestvendorsgroup'
   AND (TRIM(T.TERM_VAR_1) IS NOT NULL
       OR TRIM(T.TERM_VAR_2) IS NOT NULL
       OR TRIM(T.TERM_VAR_3) IS NOT NULL)
   AND ((cfp.FIELD_NAME = 'TERM_VAR_1' AND cfp.FIELD_LABEL = 'BV Operator AB Number')
       OR (cfp.FIELD_NAME = 'TERM_VAR_2' AND cfp.FIELD_LABEL = 'BV Site AB Number')
       OR (cfp.FIELD_NAME = 'TERM_VAR_3' AND cfp.FIELD_LABEL = 'BV Machine Number'));
COMMIT;
EXCEPTION
  WHEN OTHERS THEN
  RAISE;
  dbms_output.put_line(SQLERRM||' '||SQLCODE);
end;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/USAT_MEI_TERMINALS.prc?rev=HEAD
CREATE OR REPLACE PROCEDURE REPORT.USAT_MEI_TERMINALS AS
	LN_USER_ID REPORT.USER_LOGIN.USER_ID%TYPE;
BEGIN
	SELECT MAX(USER_ID)
	INTO LN_USER_ID
	FROM REPORT.USER_LOGIN
	WHERE USER_NAME = 'MEI-Master';

	DELETE FROM REPORT.USER_TERMINAL
	WHERE USER_ID = LN_USER_ID
		AND TERMINAL_ID NOT IN (
			SELECT T.TERMINAL_ID
			FROM REPORT.EPORT E
			JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
			JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
			WHERE E.DEVICE_TYPE_ID = 6
				AND E.EPORT_SERIAL_NUM LIKE 'M1%'
		);
	
	INSERT INTO REPORT.USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
	SELECT DISTINCT LN_USER_ID, T.TERMINAL_ID, 'N'
	FROM REPORT.EPORT E
	JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
	JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
	WHERE E.DEVICE_TYPE_ID = 6
		AND E.EPORT_SERIAL_NUM LIKE 'M1%'
		AND NOT EXISTS (
			SELECT 1
			FROM REPORT.USER_TERMINAL
			WHERE USER_ID = LN_USER_ID
				AND TERMINAL_ID = T.TERMINAL_ID
		);
	
	COMMIT;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/USAT_MISSING_TRANIMPORT.prc?rev=HEAD
CREATE OR REPLACE procedure REPORT.USAT_MISSING_TRANIMPORT as 
-- Missing transactions import

    l_cnt PLS_INTEGER;
    CURSOR l_day_cur IS
       SELECT a.SOURCE_SYSTEM_CD, a.DEVICE_TYPE_ID, a.TRAN_TYPE, a.UPLOAD_DAY, NVL(a.TRAN_COUNT, 0) - NVL(b.TRAN_COUNT, 0) MISSING_COUNT
            FROM RECON.TRAN_COUNT_AUDIT a
            JOIN RECON.TRAN_COUNT_AUDIT b 
            ON a.SOURCE_SYSTEM_CD = b.SOURCE_SYSTEM_CD
            AND a.DEVICE_TYPE_ID = b.DEVICE_TYPE_ID
            AND a.TRAN_TYPE = b.TRAN_TYPE
            AND a.UPLOAD_DAY = b.UPLOAD_DAY
            LEFT OUTER JOIN 
            (SELECT NULL SOURCE_SYSTEM_CD, 0 DEVICE_TYPE_ID, '' TRAN_TYPE, NULL UPLOAD_DAY, 0 BAD_TRAN_COUNT, 0 BAD_TRAN_AMOUNT FROM DUAL WHERE 1=0
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('01/21/2008', 'MM/DD/YYYY'), 76, 95 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('01/29/2008', 'MM/DD/YYYY'), 8, 12 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('02/11/2008', 'MM/DD/YYYY'), 1, .75 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('02/13/2008', 'MM/DD/YYYY'), 8, 8 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Credit', TO_DATE('02/13/2008', 'MM/DD/YYYY'), 1, 1 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('02/19/2008', 'MM/DD/YYYY'), 102, 3143.9 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Credit', TO_DATE('02/19/2008', 'MM/DD/YYYY'), 22, 726.9 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('03/04/2008', 'MM/DD/YYYY'), 2, 2.15 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('03/07/2008', 'MM/DD/YYYY'), 12, 11.5 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('04/28/2008', 'MM/DD/YYYY'), 3, 2.75 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Cash', TO_DATE('05/19/2008', 'MM/DD/YYYY'), 43, 44.45 FROM DUAL
            UNION ALL SELECT 'PSS', 1, 'Credit', TO_DATE('05/19/2008', 'MM/DD/YYYY'), 4, 6.5 FROM DUAL
            UNION ALL SELECT 'PSS', 0, 'Cash', TO_DATE('05/30/2008', 'MM/DD/YYYY'), 84, 290.5 FROM DUAL
            ) c 
            ON a.SOURCE_SYSTEM_CD = c.SOURCE_SYSTEM_CD
            AND a.UPLOAD_DAY = c.UPLOAD_DAY 
            AND a.DEVICE_TYPE_ID = c.DEVICE_TYPE_ID
            AND a.TRAN_TYPE = c.TRAN_TYPE
            WHERE a.SYSTEM_NAME = 'USADBP' AND b.SYSTEM_NAME = 'USARDB'
            AND (a.TRAN_COUNT <> b.TRAN_COUNT
            OR a.TRAN_AMOUNT <> b.TRAN_AMOUNT)
            AND (a.DEVICE_TYPE_ID NOT IN(5) OR a.TRAN_TYPE = 'Credit')
            /*custom check for bad tran start dates*/
            AND (c.UPLOAD_DAY IS NULL OR a.TRAN_COUNT <> b.TRAN_COUNT + c.BAD_TRAN_COUNT OR a.TRAN_AMOUNT <> b.TRAN_AMOUNT + c.BAD_TRAN_AMOUNT)
			--AND a.SOURCE_SYSTEM_CD IN('PSS')
            --AND a.TRAN_TYPE IN('Credit')
            AND a.UPLOAD_DAY > SYSDATE -4 
            ;
    l_day_update_cnt PLS_INTEGER;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Discrepancy re-processing beginning for ');
    FOR l_day_rec IN l_day_cur LOOP
        DBMS_OUTPUT.PUT_LINE('Missing ' || l_day_rec.MISSING_COUNT || ' trans for ' || l_day_rec.UPLOAD_DAY);
        l_day_update_cnt := 0;
        IF l_day_rec.MISSING_COUNT < 0 THEN -- more trans in rdb then dbp
            DECLARE
                CURSOR l_hour_cur IS
                    SELECT b.UPLOAD_HOUR 
                      FROM (
                        SELECT 
                            TRUNC(TRAN_UPLOAD_TS, 'HH') UPLOAD_HOUR,
                            COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                          FROM PSS.VW_TRAN_COUNT_BY_DAY@USADBP
                         WHERE TRAN_UPLOAD_TS >= l_day_rec.UPLOAD_DAY
                           AND TRAN_UPLOAD_TS < l_day_rec.UPLOAD_DAY + 1
                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                           AND SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(TRAN_UPLOAD_TS, 'HH')) a
                           RIGHT OUTER JOIN (
                         SELECT 
                            TRUNC(x.SERVER_DATE, 'HH') UPLOAD_HOUR,
                            COUNT(*) TRAN_COUNT,
                            SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                          FROM REPORT.TRANS x, REPORT.EPORT d
                         WHERE x.EPORT_ID = d.EPORT_ID
                           AND x.SERVER_DATE >= l_day_rec.UPLOAD_DAY
                           AND x.SERVER_DATE < l_day_rec.UPLOAD_DAY + 1
                           AND x.SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND DECODE(x.TRANS_TYPE_ID, 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(x.SERVER_DATE, 'HH')) b
                        ON a.UPLOAD_HOUR = b.UPLOAD_HOUR
                     WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                            OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
            BEGIN
            FOR l_hour_rec IN l_hour_cur LOOP
                DECLARE
                    CURSOR l_min_cur IS
                        SELECT b.UPLOAD_MINUTE
                          FROM (
                            SELECT 
                                TRUNC(TRAN_UPLOAD_TS, 'MI') UPLOAD_MINUTE,
                                COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                                SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                              FROM PSS.VW_TRAN_COUNT_BY_DAY@USADBP
                             WHERE TRAN_UPLOAD_TS >= l_hour_rec.UPLOAD_HOUR
                               AND TRAN_UPLOAD_TS < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                               AND SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                               AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(TRAN_UPLOAD_TS, 'MI')) a
                               RIGHT OUTER JOIN  (
                             SELECT 
                                TRUNC(x.SERVER_DATE, 'MI') UPLOAD_MINUTE,
                                COUNT(*) TRAN_COUNT,
                                SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                              FROM REPORT.TRANS x, REPORT.EPORT d
                             WHERE x.EPORT_ID = d.EPORT_ID
                               AND x.SERVER_DATE >= l_hour_rec.UPLOAD_HOUR
                               AND x.SERVER_DATE < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND x.SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                               AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND DECODE(x.TRANS_TYPE_ID, 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(x.SERVER_DATE, 'MI')) b
                            ON a.UPLOAD_MINUTE = b.UPLOAD_MINUTE
                         WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                                OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
                BEGIN
                     FOR l_min_rec IN l_min_cur LOOP
                         DECLARE
                                CURSOR l_tran_cur IS
                                    SELECT 
                                        a.TRAN_ID,
                                        b.MACHINE_TRANS_NO TRAN_GLOBAL_TRANS_CD, 
                                        a.TRAN_AMOUNT TRAN_AMOUNT_A,
                                        b.TOTAL_AMOUNT TRAN_AMOUNT_B
                                      FROM (
                                        SELECT 
                                            TRAN_ID,
                                            TRAN_GLOBAL_TRANS_CD,
                                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                                          FROM PSS.VW_TRAN_COUNT_BY_DAY@USADBP
                                         WHERE TRAN_UPLOAD_TS >= l_min_rec.UPLOAD_MINUTE
                                           AND TRAN_UPLOAD_TS < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                                           AND SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                                           GROUP BY TRAN_ID, TRAN_GLOBAL_TRANS_CD) a
                                         RIGHT OUTER JOIN (
                                         SELECT 
                                            x.MACHINE_TRANS_NO,
                                            x.TOTAL_AMOUNT
                                          FROM REPORT.TRANS x, REPORT.EPORT d
                                         WHERE x.EPORT_ID = d.EPORT_ID
                                           AND x.SERVER_DATE >= l_min_rec.UPLOAD_MINUTE
                                           AND x.SERVER_DATE < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND x.SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND DECODE(x.TRANS_TYPE_ID, 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                                           ) b
                                        ON a.TRAN_GLOBAL_TRANS_CD = b.MACHINE_TRANS_NO
                                     WHERE (a.TRAN_AMOUNT <> b.TOTAL_AMOUNT OR b.MACHINE_TRANS_NO IS NULL);
                        BEGIN
                            /*DBMS_OUTPUT.PUT_LINE('Found Mis-Match at ' || to_char(l_min_rec.UPLOAD_MINUTE, 'MM/DD/YYYY HH24:MI')
                                 || ' (' || l_day_rec.TRAN_TYPE || ',' || l_day_rec.SOURCE_SYSTEM_CD
                                 || ',' || l_day_rec.DEVICE_TYPE_ID || ')');*/
                            FOR l_tran_rec IN l_tran_cur LOOP
                                DBMS_OUTPUT.PUT_LINE('Found Mis-Match for ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD
                                 || ' (' || to_char(l_tran_rec.TRAN_AMOUNT_A) || ' vs ' || to_char(l_tran_rec.TRAN_AMOUNT_B)
                                 ||  ')');
                                IF l_tran_rec.tran_id IS NULL THEN
                                    DBMS_OUTPUT.PUT_LINE('Transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ' is NOT in the transaction processing system!');
                                ELSE
                                    SELECT COUNT(*)
                                      INTO l_cnt
                                      FROM REPORT.TRANS x
                                     WHERE x.MACHINE_TRANS_NO = l_tran_rec.TRAN_GLOBAL_TRANS_CD;
                                    IF l_cnt <> 1 THEN
                                       DBMS_OUTPUT.PUT_LINE('Pulling transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD);
                                       BEGIN
                                         UPDATER.PULL_DATA_PKG.pull_pss_tran_by_id(l_tran_rec.tran_id);
                                         COMMIT;
                                         l_day_update_cnt := l_day_update_cnt + 1;
                                       EXCEPTION
                                         WHEN OTHERS THEN
                                            DBMS_OUTPUT.PUT_LINE('ERROR: Could not pull transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ': ' || SQLERRM);
                                       END;
                                    ELSE
                                       DBMS_OUTPUT.PUT_LINE('Transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ' is already in reporting system');		   
                                    END IF;
                                END IF;
                            END LOOP;    
                        END;
                    END LOOP;
                END;
             END LOOP;
            END;
        ELSE
            DECLARE
                CURSOR l_hour_cur IS
                    SELECT a.UPLOAD_HOUR 
                      FROM (
                        SELECT 
                            TRUNC(TRAN_UPLOAD_TS, 'HH') UPLOAD_HOUR,
                            COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                          FROM PSS.VW_TRAN_COUNT_BY_DAY@USADBP
                         WHERE TRAN_UPLOAD_TS >= l_day_rec.UPLOAD_DAY
                           AND TRAN_UPLOAD_TS < l_day_rec.UPLOAD_DAY + 1
                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                           AND SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(TRAN_UPLOAD_TS, 'HH')) a
                           LEFT OUTER JOIN (
                         SELECT 
                            TRUNC(x.SERVER_DATE, 'HH') UPLOAD_HOUR,
                            COUNT(*) TRAN_COUNT,
                            SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                          FROM REPORT.TRANS x, REPORT.EPORT d
                         WHERE x.EPORT_ID = d.EPORT_ID
                           AND x.SERVER_DATE >= l_day_rec.UPLOAD_DAY
                           AND x.SERVER_DATE < l_day_rec.UPLOAD_DAY + 1
                           AND x.SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                           AND DECODE(x.TRANS_TYPE_ID, 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                           GROUP BY TRUNC(x.SERVER_DATE, 'HH')) b
                        ON a.UPLOAD_HOUR = b.UPLOAD_HOUR
                     WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                            OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
            BEGIN
            FOR l_hour_rec IN l_hour_cur LOOP
                DECLARE
                    CURSOR l_min_cur IS
                        SELECT a.UPLOAD_MINUTE
                          FROM (
                            SELECT 
                                TRUNC(TRAN_UPLOAD_TS, 'MI') UPLOAD_MINUTE,
                                COUNT(DISTINCT TRAN_ID) TRAN_COUNT,
                                SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                              FROM PSS.VW_TRAN_COUNT_BY_DAY@USADBP
                             WHERE TRAN_UPLOAD_TS >= l_hour_rec.UPLOAD_HOUR
                               AND TRAN_UPLOAD_TS < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                               AND SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                               AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(TRAN_UPLOAD_TS, 'MI')) a
                               LEFT OUTER JOIN  (
                             SELECT 
                                TRUNC(x.SERVER_DATE, 'MI') UPLOAD_MINUTE,
                                COUNT(*) TRAN_COUNT,
                                SUM(x.TOTAL_AMOUNT) TRAN_AMOUNT
                              FROM REPORT.TRANS x, REPORT.EPORT d
                             WHERE x.EPORT_ID = d.EPORT_ID
                               AND x.SERVER_DATE >= l_hour_rec.UPLOAD_HOUR
                               AND x.SERVER_DATE < l_hour_rec.UPLOAD_HOUR + (1/24)
                               AND x.SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                               AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                               AND DECODE(x.TRANS_TYPE_ID, 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                               GROUP BY TRUNC(x.SERVER_DATE, 'MI')) b
                            ON a.UPLOAD_MINUTE = b.UPLOAD_MINUTE
                         WHERE (a.TRAN_COUNT <> NVL(b.TRAN_COUNT, 0)
                                OR a.TRAN_AMOUNT <> NVL(b.TRAN_AMOUNT, 0));  
                BEGIN
                     FOR l_min_rec IN l_min_cur LOOP
                         DECLARE
                                CURSOR l_tran_cur IS
                                    SELECT 
                                        a.TRAN_ID,
                                        a.TRAN_GLOBAL_TRANS_CD, 
                                        a.TRAN_AMOUNT TRAN_AMOUNT_A,
                                        b.TOTAL_AMOUNT TRAN_AMOUNT_B
                                      FROM (
                                        SELECT 
                                            TRAN_ID,
                                            TRAN_GLOBAL_TRANS_CD,
                                            SUM(TRAN_LINE_ITEM_AMOUNT * TRAN_LINE_ITEM_QUANTITY) TRAN_AMOUNT
                                          FROM PSS.VW_TRAN_COUNT_BY_DAY@USADBP
                                         WHERE TRAN_UPLOAD_TS >= l_min_rec.UPLOAD_MINUTE
                                           AND TRAN_UPLOAD_TS < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND UPLOAD_DAY = l_day_rec.UPLOAD_DAY
                                           AND SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                                           AND DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND TRAN_TYPE = l_day_rec.TRAN_TYPE
                                           GROUP BY TRAN_ID, TRAN_GLOBAL_TRANS_CD) a
                                         LEFT OUTER JOIN (
                                         SELECT 
                                            x.MACHINE_TRANS_NO,
                                            x.TOTAL_AMOUNT
                                          FROM REPORT.TRANS x, REPORT.EPORT d
                                         WHERE x.EPORT_ID = d.EPORT_ID
                                           AND x.SERVER_DATE >= l_min_rec.UPLOAD_MINUTE
                                           AND x.SERVER_DATE < l_min_rec.UPLOAD_MINUTE + (1/(24*60))
                                           AND x.SOURCE_SYSTEM_CD = l_day_rec.SOURCE_SYSTEM_CD
                                           AND d.DEVICE_TYPE_ID = l_day_rec.DEVICE_TYPE_ID
                                           AND DECODE(x.TRANS_TYPE_ID, 16, 'Credit', 17, 'Special', 18, 'Special', 19, 'Credit', 22, 'Cash') = l_day_rec.TRAN_TYPE
                                           ) b
                                        ON a.TRAN_GLOBAL_TRANS_CD = b.MACHINE_TRANS_NO
                                     WHERE (a.TRAN_AMOUNT <> b.TOTAL_AMOUNT OR b.MACHINE_TRANS_NO IS NULL);
                        BEGIN
                            /*DBMS_OUTPUT.PUT_LINE('Found Mis-Match at ' || to_char(l_min_rec.UPLOAD_MINUTE, 'MM/DD/YYYY HH24:MI')
                                 || ' (' || l_day_rec.TRAN_TYPE || ',' || l_day_rec.SOURCE_SYSTEM_CD
                                 || ',' || l_day_rec.DEVICE_TYPE_ID || ')');*/
                            FOR l_tran_rec IN l_tran_cur LOOP
                                DBMS_OUTPUT.PUT_LINE('Found Mis-Match for ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD
                                 || ' (' || to_char(l_tran_rec.TRAN_AMOUNT_A) || ' vs ' || to_char(l_tran_rec.TRAN_AMOUNT_B)
                                 ||  ')');
                                 
                                SELECT COUNT(*)
                                  INTO l_cnt
                                  FROM REPORT.TRANS x
                                 WHERE x.MACHINE_TRANS_NO = l_tran_rec.TRAN_GLOBAL_TRANS_CD;
                                IF l_cnt <> 1 THEN
                                   DBMS_OUTPUT.PUT_LINE('Pulling transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD);
                                   BEGIN
                                     UPDATER.PULL_DATA_PKG.pull_pss_tran_by_id(l_tran_rec.tran_id);
                                     COMMIT;
                                     l_day_update_cnt := l_day_update_cnt + 1;
                                   EXCEPTION
                                     WHEN OTHERS THEN
                                        DBMS_OUTPUT.PUT_LINE('ERROR: Could not pull transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ': ' || SQLERRM);
                                   END;
                                ELSE
                                   DBMS_OUTPUT.PUT_LINE('Transaction ' || l_tran_rec.TRAN_GLOBAL_TRANS_CD || ' is already in reporting system');		   
                                END IF; 
                            END LOOP;    
                        END;
                    END LOOP;
                END;
             END LOOP;
            END;
        END IF;
        IF l_day_update_cnt > 0 THEN
            RECON.AUDIT_PKG.CALC_TRAN_COUNTS(l_day_rec.UPLOAD_DAY);
        END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Discrepancy re-processing complete');
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/REFUND_PKG.pbk?rev=1.13
CREATE OR REPLACE PACKAGE BODY CORP.REFUND_PKG IS
-- Purpose: Briefly explain the functionality of the package body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- Noah S.     6/15/05 Creation

   /*
   Function REFUND_PKG.issue_refund

   Creates entries in REPORT.Refund and REPORT.Trans to represent
   the issued refund.

   Return Values:
         0: Success (Normal)
         1: Successful Manager Override
        -1: Failure (General)
        -2: Failed Manager Override
   */
   FUNCTION issue_refund(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_reason_id CORP.REFUND_REASON.REFUND_REASON_ID%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE
   ) RETURN INT IS
        l_refund_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_refund_id CORP.REFUND.REFUND_ID%TYPE;
        l_refund_desc REPORT.TRANS.DESCRIPTION%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_return_val INT;
        l_num PLS_INTEGER;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        LOCK TABLE CORP.REFUND IN EXCLUSIVE MODE;
        BEGIN
            SELECT NVL(SUM(TOTAL_AMOUNT), 0), NVL(SUM(DECODE(TRANS_TYPE_ID, 20, 1, 0)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS
             WHERE ORIG_TRAN_ID = l_trans_id
               AND TRANS_TYPE_ID IN (20,21);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_REFUND_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':R'
                || TO_CHAR(l_num, 'FM9999999990'),
            rr.REFUND_REASON,
            x.TOTAL_AMOUNT
          INTO
            l_refund_trans_id,
            l_refund_id,
            l_machine_trans_no,
            l_refund_desc,
            l_trans_total
          FROM REPORT.TRANS x, CORP.REFUND_REASON rr
         WHERE x.TRAN_ID = l_trans_id
           AND rr.refund_reason_id = l_reason_id;

        IF (((ABS(l_prev_total) + ABS(l_amount)) <= l_trans_total) OR (l_override = 'Y')) THEN
            DECLARE
                l_date DATE := SYSDATE;
            BEGIN
            INSERT INTO REPORT.TRANS(tran_id, card_number, total_amount, cc_appr_code,
                start_date, close_date, server_date, settle_state_id, eport_id,
                trans_type_id, orig_tran_id, terminal_id, merchant_id,
                source_system_cd, machine_trans_no, customer_bank_id,
                process_fee_id, create_date, description, currency_id)
                (SELECT
                    l_refund_trans_id, orig.card_number, -ABS(l_amount), 'PENDING',
                    l_date, l_date, l_date, 1, orig.eport_id, 20, l_trans_id,
                    orig.terminal_id, orig.merchant_id, 'RA', l_machine_trans_no,
                    orig.customer_bank_id, orig.process_fee_id, l_date,
                    l_refund_desc, orig.currency_id
                FROM REPORT.TRANS orig WHERE orig.tran_id = l_trans_id);

            INSERT INTO REPORT.TRANS_C(tran_id, hashed_card_number, masked_card_number, close_date)
                (SELECT
                    l_refund_trans_id, orig.hashed_card_number, orig.masked_card_number, l_date
                FROM REPORT.TRANS_C orig WHERE orig.tran_id = l_trans_id);

            INSERT INTO CORP.REFUND(refund_id, creator_user_id, tran_id, reason_id,
                comment_text, processed_flag, manager_override_flag, close_date) VALUES
                (l_refund_id, l_user_id, l_refund_trans_id, l_reason_id, l_comment,
                'N', l_override, l_date);
            END;
            l_return_val := 0; --'Normal Success'
        END IF;

        IF ((ABS(l_prev_total) + ABS(l_amount)) > l_trans_total) THEN
            IF (l_override = 'Y') THEN l_return_val := 1; --'Override Success'
            ELSE l_return_val := -2; --'Override Failure'
            END IF;
        END IF;
        RETURN l_return_val;
   END;

   /*
   Function REFUND_PKG.issue_chargeback

   Creates entries in CORP.Chargeback and REPORT.Trans to represent
   the issued chargeback.

   Return Values:
         0: Success
        -1: Failure
   */
   FUNCTION issue_chargeback(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_fee REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE
   ) RETURN INT IS
        l_chargeback_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_chargeback_id CORP.CHARGEBACK.CHARGEBACK_ID%TYPE;
        l_return_val INT;
        l_card_assoc_id CORP.CARD_ASSOC.CARD_ASSOC_ID%TYPE;
        l_card_assoc_name CORP.CARD_ASSOC.CARD_ASSOC_NAME%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_total_chargeback_amt REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_num PLS_INTEGER;
        l_date DATE := SYSDATE;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        l_total_chargeback_amt := l_amount + NVL(l_fee,0);
        LOCK TABLE CORP.CHARGEBACK IN EXCLUSIVE MODE;

        BEGIN
            SELECT NVL(SUM(TOTAL_AMOUNT), 0), NVL(SUM(DECODE(TRANS_TYPE_ID, 21, 1, 0)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS
             WHERE ORIG_TRAN_ID = l_trans_id
               AND TRANS_TYPE_ID IN (20,21);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_CHARGEBACK_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':C'
                || TO_CHAR(l_num, 'FM9999999990'),
            x.TOTAL_AMOUNT
          INTO
            l_chargeback_trans_id,
            l_chargeback_id,
            l_machine_trans_no,
            l_trans_total
          FROM REPORT.TRANS x
         WHERE x.TRAN_ID = l_trans_id;

        IF (((ABS(l_prev_total) + ABS(l_total_chargeback_amt)) <= l_trans_total) OR (l_override = 'Y')) THEN
        	INSERT INTO REPORT.TRANS(
            	tran_id, 
            	card_number, 
            	total_amount, 
            	cc_appr_code,
                start_date, 
                close_date, 
                server_date, 
                settle_state_id, 
                eport_id,
                trans_type_id, 
                orig_tran_id, 
                terminal_id, 
                merchant_id,
                source_system_cd, 
                machine_trans_no, 
                customer_bank_id,
                process_fee_id, 
                create_date, 
                settle_date, 
                description, 
                currency_id)
			SELECT
            	l_chargeback_trans_id, 
            	orig.CARD_NUMBER, 
            	-ABS(l_total_chargeback_amt), 
            	'SETTLED',
                l_date, 
                l_date, 
                l_date, 
                3, 
                orig.EPORT_ID, 
                21, 
                l_trans_id,
                orig.TERMINAL_ID, 
                orig.MERCHANT_ID, 
                'RA', 
                l_machine_trans_no,
                orig.CUSTOMER_BANK_ID, 
                orig.PROCESS_FEE_ID, 
                l_date, 
                l_date,
				REPORT.CARD_NAME(orig.CARDTYPE_AUTHORITY_ID, orig.TRANS_TYPE_ID, orig.CARD_NUMBER) || ' issued chargeback ' || 
                	orig.CC_APPR_CODE || ' ' || TO_CHAR(orig.start_date, 'MM/DD/yyyy') || 
                	' Tran #: ' || orig.TRAN_ID || ' Amt: ' || curr.CURRENCY_SYMBOL || 
                	TO_CHAR(l_amount,'FM9,999,990.00') || ' ' || curr.CURRENCY_CODE || ' Fee: ' 
                	|| curr.CURRENCY_SYMBOL || TO_CHAR(NVL(l_fee,0),'FM9,999,990.00') 
                	|| ' ' || curr.CURRENCY_CODE,
                orig.CURRENCY_ID
			FROM REPORT.TRANS orig 
                INNER JOIN CORP.CURRENCY curr ON orig.CURRENCY_ID = curr.CURRENCY_ID
                WHERE orig.TRAN_ID = l_trans_id;

            INSERT INTO REPORT.TRANS_C(tran_id, hashed_card_number, masked_card_number, close_date)
                (SELECT l_chargeback_trans_id, orig.hashed_card_number, orig.masked_card_number, l_date
                FROM REPORT.TRANS_C orig WHERE orig.tran_id = l_trans_id);

            INSERT INTO CORP.CHARGEBACK(
            	chargeback_id, 
            	creator_user_id, 
            	tran_id, 
            	card_assoc_id, 
            	comment_text, 
            	manager_override_flag)
            SELECT
           		l_chargeback_id, 
           		l_user_id, 
           		l_chargeback_trans_id, 
           		(SELECT CA.CARD_ASSOC_ID 
				   FROM CORP.CARD_ASSOC CA, REPORT.TRANS X
				  WHERE CA.CARD_ASSOC_NAME = REPORT.CARD_COMPANY(x.CARD_NUMBER)
				    AND x.TRAN_ID = l_trans_id), 
           		l_comment, 
           		l_override
           	FROM DUAL;
            l_return_val := 0; --'Normal Success'
        END IF;
        
        IF ((ABS(l_prev_total) + ABS(l_total_chargeback_amt)) > l_trans_total) THEN
            IF (l_override = 'Y') THEN l_return_val := 1; --'Override Success'
            ELSE l_return_val := -2; --'Override Failure'
            END IF;
        END IF;

        RETURN l_return_val;
   END;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/TRBI_DEX_FILE.trg?rev=HEAD
CREATE OR REPLACE TRIGGER G4OP.TRBI_DEX_FILE
BEFORE INSERT ON G4OP.DEX_FILE
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF :NEW.DEX_FILE_ID IS NULL THEN
		SELECT DEX_FILE_SEQ.NEXTVAL
		INTO :NEW.DEX_FILE_ID
		FROM DUAL;
    END IF;
	SELECT 
		SYSDATE,
		SYSDATE
	INTO 
		:NEW.CREATE_DT,
		:NEW.UPD_DT
	FROM DUAL;
	UPDATE REPORT.EPORT
	SET LAST_DEX_DATE = GREATEST(:NEW.DEX_DATE, NVL(LAST_DEX_DATE, MIN_DATE))
	WHERE EPORT_ID = :NEW.EPORT_ID;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/edge_implementation/R26.USARDB.2.sql?rev=HEAD
GRANT EXECUTE ON RDW_LOADER.PKG_LOAD_DATA TO USAT_APP_LAYER_ROLE;
DROP FUNCTION CORP.CARD_COMPANY;

UPDATE REPORT.EPORT
SET LASTDIALIN_DATE = LEAST(SYSDATE, NULLIF(GREATEST(NVL(LASTDIALIN_DATE, MIN_DATE), NVL(LAST_TRAN_DATE, MIN_DATE), NVL(LAST_FILL_DATE, MIN_DATE)), MIN_DATE))
WHERE LAST_TRAN_DATE > NVL(LASTDIALIN_DATE, MIN_DATE) OR LAST_FILL_DATE > NVL(LASTDIALIN_DATE, MIN_DATE);

UPDATE REPORT.EPORT
SET LAST_FILL_DATE = NULL
WHERE LAST_FILL_DATE > SYSDATE;

COMMIT;

/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_BV_TERMINALS_REFRESH.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."B.V. TERMINALS REFRESH"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
REPORT.USAT_BV_TERMINALS();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=22;BYMINUTE=0;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'RFC000423 - Schedule Best Vendors Terminals Automated Refresh',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."B.V. TERMINALS REFRESH"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."B.V. TERMINALS REFRESH"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_MEI_TERMINALS_REFRESH.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."MEI TERMINALS REFRESH"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
REPORT.USAT_MEI_TERMINALS();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=23;BYMINUTE=15;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'Schedule MEI Terminals Automated Refresh',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."MEI TERMINALS REFRESH"', attribute => 'restartable', value => TRUE);
sys.dbms_scheduler.enable( '"REPORT"."MEI TERMINALS REFRESH"' );
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_CREATE_EXPORT_BATCH.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."CREATEEXPORTBATCH"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
REPORT.DW_PKG.CLOSE_EXPORT_BATCHES();
end;',
repeat_interval => 'freq=DAILY;BYHOUR=5,6;BYMINUTE=3,23,43',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'CREATEEXPORTBATCH',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."CREATEEXPORTBATCH"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."CREATEEXPORTBATCH"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_DAILY_JOBS.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."DAILYJOBS"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
G4OP.RETRY_NONZERO_ERRORS(); 
CORP.PAYMENTS_PKG.Scan_For_Service_Fees(); 
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=5;BYMINUTE=0;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'DailyJobs',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."DAILYJOBS"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."DAILYJOBS"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_G4_DEX_DATA_LOADER.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."G4DEXDATALOADER"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
G4OP.LOAD_DEX_FILES();
end;',
repeat_interval => 'FREQ=MINUTELY;INTERVAL=35',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'G4DEXDataLoader',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."G4DEXDATALOADER"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."G4DEXDATALOADER"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_MISSING_TRANSACTIONS_IMPORT.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."MISSING TRANSACTIONS IMPORT"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
REPORT.USAT_MISSING_TRANIMPORT();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=21;BYMINUTE=0;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'Import Missing transactions from Transactions DB',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."MISSING TRANSACTIONS IMPORT"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."MISSING TRANSACTIONS IMPORT"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_RECHECK_FILL_EVENTS.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."RECHECK FILL EVENTS"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
USAT_FILL_BATCH_COMPLETE();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=7,19;BYMINUTE=0;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'RFC000081 - Recheck Fill ',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."RECHECK FILL EVENTS"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."RECHECK FILL EVENTS"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/REPORT_SETTLEMENT_UPDATE.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"REPORT"."SETTLEMENT UPDATE"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
REPORT.USAT_SETTLT_UPDATE();
end;',
repeat_interval => 'FREQ=DAILY;BYHOUR=18;BYMINUTE=0;BYSECOND=0',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'Add settlement update cleanup RFC-136',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"REPORT"."SETTLEMENT UPDATE"', attribute => 'restartable', value => TRUE); 
sys.dbms_scheduler.enable( '"REPORT"."SETTLEMENT UPDATE"' ); 
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBA_JOBS/UPDATER_PSS_SETTLE_UPDATER.job?rev=HEAD
BEGIN
sys.dbms_scheduler.create_job( 
job_name => '"UPDATER"."PSSSETTLEUPDATER"',
job_type => 'PLSQL_BLOCK',
job_action => 'begin
UPDATER.PULL_DATA_PKG.UPDATE_SETTLED_PSS_TRANS_TMP();
UPDATER.PULL_DATA_PKG.UPDATE_SETTLED_PSS_REFUNDS() ;
end;',
repeat_interval => 'FREQ=MINUTELY;INTERVAL=10',
start_date => systimestamp at time zone 'US/Eastern',
job_class => '"DEFAULT_JOB_CLASS"',
comments => 'PSSSettleUpdater',
auto_drop => FALSE,
enabled => FALSE);
sys.dbms_scheduler.set_attribute( name => '"UPDATER"."PSSSETTLEUPDATER"', attribute => 'restartable', value => TRUE);
sys.dbms_scheduler.enable( '"UPDATER"."PSSSETTLEUPDATER"' );
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/RecompileInvalidObjects.sql?rev=HEAD
DECLARE
    CURSOR l_cur IS
select 'alter '||decode(object_type,'PACKAGE BODY'
,'package '||owner||'.'||object_name||
'  compile body'
,object_type||' '||owner||'.'||object_name
||' compile') sql_text
from dba_objects 
where status = 'INVALID' and object_type NOT IN('MATERIALIZED VIEW', 'SYNONYM')
and owner in (
	'APP_EXEC_HIST',
	'APP_LAYER',
	'APP_LOG',
	'APP_USER',
	'AUTHORITY',
	'CORP',
	'DBADMIN',
	'DEVICE',
	'ENGINE',
	'FOLIO_CONF',
	'FRONT',
	'G4OP',
	'LOCATION',
	'PSS',
	'RDW_LOADER',
	'REAL_TIME',
	'RECON',
	'REPL_MGR',
	'REPORT',
	'SONY',
	'TAZDBA',
	'TRACKING',
	'UITOOL',
	'UPDATER',
	'USAQA',
	'USAT_REPORTING',
	'WEB_CONTENT'
);
BEGIN
    FOR i IN 1..2 LOOP
        FOR l_rec in l_cur LOOP
            DBMS_OUTPUT.PUT_LINE('Executing "' || l_rec.sql_text || '"');
            BEGIN
                EXECUTE IMMEDIATE l_rec.sql_text;
            EXCEPTION
                WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('Failed "' || l_rec.sql_text || '":');
                    DBMS_OUTPUT.PUT_LINE(SQLERRM);
            END;
        END LOOP;
    END LOOP;
END;
/

