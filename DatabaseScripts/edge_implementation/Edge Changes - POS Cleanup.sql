DECLARE
	l_device_id device.device_id%TYPE;
	CURSOR l_dev_cur IS SELECT device_id FROM pss.pos GROUP BY device_id HAVING COUNT(1) > 1;
	l_dev_rec l_dev_cur%ROWTYPE;
BEGIN
	FOR l_dev_rec IN l_dev_cur LOOP
		SELECT SEQ_DEVICE_ID.NEXTVAL INTO l_device_id FROM DUAL;

		INSERT INTO device.device(device_id, device_name, device_serial_cd, device_type_id, device_active_yn_flag, encryption_key, device_encr_key_gen_ts)
		SELECT l_device_id, device_name, device_serial_cd, device_type_id, 'N', encryption_key, device_encr_key_gen_ts FROM device.device WHERE device_id = l_dev_rec.device_id;

		UPDATE pss.pos SET device_id = -l_device_id
		WHERE pos_id = (SELECT MAX(pos_id) FROM pss.pos WHERE device_id = l_dev_rec.device_id);
	END LOOP;
	
	COMMIT;		
END;