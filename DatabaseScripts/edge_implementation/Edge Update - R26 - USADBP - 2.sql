WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?rev=1.64
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
    ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    ln_cnt PLS_INTEGER;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_lock_string VARCHAR2(100);
    ln_start INTEGER;
    ln_end INTEGER;
    lv_parsed_acct_data PSS.REFUND.REFUND_PARSED_ACCT_DATA%TYPE;
BEGIN
    ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
    ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
    IF ln_end <= 0 THEN
        ln_end := LENGTH(pv_global_trans_cd) + 1;
    END IF;
    lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
    ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
    ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
    -- check if refund already exists
    BEGIN
        SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
          INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
        RETURN;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_already_inserted_flag := 'N';
    END;
    -- Find original transaction
    BEGIN
        SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS
          INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
           AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
    END;
    
    INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_REPORTABLE_ACCT_NUM,
            TRAN_PARSED_ACCT_NUM_HASH,
            TRAN_PARSED_ACCT_NUM_ENCR,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME
			)
     SELECT pn_tran_id,
            pn_orig_tran_id,
            ld_refund_ts,
            ld_refund_ts,
            NULL, /* Must be NULL so that PSSUpdater will not pick it up */
            pv_global_trans_cd,
            '8',
            O.CONSUMER_ACCT_ID,
            SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
            O.POS_PTA_ID,
            O.TRAN_DEVICE_RESULT_TYPE_CD,
            O.TRAN_ACCOUNT_PIN,
            O.TRAN_RECEIVED_RAW_ACCT_DATA,
            O.TRAN_PARSED_ACCT_NAME,
            O.TRAN_PARSED_ACCT_EXP_DATE,
            O.TRAN_PARSED_ACCT_NUM,
            O.TRAN_REPORTABLE_ACCT_NUM,
            O.TRAN_PARSED_ACCT_NUM_HASH,
            O.TRAN_PARSED_ACCT_NUM_ENCR,
			pp.PAYMENT_SUBTYPE_KEY_ID,
			ps.PAYMENT_SUBTYPE_CLASS,
			ps.CLIENT_PAYMENT_TYPE_CD,
			d.DEVICE_NAME
	FROM PSS.TRAN O
	JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
	JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
	JOIN pss.pos p ON pp.pos_id = p.pos_id
	JOIN device.device d ON p.device_id = d.device_id
    WHERE O.TRAN_ID = pn_orig_tran_id;
    SELECT MAX(AUTH_PARSED_ACCT_DATA)
      INTO lv_parsed_acct_data
      FROM (SELECT AUTH_PARSED_ACCT_DATA
              FROM PSS.AUTH
             WHERE TRAN_ID = pn_orig_tran_id
               AND AUTH_PARSED_ACCT_DATA IS NOT NULL
             ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
     WHERE ROWNUM = 1;
    INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            REFUND_PARSED_ACCT_DATA,
            ACCT_ENTRY_METHOD_CD
        ) VALUES (
            pn_tran_id,
            -ABS(pn_refund_amt),
            pn_refund_desc,
            ld_refund_ts,
            pn_refund_issue_by,
            pn_refund_type_cd,
            6,
            lv_parsed_acct_data,
            pc_entry_method_cd);
END;

PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
    lv_tran_parsed_acct_num pss.tran.tran_parsed_acct_num%TYPE;
    ld_tran_server_ts DATE;
    ln_device_id device.device_id%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
BEGIN
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    pn_tran_id := 0;

    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);

    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

    BEGIN
        SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match, pos_pta_id, sale_type_cd, tran_parsed_acct_num
        INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match, ln_pos_pta_id, lc_sale_type_cd, lv_tran_parsed_acct_num
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
                t.pos_pta_id, s.sale_type_cd, t.tran_parsed_acct_num
            FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
				t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
			)
            ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;

        ln_original_tran_id := pn_tran_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR ld_tran_upload_ts IS NOT NULL
        AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR lc_sale_type_cd = pc_sale_type_cd)) THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE OR pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;

            pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;

        ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
        ELSE
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id = 2 /* Cancelled by Auth Timeout */ AND NVL(pn_sale_amount, 0) = 0 THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        END IF;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;
		
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);

        PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);

        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd,
			payment_subtype_key_id,
			payment_subtype_class,
			client_payment_type_cd,
			device_name
        ) SELECT
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            pv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = ln_pos_pta_id;

        IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
        END IF;
    ELSIF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
            IF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                ELSE
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                END IF;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                    PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                    PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                    PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                    PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                    PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                    PKG_CONST.TRAN_STATE__INCOMPLETE,
                    PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                    PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
               -- don't change it
               NULL;
            ELSE                
                pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;
        ELSIF pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
             OR (pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
                AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            
            IF lv_tran_parsed_acct_num != NULL THEN
                INSERT INTO PSS.TRAN_C(TRAN_ID, KID, HASH_TYPE_CD, TRAN_PARSED_ACCT_NUM_H)
                VALUES(pn_tran_id, -1, 'SHA1', DBADMIN.HASH_CARD(lv_tran_parsed_acct_num));
                
                UPDATE PSS.TRAN
                SET TRAN_PARSED_ACCT_NUM = NULL,
                    TRAN_PARSED_ACCT_NAME = NULL,
                    TRAN_PARSED_ACCT_EXP_DATE = NULL
                WHERE TRAN_ID = pn_tran_id;
            END IF;
        ELSIF pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                PKG_CONST.TRAN_STATE__AUTH_FAILURE,
                PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
            IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                -- normal case
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                -- insert sale record
            ELSE
                -- sale actual uploaded
                -- don't change tran_state_cd
                -- don't update sale record
                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := 'Actual uploaded before intended';
                RETURN;
            END IF;
        -- we must let POSM processed cancelled sales too to do auth reversal
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
                PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                PKG_CONST.TRAN_STATE__INTENDED_ERROR)
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
            NULL;-- don't change tran_state_cd
        ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
             pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
        END IF;

        UPDATE pss.tran
        SET tran_state_cd = pv_tran_state_cd,
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id;

        DELETE FROM pss.tran_line_item
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
    END IF;

    SELECT c.MINOR_CURRENCY_FACTOR
      INTO ln_minor_currency_factor
      FROM PSS.POS_PTA PTA
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
     WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount / ln_minor_currency_factor,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash,
        sale_device_session_id = pn_session_id
    WHERE tran_id = pn_tran_id;

    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash,
            sale_device_session_id
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount / ln_minor_currency_factor,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash,
            pn_session_id
        );
    END IF;
END;

FUNCTION sf_find_host_id(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
BEGIN
    SELECT MAX(H.HOST_ID)
      INTO ln_host_id
      FROM DEVICE.HOST H
     WHERE H.DEVICE_ID = pn_device_id
       AND H.HOST_PORT_NUM = pn_host_port_num
       AND H.HOST_POSITION_NUM = pn_host_position_num;

    IF ln_host_id IS NULL THEN
        -- Use base host
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
         WHERE H.DEVICE_ID = pn_device_id
           AND H.HOST_PORT_NUM = 0;
    END IF;
    
    RETURN ln_host_id;
END;

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_device_id host.device_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
    ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
BEGIN
    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID
      INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    WHERE X.TRAN_ID = pn_tran_id;

    ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    -- For Kiosk type, use tran_line_item_type to find description
    IF ln_device_type_id = 11 THEN
        SELECT TRIM(SUBSTR(tran_line_item_type_desc || ' ' || pv_tli_desc, 1, 60))
          INTO ln_tli_desc
          FROM pss.tran_line_item_type
         WHERE tran_line_item_type_id = pn_tli_type_id;
    ELSIF ln_device_type_id = 5 THEN -- eSuds
        SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
                || GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
          INTO ln_tli_desc
          FROM PSS.TRAN_LINE_ITEM_TYPE tlit
         CROSS JOIN DEVICE.HOST H
          JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
          LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
          JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
            ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = pn_tli_type_id
           AND H.HOST_ID = ln_host_id;
    ELSE
        ln_tli_desc := pv_tli_desc;
    END IF;

    INSERT INTO pss.tran_line_item (
        tran_line_item_id,
        tran_id,
        tran_line_item_amount,
        tran_line_item_position_cd,
        tran_line_item_tax,
        tran_line_item_type_id,
        tran_line_item_quantity,
        tran_line_item_desc,
        host_id,
        tran_line_item_batch_type_cd,
        tran_line_item_ts,
        sale_result_id
    )
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pn_tli_type_id,
        pn_tli_quantity,
        ln_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
        pn_sale_result_id
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = pn_tli_type_id;

    -- For all device actual batch type only
    IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
		SELECT tran_line_item_type_group_cd
		INTO lc_tli_type_group_cd
		FROM pss.tran_line_item_type
		WHERE tran_line_item_type_id = pn_tli_type_id;
	
		IF lc_tli_type_group_cd IN ('P', 'S') THEN
			UPDATE PSS.TRAN_LINE_ITEM_RECENT
			SET tran_line_item_id = ln_tran_line_item_id,
				fkp_tran_id = pn_tran_id
			WHERE host_id = ln_host_id
				AND tran_line_item_type_id = pn_tli_type_id;
				
			IF SQL%NOTFOUND THEN
				BEGIN
					INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
						TRAN_LINE_ITEM_ID,
						HOST_ID,
						FKP_TRAN_ID,
						TRAN_LINE_ITEM_TYPE_ID
					) VALUES (
						ln_tran_line_item_id,
						ln_host_id,
						pn_tran_id,
						pn_tli_type_id
					);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						UPDATE PSS.TRAN_LINE_ITEM_RECENT
						SET tran_line_item_id = ln_tran_line_item_id,
							fkp_tran_id = pn_tran_id
						WHERE host_id = ln_host_id
							AND tran_line_item_type_id = pn_tli_type_id;
				END;
			END IF;
		END IF;
    END IF;

END;

FUNCTION sf_tran_import_needed (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE
)
  RETURN VARCHAR2
IS
	lc_tran_import_needed VARCHAR2(1) := 'N';
BEGIN
	BEGIN
		SELECT DECODE(S.IMPORTED, 'Y', 'N', 'Y')
		INTO lc_tran_import_needed
		FROM PSS.TRAN T
		JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
		WHERE T.TRAN_ID = pn_tran_id
			AND T.TRAN_STATE_CD != PKG_CONST.TRAN_STATE__DUPLICATE
			AND S.SALE_TYPE_CD != PKG_CONST.SALE_TYPE__INTENDED
			AND DECODE(S.SALE_TYPE_CD, PKG_CONST.SALE_TYPE__CASH, 'N', DBADMIN.PKG_UTL.EQL(T.TRAN_RECEIVED_RAW_ACCT_DATA, NULL)) = 'N';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			NULL;
	END;
	RETURN lc_tran_import_needed;
END;

-- R26+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_tli_count NUMBER;
    ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	pc_tran_import_needed := 'N';

    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;

        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;

    SELECT NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
           COUNT(1)
      INTO ln_tli_total, ln_tli_count
      FROM PSS.TRAN_LINE_ITEM
     WHERE TRAN_ID = pn_tran_id
       AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID
      INTO ln_device_id, ln_minor_currency_factor, lc_client_payment_type_cd, ln_device_type_id
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
    WHERE X.TRAN_ID = pn_tran_id;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
        OR NVL(pn_sale_amount, 0) = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := NVL(pn_sale_amount / ln_minor_currency_factor, 0) - ln_tli_total;
        IF ln_adj_amt > 0 THEN
            ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;
    
    IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
		IF ln_tli_count > 0 THEN
            -- create Transaction Amount Summary record
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            VALUES(
                pn_tran_id,
                ln_base_host_id,
                201,
                NVL(pn_sale_amount / ln_minor_currency_factor, 0),
                1,
                'Transaction Amount Summary',
                pc_tran_batch_type_cd,
                pn_sale_tax);
		END IF;
   ELSE
        IF ln_adj_tli > -1 THEN
            -- use the base host for adjustments
            SELECT MAX(H.HOST_ID)
            INTO ln_base_host_id
            FROM DEVICE.HOST H
            WHERE H.DEVICE_ID = ln_device_id
            AND H.HOST_PORT_NUM = 0;
            IF ln_base_host_id IS NULL THEN
                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                    RETURN;
                END IF;
                SELECT H.HOST_ID
                INTO ln_base_host_id
                FROM DEVICE.HOST H
                WHERE H.DEVICE_ID = ln_device_id
                AND H.HOST_PORT_NUM = 0;
            END IF;
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                ln_adj_tli,
                ln_adj_amt,
                1,
                tran_line_item_type_desc,
                pc_tran_batch_type_cd,
                pn_sale_tax
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = ln_adj_tli;
        END IF;
            
   END IF;

    pkg_call_in_record.sp_add_trans(pn_session_id, lc_client_payment_type_cd, pn_sale_amount / ln_minor_currency_factor, ln_tli_count);

	pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

-- R25 signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL
)
IS
	lc_tran_import_needed VARCHAR2(1);
BEGIN
	sp_finalize_sale (
    pn_tran_id,
    pn_session_id,
    pv_tran_device_result_type_cd,
    pn_sale_result_id,
    pn_sale_amount,
    pn_sale_tax,
    pc_tran_batch_type_cd,
    pn_sale_utc_ts_ms,
    pn_sale_duration_sec,
    pn_result_cd,
    pv_error_message,
    pc_sale_type_cd,
	lc_tran_import_needed);
END;

-- R26+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE := pv_pan;
   lv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE := pv_card_holder;
   lv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE := pv_expiration_date;
   lv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE := pv_pin;
   lv_tran_reportable_acct_num PSS.TRAN.TRAN_REPORTABLE_ACCT_NUM%TYPE := SUBSTR(pv_pan, LENGTH(pv_pan) - 3, LENGTH(pv_pan));
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_event_cd);
   lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_event_cd);
BEGIN
	pc_tran_import_needed := 'N';
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
	
    BEGIN
        SELECT tran_id, sale_type_cd, tran_state_cd, trace_number, sale_result_id
        INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id
        FROM
        (
			SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, s.sale_type_cd, t.tran_state_cd, a.trace_number, s.sale_result_id
			FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
			WHERE t.tran_device_tran_cd = pv_device_event_cd AND (
					t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
					OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
					OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
				)
            ORDER BY CASE WHEN a.trace_number = pn_trace_number THEN 1 ELSE 2 END,
				CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				t.tran_start_ts, t.created_ts, a.created_ts
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_tran_id := NULL;
    END;
	
    IF pn_tran_id IS NOT NULL THEN
		IF ld_orig_trace_number = pn_trace_number THEN
			RETURN;
		END IF;
		
		IF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
            IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND)
                OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id = 2 /*Cancelled by Auth Timeout*/ THEN
				
				IF pc_auth_result_cd IN('Y', 'P') THEN
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
					END IF;
				ELSE
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
					END IF;
				END IF;
				
				UPDATE PSS.TRAN
				   SET (TRAN_START_TS,
						TRAN_END_TS,
						TRAN_STATE_CD,
						TRAN_ACCOUNT_PIN,
						TRAN_RECEIVED_RAW_ACCT_DATA,
						TRAN_PARSED_ACCT_NAME,
						TRAN_PARSED_ACCT_EXP_DATE,
						TRAN_PARSED_ACCT_NUM,
						TRAN_PARSED_ACCT_NUM_HASH,
						TRAN_REPORTABLE_ACCT_NUM,
						POS_PTA_ID,
						CONSUMER_ACCT_ID,
						AUTH_DEVICE_SESSION_ID,
						PAYMENT_SUBTYPE_KEY_ID,
						PAYMENT_SUBTYPE_CLASS,
						CLIENT_PAYMENT_TYPE_CD,						
						AUTH_HOLD_USED,
						DEVICE_NAME) =
					(SELECT
						ld_tran_start_ts,  /* TRAN_START_TS */
						TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
						pc_tran_state_cd,  /* TRAN_STATE_CD */
						lv_pin,  /* TRAN_ACCOUNT_PIN */
						pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
						lv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
						lv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
						lv_pan,  /* TRAN_PARSED_ACCT_NUM */
						pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
						lv_tran_reportable_acct_num,  /* TRAN_REPORTABLE_ACCT_NUM */
						pn_pos_pta_id, /* POS_PTA_ID */
						pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
						pn_session_id,
						pp.payment_subtype_key_id,
						ps.payment_subtype_class,
						ps.client_payment_type_cd,
						pc_auth_hold_used,
						pv_device_name
					FROM pss.pos_pta pp
					JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
					WHERE pp.pos_pta_id = pn_pos_pta_id)
					WHERE TRAN_ID = pn_tran_id;
			ELSE
				pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
				lc_invalid_device_event_cd := 'Y';
				lv_pin := NULL;
				lv_card_holder := NULL;
				lv_expiration_date := NULL;
				lv_pan := NULL;			
			END IF;
        END IF;
	ELSE
        IF pc_auth_result_cd IN('Y', 'P') AND pc_sent_to_device = 'N' AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('F') AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('Y') THEN
            pc_tran_state_cd := '6'; -- Auth Success
        ELSIF pc_auth_result_cd IN('P') THEN
            pc_tran_state_cd := '0'; -- Auth Success Conditional
        ELSIF pc_auth_result_cd IN('N', 'O') THEN
            pc_tran_state_cd := '7'; -- Auth Decline
        ELSIF pc_auth_result_cd IN('F') THEN
            pc_tran_state_cd := '5'; -- Auth Failure
        END IF;
	END IF;
		
    IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
          INTO pn_tran_id
          FROM DUAL;    
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
			TRAN_END_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_PARSED_ACCT_NUM_HASH,
            TRAN_REPORTABLE_ACCT_NUM,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID,
            AUTH_DEVICE_SESSION_ID,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			AUTH_HOLD_USED,
			DEVICE_NAME)
        SELECT
            pn_tran_id, /* TRAN_ID */
            ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts,  /* TRAN_END_TS */
            pc_tran_state_cd,  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            lv_pin,  /* TRAN_ACCOUNT_PIN */
            pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            lv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
            lv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
            lv_pan,  /* TRAN_PARSED_ACCT_NUM */
            pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
            lv_tran_reportable_acct_num,  /* TRAN_REPORTABLE_ACCT_NUM */
            pn_pos_pta_id, /* POS_PTA_ID */
            DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
            pn_session_id,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pc_auth_hold_used,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;
    END IF;

    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO ln_auth_id
      FROM DUAL;
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        AUTH_PARSED_ACCT_DATA,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        AUTH_BALANCE_AMT,
        TRACE_NUMBER,
        AUTH_ACTION_ID,
        AUTH_ACTION_BITMAP,
        AUTH_HOLD_USED)
     VALUES(
        ln_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        pv_auth_acct_data, /* AUTH_PARSED_ACCT_DATA */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        DECODE(pc_auth_result_cd, 'Y', pn_auth_amt, 'P', pn_received_amt) / pn_minor_currency_factor, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
        pn_trace_number, /* TRACE_NUMBER */
        pn_auth_action_id,
        pn_auth_action_bitmap,
        pc_auth_hold_used
        );

    IF pc_add_auth_hold = 'Y' THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID)
          VALUES(pn_consumer_acct_id, ln_auth_id, pn_tran_id);
    END IF;
	
	pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
END;

-- R25 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2
) IS
	lc_tran_import_needed VARCHAR2(1);
BEGIN
	SP_CREATE_AUTH(
   pv_global_event_cd,
   pv_device_name,
   pn_pos_pta_id,
   pv_device_event_cd,
   pc_invalid_device_event_cd,
   pn_tran_start_time,
   pc_auth_result_cd,
   pc_entry_method,
   pc_payment_type,
   pv_track_data,
   pv_auth_acct_data,
   pv_pan,
   pv_pan_sha1,
   pv_card_holder,
   pv_expiration_date,
   pv_pin,
   pn_consumer_acct_id,
   pd_auth_ts,
   pv_authority_resp_cd,
   pv_authority_resp_desc,
   pv_authority_tran_cd,
   pv_authority_ref_cd,
   pt_authority_ts,
   pv_authority_misc_data,
   pn_trace_number,
   pn_minor_currency_factor,
   pn_auth_amt,
   pn_balance_amt,
   pn_requested_amt,
   pn_received_amt,
   pc_add_auth_hold,
   pc_auth_hold_used,
   pn_session_id,
   pc_ignore_dup,
   pn_auth_action_id,
   pn_auth_action_bitmap,
   pc_sent_to_device,
   pc_pass_thru,
   pn_tran_id,
   pc_tran_state_cd,
   lc_tran_import_needed); 
END;

PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      VALUES(pn_tran_id, 2 /* live auth "POSM" time*/, (pn_applayer_end_time - pn_applayer_start_time) / 1000);
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      VALUES(pn_tran_id, 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000);
    IF pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL THEN
        INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
          VALUES(pn_tran_id, 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000);
    END IF;
END;

PROCEDURE SP_PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER
)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND CA.CONSUMER_ACCT_CD = pv_consumer_acct_cd
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CURRENCY_CD = pv_currency_cd
            GROUP BY CA.CONSUMER_ACCT_ID, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
                     ELSE DTA.ACTION_ID END ACTION_ID
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.FILE_TRANSFER ft
             ON ft.FILE_TRANSFER_NAME = DECODE(DTA.DEVICE_TYPE_ID,
                        0, 'G4-DEFAULT-CFG',
                        1, 'G5-DEFAULT-CFG',
                        6, 'MEI-DEFAULT-CFG',
                        12, 'T2-DEFAULT-CFG',
                        'DEFAULT-CFG-' || DTA.DEVICE_TYPE_ID || '-' || ds_v.DEVICE_SETTING_VALUE)
            AND ft.FILE_TRANSFER_TYPE_CD = DECODE(DTA.DEVICE_TYPE_ID, 0, 6, 1, 6, 6, 6, 12, 15, 22)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_global_event_cd_prefix IN CHAR,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pn_sale_utc_ts_ms NUMBER,
   pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
   pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
   pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
   pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
   pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
   pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pn_result_cd OUT NUMBER,
   pv_error_message OUT VARCHAR2
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
   lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
   lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
   ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
   ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
   ln_tli_hash_match NUMBER;
   ld_current_ts DATE := SYSDATE;
   ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
   lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
   lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
BEGIN
    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
    BEGIN
        SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
        INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
				t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
			)
            ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;

            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;
	
		ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
			lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
		ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
			lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
		END IF;
		
		SELECT PSS.SEQ_TRAN_ID.NEXTVAL
		INTO pn_tran_id
		FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;		

		INSERT INTO PSS.TRAN (
			TRAN_ID,
			TRAN_START_TS,
			TRAN_END_TS,
			TRAN_UPLOAD_TS,
			TRAN_STATE_CD,
			TRAN_DEVICE_TRAN_CD,
			TRAN_RECEIVED_RAW_ACCT_DATA,
			TRAN_PARSED_ACCT_NUM_HASH,
			TRAN_REPORTABLE_ACCT_NUM,
			POS_PTA_ID,
			TRAN_GLOBAL_TRANS_CD,
			CONSUMER_ACCT_ID,
			TRAN_DEVICE_RESULT_TYPE_CD,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME)
		SELECT
			pn_tran_id, /* TRAN_ID */
			ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts, /* TRAN_END_TS */
			ld_current_ts,
			lv_tran_state_cd,  /* TRAN_STATE_CD */
			pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
			pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
			pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
			SUBSTR(pv_track_data, -4, 4),  /* TRAN_REPORTABLE_ACCT_NUM */
			pn_pos_pta_id, /* POS_PTA_ID */
			lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
			pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
			pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;

		SELECT PSS.SEQ_AUTH_ID.NEXTVAL
		  INTO ln_auth_id
		  FROM DUAL;
		INSERT INTO PSS.AUTH (
			AUTH_ID,
			TRAN_ID,
			AUTH_STATE_ID,
			AUTH_TYPE_CD,
			AUTH_PARSED_ACCT_DATA,
			ACCT_ENTRY_METHOD_CD,
			AUTH_TS,
			AUTH_RESULT_CD,
			AUTH_RESP_CD,
			AUTH_RESP_DESC,
			AUTH_AUTHORITY_TS,
			AUTH_AMT,
			AUTH_AUTHORITY_AMT_RQST,
			TRACE_NUMBER)
		 VALUES(
			ln_auth_id, /* AUTH_ID */
			pn_tran_id, /* TRAN_ID */
			DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
			'L', /* AUTH_TYPE_CD */
			pv_track_data, /* AUTH_PARSED_ACCT_DATA */
			DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
			pd_auth_ts, /* AUTH_TS */
			pc_auth_result_cd, /* AUTH_RESULT_CD */
			'LOCAL', /* AUTH_RESP_CD */
			'Local authorization not accepted', /* AUTH_RESP_DESC */
			pd_auth_ts, /* AUTH_AUTHORITY_TS */
			NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
			pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
			pn_trace_number /* TRACE_NUMBER */
			);

		INSERT INTO pss.sale (
			tran_id,
			device_batch_id,
			sale_type_cd,
			sale_start_utc_ts,
			sale_end_utc_ts,
			sale_utc_offset_min,
			sale_result_id,
			sale_amount,
			receipt_result_cd,
			hash_type_cd,
			tran_line_item_hash
		) VALUES (
			pn_tran_id,
			pn_device_batch_id,
			pc_sale_type_cd,
			lt_sale_start_utc_ts,
			lt_sale_start_utc_ts,
			pn_sale_utc_offset_min,
			pn_sale_result_id,
			pn_auth_amt / pn_minor_currency_factor,
			'U',
			pv_hash_type_cd,
			pv_tran_line_item_hash
		);
	END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/edge_implementation/R26.USADBP.FHMS.Cleanup.sql?rev=HEAD
UPDATE PSS.PAYMENT_SUBTYPE
   SET PAYMENT_SUBTYPE_CLASS = 'Authority::NOP'
 WHERE PAYMENT_SUBTYPE_CLASS = 'Authority::ISO8583::FHMS';
 
COMMIT;

 
   

/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/RecompileInvalidObjects.sql?rev=HEAD
DECLARE
    CURSOR l_cur IS
select 'alter '||decode(object_type,'PACKAGE BODY'
,'package '||owner||'.'||object_name||
'  compile body'
,object_type||' '||owner||'.'||object_name
||' compile') sql_text
from dba_objects 
where status = 'INVALID' and object_type NOT IN('MATERIALIZED VIEW', 'SYNONYM')
and owner in (
	'APP_EXEC_HIST',
	'APP_LAYER',
	'APP_LOG',
	'APP_USER',
	'AUTHORITY',
	'CORP',
	'DBADMIN',
	'DEVICE',
	'ENGINE',
	'FOLIO_CONF',
	'FRONT',
	'G4OP',
	'LOCATION',
	'PSS',
	'RDW_LOADER',
	'REAL_TIME',
	'RECON',
	'REPL_MGR',
	'REPORT',
	'SONY',
	'TAZDBA',
	'TRACKING',
	'UITOOL',
	'UPDATER',
	'USAQA',
	'USAT_REPORTING',
	'WEB_CONTENT'
);
BEGIN
    FOR i IN 1..2 LOOP
        FOR l_rec in l_cur LOOP
            DBMS_OUTPUT.PUT_LINE('Executing "' || l_rec.sql_text || '"');
            BEGIN
                EXECUTE IMMEDIATE l_rec.sql_text;
            EXCEPTION
                WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('Failed "' || l_rec.sql_text || '":');
                    DBMS_OUTPUT.PUT_LINE(SQLERRM);
            END;
        END LOOP;
    END LOOP;
END;
/

