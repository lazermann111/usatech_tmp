/*
Track one, Format B:

    * Start sentinel � one character (generally '%')
    * Format code="B" � one character (alpha only)
    * Primary account number (PAN) � up to 19 characters. Usually, but not always, matches the credit card number printed on the front of the card.
    * Field Separator � one character (generally '^')
    * Name � two to 26 characters
    * Field Separator � one character (generally '^')
    * Expiration date � four characters in the form YYMM.
    * Service code � three characters
    * Discretionary data � may include Pin Verification Key Indicator (PVKI, 1 character), Pin Verification Value (PVV, 4 characters), Card Verification Value or Card Verification Code (CVV or CVK, 3 characters)
    * End sentinel � one character (generally '?')
    * Longitudinal redundancy check (LRC) � one character

LRC is a validity character calculated from other data on the track.

'^%B' || PAN || '\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)\^([0-9]{3}?)([A-Za-z0-9 \/]*)(?:\?([0-9])?)$
1=Card Holder = 2
2=Expiration Date = 3
3 = Service Code = 3
4 = Discretionary data = 4
^(%?)B(([0-9]{13,16}[0-9]{0,3})|([0-9]{4} [0-9]{6} [0-9]{5}))\^([A-Za-z0-9 \/]{0,26})\^(([0-9]{0,4})|\^)([A-Za-z0-9 \/]{0,57})(\??)([0-9]{0,3})$

Track 2. This format was developed by the banking industry (ABA). This track is written with a 5-bit scheme (4 data bits + 1 parity), which allows for sixteen possible characters, which are the numbers 0-9, plus the six characters  : ; < = > ? . The selection of six punctuation symbols may seem odd, but in fact the sixteen codes simply map to the ASCII range 0x30 through 0x3f, which defines ten digit characters plus those six symbols. The data format is as follows:

    * Start sentinel � one character (generally ';')
    * Primary account number (PAN) � up to 19 characters. Usually, but not always, matches the credit card number printed on the front of the card.
    * Separator � one char (generally '=')
    * Expiration date � four characters in the form YYMM.
    * Service code � three characters
    * Discretionary data � as in track one
    * End sentinel � one character (generally '?')
    * LRC � one character
^;?PAN=([0-9]{4})([0-9]{3})([0-9]{1,4})\?([0-9]?)$
1=Expiration Date = 3
2=Service Code = 4
3=Discretionary data = 5
4=LRC = 12

*/

UPDATE PSS.AUTHORITY_PAYMENT_MASK 
   SET PAYMENT_SUBTYPE_ID = NULL
 WHERE PAYMENT_SUBTYPE_ID IN(2,3);
DELETE FROM PSS.PAYMENT_SUBTYPE 
 WHERE PAYMENT_SUBTYPE_ID IN(2,3);
DELETE FROM PSS.AUTHORITY_PAYMENT_MASK 
 WHERE AUTHORITY_PAYMENT_MASK_ID IN(2,3);
    
CREATE SEQUENCE AUTHORITY.SEQ_AUTHORITY_ASSN_ID;
CREATE PUBLIC SYNONYM SEQ_AUTHORITY_ASSN_ID FOR AUTHORITY.SEQ_AUTHORITY_ASSN_ID;

DECLARE
cursor l_cur is
select a.name, '^;?(' || a.prefix || '[0-9]{' || (a.pan_length - a.prefix_digits) || '})=([0-9]{4})([0-9]{3})([0-9]*)(?:\?([\x00-\xFF])?)?$' track2_regex, '1:1|2:3|3:4|4:5|5:12' track2_bref,
'^%?B(' || a.prefix || '[0-9]{' || (a.pan_length - a.prefix_digits) || '})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)\^([0-9]{3}?)([A-Za-z0-9 \/]*)(?:\?([\x00-\xFF])?)$' track1_regex, '1:1|2:2|3:3|4:4|5:5|6:12' track1_bref
from (
select '' Name, '' prefix, 0 prefix_digits, 0 pan_length, '' check_alg from dual where 1=0
UNION ALL SELECT 'American Express','3[47]', 2, 15,'L' FROM DUAL
UNION ALL SELECT 'Diners Club Carte Blanche','30[0-5]', 3, 14,'L' FROM DUAL
UNION ALL SELECT 'Diners Club International','36', 2, 14,'L' FROM DUAL
UNION ALL SELECT 'Discover Card','(?:6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})',4, 16,'L' FROM DUAL
UNION ALL SELECT 'MasterCard','5[1-5]', 2, 16,'L' FROM DUAL
UNION ALL SELECT 'Visa','4', 1, 16,'L' FROM DUAL) a;
    ln_assn_id AUTHORITY_ASSN.AUTHORITY_ASSN_ID%TYPE;
    ln_apm_id PSS.AUTHORITY_PAYMENT_MASK.AUTHORITY_PAYMENT_MASK_ID%TYPE;
    ln_pst_id PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_ID%TYPE;
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT SEQ_AUTHORITY_ASSN_ID.NEXTVAL
          INTO ln_assn_id
          FROM DUAL;
        INSERT INTO AUTHORITY.AUTHORITY_ASSN(AUTHORITY_ASSN_ID, AUTHORITY_ASSN_NAME, AUTHORITY_ASSN_DESC)
          VALUES(ln_assn_id, l_rec.name, l_rec.name || ' Credit Card');
    
        FOR ln_track IN 1..2 LOOP
            SELECT PSS.SEQ_AUTHORITY_PAYMENT_MASK_ID.NEXTVAL
              INTO ln_apm_id
              FROM DUAL;
            INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_ID, PAYMENT_SUBTYPE_ID, AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, AUTHORITY_ASSN_ID)
                VALUES(ln_apm_id, NULL, l_rec.name || ' Track ' || ln_track, l_rec.name || ' Credit Card on Track '||ln_track, DECODE(ln_track, 1, l_rec.track1_regex, 2, l_rec.track2_regex), DECODE(ln_track, 1, l_rec.track1_bref, 2, l_rec.track2_bref), ln_assn_id);
            INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_ID, PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
                SELECT PSS.SEQ_PAYMENT_SUBTYPE_ID.NEXTVAL, prefix || l_rec.name || ' (Track '||ln_track||')' || suffix, class_name, 'TERMINAL_ID', payment_type, 'TERMINAL', 'TERMINAL_DESC', ln_apm_id
                --SELECT *
                  FROM (SELECT '' suffix, '' class_name FROM DUAL WHERE 1=0
                  UNION ALL SELECT ' - USA', 'Authority::ISO8583::FHMS' from DUAL
                  UNION ALL SELECT ' - Canada', 'Authority::ISO8583::FHMS::Paymentech' from DUAL) a
                  CROSS JOIN (SELECT '' prefix, '' payment_type FROM DUAL WHERE 1=0
                  UNION ALL SELECT '', 'C' FROM DUAL
                  UNION ALL SELECT 'RFID ', 'R' FROM DUAL) b;
        END LOOP;
    END LOOP;
END;
/
/*
SELECT * FROM PSS.AUTHORITY_PAYMENT_MASK APM
JOIN PSS.PAYMENT_SUBTYPE PST ON APM.AUTHORITY_PAYMENT_MASK_ID = PST.AUTHORITY_PAYMENT_MASK_ID
WHERE PST.CREATED_TS > SYSDATE - 31
order by pst.AUTHORITY_PAYMENT_MASK_ID, pst.PAYMENT_SUBTYPE_ID;
*/
COMMIT;

INSERT INTO AUTHORITY.AUTHORITY_SERVICE(AUTHORITY_SERVICE_ID, AUTHORITY_SERVICE_NAME, AUTHORITY_SERVICE_DESC, AUTHORITY_SERVICE_TYPE_ID)
 SELECT AUTHORITY_SERVICE_TYPE_ID, AUTHORITY_SERVICE_TYPE_NAME, AUTHORITY_SERVICE_TYPE_DESC, AUTHORITY_SERVICE_TYPE_ID
   FROM AUTHORITY.AUTHORITY_SERVICE_TYPE
  WHERE AUTHORITY_SERVICE_TYPE_ID IN(3,4);

UPDATE AUTHORITY.AUTHORITY A
   SET AUTHORITY_SERVICE_ID = 4
 WHERE A.AUTHORITY_SERVICE_ID = 1
   AND A.AUTHORITY_ID IN(
SELECT DISTINCT M.AUTHORITY_ID 
FROM PSS.POS_PTA PTA
JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
JOIN PSS.TERMINAL T ON PTA.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID
JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
WHERE PST.PAYMENT_SUBTYPE_TABLE_NAME = 'TERMINAL'
AND PST.CLIENT_PAYMENT_TYPE_CD IN('C', 'R'));

COMMIT;
