ALTER TABLE DEVICE.DEVICE_SETTING_PARAMETER ADD (DEVICE_SETTING_UI_CONFIGURABLE VARCHAR2(1));
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_UI_CONFIGURABLE = 'Y';
ALTER TABLE DEVICE.DEVICE_SETTING_PARAMETER MODIFY (DEVICE_SETTING_UI_CONFIGURABLE DEFAULT 'Y' NOT NULL);

ALTER TABLE DEVICE.DEVICE_SETTING_PARAMETER ADD (DEVICE_SETTING_PARAMETER_NAME VARCHAR2(100));

BEGIN
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_UI_CONFIGURABLE = 'N' WHERE DEVICE_SETTING_PARAMETER_CD = 'Firmware Version';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE)
    VALUES ('Firmware Version', 'N');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_UI_CONFIGURABLE = 'N' WHERE DEVICE_SETTING_PARAMETER_CD = 'New Merchant ID';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE)
    VALUES ('New Merchant ID', 'N');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_UI_CONFIGURABLE = 'N' WHERE DEVICE_SETTING_PARAMETER_CD = 'Property List Version';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE)
    VALUES ('Property List Version', 'N');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_UI_CONFIGURABLE = 'N' WHERE DEVICE_SETTING_PARAMETER_CD = 'Protocol Revision';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE)
    VALUES ('Protocol Revision', 'N');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IP name' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '0';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('0', 'Auth IP name');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IPV4 name backup 1' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '1';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('1', 'Auth IPV4 name backup 1');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IPV4 address backup 2' 
WHERE DEVICE_SETTING_PARAMETER_CD = '2';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('2', 'Auth IPV4 address backup 2');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IPV4 address backup 3' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '3';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('3', 'Auth IPV4 address backup 3');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IP port' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '4';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('4', 'Auth IP port');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IP port backup 1' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '5';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('5', 'Auth IP port backup 1');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IP port backup 2' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '6';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('6', 'Auth IP port backup 2');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth IP port backup 3' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '7';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('7', 'Auth IP port backup 3');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Auth response timeout' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '10';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('10', 'Auth response timeout');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IP name' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '20';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('20', 'Misc IP name');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IP name backup 1' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '21';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('21', 'Misc IP name backup 1');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IPV4 address backup 2' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '22';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('22', 'Misc IPV4 address backup 2');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IPV4 address backup 3' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '23';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('23', 'Misc IPV4 address backup 3');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IP port' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '24';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('24', 'Misc IP port');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IP port backup 1' 
 WHERE DEVICE_SETTING_PARAMETER_CD = '25';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('25', 'Misc IP port backup 1');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IP port backup 2'
 WHERE DEVICE_SETTING_PARAMETER_CD = '26';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('26', 'Misc IP port backup 2');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc IP port backup 3'
 WHERE DEVICE_SETTING_PARAMETER_CD = '27';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('27', 'Misc IP port backup 3');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc response timeout'
 WHERE DEVICE_SETTING_PARAMETER_CD = '30';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('30', 'Misc response timeout');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc retry interval'
 WHERE DEVICE_SETTING_PARAMETER_CD = '31';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('31', 'Misc retry interval');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc retry slope'
 WHERE DEVICE_SETTING_PARAMETER_CD = '32';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('32', 'Misc retry slope');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Misc maximum number of failed connections'
 WHERE DEVICE_SETTING_PARAMETER_CD = '33';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('33', 'Misc maximum number of failed connections');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Serial number  EE1xxxxxxxx'
 WHERE DEVICE_SETTING_PARAMETER_CD = '50';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('50', 'Serial number  EE1xxxxxxxx');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Device GUID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '51';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('51', 'Device GUID');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Encryption key'
 WHERE DEVICE_SETTING_PARAMETER_CD = '52';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('52', 'Encryption key');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Master ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '53';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('53', 'Master ID');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Master ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '60';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('60', 'Master ID');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Message ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '61';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('61', 'Message ID');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Transaction ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '62';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('62', 'Transaction ID');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Batch ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '63';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('63', 'Batch ID');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Event ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '64';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('64', 'Event ID');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Activation status'
 WHERE DEVICE_SETTING_PARAMETER_CD = '70';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('70', 'Activation status');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='UTC time'
 WHERE DEVICE_SETTING_PARAMETER_CD = '80';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('80', 'UTC time');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='UTC Offset'
 WHERE DEVICE_SETTING_PARAMETER_CD = '81';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('81', 'UTC Offset');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Scheduled settlement'
 WHERE DEVICE_SETTING_PARAMETER_CD = '85';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('85', 'Scheduled settlement');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Scheduled session'
 WHERE DEVICE_SETTING_PARAMETER_CD = '86';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('86', 'Scheduled session');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--cash vended items'
 WHERE DEVICE_SETTING_PARAMETER_CD = '100';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('100', 'Pcounter--cash vended items');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--cash amount (pennies)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '101';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('101', 'Pcounter--cash amount (pennies)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--cashless vended items'
 WHERE DEVICE_SETTING_PARAMETER_CD = '102';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('102', 'Pcounter--cashless vended items');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--cashless amount (pennies)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '103';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('103', 'Pcounter--cashless amount (pennies)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--bytes transmitted (client to server)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '104';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('104', 'Pcounter--bytes transmitted (client to server)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--bytes received (client to server)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '105';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('105', 'Pcounter--bytes received (client to server)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--# of comminication sessions'
 WHERE DEVICE_SETTING_PARAMETER_CD = '106';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('106', 'Pcounter--# of comminication sessions');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--# Of failed comminication attempts'
 WHERE DEVICE_SETTING_PARAMETER_CD = '107';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('107', 'Pcounter--# Of failed comminication attempts');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Pcounter--# of incomplete sessions'
 WHERE DEVICE_SETTING_PARAMETER_CD = '108';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('108', 'Pcounter--# of incomplete sessions');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--cash vended items'
 WHERE DEVICE_SETTING_PARAMETER_CD = '200';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('200', 'Icounter--cash vended items');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--cash amount (pennies)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '201';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('201', 'Icounter--cash amount (pennies)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--cashless vended items'
 WHERE DEVICE_SETTING_PARAMETER_CD = '202';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('202', 'Icounter--cashless vended items');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--cashless amount (pennies)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '203';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('203', 'Icounter--cashless amount (pennies)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--bytes transmitted (client to server)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '204';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('204', 'Icounter--bytes transmitted (client to server)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--bytes received (client to server)'
 WHERE DEVICE_SETTING_PARAMETER_CD = '205';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('205', 'Icounter--bytes received (client to server)');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--# of comminication sessions'
 WHERE DEVICE_SETTING_PARAMETER_CD = '206';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('206', 'Icounter--# of comminication sessions');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--# Of failed comminication attempts'
 WHERE DEVICE_SETTING_PARAMETER_CD = '207';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('207', 'Icounter--# Of failed comminication attempts');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='Icounter--# of incomplete sessions'
 WHERE DEVICE_SETTING_PARAMETER_CD = '208';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('208', 'Icounter--# of incomplete sessions');
END IF;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Pricing Mode'
 WHERE DEVICE_SETTING_PARAMETER_CD = '301';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('301', 'MDB Setting Pricing Mode');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Pricing Adjustment Percentage'
 WHERE DEVICE_SETTING_PARAMETER_CD = '302';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('302', 'MDB Setting Pricing Adjustment Percentage');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Pricing Schedule Time Start'
 WHERE DEVICE_SETTING_PARAMETER_CD = '303';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('303', 'MDB Setting Pricing Schedule Time Start');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Pricing Schedule Time End'
 WHERE DEVICE_SETTING_PARAMETER_CD = '304';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('304', 'MDB Setting Pricing Schedule Time End');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Hourly Schedule Bitmap'
 WHERE DEVICE_SETTING_PARAMETER_CD = '305';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('305', 'MDB Setting Hourly Schedule Bitmap');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Weekly Schedule Bitmap'
 WHERE DEVICE_SETTING_PARAMETER_CD = '306';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('306', 'MDB Setting Weekly Schedule Bitmap');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Terminate On Insufficient Funds'
 WHERE DEVICE_SETTING_PARAMETER_CD = '307';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('307', 'MDB Setting Terminate On Insufficient Funds');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Session End After Vend Fall'
 WHERE DEVICE_SETTING_PARAMETER_CD = '308';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('308', 'MDB Setting Session End After Vend Fall');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Preferred Feature Level'
 WHERE DEVICE_SETTING_PARAMETER_CD = '309';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('309', 'MDB Setting Preferred Feature Level');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Vend Assumption'
 WHERE DEVICE_SETTING_PARAMETER_CD = '310';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('310', 'MDB Setting Vend Assumption');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Tx Drive Level'
 WHERE DEVICE_SETTING_PARAMETER_CD = '311';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('311', 'MDB Setting Tx Drive Level');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Erroneous Cash Sale Translation'
 WHERE DEVICE_SETTING_PARAMETER_CD = '312';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('312', 'MDB Setting Erroneous Cash Sale Translation');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Duplicate Vend Filtering'
 WHERE DEVICE_SETTING_PARAMETER_CD = '313';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('313', 'MDB Setting Duplicate Vend Filtering');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Max Number Of Items'
 WHERE DEVICE_SETTING_PARAMETER_CD = '314';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('314', 'MDB Setting Max Number Of Items');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Interbyte Delay 200us'
 WHERE DEVICE_SETTING_PARAMETER_CD = '315';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('315', 'MDB Setting Interbyte Delay 200us');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Response Delay 200us'
 WHERE DEVICE_SETTING_PARAMETER_CD = '316';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('316', 'MDB Setting Response Delay 200us');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Parse Delay 200us'
 WHERE DEVICE_SETTING_PARAMETER_CD = '317';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('317', 'MDB Setting Parse Delay 200us');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Cardreader ID'
 WHERE DEVICE_SETTING_PARAMETER_CD = '318';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('318', 'MDB Setting Cardreader ID');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Response Type'
 WHERE DEVICE_SETTING_PARAMETER_CD = '319';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('319', 'MDB Setting Response Type');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Monetary Format Pref'
 WHERE DEVICE_SETTING_PARAMETER_CD = '320';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('320', 'MDB Setting Monetary Format Pref');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Multi Currency Support Pref'
 WHERE DEVICE_SETTING_PARAMETER_CD = '321';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('321', 'MDB Setting Multi Currency Support Pref');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Negative Vend Support Pref'
 WHERE DEVICE_SETTING_PARAMETER_CD = '322';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('322', 'MDB Setting Negative Vend Support Pref');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Data Entry Support Pref'
 WHERE DEVICE_SETTING_PARAMETER_CD = '323';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('323', 'MDB Setting Data Entry Support Pref');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Timeout After Selection'
 WHERE DEVICE_SETTING_PARAMETER_CD = '324';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('324', 'MDB Setting Timeout After Selection');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Timeout Before Selection'
 WHERE DEVICE_SETTING_PARAMETER_CD = '325';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('325', 'MDB Setting Timeout Before Selection');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Session Finalize Timeout After Session End'
 WHERE DEVICE_SETTING_PARAMETER_CD = '326';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('326', 'MDB Setting Session Finalize Timeout After Session End');
END IF;
UPDATE DEVICE.DEVICE_SETTING_PARAMETER SET DEVICE_SETTING_PARAMETER_NAME='MDB Setting Timeout For Vend Wait'
 WHERE DEVICE_SETTING_PARAMETER_CD = '327';
IF SQL%NOTFOUND THEN
    INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_PARAMETER_NAME)
    VALUES ('327', 'MDB Setting Timeout For Vend Wait');
END IF;
END;
