/*
select rule_owner, rule_name, rule_condition  
  from dba_rules
where rule_owner in('REPL_MGR')
AND rule_name like 'TERMINAL%'
and rule_condition like '%''TERMINAL''%'; 
*/   
DECLARE
    CURSOR l_cur IS
    select rule_owner, rule_name, rule_condition  
      from dba_rules
    where rule_owner in('REPL_MGR')
    AND rule_name like 'TERMINAL%'
    and rule_condition like '%''TERMINAL''%';  
BEGIN
    for l_rec in l_cur loop
    DBMS_RULE_ADM.ALTER_RULE(rule_name => l_rec.rule_owner || '.' || l_rec.rule_name, condition => l_rec.rule_condition 
    || ' and (:dml.GET_COMMAND_TYPE() = ''INSERT'' or (:dml.GET_COMMAND_TYPE() = ''UPDATE'' and (
   :dml.get_value(''new'', ''TERMINAL_ENCRYPT_KEY'').AccessVarchar2() != :dml.get_value(''old'', ''TERMINAL_ENCRYPT_KEY'').AccessVarchar2()
or :dml.get_value(''new'', ''TERMINAL_ENCRYPT_KEY2'').AccessVarchar2() != :dml.get_value(''old'', ''TERMINAL_ENCRYPT_KEY2'').AccessVarchar2()
or :dml.get_value(''new'', ''TERMINAL_CD'').AccessVarchar2() != :dml.get_value(''old'', ''TERMINAL_CD'').AccessVarchar2()
or :dml.get_value(''new'', ''MERCHANT_ID'').AccessVarchar2() != :dml.get_value(''old'', ''MERCHANT_ID'').AccessVarchar2()
)))');
end loop;
END;

ALTER TABLE ENGINE.MACHINE_CMD_PENDING ADD SUPPLEMENTAL LOG GROUP MCP_MACHINE_ID (MACHINE_ID) ALWAYS;

GRANT ALL PRIVILEGES TO REPL_MGR; -- Needed for DBMS_STREAMS_ADM.ADD_TABLE_RULES
-- EXECUTE THE FOLLOWING AS REPL_MGR:
-- PSS.POS changes we will ignore as they shouldn't occur with out being accompanied by changes to device or pos_pta
DECLARE
    l_rep_schema VARCHAR2(100) := 'REPL_MGR';
    CURSOR l_cur IS
       SELECT OWNER, TABLE_NAME 
         FROM ALL_TABLES
       WHERE OWNER = 'ENGINE' AND TABLE_NAME IN('MACHINE_CMD_PENDING');
    l_scn NUMBER;
    l_db_name VARCHAR2(100);    
    l_rule VARCHAR2(4000);
    l_tmp VARCHAR2(4000);    
BEGIN
  SELECT CURRENT_SCN, DB_UNIQUE_NAME
    INTO l_scn, l_db_name
    FROM SYS.V_$DATABASE;
    
   FOR l_rec IN l_cur LOOP
    DBMS_CAPTURE_ADM.PREPARE_SCHEMA_INSTANTIATION(schema_name  => l_rec.OWNER);
    
       l_scn := REPL_MGR.REPLICATE_PKG.prepare_table(l_rec.OWNER,l_rec.TABLE_NAME);
       DBMS_OUTPUT.put_line('Prepared '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'; Setting up capture.');
    DBMS_STREAMS_ADM.ADD_TABLE_RULES(
       table_name          => l_rec.OWNER || '.'||l_rec.TABLE_NAME,
       streams_type        => 'capture',
       streams_name        => 'CHANGE_CAPTURE',
       queue_name          => l_rep_schema||'.Q_CHANGE_CAPTURE',
       include_dml         => TRUE,
       include_ddl         => FALSE,
       include_tagged_lcr  => TRUE,
       --source_database     => l_db_name,
       --dml_rule_name       OUT  VARCHAR2,
       --ddl_rule_name       OUT  VARCHAR2,
       inclusion_rule      => TRUE,
       and_condition       => NULL);
     DBMS_OUTPUT.put_line('Capture set up for '||l_rec.OWNER || '.'||l_rec.TABLE_NAME ||'.');   
END LOOP;
END;
/

REVOKE ALL PRIVILEGES FROM REPL_MGR;
