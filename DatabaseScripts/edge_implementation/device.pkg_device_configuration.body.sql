CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION
IS
    
    FUNCTION LOB_TO_HEX_LONG(pl_lob CLOB)
    RETURN LONG
    IS
        ll_tmp LONG := '';
        tot BINARY_INTEGER;
        n PLS_INTEGER;
    BEGIN
        tot := DBMS_LOB.GETLENGTH(pl_lob);
        FOR i IN 1..tot LOOP
            n := ASCII(DBMS_LOB.SUBSTR(pl_lob, 1, i));
            ll_tmp := ll_tmp || TO_CHAR(n, 'FM0X');
        END LOOP;   
        RETURN ll_tmp;
    END;
    
    FUNCTION HEX_LONG_TO_LOB(pl_long LONG)
    RETURN BLOB
    IS
        ll_clob CLOB;
        ll_tmp BLOB;
        tot BINARY_INTEGER;
    BEGIN
        ll_clob := pl_long;
        tot := DBMS_LOB.GETLENGTH(ll_clob) / 2;
        DBMS_LOB.CREATETEMPORARY(ll_tmp, TRUE, DBMS_LOB.CALL);
        FOR i IN 1..tot LOOP
            DBMS_LOB.WRITEAPPEND(ll_tmp, 1, DBMS_LOB.SUBSTR(ll_clob, 2, (i * 2) - 1));
        END LOOP;   
        RETURN ll_tmp;
    END;

    
PROCEDURE SP_GET_FILE_TRANSFER_BLOB
(
    pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
    pbl_file_transfer_content OUT BLOB
)
IS
    lcl_file_transfer_content_hex CLOB;
    ln_file_content_hex_length NUMBER;
    ln_position NUMBER := 1;
    ln_max_size NUMBER := 32766; -- Oracle limitation
    ln_length NUMBER;

BEGIN
    DELETE FROM device.gtmp_file_transfer WHERE file_transfer_id = pn_file_transfer_id;

    INSERT INTO device.gtmp_file_transfer(file_transfer_id, file_transfer_content_hex)
    SELECT file_transfer_id, TO_LOB(file_transfer_content)
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    SELECT file_transfer_content_hex INTO lcl_file_transfer_content_hex
    FROM device.gtmp_file_transfer
    WHERE file_transfer_id = pn_file_transfer_id AND ROWNUM = 1;

    ln_file_content_hex_length := dbms_lob.getlength(lcl_file_transfer_content_hex);
    dbms_lob.createtemporary(pbl_file_transfer_content, TRUE, dbms_lob.call);

    WHILE ln_position <= ln_file_content_hex_length LOOP
        ln_length := LEAST(ln_max_size, ln_file_content_hex_length - ln_position + 1);
        dbms_lob.writeappend(pbl_file_transfer_content, ln_length / 2, dbms_lob.substr(lcl_file_transfer_content_hex, ln_length, ln_position));
        ln_position := ln_position + ln_length;
    END LOOP;

    DELETE FROM device.gtmp_file_transfer WHERE file_transfer_id = pn_file_transfer_id;
END;

    
PROCEDURE SP_UPDATE_DEVICE_SETTINGS
(
    pn_device_id IN device.device_id%TYPE,
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(3000);
    ls_record VARCHAR2(300);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value device_setting.device_setting_value%TYPE;
    ln_return NUMBER := 0;
    ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SELECT file_transfer_type_cd, file_transfer_content
    INTO ln_file_transfer_type_cd, ll_file_transfer_content
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
        DELETE FROM device.device_setting
        WHERE device_id = pn_device_id
            AND device_setting_parameter_cd IN (
                SELECT device_setting_parameter_cd
                FROM device.device_setting_parameter
                WHERE device_setting_ui_configurable = 'Y');
    ELSE
        DELETE FROM device.config_template_setting
        WHERE file_transfer_id = pn_file_transfer_id;
    END IF;
                                        
    LOOP
        ls_content := ls_content || SUBSTR(utl_raw.cast_to_varchar2(HEXTORAW(ll_file_transfer_content)), ln_content_pos, ln_content_size);
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, CHR(10)), 0);
        IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
            ln_return := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, CHR(10)), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            IF ln_return = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
            ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');
            
            IF NVL(LENGTH(ls_param), 0) > 0 THEN
                INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
                SELECT ls_param FROM dual
                WHERE NOT EXISTS (
                    SELECT 1 FROM device.device_setting_parameter
                    WHERE device_setting_parameter_cd = ls_param);

                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
                    VALUES(pn_device_id, ls_param, ls_value);
                ELSE
                    INSERT INTO device.config_template_setting(file_transfer_id, device_setting_parameter_cd, config_template_setting_value)
                    VALUES(pn_file_transfer_id, ls_param, ls_value);
                END IF;
            END IF;

            IF ln_return = 1 THEN
                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.device_setting
                    WHERE device_id = pn_device_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                ELSE
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.config_template_setting
                    WHERE file_transfer_id = pn_file_transfer_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');                
                END IF;

                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := PKG_CONST.ERROR__NO_ERROR;
                RETURN;
            END IF;
        END LOOP;

    END LOOP;    
END;

PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS
(
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
BEGIN
    SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
END;


PROCEDURE SP_GET_HOST_BY_PORT_NUM
(
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_host_id OUT host.host_id%TYPE    
)
IS
BEGIN
    SELECT host_id INTO pn_host_id FROM (
        SELECT host_id
        FROM device.host h
        INNER JOIN device.device d ON h.device_id = d.device_id
        WHERE d.device_name = pv_device_name
            AND h.host_port_num = pn_host_port_num
            AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
        ORDER BY h.created_ts DESC
    ) WHERE ROWNUM = 1;
END;


PROCEDURE SP_GET_HOST
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_id OUT host.host_id%TYPE    
)
IS
    ln_new_host_count NUMBER;
BEGIN
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__FAILURE;
        pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
        
        sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN            
            BEGIN            
                -- fail over to the base host
                sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        sp_create_default_hosts(pn_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                            RETURN;
                        END IF;
                            
                        sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
                            pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
                            RETURN;
                    END;
            END;
    END;
        
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_CREATE_DEFAULT_HOSTS
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_new_host_count                       OUT NUMBER,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
    ln_base_host_count                      NUMBER;
    ln_other_host_count                     NUMBER;
    lv_device_serial_cd                     device.device_serial_cd%TYPE;
    ln_device_type_id                       device.device_type_id%TYPE;
    ln_def_base_host_type_id                device_type.def_base_host_type_id%TYPE;
       ln_def_base_host_equipment_id           device_type.def_base_host_equipment_id%TYPE;
       ln_def_prim_host_type_id                device_type.def_prim_host_type_id%TYPE;
       ln_def_prim_host_equipment_id           device_type.def_prim_host_equipment_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_new_host_count := 0;

    SELECT COUNT(1)
    INTO ln_base_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND dtht.device_type_host_type_cd = 'B';
    
    SELECT COUNT(1)
    INTO ln_other_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND dtht.device_type_host_type_cd <> 'B';
    
    -- if exists both a base and another host, then there's nothing to do here
    IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        RETURN;
    END IF;
    
    SELECT
        d.device_serial_cd,
        d.device_type_id,
        dt.def_base_host_type_id,
        dt.def_base_host_equipment_id,
        dt.def_prim_host_type_id,
        dt.def_prim_host_equipment_id
    INTO
        lv_device_serial_cd,
        ln_device_type_id,
        ln_def_base_host_type_id,
           ln_def_base_host_equipment_id,
           ln_def_prim_host_type_id,
           ln_def_prim_host_equipment_id
    FROM device.device_type dt, device.device d
    WHERE d.device_type_id = dt.device_type_id
       AND d.device_id = pn_device_id;
    
    -- Create base host if it doesn't exist
    IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_serial_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            lv_device_serial_cd,
            0,
            0,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_base_host_type_id,
            ln_def_base_host_equipment_id
        );
        
        pn_new_host_count := pn_new_host_count + 1;
    END IF;
    
    -- Create primary host if no other hosts exist
    IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            0,
            1,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_prim_host_type_id,
            ln_def_prim_host_equipment_id
        );
        
        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    PROCEDURE APPEND(
        l_text IN VARCHAR2,
        l_clob IN OUT NOCOPY CLOB)
    IS
    BEGIN
        IF l_text IS NOT NULL AND LENGTH(l_text) > 0 THEN
           DBMS_LOB.WRITEAPPEND(l_clob, LENGTH(l_text), l_text);
        END IF;
    END;
    
    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB)
    IS
        CURSOR l_cur IS
            SELECT dsp.DEVICE_SETTING_PARAMETER_CD, 
                   DECODE(ds.DEVICE_SETTING_PARAMETER_CD, NULL, cts.CONFIG_TEMPLATE_SETTING_VALUE, ds.DEVICE_SETTING_VALUE) DEVICE_SETTING_VALUE, 
                   DECODE(ds.DEVICE_SETTING_PARAMETER_CD, NULL, 'Y', 'N') IS_DEFAULT, 
                   d.DEVICE_TYPE_ID 
              FROM DEVICE.DEVICE d 
              JOIN DEVICE.FILE_TRANSFER ft 
                ON ft.FILE_TRANSFER_NAME = CASE d.DEVICE_TYPE_ID
						WHEN 0 THEN 'G4-DEFAULT-CFG'
						WHEN 1 THEN 'G5-DEFAULT-CFG'
						WHEN 6 THEN 'MEI-DEFAULT-CFG'
						WHEN 12 THEN 'T2-DEFAULT-CFG'
						ELSE 'DEFAULT-CFG-' || d.DEVICE_TYPE_ID || '-' || (
                           SELECT dsplv.DEVICE_SETTING_VALUE 
                             FROM DEVICE.DEVICE_SETTING dsplv
                            WHERE dsplv.DEVICE_SETTING_PARAMETER_CD = 'Property List Version' --PKG_CONST.DSP__PROPERTY_LIST_VERSION
                              AND dsplv.DEVICE_ID = d.DEVICE_ID)
					END 
                AND ft.FILE_TRANSFER_TYPE_CD IN(6,15,22)
              CROSS JOIN DEVICE.DEVICE_SETTING_PARAMETER dsp
               LEFT JOIN DEVICE.DEVICE_SETTING ds
                 ON d.DEVICE_ID = ds.DEVICE_ID
                AND ds.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
               LEFT JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts ON ft.FILE_TRANSFER_ID = cts.FILE_TRANSFER_ID
                AND cts.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD   
              WHERE d.DEVICE_ID = pn_device_id
                AND (cts.DEVICE_SETTING_PARAMETER_CD IS NOT NULL OR (dsp.DEVICE_SETTING_UI_CONFIGURABLE = 'Y' AND ds.DEVICE_SETTING_PARAMETER_CD IS NOT NULL))
              ORDER BY CASE WHEN REGEXP_LIKE(NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD), '^[0-9]+$') THEN TO_NUMBER(NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD)) ELSE 9999999999 END, 
                    NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD);
    BEGIN
        DBMS_LOB.CREATETEMPORARY(pl_file, TRUE, DBMS_LOB.CALL);
        FOR l_rec IN l_cur LOOP
            -- For now we just implement the property list format for Edge
            APPEND(l_rec.DEVICE_SETTING_PARAMETER_CD, pl_file);
            APPEND('=', pl_file);
            APPEND(l_rec.DEVICE_SETTING_VALUE, pl_file);
            APPEND(CHR(10), pl_file);
        END LOOP;       
    END;
    
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file CLOB)
    IS
    	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		ls_config_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        ln_config_file_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
    BEGIN		
        MERGE INTO DEVICE.FILE_TRANSFER O
    	   USING (
		      SELECT d.DEVICE_NAME || '-CFG' CONFIG_FILE_NAME, 
                     PKG_CONST.FILE_TYPE__CONFIG FILE_TRANSFER_TYPE_CD,
                     pl_config_file FILE_TRANSFER_CONTENT
                FROM DEVICE.DEVICE d
               WHERE d.DEVICE_ID = pn_device_id) N
	          ON (O.FILE_TRANSFER_NAME = N.CONFIG_FILE_NAME AND O.FILE_TRANSFER_TYPE_CD = N.FILE_TRANSFER_TYPE_CD)
	          WHEN MATCHED THEN 
	           UPDATE 
	              SET O.FILE_TRANSFER_CONTENT = N.FILE_TRANSFER_CONTENT
	          WHEN NOT MATCHED THEN
	           INSERT (O.FILE_TRANSFER_NAME,
					   O.FILE_TRANSFER_TYPE_CD,
				       O.FILE_TRANSFER_CONTENT)
	            VALUES(N.CONFIG_FILE_NAME,
				       N.FILE_TRANSFER_TYPE_CD,
				       N.FILE_TRANSFER_CONTENT
	            );    
    END;
    
    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_new_property_list_version NUMBER,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER)
    IS
        ln_old_property_list_version NUMBER;
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        ls_config_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE := pv_device_name || '-CFG';
        pl_config_file CLOB;
        ll_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
        ln_default_file_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
        ls_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
 	BEGIN		
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM (SELECT DEVICE_ID
             FROM DEVICE.DEVICE
            WHERE DEVICE_NAME = pv_device_name
            ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        
        IF ln_device_id IS NOT NULL THEN        
            SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
              INTO ln_old_property_list_version
              FROM DEVICE.DEVICE_SETTING ds
             WHERE ds.DEVICE_ID = ln_device_id
               AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
               AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
            IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(ln_old_property_list_version, -1) THEN
                MERGE INTO DEVICE.DEVICE_SETTING O
            	 USING (
        		      SELECT ln_device_id DEVICE_ID, PKG_CONST.DSP__PROPERTY_LIST_VERSION SETTING_NAME, pn_new_property_list_version SETTING_VALUE
        		        FROM DUAL) N
        	          ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
        	          WHEN MATCHED THEN 
        	           UPDATE 
        	              SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
        	          WHEN NOT MATCHED THEN
        	           INSERT (O.DEVICE_ID,
        					   O.DEVICE_SETTING_PARAMETER_CD,
        				       O.DEVICE_SETTING_VALUE)
        	            VALUES(N.DEVICE_ID,
        				       N.SETTING_NAME,
        				       N.SETTING_VALUE
        	            );    
            END IF;
        END IF;
        
        IF pn_new_device_type_id = 0 THEN
            ls_default_name := 'G4-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 1 THEN
            ls_default_name := 'G5-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 6 THEN
            ls_default_name := 'MEI-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 12 THEN
            ls_default_name := 'T2-DEFAULT-CFG';
            ln_default_file_type_id := 15;
        ELSE
            ls_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_new_property_list_version;
            ln_default_file_type_id := 22;
        END IF;
        
        SELECT MAX(FILE_TRANSFER_ID)
          INTO pn_file_transfer_id
          FROM DEVICE.FILE_TRANSFER 
         WHERE FILE_TRANSFER_NAME = ls_config_name
           AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG;
        
        -- handle change of property list version
        IF ln_device_id IS NOT NULL AND pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(ln_old_property_list_version, -1) THEN
            -- upsert property list version
            MERGE INTO DEVICE.DEVICE_SETTING O
        	 USING (
    		      SELECT ln_device_id DEVICE_ID, PKG_CONST.DSP__PROPERTY_LIST_VERSION SETTING_NAME, TO_CHAR(pn_new_property_list_version) SETTING_VALUE
    		        FROM DUAL) N
    	          ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
    	          WHEN MATCHED THEN 
    	           UPDATE 
    	              SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
    	          WHEN NOT MATCHED THEN
    	           INSERT (O.DEVICE_ID,
    					   O.DEVICE_SETTING_PARAMETER_CD,
    				       O.DEVICE_SETTING_VALUE)
    	            VALUES(N.DEVICE_ID,
    				       N.SETTING_NAME,
    				       N.SETTING_VALUE
    	            );
           --Create config file
           SP_CONSTRUCT_PROPERTIES_FILE(ln_device_id, pl_config_file);
           ll_file_transfer_content := LOB_TO_HEX_LONG(pl_config_file);
        ELSIF ln_device_id IS NULL OR pn_file_transfer_id IS NULL THEN
            -- Use default config file
            SELECT FILE_TRANSFER_CONTENT
              INTO ll_file_transfer_content
              FROM DEVICE.FILE_TRANSFER
	         WHERE FILE_TRANSFER_NAME = ls_default_name
               AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id;
	 	END IF;
 	   	
        IF ll_file_transfer_content IS NOT NULL THEN
            IF pn_file_transfer_id IS NULL THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CONTENT)
     			  VALUES(pn_file_transfer_id, ls_config_name, PKG_CONST.FILE_TYPE__CONFIG, ll_file_transfer_content);
            ELSE
               UPDATE DEVICE.FILE_TRANSFER
    		      SET FILE_TRANSFER_CONTENT = ll_file_transfer_content
    		    WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
    	    END IF;
            pn_updated := PKG_CONST.BOOLEAN__TRUE;
        ELSE
            pn_updated := PKG_CONST.BOOLEAN__FALSE;
        END IF;
    END;
    
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT CLOB)
    IS
        ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		ln_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
		ln_mcp_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
          INTO ln_file_transfer_id, ln_dft_id, ln_mcp_id, ln_device_id
          FROM (SELECT DEVICE_ID
          FROM DEVICE.DEVICE
         WHERE DEVICE_NAME = pv_device_name
         ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        DBMS_LOB.CREATETEMPORARY(pl_file_transfer_content, TRUE, DBMS_LOB.CALL);
        INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CONTENT)
           VALUES(ln_file_transfer_id, pn_file_transfer_type_id, pl_file_transfer_content);
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
     	   VALUES(ln_dft_id, ln_device_id, ln_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
           VALUES(ln_mcp_id, pv_device_name, pc_data_type, TO_CHAR(ln_dft_id), 'S', 999, SYSDATE);
    END;
    
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT BLOB,
		pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
	IS
 		l_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT ft.FILE_TRANSFER_ID, ft.CREATED_TS
          INTO l_file_transfer_id, pd_file_transfer_create_ts
          FROM DEVICE.FILE_TRANSFER ft
         WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
           AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id;
        SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
          INTO l_dft_id
          FROM DEVICE.DEVICE_FILE_TRANSFER dft
          JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
         WHERE d.DEVICE_NAME = pv_device_name
           AND dft.FILE_TRANSFER_ID = l_file_transfer_id
           AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
           AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
       IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
         SELECT MAX(mcp.MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
           INTO pn_mcp_id
           FROM ENGINE.MACHINE_CMD_PENDING mcp
          WHERE mcp.MACHINE_ID = pv_device_name
            AND mcp.EXECUTE_CD IN('P', 'S')
            AND UPPER(mcp.DATA_TYPE) IN('7C', 'A4', 'C7', 'C8')
            AND TRIM(mcp.COMMAND) = TRIM(TO_CHAR(l_dft_id));
        ELSE
        	SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
              INTO l_dft_id, l_device_id
              FROM (SELECT DEVICE_ID
              FROM DEVICE.DEVICE
             WHERE DEVICE_NAME = pv_device_name
             ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
             WHERE ROWNUM = 1;
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
        	  VALUES(l_dft_id, l_device_id, l_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        END IF;
        IF pn_mcp_id IS NULL THEN	
        	SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
        	  INTO pn_mcp_id
        	  FROM DUAL;
        	INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
        	  VALUES(pn_mcp_id, pv_device_name, pc_data_type, TO_CHAR(l_dft_id), 'S', 999, SYSDATE);
        END IF;
        SP_GET_FILE_TRANSFER_BLOB(l_file_transfer_id, pl_file_transfer_content);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
       		NULL; -- let variables be null
    END;
    
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE)
    IS
        ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
    BEGIN
    	UPDATE ENGINE.MACHINE_CMD_PENDING MCP
    	   SET EXECUTE_CD = 'S',
    	       EXECUTE_DATE = SYSDATE
    	 WHERE MACHINE_ID = pv_device_name
    	   AND EXECUTE_CD = 'P'
    	   AND NOT EXISTS(
                        SELECT 1
                          FROM ENGINE.MACHINE_CMD_PENDING MCP2
                         WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
                           AND MCP2.EXECUTE_CD = 'P'
                           AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
                           AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
                               OR MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))
             RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, TRIM(COMMAND) INTO pn_command_id, pv_data_type, ll_command;
        IF UPPER(pv_data_type) IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(ll_command, '^[0-9]+$') THEN
      		SELECT ft.FILE_TRANSFER_ID, ft.FILE_TRANSFER_NAME, ft.FILE_TRANSFER_TYPE_CD,
      		       dft.DEVICE_FILE_TRANSFER_GROUP_NUM, dft.DEVICE_FILE_TRANSFER_PKT_SIZE, dft.CREATED_TS
      		  INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
                   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
      		  FROM DEVICE.DEVICE_FILE_TRANSFER dft
      		  JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
      		 WHERE DEVICE_FILE_TRANSFER_ID = TO_NUMBER(ll_command); 
      		
      		 SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
        ELSE
            pr_command_bytes := HEXTORAW(ll_command);
      	END IF;  	
    END;
    
END;
/
