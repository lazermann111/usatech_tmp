CREATE OR REPLACE PACKAGE DEVICE.PKG_DEVICE_CONFIGURATION
IS
    
    FUNCTION LOB_TO_HEX_LONG(pl_lob CLOB)
    RETURN LONG;
    
    FUNCTION HEX_LONG_TO_LOB(pl_long LONG)
    RETURN BLOB;
    
    PROCEDURE SP_GET_FILE_TRANSFER_BLOB
    (
        pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
        pbl_file_transfer_content OUT BLOB
    );
    
    PROCEDURE SP_UPDATE_DEVICE_SETTINGS(
        pn_device_id IN device.device_id%TYPE,
        pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,
        pn_setting_count OUT NUMBER
    );

    PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS(
        pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,    
        pn_setting_count OUT NUMBER
    );

    PROCEDURE SP_GET_HOST_BY_PORT_NUM(
        pv_device_name IN device.device_name%TYPE,
        pn_host_port_num IN host.host_port_num%TYPE,
        pt_utc_ts IN TIMESTAMP,
        pn_host_id OUT host.host_id%TYPE    
    );

    PROCEDURE SP_GET_HOST(
        pn_device_id IN device.device_id%TYPE,
        pv_device_name IN device.device_name%TYPE,
        pn_host_port_num IN host.host_port_num%TYPE,
        pt_utc_ts IN TIMESTAMP,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,
        pn_host_id OUT host.host_id%TYPE    
    );

    PROCEDURE SP_CREATE_DEFAULT_HOSTS(
        pn_device_id        IN       device.device_id%TYPE,
        pn_new_host_count   OUT      NUMBER,
        pn_result_cd        OUT      NUMBER,
        pv_error_message    OUT      VARCHAR2
    );

    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB);
        
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file CLOB);
        
    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_new_property_list_version NUMBER,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER);
        
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT CLOB);
        
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT BLOB,
		pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE);
        
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE);
END;
/
