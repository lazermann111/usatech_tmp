CREATE TABLE PSS.PAYMENT_TYPE
(
    payment_type_cd                CHAR(1) NOT NULL,
    payment_type_desc              VARCHAR2(60) NOT NULL,
	display_order                  NUMBER NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,    
    CONSTRAINT pk_payment_type PRIMARY KEY(payment_type_cd)
)
TABLESPACE PSS_DATA;

CREATE OR REPLACE TRIGGER PSS.TRBI_PAYMENT_TYPE BEFORE INSERT ON PSS.PAYMENT_TYPE
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_PAYMENT_TYPE BEFORE UPDATE ON PSS.PAYMENT_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON PSS.PAYMENT_TYPE TO USAT_WEB_ROLE;

INSERT INTO PSS.PAYMENT_TYPE(payment_type_cd, payment_type_desc, display_order) VALUES('M', 'Cash', 100);
INSERT INTO PSS.PAYMENT_TYPE(payment_type_cd, payment_type_desc, display_order) VALUES('C', 'Credit', 200);
INSERT INTO PSS.PAYMENT_TYPE(payment_type_cd, payment_type_desc, display_order) VALUES('S', 'Special', 300);
COMMIT;


CREATE TABLE PSS.PAYMENT_ENTRY_METHOD
(
    payment_entry_method_cd        CHAR(1) NOT NULL,
    payment_entry_method_desc      VARCHAR2(60) NOT NULL,
	display_order                  NUMBER NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,    
    CONSTRAINT pk_payment_entry_method PRIMARY KEY(payment_entry_method_cd)
)
TABLESPACE PSS_DATA;

CREATE OR REPLACE TRIGGER PSS.TRBI_PAYMENT_ENTRY_METHOD BEFORE INSERT ON PSS.PAYMENT_ENTRY_METHOD
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_PAYMENT_ENTRY_METHOD BEFORE UPDATE ON PSS.PAYMENT_ENTRY_METHOD
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON PSS.PAYMENT_ENTRY_METHOD TO USAT_WEB_ROLE;

INSERT INTO PSS.PAYMENT_ENTRY_METHOD(payment_entry_method_cd, payment_entry_method_desc, display_order) VALUES('M', 'Cash', 100);
INSERT INTO PSS.PAYMENT_ENTRY_METHOD(payment_entry_method_cd, payment_entry_method_desc, display_order) VALUES('S', 'Swiped', 200);
INSERT INTO PSS.PAYMENT_ENTRY_METHOD(payment_entry_method_cd, payment_entry_method_desc, display_order) VALUES('C', 'Contactless', 300);
COMMIT;


ALTER TABLE PSS.CLIENT_PAYMENT_TYPE ADD (
	payment_type_cd                CHAR(1),
	payment_entry_method_cd        CHAR(1)
);


UPDATE PSS.CLIENT_PAYMENT_TYPE SET payment_type_cd = 'M', payment_entry_method_cd = 'M' WHERE client_payment_type_cd = 'M';
UPDATE PSS.CLIENT_PAYMENT_TYPE SET payment_type_cd = 'C', payment_entry_method_cd = 'S' WHERE client_payment_type_cd = 'C';
UPDATE PSS.CLIENT_PAYMENT_TYPE SET payment_type_cd = 'C', payment_entry_method_cd = 'C' WHERE client_payment_type_cd = 'R';
UPDATE PSS.CLIENT_PAYMENT_TYPE SET payment_type_cd = 'S', payment_entry_method_cd = 'S' WHERE client_payment_type_cd = 'S';
UPDATE PSS.CLIENT_PAYMENT_TYPE SET payment_type_cd = 'S', payment_entry_method_cd = 'C' WHERE client_payment_type_cd = 'P';
COMMIT;


ALTER TABLE PSS.CLIENT_PAYMENT_TYPE MODIFY (
	payment_type_cd                CHAR(1) NOT NULL,
	payment_entry_method_cd        CHAR(1) NOT NULL
);

ALTER TABLE PSS.CLIENT_PAYMENT_TYPE ADD (	
	CONSTRAINT FK_CPT_PAYMENT_TYPE_CD FOREIGN KEY(payment_type_cd) REFERENCES PSS.PAYMENT_TYPE(payment_type_cd),
	CONSTRAINT FK_CPT_PAYMENT_ENTRY_METHOD_CD FOREIGN KEY(payment_entry_method_cd) REFERENCES PSS.PAYMENT_ENTRY_METHOD(payment_entry_method_cd)
);	
