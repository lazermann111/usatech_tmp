CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_EVENT
IS

FUNCTION SF_GET_GLOBAL_EVENT_CD
(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN event.event_device_tran_cd%TYPE
) RETURN VARCHAR2
IS
BEGIN
    RETURN pc_global_event_cd_prefix || ':' || pv_device_name || ':' || pv_device_tran_cd;
END;

PROCEDURE SP_GET_OR_CREATE_EVENT
(
    pc_global_event_cd_prefix IN CHAR,
    pn_session_id IN NUMBER,
    pv_device_name IN device.device_name%TYPE,
    pn_host_id IN host.host_id%TYPE,
    pn_host_type_id IN host_type.host_type_id%TYPE,
    pn_event_type_id IN event_type.event_type_id%TYPE,
    pn_event_type_host_type_num IN event_type_host_type.event_type_host_type_num%TYPE,
    pn_event_state_id IN event.event_state_id%TYPE,
    pv_event_device_tran_cd IN event.event_device_tran_cd%TYPE,
    pn_event_end_flag IN NUMBER,
    pn_event_upload_complete_flag IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_id	OUT event.event_id%TYPE,
    pn_exists OUT NUMBER,
    pd_event_start_ts IN event.event_start_ts%TYPE DEFAULT SYSDATE,
    pd_event_end_ts IN event.event_end_ts%TYPE DEFAULT SYSDATE    
)
IS
    ln_event_type_id event_type.event_type_id%TYPE;
    lv_event_global_trans_cd event.event_global_trans_cd%TYPE;
    ln_db_event_state_id event.event_state_id%TYPE;
    ld_db_event_end_ts event.event_end_ts%TYPE;
    ld_db_event_upload_complete_ts event.event_upload_complete_ts%TYPE;
    ln_update_event_flag NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_update_event_state_flag NUMBER := PKG_CONST.BOOLEAN__FALSE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;

    IF pn_event_type_id > -1 THEN
        ln_event_type_id := pn_event_type_id;
    ELSE
        BEGIN
            SELECT event_type_id INTO ln_event_type_id
            FROM device.event_type_host_type
        	WHERE host_type_id = pn_host_type_id
    	      AND event_type_host_type_num = pn_event_type_host_type_num;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pv_error_message := 'Unable to create event. No event_type_id exists for host_type_id: ' || pn_host_type_id || ', event_type_host_type_num: ' || pn_event_type_host_type_num || ' in device.event_type_host_type.';
                RETURN;
        END;
    END IF;
    
    IF pv_event_device_tran_cd IS NOT NULL THEN
        lv_event_global_trans_cd := SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_event_device_tran_cd);
        
        BEGIN
            SELECT event_id, event_state_id, event_end_ts, event_upload_complete_ts
            INTO pn_event_id, ln_db_event_state_id, ld_db_event_end_ts, ld_db_event_upload_complete_ts
            FROM device.event
    		WHERE event_global_trans_cd = lv_event_global_trans_cd;
	
           	pn_exists := PKG_CONST.BOOLEAN__TRUE;
           	
            IF ln_db_event_state_id != pn_event_state_id
                AND ln_db_event_state_id NOT IN (PKG_CONST.EVENT_STATE__COMPLETE_FINAL,
                    PKG_CONST.EVENT_STATE__COMPLETE_ERROR) THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
				ln_update_event_state_flag := PKG_CONST.BOOLEAN__TRUE;
			ELSIF pn_event_end_flag = PKG_CONST.BOOLEAN__TRUE
                AND ld_db_event_end_ts IS NULL THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
			ELSIF pn_event_upload_complete_flag = PKG_CONST.BOOLEAN__TRUE
                AND ld_db_event_upload_complete_ts IS NULL THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
			END IF;
    	
    	    IF ln_update_event_flag = PKG_CONST.BOOLEAN__TRUE THEN
        		UPDATE device.event
        		SET event_state_id = CASE ln_update_event_state_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN pn_event_state_id
                        ELSE event_state_id END,
        			event_end_ts = CASE pn_event_end_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN pd_event_end_ts
                        ELSE event_end_ts END,
        			event_upload_complete_ts = CASE pn_event_upload_complete_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN SYSDATE
                        ELSE event_upload_complete_ts END
        		WHERE event_id = pn_event_id;
    	    END IF;
    	
            pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    	    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    	    RETURN;
	    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
	    END;
    END IF;
    
    SELECT seq_event_id.NEXTVAL
    INTO pn_event_id FROM DUAL;
    
	INSERT INTO device.event (
		event_id,
		event_type_id,
		event_state_id,
		event_device_tran_cd,
		event_global_trans_cd,
		host_id,
		event_start_ts,
		event_end_ts,
		event_upload_complete_ts				
	) VALUES (
		pn_event_id,
        ln_event_type_id,
        pn_event_state_id,
        pv_event_device_tran_cd,
        lv_event_global_trans_cd,
        pn_host_id,
        pd_event_start_ts,
		CASE pn_event_end_flag
            WHEN PKG_CONST.BOOLEAN__TRUE THEN pd_event_end_ts
            ELSE NULL END,
		CASE pn_event_upload_complete_flag
            WHEN PKG_CONST.BOOLEAN__TRUE THEN SYSDATE
            ELSE NULL END
	);
	
	INSERT INTO device.event_nl_device_session (event_id, session_id)
    VALUES (pn_event_id, pn_session_id);
    
   	pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_EVENT_DETAIL
(
    pn_event_id IN event.event_id%TYPE,
    pn_event_detail_type_id IN event_detail.event_detail_type_id%TYPE,
    pv_event_detail_value IN event_detail.event_detail_value%TYPE,
    pv_event_detail_value_ts_ms IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_detail_id	OUT event_detail.event_detail_id%TYPE,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;
    
    BEGIN
        SELECT event_detail_id, PKG_CONST.BOOLEAN__TRUE
        INTO pn_event_detail_id, pn_exists
        FROM
        (
            SELECT event_detail_id
            FROM device.event_detail
            WHERE event_id = pn_event_id
                AND event_detail_type_id = pn_event_detail_type_id
            ORDER BY event_detail_id DESC
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT seq_event_detail_id.NEXTVAL
            INTO pn_event_detail_id FROM DUAL;

        	INSERT INTO device.event_detail (
        	    event_detail_id,
        		event_id,
        		event_detail_type_id,
        		event_detail_value,
        		event_detail_value_ts
        	) VALUES (
        	    pn_event_detail_id,
        		pn_event_id,
                pn_event_detail_type_id,
                pv_event_detail_value,
                CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pv_event_detail_value_ts_ms) AS DATE)
        	);
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_STTLMT_EVENT
(
    pn_event_id IN event.event_id%TYPE,
    pn_device_batch_id IN settlement_event.device_batch_id%TYPE,
    pn_device_new_batch_id IN settlement_event.device_new_batch_id%TYPE,
    pn_device_event_utc_ts_ms IN NUMBER,
    pn_device_event_utc_offset_min IN settlement_event.device_event_utc_offset_min%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;
    
    BEGIN
        SELECT PKG_CONST.BOOLEAN__TRUE
        INTO pn_exists
        FROM device.settlement_event
        WHERE event_id = pn_event_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO device.settlement_event (
                event_id,
                device_batch_id,
                device_new_batch_id,
                device_event_utc_ts,
                device_event_utc_offset_min
            ) VALUES (
                pn_event_id,
                pn_device_batch_id,
                pn_device_new_batch_id,
                CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_device_event_utc_ts_ms) AS DATE),
                pn_device_event_utc_offset_min
            );
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_HOST_COUNTER
(
    pn_host_id IN host.host_id%TYPE,
    pn_event_id IN event.event_id%TYPE,
    pv_host_counter_parameter IN host_counter.host_counter_parameter%TYPE,
    pv_host_counter_value IN host_counter.host_counter_value%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_counter_id OUT host_counter.host_counter_id%TYPE,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;

    BEGIN
        SELECT hc.host_counter_id, PKG_CONST.BOOLEAN__TRUE
        INTO pn_host_counter_id, pn_exists
		FROM device.host_counter_event hce
		JOIN device.host_counter hc
		  ON hce.host_counter_id = hc.host_counter_id
		WHERE hce.event_id = pn_event_id
		  AND hc.host_counter_parameter = pv_host_counter_parameter;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT seq_host_counter_id.NEXTVAL
            INTO pn_host_counter_id FROM DUAL;
            
            INSERT INTO device.host_counter (
                host_counter_id,
                host_id,
                host_counter_parameter,
                host_counter_value
            ) VALUES (
                pn_host_counter_id,
                pn_host_id,
                pv_host_counter_parameter,
                pv_host_counter_value
            );
            
            INSERT INTO device.host_counter_event(event_id, host_counter_id)
            VALUES(pn_event_id, pn_host_counter_id);
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

END;
/
