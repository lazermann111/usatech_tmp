CREATE OR REPLACE 
PROCEDURE device.sp_update_device_settings
(
    ln_device_id IN device.device_id%TYPE,
    ln_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    ln_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(3000);
    ls_record VARCHAR2(300);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value device_setting.device_setting_value%TYPE;
    ln_exit NUMBER := 0;
    ln_return NUMBER := 0;
BEGIN
    DELETE FROM device.device_setting
    WHERE device_id = ln_device_id
        AND device_setting_parameter_cd IN (
            SELECT device_setting_parameter_cd
            FROM device.device_setting_parameter
            WHERE device_setting_parameter_ubn IS NULL);

    SELECT file_transfer_content INTO ll_file_transfer_content
	FROM device.file_transfer
	WHERE file_transfer_id = ln_file_transfer_id;
    				    				
    LOOP
        ls_content := ls_content || SUBSTR(utl_raw.cast_to_varchar2(HEXTORAW(ll_file_transfer_content)), ln_content_pos, ln_content_size);
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, CHR(10)), 0);
        if ln_pos = 0 THEN
            ln_exit := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, CHR(10)), 0);
            IF ln_pos = 0 THEN
                IF ln_exit = 1 THEN
                    ln_return := 1;
                ELSE
                    EXIT;
                END IF;
            END IF;

            IF ln_exit = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 THEN
                EXIT;
            END IF;

            ls_param := SUBSTR(ls_record, 1, ln_pos - 1);
            ls_value := SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos);
            
            INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
            SELECT ls_param FROM dual
            WHERE NOT EXISTS (
                SELECT 1 FROM device.device_setting_parameter
                WHERE device_setting_parameter_cd = ls_param);
                
            INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
            VALUES(ln_device_id, ls_param, ls_value);

            IF ln_return = 1 THEN
                SELECT COUNT(1)
                INTO ln_setting_count
                FROM device.device_setting
                WHERE device_id = ln_device_id
                    AND device_setting_parameter_cd IN (
                        SELECT device_setting_parameter_cd
                        FROM device.device_setting_parameter
                        WHERE device_setting_parameter_ubn IS NULL);            
            
                RETURN;
            END IF;
        END LOOP;

    END LOOP;
END;
/

GRANT EXECUTE ON device.sp_update_device_settings TO USAT_WEB_ROLE;