WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/edge_implementation/R29.USADBP.1.sql?rev=HEAD
BEGIN
  EXECUTE IMMEDIATE('ALTER TABLE PSS.POS_PTA ADD (POS_PTA_DISABLE_DEBIT_DENIAL VARCHAR2(1))');
EXCEPTION
  WHEN OTHERS THEN
    NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE('ALTER TABLE PSS.POS_PTA_TMPL_ENTRY ADD (POS_PTA_DISABLE_DEBIT_DENIAL VARCHAR2(1))');
EXCEPTION
  WHEN OTHERS THEN
    NULL;
END;
/

INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS(APPS_USERNAME, HOST_NAME, HOST_IP)
SELECT DISTINCT H.APPS_USERNAME, X.NEW_HOST_NAME, X.NEW_HOST_IP
FROM DBADMIN.USAT_KNOWN_APPS_HOSTS H
JOIN (SELECT '' ORIG_HOST_NAME, '' ORIG_HOST_IP, '' NEW_HOST_NAME, '' NEW_HOST_IP FROM DUAL WHERE 1=0
UNION ALL SELECT 'eccapr01', '192.168.4.41', 'eccapr11', '192.168.4.43' FROM DUAL
UNION ALL SELECT 'eccapr01', '192.168.4.41', 'eccapr12', '192.168.4.44' FROM DUAL
UNION ALL SELECT 'usaapr21', '192.168.77.71', 'usaapr31', '192.168.77.76' FROM DUAL
UNION ALL SELECT 'usaapr22', '192.168.77.72', 'usaapr32', '192.168.77.77' FROM DUAL
UNION ALL SELECT 'usaapr23', '192.168.77.73', 'usaapr33', '192.168.77.78' FROM DUAL
UNION ALL SELECT 'usaapr24', '192.168.77.74', 'usaapr34', '192.168.77.79' FROM DUAL
) x ON H.HOST_NAME = X.ORIG_HOST_NAME AND H.HOST_IP = X.ORIG_HOST_IP
WHERE ((SUBSTR(APPS_USERNAME, 1, LENGTH(APPS_USERNAME) - 2) IN('APP_LAYER', 'POSM_LAYER', 'IN_AUTH_LAYER')
AND SUBSTR(APPS_USERNAME, LENGTH(APPS_USERNAME) - 1, 2) IN('_1', '_2', '_3', '_4'))
OR APPS_USERNAME = 'POSM_ADMIN_USER')
AND SUBSTR(HOST_NAME, 4, 3) = 'apr'
AND NOT EXISTS(SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS H0 
    WHERE H0.APPS_USERNAME = H.APPS_USERNAME 
      AND H0.HOST_NAME = X.NEW_HOST_NAME
      AND H0.HOST_IP = X.NEW_HOST_IP);
      
COMMIT;

UPDATE ENGINE.APP_SETTING
   SET APP_SETTING_VALUE = 4
 WHERE APP_SETTING_CD = 'APP_LAYER_COUNT'
   AND APP_SETTING_VALUE = 5;

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
  VALUES('DEVICE_MODEM_INFO_REFRESH_HR', TO_CHAR(7 * 24), 'The number of hours after which the server will re-request modem information from a G4, Gx or MEI device');

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE,APP_SETTING_DESC)
  VALUES('DEVICE_CONFIG_REFRESH_HR', TO_CHAR(30 * 24), 'The number of hours after which the server will re-request the full config file from a G4, Gx or MEI device');

COMMIT;

GRANT SELECT ON DEVICE.SETTING_PARAMETER_TYPE TO USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE ON DEVICE.SETTING_PARAMETER_TYPE TO USATECH_UPD_TRANS;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/Add_CDMA_Comm_Method.sql?rev=1.2
ALTER TABLE DEVICE.COMM_METHOD ADD (IP_MATCH_ORDER NUMBER(3), IP_MATCH_REGEX VARCHAR2(4000));

GRANT SELECT ON DEVICE.COMM_METHOD TO ENGINE;

UPDATE DEVICE.COMM_METHOD
   SET IP_MATCH_ORDER = 40,
       IP_MATCH_REGEX = '.*'
 WHERE COMM_METHOD_CD = 'E';
 
UPDATE DEVICE.COMM_METHOD
   SET IP_MATCH_ORDER = 30,
       IP_MATCH_REGEX = DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POTS_IP_ADDRESS_REGEX')
 WHERE COMM_METHOD_CD = 'P';
 
UPDATE DEVICE.COMM_METHOD
   SET IP_MATCH_ORDER = 10,
       IP_MATCH_REGEX = '^(172|166|32|74.198)\.' --DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GPRS_IP_ADDRESS_REGEX')
 WHERE COMM_METHOD_CD = 'G';
 
INSERT INTO DEVICE.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX)
    VALUES('C', 'CDMA', 20, '^10\.22[7-9]\.');
    
COMMIT;

ALTER TABLE DEVICE.COMM_METHOD MODIFY (IP_MATCH_ORDER NOT NULL, IP_MATCH_REGEX NOT NULL);

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/Add_Counter_Reset_Table.sql?rev=1.1
CREATE TABLE DEVICE.COUNTER_RESET(
    COUNTER_RESET_CD CHAR(1),
    COUNTER_RESET_DESC VARCHAR2(100) NOT NULL,
    CONSTRAINT PK_COUNTER_RESET  PRIMARY KEY(COUNTER_RESET_CD)) TABLESPACE DEVICE_DATA;

INSERT INTO DEVICE.COUNTER_RESET(COUNTER_RESET_CD, COUNTER_RESET_DESC)
    VALUES('N', 'No counters reset');
    
INSERT INTO DEVICE.COUNTER_RESET(COUNTER_RESET_CD, COUNTER_RESET_DESC)
    VALUES('A', 'All counters reset');

INSERT INTO DEVICE.COUNTER_RESET(COUNTER_RESET_CD, COUNTER_RESET_DESC)
    VALUES('Y', 'Cash and Cashless counters reset');
      
INSERT INTO DEVICE.COUNTER_RESET(COUNTER_RESET_CD, COUNTER_RESET_DESC)
    VALUES('C', 'Cashless counters reset');
    
COMMIT;

INSERT INTO DEVICE.EVENT_DETAIL_TYPE(EVENT_DETAIL_TYPE_ID, EVENT_DETAIL_TYPE_NAME, EVENT_DETAIL_TYPE_DESC)
    VALUES(5, 'Entry Method', 'The entry method of the authorization for this event. See PSS.ACCT_ENTRY_METHOD.ACCT_ENTRY_METHOD_DESC for possible values');
    
COMMIT;
    
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/add_config_received_ts_device_column.sql?rev=HEAD
-- This migration plan step will add a new device column that represents the last time the device sent configuration data to us
-- We use this to know when configuration data for a device is stale and needs to be requested again

alter table device.device add ( received_config_file_ts date null ) ;

COMMENT ON COLUMN device.received_config_file_ts IS 'Timestamp of when the last configuration file was received from the device by the system.';

UPDATE DEVICE D SET RECEIVED_CONFIG_FILE_TS = (SELECT MAX(FT.LAST_UPDATED_TS) FROM DEVICE.FILE_TRANSFER FT WHERE FT.FILE_TRANSFER_NAME = D.DEVICE_NAME || '-CFG' ) WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y' AND d.DEVICE_TYPE_ID IN (0,1,6);

commit;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/grant_app_layer_dms_config_access.sql?rev=1.1
	grant select on DEVICE.CONFIG_TEMPLATE_SETTING to USAT_APP_LAYER_ROLE;
	grant select on DEVICE.CONFIG_TEMPLATE_REGEX to USAT_APP_LAYER_ROLE;
	grant select on DEVICE.CONFIG_TEMPLATE_CATEGORY to USAT_APP_LAYER_ROLE;
	grant select on DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP to USAT_APP_LAYER_ROLE;
	grant select on DEVICE.CONFIG_TEMPLATE_SETTING to USAT_APP_LAYER_ROLE;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/add_esuds_diagnostics_grants.sql?rev=1.1

grant select on device.host_diag_status to USAT_POSM_ROLE, USAT_DEV_READ_ONLY;
grant select on device.host to USAT_POSM_ROLE, USAT_DEV_READ_ONLY;
grant select on device.device to USAT_POSM_ROLE, USAT_DEV_READ_ONLY;
grant select on pss.pos to USAT_POSM_ROLE, USAT_DEV_READ_ONLY;
grant select on location.location to USAT_POSM_ROLE, USAT_DEV_READ_ONLY;
grant select on device.host_type to USAT_POSM_ROLE, USAT_DEV_READ_ONLY;


 



-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/Add_Mail_Forward_Lock.sql?rev=1.1
INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_DESC) VALUES ('MAIL_FORWARD_LOCK', 'Record for locking Mail Forward in an AppLayer instance');
INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC) VALUES ('MAIL_FORWARD_MAX_DURATION_SEC', '300', 'Number of seconds to allow the MAIL FORWARD code in App Layer to stay locked');
COMMIT;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R29/add_host_diag_status_fk_index.sql?rev=HEAD
-- Required by PKG_DEVICE_CONFIGURATION 1.67 FOR SP_NPC_HELPER_ESUDS_DIAG
CREATE INDEX DEVICE.IDX_HOST_DIAG_HOST ON DEVICE.HOST_DIAG_STATUS(HOST_ID) TABLESPACE DEVICE_INDX ONLINE;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DBADMIN/PKG_GLOBAL.pbk?rev=1.8
CREATE OR REPLACE PACKAGE BODY DBADMIN.PKG_GLOBAL IS
    PROCEDURE CREATE_LOCK(
        pv_object_type VARCHAR2,
        pv_object_id   VARCHAR2,
        pt_last_lock_utc_ts TIMESTAMP)
    AS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO DBADMIN.APP_LOCK(
            APP_LOCK_TYPE,
            APP_LOCK_INSTANCE,
            LAST_LOCK_UTC_TS,
            LAST_LOCK_BY
        ) VALUES(
            pv_object_type,
            pv_object_id,
            pt_last_lock_utc_ts,
            USER
        ); 
        COMMIT;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            NULL;
    END;
   
    FUNCTION REQUEST_LOCK(
        l_object_type VARCHAR2,
        l_object_id   VARCHAR2)
     RETURN VARCHAR2
    IS
        lt_last_lock_utc_ts TIMESTAMP(6);
    BEGIN
        lt_last_lock_utc_ts := SYS_EXTRACT_UTC(SYSTIMESTAMP);
        CREATE_LOCK(l_object_type, l_object_id, lt_last_lock_utc_ts);
        UPDATE DBADMIN.APP_LOCK 
           SET LAST_LOCK_UTC_TS = lt_last_lock_utc_ts,
               LAST_LOCK_BY = USER
         WHERE APP_LOCK_TYPE = l_object_type
           AND APP_LOCK_INSTANCE = l_object_id;
        IF SQL%ROWCOUNT < 1 THEN
             RAISE_APPLICATION_ERROR(-20111, 'AppLock Row for object ' || l_object_type || '.' || l_object_id || ' was deleted while using it');
        END IF;
        RETURN TO_CHAR(lt_last_lock_utc_ts);
    END;
    
    PROCEDURE RELEASE_LOCK(
        l_handle VARCHAR2)
    IS
    BEGIN
        -- this procedure is provided for backwards compatibility only
        NULL;
    END;
	
	FUNCTION GET_APP_SETTING(
		pv_app_setting_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE
	) RETURN ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE
	IS
		lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
	BEGIN
		SELECT APP_SETTING_VALUE
		INTO lv_app_setting_value
		FROM ENGINE.APP_SETTING
		WHERE APP_SETTING_CD = pv_app_setting_cd;
		
		RETURN lv_app_setting_value;
	END;
	
    PROCEDURE LOCK_PROCESS(
        pv_process_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_process_token_cd ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_token_cd OUT ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE)
    IS
		lv_app_setting_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE := pv_process_cd || '_LOCK';
		ln_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_process_cd || '_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts ENGINE.APP_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
		pv_success_flag := 'N';
	
		UPDATE ENGINE.APP_SETTING
		SET APP_SETTING_VALUE = pv_process_token_cd
		WHERE APP_SETTING_CD = lv_app_setting_cd
			AND (APP_SETTING_VALUE IS NULL
				OR APP_SETTING_VALUE = pv_process_token_cd)
		RETURNING 'Y', APP_SETTING_VALUE INTO pv_success_flag, pv_locked_by_process_token_cd;
			
		IF pv_success_flag = 'Y' THEN
			RETURN;
		END IF;
	
        SELECT APP_SETTING_VALUE, LAST_UPDATED_UTC_TS
        INTO pv_locked_by_process_token_cd, lt_last_updated_utc_ts
        FROM ENGINE.APP_SETTING
        WHERE APP_SETTING_CD = lv_app_setting_cd;
			
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_lock_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				pv_process_cd || ' has been locked by ' || pv_locked_by_process_token_cd || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'DBADMIN.PKG_GLOBAL.LOCK_PROCESS'
			);
			
			UPDATE ENGINE.APP_SETTING
			SET APP_SETTING_VALUE = pv_process_token_cd
			WHERE APP_SETTING_CD = lv_app_setting_cd
			RETURNING 'Y', APP_SETTING_VALUE INTO pv_success_flag, pv_locked_by_process_token_cd;
		END IF;
    END;
    
    PROCEDURE UNLOCK_PROCESS(
        pv_process_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_process_token_cd ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE)
    IS
    BEGIN
        UPDATE ENGINE.APP_SETTING
		SET APP_SETTING_VALUE = NULL
		WHERE APP_SETTING_CD = pv_process_cd || '_LOCK'
			AND APP_SETTING_VALUE = pv_process_token_cd;
    END;
END; 
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/APP_LAYER/VW_DEVICE_INFO.vws?rev=1.29
CREATE OR REPLACE VIEW APP_LAYER.VW_DEVICE_INFO (DEVICE_NAME, DEVICE_SERIAL_CD, ENCRYPTION_KEY, PREVIOUS_ENCRYPTION_KEY, DEVICE_TYPE_ID, ACTIVATION_STATUS_CD, TIME_ZONE_CD, TIME_ZONE_GUID, MASTER_ID, REJECT_UNTIL_TS, REJECT_UNTIL_TS_TIMESTAMP, LOCALE, INIT_ONLY, ACTION_CD, PROPS_TO_REQUEST, ENCRYPTION_KEY_TIMESTAMP, PREV_ENCR_KEY_TIMESTAMP, ACT_STATUS_TIMESTAMP, TIME_ZONE_TIMESTAMP, MASTER_ID_TIMESTAMP, ACTION_CD_TIMESTAMP, PROPS_TO_REQUEST_TIMESTAMP, LAST_ACTIVITY_TS) AS 
 SELECT D.DEVICE_NAME,
        D.DEVICE_SERIAL_CD,
        UTL_RAW.CAST_TO_RAW(D.ENCRYPTION_KEY) ENCRYPTION_KEY,
        D.PREVIOUS_ENCRYPTION_KEY,
        D.DEVICE_TYPE_ID,
        DECODE(D.DEVICE_TYPE_ID, 13, NVL(TO_NUMBER_OR_NULL(DS_A.DEVICE_SETTING_VALUE), 0), 3) ACTIVATION_STATUS_CD,
        NVL(L.LOCATION_TIME_ZONE_CD, 'EST') TIME_ZONE_CD,
        NVL(tz.TIME_ZONE_GUID, 'US/Eastern') TIME_ZONE_GUID,
        NVL(TO_NUMBER_OR_NULL(DS_MT.DEVICE_SETTING_VALUE), 0) MASTER_ID,
        NVL(DATE_TO_MILLIS(D.REJECT_UNTIL_TS), 0) REJECT_UNTIL_TS,
		NVL(DATE_TO_MILLIS(D.REJECT_UNTIL_UPDATED_TS), 0) REJECT_UNTIL_TS_TIMESTAMP,
        'en_US' LOCALE,
        'N' INIT_ONLY,
        DECODE(NVL(TO_NUMBER_OR_NULL(DS_A.DEVICE_SETTING_VALUE), 0), 2, 2, 3, DECODE(D.CMD_PENDING_CNT, 0, 0, 1), 0) ACTION_CD,
        NVL(REGEXP_REPLACE(NVL(DECODE(DBADMIN.PKG_UTL.COMPARE(SYSDATE - 1, NVL(MIN(DS_300.LAST_UPDATED_TS), MIN_DATE)), 1, DECODE(MAX(H.HOST_TYPE_ID), 202, '300|301|', 204, '300|301|')), '') || DS_P.DEVICE_SETTING_VALUE, '[|]$', ''), '99999') PROPS_TO_REQUEST,
        NVL(DATE_TO_MILLIS(D.DEVICE_ENCR_KEY_GEN_TS), 0) ENCRYPTION_KEY_TIMESTAMP,
        NVL(DATE_TO_MILLIS(D.DEVICE_ENCR_KEY_GEN_TS), 0) PREV_ENCR_KEY_TIMESTAMP,
        NVL(DATE_TO_MILLIS(DS_A.LAST_UPDATED_TS), 0) ACT_STATUS_TIMESTAMP,
        GREATEST(NVL(DATE_TO_MILLIS(P.LAST_UPDATED_TS), 0), NVL(DATE_TO_MILLIS(L.LAST_UPDATED_TS), 0)) TIME_ZONE_TIMESTAMP,
        NVL(DATE_TO_MILLIS(DS_MT.LAST_UPDATED_TS), 0) MASTER_ID_TIMESTAMP,
        DECODE(TO_NUMBER_OR_NULL(DS_A.DEVICE_SETTING_VALUE), 3, GREATEST(NVL(DATE_TO_MILLIS(DS_A.LAST_UPDATED_TS), 0), NVL(DATE_TO_MILLIS(D.CMD_PENDING_UPDATED_TS), 0)), NVL(DATE_TO_MILLIS(DS_A.LAST_UPDATED_TS), 0)) ACTION_CD_TIMESTAMP,
        GREATEST(DECODE(MAX(DS_300.LAST_UPDATED_TS), NULL, 0, DATE_TO_MILLIS(MAX(DS_300.LAST_UPDATED_TS) + DECODE(DBADMIN.PKG_UTL.COMPARE(SYSDATE - 1, MAX(DS_300.LAST_UPDATED_TS)), 1, 1, 0))), NVL(DATE_TO_MILLIS(MAX(H.CREATED_TS)), NVL(DATE_TO_MILLIS(D.CREATED_TS), 0)), NVL(DATE_TO_MILLIS(DS_P.LAST_UPDATED_TS), 0)) PROPS_TO_REQUEST_TIMESTAMP,
		D.LAST_ACTIVITY_TS
   FROM DEVICE.DEVICE D
   LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_TYPE_ID IN(202,204)
   LEFT OUTER JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
   LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
   LEFT OUTER JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_A 
     ON D.DEVICE_ID = DS_A.DEVICE_ID
    AND DS_A.DEVICE_SETTING_PARAMETER_CD = '70'
    AND REGEXP_LIKE (DS_A.DEVICE_SETTING_VALUE, '^[0-9]+$')
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MT
     ON D.DEVICE_ID = DS_MT.DEVICE_ID
    AND DS_MT.DEVICE_SETTING_PARAMETER_CD = '60'
    AND REGEXP_LIKE (DS_MT.DEVICE_SETTING_VALUE, '^[0-9]+$')
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_P
     ON D.DEVICE_ID = DS_P.DEVICE_ID
    AND DS_P.DEVICE_SETTING_PARAMETER_CD = 'PROPS_TO_REQUEST'
    AND REGEXP_LIKE (DS_P.DEVICE_SETTING_VALUE, '^([0-9]+)([|-][0-9]+)*$')
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_300 
     ON D.DEVICE_ID = DS_300.DEVICE_ID
    AND DS_300.DEVICE_SETTING_PARAMETER_CD = '300'
    AND REGEXP_LIKE (DS_300.DEVICE_SETTING_VALUE, '^[0-9]+$')   
  WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
  GROUP BY 
        D.DEVICE_NAME,
        D.DEVICE_SERIAL_CD,
        D.ENCRYPTION_KEY,
        D.PREVIOUS_ENCRYPTION_KEY,
        D.DEVICE_TYPE_ID,
        DS_A.DEVICE_SETTING_VALUE,
        L.LOCATION_TIME_ZONE_CD,
        tz.TIME_ZONE_GUID,
        DS_MT.DEVICE_SETTING_VALUE,
        DS_P.DEVICE_SETTING_VALUE,
        D.DEVICE_ENCR_KEY_GEN_TS,
        DS_A.LAST_UPDATED_TS,
        P.LAST_UPDATED_TS,
        L.LAST_UPDATED_TS,
        DS_MT.LAST_UPDATED_TS,
        D.CREATED_TS,
        DS_P.LAST_UPDATED_TS,
		D.CMD_PENDING_CNT,
		D.CMD_PENDING_UPDATED_TS,
		D.REJECT_UNTIL_TS,
		D.REJECT_UNTIL_UPDATED_TS,
		D.LAST_ACTIVITY_TS
  UNION ALL
 SELECT DEVICE_DEFAULT_NAME DEVICE_NAME,
	    DEVICE_DEFAULT_NAME DEVICE_SERIAL_CD,
	    ENCRYPTION_KEY,
	    NULL PREVIOUS_ENCRYPTION_KEY,
	    -1 DEVICE_TYPE_ID,
	    0 ACTIVATION_STATUS_CD,
	    'EST' TIME_ZONE_CD,
	    'US/Eastern' TIME_ZONE_GUID,
	    0 MASTER_ID,
	    0 REJECT_UNTIL_TS,
		0 REJECT_UNTIL_TS_TIMESTAMP,
	    'en_US' LOCALE,
	    'Y' INIT_ONLY,
	    0 ACTION_CD,
	    NULL,
	    0,
	    0,
	    0,
	    0,
	    0,
	    0,
	    0,
		SYSDATE LAST_ACTIVITY_TS
   FROM DEVICE.DEVICE_DEFAULT
  WHERE DEVICE_DEFAULT_ACTIVE_YN_FLAG = 'Y'
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/APP_LAYER/VW_DEVICE_PTA.vws?rev=1.13
CREATE OR REPLACE VIEW APP_LAYER.VW_DEVICE_PTA (POS_PTA_ID, DEVICE_NAME, PTA_PRIORITY, PTA_CARD_REGEX, PTA_CARD_REGEX_BREF, PTA_ENCRYPT_KEY, PTA_ENCRYPT_KEY2, PTA_ALLOW_PASSTHRU, PREF_AUTH_AMT, PREF_AUTH_AMT_MAX, PTA_DISABLE_DEBIT_DENIAL, PTA_TERMINAL_CD, CURRENCY_CD, MINOR_CURRENCY_FACTOR, PAYMENT_SUBTYPE_ID, PAYMENT_SUBTYPE_KEY_ID, PTA_ACTIVE_TS, PTA_DEACTIVE_TS, PTA_TIMESTAMP, PTA_HASH, LAST_ACTIVITY_TS) AS 
  SELECT pta.POS_PTA_ID,
         d.DEVICE_NAME, 
         pta.POS_PTA_PRIORITY PTA_PRIORITY, 
         NVL(pta.POS_PTA_REGEX, apm.AUTHORITY_PAYMENT_MASK_REGEX) PTA_CARD_REGEX, 
         DECODE(pta.POS_PTA_REGEX, NULL, apm.AUTHORITY_PAYMENT_MASK_BREF, pta.POS_PTA_REGEX_BREF) PTA_CARD_REGEX_BREF,
         pta.POS_PTA_ENCRYPT_KEY PTA_ENCRYPT_KEY,			       
    	 pta.POS_PTA_ENCRYPT_KEY2 PTA_ENCRYPT_KEY2,			       
    	 pta.POS_PTA_PASSTHRU_ALLOW_YN_FLAG PTA_ALLOW_PASSTHRU,
         pta.POS_PTA_PREF_AUTH_AMT * c.MINOR_CURRENCY_FACTOR PREF_AUTH_AMT,
         pta.POS_PTA_PREF_AUTH_AMT_MAX * c.MINOR_CURRENCY_FACTOR PREF_AUTH_AMT_MAX,
		 pta.POS_PTA_DISABLE_DEBIT_DENIAL PTA_DISABLE_DEBIT_DENIAL,
         DECODE(pst.PAYMENT_SUBTYPE_CLASS, 'POSGateway::AquaFill', d.DEVICE_SERIAL_CD, pta.POS_PTA_DEVICE_SERIAL_CD) PTA_TERMINAL_CD, 		    
    	 c.CURRENCY_CD,
    	 c.MINOR_CURRENCY_FACTOR,
         pta.PAYMENT_SUBTYPE_ID,
         psd.PAYMENT_SUBTYPE_KEY_ID,
         SYS_EXTRACT_UTC(CAST(pta.POS_PTA_ACTIVATION_TS AS TIMESTAMP)) PTA_ACTIVE_TS,
         SYS_EXTRACT_UTC(CAST(pta.POS_PTA_DEACTIVATION_TS AS TIMESTAMP)) PTA_DEACTIVE_TS,
         DATE_TO_MILLIS(GREATEST(d.LAST_UPDATED_TS, pos.LAST_UPDATED_TS, pta.LAST_UPDATED_TS, pst.LAST_UPDATED_TS, NVL(apm.LAST_UPDATED_TS, MIN_DATE))),
         DBMS_CRYPTO.HASH(UTL_RAW.CAST_FROM_NUMBER(pta.POS_PTA_PRIORITY) 
            || UTL_RAW.CAST_TO_RAW(pta.POS_PTA_REGEX)
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX)
			|| UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF)
            || UTL_RAW.CAST_TO_RAW(pta.POS_PTA_REGEX_BREF)
            || UTL_RAW.CAST_TO_RAW(pta.POS_PTA_ENCRYPT_KEY) 
            || UTL_RAW.CAST_TO_RAW(pta.POS_PTA_ENCRYPT_KEY2) 
            || UTL_RAW.CAST_TO_RAW(pta.POS_PTA_PASSTHRU_ALLOW_YN_FLAG) 
            || UTL_RAW.CAST_FROM_NUMBER(pta.POS_PTA_PREF_AUTH_AMT)
            || UTL_RAW.CAST_FROM_NUMBER(pta.POS_PTA_PREF_AUTH_AMT_MAX)
			|| UTL_RAW.CAST_TO_RAW(pta.POS_PTA_DISABLE_DEBIT_DENIAL)
            || UTL_RAW.CAST_TO_RAW(c.CURRENCY_CD)
            || UTL_RAW.CAST_FROM_NUMBER(c.MINOR_CURRENCY_FACTOR) 
            || UTL_RAW.CAST_TO_RAW(d.DEVICE_NAME) -- this should not change but will include it for completeness
            || UTL_RAW.CAST_TO_RAW(d.DEVICE_SERIAL_CD) -- this should not change but will include it for completeness
            || UTL_RAW.CAST_TO_RAW(pta.POS_PTA_DEVICE_SERIAL_CD)
            || UTL_RAW.CAST_FROM_NUMBER(pta.PAYMENT_SUBTYPE_ID) 
            || UTL_RAW.CAST_FROM_NUMBER(psd.PAYMENT_SUBTYPE_KEY_ID) 
            || UTL_RAW.CAST_FROM_NUMBER(DATE_TO_MILLIS(pta.POS_PTA_ACTIVATION_TS))
            || UTL_RAW.CAST_FROM_NUMBER(DATE_TO_MILLIS(pta.POS_PTA_DEACTIVATION_TS))
            , 2),
		d.LAST_ACTIVITY_TS
    FROM DEVICE.DEVICE d
    JOIN PSS.POS POS ON d.DEVICE_ID = pos.DEVICE_ID
    JOIN PSS.POS_PTA pta ON pos.POS_ID = pta.POS_ID
    JOIN PSS.PAYMENT_SUBTYPE pst ON pta.PAYMENT_SUBTYPE_ID = pst.PAYMENT_SUBTYPE_ID
    JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    LEFT OUTER JOIN PSS.AUTHORITY_PAYMENT_MASK apm ON pta.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
    JOIN APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL psd ON pta.PAYMENT_SUBTYPE_ID = psd.PAYMENT_SUBTYPE_ID
     AND (pst.PAYMENT_SUBTYPE_CLASS IN('Authority::NOP', 'Cash') 
            OR (pst.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash') AND pta.PAYMENT_SUBTYPE_KEY_ID = psd.PAYMENT_SUBTYPE_KEY_ID))
   WHERE d.DEVICE_ACTIVE_YN_FLAG = 'Y'
	 AND pta.POS_PTA_ACTIVATION_TS IS NOT NULL
     AND SYSDATE < NVL(pta.POS_PTA_DEACTIVATION_TS, MAX_DATE)
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/SP_UPDATE_CONFIG_AND_RETURN.prc?rev=1.2
/* For R29, to return command data. */
CREATE OR REPLACE PROCEDURE DEVICE.SP_UPDATE_CONFIG_AND_RETURN
(
    p_device_id         IN NUMBER,
    p_offset            IN INT,
    p_new_data          IN VARCHAR2,
    p_data_type         IN CHAR,
    p_device_type_id    IN NUMBER,
    p_debug             IN INT,
    p_return_code       OUT NUMBER,
    p_return_msg        OUT VARCHAR2,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE    
) IS
    l_ev_number         VARCHAR2 (12);
    l_file_transfer_id  NUMBER;
    l_orig_complete     VARCHAR2 (32000);
    l_substr_length     INT;
    l_orig_substr       VARCHAR2 (32000);
    l_file_offset       INT;
    l_replace_with      VARCHAR2 (32000);
    l_new_complete      VARCHAR2 (32000);
    l_poke_location     NUMBER(5,1);
    l_poke_length       INT;
    l_poke_command      VARCHAR2 (32000);
    l_pending_count     INT;
    l_execute_order     INT;
    
BEGIN

    BEGIN
        -- load data into a varchar for usage
        -- entire file is hex encoded
        SELECT device_name, file_transfer_id, file_transfer_content
        INTO l_ev_number, l_file_transfer_id, l_orig_complete
        FROM (
        SELECT device.device_name, file_transfer.file_transfer_id, file_transfer.file_transfer_content
        FROM device, file_transfer
        WHERE device.device_id = p_device_id
        AND device.device_name || '-CFG' = file_transfer.file_transfer_name
        AND file_transfer.file_transfer_type_cd = 1
        ORDER BY file_transfer.created_ts
        ) WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_return_code := -1;
            p_return_msg := 'Device or configuration record not found for device_id: ' || p_device_id;
            RETURN;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    p_return_msg := p_device_id || ',' || l_ev_number || ',' || p_offset || ': ';

    -- expect hex data will be passed in hex encoded, ascii and choice data will not
    IF p_data_type = 'H' THEN
        l_replace_with := p_new_data;
    ELSE
        l_replace_with := pkg_conversions.string_to_hex(p_new_data);
    END IF;

    l_substr_length := LENGTH(l_replace_with);

    -- expect p_offset refers to actual data location
    l_orig_substr := SUBSTR(l_orig_complete, ((p_offset*2)+1), l_substr_length);

    IF p_data_type = 'H' THEN
        p_return_msg := p_return_msg || UPPER(l_orig_substr) || ' = ' || UPPER(l_replace_with);
    ELSE
        p_return_msg := p_return_msg || pkg_conversions.hex_to_string(l_orig_substr) || ' = ' || pkg_conversions.hex_to_string(l_replace_with);
    END IF;

    -- compare the hex values to see if there was a change
    IF UPPER(l_orig_substr) = UPPER(l_replace_with) THEN
        p_return_code := 0;
        p_return_msg :=  p_return_msg || ': TRUE - NO CHANGE!';
        RETURN;
    END IF;

    p_return_msg :=  p_return_msg || ': FALSE - CHANGED!';
    p_return_code := 1;

    -- there was a change, so replace the original substring with the new one
    l_new_complete := substr(l_orig_complete, 0,(p_offset*2)) || l_replace_with || substr(l_orig_complete, ((p_offset*2)+l_substr_length+1));

    -- now we need to create a pending Poke for this change
    l_poke_length := (l_substr_length/2);

    IF p_device_type_id IN (0,1) THEN
        -- G4/G5 devices addresses memory in words
        l_poke_location := trunc((p_offset/2));
        -- must send at least 2 bytes at a time to a G4/G5
        IF l_poke_length = 1 THEN
            l_poke_length := 2;
        END IF;
    ELSIF p_device_type_id IN (6) THEN
        -- MEI device should always Poke the entire config
        l_poke_location := 0;
        l_poke_length := (length(l_new_complete)/2);
    ELSE
        l_poke_location := p_offset;
    END IF;
    
    l_poke_command := '42' || lpad(pkg_conversions.to_hex(l_poke_location), 8, '0') || lpad(pkg_conversions.to_hex(l_poke_length), 8, '0');

    -- check if the Poke already exists; no need to send 2 pokes for the same memory segment
    SELECT count(1)
    INTO l_pending_count
    FROM machine_command_pending
    WHERE machine_id = l_ev_number
    AND data_type = '88'
    AND command = l_poke_command
    AND execute_cd IN ('P', 'S');

    -- query for the max pending command execute_order
    -- we want to put the Poke at the end of the pending list
    SELECT nvl(max(execute_order)+1, 0)
    INTO l_execute_order
    FROM machine_command_pending
    WHERE machine_id = l_ev_number
    AND execute_cd IN ('P', 'S');

    p_return_msg := p_return_msg || ' - POKE: ' || l_ev_number || ' 88 ' || l_poke_command || ' P ' || l_execute_order;

    -- if not debugging then update the file contents and save the poke
    IF l_pending_count = 0 THEN
        p_return_msg := p_return_msg || ', poke does not exist';
        IF p_debug = 0 THEN
            INSERT INTO machine_command_pending(machine_id, data_type, command, execute_cd, execute_order)
            VALUES (l_ev_number, '88', l_poke_command, 'P', l_execute_order)
            RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, TRIM(COMMAND) INTO pn_command_id, pv_data_type, pl_command;
            p_return_msg := p_return_msg || ', poke inserted';
        ELSE
            p_return_msg := p_return_msg || ', poke not inserted (debug on)';
        END IF;
    ELSE
        p_return_msg := p_return_msg || ', poke not inserted (already exists)';
    END IF;

    IF p_debug = 0 THEN
        UPDATE file_transfer set file_transfer_content = l_new_complete where file_transfer_id = l_file_transfer_id;
        p_return_msg := p_return_msg || ', file_transfer updated';
    ELSE
        p_return_msg := p_return_msg || ', file_transfer not updated (debug on)';
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        BEGIN
            p_return_code := -1;
            p_return_msg := 'Unexpected error occured! ' || SQLCODE || ': ' || SQLERRM;
            RETURN;
        END;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/ENGINE/PKG_DEVICE_SESSION.psk?rev=1.15
CREATE OR REPLACE PACKAGE ENGINE.PKG_DEVICE_SESSION 
IS
    FUNCTION GET_OR_CREATE_SESSION(
      pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE)
     RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;

    FUNCTION UPDATE_SESSION_START(
      pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
      pn_net_layer_id ENGINE.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address ENGINE.DEVICE_SESSION.SESSION_CLIENT_IP_ADDR%TYPE,
      pn_remote_port ENGINE.DEVICE_SESSION.SESSION_CLIENT_PORT%TYPE,
      pn_session_start_time NUMBER,
      pd_device_call_start_date DATE)
     RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;
    
    -- R26+ signature
    FUNCTION UPDATE_SESSION_END(
        pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
        pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_bytes_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_BYTE_CNT%TYPE,
        pn_bytes_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_BYTE_CNT%TYPE,
        pn_messages_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_MSG_CNT%TYPE,
        pn_messages_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_MSG_CNT%TYPE,
        pn_session_end_time NUMBER,
        pd_device_call_end_date DEVICE_CALL_IN_RECORD.CALL_IN_FINISH_TS%TYPE,
        pc_initialized DEVICE_CALL_IN_RECORD.DEVICE_INITIALIZED_FLAG%TYPE,
        pc_device_sent_config DEVICE_CALL_IN_RECORD.DEVICE_SENT_CONFIG_FLAG%TYPE,
        pc_server_sent_config DEVICE_CALL_IN_RECORD.SERVER_SENT_CONFIG_FLAG%TYPE,
        pn_dex_file_size DEVICE_CALL_IN_RECORD.DEX_FILE_SIZE%TYPE,
        pn_auth_amt DEVICE_CALL_IN_RECORD.AUTH_AMOUNT%TYPE,
        pc_auth_result_cd CHAR,
        pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
        pd_auth_ts DEVICE_CALL_IN_RECORD.LAST_AUTH_IN_TS%TYPE,
        pc_call_in_type DEVICE_CALL_IN_RECORD.CALL_IN_TYPE%TYPE,
        pc_call_in_status DEVICE_CALL_IN_RECORD.CALL_IN_STATUS%TYPE,
		pv_device_serial_num OUT DEVICE.DEVICE_SERIAL_CD%TYPE)
    RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;
    
    -- R25 Signature
    FUNCTION UPDATE_SESSION_END(
      pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
      pn_bytes_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_BYTE_CNT%TYPE,
      pn_bytes_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_BYTE_CNT%TYPE,
      pn_messages_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_MSG_CNT%TYPE,
      pn_messages_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_MSG_CNT%TYPE,
      pn_session_end_time NUMBER,
      pd_device_call_end_date DATE,
      pc_initialized CHAR DEFAULT 'F')
    RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;

    -- R24 Signature
    PROCEDURE INSERT_MESSAGE_PROCESSED (
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
        pn_net_layer_id ENGINE.DEVICE_SESSION.NET_LAYER_ID%TYPE,
        pn_message_sequence ENGINE.DEVICE_MESSAGE.MESSAGE_SEQUENCE%TYPE,
        pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
        pc_reply_complete CHAR,
        pc_reply_success CHAR,
        pv_inbound_message_type VARCHAR2,
        pv_inbound_message RAW,
        pv_outbound_message_type VARCHAR2,
        pv_outbound_message RAW,
        pn_message_start_time NUMBER,
        pn_message_end_time NUMBER,
        pd_message_start_date DATE,
        pd_message_end_date DATE,
        pn_inbound_message_length ENGINE.DEVICE_MESSAGE.INBOUND_MESSAGE_LENGTH%TYPE,
        pn_outbound_message_length ENGINE.DEVICE_MESSAGE.OUTBOUND_MESSAGE_LENGTH%TYPE,
        pc_check_for_dup CHAR DEFAULT 'Y'
    );
    
    FUNCTION GET_OR_CREATE_NET_LAYER(
        l_net_layer_desc ENGINE.NET_LAYER.NET_LAYER_DESC%TYPE,
        l_server_name ENGINE.NET_LAYER.SERVER_NAME%TYPE,
        l_server_port_num ENGINE.NET_LAYER.SERVER_PORT_NUM%TYPE)
      RETURN ENGINE.NET_LAYER.NET_LAYER_ID%TYPE;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/ENGINE/PKG_DEVICE_SESSION.pbk?rev=1.46
CREATE OR REPLACE PACKAGE BODY ENGINE.PKG_DEVICE_SESSION IS
  /* IMPORTANT: Before you update DEVICE.DEVICE, DEVICE.DEVICE_CALL_IN_RECORD, OR ENGINE.DEVICE_SESSION, 
                you must call this function to obtain a lock on the DEVICE_SESSION row to avoid deadlocks. */
    FUNCTION GET_OR_CREATE_SESSION_INTERNAL(
      pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pn_retries PLS_INTEGER)
     RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    AS
        ln_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;
    BEGIN
        BEGIN
            SELECT DEVICE_SESSION_ID
              INTO ln_session_id
              FROM ENGINE.DEVICE_SESSION
             WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT ENGINE.SEQ_DEVICE_SESSION_ID.NEXTVAL
                  INTO ln_session_id
                  FROM DUAL;
                  
                BEGIN
                    INSERT INTO ENGINE.DEVICE_SESSION(
                        DEVICE_SESSION_ID,
                        GLOBAL_SESSION_CD,
                        DEVICE_NAME
                    ) VALUES(
                        ln_session_id,
                        pv_global_session_cd,
                        pv_device_name
                    );
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        IF pn_retries > 0 THEN
                            RETURN GET_OR_CREATE_SESSION_INTERNAL(pv_global_session_cd, pv_device_name, pn_retries - 1);
                        ELSE
                            RAISE;
                        END IF;
                END;  
                -- Ensure Device Call In Record exists
                INSERT INTO DEVICE.DEVICE_CALL_IN_RECORD (
                    CALL_IN_START_TS,
                    CALL_IN_STATUS,
                    SERIAL_NUMBER,
                    SESSION_CD,
                    SESSION_ID
                 ) VALUES (
                    SYSDATE,
                    'I',
                    pv_device_name,
                    pv_global_session_cd,
                    ln_session_id
                 );
        END;
    
        RETURN ln_session_id;
    END;
    
    FUNCTION GET_OR_CREATE_SESSION(
      pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE)
     RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    AS
	BEGIN
        IF pv_global_session_cd IS NULL THEN
            RAISE_APPLICATION_ERROR(-20701, 'Invalid pv_global_session_cd: ' || pv_global_session_cd);
        END IF;

        IF pv_device_name IS NULL THEN
            RAISE_APPLICATION_ERROR(-20701, 'Invalid pv_device_name: ' || pv_device_name);
        END IF;

        RETURN GET_OR_CREATE_SESSION_INTERNAL(pv_global_session_cd, pv_device_name, 10);
    END;

    FUNCTION UPDATE_SESSION_START(
      pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
      pn_net_layer_id ENGINE.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address ENGINE.DEVICE_SESSION.SESSION_CLIENT_IP_ADDR%TYPE,
      pn_remote_port ENGINE.DEVICE_SESSION.SESSION_CLIENT_PORT%TYPE,
      pn_session_start_time NUMBER,
      pd_device_call_start_date DATE)
     RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    AS
	    ln_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;
        l_old_session_ids NUMBER_TABLE;
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE;
        lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
        lv_location_name LOCATION.LOCATION_NAME%TYPE;
        lv_customer_name CUSTOMER.CUSTOMER_NAME%TYPE;
        lv_net_layer_desc ENGINE.NET_LAYER.NET_LAYER_DESC%TYPE;
		lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
    BEGIN
        IF pv_global_session_cd IS NULL THEN
            RAISE_APPLICATION_ERROR(-20701, 'Invalid pv_global_session_cd: ' || pv_global_session_cd);
        END IF;

        IF pv_device_name IS NULL THEN
            RAISE_APPLICATION_ERROR(-20701, 'Invalid pv_device_name: ' || pv_device_name);
        END IF;
		
        SELECT MAX(COMM_METHOD_CD)
          INTO lc_comm_method_cd
          FROM (SELECT COMM_METHOD_CD
                  FROM DEVICE.COMM_METHOD
                 WHERE REGEXP_LIKE(pv_remote_address, IP_MATCH_REGEX)
                 ORDER BY IP_MATCH_ORDER, LENGTH(IP_MATCH_REGEX) DESC, COMM_METHOD_CD)
         WHERE ROWNUM = 1;
		
        BEGIN
			SELECT D.DEVICE_ID, D.DEVICE_NAME, D.DEVICE_SERIAL_CD, L.LOCATION_NAME, C.CUSTOMER_NAME
              INTO ln_device_id, lv_device_name, lv_device_serial_cd, lv_location_name, lv_customer_name
              FROM DEVICE.DEVICE D
              LEFT OUTER JOIN PSS.POS P on D.DEVICE_ID = P.DEVICE_ID
              LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
              LEFT OUTER JOIN LOCATION.CUSTOMER C ON P.CUSTOMER_ID = C.CUSTOMER_ID
             WHERE D.DEVICE_ID = PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_device_call_start_date);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF pv_device_serial_cd IS NOT NULL THEN
                    BEGIN
                        -- Use serialCD
                        SELECT D.DEVICE_ID, D.DEVICE_NAME, D.DEVICE_SERIAL_CD, L.LOCATION_NAME, C.CUSTOMER_NAME
                          INTO ln_device_id, lv_device_name, lv_device_serial_cd, lv_location_name, lv_customer_name
                          FROM DEVICE.DEVICE D
                          LEFT OUTER JOIN PSS.POS P on D.DEVICE_ID = P.DEVICE_ID
                          LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
                          LEFT OUTER JOIN LOCATION.CUSTOMER C ON P.CUSTOMER_ID = C.CUSTOMER_ID
                         WHERE D.DEVICE_ID = PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_SERIAL(pv_device_serial_cd, pd_device_call_start_date);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            lv_device_name := pv_device_name;
                    END;
                ELSE
                    lv_device_name := pv_device_name;
                END IF;
        END;
        
        SELECT NET_LAYER_DESC 
          INTO lv_net_layer_desc
          FROM ENGINE.NET_LAYER 
         WHERE NET_LAYER_ID = pn_net_layer_id;

        ln_session_id := GET_OR_CREATE_SESSION(pv_global_session_cd, pv_device_name);
        COMMIT;
        
        UPDATE ENGINE.DEVICE_SESSION 
           SET DEVICE_NAME = lv_device_name,
               NET_LAYER_ID = pn_net_layer_id,
               SESSION_CLIENT_IP_ADDR = pv_remote_address,
               SESSION_CLIENT_PORT = pn_remote_port,
               SESSION_START_UTC_TS = MILLIS_TO_TIMESTAMP(pn_session_start_time)
         WHERE DEVICE_SESSION_ID = ln_session_id;
        COMMIT;
        
        -- Mark any previous sessions as complete
        UPDATE /*+INDEX(DS IX_DS_DEVICE_NAME) */ ENGINE.DEVICE_SESSION DS
           SET SESSION_STATE_ID = 3
         WHERE DEVICE_NAME = lv_device_name
           AND SESSION_STATE_ID = 1
           AND DEVICE_SESSION_ID <> ln_session_id
           AND SESSION_CLIENT_PORT = pn_remote_port
           AND SESSION_START_UTC_TS < MILLIS_TO_TIMESTAMP(pn_session_start_time)
         RETURNING DEVICE_SESSION_ID BULK COLLECT INTO l_old_session_ids;
        COMMIT;

        UPDATE DEVICE.DEVICE_CALL_IN_RECORD
           SET CALL_IN_START_TS = NVL(pd_device_call_start_date, CALL_IN_START_TS),
               CALL_IN_FINISH_TS = DECODE(CALL_IN_FINISH_TS, NULL, CALL_IN_FINISH_TS, GREATEST(NVL(pd_device_call_start_date, CALL_IN_START_TS), CALL_IN_FINISH_TS)),
               NETWORK_LAYER = lv_net_layer_desc,
               IP_ADDRESS = pv_remote_address,
               SERIAL_NUMBER = NVL(lv_device_serial_cd, lv_device_name),
               LOCATION_NAME = lv_location_name,
               CUSTOMER_NAME = lv_customer_name
         WHERE SESSION_ID = ln_session_id;
        COMMIT;
               
        UPDATE DEVICE.DEVICE_CALL_IN_RECORD
           SET CALL_IN_STATUS = 'U'
         WHERE CALL_IN_STATUS = 'I'
           AND SESSION_ID IN (SELECT COLUMN_VALUE FROM TABLE(l_old_session_ids));
        COMMIT;
		
		-- make sure that device command pending count is accurate and update other device fields
		UPDATE DEVICE.DEVICE D
		   SET (CMD_PENDING_CNT, LAST_ACTIVITY_TS, COMM_METHOD_CD, COMM_METHOD_TS)
             = (SELECT COUNT(1),
                       GREATEST(pd_device_call_start_date, NVL(D.LAST_ACTIVITY_TS, MIN_DATE)),
                       DECODE(DBADMIN.PKG_UTL.COMPARE(pd_device_call_start_date, NVL(D.COMM_METHOD_TS, MIN_DATE)), 1, lc_comm_method_cd, D.COMM_METHOD_CD),
					   DECODE(DBADMIN.PKG_UTL.COMPARE(pd_device_call_start_date, NVL(D.COMM_METHOD_TS, MIN_DATE)), 1, pd_device_call_start_date, D.COMM_METHOD_TS)
                  FROM ENGINE.MACHINE_CMD_PENDING MCP
                 WHERE MACHINE_ID = lv_device_name)
		 WHERE DEVICE_ID = ln_device_id;
        COMMIT;
        
	    RETURN ln_session_id;
    END;
	
    FUNCTION UPDATE_SESSION_END(
        pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
        pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_bytes_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_BYTE_CNT%TYPE,
        pn_bytes_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_BYTE_CNT%TYPE,
        pn_messages_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_MSG_CNT%TYPE,
        pn_messages_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_MSG_CNT%TYPE,
        pn_session_end_time NUMBER,
        pd_device_call_end_date DEVICE_CALL_IN_RECORD.CALL_IN_FINISH_TS%TYPE,
        pc_initialized DEVICE_CALL_IN_RECORD.DEVICE_INITIALIZED_FLAG%TYPE,
        pc_device_sent_config DEVICE_CALL_IN_RECORD.DEVICE_SENT_CONFIG_FLAG%TYPE,
        pc_server_sent_config DEVICE_CALL_IN_RECORD.SERVER_SENT_CONFIG_FLAG%TYPE,
        pn_dex_file_size DEVICE_CALL_IN_RECORD.DEX_FILE_SIZE%TYPE,
        pn_auth_amt DEVICE_CALL_IN_RECORD.AUTH_AMOUNT%TYPE,
        pc_auth_result_cd CHAR,
        pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
        pd_auth_ts DEVICE_CALL_IN_RECORD.LAST_AUTH_IN_TS%TYPE,
        pc_call_in_type DEVICE_CALL_IN_RECORD.CALL_IN_TYPE%TYPE,
        pc_call_in_status DEVICE_CALL_IN_RECORD.CALL_IN_STATUS%TYPE,
		pv_device_serial_num OUT DEVICE.DEVICE_SERIAL_CD%TYPE)
    RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    AS
        ln_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE;
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE;
        lv_location_name LOCATION.LOCATION_NAME%TYPE;
        lv_customer_name CUSTOMER.CUSTOMER_NAME%TYPE;      
    BEGIN
        ln_session_id := GET_OR_CREATE_SESSION(pv_global_session_cd, pv_device_name);
        COMMIT;
        BEGIN
            SELECT D.DEVICE_ID, D.DEVICE_NAME, D.DEVICE_SERIAL_CD, L.LOCATION_NAME, C.CUSTOMER_NAME
              INTO ln_device_id, lv_device_name, pv_device_serial_num, lv_location_name, lv_customer_name
              FROM DEVICE.DEVICE D
              LEFT OUTER JOIN PSS.POS P on D.DEVICE_ID = P.DEVICE_ID
              LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
              LEFT OUTER JOIN LOCATION.CUSTOMER C ON P.CUSTOMER_ID = C.CUSTOMER_ID
             WHERE D.DEVICE_ID = PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_device_call_end_date);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF pv_device_serial_cd IS NOT NULL THEN
                    BEGIN
                        -- Use serialCD
                        SELECT D.DEVICE_ID, D.DEVICE_NAME, D.DEVICE_SERIAL_CD, L.LOCATION_NAME, C.CUSTOMER_NAME
                          INTO ln_device_id, lv_device_name, pv_device_serial_num, lv_location_name, lv_customer_name
                          FROM DEVICE.DEVICE D
                          LEFT OUTER JOIN PSS.POS P on D.DEVICE_ID = P.DEVICE_ID
                          LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
                          LEFT OUTER JOIN LOCATION.CUSTOMER C ON P.CUSTOMER_ID = C.CUSTOMER_ID
                         WHERE D.DEVICE_ID = PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_SERIAL(pv_device_serial_cd, pd_device_call_end_date);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            lv_device_name := pv_device_name;
                    END;
                ELSE
                    lv_device_name := pv_device_name;
                END IF;
        END;
        
        UPDATE ENGINE.DEVICE_SESSION
    	   SET SESSION_INBOUND_BYTE_CNT = pn_bytes_received,
    	       SESSION_OUTBOUND_BYTE_CNT = pn_bytes_sent,
    	       SESSION_INBOUND_MSG_CNT = pn_messages_received,
    	       SESSION_OUTBOUND_MSG_CNT = pn_messages_sent,
    	       SESSION_END_UTC_TS = MILLIS_TO_TIMESTAMP(pn_session_end_time),
    	       SESSION_STATE_ID = DECODE(pc_call_in_status, 'S', 2, 'U', 3, 3),
               DEVICE_NAME = lv_device_name
    	 WHERE DEVICE_SESSION_ID = ln_session_id;
        COMMIT;
             
        UPDATE DEVICE.DEVICE_CALL_IN_RECORD
           SET CALL_IN_START_TS = LEAST(CALL_IN_START_TS, NVL(pd_device_call_end_date, CALL_IN_START_TS)),
               CALL_IN_FINISH_TS = NVL(pd_device_call_end_date, CALL_IN_START_TS),
               CALL_IN_STATUS = NVL(pc_call_in_status, CALL_IN_STATUS),
               INBOUND_MESSAGE_COUNT = pn_messages_received,
               OUTBOUND_MESSAGE_COUNT = pn_messages_sent,
               INBOUND_BYTE_COUNT = pn_bytes_received,
               OUTBOUND_BYTE_COUNT = pn_bytes_sent,
               SERIAL_NUMBER = NVL(pv_device_serial_num, lv_device_name),
               LOCATION_NAME = lv_location_name,
               CUSTOMER_NAME = lv_customer_name,
               DEVICE_INITIALIZED_FLAG = pc_initialized,
               DEX_FILE_SIZE = NVL(pn_dex_file_size, DEX_FILE_SIZE),
               DEX_RECEIVED_FLAG = DECODE(pn_dex_file_size, NULL, DEX_RECEIVED_FLAG, 0, 'N', 'Y'),
               DEVICE_SENT_CONFIG_FLAG = NVL(pc_device_sent_config, DEVICE_SENT_CONFIG_FLAG),
               SERVER_SENT_CONFIG_FLAG = NVL(pc_server_sent_config, SERVER_SENT_CONFIG_FLAG),
               AUTH_AMOUNT = NVL(pn_auth_amt, AUTH_AMOUNT),
               AUTH_APPROVED_FLAG = DECODE(pc_auth_result_cd, NULL, AUTH_APPROVED_FLAG, 'Y', 'Y', 'N', 'N', 'P', 'Y', 'O', 'N', 'F', 'N'),
               AUTH_CARD_TYPE = NVL(pc_payment_type, AUTH_CARD_TYPE),
               LAST_AUTH_IN_TS = NVL(pd_auth_ts, LAST_AUTH_IN_TS),
               CALL_IN_TYPE = NVL(pc_call_in_type, CALL_IN_TYPE)
         WHERE SESSION_ID = ln_session_id;
        COMMIT;
        
        UPDATE DEVICE.DEVICE
           SET LAST_ACTIVITY_TS = pd_device_call_end_date,
               RECEIVED_CONFIG_FILE_TS = DECODE(pc_device_sent_config,
               										'Y', pd_device_call_end_date,
               										 RECEIVED_CONFIG_FILE_TS)               
         WHERE DEVICE_ID = ln_device_id
           AND pd_device_call_end_date > NVL(LAST_ACTIVITY_TS, MIN_DATE);
	    COMMIT;
		
        RETURN ln_session_id;
    END;

    FUNCTION UPDATE_SESSION_END(
        pv_global_session_cd ENGINE.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
        pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_bytes_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_BYTE_CNT%TYPE,
        pn_bytes_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_BYTE_CNT%TYPE,
        pn_messages_received ENGINE.DEVICE_SESSION.SESSION_INBOUND_MSG_CNT%TYPE,
        pn_messages_sent ENGINE.DEVICE_SESSION.SESSION_OUTBOUND_MSG_CNT%TYPE,
        pn_session_end_time NUMBER,
        pd_device_call_end_date DATE,
        pc_initialized CHAR DEFAULT 'F')
        RETURN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    AS
        lc_initialized_flag CHAR;
		lv_device_serial_num DEVICE.DEVICE_SERIAL_CD%TYPE;
    BEGIN
        IF pc_initialized IN('T','t') THEN
            lc_initialized_flag := 'Y';
        ELSE
            lc_initialized_flag := 'N';
        END IF;
        RETURN UPDATE_SESSION_END(pv_global_session_cd,
            pv_device_name,
            pv_device_serial_cd,
            pn_bytes_received,
            pn_bytes_sent,
            pn_messages_received,
            pn_messages_sent,
            pn_session_end_time,
            pd_device_call_end_date,
            lc_initialized_flag,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
			lv_device_serial_num);
    END;
    
    PROCEDURE INSERT_MESSAGE_PROCESSED (
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
        pn_net_layer_id ENGINE.DEVICE_SESSION.NET_LAYER_ID%TYPE,
        pn_message_sequence ENGINE.DEVICE_MESSAGE.MESSAGE_SEQUENCE%TYPE,
        pv_device_name ENGINE.DEVICE_SESSION.DEVICE_NAME%TYPE,
        pc_reply_complete CHAR,
        pc_reply_success CHAR,
        pv_inbound_message_type VARCHAR2,
        pv_inbound_message RAW,
        pv_outbound_message_type VARCHAR2,
        pv_outbound_message RAW,
        pn_message_start_time NUMBER,
        pn_message_end_time NUMBER,
        pd_message_start_date DATE,
        pd_message_end_date DATE,
        pn_inbound_message_length ENGINE.DEVICE_MESSAGE.INBOUND_MESSAGE_LENGTH%TYPE,
        pn_outbound_message_length ENGINE.DEVICE_MESSAGE.OUTBOUND_MESSAGE_LENGTH%TYPE,
        pc_check_for_dup CHAR DEFAULT 'Y'
    ) IS
        ln_cnt PLS_INTEGER;
    BEGIN
        IF UPPER(pc_reply_complete) = 'T' THEN
            IF pc_check_for_dup = 'Y' THEN
                SELECT COUNT(*)
                 INTO ln_cnt
                 FROM ENGINE.DEVICE_MESSAGE
                WHERE DEVICE_SESSION_ID = pn_session_id
                  AND MESSAGE_SEQUENCE = pn_message_sequence;
                IF ln_cnt > 0 THEN
                    RETURN;
                END IF;
            END IF;
            -- Insert to message_stat
            INSERT INTO ENGINE.DEVICE_MESSAGE(
                DEVICE_MESSAGE_ID,
                DEVICE_SESSION_ID,
                MESSAGE_SEQUENCE,
                INBOUND_MESSAGE_UTC_TS,
                INBOUND_MESSAGE_TYPE,
                INBOUND_MESSAGE,
                OUTBOUND_MESSAGE_UTC_TS,
                OUTBOUND_MESSAGE_TYPE,
                OUTBOUND_MESSAGE,
                MESSAGE_TRANSMITTED_FLAG,
                MESSAGE_DURATION_MILLIS,
                INBOUND_MESSAGE_LENGTH,
                OUTBOUND_MESSAGE_LENGTH,
				DEVICE_NAME,
				NET_LAYER_ID,
				INBOUND_MESSAGE_TS,
				OUTBOUND_MESSAGE_TS)
            SELECT 
                ENGINE.SEQ_DEVICE_MESSAGE_ID.NEXTVAL,
                pn_session_id,
                pn_message_sequence,
                MILLIS_TO_TIMESTAMP(pn_message_start_time),
                pv_inbound_message_type,
                pv_inbound_message,
                MILLIS_TO_TIMESTAMP(pn_message_end_time),
                pv_outbound_message_type,
                pv_outbound_message,
                DECODE(UPPER(pc_reply_success), 'T', 'Y', 'N'),
                pn_message_end_time - pn_message_start_time,
                pn_inbound_message_length,
                pn_outbound_message_length,
				pv_device_name,
				pn_net_layer_id,
				pd_message_start_date,
				pd_message_end_date
              FROM DUAL;
        END IF;
    END;
    
    FUNCTION GET_OR_CREATE_NET_LAYER(
        l_net_layer_desc ENGINE.NET_LAYER.NET_LAYER_DESC%TYPE,
        l_server_name ENGINE.NET_LAYER.SERVER_NAME%TYPE,
        l_server_port_num ENGINE.NET_LAYER.SERVER_PORT_NUM%TYPE)
      RETURN ENGINE.NET_LAYER.NET_LAYER_ID%TYPE
    AS        
	    i PLS_INTEGER := 1;
	    l_net_layer_id ENGINE.NET_LAYER.NET_LAYER_ID%TYPE;
	BEGIN
	    LOOP
	        BEGIN
	            SELECT NET_LAYER_ID
	              INTO l_net_layer_id
	              FROM ENGINE.NET_LAYER
	             WHERE NET_LAYER_DESC = l_net_layer_desc;
	            EXIT;
	        EXCEPTION
	            WHEN NO_DATA_FOUND THEN
	                BEGIN
	                    SELECT ENGINE.SEQ_NET_LAYER_ID.NEXTVAL
	                      INTO l_net_layer_id
	                      FROM DUAL;
	                    INSERT INTO ENGINE.NET_LAYER(
	                        NET_LAYER_ID, 
	                        NET_LAYER_DESC,
	                        SERVER_NAME,
	                        SERVER_PORT_NUM,
	                        NET_LAYER_ACTIVE_YN_FLAG)
	                      VALUES(
	                        l_net_layer_id,
	                        l_net_layer_desc,
	                        l_server_name,
	                        l_server_port_num,
	                        'Y');
	                    EXIT;
	                EXCEPTION
	                    WHEN DUP_VAL_ON_INDEX THEN
	                    	IF i > 9 THEN
	                    		RAISE;
	                    	END IF;
	                        i := i + 1;
	                END;
	        END;
	    END LOOP;
	    RETURN l_net_layer_id;
	END;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.psk?rev=1.26
CREATE OR REPLACE PACKAGE DEVICE.PKG_DEVICE_CONFIGURATION IS
    
    FUNCTION LOB_TO_HEX_LONG(pl_lob CLOB)
    RETURN LONG;
    /*
    FUNCTION HEX_LONG_TO_LOB(pl_long LONG)
    RETURN BLOB;
    */
    PROCEDURE SP_GET_FILE_TRANSFER_BLOB
    (
        pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
        pbl_file_transfer_content OUT BLOB
    );
	
	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	
    PROCEDURE SP_UPDATE_DEVICE_SETTING(
        pn_device_id IN device.device_id%TYPE,
        pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
		pv_device_setting_value IN device_setting.device_setting_value%TYPE
    );
	
	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	);
    
    FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
        pn_device_id IN device.device_id%TYPE,
        pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
        pv_device_setting_value IN device_setting.device_setting_value%TYPE
    )
    RETURN VARCHAR;

    PROCEDURE SP_UPDATE_DEVICE_SETTINGS(
        pn_device_id IN device.device_id%TYPE,
        pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,
        pn_setting_count OUT NUMBER
    );

    PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS(
        pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,    
        pn_setting_count OUT NUMBER
    );

    PROCEDURE SP_GET_HOST_BY_PORT_NUM(
        pv_device_name IN device.device_name%TYPE,
        pn_host_port_num IN host.host_port_num%TYPE,
        pt_utc_ts IN TIMESTAMP,
        pn_host_id OUT host.host_id%TYPE    
    );

    PROCEDURE SP_GET_HOST(
        pn_device_id IN device.device_id%TYPE,
        pv_device_name IN device.device_name%TYPE,
        pn_host_port_num IN host.host_port_num%TYPE,
        pt_utc_ts IN TIMESTAMP,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,
        pn_host_id OUT host.host_id%TYPE    
    );

    PROCEDURE SP_CREATE_DEFAULT_HOSTS(
        pn_device_id        IN       device.device_id%TYPE,
        pn_new_host_count   OUT      NUMBER,
        pn_result_cd        OUT      NUMBER,
        pv_error_message    OUT      VARCHAR2
    );

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER);
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER);
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE);
        
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE);
    
    PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE);
        
    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB);
        
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG);
    
    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER);
            
    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER);
        
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE);

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE);
        
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT BLOB,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE);
        
    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pl_file_content_hex FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    );
	
/**
  * r28 compatible call-in normalization version
  */ 
PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	);

/**
  * r29 Call-in normalization version
  */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	);
    
/**
  * r28 compatible version of SP_NEXT_PENDING_COMMAND
  * Does not support the stateful session attribute parameter
  */
  PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL);

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2); -- contains character 'C' if configuration has been requested previously       
        
    /**
      * r28 compatible version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
         pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
         pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
         pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
         pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
         pn_rows_inserted OUT PLS_INTEGER);

    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE);

  /**
    * pre-R28 compatible version
    */
    PROCEDURE UPSERT_PENDING_COMMAND(
         pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
         pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
         pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
         pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999);
         
    PROCEDURE CONFIG_POKE(pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE);
     
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.pbk?rev=1.76
create or replace PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION IS
    FUNCTION LOB_TO_HEX_LONG(pl_lob BLOB)
    RETURN LONG
    IS
        ll_tmp LONG := '';
        tot BINARY_INTEGER;
    BEGIN
        tot := DBMS_LOB.GETLENGTH(pl_lob);
        FOR i IN 1..tot LOOP
            ll_tmp := ll_tmp || RAWTOHEX(DBMS_LOB.SUBSTR(pl_lob, 1, i));
        END LOOP;
        RETURN ll_tmp;
    END;

    FUNCTION LOB_TO_HEX_LONG(pl_lob CLOB)
    RETURN LONG
    IS
        ll_tmp LONG := '';
        tot BINARY_INTEGER;
        n PLS_INTEGER;
    BEGIN
        tot := DBMS_LOB.GETLENGTH(pl_lob);
        FOR i IN 1..tot LOOP
            n := ASCII(DBMS_LOB.SUBSTR(pl_lob, 1, i));
            ll_tmp := ll_tmp || TO_CHAR(n, 'FM0X');
        END LOOP;
        RETURN ll_tmp;
    END;
    /*
    FUNCTION HEX_LONG_TO_LOB(pl_long LONG)
    RETURN BLOB
    IS
        ll_clob CLOB;
        ll_tmp BLOB;
        tot BINARY_INTEGER;
    BEGIN
        ll_clob := pl_long;
        tot := DBMS_LOB.GETLENGTH(ll_clob) / 2;
        DBMS_LOB.CREATETEMPORARY(ll_tmp, TRUE, DBMS_LOB.CALL);
        FOR i IN 1..tot LOOP
            DBMS_LOB.WRITEAPPEND(ll_tmp, 1, DBMS_LOB.SUBSTR(ll_clob, 2, (i * 2) - 1));
        END LOOP;
        RETURN ll_tmp;
    END;
*/
    
PROCEDURE SP_GET_FILE_TRANSFER_BLOB
(
    pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
    pbl_file_transfer_content OUT BLOB
)
IS
    lcl_file_transfer_content_hex CLOB;
    ln_file_content_hex_length NUMBER;
    ln_position NUMBER := 1;
    ln_max_size NUMBER := 32766; -- Oracle limitation
    ln_length NUMBER;

BEGIN
    DELETE FROM device.gtmp_file_transfer WHERE file_transfer_id = pn_file_transfer_id;

    INSERT INTO device.gtmp_file_transfer(file_transfer_id, file_transfer_content_hex)
    SELECT file_transfer_id, TO_LOB(file_transfer_content)
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    SELECT file_transfer_content_hex INTO lcl_file_transfer_content_hex
    FROM device.gtmp_file_transfer
    WHERE file_transfer_id = pn_file_transfer_id AND ROWNUM = 1;

    ln_file_content_hex_length := dbms_lob.getlength(lcl_file_transfer_content_hex);
    dbms_lob.createtemporary(pbl_file_transfer_content, TRUE, dbms_lob.call);

    WHILE ln_position <= ln_file_content_hex_length LOOP
        ln_length := LEAST(ln_max_size, ln_file_content_hex_length - ln_position + 1);
        dbms_lob.writeappend(pbl_file_transfer_content, ln_length / 2, REPLACE(dbms_lob.substr(lcl_file_transfer_content_hex, ln_length, ln_position), ' ', '0'));
        ln_position := ln_position + ln_length;
    END LOOP;

    DELETE FROM device.gtmp_file_transfer WHERE file_transfer_id = pn_file_transfer_id;
END;

FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
RETURN VARCHAR
IS
    ln_cnt PLS_INTEGER; 
BEGIN
	SELECT COUNT(*)
      INTO ln_cnt
      FROM DEVICE.DEVICE d
      JOIN DEVICE.FILE_TRANSFER ft
        ON ft.FILE_TRANSFER_NAME = CASE d.DEVICE_TYPE_ID
                WHEN 0 THEN 'G4-DEFAULT-CFG'
                WHEN 1 THEN 'G5-DEFAULT-CFG'
                WHEN 6 THEN 'MEI-DEFAULT-CFG'
                WHEN 12 THEN 'T2-DEFAULT-CFG'
                ELSE 'DEFAULT-CFG-' || d.DEVICE_TYPE_ID || '-' || (
                   SELECT dsplv.DEVICE_SETTING_VALUE
                     FROM DEVICE.DEVICE_SETTING dsplv
                    WHERE dsplv.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
                      AND dsplv.DEVICE_ID = d.DEVICE_ID)
            END
        AND ft.FILE_TRANSFER_TYPE_CD IN(6,15,22)
      CROSS JOIN DEVICE.DEVICE_SETTING_PARAMETER dsp
       LEFT JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts ON ft.FILE_TRANSFER_ID = cts.FILE_TRANSFER_ID
        AND cts.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
      WHERE d.DEVICE_ID = pn_device_id
        AND (cts.DEVICE_SETTING_PARAMETER_CD IS NOT NULL OR (dsp.DEVICE_SETTING_UI_CONFIGURABLE = 'Y' AND pv_device_setting_value IS NOT NULL))
        AND dsp.device_setting_parameter_cd = pv_device_setting_parameter_cd;
    IF ln_cnt = 0 THEN
        RETURN 'N';
    ELSE
        RETURN 'Y';
    END IF;
END;

	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	IS
		lv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	BEGIN
		SELECT DEVICE_SETTING_VALUE
		INTO lv_device_setting_value
		FROM DEVICE.DEVICE_SETTING
		WHERE DEVICE_ID = pn_device_id
			AND DEVICE_SETTING_PARAMETER_CD = pv_device_setting_parameter_cd;
		
		RETURN lv_device_setting_value;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

PROCEDURE SP_UPDATE_DEVICE_SETTING(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
IS
	ln_exists NUMBER;
BEGIN
	SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, ln_exists);
END;

	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	)
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
			VALUES(pv_device_setting_parameter_cd);
		END IF;
		
		UPDATE device.device_setting
		SET device_setting_value = pv_device_setting_value
		WHERE device_id = pn_device_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
			VALUES(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value);
		ELSE
			pn_exists := 1;
		END IF;
    END;

PROCEDURE SP_UPDATE_DEVICE_SETTINGS
(
    pn_device_id IN device.device_id%TYPE,
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(3000);
    ls_record VARCHAR2(300);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value device_setting.device_setting_value%TYPE;
    ln_return NUMBER := 0;
    ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SELECT file_transfer_type_cd, file_transfer_content
    INTO ln_file_transfer_type_cd, ll_file_transfer_content
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
        DELETE FROM device.device_setting
        WHERE device_id = pn_device_id
            AND device_setting_parameter_cd IN (
                SELECT device_setting_parameter_cd
                FROM device.device_setting_parameter
                WHERE device_setting_ui_configurable = 'Y');
    ELSE
        DELETE FROM device.config_template_setting
        WHERE file_transfer_id = pn_file_transfer_id;
    END IF;

    LOOP
        ls_content := ls_content || SUBSTR(utl_raw.cast_to_varchar2(HEXTORAW(ll_file_transfer_content)), ln_content_pos, ln_content_size);
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
        IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
            ln_return := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            IF ln_return = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
            ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');

            IF NVL(LENGTH(ls_param), 0) > 0 THEN
                INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
                SELECT ls_param FROM dual
                WHERE NOT EXISTS (
                    SELECT 1 FROM device.device_setting_parameter
                    WHERE device_setting_parameter_cd = ls_param);

                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
                    VALUES(pn_device_id, ls_param, ls_value);
                ELSE
                    INSERT INTO device.config_template_setting(file_transfer_id, device_setting_parameter_cd, config_template_setting_value)
                    VALUES(pn_file_transfer_id, ls_param, ls_value);
                END IF;
            END IF;

            IF ln_return = 1 THEN
                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.device_setting
                    WHERE device_id = pn_device_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                ELSE
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.config_template_setting
                    WHERE file_transfer_id = pn_file_transfer_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                END IF;

                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := PKG_CONST.ERROR__NO_ERROR;
                RETURN;
            END IF;
        END LOOP;

    END LOOP;
END;

PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS
(
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
BEGIN
    SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
END;

PROCEDURE SP_GET_HOST_BY_PORT_NUM
(
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_host_id OUT host.host_id%TYPE
)
IS
BEGIN
    SELECT host_id INTO pn_host_id FROM (
        SELECT host_id
        FROM device.host h
        INNER JOIN device.device d ON h.device_id = d.device_id
        WHERE d.device_name = pv_device_name
            AND h.host_port_num = pn_host_port_num
            AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
        ORDER BY h.created_ts DESC
    ) WHERE ROWNUM = 1;
END;

PROCEDURE SP_GET_HOST
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_id OUT host.host_id%TYPE
)
IS
    ln_new_host_count NUMBER;
BEGIN
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__FAILURE;
        pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

        sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                -- fail over to the base host
                sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        sp_create_default_hosts(pn_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                            RETURN;
                        END IF;

                        sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
                            pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
                            RETURN;
                    END;
            END;
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_CREATE_DEFAULT_HOSTS
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_new_host_count                       OUT NUMBER,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
    ln_base_host_count                      NUMBER;
    ln_other_host_count                     NUMBER;
    lv_device_serial_cd                     device.device_serial_cd%TYPE;
    ln_device_type_id                       device.device_type_id%TYPE;
    ln_def_base_host_type_id                device_type.def_base_host_type_id%TYPE;
       ln_def_base_host_equipment_id           device_type.def_base_host_equipment_id%TYPE;
       ln_def_prim_host_type_id                device_type.def_prim_host_type_id%TYPE;
       ln_def_prim_host_equipment_id           device_type.def_prim_host_equipment_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_new_host_count := 0;

    SELECT COUNT(1)
    INTO ln_base_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND (dtht.device_type_host_type_cd = 'B'
            OR h.host_port_num = 0);

    SELECT COUNT(1)
    INTO ln_other_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND dtht.device_type_host_type_cd <> 'B'
       AND h.host_port_num <> 0;

    -- if exists both a base and another host, then there's nothing to do here
    IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        RETURN;
    END IF;

    SELECT
        d.device_serial_cd,
        d.device_type_id,
        dt.def_base_host_type_id,
        dt.def_base_host_equipment_id,
        dt.def_prim_host_type_id,
        dt.def_prim_host_equipment_id
    INTO
        lv_device_serial_cd,
        ln_device_type_id,
        ln_def_base_host_type_id,
           ln_def_base_host_equipment_id,
           ln_def_prim_host_type_id,
           ln_def_prim_host_equipment_id
    FROM device.device_type dt, device.device d
    WHERE d.device_type_id = dt.device_type_id
       AND d.device_id = pn_device_id;

    -- Create base host if it doesn't exist
    IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_serial_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            lv_device_serial_cd,
            0,
            0,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_base_host_type_id,
            ln_def_base_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    -- Create primary host if no other hosts exist
    IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            0,
            1,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_prim_host_type_id,
            ln_def_prim_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE
    IS
        ln_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    BEGIN
        SELECT MAX(HOST_EQUIPMENT_ID), 1
          INTO ln_host_equipment_id, pn_existing_cnt
          FROM DEVICE.HOST_EQUIPMENT
         WHERE HOST_EQUIPMENT_MFGR = pv_host_equipment_mfgr
           AND HOST_EQUIPMENT_MODEL = pv_host_equipment_model;
        IF ln_host_equipment_id IS NULL THEN
            SELECT SEQ_HOST_EQUIPMENT_ID.NEXTVAL, 0
              INTO ln_host_equipment_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST_EQUIPMENT(HOST_EQUIPMENT_ID, HOST_EQUIPMENT_MFGR, HOST_EQUIPMENT_MODEL)
                 VALUES(ln_host_equipment_id, pv_host_equipment_mfgr, pv_host_equipment_model);
        END IF;
        RETURN ln_host_equipment_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, pn_existing_cnt);
    END;
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
    BEGIN
        UPDATE DEVICE.HOST
           SET (HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_ACTIVE_YN_FLAG) =
               (SELECT pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'Y' FROM DUAL)
         WHERE DEVICE_ID = pn_device_id
           AND HOST_PORT_NUM = pn_host_port_num
           AND HOST_POSITION_NUM = pn_host_position_num
           RETURNING HOST_ID, 1 INTO pn_host_id, pn_existing_cnt;
        IF pn_host_id IS NULL THEN
            SELECT SEQ_HOST_ID.NEXTVAL, 0
              INTO pn_host_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST(HOST_ID, DEVICE_ID, HOST_PORT_NUM, HOST_POSITION_NUM, HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_SETTING_UPDATED_YN_FLAG, HOST_ACTIVE_YN_FLAG)
                 VALUES(pn_host_id, pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'N', 'Y');	        		
       END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
        ln_host_equipment_existing_cnt PLS_INTEGER;
    BEGIN
        UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, ln_host_equipment_existing_cnt), pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;

    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
       UPDATE DEVICE.HOST_SETTING
           SET HOST_SETTING_VALUE = pv_host_setting_value
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
	    IF SQL%NOTFOUND THEN
            INSERT INTO DEVICE.HOST_SETTING(HOST_ID, HOST_SETTING_PARAMETER, HOST_SETTING_VALUE)
                VALUES(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
        SELECT MAX(HOST_SETTING_VALUE)
          INTO pv_old_host_setting_value
          FROM DEVICE.HOST_SETTING
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;         
        UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
     PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE)
    IS
        ln_host_id HOST.HOST_ID%TYPE;
    BEGIN
		UPDATE DEVICE.GPRS_DEVICE GD
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = pn_device_id
			AND ICCID NOT IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = GD.DEVICE_ID AND HOST_TYPE_ID = 202);
	
		UPDATE DEVICE.GPRS_DEVICE GD
		   SET RSSI = pn_rssi || ',' || pn_ber, 
               RSSI_TS = pd_update_ts, 
               LAST_FILE_TRANSFER_ID = NULL, 
               MODEM_INFO_RECEIVED_TS = SYSDATE, 
               MODEM_INFO = pv_modem_info,
		   	   DEVICE_ID = pn_device_id, 
               GPRS_DEVICE_STATE_ID = 5,
		       ASSIGNED_BY = DECODE(DEVICE_ID, pn_device_id, ASSIGNED_BY, 'APP_LAYER'), 
		   	   ASSIGNED_TS = DECODE(DEVICE_ID, pn_device_id, ASSIGNED_TS, SYSDATE)
		 WHERE ICCID IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = pn_device_id AND HOST_TYPE_ID = 202)
		   AND (RSSI_TS IS NULL OR RSSI_TS < pd_update_ts);
        SELECT MAX(HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST
         WHERE DEVICE_ID = pn_device_id
           AND HOST_TYPE_ID IN(202,204);
        IF ln_host_id IS NOT NULL THEN
            PKG_DEVICE_CONFIGURATION.UPSERT_HOST_SETTING(ln_host_id,'CSQ',pn_rssi || ',' || pn_ber);
        END IF;
	END;
    
    PROCEDURE APPEND(
        l_text IN VARCHAR2,
        l_clob IN OUT NOCOPY CLOB)
    IS
    BEGIN
        IF l_text IS NOT NULL AND LENGTH(l_text) > 0 THEN
           DBMS_LOB.WRITEAPPEND(l_clob, LENGTH(l_text), l_text);
        END IF;
    END;

    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB)
    IS
        lv_cfg_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        CURSOR l_cur (cv_cfg_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE) IS
            SELECT dsp.DEVICE_SETTING_PARAMETER_CD,
                   DECODE(ds.DEVICE_SETTING_PARAMETER_CD, NULL, cts.CONFIG_TEMPLATE_SETTING_VALUE, ds.DEVICE_SETTING_VALUE) DEVICE_SETTING_VALUE,
                   DECODE(ds.DEVICE_SETTING_PARAMETER_CD, NULL, 'Y', 'N') IS_DEFAULT
              FROM DEVICE.DEVICE_SETTING_PARAMETER dsp
              LEFT JOIN DEVICE.DEVICE_SETTING ds
                ON ds.DEVICE_ID = pn_device_id
               AND ds.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
              LEFT JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts ON cts.FILE_TRANSFER_ID = cv_cfg_file_id
               AND cts.DEVICE_SETTING_PARAMETER_CD = dsp.DEVICE_SETTING_PARAMETER_CD
             WHERE (cts.DEVICE_SETTING_PARAMETER_CD IS NOT NULL OR (dsp.DEVICE_SETTING_UI_CONFIGURABLE = 'Y' AND ds.DEVICE_SETTING_PARAMETER_CD IS NOT NULL))
             ORDER BY CASE WHEN REGEXP_LIKE(NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD), '^[0-9]+$') THEN TO_NUMBER(NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD)) ELSE 9999999999 END,
                    NVL(cts.DEVICE_SETTING_PARAMETER_CD, ds.DEVICE_SETTING_PARAMETER_CD);
    BEGIN
        SELECT ft.FILE_TRANSFER_ID
          INTO lv_cfg_file_id
          FROM DEVICE.DEVICE d
          JOIN DEVICE.FILE_TRANSFER ft
            ON ft.FILE_TRANSFER_NAME = CASE d.DEVICE_TYPE_ID
                    WHEN 0 THEN 'G4-DEFAULT-CFG'
                    WHEN 1 THEN 'G5-DEFAULT-CFG'
                    WHEN 6 THEN 'MEI-DEFAULT-CFG'
                    WHEN 12 THEN 'T2-DEFAULT-CFG'
                    ELSE 'DEFAULT-CFG-' || d.DEVICE_TYPE_ID || '-' || (
                       SELECT dsplv.DEVICE_SETTING_VALUE
                         FROM DEVICE.DEVICE_SETTING dsplv
                        WHERE dsplv.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
                          AND dsplv.DEVICE_ID = d.DEVICE_ID)
                END
            AND ft.FILE_TRANSFER_TYPE_CD IN(6,15,22)
            AND d.DEVICE_ID = pn_device_id;
        DBMS_LOB.CREATETEMPORARY(pl_file, TRUE, DBMS_LOB.CALL);
        FOR l_rec IN l_cur(lv_cfg_file_id) LOOP
            -- For now we just implement the property list format for Edge
            APPEND(l_rec.DEVICE_SETTING_PARAMETER_CD, pl_file);
            APPEND('=', pl_file);
            APPEND(l_rec.DEVICE_SETTING_VALUE, pl_file);
            APPEND(PKG_CONST.ASCII__LF, pl_file);
        END LOOP;
    END;

    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG)
    IS
        lv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_last_lock_utc_ts VARCHAR2(128);
    BEGIN
		SELECT DEVICE_NAME || '-CFG'
		INTO lv_file_transfer_name
		FROM DEVICE.DEVICE
		WHERE DEVICE_ID = pn_device_id;
		
		lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('DEVICE.FILE_TRANSFER', lv_file_transfer_name);

		UPDATE DEVICE.FILE_TRANSFER
		SET FILE_TRANSFER_CONTENT = pl_config_file
		WHERE FILE_TRANSFER_ID = (
			SELECT FILE_TRANSFER_ID FROM (
				SELECT FILE_TRANSFER_ID
				FROM DEVICE.FILE_TRANSFER
				WHERE FILE_TRANSFER_NAME = lv_file_transfer_name
					AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG
				ORDER BY CREATED_TS, FILE_TRANSFER_ID
			) WHERE ROWNUM = 1
		);
		
		IF SQL%NOTFOUND THEN
			INSERT INTO DEVICE.FILE_TRANSFER (
				FILE_TRANSFER_NAME,
				FILE_TRANSFER_TYPE_CD,
				FILE_TRANSFER_CONTENT
			) VALUES (
				lv_file_transfer_name,
				PKG_CONST.FILE_TYPE__CONFIG,
				pl_config_file
			);
		END IF;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_config_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE := pv_device_name || '-CFG';
        pl_config_file CLOB;
        ll_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
        ln_default_file_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
        lv_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        lv_old_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        ln_result_cd NUMBER;
        lv_error_message VARCHAR2(255);
        ln_setting_count NUMBER;
     BEGIN
        IF pn_new_device_type_id = 0 THEN
            lv_default_name := 'G4-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 1 THEN
            lv_default_name := 'G5-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 6 THEN
            lv_default_name := 'MEI-DEFAULT-CFG';
            ln_default_file_type_id := 6;
        ELSIF pn_new_device_type_id = 12 THEN
            lv_default_name := 'T2-DEFAULT-CFG';
            ln_default_file_type_id := 15;
        ELSE
            lv_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_new_property_list_version;
            ln_default_file_type_id := 22;
        END IF;

        SELECT MIN(FILE_TRANSFER_ID)
        INTO pn_file_transfer_id
        FROM DEVICE.FILE_TRANSFER
        WHERE FILE_TRANSFER_NAME = lv_config_name
            AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG;

        -- for legacy only
    IF pn_new_device_type_id in (0, 1, 6) THEN
        IF pn_file_transfer_id is NULL THEN
            SELECT FILE_TRANSFER_CONTENT
            INTO ll_file_transfer_content
            FROM (
                SELECT FILE_TRANSFER_CONTENT
                FROM DEVICE.FILE_TRANSFER
                WHERE FILE_TRANSFER_NAME = lv_default_name
                    AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                ORDER BY CREATED_TS
            ) WHERE ROWNUM = 1;

            SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
              INTO pn_file_transfer_id
              FROM DUAL;

            INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CONTENT)
            VALUES(pn_file_transfer_id, lv_config_name, PKG_CONST.FILE_TYPE__CONFIG, ll_file_transfer_content);
        END IF;

        RETURN;
    ELSE    -- ensure default config exists
        SELECT COUNT(*)
          INTO ln_setting_count
          FROM DEVICE.FILE_TRANSFER
         WHERE FILE_TRANSFER_NAME = lv_default_name
           AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id;
        IF ln_setting_count = 0 THEN
            RAISE_APPLICATION_ERROR(-20700, 'Default Config File ''' || lv_default_name || ''' does not exist');
        END IF;
    END IF;

        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM (SELECT DEVICE_ID
             FROM DEVICE.DEVICE
            WHERE DEVICE_NAME = pv_device_name
            ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;

        IF ln_device_id IS NOT NULL THEN
            SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
              INTO pn_old_property_list_version
              FROM DEVICE.DEVICE_SETTING ds
             WHERE ds.DEVICE_ID = ln_device_id
               AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
               AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
            IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(pn_old_property_list_version, -1) THEN
                SP_UPDATE_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__PROPERTY_LIST_VERSION, pn_new_property_list_version);
				
               -- Update all values that equal the default in the old version and where the default in the new version is differnt
               lv_old_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_old_property_list_version;
               MERGE INTO DEVICE.DEVICE_SETTING O
                 USING (
                       SELECT ln_device_id DEVICE_ID, cts.DEVICE_SETTING_PARAMETER_CD SETTING_NAME, cts.CONFIG_TEMPLATE_SETTING_VALUE SETTING_VALUE
                        FROM DEVICE.FILE_TRANSFER ft
                        JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
                          ON cts.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
                        JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts0
                          ON cts0.DEVICE_SETTING_PARAMETER_CD = cts.DEVICE_SETTING_PARAMETER_CD
                        JOIN DEVICE.FILE_TRANSFER ft0
                          ON cts0.FILE_TRANSFER_ID = ft0.FILE_TRANSFER_ID
                        JOIN DEVICE.DEVICE_SETTING ds
                          ON cts.DEVICE_SETTING_PARAMETER_CD = ds.DEVICE_SETTING_PARAMETER_CD
                       WHERE cts.CONFIG_TEMPLATE_SETTING_VALUE != cts0.CONFIG_TEMPLATE_SETTING_VALUE
                         AND cts0.CONFIG_TEMPLATE_SETTING_VALUE = ds.DEVICE_SETTING_VALUE
                         AND cts.DEVICE_SETTING_PARAMETER_CD NOT IN('50','51','52','60','61','62','63','64','80','81','70','100','101','102','103','104','105','106','107','108','200','201','202','203','204','205','206','207','208','300','301')
                         AND TO_NUMBER_OR_NULL(cts.DEVICE_SETTING_PARAMETER_CD) IS NOT NULL
                         AND ft.FILE_TRANSFER_NAME = lv_default_name
                         AND ft.FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                         AND ft0.FILE_TRANSFER_NAME = lv_old_default_name
                         AND ft0.FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                         AND ds.DEVICE_ID = ln_device_id) N
                      ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
                      WHEN MATCHED THEN
                       UPDATE
                          SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
                      WHEN NOT MATCHED THEN
                       INSERT (O.DEVICE_ID,
                               O.DEVICE_SETTING_PARAMETER_CD,
                               O.DEVICE_SETTING_VALUE)
                        VALUES(N.DEVICE_ID,
                               N.SETTING_NAME,
                               N.SETTING_VALUE
                        );
               --Create config file
               SP_CONSTRUCT_PROPERTIES_FILE(ln_device_id, pl_config_file);
               ll_file_transfer_content := LOB_TO_HEX_LONG(pl_config_file);
            END IF;
        END IF;

        -- handle change of property list version
        IF ll_file_transfer_content IS NULL AND (ln_device_id IS NULL OR pn_file_transfer_id IS NULL) THEN
            -- Use default config file
            SELECT FILE_TRANSFER_CONTENT
            INTO ll_file_transfer_content
            FROM (
                SELECT FILE_TRANSFER_CONTENT
                FROM DEVICE.FILE_TRANSFER
                WHERE FILE_TRANSFER_NAME = lv_default_name
                    AND FILE_TRANSFER_TYPE_CD = ln_default_file_type_id
                ORDER BY CREATED_TS
            ) WHERE ROWNUM = 1;
         END IF;

        IF ll_file_transfer_content IS NOT NULL THEN
            IF pn_file_transfer_id IS NULL THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CONTENT)
                   VALUES(pn_file_transfer_id, lv_config_name, PKG_CONST.FILE_TYPE__CONFIG, ll_file_transfer_content);
            ELSE
               UPDATE DEVICE.FILE_TRANSFER
                  SET FILE_TRANSFER_CONTENT = ll_file_transfer_content
                WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
            END IF;

            SP_UPDATE_DEVICE_SETTINGS(ln_device_id, pn_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
            IF ln_result_cd = PKG_CONST.RESULT__SUCCESS THEN
                pn_updated := PKG_CONST.BOOLEAN__TRUE;
            ELSE
                pn_updated := PKG_CONST.BOOLEAN__FALSE;
            END IF;
        ELSE
            pn_updated := PKG_CONST.BOOLEAN__FALSE;
        END IF;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER)
    IS
        ln_old_property_list_version NUMBER;
    BEGIN
        SP_INITIALIZE_CONFIG_FILE(pv_device_name, pn_new_device_type_id, pn_new_property_list_version, pn_file_transfer_id, pn_updated, ln_old_property_list_version);
    END;

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE
        )
    IS
        ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
    BEGIN
      ADD_PENDING_FILE_TRANSFER(
        pv_device_name,pv_file_transfer_name,pn_file_transfer_type_id,
        pc_data_type,pn_file_transfer_group,pn_file_transfer_packet_size,pn_execute_order,pn_command_id, pn_file_transfer_id,
        ll_command);
        
    END; -- stub for ADD_PENDING_FILE_TRANFER without returned data parameters
    
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
        )
    IS
        ln_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
          INTO pn_file_transfer_id, ln_dft_id, pn_command_id, ln_device_id
          FROM (SELECT DEVICE_ID
          FROM DEVICE.DEVICE
         WHERE DEVICE_NAME = pv_device_name
         ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
           VALUES(pn_file_transfer_id, pn_file_transfer_type_id, pv_file_transfer_name);
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
            VALUES(ln_dft_id, ln_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
           VALUES(pn_command_id, pv_device_name, pc_data_type, TO_CHAR(ln_dft_id), 'S', NVL(pn_execute_order, 999), SYSDATE)
           RETURNING TRIM(COMMAND) into pl_command;
    END;

    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT BLOB,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
         l_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT FILE_TRANSFER_ID, CREATED_TS
        INTO l_file_transfer_id, pd_file_transfer_create_ts
        FROM (
            SELECT FILE_TRANSFER_ID, CREATED_TS
            FROM DEVICE.FILE_TRANSFER
            WHERE FILE_TRANSFER_NAME = pv_file_transfer_name
                AND FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id
            ORDER BY CREATED_TS
        ) WHERE ROWNUM = 1;
        SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
          INTO l_dft_id
          FROM DEVICE.DEVICE_FILE_TRANSFER dft
          JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
         WHERE d.DEVICE_NAME = pv_device_name
           AND dft.FILE_TRANSFER_ID = l_file_transfer_id
           AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
           AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
       IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
         SELECT MAX(mcp.MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
           INTO pn_mcp_id
           FROM ENGINE.MACHINE_CMD_PENDING mcp
          WHERE mcp.MACHINE_ID = pv_device_name
            AND mcp.EXECUTE_CD IN('P', 'S')
            AND UPPER(mcp.DATA_TYPE) IN('7C', 'A4', 'C7', 'C8')
            AND TRIM(mcp.COMMAND) = TRIM(TO_CHAR(l_dft_id));
        ELSE
            SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
              INTO l_dft_id, l_device_id
              FROM (SELECT DEVICE_ID
              FROM DEVICE.DEVICE
             WHERE DEVICE_NAME = pv_device_name
             ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
             WHERE ROWNUM = 1;
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
              VALUES(l_dft_id, l_device_id, l_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        END IF;
        IF pn_mcp_id IS NULL THEN
            SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
              INTO pn_mcp_id
              FROM DUAL;
            INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
              VALUES(pn_mcp_id, pv_device_name, pc_data_type, TO_CHAR(l_dft_id), 'S', 999, SYSDATE);
        END IF;
        SP_GET_FILE_TRANSFER_BLOB(l_file_transfer_id, pl_file_transfer_content);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
               NULL; -- let variables be null
    END;

    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pl_file_content_hex FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    )
    IS
        ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
		ln_device_id := GET_DEVICE_ID_BY_NAME(pv_device_name, pd_file_transfer_ts);
		
		-- duplicate detection of already stored files re-delivered by QueueLayer
		SELECT MAX(DEVICE_FILE_TRANSFER_ID)
		INTO ln_device_file_transfer_id
		FROM DEVICE.DEVICE_FILE_TRANSFER
		WHERE DEVICE_ID = ln_device_id
			AND DEVICE_SESSION_ID = pn_session_id
			AND DEVICE_FILE_TRANSFER_TS = pd_file_transfer_ts;
		IF ln_device_file_transfer_id IS NOT NULL THEN
			RETURN;
		END IF;
		
        BEGIN
            SELECT FILE_TRANSFER_ID
              INTO pn_file_transfer_id
              FROM (SELECT ft.FILE_TRANSFER_ID
              FROM DEVICE.FILE_TRANSFER ft
              LEFT JOIN DEVICE.DEVICE_FILE_TRANSFER dft on ft.FILE_TRANSFER_ID = dft.FILE_TRANSFER_ID
             WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
               AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd
               AND NVL(dft.DEVICE_FILE_TRANSFER_DIRECT, 'I') = 'I'
               AND NVL(dft.DEVICE_FILE_TRANSFER_STATUS_CD, 1) = 1
               AND pc_overwrite_flag = 'Y'
             ORDER BY ft.FILE_TRANSFER_ID DESC)
             WHERE ROWNUM = 1;
            UPDATE DEVICE.FILE_TRANSFER SET FILE_TRANSFER_CONTENT = pl_file_content_hex
             WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER (
                    FILE_TRANSFER_ID,
                    FILE_TRANSFER_NAME,
                    FILE_TRANSFER_TYPE_CD,
                    FILE_TRANSFER_CONTENT)
                  VALUES(
                    pn_file_transfer_id,
                    pv_file_transfer_name,
                    pn_file_transfer_type_cd,
                    pl_file_content_hex);
        END;
        SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL
          INTO ln_device_file_transfer_id
          FROM DUAL;
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
            DEVICE_FILE_TRANSFER_ID,
            DEVICE_ID,
            FILE_TRANSFER_ID,
            DEVICE_FILE_TRANSFER_DIRECT,
            DEVICE_FILE_TRANSFER_STATUS_CD,
            DEVICE_FILE_TRANSFER_TS,
            DEVICE_SESSION_ID)
         VALUES(
            ln_device_file_transfer_id,
            ln_device_id,
            pn_file_transfer_id,
            'I',
            1,
            pd_file_transfer_ts,
            pn_session_id);
        IF pv_event_global_trans_cd IS NOT NULL THEN
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID, DEVICE_FILE_TRANSFER_ID)
              SELECT EVENT_ID, ln_device_file_transfer_id
                FROM DEVICE.EVENT
                WHERE EVENT_GLOBAL_TRANS_CD = pv_event_global_trans_cd;
        END IF;        
    END;
	
	FUNCTION GET_NORMALIZED_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2
	) RETURN VARCHAR2
	IS
		ln_call_in_start_min NUMBER;
		ln_call_in_end_min NUMBER;
		ln_call_in_window_min NUMBER;
		ln_call_in_time_min NUMBER;
		ln_call_in_window_start NUMBER;
		ln_call_in_window_hours NUMBER;
		ln_seed_4 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -4)), 0);
		ln_convert_time_zone NUMBER := PKG_CONST.BOOLEAN__TRUE;
	BEGIN
		ln_call_in_window_start := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_start_cd)), -1));
		ln_call_in_window_hours := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_hours_cd)), 0));
		IF ln_call_in_window_start BETWEEN 0 AND 2359 AND ln_call_in_window_hours BETWEEN 1 AND 23 THEN
			ln_call_in_start_min := FLOOR(ln_call_in_window_start / 100) * 60 + MOD(ln_call_in_window_start, 100);
			ln_call_in_window_min := ln_call_in_window_hours * 60;
			ln_convert_time_zone := PKG_CONST.BOOLEAN__FALSE;
		ELSE
			ln_call_in_start_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_start_min_cd));
			ln_call_in_end_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_end_min_cd));
			
			IF ln_call_in_start_min < 0 OR ln_call_in_start_min = ln_call_in_end_min THEN
				RETURN NULL;
			END IF;

			ln_call_in_window_min := MOD(ln_call_in_end_min - ln_call_in_start_min, 1440);
			IF ln_call_in_window_min < 0 THEN
				ln_call_in_window_min := ln_call_in_window_min + 1440;
			END IF;
		END IF;
			
		-- use last 4 digits of device serial number to calculate its call-in time
		ln_call_in_time_min := MOD(ln_call_in_start_min + ROUND(ln_call_in_window_min * ln_seed_4 / 9999), 1440);
		
		IF ln_convert_time_zone = PKG_CONST.BOOLEAN__TRUE THEN
			-- convert call-in time from server time zone to device local time zone
			ln_call_in_time_min := MOD(ln_call_in_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(pv_device_time_zone_guid, PKG_CONST.DB_TIME_ZONE), 1440);
			IF ln_call_in_time_min < 0 THEN
				ln_call_in_time_min := ln_call_in_time_min + 1440;
			END IF;
		END IF;
		
		RETURN LPAD(FLOOR(ln_call_in_time_min / 60), 2, '0') || LPAD(MOD(ln_call_in_time_min, 60), 2, '0');			
	END;	

    PROCEDURE NORMALIZE_SCHEDULE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_schedule_type DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_normalized_offset NUMBER,
		pn_result_cd IN OUT NUMBER,
		pv_schedule IN OUT DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	)
    IS
		lv_current_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_interval NUMBER;
		ln_min_allowed_interval NUMBER;
		lc_reoccurrence_type CHAR(1);
		ln_exists NUMBER;
		lv_normalized_time VARCHAR2(4);
		lv_schedule DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE := NULL;
    BEGIN
		lv_current_schedule := NVL(GET_DEVICE_SETTING(pn_device_id, pv_schedule_type), '0');
		IF lv_current_schedule = '0' THEN
			IF pv_schedule_type = PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED THEN
				lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__INTERVAL || PKG_CONST.SCHEDULE__FS || pn_normalized_offset || PKG_CONST.SCHEDULE__FS || DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EDGE_NON_ACTIVATED_CALL_IN_INTERVAL_SEC');
			ELSIF pv_schedule_type = PKG_CONST.DSP__SETTLEMENT_SCHEDULE THEN
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__WEEKLY || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || PKG_CONST.SCHEDULE__SUNDAY;
				END IF;
			ELSE
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		ELSE
			lc_reoccurrence_type := SUBSTR(lv_current_schedule, 1, 1);
			IF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__INTERVAL THEN
				ln_current_interval := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1)), -1);
				ln_min_allowed_interval := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INTERVAL_SCHEDULE_SEC'));
				IF ln_current_interval < ln_min_allowed_interval THEN
					ln_current_interval := ln_min_allowed_interval;
				END IF;
				lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || pn_normalized_offset || PKG_CONST.SCHEDULE__FS || ln_current_interval;
			ELSIF lc_reoccurrence_type IN (PKG_CONST.REOCCURRENCE_TYPE__MONTHLY, PKG_CONST.REOCCURRENCE_TYPE__WEEKLY) THEN
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1);
				END IF;
			ELSE
				lv_normalized_time := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		END IF;
		IF lv_schedule IS NOT NULL AND lv_schedule != lv_current_schedule THEN
			SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_schedule_type, lv_schedule, ln_exists);
			pn_result_cd := 1;
			pv_schedule := lv_schedule;
		END IF;		
    END;
	
    /**
    * r28 compatible call-in normalization version
    */ 
  PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	)
	IS
    ln_command_id       ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    lv_data_type        ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
    ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
  BEGIN
    SP_NORMALIZE_CALL_IN_TIME(
      pn_device_id, 
      pn_device_type_id, 
      pv_device_serial_cd, 
      pv_device_time_zone_guid, 
      pn_result_cd, 
      pv_activated_call_in_schedule, 
      pv_non_activ_call_in_schedule, 
      pv_settlement_schedule, 
      pv_dex_schedule,
      ln_command_id, 
      lv_data_type, 
      ll_command);
  END; -- SP_NORMALIZE_CALL_IN_TIME stub
  
  /**
    * r29 Call-in normalization version
    */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	)
	IS
		lv_return_msg VARCHAR2(2048);
		ll_file CLOB;
		ln_seed_5 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -5)), 0);
		ln_normalized_offset NUMBER;
	BEGIN
		-- normalize device call-in time to avoid load peaks
		pn_result_cd := 0;
		pv_activated_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_non_activ_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_settlement_schedule := PKG_CONST.ASCII__NUL;
		pv_dex_schedule := PKG_CONST.ASCII__NUL;
		
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, 'GX_CALL_IN_NORMALIZER_START_MIN', 'GX_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule BETWEEN '0000' AND '0004' THEN
                pv_activated_call_in_schedule := '001' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
            ELSIF pv_activated_call_in_schedule BETWEEN '2356' AND '2359' THEN
                pv_activated_call_in_schedule := '234' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
            END IF;
			SP_UPDATE_CONFIG_AND_RETURN(pn_device_id, 172, pv_activated_call_in_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
			ln_normalized_offset := ROUND(86400 * ln_seed_5 / 99999);
			
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_activated_call_in_schedule);
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_non_activ_call_in_schedule);
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__SETTLEMENT_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_settlement_schedule);

			IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('NORMALIZE_EDGE_DEX_SCHEDULE_FLAG') = 'Y'
				AND NVL(GET_DEVICE_SETTING(pn_device_id, PKG_CONST.DSP__VMC_INTERFACE_TYPE), PKG_CONST.VMC_INTERFACE__STANDARD_MDB) = PKG_CONST.VMC_INTERFACE__STANDARD_MDB THEN
				NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__DEX_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
					'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid, ln_normalized_offset, pn_result_cd, pv_dex_schedule);
			END IF;
				
			IF pn_result_cd = 1 THEN
				SP_CONSTRUCT_PROPERTIES_FILE(pn_device_id, ll_file);
				SP_UPDATE_DEVICE_CONFIG_FILE(pn_device_id, LOB_TO_HEX_LONG(ll_file));
			END IF;
		END IF;
		
		UPDATE DEVICE.DEVICE
		SET CALL_IN_TIME_NORMALIZED_TS = SYSDATE
		WHERE DEVICE_ID = pn_device_id;
	END;

/**
  * r28 compatible version of SP_NEXT_PENDING_COMMAND
  * Does not support the stateful session attribute parameter
  */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL)
    IS
        lv_session_attributes VARCHAR2(20); -- contains character 'C' if configuration has been requested previously
    BEGIN
    
    lv_session_attributes := 'CGPE'; -- ensure that we don't continual do the same command
    SP_NEXT_PENDING_COMMAND(
      pv_device_name, 
      pn_command_id, 
      pv_data_type, 
      pr_command_bytes,
      pn_file_transfer_id,
      pv_file_transfer_name,
      pn_file_transfer_type_id,
      pl_file_transfer_content,
      pn_file_transfer_group_num,
      pn_file_transfer_pkt_size,
      pd_file_transfer_created_ts,
      pn_max_execute_order,
      pn_priority_command_id,
      lv_session_attributes);
  END; --    SP_NEXT_PENDING_COMMAND 
         
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for ESUDS Diagnostics, if existing diagnostics are beyond 16 hours old.
    */
    PROCEDURE SP_NPC_HELPER_ESUDS_DIAG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'E' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_oldest_diag DATE;
    BEGIN
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__ESUDS) AND INSTR(NVL(pv_session_attributes,'-'),'E') = 0 THEN        
            pv_session_attributes := NVL(pv_session_attributes,'') || 'E';       
            -- if any records for the given device are old, (no records = old records)       
            SELECT MIN(MOST_RECENT_PORT_DIAG_DATE) 
              INTO ld_oldest_diag
              FROM (
                  SELECT d.device_id, h.host_port_num, MAX(NVL(hds.host_diag_last_reported_ts, MIN_DATE)) AS MOST_RECENT_PORT_DIAG_DATE
                    FROM device d  
                    LEFT OUTER JOIN host h ON (d.device_id = h.device_id) 
                    LEFT OUTER JOIN host_diag_status hds ON (hds.host_id = h.host_id) 
                    WHERE d.device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS
                    AND d.device_name = pv_device_name
                    AND d.device_active_yn_flag = 'Y'
                    AND NVL(h.host_active_yn_flag,'Y') = 'Y'
                    AND h.host_port_num <> 0
                    GROUP BY d.device_id, h.host_port_num            
              );
    
            IF ld_oldest_diag < SYSDATE - 1 THEN           
                pv_data_type := '9A61';
                pl_command := '';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);                                   
            END IF;
		END IF;  
    END; -- SP_NPC_HELPER_ESUDS_DIAGNOSTICS

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for GPRS configuration files, if needed.
    */
    PROCEDURE SP_NPC_HELPER_MODEM(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration test has been requested previously, R if configuration requested       
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_update_ts DATE;
    BEGIN
        -- For Gx and MEI, test to see if modem data is outdated by 7 days. If so, send the appropriate message (both happen to be 87)	
        -- Some G4's only send garbage for modem info, so don't keep requesting it for G4 (PKG_CONST.DEVICE_TYPE__G4)
    	IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI) AND INSTR(NVL(pv_session_attributes,'-'),'C') = 0 THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'C';
            SELECT NVL(MAX(H.LAST_UPDATED_TS), MIN_DATE)
              INTO ld_last_update_ts
              FROM DEVICE.DEVICE D
              JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
               AND H.HOST_PORT_NUM = 2;
            IF ld_last_update_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_MODEM_INFO_REFRESH_HR')) / 24, 7) THEN				
                pv_data_type := '87';					
                IF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI) THEN 
                    pl_command := '4200000000000000FF';							
                ELSIF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__GX OR pn_device_type_id = PKG_CONST.DEVICE_TYPE__G4) THEN 
                    pl_command := '4400802C0000000190';
                END IF;
				pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
            END IF;
        END IF;
    END;

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for G4 configuration files, if existing ones are stale.
    */
    PROCEDURE SP_NPC_HELPER_STALE_CONFIG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'G' if GX configuration has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_config_ts DATE;
    BEGIN
        -- For MEI/Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI, PKG_CONST.DEVICE_TYPE__G4) AND INSTR(NVL(pv_session_attributes,'-'),'G') = 0 THEN              
	        pv_session_attributes := NVL(pv_session_attributes,'') || 'G';	        
	        IF NOT(pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI AND INSTR(NVL(pv_session_attributes,'-'), 'R') > 0 ) THEN	        	 
	        	-- Now check last configuration received timestamp 	        	
		        SELECT NVL(D.RECEIVED_CONFIG_FILE_TS, MIN_DATE)
		          INTO ld_last_config_ts
		          FROM DEVICE.DEVICE D 
		         WHERE D.DEVICE_NAME = pv_device_name 
		           AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
                IF ld_last_config_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CONFIG_REFRESH_HR')) / 24, 30) THEN				
                    pv_data_type := '87';
					-- same command for all devices handled by this procedure					
					pl_command := '4200000000000000FF';		
					pv_session_attributes := NVL(pv_session_attributes,'') || 'R';	
					UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);	                						
				END IF; -- actual test	
			END IF; -- 'R' flag			
    	END IF; -- 'G' flag    	
    END; -- SP_NPC_HELPER_STALE_CONFIG
    
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands to set the Gx verbosity flag 
    * on a non-remotely updatable device.
    */
    PROCEDURE SP_NPC_HELPER_PHILLY_COKE(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'P' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pv_firmware_version IN DEVICE.FIRMWARE_VERSION%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        l_is_philly_coke NUMBER;
    BEGIN
        -- For Gx test age of configuration data
        IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX) AND INSTR(NVL(pv_session_attributes,'-'),'P') = 0 AND REGEXP_LIKE(pv_firmware_version, 'USA-G5[0-9] ?[vV]4\.2\.(0|1[A-S]).*') THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'P';      
            pv_data_type := '88';
            pl_command := '4400807FE001';
            UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 2, pn_commands_inserted, pn_command_id);
		END IF;   
    END; -- SP_NPC_HELPER_PHILLY_COKE

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT BLOB,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2 -- contains character 'C' if configuration has been requested previously       
        )
    IS
        ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
        ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ld_call_in_time_normalized_ts DEVICE.CALL_IN_TIME_NORMALIZED_TS%TYPE;
		lv_device_time_zone_guid VARCHAR2(60);
		ln_result_cd NUMBER;
		lv_activated_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_non_activ_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_settlement_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_dex_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        l_addr NUMBER;
        l_len NUMBER;
        l_total_len NUMBER;
        l_pos NUMBER;
        l_commands_inserted NUMBER;
		ln_sending_file_cnt NUMBER;
		ln_sending_file_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DOWNLOAD_LIMIT_APP_UPGRADE'));
    BEGIN
		-- throttle the number of concurrent app upgrade file downloads
		SELECT COUNT(1) INTO ln_sending_file_cnt
		FROM engine.machine_cmd_pending mcp
		JOIN device.device_file_transfer dft ON mcp.command = dft.device_file_transfer_id
		JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
		WHERE mcp.execute_cd = 'S'
			AND mcp.execute_date > SYSDATE - 15/1440
			AND UPPER(mcp.data_type) IN('7C', 'A4', 'C8')
			AND REGEXP_LIKE(mcp.command, '^[0-9]+$')
			AND ft.file_transfer_type_cd = PKG_CONST.FILE_TYPE__APP_UPGRADE;
	
        UPDATE ENGINE.MACHINE_CMD_PENDING MCP
           SET EXECUTE_CD = 'S',
               EXECUTE_DATE = SYSDATE
         WHERE MCP.MACHINE_ID = pv_device_name
           AND MCP.EXECUTE_ORDER <= pn_max_execute_order
           AND MCP.EXECUTE_CD IN('S', 'P')
		   AND (ln_sending_file_cnt < ln_sending_file_limit
				OR UPPER(MCP.DATA_TYPE) NOT IN('7C', 'A4', 'C8')
				OR UPPER(MCP.DATA_TYPE) IN('7C', 'A4', 'C8')
					AND REGEXP_LIKE(MCP.COMMAND, '^[0-9]+$')
					AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.COMMAND
							AND FT.FILE_TRANSFER_TYPE_CD != PKG_CONST.FILE_TYPE__APP_UPGRADE
					)
			)
           AND NOT EXISTS(
                        SELECT 1
                          FROM ENGINE.MACHINE_CMD_PENDING MCP2
                         WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
                           AND MCP2.EXECUTE_CD IN('S', 'P')
                           AND MCP.MACHINE_COMMAND_PENDING_ID != MCP2.MACHINE_COMMAND_PENDING_ID
                           AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
                           AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
                               OR MCP2.MACHINE_COMMAND_PENDING_ID = pn_priority_command_id
                               OR (MCP.MACHINE_COMMAND_PENDING_ID != NVL(pn_priority_command_id, 0)
                                    AND MCP.CREATED_TS > MCP2.CREATED_TS
                                    OR (MCP.CREATED_TS = MCP2.CREATED_TS AND MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))))
            RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, TRIM(COMMAND) INTO pn_command_id, pv_data_type, ll_command;
			
		IF pn_command_id IS NULL OR pv_data_type = '88' THEN
			SELECT D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SERIAL_CD, D.CALL_IN_TIME_NORMALIZED_TS, TZ.TIME_ZONE_GUID, D.FIRMWARE_VERSION
			  INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, ld_call_in_time_normalized_ts, lv_device_time_zone_guid, lv_firmware_version
              FROM DEVICE.DEVICE D
			  JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
			  JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
			  JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
		END IF;
		
		IF pn_command_id IS NULL THEN
			IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__EDGE)
				AND NVL(ld_call_in_time_normalized_ts, MIN_DATE) < SYSDATE - TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CALL_IN_NORMALIZATION_INTERVAL_HR')) / 24 THEN
				SP_NORMALIZE_CALL_IN_TIME(ln_device_id, ln_device_type_id, lv_device_serial_cd, lv_device_time_zone_guid, ln_result_cd, lv_activated_call_in_schedule, lv_non_activ_call_in_schedule, lv_settlement_schedule, lv_dex_schedule, pn_command_id, pv_data_type, ll_command);
				IF ln_result_cd = 1 THEN       
                    IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
                        pv_data_type := 'C7';
                        ADD_PENDING_FILE_TRANSFER(pv_device_name, pv_device_name || '-CFG-' || DBADMIN.TIMESTAMP_TO_MILLIS(SYSTIMESTAMP), 
                          PKG_CONST.FILE_TYPE__PROPERTY_LIST, pv_data_type, 0, 1024, 1, ln_command_id, ln_file_transfer_id, ll_command);
                        pn_command_id := ln_command_id; 
                        
                        UPDATE DEVICE.FILE_TRANSFER
                        SET FILE_TRANSFER_CONTENT = RAWTOHEX(DECODE(lv_settlement_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__SETTLEMENT_SCHEDULE || '=' || lv_settlement_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_non_activ_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED || '=' || lv_non_activ_call_in_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_activated_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED || '=' || lv_activated_call_in_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_dex_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__DEX_SCHEDULE || '=' || lv_dex_schedule || PKG_CONST.ASCII__LF))
                        WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
                    ELSE
                        UPDATE ENGINE.MACHINE_CMD_PENDING MCP
                           SET EXECUTE_CD = 'S',
                               EXECUTE_DATE = SYSDATE
                         WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
                    END IF;
                END IF;
            END IF;
            IF pn_command_id IS NULL THEN
                SP_NPC_HELPER_MODEM(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command); 
                IF pn_command_id IS NULL THEN
                    SP_NPC_HELPER_STALE_CONFIG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
                    IF pn_command_id IS NULL THEN
                        SP_NPC_HELPER_ESUDS_DIAG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
                        IF pn_command_id IS NULL THEN
                            SP_NPC_HELPER_PHILLY_COKE(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, lv_firmware_version, l_commands_inserted, ll_command);
                            IF pn_command_id IS NULL THEN
                                RETURN;
                            END IF;
                        END IF;
                    END IF;
                END IF;
            END IF;
        END IF;
        IF UPPER(pv_data_type) IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(ll_command, '^[0-9]+$') THEN
            SELECT ft.FILE_TRANSFER_ID, ft.FILE_TRANSFER_NAME, ft.FILE_TRANSFER_TYPE_CD,
                   dft.DEVICE_FILE_TRANSFER_GROUP_NUM, dft.DEVICE_FILE_TRANSFER_PKT_SIZE, dft.CREATED_TS
              INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
                   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
              FROM DEVICE.DEVICE_FILE_TRANSFER dft
              JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
             WHERE DEVICE_FILE_TRANSFER_ID = TO_NUMBER(ll_command);

            SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
        ELSIF UPPER(pv_data_type) = '88' THEN
            l_addr :=to_number(substr(ll_command, 3,8),'XXXXXXXX');
            IF substr(ll_command, 1, 2) = '42' THEN -- EEROM: get data from file transfer            
                l_len := to_number(substr(ll_command, 11,8),'XXXXXXXX');
    
                IF ln_device_type_id in (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
                      l_pos :=l_addr*2;
                ELSE
                      l_pos :=l_addr;
                END IF;
    
                IF l_pos>0 THEN
                    l_pos :=l_pos+1;
                END IF;
    
                select file_transfer_id into pn_file_transfer_id
                from (
                    select file_transfer_id from device.file_transfer
                    where file_transfer_name = pv_device_name||'-CFG'
                        and file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG
                    order by created_ts
                ) where rownum = 1;
    
                SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
                l_total_len :=UTL_RAW.LENGTH(pl_file_transfer_content);
                IF (l_pos+l_len) > l_total_len THEN
                        pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1,10)), UTL_RAW.SUBSTR(pl_file_transfer_content, l_pos)) ;
                ELSE
                        pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1,10)), UTL_RAW.SUBSTR(pl_file_transfer_content, l_pos, l_len)) ;
                END IF;
            ELSE
                pr_command_bytes := HEXTORAW(ll_command);
            END IF;
        ELSE
            pr_command_bytes := HEXTORAW(ll_command);
        END IF;
    END;

    /**
      * r28 compatible version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER)
    IS
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
      UPSERT_PENDING_COMMAND(pv_device_name,pv_date_type,pv_command,'P',pv_execute_order,pn_rows_inserted,ln_command_id);
    END;
    
    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
      ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        select SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL into ln_command_id from DUAL;
        
        -- This isn't atomic so could result in duplicate pending commands but it's not critical to avoid duplicates only nice
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_DATE, EXECUTE_ORDER)
            SELECT ln_command_id, pv_device_name, pv_data_type, pv_command, pv_execute_cd, DECODE(pv_execute_cd, 'S', SYSDATE, NULL), pv_execute_order FROM DUAL
             WHERE NOT EXISTS(SELECT 1 FROM ENGINE.MACHINE_CMD_PENDING
                               WHERE MACHINE_ID = pv_device_name
                                 AND DATA_TYPE = pv_data_type
                                 AND COMMAND = pv_command
                                 AND EXECUTE_CD in ('P', 'S'));
        
        pn_rows_inserted := SQL%ROWCOUNT;
        
        if pn_rows_inserted = 1 THEN
          pn_command_id := ln_command_id;
        END IF;
    END;

  /**
    * pre-R28 compatible version
    */
     PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999)
    IS
        ln_rows_inserted PLS_INTEGER;
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        UPSERT_PENDING_COMMAND(pv_device_name, pv_date_type, pv_command, 'P', pv_execute_order, ln_rows_inserted, ln_command_id);
    END;
    
    PROCEDURE CONFIG_POKE
    (pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE)
    IS
        l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
        l_file_len NUMBER;
        l_num_parts NUMBER;
        l_pos NUMBER;
        l_poke_size NUMBER:=200;
        l_len NUMBER;
    BEGIN
        IF pv_device_type_id in (0, 1) THEN
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND EXECUTE_ORDER > 2
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 0);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 1);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 2);
        ELSE
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            select FILE_CONTENT_LENGTH(rowid)/2 into l_file_len
            from (
                select rowid from device.file_transfer
                where file_transfer_name = pv_device_name||'-CFG'
                    and file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG
                order by created_ts
            ) where rownum = 1;

            l_num_parts:= CEIL(l_file_len/l_poke_size)-1;

            FOR i in 0 .. l_num_parts LOOP
                l_pos:= i*l_poke_size;
                IF (l_pos+l_poke_size) > l_file_len THEN
                    l_len:= l_file_len-l_pos;
                ELSE
                    l_len:= l_poke_size;
                END IF;

                l_command:= '42'||LPAD(PKG_CONVERSIONS.TO_HEX(l_pos), 8, '0')||LPAD(PKG_CONVERSIONS.TO_HEX(l_len), 8, '0');

                UPSERT_PENDING_COMMAND(pv_device_name, '88', l_command, i);
            END LOOP;

        END IF;
    END;
    
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_NAME = pv_device_name
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_NAME ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_NAME = pv_device_name)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
            IF ln_device_id IS NULL THEN
                SELECT MAX(DEVICE_ID)
                  INTO ln_device_id
                  FROM (SELECT DEVICE_ID
                          FROM DEVICE.DEVICE
                         WHERE DEVICE_NAME = pv_device_name
                           AND CREATED_TS > pd_effective_date
                         ORDER BY CREATED_TS ASC)
                 WHERE ROWNUM = 1;
            END IF;
        END IF;
        RETURN ln_device_id;        
    END;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_SERIAL_CD = pv_device_serial_cd
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_SERIAL_CD ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
        END IF;
        RETURN ln_device_id;        
    END;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_EVENT.psk?rev=1.5
CREATE OR REPLACE PACKAGE DEVICE.PKG_EVENT   IS

FUNCTION SF_GET_GLOBAL_EVENT_CD
(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN event.event_device_tran_cd%TYPE
) RETURN VARCHAR2;

PROCEDURE SP_GET_OR_CREATE_EVENT
(
    pc_global_event_cd_prefix IN CHAR,
    pn_session_id IN NUMBER,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_event_type_id IN event_type.event_type_id%TYPE,
    pn_event_type_host_type_num IN event_type_host_type.event_type_host_type_num%TYPE,
    pn_event_state_id IN event.event_state_id%TYPE,
    pv_event_device_tran_cd IN event.event_device_tran_cd%TYPE,
    pn_event_end_flag IN NUMBER,
    pn_event_upload_complete_flag IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_id	OUT event.event_id%TYPE,
    pn_exists OUT NUMBER,
    pd_event_start_ts IN event.event_start_ts%TYPE DEFAULT SYSDATE,
    pd_event_end_ts IN event.event_end_ts%TYPE DEFAULT SYSDATE
);

PROCEDURE SP_GET_OR_CREATE_EVENT_DETAIL
(
    pn_event_id IN event.event_id%TYPE,
    pn_event_detail_type_id IN event_detail.event_detail_type_id%TYPE,
    pv_event_detail_value IN event_detail.event_detail_value%TYPE,
    pv_event_detail_value_ts_ms IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_detail_id	OUT event_detail.event_detail_id%TYPE,
    pn_exists OUT NUMBER
);

PROCEDURE SP_GET_OR_CREATE_STTLMT_EVENT
(
    pn_event_id IN event.event_id%TYPE,
    pn_device_batch_id IN settlement_event.device_batch_id%TYPE,
    pn_device_new_batch_id IN settlement_event.device_new_batch_id%TYPE,
    pn_device_event_utc_ts_ms IN NUMBER,
    pn_device_event_utc_offset_min IN settlement_event.device_event_utc_offset_min%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_exists OUT NUMBER
);

PROCEDURE SP_GET_OR_CREATE_HOST_COUNTER
(
    pn_event_id IN event.event_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pv_host_counter_parameter IN host_counter.host_counter_parameter%TYPE,
    pv_host_counter_value IN host_counter.host_counter_value%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_counter_id OUT host_counter.host_counter_id%TYPE,
    pn_exists OUT NUMBER
);

FUNCTION SF_GET_PREV_FILL_SETTLEMENT(
    pn_event_id EVENT.EVENT_ID%TYPE,
    pv_device_name IN device.device_name%TYPE
) RETURN EVENT.EVENT_ID%TYPE;

FUNCTION SF_GET_PREV_FILL_SETTLEMENT(
    pn_event_id EVENT.EVENT_ID%TYPE
) RETURN EVENT.EVENT_ID%TYPE;

    FUNCTION GET_FILL_COUNTER_RESET_CD(
        pv_event_global_trans_cd EVENT.EVENT_GLOBAL_TRANS_CD%TYPE,
        pn_fill_version PLS_INTEGER,
        pn_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE,
        pv_event_auth_type EVENT_DETAIL.EVENT_DETAIL_VALUE%TYPE)
    RETURN COUNTER_RESET.COUNTER_RESET_CD%TYPE;
        
     FUNCTION GET_FILL_COUNTER_RESET_CD_REQ(
        pn_event_id EVENT.EVENT_ID%TYPE)
    RETURN COUNTER_RESET.COUNTER_RESET_CD%TYPE;    
END; 
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_EVENT.pbk?rev=1.9
CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_EVENT 
IS

FUNCTION SF_GET_GLOBAL_EVENT_CD
(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN event.event_device_tran_cd%TYPE
) RETURN VARCHAR2
IS
BEGIN
    RETURN pc_global_event_cd_prefix || ':' || pv_device_name || ':' || pv_device_tran_cd;
END;

PROCEDURE SP_GET_OR_CREATE_EVENT
(
    pc_global_event_cd_prefix IN CHAR,
    pn_session_id IN NUMBER,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_event_type_id IN event_type.event_type_id%TYPE,
    pn_event_type_host_type_num IN event_type_host_type.event_type_host_type_num%TYPE,
    pn_event_state_id IN event.event_state_id%TYPE,
    pv_event_device_tran_cd IN event.event_device_tran_cd%TYPE,
    pn_event_end_flag IN NUMBER,
    pn_event_upload_complete_flag IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_id	OUT event.event_id%TYPE,
    pn_exists OUT NUMBER,
    pd_event_start_ts IN event.event_start_ts%TYPE DEFAULT SYSDATE,
    pd_event_end_ts IN event.event_end_ts%TYPE DEFAULT SYSDATE    
)
IS
    ln_event_type_id event_type.event_type_id%TYPE;
    lv_event_global_trans_cd event.event_global_trans_cd%TYPE;
    ln_db_event_state_id event.event_state_id%TYPE;
    ld_db_event_end_ts event.event_end_ts%TYPE;
    ld_db_event_upload_complete_ts event.event_upload_complete_ts%TYPE;
    ln_update_event_flag NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_update_event_state_flag NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_host_id event.host_id%type;
    ln_host_type_id host.host_type_id%type;
	ln_device_id device.device_id%TYPE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;

    BEGIN
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_event_start_ts);
	
        SELECT HOST_TYPE_ID, HOST_ID
        INTO ln_host_type_id, ln_host_id
        FROM DEVICE.HOST
        WHERE DEVICE_ID = ln_device_id
			AND HOST_PORT_NUM = pn_host_port_num;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pv_error_message := 'Unable to create event. No host_id exists for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num || '.';
            RETURN;
    END;
    IF pn_event_type_id > -1 THEN
        ln_event_type_id := pn_event_type_id;
    ELSE
        BEGIN
            SELECT EVENT_TYPE_ID 
              INTO ln_event_type_id
              FROM DEVICE.EVENT_TYPE_HOST_TYPE
             WHERE HOST_TYPE_ID = ln_host_type_id
    	       AND EVENT_TYPE_HOST_TYPE_NUM = pn_event_type_host_type_num;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pv_error_message := 'Unable to create event. No event_type_id for host_type_id: ' || ln_host_type_id || ', event_type_host_type_num: ' || pn_event_type_host_type_num || ' in device.event_type_host_type.';
                RETURN;
        END;
    END IF;
    
    IF pv_event_device_tran_cd IS NOT NULL THEN
        lv_event_global_trans_cd := SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_event_device_tran_cd);
        
        BEGIN
            SELECT event_id, event_state_id, event_end_ts, event_upload_complete_ts
            INTO pn_event_id, ln_db_event_state_id, ld_db_event_end_ts, ld_db_event_upload_complete_ts
            FROM device.event
    		WHERE event_global_trans_cd = lv_event_global_trans_cd;
	
           	pn_exists := PKG_CONST.BOOLEAN__TRUE;
           	
            IF ln_db_event_state_id != pn_event_state_id
                AND ln_db_event_state_id NOT IN (PKG_CONST.EVENT_STATE__COMPLETE_FINAL,
                    PKG_CONST.EVENT_STATE__COMPLETE_ERROR) THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
				ln_update_event_state_flag := PKG_CONST.BOOLEAN__TRUE;
			ELSIF pn_event_end_flag = PKG_CONST.BOOLEAN__TRUE
                AND ld_db_event_end_ts IS NULL THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
			ELSIF pn_event_upload_complete_flag = PKG_CONST.BOOLEAN__TRUE
                AND ld_db_event_upload_complete_ts IS NULL THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
			END IF;
    	
    	    IF ln_update_event_flag = PKG_CONST.BOOLEAN__TRUE THEN
        		UPDATE device.event
        		SET event_state_id = CASE ln_update_event_state_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN pn_event_state_id
                        ELSE event_state_id END,
					event_start_ts = CASE WHEN pd_event_start_ts < event_start_ts THEN pd_event_start_ts
						ELSE event_start_ts END,
        			event_end_ts = CASE pn_event_end_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN pd_event_end_ts
                        ELSE event_end_ts END,
        			event_upload_complete_ts = CASE pn_event_upload_complete_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN SYSDATE
                        ELSE event_upload_complete_ts END
        		WHERE event_id = pn_event_id;
    	    END IF;
    	
            pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    	    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    	    RETURN;
	    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
	    END;
    END IF;
    
    SELECT seq_event_id.NEXTVAL
    INTO pn_event_id FROM DUAL;
    
	INSERT INTO device.event (
		event_id,
		event_type_id,
		event_state_id,
		event_device_tran_cd,
		event_global_trans_cd,
		host_id,
		event_start_ts,
		event_end_ts,
		event_upload_complete_ts				
	) VALUES (
		pn_event_id,
        ln_event_type_id,
        pn_event_state_id,
        pv_event_device_tran_cd,
        lv_event_global_trans_cd,
        ln_host_id,
        pd_event_start_ts,
		CASE pn_event_end_flag
            WHEN PKG_CONST.BOOLEAN__TRUE THEN pd_event_end_ts
            ELSE NULL END,
		CASE pn_event_upload_complete_flag
            WHEN PKG_CONST.BOOLEAN__TRUE THEN SYSDATE
            ELSE NULL END
	);
	
	INSERT INTO device.event_nl_device_session (event_id, session_id)
    VALUES (pn_event_id, pn_session_id);
    
   	pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_EVENT_DETAIL
(
    pn_event_id IN event.event_id%TYPE,
    pn_event_detail_type_id IN event_detail.event_detail_type_id%TYPE,
    pv_event_detail_value IN event_detail.event_detail_value%TYPE,
    pv_event_detail_value_ts_ms IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_detail_id	OUT event_detail.event_detail_id%TYPE,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;
    
    BEGIN
        SELECT event_detail_id, PKG_CONST.BOOLEAN__TRUE
        INTO pn_event_detail_id, pn_exists
        FROM
        (
            SELECT event_detail_id
            FROM device.event_detail
            WHERE event_id = pn_event_id
                AND event_detail_type_id = pn_event_detail_type_id
            ORDER BY event_detail_id DESC
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT seq_event_detail_id.NEXTVAL
            INTO pn_event_detail_id FROM DUAL;

        	INSERT INTO device.event_detail (
        	    event_detail_id,
        		event_id,
        		event_detail_type_id,
        		event_detail_value,
        		event_detail_value_ts
        	) VALUES (
        	    pn_event_detail_id,
        		pn_event_id,
                pn_event_detail_type_id,
                pv_event_detail_value,
                CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pv_event_detail_value_ts_ms) AS DATE)
        	);
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_STTLMT_EVENT
(
    pn_event_id IN event.event_id%TYPE,
    pn_device_batch_id IN settlement_event.device_batch_id%TYPE,
    pn_device_new_batch_id IN settlement_event.device_new_batch_id%TYPE,
    pn_device_event_utc_ts_ms IN NUMBER,
    pn_device_event_utc_offset_min IN settlement_event.device_event_utc_offset_min%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;
    
    BEGIN
        SELECT PKG_CONST.BOOLEAN__TRUE
        INTO pn_exists
        FROM device.settlement_event
        WHERE event_id = pn_event_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO device.settlement_event (
                event_id,
                device_batch_id,
                device_new_batch_id,
                device_event_utc_ts,
                device_event_utc_offset_min
            ) VALUES (
                pn_event_id,
                pn_device_batch_id,
                pn_device_new_batch_id,
                CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_device_event_utc_ts_ms) AS DATE),
                pn_device_event_utc_offset_min
            );
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_HOST_COUNTER
(
    pn_event_id IN event.event_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pv_host_counter_parameter IN host_counter.host_counter_parameter%TYPE,
    pv_host_counter_value IN host_counter.host_counter_value%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_counter_id OUT host_counter.host_counter_id%TYPE,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;

    BEGIN
        SELECT hc.host_counter_id, PKG_CONST.BOOLEAN__TRUE
          INTO pn_host_counter_id, pn_exists
		  FROM device.host_counter_event hce
		  JOIN device.host_counter hc
		    ON hce.host_counter_id = hc.host_counter_id
		 WHERE hce.event_id = pn_event_id
		   AND hc.host_counter_parameter = pv_host_counter_parameter;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT seq_host_counter_id.NEXTVAL
            INTO pn_host_counter_id FROM DUAL;
            
            INSERT INTO device.host_counter (
                host_counter_id,
                host_id,
                host_counter_parameter,
                host_counter_value)
            SELECT pn_host_counter_id,
                   e.host_id,
                   pv_host_counter_parameter,
                   pv_host_counter_value
              FROM DEVICE.EVENT e
             WHERE e.EVENT_ID = pn_event_id;
            
            INSERT INTO device.host_counter_event(event_id, host_counter_id)
            VALUES(pn_event_id, pn_host_counter_id);
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

FUNCTION SF_GET_PREV_FILL_SETTLEMENT(
    pn_event_id EVENT.EVENT_ID%TYPE,
    pv_device_name IN device.device_name%TYPE
) RETURN EVENT.EVENT_ID%TYPE
IS
    ln_curr_event_id EVENT.EVENT_ID%TYPE := pn_event_id;
    ln_event_type_id EVENT.EVENT_TYPE_ID%TYPE;
BEGIN
    LOOP
        SELECT E.EVENT_ID, E.EVENT_TYPE_ID
          INTO ln_curr_event_id, ln_event_type_id
          FROM DEVICE.SETTLEMENT_EVENT NSE
          JOIN DEVICE.SETTLEMENT_EVENT SE ON NSE.DEVICE_BATCH_ID = SE.DEVICE_NEW_BATCH_ID
          JOIN DEVICE.EVENT E on SE.EVENT_ID = E.EVENT_ID
          JOIN DEVICE.HOST h ON e.HOST_ID = h.HOST_ID
          JOIN DEVICE.DEVICE d ON h.DEVICE_ID = d.DEVICE_ID 
         WHERE NSE.EVENT_ID = ln_curr_event_id
           AND D.DEVICE_NAME = pv_device_name;
		IF ln_event_type_id = 2 THEN
            RETURN ln_curr_event_id;
        END IF;	
        IF ln_curr_event_id IS NULL THEN
            EXIT;
        END IF;
    END LOOP; 
END;

FUNCTION SF_GET_PREV_FILL_SETTLEMENT(
    pn_event_id EVENT.EVENT_ID%TYPE
) RETURN EVENT.EVENT_ID%TYPE
IS
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
BEGIN
    SELECT D.DEVICE_NAME
      INTO lv_device_name
      FROM DEVICE.EVENT E 
      JOIN DEVICE.HOST h ON e.HOST_ID = h.HOST_ID
      JOIN DEVICE.DEVICE d ON h.DEVICE_ID = d.DEVICE_ID 
     WHERE E.EVENT_ID = pn_event_id;
    RETURN SF_GET_PREV_FILL_SETTLEMENT(pn_event_id, lv_device_name);
END;

    FUNCTION GET_FILL_COUNTER_RESET_CD(
        pv_event_global_trans_cd EVENT.EVENT_GLOBAL_TRANS_CD%TYPE,
        pn_fill_version PLS_INTEGER,
        pn_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE,
        pv_event_auth_type EVENT_DETAIL.EVENT_DETAIL_VALUE%TYPE)
    RETURN COUNTER_RESET.COUNTER_RESET_CD%TYPE
    IS
    BEGIN
        IF pn_fill_version = 1 THEN
            RETURN 'N';
        ELSIF pn_fill_version = 3 THEN
            RETURN 'Y';
        ELSIF pn_fill_version = 2 THEN
            IF UPPER(pv_event_auth_type) = 'LOCAL' THEN
                IF pn_device_type_id = 1 THEN
                    RETURN 'C';
                ELSE
                    RETURN 'N';
                END IF;
            ELSIF UPPER(pv_event_auth_type) = 'MANUAL' THEN
                RETURN 'N';
            ELSE
                DECLARE
                    lc_counter_reset_flag COUNTER_RESET.COUNTER_RESET_CD%TYPE;
                BEGIN
                    SELECT DECODE(PA.ACTION_ID, 1, 'N', 2, 'A', 3, 'N', 4, 'A', 5, 'N', 6, 'C')
                      INTO lc_counter_reset_flag
                      FROM PSS.TRAN T
                      JOIN PSS.CONSUMER_ACCT_PERMISSION CAP ON T.CONSUMER_ACCT_ID = CAP.CONSUMER_ACCT_ID
                      JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
                      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON PA.ACTION_ID = DTA.ACTION_ID 
                     WHERE T.TRAN_GLOBAL_TRANS_CD = pv_event_global_trans_cd
                       AND DTA.DEVICE_TYPE_ID = pn_device_type_id;
                    RETURN lc_counter_reset_flag;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        IF UPPER(pv_event_auth_type) = 'NETWORK' THEN
                            RETURN '?';
                        ELSIF pn_device_type_id = 1 THEN
                            RETURN 'C';
                        ELSE
                            RETURN 'N';
                        END IF;
                END;          
            END IF;
        END IF;
    END;
    
    FUNCTION GET_FILL_COUNTER_RESET_CD_REQ(
        pn_event_id EVENT.EVENT_ID%TYPE)
    RETURN COUNTER_RESET.COUNTER_RESET_CD%TYPE
    IS
        lv_event_global_trans_cd EVENT.EVENT_GLOBAL_TRANS_CD%TYPE;
        ln_fill_version PLS_INTEGER;
        ln_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE;
        lv_event_auth_type EVENT_DETAIL.EVENT_DETAIL_VALUE%TYPE;
        lc_counter_reset_cd CHAR(1);
    BEGIN
        SELECT E.EVENT_GLOBAL_TRANS_CD, TO_NUMBER_OR_NULL(ED_V.EVENT_DETAIL_VALUE), D.DEVICE_TYPE_ID, ED_A.EVENT_DETAIL_VALUE
          INTO lv_event_global_trans_cd, ln_fill_version, ln_device_type_id, lv_event_auth_type
          FROM DEVICE.EVENT E
          JOIN DEVICE.EVENT_DETAIL ED_V ON E.EVENT_ID = ED_V.EVENT_ID AND ED_V.EVENT_DETAIL_TYPE_ID = 1 /* Event Type Version */
          LEFT OUTER JOIN DEVICE.EVENT_DETAIL ED_A ON E.EVENT_ID = ED_A.EVENT_ID AND ED_A.EVENT_DETAIL_TYPE_ID = 4 /* Event Auth Type */
          JOIN DEVICE.HOST H ON E.HOST_ID = H.HOST_ID
          JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
         WHERE E.EVENT_ID = pn_event_id;
        lc_counter_reset_cd := GET_FILL_COUNTER_RESET_CD(lv_event_global_trans_cd, ln_fill_version, ln_device_type_id, lv_event_auth_type);
        IF lc_counter_reset_cd = '?' THEN
    		RAISE_APPLICATION_ERROR(-20601, 'Authorization for Network Auth Fill Event ''' || lv_event_global_trans_cd || ''' not uploaded yet');
    	END IF;
        RETURN lc_counter_reset_cd;
    END;   
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/VW_DEVICE_LAST_ACTIVE.vws?rev=1.9
CREATE OR REPLACE VIEW device.vw_device_last_active
(
    device_id
  , device_name
  , device_serial_cd
  , created_by
  , created_ts
  , last_updated_by
  , last_updated_ts
  , device_type_id
  , device_active_yn_flag
  , encryption_key
  , last_activity_ts
  , device_desc
  , device_client_serial_cd
  , device_encr_key_gen_ts
  , previous_encryption_key
  , comm_method_cd
  , comm_method_ts
  , cmd_pending_cnt
  , cmd_pending_updated_ts
  , reject_until_ts
  , reject_until_updated_ts
  , call_in_time_normalized_ts
  , firmware_version
  , received_config_file_ts
)
AS
	SELECT
		d.device_id
	  , d.device_name
	  , d.device_serial_cd
	  , d.created_by
	  , d.created_ts
	  , d.last_updated_by
	  , d.last_updated_ts
	  , d.device_type_id
	  , d.device_active_yn_flag
	  , d.encryption_key
	  , d.last_activity_ts
	  , d.device_desc
	  , d.device_client_serial_cd
	  , d.device_encr_key_gen_ts
	  , d.previous_encryption_key
	  , d.comm_method_cd
	  , d.comm_method_ts
	  , d.cmd_pending_cnt
	  , d.cmd_pending_updated_ts
	  , d.reject_until_ts
	  , d.reject_until_updated_ts
	  , d.call_in_time_normalized_ts
	  , d.firmware_version
	  , d.received_config_file_ts
	FROM (
		SELECT DISTINCT FIRST_VALUE(DEVICE_ID) OVER(PARTITION BY DEVICE_NAME ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, NVL(LAST_ACTIVITY_TS, TO_DATE('01/01/1970', 'MM/DD/YYYY')) DESC, CREATED_TS DESC) DEVICE_ID
		FROM DEVICE.DEVICE
	) DX
	JOIN DEVICE.DEVICE D ON DX.DEVICE_ID = D.DEVICE_ID
;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_MAINT.pbk?rev=1.11
CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_DEVICE_MAINT IS
   e_device_id_notfound         EXCEPTION;
   e_location_id_notfound       EXCEPTION;
   e_customer_id_notfound       EXCEPTION;
   e_pos_id_notfound            EXCEPTION;
   cv_package_name              CONSTANT VARCHAR2 (50) := 'PKG_DEVICE_MAINT';

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_ROOM_CNTRL_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    MDH       24-NOV-2001    Intial Creation
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
    JKB       11-JUL-2006    does not exist (replaced by SP_ASSIGN_NEW_DEVICE_ID ??)
   *******************************************************************************/

     /*******************************************************************************
    Function Name: SP_CREATE_NEW_POS_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
    EJR       04-DEC-2006    Updated to optionally copy or insert base host and/or
                             other hosts based on input boolean flags.
    PWC       23-MAR-2007    Updated to update gprs_modem table
    PWC       29-MAR-2007    Updated to copy host_settings table values for each host
   *******************************************************************************/

   PROCEDURE sp_create_new_pos_id (
      pn_old_pos_id       IN       pos.pos_id%TYPE,
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_location_id      IN       location.location_id%TYPE,
      pn_customer_id      IN       customer.customer_id%TYPE,
      pb_dup_base_host    IN       BOOLEAN,
      pb_dup_other_hosts  IN       BOOLEAN,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_CREATE_NEW_POS_ID';
      
      n_old_device_id              device.device_id%TYPE;
      n_old_host_id                host.host_id%TYPE;
      n_new_host_id                host.host_id%TYPE;
      n_old_device_type_id         device_type.device_type_id%TYPE;
      v_device_name                device.device_name%TYPE;
      n_def_prim_host_type_id      device_type.def_prim_host_type_id%TYPE;
      n_def_prim_host_equipment_id device_type.def_prim_host_equipment_id%TYPE;
      
      CURSOR c_get_base_host_info (device_id_in device.device_id%TYPE)
      IS
         SELECT host_id
         FROM host h, host_type_host_group_type hthgt, host_group_type hgt
         WHERE h.host_type_id = hthgt.host_type_id
         AND hgt.host_group_type_id = hthgt.host_group_type_id
         AND hgt.host_group_type_cd = 'BASE_HOST'
         AND device_id = device_id_in;

      CURSOR c_get_other_host_info (device_id_in device.device_id%TYPE)
      IS
         SELECT host_id
         FROM host h, host_type_host_group_type hthgt, host_group_type hgt
         WHERE h.host_type_id = hthgt.host_type_id(+)
         AND hthgt.host_group_type_id = hgt.host_group_type_id(+)
         AND (hgt.host_group_type_cd IS NULL OR hgt.host_group_type_cd != 'BASE_HOST')
         AND device_id = device_id_in;

      CURSOR c_get_def_prim_host_info (device_id_in device.device_id%TYPE)
      IS
         SELECT def_prim_host_type_id, def_prim_host_equipment_id
         FROM device d, device_type dt
         WHERE d.device_type_id = dt.device_type_id
         AND device_id = device_id_in;

   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_new_pos_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_pos_id IS NULL THEN
         RAISE e_pos_id_notfound;
      END IF;

      -- this should never happen
      IF pn_customer_id IS NULL OR pn_customer_id < 1 THEN
         RAISE e_customer_id_notfound;
      END IF;

      -- this should never happen
      IF pn_location_id IS NULL OR pn_location_id < 1 THEN
         RAISE e_customer_id_notfound;
      END IF;

      BEGIN
          SELECT device_name
          INTO v_device_name
          FROM device
          WHERE device_id = pn_old_device_id;
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
                RAISE e_device_id_notfound;
      END;

      -- use the current active device
      SELECT NVL(MAX(device_id), pn_old_device_id)
      INTO n_old_device_id
      FROM device
      WHERE device_name = v_device_name
            AND device_active_yn_flag = 'Y';

      -- set the old devices to inactive
      UPDATE device
         SET device_active_yn_flag = 'N'
       WHERE device_name = v_device_name;

      -- create the new device record by copying old device record
      SELECT seq_device_id.NEXTVAL
        INTO pn_new_device_id
        FROM DUAL;

      INSERT INTO device
                  (device_id,
                   device_name,
                   device_serial_cd,
                   device_type_id,
                   device_active_yn_flag,
                   encryption_key,
                   last_activity_ts,
                   device_client_serial_cd,
                   device_desc,
                   device_encr_key_gen_ts,
				   previous_encryption_key,
				   comm_method_cd,
				   comm_method_ts,
				   cmd_pending_cnt,
				   cmd_pending_updated_ts,
				   reject_until_ts,
				   reject_until_updated_ts,
				   call_in_time_normalized_ts,
				   firmware_version,
				   received_config_file_ts
                  )
         SELECT pn_new_device_id,
                device_name,
                device_serial_cd,
                device_type_id,
               'Y',
                encryption_key,
                last_activity_ts,
                device_client_serial_cd,
                device_desc,
                device_encr_key_gen_ts,
				previous_encryption_key,
				comm_method_cd,
				comm_method_ts,
				cmd_pending_cnt,
				cmd_pending_updated_ts,
				reject_until_ts,
				reject_until_updated_ts,
				call_in_time_normalized_ts,
				firmware_version,
				received_config_file_ts
           FROM device
          WHERE device_id = n_old_device_id;
          
      -- copy or create base host record (assume business rule of only 1 per device)
      IF (pb_dup_base_host = TRUE) THEN
          OPEN c_get_base_host_info (n_old_device_id);
          FETCH c_get_base_host_info INTO n_old_host_id;

          IF (c_get_base_host_info%NOTFOUND) THEN
              -- create the default base host
              INSERT INTO HOST
                          (host_last_start_ts, host_status_cd, host_serial_cd,
                           host_est_complete_minut, host_port_num,
                           host_setting_updated_yn_flag, device_id, host_position_num,
                           host_label_cd, host_active_yn_flag, host_type_id,
                           host_equipment_id)
              (SELECT NULL, 0, d.device_serial_cd,
                      0, 0,
                      'N', d.device_id, 0,
                      NULL, 'Y', dt.def_base_host_type_id,
                      dt.def_base_host_equipment_id
                 FROM device d, device_type dt
                WHERE d.device_type_id = dt.device_type_id
                  AND d.device_id = pn_new_device_id);
          ELSE
          
                SELECT seq_HOST_id.nextval
                INTO n_new_host_id
                FROM dual;
          
              -- copy the base host
              INSERT INTO HOST
                          (host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                           host_est_complete_minut, host_port_num,
                           host_setting_updated_yn_flag, device_id, host_position_num,
                           host_label_cd, host_active_yn_flag, host_type_id,
                           host_equipment_id)
              (SELECT n_new_host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                      host_est_complete_minut, host_port_num,
                      host_setting_updated_yn_flag, pn_new_device_id, host_position_num,
                      host_label_cd, host_active_yn_flag, host_type_id,
                      host_equipment_id
                 FROM host
                WHERE host_id = n_old_host_id);

             -- copy any host_settings
             INSERT INTO host_setting (host_id, host_setting_parameter, host_setting_value)
             (SELECT n_new_host_id, host_setting_parameter, host_setting_value
                FROM host_setting
               WHERE host_id = n_old_host_id);
               
          END IF;
      
          CLOSE c_get_base_host_info;
      END IF;

      -- determine which host records should be copied and which should be created
      IF (pb_dup_other_hosts = TRUE) THEN
          OPEN c_get_other_host_info (n_old_device_id);
          FETCH c_get_other_host_info INTO n_old_host_id;

          IF (c_get_other_host_info%NOTFOUND) THEN
              -- determine if we need to create primary host or not
              OPEN c_get_def_prim_host_info (pn_new_device_id);
              IF (c_get_def_prim_host_info%FOUND) THEN
              
                  -- create the default primary host
                  FETCH c_get_def_prim_host_info INTO n_def_prim_host_type_id, n_def_prim_host_equipment_id;
                  INSERT INTO HOST
                              (host_last_start_ts, host_status_cd, host_serial_cd,
                               host_est_complete_minut, host_port_num,
                               host_setting_updated_yn_flag, device_id, host_position_num,
                               host_label_cd, host_active_yn_flag, host_type_id,
                               host_equipment_id)
                  (SELECT NULL, 0, device_serial_cd,
                          0, 0,
                          'N', device_id, 0,
                          NULL, 'Y', n_def_prim_host_type_id,
                          n_def_prim_host_equipment_id
                     FROM device
                    WHERE device_id = pn_new_device_id);
              END IF;
              CLOSE c_get_def_prim_host_info;
          ELSE
              -- copy all other host info
              LOOP
              
                    SELECT seq_HOST_id.nextval
                    INTO n_new_host_id
                    FROM dual;
                    
                  INSERT INTO HOST
                              (host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                               host_est_complete_minut, host_port_num,
                               host_setting_updated_yn_flag, device_id, host_position_num,
                               host_label_cd, host_active_yn_flag, host_type_id,
                               host_equipment_id)
                  (SELECT n_new_host_id, host_last_start_ts, host_status_cd, host_serial_cd,
                          host_est_complete_minut, host_port_num,
                          host_setting_updated_yn_flag, pn_new_device_id, host_position_num,
                          host_label_cd, host_active_yn_flag, host_type_id,
                          host_equipment_id
                     FROM host
                    WHERE host_id = n_old_host_id);

                    -- copy any host_settings
                    INSERT INTO host_setting (host_id, host_setting_parameter, host_setting_value)
                    (SELECT n_new_host_id, host_setting_parameter, host_setting_value
                       FROM host_setting
                      WHERE host_id = n_old_host_id);
                      
              FETCH c_get_other_host_info INTO n_old_host_id;
              EXIT WHEN c_get_other_host_info%NOTFOUND;
              END LOOP;
          END IF;
              
          CLOSE c_get_other_host_info;
      END IF;

      -- copy and create new POS and POS_Payment_Type_Authority records
      SELECT pss.seq_pos_id.NEXTVAL
        INTO pn_new_pos_id
        FROM DUAL;

      INSERT INTO pos
                  (
                   pos_id,
                   location_id,
                   customer_id,
                   device_id
                  )
         VALUES
         (
                pn_new_pos_id,
                pn_location_id,
                pn_customer_id,
                pn_new_device_id
         );

      INSERT INTO pos_pta
                  (pos_id,
                    payment_subtype_id,
                    pos_pta_encrypt_key,
                    pos_pta_deactivation_ts,
                    created_by,
                    created_ts,
                    last_updated_by,
                    last_updated_ts,
                    payment_subtype_key_id,
                    pos_pta_regex,
                    pos_pta_regex_bref,
                    pos_pta_activation_ts,
                    pos_pta_device_serial_cd,
                    pos_pta_pin_req_yn_flag,
                    pos_pta_encrypt_key2,
                    authority_payment_mask_id,
                    pos_pta_priority,
                    terminal_id,
                    merchant_bank_acct_id,
                    currency_cd,
                    pos_pta_passthru_allow_yn_flag,
                    pos_pta_pref_auth_amt,
                    pos_pta_pref_auth_amt_max
                  )
         SELECT pn_new_pos_id,
                payment_subtype_id,
                pos_pta_encrypt_key,
                pos_pta_deactivation_ts,
                created_by,
                created_ts,
                last_updated_by,
                last_updated_ts,
                payment_subtype_key_id,
                pos_pta_regex,
                pos_pta_regex_bref,
                pos_pta_activation_ts,
                pos_pta_device_serial_cd,
                pos_pta_pin_req_yn_flag,
                pos_pta_encrypt_key2,
                authority_payment_mask_id,
                pos_pta_priority,
                terminal_id,
                merchant_bank_acct_id,
                currency_cd,
                pos_pta_passthru_allow_yn_flag,
                pos_pta_pref_auth_amt,
                pos_pta_pref_auth_amt_max
           FROM pos_pta
          WHERE pos_id = pn_old_pos_id;

      -- change any waiting outgoing file transfers over to this new device
      UPDATE device_file_transfer
         SET device_id = pn_new_device_id
       WHERE device_id = n_old_device_id
         AND device_file_transfer_direct = 'O'
         AND device_file_transfer_status_cd = 0;

      -- copy any existing device settings to the new device
      INSERT INTO device_setting
                  (device_id,
                   device_setting_parameter_cd,
                   device_setting_value
                  )
         SELECT pn_new_device_id,
                device_setting_parameter_cd,
                device_setting_value
           FROM device_setting
          WHERE device_setting.device_id = n_old_device_id;
          
      -- update gprs_device row if exists
      UPDATE gprs_device
         SET gprs_device.device_id = pn_new_device_id
       WHERE gprs_device.device_id = n_old_device_id;

   EXCEPTION
      WHEN e_pos_id_notfound THEN
         pv_error_message := 'The pos_id = ' || pn_old_pos_id ||
               ' , could not be found in the pos table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_location_id_notfound THEN
         pv_error_message := 'The location_id = ' || pn_location_id ||
               ' , could not be found in the location table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_customer_id_notfound THEN
         pv_error_message := 'The customer_id = ' || pn_customer_id ||
               ' , could not be found in the customer table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

   END;

     /*******************************************************************************
    Function Name: SP_ASSIGN_NEW_DEVICE_ID
    Description: Copies specific device, host, pos, and pos_pta state information
                 linked to a new device_id.  *WARNING*: This method should ONLY be used
                 in the context of a device that is updating host/component information.
                 In any other context, all host information may be lost resulting in 
                 potentially lost transactions (as the device will be unaware that
                 the server no longer knows its host/component state(s)).
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    ???       ??-???-????    converted from SP_ASSIGN_NEW_ROOM_CNTRL_ID ??
    EJR       13-OCT-2004    Updated to support new device, pos_pta table changes
    JKB       11-JUL-2006    Updated to use SP_CREATE_NEW_POS_ID
    EJR       04-DEC-2006    Updated to use additional params of SP_CREATE_NEW_POS_ID
   *******************************************************************************/

   PROCEDURE sp_assign_new_device_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      n_old_pos_id                 pos.pos_id%TYPE;
      n_location_id                location.location_id%TYPE;
      n_customer_id                customer.customer_id%TYPE;
      n_max_old_pos_date           pos.pos_activation_ts%TYPE;
      n_new_pos_id                 pos.pos_id%TYPE;
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_ASSIGN_NEW_DEVICE_ID';
      b_dup_base_host              BOOLEAN := FALSE;
      b_dup_other_hosts            BOOLEAN := FALSE;
      n_old_device_type_id         device_type.device_type_id%TYPE;
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- get the max pos activation time stamp for this device
      SELECT MAX(pos.pos_activation_ts)
        INTO n_max_old_pos_date
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      -- get the old pos_id, location_id and customer_id
      SELECT pos.pos_id, pos.location_id, pos.customer_id
        INTO n_old_pos_id, n_location_id, n_customer_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id
         AND pos.pos_activation_ts = n_max_old_pos_date;

      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_pos_id_notfound;
      END IF;
      
      -- determine what types of host data should be retained
      SELECT device_type_id
        INTO n_old_device_type_id
        FROM device
       WHERE device_id = pn_old_device_id;

      IF (n_old_device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS) THEN
          -- assume eSuds sends all host info except for base host
          b_dup_base_host   := TRUE;
          b_dup_other_hosts := FALSE;
      ELSIF n_old_device_type_id >= PKG_CONST.DEVICE_TYPE__EDGE THEN
          -- device types starting with Edge: send all host info including base host
          b_dup_base_host   := FALSE;
          b_dup_other_hosts := FALSE;
      ELSE
          -- all other legacy device types: none send host info (yet)
          b_dup_base_host   := TRUE;
          b_dup_other_hosts := TRUE;
      END IF;

      sp_create_new_pos_id(n_old_pos_id, pn_old_device_id, n_location_id,
                           n_customer_id, b_dup_base_host, b_dup_other_hosts,
                           pn_new_device_id, n_new_pos_id, pn_return_code,
                           pv_error_message);

   EXCEPTION

      WHEN e_pos_id_notfound THEN
         pv_error_message := 'A pos_id could not be found in the pos table for the device_id = ' ||
               pn_old_device_id || '.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

   END;

     /*******************************************************************************
    Function Name: SP_UPDATE_LOC_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/

   PROCEDURE sp_update_loc_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_location_id  IN       location.location_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_LOC_ID';
   BEGIN

      sp_update_loc_id_cust_id(pn_old_device_id, pn_new_location_id, -1,
                               pn_new_device_id, pn_new_pos_id,
                               pn_return_code, pv_error_message);

   END;

     /*******************************************************************************
    Function Name: SP_UPDATE_CUST_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
   *******************************************************************************/

   PROCEDURE sp_update_cust_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_customer_id  IN       customer.customer_id%TYPE,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_CUST_ID';
   BEGIN

      sp_update_loc_id_cust_id(pn_old_device_id, -1, pn_new_customer_id,
                               pn_new_device_id, pn_new_pos_id,
                               pn_return_code, pv_error_message);

   END;

     /*******************************************************************************
    Function Name: SP_UPDATE_CUST_ID_LOC_ID
    Description: Descriptions of the function
    Parameters:

                Function Return Value
                   Indicates if the exception was recorded successfully.  A value of
                   0 indicates a successful execution.  A value of 1 indicates an
                   unsuccessful execution.  A Boolean value was not used because it
                   may be misinterpreted by non-Oracle systems.
    MODIFICATION HISTORY:
    Person     Date          Comments
    ---------  ----------    -------------------------------------------
    JKB       11-JUL-2006    Initial Creation
    EJR       04-DEC-2006    Updated to use additional params of SP_CREATE_NEW_POS_ID
   *******************************************************************************/

   PROCEDURE sp_update_loc_id_cust_id (
      pn_old_device_id    IN       device.device_id%TYPE,
      pn_new_location_id  IN       location.location_id%TYPE := -1,
      pn_new_customer_id  IN       customer.customer_id%TYPE := -1,
      pn_new_device_id    OUT      device.device_id%TYPE,
      pn_new_pos_id       OUT      pos.pos_id%TYPE,
      pn_return_code      OUT      exception_code.exception_code_id%TYPE,
      pv_error_message    OUT      exception_data.additional_information%TYPE
   ) AS
      n_old_device_id              device.device_id%TYPE;
      n_location_id                location.location_id%TYPE;
      n_customer_id                customer.customer_id%TYPE;
      n_old_pos_id                 pos.pos_id%TYPE;
      n_max_old_pos_date           pos.pos_activation_ts%TYPE;
      b_create_new_pos             BOOLEAN;
      b_update_pos                 BOOLEAN;
      n_tran_count                 NUMBER;
      cv_procedure_name            CONSTANT VARCHAR2 (50) := 'SP_UPDATE_CUST_ID_LOC_ID';
   BEGIN
      --Initialise all variables
      pn_new_device_id := -1;
      pn_new_pos_id := -1;
      pn_return_code := pkg_app_exec_hist_globals.successful_execution;

      b_create_new_pos := FALSE;
      b_update_pos := FALSE;

      IF pn_old_device_id IS NULL THEN
         RAISE e_device_id_notfound;
      END IF;

      -- this should never happen
      IF pn_new_customer_id IS NULL THEN
         RAISE e_customer_id_notfound;
      END IF;

      -- this should never happen
      IF pn_new_location_id IS NULL THEN
         RAISE e_location_id_notfound;
      END IF;

      -- get the max pos activation time stamp for this device
      SELECT MAX(pos.pos_activation_ts)
        INTO n_max_old_pos_date
        FROM pos
       WHERE pos.device_id = pn_old_device_id;

      -- get the old pos_id, location_id and customer_id
      SELECT pos.pos_id, pos.location_id, pos.customer_id
        INTO n_old_pos_id, n_location_id, n_customer_id
        FROM pos
       WHERE pos.device_id = pn_old_device_id
         AND pos.pos_activation_ts = n_max_old_pos_date;

      IF SQL%NOTFOUND = TRUE THEN
         RAISE e_pos_id_notfound;
      END IF;

      -- check if updating customer
      IF pn_new_customer_id != -1 THEN

         IF n_customer_id != pn_new_customer_id THEN

            b_update_pos := TRUE;

            IF n_customer_id != 1 THEN
               b_create_new_pos := TRUE;
            END IF;

            -- check to make sure this customer exists
            SELECT customer_id
              INTO n_customer_id
              FROM location.customer
             WHERE customer_id = pn_new_customer_id;

            IF SQL%NOTFOUND = TRUE THEN
               RAISE e_customer_id_notfound;
            END IF;

         END IF;

      END IF;

      -- check if updating location
      IF pn_new_location_id != -1 THEN

         IF n_location_id != pn_new_location_id THEN

            b_update_pos := TRUE;

            IF n_location_id != 1 THEN
               b_create_new_pos := TRUE;
            END IF;

            -- check to make sure this location exists
            SELECT location_id
              INTO n_location_id
              FROM location.location
             WHERE location_id = pn_new_location_id;

            IF SQL%NOTFOUND = TRUE THEN
               RAISE e_location_id_notfound;
            END IF;

         END IF;

      END IF;

      -- check to see if we really need a new pos (i.e. does it have transactions)
      IF b_create_new_pos THEN

         SELECT COUNT(tran.tran_id) tran_count
           INTO n_tran_count
           FROM tran, pos_pta
          WHERE tran.pos_pta_id = pos_pta.pos_pta_id
            AND pos_pta.pos_id = n_old_pos_id;

         IF n_tran_count = 0 THEN

             b_create_new_pos := FALSE;

             -- we are going to make these negative to use the backdoor on the
             -- pos table trigger (bu)
             n_location_id := -(ABS(n_location_id));
             n_customer_id := -(ABS(n_customer_id));

         END IF;

      END IF;

      -- do the correct update

      IF b_create_new_pos THEN
         -- create a new POS (and retain all current host information)
         sp_create_new_pos_id(n_old_pos_id, pn_old_device_id,
                              n_location_id, n_customer_id, TRUE, TRUE,
                              pn_new_device_id, pn_new_pos_id,
                              pn_return_code, pv_error_message);

      ELSE

         pn_new_device_id := pn_old_device_id;
         pn_new_pos_id := n_old_pos_id;

         -- double check that we have to do anything
         -- (don't update unless something has changed)
         IF b_update_pos THEN

            UPDATE pos
               SET pos.location_id = n_location_id,
                   pos.customer_id = n_customer_id
             WHERE pos.pos_id = n_old_pos_id;

         END IF;

      END IF;

   EXCEPTION

      WHEN e_pos_id_notfound THEN
         pv_error_message := 'A pos_id could not be found in the pos table for the device_id = ' ||
               pn_old_device_id || '.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_device_id_notfound THEN
         pv_error_message := 'The device_id = ' || pn_old_device_id ||
               ' , could not be found in the device table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.invalid_machine_id,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_location_id_notfound THEN
         pv_error_message := 'The location_id = ' || pn_new_location_id ||
               ' , could not be found in the location table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

      WHEN e_customer_id_notfound THEN
         pv_error_message := 'The customer_id = ' || pn_new_customer_id ||
               ' , could not be found in the customer table.';
         pkg_exception_processor.sp_log_exception
                                            (pkg_app_exec_hist_globals.unsuccessful_execution,
                                             pv_error_message,
                                             NULL,
                                             cv_package_name || cv_procedure_name
                                            );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;

   END;

END; 
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.psk?rev=1.17
CREATE OR REPLACE PACKAGE PSS.PKG_SETTLEMENT AS
    WRONG_SETTLE_BATCH_STATE EXCEPTION;
    PRAGMA EXCEPTION_INIT(WRONG_SETTLE_BATCH_STATE, -20561);
    
    WRONG_TRAN_STATE EXCEPTION;
    PRAGMA EXCEPTION_INIT(WRONG_TRAN_STATE, -20562);
    
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC;
    
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE);
        
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2);
        
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE);
        
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER);
    
    --R29+ signature
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N',
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE);
    
    -- R28 signature    
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N');

    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2;

    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N');
        
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER);
    
    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE);
        
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE);
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE);
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE);
    
    -- R29+ signature    
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE);
        
    -- R28 signature    
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR);
END PKG_SETTLEMENT;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.pbk?rev=1.49
CREATE OR REPLACE PACKAGE BODY PSS.PKG_SETTLEMENT AS
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC
    IS       
    BEGIN
        IF pc_auth_result_cd = 'Y' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'D'; -- Complete
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C', 'V') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'D'; -- Complete
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        ELSIF pc_auth_result_cd = 'P' THEN
            RETURN 'Q'; -- settlement processed
        ELSIF pc_auth_result_cd = 'N' THEN
            RETURN 'N'; -- PROCESSED_SERVER_SETTLEMENT_INCOMPLETE
        ELSIF pc_auth_result_cd = 'F' THEN
            RETURN 'R'; -- PROCESSED_SERVER_SETTLEMENT_ERROR
        ELSIF pc_auth_result_cd = 'O' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C', 'V') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION CREATE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_TERMINAL_BATCH_ID.NEXTVAL
          INTO ln_terminal_batch_id
          FROM DUAL;
        INSERT INTO PSS.TERMINAL_BATCH (
                TERMINAL_BATCH_ID,
                TERMINAL_ID,
                TERMINAL_BATCH_NUM,
                TERMINAL_BATCH_OPEN_TS,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_CAPTURE_FLAG) 
         SELECT ln_terminal_batch_id, 
                pn_terminal_id, 
                T.TERMINAL_NEXT_BATCH_NUM,
                SYSDATE,
                T.TERMINAL_BATCH_CYCLE_NUM,
                A.TERMINAL_CAPTURE_FLAG
           FROM PSS.TERMINAL T
           JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
           JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          WHERE TERMINAL_ID = pn_terminal_id;
          
         UPDATE PSS.TERMINAL
            SET TERMINAL_NEXT_BATCH_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN NVL(TERMINAL_MIN_BATCH_NUM, 1) /* reset batch num */ 
                    ELSE TERMINAL_NEXT_BATCH_NUM + 1 /* increment batch num */
                END,
                TERMINAL_BATCH_CYCLE_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN TERMINAL_BATCH_CYCLE_NUM + 1 /* next cycle */ 
                    ELSE TERMINAL_BATCH_CYCLE_NUM /* same cycle */
                END
            WHERE TERMINAL_ID = pn_terminal_id;
        RETURN ln_terminal_batch_id;
    END;
            
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION GET_AVAILABLE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE,
        pn_allowed_trans OUT PLS_INTEGER,
        pb_create_if_needed BOOLEAN)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_is_closed CHAR(1);
        ln_max_tran PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE; 
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_attempts PLS_INTEGER;
    BEGIN
        -- get last terminal batch record and verify that it is open       
        SELECT MAX(TERMINAL_BATCH_ID), MAX(DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y')), MAX(TERMINAL_BATCH_MAX_TRAN), MAX(TERMINAL_STATE_ID), MAX(NVL(TERMINAL_CAPTURE_FLAG, 'N'))
          INTO ln_terminal_batch_id, lc_is_closed, ln_max_tran, ln_terminal_state_id, lc_terminal_capture_flag
          FROM (SELECT TB.TERMINAL_BATCH_ID,
                       TB.TERMINAL_BATCH_NUM, 
                       TB.TERMINAL_BATCH_CYCLE_NUM, 
                       TB.TERMINAL_BATCH_CLOSE_TS,
                       COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) TERMINAL_BATCH_MAX_TRAN,
                       T.TERMINAL_STATE_ID,
                       TB.TERMINAL_CAPTURE_FLAG
                  FROM PSS.TERMINAL T
                  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
                  JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID 
                  LEFT OUTER JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID                   
                 WHERE T.TERMINAL_ID = pn_terminal_id
                 ORDER BY TB.TERMINAL_BATCH_CYCLE_NUM DESC,
                          TB.TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        pn_allowed_trans := ln_max_tran;
        IF ln_terminal_state_id NOT IN(3) THEN
            RAISE_APPLICATION_ERROR(-20559, 'Terminal ' || pn_terminal_id || ' is not locked and a new terminal batch can not be created for it');
        ELSIF ln_terminal_batch_id IS NULL THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSIF lc_is_closed  = 'Y' THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSE
            -- do we need a new batch?
            SELECT ln_max_tran - COUNT(DISTINCT TRAN_ID)
              INTO pn_allowed_trans
              FROM (SELECT A.TRAN_ID
                      FROM PSS.AUTH A
                     WHERE A.TERMINAL_BATCH_ID = ln_terminal_batch_id
                    UNION ALL
                    SELECT R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = ln_terminal_batch_id);
            IF lc_terminal_capture_flag = 'Y' THEN              
                IF pn_allowed_trans > 0 THEN
                    SELECT COUNT(*)
                      INTO ln_attempts
                      FROM PSS.SETTLEMENT_BATCH
                     WHERE TERMINAL_BATCH_ID = ln_terminal_batch_id;
                END IF;
                IF ln_attempts > 0 OR pn_allowed_trans <= 0 THEN
                    -- Create new terminal batch
                    IF pb_create_if_needed THEN
                        ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
                    END IF;
                    pn_allowed_trans := ln_max_tran;
                END IF;
            END IF;
        END IF;
        
        RETURN ln_terminal_batch_id;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pc_ignore_minimums_flag CHAR,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
         -- The following should never occur:
        /*
        -- Find retry settlements
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (SELECT TB.TERMINAL_BATCH_ID
                  FROM PSS.SETTLEMENT_BATCH SB 
                  JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                 WHERE TB.TERMINAL_ID = pn_payment_subtype_key_id 
                   AND SB.SETTLEMENT_BATCH_STATE_ID = 4 
                 ORDER BY TB.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
     
        IF pn_pending_terminal_batch_ids.COUNT > 0 THEN
            RETURN;
        END IF;
        */
        -- Find new open batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM PSS.MERCHANT M
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
              JOIN (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, 
                           TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END)) C  ON C.MERCHANT_ID = M.MERCHANT_ID 
             WHERE C.NUM_TRAN > 0
               AND (pc_ignore_minimums_flag = 'Y'
                OR C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) 
                OR SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MAX_BATCH_CLOSE_HR/24, AU.AUTHORITY_MAX_BATCH_CLOSE_HR/24, 1)
                OR (SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MIN_BATCH_CLOSE_HR/24, AU.AUTHORITY_MIN_BATCH_CLOSE_HR/24, 0)
                    AND C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MIN_TRAN, AU.AUTHORITY_BATCH_MIN_TRAN, 25)))
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_RETRY_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
        -- Find retyable batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
             WHERE C.NUM_TRAN > 0
               AND SYSDATE >= pn_settlement_retry_interval + (
                        SELECT NVL(MAX(LA.SETTLEMENT_BATCH_START_TS), MIN_DATE)
                          FROM PSS.SETTLEMENT_BATCH LA
                          WHERE C.TERMINAL_BATCH_ID = LA.TERMINAL_BATCH_ID)
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_SETTLE_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        ln_is_open PLS_INTEGER;
        lv_msg VARCHAR2(4000);
    BEGIN
        -- check terminal state
        SELECT T.TERMINAL_ID, T.TERMINAL_STATE_ID
          INTO ln_terminal_id, ln_terminal_state_id
          FROM PSS.TERMINAL T
          JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
        IF ln_terminal_state_id NOT IN(3,5) THEN
            SELECT 'Terminal ' || ln_terminal_id || ' is currently ' || DECODE(ln_terminal_state_id, 1, 'not locked', 2, 'disabled', 5, 'busy with retry', 'unavailable')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20560, lv_msg);
        END IF;
        
        -- Check status of most recent settlement batch to ensure it is failue or decline
        SELECT MAX(SETTLEMENT_BATCH_ID), NVL(MAX(SETTLEMENT_BATCH_STATE_ID), 0)
          INTO ln_settlement_batch_id, ln_settlement_batch_state_id
          FROM (
            SELECT SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
              FROM PSS.SETTLEMENT_BATCH
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             ORDER BY SETTLEMENT_BATCH_START_TS DESC, SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_settlement_batch_state_id NOT IN(2, 3) THEN
            SELECT 'Terminal Batch ' || pn_terminal_batch_id || DECODE(ln_settlement_batch_state_id, 0, ' has not yet been tried', 1, ' was successfully settled already', 4, ' is awaiting a retry', 7, 'was partially settled already', ' is not ready for forced settlement')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20561, lv_msg);
        END IF;
        
        -- create new settlement batch
        SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
          INTO ln_settlement_batch_id
          FROM DUAL;            
        
        INSERT INTO PSS.SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            SETTLEMENT_BATCH_STATE_ID,
            SETTLEMENT_BATCH_START_TS,
            TERMINAL_BATCH_ID,
            SETTLEMENT_BATCH_RESP_CD,
            SETTLEMENT_BATCH_RESP_DESC,
            SETTLEMENT_BATCH_REF_CD,
            SETTLEMENT_BATCH_END_TS
        ) VALUES (
            ln_settlement_batch_id,
            1,
            SYSDATE,
            pn_terminal_batch_id,
            0,
            pv_force_reason, 
            'FORCE_SETTLEMENT',
            SYSDATE);
        -- add all trans and update their tran state cd
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            AUTH_ID,
            TRAN_ID,
            TRAN_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
            FROM PSS.TRAN T
            JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
           WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND A.AUTH_STATE_ID IN(2,6);
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            REFUND_ID,
            TRAN_ID,
            REFUND_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
            FROM PSS.TRAN T
            JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
           WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND R.REFUND_STATE_ID IN(1);
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'D'
         WHERE TRAN_STATE_CD IN('R', 'N', 'Q', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_STATE_ID IN(2,6)
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1));
                   
        -- update terminal batch
        UPDATE PSS.TERMINAL_BATCH
           SET TERMINAL_BATCH_CLOSE_TS = SYSDATE
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_TRAN(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_sale_auth_id PSS.AUTH.AUTH_ID%TYPE; 
        ln_force_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_force_refund_id PSS.REFUND.REFUND_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
    BEGIN
        -- check terminal state
        SELECT T.TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN T
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be forced');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = CASE WHEN pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN 'T' ELSE 'D' END
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
        
        IF SQL%ROWCOUNT < 1 THEN
            FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, pn_tran_id, pv_force_reason);
        ELSE
            -- add to batch if necessary
            SELECT MAX(AUTH_ID), MAX(AUTH_AMT)
              INTO ln_sale_auth_id, ln_auth_amt
              FROM (
                SELECT AUTH_ID, AUTH_TS, AUTH_AMT
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id
                   AND AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
                 ORDER BY AUTH_TS DESC, AUTH_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_sale_auth_id IS NULL THEN
                SELECT MAX(REFUND_ID), MAX(REFUND_AMT)
                  INTO ln_force_refund_id, ln_auth_amt
                  FROM (
                    SELECT CREATED_TS, REFUND_ID, REFUND_AMT
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id
                     ORDER BY CREATED_TS DESC, REFUND_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;             
            IF pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN
                IF ln_sale_auth_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.AUTH
                     WHERE TRAN_ID = pn_tran_id;
                ELSIF ln_force_refund_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id;
                END IF;
                IF ln_terminal_batch_id IS NULL THEN
                    ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, TRUE);
                    IF ln_allowed_trans <= 0 THEN
                        RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || pn_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                    END IF;
                END IF;
            ELSE
                -- create settlement batch record
                SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
                  INTO ln_settlement_batch_id
                  FROM DUAL;
                INSERT INTO PSS.SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    SETTLEMENT_BATCH_STATE_ID,
                    SETTLEMENT_BATCH_START_TS,
                    SETTLEMENT_BATCH_END_TS,
                    SETTLEMENT_BATCH_RESP_CD,
                    SETTLEMENT_BATCH_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    1,
                    SYSDATE,
                    SYSDATE,
                    'MANUAL',
                    pv_force_reason);
            END IF;
            IF ln_sale_auth_id IS NOT NULL THEN  
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO ln_force_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_RESULT_CD,
                        AUTH_RESP_CD,
                        AUTH_RESP_DESC,
                        AUTH_AMT_APPROVED)
                 SELECT ln_force_auth_id,
                        pn_tran_id,
                        A.AUTH_TYPE_CD,
                        2,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        a.AUTH_AMT,	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER,
                        'Y',
                        'MANUAL',
                        pv_force_reason,
                        a.AUTH_AMT
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_sale_auth_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        AUTH_ID,
                        TRAN_ID,
                        TRAN_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_auth_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            ELSIF ln_force_refund_id IS NOT NULL THEN
                UPDATE PSS.REFUND
                   SET REFUND_STATE_ID = 1,
                       REFUND_RESP_CD = 'MANUAL',
                       REFUND_RESP_DESC = pv_force_reason,
                       REFUND_AUTHORITY_TS = SYSDATE,
                       TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID)
                 WHERE REFUND_ID = ln_force_refund_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        REFUND_ID,
                        TRAN_ID,
                        REFUND_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_refund_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE ERROR_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    BEGIN
        -- check terminal state
        SELECT TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be errored');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'E'
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
           
        IF SQL%ROWCOUNT < 1 THEN
            ERROR_TRAN(pn_tran_id, pv_force_reason);
        END IF;
    END;
         
    PROCEDURE MARK_ADMIN_CMD_EXECUTED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 2,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;

    PROCEDURE MARK_ADMIN_CMD_ERRORED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE,
        pv_error_msg PSS.ADMIN_CMD.ERROR_MSG%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 4,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP),
               ERROR_MSG = pv_error_msg
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;
    
    PROCEDURE GET_ACTIONS_FROM_ADMIN_CMDS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pc_allow_add_to_batch CHAR,
        pc_retry_only CHAR,
        pc_settle_processing_enabled CHAR,
        pc_tran_processing_enabled CHAR,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        CURSOR l_cur IS
            SELECT ADMIN_CMD_ID, ADMIN_CMD_TYPE_ID
              FROM PSS.ADMIN_CMD
             WHERE PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
               AND PAYMENT_SUBTYPE_CLASS =  pv_payment_subtype_class
               AND ADMIN_CMD_STATE_ID = 1
               AND (pc_settle_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(1,2,3,6,8))
               AND (pc_tran_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(4,5,7))
               AND (pc_retry_only != 'Y' OR ADMIN_CMD_TYPE_ID IN(2, 3, 4, 5, 6, 7))
               AND (pc_allow_add_to_batch = 'Y' OR pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' OR ADMIN_CMD_TYPE_ID NOT IN(5))
             ORDER BY PRIORITY ASC, CREATED_UTC_TS ASC;
    BEGIN
        -- get the first command and translate it into a terminal batch or tran to process
        FOR l_rec IN l_cur LOOP
            IF l_rec.ADMIN_CMD_TYPE_ID IN(1,2,3,6,5,8) AND pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' THEN
                MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Settlement commands are not allowed for Payment Subtype Class ''' || pv_payment_subtype_class || '''');
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 1 THEN -- Settle All
                GET_PENDING_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    'Y', 
                    pn_max_settlements,
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 2 THEN -- Retry All
                GET_RETRY_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class,
                    0,
                    pn_max_settlements, 
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 3 THEN -- Retry Batch
                SELECT TERMINAL_BATCH_ID
                  BULK COLLECT INTO pn_pending_terminal_batch_ids
                  FROM (
                    SELECT C.TERMINAL_BATCH_ID
                      FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                                   SUM(CASE WHEN TRAN_STATE_CD IN('R','N') THEN 1 ELSE 0 END) NUM_TRAN
                              FROM (SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id
                                     UNION 
                                    SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                             GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                             HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
                      JOIN PSS.ADMIN_CMD_PARAM ACP ON C.TERMINAL_BATCH_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                     WHERE C.NUM_TRAN > 0
                       AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID'
                   ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
                 WHERE ROWNUM <= pn_max_settlements;
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 4 THEN -- Retry Tran
                SELECT TRAN_ID
                  BULK COLLECT INTO pn_pending_tran_ids
                  FROM (SELECT T.TRAN_ID 
                          FROM PSS.TRAN T
                          LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                          LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                          JOIN PSS.ADMIN_CMD_PARAM ACP ON T.TRAN_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                         WHERE T.TRAN_STATE_CD IN('I', 'J') /* transaction_incomplete, transaction_incomplete_error */
                           AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                           AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                           AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                           AND ACP.PARAM_NAME = 'TRAN_ID'
                           AND (pc_allow_add_to_batch = 'Y' OR NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL)
                         GROUP BY T.TRAN_ID
                         ORDER BY T.TRAN_UPLOAD_TS ASC)
                 WHERE ROWNUM <= pn_max_trans;
                IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT < pn_max_trans THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 5 THEN -- Force Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, ln_tran_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE IN(1, 20556) THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 6 THEN -- Force Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_SETTLE_BATCH(ln_terminal_batch_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' or ''FORCE_REASON'' not found');
                    WHEN WRONG_SETTLE_BATCH_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 7 THEN -- Error Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_error_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_error_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'ERROR_REASON';    
                    ERROR_TRAN(ln_tran_id, lv_error_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 8 THEN -- Settle Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lc_terminal_batch_closed CHAR;
                    ln_num_tran PLS_INTEGER;
                    ln_count_tran PLS_INTEGER;
                    ln_okay_tran PLS_INTEGER;          
                BEGIN
                    SELECT MAX(TO_NUMBER_OR_NULL(ACP.PARAM_VALUE))
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    IF ln_terminal_batch_id IS NULL THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' not found');
                    ELSE
                        SELECT DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y'), 
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN,
                           COUNT(X.TRAN_STATE_CD) COUNT_TRAN,
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END) OKAY_TRAN
                          INTO lc_terminal_batch_closed,
                               ln_num_tran,
                               ln_count_tran,
                               ln_okay_tran
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN (SELECT A.TRAN_ID, A.TERMINAL_BATCH_ID
                                  FROM PSS.AUTH A
                                UNION
                                SELECT R.TRAN_ID, R.TERMINAL_BATCH_ID
                                  FROM PSS.REFUND R) A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                         WHERE TB.TERMINAL_BATCH_ID = ln_terminal_batch_id 
                         GROUP BY DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y');
                        IF lc_terminal_batch_closed != 'N' THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch '|| ln_terminal_batch_id ||' is already closed');
                        ELSIF ln_okay_tran != ln_count_tran THEN
                            NULL; -- Not ready for settlement yet
                        ELSIF ln_num_tran = 0 THEN
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID); -- No non-error transaction, so mark as complete and continue
                        ELSE
                            SELECT ln_terminal_batch_id
                              BULK COLLECT INTO pn_pending_terminal_batch_ids
                              FROM DUAL;
                            -- Mark command executed                          
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                            RETURN; -- found something to process so exit
                        END IF;
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch ' ||ln_terminal_batch_id||' not found');
                END;
            ELSE
                -- Unknown cmd type
                NULL;
            END IF;
        END LOOP;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        ln_batch_max_trans PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_settle_processing_enabled CHAR(1);
        lc_tran_processing_enabled CHAR(1);
        lv_terminal_global_token_cd PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
          INTO lc_tran_processing_enabled
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'TRAN_PROCESSING_ENABLED';
        
        -- Get terminal state id
        IF pv_payment_subtype_class  LIKE 'Authority::ISO8583%' THEN -- this is same as in APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL
            SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
              INTO lc_settle_processing_enabled
              FROM PSS.POSM_SETTING
             WHERE POSM_SETTING_NAME = 'SETTLE_PROCESSING_ENABLED';
        
            SELECT T.TERMINAL_STATE_ID, COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999)
              INTO pn_terminal_state_id, ln_batch_max_trans
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID                       
             WHERE T.TERMINAL_ID = pn_payment_subtype_key_id;
            IF pn_terminal_state_id = 5 THEN
                -- see if there are retry admin commands
                GET_ACTIONS_FROM_ADMIN_CMDS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    pn_max_settlements, 
                    0,
                    'N', -- pc_allow_add_to_batch
                    'Y', -- pc_retry_only
                    lc_settle_processing_enabled,
                    lc_tran_processing_enabled,
                    pn_pending_terminal_batch_ids,
                    pn_pending_tran_ids);
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- Early Exit if we found something to process
                END IF;
                IF lc_settle_processing_enabled = 'Y' THEN
                    -- Only settlement retry is available
                    -- Find retyable batches
                    GET_RETRY_SETTLEMENTS(
                        pn_payment_subtype_key_id, 
                        pv_payment_subtype_class,
                        pn_settlement_retry_interval,
                        pn_max_settlements, 
                        pn_pending_terminal_batch_ids);
                END IF;
                RETURN; -- if terminal is in state 5, we don't process anything else
            ELSIF pn_terminal_state_id != 3 THEN
                RETURN; -- Terminal not locked for processing
            END IF;
        ELSE
            lv_terminal_global_token_cd := GET_TERMINAL_GLOBAL_TOKEN(pv_payment_subtype_class, pn_payment_subtype_key_id);
            IF lv_terminal_global_token_cd IS NULL THEN
                pn_terminal_state_id := 1;
                RETURN;  -- Terminal not locked for processing
            ELSE
                pn_terminal_state_id := 3;
                lc_settle_processing_enabled := 'N';
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' AND lc_settle_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any processing right now
        END IF;
        
        -- limit num of trans if need be
        IF ln_batch_max_trans IS NOT NULL THEN
            ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, FALSE);
        ELSE
            ln_allowed_trans := pn_max_trans;
        END IF;
        
        -- Check pending commands and process them first
        IF ln_allowed_trans <= 0 THEN            
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                pn_max_trans,
                'N', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        ELSE
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                ln_allowed_trans,
                'Y', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        END IF;
        IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
            RETURN; -- Early Exit if we found something to process
        ELSIF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
        
        -- Check for Settlements
        IF lc_settle_processing_enabled = 'Y' THEN
            GET_PENDING_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                'N', 
                pn_max_settlements,
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
            GET_RETRY_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class,
                pn_settlement_retry_interval,
                pn_max_settlements, 
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any tran processing right now
        END IF;
        
        -- Find retry trans
        IF ln_allowed_trans <= 0 THEN
             -- We can only process trans already in a batch
             SELECT DISTINCT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                      LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                      LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                     WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                       AND NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL
                     GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                    HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= pn_max_trans;
            RETURN; -- we can't process trans right now
        END IF;
        
        SELECT DISTINCT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                 WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;
        IF pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
          
        -- get new pending trans
        SELECT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                 WHERE T.TRAN_STATE_CD IN ('8', '9', 'A', 'W') /* processed_server_batch, processed_server_batch_intended, processing_server_tran, processed_server_auth_pending_reversal */
                   AND (T.TRAN_STATE_CD != '9' OR T.AUTH_HOLD_USED != 'Y') /* Filters out the Intended Transactions which have an Auth Hold */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;        
    END;
    
    PROCEDURE SET_UNIQUE_SECONDS(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE,
        pn_unique_seconds IN OUT PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE)
    IS
    BEGIN
        UPDATE PSS.TERMINAL_BATCH
           SET UNIQUE_SECONDS = pn_unique_seconds,
               AUTHORITY_ID = pn_authority_id
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            pn_unique_seconds := pn_unique_seconds + 1;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, pn_authority_id, pn_unique_seconds);
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2)
    IS
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_id PSS.TERMINAL_BATCH.TERMINAL_ID%TYPE;
        ln_unique_seconds PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE;
        ln_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE;
    BEGIN
        -- check for an incomplete settlement_batch
        SELECT MAX(SB.SETTLEMENT_BATCH_ID), TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS
          INTO pn_settlement_batch_id, ln_terminal_id, lc_terminal_capture_flag, ln_unique_seconds
          FROM PSS.TERMINAL_BATCH TB
          LEFT OUTER JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 4
         WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
         GROUP BY TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS;
        IF pn_settlement_batch_id IS NULL THEN
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO pn_settlement_batch_id
              FROM DUAL;            
            
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                TERMINAL_BATCH_ID
            ) VALUES (
                pn_settlement_batch_id,
                4,
                SYSDATE,
                pn_terminal_batch_id);
            /* Do this after transaction is uploaded (SALEd) with the Authority)    
            -- add all trans and update their tran state cd
            INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                AUTH_ID,
                TRAN_ID,
                TRAN_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
                FROM PSS.TRAN T
                JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
               WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND (A.AUTH_STATE_ID = 2
                      OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'));
            INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                REFUND_ID,
                TRAN_ID,
                REFUND_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
                FROM PSS.TRAN T
                JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
               WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND R.REFUND_STATE_ID IN(1);
            */
        END IF;
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'S'
         WHERE TRAN_STATE_CD IN('R', 'N', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (A.AUTH_STATE_ID IN(2, 5, 6)
                  OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'))
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1, 6));
        UPDATE PSS.TERMINAL
           SET TERMINAL_STATE_ID = 5
         WHERE TERMINAL_ID = ln_terminal_id;
        SELECT COUNT(*)
          INTO pn_prior_attempts
    	  FROM PSS.SETTLEMENT_BATCH  
    	 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
    	   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3);
        IF pn_prior_attempts > 0 THEN
            SELECT DECODE(SETTLEMENT_BATCH_STATE_ID, 2, 'N', 3, 'Y')
              INTO pc_upload_needed_flag
              FROM (
                SELECT SETTLEMENT_BATCH_END_TS, SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
                  FROM PSS.SETTLEMENT_BATCH  
                 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
                   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3)           
                 ORDER BY SETTLEMENT_BATCH_END_TS DESC, SETTLEMENT_BATCH_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            pc_upload_needed_flag := 'N';
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND ln_unique_seconds IS NULL THEN
            SELECT ROUND(DATE_TO_MILLIS(SYSDATE) / 1000), M.AUTHORITY_ID
              INTO ln_unique_seconds, ln_authority_id
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
             WHERE T.TERMINAL_ID = ln_terminal_id;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, ln_authority_id, ln_unique_seconds);
        END IF;
    END;
    /*
    FUNCTION IS_LAST_TERMINAL_BATCH_OPEN(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PLS_INTEGER
    IS
        ln_is_open PLS_INTEGER;
    BEGIN
        SELECT DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 1, 0)
          INTO ln_is_open
          FROM (SELECT TERMINAL_BATCH_NUM, 
                       TERMINAL_BATCH_CYCLE_NUM, 
                       TERMINAL_BATCH_CLOSE_TS
                  FROM PSS.TERMINAL_BATCH
                 WHERE TERMINAL_ID = pn_terminal_id
                 ORDER BY TERMINAL_BATCH_CYCLE_NUM DESC,
                          TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        RETURN ln_is_open;
    END;
    */
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE)
    IS
        CURSOR lc_tran IS
            SELECT SA.TRAN_ID, SA.AUTH_ID, NULL REFUND_ID, SA.AUTH_AMT_APPROVED AMOUNT,
                   AFTER_SETTLE_TRAN_STATE_CD(sa.AUTH_TYPE_CD, NULL, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD) NEW_TRAN_STATE_CD
              FROM PSS.AUTH SA 
              JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SA.TERMINAL_BATCH_ID
              JOIN PSS.TRAN X ON SA.TRAN_ID = X.TRAN_ID
             WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
               AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                    OR (SA.AUTH_STATE_ID IN(6) AND TB.TERMINAL_CAPTURE_FLAG = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                    OR (SA.AUTH_STATE_ID = 4 AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y'))
               AND X.TRAN_STATE_CD IN('S')
            UNION ALL
            SELECT R.TRAN_ID, NULL, R.REFUND_ID, R.REFUND_AMT,
                   AFTER_SETTLE_TRAN_STATE_CD(NULL, r.REFUND_TYPE_CD, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)
              FROM PSS.REFUND R
              JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
             WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (R.REFUND_STATE_ID IN(1) OR (TB.TERMINAL_CAPTURE_FLAG = 'Y' AND R.REFUND_STATE_ID IN(6)))
               AND X.TRAN_STATE_CD IN('S');
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_retain_acct_data CHAR(1);
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_STATE_ID
          INTO ln_settlement_batch_state_id
          FROM PSS.SETTLEMENT_BATCH
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        -- update the settlement batch
        UPDATE PSS.SETTLEMENT_BATCH
           SET SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SETTLEMENT_BATCH_RESP_CD = pv_authority_response_cd,
               SETTLEMENT_BATCH_RESP_DESC = pv_authority_response_desc,
               SETTLEMENT_BATCH_REF_CD = pv_authority_ref_cd,
               SETTLEMENT_BATCH_END_TS = pd_batch_closed_ts
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
            
        -- update the tran_state_cd of all transactions in the batch
        FOR lr_tran IN lc_tran LOOP
            IF lr_tran.NEW_TRAN_STATE_CD IN('V') THEN
                lc_retain_acct_data := 'N';
            ELSE
                lc_retain_acct_data := 'Y';
            END IF;
            UPDATE PSS.TRAN
               SET TRAN_STATE_CD = lr_tran.NEW_TRAN_STATE_CD,
                   TRAN_PARSED_ACCT_EXP_DATE = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_EXP_DATE),
                   TRAN_PARSED_ACCT_NUM = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_NUM),
                   TRAN_PARSED_ACCT_NAME = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_NAME),
                   TRAN_ACCOUNT_PIN = DECODE(lc_retain_acct_data, 'Y', TRAN_ACCOUNT_PIN)
             WHERE TRAN_ID = lr_tran.TRAN_ID;
            IF lr_tran.AUTH_ID IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    pn_settlement_batch_id,
                    lr_tran.AUTH_ID,
                    lr_tran.TRAN_ID,
                    DECODE(pc_auth_result_cd, 'Y', lr_tran.AMOUNT),
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );   
            ELSIF lr_tran.REFUND_ID IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT,
                    REFUND_SETTLEMENT_B_RESP_CD,
                    REFUND_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    pn_settlement_batch_id,
                    lr_tran.REFUND_ID,
                    lr_tran.TRAN_ID,
                    DECODE(pc_auth_result_cd, 'Y', lr_tran.AMOUNT),
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );   
            END IF;
        END LOOP;
          
        -- if settlement successful or permanently declined, close old terminal batch and create new terminal batch
        IF pn_terminal_batch_id IS NOT NULL AND pc_auth_result_cd IN('Y', 'P', 'O') THEN
            UPDATE PSS.TERMINAL_BATCH
               SET TERMINAL_BATCH_CLOSE_TS = pd_batch_closed_ts
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             RETURNING TERMINAL_ID INTO ln_terminal_id;
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 3
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_actual_amt PSS.AUTH.AUTH_AMT%TYPE;
        ln_intended_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ln_previous_saled_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_is_open PLS_INTEGER;   
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE;
    BEGIN
        SELECT DEVICE_NAME || ':' || TRAN_DEVICE_TRAN_CD, 
               TRAN_STATE_CD,
               NVL(AUTH_HOLD_USED, 'N'),
               TERMINAL_ID,
               NVL(TERMINAL_CAPTURE_FLAG, 'N'),
               AUTH_AUTH_ID,
               SALE_AUTH_ID,
               REFUND_ID,
               TRAN_DEVICE_RESULT_TYPE_CD
          INTO lv_tran_lock_cd,
               lc_tran_state_cd,
               lc_auth_hold_used,
               ln_terminal_id,
               lc_terminal_capture_flag,
               ln_auth_auth_id,
               pn_sale_auth_id,
               pn_refund_id,
               ln_device_result_type_cd
          FROM (SELECT T.DEVICE_NAME, 
                       T.TRAN_DEVICE_TRAN_CD, 
                       A.AUTH_ID AUTH_AUTH_ID,
                       SA.AUTH_ID SALE_AUTH_ID,
                       R.REFUND_ID, 
                       T.TRAN_STATE_CD,
                       T.AUTH_HOLD_USED,
                       TE.TERMINAL_ID,
                       AU.TERMINAL_CAPTURE_FLAG,
                       T.TRAN_DEVICE_RESULT_TYPE_CD
                  FROM PSS.TRAN T
				  LEFT OUTER JOIN PSS.AUTH RA  ON T.TRAN_ID = RA.TRAN_ID AND RA.AUTH_TYPE_CD IN ('C', 'E')
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P') OR T.TRAN_STATE_CD IN('W') OR RA.AUTH_ID IS NOT NULL)
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                  LEFT OUTER JOIN PSS.AUTH SA  ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I') AND SA.AUTH_STATE_ID = 6
                  LEFT OUTER JOIN (PSS.TERMINAL TE
                        JOIN PSS.MERCHANT M ON TE.MERCHANT_ID = M.MERCHANT_ID
                        JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
                  ) ON (T.PAYMENT_SUBTYPE_CLASS LIKE 'Authority::ISO8583%' OR T.PAYMENT_SUBTYPE_CLASS LIKE 'POSGateway%') AND T.PAYMENT_SUBTYPE_KEY_ID = TE.TERMINAL_ID
                 WHERE T.TRAN_ID = pn_tran_id
                 ORDER BY A.AUTH_RESULT_CD DESC, A.AUTH_TS DESC, A.AUTH_ID DESC, SA.AUTH_RESULT_CD DESC, SA.AUTH_TS DESC, SA.AUTH_ID DESC, R.CREATED_TS DESC, R.REFUND_ID DESC, RA.AUTH_RESULT_CD DESC, RA.AUTH_TS DESC, RA.AUTH_ID DESC)
         WHERE ROWNUM = 1;
        IF NOT(lc_tran_state_cd IN('A', '8', 'I', 'W')
            OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')) THEN
            pc_sale_phase_cd := '-'; -- Do nothing
            pn_prior_attempts := -1;
            RETURN;
        END IF;
        -- Thus, "lc_tran_state_cd IN('A', '8', 'I', 'W') OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')"
		lc_new_tran_state_cd := lc_tran_state_cd;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF ln_auth_auth_id IS NOT NULL THEN -- it's a sale or a reversal
            IF lc_terminal_capture_flag = 'Y' AND lc_tran_state_cd != '9' THEN
                -- get the open terminal batch
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT
                  INTO ln_actual_amt
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                pc_sale_phase_cd := '-'; -- No further processing
                IF ln_actual_amt != 0 THEN
                    lc_auth_type_cd := 'U'; -- pre-auth sale
                    lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSIF lc_auth_hold_used = 'Y' THEN
                     lc_auth_type_cd := 'C'; -- Auth Reversal
                     lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSE
                    -- do not create sale auth record
                    lc_new_tran_state_cd := 'C'; -- client cancelled
                END IF;
            ELSIF lc_tran_state_cd = 'W' THEN
                pc_sale_phase_cd := 'R'; -- Reversal
                ln_actual_amt := 0;
                lc_auth_type_cd := 'C'; -- Auth reversal
                lc_new_tran_state_cd := 'A'; -- processing tran
            ELSE
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT,
                       NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) INTENDED_AMT,
                       T.TRAN_STATE_CD -- get this again now that transaction is locked
                  INTO ln_actual_amt, ln_intended_amt, lc_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 RIGHT OUTER JOIN PSS.TRAN T ON T.TRAN_ID = TLI.TRAN_ID
                 WHERE T.TRAN_ID = pn_tran_id 
                 GROUP BY T.TRAN_STATE_CD;
                IF lc_tran_state_cd = '9' THEN
                    IF ln_actual_amt > 0 THEN -- sanity check
                        RAISE_APPLICATION_ERROR(-20555, 'Invalid tran_state_cd for tran ' || pn_tran_id || ': Intended batch with Actual Amount');
                    ELSIF ln_intended_amt > 0 THEN
                        pc_sale_phase_cd := 'I'; -- needs processing of intended sale
                        ln_actual_amt := NULL; -- Use Intended Amt
                        lc_auth_type_cd := 'S'; -- network sale
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSE -- we can assume lc_auth_hold_used != 'Y' or it would not have gotten here
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'U'; -- processed_server_tran_intended
                    END IF;             
                ELSE
                    SELECT NVL(MAX(AUTH_AMT), 0)
                      INTO ln_previous_saled_amt
                      FROM (SELECT /*+ index(AT IDX_AUTH_TRAN_ID) */ AT.AUTH_AMT
                              FROM PSS.AUTH AT
                             WHERE AT.TRAN_ID = pn_tran_id
                               AND AT.AUTH_TYPE_CD IN('U', 'S')
                               AND AT.AUTH_STATE_ID IN(2)
                             ORDER BY AT.AUTH_TS DESC, AT.AUTH_ID DESC)
                     WHERE ROWNUM = 1;
                    IF ln_previous_saled_amt != ln_actual_amt THEN
                        pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                        IF ln_actual_amt = 0 THEN -- had previous amount, but now cancelled
                            lc_auth_type_cd := 'E'; -- Sale Reversal
                        ELSIF ln_previous_saled_amt > 0 THEN
                            lc_auth_type_cd := 'D'; -- Sale Adjustment
                        ELSIF lc_auth_hold_used = 'Y' THEN                    
                            lc_auth_type_cd := 'U'; -- Pre-Authed Sale
                        ELSE
                            lc_auth_type_cd := 'S'; -- Sale
                        END IF;
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSIF ln_actual_amt = 0 THEN -- previous amount = actual amount = 0
                        IF ln_intended_amt > 0 AND lc_tran_state_cd != '8' THEN
                            -- This may have been an intended sale that failed, in which case we need to retry the intended amt
                            SELECT SALE_TYPE_CD
                              INTO pc_sale_phase_cd
                              FROM PSS.SALE
                             WHERE TRAN_ID = pn_tran_id;
                            IF pc_sale_phase_cd = 'I' THEN -- needs re-processing of intended sale
                                ln_actual_amt := NULL; -- Use Intended Amt
                                lc_auth_type_cd := 'S'; -- network sale
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSIF lc_auth_hold_used = 'Y' THEN
                                pc_sale_phase_cd := 'A'; -- Actual
                                lc_auth_type_cd := 'C'; -- Auth reversal
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSE
                                pc_sale_phase_cd := '-'; -- no processing needed
                                -- do not create sale auth record
                                lc_new_tran_state_cd := 'C'; -- client cancelled
                            END IF;
                        ELSIF lc_auth_hold_used = 'Y' THEN
                            IF lc_tran_state_cd IN('I', 'A') AND ln_device_result_type_cd IS NULL THEN
                                pc_sale_phase_cd := 'R'; -- Reversal
                            ELSE
                                pc_sale_phase_cd := 'A'; -- Actual
                            END IF;
                            lc_auth_type_cd := 'C'; -- Auth reversal
                            lc_new_tran_state_cd := 'A'; -- processing tran
                        ELSE
                            pc_sale_phase_cd := '-'; -- no processing needed
                            -- do not create sale auth record
                            lc_new_tran_state_cd := 'C'; -- client cancelled
                        END IF;
                    ELSE
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'D'; -- tran complete
                    END IF;
                END IF;
                IF pc_sale_phase_cd != '-' THEN
                    SELECT COUNT(*)
                      INTO pn_prior_attempts
                      FROM PSS.AUTH A
                     WHERE A.TRAN_ID = pn_tran_id
                       AND A.AUTH_TYPE_CD IN ('S','I','U','V')
                       AND A.AUTH_STATE_ID IN(3, 4);  	
                END IF;
            END IF;
            IF pn_sale_auth_id IS NULL AND lc_auth_type_cd IS NOT NULL THEN
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        pn_tran_id,
                        lc_auth_type_cd,
                        6,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        NVL(ln_actual_amt, ln_intended_amt),	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN -- it's a refund
            IF lc_terminal_capture_flag = 'Y' THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                -- stick it in the open terminal batch
                UPDATE PSS.REFUND
                   SET TERMINAL_BATCH_ID = ln_terminal_batch_id,
                       REFUND_STATE_ID = 6
                 WHERE REFUND_ID = pn_refund_id;
                pc_sale_phase_cd := '-'; -- No further processing
                lc_new_tran_state_cd := 'T'; -- Processed Server Batch
            ELSE
                -- We could check the actual amount and the previous refunded amount for sanity, but that seems overkill. 
                -- Just assume actual amount > 0 and previous refunded amount = 0
                -- Also, assume REFUND_STATE_ID IN(6, 2, 3)
                pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                lc_new_tran_state_cd := 'A'; -- processing tran
                SELECT COUNT(*)
                  INTO pn_prior_attempts
                  FROM PSS.REFUND R
                 WHERE R.TRAN_ID = pn_tran_id
                   AND R.REFUND_TYPE_CD IN ('G','C','V')
                   AND R.REFUND_STATE_ID IN(2, 3);
            END IF;
        END IF;
		IF lc_new_tran_state_cd != lc_tran_state_cd THEN
			UPDATE PSS.TRAN 
			   SET TRAN_STATE_CD = lc_new_tran_state_cd
			 WHERE TRAN_ID = pn_tran_id;
		END IF;
    END;
    
    -- R29+ signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N',
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
    BEGIN
        SELECT T.DEVICE_NAME || ':' || T.TRAN_DEVICE_TRAN_CD, T.PAYMENT_SUBTYPE_CLASS, T.PAYMENT_SUBTYPE_KEY_ID, T.TRAN_STATE_CD, T.TRAN_GLOBAL_TRANS_CD, AA.AUTH_AUTHORITY_TRAN_CD
          INTO lv_tran_lock_cd, lv_payment_subtype_class, ln_payment_subtype_key_id, lc_tran_state_cd, pv_tran_global_trans_cd, pv_auth_authority_tran_cd
          FROM PSS.TRAN T
          LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_STATE_ID IN(2,5)
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd != 'A' THEN
            IF pc_ignore_reprocess_flag = 'Y' THEN
                RETURN;
            ELSE
                RAISE_APPLICATION_ERROR(-20554, 'Invalid tran_state_cd for tran ' || pn_tran_id || ' when updating processed tran');      
            END IF;
        END IF;
        IF pc_settled_flag = 'Y' THEN
            -- create settlement batch record
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO ln_settlement_batch_id
              FROM DUAL;
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                SETTLEMENT_BATCH_END_TS,
                SETTLEMENT_BATCH_RESP_CD,
                SETTLEMENT_BATCH_RESP_DESC,
                SETTLEMENT_BATCH_REF_CD
            ) VALUES (
                ln_settlement_batch_id,
                DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                pd_authority_ts,
                pd_authority_ts,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_ref_cd);
        ELSIF pc_auth_result_cd IN('Y', 'P') AND (lv_payment_subtype_class  LIKE 'Authority::ISO8583%') THEN
            -- If this is a retry of a failure, it may already have a terminal_batch assigned
            IF pn_sale_auth_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id;
            ELSIF pn_refund_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.REFUND
                 WHERE TRAN_ID = pn_tran_id;
            END IF;
            IF ln_terminal_batch_id IS NULL THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_payment_subtype_key_id, ln_allowed_trans, TRUE);
                IF ln_allowed_trans <= 0 THEN
                    RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || ln_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                END IF;
            END IF;
        END IF;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF pn_sale_auth_id IS NOT NULL THEN
            UPDATE PSS.AUTH 
               SET (
			    AUTH_STATE_ID,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TRAN_CD,
                AUTH_AUTHORITY_REF_CD,
                AUTH_AUTHORITY_TS,
                TERMINAL_BATCH_ID,
                AUTH_AMT_APPROVED,
                AUTH_AUTHORITY_MISC_DATA) =
            (SELECT 
                DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4),
                pc_auth_result_cd,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_tran_cd,
                pv_authority_ref_cd,
                pd_authority_ts,
                ln_terminal_batch_id,
                pn_auth_approved_amt / pn_minor_currency_factor,
                pv_authority_misc_data
             FROM DUAL)
             WHERE AUTH_ID = pn_sale_auth_id
             RETURNING AUTH_TYPE_CD INTO lc_auth_type_cd;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_sale_auth_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor,
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );               
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN
           UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, REFUND_AUTHORITY_TRAN_CD),
                REFUND_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, REFUND_AUTHORITY_REF_CD),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
            WHERE REFUND_ID = pn_refund_id;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_refund_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor
                );               
            END IF;
        END IF;
        
        -- update tran_state_cd
        IF pc_auth_result_cd IN('F') THEN
            lc_tran_state_cd := 'J'; -- Incomplete Error: Manual intervention needed
        ELSIF pc_auth_result_cd IN('O') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                lc_tran_state_cd := 'V';
            ELSIF lc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                lc_tran_state_cd := 'C'; -- Client Cancelled
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'K', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;         
            ELSE
                lc_tran_state_cd := 'E'; -- Complete Error: don't retry
            END IF;
        ELSIF pc_auth_result_cd IN('N') THEN
            lc_tran_state_cd := 'I'; -- Incomplete: will retry
        ELSIF pc_auth_result_cd IN('Y', 'P') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                IF pc_settled_flag = 'Y' THEN
                    lc_tran_state_cd := 'V';
                ELSE
                     lc_tran_state_cd := 'T';
                END IF;
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'U', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;
            ELSIF pc_settled_flag = 'Y' THEN
                IF lc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                    lc_tran_state_cd := 'C'; -- Client Cancelled
                ELSE
                    lc_tran_state_cd := 'D'; -- Complete
                END IF;
            ELSE
                lc_tran_state_cd := 'T'; -- Processed Server Batch
            END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20557, 'Invalid auth result cd "' ||  pc_auth_result_cd + '" for tran ' || pn_tran_id);
        END IF;
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = lc_tran_state_cd
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd IN('V') THEN
            UPDATE PSS.TRAN
               SET TRAN_PARSED_ACCT_EXP_DATE = NULL,
                   TRAN_PARSED_ACCT_NUM = NULL,
                   TRAN_PARSED_ACCT_NAME = NULL,
                   TRAN_ACCOUNT_PIN = NULL
             WHERE TRAN_ID = pn_tran_id;
        END IF;
    END;      

    -- R28 signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N')
    IS
        lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
        lv_auth_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE;
    BEGIN
        UPDATE_PROCESSED_TRAN(
            pn_tran_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_settled_flag,
            pc_sale_phase_cd,
            pc_auth_result_cd,
            pn_minor_currency_factor,
            pn_auth_approved_amt,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pv_authority_misc_data,
            pd_authority_ts,
            pc_ignore_reprocess_flag,
            lv_tran_global_trans_cd,
            lv_auth_authority_tran_cd);
    END;
    
    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2
    IS
        lv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.ARAMARK_PAYMENT_TYPE
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.BLACKBRD_TENDER
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.INTERNAL_PAYMENT_TYPE
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
        RETURN lv_global_token;
    END;
    
    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N')
    IS
		ln_term_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_TERMINAL_LOCK_MAX_DURATION_SEC'));
		ld_sysdate DATE := SYSDATE;
		ln_forced_unlock_count NUMBER := 0;
    BEGIN
        IF pv_new_global_token IS NULL THEN
            RAISE_APPLICATION_ERROR(-20864, 'Global Token Code may not be null');
        ELSIF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
			UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = pv_new_global_token,
                   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL AND TERMINAL_STATE_ID IN(1, 5))
                OR (GLOBAL_TOKEN_CD = pv_old_global_token AND TERMINAL_STATE_ID IN(3, 5)));
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.TERMINAL
				   SET GLOBAL_TOKEN_CD = pv_new_global_token,
					   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
				 WHERE TERMINAL_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.ARAMARK_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.BLACKBRD_TENDER
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.INTERNAL_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
		
		IF ln_forced_unlock_count > 0 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM terminal ' || pn_payment_subtype_key_id || '(' || pv_payment_subtype_class || ') was locked for more than ' || ln_term_lock_max_duration_sec || ' seconds, forcefully unlocked',
				NULL,
				'PSS.PKG_SETTLEMENT.UPDATE_TERMINAL_GLOBAL_TOKEN'
			);
		END IF;
    END;
    
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER)
    IS
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = NULL,
                   TERMINAL_STATE_ID = DECODE(TERMINAL_STATE_ID, 5, 5, 1), -- terminal is active or settlement retry now
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
    END;

    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE
    IS
        ln_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_ID.NEXTVAL
          INTO ln_admin_cmd_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD (
                ADMIN_CMD_ID,
                ADMIN_CMD_TYPE_ID,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                REQUESTED_BY,
                PRIORITY) 
         VALUES(ln_admin_cmd_id, 
                pn_admin_cmd_type_id, 
                pn_payment_subtype_key_id,
                pv_payment_subtype_class,
                pv_requested_by,
                pn_priority);
         RETURN ln_admin_cmd_id;
    END;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE
    IS
        ln_admin_cmd_param_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_PARAM_ID.NEXTVAL
          INTO ln_admin_cmd_param_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD_PARAM (
                ADMIN_CMD_PARAM_ID,
                ADMIN_CMD_ID,
                PARAM_NAME,
                PARAM_VALUE) 
         VALUES(ln_admin_cmd_param_id, 
                pn_admin_cmd_id,
                pv_param_name, 
                pv_param_value);
         RETURN ln_admin_cmd_param_id;
    END;   
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
		ln_polling_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_FEEDBACK_POLLING_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts PSS.POSM_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
        SELECT POSM_SETTING_VALUE, 'N', LAST_UPDATED_UTC_TS
          INTO pv_locked_by_process_id, pv_success_flag, lt_last_updated_utc_ts
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
		 
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_polling_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM feedback polling has been locked by ' || pv_locked_by_process_id || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'PSS.PKG_SETTLEMENT.LOCK_PARTIAL_SETTLE_POLLING'
			);
			UNLOCK_PARTIAL_SETTLE_POLLING(pv_locked_by_process_id);
		END IF;
		
        SELECT POSM_SETTING_VALUE, 'N'
        INTO pv_locked_by_process_id, pv_success_flag
        FROM PSS.POSM_SETTING
        WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                INSERT INTO PSS.POSM_SETTING(POSM_SETTING_NAME, POSM_SETTING_VALUE)
                  VALUES('PARTIAL_SETTLEMENT_POLLING_LOCK', pv_process_id);
                pv_success_flag := 'Y';
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    LOCK_PARTIAL_SETTLE_POLLING(pv_process_id, pv_success_flag, pv_locked_by_process_id);
            END;
    END;
    
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
    BEGIN
        DELETE 
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK'
           AND POSM_SETTING_VALUE = pv_process_id;
        IF SQL%ROWCOUNT != 1 THEN
            RAISE NO_DATA_FOUND;
        END IF;
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
         WHERE SB.SETTLEMENT_BATCH_REF_CD = pv_settlement_batch_ref_cd
           AND H.HANDLER_CLASS = pv_payment_subtype_class
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
        
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
           
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
         WHERE M.MERCHANT_CD = pv_merchant_cd
           AND T.TERMINAL_CD = pv_terminal_cd
           AND TB.TERMINAL_BATCH_NUM = pv_terminal_batch_num
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
           
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
   
   -- R29+ signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE)
    IS
        ln_original_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
        lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
        lv_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
        ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        lc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE;
    BEGIN
        IF pn_sale_auth_id IS NOT NULL THEN
            ln_auth_id := pn_sale_auth_id;
        ELSIF pn_refund_id IS NOT NULL THEN
            ln_refund_id := pn_refund_id;
        ELSIF pc_tran_type_cd = '+' THEN
            SELECT AUTH_ID
              INTO ln_auth_id
              FROM (SELECT SA.AUTH_ID
              FROM PSS.AUTH A
              JOIN PSS.AUTH SA ON A.TRAN_ID = SA.TRAN_ID
             WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
               AND A.AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
               AND A.AUTH_TYPE_CD IN('L','N')
             ORDER BY SA.AUTH_TS DESC, SA.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        ELSIF pc_tran_type_cd = '-' THEN
            SELECT REFUND_ID
              INTO ln_refund_id
              FROM (SELECT R.REFUND_ID, R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                       AND R.REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
                       AND R.REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd
                       AND R.REFUND_STATE_ID IN(6, 1)
                     ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            RAISE_APPLICATION_ERROR(-20558, 'Invalid tran type cd "' ||  pc_tran_type_cd + '"');
        END IF;
        
        IF ln_auth_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.AUTH A ON A.TRAN_ID = X.TRAN_ID
                 WHERE A.AUTH_ID = ln_auth_id;
            END IF;
            UPDATE PSS.AUTH 
               SET AUTH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4),
                   AUTH_RESULT_CD = pc_auth_result_cd,
                   AUTH_RESP_CD = NVL(pv_authority_response_cd, AUTH_RESP_CD),
                   AUTH_RESP_DESC = NVL(pv_authority_response_desc, AUTH_RESP_DESC),
                   AUTH_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, AUTH_AUTHORITY_TRAN_CD),
                   AUTH_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, AUTH_AUTHORITY_REF_CD),
                   AUTH_AUTHORITY_TS = NVL(pd_authority_ts, AUTH_AUTHORITY_TS),
                   AUTH_AMT_APPROVED = NVL(pn_approved_amt / ln_minor_currency_factor, AUTH_AMT_APPROVED),
                   AUTH_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, AUTH_AUTHORITY_MISC_DATA)
             WHERE AUTH_ID = ln_auth_id
             RETURNING TRAN_ID, AUTH_TYPE_CD INTO ln_tran_id, lc_auth_type_cd;
            IF pn_settlement_batch_id IS NOT NULL THEN
                UPDATE PSS.TRAN_SETTLEMENT_BATCH
                   SET TRAN_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, TRAN_SETTLEMENT_B_AMT),
                       TRAN_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, TRAN_SETTLEMENT_B_RESP_CD),
                       TRAN_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, TRAN_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND AUTH_ID = ln_auth_id;               
            END IF;
        ELSIF ln_refund_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
                 WHERE R.REFUND_ID = ln_refund_id;
            END IF;          
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
             WHERE REFUND_ID = ln_refund_id
             RETURNING TRAN_ID, REFUND_TYPE_CD INTO ln_tran_id, lc_refund_type_cd;
            IF pn_settlement_batch_id IS NOT NULL THEN 
                UPDATE PSS.REFUND_SETTLEMENT_BATCH
                   SET REFUND_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, REFUND_SETTLEMENT_B_AMT),
                       REFUND_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, REFUND_SETTLEMENT_B_RESP_CD),
                       REFUND_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, REFUND_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND REFUND_ID = ln_refund_id;   
            END IF;
        END IF;
        
        -- update tran_state_cd
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = AFTER_SETTLE_TRAN_STATE_CD(lc_auth_type_cd, lc_refund_type_cd, pc_auth_result_cd, TRAN_DEVICE_RESULT_TYPE_CD)
         WHERE TRAN_ID = ln_tran_id
         RETURNING TRAN_GLOBAL_TRANS_CD INTO pv_tran_global_trans_cd;
    END;
    
    -- R28 signature
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR)
    IS
        lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
    BEGIN
        UPDATE_TRAN_FEEDBACK(
            pn_terminal_batch_id,
            pn_settlement_batch_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_tran_type_cd,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pc_auth_result_cd,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_misc_data,
            pd_authority_ts,
            pn_approved_amt,
            pn_sale_amt,
            pc_major_currency_used,
            lv_tran_global_trans_cd);
    END;
    
END PKG_SETTLEMENT;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?rev=1.72
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
    ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    ln_cnt PLS_INTEGER;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_lock_string VARCHAR2(100);
    ln_start INTEGER;
    ln_end INTEGER;
    lv_parsed_acct_data PSS.REFUND.REFUND_PARSED_ACCT_DATA%TYPE;
BEGIN
    ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
    ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
    IF ln_end <= 0 THEN
        ln_end := LENGTH(pv_global_trans_cd) + 1;
    END IF;
    lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
    ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
    ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
    -- check if refund already exists
    BEGIN
        SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
          INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
        RETURN;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_already_inserted_flag := 'N';
    END;
    -- Find original transaction
    BEGIN
        SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS
          INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
           AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
    END;
    
    INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_REPORTABLE_ACCT_NUM,
            TRAN_PARSED_ACCT_NUM_HASH,
            TRAN_PARSED_ACCT_NUM_ENCR,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME
			)
     SELECT pn_tran_id,
            pn_orig_tran_id,
            ld_refund_ts,
            ld_refund_ts,
            NULL, /* Must be NULL so that PSSUpdater will not pick it up */
            pv_global_trans_cd,
            '8',
            O.CONSUMER_ACCT_ID,
            SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
            O.POS_PTA_ID,
            O.TRAN_DEVICE_RESULT_TYPE_CD,
            O.TRAN_ACCOUNT_PIN,
            O.TRAN_RECEIVED_RAW_ACCT_DATA,
            O.TRAN_PARSED_ACCT_NAME,
            O.TRAN_PARSED_ACCT_EXP_DATE,
            O.TRAN_PARSED_ACCT_NUM,
            O.TRAN_REPORTABLE_ACCT_NUM,
            O.TRAN_PARSED_ACCT_NUM_HASH,
            O.TRAN_PARSED_ACCT_NUM_ENCR,
			pp.PAYMENT_SUBTYPE_KEY_ID,
			ps.PAYMENT_SUBTYPE_CLASS,
			ps.CLIENT_PAYMENT_TYPE_CD,
			d.DEVICE_NAME
	FROM PSS.TRAN O
	JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
	JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
	JOIN pss.pos p ON pp.pos_id = p.pos_id
	JOIN device.device d ON p.device_id = d.device_id
    WHERE O.TRAN_ID = pn_orig_tran_id;
    SELECT MAX(AUTH_PARSED_ACCT_DATA)
      INTO lv_parsed_acct_data
      FROM (SELECT AUTH_PARSED_ACCT_DATA
              FROM PSS.AUTH
             WHERE TRAN_ID = pn_orig_tran_id
               AND AUTH_PARSED_ACCT_DATA IS NOT NULL
             ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
     WHERE ROWNUM = 1;
    INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            REFUND_PARSED_ACCT_DATA,
            ACCT_ENTRY_METHOD_CD
        ) VALUES (
            pn_tran_id,
            -ABS(pn_refund_amt),
            pn_refund_desc,
            ld_refund_ts,
            pn_refund_issue_by,
            pn_refund_type_cd,
            6,
            lv_parsed_acct_data,
            pc_entry_method_cd);
END;

PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
    lv_tran_parsed_acct_num pss.tran.tran_parsed_acct_num%TYPE;
    ld_tran_server_ts DATE;
    ln_device_id device.device_id%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    lc_auth_hold_used PSS.TRAN.AUTH_HOLD_USED%TYPE;
    lv_orig_tran_state_cd pss.tran.tran_state_cd%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    pn_tran_id := 0;

    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);

    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

    BEGIN
        SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match, pos_pta_id, sale_type_cd, tran_parsed_acct_num, auth_hold_used
        INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match, ln_pos_pta_id, lc_sale_type_cd, lv_tran_parsed_acct_num, lc_auth_hold_used
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
                t.pos_pta_id, s.sale_type_cd, t.tran_parsed_acct_num, NVL(t.auth_hold_used, 'N') auth_hold_used
            FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
				t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
			)
            ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;
    
        ln_original_tran_id := pn_tran_id;
        lv_orig_tran_state_cd := pv_tran_state_cd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR ld_tran_upload_ts IS NOT NULL
        AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR lc_sale_type_cd = pc_sale_type_cd)) THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE OR pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;

            pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;

        ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
        ELSE
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                SELECT CASE WHEN (lc_auth_hold_used = 'Y' OR NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) > 0) AND NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) = 0 
                            THEN PKG_CONST.TRAN_STATE__COMPLETE_ERROR
                            ELSE PKG_CONST.TRAN_STATE__DUPLICATE
                       END
                  INTO pv_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        END IF;

        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;
		
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);

        PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);

        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd,
			payment_subtype_key_id,
			payment_subtype_class,
			client_payment_type_cd,
			device_name
        ) SELECT
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            pv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = ln_pos_pta_id;

        IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
        END IF;
    ELSIF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
            IF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                ELSE
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                END IF;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                    PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                    PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                    PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                    PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                    PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                    PKG_CONST.TRAN_STATE__INCOMPLETE,
                    PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                    PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
               -- don't change it
               NULL;
            ELSE                
                pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;
        ELSIF pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
             OR (pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
                AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            
            IF lv_tran_parsed_acct_num != NULL THEN
                INSERT INTO PSS.TRAN_C(TRAN_ID, KID, HASH_TYPE_CD, TRAN_PARSED_ACCT_NUM_H)
                VALUES(pn_tran_id, -1, 'SHA1', DBADMIN.HASH_CARD(lv_tran_parsed_acct_num));
                
                UPDATE PSS.TRAN
                SET TRAN_PARSED_ACCT_NUM = NULL,
                    TRAN_PARSED_ACCT_NAME = NULL,
                    TRAN_PARSED_ACCT_EXP_DATE = NULL
                WHERE TRAN_ID = pn_tran_id;
            END IF;
        ELSIF pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                PKG_CONST.TRAN_STATE__AUTH_FAILURE,
                PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                PKG_CONST.TRAN_STATE__INTENDED_ERROR) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
            IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                -- normal case
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                -- insert sale record
            ELSE
                -- sale actual uploaded
                -- don't change tran_state_cd
                -- don't update sale record
                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := 'Actual uploaded before intended';
                RETURN;
            END IF;
        -- we must let POSM processed cancelled sales too to do auth reversal
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
                PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                PKG_CONST.TRAN_STATE__BATCH_INTENDED)
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
            NULL;-- don't change tran_state_cd
        ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
             pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
        END IF;

        UPDATE pss.tran
        SET tran_state_cd = DECODE(TRAN_STATE_CD, lv_orig_tran_state_cd, pv_tran_state_cd, TRAN_STATE_CD), -- it might have changed if POSMLayer is processing it
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id;

        DELETE FROM pss.tran_line_item
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
    END IF;

    SELECT c.MINOR_CURRENCY_FACTOR
      INTO ln_minor_currency_factor
      FROM PSS.POS_PTA PTA
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
     WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount / ln_minor_currency_factor,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash,
        sale_device_session_id = pn_session_id
    WHERE tran_id = pn_tran_id;

    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash,
            sale_device_session_id
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount / ln_minor_currency_factor,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash,
            pn_session_id
        );
    END IF;
END;

FUNCTION sf_find_host_id(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
BEGIN
    SELECT MAX(H.HOST_ID)
      INTO ln_host_id
      FROM DEVICE.HOST H
     WHERE H.DEVICE_ID = pn_device_id
       AND H.HOST_PORT_NUM = pn_host_port_num
       AND H.HOST_POSITION_NUM = pn_host_position_num;

    IF ln_host_id IS NULL THEN
        -- Use base host
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
         WHERE H.DEVICE_ID = pn_device_id
           AND H.HOST_PORT_NUM = 0;
    END IF;
    
    RETURN ln_host_id;
END;

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_device_id host.device_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
    ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
BEGIN
    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID
      INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    WHERE X.TRAN_ID = pn_tran_id;

    ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    -- For Kiosk type, use tran_line_item_type to find description
    IF ln_device_type_id = 11 THEN
        SELECT TRIM(SUBSTR(tran_line_item_type_desc || ' ' || pv_tli_desc, 1, 60))
          INTO ln_tli_desc
          FROM pss.tran_line_item_type
         WHERE tran_line_item_type_id = pn_tli_type_id;
    ELSIF ln_device_type_id = 5 THEN -- eSuds
        SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
                || GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
          INTO ln_tli_desc
          FROM PSS.TRAN_LINE_ITEM_TYPE tlit
         CROSS JOIN DEVICE.HOST H
          JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
          LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
          JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
            ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = pn_tli_type_id
           AND H.HOST_ID = ln_host_id;
    ELSE
        ln_tli_desc := pv_tli_desc;
    END IF;

    INSERT INTO pss.tran_line_item (
        tran_line_item_id,
        tran_id,
        tran_line_item_amount,
        tran_line_item_position_cd,
        tran_line_item_tax,
        tran_line_item_type_id,
        tran_line_item_quantity,
        tran_line_item_desc,
        host_id,
        tran_line_item_batch_type_cd,
        tran_line_item_ts,
        sale_result_id
    )
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pn_tli_type_id,
        pn_tli_quantity,
        ln_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
        pn_sale_result_id
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = pn_tli_type_id;

    -- For all device actual batch type only
    IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
		SELECT tran_line_item_type_group_cd
		INTO lc_tli_type_group_cd
		FROM pss.tran_line_item_type
		WHERE tran_line_item_type_id = pn_tli_type_id;
	
		IF lc_tli_type_group_cd IN ('P', 'S') THEN
			UPDATE PSS.TRAN_LINE_ITEM_RECENT
			SET tran_line_item_id = ln_tran_line_item_id,
				fkp_tran_id = pn_tran_id
			WHERE host_id = ln_host_id
				AND tran_line_item_type_id = pn_tli_type_id;
				
			IF SQL%NOTFOUND THEN
				BEGIN
					INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
						TRAN_LINE_ITEM_ID,
						HOST_ID,
						FKP_TRAN_ID,
						TRAN_LINE_ITEM_TYPE_ID
					) VALUES (
						ln_tran_line_item_id,
						ln_host_id,
						pn_tran_id,
						pn_tli_type_id
					);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						UPDATE PSS.TRAN_LINE_ITEM_RECENT
						SET tran_line_item_id = ln_tran_line_item_id,
							fkp_tran_id = pn_tran_id
						WHERE host_id = ln_host_id
							AND tran_line_item_type_id = pn_tli_type_id;
				END;
			END IF;
		END IF;
    END IF;

END;

FUNCTION sf_tran_import_needed (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE
)
  RETURN VARCHAR2
IS
	lc_tran_import_needed VARCHAR2(1) := 'N';
BEGIN
	BEGIN
		SELECT DECODE(S.IMPORTED, 'Y', 'N', 'Y')
		INTO lc_tran_import_needed
		FROM PSS.TRAN T
		JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
		WHERE T.TRAN_ID = pn_tran_id
			AND T.TRAN_STATE_CD != PKG_CONST.TRAN_STATE__DUPLICATE
			AND S.SALE_TYPE_CD != PKG_CONST.SALE_TYPE__INTENDED
			AND DECODE(S.SALE_TYPE_CD, PKG_CONST.SALE_TYPE__CASH, 'N', DBADMIN.PKG_UTL.EQL(T.TRAN_RECEIVED_RAW_ACCT_DATA, NULL)) = 'N';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			NULL;
	END;
	RETURN lc_tran_import_needed;
END;

-- R26+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_tli_count NUMBER;
    ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	pc_tran_import_needed := 'N';

    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;

        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;

    SELECT NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
           COUNT(1)
      INTO ln_tli_total, ln_tli_count
      FROM PSS.TRAN_LINE_ITEM
     WHERE TRAN_ID = pn_tran_id
       AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID
      INTO ln_device_id, ln_minor_currency_factor, lc_client_payment_type_cd, ln_device_type_id
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
    WHERE X.TRAN_ID = pn_tran_id;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
        OR NVL(pn_sale_amount, 0) = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := NVL(pn_sale_amount / ln_minor_currency_factor, 0) - ln_tli_total;
        IF ln_adj_amt > 0 THEN
            ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;
    
    IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
		IF ln_tli_count > 0 AND ln_adj_amt != 0 THEN
            IF ln_adj_amt > 0 THEN
                -- create Transaction Amount Summary record
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                VALUES(
                    pn_tran_id,
                    ln_base_host_id,
                    201,
                    ln_adj_amt,
                    1,
                    'Transaction Amount Summary',
                    pc_tran_batch_type_cd,
                    pn_sale_tax);
            ELSE
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    ln_adj_amt,
                    1,
                    tran_line_item_type_desc,
                    pc_tran_batch_type_cd,
                    pn_sale_tax
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
            END IF;
		END IF;
   ELSE
        IF ln_adj_tli > -1 THEN
            -- use the base host for adjustments
            SELECT MAX(H.HOST_ID)
            INTO ln_base_host_id
            FROM DEVICE.HOST H
            WHERE H.DEVICE_ID = ln_device_id
            AND H.HOST_PORT_NUM = 0;
            IF ln_base_host_id IS NULL THEN
                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                    RETURN;
                END IF;
                SELECT H.HOST_ID
                INTO ln_base_host_id
                FROM DEVICE.HOST H
                WHERE H.DEVICE_ID = ln_device_id
                AND H.HOST_PORT_NUM = 0;
            END IF;
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                ln_adj_tli,
                ln_adj_amt,
                1,
                tran_line_item_type_desc,
                pc_tran_batch_type_cd,
                pn_sale_tax
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = ln_adj_tli;
        END IF;
            
   END IF;

    pkg_call_in_record.sp_add_trans(pn_session_id, lc_client_payment_type_cd, pn_sale_amount / ln_minor_currency_factor, ln_tli_count);

	pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

-- R25 signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL
)
IS
	lc_tran_import_needed VARCHAR2(1);
BEGIN
	sp_finalize_sale (
    pn_tran_id,
    pn_session_id,
    pv_tran_device_result_type_cd,
    pn_sale_result_id,
    pn_sale_amount,
    pn_sale_tax,
    pc_tran_batch_type_cd,
    pn_sale_utc_ts_ms,
    pn_sale_duration_sec,
    pn_result_cd,
    pv_error_message,
    pc_sale_type_cd,
	lc_tran_import_needed);
END;

-- R26+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE := pv_pan;
   lv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE := pv_card_holder;
   lv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE := pv_expiration_date;
   lv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE := pv_pin;
   lv_tran_reportable_acct_num PSS.TRAN.TRAN_REPORTABLE_ACCT_NUM%TYPE := SUBSTR(pv_pan, LENGTH(pv_pan) - 3, LENGTH(pv_pan));
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_event_cd);
   lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_event_cd);
BEGIN
	pc_tran_import_needed := 'N';
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
	
    BEGIN
        SELECT tran_id, sale_type_cd, tran_state_cd, trace_number, sale_result_id
        INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id
        FROM
        (
			SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, s.sale_type_cd, t.tran_state_cd, a.trace_number, s.sale_result_id
			FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
			WHERE t.tran_device_tran_cd = pv_device_event_cd AND (
					t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
					OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
					OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
				)
            ORDER BY CASE WHEN a.trace_number = pn_trace_number THEN 1 ELSE 2 END,
				CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				t.tran_start_ts, t.created_ts, a.created_ts
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_tran_id := NULL;
    END;
	
    IF pn_tran_id IS NOT NULL THEN
		IF ld_orig_trace_number = pn_trace_number THEN
			RETURN;
		END IF;
		
		IF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
            IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND)
                OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id != 0 /*Not 'Success'*/ THEN
				
				IF pc_auth_result_cd IN('Y', 'P') THEN
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
					END IF;
				ELSE
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
					END IF;
				END IF;
				
				UPDATE PSS.TRAN
				   SET (TRAN_START_TS,
						TRAN_END_TS,
						TRAN_STATE_CD,
						TRAN_ACCOUNT_PIN,
						TRAN_RECEIVED_RAW_ACCT_DATA,
						TRAN_PARSED_ACCT_NAME,
						TRAN_PARSED_ACCT_EXP_DATE,
						TRAN_PARSED_ACCT_NUM,
						TRAN_PARSED_ACCT_NUM_HASH,
						TRAN_REPORTABLE_ACCT_NUM,
						POS_PTA_ID,
						CONSUMER_ACCT_ID,
						AUTH_DEVICE_SESSION_ID,
						PAYMENT_SUBTYPE_KEY_ID,
						PAYMENT_SUBTYPE_CLASS,
						CLIENT_PAYMENT_TYPE_CD,						
						AUTH_HOLD_USED,
						DEVICE_NAME) =
					(SELECT
						ld_tran_start_ts,  /* TRAN_START_TS */
						TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
						pc_tran_state_cd,  /* TRAN_STATE_CD */
						lv_pin,  /* TRAN_ACCOUNT_PIN */
						pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
						lv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
						lv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
						lv_pan,  /* TRAN_PARSED_ACCT_NUM */
						pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
						lv_tran_reportable_acct_num,  /* TRAN_REPORTABLE_ACCT_NUM */
						pn_pos_pta_id, /* POS_PTA_ID */
						pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
						pn_session_id,
						pp.payment_subtype_key_id,
						ps.payment_subtype_class,
						ps.client_payment_type_cd,
						pc_auth_hold_used,
						pv_device_name
					FROM pss.pos_pta pp
					JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
					WHERE pp.pos_pta_id = pn_pos_pta_id)
					WHERE TRAN_ID = pn_tran_id;
			ELSE
				pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
				lc_invalid_device_event_cd := 'Y';
				lv_pin := NULL;
				lv_card_holder := NULL;
				lv_expiration_date := NULL;
				lv_pan := NULL;			
			END IF;
        END IF;
	ELSE
        IF pc_auth_result_cd IN('Y', 'P') AND pc_sent_to_device = 'N' AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('F') AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('Y') THEN
            pc_tran_state_cd := '6'; -- Auth Success
        ELSIF pc_auth_result_cd IN('P') THEN
            pc_tran_state_cd := '0'; -- Auth Success Conditional
        ELSIF pc_auth_result_cd IN('N', 'O') THEN
            pc_tran_state_cd := '7'; -- Auth Decline
        ELSIF pc_auth_result_cd IN('F') THEN
            pc_tran_state_cd := '5'; -- Auth Failure
        END IF;
	END IF;
		
    IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
          INTO pn_tran_id
          FROM DUAL;    
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
			TRAN_END_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_PARSED_ACCT_NUM_HASH,
            TRAN_REPORTABLE_ACCT_NUM,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID,
            AUTH_DEVICE_SESSION_ID,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			AUTH_HOLD_USED,
			DEVICE_NAME)
        SELECT
            pn_tran_id, /* TRAN_ID */
            ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts,  /* TRAN_END_TS */
            pc_tran_state_cd,  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            lv_pin,  /* TRAN_ACCOUNT_PIN */
            pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            lv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
            lv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
            lv_pan,  /* TRAN_PARSED_ACCT_NUM */
            pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
            lv_tran_reportable_acct_num,  /* TRAN_REPORTABLE_ACCT_NUM */
            pn_pos_pta_id, /* POS_PTA_ID */
            DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
            pn_session_id,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pc_auth_hold_used,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;
    END IF;

    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO ln_auth_id
      FROM DUAL;
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        AUTH_PARSED_ACCT_DATA,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        AUTH_BALANCE_AMT,
        TRACE_NUMBER,
        AUTH_ACTION_ID,
        AUTH_ACTION_BITMAP,
        AUTH_HOLD_USED)
     VALUES(
        ln_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        pv_auth_acct_data, /* AUTH_PARSED_ACCT_DATA */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        DECODE(pc_auth_result_cd, 'Y', pn_auth_amt, 'P', pn_received_amt) / pn_minor_currency_factor, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
        pn_trace_number, /* TRACE_NUMBER */
        pn_auth_action_id,
        pn_auth_action_bitmap,
        pc_auth_hold_used
        );

    IF pc_add_auth_hold = 'Y' THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID)
          VALUES(pn_consumer_acct_id, ln_auth_id, pn_tran_id);
    END IF;
	
	pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
END;

-- R25 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2
) IS
	lc_tran_import_needed VARCHAR2(1);
BEGIN
	SP_CREATE_AUTH(
   pv_global_event_cd,
   pv_device_name,
   pn_pos_pta_id,
   pv_device_event_cd,
   pc_invalid_device_event_cd,
   pn_tran_start_time,
   pc_auth_result_cd,
   pc_entry_method,
   pc_payment_type,
   pv_track_data,
   pv_auth_acct_data,
   pv_pan,
   pv_pan_sha1,
   pv_card_holder,
   pv_expiration_date,
   pv_pin,
   pn_consumer_acct_id,
   pd_auth_ts,
   pv_authority_resp_cd,
   pv_authority_resp_desc,
   pv_authority_tran_cd,
   pv_authority_ref_cd,
   pt_authority_ts,
   pv_authority_misc_data,
   pn_trace_number,
   pn_minor_currency_factor,
   pn_auth_amt,
   pn_balance_amt,
   pn_requested_amt,
   pn_received_amt,
   pc_add_auth_hold,
   pc_auth_hold_used,
   pn_session_id,
   pc_ignore_dup,
   pn_auth_action_id,
   pn_auth_action_bitmap,
   pc_sent_to_device,
   pc_pass_thru,
   pn_tran_id,
   pc_tran_state_cd,
   lc_tran_import_needed); 
END;

PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      SELECT * FROM (
        SELECT pn_tran_id TRAN_ID, 2 /* live auth "POSM" time*/ TRAN_STAT_TYPE_ID, (pn_applayer_end_time - pn_applayer_start_time) / 1000 TRAN_STAT_VALUE FROM DUAL WHERE pn_applayer_start_time IS NOT NULL AND pn_applayer_end_time IS NOT NULL
        UNION ALL SELECT pn_tran_id, 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000 FROM DUAL WHERE pn_request_time IS NOT NULL AND pn_response_time IS NOT NULL
        UNION ALL SELECT pn_tran_id, 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000 FROM DUAL WHERE pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL) a
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_STAT TS WHERE A.TRAN_ID = TS.TRAN_ID AND A.TRAN_STAT_TYPE_ID = TS.TRAN_STAT_TYPE_ID);    
END;

PROCEDURE SP_PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER
)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND CA.CONSUMER_ACCT_CD = pv_consumer_acct_cd
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CURRENCY_CD = pv_currency_cd
            GROUP BY CA.CONSUMER_ACCT_ID, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
                     ELSE DTA.ACTION_ID END ACTION_ID
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.FILE_TRANSFER ft
             ON ft.FILE_TRANSFER_NAME = DECODE(DTA.DEVICE_TYPE_ID,
                        0, 'G4-DEFAULT-CFG',
                        1, 'G5-DEFAULT-CFG',
                        6, 'MEI-DEFAULT-CFG',
                        12, 'T2-DEFAULT-CFG',
                        'DEFAULT-CFG-' || DTA.DEVICE_TYPE_ID || '-' || ds_v.DEVICE_SETTING_VALUE)
            AND ft.FILE_TRANSFER_TYPE_CD = DECODE(DTA.DEVICE_TYPE_ID, 0, 6, 1, 6, 6, 6, 12, 15, 22)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_global_event_cd_prefix IN CHAR,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pn_sale_utc_ts_ms NUMBER,
   pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
   pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
   pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
   pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
   pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
   pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pn_result_cd OUT NUMBER,
   pv_error_message OUT VARCHAR2
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
   lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
   lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
   ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
   ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
   ln_tli_hash_match NUMBER;
   ld_current_ts DATE := SYSDATE;
   ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
   lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
   lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
BEGIN
    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
    BEGIN
        SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
        INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
				t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
			)
            ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;

            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;
	
		ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
			lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
		ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
			lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
		END IF;
		
		SELECT PSS.SEQ_TRAN_ID.NEXTVAL
		INTO pn_tran_id
		FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;		

		INSERT INTO PSS.TRAN (
			TRAN_ID,
			TRAN_START_TS,
			TRAN_END_TS,
			TRAN_UPLOAD_TS,
			TRAN_STATE_CD,
			TRAN_DEVICE_TRAN_CD,
			TRAN_RECEIVED_RAW_ACCT_DATA,
			TRAN_PARSED_ACCT_NUM_HASH,
			TRAN_REPORTABLE_ACCT_NUM,
			POS_PTA_ID,
			TRAN_GLOBAL_TRANS_CD,
			CONSUMER_ACCT_ID,
			TRAN_DEVICE_RESULT_TYPE_CD,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME)
		SELECT
			pn_tran_id, /* TRAN_ID */
			ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts, /* TRAN_END_TS */
			ld_current_ts,
			lv_tran_state_cd,  /* TRAN_STATE_CD */
			pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
			pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
			pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
			SUBSTR(pv_track_data, -4, 4),  /* TRAN_REPORTABLE_ACCT_NUM */
			pn_pos_pta_id, /* POS_PTA_ID */
			lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
			pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
			pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;

		SELECT PSS.SEQ_AUTH_ID.NEXTVAL
		  INTO ln_auth_id
		  FROM DUAL;
		INSERT INTO PSS.AUTH (
			AUTH_ID,
			TRAN_ID,
			AUTH_STATE_ID,
			AUTH_TYPE_CD,
			AUTH_PARSED_ACCT_DATA,
			ACCT_ENTRY_METHOD_CD,
			AUTH_TS,
			AUTH_RESULT_CD,
			AUTH_RESP_CD,
			AUTH_RESP_DESC,
			AUTH_AUTHORITY_TS,
			AUTH_AMT,
			AUTH_AUTHORITY_AMT_RQST,
			TRACE_NUMBER)
		 VALUES(
			ln_auth_id, /* AUTH_ID */
			pn_tran_id, /* TRAN_ID */
			DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
			'L', /* AUTH_TYPE_CD */
			pv_track_data, /* AUTH_PARSED_ACCT_DATA */
			DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
			pd_auth_ts, /* AUTH_TS */
			pc_auth_result_cd, /* AUTH_RESULT_CD */
			'LOCAL', /* AUTH_RESP_CD */
			'Local authorization not accepted', /* AUTH_RESP_DESC */
			pd_auth_ts, /* AUTH_AUTHORITY_TS */
			NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
			pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
			pn_trace_number /* TRACE_NUMBER */
			);

		INSERT INTO pss.sale (
			tran_id,
			device_batch_id,
			sale_type_cd,
			sale_start_utc_ts,
			sale_end_utc_ts,
			sale_utc_offset_min,
			sale_result_id,
			sale_amount,
			receipt_result_cd,
			hash_type_cd,
			tran_line_item_hash
		) VALUES (
			pn_tran_id,
			pn_device_batch_id,
			pc_sale_type_cd,
			lt_sale_start_utc_ts,
			lt_sale_start_utc_ts,
			pn_sale_utc_offset_min,
			pn_sale_result_id,
			pn_auth_amt / pn_minor_currency_factor,
			'U',
			pv_hash_type_cd,
			pv_tran_line_item_hash
		);
	END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/QUARTZ/create_quartz_tables.sql?rev=HEAD
-- Default Quartz Configuration, updated with the following:
-- * QUARTZ.QRTZ_ prefix
-- * TABLESPACES
-- * PK and FK names
-- DELETE statements removed
-- DROP statements moved to drop_quartz_tables.sql
--
-- A hint submitted by a user: Oracle DB MUST be created as "shared" and the 
-- job_queue_processes parameter  must be greater than 2, otherwise a DB lock 
-- will happen.   However, these settings are pretty much standard after any
-- Oracle install, so most users need not worry about this.
--
-- Many other users (including the primary author of Quartz) have had success
-- runing in dedicated mode, so only consider the above as a hint ;-)
--

CREATE TABLE QUARTZ.qrtz_job_details
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR2(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT PK_QRTZ_JOB_DETAILS PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL, 
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT PK_QRTZ_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_QJD_SCHED_N_JOB_N_AND_G FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP) 
	REFERENCES QUARTZ.qrtz_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP) 
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_simple_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(10) NOT NULL,
    CONSTRAINT PK_QRTZ_SIMPLE_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_QT_SCHED_N_TRIGGER_N_AND_G FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES QUARTZ.qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_cron_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSION VARCHAR2(120) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    CONSTRAINT PK_QRTZ_CRON_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_QCTQT_SCHED_N_TRIG_N_AND_G FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES QUARTZ.qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_simprop_triggers
  (          
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    STR_PROP_1 VARCHAR(512) NULL,
    STR_PROP_2 VARCHAR(512) NULL,
    STR_PROP_3 VARCHAR(512) NULL,
    INT_PROP_1 NUMBER(10) NULL,
    INT_PROP_2 NUMBER(10) NULL,
    LONG_PROP_1 NUMBER(13) NULL,
    LONG_PROP_2 NUMBER(13) NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR(1) NULL,
    BOOL_PROP_2 VARCHAR(1) NULL,
    CONSTRAINT PK_QRTZ_SIMPROP_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT FK_QSTQT_SCHED_N_TRIG_N_AND_G FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES QUARTZ.qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_blob_triggers
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    CONSTRAINT PK_QRTZ_BLOB_TRIGGERS PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES QUARTZ.qrtz_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_calendars
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    CALENDAR_NAME  VARCHAR2(200) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    CONSTRAINT PK_QRTZ_CALENDARS PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_paused_trigger_grps
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL, 
    CONSTRAINT PK_QRTZ_PAUSED_TRIGGER_GRPS PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_fired_triggers 
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_NONCONCURRENT VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    CONSTRAINT PK_QRTZ_FIRED_TRIGGERS PRIMARY KEY (SCHED_NAME,ENTRY_ID)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_scheduler_state 
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    CONSTRAINT PK_QRTZ_SCHEDULER_STATE PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
) TABLESPACE QUARTZ_DATA;
CREATE TABLE QUARTZ.qrtz_locks
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    CONSTRAINT PK_QRTZ_LOCKS PRIMARY KEY (SCHED_NAME,LOCK_NAME)
) TABLESPACE QUARTZ_DATA;

create index QUARTZ.idx_qrtz_j_req_recovery on QUARTZ.qrtz_job_details(SCHED_NAME,REQUESTS_RECOVERY) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_j_grp on QUARTZ.qrtz_job_details(SCHED_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;

create index QUARTZ.idx_qrtz_t_j on QUARTZ.qrtz_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_jg on QUARTZ.qrtz_triggers(SCHED_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_c on QUARTZ.qrtz_triggers(SCHED_NAME,CALENDAR_NAME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_g on QUARTZ.qrtz_triggers(SCHED_NAME,TRIGGER_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_state on QUARTZ.qrtz_triggers(SCHED_NAME,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_n_state on QUARTZ.qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_n_g_state on QUARTZ.qrtz_triggers(SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_next_fire_time on QUARTZ.qrtz_triggers(SCHED_NAME,NEXT_FIRE_TIME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_nft_st on QUARTZ.qrtz_triggers(SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_nft_misfire on QUARTZ.qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_nft_st_misfire on QUARTZ.qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_t_nft_st_misfire_grp on QUARTZ.qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE) TABLESPACE QUARTZ_INDX;

create index QUARTZ.idx_qrtz_ft_trig_inst_name on QUARTZ.qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_ft_inst_job_req_rcvry on QUARTZ.qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_ft_j_g on QUARTZ.qrtz_fired_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_ft_jg on QUARTZ.qrtz_fired_triggers(SCHED_NAME,JOB_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_ft_t_g on QUARTZ.qrtz_fired_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) TABLESPACE QUARTZ_INDX;
create index QUARTZ.idx_qrtz_ft_tg on QUARTZ.qrtz_fired_triggers(SCHED_NAME,TRIGGER_GROUP) TABLESPACE QUARTZ_INDX;


commit;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R29/grant_quartz.sql?rev=HEAD
grant select on QUARTZ.qrtz_calendars to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_fired_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_blob_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_cron_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_simple_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_simprop_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_triggers to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_job_details to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_paused_trigger_grps to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_locks to USAT_DEV_READ_ONLY;
grant select on QUARTZ.qrtz_scheduler_state to USAT_DEV_READ_ONLY;

grant select,insert,update,delete on QUARTZ.qrtz_calendars to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_fired_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_blob_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_cron_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_simple_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_simprop_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_triggers to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_job_details to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_paused_trigger_grps to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_locks to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_scheduler_state to USAT_APP_LAYER_ROLE, USAT_POSM_ROLE;

grant insert,update,delete on QUARTZ.qrtz_calendars to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_fired_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_blob_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_cron_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_simple_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_simprop_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_triggers to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_job_details to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_paused_trigger_grps to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_locks to USATECH_UPD_TRANS;
grant insert,update,delete on QUARTZ.qrtz_scheduler_state to USATECH_UPD_TRANS;

