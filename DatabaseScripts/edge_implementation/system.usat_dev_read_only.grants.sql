BEGIN
    FOR x IN (SELECT * FROM all_objects WHERE
        owner IN ('AUTHORITY', 'APP_EXEC_HIST', 'APP_LOG', 'APP_USER', 'CORP', 'DEVICE', 'DBADMIN', 'ENGINE',
            'ESUDS_OPERATOR_REPORT_USER', 'FOLIO_CONF', 'G4OP', 'LOCATION', 'PSS', 'RDW', 'RECON', 'REPORT',
            'SONY', 'TRACKING', 'UPDATER', 'USA_CUSTOM', 'WEB_CONTENT')
        AND object_type IN ('TABLE', 'VIEW', 'SEQUENCE')
        AND status = 'VALID'
    )
    LOOP
      DBMS_OUTPUT.PUT_LINE('Adding SELECT privilege on ' || x.owner || '.' || x.object_name);
      EXECUTE IMMEDIATE 'GRANT SELECT ON ' || x.owner || '.' || x.object_name || ' TO USAT_DEV_READ_ONLY';
    END LOOP;

    FOR x IN (SELECT * FROM all_objects WHERE
        owner IN ('AUTHORITY', 'APP_EXEC_HIST', 'APP_LOG', 'APP_USER', 'CORP', 'DEVICE', 'DBADMIN', 'ENGINE',
            'ESUDS_OPERATOR_REPORT_USER', 'FOLIO_CONF', 'G4OP', 'LOCATION', 'PSS', 'RDW', 'RECON', 'REPORT',
            'SONY', 'TRACKING', 'UPDATER', 'USA_CUSTOM', 'WEB_CONTENT')
        AND object_type IN ('FUNCTION', 'PACKAGE', 'PACKAGE BODY', 'PROCEDURE', 'TYPE')
        AND status = 'VALID'
    )
    LOOP
      DBMS_OUTPUT.PUT_LINE('Adding DEBUG privilege on ' || x.owner || '.' || x.object_name);
      EXECUTE IMMEDIATE 'GRANT DEBUG ON ' || x.owner || '.' || x.object_name || ' TO USAT_DEV_READ_ONLY';
    END LOOP;
END;
