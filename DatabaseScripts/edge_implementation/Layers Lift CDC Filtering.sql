DECLARE
    CURSOR l_cur IS
        SELECT '' new_cond, '' table_owner, '' table_name from dual where 0=1
UNION ALL SELECT '(:dml.get_object_owner() = ''DEVICE'' and :dml.get_object_name() = ''DEVICE'' and (
        (:dml.get_command_type() = ''INSERT'' and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'')) NOT LIKE ''APP_LAYER%'' and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''DEVICE_ACTIVE_YN_FLAG'')) = ''Y'')
     or (:dml.get_command_type() = ''UPDATE''
        and (
                DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''DEVICE_ACTIVE_YN_FLAG''), :dml.GET_VALUE(''old'', ''DEVICE_ACTIVE_YN_FLAG'')) = ''N''
             or (
                    DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''DEVICE_ACTIVE_YN_FLAG'')) = ''Y''
                    and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'', ''N'')) NOT LIKE ''APP_LAYER%''
                    and (
                        DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''DEVICE_NAME''), :dml.GET_VALUE(''old'', ''DEVICE_NAME'')) = ''N''
                     or DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''DEVICE_TYPE_ID''), :dml.GET_VALUE(''old'', ''DEVICE_TYPE_ID'')) = ''N''
                     or DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''ENCRYPTION_KEY''), :dml.GET_VALUE(''old'', ''ENCRYPTION_KEY'')) = ''N''
                     or DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''PREVIOUS_ENCRYPTION_KEY''), :dml.GET_VALUE(''old'', ''PREVIOUS_ENCRYPTION_KEY'')) = ''N''
                    )
                 )
             )
        )
    or (:dml.get_command_type() = ''DELETE'' and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''old'', ''DEVICE_ACTIVE_YN_FLAG'')) = ''Y'')
    )
)
', 'DEVICE', 'DEVICE' FROM DUAL
UNION ALL SELECT '(:dml.get_object_owner() = ''DEVICE'' and :dml.get_object_name() = ''DEVICE_SETTING'' 
    and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'', ''N'')) NOT LIKE ''APP_LAYER%''
    and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''DEVICE_SETTING_PARAMETER_CD'')) IN(''60'', ''61'', ''70'')
	and DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''DEVICE_SETTING_VALUE''), :dml.GET_VALUE(''old'', ''DEVICE_SETTING_VALUE'')) = ''N'' 
)', 'DEVICE', 'DEVICE_SETTING' FROM DUAL
UNION ALL SELECT '(:dml.get_object_owner() = ''ENGINE'' and :dml.get_object_name() = ''MACHINE_CMD_PENDING'' 
    and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'', ''N'')) NOT LIKE ''APP_LAYER%''
    and DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''EXECUTE_CD''), :dml.GET_VALUE(''old'', ''EXECUTE_CD'')) = ''N'' 
)', 'ENGINE', 'MACHINE_CMD_PENDING' FROM DUAL
UNION ALL SELECT '(:dml.get_object_owner() = ''LOCATION'' and :dml.get_object_name() = ''LOCATION'' 
    and DBADMIN.PKG_UTL.GET_VARCHAR_SAFELY(:dml.GET_VALUE(''new'', ''LAST_UPDATED_BY'', ''N'')) NOT LIKE ''APP_LAYER%''
    and DBADMIN.PKG_UTL.EQL(:dml.GET_VALUE(''new'', ''LOCATION_TIME_ZONE_CD''), :dml.GET_VALUE(''old'', ''LOCATION_TIME_ZONE_CD'')) = ''N'' 
)', 'LOCATION', 'LOCATION' FROM DUAL;
    lv_rule_owner ALL_RULES.RULE_OWNER%TYPE;
    lv_rule_name ALL_RULES.RULE_NAME%TYPE;
    lv_rule_condition VARCHAR2(4000);
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT r.RULE_OWNER, r.RULE_NAME, DBMS_LOB.SUBSTR(R.RULE_CONDITION, 4000, 1)
          INTO lv_rule_owner, lv_rule_name, lv_rule_condition
          FROM DBA_RULES r
          JOIN DBA_RULE_SET_RULES RSR ON R.RULE_OWNER = RSR.RULE_OWNER AND R.RULE_NAME = RSR.RULE_NAME
          JOIN DBA_CAPTURE D ON D.RULE_SET_OWNER = RSR.RULE_SET_OWNER AND D.RULE_SET_NAME = RSR.RULE_SET_NAME
         WHERE D.CAPTURE_NAME = 'CHANGE_CAPTURE'
           AND REGEXP_LIKE(R.RULE_NAME, l_rec.TABLE_NAME || '[0-9]+','xn')
           AND R.RULE_OWNER = 'REPL_MGR' 
           AND REGEXP_LIKE(DBMS_LOB.SUBSTR(R.RULE_CONDITION, 4000, 1), '^(\s*\()+\s*:dml\.get_object_owner\(\s*\)\s*=\s*''' || l_rec.TABLE_OWNER || '''\s+and\s+:dml\.get_object_name\(\s*\)\s*=\s*''' || l_rec.TABLE_NAME || '''', 'xin');
        DBMS_RULE_ADM.ALTER_RULE(rule_name => lv_rule_owner || '.' || lv_rule_name, condition => l_rec.new_cond);
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Updated rule "' || lv_rule_owner || '.' || lv_rule_name || '":');
        DBMS_OUTPUT.PUT_LINE('from: ' || lv_rule_condition);
        DBMS_OUTPUT.PUT_LINE('to  : ' || l_rec.new_cond);
    END LOOP;
END;
/

/*
select * from dba_rules r
join dba_rule_set_rules rsr on r.rule_owner = rsr.rule_owner and r.rule_name = rsr.rule_name
join (select capture_name capture_apply_name, queue_name, queue_owner, rule_set_name, rule_set_owner from dba_capture
union all select apply_name, queue_name, queue_owner, rule_set_name, rule_set_owner from dba_apply) d on d.rule_set_owner = rsr.rule_set_owner and d.rule_set_name = rsr.rule_set_name
where REGEXP_LIKE(DBMS_LOB.SUBSTR(R.RULE_CONDITION, 4000, 1), '^(\s*\()+\s*:dml\.get_object_owner\(\s*\)\s*=\s*''DEVICE''\s+and\s+:dml\.get_object_name\(\s*\)\s*=\s*''DEVICE''(\s*\))+\s*$', 'xin')
-- REGEXP_LIKE(DBMS_LOB.SUBSTR(R.RULE_CONDITION, 4000, 1), '\(+:dml\.get_object_owner\(\)=''DEVICE'' and :dml\.get_object_name\(\)=''DEVICE''\)+', 'xin')

select * from dba_capture
select DBMS_LOB.SUBSTR(REPL_MGR.REPLICATE_PKG.GET_XML(q.USER_DATA), 4000, 1), q.* from repl_mgr.qt_change_apply_layers q;

select state, count(*), min(enq_time), max(enq_time)
from repl_mgr.qt_change_apply_layers
group by state;


*/
