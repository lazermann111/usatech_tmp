DECLARE
	l_device_id device.device_id%TYPE;
	CURSOR l_dev_cur IS SELECT device_id FROM device.host WHERE host_port_num = 0 GROUP BY device_id HAVING COUNT(1) > 1;
	l_dev_rec l_dev_cur%ROWTYPE;
BEGIN
	FOR l_dev_rec IN l_dev_cur LOOP
		SELECT SEQ_DEVICE_ID.NEXTVAL INTO l_device_id FROM DUAL;

		INSERT INTO device.device(device_id, device_name, device_serial_cd, device_type_id, device_active_yn_flag, encryption_key, device_encr_key_gen_ts)
		SELECT l_device_id, device_name, device_serial_cd, device_type_id, 'N', encryption_key, device_encr_key_gen_ts FROM device.device WHERE device_id = l_dev_rec.device_id;

		UPDATE device.host SET device_id = l_device_id
		WHERE host_id = (SELECT MAX(host_id) FROM device.host WHERE device_id = l_dev_rec.device_id AND host_port_num = 0);
	END LOOP;
	
	COMMIT;		
END;