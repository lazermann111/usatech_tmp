UPDATE DEVICE.DEVICE_TYPE SET DEVICE_TYPE_SERIAL_CD_REGEX = '^K(1[0-9A-F]{16}|2MT[0-9A-Z]{6,16})$' WHERE DEVICE_TYPE_ID = 11;

GRANT SELECT ON TAZDBA.RERIX_MACHINE_ID_SEQ TO DEVICE;

CREATE SEQUENCE DEVICE.SEQ_TD_DEVICE_NAME INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE OR REPLACE FUNCTION DEVICE.SF_GENERATE_DEVICE_NAME
(
    PN_DEVICE_TYPE_ID IN DEVICE_TYPE.DEVICE_TYPE_ID%TYPE
)
RETURN VARCHAR2 IS

LS_DEVICE_NAME VARCHAR2(8);

BEGIN
    IF PN_DEVICE_TYPE_ID = 13 THEN
        SELECT 'TD' || LPAD(SEQ_TD_DEVICE_NAME.NEXTVAL, 6, '0') INTO LS_DEVICE_NAME FROM DUAL;
    ELSE
        SELECT 'EV' || LPAD(RERIX_MACHINE_ID_SEQ.NEXTVAL, 6, '0') INTO LS_DEVICE_NAME FROM DUAL;
    END IF;

    RETURN LS_DEVICE_NAME;
END;
/

GRANT EXECUTE ON DEVICE.SF_GENERATE_DEVICE_NAME TO USAT_WEB_ROLE;

GRANT SELECT ON DEVICE.SEQ_DEVICE_ID TO USAT_WEB_ROLE;



CREATE TABLE DEVICE.DEVICE_DEFAULT
(
    device_default_id              NUMBER(20,0) NOT NULL,
    device_default_name            VARCHAR2(60) NOT NULL,
    device_default_desc            VARCHAR2(60) NOT NULL,
    device_default_active_yn_flag  VARCHAR2(1) NOT NULL,
    encryption_key                 RAW(64),
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,    
    CONSTRAINT pk_device_default_id PRIMARY KEY(device_default_id)
)
TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_DEFAULT BEFORE INSERT ON DEVICE.DEVICE_DEFAULT
  FOR EACH ROW
BEGIN
	IF :NEW.device_default_id IS NULL
	THEN
		SELECT SEQ_DEVICE_ID.NEXTVAL
		INTO :NEW.device_default_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_DEFAULT BEFORE UPDATE ON DEVICE.DEVICE_DEFAULT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.DEVICE_DEFAULT TO USAT_WEB_ROLE;

INSERT INTO DEVICE.DEVICE_DEFAULT(device_default_name, device_default_desc, device_default_active_yn_flag, encryption_key)
VALUES('EV000000', 'Default device name', 'Y', utl_raw.cast_to_raw('9265027739274401'));

INSERT INTO DEVICE.DEVICE_DEFAULT(device_default_name, device_default_desc, device_default_active_yn_flag, encryption_key)
VALUES('TD000000', 'Default Edge device name', 'Y', hextoraw('6DF7E5A6FBC84C50A79F76762826C0DC'));


GRANT EXECUTE ON PSS.PKG_POS_PTA TO USAT_WEB_ROLE;

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_ID, POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
VALUES(13, 'DEFAULT: Edge Initialization', 'Default template for Edge during initialization');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(
       pos_pta_tmpl_id, payment_subtype_id,
       pos_pta_encrypt_key, pos_pta_activation_oset_hr,
       pos_pta_deactivation_oset_hr, payment_subtype_key_id,
       pos_pta_pin_req_yn_flag, pos_pta_device_serial_cd,
       pos_pta_encrypt_key2, authority_payment_mask_id,
       pos_pta_priority, terminal_id, merchant_bank_acct_id,
       currency_cd, pos_pta_passthru_allow_yn_flag,
       pos_pta_pref_auth_amt, pos_pta_pref_auth_amt_max)
SELECT 13, payment_subtype_id,
       pos_pta_encrypt_key, pos_pta_activation_oset_hr,
       pos_pta_deactivation_oset_hr, payment_subtype_key_id,
       pos_pta_pin_req_yn_flag, pos_pta_device_serial_cd,
       pos_pta_encrypt_key2, authority_payment_mask_id,
       pos_pta_priority, terminal_id, merchant_bank_acct_id,
       currency_cd, pos_pta_passthru_allow_yn_flag,
       pos_pta_pref_auth_amt, pos_pta_pref_auth_amt_max
FROM PSS.POS_PTA_TMPL_ENTRY WHERE POS_PTA_TMPL_ID =
(SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: G5 Initialization');

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(201, 'ePort Edge', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(202, 'Modem', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(203, 'Card Reader', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(311, 'Printer', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(312, 'Copier', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(313, 'Fax', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(314, 'Scanner', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(315, 'Laptop', 0);

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
VALUES(316, 'MFP', 0);


INSERT INTO DEVICE.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC, DEVICE_TYPE_SERIAL_CD_REGEX, POS_PTA_TMPL_ID, DEF_BASE_HOST_TYPE_ID, DEF_BASE_HOST_EQUIPMENT_ID, DEF_PRIM_HOST_TYPE_ID, DEF_PRIM_HOST_EQUIPMENT_ID)
VALUES(13, 'ePort Edge', '^EE1[0-9]{8}$', 13,
	201,
	(SELECT HOST_EQUIPMENT_ID FROM DEVICE.HOST_EQUIPMENT WHERE HOST_EQUIPMENT_MFGR = 'USA Technologies, Inc.' AND HOST_EQUIPMENT_MODEL = 'Unknown'),
	200,
	(SELECT HOST_EQUIPMENT_ID FROM DEVICE.HOST_EQUIPMENT WHERE HOST_EQUIPMENT_MFGR = 'Unknown' AND HOST_EQUIPMENT_MODEL = 'Unknown')
);

INSERT INTO DEVICE.DEVICE_TYPE_HOST_TYPE(DEVICE_TYPE_HOST_TYPE_CD, DEVICE_TYPE_ID, HOST_TYPE_ID)
VALUES('B', 13, 201);

INSERT INTO DEVICE.DEVICE_TYPE_HOST_TYPE(DEVICE_TYPE_HOST_TYPE_CD, DEVICE_TYPE_ID, HOST_TYPE_ID)
VALUES('V', 13, 200);

INSERT INTO device.device_serial_format(device_type_id, device_serial_format_name, device_serial_format_desc, device_serial_format_prefix, device_serial_format_length, device_serial_format_num_base)
VALUES(13, 'Edge Serial Format', 'Format of Edge serial code', 'EE1', 11, 10);


INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES(13, 2, 1);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES(13, 3, 2);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES(13, 4, 3);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES(13, 5, 4);
INSERT INTO device.device_type_action(device_type_id, action_id, device_type_action_cd) VALUES(13, 6, 5);


INSERT INTO device.device_setting_parameter(device_setting_parameter_cd, device_setting_parameter_ubn) VALUES('Protocol Revision', -999999);
INSERT INTO device.device_setting_parameter(device_setting_parameter_cd, device_setting_parameter_ubn) VALUES('Property List Version', -999999);
UPDATE device.device_setting_parameter SET device_setting_parameter_ubn = -999999 WHERE device_setting_parameter_cd = 'Firmware Version';
UPDATE device.device_setting_parameter SET device_setting_parameter_ubn = -999999 WHERE device_setting_parameter_cd = 'New Merchant ID';

INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name) VALUES(18, 'Generic Binary Data');
INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name) VALUES(19, 'Property List');
INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name) VALUES(20, 'Property List Request');
INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name) VALUES(21, 'Message Set');
INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name) VALUES(22, 'Edge Default Configuration Template');
INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name) VALUES(23, 'Edge Custom Configuration Template');


INSERT INTO device.file_transfer(file_transfer_name, file_transfer_type_cd, file_transfer_content)
VALUES('DEFAULT-CFG-13-0', 22, '');


CREATE TABLE DEVICE.DEVICE_TYPE_PROPERTY_LIST
(
    device_type_id                 NUMBER(20,0) NOT NULL,
    property_list_version          NUMBER(20,0) NOT NULL,
    all_property_list_ids	   CLOB NOT NULL,
    configurable_property_list_ids CLOB NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,    
    CONSTRAINT pk_device_type_property_list PRIMARY KEY(device_type_id, property_list_version),
    CONSTRAINT FK_DEV_TYP_PROP_LST_DEV_TYP_ID foreign key(device_type_id) references DEVICE.DEVICE_TYPE(DEVICE_TYPE_ID)
)
TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_TYPE_PROPERTY_LIST BEFORE INSERT ON DEVICE.DEVICE_TYPE_PROPERTY_LIST
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_TYPE_PROPERTY_LIST BEFORE UPDATE ON DEVICE.DEVICE_TYPE_PROPERTY_LIST
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.DEVICE_TYPE_PROPERTY_LIST TO USAT_WEB_ROLE;

INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
VALUES(13, 0, 
	'0-7|10|20-27|30-33|50-64|70|80|81|85|86|100-108|200-208',
	'0-7|10|20-27|30-33|53-59|70|85|86|100-108|200-208');
	
	
CREATE TABLE DEVICE.GENERIC_RESPONSE_S2C_ACTION
(
    device_type_id                 NUMBER(20,0) NOT NULL,    
    action_cd 	   		   NUMBER(20,0) NOT NULL,
    action_desc   		   VARCHAR2(60) NOT NULL,
    protocol_revision              NUMBER(20,0) NOT NULL,
    ui_visible_flag                CHAR(1) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,    
    CONSTRAINT PK_GENERIC_RESPONSE_S2C_ACTION PRIMARY KEY(device_type_id, action_cd),
    CONSTRAINT FK_GEN_RESP_S2C_ACT_DEV_TYP_ID foreign key(device_type_id) references DEVICE.DEVICE_TYPE(DEVICE_TYPE_ID)
)
TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_GEN_RESPONSE_S2C_ACTION BEFORE INSERT ON DEVICE.GENERIC_RESPONSE_S2C_ACTION
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_GEN_RESPONSE_S2C_ACTION BEFORE UPDATE ON DEVICE.GENERIC_RESPONSE_S2C_ACTION
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.GENERIC_RESPONSE_S2C_ACTION TO USAT_WEB_ROLE;

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 0, 'No Action', 'N');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 5, 'Reboot', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 6, 'Re-initialize', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 7, 'Create and send Settlement message to the Server', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 8, 'Upload Property Value List to the Server', 'N');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 9, 'Stop File Transfer', 'N');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 10, 'Disconnect immediately from the Server', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 11, 'Perform Standard Server Session Process', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 12, 'Perform Standard Server Session Process at end of session', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 13, 'Send Component Identification to the Server', 'Y');

INSERT INTO DEVICE.GENERIC_RESPONSE_S2C_ACTION(device_type_id, protocol_revision, action_cd, action_desc, ui_visible_flag)
VALUES(13, 4001005, 14, 'Process Attached Property Value List', 'N');


ALTER TABLE DEVICE.HOST MODIFY (
  HOST_SERIAL_CD VARCHAR2(255),  
  HOST_PORT_NUM NUMBER(3,0),
  HOST_LABEL_CD VARCHAR2(255)
);

ALTER TABLE DEVICE.HOST_SETTING MODIFY (
  HOST_SETTING_VALUE VARCHAR2(255)
);


INSERT INTO DEVICE.EVENT_TYPE(EVENT_TYPE_ID, EVENT_TYPE_NAME, EVENT_TYPE_DESC)
VALUES(3, 'Scheduled', 'Scheduled settlement');



INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE(EVENT_TYPE_ID, HOST_TYPE_ID, EVENT_TYPE_HOST_TYPE_NUM)
VALUES(1, 201, 66);

INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE(EVENT_TYPE_ID, HOST_TYPE_ID, EVENT_TYPE_HOST_TYPE_NUM)
VALUES(2, 201, 70);

INSERT INTO DEVICE.EVENT_TYPE_HOST_TYPE(EVENT_TYPE_ID, HOST_TYPE_ID, EVENT_TYPE_HOST_TYPE_NUM)
VALUES(3, 201, 83);



CREATE TABLE DEVICE.SETTLEMENT_EVENT
  (
    EVENT_ID                  	NUMBER(20,0)    not null,
    DEVICE_BATCH_ID           	NUMBER(20,0)    not null,
    DEVICE_NEW_BATCH_ID       	NUMBER(20,0)    not null,
    DEVICE_EVENT_UTC_TS	      	DATE		not null,
    DEVICE_EVENT_UTC_OFFSET_MIN	NUMBER(3,0)	not null,        
    CREATED_BY                	VARCHAR2(30)    not null,
    CREATED_UTC_TS              TIMESTAMP       not null,
    LAST_UPDATED_BY           	VARCHAR2(30)    not null,
    LAST_UPDATED_UTC_TS       	TIMESTAMP       not null,
    CONSTRAINT PK_SETTLEMENT_EVENT PRIMARY KEY(EVENT_ID),
    CONSTRAINT FK_SETTLEMENT_EVENT_EVENT_ID foreign key(EVENT_ID) references DEVICE.EVENT(EVENT_ID)
  )
  PARTITION BY RANGE (EVENT_ID)
  (
  PARTITION SETTLEMENT_EVENT_1 VALUES LESS THAN (10000000),
  PARTITION SETTLEMENT_EVENT_2 VALUES LESS THAN (15000000),
  PARTITION SETTLEMENT_EVENT_3 VALUES LESS THAN (20000000)
  )
  TABLESPACE DEVICE_DATA;


CREATE OR REPLACE TRIGGER DEVICE.TRBI_SETTLEMENT_EVENT BEFORE INSERT ON DEVICE.SETTLEMENT_EVENT
  FOR EACH ROW
BEGIN
	SELECT 
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO 
		:NEW.created_utc_ts,
		:NEW.created_by,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_SETTLEMENT_EVENT BEFORE UPDATE ON DEVICE.SETTLEMENT_EVENT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_utc_ts,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO
		:NEW.created_by,
		:NEW.created_utc_ts,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.SETTLEMENT_EVENT TO USAT_WEB_ROLE;



CREATE TABLE PSS.RECEIPT_RESULT
(
    receipt_result_cd         CHAR(1) NOT NULL,    
    receipt_result_desc       VARCHAR2(100) NOT NULL,
    CONSTRAINT PK_RECEIPT_RESULT PRIMARY KEY(RECEIPT_RESULT_CD)
)
TABLESPACE PSS_DATA;

GRANT SELECT, INSERT, UPDATE ON PSS.RECEIPT_RESULT TO USAT_WEB_ROLE;

INSERT INTO PSS.RECEIPT_RESULT(receipt_result_cd, receipt_result_desc) VALUES('P', 'Receipt requested and sent successfully');
INSERT INTO PSS.RECEIPT_RESULT(receipt_result_cd, receipt_result_desc) VALUES('E', 'Receipt requested but error during processing');
INSERT INTO PSS.RECEIPT_RESULT(receipt_result_cd, receipt_result_desc) VALUES('N', 'Receipt not requested');
INSERT INTO PSS.RECEIPT_RESULT(receipt_result_cd, receipt_result_desc) VALUES('D', 'Receipt printing disabled');
INSERT INTO PSS.RECEIPT_RESULT(receipt_result_cd, receipt_result_desc) VALUES('U', 'Feature unavailable');


CREATE TABLE PSS.SALE_TYPE
(
    sale_type_cd         CHAR(1) NOT NULL,    
    sale_type_desc       VARCHAR2(60) NOT NULL,
    CONSTRAINT PK_SALE_TYPE PRIMARY KEY(sale_type_cd)
)
TABLESPACE PSS_DATA;

GRANT SELECT, INSERT, UPDATE ON PSS.SALE_TYPE TO USAT_WEB_ROLE;

INSERT INTO PSS.SALE_TYPE(sale_type_cd, sale_type_desc) VALUES('A', 'Actual Purchase');
INSERT INTO PSS.SALE_TYPE(sale_type_cd, sale_type_desc) VALUES('C', 'Cash');
INSERT INTO PSS.SALE_TYPE(sale_type_cd, sale_type_desc) VALUES('I', 'Intended Purchase');


CREATE TABLE PSS.SALE_RESULT
(
    sale_result_id       NUMBER(3,0) NOT NULL,    
    sale_result_desc     VARCHAR2(100) NOT NULL,
    CONSTRAINT PK_SALE_RESULT PRIMARY KEY(sale_result_id)
)
TABLESPACE PSS_DATA;

GRANT SELECT, INSERT, UPDATE ON PSS.SALE_RESULT TO USAT_WEB_ROLE;

INSERT INTO PSS.SALE_RESULT(sale_result_id, sale_result_desc) VALUES(0, 'Successful');
INSERT INTO PSS.SALE_RESULT(sale_result_id, sale_result_desc) VALUES(1, 'Cancelled by user');
INSERT INTO PSS.SALE_RESULT(sale_result_id, sale_result_desc) VALUES(2, 'Cancelled by authorization timeout');
INSERT INTO PSS.SALE_RESULT(sale_result_id, sale_result_desc) VALUES(3, 'Cancelled by authorization failure');
INSERT INTO PSS.SALE_RESULT(sale_result_id, sale_result_desc) VALUES(4, 'Cancelled because of machine failure');


CREATE TABLE PSS.SALE
  (
    TRAN_ID                  	NUMBER(20,0)    not null,
    DEVICE_BATCH_ID           	NUMBER(20,0)    not null,
    SALE_TYPE_CD		CHAR(1)		not null,
    SALE_START_UTC_TS	      	DATE		not null,
    SALE_END_UTC_TS	      	DATE		not null,
    SALE_UTC_OFFSET_MIN		NUMBER(3,0)	not null,
    SALE_RESULT_ID		NUMBER(3,0)     not null,
    SALE_AMOUNT			NUMBER(14,4)	not null,
    RECEIPT_RESULT_CD		CHAR(1)		not null,
    CREATED_BY                	VARCHAR2(30)    not null,
    CREATED_UTC_TS              TIMESTAMP       not null,
    LAST_UPDATED_BY           	VARCHAR2(30)    not null,
    LAST_UPDATED_UTC_TS       	TIMESTAMP       not null,
	DUPLICATE_COUNT				NUMBER(20,0) DEFAULT 0 not null,
	HASH_TYPE_CD				VARCHAR2(16) not null,
	TRAN_LINE_ITEM_HASH			VARCHAR2(128) not null,
    CONSTRAINT PK_SALE PRIMARY KEY(TRAN_ID),
    CONSTRAINT FK_SALE_TRAN_ID foreign key(TRAN_ID) references PSS.TRAN(TRAN_ID),
    CONSTRAINT FK_SALE_TYPE_CD foreign key(SALE_TYPE_CD) references PSS.SALE_TYPE(SALE_TYPE_CD),
    CONSTRAINT FK_SALE_RESULT_ID foreign key(SALE_RESULT_ID) references PSS.SALE_RESULT(SALE_RESULT_ID),
    CONSTRAINT FK_SALE_RECEIPT_RESULT_CD foreign key(RECEIPT_RESULT_CD) references PSS.RECEIPT_RESULT(RECEIPT_RESULT_CD),
	CONSTRAINT FK_SALE_HASH_TYPE_CD foreign key(HASH_TYPE_CD) references PSS.HASH_TYPE(HASH_TYPE_CD)
  )
  PARTITION BY RANGE (TRAN_ID)
  (
  PARTITION SALE_1 VALUES LESS THAN (10000000),
  PARTITION SALE_2 VALUES LESS THAN (15000000),
  PARTITION SALE_3 VALUES LESS THAN (20000000)
  )
  TABLESPACE PSS_DATA;


CREATE OR REPLACE TRIGGER DEVICE.TRBI_SALE BEFORE INSERT ON PSS.SALE
  FOR EACH ROW
BEGIN
	SELECT 
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO 
		:NEW.created_utc_ts,
		:NEW.created_by,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_SALE BEFORE UPDATE ON PSS.SALE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_utc_ts,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO
		:NEW.created_by,
		:NEW.created_utc_ts,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON PSS.SALE TO USAT_WEB_ROLE;


INSERT INTO DEVICE.EVENT_TYPE(EVENT_TYPE_ID, EVENT_TYPE_NAME, EVENT_TYPE_DESC)
VALUES(4, 'Error', 'Generic error');


INSERT INTO DEVICE.EVENT_DETAIL_TYPE(EVENT_DETAIL_TYPE_ID, EVENT_DETAIL_TYPE_NAME, EVENT_DETAIL_TYPE_DESC)
VALUES(3, 'Event Data', 'Event data reported by host');



GRANT EXECUTE ON DBMS_LOCK TO DBADMIN;



CREATE TABLE DEVICE.CONFIG_TEMPLATE_SETTING
  (
    FILE_TRANSFER_ID            	NUMBER(20,0)    not null,
    DEVICE_SETTING_PARAMETER_CD 	VARCHAR2(60)    not null,
    CONFIG_TEMPLATE_SETTING_VALUE	VARCHAR2(200),
    CREATED_BY                		VARCHAR2(30)    not null,
    CREATED_UTC_TS              	TIMESTAMP       not null,
    LAST_UPDATED_BY           		VARCHAR2(30)    not null,
    LAST_UPDATED_UTC_TS       		TIMESTAMP       not null,
    CONSTRAINT PK_CONFIG_TEMPLATE_SETTING PRIMARY KEY(FILE_TRANSFER_ID, DEVICE_SETTING_PARAMETER_CD),
    CONSTRAINT FK_CFG_TMPL_SET_FT_ID foreign key(FILE_TRANSFER_ID) references DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID),
    CONSTRAINT FK_CFG_TMPL_SET_DEV_SET_PRM_CD foreign key(DEVICE_SETTING_PARAMETER_CD) references DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD)
  )
  TABLESPACE DEVICE_DATA;


CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE_SETTING BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE_SETTING
  FOR EACH ROW
BEGIN
	SELECT 
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO 
		:NEW.created_utc_ts,
		:NEW.created_by,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE_SETTING BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE_SETTING
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_utc_ts,
		SYS_EXTRACT_UTC(SYSTIMESTAMP),
		USER
	INTO
		:NEW.created_by,
		:NEW.created_utc_ts,
		:NEW.last_updated_utc_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE_SETTING TO USAT_WEB_ROLE;


INSERT INTO pss.tran_state VALUES('K', 'INTENDED_ERROR');
INSERT INTO pss.tran_state VALUES('F', 'SALE_NO_AUTH');
INSERT INTO pss.tran_state VALUES('G', 'SALE_NO_AUTH_ERROR');
INSERT INTO pss.tran_state VALUES('H', 'SALE_NO_AUTH_INTENDED');


CREATE GLOBAL TEMPORARY TABLE device.gtmp_file_transfer
(
	file_transfer_id NUMBER(20,0),
	file_transfer_content_hex CLOB
);


GRANT SELECT on engine.device_session to PSS;


ALTER TABLE LOCATION.TIME_ZONE ADD (TIME_ZONE_GUID VARCHAR2(60));

UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Australia/Adelaide' WHERE TIME_ZONE_CD IN ('ACST', 'ACDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Atlantic/Bermuda' WHERE TIME_ZONE_CD IN ('AST', 'ADT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Australia/Canberra' WHERE TIME_ZONE_CD IN ('AEST', 'AEDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Australia/Perth' WHERE TIME_ZONE_CD IN ('AWST');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Canada/Newfoundland' WHERE TIME_ZONE_CD IN ('NST', 'NDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Europe/Amsterdam' WHERE TIME_ZONE_CD IN ('CET', 'CEST');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Europe/Athens' WHERE TIME_ZONE_CD IN ('EET', 'EEST');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Europe/Lisbon' WHERE TIME_ZONE_CD IN ('WET', 'WEST');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Europe/London' WHERE TIME_ZONE_CD IN ('BST', 'GMT', 'IST');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'Europe/Moscow' WHERE TIME_ZONE_CD IN ('MSK');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'US/Alaska' WHERE TIME_ZONE_CD IN ('AKST', 'AKDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'US/Central' WHERE TIME_ZONE_CD IN ('CST', 'CDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'US/Eastern' WHERE TIME_ZONE_CD IN ('EST', 'EDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'US/Hawaii' WHERE TIME_ZONE_CD IN ('HST');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'US/Mountain' WHERE TIME_ZONE_CD IN ('MST', 'MDT');
UPDATE LOCATION.TIME_ZONE SET TIME_ZONE_GUID = 'US/Pacific' WHERE TIME_ZONE_CD IN ('PST', 'PDT');

ALTER TABLE LOCATION.TIME_ZONE MODIFY (TIME_ZONE_GUID VARCHAR2(60) NOT NULL);


ALTER TABLE DEVICE.DEVICE ADD (PREVIOUS_ENCRYPTION_KEY RAW(64));

CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE BEFORE UPDATE ON DEVICE.DEVICE

  FOR EACH ROW
BEGIN
   IF (:OLD.encryption_key IS NULL AND :NEW.encryption_key IS NOT NULL)
      OR (:OLD.encryption_key IS NOT NULL AND :NEW.encryption_key IS NULL)
      OR (:OLD.encryption_key <> :NEW.encryption_key)
   THEN
      :NEW.device_encr_key_gen_ts := SYSDATE;
	  :NEW.previous_encryption_key := utl_raw.cast_to_raw(:OLD.encryption_key);
   END IF;

   SELECT :OLD.created_by, :OLD.created_ts, SYSDATE,
          USER
     INTO :NEW.created_by, :NEW.created_ts, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE FUNCTION DBADMIN.DATE_TO_MILLIS(pd_date DATE)
   RETURN NUMBER DETERMINISTIC PARALLEL_ENABLE
IS
BEGIN
    RETURN (CAST(SYS_EXTRACT_UTC(CAST(pd_date AS TIMESTAMP)) AS DATE) - TO_DATE('01/01/1970', 'MM/DD/YYYY')) * 86400000;
END;
/

CREATE PUBLIC SYNONYM DATE_TO_MILLIS FOR DBADMIN.DATE_TO_MILLIS;

GRANT EXECUTE ON DATE_TO_MILLIS TO PUBLIC;


COMMIT;