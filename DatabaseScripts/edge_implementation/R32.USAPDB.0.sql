ALTER TABLE REPORT.PURCHASE MODIFY DESCRIPTION VARCHAR2(4000);
ALTER TABLE REPORT.PURCHASE MODIFY VEND_COLUMN VARCHAR2(4000);

ALTER TABLE PSS.SALE ADD(VOID_ALLOWED CHAR(1));

ALTER TABLE CORP.SERVICE_FEES ADD(
    FIRST_PAYMENT DATE, 
    GRACE_PERIOD_DATE DATE, 
    TRIGGERING_DATE DATE, 
    CREATED_BY                     VARCHAR2(30),                                                                                                                                                                                 
    CREATED_UTC_TS                 TIMESTAMP(6),                                                                                                                                                                                  
    LAST_UPDATED_BY                VARCHAR2(30),                                                                                                                                                                                 
    LAST_UPDATED_UTC_TS            TIMESTAMP(6));

ALTER TABLE CORP.SERVICE_FEES MODIFY(LAST_PAYMENT DEFAULT NULL);

UPDATE CORP.SERVICE_FEES SET CREATED_UTC_TS = FROM_TZ(CAST(CREATE_DATE AS TIMESTAMP), 'US/Eastern') AT TIME ZONE 'GMT';
COMMIT;

ALTER TABLE CORP.SERVICE_FEES DROP COLUMN CREATE_DATE;

CREATE OR REPLACE TRIGGER CORP.TRBI_SERVICE_FEES BEFORE INSERT ON CORP.SERVICE_FEES
  FOR EACH ROW
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE OR REPLACE TRIGGER CORP.TRBU_SERVICE_FEES BEFORE UPDATE ON CORP.SERVICE_FEES
  FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

ALTER TABLE CORP.PROCESS_FEES ADD(
CREATED_BY                     VARCHAR2(30),                                                                                                                                                                                 
CREATED_UTC_TS                 TIMESTAMP(6),                                                                                                                                                                                  
LAST_UPDATED_BY                VARCHAR2(30),                                                                                                                                                                                 
LAST_UPDATED_UTC_TS            TIMESTAMP(6));

UPDATE CORP.PROCESS_FEES SET CREATED_UTC_TS = FROM_TZ(CAST(CREATE_DATE AS TIMESTAMP), 'US/Eastern') AT TIME ZONE 'GMT';
COMMIT;

ALTER TABLE CORP.PROCESS_FEES DROP COLUMN CREATE_DATE;

CREATE OR REPLACE TRIGGER CORP.TRBI_PROCESS_FEES BEFORE INSERT ON CORP.PROCESS_FEES
  FOR EACH ROW
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE OR REPLACE TRIGGER CORP.TRBU_PROCESS_FEES BEFORE UPDATE ON CORP.PROCESS_FEES
  FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

ALTER TABLE CORP.LEDGER ADD(CREATE_BY_NAME VARCHAR2(30), DELETE_DATE DATE, DELETE_BY VARCHAR2(30));
ALTER TABLE CORP.LEDGER MODIFY(CREATE_BY_NAME DEFAULT USER);

CREATE OR REPLACE TRIGGER CORP.TRBU_LEDGER_DELETED
BEFORE UPDATE
OF DELETED
ON CORP.LEDGER
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
	IF :OLD.DELETED = 'N' AND :NEW.DELETED != 'N' THEN
		:NEW.DELETE_DATE := SYSDATE;
        :NEW.DELETE_BY := USER;
	END IF;
END;
/
