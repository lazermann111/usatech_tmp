GRANT EXECUTE ON RDW_LOADER.PKG_LOAD_DATA TO USAT_APP_LAYER_ROLE;
DROP FUNCTION CORP.CARD_COMPANY;

UPDATE REPORT.EPORT
SET LASTDIALIN_DATE = LEAST(SYSDATE, NULLIF(GREATEST(NVL(LASTDIALIN_DATE, MIN_DATE), NVL(LAST_TRAN_DATE, MIN_DATE), NVL(LAST_FILL_DATE, MIN_DATE)), MIN_DATE))
WHERE LAST_TRAN_DATE > NVL(LASTDIALIN_DATE, MIN_DATE) OR LAST_FILL_DATE > NVL(LASTDIALIN_DATE, MIN_DATE);

UPDATE REPORT.EPORT
SET LAST_FILL_DATE = NULL
WHERE LAST_FILL_DATE > SYSDATE;

COMMIT;
