DECLARE
    CURSOR l_cur IS
    select rule_owner, rule_name, rule_condition  
      from dba_rules
    where rule_owner in('REPL_MGR')
    AND rule_name like 'MACHINE_CMD_PENDING%'
    and rule_condition like '%''MACHINE_CMD_PENDING''%';  
BEGIN
    for l_rec in l_cur loop
        DBMS_STREAMS_ADM.REMOVE_RULE(
            rule_name    => l_rec.rule_name
            streams_type => 'capture',
            streams_name => 'CHANGE_CAPTURE');
    END LOOP;
END;
/

DECLARE
    CURSOR l_cur IS
    select rule_owner, rule_name, rule_condition  
      from dba_rules
    where rule_owner in('REPL_MGR')
    AND rule_name like 'TERMINAL%'
    and rule_condition = '(((:dml.get_object_owner() = ''PSS'' and :dml.get_object_name() = ''TERMINAL'')) ) and (:dml.GET_COMMAND_TYPE() = ''INSERT'' or (:dml.GET_COMMAND_TYPE() = ''UPDATE'' and (
   :dml.get_value(''new'', ''TERMINAL_ENCRYPT_KEY'').AccessVarchar2() != :dml.get_value(''old'', ''TERMINAL_ENCRYPT_KEY'').AccessVarchar2()
or :dml.get_value(''new'', ''TERMINAL_ENCRYPT_KEY2'').AccessVarchar2() != :dml.get_value(''old'', ''TERMINAL_ENCRYPT_KEY2'').AccessVarchar2()
or :dml.get_value(''new'', ''TERMINAL_CD'').AccessVarchar2() != :dml.get_value(''old'', ''TERMINAL_CD'').AccessVarchar2()
or :dml.get_value(''new'', ''MERCHANT_ID'').AccessVarchar2() != :dml.get_value(''old'', ''MERCHANT_ID'').AccessVarchar2()
)))';  
BEGIN
    for l_rec in l_cur loop
    DBMS_RULE_ADM.ALTER_RULE(rule_name => l_rec.rule_owner || '.' || l_rec.rule_name, condition => '(((:dml.get_object_owner() = ''PSS'' and :dml.get_object_name() = ''TERMINAL'')) )');
end loop;
END;
/

ALTER TABLE ENGINE.MACHINE_CMD_PENDING DROP SUPPLEMENTAL LOG GROUP MCP_MACHINE_ID;
