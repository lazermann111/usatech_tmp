WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/GLOBALS_PKG.pbk?rev=1.7
CREATE OR REPLACE PACKAGE BODY CORP.GLOBALS_PKG IS
    PROCEDURE CREATE_LOCK(
        pv_object_type VARCHAR2,
        pv_object_id   VARCHAR2,
        pt_last_lock_utc_ts TIMESTAMP)
    AS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO DBADMIN.APP_LOCK(
            APP_LOCK_TYPE,
            APP_LOCK_INSTANCE,
            LAST_LOCK_UTC_TS,
            LAST_LOCK_BY
        ) VALUES(
            pv_object_type,
            pv_object_id,
            pt_last_lock_utc_ts,
            USER
        ); 
        COMMIT;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            NULL;
    END;
   
   FUNCTION REQUEST_LOCK(
        l_object_type VARCHAR2,
        l_object_id   NUMBER)
     RETURN VARCHAR2
    IS
        lt_last_lock_utc_ts TIMESTAMP(6);
        lv_object_id DBADMIN.APP_LOCK.APP_LOCK_INSTANCE%TYPE := TO_CHAR(l_object_id);
    BEGIN
        lt_last_lock_utc_ts := SYS_EXTRACT_UTC(SYSTIMESTAMP);
        CREATE_LOCK(l_object_type, lv_object_id, lt_last_lock_utc_ts);
        UPDATE DBADMIN.APP_LOCK 
           SET LAST_LOCK_UTC_TS = lt_last_lock_utc_ts,
               LAST_LOCK_BY = USER
         WHERE APP_LOCK_TYPE = l_object_type
           AND APP_LOCK_INSTANCE = lv_object_id;
        IF SQL%ROWCOUNT < 1 THEN
            RAISE_APPLICATION_ERROR(-20111, 'AppLock Row for object ' || l_object_type || '.' || l_object_id || ' was deleted while using it');
        END IF;
        RETURN TO_CHAR(lt_last_lock_utc_ts);
    END;
    
    PROCEDURE RELEASE_LOCK(
        l_handle VARCHAR2)
    IS
    BEGIN
        -- this procedure is provided for backwards compatibility only
        NULL;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_trunc_to IN VARCHAR,
        l_months IN NUMBER,
        l_days IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_date_list REPORT.DATE_LIST := REPORT.DATE_LIST();
        l_date DATE;
        l_real_end_date DATE;
    BEGIN
        IF (l_days <= 0 AND l_months <= 0) OR l_days + l_months * 30 <= 0 THEN
            RAISE_APPLICATION_ERROR(-20900, 'Invalid days and months specified (Days = '
                || TO_CHAR(l_days) || ', Months = ' || TO_CHAR(l_months)
                || '); would result in endless loop');
        ELSE
            SELECT ADD_MONTHS(TRUNC(l_start_date, l_trunc_to) + (l_days * (1-l_num_before)), l_months * (1-l_num_before)),
                   ADD_MONTHS(l_end_date + (l_days * l_num_after), l_months * l_num_after)
              INTO l_date, l_real_end_date
              FROM DUAL;
            WHILE l_date < l_real_end_date LOOP
                l_date_list.EXTEND;
                l_date_list(l_date_list.LAST) := l_date;
                SELECT ADD_MONTHS(l_date + l_days, l_months)
                  INTO l_date
                  FROM DUAL;
            END LOOP;
        END IF;
        RETURN l_date_list ;
    END;
    
    FUNCTION GET_DATE_LIST
      ( l_start_date IN DATE,
        l_end_date IN DATE,
        l_frequency_id IN NUMBER,
        l_num_before IN NUMBER DEFAULT 1,
        l_num_after IN NUMBER DEFAULT 0)
      RETURN REPORT.DATE_LIST IS
    --
    -- Creates a DATE_LIST and returns it for the given parameters
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  -------------------------------------------
    -- B KRUG       09-19-04 NEW
        l_trunc_to FREQUENCY.INTERVAL%TYPE;
        l_months FREQUENCY.MONTHS%TYPE;
        l_days FREQUENCY.DAYS%TYPE;
    BEGIN
        SELECT INTERVAL, MONTHS, DAYS
          INTO l_trunc_to, l_months, l_days
          FROM FREQUENCY
         WHERE FREQUENCY_ID = l_frequency_id;
         RETURN GET_DATE_LIST(l_start_date,l_end_date,l_trunc_to,l_months,l_days,l_num_before,l_num_after);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20901, 'Invalid frequency id (' || TO_CHAR(l_frequency_id) || ')');
        WHEN OTHERS THEN
            RAISE;
    END;
    
    FUNCTION LIST_TO_STRING(
        l_list IN REPORT.STRING_LIST,
        l_sep  IN VARCHAR)
     RETURN VARCHAR IS
        l_str VARCHAR2(4000) := '';
    BEGIN
         IF l_list.FIRST IS NOT NULL THEN
            FOR i IN l_list.FIRST..l_list.LAST LOOP
                IF l_list(i) IS NOT NULL THEN
                   IF LENGTH(l_str) > 0 THEN
                      l_str := l_str || l_sep;
                   END IF;
                   IF LENGTH(l_str) + LENGTH(l_list(i)) + LENGTH(l_sep) > 3997 THEN
                      l_str := l_str || '...';
                      EXIT;
                   ELSE
                      l_str := l_str || l_list(i);
                   END IF;
                END IF;
            END LOOP;
        END IF;
        RETURN l_str;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.pbk?rev=1.67
CREATE OR REPLACE PACKAGE BODY CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)
-- B Krug       01-31-08  Added Batch Confirmation Logic

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
     )
     RETURN DOC.DOC_ID%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_batch_ref DOC.REF_NBR%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- lock this bank id until commit to ensure the the doc stays open until the ledger entry is set with it
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_cust_bank_id);
        SELECT MAX(D.DOC_ID) -- just to be safe in case there is more than one
          INTO l_doc_id
          FROM DOC D
         WHERE D.CUSTOMER_BANK_ID = l_cust_bank_id
           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
           AND NVL(D.BUSINESS_UNIT_ID, 0) = NVL(l_business_unit, 0)
           AND D.STATUS = 'O';
        IF l_doc_id IS NULL THEN
            -- create new doc record
            SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
              INTO l_doc_id, l_batch_ref
              FROM DUAL;
            INSERT INTO DOC(
                DOC_ID,
                DOC_TYPE,
                REF_NBR,
                DESCRIPTION,
                CUSTOMER_BANK_ID,
                CURRENCY_ID,
                BUSINESS_UNIT_ID,
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                STATUS)
              SELECT
                l_doc_id,
                '--',
                l_batch_ref,
                NVL(EFT_PREFIX, 'USAT: ') || l_batch_ref,
                CUSTOMER_BANK_ID,
                DECODE(l_currency_id, 0, NULL, l_currency_id),
                DECODE(l_business_unit, 0, NULL, l_business_unit),
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                'O'
              FROM CUSTOMER_BANK
              WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        RETURN l_doc_id;
    END;

    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_doc_id DOC.DOC_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
        l_start_fill_id BATCH_FILL.START_FILL_ID%TYPE;
        l_end_fill_id BATCH_FILL.END_FILL_ID%TYPE;
        l_counters_must_show CHAR(1);
        l_lock VARCHAR2(128);
    BEGIN
        -- calculate batch (and create if necessary)
        -- Get an "Open" doc record
        l_doc_id := GET_OR_CREATE_DOC(l_cust_bank_id, l_currency_id, l_business_unit);
        --l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id); -- this may not be necessary since we lock the doc on customer_bank_id
        SELECT MAX(B.BATCH_ID) -- just to be safe in case there is more than one
          INTO l_batch_id
          FROM BATCH B
         WHERE B.DOC_ID = l_doc_id
           AND B.TERMINAL_ID = l_terminal_id
           AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
           AND B.BATCH_STATE_CD IN('O', 'L') -- Open or Closeable only
           -- for "As Accumulated", batch start date does not matter
           AND (B.START_DATE <= l_entry_date OR l_pay_sched IN(1,5,8))
           AND (NVL(B.END_DATE, MAX_DATE) > l_entry_date OR (B.END_DATE = l_entry_date AND l_pay_sched IN(8)))
           AND (l_pay_sched NOT IN(8) OR DECODE(B.END_DATE, NULL, 0, 1) = DECODE(l_always_as_accum, 'E', 1, 0));
        IF l_batch_id IS NULL THEN
            -- calc start and end dates
            IF l_pay_sched IN(1, 5) THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'L'; -- Closeable
                -- end date is null
            ELSIF l_pay_sched = 2 THEN
                SELECT DECODE(MAX(BCOT.BATCH_CONFIRM_OPTION_TYPE_ID), NULL, 'N', 'Y')
                  INTO l_counters_must_show
                  FROM REPORT.TERMINAL T
                  JOIN CORP.BATCH_CONFIRM BC ON T.CUSTOMER_ID = BC.CUSTOMER_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION BCO
                    ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
                 WHERE BC.PAYMENT_SCHEDULE_ID = l_pay_sched 
                   AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'                  
                   AND T.TERMINAL_ID = l_terminal_id;
                       
                SELECT NVL(MAX(FILL_DATE), MIN_DATE), MAX(FILL_ID)
                  INTO l_start_date, l_start_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE <= l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE DESC)
                 WHERE ROWNUM = 1 ;
                 SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN (SELECT CONNECT_BY_ROOT FILL_ID START_FILL_ID, FILL_ID END_FILL_ID, LEVEL - 1 DEPTH
                              FROM REPORT.FILL
                              START WITH FILL_ID = l_start_fill_id
                              CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID
                              ) H ON F.FILL_ID = H.END_FILL_ID
                     JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE > l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
                
                l_batch_state_cd := 'O'; -- Open
            ELSIF l_pay_sched = 8 THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'O'; -- Open
            ELSE
                SELECT TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + (OFFSET_HOURS / 24),
                       ADD_MONTHS(TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + DAYS, MONTHS) + (OFFSET_HOURS / 24)
                  INTO l_start_date, l_end_date
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                IF l_end_date <= l_entry_date THEN -- trouble, should not happen
                    l_end_date := l_entry_date + (1.0/(24*60*60));
                END IF;
                l_batch_state_cd := 'O'; -- Open
            END IF;

            -- create new batch record
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                l_terminal_id,
                l_pay_sched,
                l_start_date,
                l_end_date,
                l_batch_state_cd);
            IF l_pay_sched = 2 THEN
                INSERT INTO CORP.BATCH_FILL(
                    BATCH_ID,
                    START_FILL_ID,
                    END_FILL_ID)
                  VALUES(
                    l_batch_id,
                    l_start_fill_id,
                    l_end_fill_id);
            END IF;
        END IF;
        RETURN l_batch_id;
    END;
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        SELECT DECODE(l_always_as_accum,
                    'Y', 1,
                    'A', 5,
                    PAYMENT_SCHEDULE_ID),
               BUSINESS_UNIT_ID
          INTO l_pay_sched, l_business_unit
          FROM TERMINAL
         WHERE TERMINAL_ID = l_terminal_id;
        RETURN GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id,l_entry_date,l_currency_id,l_pay_sched, l_business_unit, l_always_as_accum);
    END;
    
    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_id BATCH.TERMINAL_ID%TYPE;
    BEGIN

        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.TERMINAL_ID
          INTO l_cb_id, l_curr_id, l_term_id
          FROM BATCH B, DOC D
          WHERE D.DOC_ID = B.DOC_ID
            AND B.BATCH_ID = l_batch_id;
        RETURN GET_OR_CREATE_BATCH(l_term_id, l_cb_id, l_entry_date, l_curr_id, 'Y');
    END;
    
    FUNCTION GET_DOC_STATUS(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT STATUS INTO l_status FROM CORP.DOC WHERE DOC_ID = l_doc_id;
        RETURN l_status;
    END;

    FUNCTION FREEZE_DOC_STATUS_LEDGER_ID(
        l_ledger_id LEDGER.LEDGER_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.DOC_ID, D.CUSTOMER_BANK_ID
          INTO l_doc_id, l_customer_bank_id
          FROM CORP.DOC D
         INNER JOIN CORP.BATCH B ON D.DOC_ID = B.DOC_ID
         INNER JOIN CORP.LEDGER L ON L.BATCH_ID = B.BATCH_ID
         WHERE L.LEDGER_ID = l_ledger_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    FUNCTION FREEZE_DOC_STATUS_DOC_ID(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID
          INTO l_customer_bank_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
    IS
        l_cnt NUMBER;
    BEGIN
        IF l_batch_state_cd IN('L', 'F') THEN
            RETURN 'Y';
        ELSIF l_batch_state_cd IN('C', 'D') THEN
            IF l_pay_sched IN(2) THEN -- Fill To Fill
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM LEDGER l
                 WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
                   AND L.DELETED = 'N'
                   AND L.BATCH_ID = l_batch_id;
                IF l_cnt = 0 THEN
                    RETURN 'Y';
                ELSE
                    RETURN 'N';
                END IF;
            ELSE
                RETURN 'Y';
            END IF;
        ELSIF l_batch_state_cd IN('O') AND l_pay_sched NOT IN(1,2,5,7,8) AND l_end_date <= SYSDATE THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    END;

    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE, BATCH_STATE_CD
          INTO l_pay_sched, l_end_date, l_batch_state_cd
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        RETURN BATCH_CLOSABLE(l_batch_id, l_pay_sched, l_end_date, l_batch_state_cd);
    END;

    PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_check_cur IS
           SELECT L.TRANS_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
             GROUP BY L.TRANS_ID
             HAVING SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)) > 0
                AND SUM(DECODE(D.STATUS, 'O', 1, 0)) = 0;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        FOR l_check_rec IN l_check_cur LOOP
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_check_rec.TRANS_ID
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            IF l_batch_id IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20701, 'PROCESS FEE (process_fee_id='||TO_CHAR(l_process_fee_id)||' is used in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
            END IF;
        END LOOP;
    END;

    -- Checks if the specified transaction is in a closed batch and if so raises an
    -- exception
    PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
    IS
        l_closed_cnt PLS_INTEGER;
        l_open_cnt PLS_INTEGER;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- get the batch & status
        SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)),
               SUM(DECODE(D.STATUS, 'O', 1, 0))
          INTO l_closed_cnt, l_open_cnt
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID;
        IF l_open_cnt = 0 AND l_closed_cnt > 0 THEN
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
        END IF;
    END;
    
    PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_batch_amt NUMBER;
        l_batch_tran_cnt NUMBER;
        l_src_credit_amt NUMBER;
        l_src_credit_cnt NUMBER;
        l_prev_fill_id REPORT.FILL.FILL_ID%TYPE;
        l_start_fill_id REPORT.FILL.FILL_ID%TYPE;
        l_first_tran_ts DATE;
        l_last_tran_ts DATE;
    BEGIN
        SELECT SUM(l.AMOUNT), MIN(l.ENTRY_DATE)
          INTO l_batch_amt, l_first_tran_ts
     	  FROM CORP.LEDGER l
         WHERE l.ENTRY_TYPE = 'CC'
           AND l.DELETED = 'N'
           AND l.BATCH_ID = l_batch_id;
                   
        SELECT SUM(p.AMOUNT)
          INTO l_batch_tran_cnt
          FROM REPORT.PURCHASE P
         WHERE P.TRAN_ID IN(
            SELECT l.TRANS_ID 
              FROM CORP.LEDGER l
             WHERE l.ENTRY_TYPE = 'CC'
               AND l.DELETED = 'N'
               AND l.BATCH_ID = l_batch_id);
        
        BEGIN
            SELECT SUM(HF.CREDIT_AMOUNT), SUM(HF.CREDIT_COUNT)
              INTO l_src_credit_amt, l_src_credit_cnt
              FROM CORP.BATCH_FILL BF
              JOIN (SELECT CONNECT_BY_ROOT FILL_ID ANCESTOR_FILL_ID, F.*
                      FROM REPORT.FILL F
                     START WITH F.FILL_ID = (SELECT START_FILL_ID FROM CORP.BATCH_FILL WHERE BATCH_ID = l_batch_id)
                   CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID) HF ON BF.START_FILL_ID = HF.ANCESTOR_FILL_ID AND BF.END_FILL_ID = HF.FILL_ID
             WHERE BF.BATCH_ID = l_batch_id 
            HAVING COUNT(*) = COUNT(HF.CREDIT_AMOUNT) 
               AND COUNT(*) =  COUNT(HF.CREDIT_COUNT);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                DECLARE
                    CURSOR l_cur IS
                        SELECT e.EPORT_SERIAL_NUM DEVICE_SERIAL_CD, 
                               GREATEST(l_first_tran_ts, NVL(te.START_DATE, MIN_DATE)) START_DATE,
                               LEAST(B.END_DATE,NVL(te.END_DATE, MAX_DATE)) END_DATE 
                          FROM CORP.BATCH B
                          JOIN REPORT.TERMINAL_EPORT te ON b.TERMINAL_ID = te.TERMINAL_ID
            	          JOIN REPORT.EPORT E ON te.EPORT_ID = e.EPORT_ID
        		         WHERE B.BATCH_ID = l_batch_id;
        		BEGIN
                    l_src_credit_amt := 0;
                    l_src_credit_cnt := 0;
                    FOR l_rec IN l_cur LOOP
                        SELECT l_src_credit_amt + NVL(SUM(T.TRAN_LINE_ITEM_AMOUNT * T.TRAN_LINE_ITEM_QUANTITY), 0), 
                               l_src_credit_cnt + NVL(SUM(DECODE(T.TRAN_LINE_ITEM_TYPE_GROUP_CD, 'U', NULL, 'I', NULL, T.TRAN_LINE_ITEM_QUANTITY)), 0)
                	      INTO l_src_credit_amt, l_src_credit_cnt                         
                          FROM PSS.VW_REPORTING_TRAN_LINE_ITEM@USADBP_PSS T
                          WHERE T.DEVICE_SERIAL_CD = l_rec.DEVICE_SERIAL_CD
                            AND T.CLIENT_PAYMENT_TYPE_CD IN('C', 'R')
                            AND T.TRAN_START_TS BETWEEN l_rec.START_DATE AND l_rec.END_DATE;
                    END LOOP; 
                END;       
            WHEN OTHERS THEN
                RAISE;
        END;

        IF l_batch_amt = l_src_credit_amt AND l_batch_tran_cnt = l_src_credit_cnt THEN
            UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
        END IF;	
    END;
    
    PROCEDURE ADD_BATCH_ROUNDING_ENTRY(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = l_batch_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;

    PROCEDURE ADD_BATCH_ROUNDING_ENTRIES(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM BATCH
             WHERE DOC_ID = l_doc_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            ADD_BATCH_ROUNDING_ENTRY(l_rec.BATCH_ID);
        END LOOP;
    END;

    PROCEDURE ADD_ROUNDING_ENTRY(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L, BATCH B
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_doc_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            SELECT MAX(BATCH_ID)
              INTO l_batch_id
              FROM BATCH
             WHERE DOC_ID = l_doc_id
               AND TERMINAL_ID IS NULL;
            IF l_batch_id IS NULL THEN
                -- create a batch and set the doc id
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, DOC_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE, BATCH_STATE_CD)
                    SELECT l_batch_id, l_doc_id, NULL, 1, SYSDATE, SYSDATE, 'F'
                      FROM DOC
                     WHERE DOC_ID = l_doc_id;
            END IF;

            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;
    /*
     * This procedure references REPORT.TRAN
    */
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
        CURSOR l_cur IS
            SELECT D.DOC_ID, B.BATCH_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS = 'O';
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_batch_ids NUMBER_TABLE := NUMBER_TABLE();
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_recs(i).CUSTOMER_BANK_ID);
                IF GET_DOC_STATUS(l_recs(i).DOC_ID) = 'O' THEN
                   l_batch_ids.EXTEND;
                   l_batch_ids(l_batch_ids.LAST) := l_recs(i).BATCH_ID;
                ELSE
                    EXIT;
                END IF;
            END LOOP;
        END IF;
        IF l_recs.FIRST IS NOT NULL AND l_recs.LAST = l_batch_ids.LAST THEN -- we can do direct update Yippee!
            UPDATE LEDGER SET
                SETTLE_STATE_ID = l_settle_state_id,
                LEDGER_DATE = l_settle_date
             WHERE TRANS_ID = l_trans_id
               AND BATCH_ID MEMBER OF l_batch_ids;
        ELSE
           DECLARE
                l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                l_close_date LEDGER.LEDGER_DATE%TYPE;
                l_total_amount LEDGER.AMOUNT%TYPE;
                l_terminal_id BATCH.TERMINAL_ID%TYPE;
                l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_process_fee_id LEDGER.PROCESS_FEE_ID%TYPE;
                l_currency_id DOC.CURRENCY_ID%TYPE;
            BEGIN
                SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                       CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
                  INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                       l_customer_bank_id, l_process_fee_id, l_currency_id
                  FROM TRANS
                 WHERE TRAN_ID = l_trans_id;

                UPDATE_LEDGER(l_trans_id, l_trans_type_id, l_close_date,
                    l_settle_date, l_total_amount, l_settle_state_id,
                    l_terminal_id, l_customer_bank_id,l_process_fee_id, l_currency_id);
            END;
        END IF;
    END;

    PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_tran_cur IS
           SELECT DISTINCT L.TRANS_ID, X.TRANS_TYPE_ID, X.CLOSE_DATE, X.TOTAL_AMOUNT,
                  X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.SETTLE_STATE_ID,
                  X.SETTLE_DATE, X.CURRENCY_ID
              FROM LEDGER L, REPORT.TRANS X
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE = 'PF'
               AND L.TRANS_ID = X.TRAN_ID;
    BEGIN
        FOR l_tran_rec IN l_tran_cur LOOP
            UPDATE_LEDGER(
                l_tran_rec.TRANS_ID,
                l_tran_rec.trans_type_id,
                l_tran_rec.close_date,
                l_tran_rec.settle_date,
                l_tran_rec.total_amount,
                l_tran_rec.settle_state_id,
                l_tran_rec.terminal_id,
                l_tran_rec.customer_bank_id,
                l_process_fee_id,
                l_tran_rec.currency_id);
        END LOOP;
    END;

    -- Puts CC, RF, or CB record into Ledger and also any required PF record
    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_batch_id LEDGER.BATCH_ID%TYPE)
    IS
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_real_amt LEDGER.AMOUNT%TYPE;
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        IF NVL(l_amount, 0) = 0 THEN
            RETURN;
        END IF;
        IF l_trans_type_id IN(16,19,20,21) THEN
            SELECT LEDGER_SEQ.NEXTVAL,
                   DECODE(l_trans_type_id, 20, -ABS(l_amount), 21, -ABS(l_amount), l_amount)
              INTO l_ledger_id,
                   l_real_amt
              FROM DUAL;
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                l_ledger_id,
                DECODE(l_trans_type_id, 16, 'CC', 19, 'CC', 20, 'RF', 21, 'CB'),--, 'AU'), -- Audit trail of other transactions
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date
              FROM DUAL;
            -- create net revenue fee on transaction, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        IF l_process_fee_id IS NOT NULL THEN
            BEGIN
                SELECT LEDGER_SEQ.NEXTVAL,
                       -GREATEST((ABS(l_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT), pf.MIN_AMOUNT)
                  INTO l_ledger_id,
                       l_real_amt
                  FROM PROCESS_FEES pf
                WHERE pf.PROCESS_FEE_ID = l_process_fee_id
                  AND (pf.FEE_PERCENT > 0 OR pf.FEE_AMOUNT > 0);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RETURN;
              WHEN OTHERS THEN
                RAISE;
            END;
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES(
                l_ledger_id,
                'PF',
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date);
            -- create net revenue fee on process fee, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        -- Now check batch complete
        IF l_trans_type_id IN(16,19) THEN
            SELECT PAYMENT_SCHEDULE_ID, END_DATE
              INTO l_pay_sched, l_end_date
              FROM CORP.BATCH B
             WHERE B.BATCH_ID = l_batch_id;
            IF l_pay_sched IN(2) AND l_end_date IS NOT NULL THEN
               CHECK_FILL_BATCH_COMPLETE(l_batch_id);
            END IF;
        END IF;
    END;

    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_doc_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- if refund or chargeback get the batch that the
        -- original trans is in, if open use it else use "as accumulated"
        IF l_trans_type_id IN(20, 21) THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
            l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id);
            BEGIN
                SELECT BATCH_ID, STATUS
                  INTO l_batch_id, l_doc_status
                  FROM (SELECT B.BATCH_ID, D.STATUS
                          FROM LEDGER L, BATCH B, TRANS T, DOC D
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_id
                           AND L.BATCH_ID = B.BATCH_ID
                           AND L.DELETED = 'N'
                           AND B.DOC_ID = D.DOC_ID
                           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                           AND L.ENTRY_TYPE = 'CC'
                           AND B.PAYMENT_SCHEDULE_ID NOT IN(8) -- As Exported requires each trans to be put in "open" batch
                         ORDER BY DECODE(D.STATUS, 'O', 0, 1), B.BATCH_ID DESC)
                 WHERE ROWNUM = 1;
                IF NVL(l_doc_status, ' ') <> 'O' THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                -- The following should be removed and the error re-enabled,
                -- once we enforce orig_tran_id for all refunds/ chargebacks
                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                --RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                WHEN OTHERS THEN
                    RAISE;
            END;
        ELSE
            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'N');
        END IF;
        INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
            l_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
    END;
    
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_closed_cnt PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_prev_cb_ids NUMBER_TABLE;
    BEGIN
        IF l_trans_type_id IN(22) THEN -- cash; ignore
            RETURN;
        END IF;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        -- get the batch & status
        SELECT MAX(B.BATCH_ID), SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)), -- are there ANY closed batches on this transaction?
               CAST(COLLECT(CAST(D.CUSTOMER_BANK_ID AS NUMBER)) AS NUMBER_TABLE)
          INTO l_batch_id, l_closed_cnt, l_prev_cb_ids
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND L.DELETED = 'N'
           AND B.DOC_ID = D.DOC_ID;
        IF l_batch_id IS NULL THEN -- Create new batch for this ledger record
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
                RETURN; -- all done
            END IF;
        ELSIF l_closed_cnt = 0 THEN -- lock previous customer_bank_id's to ensure that none of the doc's closes
            FOR i IN l_prev_cb_ids.FIRST..l_prev_cb_ids.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_prev_cb_ids(i));
            END LOOP;
            -- double-check closed count
            SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1))
              INTO l_closed_cnt
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID;
        END IF;
        IF l_closed_cnt > 0 THEN -- add adjustment for prev paid
            DECLARE
                CURSOR l_prev_cur IS
                    SELECT COUNT(*) PAID_CNT,
                           SUM(DECODE(ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE), 'Y', AMOUNT, NULL)) PAID_AMT, B.TERMINAL_ID,
                           D.CUSTOMER_BANK_ID, D.CURRENCY_ID
                      FROM LEDGER L, BATCH B, DOC D
                     WHERE L.TRANS_ID = l_trans_id
                       AND L.BATCH_ID = B.BATCH_ID
                       AND B.DOC_ID = D.DOC_ID
                       AND L.DELETED = 'N'
                       AND L.RELATED_LEDGER_ID IS NULL -- Must exclude Net Revenue Fees
                     GROUP BY B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID;

                l_adj_amt LEDGER.AMOUNT%TYPE;
                l_adjusted_new_cb BOOLEAN := FALSE;
                l_old_pf_amt LEDGER.AMOUNT%TYPE;
                l_old_amt LEDGER.AMOUNT%TYPE;
                l_desc LEDGER.DESCRIPTION%TYPE;
                l_new_payable CHAR(1);
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
            BEGIN
                --figure out what change has occurred
                FOR l_prev_rec IN l_prev_cur LOOP
                    IF NVL(l_customer_bank_id, 0) = l_prev_rec.CUSTOMER_BANK_ID
                       AND NVL(l_currency_id, 0) = NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                        IF NVL(l_prev_rec.PAID_CNT, 0) <> 0 AND NVL(l_prev_rec.PAID_AMT, 0) <> 0 THEN
                            --calc difference and add as adjustment
                            SELECT ENTRY_PAYABLE(l_settle_state_id, DECODE(l_trans_type_id, 16, 'CC', 19, 'CC', 20, 'RF', 21, 'CB'))
                              INTO l_new_payable
                              FROM DUAL;
                            SELECT (CASE WHEN l_trans_type_id IN(16,19,20,21) THEN l_total_amount ELSE 0 END
                                   - NVL(SUM(GREATEST(ABS(l_total_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT, pf.MIN_AMOUNT)), 0))
                                   * DECODE(l_new_payable, 'Y', 1, 0)
                                   - l_prev_rec.PAID_AMT
                              INTO l_adj_amt
                              FROM PROCESS_FEES pf
                             WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                            IF l_adj_amt <> 0 THEN
                                IF l_new_payable = 'Y' THEN
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('CC')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_pf_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('PF')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    IF NVL(l_old_amt, 0) = 0 AND l_trans_type_id IN(16,19,20,21) THEN
                                        l_desc := 'Correction for a previously ignored transaction';
                                    ELSIF l_old_amt <> l_total_amount AND l_trans_type_id IN(16,19,20,21) THEN
                                        l_desc := 'Correction for a value amount change on a transaction that has already been paid';
                                    ELSIF NVL(l_old_pf_amt, 0) = 0 THEN
                                        l_desc := 'Correction for a missing processing fee on a transaction that has already been paid';
                                    ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                        l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                    ELSE
                                        l_desc := 'Correction for a change in processing fee on a transaction that has already been paid';
                                    END IF;
                                ELSE -- should not be paid now
                                    l_desc := 'Correction for a transaction that has already been paid but has since failed settlement';
                                END IF;
                                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                                -- create a negative adjustment based on this previous payment
                                SELECT LEDGER_SEQ.NEXTVAL
                                  INTO l_ledger_id
                                  FROM DUAL;
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                VALUES(
                                    l_ledger_id,
                                    'AD',
                                    l_trans_id,
                                    NULL,
                                    l_adj_amt,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    l_desc);
                                -- create net revenue adjustment, if any
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    RELATED_LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    SERVICE_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                SELECT
                                    LEDGER_SEQ.NEXTVAL,
                                    l_ledger_id,
                                    'SF',
                                    l_trans_id,
                                    NULL,
                                    SF.SERVICE_FEE_ID,
                                    -l_adj_amt * SF.FEE_PERCENT,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    F.FEE_NAME
                                FROM SERVICE_FEES SF, FEES F
                               WHERE SF.FEE_ID = F.FEE_ID
                                 AND SF.TERMINAL_ID = l_terminal_id
                                 AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                      AND NVL(SF.END_DATE, MAX_DATE)
                                 AND SF.FREQUENCY_ID = 6;
                            END IF;
                            l_adjusted_new_cb := TRUE; -- whether we adjust or not the new cb of the tran is appropriately dealt with
                        END IF;
                    --case: bank acct changed
                    ELSIF l_prev_rec.PAID_AMT <> 0 THEN
                        IF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                            l_desc := 'Correction for a transaction that was assigned to the wrong location';
                        ELSIF NVL(l_currency_id, 0) <> NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                            l_desc := 'Correction for a change in currency';
                        ELSE
                            l_desc := 'Correction for a transaction that was paid to the wrong bank account';
                        END IF;
                        -- deduct entire amt from old
                        l_batch_id := GET_OR_CREATE_BATCH(
                                l_prev_rec.TERMINAL_ID,
                                l_prev_rec.CUSTOMER_BANK_ID,
                                l_close_date,
                                l_prev_rec.CURRENCY_ID,
                                'A');
                        SELECT LEDGER_SEQ.NEXTVAL
                          INTO l_ledger_id
                          FROM DUAL;
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES(
                            l_ledger_id,
                            'AD',
                            l_trans_id,
                            NULL,
                            -l_prev_rec.PAID_AMT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            l_desc);
                        -- create net revenue adjustment, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_ledger_id,
                            'SF',
                            l_trans_id,
                            NULL,
                            SF.SERVICE_FEE_ID,
                            l_prev_rec.PAID_AMT * SF.FEE_PERCENT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                         AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END IF;
                END LOOP;
                IF NOT l_adjusted_new_cb AND l_customer_bank_id IS NOT NULL THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                    INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                        l_close_date, l_settle_date, l_total_amount,
                        l_settle_state_id, l_process_fee_id, l_batch_id);
                END IF;
            END;
        ELSE -- an open batch exists
            -- To make it easy let's just delete what's there and re-insert
            --DELETE FROM LEDGER WHERE TRANS_ID = l_trans_id;
            UPDATE LEDGER SET DELETED = 'Y' WHERE TRANS_ID = l_trans_id;
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
            END IF;
        END IF;
    END;
    
    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID,
                   B.START_DATE, CASE WHEN BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL THEN  'N' ELSE 'Y' END COUNTER_MUST_SHOW
              FROM CORP.BATCH B
              JOIN REPORT.TERMINAL_EPORT TE ON B.TERMINAL_ID = TE.TERMINAL_ID
              JOIN REPORT.FILL F ON TE.EPORT_ID = F.EPORT_ID
               AND F.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
              JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
              JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
              LEFT OUTER JOIN (CORP.BATCH_CONFIRM BC
              JOIN CORP.BATCH_CONFIRM_OPTION BCO
                ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
              JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
               AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'
              ) ON BC.CUSTOMER_ID = T.CUSTOMER_ID
               AND BC.PAYMENT_SCHEDULE_ID = B.PAYMENT_SCHEDULE_ID
             WHERE B.START_DATE < F.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > F.FILL_DATE
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL)
               AND F.FILL_ID = l_fill_id;
        l_end_date CORP.BATCH.END_DATE%TYPE;
        l_end_fill_id CORP.BATCH_FILL.END_FILL_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f, REPORT.TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_batch_rec.START_DATE
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_batch_rec.TERMINAL_ID
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_batch_rec.COUNTER_MUST_SHOW = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE CORP.BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
            UPDATE CORP.BATCH_FILL BF SET BF.END_FILL_ID = l_end_fill_id
             WHERE BF.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are after fill_date
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND L.ENTRY_DATE >= l_end_date;
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
            GLOBALS_PKG.RELEASE_LOCK(l_lock);
            COMMIT;
            
            -- Now check batch complete
            CHECK_FILL_BATCH_COMPLETE(l_batch_rec.BATCH_ID);
            COMMIT;
         END LOOP;
    END;
    
    PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT DISTINCT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.START_DATE, EX.TRAN_CREATE_DT_END NEW_END_DATE
              FROM CORP.BATCH B
             INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
             INNER JOIN CORP.CUSTOMER_BANK CB ON D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
             /*
             INNER JOIN REPORT.TERMINAL TERMINAL_ID ON B.TERMINAL_ID = T.TERMINAL_ID
             INNER JOIN FRONT.VW_LOCATION_HIERARCHY H ON T.LOCATION_ID = H.DESCENDANT_LOCATION_ID
             */
             INNER JOIN REPORT.EXPORT_BATCH EX ON CB.CUSTOMER_ID = EX.CUSTOMER_ID /* AND EX.LOCATION_ID = H.ANCESTOR_LOCATION_ID */
             WHERE B.BATCH_STATE_CD = 'O'
               AND B.END_DATE IS NULL
               AND B.PAYMENT_SCHEDULE_ID = 8
               AND EX.BATCH_ID = l_export_id;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE BATCH B
               SET B.END_DATE = l_batch_rec.new_end_date,
                   B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are not exported
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND (L.ENTRY_DATE > l_batch_rec.new_end_date
                         OR NOT EXISTS(SELECT 1 FROM REPORT.ACTIVITY_REF A
                              INNER JOIN REPORT.EXPORT_BATCH EB ON A.BATCH_ID = EB.BATCH_ID
                              WHERE L.TRANS_ID = A.TRAN_ID AND EB.STATUS IN('A','S')));
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
         END LOOP;
    END;
/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS.
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/

    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
                SELECT CUSTOMER_BANK_ID,
                       SUM(DECODE(ENTRY_TYPE, 'CC', AMOUNT, NULL)) CREDIT_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'RF', AMOUNT, NULL)) REFUND_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'CB', AMOUNT, NULL)) CHARGEBACK_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'PF', AMOUNT, NULL)) PROCESS_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'SF', AMOUNT, NULL)) SERVICE_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'AD', AMOUNT, NULL)) ADJUST_AMOUNT
                  FROM (
                    SELECT D.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
                      FROM LEDGER L, BATCH B, DOC D
                      WHERE L.BATCH_ID = B.BATCH_ID
                      AND L.DELETED = 'N'
                      AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                      AND B.DOC_ID = D.DOC_ID
                      AND NVL(L.PAID_DATE, MIN_DATE) < l_as_of
                      AND L.LEDGER_DATE < l_as_of
                      GROUP BY D.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
                  GROUP BY CUSTOMER_BANK_ID) A
            WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
            AND CB.CUSTOMER_ID = C.CUSTOMER_ID
            ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR;
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
      l_batch_id LEDGER.BATCH_ID%TYPE;
      l_entry_date LEDGER.ENTRY_DATE%TYPE;
      l_new_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        SELECT L.BATCH_ID, L.ENTRY_DATE
          INTO l_batch_id, l_entry_date
          FROM LEDGER L
         WHERE LEDGER_ID = l_ledger_id;
        l_new_batch_id := GET_NEW_BATCH(l_batch_id, l_entry_date);
        UPDATE LEDGER L
           SET BATCH_ID = l_new_batch_id
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted (remove net revenue fees also)
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id
            OR RELATED_LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_curr_id DOC.CURRENCY_ID%TYPE;
        l_fee_date LEDGER.ENTRY_DATE%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID
          INTO l_customer_bank_id, l_curr_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        l_status := GET_DOC_STATUS(l_doc_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to add adjustments. Adjustment not added');
    	END IF;
        BEGIN
            SELECT NVL(B.BATCH_ID, NULL), T.FEE_CURRENCY_ID
              INTO l_batch_id, l_term_curr_id
              FROM BATCH B
              LEFT JOIN REPORT.TERMINAL T
                ON T.TERMINAL_ID = l_terminal_id
             WHERE B.DOC_ID = l_doc_id
               AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
               AND B.PAYMENT_SCHEDULE_ID = 1;
            -- ensure that currency matches
            IF l_terminal_id IS NOT NULL AND NVL(l_curr_id, 0) <> NVL(l_term_curr_id, 0) THEN
                RAISE_APPLICATION_ERROR(-20020, 'Terminal uses a different currency than the doc''s currency. Adjustment not added');
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL; -- Ignore
            WHEN OTHERS THEN
                RAISE;
        END;
    
        IF l_batch_id IS NULL THEN
            -- create a batch and set the doc id
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                DECODE(l_terminal_id, 0, NULL, l_terminal_id),
                1,
                SYSDATE,
                SYSDATE,
                DECODE(l_status, 'L', 'F', 'O', 'L'));
        END IF;
    	SELECT LEDGER_SEQ.NEXTVAL, SYSDATE
          INTO l_ledger_id, l_fee_date
          FROM DUAL;
    	INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION,
            CREATE_BY)
           VALUES (
            l_ledger_id,
            'AD',
            l_amt,
            l_fee_date,
            l_batch_id,
            2 /*process-no require*/,
            l_fee_date,
            l_reason,
            l_user_id);
        -- create net revenue fee also
    	INSERT INTO LEDGER(
            LEDGER_ID,
            RELATED_LEDGER_ID,
            SERVICE_FEE_ID,
            ENTRY_TYPE,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
         SELECT
            LEDGER_SEQ.NEXTVAL,
            l_ledger_id,
            SF.SERVICE_FEE_ID,
            'SF',
            -l_amt * SF.FEE_PERCENT,
            l_fee_date,
            l_batch_id,
            2 /*process-no require*/,
            l_fee_date,
            F.FEE_NAME
          FROM SERVICE_FEES SF, FEES F
         WHERE SF.FEE_ID = F.FEE_ID
           AND SF.TERMINAL_ID = l_terminal_id
           AND l_fee_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
           AND SF.FREQUENCY_ID = 6;
    END;
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_status CORP.DOC.STATUS%TYPE;
    BEGIN
  	    l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to update adjustments. Adjustment not updated');
    	END IF;
        UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
         -- update net revenue
         UPDATE LEDGER L SET AMOUNT = -l_amt * (
            SELECT SF.FEE_PERCENT
              FROM SERVICE_FEES SF
             WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
               AND SF.FREQUENCY_ID = 6)
          WHERE L.RELATED_LEDGER_ID = l_ledger_id
            AND L.ENTRY_TYPE = 'SF'
            AND EXISTS(SELECT 1
                FROM SERVICE_FEES SF
                WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                AND SF.FREQUENCY_ID = 6
            );
    END;

    PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE)
    IS
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'O' THEN
            UPDATE DOC
               SET STATUS = 'L', UPDATE_DATE = SYSDATE
             WHERE DOC_ID = l_doc_id;

            --Close all closeable batches
            UPDATE BATCH B
               SET B.BATCH_STATE_CD = 'F',
                   B.BATCH_CLOSED_TS = SYSDATE,
                   B.END_DATE = NVL(B.END_DATE, SYSDATE)
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'L';

            UPDATE BATCH B
               SET B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8)
               AND B.END_DATE <= SYSDATE;

            -- Remove unclosed batches
            DECLARE
                l_cnt PLS_INTEGER;
                l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_cur_id DOC.CURRENCY_ID%TYPE;
                l_bu_id DOC.BUSINESS_UNIT_ID%TYPE;
                l_new_doc_id DOC.DOC_ID%TYPE;
            BEGIN
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM BATCH B
                 WHERE B.DOC_ID = l_doc_id
                   AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                IF l_cnt > 0 THEN
                    SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
                      INTO l_cb_id, l_cur_id, l_bu_id
                      FROM DOC D
                    WHERE D.DOC_ID = l_doc_id;
                    l_new_doc_id := GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
                   UPDATE BATCH B
                       SET B.DOC_ID = l_new_doc_id
                     WHERE B.DOC_ID = l_doc_id
                       AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                END IF;
            END;
            
            -- Remove any unsettled trans from as batches
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.BATCH_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
                     AND L.BATCH_ID IN(
                            SELECT B.BATCH_ID
                              FROM BATCH B
                             WHERE B.DOC_ID = l_doc_id);
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_NEW_BATCH(l_recs(i).BATCH_ID, l_recs(i).ENTRY_DATE);
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;

            -- add rounding entries
            ADD_BATCH_ROUNDING_ENTRIES(l_doc_id);
            ADD_ROUNDING_ENTRY(l_doc_id);
        END IF;
    END;

    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
      l_total DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'L' THEN
            -- add Positive Net Revenue Adjustment, if necessary
            DECLARE
                l_nrf_amt LEDGER.AMOUNT%TYPE;
                l_fee_date LEDGER.ENTRY_DATE%TYPE;
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
                l_batch_id BATCH.BATCH_ID%TYPE;
            BEGIN
                SELECT NVL(SUM(AMOUNT), 0), SYSDATE
                  INTO l_nrf_amt, l_fee_date
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF
                 WHERE L.BATCH_ID = B.BATCH_ID
                   AND B.DOC_ID = l_doc_id
                   AND L.DELETED = 'N'
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND SF.FREQUENCY_ID = 6;
                 IF l_nrf_amt > 0 THEN
                    -- l_nrf_amt is positive (money to the customer), so back it out with an adjustment
                    SELECT MAX(B.BATCH_ID)
                      INTO l_batch_id
                      FROM BATCH B
                     WHERE B.DOC_ID = l_doc_id
                       AND B.PAYMENT_SCHEDULE_ID = 7;
                    IF l_batch_id IS NULL THEN
                	    SELECT BATCH_SEQ.NEXTVAL
                          INTO l_batch_id
                          FROM DUAL;
                        INSERT INTO BATCH(
                            BATCH_ID,
                            DOC_ID,
                            TERMINAL_ID,
                            PAYMENT_SCHEDULE_ID,
                            START_DATE,
                            END_DATE,
                            BATCH_STATE_CD)
                          VALUES(
                            l_batch_id,
                            l_doc_id,
                            NULL,
                            7,
                            l_fee_date,
                            l_fee_date,
                            'F');
                    END IF;
                	SELECT LEDGER_SEQ.NEXTVAL
                      INTO l_ledger_id
                      FROM DUAL;
                	INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION,
                        CREATE_BY)
                       VALUES (
                        l_ledger_id,
                        'AD',
                        -l_nrf_amt,
                        l_fee_date,
                        l_batch_id,
                        2 /*process-no require*/,
                        l_fee_date,
                        'Net Revenue Hurdle Not Met',
                        l_user_id);
                 END IF;
            END;
            ADD_ROUNDING_ENTRY(l_doc_id);
            SELECT SUM(AMOUNT)
              INTO l_total
              FROM LEDGER L, BATCH B
             WHERE L.DELETED = 'N'
               AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = l_doc_id;

        	UPDATE DOC D
               SET APPROVE_BY = l_user_id,
                   UPDATE_DATE = SYSDATE,
                   STATUS = 'A',
                   TOTAL_AMOUNT = l_total,
                   DOC_TYPE = (
                        SELECT DECODE(CB.IS_EFT, 'Y', 'E', 'P') -- electronic or paper
                               || CASE WHEN l_total > 0 THEN 'C' ELSE 'D' END -- credit or debit
                          FROM CUSTOMER_BANK CB
                         WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID)
      	 		WHERE DOC_ID = l_doc_id;
          END IF;
    END;
    
    PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
        UPDATE DOC D SET APPROVE_BY = NULL, UPDATE_DATE = SYSDATE, STATUS = 'L',
            TOTAL_AMOUNT = NULL, DOC_TYPE = '--'
  	 		WHERE DOC_ID = l_doc_id
              AND STATUS = 'A';
        IF SQL%FOUND THEN
            -- Remove adjustments for positive net revenue fees
            UPDATE LEDGER
               SET DELETED = 'Y'
             WHERE ENTRY_TYPE = 'AD'
               AND TRANS_ID IS NULL
               AND BATCH_ID IN(
                    SELECT BATCH_ID
                      FROM BATCH
                     WHERE DOC_ID = l_doc_id
                       AND PAYMENT_SCHEDULE_ID = 7);
        END IF;
    END;
    
    PROCEDURE      UPLOAD_TO_ORF (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
    	 INSERT INTO USAT_INVOICE_INTERFACE@FINANCIALS(
            VENDOR_NAME_I, --                                        VARCHAR2 (240)                        REQUIRED     vendor name
            INVOICE_NUMBER_I, --                                    VARCHAR2 (240)                       REQUIRED      invoice number
            INVOICE_DATE_I, --                                           DATE                                           OPTIONAL
            INVOICE_AMOUNT_I, --                                    NUMBER                                     REQUIRED      amount of invoice
            DISTRIBUTION_SET_NAME_I, --                     VARCHAR2 (240)                       REQUIRED
            GL_DATE_I, --                                                      DATE                                            OPTIONAL
            PAY_GROUP_LOOKUP_CODE_I, --                 VARCHAR2 (240)                        REQUIRED     pay group
            VENDOR_SITE_CODE_I, --                                VARCHAR2 (25)                          REQUIRED     site code
            INVOICE_CURRENCY_CODE_I, --                  VARCHAR2 (15)                           REQUIRED     invoice currency
            PAYMENT_METHOD_LOOKUP_CODE_I, --   VARCHAR2 (25)                         REQUIRED     payment method
            VENDOR_NUMBER_I) --                                    VARCHAR2 (200)                        OPTIONAL
        SELECT
            C.CUSTOMER_NAME,
            D.REF_NBR,
            D.SENT_DATE,
            D.TOTAL_AMOUNT,
            BU.DISTRIBUTION_SET_CD,
            NVL((SELECT MAX(B.END_DATE-1)
                  FROM CORP.BATCH B
                 WHERE D.DOC_ID = B.DOC_ID
                   AND B.PAYMENT_SCHEDULE_ID IN(3,4,6)), D.SENT_DATE),
            BU.PAY_GROUP_CD,
            CB.CUSTOMER_BANK_ID,
            CU.CURRENCY_CODE,
            DECODE(CB.IS_EFT, 'Y', 'EFT', 'CHECK'),
            C.CUSTOMER_ID
        FROM CORP.CUSTOMER C, CORP.CUSTOMER_BANK CB, CORP.DOC D, CORP.BUSINESS_UNIT BU, CORP.CURRENCY CU
        WHERE D.DOC_ID = l_doc_id
        AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
        AND CB.CUSTOMER_ID = C.CUSTOMER_ID
        AND D.CURRENCY_ID = CU.CURRENCY_ID
        AND D.BUSINESS_UNIT_ID = BU.BUSINESS_UNIT_ID;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
        l_paid_date DATE := SYSDATE;
        l_total CORP.DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
    	 UPDATE DOC SET SENT_BY = l_user_id, UPDATE_DATE = l_paid_date, STATUS = 'P',
                SENT_DATE = l_paid_date, TOTAL_AMOUNT = NVL(l_total, TOTAL_AMOUNT)
          WHERE DOC_ID = l_doc_id
            AND STATUS = 'A';
         IF SQL%FOUND THEN
             UPDATE LEDGER SET PAID_DATE = l_paid_date
              WHERE BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
             UPLOAD_TO_ORF(l_doc_id);
         END IF;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N')
    IS
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_override <> 'Y' AND l_effective_date < SYSDATE THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T, DOC D
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND L.DELETED = 'N'
                    AND D.STATUS NOT IN('O', 'D');
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
    	 UPDATE PROCESS_FEES SET END_DATE = l_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_effective_date) >= l_effective_date;
    	 BEGIN
            DELETE FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
        	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(START_DATE, MIN_DATE) >= NVL(END_DATE, MAX_DATE);	
         EXCEPTION
            WHEN CHILD_RECORD_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
         END;
         IF NVL(l_fee_percent, 0) <> 0 OR NVL(l_fee_amount, 0) <> 0 THEN	
        	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
         	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_fee_minimum, l_effective_date);
 	     END IF;
    END;
    
    FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
    IS
    BEGIN
        IF l_months >= 12  AND MOD(l_months, 12.0) = 0 THEN
            RETURN 'FMYYYY';
        ELSIF l_months > 0 THEN
            RETURN 'FMMonth, YYYY';
        ELSIF l_days >= 1 AND MOD(l_days, 1.0) = 0 THEN
            RETURN 'FMMM/DD/YYYY';
        ELSE
            RETURN 'FMMM/DD/YYYY HH:MI AM';
        END IF;
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   F.FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE) EFFECTIVE_END_DATE,
                   F.FEE_NAME,
                   T.FEE_CURRENCY_ID,
                   DECODE(T.PAYMENT_SCHEDULE_ID, 2, 'Y', 8, 'Y', 'N') AS_ACCUM
              FROM CORP.SERVICE_FEES SF, CORP.FREQUENCY Q, CORP.CUSTOMER_BANK_TERMINAL CBT, CORP.FEES F, REPORT.TERMINAL T
             WHERE SF.FREQUENCY_ID = Q.FREQUENCY_ID
               AND SF.TERMINAL_ID = CBT.TERMINAL_ID
               AND SF.FEE_ID = F.FEE_ID
               AND CORP.WITHIN1(SYSDATE, CBT.START_DATE, CBT.END_DATE) = 1
               AND (Q.DAYS > 0 OR Q.MONTHS > 0)
               AND CASE WHEN Q.MONTHS = 0 THEN NVL(SF.LAST_PAYMENT, MIN_DATE) + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(NVL(SF.LAST_PAYMENT, MIN_DATE), Q.MONTHS)), 'DD')
                        END < LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE)
               AND SF.TERMINAL_ID = T.TERMINAL_ID;

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        FOR r_fee IN c_fee LOOP
            -- If last payment is null, Use first transaction as last payment date
            IF r_fee.LAST_PAYMENT IS NULL THEN
              SELECT MIN(x.CLOSE_DATE)
                INTO l_last_payment
                FROM REPORT.TRANS x
              WHERE x.TERMINAL_ID = r_fee.TERMINAL_ID
                AND x.CLOSE_DATE >= NVL(r_fee.START_DATE, MIN_DATE);
              IF l_last_payment IS NOT NULL THEN
                 UPDATE SERVICE_FEES
                    SET LAST_PAYMENT = l_last_payment
                  WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;
              END IF;
            ELSE
              l_last_payment := r_fee.LAST_PAYMENT;
            END IF;
            IF l_last_payment IS NOT NULL THEN
              l_fmt := GET_SERVICE_FEE_DATE_FMT(r_fee.MONTHS, r_fee.DAYS);
              LOOP
                  IF r_fee.MONTHS > 0 THEN
                      SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                        INTO l_last_payment
                        FROM DUAL;
                  ELSE
                      SELECT l_last_payment + r_fee.DAYS
                        INTO l_last_payment
                        FROM DUAL;
                  END IF;
                  EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
  
                  DECLARE
                     l_fee_amt CORP.LEDGER.AMOUNT%TYPE;
                     l_desc CORP.LEDGER.DESCRIPTION%TYPE;
                     l_hosts PLS_INTEGER;
                  BEGIN -- catch exception here
                      -- skip creation of fee if start date is after fee date
                      IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                          l_batch_id := GET_OR_CREATE_BATCH(
                                      r_fee.TERMINAL_ID,
                                      r_fee.CUSTOMER_BANK_ID,
                                      l_last_payment,
                                      r_fee.FEE_CURRENCY_ID,
                                      r_fee.AS_ACCUM);
                          SELECT LEDGER_SEQ.NEXTVAL
                            INTO l_ledger_id
                            FROM DUAL;
                          IF r_fee.fee_id = 9 THEN
                             SELECT MAX(REAL_TIME.DEVICE_HOST_COUNT(e.EPORT_SERIAL_NUM, l_last_payment))
                               INTO l_hosts
                               FROM REPORT.EPORT e, REPORT.TERMINAL_EPORT te
                              WHERE e.EPORT_ID = te.EPORT_ID
                                AND te.TERMINAL_ID = r_fee.TERMINAL_ID
                                AND l_last_payment BETWEEN NVL(te.START_DATE, MIN_DATE) AND NVL(te.END_DATE, MAX_DATE);
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT * l_hosts);
                             l_desc := r_fee.FEE_NAME || ' ('||TO_CHAR(l_hosts)||' hosts) ' || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          ELSE
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT);
                             l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          END IF;
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          VALUES (
                              l_ledger_id,
                              'SF',
                              r_fee.SERVICE_FEE_ID,
                              l_fee_amt,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              l_desc);
                          -- create net revenue fee on transaction, if any
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              RELATED_LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          SELECT
                              LEDGER_SEQ.NEXTVAL,
                              l_ledger_id,
                              'SF',
                              SF.SERVICE_FEE_ID,
                              -l_fee_amt * SF.FEE_PERCENT,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              F.FEE_NAME
                          FROM SERVICE_FEES SF, FEES F
                         WHERE SF.FEE_ID = F.FEE_ID
                           AND SF.TERMINAL_ID = r_fee.TERMINAL_ID
                           AND l_last_payment BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                AND NVL(SF.END_DATE, MAX_DATE)
                           AND SF.FREQUENCY_ID = 6;
                      END IF;
  
                      UPDATE SERVICE_FEES
                         SET LAST_PAYMENT = l_last_payment
                       WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;
  
                      COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                  EXCEPTION
                                   WHEN DUP_VAL_ON_INDEX THEN
                                           ROLLBACK;
                  END;
              END LOOP;
            END IF;
        END LOOP;
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N')
    IS
        l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_sf_id SERVICE_FEES.SERVICE_FEE_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_next_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
    BEGIN
        BEGIN
            IF l_freq_id = 6 THEN
                --disallow change to net revenue fees that affect already paid entries
                SELECT MAX(ENTRY_DATE)
                  INTO l_last_payment
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF, DOC D
                 WHERE B.TERMINAL_ID = l_terminal_id
                   AND SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND B.DOC_ID = D.DOC_ID
                   AND D.STATUS NOT IN('O', 'D')
                   AND B.BATCH_ID = L.BATCH_ID
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.DELETED = 'N';
                IF NVL(l_last_payment, l_effective_date) > l_effective_date THEN -- BIG PROBLEM
                    RAISE_APPLICATION_ERROR(-20012, 'This net revenue fee was charged to the customer after the specified effective date.');
                ELSE
                    l_last_payment := MIN_DATE;
                END IF;
            ELSIF l_fee_id NOT IN(7) THEN
                SELECT MAX(LAST_PAYMENT), 
                      CASE WHEN Q.MONTHS = 0 THEN l_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_effective_date, Q.MONTHS)), 'DD')
                      END
                  INTO l_last_payment, l_next_payment
                  FROM SERVICE_FEES SF, CORP.FREQUENCY Q
                 WHERE SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
                 GROUP BY Q.MONTHS, Q.DAYS;
                IF l_last_payment is not null AND l_last_payment > l_next_payment THEN
                    IF l_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
                    END IF;
                END IF;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            	 NULL;
            WHEN OTHERS THEN
            	 RAISE;
        END;
        IF l_freq_id = 6 THEN
            --delete any old net revenue fees >= l_effective_date
            DELETE FROM LEDGER L
             WHERE L.ENTRY_TYPE = 'SF'
               AND L.SERVICE_FEE_ID IN(
                 SELECT SF.SERVICE_FEE_ID
                   FROM SERVICE_FEES SF
                  WHERE SF.TERMINAL_ID = l_terminal_id
                    AND SF.FEE_ID = l_fee_id
                    AND SF.FREQUENCY_ID = l_freq_id
                    --AND NVL(SF.END_DATE, l_effective_date) >= l_effective_date
                    )
               AND L.ENTRY_DATE >= l_effective_date
               AND NOT EXISTS(
                    SELECT 1
                    FROM BATCH B, DOC D
                    WHERE B.BATCH_ID = L.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND D.STATUS NOT IN('O', 'D'));
        END IF;
        IF l_fee_id NOT IN(7) THEN
            UPDATE SERVICE_FEES SET END_DATE = l_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = l_freq_id
               AND NVL(END_DATE, l_effective_date) >= l_effective_date;
        END IF;
        IF NVL(l_fee_amt, 0) <> 0 OR NVL(l_fee_perc, 0) <> 0 THEN
            SELECT SERVICE_FEE_SEQ.NEXTVAL INTO l_sf_id FROM DUAL;
            INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID,
                FREQUENCY_ID, FEE_AMOUNT, FEE_PERCENT, START_DATE,
                END_DATE, LAST_PAYMENT)
                SELECT l_sf_id, l_terminal_id, l_fee_id, l_freq_id, l_fee_amt,
                       l_fee_perc, l_effective_date, l_end_date,
                       NVL(l_last_payment, l_effective_date)
                  FROM FREQUENCY F
                 WHERE F.FREQUENCY_ID = l_freq_id;
            IF l_freq_id = 6 THEN
                --add any new net revenue fees
                INSERT INTO LEDGER(
                    LEDGER_ID,
                    RELATED_LEDGER_ID,
                    ENTRY_TYPE,
                    TRANS_ID,
                    PROCESS_FEE_ID,
                    SERVICE_FEE_ID,
                    AMOUNT,
                    ENTRY_DATE,
                    BATCH_ID,
                    SETTLE_STATE_ID,
                    LEDGER_DATE,
                    DESCRIPTION)
                SELECT
                    LEDGER_SEQ.NEXTVAL,
                    L.LEDGER_ID,
                    'SF',
                    L.TRANS_ID,
                    L.PROCESS_FEE_ID,
                    l_sf_id,
                    -L.AMOUNT * l_fee_perc,
                    L.ENTRY_DATE,
                    L.BATCH_ID,
                    L.SETTLE_STATE_ID,
                    L.LEDGER_DATE,
                    F.FEE_NAME
                FROM LEDGER L, BATCH B, FEES F, DOC D
               WHERE F.FEE_ID = l_fee_id
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.TERMINAL_ID = l_terminal_id
                 AND L.ENTRY_DATE >= l_effective_date
                 AND L.RELATED_LEDGER_ID IS NULL -- Avoid Percent of Percent Fee
                 AND L.DELETED = 'N'
                 AND B.DOC_ID = D.DOC_ID
                 AND D.STATUS IN('O')
                 AND L.ENTRY_TYPE IN('CC', 'PF', 'SF', 'AD', 'CB', 'RF');
            ELSIF l_override = 'Y' THEN -- let's insert any missing (No need to do this for net rev fees)
                DECLARE
                    CURSOR l_cur IS
                        SELECT LEDGER_SEQ.NEXTVAL LEDGER_ID,
                               A.FEE_DATE,
                               CBT.CUSTOMER_BANK_ID,
                               T.FEE_CURRENCY_ID,
                               F.FEE_NAME || ' for ' || TO_CHAR(A.FEE_DATE, A.FMT) FEE_DESC,
                               'Y' AS_ACCUM
                    FROM (SELECT DECODE(Q.DAYS, 0, LAST_DAY(D.COLUMN_VALUE), D.COLUMN_VALUE + Q.DAYS - 1) FEE_DATE,
                                 GET_SERVICE_FEE_DATE_FMT(Q.MONTHS, Q.DAYS) FMT
                            FROM TABLE(CAST(CORP.GLOBALS_PKG.GET_DATE_LIST(l_effective_date,
                                 LEAST(NVL(l_last_payment, (SELECT  
                                   CASE WHEN Q.MONTHS = 0 THEN NVL(SYSDATE, MIN_DATE) - Q.DAYS
                                        ELSE TRUNC(LAST_DAY(ADD_MONTHS(NVL(SYSDATE, MIN_DATE), -Q.MONTHS)), 'DD')
                                   END FROM CORP.FREQUENCY Q WHERE Q.FREQUENCY_ID = l_freq_id)), NVL(l_end_date, MAX_DATE)),
                                 l_freq_id, 0, 0) AS REPORT.DATE_LIST)) D,
                                 FREQUENCY Q
                            WHERE Q.FREQUENCY_ID = l_freq_id
                              AND (Q.MONTHS > 0 OR Q.DAYS > 0)) A,
                          CUSTOMER_BANK_TERMINAL CBT,
                          FEES F,
                          REPORT.TERMINAL T
                    WHERE NOT EXISTS(SELECT 1
                        FROM CORP.LEDGER L, CORP.BATCH B
                        WHERE L.BATCH_ID = B.BATCH_ID
                          AND B.TERMINAL_ID = l_terminal_id
                          AND L.ENTRY_TYPE = 'SF'
                          AND L.DELETED = 'N'
                          AND L.AMOUNT <> 0
                          AND L.ENTRY_DATE = A.FEE_DATE)
                      AND WITHIN1(A.FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                      AND CBT.TERMINAL_ID = l_terminal_id
                      AND F.FEE_ID = l_fee_id
                      AND T.TERMINAL_ID = l_terminal_id;
                BEGIN
                    FOR l_rec IN l_cur LOOP
                        l_batch_id := GET_OR_CREATE_BATCH(
                                    l_terminal_id,
                                    l_rec.CUSTOMER_BANK_ID,
                                    l_rec.FEE_DATE,
                                    l_rec.FEE_CURRENCY_ID,
                                    l_rec.AS_ACCUM);

                        INSERT INTO LEDGER(
                                LEDGER_ID,
                                ENTRY_TYPE,
                                SERVICE_FEE_ID,
                                AMOUNT,
                                ENTRY_DATE,
                                BATCH_ID,
                                SETTLE_STATE_ID,
                                LEDGER_DATE,
                                DESCRIPTION)
                            VALUES(
                                l_rec.LEDGER_ID,
                                'SF',
                                l_sf_id,
                                -ABS(l_fee_amt),
                                l_rec.FEE_DATE,
                                l_batch_id,
                                2,
                                l_rec.FEE_DATE,
                                l_rec.FEE_DESC);
                        -- create net revenue fee on service fee, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_rec.LEDGER_ID,
                            'SF',
                            SF.SERVICE_FEE_ID,
                            ABS(l_fee_amt) * SF.FEE_PERCENT,
                            l_rec.FEE_DATE,
                            l_batch_id,
                            2,
                            l_rec.FEE_DATE,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_terminal_id
                         AND l_rec.FEE_DATE BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END LOOP;
                    -- TODO: Set LAST_PAYMENT if necessary
                END;
            END IF;
        END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
    	IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) <> 'L' THEN
           RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
   	    END IF;
    	--Remove rounding entries
    	UPDATE LEDGER
           SET DELETED = 'Y'
         WHERE ENTRY_TYPE = 'SB'
           AND TRANS_ID IS NULL
           AND BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
           
        -- Blank out the end date of any as accum batches
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'L',
               B.BATCH_CLOSED_TS = NULL,
               B.END_DATE = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD = 'F'
           AND B.PAYMENT_SCHEDULE_ID IN(1,5);

        -- Change time-period batches to Open
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'O',
               B.BATCH_CLOSED_TS = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD IN('U', 'D')
           AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8);

        -- first condense any docs that may match once they are re-opened
    	DECLARE
    	   l_new_doc_id DOC.DOC_ID%TYPE;
        BEGIN
    	    SELECT MAX(DNEW.DOC_ID)
              INTO l_new_doc_id
              FROM DOC DOLD, DOC DNEW
             WHERE DOLD.DOC_ID = l_doc_id
               AND DNEW.DOC_ID <> l_doc_id
               AND DOLD.CUSTOMER_BANK_ID = DNEW.CUSTOMER_BANK_ID
               AND NVL(DOLD.CURRENCY_ID, 0) = NVL(DNEW.CURRENCY_ID, 0)
               AND DNEW.STATUS = 'O';
            IF l_new_doc_id IS NOT NULL THEN
            	-- Update batches and delete old doc
            	UPDATE BATCH SET DOC_ID = l_new_doc_id WHERE DOC_ID = l_doc_id;
            	DELETE FROM DOC WHERE DOC_ID = l_doc_id;
            	
            	--now condense any batches that may match once they are moved
            	DECLARE
            	   CURSOR l_cur IS
                    	SELECT BNEW.BATCH_ID NEW_BATCH_ID,
                               BOLD.BATCH_ID OLD_BATCH_ID,
                               BOLD.END_DATE OLD_END_DATE,
                               BOLD.START_DATE OLD_START_DATE
                          FROM BATCH BOLD, BATCH BNEW
                         WHERE BOLD.DOC_ID = l_doc_id
                           AND BNEW.DOC_ID = l_doc_id
                           AND BOLD.PAYMENT_SCHEDULE_ID = BNEW.PAYMENT_SCHEDULE_ID
                           AND NVL(BOLD.TERMINAL_ID, 0) = NVL(BNEW.TERMINAL_ID, 0)
                           AND BOLD.BATCH_ID > BNEW.BATCH_ID
                           AND (BOLD.PAYMENT_SCHEDULE_ID IN(1,5)
                             OR BOLD.START_DATE = BNEW.START_DATE);
            	BEGIN
            	   FOR l_rec IN l_cur LOOP
            	       UPDATE LEDGER
            	          SET BATCH_ID = l_rec.NEW_BATCH_ID
                        WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                        
                       DELETE FROM BATCH WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       
                       UPDATE BATCH
                          SET START_DATE = LEAST(START_DATE, l_rec.OLD_START_DATE),
                              END_DATE = GREATEST(END_DATE, l_rec.OLD_END_DATE)
                        WHERE BATCH_ID = l_rec.NEW_BATCH_ID;
            	   END LOOP;
         	   END;
            ELSE
                -- Update doc
            	UPDATE DOC SET STATUS = 'O' WHERE DOC_ID = l_doc_id;
            END IF;
    	END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
    IS
        l_old_payable VARCHAR(4000); -- oracle raises an exception if you use anything with smaller length
        l_old_terminal_id BATCH.TERMINAL_ID%TYPE;
        l_old_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_desc LEDGER.DESCRIPTION%TYPE;
    BEGIN
        -- figure out why this was added
        --possible reasons:
        --  1. previously denied
        --  2. changed location
        --  3. changed customer bank
        
        SELECT *
          INTO l_old_payable, l_old_terminal_id, l_old_customer_bank_id
          FROM (
            SELECT PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE),
                   B.TERMINAL_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND L.TRANS_ID = l_trans_id
               AND L.ENTRY_TYPE = 'CC'
               AND B.PAYMENT_SCHEDULE_ID <> 5
             ORDER BY L.CREATE_DATE DESC) A
         WHERE ROWNUM = 1;
        IF l_old_payable = 'N' THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_terminal_id <> l_old_terminal_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_customer_bank_id <> l_old_customer_bank_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSE
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
    END;
    
    PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE)
    IS
    	CURSOR l_cur IS
    		SELECT TRANS_TYPE_ID, CLOSE_DATE, SETTLE_DATE, TOTAL_AMOUNT, SETTLE_STATE_ID,
                   TERMINAL_ID, CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
    		  FROM REPORT.TRANS
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
         FOR l_rec IN l_cur LOOP
    	     UPDATE_LEDGER(l_tran_id,l_rec.trans_type_id,l_rec.close_date,l_rec.settle_date,
                           l_rec.total_amount,l_rec.settle_state_id,l_rec.terminal_id,
                           l_rec.customer_bank_id,l_rec.process_fee_id,l_rec.currency_id);
   	     END LOOP;
    END;
    
    PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE)
    IS
        l_batch_id CORP.BATCH.BATCH_ID%TYPE;
        l_service_fee_id CORP.LEDGER.SERVICE_FEE_ID%TYPE;
        l_desc CORP.LEDGER.DESCRIPTION%TYPE;
        l_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
        l_cust_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_fee_currency_id CORP.DOC.CURRENCY_ID%TYPE;
    BEGIN
        SELECT cbt.CUSTOMER_BANK_ID, t.FEE_CURRENCY_ID,
               'Activation Fee for Terminal ' || t.TERMINAL_NBR
          INTO l_cust_bank_id, l_fee_currency_id, l_desc
          FROM REPORT.TERMINAL t, CORP.CUSTOMER_BANK_TERMINAL cbt
         WHERE t.TERMINAL_ID = l_terminal_id
           AND t.TERMINAL_ID = cbt.TERMINAL_ID
           AND l_activation_date >= NVL(cbt.START_DATE, MIN_DATE)
           AND l_activation_date < NVL(cbt.END_DATE, MAX_DATE);
        l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_cust_bank_id, l_activation_date, l_fee_currency_id, 'N');
        SELECT SERVICE_FEE_SEQ.NEXTVAL
          INTO l_service_fee_id
          FROM DUAL;
        INSERT INTO SERVICE_FEES(
            SERVICE_FEE_ID,
            TERMINAL_ID,
            FEE_ID,
            FEE_AMOUNT,
            FREQUENCY_ID,
            LAST_PAYMENT,
            START_DATE)
       	  VALUES(
       	    l_service_fee_id,
       	    l_terminal_id,
       	    10,
       	    l_fee_amt,
       	    7,
       	    l_activation_date,
       	    l_activation_date);
       	    
        SELECT LEDGER_SEQ.NEXTVAL
          INTO l_ledger_id
          FROM DUAL;
        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            SERVICE_FEE_ID,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
        VALUES (
            l_ledger_id,
            'SF',
            l_service_fee_id,
            -l_fee_amt,
            l_activation_date,
            l_batch_id,
            2,
            l_activation_date,
            l_desc);
    END;
    
    PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE)
    IS
        l_lock VARCHAR(128);
        l_adj_amt CORP.LEDGER.AMOUNT%TYPE;
        l_batch_id CORP.LEDGER.BATCH_ID%TYPE;
       	CURSOR l_cur IS
    		SELECT X.TRANS_TYPE_ID, X.CLOSE_DATE, X.SETTLE_DATE, X.TOTAL_AMOUNT, X.SETTLE_STATE_ID,
                   X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.PROCESS_FEE_ID, X.CURRENCY_ID, T.BUSINESS_UNIT_ID
    		  FROM REPORT.TRANS X
  		     INNER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF NVL(l_rec.terminal_id, 0) <> 0 AND NVL(l_rec.customer_bank_id, 0) <> 0 THEN
                l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_tran_id);
                -- Remove any open entries in different pay sched
                UPDATE CORP.LEDGER L SET DELETED = 'Y'
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND EXISTS(
                      SELECT 1
                        FROM CORP.BATCH B
                       INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                       WHERE B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                         AND B.BATCH_STATE_CD IN('O', 'L')
                         AND D.STATUS IN('O')
                         AND L.BATCH_ID = B.BATCH_ID);

                -- Make adjustment for old
                SELECT SUM(L.AMOUNT)
                  INTO l_adj_amt
                  FROM CORP.LEDGER L
                 INNER JOIN CORP.BATCH B ON L.BATCH_ID = B.BATCH_ID
                 INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND D.STATUS NOT IN('D')
                   AND B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                   AND ENTRY_PAYABLE(l.settle_state_id,l.entry_type) = 'Y';

                IF NVL(l_adj_amt, 0) <> 0 THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id,
                                    l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, 'A');
                    INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        TRANS_ID,
                        PROCESS_FEE_ID,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION)
                    SELECT
                        LEDGER_SEQ.NEXTVAL,
                        'AD',
                        l_tran_id,
                        NULL,
                        -l_adj_amt,
                        l_rec.close_date,
                        l_batch_id,
                        l_rec.SETTLE_STATE_ID,
                        NVL(l_rec.SETTLE_DATE, SYSDATE),
                        'Payment schedule switched to ' || DESCRIPTION
                      FROM CORP.PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_payment_schedule_id;
                END IF;
                -- insert as new
                l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id, l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, l_payment_schedule_id, l_rec.BUSINESS_UNIT_ID, 'N');
                INSERT_TRANS_TO_LEDGER(l_tran_id, l_rec.trans_type_id, l_rec.close_date, l_rec.settle_date,
                     l_rec.total_amount, l_rec.settle_state_id, l_rec.process_fee_id, l_batch_id);
            END IF;
        END LOOP;
    END;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.pbk?rev=1.22
CREATE OR REPLACE PACKAGE BODY REPORT.DATA_IN_PKG IS
   FK_NOT_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(FK_NOT_FOUND, -2291);

    FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_device_id EPORT.EPORT_ID%TYPE;
        l_use_device_type EPORT.DEVICE_TYPE_ID%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_device_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_device_serial;
         -- NOTE: eventually the above should be source system dependent
         RETURN l_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT EPORT_SEQ.NEXTVAL INTO l_device_id FROM DUAL;
            IF l_device_type IS NOT NULL THEN
                l_use_device_type := l_device_type;
            ELSIF l_device_serial LIKE 'E4%' THEN
                l_use_device_type := 0; -- G4
			ELSIF l_device_serial LIKE 'EE%' THEN
                l_use_device_type := 13; -- Edge
            ELSIF l_device_serial LIKE 'G%' THEN
                l_use_device_type := 1; -- G5-G8
			ELSIF l_device_serial LIKE 'K%' THEN
                l_use_device_type := 11; -- Kiosk
            ELSIF l_device_serial LIKE 'M1%' THEN
                l_use_device_type := 6; -- MEI
            --ELSIF l_device_serial LIKE '10%' THEN -- esuds
            ELSIF l_device_serial LIKE '10%' THEN
                l_use_device_type := 3; -- Radisys Brick
            ELSE
                l_use_device_type := 10;
            END IF;
            BEGIN
                INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_device_id, l_device_serial, SYSDATE, l_use_device_type);
 		        COMMIT;
            EXCEPTION
                WHEN FK_NOT_FOUND THEN
                    ROLLBACK;
                    RAISE_APPLICATION_ERROR(-20889, 'Device Type ''' || l_use_device_type || ''' does not exist');
                WHEN DUP_VAL_ON_INDEX THEN
                    ROLLBACK;
                    IF l_tries > 0 THEN
                        RETURN GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type, l_tries - 1);
                    ELSE
                        RAISE;
                    END IF;
                WHEN OTHERS THEN
                    ROLLBACK;
                    RAISE;
            END;
            RETURN l_device_id;
        WHEN OTHERS THEN
            RAISE;
    END;
  
    FUNCTION GET_OR_CREATE_MERCHANT(
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN CORP.MERCHANT.MERCHANT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_merchant_id CORP.MERCHANT.MERCHANT_ID%TYPE;
    BEGIN
        SELECT MERCHANT_ID
          INTO l_merchant_id
          FROM CORP.MERCHANT
         WHERE MERCHANT_NBR = l_merchant_num;
         RETURN l_merchant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT CORP.MERCHANT_SEQ.NEXTVAL INTO l_merchant_id FROM DUAL;
            INSERT INTO CORP.MERCHANT(MERCHANT_ID, MERCHANT_NBR, DESCRIPTION, UPD_BY)
			  		 VALUES(l_merchant_id, l_merchant_num, 'Created for source system "'||l_source_system_cd||'"', 0);
            COMMIT;
            RETURN l_merchant_id;
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END;

  /* This allows external systems to add transactions. Credit, debit,
   * pass, access, or maintenance cards or refund, chargeback transaction
   * or cash should be added this way. Refunds
   * and chargebacks should always provide a valid original machine tran number.
   */
  PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_masked_card_number TRANS_C.MASKED_CARD_NUMBER%TYPE,
        l_hashed_card_number TRANS_C.HASHED_CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
		l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
		l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL)
    IS
        l_create_date TRANS.CREATE_DATE%TYPE;
        l_eport_id TRANS.EPORT_ID%TYPE;
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE;
        l_real_amount TRANS.TOTAL_AMOUNT%TYPE;
        l_currency_id TRANS.CURRENCY_ID%TYPE;
        l_tran_id TRANS.TRAN_ID%TYPE;
		l_cardtype_authority_id TRANS.CARDTYPE_AUTHORITY_ID%TYPE := NULL;
		l_lock VARCHAR2(128) := GLOBALS_PKG.REQUEST_LOCK('REPORT.TRANS::' || l_machine_trans_no, 0);
    BEGIN
        -- check for dup
        BEGIN
            SELECT CREATE_DATE
              INTO l_create_date
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            RAISE_APPLICATION_ERROR(-20880, 'A transaction with MACHINE_TRANS_NUM = "'
                ||l_machine_trans_no||'" from the source system "'
                ||l_source_system_cd||'" already exists. It was received '
                ||TO_CHAR(l_create_date, 'MM/DD/YYYY HH24:MI:SS')
                ||'. Transaction was NOT added.');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL; -- CONTINUE, no dup found
            WHEN OTHERS THEN
                RAISE;
        END;
        
        --get needed lookup values
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type);
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        
        IF l_orig_machine_trans_no IS NOT NULL THEN
            BEGIN
                SELECT TRAN_ID
                  INTO l_orig_tran_id
                  FROM TRANS
                 WHERE MACHINE_TRANS_NO = l_orig_machine_trans_no
                   AND SOURCE_SYSTEM_CD = NVL(l_orig_source_system_cd, l_source_system_cd);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with MACHINE_TRANS_NUM = "'||l_orig_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
        
        --get currency id from currency code
        IF l_currency_code IS NOT NULL THEN
        	BEGIN
        		SELECT currency_id
        		  INTO l_currency_id
        		  FROM CORP.currency
        		 WHERE currency_code = l_currency_code;
        	EXCEPTION
        		WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'No currency is mapped in the'
						|| ' system for currency code ''' || l_currency_code
						|| '''. A transaction can not be added with no currency'
						|| ' identified. Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
        	END;
        ELSE
        	RAISE_APPLICATION_ERROR(-20880, 'A transaction can not be entered '
				|| 'without a currency identified. Transaction was NOT added.');
        END IF;
		
		IF l_card_name IS NOT NULL THEN
			SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
			INTO l_cardtype_authority_id
			FROM REPORT.CARDTYPE_AUTHORITY CA
			JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
			WHERE CT.CARD_NAME = l_card_name;
			
			IF l_cardtype_authority_id IS NULL THEN
				SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
				INTO l_cardtype_authority_id
				FROM REPORT.CARDTYPE_AUTHORITY CA
				JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
				JOIN REPORT.TRANS_TYPE TT ON CT.CARD_NAME = TT.TRANS_TYPE_NAME
				WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
			END IF;
		END IF;
        
        IF l_trans_type_id IN(20,21) THEN -- make it negative
            l_real_amount := -ABS(l_total_amount);
        ELSE
            l_real_amount := ABS(l_total_amount);
        END IF;
        SELECT TRANS_SEQ.NEXTVAL
          INTO l_tran_id
          FROM DUAL;
        INSERT INTO TRANS(
            TRAN_ID,
            MACHINE_TRANS_NO,
            SOURCE_SYSTEM_CD,
            EPORT_ID,
			CARDTYPE_AUTHORITY_ID,
            START_DATE,
            CLOSE_DATE,
            TRANS_TYPE_ID,
            TOTAL_AMOUNT,
            CARD_NUMBER,
            SERVER_DATE,
            SETTLE_STATE_ID,
            SETTLE_DATE,
            PREAUTH_AMOUNT,
            PREAUTH_DATE,
            CC_APPR_CODE,
            MERCHANT_ID,
            DESCRIPTION,
            ORIG_TRAN_ID,
			CURRENCY_ID)
          SELECT
            l_tran_id,
            l_machine_trans_no,
            l_source_system_cd,
            l_eport_id,
			l_cardtype_authority_id,
            l_start_date,
            l_close_date,
            l_trans_type_id,
            l_real_amount,
            l_masked_card_number,
            l_received_date,
            l_settle_state_id,
            l_settle_date,
            l_preauth_amount,
            l_preauth_date,
            l_approval_cd,
            l_merchant_id,
            l_description,
            l_orig_tran_id,
            l_currency_id
          FROM DUAL;
          IF l_trans_type_id <> 22 AND l_hashed_card_number IS NOT NULL THEN
              INSERT INTO TRANS_C(
                TRAN_ID,
                MASKED_CARD_NUMBER,
                HASHED_CARD_NUMBER,
                CLOSE_DATE)
              VALUES(
                l_tran_id,
                l_masked_card_number,
                l_hashed_card_number,
                l_close_date);
          END IF;
    END;

    PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PLS_INTEGER DEFAULT 1,
        l_price PURCHASE.AMOUNT%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL)
    IS
    BEGIN
        --FOR i IN 1..l_quantity LOOP
            INSERT INTO PURCHASE(
                PURCHASE_ID,
                TRAN_ID,
                TRAN_DATE,
                AMOUNT,
                PRICE,
                VEND_COLUMN,
                DESCRIPTION,
                MDB_NUMBER)
              SELECT
                PURCHASE_SEQ.NEXTVAL,
                TRAN_ID,
                CLOSE_DATE,
                l_quantity,
                l_price,
                l_column_label,
                l_product_desc,
                l_column_num
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            IF SQL%ROWCOUNT = 0 THEN
                RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Item was NOT added.');
            END IF;
--        END LOOP;
    END;
    
    /* This procedure allows external systems to update the settlement info
     * of a transaction.
     *
     */
    PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE)
    IS
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        ln_tran_id REPORT.TRANS.TRAN_ID%TYPE;
    BEGIN
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        UPDATE TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID) =
            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
                    l_settle_date,
                    NVL(l_approval_cd, T.CC_APPR_CODE),
                    NVL(l_merchant_id, T.MERCHANT_ID)
              FROM DUAL)
         WHERE T.MACHINE_TRANS_NO = l_machine_trans_no
           AND T.SOURCE_SYSTEM_CD = l_source_system_cd
         RETURNING TRAN_ID INTO ln_tran_id;
        IF SQL%ROWCOUNT = 0 THEN
            RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Settle info was NOT updated.');
        END IF;
        REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(ln_tran_id, l_settle_state_id, l_settle_date, l_approval_cd);
    END;
    
    /* This procedure allows external systems to update the device info
     * of their devices
     */
    PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_ID = l_device_id;
    END;
    
    PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE)
    IS
       l_prev_fill_date FILL.FILL_DATE%TYPE;
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
  	    INSERT INTO FILL (FILL_ID, EPORT_ID, FILL_DATE)
 	         VALUES(FILL_SEQ.NEXTVAL, l_eport_id, l_fill_date);
    END;
    /* need to change alerts to relate to devices not terminals
    PROCEDURE ADD_ALERT(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_alert_id ALERT.ALERT_ID%TYPE,
       l_alert_date TERMINAL_ALERT.ALERT_DATE%TYPE,
       l_details TERMINAL_ALERT.DETAILS%TYPE)
    IS
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
    	 SELECT TERMINAL_ALERT_SEQ.NEXTVAL INTO l_id FROM DUAL;
    	 INSERT INTO TERMINAL_ALERT (TERMINAL_ALERT_ID, ALERT_ID, TERMINAL_ID, ALERT_DATE, DETAILS, RESPONSE_SENT )
     		VALUES(l_id,l_alert_id,l_terminal_id,l_alert_date,l_details, l_sent);
    END;
    */
    /*
    Right now Legacy G4 and BEX do not have any additional refund information (PROBLEM_DATE is the TRAN_DATE, REFUND_STATUS is always 1)
    */
    
    PROCEDURE UPDATE_POS_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_effective_date TERMINAL_EPORT.START_DATE%TYPE)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        /*UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_SERIAL_NUM = l_device_serial;*/
    END;

    /* Creates a customer, location and terminal, if necessary for the given device
     * This should only be used by BEX machines because they are currently not
     * configured in the Customer Reporting System.
     */
    PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE)
    IS
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_location_id REPORT.LOCATION.LOCATION_ID%TYPE;
        l_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
        l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
        l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
        l_te_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
        l_old_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_eport_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_serial_num;

        SELECT MAX(CUSTOMER_ID)
          INTO l_customer_id
          FROM CORP.CUSTOMER
         WHERE CUSTOMER_ALT_NAME = l_customer_abbr;

        IF l_customer_id IS NULL THEN
            SELECT CORP.CUSTOMER_SEQ.NEXTVAL
              INTO l_customer_id
              FROM DUAL;

            INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_ALT_NAME, STATUS, CREATE_BY)
	           VALUES(l_customer_id, l_customer_abbr, l_customer_abbr, 'A', 0);	
        END IF;

        SELECT MAX(LOCATION_ID)
          INTO l_location_id
          FROM LOCATION
         WHERE LOCATION_NAME = l_location_name
           AND EPORT_ID = l_eport_id;

        IF l_location_id IS NULL THEN
		  	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	  		 INSERT INTO LOCATION(LOCATION_ID, TERMINAL_ID, LOCATION_NAME, CREATE_BY, STATUS, EPORT_ID)
	  		     VALUES(l_location_id, 0, l_location_name, 0, 'A', l_eport_id);
        END IF;

        SELECT MAX(TE.TERMINAL_ID)
          INTO l_terminal_id
          FROM TERMINAL_EPORT TE, TERMINAL T
         WHERE TE.TERMINAL_ID = T.TERMINAL_ID
           AND TE.EPORT_ID = l_eport_id
           AND T.CUSTOMER_ID = l_customer_id
           AND T.LOCATION_ID = l_location_id;

        IF l_terminal_id IS NULL THEN
            --customer_id or location_id changed, or its new
            SELECT MAX(T.TERMINAL_NBR)
              INTO l_terminal_nbr
              FROM TERMINAL_EPORT TE, TERMINAL T
             WHERE TE.TERMINAL_ID = T.TERMINAL_ID
               AND TE.EPORT_ID = l_eport_id;
            IF l_terminal_nbr IS NULL THEN
                l_terminal_nbr := l_serial_num;
            ELSIF l_terminal_nbr = l_serial_num THEN
                l_terminal_nbr := l_serial_num || '-1';
            ELSIF l_terminal_nbr LIKE l_serial_num || '-%' THEN
                l_terminal_nbr := l_serial_num || '-' || TO_CHAR(TO_NUMBER(SUBSTR(l_terminal_nbr, INSTR(l_terminal_nbr, '-', -1) + 1)) + 1);
            ELSE
                l_terminal_nbr := l_serial_num;
            END IF;
            SELECT TERMINAL_SEQ.NEXTVAL INTO l_terminal_id FROM DUAL;
            INSERT INTO TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, EPORT_ID, CUSTOMER_ID, LOCATION_ID, BUSINESS_UNIT_ID, PAYMENT_SCHEDULE_ID)
                VALUES(l_terminal_id, l_terminal_nbr, l_serial_num, l_eport_id, l_customer_id, l_location_id, 2, 4);
            TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
        ELSE
            -- ensure terminal_eport start date is early enough
            BEGIN
                SELECT TERMINAL_EPORT_ID, START_DATE
                  INTO l_te_id, l_old_start_date
                  FROM (SELECT TERMINAL_EPORT_ID, START_DATE
                          FROM TERMINAL_EPORT
                         WHERE TERMINAL_ID = l_terminal_id
                           AND EPORT_ID = l_eport_id
                           AND NVL(END_DATE, MAX_DATE) > l_effective_date
                           ORDER BY START_DATE ASC)
                 WHERE ROWNUM = 1;
                IF l_old_start_date > l_effective_date THEN -- we need to adjust
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, l_te_id);
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- Need to add new entry
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    END;
END; 
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/UPDATER/PULL_DATA_PKG.psk?rev=1.14
CREATE OR REPLACE PACKAGE UPDATER.PULL_DATA_PKG IS
/*
    FUNCTION GET_PAYMAN_SETTLE_STATE_ID(
        l_curr_state NUMBER,
        l_deposit_amt NUMBER,
        l_time_created DATE,
        l_batch_state NUMBER)
     RETURN NUMBER;
*/
    PROCEDURE PULL_PSS_TRANS(
        l_device_type NUMBER,
        l_source_system_cd VARCHAR DEFAULT NULL, -- null means all
        l_tran_type_id NUMBER DEFAULT NULL  -- null means all
     );
     
    PROCEDURE UPDATE_SETTLED_PSS_TRANS_TMP(
        pd_latest_date DATE DEFAULT SYSDATE);

    PROCEDURE UPDATE_SETTLED_PSS_TRANS(
        pd_latest_date DATE DEFAULT SYSDATE);
    /*
    PROCEDURE UPDATE_SETTLED_PAYMAN_TRANS;
    PROCEDURE UPDATE_SETTLED_PAYMAN_REFUNDS;
    PROCEDURE UPDATE_PAYMAN_SETTLE_INFO(
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd REPORT.TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_tran_legacy_trans_no NUMBER,
        l_device_name VARCHAR2);
        */
    PROCEDURE UPDATE_PSS_SETTLE_INFO(
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd REPORT.TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_tran_id NUMBER,
        l_tran_state_cd VARCHAR2);
        
  PROCEDURE UPDATE_SETTLED_PSS_REFUNDS(
        pd_latest_date DATE DEFAULT SYSDATE);
        
  PROCEDURE PULL_PSS_TRAN_BY_ID(
        l_tran_id NUMBER
     );
END; -- Package spec 
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/UPDATER/PULL_DATA_PKG.pbk?rev=1.61
CREATE OR REPLACE PACKAGE BODY UPDATER.PULL_DATA_PKG IS
    PENDING_AP_CODE CONSTANT VARCHAR2(10) := 'Pending';
    FAILED_AP_CODE CONSTANT VARCHAR2(10) := 'Declined';
    RETRY_AP_CODE CONSTANT VARCHAR2(10) := 'Pending';
    NO_CHARGE_AP_CODE CONSTANT VARCHAR2(10) := 'No Charge';
    CANCELLED_AP_CODE CONSTANT VARCHAR2(10) := 'Cancelled';
    SETTLED_AP_CODE CONSTANT VARCHAR2(10) := 'Settled';
    
    FETCH_OUT_OF_SEQUENCE EXCEPTION;
    PRAGMA EXCEPTION_INIT(FETCH_OUT_OF_SEQUENCE, -1002);

    -- This should be for SOURCE_SYSTEM_CD = 'PSS'
    PROCEDURE UPDATE_PSS_SETTLE_INFO(
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd REPORT.TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_tran_id NUMBER,
        l_tran_state_cd VARCHAR2)
    IS
        l_apcode VARCHAR2(100);
        l_settle_state_id NUMBER;
        l_settle_date DATE;
        l_preauth_amount REPORT.TRANS.PREAUTH_AMOUNT%TYPE;
        l_preauth_date DATE;
        l_merchant_terminal_cd VARCHAR2(100);
    BEGIN
        BEGIN
            SELECT
                A.AUTH_AMT_APPROVED,
                A.AUTH_TS,
                A.AUTH_AUTHORITY_TRAN_CD,
                NULL
            INTO
                l_preauth_amount,
                l_preauth_date,
                l_apcode,
                l_merchant_terminal_cd
            FROM (SELECT
                    DECODE(a.AUTH_TYPE_CD, 'N', AUTH_AMT_APPROVED, NULL) AUTH_AMT_APPROVED,
                    DECODE(a.AUTH_TYPE_CD, 'N', AUTH_TS, NULL) AUTH_TS,
                    DECODE(a.AUTH_TYPE_CD, 'N', AUTH_AUTHORITY_TRAN_CD, 'Approved') AUTH_AUTHORITY_TRAN_CD
                  FROM
                    PSS.VW_REPORTING_AUTH_INFO@USADBP_PSS a
                  WHERE a.TRAN_ID = l_tran_id
                    AND a.AUTH_TYPE_CD IN('N', 'S', 'O')
                    AND a.AUTH_STATE_ID IN(2,5)
                  ORDER BY AUTH_TS DESC) A
           WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            MAX(SB.SETTLEMENT_BATCH_END_TS)
        INTO
            l_settle_date
        FROM PSS.VW_REPORTING_AUTH_INFO@USADBP_PSS A, PSS.TRAN_SETTLEMENT_BATCH@USADBP_PSS TSB,
             PSS.SETTLEMENT_BATCH@USADBP_PSS SB
        WHERE A.TRAN_ID = l_tran_id
          AND A.AUTH_ID = TSB.AUTH_ID
          AND TSB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID
          AND SB.SETTLEMENT_BATCH_STATE_ID = 1;
        SELECT DECODE(l_tran_state_cd,
            'D', DECODE(l_settle_date,
                    NULL, 2, -- processed
                    3 -- settled
                    ),
            'E', 5, -- failed
            'B', 4, 'I', 4, 'J', 4, 'R', 4, -- retrying
            1 -- pending
            )
          INTO l_settle_state_id
          FROM DUAL;
        SELECT DECODE(l_settle_state_id,
                1, PENDING_AP_CODE,
                2, l_apcode,
                3, l_apcode,
                4, RETRY_AP_CODE,
                5, FAILED_AP_CODE,
                PENDING_AP_CODE)
          INTO l_apcode
          FROM DUAL;
        REPORT.DATA_IN_PKG.UPDATE_SETTLE_INFO(
            l_machine_trans_no,
            l_source_system_cd,
            l_settle_state_id,
            l_settle_date,
            l_apcode,
            l_merchant_terminal_cd);
    END;

    PROCEDURE UPDATE_PSS_REFUND_SETTLE_INFO(
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd REPORT.TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_tran_id NUMBER,
        l_tran_state_cd VARCHAR2)
    IS
        l_settle_state_id NUMBER;
        l_settle_date DATE;
        l_apcode VARCHAR2(100);
    BEGIN
        SELECT
            MAX(SB.SETTLEMENT_BATCH_END_TS), MAX(R.REFUND_AUTHORITY_TRAN_CD)
        INTO
            l_settle_date, l_apcode
        FROM PSS.VW_REPORTING_REFUND_INFO@USADBP_PSS R, PSS.REFUND_SETTLEMENT_BATCH@USADBP_PSS RSB,
             PSS.SETTLEMENT_BATCH@USADBP_PSS SB
        WHERE R.TRAN_ID = l_tran_id
          AND R.REFUND_ID = RSB.REFUND_ID
          AND RSB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID
          AND SB.SETTLEMENT_BATCH_STATE_ID = 1;
        SELECT DECODE(l_tran_state_cd,
            'D', DECODE(l_settle_date,
                    NULL, 2, -- processed
                    3 -- settled
                    ),
            'E', 5, -- failed
            'B', 4, 'I', 4, 'J', 4, 'R', 4, -- retrying
            1 -- pending
            )
          INTO l_settle_state_id
          FROM DUAL;
        SELECT DECODE(l_settle_state_id,
                1, PENDING_AP_CODE,
                2, NVL(l_apcode, SETTLED_AP_CODE),
                3, NVL(l_apcode, SETTLED_AP_CODE),
                4, RETRY_AP_CODE,
                5, FAILED_AP_CODE,
                PENDING_AP_CODE)
          INTO l_apcode
          FROM DUAL;
        REPORT.DATA_IN_PKG.UPDATE_SETTLE_INFO(
            l_machine_trans_no,
            l_source_system_cd,
            l_settle_state_id,
            l_settle_date,
            l_apcode,
            NULL);
    END;

    PROCEDURE UPDATE_SETTLED_PSS_TRANS(
        pd_latest_date DATE DEFAULT SYSDATE)
    IS
        CURSOR l_cur(l_last_mtn REPORT.TRANS.MACHINE_TRANS_NO%TYPE, l_latest_date DATE) IS
           SELECT * FROM (
               SELECT /*+FIRST_ROWS*/
                    X.MACHINE_TRANS_NO,
                    X.SOURCE_SYSTEM_CD,
                    X.SETTLE_STATE_ID
                 FROM REPORT.TRANS X
                WHERE X.SOURCE_SYSTEM_CD = 'PSS'
                  AND X.SETTLE_STATE_ID IN(1,4)
                  AND X.MACHINE_TRANS_NO > l_last_mtn
                  AND X.SERVER_DATE <= l_latest_date
              ORDER BY x.MACHINE_TRANS_NO)
              WHERE ROWNUM < 500;
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_tran_id NUMBER;
        l_tran_state_cd CHAR(1);
        l_last_mtn REPORT.TRANS.MACHINE_TRANS_NO%TYPE := ' ';
    BEGIN
        LOOP
            OPEN l_cur(l_last_mtn, pd_latest_date);
            FETCH l_cur BULK COLLECT INTO l_recs;
            CLOSE l_cur;
            IF l_recs.FIRST IS NOT NULL THEN
                l_last_mtn := l_recs(l_recs.LAST).MACHINE_TRANS_NO;
                FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                    BEGIN
						SELECT TRAN_ID, TRAN_STATE_CD
						  INTO l_tran_id, l_tran_state_cd
						FROM (
						  SELECT T.TRAN_ID, T.TRAN_STATE_CD
                          FROM PSS.VW_TRAN_SAFE@USADBP_PSS T
                          WHERE T.TRAN_GLOBAL_TRANS_CD = l_recs(i).MACHINE_TRANS_NO
						  ORDER BY T.TRAN_STATE_CD DESC, T.TRAN_UPLOAD_TS
						 ) WHERE ROWNUM = 1;
                        IF l_tran_state_cd IN('D', 'E', 'B', 'I', 'J', 'R')
                          AND (l_recs(i).SETTLE_STATE_ID = 1 OR l_tran_state_cd IN('D', 'E')) THEN
                                REPORT.LOG_MSG('TRACE','UPDATE_SETTLED_PSS_TRANS','UPDATING ''' || l_recs(i).machine_trans_no || '''', 'Updating transaction because Trans State Cd = ' || l_tran_state_cd, NULL);
                                UPDATE_PSS_SETTLE_INFO(
                                    l_recs(i).MACHINE_TRANS_NO,
                                    l_recs(i).SOURCE_SYSTEM_CD,
                                    l_tran_id,
                                    l_tran_state_cd);
                                COMMIT;

                        END IF;
                    EXCEPTION
                        WHEN OTHERS THEN
                            DECLARE
                                l_msg VARCHAR(4000) := SQLERRM;
                                l_err_num NUMBER := SQLCODE;
                            BEGIN
                                ROLLBACK;
                                REPORT.LOG_MSG('ERROR','UPDATE_SETTLED_PSS_TRANS','UPDATING ''' || l_recs(i).machine_trans_no || '''', l_msg, l_err_num);
                            END;
                    END;
                END LOOP;
            ELSE
                EXIT;
            END IF;
        END LOOP;
    END;

    PROCEDURE UPDATE_SETTLED_PSS_TRANS_TMP(
        pd_latest_date DATE DEFAULT SYSDATE)
    IS
        CURSOR l_cur IS
           SELECT * FROM UPDATER.TMP_UNSETTLED_TRAN;
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_tran_id NUMBER;
        l_tran_state_cd CHAR(1);
    BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE UPDATER.TMP_UNSETTLED_TRAN';
        INSERT INTO UPDATER.TMP_UNSETTLED_TRAN(MACHINE_TRANS_NO, SOURCE_SYSTEM_CD, SETTLE_STATE_ID)
          SELECT /*+ INDEX(x USAT_IX_TRANS_SETTLE_ST_ID) */
                X.MACHINE_TRANS_NO,
                X.SOURCE_SYSTEM_CD,
                X.SETTLE_STATE_ID
             FROM REPORT.TRANS X
            WHERE X.SOURCE_SYSTEM_CD = 'PSS'
              AND X.SETTLE_STATE_ID IN(1,4)
              AND X.SERVER_DATE <= pd_latest_date;
       COMMIT;
       OPEN l_cur;
       BEGIN
           DBMS_OUTPUT.PUT_LINE('OPENED CURSOR');
           LOOP
              FETCH l_cur BULK COLLECT INTO l_recs LIMIT 500;
              DBMS_OUTPUT.PUT_LINE('FETCHED ' || to_char(l_cur%ROWCOUNT) || ' ROWS ('|| to_char(l_recs.COUNT)||' RECORDS)');
              EXIT WHEN l_recs.COUNT < 1;
              FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                BEGIN
                    --DBMS_OUTPUT.PUT_LINE('PROCESS TRAN ' || l_recs(i).MACHINE_TRANS_NO);
                    SELECT TRAN_ID, TRAN_STATE_CD
                      INTO l_tran_id, l_tran_state_cd
					FROM (
					SELECT T.TRAN_ID, T.TRAN_STATE_CD
                      FROM PSS.VW_TRAN_SAFE@USADBP_PSS T
                     WHERE T.TRAN_GLOBAL_TRANS_CD = l_recs(i).MACHINE_TRANS_NO
					 ORDER BY T.TRAN_STATE_CD DESC, T.TRAN_UPLOAD_TS
					 ) WHERE ROWNUM = 1;
                    IF l_tran_state_cd IN('D', 'E', 'B', 'I', 'J', 'R')
                      AND (l_recs(i).SETTLE_STATE_ID = 1 OR l_tran_state_cd IN('D', 'E')) THEN
                            REPORT.LOG_MSG('TRACE','UPDATE_SETTLED_PSS_TRANS','UPDATING ''' || l_recs(i).machine_trans_no || '''', 'Updating transaction because Trans State Cd = ' || l_tran_state_cd, NULL);
                            UPDATE_PSS_SETTLE_INFO(
                                l_recs(i).MACHINE_TRANS_NO,
                                l_recs(i).SOURCE_SYSTEM_CD,
                                l_tran_id,
                                l_tran_state_cd);
                            COMMIT;

                    END IF;
                EXCEPTION
                    WHEN OTHERS THEN
                        DECLARE
                            l_msg VARCHAR(4000) := SQLERRM;
                            l_err_num NUMBER := SQLCODE;
                        BEGIN
                            ROLLBACK;
                            REPORT.LOG_MSG('ERROR','UPDATE_SETTLED_PSS_TRANS','UPDATING ''' || l_recs(i).machine_trans_no || '''', l_msg, l_err_num);
                        END;
                END;
                END LOOP;
                DBMS_OUTPUT.PUT_LINE('PROCESSED ALL ROWS LOOPING AGAIN');          
           END LOOP;
       EXCEPTION
          WHEN FETCH_OUT_OF_SEQUENCE THEN
              DBMS_OUTPUT.PUT_LINE('NO MORE ROWS (FETCH OUT OF SEQUENCE)');          
              NULL;
          WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE('CLOSING CURSOR');          
              CLOSE l_cur;
              RAISE;
       END;
       DBMS_OUTPUT.PUT_LINE('CLOSING CURSOR');          
       CLOSE l_cur;
       EXECUTE IMMEDIATE 'TRUNCATE TABLE UPDATER.TMP_UNSETTLED_TRAN';
    END;
    
    PROCEDURE UPDATE_SETTLED_PSS_REFUNDS(
        pd_latest_date DATE DEFAULT SYSDATE)
    IS
        CURSOR l_cur IS
           SELECT /*+ index(T UIX_TRAN_TRAN_GLOBAL_TRANS_CD) */ 
                X.MACHINE_TRANS_NO,
                X.SOURCE_SYSTEM_CD,
                T.TRAN_ID,
                T.TRAN_STATE_CD,
                X.TRAN_ID REPORT_TRAN_ID
             FROM REPORT.TRANS X, REPORT.TRANS O, PSS.VW_TRAN_SAFE@USADBP_PSS T
            WHERE X.ORIG_TRAN_ID = O.TRAN_ID
              AND X.MACHINE_TRANS_NO = T.TRAN_GLOBAL_TRANS_CD
              AND X.SOURCE_SYSTEM_CD = 'RA'
              AND O.SOURCE_SYSTEM_CD = 'PSS'
              AND X.SETTLE_STATE_ID IN(1,4)
              AND T.TRAN_STATE_CD IN('D', 'E', 'B', 'I', 'J', 'R')
              AND (X.SETTLE_STATE_ID = 1 OR T.TRAN_STATE_CD IN('D', 'E'))
              AND X.SERVER_DATE <= pd_latest_date;
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
    BEGIN
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                BEGIN
                    UPDATE_PSS_REFUND_SETTLE_INFO(
                        l_recs(i).MACHINE_TRANS_NO,
                        l_recs(i).SOURCE_SYSTEM_CD,
                        l_recs(i).TRAN_ID,
                        l_recs(i).TRAN_STATE_CD);
                    -- If the refund is in pss, it obviously was successfully propagated
                    UPDATE CORP.REFUND SET PROCESSED_FLAG = 'Y'
                      WHERE TRAN_ID = l_recs(i).REPORT_TRAN_ID;
                    COMMIT;
                EXCEPTION
                    WHEN OTHERS THEN
                        DECLARE
                            l_msg VARCHAR(4000) := SQLERRM;
                            l_err_num NUMBER := SQLCODE;
                        BEGIN
                            ROLLBACK;
                            REPORT.LOG_MSG('ERROR','UPDATE_SETTLED_PSS_TRANS','UPDATING ''' || l_recs(i).machine_trans_no || '''', l_msg, l_err_num);
                        END;
                END;
            END LOOP;
        END IF;
    END;

    PROCEDURE PULL_PSS_TRAN_BY_ID(
        l_tran_id NUMBER
     )
    IS
        CURSOR l_cur IS
           SELECT
                T.TRAN_ID,
                T.TRAN_STATE_CD,
                T.TRAN_GLOBAL_TRANS_CD MACHINE_TRANS_NO,
                T.TRAN_LEGACY_TRANS_NO,
                T.DEVICE_NAME,
                DECODE(T.TRAN_LEGACY_TRANS_NO,
                	NULL, 'PSS',
                    'PSS-LEGACY') SOURCE_SYSTEM_CD,
                T.DEVICE_SERIAL_CD,
                T.TRAN_START_TS START_DATE,
                NVL(T.TRAN_END_TS, T.TRAN_START_TS) CLOSE_DATE,
                T.TRANS_TYPE_ID,
                SUM(TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY) TOTAL_AMOUNT,
                t.CARD_NUMBER,
		        t.TRAN_PARSED_ACCT_NUM_H CARD_NUMBER_HASH,
			    T.TRAN_UPLOAD_TS RECEIVED_DATE,
                DECODE(T.TRANS_TYPE_ID,
                    16, 1, /*  pending */
                    18, 2, /* processed*/
                    17, DECODE(T.TRAN_LEGACY_TRANS_NO, NULL, 1, 2), /* legacy => processed, access => pending */
                    22, 2, /* processed*/
                    1) SETTLE_STATE_ID,
                NULL MERCHANT_TERMINAL_CD,
                NULL DESCRIPTION,
                T.CURRENCY_CD,
                T.DEVICE_TYPE_ID
             FROM PSS.VW_REPORTING_TRAN_INFO@USADBP_PSS T
			 JOIN PSS.TRAN_LINE_ITEM@USADBP_PSS TLI ON T.TRAN_ID = TLI.TRAN_ID
            WHERE T.TRAN_ID = l_tran_id
              AND TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
		      AND NOT(TLI.TRAN_LINE_ITEM_AMOUNT = 0 /*See Bugzilla #497*/
		              AND TLI.TRAN_LINE_ITEM_POSITION_CD = '00'
		              AND TLI.TRAN_LINE_ITEM_TYPE_ID = 200
		              AND T.DEVICE_TYPE_ID = 6)
            GROUP BY T.TRAN_ID,
                T.TRAN_STATE_CD,
                T.TRAN_GLOBAL_TRANS_CD,
                T.TRAN_LEGACY_TRANS_NO,
                T.DEVICE_NAME,
                T.DEVICE_SERIAL_CD,
                T.TRAN_START_TS,
                T.TRAN_END_TS,
                T.TRANS_TYPE_ID,
                T.CARD_NUMBER,
		        T.TRAN_PARSED_ACCT_NUM_H,
			    T.TRAN_UPLOAD_TS,
                T.TRANS_TYPE_ID,
                T.CURRENCY_CD,
                T.DEVICE_TYPE_ID;
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
    BEGIN
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;
        
        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP -- there should only be one row
                REPORT.DATA_IN_PKG.ADD_TRANSACTION(
                    l_recs(i).machine_trans_no,
                    l_recs(i).source_system_cd,
                    l_recs(i).device_serial_cd,
                    l_recs(i).start_date,
                    l_recs(i).close_date,
                    l_recs(i).trans_type_id,
                    l_recs(i).total_amount,
                    REPORT.MASK_CARD(l_recs(i).card_number, l_recs(i).trans_type_id),
                    NVL(l_recs(i).card_number_hash, REPORT.HASH_CARD(l_recs(i).card_number)),
                    l_recs(i).received_date,
                    CASE WHEN NVL(l_recs(i).total_amount, 0) = 0 THEN 2 ELSE l_recs(i).settle_state_id END, -- l_settle_state
                    CASE WHEN NVL(l_recs(i).total_amount, 0) = 0 THEN l_recs(i).close_date ELSE NULL END, -- l_settle_date
                    NULL,
                    NULL,
                    CASE WHEN NVL(l_recs(i).total_amount, 0) = 0 THEN NO_CHARGE_AP_CODE ELSE NULL END, -- ap code
                    l_recs(i).MERCHANT_TERMINAL_CD,
                    l_recs(i).description,
                    NULL, --orig_machine_trans_no,
                    NULL, --orig_source_system_cd,
                    l_recs(i).currency_cd,
                    l_recs(i).device_type_id);
                DECLARE
                    CURSOR l_item_cur IS
                        SELECT NVL(I.TRAN_LINE_ITEM_AMOUNT, 0) PRICE,
                               I.TRAN_LINE_ITEM_QUANTITY QUANTITY,
                               CASE WHEN I.TRAN_LINE_ITEM_TYPE_ID = 200 AND l_recs(i).device_type_id IN(0,1,6) THEN NULL ELSE NVL(I.TRAN_LINE_ITEM_DESC, IT.TRAN_LINE_ITEM_TYPE_DESC) END PRODUCT_DESC,
                               CASE WHEN REGEXP_LIKE(TRIM(I.TRAN_LINE_ITEM_POSITION_CD), '^[0-9]+$') THEN TO_NUMBER(TRIM(I.TRAN_LINE_ITEM_POSITION_CD))
                                    WHEN REGEXP_LIKE(TRIM(I.TRAN_LINE_ITEM_POSITION_CD), '^[0-9a-fA-F]+$') THEN TO_NUMBER(TRIM(I.TRAN_LINE_ITEM_POSITION_CD), 'FM' || LPAD('0X', LENGTH(TRIM(I.TRAN_LINE_ITEM_POSITION_CD)), '0'))
                                    ELSE NULL
                               END COLUMN_NUM,
                               CASE WHEN I.TRAN_LINE_ITEM_TYPE_ID = 200 AND l_recs(i).device_type_id IN(0,1,6) THEN NULL
                                    WHEN l_recs(i).device_type_id = 13 AND IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('S','P') THEN I.TRAN_LINE_ITEM_DESC
                                    ELSE IT.TRAN_LINE_ITEM_TYPE_DESC
                               END COLUMN_LABEL
                          FROM PSS.TRAN_LINE_ITEM@USADBP_PSS I,
                               PSS.TRAN_LINE_ITEM_TYPE@USADBP_PSS IT
                         WHERE I.TRAN_ID = l_recs(i).tran_id
                           AND I.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
                           AND I.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
                           AND IT.TRAN_LINE_ITEM_TYPE_GROUP_CD NOT IN('U', 'I')
                           AND I.TRAN_LINE_ITEM_QUANTITY > 0;        
                    TYPE t_item_rec_list IS TABLE OF l_item_cur%ROWTYPE;
                    l_item_recs t_item_rec_list;
                BEGIN
                    OPEN l_item_cur;
                    FETCH l_item_cur BULK COLLECT INTO l_item_recs;
                    CLOSE l_item_cur;

                    IF l_item_recs.FIRST IS NOT NULL THEN
                        FOR k IN l_item_recs.FIRST .. l_item_recs.LAST LOOP
                            IF l_item_recs(k).quantity > 0 THEN
                                REPORT.DATA_IN_PKG.add_tran_item(
                                    l_recs(i).machine_trans_no,
                                    l_recs(i).source_system_cd,
                                    l_item_recs(k).column_num,
                                    l_item_recs(k).column_label,
                                    l_item_recs(k).quantity,
                                    l_item_recs(k).price,
                                    l_item_recs(k).product_desc);
                            END IF;
                        END LOOP;
                    END IF;
                END;
                IF l_recs(i).SETTLE_STATE_ID = 1 AND NVL(l_recs(i).total_amount, 0) <> 0 THEN
                    IF l_recs(i).SOURCE_SYSTEM_CD = 'PSS' THEN
                        UPDATE_PSS_SETTLE_INFO(
                            l_recs(i).machine_trans_no,
                            l_recs(i).source_system_cd,
                            l_recs(i).tran_id,
                            l_recs(i).tran_state_cd);
                    ELSIF l_recs(i).SOURCE_SYSTEM_CD = 'PSS-LEGACY' THEN
                        RAISE_APPLICATION_ERROR(-20700, 'Payman Transactions no longer supported');/*
                        UPDATE_PAYMAN_SETTLE_INFO(
                            l_recs(i).machine_trans_no,
                            l_recs(i).source_system_cd,
                            l_recs(i).tran_legacy_trans_no,
                            l_recs(i).device_name);*/
                    END IF;
                END IF;
            END LOOP;
        END IF;
    END;

    PROCEDURE PULL_PSS_TRANS(
        l_device_type NUMBER,
        l_source_system_cd VARCHAR, -- null means all,
        l_tran_type_id NUMBER, -- null means all
        l_last_upload_ts DATE,
        l_last_tran_id NUMBER
     )
    IS
        CURSOR l_cur IS
           SELECT
                T.TRAN_ID,
                T.TRAN_UPLOAD_TS RECEIVED_DATE,
                T.TRAN_GLOBAL_TRANS_CD MACHINE_TRANS_NO
             FROM PSS.VW_REPORTING_TRAN_INFO@USADBP_PSS T
            WHERE T.TRAN_UPLOAD_TS < SYSDATE - (1/1400)
              AND T.DEVICE_TYPE_ID = l_device_type
              AND (l_tran_type_id IS NULL OR T.TRANS_TYPE_ID = l_tran_type_id)
              AND DECODE(T.TRAN_LEGACY_TRANS_NO, NULL, 'PSS', 'PSS-LEGACY')
                    = NVL(l_source_system_cd, DECODE(T.TRAN_LEGACY_TRANS_NO, NULL, 'PSS', 'PSS-LEGACY'))
              -- the follow line helps oracle be more efficient
              AND T.TRAN_UPLOAD_TS >= l_last_upload_ts
              AND (T.TRAN_UPLOAD_TS > l_last_upload_ts
                  OR (T.TRAN_UPLOAD_TS = l_last_upload_ts
                        AND T.TRAN_ID > l_last_tran_id))
            ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC;
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
    BEGIN
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        REPORT.LOG_MSG('TRACE','PULL_PSS_TRANS','PROCESSING', 'INSERTING Trans #: ''' || l_recs(i).machine_trans_no || ''' - Begin');
                BEGIN
                    PULL_PSS_TRAN_BY_ID(l_recs(i).TRAN_ID);
                    UPDATE LAST_PROCESSED
                       SET LAST_PROCESSED_TS = l_recs(i).RECEIVED_DATE,
                           LAST_PROCESSED_ID = l_recs(i).TRAN_ID
                     WHERE PROCESS_NAME = 'PULL_PSS_TRANS:' || TO_CHAR(l_device_type) || DECODE(l_tran_type_id, NULL, '', ':' || TO_CHAR(l_tran_type_id));
                    COMMIT;
                    REPORT.LOG_MSG('TRACE','PULL_PSS_TRANS','PROCESSING', 'INSERTING Trans #: ''' || l_recs(i).machine_trans_no || ''' - End');
                EXCEPTION
                    WHEN OTHERS THEN
                        DECLARE
                            l_msg VARCHAR(4000) := SQLERRM;
                            l_err_num NUMBER := SQLCODE;
                        BEGIN
                            ROLLBACK;
                            REPORT.LOG_MSG('ERROR','PULL_PSS_TRANS','INSERTING ''' || l_recs(i).machine_trans_no || '''', l_msg, l_err_num);
                        END;
                END;
            END LOOP;
        END IF;
    END;
    
    PROCEDURE PULL_PSS_TRANS(
        l_device_type NUMBER,
        l_source_system_cd VARCHAR DEFAULT NULL, -- null means all
        l_tran_type_id NUMBER DEFAULT NULL -- null means all
     )
    IS
        l_last_upload_ts DATE;
        l_last_tran_id NUMBER;
    BEGIN
       SELECT LP.LAST_PROCESSED_TS, LP.LAST_PROCESSED_ID
         INTO l_last_upload_ts, l_last_tran_id
         FROM UPDATER.LAST_PROCESSED LP
        WHERE LP.PROCESS_NAME = 'PULL_PSS_TRANS:' || TO_CHAR(l_device_type) || DECODE(l_tran_type_id, NULL, '', ':' || TO_CHAR(l_tran_type_id));
       PULL_PSS_TRANS(l_device_type, l_source_system_cd, l_tran_type_id, l_last_upload_ts, l_last_tran_id);
    END;

   -- Enter further code below as specified in the Package spec.
END; 
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/edge_implementation/R29.USARDB.1.sql?rev=HEAD
GRANT EXECUTE ON REPORT.DATA_IN_PKG TO USAT_POSM_ROLE;
--DROP TRIGGER REPORT.TRAU_TRANS_SETTLE;
DROP TRIGGER REPORT.USAT_TRAU_TRANS_SETTLE;
BEGIN
    SYS.DBMS_SCHEDULER.SET_ATTRIBUTE('"UPDATER"."PSSSETTLEUPDATER"', 'job_action', 'BEGIN
UPDATER.PULL_DATA_PKG.UPDATE_SETTLED_PSS_TRANS_TMP(SYSDATE - 1);
UPDATER.PULL_DATA_PKG.UPDATE_SETTLED_PSS_REFUNDS(SYSDATE - 1);
END;');
END;
/

INSERT INTO DBADMIN.USAT_KNOWN_APPS_HOSTS(APPS_USERNAME, HOST_NAME, HOST_IP)
SELECT DISTINCT H.APPS_USERNAME, X.NEW_HOST_NAME, X.NEW_HOST_IP
FROM DBADMIN.USAT_KNOWN_APPS_HOSTS H
JOIN (SELECT '' ORIG_HOST_NAME, '' ORIG_HOST_IP, '' NEW_HOST_NAME, '' NEW_HOST_IP FROM DUAL WHERE 1=0
UNION ALL SELECT 'eccapr01', '192.168.4.41', 'eccapr11', '192.168.4.43' FROM DUAL
UNION ALL SELECT 'eccapr01', '192.168.4.41', 'eccapr12', '192.168.4.44' FROM DUAL
UNION ALL SELECT 'usaapr21', '192.168.77.71', 'usaapr31', '192.168.77.76' FROM DUAL
UNION ALL SELECT 'usaapr22', '192.168.77.72', 'usaapr32', '192.168.77.77' FROM DUAL
UNION ALL SELECT 'usaapr23', '192.168.77.73', 'usaapr33', '192.168.77.78' FROM DUAL
UNION ALL SELECT 'usaapr24', '192.168.77.74', 'usaapr34', '192.168.77.79' FROM DUAL
) x ON H.HOST_NAME = X.ORIG_HOST_NAME AND H.HOST_IP = X.ORIG_HOST_IP
WHERE SUBSTR(APPS_USERNAME, 1, LENGTH(APPS_USERNAME) - 2) IN('APP_LAYER', 'POSM_LAYER')
AND SUBSTR(APPS_USERNAME, LENGTH(APPS_USERNAME) - 1, 2) IN('_1', '_2', '_3', '_4')
AND SUBSTR(HOST_NAME, 4, 3) = 'apr'
AND NOT EXISTS(SELECT 1 FROM DBADMIN.USAT_KNOWN_APPS_HOSTS H0 
    WHERE H0.APPS_USERNAME = H.APPS_USERNAME 
      AND H0.HOST_NAME = X.NEW_HOST_NAME
      AND H0.HOST_IP = X.NEW_HOST_IP);
      
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/BVFUSION_tables_grants.sql?rev=HEAD
-- Latest values
CREATE TABLE G4OP.DEX_COUNTER 
(
  EPORT_ID NUMBER NOT NULL
, SALES_GROSS NUMBER(17,4) NOT NULL
, SALES_NET NUMBER(17,4) NOT NULL
, SALES_CASH NUMBER(17,4) NOT NULL
, SALES_CASHLESS NUMBER(17,4) NOT NULL 
, QTY NUMBER NOT NULL
, QTY_CASH NUMBER NOT NULL
, QTY_CASHLESS NUMBER NOT NULL 
, TERMINAL_ID NUMBER NOT NULL
, DEX_COUNTER_ID NUMBER NOT NULL 
, AS_OF_DEX_DATE DATE NOT NULL 
, CONSTRAINT DEX_COUNTER_PK PRIMARY KEY 
  (
    DEX_COUNTER_ID 
  )
  ENABLE 
) TABLESPACE "G4OP_DATA01";

-- Deltas
CREATE TABLE G4OP.DEX_DELTA 
(
  EPORT_ID NUMBER NOT NULL 
, SALES_GROSS NUMBER(17,4) NOT NULL 
, SALES_NET NUMBER(17,4) NOT NULL
, SALES_CASH NUMBER(17,4) NOT NULL 
, SALES_CASHLESS NUMBER(17,4) NOT NULL 
, QTY NUMBER NOT NULL 
, QTY_CASH NUMBER NOT NULL 
, QTY_CASHLESS NUMBER NOT NULL 
, DEX_FILE_ID NUMBER NOT NULL 
, TERMINAL_ID NUMBER NOT NULL 
, DEX_DELTA_ID NUMBER NOT NULL 
, CONSTRAINT DEX_DELTA_PK PRIMARY KEY 
  (
    DEX_DELTA_ID 
  ) ENABLE
) TABLESPACE "G4OP_DATA01";

CREATE INDEX G4OP.IX_DEX_DELTA_EPORT_TERMINAL ON G4OP.DEX_DELTA (EPORT_ID, TERMINAL_ID) TABLESPACE "G4OP_INDX";
CREATE INDEX G4OP.IX_DEX_COUNTER_EPORT_TERMINAL ON G4OP.DEX_COUNTER (EPORT_ID, TERMINAL_ID) TABLESPACE "G4OP_INDX";

create sequence G4OP.DEX_DELTA_SEQ MINVALUE 1 INCREMENT BY 1 ;
create sequence G4OP.DEX_COUNTER_SEQ MINVALUE 1 INCREMENT BY 1 ;

grant select on g4op.DEX_DELTA to USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY, USATECH_UPD_TRANS;
grant insert,update on g4op.DEX_DELTA to USAT_APP_LAYER_ROLE, USATECH_UPD_TRANS;
grant delete on g4op.DEX_DELTA to USATECH_UPD_TRANS;

grant select on g4op.dex_counter to USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY, USATECH_UPD_TRANS;
grant insert,update on g4op.dex_counter to USAT_APP_LAYER_ROLE, USATECH_UPD_TRANS;
grant delete on g4op.dex_counter to USATECH_UPD_TRANS;

grant select on report.terminal_eport to USAT_APP_LAYER_ROLE;
grant select on report.terminal to USAT_APP_LAYER_ROLE;

// For report generation (to RPT_GEN_APP via USALIVE_APP_ROLE)
grant select on g4op.DEX_DELTA to USALIVE_APP_ROLE;
grant select on g4op.dex_counter to USALIVE_APP_ROLE;
grant select on report.terminal_eport to USALIVE_APP_ROLE;
grant select on report.terminal to USALIVE_APP_ROLE;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/BVFUSION.pks?rev=HEAD
create or replace
package g4op.bvfusion as 

    type totalCurType is ref cursor return DEX_DELTA%ROWTYPE;
    
    /*
      Inserts or updates a row in the DEX_DELTA table
      Sums the existing values with the incoming values
      Sets dex file id to zero if null
    */
    procedure ACCUMULATE_DEX
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    );  
    
    /*
      Retrieves data from DEX_DELTA by terminal and eport 
      and locks the rows in the table. Caller should reset.
    */
    procedure GET_TOTALS_FOR_UPDATE
    (
        prec_totalRow OUT totalCurType,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE
    );

end bvfusion;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/BVFUSION.pkb?rev=HEAD
create or replace
package body g4op.bvfusion as

  procedure ACCUMULATE_DEX_OLD
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    ) as
    totalRow DEX_DELTA%ROWTYPE;
  begin

      begin 
        select t.* into totalRow from DEX_DELTA t 
          where t.EPORT_ID = pn_eportId and t.TERMINAL_ID = pn_terminalId 
          for update of SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS;
        
          totalRow.SALES_GROSS := totalRow.SALES_GROSS + pn_salesGross;
          totalRow.SALES_NET := totalRow.SALES_NET + pn_salesNet;
          totalRow.SALES_CASH := totalRow.SALES_CASH + pn_salesCash;
          totalRow.SALES_CASHLESS := totalRow.SALES_CASHLESS + pn_salesCashless;
          totalRow.QTY := totalRow.QTY + pn_qty;
          totalRow.QTY_CASH := totalRow.QTY_CASH + pn_qtyCash;
          totalRow.QTY_CASHLESS := totalRow.QTY_CASHLESS + pn_qtyCashless;
          
          pn_dexTotalId := totalRow.DEX_DELTA_ID;
        
          update DEX_DELTA set row = totalRow;
          
        exception 
          when NO_DATA_FOUND then
          
            select DEX_DELTA_SEQ.NEXTVAL into pn_dexTotalId from DUAL;
            
            insert into DEX_DELTA columns (DEX_DELTA_ID,EPORT_ID, TERMINAL_ID, SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS,DEX_FILE_ID)
              select pn_dexTotalId, 
                      pn_eportId, pn_terminalId, 
                      pn_salesGross, pn_salesNet, pn_salesCash, pn_salesCashless, 
                      pn_qty, pn_qtyCash, pn_qtyCashless,
                      DEX_FILE_ID 
              from DEX_FILE
              where FILE_NAME = pv_fileName and FILE_TRANSFER_ID = pn_fileTransferId; 
              
      end;     
  end ACCUMULATE_DEX_OLD;

/**
  * This procedures works with single rows of dex totals, by looking for the previous row and storing deltas
  */
  procedure ACCUMULATE_DEX_DELTA
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    ) as
    totalRow DEX_DELTA%ROWTYPE;
  begin
   
    select pn_eportId as EPORT_ID,
           SUM(t.SALES_GROSS) as SALES_GROSS,
           SUM(t.SALES_NET) as SALES_NET,
           SUM(t.SALES_CASH) as SALES_CASH,
           SUM(t.SALES_CASHLESS) as SALES_CASHLESS,
           SUM(t.QTY) as QTY,
           SUM(t.QTY_CASH) as QTY_CASH,
           SUM(t.QTY_CASHLESS) as QTY_CASHLESS,
           f.DEX_FILE_ID as DEX_FILE_ID,
           pn_terminalId as TERMINAL_ID,
           COUNT(1) as DEX_DELTA_ID
      into totalRow 
      from DEX_DELTA t, DEX_FILE f -- these are not a direct join, the second table is used for the following insert
      where t.EPORT_ID = pn_eportId and t.TERMINAL_ID = pn_terminalId
        and f.FILE_NAME = pv_fileName and f.FILE_TRANSFER_ID = pn_fileTransferId
      GROUP BY pn_eportId, f.DEX_FILE_ID, pn_terminalId;

      select DEX_DELTA_SEQ.NEXTVAL into pn_dexTotalId from DUAL;
      
      -- if no rows found, insert baseline as is 
      -- otherwise insert delta
      if totalRow.DEX_DELTA_ID > 0 THEN          
		
          totalRow.DEX_DELTA_ID := pn_dexTotalId;
          totalRow.SALES_GROSS := pn_salesGross - totalRow.SALES_GROSS;
          totalRow.SALES_NET := pn_salesNet - totalRow.SALES_NET;
          totalRow.SALES_CASH := pn_salesCash - totalRow.SALES_CASH;
          totalRow.SALES_CASHLESS := pn_salesCashless - totalRow.SALES_CASHLESS;
          totalRow.QTY := pn_qty - totalRow.QTY;
          totalRow.QTY_CASH := pn_qtyCash - totalRow.QTY_CASH;
          totalRow.QTY_CASHLESS := pn_qtyCashless - totalRow.QTY_CASHLESS;
              
      END IF;               
      
      totalRow.DEX_DELTA_ID := pn_dexTotalId;
      DBMS_OUTPUT.PUT_LINE(' new sequence number: ' || totalRow.DEX_DELTA_ID);
      insert into DEX_DELTA values totalRow;

  end ACCUMULATE_DEX_DELTA;

/**
  * This procedures works with two tables - it checks the counter table,
  * and subtracts that from the current dex, storing it in the delta table
  */
  procedure ACCUMULATE_DEX
    (
        pn_dexTotalId OUT DEX_DELTA.DEX_DELTA_ID%TYPE,
        pv_fileName IN DEX_FILE.FILE_NAME%TYPE,
        pn_fileTransferId IN DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE,
        pn_salesGross IN DEX_DELTA.SALES_GROSS%TYPE,
        pn_salesNet IN DEX_DELTA.SALES_NET%TYPE,
        pn_salesCash IN DEX_DELTA.SALES_CASH%TYPE,
        pn_salesCashless IN DEX_DELTA.SALES_CASHLESS%TYPE,
        pn_qty IN DEX_DELTA.QTY%TYPE,
        pn_qtyCash IN DEX_DELTA.QTY_CASH%TYPE,
        pn_qtyCashless IN DEX_DELTA.QTY_CASHLESS%TYPE
    ) as
    lr_counterRow DEX_COUNTER%ROWTYPE;
    ld_thisDexDate DATE;
  begin

	begin 
      select DEX_DELTA_SEQ.NEXTVAL into pn_dexTotalId from DUAL;

      select f.DEX_DATE into ld_thisDexDate 
        from G4OP.DEX_FILE f 
        where f.FILE_TRANSFER_ID = pn_fileTransferId;
        
	    select c.* into lr_counterRow from DEX_COUNTER c 
          where c.EPORT_ID = pn_eportId and c.TERMINAL_ID = pn_terminalId 
          for update of SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS,AS_OF_DEX_DATE;
                    
        if ld_thisDexDate > lr_counterRow.AS_OF_DEX_DATE then        

			insert into DEX_DELTA (
				DEX_DELTA_ID, EPORT_ID, TERMINAL_ID, DEX_FILE_ID,
				SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS
			)  select
				pn_dexTotalId, pn_eportId, pn_terminalId, f.DEX_FILE_ID,
				pn_salesGross - lr_counterRow.SALES_GROSS,
				pn_salesNet - lr_counterRow.SALES_NET,
				pn_salesCash - lr_counterRow.SALES_CASH,
				pn_salesCashless - lr_counterRow.SALES_CASHLESS,
				pn_qty - lr_counterRow.QTY,
				pn_qtyCash - lr_counterRow.QTY_CASH,
				pn_qtyCashless - lr_counterRow.QTY_CASHLESS			
	          from 	G4OP.DEX_FILE f 
	          where f.FILE_TRANSFER_ID = pn_fileTransferId;
		       
		       -- update values in counter table
	          lr_counterRow.SALES_GROSS := pn_salesGross;
	          lr_counterRow.SALES_NET := pn_salesNet;
	          lr_counterRow.SALES_CASH := pn_salesCash;
	          lr_counterRow.SALES_CASHLESS := pn_salesCashless;
	          lr_counterRow.QTY := pn_qty;
	          lr_counterRow.QTY_CASH := pn_qtyCash;
	          lr_counterRow.QTY_CASHLESS := pn_qtyCashless;
	          lr_counterRow.AS_OF_DEX_DATE := ld_thisDexDate; 
	                    
		      update DEX_COUNTER SET row = lr_counterRow WHERE DEX_COUNTER_ID = lr_counterRow.DEX_COUNTER_ID;
		       
			end if;	    
        exception 
          when NO_DATA_FOUND then
        
        	-- as before, but we insert into the counter table and put absolutes in both tables

			insert into DEX_DELTA (
				DEX_DELTA_ID, EPORT_ID, TERMINAL_ID, DEX_FILE_ID,
				SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS
			)  select pn_dexTotalId, pn_eportId, pn_terminalId, f.DEX_FILE_ID,
				pn_salesGross,
				pn_salesNet,
				pn_salesCash,
				pn_salesCashless,
				pn_qty,
				pn_qtyCash,
				pn_qtyCashless			
	          from 	G4OP.DEX_FILE f 
	          where f.FILE_TRANSFER_ID = pn_fileTransferId;
		       
		    -- insert values in counter table, too
			insert into DEX_COUNTER (
				DEX_COUNTER_ID, EPORT_ID, TERMINAL_ID, AS_OF_DEX_DATE,
				SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS
			)  values (
				DEX_COUNTER_SEQ.NEXTVAL, pn_eportId, pn_terminalId, ld_thisDexDate,
				pn_salesGross,
				pn_salesNet,
				pn_salesCash,
				pn_salesCashless,
				pn_qty,
				pn_qtyCash,
				pn_qtyCashless);	
	                    
    end;

  end ACCUMULATE_DEX;

   /*
      Retrieves data from DEX_DELTA by terminal and eport 
      and locks the rows in the table. Caller should reset.
    */
    procedure GET_TOTALS_FOR_UPDATE
    (
        prec_totalRow OUT totalCurType,
        pn_terminalId IN DEX_DELTA.TERMINAL_ID%TYPE,
        pn_eportId IN DEX_DELTA.EPORT_ID%TYPE
    ) as
  begin
  
    open prec_totalRow for
      select * from DEX_DELTA 
      where TERMINAL_ID = pn_terminalId and EPORT_ID = pn_eportId 
      for update of SALES_GROSS,SALES_NET,SALES_CASH,SALES_CASHLESS,QTY,QTY_CASH,QTY_CASHLESS,DEX_FILE_ID;
      
  end GET_TOTALS_FOR_UPDATE;

end bvfusion;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/BVFUSION_pkg_grants.sql?rev=HEAD
-- After package creation, grant execute to the user
grant execute on g4op.bvfusion to USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY, USATECH_UPD_TRANS;
grant execute on g4op.bvfusion to USALIVE_APP_ROLE;

