CREATE OR REPLACE PACKAGE DBADMIN.PKG_UTL
AS
FUNCTION EQL(o1 VARCHAR2, o2 VARCHAR2) RETURN CHAR DETERMINISTIC PARALLEL_ENABLE;
FUNCTION EQL(o1 NUMBER, o2 NUMBER) RETURN CHAR DETERMINISTIC PARALLEL_ENABLE;
FUNCTION EQL(o1 DATE, o2 DATE) RETURN CHAR DETERMINISTIC PARALLEL_ENABLE;
FUNCTION EQL(o1 TIMESTAMP, o2 TIMESTAMP) RETURN CHAR DETERMINISTIC PARALLEL_ENABLE;
FUNCTION EQL(o1 BLOB, o2 BLOB) RETURN CHAR DETERMINISTIC PARALLEL_ENABLE;
FUNCTION EQL(o1 CLOB, o2 CLOB) RETURN CHAR DETERMINISTIC PARALLEL_ENABLE;
FUNCTION EQL(o1 ANYDATA, o2 ANYDATA) RETURN CHAR;

FUNCTION GET_VARCHAR_SAFELY(pa_value ANYDATA, pv_default VARCHAR2)
RETURN VARCHAR2;
FUNCTION GET_VARCHAR_SAFELY(pa_value ANYDATA)
RETURN VARCHAR2;
END;
/
CREATE OR REPLACE PACKAGE BODY DBADMIN.PKG_UTL
IS
FUNCTION GET_VARCHAR_SAFELY(pa_value ANYDATA, pv_default VARCHAR2)
RETURN VARCHAR2
IS
    lv_typename VARCHAR2(4000);
BEGIN
    IF pa_value IS NULL THEN
        RETURN pv_default;
    END IF;

    lv_typename := LOWER(pa_value.GETTYPENAME());
    IF lv_typename IN('sys.varchar2') THEN
        RETURN NVL(pa_value.ACCESSVARCHAR2(), pv_default);
    ELSIF lv_typename IN('sys.char') THEN
        RETURN NVL(pa_value.ACCESSCHAR(), pv_default);
    ELSIF lv_typename IN('sys.varchar') THEN
        RETURN NVL(pa_value.ACCESSVARCHAR(), pv_default);
    ELSIF lv_typename IN('sys.raw') THEN
        RETURN NVL(pa_value.ACCESSRAW(), pv_default);
    ELSIF lv_typename IN('sys.nchar') THEN
        RETURN NVL(pa_value.ACCESSNCHAR(), pv_default);
    ELSIF lv_typename IN('sys.nvarchar2') THEN
        RETURN NVL(pa_value.ACCESSNVARCHAR2(), pv_default);
    ELSE
        RETURN pv_default;
    END IF;
END;

FUNCTION GET_VARCHAR_SAFELY(pa_value ANYDATA)
RETURN VARCHAR2
IS
BEGIN
    RETURN GET_VARCHAR_SAFELY(pa_value, CHR(0));
END;

FUNCTION EQL(o1 VARCHAR2, o2 VARCHAR2)
RETURN CHAR
IS
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSIF o1 = o2 THEN
        RETURN 'Y';
    ELSE
        RETURN 'N';
    END IF;
END;

FUNCTION EQL(o1 NUMBER, o2 NUMBER)
RETURN CHAR
IS
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSIF o1 = o2 THEN
        RETURN 'Y';
    ELSE
        RETURN 'N';
    END IF;
END;

FUNCTION EQL(o1 TIMESTAMP, o2 TIMESTAMP)
RETURN CHAR
IS
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSIF o1 = o2 THEN
        RETURN 'Y';
    ELSE
        RETURN 'N';
    END IF;
END;

FUNCTION EQL(o1 DATE, o2 DATE)
RETURN CHAR
IS
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSIF o1 = o2 THEN
        RETURN 'Y';
    ELSE
        RETURN 'N';
    END IF;
END;

FUNCTION EQL(o1 BLOB, o2 BLOB)
RETURN CHAR
IS
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSIF DBMS_LOB.COMPARE(o1, o2)  = 0 THEN
        RETURN 'Y';
    ELSE
        RETURN 'N';
    END IF;
END;

FUNCTION EQL(o1 CLOB, o2 CLOB)
RETURN CHAR
IS
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSIF DBMS_LOB.COMPARE(o1, o2)  = 0 THEN
        RETURN 'Y';
    ELSE
        RETURN 'N';
    END IF;
END;

FUNCTION EQL(o1 ANYDATA, o2 ANYDATA)
RETURN CHAR
IS
    lv_typename VARCHAR2(4000);
BEGIN
    IF o1 IS NULL THEN
        IF o2 IS NULL THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    ELSIF o2 IS NULL THEN
        RETURN 'N';
    ELSE
        lv_typename := LOWER(o1.GETTYPENAME());
        IF lv_typename IN('sys.varchar2') THEN
            RETURN EQL(o1.ACCESSVARCHAR2(), o2.ACCESSVARCHAR2);
        ELSIF lv_typename IN('sys.number') THEN
            RETURN EQL(o1.ACCESSNUMBER(), o2.ACCESSNUMBER);
        ELSIF lv_typename IN('sys.char') THEN
            RETURN EQL(o1.ACCESSCHAR(), o2.ACCESSCHAR);
        ELSIF lv_typename IN('sys.date') THEN
            RETURN EQL(o1.ACCESSDATE(), o2.ACCESSDATE());
        ELSIF lv_typename IN('sys.timestamp') THEN
            RETURN EQL(o1.ACCESSTIMESTAMP(), o2.ACCESSTIMESTAMP);
        ELSIF lv_typename IN('sys.varchar') THEN
            RETURN EQL(o1.ACCESSVARCHAR(), o2.ACCESSVARCHAR);
        ELSIF lv_typename IN('sys.raw') THEN
            RETURN EQL(o1.ACCESSRAW(), o2.ACCESSRAW);
        ELSIF lv_typename IN('sys.blob') THEN
            RETURN EQL(o1.ACCESSBLOB(), o2.ACCESSBLOB);
        ELSIF lv_typename IN('sys.clob') THEN
            RETURN EQL(o1.ACCESSCLOB(), o2.ACCESSCLOB);
        ELSIF lv_typename IN('sys.timestamp with tz') THEN
            RAISE_APPLICATION_ERROR(-20402, 'Type "' || lv_typename ||'" is not implemented');
        ELSIF lv_typename IN('sys.timestamp with local tz') THEN
            RAISE_APPLICATION_ERROR(-20402, 'Type "' || lv_typename ||'" is not implemented');
        ELSIF lv_typename IN('sys.binary_double') THEN
            RETURN EQL(o1.ACCESSBDOUBLE(), o2.ACCESSBDOUBLE);
        ELSIF lv_typename IN('sys.bfile') THEN
            RAISE_APPLICATION_ERROR(-20402, 'Type "' || lv_typename ||'" is not implemented');
        ELSIF lv_typename IN('sys.binary_float') THEN
            RETURN EQL(o1.ACCESSBFLOAT(), o2.ACCESSBFLOAT);
        ELSIF lv_typename IN('interval day to second') THEN
            RETURN EQL(o1.ACCESSINTERVALDS(), o2.ACCESSINTERVALDS);
        ELSIF lv_typename IN('interval year to month') THEN
            RETURN EQL(o1.ACCESSINTERVALYM(), o2.ACCESSINTERVALYM);
        ELSIF lv_typename IN('sys.nchar') THEN
            RETURN EQL(o1.ACCESSNCHAR(), o2.ACCESSNCHAR);
        ELSIF lv_typename IN('sys.nclob') THEN
            RETURN EQL(o1.ACCESSNCLOB(), o2.ACCESSNCLOB);
        ELSIF lv_typename IN('sys.nvarchar2') THEN
            RETURN EQL(o1.ACCESSNVARCHAR2(), o2.ACCESSNVARCHAR2);
        ELSIF lv_typename IN('sys.urowid') THEN
            RETURN EQL(o1.ACCESSUROWID(), o2.ACCESSUROWID);
        ELSE
            RAISE_APPLICATION_ERROR(-20402, 'Type "' || lv_typename ||'" is not implemented');
        END IF;
    END IF;
END;
END;
/
GRANT EXECUTE ON DBADMIN.PKG_UTL TO PUBLIC;
