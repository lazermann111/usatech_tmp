GRANT EXECUTE ON ENGINE.PKG_DATA_SYNC TO AUTHORITY, DEVICE, ENGINE, LOCATION, PSS, USAT_APP_LAYER_ROLE, USATECH_UPD_TRANS;
GRANT SELECT ON PSS.VW_REPORTING_TRAN_INFO_DETAIL TO USAT_APP_LAYER_ROLE;

DECLARE
	ln_cmd_pending_cnt DEVICE.CMD_PENDING_CNT%TYPE;
	ld_cmd_pending_updated_ts DEVICE.CMD_PENDING_UPDATED_TS%TYPE;

	CURSOR cur_devices IS
		SELECT DEVICE_ID, DEVICE_NAME
		FROM DEVICE.VW_DEVICE_LAST_ACTIVE
		ORDER BY LAST_ACTIVITY_TS DESC;
BEGIN
    FOR rec_device IN cur_devices LOOP
		--DBMS_OUTPUT.put_line('Processing device ' || rec_device.device_name || '...');
	
		SELECT COUNT(1), MAX(CREATED_TS)
		INTO ln_cmd_pending_cnt, ld_cmd_pending_updated_ts
		FROM ENGINE.MACHINE_CMD_PENDING
		WHERE MACHINE_ID = rec_device.device_name;
		
		UPDATE DEVICE.DEVICE
		SET cmd_pending_cnt = ln_cmd_pending_cnt,
			cmd_pending_updated_ts = NVL(ld_cmd_pending_updated_ts, cmd_pending_updated_ts)
		WHERE device_id = rec_device.device_id
			AND (cmd_pending_cnt != ln_cmd_pending_cnt
				OR cmd_pending_updated_ts != NVL(ld_cmd_pending_updated_ts, cmd_pending_updated_ts));
				
		UPDATE DEVICE.DEVICE
		SET COMM_METHOD_TS = LAST_ACTIVITY_TS
		WHERE device_id = rec_device.device_id
			AND COMM_METHOD_CD IS NOT NULL
			AND COMM_METHOD_TS IS NULL
			AND LAST_ACTIVITY_TS IS NOT NULL;
		
		COMMIT;
    END LOOP;
END;
/

UPDATE LOCATION.LOCATION
   SET LOCATION_TIME_ZONE_CD = 'EST'
 WHERE LOCATION_TIME_ZONE_CD IS NULL;
 
COMMIT;

ALTER TABLE LOCATION.LOCATION MODIFY (LOCATION_TIME_ZONE_CD NOT NULL);