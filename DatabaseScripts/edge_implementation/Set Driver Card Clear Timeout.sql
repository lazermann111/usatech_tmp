select * from device.device_setting ds
join device.device d on ds.device_id = d.device_id
where D.DEVICE_ACTIVE_YN_FLAG = 'Y'
--and D.DEVICE_NAME = 'TD000276'
and d.device_serial_cd = 'EE100000002'
and DS.DEVICE_SETTING_PARAMETER_CD = (select A.ACTION_CLEAR_PARAMETER_CD from device.action a where action_id = 7);

insert into  device.device_setting (device_id, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_value)
select d.device_id, A.ACTION_CLEAR_PARAMETER_CD, '600'
  from device.device d, device.action a
where D.DEVICE_ACTIVE_YN_FLAG = 'Y'
and d.device_serial_cd = 'EE100000002'
--and D.DEVICE_NAME = 'TD000276'
and a.action_id = 7;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER
   SET DEVICE_SETTING_UI_CONFIGURABLE = 'N'
 WHERE DEVICE_SETTING_PARAMETER_CD IN(
        SELECT ACTION_CLEAR_PARAMETER_CD 
          FROM DEVICE.ACTION 
         WHERE ACTION_CLEAR_PARAMETER_CD IS NOT NULL);