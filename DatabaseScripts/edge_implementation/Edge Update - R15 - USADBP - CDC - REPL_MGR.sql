-- EXECUTE THE FOLLOWING AS REPL_MGR:
ALTER TABLE DEVICE.EVENT ADD SUPPLEMENTAL LOG GROUP EVENT_TYPE_ID_EVENT_STATE_ID (EVENT_TYPE_ID, EVENT_STATE_ID) ALWAYS;

/*
select  *
  from dba_rules
where rule_owner in('REPL_MGR')
AND rule_name like 'EVENT%'
and rule_condition like '%''EVENT''%'; 

select * 
from all_capture c 
join dba_rule_set_rules rs on c.rule_set_name = rs.rule_set_name and c.rule_set_owner = rs.rule_set_owner
join dba_rules r on rs.rule_name = r.rule_name and rs.rule_owner = r.rule_owner
where r.rule_condition like '%''EVENT''%'; 

select * 
from all_apply a 
join dba_rule_set_rules rs on a.rule_set_name = rs.rule_set_name and a.rule_set_owner = rs.rule_set_owner
join dba_rules r on rs.rule_name = r.rule_name and rs.rule_owner = r.rule_owner
where r.rule_condition like '%''EVENT''%'; 

*/   
DECLARE
    CURSOR l_cur IS
    select r.rule_owner, r.rule_name, r.rule_condition  
      from all_capture c 
      join dba_rule_set_rules rs on c.rule_set_name = rs.rule_set_name and c.rule_set_owner = rs.rule_set_owner
      join dba_rules r on rs.rule_name = r.rule_name and rs.rule_owner = r.rule_owner
     where r.rule_condition like '%''EVENT''%';  
BEGIN
    for l_rec in l_cur loop
    DBMS_RULE_ADM.ALTER_RULE(rule_name => l_rec.rule_owner || '.' || l_rec.rule_name, condition => l_rec.rule_condition 
    || ' and :dml.get_value(''new'', ''EVENT_TYPE_ID'').AccessNumber() = 2 and :dml.get_value(''new'', ''EVENT_STATE_ID'').AccessNumber() = 2');
end loop;
END;
/
