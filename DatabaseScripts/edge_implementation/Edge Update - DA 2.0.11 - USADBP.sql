ALTER TABLE device.generic_response_s2c_action ADD (
	payload_flag CHAR(1),
	payload_default VARCHAR2(1024)
);
UPDATE device.generic_response_s2c_action SET payload_flag = 'N';
UPDATE device.generic_response_s2c_action SET payload_flag = 'Y' WHERE action_cd IN (5, 8, 10, 14);
UPDATE device.generic_response_s2c_action SET payload_default = '300' WHERE action_cd IN (5, 10);
UPDATE device.generic_response_s2c_action SET ui_visible_flag = 'Y' WHERE action_cd IN (8);
COMMIT;
ALTER TABLE device.generic_response_s2c_action MODIFY payload_flag CHAR(1) NOT NULL;

UPDATE device.device_type_property_list SET
	all_property_list_ids = '0-7|10|20-27|30-33|50-52|60-64|70|71|80|81|85-87|100-108|200-208|300|301|1001-1023|1101-1104|1200',
	configurable_property_list_ids = '0-7|10|20-27|30-33|70|71|85-87|1001-1023|1101-1104|1200'
WHERE device_type_id = 13 AND property_list_version = 1;
COMMIT;
