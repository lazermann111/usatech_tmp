package com.usatech.ftps;

/** The encryption level requested by the client
 * @author Brian S. Krug
 */
public enum EncryptType {
	 NONE, // ftps must not be used
	 SSL_IF_AVAILABLE, // ftps should be used if the server supports it 
	 /* SSL_IF_REQUIRED is not yet implemented
	 SSL_IF_REQUIRED, // ftps should be used if the server requires it 
	  */
	 SSL_REQUIRE, // ftps must be used
	 SSL_IMPLICIT // use implicit ssl
}
