package com.usatech.ftps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Base64;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import ftp.FtpBean;
import ftp.FtpException;

/** Extends FtpBean to provide ftps support. Requires Java 1.4 or higher as it uses it's built-in SSL capabilities.
 * @author Brian S. Krug
 */
public class FtpsBean extends FtpBean {
	protected final static String CMD_AUTH = "AUTH "; // (Authentication/Security Mechanism)
	protected final static String CMD_ADAT = "ADAT "; // (Authentication/Security Data)
	protected final static String CMD_PROT = "PROT "; // (Data Channel Protection Level)
	protected final static String CMD_PBSZ = "PBSZ "; // (Protection Buffer Size)
	protected final static String CMD_CCC = "CCC "; // (Clear Command Channel)
	protected final static String CMD_MIC = "MIC "; // (Integrity Protected Command)
	protected final static String CMD_CONF = "CONF "; // (Confidentiality Protected Command)
	protected final static String CMD_ENC = "ENC "; // (Privacy Protected Command)

	protected final static String REPLY_NOT_RECOGNIZED = "500";
	protected final static String REPLY_NOT_IMPLEMENTED = "502";
	protected final static String REPLY_NOT_UNDERSTOOD = "504";
	protected final static String REPLY_NOT_ACCEPTED = "534";

	protected final static ResourceBundle ftpsResourceBundle = new FtpsReplyResourceBundle();
	protected EncryptType encryptType;

	protected SSLContext sslCtx;
	protected InputStream inputStream;
	protected OutputStream outputStream;

	protected boolean hostVerifying;

	/** Creates a new FtpsBean capable of both plain ftp and ftps. Which is used depends
	 *  on the value of the <code>encryptType</code> property and the capabilities of the server.
	 *
	 */
	public FtpsBean() {
		super(ftpsResourceBundle);
		encryptType = EncryptType.SSL_IF_AVAILABLE;
	}

	@Override
	protected void setupStreams() throws IOException, FtpException {
		// Input and Output Streams are initially those from the plain socket
		inputStream = socket.getInputStream();
		outputStream = socket.getOutputStream();
		in = new BufferedReader(new InputStreamReader(new InputStream() {
			@Override
			public int read() throws IOException {
				return inputStream.read();
			}

			@Override
			public int available() throws IOException {
				return inputStream.available();
			}

			@Override
			public void close() throws IOException {
				inputStream.close();
			}

			@Override
			public void mark(int readlimit) {
				inputStream.mark(readlimit);
			}

			@Override
			public boolean markSupported() {
				return inputStream.markSupported();
			}

			@Override
			public int read(byte[] b, int off, int len) throws IOException {
				return inputStream.read(b, off, len);
			}

			@Override
			public int read(byte[] b) throws IOException {
				return inputStream.read(b);
			}

			@Override
			public void reset() throws IOException {
				inputStream.reset();
			}

			@Override
			public long skip(long n) throws IOException {
				return inputStream.skip(n);
			}
		}, FTP_ENCODING));
		out = new PrintWriter(new OutputStreamWriter(new OutputStream() {
			@Override
			public void close() throws IOException {
				outputStream.close();
			}

			@Override
			public void flush() throws IOException {
				outputStream.flush();
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				outputStream.write(b, off, len);
			}

			@Override
			public void write(byte[] b) throws IOException {
				outputStream.write(b);
			}

			@Override
			public void write(int b) throws IOException {
				outputStream.write(b);
			}
		}, FTP_ENCODING), true);

		if(encryptType == EncryptType.SSL_IMPLICIT)
			startSSL();
	}


	/** Overrides ancestor method to attempt ftps AUTH command if encryption is requested
	 * @see ftp.FtpBean#ftpLogin(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	protected void ftpLogin(String user, String password, String acct) throws IOException, FtpException {
		switch(encryptType) {
			case SSL_IF_AVAILABLE: case SSL_REQUIRE:
				ftpsAuth();
				break;
		}
		super.ftpLogin(user, password, acct);
	}

	/** Attempts the ftps AUTH command and calls startSSL() if successful.
	 * @throws IOException If an io exception occurs
	 * @throws FtpException If the server does not support ftps and this bean requires it
	 */
	protected void ftpsAuth() throws IOException, FtpException {
		// negotiate encryption
		String mechanism = "TLS";
		ftpCommand(CMD_AUTH, mechanism); // send auth method
		if(getReplyType(reply).equals(REPLY_PER_NEG)) { // server does not support ftps
			String message;
			if(reply.equals(REPLY_NOT_RECOGNIZED)) {
				message = "Server does not recognize FTPS protocol";
			} else if(reply.equals(REPLY_NOT_IMPLEMENTED)) {
				message = "Server does not implement FTPS protocol";
			} else if(reply.equals(REPLY_NOT_UNDERSTOOD)) {
				message = "Server does not understand security mechanism '" + mechanism + '\'';
			} else if(reply.equals(REPLY_NOT_ACCEPTED)) {
				message = "Server does not accept security mechanism '" + mechanism + '\'';
			} else {
				message = "Server reponded with '" + reply + "'";
			}
			switch(encryptType) {
				case SSL_REQUIRE:
					throw new FtpException("Secure transfer required and " + message);
			}
		} else if(getReplyType(reply).equals(REPLY_TRA_NEG)) {
			switch(encryptType) {
				case SSL_REQUIRE:
					throw new FtpException(
							"Secure transfer required and security mechanism is currently unavailable on the server");
			}
		} else if(getReplyType(reply).equals(REPLY_POS_INT)) {
			startSSL();
			sendProt();
		} else if(getReplyType(reply).equals(REPLY_POS_CMP)) {
			startSSL();
			sendProt();
		}
	}

	/** Creates and initializes the SSLContext object and sets the Input and Output Streams to
	 *  SSL encrypted streams. Also, issues the ftps PBSZ and PROT commands.
	 * @throws IOException
	 * @throws FtpException
	 */
	protected void startSSL() throws IOException, FtpException {
		try {
			sslCtx = SSLContext.getInstance("TLS");
			TrustManager[] tms;
			if(isHostVerifying())
				tms = null; //use system-configured TrustManager
			else
				tms = new TrustManager[] {
					new X509TrustManager() {
				        public X509Certificate[] getAcceptedIssuers() {
				            return new X509Certificate[0];
				        }
				        public void checkClientTrusted(final X509Certificate[] certs, final String authType) {
				        	logStatus(new Object() { public String toString() { return "Checking client trusted for auth type '" + authType + "' on cerificates:\n" + Arrays.toString(certs); }});
				        }
				        public void checkServerTrusted(final X509Certificate[] certs, final String authType) {
				        	logStatus(new Object() { 
			        			public String toString() { 
					        		StringBuilder sb = new StringBuilder();
					        		sb.append("Checking server trusted for auth type '").append(authType).append("' on cerificates:");
					        		for(X509Certificate cert : certs) {
					        			sb.append('\n').append(cert).append("\n========================================\n");
					        			sb.append("-----BEGIN CERTIFICATE-----\n");
					        			try {
											//sb.append(com.sun.org.apache.xml.internal.security.utils.Base64.encode(cert.getEncoded()));
											sb.append(Base64.getMimeEncoder().encodeToString(cert.getEncoded()));
										} catch(CertificateEncodingException e) {
											StringWriter sw = new StringWriter();
											e.printStackTrace(new PrintWriter(sw));
											sb.append(sw.getBuffer());
										}
					        			sb.append("\n-----END CERTIFICATE-----");
					        		}
					        		return sb.toString();
					        	}
				        	});
				        }
				    }
				};
			sslCtx.init(null, tms, null);
		} catch(NoSuchAlgorithmException e) {
			FtpException ftpe = new FtpException("No such algorithm");
			ftpe.initCause(e);
			throw ftpe;
		} catch(KeyManagementException e) {
			FtpException ftpe = new FtpException("Key Problem");
			ftpe.initCause(e);
			throw ftpe;
		}
		Socket sslSocket = sslCtx.getSocketFactory().createSocket(socket, server, port, true);
		inputStream = sslSocket.getInputStream();
		outputStream = sslSocket.getOutputStream();
		socketCreated(sslSocket);
	}

	protected void sendProt() throws IOException, FtpException {
		//do pbsz and prot
		ftpCommand(CMD_PBSZ, "0");
		ftpCommand(CMD_PROT, "P");
	
	}
	/** Overrides ancestor method to create an SSLServerSocket when using ftps
	 * @see ftp.FtpBean#getActiveDataSocket()
	 */
	@Override
	protected ServerSocket getActiveDataSocket() throws IOException, FtpException {
		if(sslCtx != null) {
			// unfortunately SSLServerSocket cannot wrap a ServerSocket like SSLSocket does for Socket
			int[] port_numbers = new int[6];            // Array that contains

	        // Get ip address of local machine. ip address and port numbers
	        String local_address = socket.getLocalAddress().getHostAddress();

	        // Assign the ip address of local machine to the array.
	        StringTokenizer st = new StringTokenizer(local_address, ".");
	        for(int i = 0; i < 4; i++)
	            port_numbers[i] = Integer.parseInt(st.nextToken());

	        ServerSocket ssocket = sslCtx.getServerSocketFactory().createServerSocket(0); // ServerSocket to listen to a random free port number
	        int local_port = ssocket.getLocalPort();     // The port number it is listenning to

	        // Assign port numbers the array
	        port_numbers[4] = ((local_port & 0xff00) >> 8);
	        port_numbers[5] = (local_port & 0x00ff);

	        // Send "PORT" command to server
	        String port_param = "";
	        for(int i = 0; i < port_numbers.length; i++)
	        {
	            port_param = port_param.concat(String.valueOf(port_numbers[i]));
	            if(i < port_numbers.length - 1)
	                port_param = port_param.concat(",");
	        }
	        ftpCommand(CMD_PORT, port_param);

	        return ssocket;
		}
		return super.getActiveDataSocket();
	}

	@Override
	protected void socketCreated(Socket sock) {
		/* NOTE: For some reason calling getSession() causes the connection to hang trying to do the SSL Handshake
		if(sock instanceof SSLSocket) {
			SSLSession ss = ((SSLSocket) sock).getSession();
			logStatus(new StringBuilder().append("Created SSL Socket to ").append(sock.getInetAddress()).append(':').append(sock.getPort()).append(" using ").append(ss.getProtocol()).append(" with ").append(ss.getCipherSuite()));
		}
		*/
	}
	/** Overrides ancestor method to wrap the plain socket in an SSLSocket when using ftps
	 * @see ftp.FtpBean#getPassiveDataSocket()
	 */
	@Override
	protected Socket getPassiveDataSocket() throws IOException, FtpException {
		Socket sock = super.getPassiveDataSocket();
		if(sslCtx != null) {
			sock = sslCtx.getSocketFactory().createSocket(sock, getServerName(), sock.getPort(), true);
		}
		return sock;
	}

	/**
	 * @return The encryption type
	 */
	public EncryptType getEncryptType() {
		return encryptType;
	}

	/**
	 * @param encryptType The new encryption type
	 */
	public void setEncryptType(EncryptType encryptType) {
		this.encryptType = encryptType;
	}

	/**
	 * @return Whether the remote host's certificate is verified or not
	 */
	public boolean isHostVerifying() {
		return hostVerifying;
	}

	/**
	 * @param hostVerifying If true then SSL uses the system configured TrustManager - @see javax.net.ssl.TrustManager
	 */
	public void setHostVerifying(boolean hostVerifying) {
		this.hostVerifying = hostVerifying;
	}
}
