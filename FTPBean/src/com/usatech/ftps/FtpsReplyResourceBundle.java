package com.usatech.ftps;

import java.util.ListResourceBundle;

import ftp.FtpBean;
import ftp.FtpReplyResourceBundle;


/**
 * This class is used to store the valid reply code for various ftps commands.
 * It builds upon ftp.FtpReplyResourceBundle.
 * @author Brian S. Krug
 */
public class FtpsReplyResourceBundle extends ListResourceBundle {
	
	public FtpsReplyResourceBundle() {
		super();
		setParent(new FtpReplyResourceBundle());
	}

	public Object[][] getContents() {
		return cmdGrps;
	}
	
	protected static final Object[][] cmdGrps = {
			{ FtpsBean.CMD_AUTH, new String[] { FtpBean.REPLY_POS_CMP, FtpBean.REPLY_POS_INT, FtpBean.REPLY_TRA_NEG, FtpBean.REPLY_PER_NEG } },
			{ FtpsBean.CMD_ADAT, new String[] { FtpBean.REPLY_POS_CMP } },
			{ FtpsBean.CMD_CCC, new String[] { FtpBean.REPLY_POS_CMP } },
			{ FtpsBean.CMD_CONF, new String[] { FtpBean.REPLY_POS_CMP } },
			{ FtpsBean.CMD_ENC, new String[] { FtpBean.REPLY_POS_CMP } },
			{ FtpsBean.CMD_MIC, new String[] { FtpBean.REPLY_POS_CMP } },
			{ FtpsBean.CMD_PBSZ, new String[] { FtpBean.REPLY_POS_CMP } },
			{ FtpsBean.CMD_PROT, new String[] { FtpBean.REPLY_POS_CMP } }
		};
}
