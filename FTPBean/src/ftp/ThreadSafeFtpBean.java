package ftp;

import java.io.BufferedInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/* Adapted and enhanced starting 11/15/2006. 
 * @author Brian S. Krug
 * 
 * This class just wraps each method with acquire / release to provide thread safety
 */

/*
 * FtpBean			Version 1.4.4
 * Copyright 1999 Calvin Tai
 * E-mail: calvin_tai2000@yahoo.com.hk
 * URL: http://www.geocities.com/SiliconValley/Code/9129/javabean/ftpbean
 *
 * COPYRIGHT NOTICE
 * Copyright 1999 Calvin Tai All Rights Reserved.
 *
 * FtpBean may be modified and used in any application free of charge by
 * anyone so long as this copyright notice and the comments above remain
 * intact. By using this code you agree to indemnify Calvin Tai from any
 * liability that might arise from it's use.
 *
 * Selling the code for this java bean alone is expressly forbidden.
 * In other words, please ask first before you try and make money off of
 * this java bean as a standalone application.
 *
 * Obtain permission before redistributing this software over the Internet or
 * in any other medium.  In all cases copyright and header must remain intact.
 */

/*
 * Class Name: FtpBean
 * Author: Calvin Tai
 * Date: 20 Aug 1999
 * Last Updated: 28Mar2002
 *
 * Note:
 * 1) To turn on debug mode, change the field DEBUG to true,
 *    then re-compile the class file.
 *
 * Updates:
 * version 1.4.5 - 28 Mar 2002
 * 1) Check the reply code and login procedures according to the state diagram specified in RFC959,
 *    added class FtpReplyResourceBundle.
 * 2) Added property account information (acctInfo).
 * 3) Public methods ftpConnect(String, String) and ftpConnect(String, String, String, String) added.
 * 4) Use US-ASCII encoding for both the input, output stream and String to fix the problem where
 *    some system is not using US-ASCII as the default encoding.(e.g. EBCDIC)
 * 5) Handle the string is null in method checkReply
 *
 * version 1.4.4 - 10 March 2001
 * 1) Fixed bug on the getPassiveSocket method which pass incorrect server and port information
 *    when using the SocketOpener class for openning new socket.
 * 2) Fixed bug on the getDataSocket method where the ServerSocket is not closed when using
 *    active mode transfer. This may cause problem if there are too many ServerSocket opened.
 * 3) Fixed bug on most of the get/put file methods where the setTransferType is not enclosed
 *    in the "try" block.
 * 4) Replaced those depreciated methods for multi-thread.
 *
 * version 1.4.3 - 16, Nov 2000
 * 1) Increase code efficiency for private method getBytes(BufferedInputStream, FtpObserver).
 *    This increase the of those get file methods that return a byte array or String.
 * 2) Private method getPassiveDataSocket() changed, so that it read the ip address returned
 *    from the PASV command rather than using the getServerName method as the host.
 * 3) Added the SocketOpener class to deal with add timeout feature for openning Socket.
 * 4) Changed the way to get new Socket in ftpConnect and getPassiveSocket methods to support
 *    the timeout feature if it is being set.
 * 5) Public method getSocketTimeout() added.
 * 6) Public method setSocketTimeout(int) added.
 * 7) Public method getAsciiFile(String, String, String) added.
 * 8) Public method getAsciiFile(String, String, String, FtpObserver) added.
 *
 * version 1.4.2 - 1, Aug 2000
 * 1) Updated dead lock in close() method when using putBinaryFile(String, String) to upload a
 *    non-existing local file.
 *
 * version 1.4.1 - 19, May 2000
 * 1) Fixed bug on deadlock may caused by using active transfer mode to get data connection.
 * 2) Fixed bug on deadlock may caused by using passive transfer mode to get data connection.
 * 3) Private method getDataSocket(String[]) changed to getDataSocet(String, long).
 * 4) Public method execute() added.
 * 5) Public method getSystemType() added.
 *
 * version 1.4 - 28, March 2000
 * 1) Private method aquire() changed to acquire().
 * 2) All get/put file methods changed to support the feature of FtpObserver class.
 *
 * version 1.3.1 - 1, Feb 2000
 * 1) Fixed bug on deadlock during incorrect login.
 * 2) Private method closeSocket() added.
 *
 * version 1.3 - 20, Jan 2000
 * 1) Public method getDirectoryContent() changed to getDirectoryContentAsString().
 * 2) Public method getPassiveModeTransfer() changed to isPassiveModeTransfer().
 * 3) Public method getDirectoryContent() added.
 * 4) Thread safe.
 * 5) Private method aquire added.
 * 6) Private method release added.
 *
 * version 1.2 - 5, Nov 1999
 * 1) Debug mode added.
 * 2) Public method putBinaryFile(String, String) added.
 * 3) Public method putBinaryFile(String, String, long) added.
 * 4) Public method getReply() added.
 * 5) Public method getAsciiFile(String) changed to getAsciiFile(String, String).
 * 6) Public method putAsciiFile(String, String) changed to putAsciiFile(String, String, String).
 * 7) Public method getPassive() changed to getPassiveModeTransfer().
 * 8) Public method setPassive(boolean) changed to setPassiveModeTransfer(boolean).
 *
 * version 1.1 - 21, Aug 1999
 * 1) Provide active data connection (Using PORT command).
 * 2) Public method setPassive(boolean) added.
 * 3) Public method getPassive() added.
 * 4) Fixed bug on getting file with a restarting point as an byte array.
 */

/**
 * This bean provide some basic FTP functions.<br>
 * You can use this bean in some visual development tools (eg. IBM VA)
 * or just include its files and use its methods.<br>
 * This class is thread safe, only one thread can acquire the object and do some ftp operation at a time.
 * <p>
 * <b><u>How to use it?</u></b><br>
 * To start a new instance of this FTP bean<br>
 * <font color="#0000ff"> FtpBean ftp = new FtpBean(); </font><br>
 * // To connect to a FTP server<br>
 * <font color="#0000ff"> ftp.ftpConnect("servername", "username", "password"); </font><br>
 * // Some methods return null if you call them without establish connection first. <br>
 * //Then just call the functions provided by this bean.<br>
 * //After you end the ftp section. Just close the connection.<br>
 * <font color="#0000ff"> ftp.close(); </font>
 * <p>
 * Remarks:<br>
 * 1) Whenever a <b>ftp command failed</b> (eg. Permission denied), the methods throw an FtpException.<br>
 * So you need to catch the FtpException wherever you invoke those methods of this class.<br>
 * 2) This bean use <b>passive mode</b> to establish data connection by <b>default</b>. If this cause problem from firewall of the network, try using active mode:<br>
 * ftp.setPassiveModeTransfer(false);<br>
 * 3) To turn on <b>debug mode</b>, you need to change the source of this class. Then re-compile it.<br>
 * 4) For <b>timeout on creating Socket</b>. if a timeout is being set and operation timeout, a java.io.InterruptedIOException is throw. This is the case for both passive transfer mode and
 * establishment of connection to the server at the beginning. For active transfer mode, timeout is set in the servers ftpd. If there is timeout, the servers ftp software return an error code which
 * causing the bean to throw a ftp.FtpException.
 * <p>
 * 
 * 
 * <b><u>IMPORTANT for using in an Applet:</u></b><br>
 * 1) If you use this bean in an applet and the applet is open to the public, please don't include the user name and password in the source code of your applet. As anyone who can get your class files
 * can get your user name and password. It is reasonable to ask the user for user name and password if you are going to use FTP in the applet.<br>
 * 2) If you use it in an applet, please be aware of the security restriction from the browser. As an unsigned applet can ONLY connect to the host which serves it. Also, some methods in this bean will
 * write/read to the local file system. These methods are also restricted by the browser.<br>
 * <br>
 * 
 * If you find any bugs in this bean or any comment, please give me a notice at<br>
 * <a href="mailto:calvin_tai2000@yahoo.com.hk">Calvin(calvin_tai2000@yahoo.com.hk)</a><br>
 * 
 */

public class ThreadSafeFtpBean extends FtpBean {

	// Needed for thread safety
	protected int[] lock = new int[0]; // For synchronized locking
	protected boolean acquired = false; // Acquired by a thread or not
	protected List<Thread> thread_spool = new ArrayList<Thread>(); // Spool for the waiting threads

	/**
	 * Constructor
	 */
	public ThreadSafeFtpBean() {
		super();
	}

	protected ThreadSafeFtpBean(ResourceBundle ftpReplies) {
		super(ftpReplies);
	}

	/**
	 * Connect to FTP server and login.
	 * 
	 * @param server
	 *            Name of server
	 * @param user
	 *            User name for login
	 * @param password
	 *            Password for login
	 * @param acct
	 *            account information
	 * @exception FtpException
	 *                if a ftp error occur (eg. Login fail in this case).
	 * @exception IOException
	 *                if an I/O error occur
	 */
	public void ftpConnect(String server, String user, String password, String acct) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.ftpConnect(server, user, password, acct);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Close FTP connection.
	 * 
	 * @exception IOException
	 *                if an I/O error occur
	 * @exception FtpException
	 *                if a ftp error occur
	 */
	public void close() throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.close();
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Delete a file at the FTP server.
	 * 
	 * @param filename
	 *            Name of the file to be deleted.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. no such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void fileDelete(String filename) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.fileDelete(filename);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Rename a file at the FTP server.
	 * 
	 * @param oldfilename
	 *            The name of the file to be renamed
	 * @param newfilename
	 *            The new name of the file
	 * @exception FtpException
	 *                if a ftp error occur. (eg. A file named the new file name already in this case.)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void fileRename(String oldfilename, String newfilename) throws IOException, FtpException {
		acquire(); // Acquire this object
		try {
			super.fileRename(oldfilename, newfilename);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get an ASCII file from the server and return as String.
	 * 
	 * @param filename
	 *            Name of ASCII file to be getted.
	 * @param separator
	 *            The line separator you want in the return String (eg. "\r\n", "\n", "\r").
	 * @param observer
	 *            The observer of the downloading progress
	 * @return The Ascii content of the file. It uses parameter 'separator' as the line separator.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. no such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 * @see FtpObserver
	 */
	public String getAsciiFile(String filename, String separator, FtpObserver observer) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			return super.getAsciiFile(filename, separator, observer);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get an ascii file from the server and write to local file system.
	 * 
	 * @param ftpfile
	 *            Name of ascii file in the server side.
	 * @param localfile
	 *            Name of ascii file in the local file system.
	 * @param separator
	 *            The line separator you want in the local ascii file (eg. "\r\n", "\n", "\r").
	 * @param observer
	 *            The observer of the downloading progress.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. no such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 * @see FtpObserver
	 */
	public void getAsciiFile(String ftpfile, String localfile, String separator, FtpObserver observer) throws IOException, FtpException {
		acquire();
		try {
			super.getAsciiFile(ftpfile, localfile, separator, observer);
		} finally {
			release();
		}
	}

	/**
	 * Append an ascii file to the server. <br>
	 * Remark:<br>
	 * this method convert the line separator of the String content to <br>
	 * NVT-ASCII format line separator "\r\n". Then the ftp daemon will <br>
	 * convert the NVT-ASCII format line separator into its system line separator.
	 * 
	 * @param filename
	 *            The name of file
	 * @param content
	 *            The String content of the file
	 * @param separator
	 *            Line separator of the content
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void appendAsciiFile(String filename, String content, String separator) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.appendAsciiFile(filename, content, separator);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Save an ascii file to the server. <br>
	 * Remark:<br>
	 * this method convert the line separator of the String content to <br>
	 * NVT-ASCII format line separator "\r\n". Then the ftp daemon will <br>
	 * convert the NVT-ASCII format line separator into its system line separator.
	 * 
	 * @param filename
	 *            The name of file
	 * @param content
	 *            The String content of the file
	 * @param separator
	 *            Line separator of the content
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void putAsciiFile(String filename, String content, String separator) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.putAsciiFile(filename, content, separator);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get a binary file at a restarting point.
	 * Return null if restarting point is less than zero.
	 * 
	 * @param filename
	 *            Name of binary file to be getted.
	 * @param restart
	 *            Restarting point, ignored if less than or equal to zero.
	 * @param observer
	 *            The FtpObserver which monitor this downloading progress
	 * @return An array of byte of the content of the binary file.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. No such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 * @see FtpObserver
	 */
	public byte[] getBinaryFile(String filename, long restart, FtpObserver observer) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			return super.getBinaryFile(filename, restart, observer);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Read from a ftp file and restart at a specific point.
	 * This method is much faster than those method which return a byte array<br>
	 * if the network is fast enough.<br>
	 * Remark:<br>
	 * Cannot be used in unsigned applet.
	 * 
	 * @param ftpfile
	 *            Name of file to be get from the ftp server, can be in full path.
	 * @param localfile
	 *            File name of local file
	 * @param restart
	 *            Restarting point, ignored if equal or less than zero.
	 * @param observer
	 *            The FtpObserver which monitor this downloading progress
	 * @exception FtpException
	 *                if a ftp error occur. (eg. No such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 * @see FtpObserver
	 */
	public void getBinaryFile(String ftpfile, String localfile, long restart, FtpObserver observer) throws IOException, FtpException {
		acquire(); // Acquire the object

		try {
			super.getBinaryFile(ftpfile, localfile, restart, observer);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Put a binary file to the server from an array of byte with a restarting point
	 * 
	 * @param filename
	 *            The name of file.
	 * @param content
	 *            The byte array to be write to the server.
	 * @param restart
	 *            The restarting point, ingored if less than or equal to zero.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void putBinaryFile(String filename, byte[] content, long restart) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.putBinaryFile(filename, content, restart);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Read a file from local hard disk and write to the server with restarting point.
	 * Remark:<br>
	 * Cannot be used in unsigned applet.
	 * 
	 * @param local_file
	 *            Name of local file, can be in full path.
	 * @param remote_file
	 *            Name of file in the ftp server, can be in full path.
	 * @param observer
	 *            The FtpObserver which monitor this uploading progress
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void putBinaryFile(String local_file, String remote_file, long restart, FtpObserver observer) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.putBinaryFile(local_file, remote_file, restart, observer);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Read a file from local hard disk and append to the server with restarting point.
	 * Remark:<br>
	 * Cannot be used in unsigned applet.
	 * 
	 * @param local_file
	 *            Name of local file, can be in full path.
	 * @param remote_file
	 *            Name of file in the ftp server, can be in full path.
	 * @param observer
	 *            The FtpObserver which monitor this uploading progress
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void appendBinaryFile(String local_file, String remote_file, FtpObserver observer) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.appendBinaryFile(local_file, remote_file, observer);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get current directory name.
	 * 
	 * @return The name of the current directory.
	 * @exception FtpException
	 *                if a ftp error occur.
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public String getDirectory() throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			return super.getDirectory();
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Change directory.
	 * 
	 * @param directory
	 *            Name of directory
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void setDirectory(String directory) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.setDirectory(directory);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Change to parent directory.
	 * 
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void toParentDirectory() throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.toParentDirectory();
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get the content of current directory.
	 * 
	 * @return A list of directories, files and links in the current directory.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public String getFileInfoAsString(String fileName) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			return super.getFileInfoAsString(fileName);
		} finally {
			release(); // Release the object
		}
	}


	/**
	 * Get the content of current directory.
	 * 
	 * @return A list of directories, files and links in the current directory.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public String getDirectoryContentAsString() throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			return super.getDirectoryContentAsString();
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Make a directory in the server.
	 * 
	 * @param directory
	 *            The name of directory to be made.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void makeDirectory(String directory) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.makeDirectory(directory);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Remove a directory in the server
	 * 
	 * @param directory
	 *            The name of directory to be removed
	 * @exception FtpException
	 *                if a ftp error occur. (eg. permission denied in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void removeDirectory(String directory) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.removeDirectory(directory);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Execute a command using ftp.
	 * e.g. chmod 700 file
	 * 
	 * @param exec
	 *            The command to execute.
	 * @exception FtpException
	 *                if a ftp error occur. (eg. command not understood)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void execute(String exec) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.execute(exec);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get the type of operating system of the server.
	 * Return null if it is not currently connected to any ftp server.
	 * 
	 * @return Name of the operating system.
	 */
	public String getSystemType() throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			return super.getSystemType();
		} finally {
			release(); // Release the object
		}
	}


	/**
	 * Set port number if the port number of ftp is not 21
	 */
	public void setPort(int port) {
		acquire(); // Acquire the object
		try {
			super.setPort(port);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Set timeout when creating a socket.
	 * This include trying to connect to the ftp server at the beginnig and
	 * trying to connect to the server in order to establish a data connection.
	 * 
	 * @param timeout
	 *            Timeout in milliseconds, 0 means infinity.
	 */
	public void setSocketTimeout(int timeout) {
		acquire(); // Acquire the object
		try {
			super.setSocketTimeout(timeout);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Set passive transfer mode. Default is true.
	 * 
	 * @param passive
	 *            Using passive transfer if true.
	 */
	public void setPassiveModeTransfer(boolean passive) {
		acquire(); // Acquire the object
		try {
			super.setPassiveModeTransfer(passive);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Method added to allow streaming insteading of put byte[] to write out
	 * 
	 * @param filename
	 * @param ins
	 * @param restart
	 * @throws IOException
	 * @throws FtpException
	 */
	public void putFile(String filename, InputStream ins, long restart) throws IOException, FtpException {
		acquire();
		try {
			super.putFile(filename, ins, restart);
		} finally {
			release();
		}
	}

	/**
	 * Method added to allow streaming insteading of put byte[] to write out. NOTE: the caller
	 * of this function MUST close() the returned OutputStream to clean-up resources.
	 * 
	 * @param filename
	 * @param ins
	 * @param restart
	 * @throws IOException
	 * @throws FtpException
	 */
	public OutputStream putFileStream(String filename, long restart) throws IOException, FtpException {
		acquire(); // Acquire the object
		boolean okay = false;
		try {
			final FtpConnection sock = getDataSocket(CMD_STOR, filename, restart);
			final OutputStream out = sock.getOutputStream();
			okay = true;
			// Prepare output stream to write data to server
			return new FilterOutputStream(out) {
				@Override
				public void close() throws IOException {
					try {
						super.close();
						sock.close();
						getRespond(CMD_STOR);
					} catch(FtpException e) {
						throw new IOException(e);
					} finally {
						release(); // Release the object
					}
				}
			};
		} finally {
			if(!okay)
				release();
		}
	}

	/**
	 * Get a binary file at a restarting point and write it into the provided stream.
	 * 
	 * @param filename
	 *            Name of binary file to be getted.
	 * @param restart
	 *            Restarting point, ignored if less than or equal to zero.
	 * @param out
	 *            The stream into which the file content should be written
	 * @exception FtpException
	 *                if a ftp error occur. (eg. No such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public void getFile(String filename, long restart, OutputStream out) throws IOException, FtpException {
		acquire(); // Acquire the object
		try {
			super.getFile(filename, restart, out);
		} finally {
			release(); // Release the object
		}
	}

	/**
	 * Get an inputstream of the content of a binary file at a restarting point.
	 * NOTE: The calling application MUST always call close() on the returned InputStream
	 * in order to release the lock on this FtpBean instance.
	 * 
	 * @param filename
	 *            Name of binary file to be getted.
	 * @param restart
	 *            Restarting point, ignored if less than or equal to zero.
	 * @return The stream from which the file content can be read
	 * @exception FtpException
	 *                if a ftp error occur. (eg. No such file in this case)
	 * @exception IOException
	 *                if an I/O error occur.
	 */
	public InputStream getFileStream(String filename, long restart) throws IOException, FtpException {
		acquire(); // Acquire the object
		boolean okay = false;
		try {
			setTransferType(false);
			if(this.out == null)
				return null;

			final FtpConnection sock = getDataSocket(CMD_RETR, filename, restart);
			final InputStream in = sock.getInputStream();
			okay = true;
			// Read bytes from server
			return new BufferedInputStream(in) {
				@Override
				public void close() throws IOException {
					try {
						super.close();
						sock.close();
						getRespond(CMD_RETR);
						if(!reply.substring(0, 3).equals("226")) {
							throw new FtpException(reply); // transfer incomplete
						}
					} catch(FtpException e) {
						throw new IOException(e);
					} finally {
						release(); // Release the object
					}
				}
			};
		} finally {
			if(!okay)
				release();
		}
	}


	// Methods for thread safe.
	// All of this thread safe operation are transparent to the programmers who are using this bean.
	// All threads that want to do some ftp operation must acquire this object first.
	// Then release the object after those operation or an exception is throwed.
	// When the object is acquired by a thread, other threads that want to do ftp operation,
	// will be placed in the thread_spool. Then have rights to access this thread when the
	// previous thread is done.
	// Normally, calling the acquire() and release() methods are like this:
	//
	// acquire();
	// try
	// {
	// // Do operation that may cause Exception here
	// } finally
	// {
	// release();
	// }
	//
	// This can ensure the thread will release the object even an exception is threw.

	/*
	 * Acquire this FtpBean object.
	 * If there is a thread acquired this object already. Put itself into the thread spool.
	 */
	protected void acquire() {
		Thread thread = Thread.currentThread();
		synchronized(lock) {
			thread_spool.add(thread); // Add thread to thread_spool
			logStatus("Add thread to spool, size: " + thread_spool.size());
		}
		try {
			while(acquired && !thread_spool.get(0).equals(thread))
				Thread.sleep(10);
		} catch(InterruptedException e) {
			logStatus("Thread interrupted");
			return;
		}

		acquired = true;
		logStatus("Acquired by thread.");
		/* Old locking
		synchronized(lock)
		{

		    // Loop if object is acquired by a thread
		    // or there are other threads waiting in the thread_spool
		    loop: while(acquired || thread_spool.size() > 0)
		    {
		        if(thread_spool.contains(thread))
		        {
		            if(thread_spool.elementAt(0).equals(thread) && !acquired)
		            {
		                // Object is released by previous thread
		                // And this is the first thread in the thread_spool
		                // Then break the loop.
		                thread_spool.removeElement(thread);
		                break loop;
		            } else
		                lock.notify();    // Notify other threads
		        } else
		            thread_spool.addElement(thread);    // Add thread to thread_spool
		        // Go to wait
		        try { lock.wait(); }
		        catch(Exception e) { System.out.println(e); }

		    }
		    // Acquire this object
		    acquired= true;

		    logStatus("Acquired by thread.");
		}
		*/
	}

	/*
	 * Release this FtpBean object and notify other waiting threads.
	 */
	protected void release() {
		synchronized(lock) {
			thread_spool.remove(0);
		}
		acquired = true;
		logStatus("Released by thread.");
		/*
		synchronized(lock)
		{
		    logStatus("Released by thread.");

		    // Releasject
		    acquired = false;

		    // Notify other threads
		    if(thread_spool.size() > 0)
		        lock.notify();
		    else
		        lock.notifyAll();
		}
		*/
	}

	public void setReadTimeout(int readTimeout) throws SocketException {
		acquire();
		try {
			super.setReadTimeout(readTimeout);
		} finally {
			release();
		}
	}
}
