// SocketOpener.java
//
// Java 1.1.
// Use at your own risk!

package ftp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

// The SocketOpener class opens sockets with timeout.  JDK 1.1 does not
// allow sockets to be opened with timeout; this class is a workaround.
//
// This code was donated to the public domain by the author on 17 December
// 1997 in a message on Sun's "Duke Bucks" forum.
//
// Known problem: Under JDK 1.1.5, when a timeout occurs a phantom thread
// gets left behind for a while.  That's because stopping a thread that is
// blocked on winsock i/o does not take effect immediately.
//
// When this code is moved to JDK 1.2, the stop should be changed to an
// interrupt (since stop is deprecated starting with JDK 1.2).  We can't
// just use interrupt now, because it doesn't work under JDK 1.1
//
// Version: 8 May 1998
// Originally by Wayne Conrad.
// Major design improvements by Jon Steelman <steelman@mindspring.com>.

public class SocketOpener {
	protected InetSocketAddress socketAddress;
	protected InetSocketAddress localSocketAddress;

	// Constructor. This SocketOpener will use Socket (String, int) to create
	// the socket.
	public SocketOpener(final String host, final int port) throws UnknownHostException {
		if(host == null) {
			socketAddress = new InetSocketAddress(InetAddress.getByName(null), port);
		} else {
			socketAddress = new InetSocketAddress(host, port);
		}
	}

	// Constructor. This SocketOpener will use Socket (InetAddress, int, int)
	// to create the socket.
	public SocketOpener(final InetAddress address, final int port) {
		socketAddress = new InetSocketAddress(address, port);
	}

	// Constructor. This SocketOpener will use Socket (String, int,
	// InetAddress, int) to create the socket.

	public SocketOpener(final String host, final int port, final InetAddress localAddr, final int localPort) throws UnknownHostException {
		this(host, port);
		localSocketAddress = new InetSocketAddress(localAddr, localPort);
	}

	// Constructor. This SocketOpener will use Socket (String, int,
	// InetAddress, int) to create the socket.

	public SocketOpener(final InetAddress address, final int port, final InetAddress localAddr, final int localPort) {
		this(address, port);
		localSocketAddress = new InetSocketAddress(localAddr, localPort);
	}

	// Open the socket with a timeout in milliseconds. If timeout == 0, then
	// use the socket's natural timeout. Note that if timeout is greater than
	// the socket's natural timeout, the natural timeout will happen anyhow.
	public Socket makeSocket(long timeout) throws IOException {
		Socket socket = new Socket();
		socket.connect(socketAddress, (int) timeout);
		return socket;
	}
}
