package ftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class CompressingSocketFtpConnection extends SocketFtpConnection {

	public CompressingSocketFtpConnection(Socket socket) {
		super(socket);
	}

	@Override
	public boolean isCompressing() {
		return true;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new InflaterInputStream(super.getInputStream());
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return new DeflaterOutputStream(super.getOutputStream());
	}
}
