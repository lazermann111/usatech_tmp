package test;


import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import com.usatech.ftps.EncryptType;
import com.usatech.ftps.FtpsBean;

import ftp.FtpException;
import ftp.FtpListResult;

public class FtpsTest {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
        
		int port = 21;
		String host = "localhost";
		String username = "anonymous";
		String password = "testftp";
		String remoteDir = "BIN_RANGE"; // "reports"; //
		String remoteFileName = "ind_db_ardef.txt"; // "testTransportFileName"; //"test-ftps-file.txt"; //
		EncryptType encryptType = EncryptType.NONE; // EncryptType.SSL_REQUIRE;
        //int port = 990; String host = "198.203.191.201"; String username = "usatech"; String password = "2tt89m"; String remoteDir = "inbox"; EncryptType encryptType = EncryptType.SSL_IMPLICIT;
		// int port = 990; String host = "ecgitef.pbsg.com"; String username = "usatech"; String password = "Test@12"; String remoteDir = "Inbound"; EncryptType encryptType = EncryptType.SSL_REQUIRE;
        //int port = 990; String host = "204.136.108.216"; String username = "usatech"; String password = "Test@12"; String remoteDir = "Inbound"; EncryptType encryptType = EncryptType.SSL_REQUIRE;
        //int port = 990; String host = "204.136.110.216"; String username = "usatech"; String password = "Test@12"; String remoteDir = "Inbound"; EncryptType encryptType = EncryptType.SSL_REQUIRE;
        //int port = 21; String host = "12.107.55.68"; String username = "usatech$77"; String password = "cv092006$"; String remoteDir = "inbox/weekly"; EncryptType encryptType = EncryptType.SSL_REQUIRE;
        
		// File localDir = new File("C:\\inetpub\\ftproot\\INBOX");
		// File localDir = new File("C:\\FileZilla Server\\base\\reports");
		File localDir = new File("C:\\FileZilla Server\\base\\BIN_RANGE");
		final Pattern localFilePattern = Pattern.compile(".*"); // Pattern.compile("P8009100706135[67]\\d{2}\\.uat");
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSS");
        //String contents = "This is a test. Please ignore.";
        
		class MyFtpsBean extends FtpsBean {
			@Override
			protected void ftpCommand(String cmd, String param) throws IOException, FtpException {
				System.out.print("--> ");
				System.out.print(cmd);
				if(param != null && param.trim().length() > 0) {
					System.out.print(' ');
					System.out.print(param);
				}
				System.out.println();
				super.ftpCommand(cmd, param);
			}

			@Override
			protected void setReply(String reply) {
				System.out.print("<-- ");
				System.out.println(reply);
				if(getReplyMessage() != null && getReplyMessage().trim().length() > 0) {
					System.out.println(getReplyMessage());
				}
				super.setReply(reply);
			}
			protected void logStatus(Object message) {
				System.out.print(df.format(new Date()));
				System.out.println(" FtpBean: " + message);
		    }
		    protected void logCommand(String cmd, String param) {
		    	System.out.print(df.format(new Date()));
				System.out.println(" FtpBean: Send command \"" + cmd + param + "\"");
		    }
		    protected void logResponse(String reply) {
		    	System.out.print(df.format(new Date()));
				System.out.println(" FtpBean: Receive reply \"" + reply + "\"");
		    }
		}
		// Make a client connection
		MyFtpsBean ftps = new MyFtpsBean();
        ftps.setPort(port);
        ftps.setEncryptType(encryptType);
        ftps.setHostVerifying(false);
        ftps.setSocketTimeout(2* 60 * 1000);
        try {
            // login
        	ftps.ftpConnect(host, username, password);
			ftps.enableCompression();
        	System.out.println("Current Path: " + ftps.getDirectory());
        	// Print directory contents
            FtpListResult flr = ftps.getDirectoryContent();
            System.out.println("FileName\tDate\tSize");
            while(flr.next()) {
            	System.out.println(flr.getName() + "\t" + flr.getDate() + "\t" + flr.getSize());
            }
            System.out.println("------------------");
            // Change directory
            if(remoteDir != null && remoteDir.trim().length() > 0) {
            	ftps.setDirectory(remoteDir);
            	System.out.println("Current Path: " + ftps.getDirectory());             
            }
            
            // Print directory contents
            flr = ftps.getDirectoryContent();
            System.out.println("FileName\tDate\tSize");
            while(flr.next()) {
            	System.out.println(flr.getName() + "\t" + flr.getDate() + "\t" + flr.getSize());
            }
            System.out.println("------------------");
            //ftps.putBinaryFile(filename, contents.getBytes());
            if(remoteFileName != null) {
	            byte[] content = ftps.getBinaryFile(remoteFileName);
				System.out.println("Got File of " + content.length + " bytes:");
				if(content.length > 200)
					System.out.println(new String(content, 0, 200) + "...");
				else
					System.out.println(new String(content));
            }
            if(localDir != null && localDir.exists()) {
            	File[] files = localDir.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return localFilePattern.matcher(name).matches();
					}
				});
            	for(File file : files) {
            		String finalName = file.getName().replaceAll("\\.dep$", ".uat");
					if(finalName.startsWith("P")) {
            		String tmpName = "T" + finalName.substring(1);
            		ftps.putFile(tmpName, new FileInputStream(file), 0);
            		ftps.fileRename(tmpName, finalName);
					} else {
						ftps.putFile(finalName, new FileInputStream(file), 0);
					}
            		System.out.println("Uploaded '" + file.getName() + "' as '" + finalName + "'");
            	}
            }
        } catch(Exception e) {
        	e.printStackTrace();
		} finally {
            ftps.close();
        }
        /*String appName = "ReportGenerator";
        App.setMasterAppName(appName);
        ReportGenerator rg = new ReportGenerator();
        rg.setProps(App.getProperties(App.getMasterAppName()));
        rg.generateBatchReport(575, 20079); 
        rg.generateBatchReport(559, 20079);
        */
        /*
        System.out.println(Arrays.toString(MailcapCommandMap.getDefaultCommandMap().getMimeTypes()));
        CommandInfo[] cis = MailcapCommandMap.getDefaultCommandMap().getAllCommands("application/xls");
        DataContentHandler dch = MailcapCommandMap.getDefaultCommandMap().createDataContentHandler("application/xls");
        
        System.out.println(dch);
        System.out.println(Arrays.toString(MailcapCommandMap.getDefaultCommandMap().getAllCommands("application/pdf")));
        */
        //Log.finish();        
    }

}
