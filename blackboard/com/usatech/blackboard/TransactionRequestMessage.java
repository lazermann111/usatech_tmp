package com.usatech.blackboard;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.usatech.util.Log;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;
import com.usatech.util.crypto.CryptUtil;

/**
 * Represents a Blackboard Transaction Request message. It is kept intentionally simple due to the fact that
 * there is relatively little to communications with a Blackboard server.
 *
 * Note: Refer to the documents entitled "Blackboard Transaction Integration Agent Developer Guide" and "Blackboard
 * Transaction Integration Agent Developer Guide Version 2 Protocol" by Blackboard, Inc. for further information.
 *
 * @author Steve Lukshides - IBM Global Services - April 2004
 */
class TransactionRequestMessage	//not needed visible outside of the package
{
	//Get the encryption algorithm to be used
	private static final Crypt cryptoAlgorithm = CryptUtil.getCrypt("AES_CBC");
	{
		if (cryptoAlgorithm == null)
			Log.error(TransactionResponseMessage.class, "Null returned by CryptUtil.getCrypt() for type AES_CBC.");
	}

	private static final Charset charset = Charset.forName("US-ASCII");
	
	private static final String CARD_DATA_REGEX = "([0-9]{7,})";
	private static Pattern cardDataPattern = Pattern.compile(CARD_DATA_REGEX);	

	private long	timeQueued;			//time this message was queued to be sent to server
	private long	timeSent;			//time this message was sent to the server

	private TransactionResponseMessage response = null;	//the response received for this message

	//Individual message contents
	private final int		tiaProtocolVersion;	//identifies format of message expected by the server
	private final String  vendorNumber;       //unique vendor number assigned to the processor
	private final int		terminalNumber;		//used to track and report on transactions originating from the vendor
	private final int		sequenceNumber;		//number that identifies individual transactions
	private final byte	transactionType;	//one of eight Blackboard pre-defined transaction types
	private final int		tenderNumber;		//identifies an account for the transaction type
	private final long	transactionTime;	//time of transaction, needs to be set in case of off line transactions
	private final TimeZone timeZone; //time zone of the device
	private final int		amount;				//amount of transaction in pennies
	private final boolean	onlineFlag;			//flag indicating an online or off-line transaction
	private final boolean	manuallyEnteredId;	//flag indicating whether or not the ID was manually entered or swiped
	private final String	trackDataOrId;		//contents of track data or manually entered user ID
	private String	pin;				//PIN associated with account
	private final byte[]	encryptionKey;		//key with which to encrypt the message

	/**
	 * Constructor for class used to represent a Transaction Request Message sent to a Blackboard server.
	 *
	 * @param tiaProtocolVersion identifies the format of the message expected by the server
	 * @param vendorNumber unique vendor number assigned to the processor
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param transactionType one of Blackboard's eight pre-defined transaction types
	 * @param tenderNumber account to use for this transaction
	 * @param amount amount of the transaction, zero for auths or balance inquiries
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param encryptionKey the key with which to encrypt the message
	 */
	public TransactionRequestMessage(int tiaProtocolVersion, String vendorNumber, int terminalNumber, int sequenceNumber, byte transactionType,
		int tenderNumber, int amount, boolean manuallyEnteredId, String trackDataOrId, String pin, boolean onlineFlag,
		long transactionTime, TimeZone timeZone, byte[] encryptionKey)
	{
		this.tiaProtocolVersion = tiaProtocolVersion;
		this.vendorNumber = vendorNumber;
		this.terminalNumber = terminalNumber;
		this.sequenceNumber = sequenceNumber;
		this.transactionType = transactionType;
		this.tenderNumber = tenderNumber;
		this.amount = amount;
		this.manuallyEnteredId = manuallyEnteredId;
		this.trackDataOrId = trackDataOrId;
		if (pin == null)
			this.pin = "";
		else
			this.pin = pin;
		this.onlineFlag = onlineFlag;
		this.transactionTime = transactionTime;
		this.timeZone = timeZone;
		this.encryptionKey = encryptionKey;
	}

	/**
	 * Returns an identifier assigned and configured in the Blackboard Transaction System used to track and report
	 * transactions that originate from a vendor.
	 *
	 * @return int that identifies a terminal used to originate a transaction
	 */
	public int getTerminalNumber()
	{
		return terminalNumber;
	}

	/**
	 * Returns a number that identifies individual transactions.
	 *
	 * @return int a number that identifies individual transactions
	 */
	public int getSequenceNumber()
	{
		return sequenceNumber;
	}

	/**
	 * Returns the encryption key with which to encode the outgoing message and to decode its response.
	 */
	public byte[] getEncryptionKey()
	{
		return encryptionKey;
	}

	/**
	 * Returns the time the message was queued to be sent
	 *
	 * @return long number of milliseconds since 1/1/1970 that identifies when this message was sent
	 */
	public long getTimeQueued()
	{
		return timeQueued;
	}

	/**
	 * Sets the time the message was queued to be sent
	 */
	public void setTimeQueued(long _timeQueued)
	{
		timeQueued = _timeQueued;
	}

	/**
	 * Returns the time the message was sent
	 *
	 * @return long number of milliseconds since 1/1/1970 that identifies when this message was sent
	 */
	public long getTimeSent()
	{
		return timeSent;
	}

	/**
	 * Sets the time the message was sent
	 */
	public void setTimeSent(long _timeSent)
	{
		timeSent = _timeSent;
	}

	/**
	 * Returns the TransactionResponseMessage received for this request
	 *
	 * @return TransactionResponseMessage response received for this request or null if request not sent or reply not receieved
	 */
	public TransactionResponseMessage getResponse()
	{
		return response;
	}

	/**
	 * Sets the TransactionResponseMessage that was received for this request and notifies all threads that are waiting
	 * for the response.
	 */
	public void setResponse(TransactionResponseMessage _response)
	{
		response = _response;
		synchronized (this)
		{
			notifyAll();
		}
	}

	/**
	 * Returns an array of bytes representing this message that is ready to be sent to a Blackboard server. Both version
	 * 1 and 2 of the Blackboard protocol are supported.
	 *
	 * @return an array of bytes ready for transmission or null if encryption was specified but the data was unable
	 * to be encrypted for some reason
	 */
	public byte[] formatForTransmission()
	{
		if ((encryptionKey != null) && (cryptoAlgorithm == null))
		{
			Log.error(TransactionRequestMessage.class, "No encryption algorithm available to encrypt message.");
			return null;
		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat ("yyyyMMddHHmmss");
		dateFormatter.setTimeZone(timeZone);
 		byte[] content = (tiaProtocolVersion + "~" + transactionType + "~" + sequenceNumber + "~" + tenderNumber + "~" +
				dateFormatter.format(new Date(transactionTime)) + "~" + ((onlineFlag == true) ? "T~" : "F~") + amount +
				"~USD~" + ((manuallyEnteredId == true) ? "T~" : "F~") + trackDataOrId + "~" + pin +
				((tiaProtocolVersion == 2) ? "~~" : "~")).getBytes(charset);

		byte[] encryptedContent = null;
		if (encryptionKey != null)
		{
			try
			{
				encryptedContent = cryptoAlgorithm.encrypt(content, encryptionKey, (byte) 0);
			}
			catch (InvalidKeyException ikx)
			{
				Log.error(TransactionRequestMessage.class, "Invalid encryption key.");
				Log.error(TransactionRequestMessage.class, ikx.toString());
				return null;
			}
			catch (CryptException cx)
			{
				Log.error(TransactionRequestMessage.class, "Encryption algorithm failure.");
				Log.error(TransactionRequestMessage.class, cx.toString());
				return null;
			}
		}

		byte[] envelope;
		int messageLength;
		if (encryptedContent == null)
		{
 			envelope = ("~0~" + vendorNumber+ "~" + terminalNumber + "~0~").getBytes(charset);
			messageLength = content.length + envelope.length + 1 + 2;	//+1 is for LRC, +2 is for message length
		}
		else
		{
 			envelope = ("~1~" + vendorNumber + "~" + terminalNumber + "~" +
 				String.valueOf(content.length) + "~").getBytes(charset);
			messageLength = encryptedContent.length + envelope.length + 2 + 2;	//+2 is for CRC, +2 is for message length
		}

		if (messageLength > 99)
			messageLength++;
		if (messageLength > 999)
			messageLength++;
		byte[] messageLengthBytes = String.valueOf(messageLength).getBytes(charset);

		byte[] formattedMessage = new byte[messageLength];
		System.arraycopy(messageLengthBytes, 0, formattedMessage, 0, messageLengthBytes.length);
		System.arraycopy(envelope, 0, formattedMessage, messageLengthBytes.length, envelope.length);
		if (encryptedContent == null)
		{
			System.arraycopy(content, 0, formattedMessage, messageLengthBytes.length + envelope.length, content.length);
			formattedMessage[formattedMessage.length - 1] = BlackboardProxy.calcLRC(formattedMessage, formattedMessage.length - 1);
		}
		else
		{
			System.arraycopy(encryptedContent, 0, formattedMessage, messageLengthBytes.length + envelope.length,
				encryptedContent.length);
			int crc = BlackboardProxy.calcCRC16(formattedMessage, formattedMessage.length - 2);
			formattedMessage[formattedMessage.length - 2] = (byte) ((crc >> 8) & 0xff);
			formattedMessage[formattedMessage.length - 1] = (byte) (crc & 0xff);
		}

		return formattedMessage;
	}

	/**
	 * This method is for debugging purposes only. It is a simplified cut and paste from formatForTransmission(), keep
	 * these two methods in sync.
	 */
	@Override
	public String toString()
	{
		SimpleDateFormat dateFormatter = new SimpleDateFormat ("yyyyMMddHHmmss");
		dateFormatter.setTimeZone(timeZone);
 		
 		String content = tiaProtocolVersion + "~" + transactionType + "~" + sequenceNumber + "~" + tenderNumber + "~" +
 			dateFormatter.format(new Date(transactionTime)) + "~" + ((onlineFlag == true) ? "T~" : "F~") + amount +
 			"~USD~" + ((manuallyEnteredId == true) ? "T~" : "F~") + maskCard(trackDataOrId) + "~" + pin +
 			((tiaProtocolVersion == 2) ? "~~" : "~");

		String envelope;
		int messageLength;
		if (encryptionKey == null)
		{
 			envelope = "~0~" + vendorNumber + "~" + terminalNumber + "~0~";
			messageLength = content.length() + envelope.length() + 2;	//+2 is for message length
		}
		else
		{
 			envelope = "~1~" + vendorNumber + "~" + terminalNumber + "~" +
 				String.valueOf(content.length()) + "~";
			messageLength = content.length() + envelope.length() + 2;	//+2 is for message length
		}

		return messageLength + envelope + content;
	}
    private static String maskCard(String card) {
    	if(card == null) return null;
    	Matcher cardDataMatcher = cardDataPattern.matcher(card);
    	if (cardDataMatcher.find()) {
    		byte[] digits = cardDataMatcher.group(1).getBytes();
			int end = digits.length - 4;
			for(int i = 2; i < end; i++)
				digits[i] = '*';
			return card.length() + " bytes, masked: " + new String(digits);    		
    	} else
			return card;
    }		
}
