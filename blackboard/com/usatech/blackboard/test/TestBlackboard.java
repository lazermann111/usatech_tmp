/**
 * This class is used to test the Blackboard related classes for certification purposes.
 * 
 * NOTE: The BlackboardProxy class must be made public in order to use this test.
 * NOTE2: This entire class is commented out to enable it to compile.  BlackboardProxy is not public because it
 * doesn't need to be.  If you want to test it in isolation the contents of this class can be uncommented and
 * used to test BlackboardProxy.
 * 
 * @author Steve Lukshides - IBM Global Services - April 2004
 */
package com.usatech.blackboard.test;

import com.usatech.blackboard.*;
import com.usatech.util.*;

public class TestBlackboard
{
/*
 	private static final String preferredHost = "66.210.59.124";
	private static final String alternateHost = "69.26.224.124";
	private static final int port = 9003;
	private static byte[] encryptionKey = {(byte) 0xab, (byte) 0xcd, (byte) 0xef, 0x01, 0x23, 0x45, 0x67, (byte) 0x89,
		(byte) 0xab, (byte) 0xcd, (byte) 0xef, 0x01, 0x23, 0x45, 0x67, (byte) 0x89};

	private static class TestCase
	{
		public static final int TRANS_TYPE_AUTHORIZATION_LIMIT = 0;
		public static final int TRANS_TYPE_BALANCE_INQUIRY = 1;
		public static final int TRANS_TYPE_DEBIT = 2;
		public static final int TRANS_TYPE_REFUND = 3;

		BlackboardProxy proxy;
		int		transactionType;
		int		terminalNumber;
		int		sequenceNumber;
		int		tenderNumber;
		int		amount;
		boolean	manuallyEntered;
		String		account;
		String		pin;
		boolean	onlineFlag;
		long		transactionTime;
		byte[]		encryptionKey;
		
		public TestCase(BlackboardProxy _proxy, boolean _manuallyEntered, String _account, int _amount, int _terminalNumber,
			int _sequenceNumber, int _tenderNumber, byte[] _encryptionKey, boolean _onlineFlag, int _transactionType)
		{
			proxy = _proxy;
			transactionType = _transactionType;
			terminalNumber = _terminalNumber;
			sequenceNumber = _sequenceNumber;
			tenderNumber = _tenderNumber;
			amount = _amount;
			manuallyEntered = _manuallyEntered;
			account = _account;
			pin = null;
			onlineFlag = _onlineFlag;
			transactionTime = System.currentTimeMillis();
			encryptionKey = _encryptionKey;
		}
	};

	public static void main(String[] args)
	{
		Log.setDebugEnabled(true);
		Log.info(TestBlackboard.class, "TestBlackboard v1.0 - Copyright (c) 2004 USA Technologies.  All rights reserved.");

		//Create and start Blackboard proxies
		BlackboardProxy v1Proxy = null;
		BlackboardProxy v2Proxy = null;
		try
		{
			v1Proxy = new BlackboardProxy("Certification-Test1", preferredHost, alternateHost, port, 1);
			v2Proxy = new BlackboardProxy("Certification-Test2", preferredHost, alternateHost, port, 2);
		}
		catch (BlackboardException bbx)
		{
			Log.error(TestBlackboard.class, bbx.toString());
			System.exit(1);
		}

		//Setup the test cases
		int t2SeqNum = 1;
		int t3SeqNum = 1;
		boolean useEncryption = true;
		if (useEncryption == false)
			encryptionKey = null;
		TestCase testCases[] =
		{
			new TestCase(v2Proxy, false, "375019001001900", 100, 2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001900", 90000, 2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001900", 100, 2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT)
		};
		TestCase initialCertificationTestCases[] =
		{
			//Sample
			new TestCase(v2Proxy, false, "375019001001880", 999, 2, 45, 910, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),

			//Reset accounts to original values
			new TestCase(v2Proxy, false, "375019001001880",     4159,   2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 120001, 2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "6329082000000000056", 900000, 2, t2SeqNum++, 387, null, true, TestCase.TRANS_TYPE_DEBIT),
			
			//Use these to examine balances of relevant accounts/tenders to make sure they are at the initial state
			new TestCase(v2Proxy, false, "375019001001880",     1, 2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 1, 2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000056", 1, 2, t2SeqNum++, 387, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),

			new TestCase(v2Proxy, true,  "900100188",           1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, true,  "900100188",           1, 3, t3SeqNum++, 900, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001900",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001900",     1, 3, t3SeqNum++, 600, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001910",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001920",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001940",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001950",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001960",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001970",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001990",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001002010",     1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 1, 3, t3SeqNum++, 900, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000054", 1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000055", 1, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),


			//Test cases 1-14 (from spreadsheet), unencrypted communications
			new TestCase(v2Proxy, false, "375019001001880", 0,     2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001880", 2299,  2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001880", 99,    2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_REFUND),
			new TestCase(v2Proxy, false, "375019001001880", 3641,  2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001880", 100,   2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v2Proxy, false, "375019001001880", 100,   2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 0,      2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v2Proxy, false, "6329082000000000053", 29999,  2, t2SeqNum++, 910, null, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "6329082000000000056", 0,      2, t2SeqNum++, 387, null, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
				
			//Test cases 15- (from spreadsheet), encrypted communications
			new TestCase(v2Proxy, false, "375019001001880", 0,    3, t3SeqNum++, 910, encryptionKey, true,  TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001880", 1023, 3, t3SeqNum++, 910, encryptionKey, true,  TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, true,  "900100188",       36,   3, t3SeqNum++, 910, encryptionKey, true,  TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001880", 45,   3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_DEBIT),

			new TestCase(v2Proxy, false, "6329082000000000053", 0,      3, t3SeqNum++, 910, encryptionKey, true,  TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 4321,   3, t3SeqNum++, 910, encryptionKey, true,  TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "6329082000000000053", 123456, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_DEBIT),

			new TestCase(v2Proxy, false, "6329082000000000053", 0, 3, t3SeqNum++, 910, encryptionKey, true,  TestCase.TRANS_TYPE_BALANCE_INQUIRY),

			new TestCase(v2Proxy, false, "375019001001900", 0,     3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v2Proxy, false, "375019001001900", 11114, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001900", 0,     3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v2Proxy, false, "375019001001900", 0,     3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001900", 10000, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001900", 116,   3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),

			new TestCase(v2Proxy, false, "375019001001910", 1063,   3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001920", 678053, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "6329082000000000054", 779, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001940", 222,    3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001950", 121,    3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001960", 5200,   3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001960", 4900,   3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001960", 4900,   3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "6329082000000000055", 120000, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),

			new TestCase(v2Proxy, false, "375019001001990", 4664, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001002010", 5000, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001002010", 5000, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001970", 8501, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001900", 100,  3, t3SeqNum, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001900", 2516, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT),
		};
		
		TestCase secondaryCertificationTestCases[] =
		{	//Secondary certification test cases
			new TestCase(v1Proxy, false, "375019001001900", 0, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v1Proxy, false, "375019001001900", 1000, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_REFUND),
			new TestCase(v1Proxy, false, "375019001001900", 500, 3, t3SeqNum++, 910, encryptionKey, true, TestCase.TRANS_TYPE_DEBIT)
		};

		TestCase offlineCertificationTestCases[] =
		{
			//Offline certification test cases
			new TestCase(v2Proxy, false, "375019001001900", 500, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_REFUND),
			new TestCase(v2Proxy, false, "375019001001900", 500, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001900", 500, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v2Proxy, false, "375019001001900", 699, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_REFUND),
			new TestCase(v2Proxy, false, "375019001001900", 325, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT),
			new TestCase(v2Proxy, false, "375019001001900", 325, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_DEBIT),

			new TestCase(v1Proxy, false, "375019001001900", 325, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_REFUND),
			new TestCase(v1Proxy, false, "375019001001900", 374, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v1Proxy, false, "375019001001900", 374, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_DEBIT),
			new TestCase(v1Proxy, false, "375019001001900", 374, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_REFUND),
			new TestCase(v1Proxy, false, "375019001001900", 699, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_BALANCE_INQUIRY),
			new TestCase(v1Proxy, false, "375019001001900", 699, 3, t3SeqNum++, 910, encryptionKey, false, TestCase.TRANS_TYPE_DEBIT)
		};

		for (int i=0; i < testCases.length; i++)
		{
			try
			{
				switch(testCases[i].transactionType)
				{
					case TestCase.TRANS_TYPE_AUTHORIZATION_LIMIT:
						Log.info(TestBlackboard.class, "Authorization Limit...");
						int authorizedAmount = testCases[i].proxy.authorize(testCases[i].terminalNumber,
							testCases[i].sequenceNumber, testCases[i].tenderNumber, testCases[i].amount,
							testCases[i].manuallyEntered, testCases[i].account, testCases[i].pin, testCases[i].onlineFlag,
							testCases[i].transactionTime, testCases[i].encryptionKey);
						Log.info(TestBlackboard.class, "Authorized amount = " + authorizedAmount);
						break;

					case TestCase.TRANS_TYPE_BALANCE_INQUIRY:
						Log.info(TestBlackboard.class, "Balance Inquiry...");
						int balance = testCases[i].proxy.authorize(testCases[i].terminalNumber, testCases[i].sequenceNumber,
							testCases[i].tenderNumber, testCases[i].amount, testCases[i].manuallyEntered,
							testCases[i].account, testCases[i].pin, testCases[i].onlineFlag, testCases[i].transactionTime,
							testCases[i].encryptionKey);
						Log.info(TestBlackboard.class, "Balance = " + balance);
						break;

					case TestCase.TRANS_TYPE_DEBIT:
						Log.info(TestBlackboard.class, "Debit...");
						testCases[i].proxy.debit(testCases[i].terminalNumber, testCases[i].sequenceNumber,
							testCases[i].tenderNumber, testCases[i].amount, testCases[i].manuallyEntered,
							testCases[i].account, testCases[i].pin, testCases[i].onlineFlag, testCases[i].transactionTime,
							testCases[i].encryptionKey);
						Log.info(TestBlackboard.class, "Debit successful.");
						break;

					case TestCase.TRANS_TYPE_REFUND:
						Log.info(TestBlackboard.class, "Refund...");
						testCases[i].proxy.refund(testCases[i].terminalNumber, testCases[i].sequenceNumber,
							testCases[i].tenderNumber, testCases[i].amount, testCases[i].manuallyEntered,
							testCases[i].account, testCases[i].pin, testCases[i].onlineFlag, testCases[i].transactionTime,
							testCases[i].encryptionKey);
						Log.info(TestBlackboard.class, "Refund successful.");
						break;

					default:
						Log.error(TestBlackboard.class, "Unsupported transaction type in test case " + i);
						break;
				}
			}
			catch (BlackboardException bbx)
			{
				Log.error(TestBlackboard.class, bbx.toString());
			}
		}

		//Shutdown Blackboard agent
		v1Proxy.shutdown();
		v2Proxy.shutdown();
			
		Util.sleep(1000);
		Log.info(TestBlackboard.class, "TestBlackboard v1.0 finished.");
		System.exit(0);
	}
*/
}
