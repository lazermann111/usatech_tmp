package com.usatech.blackboard.test;

import java.io.*;
import java.net.*;

/**
 * This is a quick and dirty program that sends a file to a given host/port then waits for and displays the response
 * received.  Both the file to send and the response should be text data.
 * 
 * @author Steve Lukshides - IBM Global Services - May 2004
 */
public class TestGateway
{
	/**
	 * Constructor for TestGateway.
	 */
	public TestGateway()
	{
		super();
	}

	/**
	 * Displays usage information to user and exits the application.
	 */
	private static void displayUsageAndExit(int exitCode)
	{
		System.err.println("Usage: java com.usatech.blackboard.test.TestGateway file-to-send gateway-ip-addr gateway-port");
		System.err.println("   file-to-send - name of file whose contents should be sent to gateway");
		System.err.println("   gateway-ip-addr - IP address of gateway");
		System.err.println("   gateway-port - port on which gateway is accepting connections");
		System.exit(exitCode);
	}

	public static void main(String[] args)
	{
		if (args.length != 3)
		{
			displayUsageAndExit(1);
		}
		
		File fileToSend = new File(args[0]);
		if (fileToSend.canRead() != true)
		{
			System.err.println("Cannot read input file.");
			System.exit(1);
		}

		String gatewayIpAddr = args[1];
		int gatewayPort = Integer.parseInt(args[2]);
		try
		{
			Socket socket = new Socket(gatewayIpAddr, gatewayPort);
			if (socket == null)	//a connection can't be established so the message can't be sent
			{
				System.err.println("Attempt to connect to " + gatewayIpAddr + " failed.");
				System.exit(1);
			}
			InputStream in = socket.getInputStream();
			OutputStream out = socket.getOutputStream();
			
			//Read the file (small files only), send the header, then send the file content
			char fileContent[] = new char[(int) fileToSend.length()];
			FileReader reader = new FileReader(fileToSend);
			reader.read(fileContent);
			out.write("POST /BlackboardGateway HTTP/1.1\r\n".getBytes());
			out.write("Content-Type: application/soap+xml\r\n".getBytes());
			out.write(("Content-Length: " + fileContent.length + "\r\n").getBytes());
			out.write("\r\n".getBytes());
			for (int i=0; i < fileContent.length; i++)
				out.write(fileContent[i]);
				
			//Process the response, but allow up to 3 minutes to receive since we are using this as a debug tool
			int ch = -1;
			long timeToStop = System.currentTimeMillis() + (3 * 60000);
			do
			{
				ch = in.read();
				if (ch != -1)
				{
					System.out.print((char) ch);
				}
				else
				{
					try
					{
						Thread.sleep(250);
					}
					catch (InterruptedException ix)
					{
					}
				}
			} while(timeToStop > System.currentTimeMillis());
			System.out.println();
		}
		catch (Exception x)
		{
			System.err.println(x.toString());
			System.exit(1);
		}
		System.exit(0);
	}
}
