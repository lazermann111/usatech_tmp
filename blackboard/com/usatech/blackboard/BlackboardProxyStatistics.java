package com.usatech.blackboard;

/**
 * Contains statistical information about a BlackboardProxy.
 *
 * @author Steve Lukshides - IBM Global Services - June 2004
 */
public class BlackboardProxyStatistics
{
	/**	time the proxy was started */
	public long timeStarted = System.currentTimeMillis();
	/** time server connection was established */
	public volatile long lastTimeConnected;
	/** duration of last server session, in milliseconds */
	public volatile long durationOfLastSession;
	/** total number of server sessions initiated successfully */
	public volatile long numSessionsEstablished = 0;
	/** total number of server sessions initiated */
	public volatile long numSessionsAttempted = 0;
	/** total number of IO Errors */
	public volatile int	numIOErrors = 0;
	/** total number of connect problems */
	public volatile int	numConnectErrors = 0;
	/** total number of other exceptions that occurred */
	public volatile int	numOtherExceptions = 0;
	/** time last IO Error occurred */
	public volatile long timeLastIOError;
	/** time last connect error occurred */
	public volatile long timeLastConnectError;
	/** time last other error occurred */
	public volatile long timeLastOtherError;
	/** number of messages received from server this session */
	public volatile long numMessagesReceivedInSession = 0;
	/** number of messages sent to server this session */
	public volatile long numMessagesSentInSession = 0;
	/** total number of messages received from server */
	public volatile long numMessagesReceived = 0;
	/** total number of messages sent to server */
	public volatile long numMessagesSent = 0;
	/** total time spent waiting for a response from the Bb server */
	public volatile long timeSpentWaitingForResponse = 0;
	/** number of authorizations attempted */
	public volatile long numAuthorizationsAttempted = 0;
	/** number of authorizations processed */
	public volatile long numAuthorizationsProcessed = 0;
	/** number of debits attempted */
	public volatile long numDebitsAttempted = 0;
	/** number of debits processed */
	public volatile long numDebitsProcessed = 0;
	/** number of refunds attempted */
	public volatile long numRefundsAttempted = 0;
	/** number of refunds processed */
	public volatile long numRefundsProcessed = 0;
	
	/** response time histogram data
	 * index 0 =   0 - 124 ms
	 * index 1 = 125 - 249 ms
	 * index 2 = 250 - 374 ms
	 * index 3 = 375 - 499 ms
	 * index 4 = > 500 ms */
	public static final int HISTOGRAM_WIDTH = 125;
	public static final int HISTOGRAM_COLUMNS = 5;
	public volatile long responseTime[] = new long[HISTOGRAM_COLUMNS];
}
