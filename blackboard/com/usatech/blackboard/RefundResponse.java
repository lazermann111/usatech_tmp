/*
 * Created on Feb 3, 2005
 */
package com.usatech.blackboard;

/**
 * Encapsulates Blackboard server response to refund request
 * 
 * @author dKouznetsov
 */
public class RefundResponse 
{
    private boolean success;
    private BlackboardException explanation;
    
    public RefundResponse()
    {
        success = false;
        explanation = null;
    }
    
    public RefundResponse(boolean success, int amount,
            BlackboardException explanation) {
        this.success = success;
        this.explanation = explanation;
    }
    
    /**
     * @return Returns the explanation.
     */
    public BlackboardException getExplanation() {
        return explanation;
    }
    /**
     * @param explanation The explanation to set.
     */
    public void setExplanation(BlackboardException explanation) {
        this.explanation = explanation;
    }
    /**
     * @return Returns the success.
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @param success The success to set.
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
