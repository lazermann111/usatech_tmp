/*
 * Created on Feb 3, 2005
 */
package com.usatech.blackboard;

/**
 * Encapsulates Blackboard server response to debit request
 * 
 * @author dKouznetsov
 */
public class DebitResponse 
{
    private boolean success;
    private BlackboardException explanation;
    
    public DebitResponse()
    {
        success = false;
        explanation = null;
    }
    
    public DebitResponse(boolean success, int amount,
            BlackboardException explanation) {
        this.success = success;
        this.explanation = explanation;
    }
    
    /**
     * @return Returns the explanation.
     */
    public BlackboardException getExplanation() {
        return explanation;
    }
    /**
     * @param explanation The explanation to set.
     */
    public void setExplanation(BlackboardException explanation) {
        this.explanation = explanation;
    }
    /**
     * @return Returns the success.
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @param success The success to set.
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
