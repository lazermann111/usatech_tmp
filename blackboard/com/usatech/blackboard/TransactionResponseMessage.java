package com.usatech.blackboard;

import java.util.StringTokenizer;
import java.security.InvalidKeyException;

import com.usatech.util.Log;
import com.usatech.util.crypto.*;

/**
 * Represents a Blackboard Transaction Response message.
 * 
 * Note: Refer to the documents entitled "Blackboard Transaction Integration Agent Developer Guide" and "Blackboard
 * Transaction Integration Agent Developer Guide Version 2 Protocol" by Blackboard, Inc. for further information.
 * 
 * @author Steve Lukshides - IBM Global Services - April 2004; Eric Rybski - February 2005
 */
class TransactionResponseMessage	//not needed visible outside of the package
{
	private static final String[] ERROR_DESC_100_SERIES = 
	{
		"Message Length <'44' or >'145' or not numeric type",
		"Must be '0' (no encryption/LRC) or '1' (AES/CRC)",
		"Vendor number '0' or >'9999' or not numeric type",
		"Terminal number '0' or >'99999999' or not numeric type",
		"Encrypted length <'48' or >'112' or not numeric type",
		"Version number not '2'",
		"Message type '0' or >'8' or not numeric type",
		"Sequence number invalid, '0' or >'999999'",
		"Tender number invalid, '0' or >'9999' or not numeric type",
		"Date and time invalid character(s) or not 14 characters",
		"Online flag invalid, not 'T' or 'F'",
		"Transaction amount invalid characters or formatting",
		"Currency entry not alphabetic or invalid type",
		"Manual ID entry invalid, not 'T' or 'F'",
		"ID or Card data error - invalid characters or mismatch with Manual ID entry field",
		"PIN number '0' or >'999999999'",
		"Cash Equivalency amount invalid characters or not numeric type",
		"",
		"CRC/LRC error - calculated versus expected",
		"",
		"",
		"Vendor and/or Terminal not defined",
		"ID number or Track 2 card data ID does not match the card definition encoding.",
		"Encryption/error checking of '0' and a defined encryption key in the configuration file requires the " +
			"transaction request to be encrypted."
	};

	private static final String[] ERROR_DESC_200_SERIES = 
	{
		"Card is not entered in System.",
		"Issue code on the card does not match the issue code the system is expecting.",
		"Card has been reported lost or stolen.",
		"Card has expired.",
		"Card has been deleted from the Blackboard System and is no longer valid.",
		"Card has been suspended.",
		"Card may not be used until the hold expires.",
		"Card account does not have a sufficient balance or credit to cover the purchase.",
		"Cardholder's transaction is over the amount allowed per transaction as defined in the account plan rules.",
		"Cardholder's transaction is over the amount allowed per day as defined in the account plan rules.",
		"Cardholder's transaction is over the credit limit defined in the account plan rules.",
		"Cardholder's transaction is over the cardholder's personal account or is about to exceed the limit.",
		"Cardholder is not assigned the privilege that he/she attempted to use.",
		"Cardholder's privilege has been suspended.",
		"Cardholder's privilege has expired.",
		"Privilege plan rules do not allow the use of the privilege during the time interval in effect.",
		"Privilege plan rules deny the use of the privilege during the current time period.",
		"Cardholder has already exercised the privilege because it was used at a previous time period during the day.",
		"Cardholder's privilege plan has expired.",
		"PIN entered does not match the cardholder's PIN defined in the System.",
		"Privilege plan rules deny the use of the privilege at the location where the cardholder has attempted to use it.",
		"Privilege plan rules do not allow the use of the privilege during the time interval in effect.",
		"Privilege plan rules do not allow the use of the privilege during the time interval in effect.",
		"System is programmed to recognize holidays. The privilege plan rules do not allow the use of the privilege during a holiday",
		"This error code does not have a defined description."
	};

	//Get the encryption algorithm to be used
	private static final Crypt cryptoAlgorithm = CryptUtil.getCrypt("AES_CBC");
	{
		if (cryptoAlgorithm == null)
			Log.error(TransactionResponseMessage.class, "Null returned by CryptUtil.getCrypt() for type AES_CBC.");
	}

	private static final int MINIMUM_NUM_TOKENS = 6;			//must be at least 6 tokens for a valid (encrypted) message
	private static final int NUM_FIELDS_IN_VALID_MESSAGE = 10;	//encryption pad and LRC/CRC not included
	private static final int NUM_UNENCRYPTED_FIELDS = 5;		//num unencrypted fields in an encrypted message

	private static final int FIELD_MESSAGE_LENGTH = 0;
	private static final int FIELD_ENCRYPTED_FLAG = 1;
	private static final int FIELD_VENDOR_NUMBER = 2;
	private static final int FIELD_TERMINAL_NUMBER = 3;
	private static final int FIELD_ENCRYPTED_LENGTH = 4;
	private static final int FIELD_VERSION_NUMBER = 5;
	private static final int FIELD_ENCRYPTED_FIELDS = 5;
	private static final int FIELD_MESSAGE_TYPE = 6;
	private static final int FIELD_SEQUENCE_NUMBER = 7;
	private static final int FIELD_RESPONSE_CODE = 8;
	private static final int FIELD_DISPLAY_TEXT = 9;

	private short responseCode;				//server response as a numeric value to assist in evaluating the response
	private String responseText;			//display text or error messages
	private byte[] rawMessage;				//the encrypted, unparsed message received from the server
	private String humanReadableMessage;	//a human readable version of the message received from the server

	/**
	 * Constructor for class used to represent a Transaction Response Message received from a Blackboard server.
	 */
	public TransactionResponseMessage(short responseCode, String responseText, byte[] rawMessage,
		String humanReadableMessage)
	{
		this.responseCode = responseCode;
		this.responseText = responseText;
		this.rawMessage = rawMessage;
		this.humanReadableMessage = humanReadableMessage;
	}

	/**
	 * Returns the server response as a numeric value to assist in evaluating the server response
	 * 
	 * @return short server response as a numeric value
	 */
	public short getResponseCode()
	{
		return responseCode;
	}

	/**
	 * Returns the server's display text or error message
	 * 
	 * @return String text returned by the server
	 */
	public String getResponseText()
	{
		return responseText;
	}

	/**
	 * Returns a textual description that further describes an error code or null if no description is available.
	 * 
	 * @return String text description of a Blackboard error or null if no error occurred or no description available
	 */
	public String getResponseDescription()
	{
		if ((responseCode >= 100) && (responseCode <= 123))
		{
			return (ERROR_DESC_100_SERIES[responseCode - 100]);
		}
		else if ((responseCode >= 200) && (responseCode <= 224))
		{
			return (ERROR_DESC_200_SERIES[responseCode - 200]);
		}
		return null;
	}

	/**
	 * Returns the unencrypted, unparsed message received from the server
	 * 
	 * @return String containing the unencrypted, unparsed message that was received from the server
	 */
	public byte[] getRawMessage()
	{
		return rawMessage;
	}

	/**
	 * Used to create a TransactionResponseMessage object from a byte array
	 * 
	 * @param rawMessage bytes received that should be interpreted as a TransactionResponseMessage
	 * @param encryptionKey key used to decrypt message if an encrypted message is received
	 * @param expectedSequenceNumber number of the request message that should match this response
	 * @param expectedTerminalNumber number of the terminal that sent the request that should match this response
	 * @return TransactionResponseMessage object if array of bytes represents a valid message, null otherwise
	 */
	public static TransactionResponseMessage createFromRawMessage(byte[] rawMessage, byte[] encryptionKey,
		int expectedSequenceNumber, int expectedTerminalNumber)
	{
		//Make sure the message contains at least the minimum number of fields
		String response = new String(byteArrayToCharArray(rawMessage));
		StringTokenizer tokenizer = new StringTokenizer(response, "~");
		if (tokenizer.countTokens() < MINIMUM_NUM_TOKENS)
		{
			Log.warn(TransactionResponseMessage.class, "Message does not contain the minimum number of fields.");
			return null;
		}

		String _humanReadableMessage = "";	//Initialize human readable version of message

		//Strip off the CRC or LRC before converting into tokens. This is done because the LRC or CRC may contain the
		//token delimiter character which can cause the tokenizer to count more tokens than we should have.
		tokenizer.nextToken();	//skip over message length
		boolean encryptedMessage = (tokenizer.nextToken().equals("0") ? false : true);
		if (encryptedMessage == true)
			response = new String(byteArrayToCharArray(rawMessage), 0, rawMessage.length - 2);
		else
			response = new String(byteArrayToCharArray(rawMessage), 0, rawMessage.length - 1);

		//Now parse the message into the various fields
		tokenizer = new StringTokenizer(response, "~");
		String fields[] = new String[tokenizer.countTokens()];
		for (int i=0; tokenizer.hasMoreTokens(); i++)
		{
			fields[i] = tokenizer.nextToken();
		}

		if (encryptedMessage == true)
		{	//data is encrypted
			if (encryptionKey == null)
			{
				Log.error(TransactionResponseMessage.class, "Encrypted message received but no encryption key supplied.");
				return null;
			}

			if (cryptoAlgorithm == null)
			{
				Log.error(TransactionResponseMessage.class, "No decryption algorithm available to decrypt message.");
				return null;
			}

			//Validate CRC
			int messageCRC = ((rawMessage[rawMessage.length - 2] & 0xff) << 8) | (rawMessage[rawMessage.length - 1] & 0xff);
			if (messageCRC != BlackboardProxy.calcCRC16(rawMessage, rawMessage.length - 2))
			{
				Log.warn(TransactionResponseMessage.class, "Message failed CRC check.");
				return null;
			}

			//Decrypt encrypted portion
			int encryptedLength = Integer.parseInt(fields[FIELD_ENCRYPTED_LENGTH]);
			int encryptionPad = 16 - (encryptedLength % 16);
			// bug fix - 08/22/2005 - pcowan
			if(encryptionPad == 16)
				encryptionPad = 0;
			String unParsedDecryptedFields = null;
			try
			{	//Copy the encrypted bytes out of the raw message into a byte array for the decryption routine
				byte[] encryptedData = new byte[encryptedLength + encryptionPad];
				System.arraycopy(rawMessage, rawMessage.length - 2 - encryptedLength - encryptionPad, encryptedData, 0,
					encryptedLength + encryptionPad);
				unParsedDecryptedFields = new String(byteArrayToCharArray(cryptoAlgorithm.decrypt(encryptedData, encryptionKey, encryptedLength)));
			}
			catch (InvalidKeyException ikx) 
			{
				Log.error(TransactionResponseMessage.class, "Invalid encryption key: " + encryptionKey); 
				Log.error(TransactionResponseMessage.class, ikx.toString());
				return null;
			}
			catch (CryptException cx) 
			{ 
				Log.error(TransactionResponseMessage.class, "Encryption algorithm failure."); 
				Log.error(TransactionResponseMessage.class, cx.toString());
				return null;
			}

			//Now parse the message into the various fields and create a human readable version
			tokenizer = new StringTokenizer(unParsedDecryptedFields, "~");
			String decryptedFields[] = new String[tokenizer.countTokens() + NUM_UNENCRYPTED_FIELDS];
			for (int i=0; i < NUM_UNENCRYPTED_FIELDS; i++)	//only the first NUM_UNENCRYPTED_FIELDS fields are unencrypted
			{
				decryptedFields[i] = fields[i];
				_humanReadableMessage += fields[i] + "~";
			}
			_humanReadableMessage += unParsedDecryptedFields;
			for (int i=NUM_UNENCRYPTED_FIELDS; tokenizer.hasMoreTokens(); i++)
				decryptedFields[i] = tokenizer.nextToken();
			
			//decryptedFields now holds all the available fields
			fields = decryptedFields;
		}
		else
		{	//Validate LRC
			if (rawMessage[rawMessage.length - 1] != BlackboardProxy.calcLRC(rawMessage, rawMessage.length - 1))
			{
				Log.warn(TransactionResponseMessage.class, "Message failed LRC check.");
				return null;
			}
			
			_humanReadableMessage = new String(byteArrayToCharArray(rawMessage), 0, rawMessage.length - 1);
		}

		if (fields.length != NUM_FIELDS_IN_VALID_MESSAGE)
		{
			Log.warn(TransactionResponseMessage.class, "Message with " + fields.length + " does not contain expected number of fields " + NUM_FIELDS_IN_VALID_MESSAGE + ".");
			return null;
		}

		//Get the fields we need to use more than once
		int sequenceNumber = Integer.parseInt(fields[FIELD_SEQUENCE_NUMBER]);
		int terminalNumber = Integer.parseInt(fields[FIELD_TERMINAL_NUMBER]);

		//Do sanity checks
		if (terminalNumber == 0)
		{
			terminalNumber = expectedTerminalNumber;
		}
		else if (terminalNumber != expectedTerminalNumber)
		{
			Log.warn(TransactionResponseMessage.class, "Message received from terminal " + expectedTerminalNumber + " is not for the expected terminal " + terminalNumber + ".");
			return null;
		}

		//In case of certain 1xx series errors (eg. 123), fields such as sequence number (also message type) will contain zeros
		//We have to change the sequence number to match the expected sequence number in order to match responses with requests
		if (sequenceNumber == 0)
		{
			sequenceNumber = expectedSequenceNumber;
		}
		else if (sequenceNumber != expectedSequenceNumber)
		{
			Log.warn(TransactionResponseMessage.class, "Sequence number " + sequenceNumber + " of message received does not match expected value " + expectedSequenceNumber + ".");
			return null;
		}

		return new TransactionResponseMessage(Short.parseShort(fields[FIELD_RESPONSE_CODE]), fields[FIELD_DISPLAY_TEXT],
			rawMessage, _humanReadableMessage);
	}

	/**
	 * This method is used for debugging purposes only
	 */
	public String toString()
	{
		return humanReadableMessage;
	}
	
	private static char[] byteArrayToCharArray(byte[] byteArray)
	{
		char buffer[] = new char[byteArray.length];

		for (int i=0; i < byteArray.length; i++)
		{
			buffer[i] = (char)(byteArray[i] & 0xff);
		}
		return buffer;
	}

}
