/*
 * Created on Feb 3, 2005
 */
package com.usatech.blackboard;

/**
 * Encapsulates Blackboard server response to authorization request
 * 
 * @author dKouznetsov
 */
public class AuthorizationResponse 
{
    private boolean success;
    private int amount;
    private BlackboardException explanation;
    
    public AuthorizationResponse()
    {
        success = false;
        amount = 0;
        explanation = null;
    }
    
    public AuthorizationResponse(boolean success, int amount,
            BlackboardException explanation) {
        this.success = success;
        this.amount = amount;
        this.explanation = explanation;
    }
    
    /**
     * @return Returns the amount.
     */
    public int getAmount() {
        return amount;
    }
    /**
     * @param amount The amount to set.
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }
    /**
     * @return Returns the explanation.
     */
    public BlackboardException getExplanation() {
        return explanation;
    }
    /**
     * @param explanation The explanation to set.
     */
    public void setExplanation(BlackboardException explanation) {
        this.explanation = explanation;
    }
    /**
     * @return Returns the success.
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @param success The success to set.
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
