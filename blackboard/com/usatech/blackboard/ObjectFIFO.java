/**
 * This code is publicly available and is based on code originally published in the book "Java Thread Programming"
 * by Paul Hyde, 1999.
 */
package com.usatech.blackboard;

class ObjectFIFO extends Object
{
	private Object[] queue;
	private int capacity;
	private int size;
	private int head;
	private int tail;

	public ObjectFIFO(int cap)
	{
		capacity = (cap > 0) ? cap : 1; // at least 1
		queue = new Object[capacity];
		head = 0;
		tail = 0;
		size = 0;
	}

	public int getCapacity()
	{
		return capacity;
	}

	public synchronized int getSize()
	{
		return size;
	}

	public synchronized boolean isEmpty()
	{
		return (size == 0);
	}

	public synchronized boolean isFull()
	{
		return (size == capacity);
	}

	public synchronized void add(Object obj) throws InterruptedException
	{
		waitWhileFull();

		queue[head] = obj;
		head = (head + 1) % capacity;
		size++;

		notifyAll(); // let any waiting threads know about change
	}

	public synchronized Object remove() throws InterruptedException
	{
		waitWhileEmpty();

		Object obj = queue[tail];

		// don't block GC by keeping unnecessary reference
		queue[tail] = null;

		tail = (tail + 1) % capacity;
		size--;

		notifyAll(); // let any waiting threads know about change

		return obj;
	}

	public synchronized void waitWhileEmpty() throws InterruptedException
	{
		while (isEmpty())
		{
			wait();
		}
	}

	public synchronized void waitWhileFull() throws InterruptedException
	{
		while (isFull())
		{
			wait();
		}
	}
}
