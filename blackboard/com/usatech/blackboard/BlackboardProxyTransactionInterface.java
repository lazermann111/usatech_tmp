package com.usatech.blackboard;

import java.util.TimeZone;

/**
 * Specifies methods available from the BlackboardProxy that are used for transaction processing.
 * 
 * @author Steve Lukshides - IBM Global Services - May 2004
 */
public interface BlackboardProxyTransactionInterface
{
	/**
	 * Contacts Blackboard server to do an authorization limit or balance inquiry request (depending on what the server
	 * supports) for the given account and returns the authorized value in pennies up to the amount requested.
	 * 
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param requestedAmount amount for which to authorize
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param timeZone the Time Zone of the device
	 * @param encryptionKey the key with which to encrypt the message
	 * 
	 * @throws BlackboardException if there was a problem contacting the Blackboard server or interpreting its response
	 * 
	 * @return int amount authorized for transaction, will not be greater than requested amount but could be lower
	 */
	public int authorize(int terminalNumber, int sequenceNumber, int tenderNumber, int requestedAmount,
		boolean manuallyEnteredId, String trackDataOrId, String pin, boolean onlineFlag, long transactionTime, TimeZone timeZone, 
		byte[] encryptionKey) throws BlackboardException;

	/**
	 * Contacts Blackboard server to do a debit from the given account.
	 * 
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param amount amount to deposit in pennies
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param timeZone the Time Zone of the device
	 * @param encryptionKey the key with which to encrypt the message
	 * 
	 * @throws BlackboardException if there was a problem contacting the Blackboard server or interpreting its response
	 */
	public void debit(int terminalNumber, int sequenceNumber, int tenderNumber, int amount, boolean manuallyEnteredId,
		String trackDataOrId, String pin, boolean onlineFlag, long transactionTime, TimeZone timeZone, byte[] encryptionKey)
		throws BlackboardException;

	/**
	 * Contacts Blackboard server to do a refund to the given account.
	 * 
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param amount amount to refund in pennies
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param timeZone the Time Zone of the device
	 * @param encryptionKey the key with which to encrypt the message
	 * 
	 * @throws BlackboardException if there was a problem contacting the Blackboard server or interpreting its response
	 */
	public void refund(int terminalNumber, int sequenceNumber, int tenderNumber, int amount, boolean manuallyEnteredId,
		String trackDataOrId, String pin, boolean onlineFlag, long transactionTime, TimeZone timeZone, byte[] encryptionKey)
		throws BlackboardException;
}
