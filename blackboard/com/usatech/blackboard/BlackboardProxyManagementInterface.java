package com.usatech.blackboard;

/**
 * Specifies methods available from the BlackboardProxy that are used for managing the proxy.
 * 
 * @author Steve Lukshides - IBM Global Services - May 2004
 */
public interface BlackboardProxyManagementInterface
{
	/**
	 * Returns the name assigned to this proxy
	 */
	public String getName();

	/**
	 * Returns true if currently connected to the server.
	 */
	public boolean isConnected();

	/**
	 * Returns true if currently servicing a request.
	 */
	public boolean isBusy();

	/**
	 * Used to determine if the internal thread is still alive.
	 */
	public boolean isAlive();

	/**
	 * This method is for use by the management user interface.
	 */
	public BlackboardProxyStatistics getStatistics();

	/**
	 * Returns the port on which to contact the Blackboard servers
	 */
	public int getPort();

	/**
	 * Returns the IP address or resolvable name of the preferred Blackboard host
	 */
	public String getPreferredHost();

	/**
	 * Returns the IP address or resolvable name of the alternate Blackboard host
	 */
	public String getAlternateHost();

	/**
	 * Returns the Blackboard TIA protocol version implemented by the server
	 */
	public int getBlackboardProtocolVersion();

	/**
	 * Returns the number of messages in the outbound queue
	 */
	public int getNumMessagesWaitingToBeSent();
}
