package com.usatech.blackboard;

//Import java classes
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import com.usatech.util.Log;

/**
* Handles messaging to a Blackboard server. This class is responsible for sending requests and receiving responses
* from a Blackboard server. Versions 1 and 2 messages are supported.  Methods to perform Blackboard specific functions
* are provided for Balance Inquiry, (version 1 servers only), Authorization Limit (vesion 2 servers only), Debit and
* Refund. The Count, Activity, Deposit and Cash Equivalency Blackboard transaction types are not supported. Messages
* are not guaranteed to be delivered. If connection to a host cannot be established, the message will be discarded.
*
* Note: Refer to the documents entitled "Blackboard Transaction Integration Agent Developer Guide" and "Blackboard
* Transaction Integration Agent Developer Guide Version 2 Protocol" by Blackboard, Inc. for further information.
*
* @author Steve Lukshides - IBM Global Services - April, May 2004; Eric Rybski - February 2005
*/
public class BlackboardProxy implements Runnable, BlackboardProxyManagementInterface, BlackboardProxyTransactionInterface
{
	public static final String USATECH_VENDOR_NUMBER = "2548";	//USA Tech's unique ID assigned by Blackboard

	//Message Types
	private static final byte MESSAGE_TYPE_MIN = 1;
	private static final byte MESSAGE_TYPE_MAX = 8;
	private static final byte MESSAGE_TYPE_DEBIT = 1;
	private static final byte MESSAGE_TYPE_BALANCE_INQUIRY = 2;
	private static final byte MESSAGE_TYPE_COUNT = 3;
	private static final byte MESSAGE_TYPE_ACTIVITY = 4;
	private static final byte MESSAGE_TYPE_REFUND = 5;
	private static final byte MESSAGE_TYPE_DEPOSIT = 6;
	private static final byte MESSAGE_TYPE_AUTHORIZATION_LIMIT = 7;
	private static final byte MESSAGE_TYPE_CASH_EQUIVALENCY = 8;

	private static int SOCKET_CONNECT_DELAY = 2000;	//time to allow for successful socket connection

	//Blackboard recommended comm settings
	private static int NETWORK_MSG_RECV_TIMEOUT = 12000;	//time to wait before resending message if response not received; spec recommends 4000
	private static int NETWORK_MSG_RECV_RETRY_MAX_TIME = 12000; //max time range during which a retry may still be executed; spec recommends 12000
	private static int NETWORK_MSG_RECV_RETRY_MAX = Math.max(0, (int)Math.floor((NETWORK_MSG_RECV_RETRY_MAX_TIME - NETWORK_MSG_RECV_TIMEOUT) / NETWORK_MSG_RECV_TIMEOUT));

	private static int NETWORK_MSG_RESPONSE_TIMEOUT = NETWORK_MSG_RECV_RETRY_MAX_TIME + 4000;
	private static int CONNECTION_IDLE_TIMEOUT = 60000;

	//Blackboard server parameters provided by the instantiator of this object
	private final String name;            //not a server parameter, but an identifier of this instance to help make debugging easier
	private final String preferredHost;   //perferred server to which we will connect
	private String alternateHost;   //alternate server to which we will connect if perferred is not available
	private final int	port;               //the port on the perferred or alternate host to which we will connect
	private final int	tiaProtocolVersion; //the version of Blackboard TIA Message Protocol supported by server
	private final String vendorNumber;

	private final List<TransactionRequestMessage> outboundQueue = Collections.synchronizedList(new LinkedList<TransactionRequestMessage>());

	private final Thread internalThread;               //the thread that implements functionality of this class
	private volatile boolean stop = false;       //indicates if thread should stop
	private volatile boolean connected = false;  //true while online, false otherwise
	private volatile boolean busy = false;       //true if presently servicing a request
	private volatile long timeOfLastNetActivity; //indicates time last of last data transfer
	private volatile boolean lastConnectSuccess = false; //indicates whether last connection attempt succeeded

	//Attributes used for status reporting
	private final BlackboardProxyStatistics statistics = new BlackboardProxyStatistics();

	/**
	 * Constructs class used to communicate with a Blackboard server.
	 *
	 * @param name identifies this instance so it can be matched up with an incoming request
	 * @param preferredHost IP address of Blackboard server
	 * @param alternateHost IP address of alternate Blackboard server
	 * @param port Blackboard server port used for communicating to Blackboard server
	 * @param tiaProtocolVersion version of Blackboard TIA Message Protocol supported by server
	 */
	public BlackboardProxy(String name, String preferredHost, String alternateHost, int port, int tiaProtocolVersion, String vendorNumber)
		throws BlackboardException
	{
		super();

		//Do error checking on parameters
		String errorText;
		if ((name == null) || (name.equals("")))
		{
			errorText = "Missing name.";
			Log.error(BlackboardProxy.class, errorText);
			throw new BlackboardException(errorText);
		}

		if ((preferredHost == null) || (preferredHost.equals("")))
		{
			errorText = "Missing host IP address or resolvable name.";
			Log.error(BlackboardProxy.class, errorText);
			throw new BlackboardException(errorText);
		}

		if ((port < 1) || (port > 65535))
		{
			errorText = "Invalid port.";
			Log.error(BlackboardProxy.class, errorText);
			throw new BlackboardException(errorText);
		}

		if ((tiaProtocolVersion < 1) || (tiaProtocolVersion > 2))
		{
			errorText = "Invalid Blackboard protocol version.";
			Log.error(BlackboardProxy.class, errorText);
			throw new BlackboardException(errorText);
		}

		this.name = name;
		this.preferredHost = preferredHost;
		this.alternateHost = preferredHost;
		if ((alternateHost != null) && (!alternateHost.equals("")))
			this.alternateHost = alternateHost;
		this.port = port;
		this.tiaProtocolVersion = tiaProtocolVersion;
		this.vendorNumber = vendorNumber;
		internalThread = new Thread(this);
		internalThread.setName(this.name);
		internalThread.start();
	}

	/**
	 * Do not call directly, use start() to start thread execution
	 */
	public void run()
	{
		// Check that no one has erroneously invoked this public method
		if (Thread.currentThread() != internalThread)
		{
			throw new RuntimeException("Only the internal thread is allowed to invoke run()");
		}

		boolean ioErrorOccurred = false;	//true if an I/O error occurred
		boolean connectError = false;		//true if a problem connecting to host occurred

		//Current host to connect to. If preferred is not available the alternate host will be tried
		String hostToContact = preferredHost;

		Log.info(BlackboardProxy.class, "Starting " + internalThread.getName() + ", host=" + preferredHost + ", alt=" +
			alternateHost + ", port=" + port + ", TIA Ver=" + tiaProtocolVersion);
		while (isShuttingDown() == false)
		{
			if (outboundQueue.isEmpty() == true)
			{	//wait for a message to be added to the outbound queue or a shutdown request
				try
				{
					synchronized(outboundQueue)
					{
						outboundQueue.wait();
					}
				}
				catch (InterruptedException ie)
				{
				}
			}

			//A message has either been added or a request to shutdown was received
			if (isShuttingDown() == true)
				break;

			//Establish a socket connection to the server and send and receive any waiting messages
			try
			{
				Log.info(BlackboardProxy.class, "Initiating connection to " + hostToContact);
				statistics.numSessionsAttempted++;
				//Socket socket = TimedSocket.getSocket(hostToContact, port, SOCKET_CONNECT_DELAY);
				Socket socket = new Socket();
				socket.connect(new InetSocketAddress(hostToContact, port), SOCKET_CONNECT_DELAY);
				if (socket == null)	//a connection can't be established so the message can't be sent
				{
					lastConnectSuccess = false;
					Log.warn(BlackboardProxy.class, "Attempt to connect to " + hostToContact + " failed.");
					continue;
				}

				lastConnectSuccess = true;
				Log.info(BlackboardProxy.class, "Connected to " + hostToContact);
				connected = true;
				statistics.lastTimeConnected = System.currentTimeMillis();
				statistics.numSessionsEstablished++;
				statistics.numMessagesReceivedInSession = 0;
				statistics.numMessagesSentInSession = 0;
				socket.setSoTimeout(CONNECTION_IDLE_TIMEOUT);
				InputStream in = socket.getInputStream();
				OutputStream out = socket.getOutputStream();

				//Send and receive all waiting messages
				timeOfLastNetActivity = System.currentTimeMillis();
				while (((timeOfLastNetActivity + CONNECTION_IDLE_TIMEOUT) > System.currentTimeMillis()) &&
					(isShuttingDown() != true))
				{
					if (outboundQueue.isEmpty())
					{	//All outbound requests have been sent, wait up to CONNECTION_IDLE_TIMEOUT milliseconds for
						//additional outbound messages before closing connection.
						try
						{
							synchronized(outboundQueue)
							{
								outboundQueue.wait(CONNECTION_IDLE_TIMEOUT);
							}
						}
						catch (InterruptedException ie)
						{
						}
						continue;
					}

					busy = true;

					//Send and wait for response; if no response received, resend up to maxRetries time(s)
					TransactionRequestMessage request = sendNextRequestMessage(out);
					for (int i=0; (i < NETWORK_MSG_RECV_RETRY_MAX) && (request != null); i++)
					{
						long timeToGiveUp = System.currentTimeMillis() + NETWORK_MSG_RECV_TIMEOUT;
						while ((in.available() <= 0) && (timeToGiveUp > System.currentTimeMillis()))
						{
							//Thread.currentThread().yield();
							Thread.sleep(5);
						}
						if (in.available() > 0)
							break;
						request = sendRequestMessage(out, request);
					}
					
					long timeToGiveUp = System.currentTimeMillis() + NETWORK_MSG_RECV_TIMEOUT;
					while ((in.available() <= 0) && (timeToGiveUp > System.currentTimeMillis()))
					{
						//Thread.currentThread().yield();
						Thread.sleep(5);
					}

					//Poll for correct response message; if not found, break and disconnect proxy session (to explicitly flush TIA server cache)
					boolean getResponseMessageResult = false;
					while (in.available() > 0 && getResponseMessageResult == false)
					{
						getResponseMessageResult = getResponseMessage(in, request);
						if (getResponseMessageResult == true)
							break;
						while ((in.available() <= 0) && (timeToGiveUp > System.currentTimeMillis()))
						{
							//Thread.currentThread().yield();
							Thread.sleep(5);
						}
					}
					if (getResponseMessageResult != true)
					{
						Log.info(BlackboardProxy.class, "Expected response not received. Preparing to disconnect from " + hostToContact);
						break;
					}

					busy = false;
				}

				Log.info(BlackboardProxy.class, "Disconnecting from " + hostToContact);
				busy = false;
				in.close();
				out.close();
				socket.close();
				Log.info(BlackboardProxy.class, "Disconnected from " + hostToContact);
				statistics.durationOfLastSession = System.currentTimeMillis() - statistics.lastTimeConnected;
				hostToContact = preferredHost;	//go back to preferred host, regardless of connection we have now
			}
			catch (UnknownHostException uhx)
			{
				connectError = true;
				statistics.numConnectErrors++;
				statistics.timeLastConnectError = System.currentTimeMillis();
				Log.error(BlackboardProxy.class, uhx.toString());
			}
			catch (ConnectException cx)
			{
				connectError = true;
				statistics.numConnectErrors++;
				statistics.timeLastConnectError = System.currentTimeMillis();
				Log.error(BlackboardProxy.class, cx.toString());
			}
			catch (InterruptedIOException iiox)
			{
				ioErrorOccurred = true;
				statistics.numIOErrors++;
				statistics.timeLastIOError = System.currentTimeMillis();
				Log.error(BlackboardProxy.class, iiox.toString());
			}
			catch (IOException iox)
			{
				ioErrorOccurred = true;
				statistics.numIOErrors++;
				statistics.timeLastIOError = System.currentTimeMillis();
				Log.error(BlackboardProxy.class, iox.toString());
			}
			catch (Exception ex)
			{
				statistics.numOtherExceptions++;
				statistics.timeLastOtherError = System.currentTimeMillis();
				Log.error(BlackboardProxy.class, ex.toString());
			}
			finally
			{
				connected = false;
			}

			if (connectError == true)
			{
				connectError = false;
				lastConnectSuccess = false;
				if (hostToContact.equals(preferredHost))
				{
					Log.info(BlackboardProxy.class, "Unable to connect to preferred host " + preferredHost);
					hostToContact = alternateHost;
				}
				else
				{
					Log.info(BlackboardProxy.class, "Unable to connect to alternate host " + alternateHost);
					hostToContact = preferredHost;
				}
				Log.info(BlackboardProxy.class, "Thread will sleep for one second before attempting next connection.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ie) {
					//just ignore if it happens
				}
			}
			else if (ioErrorOccurred == true)
			{	//this code not done inside catch blocks due to problem that occurred on J9 VM. Not sure of reason,
				//still investigating. Symptom shown is an InterruptedIOException. The sleep below was previously
				//executed in the catch block but failed to delay for the requested time.
				ioErrorOccurred = false;
				if (hostToContact.equals(preferredHost))
				{
					Log.info(BlackboardProxy.class, "Trouble communicating with preferred host " + preferredHost);
					hostToContact = alternateHost;
				}
				else
				{
					Log.info(BlackboardProxy.class, "Trouble communicating with alternate host " + alternateHost);
					hostToContact = preferredHost;
				}
				Log.info(BlackboardProxy.class, "Thread will sleep for one second before attempting next connection.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ie) {
					//just ignore if it happens
				}
			}

			removeOldMessagesFromOutboundQueue();
		}
		Log.info(BlackboardProxy.class, internalThread.getName() + " finished.");
	}

	/**
	 * Processes incoming data stream to assemble messages. Any messages received will be placed in the inbound queue.
	 *
	 * @param in an InputStream stream used to read from
	 * @param request the request whose response will be retrieved
	 * @throws IOException
	 * @return true if we should continue session, false otherwise
	 */
	private boolean getResponseMessage(InputStream in, TransactionRequestMessage request) throws IOException
	{
		//Get the message length, read at most 5 characters from input, this is num bytes in message plus field separator
		byte bytesRead[] = new byte[5];
		for (int i=0; (i < 5) && (in.available() > 0); i++)
		{
			int nextVal = in.read();
			bytesRead[i] = (byte) nextVal;
			if (nextVal == -1)	//check for end of stream
			{
				Log.warn(BlackboardProxy.class, "End of stream reached prematurely. Possible communications failure.");
				break;
			}
			else if (bytesRead[i] == '~')
				break;
		}

		int messageLength;
		byte messageLengthBytes[] = new byte[bytesRead.length - 1];
		System.arraycopy(bytesRead, 0, messageLengthBytes, 0, messageLengthBytes.length);
		try
		{
			messageLength = Integer.parseInt(new String(messageLengthBytes));
		}
		catch (NumberFormatException nfx)
		{
			Log.warn(BlackboardProxy.class, "Invalid message length received in message.");
			return false;
		}

		int bytesRemaining = messageLength - bytesRead.length;
		if (in.available() < bytesRemaining)
		{
			return false;
		}

		byte buffer[] = new byte[messageLength];
		System.arraycopy(bytesRead, 0, buffer, 0, bytesRead.length);
		timeOfLastNetActivity = System.currentTimeMillis();
		if (in.read(buffer, bytesRead.length, bytesRemaining) != bytesRemaining)
		{
			Log.warn(BlackboardProxy.class, "Failed to read required number of bytes from input stream.");
			return false;
		}

		if (buffer != null)
		{
			TransactionResponseMessage response = TransactionResponseMessage.createFromRawMessage(buffer,
				request.getEncryptionKey(), request.getSequenceNumber(), request.getTerminalNumber());
			if (response != null)
			{
				long timeSpentWaitingForResponse = (System.currentTimeMillis() - request.getTimeSent());
				statistics.responseTime[(int) Math.min(timeSpentWaitingForResponse / BlackboardProxyStatistics.HISTOGRAM_WIDTH,
					BlackboardProxyStatistics.HISTOGRAM_COLUMNS - 1)]++;
				statistics.timeSpentWaitingForResponse += timeSpentWaitingForResponse;
				statistics.numMessagesReceived++;
				statistics.numMessagesReceivedInSession++;
				Log.info(BlackboardProxy.class, "Received: " + response.toString());
				request.setResponse(response);
			}
			else
			{
				Log.warn(BlackboardProxy.class, "Received unexpected or unrecognized message: " + formatHex(buffer));
				return false;
			}
		}

		return true;
	}

	/**
	 * Sends message at the head of the outbound queue.
	 *
	 * @param out OutputStream to which to write
	 * @return TransactionRequestMessage that was sent or null if it could not be sent
	 * @throws IOException if an error occurs while writing to the OutputStream
	 */
	private TransactionRequestMessage sendNextRequestMessage(OutputStream out) throws IOException
	{
		if (outboundQueue.isEmpty() == false)
		{
			TransactionRequestMessage outboundMessage = null;

			synchronized (outboundQueue)
			{
				outboundMessage = (TransactionRequestMessage) outboundQueue.remove(0);
			}
			return sendRequestMessage(out, outboundMessage);
		}
		return null;
	}

	/**
	 * Sends the given message to a Blackboard server.
	 *
	 * @param out OutputStream to write to
	 * @param request TransactionRequestMessage to send to Blackboard
	 * @return true if message was sent, false if there is a problem with the message (do not retry if false)
	 * @throws IOException if an error occurs while writing to the OutputStream
	 */
	private TransactionRequestMessage sendRequestMessage(OutputStream out, TransactionRequestMessage request) throws IOException
	{
		byte[] messageBytes = request.formatForTransmission();
		if (messageBytes == null)
		{
			request.setResponse(new TransactionResponseMessage((short) -1,
				"Unable to format TransactionRequestMessage for transmission.", null, null));
			return null;
		}

		out.write(messageBytes);	//write it out
		request.setTimeSent(System.currentTimeMillis());
		Log.info(BlackboardProxy.class, "Sent: " + request.toString());

		//Update statistics
		timeOfLastNetActivity = System.currentTimeMillis();
		statistics.numMessagesSent++;
		statistics.numMessagesSentInSession++;
		return request;
	}

	/**
	 * Removes all messages from the outbound queue that have been in the queue too long. This method exists
	 * because of the possibility the network may become unavailable.  If this should happen, outbound messages will start
	 * to pile up. If this condition persists for a long period of time, the messages would pile up until memory is exhausted
	 * and the JVM crashes. Authorization Limit and Balance Inquiry messages have no value if they can't be processed
	 * immediately, others such as refunds or debits must be stored and processed at a later time by the application that
	 * sent the message to begin with.
	 */
	private void removeOldMessagesFromOutboundQueue()
	{
		synchronized(outboundQueue)
		{
			Iterator<TransactionRequestMessage> iterator = outboundQueue.iterator();
			TransactionRequestMessage outboundMessage;
			while (iterator.hasNext())
			{
				outboundMessage = iterator.next();
				//if ((outboundMessage.getTimeQueued() + (NETWORK_MSG_RESPONSE_TIMEOUT * 2)) < System.currentTimeMillis())
				if ((outboundMessage.getTimeQueued() + NETWORK_MSG_RESPONSE_TIMEOUT ) < System.currentTimeMillis())
				{
					Log.warn(BlackboardProxy.class, "Removing message from outbound queue because it exceeded max allowed " +
						"time in queue. Message=" + outboundMessage.toString());
					iterator.remove();
				}
			}
		}
	}

	/**
	 * Checks to see if the message is already in the outbound message queue.
	 *
	 * @param outboundMessage a TransactionRequestMessage that will be located in the outbound queue
	 * @return TransactionRequestMessage the message if it is already in the queue, null otherwise
	 */
	private TransactionRequestMessage getDuplicateMessage(TransactionRequestMessage outboundMessage)
	{
		if (isOutboundQueueEmpty())
			return null;
		TransactionRequestMessage messageInQueue = null;

		//Look through queue for the message
		synchronized (outboundQueue)
		{
			Iterator<TransactionRequestMessage> iterator = outboundQueue.iterator();
			while (iterator.hasNext())
			{
				messageInQueue = iterator.next();
				if (outboundMessage.equals(messageInQueue))
					return messageInQueue;
			}
		}
		return null;
	}

	/**
	 * Used to determine if thread should shutdown (multiprocessor safe implementation)
	 */
	private synchronized final boolean isShuttingDown()
	{
		return stop;
	}

	/**
	 * Used to indicate that this thread should stop execution.
	 */
	public void shutdown()
	{
		Log.info(BlackboardProxy.class, internalThread.getName() + " told to stop.");
		synchronized(this)
		{
			stop = true;
		}
		synchronized(outboundQueue)
		{
			outboundQueue.notifyAll();	//notify thread that a  message is available to send
		}
	}

	/**
	 * Determines whether or not the outgoing message queue is empty.
	 *
	 * @return true if the message queue is empty, false otherwise
	 */
	private boolean isOutboundQueueEmpty()
	{
		return outboundQueue.isEmpty();
	}

	/**
	 * Returns true if currently connected to the server.
	 */
	public boolean isConnected()
	{
		return connected;
	}

	/**
	 * Returns true if currently servicing a request.
	 */
	public boolean isBusy()
	{
		return busy;
	}

	/**
	 * Used to determine if the internal thread is still alive.
	 */
	public boolean isAlive()
	{
		return internalThread.isAlive();
	}

	/**
	 * Returns the name assigned to this proxy
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Returns the IP address or resolvable name of the preferred Blackboard host
	 */
	public String getPreferredHost()
	{
		return preferredHost;
	}

	/**
	 * Returns the IP address or resolvable name of the alternate Blackboard host
	 */
	public String getAlternateHost()
	{
		return alternateHost;
	}

	/**
	 * Returns the port on which to contact the Blackboard servers
	 */
	public int getPort()
	{
		return port;
	}

	/**
	 * Returns the Blackboard TIA protocol version implemented by the server
	 */
	public int getBlackboardProtocolVersion()
	{
		return tiaProtocolVersion;
	}

	/**
	 * Returns the number of messages in the outbound queue
	 */
	public int getNumMessagesWaitingToBeSent()
	{
		return outboundQueue.size();
	}

	/**
	 * Adds a TransactionRequestMessage to the outbound queue.
	 *
	 * @param outboundMessage a TransactionRequestMessage to send to the network
	 */
	private void queueOutboundMessage(TransactionRequestMessage outboundMessage)
	{
		TransactionRequestMessage messageAlreadySent = getDuplicateMessage(outboundMessage);
		outboundMessage.setTimeQueued(System.currentTimeMillis());
		if (messageAlreadySent == null)
		{
			Log.info(BlackboardProxy.class, "Queued: " + outboundMessage.toString());
			outboundQueue.add(outboundMessage);	//Add the message to the outbound queue
			synchronized (outboundQueue)
			{
				outboundQueue.notifyAll();	//notify thread that a  message is available to send
			}
		}
		return;
	}

	/**
	 * Contacts Blackboard server to do an authorization limit or balance inquiry request (depending on what the server
	 * supports) for the given account and returns the authorized value in pennies up to the amount requested.
	 *
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param requestedAmount amount for which to authorize
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param encryptionKey the key with which to encrypt the message
	 *
	 * @throws BlackboardException if there was a problem contacting the Blackboard server or interpreting its response
	 *
	 * @return int amount authorized for transaction, will not be greater than requested amount but could be lower
	 */
	public int authorize(int terminalNumber, int sequenceNumber, int tenderNumber, int requestedAmount,
		boolean manuallyEnteredId, String trackDataOrId, String pin, boolean onlineFlag, long transactionTime,
		TimeZone timeZone, byte[] encryptionKey) throws BlackboardException
	{
		statistics.numAuthorizationsAttempted++;
		byte messageType = ((tiaProtocolVersion == 2) ? MESSAGE_TYPE_AUTHORIZATION_LIMIT : MESSAGE_TYPE_BALANCE_INQUIRY);
		TransactionRequestMessage request = sendBlackboardRequest(messageType, terminalNumber, sequenceNumber,
			tenderNumber, 0, manuallyEnteredId, trackDataOrId, pin, onlineFlag, transactionTime, timeZone, encryptionKey);

		TransactionResponseMessage response = request.getResponse();
		if ((response.getResponseCode() != 1) && (response.getResponseCode() != 2))
		{
			throw new BlackboardException(response.getResponseCode(), response.getResponseText(),
				response.getResponseDescription());
		}

		//Get the amount we have available
		String authorizedAmountStr;
		authorizedAmountStr = response.getResponseText().toUpperCase();
		if (authorizedAmountStr.startsWith("BALANCE ") == true)
		{
			authorizedAmountStr = authorizedAmountStr.substring("BALANCE ".length()).trim();
		}
		else if (authorizedAmountStr.startsWith("LIMIT ") == true)
		{
			authorizedAmountStr = authorizedAmountStr.substring("LIMIT ".length()).trim();
		}
		else
		{
			throw new BlackboardException(response.getResponseCode(), response.getResponseText(),
				"Unable to interpret response.");
		}

		int decimalLocation = authorizedAmountStr.indexOf('.');
		if (decimalLocation == -1)
		{
			throw new BlackboardException(response.getResponseCode(), response.getResponseText(),
				"The response is not in the format documented by Blackboard. No decimal point was found in the amount returned.");
		}
		authorizedAmountStr = authorizedAmountStr.substring(0, decimalLocation) +
			authorizedAmountStr.substring(decimalLocation + 1, authorizedAmountStr.length());

		int authorizedAmount;
		try
		{
			authorizedAmount = Integer.parseInt(authorizedAmountStr);
		}
		catch (NumberFormatException nfx)
		{
			throw new BlackboardException(response.getResponseCode(), response.getResponseText(),
				"NumberFormatException caught while parsing amount.");
		}

		statistics.numAuthorizationsProcessed++;
		//Return the lower of requested amount or authorized amount
		//return (requestedAmount > authorizedAmount) ? authorizedAmount : requestedAmount;
		return authorizedAmount;
	}

	/**
	 * Contacts Blackboard server to do a debit from the given account.
	 *
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param amount amount to deposit in pennies
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param encryptionKey the key with which to encrypt the message
	 *
	 * @throws BlackboardException if debit failed or there was a problem contacting the Blackboard server or
	 * interpreting its response
	 */
	public void debit(int terminalNumber, int sequenceNumber, int tenderNumber, int amount, boolean manuallyEnteredId,
		String trackDataOrId, String pin, boolean onlineFlag, long transactionTime, TimeZone timeZone, byte[] encryptionKey)
		throws BlackboardException
	{
		statistics.numDebitsAttempted++;
		TransactionRequestMessage request = sendBlackboardRequest(MESSAGE_TYPE_DEBIT, terminalNumber, sequenceNumber,
			tenderNumber, amount, manuallyEnteredId, trackDataOrId, pin, onlineFlag, transactionTime, timeZone, encryptionKey);

		TransactionResponseMessage response = request.getResponse();
		if (response.getResponseCode() != 0)
		{
			throw new BlackboardException(response.getResponseCode(), response.getResponseText(),
				response.getResponseDescription());
		}
		statistics.numDebitsProcessed++;	//increment the count of debits processed successfully
		return;
	}

	/**
	 * Contacts Blackboard server to do a refund to the given account.
	 *
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param amount amount to refund in pennies
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param encryptionKey the key with which to encrypt the message
	 *
	 * @throws BlackboardException if refund failed or there was a problem contacting the Blackboard server or
	 * interpreting its response
	 */
	public void refund(int terminalNumber, int sequenceNumber, int tenderNumber, int amount, boolean manuallyEnteredId,
		String trackDataOrId, String pin, boolean onlineFlag, long transactionTime, TimeZone timeZone, byte[] encryptionKey)
		throws BlackboardException
	{
		statistics.numRefundsAttempted++;
		TransactionRequestMessage request = sendBlackboardRequest(MESSAGE_TYPE_REFUND, terminalNumber, sequenceNumber,
			tenderNumber, amount, manuallyEnteredId, trackDataOrId, pin, onlineFlag, transactionTime, timeZone, encryptionKey);

		TransactionResponseMessage response = request.getResponse();
		if (response.getResponseCode() != 0)
		{
			throw new BlackboardException(response.getResponseCode(), response.getResponseText(),
				response.getResponseDescription());
		}
		statistics.numRefundsProcessed++;
		return;
	}

	/**
	 * Queues a TransactionRequestMessage for sending to a Blackboard server and waits for the reply. Returns when the
	 * reply is received or throws a BlackboardException if no reply was received before the timeout expired.
	 *
	 * @param messageType defines the type of transaction, one of the MESSAGE_TYPE_* constants
	 * @param terminalNumber identifies the terminal that is initiating the transaction
	 * @param sequenceNumber number used to identify the request
	 * @param tenderNumber account to use for this transaction
	 * @param amount amount to refund in pennies
	 * @param manuallyEnteredId true if ID was manually entered, false if it came from a card swipe
	 * @param trackDataOrId contains a manually entered Id or the contents of track 2 from a magstripe card
	 * @param pin user's PIN number, if available, null otherwise
	 * @param onlineFlag true if this is an online transaction, false otherwise
	 * @param transactionTime the time the transaction took place in milliseconds since 1/1/1970
	 * @param encryptionKey the key with which to encrypt the message
	 *
	 * @throws BlackboardException if there was a problem contacting the Blackboard server or interpreting its response
	 *
	 * @return TransactionRequestMessage sent if successful
	 */
	private TransactionRequestMessage sendBlackboardRequest(byte messageType, int terminalNumber, int sequenceNumber,
		int tenderNumber, int amount, boolean manuallyEnteredId, String trackDataOrId, String pin, boolean onlineFlag,
		long transactionTime, TimeZone timeZone, byte[] encryptionKey) throws BlackboardException
	{
		TransactionRequestMessage request = new TransactionRequestMessage(tiaProtocolVersion, vendorNumber, terminalNumber, sequenceNumber,
			messageType, tenderNumber, amount, manuallyEnteredId, trackDataOrId, pin, onlineFlag, transactionTime, timeZone, encryptionKey);
		queueOutboundMessage(request);
		Thread.yield();

		//Wait for the response to be received. There is a delay before the message is actually sent. With
		//Blackboard you can only have one request pending at a time so there may be messages in the queue that are
		//waiting to be sent ahead of this message. Previous versions of this method had a delay that waited for
		//the message to be sent followed by a delay that waited for the response to be received. Both delays have
		//been combined here with a longer wait time.
		long sysTime = System.currentTimeMillis();
		//long timeToGiveUp = sysTime + (NETWORK_MSG_RESPONSE_TIMEOUT * 2);
		long timeToGiveUp = sysTime + NETWORK_MSG_RESPONSE_TIMEOUT;
		TransactionResponseMessage response = request.getResponse();
		while ((response == null) && (timeToGiveUp > System.currentTimeMillis()))
		{	//Guard against changes to system clock
			if (System.currentTimeMillis() < sysTime)
				break;
			try
			{
				synchronized(request)
				{
					request.wait(1000);
				}
			}
			catch (InterruptedException ie)
			{
			}
			response = request.getResponse();
		}

		if (response == null)
			throw new BlackboardException(lastConnectSuccess ? "Response timeout" : "Connection timeout");

		return request;
	}

	/**
	 * Calculates an LRC for an array of bytes, translated from C code provided by Blackboard.
	 * Default access, for use in this package only.
	 */
	static byte calcLRC(byte[] data, int dataLength)
	{
		byte lrc = 0;
		for (int i=0; i < dataLength; i++)
			lrc ^= (data[i] & 0xff);
		return lrc;
	}

	/**
	 * Calculates a CRC for an array of bytes, translated from C code provided by Blackboard.
	 * Default access, for use in this package only.
	 */
	static int calcCRC16(byte[] data, int dataLength)
	{
		int crc = 0;
		int temp = 0;

		for (int i = 0; i < dataLength; i++)
		{
			temp = (data[i] & 0xff) ^ ((crc & 0xff00) >>> 8);
			temp ^= (temp >>> 4);
			temp ^= (temp >>> 2);
			temp ^= (temp >>> 1);
			crc = ((crc << 8) ^ (temp << 15) ^ (temp << 2) ^ temp);
		}
		return crc & 0xffff;
	}

	/**
	 * Takes a byte array and returns a String containing printable hexadecimal representation.
	 */
	private String formatHex(byte[] byteArray)
	{
		StringBuffer buffer = new StringBuffer();

		for (int i=0; i < byteArray.length; i++)
		{
			buffer.append(Integer.toHexString((byteArray[i] & 0xf0) >>> 4));
			buffer.append(Integer.toHexString(byteArray[i] & 0xf));
			buffer.append(" ");
		}
		return buffer.toString().toUpperCase();
	}

	/**
	 * This method is for use by the management user interface.
	 */
	public BlackboardProxyStatistics getStatistics()
	{
		return statistics;
	}
}
