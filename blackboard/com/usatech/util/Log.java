package com.usatech.util;

//Import 3rd party classes
import org.apache.commons.logging.LogFactory;

/**
 * Provides a facility for logging information.  All logging takes place to log4j which can be redirected
 * to a file if desired.  All log messages include a severity indicator that indicates if the error is of type
 * debug (D), error (E), info (I), security (S) or warning (W).  This not only identifies the severity of what is being
 * logged but will also facilitate sorting and/or filtering of the log by severity.
 *
 * @author  Steve Lukshides - IBM Global Servcices - 2003
 * @version 1.0
 *
 * Modifications:
 * 02/08/2005 - dkouznetsov - Migration to Apache Axis
 */
public class Log
{
	private static Log logInstance = null;
	private static org.apache.commons.logging.Log log = LogFactory.getLog("BlackboardGateway");

	/**
	 * This class is entirely static, a Log object cannot be instantiated.
	 */
	private Log()
	{
	}

	/**
	 * Used to log debugging messages
	 *
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void debug(Class classGeneratingMessage, String message)
	{
	    if (log.isDebugEnabled())
	    {
	        log.debug(classGeneratingMessage.getName() + " - D: " + message);
	    }
	}

	/**
	 * Used to log error messages
	 *
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void error(Class classGeneratingMessage, String message)
	{
	    if (log.isErrorEnabled())
	    {
	        log.error(classGeneratingMessage.getName() + " - E: " + message);
	    }
	}

	/**
	 * Used to log informational messages
	 *
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void info(Class classGeneratingMessage, String message)
	{
	    if (log.isInfoEnabled())
	    {
	        log.info(classGeneratingMessage.getName() + " - I: " + message);
	    }
	}

	/**
	 * Used to log security related messages
	 *
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void security(Class classGeneratingMessage, String message)
	{
	    if (log.isWarnEnabled())
	    {
	        log.warn(classGeneratingMessage.getName() + " - S: " + message);
	    }
	}

	/**
	 * Used to log warning messages
	 *
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void warn(Class classGeneratingMessage, String message)
	{
	    if (log.isWarnEnabled())
	    {
	        log.warn(classGeneratingMessage.getName() + " - W: " + message);
	    }
	}
}
