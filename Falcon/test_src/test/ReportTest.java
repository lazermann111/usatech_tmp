/*
 * Created on Nov 19, 2004
 *
 */
package test;

import simple.io.Log;

/**
 * @author bkrug
 *
 */
public class ReportTest {

    public static void main(String[] args) throws Throwable {
        System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
        //System.out.println("Match = " + RegexUtils.matches("((abc)|(def)|(ghi)).*", "defd", null));
        /*Set unjoined = new MapBackedSet(new simple.util.ConcurrentTreeMap(new CaseInsensitiveComparator())); //allows concurrent modifications
        unjoined.addAll(Arrays.asList(new String[]{"a", "b", "c"}));
        System.out.println("Set=" + unjoined);*/
        //System.out.println(new ExcludingMap(ReflectionUtils.toPropertyMap(new Operator.BaseOperator(1, "=", null, null, null, 1, 0, false)), Collections.singleton("class")));
        
        //test speed of messageformat
        /*
        for(int k = 0; k < 5; k++) {
	        Object[] values = new Object[1000000];
	        Arrays.fill(values, "This is one");
	        Format fmt = new ToStringFormat();
	        //Format fmt = new MessageFormat("{0}");
	        long start = System.currentTimeMillis();
	        for(int i = 0; i < values.length; i++)  {
	            //Object[] tmp = new Object[] {values[i]};
	            //String text = fmt.format(values[i]);
	            String text = (values[i] == null ? "" : values[i].toString());
	            //String text = (tmp[0] == null ? "" : tmp[0].toString());
	        }
	        System.out.println("Formatted " + values.length + " entries in " + (System.currentTimeMillis() - start) + " ms");
        }
        */
        //test xsl conversion
        //String xml = "<book><page><text>This is the story so there you are<note>12</note></text></page></book>";
        /*
        String xml = "<symbols><letter>A</letter><letter>B</letter><number>1</number><number>2</number><letter>C</letter><letter>D</letter><number>3</number><letter>E</letter></symbols>";        
        InputStream xsl = ReportTest.class.getClassLoader().getResourceAsStream("test/testNote.xsl");
        Transformer t = TransformerFactory.newInstance().newTransformer(new StreamSource(xsl));
        t.transform(new StreamSource(new StringBufferInputStream(xml)), new StreamResult(System.out));
        */
        /*
        //test date formatter
        String[] dates = new String[] {
               "10/2005","070405 111530", "08-03-1975 2:15:33.123", "09/01/03 b.c.", "10-02-2003 14-20-33 mst", "11-12-2005 12:30 P.M. CST", "{-1092304}", "{2000}" , "{*-3600000}", "{*60000}" 
        };
        XDateConverter conv = new XDateConverter();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy G HH:mm:ss.SSS z");
        for(int i = 0; i < dates.length; i++) {
            java.util.Date d = (java.util.Date)conv.convert(dates[i]);
            System.out.println("Date '" + dates[i] + "' = '" + df.format(d) + "'");
        }*/
//      System.out.println("SqlType 2 = " + ConfigLoader.getSqlTypeName(2));
        /*
        DataSourceFactory dsf = new simple.db.BasicDataSourceFactory(App.getProperties("ReportTest"));
        DataLayerMgr.setDataSourceFactory(dsf);

        final URL baseURL = new URL("file:///C:/Java Projects/Falcon/web");
        ConfigLoader.loadConfig(ReportTest.class.getClassLoader().getResourceAsStream("simple/falcon/engine/design-data-layer.xml"));
        DesignEngine de = new DesignEngine();
        ExecuteEngine ee = new ExecuteEngine(de);
        ee.setDataSourceFactory(dsf);
        ee.setResourceResolver(new ResourceResolver(){
            public URL getResourceURL(String path) throws MalformedURLException {
                if(!path.startsWith("/")) path = "/" + path;
                return new URL(baseURL.toString() + path);
            }
        });
        OutputStream out = new FileOutputStream("C:/TEMP/report-41.xml");
        Map params = new HashMap();
        params.put("AsOfDate", "02/2005");        
        ee.buildReport(de.retrieveFolio(41), params, new long[] {1}, de.getOutputType(25), out);
        */
        /*
        Class cls = Class.forName("J");
        Object o = new long[] {1,2,3};
        System.out.println("ClassName='" + o.getClass().getName() + "'");
        *//*
        DataSourceFactory dsf = new simple.db.BasicDataSourceFactory(App.getProperties("Falcon"));
        DataLayerMgr.setDataSourceFactory(dsf);
        ConfigLoader.loadConfig(ReportTest.class.getClassLoader().getResourceAsStream("simple/falcon/engine/design-data-layer.xml"));
        Results results = DataLayerMgr.executeQuery("GET_OPERATORS_OLD", null, false);
        while(results.next()) {
            int operatorId = ConvertUtils.convert(int.class, results.getValue("operatorId"));
            String op = ConvertUtils.convert(String.class, results.getValue("op"));
            int parameterCount = ConvertUtils.convert(int.class, results.getValue("parameterCount"));
            String prefix = ConvertUtils.getString(results.getValue("prefix"), "");
            String suffix = ConvertUtils.getString(results.getValue("suffix"), "");
            String sep = ConvertUtils.convert(String.class, results.getValue("sep"));           
            System.out.println("INSERT INTO FILTER_OPERATOR(FILTER_OPERATOR_ID, OPERATOR_NAME, OPERATOR_DESCRIPTION, OPERATOR_PATTERN, SQL_TYPES)" +
                    "\n\tVALUES(" + operatorId + ", '" + prefix + op + suffix + "', '', '" + toPattern(operatorId, op, sep, prefix, suffix, parameterCount, 0, false)
                    + "', NULL);");
        }*/
        //String[] s = "ARRAY OF STING_LIST".split(":|(\\s+[oO][fF]\\s+)", 2);
        /*
        String s = "1,2,33,,5,,,6";
        System.out.println("String " + s + " turns into array " + Arrays.asList(ConvertUtils.convert(Integer[].class, s)));
        System.out.println("String " + s + " turns into array " + new PrimitiveArrayList(ConvertUtils.convert(int[].class, s)));
        */
        //System.out.println("Arr=" + Arrays.asList(s));
        //Object o = new D() {};
        //System.out.println("Classes of " + o.getClass().getName() + ": " + Arrays.asList(o.getClass().getInterfaces()));
        //System.out.println("Classes of " + o.getClass().getName() + ": " + Arrays.asList(ReflectionUtils.getSuperTypes(o.getClass())));
        /*
        System.out.println("DEBUG=" + Log.getLog(ReflectionUtils.class.getName()).isDebugEnabled());
        Object o = new Interface() {
            public String getSomething() {
                return "This does work";
            }
        };
        try {
        System.out.println("SOMETHING=" + ReflectionUtils.getProperty(o, "something"));
        } catch(Exception e) {
            e.printStackTrace();
        }//*/
        /*
        Collection<String> PARAMETER_NAMES = StaticCollection.create(new String[] {
                "display", "sort", "0"
        });
        System.out.println(PARAMETER_NAMES);
        System.out.println("***DONE***");
        Log.finish();
        */
        //AddProtocols.run();
        //System.out.println(simple.text.RegexUtils.matches("^[a-zA-Z]*:.*", "resource:simple/falcon/templates/report/show-report-pdf.xsl", null));
        //System.out.println(new URL("resource:simple/falcon/templates/report/show-report-pdf.xsl"));
        //Thread.sleep(3000);
        Log log = Log.getLog();
        for(int i = 1; i <= 20; i++) {
            log.debug("" + i);
        }
        Log.finish();
    }
    
    public static interface Interface {
        public String getSomething() ;
    }
    
    public static interface A {}
    public static interface B extends A {}
    public static interface C extends B {}
    public static interface D extends C {}
    
    protected static String toPattern(int operatorId, String op, String sep, String prefix, String suffix, int paramCount, int fieldIndex, boolean parens) {
        if(sep == null || sep.trim().length() == 0) sep = " ";
        else sep = " " + sep.trim() + " ";
        sep = sep == null ? "" : sep;
        String s = "";
        for(int i = 0; i < paramCount + 1; i++) {
            if(prefix != null) s += prefix;
            if(i == fieldIndex) s += "{display}";
            else s += "?";
            if(suffix != null) s += suffix;                
            if(i == 0) {
                s += " " + op;
                if(parens) s += "(";
                else if(paramCount != 0) s += " ";
            } else if(i < paramCount) {
                s += sep;
            }
        }
        if(parens) s += ")";
        return s;            
    }

}
