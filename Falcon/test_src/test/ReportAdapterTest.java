///*
// * Created on Nov 10, 2005
// *
// */
//package test;
//
//import java.io.IOException;
//import java.io.OutputStream;
//import java.io.PrintWriter;
//import java.io.Writer;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Properties;
//
//import javax.xml.transform.Result;
//import javax.xml.transform.stream.StreamResult;
//
//import simple.db.BasicDataSourceFactory;
//import simple.db.DataLayerMgr;
//import simple.db.DataSourceFactory;
//import simple.db.config.ConfigLoader;
//import simple.falcon.engine.DesignEngine;
//import simple.falcon.engine.ExecuteEngine;
//import simple.falcon.engine.Executor;
//import simple.falcon.engine.Folio;
//import simple.falcon.engine.Output;
//import simple.falcon.engine.Report;
//import simple.falcon.engine.ResultsType;
//import simple.falcon.engine.Scene;
//import simple.falcon.engine.standard.StandardDesignEngine;
//import simple.falcon.engine.standard.StandardExecuteEngine;
//import simple.net.protocol.AddProtocols;
//
//public class ReportAdapterTest {
//
//    /**
//     * @param args
//     */
//    public static void main(String[] args) throws Throwable {
//        AddProtocols.run();
//        System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.processor.TransformerFactoryImpl");
//        Properties props = new Properties();
//        addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
//        addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "FOLIO_CONF", "FOLIO_CONF");
//        DataSourceFactory dsf = new BasicDataSourceFactory(props);
//        DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
//        ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml");
//        DesignEngine de = new StandardDesignEngine();
//        ExecuteEngine ee = new StandardExecuteEngine(de);
//        ee.setDataSourceFactory(dsf);
//
//        Report report = de.newReport();
//        report.setFolios(new Folio[] {de.getFolio(489)});
//        report.setOutputType(de.getOutputType(22));
//        Map<String,Object> parameters = new HashMap<String, Object>();
//        Output out = new Output() {
//            public Writer getWriter() throws IOException {
//                return new PrintWriter(System.out);
//            }
//
//            public OutputStream getOutputStream() throws IOException {
//                return System.out;
//            }
//
//			public Result getResult() throws IOException {
//				return new StreamResult(System.out);
//			}
//        };
//        Executor executor = new Executor() {
//            public long[] getUserGroupIds() {
//                return new long[] {1};
//            }
//
//            public long getUserId() {
//                return 1;
//            }
//            public Map<String, String> getUserProperties() {
//                return Collections.EMPTY_MAP;
//            }
//
//			//@Override
//			public String getDisplayName() {
//				return null;
//			}
//        };
//        ee.buildReport(report, parameters, executor, report.getOutputType(), new Scene(), out, ResultsType.FORWARD);
//    }
//
//    private static void addDS(Properties props, String name, String driver, String url, String username, String password) {
//        props.put(name + "." + BasicDataSourceFactory.PROP_DATABASE, url);
//        props.put(name + "." + BasicDataSourceFactory.PROP_DRIVER, driver);
//        props.put(name + "." + BasicDataSourceFactory.PROP_USERNAME, username);
//        props.put(name + "." + BasicDataSourceFactory.PROP_PASSWORD, password);
//    }
//
//}
