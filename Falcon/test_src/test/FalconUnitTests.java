//package test;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.io.OutputStream;
//import java.io.PrintWriter;
//import java.io.Writer;
//import java.lang.management.ManagementFactory;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Locale;
//import java.util.Map;
//import java.util.Properties;
//import java.util.TimeZone;
//import java.util.Timer;
//import java.util.TimerTask;
//import java.util.zip.Deflater;
//import java.util.zip.DeflaterOutputStream;
//import java.util.zip.Inflater;
//import java.util.zip.InflaterInputStream;
//
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.transform.Result;
//import javax.xml.transform.stream.StreamResult;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.xml.sax.SAXException;
//
//import simple.bean.ConvertException;
//import simple.db.CacheableCall;
//import simple.db.Call;
//import simple.db.Cursor;
//import simple.db.DataLayerException;
//import simple.db.DataLayerMgr;
//import simple.db.DataSourceFactory;
//import simple.db.DataSourceNotFoundException;
//import simple.db.ParameterException;
//import simple.db.config.ConfigException;
//import simple.db.config.ConfigLoader;
//import simple.db.dbcp.DBCPDataSourceFactory;
//import simple.event.TaskListener;
//import simple.event.WriteCSVTaskListener;
//import simple.falcon.engine.DesignEngine;
//import simple.falcon.engine.DesignException;
//import simple.falcon.engine.ExecuteEngine;
//import simple.falcon.engine.ExecuteException;
//import simple.falcon.engine.Executor;
//import simple.falcon.engine.Folio;
//import simple.falcon.engine.Generator;
//import simple.falcon.engine.Layout;
//import simple.falcon.engine.Output;
//import simple.falcon.engine.Report;
//import simple.falcon.engine.Scene;
//import simple.falcon.engine.standard.DirectGenerator;
//import simple.falcon.engine.standard.StandardDesignEngine;
//import simple.falcon.engine.standard.StandardDesignEngine2;
//import simple.falcon.engine.standard.StandardExecuteEngine;
//import simple.falcon.engine.standard.XSLGenerator;
//import simple.falcon.run.BasicExecutor;
//import simple.falcon.run.ReportRunner;
//import simple.io.IOUtils;
//import simple.io.LoadingException;
//import simple.io.Log;
//import simple.net.protocol.AddProtocols;
//import simple.results.BeanException;
//import simple.results.CacheableResults;
//import simple.results.CachedForwardOnlyResults;
//import simple.results.CachedResultsCreator.EOFMarker;
//import simple.results.Results;
//import simple.sql.SQLType;
//import simple.test.UnitTest;
//import simple.translator.DBFullTranslatorFactory;
//import simple.translator.DefaultTranslatorFactory;
//import simple.translator.TranslatorFactory;
//import simple.xml.ObjectBuilder;
//
//public class FalconUnitTests extends UnitTest {
//	protected StandardDesignEngine designEngine;
//	protected ExecuteEngine executeEngine;
//	protected static class FileOutput implements Output {
//		protected final File file;
//
//        public FileOutput(File file) {
//			super();
//			this.file = file;
//		}
//		public Writer getWriter() throws IOException {
//            return new FileWriter(file);
//        }
//        public OutputStream getOutputStream() throws IOException {
//            return new FileOutputStream(file);
//        }
//		public Result getResult() throws IOException {
//			return new StreamResult(getWriter());
//		}
//    }
//
//	@Before
//	public void setUp() throws Exception {
//		setupLog();
//		System.setProperty("javax.xml.transform.TransformerFactory", "com.jclark.xsl.trax.TransformerFactoryImpl");
//        AddProtocols.run();
//	}
//
//	protected void setupEngine() throws ConfigException, IOException, LoadingException, SQLException, DataLayerException, ConvertException, BeanException, DesignException {
//        Properties props = new Properties();
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "FOLIO_CONF", "FOLIO_CONF");
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(FAILOVER=TRUE)(LOAD_BALANCE=FALSE)(ADDRESS=(PROTOCOL=TCP)(HOST=10.0.0.45)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=10.0.0.49)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USARDB.world)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC)(RETRIES=1)(DELAY=5))))", "REPORT", "USATECH");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@10.0.0.45:1521:usardb01", "FOLIO_CONF", "fc45popwq");
//		//addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))", "USALIVE_APP_1", "1_PPA_EVILASU");
//		addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "USALIVE_APP_1", "1_PPA_EVILASU");
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@usadbd02.usatech.com:1531:usadbd02", "REPORT", "USATECH");
//		addDS(props, "MAIN", "org.postgresql.Driver", "jdbc:postgresql://devdbs11:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true", "usalive_2", "USALIVE_1");
//		addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "FOLIO_CONF", "FOLIO_CONF");
//		//addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))", "FOLIO_CONF", "FOLIO_CONF");
//        DataSourceFactory dsf = new DBCPDataSourceFactory(props);
//        DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
//        ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml");
//        final File taskTimesFile = File.createTempFile("TaskTimes-", ".csv");
//        PrintWriter pw = new PrintWriter(taskTimesFile);
//        final StandardDesignEngine sde2 = new StandardDesignEngine2();
//        executeEngine = new StandardExecuteEngine(sde2);
//        executeEngine.setDataSourceFactory(dsf);
//		// sde2.init(dsf.getDataSource("REPORT"), dsf.getDataSource("REPORT"));
//
//        TaskListener tl2 = new WriteCSVTaskListener(pw) {
//			@Override
//			protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
//				super.taskRan(taskName, taskDetails, threadId, "Engine #2", startTime, endTime);
//			}
//        };
//        executeEngine.setTaskListener(tl2);
//
//        executeEngine.setCssDirectory("file:///E:/Java Projects/usalive/web/");
//	}
//	@Test
//	public void testExpressionParsing() throws Throwable {
//		setupEngine();
//		String[] expressions = new String[] {
//				"REPORT.EPORT.EPORT_SERIAL_NUM",
//				"DECODE(:param1, 'A', 'B', '1', '2', 'h')",
//				":param2::NUMBER(9) + 3",
//				":param3::TIMESTAMP"
//		};
//		for(String exp : expressions) {
//			SQLType[] sqltypes = executeEngine.getDesignEngine().validateFieldExpressions(exp, exp);
//			log.info("Expression '" + exp + "' has sqltype " + sqltypes[0]);
//		}
//	}
//
//	@Test
//	public void testCachedFileConvert() throws Throwable {
//		File cacheFile = new File("E:/TEMP/cached-results-42554.dat"); //"E:/TEMP/cached-results-62709.dat");
//		File targetFile = new File("E:/TEMP/cached-results-42554-expand.dat"); //"E:/TEMP/cached-results-62709.dat");
//		IOUtils.copy(new InflaterInputStream(new FileInputStream(cacheFile), new Inflater(), 8192), new FileOutputStream(targetFile));
//
//	}
//	@Test
//	public void testCachedFileRead() throws Throwable {
//		File cacheFile = new File("E:/TEMP/cached-results-42554.dat"); //"E:/TEMP/cached-results-62709.dat");
//		final File taskTimesFile = File.createTempFile("TaskTimes-", ".csv");
//        PrintWriter pw = new PrintWriter(taskTimesFile);
//        TaskListener tl = new WriteCSVTaskListener(pw) ;
//		//File cacheFile = new File("E:/TEMP/cached-results-42554-expand.dat"); //"E:/TEMP/cached-results-62709.dat");
//		/*
//        for(int i = 0; i < 8; i++) {
//			for(int k = 0; k < 8; k++) {
//				for(int t = 0; t < 10; t++) {
//					testCacheFileRead(cacheFile, (int)Math.pow(2, i), (int)Math.pow(2, k), true, tl);
//				}
//			}
//		}
//		*/
//		for(int t = 0; t < 5; t++) {
//			testCacheFileRead(cacheFile, 512, 512, true, tl);
//		}
//		for(int t = 0; t < 5; t++) {
//			testCacheFileRead(cacheFile, 512, 1024, true, tl);
//		}
//		tl.flush();
//		pw.flush();
//		log.debug("Tests Complete");
//	}
//
//	@Test
//	public void testCachedFileWrite() throws Throwable {
//		File cacheFile = new File("E:/TEMP/cached-results-42554.dat"); //"E:/TEMP/cached-results-62709.dat");
//		final File taskTimesFile = File.createTempFile("TaskTimes-", ".csv");
//        PrintWriter pw = new PrintWriter(taskTimesFile);
//        TaskListener tl = new WriteCSVTaskListener(pw) ;
//		//File cacheFile = new File("E:/TEMP/cached-results-42554-expand.dat"); //"E:/TEMP/cached-results-62709.dat");
//		/*
//        for(int i = 0; i < 8; i++) {
//			for(int k = 0; k < 8; k++) {
//				for(int t = 0; t < 10; t++) {
//					testCacheFileRead(cacheFile, (int)Math.pow(2, i), (int)Math.pow(2, k), true, tl);
//				}
//			}
//		}
//		*/
//        Object[][] data = new Object[10000][];
//        ObjectInputStream cacheStream = new ObjectInputStream(new BufferedInputStream(new InflaterInputStream(new FileInputStream(cacheFile), new Inflater(), 512), 1024));
//		for(int i = 0; i < data.length; i++) {
//		    Object o = cacheStream.readUnshared();
//		    try {
//		    	data[i] = (Object[]) o;
//		    } catch(ClassCastException e) {
//		        if(o instanceof EOFMarker) {
//		            log.debug("Read end of file marker at row " + (i+1) + " while preset size = " + data.length);
//		        } else
//		            throw e;
//		    }
//		}
//        int times = 2;
//		for(int t = 0; t < times; t++) {
//			testCacheFileWrite(data, 1024, 1024, true, tl);
//		}
//		for(int t = 0; t < times; t++) {
//			testCacheFileWrite(data, 0, 1024, true, tl);
//		}
//		for(int t = 0; t < times; t++) {
//			testCacheFileWrite(data, 1024, 0, true, tl);
//		}
//		for(int t = 0; t < times; t++) {
//			testCacheFileWrite(data, 0, 0, true, tl);
//		}
//		tl.flush();
//		pw.flush();
//		log.debug("Tests Complete");
//	}
//
//	protected void testCacheFileWrite(Object[][] data, int deflaterBuffer, int bufferBuffer, boolean compression, TaskListener taskListener) throws IOException, ClassNotFoundException {
//		File file = File.createTempFile("cached-results-test-", ".dat");
//		OutputStream os = new FileOutputStream(file);
//		if(compression) {
//			if(deflaterBuffer > 0)
//				os = new BufferedOutputStream(os, deflaterBuffer);
//			 os = new DeflaterOutputStream(os, new Deflater(1), 512);
//		}
//	    if(bufferBuffer > 0)
//	        os = new BufferedOutputStream(os, bufferBuffer);
//	    ObjectOutputStream cacheStream = new ObjectOutputStream(os);
//	    String s = "with " + (compression ? " compression and deflater buffer of " + deflaterBuffer : "no compression")
//		+ " and normal buffer of " + bufferBuffer;
//		log.debug("Start writing " + s);
//		long start = System.currentTimeMillis();
//		taskListener.taskStarted("Write File", s);
//		for(Object[] row : data) {
//	    	cacheStream.writeUnshared(row);
//	    }
//	    cacheStream.flush();
//	    taskListener.taskEnded("Write File");
//		long time = System.currentTimeMillis() - start;
//		log.debug("Wrote " + data.length + " rows in " + time + " milliseconds " + s);
//	}
//	protected void testCacheFileRead(File file, int inflaterBufferK, int bufferBufferK, boolean compression, TaskListener taskListener) throws IOException, ClassNotFoundException {
//		InputStream in = new FileInputStream(file);
//		if(compression)
//			in = new InflaterInputStream(in, new Inflater(), inflaterBufferK);
//		if(bufferBufferK > 0)
//			in = new BufferedInputStream(in, bufferBufferK);
//		ObjectInputStream cacheStream = new ObjectInputStream(in);
//		int lastReadRow = 0;
//		int rows = -1;
//		boolean atEnd = false;
//		String s = "'" + file.getPath() + "' with " + (compression ? " compression and inflater buffer of " + inflaterBufferK + "K" : "no compression")
//				+ " and normal buffer of " + bufferBufferK + "K";
//		log.debug("Start reading from " + s);
//		long start = System.currentTimeMillis();
//		taskListener.taskStarted("Read File", s);
//		while(!atEnd) {
//			if(rows >= 0 && lastReadRow >= rows) break;
//		    Object o = cacheStream.readUnshared();
//		    try {
//		        Object[] row = (Object[]) o;
//		        if(++lastReadRow - 1 == rows && rows >= 0) {
//		            log.debug("Reached end of file");
//		            //cacheStream.close();
//		            //cacheStream = null;
//		            //cacheFile = null;
//		            atEnd = true;
//		        }
//		    } catch(ClassCastException e) {
//		        if(o instanceof EOFMarker) {
//		            log.debug("Read end of file marker at row " + lastReadRow + " while preset size = " + rows);
//		            //cacheStream.close();
//		            //cacheStream = null;
//		            //cacheFile = null;
//		            atEnd = true;
//		        } else
//		            throw e;
//		    }
//		}
//		taskListener.taskEnded("Read File");
//		long time = System.currentTimeMillis() - start;
//		log.debug("Read " + lastReadRow + " rows in " + time + " milliseconds from " + s);
//	}
//	@Test
//	public void testDirectGenerator() throws Throwable {
//		setupEngine();
//		testGenerator(new DirectGenerator(executeEngine));
//	}
//
//	@Test
//	public void testXSLGenerator() throws Throwable {
//		setupEngine();
//		testGenerator(new XSLGenerator(executeEngine));
//	}
//
//	@Test
//	public void testCachingResults() throws Throwable {
//		setupEngine();
//		Folio folio = executeEngine.getDesignEngine().getFolio(1605);
//    	CacheableCall call = executeEngine.getDesignEngine().getCall(folio, new long[] {1});
//		Map<String,Object> parameters = new HashMap<String, Object>();
//		parameters.put("StartDate", "06/01/2008");
//		parameters.put("EndDate", "06/30/2008");
//		parameters.put("CustomerName", "Coca-Cola Enterprises, Inc.");
//		Connection conn = executeEngine.getDataSourceFactory().getDataSource(executeEngine.getDefaultDataSourceName()).getConnection();
//		try {
//			CacheableResults results = (CacheableResults)call.executeQuery(conn, parameters, false, null);
//			results.getCachedResultsCreator().createResults();
//		} finally {
//			conn.close();
//		}
//	}
//	protected void testGenerator(Generator generator) throws IOException, ExecuteException, DesignException, DataSourceNotFoundException, SQLException, ParameterException {
//		Folio folio = executeEngine.getDesignEngine().getFolio(1605);
//    	Report report = wrapWithReport(folio, executeEngine.getDesignEngine());
//		File resultsFile = new File("E:/TEMP/cached-results-42554.dat"); //"E:/TEMP/cached-results-62709.dat");
//		CacheableCall call = executeEngine.getDesignEngine().getCall(folio, new long[] {1});
//		Map<String,Object> parameters = new HashMap<String, Object>();
//		parameters.put("StartDate", "06/01/2008");
//		parameters.put("EndDate", "06/30/2008");
//		parameters.put("CustomerName", "Coca-Cola Enterprises, Inc.");
//		Results results = new CachedForwardOnlyResults(((Cursor)call.getArgument(0)).getColumns(), resultsFile, -1, 1);
//		Results[] resultsArr = new Results[] {results};
//		File memoryFile = File.createTempFile("Memory-", ".csv");
//        final PrintWriter pw = new PrintWriter(memoryFile);
//        pw.println("System Time,Date,Generator,Heap Memory Used, Non-heap Memory Used");
//        Timer timer = new Timer(true);
//		final String className = generator.getClass().getName();
//		final DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
//		timer.schedule(new TimerTask() {
//			@Override
//			public void run() {
//				//print memory usage
//				long time = System.currentTimeMillis();
//				pw.print(time);
//				pw.print(',');
//				pw.print(df.format(new Date(time)));
//				pw.print(',');
//				pw.print(className);
//				pw.print(',');
//				pw.print(ManagementFactory.getMemoryMXBean().getHeapMemoryUsage());
//				pw.print(',');
//				pw.print(ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage());
//				pw.println();
//				pw.flush();
//			}
//		}, 0, 1000);
//		generateReport(generator, report, resultsArr, new BasicExecutor(), createScene(), parameters);
//		log.debug("Calling GC");
//		System.gc();
//		log.debug("Waiting for a few seconds");
//		try {
//			Thread.sleep(5000);
//		} catch(InterruptedException e) {
//			log.warn("Interrupted", e);
//		}
//		pw.flush();
//		pw.close();
//	}
//
//	@Test
//	public void testSql_custom() throws FileNotFoundException, IOException, SAXException, ParserConfigurationException, DesignException, ConfigException, LoadingException, SQLException, DataLayerException, ConvertException, BeanException {
//		setupEngine();
//		String filePath = "H:/4 Brian K/dex_status_folio.xml";
//		// String filePath = "C:/USers/bkrug/Documents/dex_report_def.xml";
//		Folio folio;
//		ObjectBuilder builder = new ObjectBuilder(new FileInputStream(filePath));
//		if("report".equals(builder.getTag())) {
//			Report report = executeEngine.getDesignEngine().newReport();
//			report.readXML(builder);
//			Folio[] folios = report.getFolios();
//			if(folios != null && folios.length > 0)
//				folio = folios[0];
//			else {
//				log.warn("Report contains zero folios");
//				return;
//			}
//		} else if("folio".equals(builder.getTag())) {
//			folio = executeEngine.getDesignEngine().newFolio();
//			folio.readXML(builder);
//		} else {
//			log.warn("XML does not have 'report' or 'folio' as top level");
//			return;
//		}
//		try {
//			Call call = executeEngine.getDesignEngine().getCall(folio, new long[] { 8 });
//			log.info("SQL for Folio:\n" + call.getSql());
//		} catch(DesignException e) {
//			log.warn("Could not get call for folio", e);
//		}
//	}
//	@Test
//	public void testSql_id() throws FileNotFoundException, IOException, SAXException, ParserConfigurationException, DesignException, ConfigException, LoadingException, SQLException, DataLayerException, ConvertException, BeanException {
//		setupEngine();
//		long folioId = 1464;
//		Folio folio = executeEngine.getDesignEngine().getFolio(folioId);
//		try {
//			Call call = executeEngine.getDesignEngine().getCall(folio, new long[] { 8, 100, 120 });
//			log.info("SQL for Folio " + folioId + ":\n" + call.getSql());
//		} catch(DesignException e) {
//			log.warn("Could not get call for folio " + folioId, e);
//		}
//	}
//
//	@Test
//	public void testGenerators() throws IOException, ExecuteException, DesignException, SQLException, ConfigException, LoadingException, DataLayerException, ConvertException, BeanException {
//		setupEngine();
//		Folio folio = executeEngine.getDesignEngine().getFolio(1605);
//    	Report report = wrapWithReport(folio, executeEngine.getDesignEngine());
//		File resultsFile = new File("E:/TEMP/cached-results-42554.dat"); //"E:/TEMP/cached-results-62709.dat");
//		CacheableCall call = executeEngine.getDesignEngine().getCall(folio, new long[] {1});
//		Map<String,Object> parameters = new HashMap<String, Object>();
//		parameters.put("StartDate", "06/01/2008");
//		parameters.put("EndDate", "06/30/2008");
//		parameters.put("CustomerName", "Coca-Cola Enterprises, Inc.");
//
//		Generator[] generators = new Generator[] {new XSLGenerator(executeEngine), new DirectGenerator(executeEngine) };
//		for(int i = 0; i < 3; i++)
//			for(Generator generator : generators) {
//				Results results = new CachedForwardOnlyResults(((Cursor)call.getArgument(0)).getColumns(), resultsFile, -1, 1);
//				Results[] resultsArr = new Results[] {results};
//				generateReport(generator, report, resultsArr, new BasicExecutor(), createScene(), parameters);
//			}
//		log.debug("Tests Complete");
//	}
//
//	@Test
//	public void testScene() throws ConfigException, IOException, LoadingException, SQLException, DataLayerException, ConvertException, BeanException, DesignException, ExecuteException {
//		setupEngine();
//		log.info("Current Directory: " + new File("").getAbsolutePath());
//		ConfigLoader.loadConfig("file:../ReportGenerator/src/com/usatech/report/rgg-data-layer.xml");
//		DBFullTranslatorFactory tf = new DBFullTranslatorFactory();
//		tf.setCallId("GET_ALL_TRANSLATIONS");
//		TranslatorFactory.setDefaultFactory(tf);
//		ReportRunner rr = new ReportRunner(executeEngine);
//		Scene scene = rr.createScene(TimeZone.getDefault(), Locale.getDefault(), "https://usalive.usatech.com", "run_report.i", "../usalive/web/", false, Layout.OFFLINE);
//		log.info("Stylesheets=" + Arrays.toString(scene.getStylesheets()) + "; Scripts=" + Arrays.toString(scene.getScripts()));
//	}
//	protected Scene createScene() {
//		Scene scene = new Scene();
//        scene.setBaseUrl("http://localhost:8580/usalive/");
//        //scene.setStylesheets(new String[] {"css/default-style.css"});
//        scene.setStylesheets(new String[] {
//                "file:///E:/Java Projects/usalive/web/css/usalive-general-style.css",
//                "file:///E:/Java Projects/usalive/web/css/usalive-report-style.css"
//            });
//
//        //scene.setStylesheets(new String[] {});
//        scene.setTranslator(DefaultTranslatorFactory.getTranslatorInstance());
//        //scene.setLocale(?);
//        return scene;
//	}
//
//	protected void generateReport(Generator generator, Report report, Results[] results, Executor executor, Scene scene, Map<String,Object> parameters) throws IOException, ExecuteException {
//		TaskListener taskListener = executeEngine.getTaskListener();
//		String reportDesc;
//		{
//			StringBuilder sb = new StringBuilder();
//        	if(report.getReportId() != null)
//        		sb.append("ReportId=").append(report.getReportId());
//        	else {
//        		sb.append("FolioId=");
//        		for(Folio folio : report.getFolios()) {
//        			sb.append(folio.getFolioId()).append(',');
//        		}
//        		sb.setLength(sb.length()-1);
//        	}
//        	reportDesc = sb.toString();
//		}
//		if(taskListener != null) {
//        	taskListener.taskStarted("generate-report", "Generator=" + generator + "; " + reportDesc + "; OutputType=" + report.getOutputType().getLabel());
//        }
//		String fn = report.getOutputType().generateFileName(report, parameters).replaceAll("[\\s/\\\\]", "_");
//		String prefix, suffix;
//		int pos = fn.lastIndexOf('.');
//		if(pos > 0) {
//			prefix = fn.substring(0, pos);
//			suffix = fn.substring(pos);
//		} else {
//			prefix = fn;
//			suffix = "";
//		}
//		final File reportFile = File.createTempFile(prefix + '(',  ")" + suffix);
//		generator.generate(report, report.getOutputType(), parameters, results, executor, scene, new FileOutput(reportFile));
//        if(taskListener != null) taskListener.taskEnded("generate-report");
//        log.info("Wrote the report '" + reportDesc + "' to " + reportFile.getAbsolutePath());
//	}
//	protected Report wrapWithReport(Folio folio, DesignEngine designEngine) throws DesignException {
//		Report report = designEngine.newReport();
//        report.setFolios(new Folio[] {folio});
//        report.setOutputType(folio.getOutputType());
//        return report;
//	}
//
//	@Test
//	public void testNullParamNames() throws Exception {
//		setupEngine();
//		Folio folio = executeEngine.getDesignEngine().newFolio();
//		Report report = executeEngine.getDesignEngine().newReport();
//		report.setFolios(new Folio[] { folio });
//		report.setOutputType(folio.getOutputType());
//		folio.setQuery("SELECT * FROM PSS.TRAN WHERE TRAN_ID = ? AND TRAN_STATE_CD = ?");
//		Map<String, Object> reportParams = new HashMap<String, Object>();
//		// reportParams.put("params.tranId", 1234);
//		// reportParams.put("params.1", 1234);
//		// reportParams.put("params.2", 'C');
//		// reportParams.put("paramNames", new String[] { "tranId", "tranStateCd" });
//		// reportParams.put("paramTypes", new SQLType[] { new SQLType(Types.BIGINT), new SQLType(Types.CHAR) });
//		((StandardDesignEngine) executeEngine.getDesignEngine()).createQueryPillars(report, reportParams);
//		Call call = executeEngine.getDesignEngine().getCall(folio, new long[0]);
//		log.info("Produced call with args: " + Arrays.toString(call.getArguments()));
//	}
//	@After
//	public void tearDown() throws Exception {
//		if(executeEngine != null) {
//			TaskListener taskListener = executeEngine.getTaskListener();
//			if(taskListener != null) taskListener.flush();
//		}
//		Log.finish();
//	}
//
//}
