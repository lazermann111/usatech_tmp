/*
 * Created on Sep 22, 2005
 *
 */
package test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.xml.sax.SAXException;

import simple.db.BasicDataSourceFactory;
import simple.db.DataSourceFactory;
import simple.param.DefaultTextParamEditor;
import simple.text.StringUtils;

public class GeneralTest {
protected static enum TestEnum { ONE, TWO, THREE, FOUR, FIVE};
protected static final int TEST_ONE = 1;
protected static final int TEST_TWO = 2;
protected static final int TEST_THREE = 3;
protected static final int TEST_FOUR = 4;
protected static final int TEST_FIVE = 5;

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
        DefaultTextParamEditor pe = new DefaultTextParamEditor("1-");
        System.out.println(pe.toString());
        /*
        //System.out.println(Arrays.asList(RegexUtils.match("^\\w+[:][/]*([^/?])+", "http://www.usatech.com/go/news/html", null)));
        Properties props = new Properties();
        addDS(props, "report", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
        
        DataSourceFactory dsf = new BasicDataSourceFactory(props);
        Connection conn = dsf.getDataSource("report").getConnection();
        try {
            ResultSet rs;
            rs = conn.getMetaData().getUDTs(null, "%", "%", null);
            StringUtils.writeCSV(rs, System.out, true);
            System.out.println();
            rs = conn.getMetaData().getTableTypes();
            StringUtils.writeCSV(rs, System.out, true);
            System.out.println();
            rs = conn.getMetaData().getTypeInfo();
            StringUtils.writeCSV(rs, System.out, true);
            System.out.flush();
        } finally {
            try { conn.close(); } catch(SQLException e) {}
        }
        System.out.println("---- DONE"); 
        
        /*SQLType type = ConvertUtils.convert(SQLType.class, "NUMERIC");
        System.out.println("Did it. " + type);
        //System.out.println(Arrays.asList(RegexUtils.match("^\\w+[:][/]*([^/?]+).*", "http://www.usatech.com/go/news/html", null)));
        /*
        Date d = new Date();
        System.out.println("SHORT,FULL=" + DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.FULL).format(d));
        System.out.println("MEDIUM,LONG=" + DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG).format(d));
        System.out.println("LONG,FULL=" + DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.FULL).format(d));
        System.out.println("SHORT,LONG=" + DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG).format(d));
        /*AddProtocols.run();
        Properties props = new Properties();
        addDS(props, "report", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
        
        DataSourceFactory dsf = new BasicDataSourceFactory(props);
        Connection conn = dsf.getDataSource("report").getConnection();
        try {
            //ResultSet rs = conn.getMetaData().getUDTs(null, "%", "%", null);
            ResultSet rs = conn.getMetaData().getTypeInfo();
            StringUtils.writeCSV(rs, System.out, true);
            System.out.flush();
        } finally {
            try { conn.close(); } catch(SQLException e) {}
        }
        System.out.println("---- DONE"); 
        
        //Date d = new Date(2005-1900, 9-1, 23);
        //System.out.println("" + d  + " is " + ((System.currentTimeMillis() - d.getTime()) / (1000 * 60 * 60 * 24)) + " days ago");
        /*
        String[] words = StringUtils.split("This is the & and > but < and \" go \n so \t there! and now a long stretch of stuff", ' ');
        //Map<String,Integer> map = new HashMap<String, Integer>();
        Map<String,Integer> map = new CaseInsensitiveStringMap<Integer>(4, 100);
        for(String w : words) map.put(w, map.size()+1);
        String[] keys = words;
        Integer[] values = new Integer[keys.length];
        for(int i = 0; i < values.length; i++) values[i] = i+1;
        Map<String,Integer> map = StaticMap.create(new CaseInsensitiveComparator<String>(), keys, values);
        
        Arrays.sort(words);
        long start = System.currentTimeMillis();
        for(int k = 0; k < 50000; k++) {
            for(String w : words) { int n = map.get(w); }
        }
        System.out.println(map.getClass().getName() + " took " + (System.currentTimeMillis() - start) + " milliseconds");
        */
        /*TestEnum currEnum = TestEnum.ONE;
        for(int i = 0; i < 1000000; i++) {
            switch(currEnum) {
                case ONE: currEnum = TestEnum.TWO; break;
                case TWO: currEnum = TestEnum.THREE; break;
                case THREE: currEnum = TestEnum.FOUR; break;
                case FOUR: currEnum = TestEnum.FIVE; break;
                case FIVE: currEnum = TestEnum.ONE; break;                
            }
        }
        System.out.println("Enum took " + (System.currentTimeMillis() - start) + " milliseconds");
        
        start = System.currentTimeMillis();
        int currInt = TEST_ONE;
        for(int i = 0; i < 1000000; i++) {
            switch(currInt) {
                case TEST_ONE: currInt = TEST_TWO; break;
                case TEST_TWO: currInt = TEST_THREE; break;
                case TEST_THREE: currInt = TEST_FOUR; break;
                case TEST_FOUR: currInt = TEST_FIVE; break;
                case TEST_FIVE: currInt = TEST_ONE; break;                
            }
        }*/
        //String[] words = new String[] {"TWO", "THREE", "ONE", "FOUR", "FIVE"};
        /*StaticMap<String,Integer> map = StaticMap.create(new CaseInsensitiveComparator<String>(), 
                words, new Integer[] {2,3,1,4,5});
        for(int i = 0; i < 5; i++) {
            System.out.println("Value for '" + words[i] + " = " + map.get(words[i]));
        }
        *//*
        int offset = 0;
        char[] buffer = new char[512];
        //Pattern pattern = Pattern.compile("[^\\x20-\\x7E]*");
        for(int k = 0; k < 500000; k++) {
            char[] chars = "This is the & and > but < and \" go \n so \t there! and now a long stretch of stuff".toCharArray();
            
            String s = new String(chars);
            Matcher matcher = pattern.matcher(s);
            int lastPos = 0;
            while(matcher.find()) {
                int len = matcher.start() - lastPos;
                if(offset + len > buffer.length) offset = 0; 
                s.getChars(lastPos, matcher.start(), buffer, offset);
                offset += len;
                lastPos = matcher.end();
                for(int i = matcher.start(); i < matcher.end(); i++) {
                    char ch = chars[i];
                    switch(ch) {
                        case '\t': case '\n': case '\r': case '\f': 
                        case ' ': case 29: case 30: case 31: case 11:
                            if(offset >= buffer.length) offset = 0; buffer[offset++] = ' '; break;
                        case '&': 
                            s = "&amp;";
                            if(offset + s.length() > buffer.length) offset = 0; 
                            s.getChars(0, s.length(), buffer, offset);
                            offset += s.length();
                            break;
                        case '<':
                            s = "&lt;";
                            if(offset + s.length() > buffer.length) offset = 0; 
                            s.getChars(0, s.length(), buffer, offset);
                            offset += s.length();
                            break;
                        case '>':
                            s = "&gt;";
                            if(offset + s.length() > buffer.length) offset = 0; 
                            s.getChars(0, s.length(), buffer, offset);
                            offset += s.length();
                            break;
                        default:
                            if(offset >= buffer.length) offset = 0; buffer[offset++] = '&';
                            if(offset >= buffer.length) offset = 0; buffer[offset++] = '#';
                            String s1 = "" + (int)ch;
                            if(offset + s1.length() > buffer.length) offset = 0; 
                            s1.getChars(0, s1.length(), buffer, offset);
                            offset += s1.length();
                            if(offset >= buffer.length) offset = 0; buffer[offset++] = ';';                            
                    }
                }
            }
            int len = s.length() - lastPos;
            if(offset + len > buffer.length) offset = 0; 
            s.getChars(lastPos, s.length(), buffer, offset);
            offset += len;
            
            
            int lastPos = 0;
            for(int i = 0; i < chars.length; i++) {
                char ch = chars[i];
                if(ch >= 32 && ch <= 126) continue;
                if(lastPos < i) {
                    int len = i - lastPos;
                if(offset + len > buffer.length) offset = 0; 
                    System.arraycopy(chars, lastPos, buffer, offset, len);
                    offset += len;
                }
                lastPos = i+1; 
                String s;
                switch(ch) {
                    case '\t': case '\n': case '\r': case '\f': 
                    case ' ': case 29: case 30: case 31: case 11:
                        if(offset >= buffer.length) offset = 0; buffer[offset++] = ' '; break;
                    case '&': 
                        s = "&amp;";
                        if(offset + s.length() > buffer.length) offset = 0; 
                        s.getChars(0, s.length(), buffer, offset);
                        offset += s.length();
                        break;
                    case '<':
                        s = "&lt;";
                        if(offset + s.length() > buffer.length) offset = 0; 
                        s.getChars(0, s.length(), buffer, offset);
                        offset += s.length();
                        break;
                    case '>':
                        s = "&gt;";
                        if(offset + s.length() > buffer.length) offset = 0; 
                        s.getChars(0, s.length(), buffer, offset);
                        offset += s.length();
                        break;
                    default:
                        if(offset >= buffer.length) offset = 0; buffer[offset++] = '&';
                        if(offset >= buffer.length) offset = 0; buffer[offset++] = '#';
                        String s1 = "" + (int)ch;
                        if(offset + s1.length() > buffer.length) offset = 0; 
                        s1.getChars(0, s1.length(), buffer, offset);
                        offset += s1.length();
                        if(offset >= buffer.length) offset = 0; buffer[offset++] = ';';                            

                }                
            }
            if(lastPos < chars.length) {
                int len = chars.length - lastPos;
                if(offset + len > buffer.length) offset = 0; 
                System.arraycopy(chars, lastPos, buffer, offset, len);
                offset += len;
            }
//          
        }
        System.out.println("Int took " + (System.currentTimeMillis() - start) + " milliseconds");
        */
        /*System.out.println("4 & 4=" +(4&4));
        System.out.println("0 & 4=" +(0&4));
        System.out.println("2 & 4=" +(2&4));
        System.out.println("1 & 2=" +(1&2));
        */
        /*Pattern attributePattern = Pattern.compile("\\s*([A-za-z](?:\\w|-)*)[=]([\"'])(.*?)\\2\\s*");
        String data = "user-agent=\"BLAH BALH\" via='lolol' goodstuff=\"ab'cd'ef\"";
        Matcher matcher = attributePattern.matcher(data);
        Map<String,String> attributes = new HashMap<String, String>();
        while(matcher.lookingAt()) {
            attributes.put(matcher.group(1), matcher.group(3));
            matcher.region(matcher.end(), data.length());
        }
        if(matcher.regionStart() < matcher.regionEnd()) {
            System.out.println("extra data '" + data.substring(matcher.end()) + "' in processing instruction ");
        }
        System.out.println(attributes);*/
        /*Format f = new NumberAsWordsFormat(true);
        for(int i = 1; i < 23; i+= 7) {
            System.out.println("The " + i + ": " + f.format(i));             
        }*/
        /*SlidingArray<String> sa = new SlidingArray<String>(5, String.class);
        for(int i = 1; i < 11; i+=2)
            sa.set(i, "" + i);
        for(int i = 10; i >=0; i-=2)
            sa.set(i, "" + i);
        for(int i = 1; i < 11; i++)
            System.out.println("Element " + i + ": " + sa.get(i));
        
        
        *//*
        AddProtocols.run();
        Properties props = new Properties();
        addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "FOLIO_CONF", "FOLIO_CONF");
        addDS(props, "report", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
        
        DataSourceFactory dsf = new BasicDataSourceFactory(props);
        DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
        ConfigLoader.loadConfig("file://C:\\Java Projects\\Falcon\\src\\simple\\falcon\\engine\\standard\\design-data-layer.xml");
        /*StandardDesignEngine de = new StandardDesignEngine();
        StandardExecuteEngine ee = new StandardExecuteEngine(de);
        ee.setDataSourceFactory(dsf);
        Map<String,Object> paramValues = new HashMap<String,Object>();
        paramValues.put("resultsType", "FORWARD");
        ee.buildReport(de.retrieveFolio(489), paramValues, new long[] {1L}, de.getOutputType(22), new Output() {
            public Writer getWriter() throws IOException {
                return new PrintWriter(System.out);
            }
            public OutputStream getOutputStream() throws IOException {
                return System.out;
            }            
        });
        //*//*
        Connection conn = DataLayerMgr.getGlobalDataLayer().getConnection("metadata");
        Call call = DataLayerMgr.getGlobalDataLayer().findCall("GET_OPERATORS");
        Cursor cur = (Cursor)call.getArguments()[0];
        cur.setLazyAccess(true);
        call.setResultSetType(ResultSet.TYPE_FORWARD_ONLY);
        try {
            Results results = call.executeQuery(conn, null);
            try {
                System.out.println();
                //results.trackAggregate("operatorId");
                for(int i = 1; i <= results.getColumnCount(); i++)
                    System.out.print(results.getColumnName(i) + "\t");
                System.out.println();
                System.out.println(StringUtils.repeat("-", 100));
                Object tot = null;
                results.trackAggregate("operatorId");
                Results subResults= results.nextSubResult(0);
                while(subResults.next()) {
                    if(subResults.isGroupEnding(0)) {
                        tot = results.getAggregate("operatorId", null, Aggregate.COUNT);
                    }
                    for(int i = 1; i <= results.getColumnCount(); i++)
                        System.out.print(subResults.getFormattedValue(i) + "\t");
                    System.out.println();
                }
                System.out.println("Count = " + tot);
            } finally {
                results.close();
            }
        } finally {
            conn.close();            
        }
        //*/
        //System.out.println(StringUtils.repeat("-", 100));
        // */
        //System.out.println(ConvertUtils.convert(SortOrder[].class, new Object[] {"0"}));
        /*
        AddProtocols.run();
        String s = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAFeCAIAAADWtfYLAAAnoElEQVR42u3dDUyb 973ocSRk4bmMeWqDeiOUUwnm0SprScoya+I6qdVNJDlVpXTtkWBZpizlNKqmvXTS pCY5SaulPWPr6Sk5nF6fjmUeddyUpNShLji8+gUIgRJDYhiGhq1pF0bc0t3t3qw3 2nJ/4Wk8H79hEgK2n+9HlvXPgzHOn8f++m/8kvMRAAAqlsMUAAAIIQAAhBAAAEKY NT788MNz58698sor3QAAdHc7HA4VhfDSpUstLS1vvvlmV1fXVQAArl5tbW1VRQiV hWBjY+Pg4OC7777b09PD7x4AoJYQhheCwWDw3XmEEACgihBGLQTDCCEAIPtDGLsQ JIQAAFWEMNFCkBACALI/hEkWgoQQAJD9ITxy5EiihSAhBABkfwjfeOONdxdCCAEA hBAAAEIIACCEhBAAENbRMf3aa74jR7zJDw6Ht7vbSwgJIQBkj9nZ//vLXw4ePOjZ urV306aEh8ce62tq8tlsbpfrFCEkhACQPQtBi8X3+OPeBx5IVsF//df+lhbP/v0e s7m3rW2AEBJCAFDdQvDRRz/dQggJIQCocSEY3kgICSEAqHEhSAgJIQCoeiFICAkh AKh6IUgIMz6EExMTEsLp6em5uTmZAjlmzJgxY/WMOzp8Lpf3+iHhuLvbvXdvp8nk luBVVLiNxk45jho7nX1ZMCcOh0NdIQwEAn6/nxUhANU6dsy/adPVBQ8nT3qSrBdZ EWZkCCcnJ3loFAAIoUpDODo6KgtBQggAhFClIYxcDhJCAISQEKolhOfm8axRACCE 6RvCPXv25ORc+3E2m62goECj0ZSUlMg4xS1JQiirwLGxMV4+AQCEMH1DKBV84YUX lBCaTKbGxkYZWCyWysrKFLfEDaHf7w8Gg7yOEAAIYVqHUKngtR82H0KdThcKhWQg x/n5+SluiXRgXm1tbSAQ4AX1AEAI0zqE4QqGQ5ibmxv+qkajSXEL7ywDAIQwI0OY E0Or1Uat9lLZQggBgBBmZAijoijHZrNZef6L3W43mUwpbiGEAEAIsySEVqtVp9PJ WK/XS+dS3EIIAYAQZnwI+fQJACCEhJAQAgAhJISEEAAIISEkhABACAkhIQQAQkgI CSEAEEJCSAgBgBASQkIIAISQEBJCACCEhJAQAgAhJISEEAAIISEkhABACAkhIQQA QkgICSEAEEJCSAgBgBASQkIIAISQEBJCACCEhDCxiYkJCeH09PTc3JxMgRwzZsyY sXrGEsKKiitG42U5luAlGksIjcbOigq3BE+O446dzr4smBOHw6GuEAYCAb/fz4oQ ACtCVoSqWxFOTk7y0CgAEEKVhnB0dFQWgoQQAAihSkMYuRwkhAAIISFUSwjPzeNZ owBACNMuhF6vt6ysTKPRlJaWtrS0yJbx8fGcCLLFZrMVFBTIaUpKSmQcd0uSEMoq cGxsjJdPAAAhTMcQSv+sVqsMnE7nqlWrZNDQ0FBTUxN5GpPJ1NjYKAOLxVJZWRl3 S9wQ+v3+YDDI6wgBgBCmbwgVoVDIbrfL8k7GVVVVRqNRq9UaDAa32y1bdDqdnEA5 WX5+ftwtkQ7Mq62tDQQCvKAeAAhhuodwdnZWwpaTk7N//375Z3FxscvlkoFkTFoo g9zc3PCJNRpN3C28swwAEMIMXhEqfyzMy8uL2qhskdVh1PovdgshBABCmJEhlPWf JFAGw8PDRUVFypahoSFlRWgymWRgNpuVZ8TY7fZEWwghABDCjAxhe3t7aWmpRqMp Kyvr7OyULS6Xy2AwyBaj0SgtlC1Wq1V57FSv10v54m4hhABACDMyhHz6BAAQQkJI CAGAEBJCQggAhJAQEkIAIISEkBACACEkhIQQAAghISSEAEAICSEhBABCSAgJIQAQ QkJICAGAEBJCQggAhJAQEkIAIISEkBACACEkhIQQAAghISSEAEAICSEhBABCSAgX EcKJiQkJ4fT09NzcnEyBHDNmzJixesYSwoqKK0bjZTmW4CUaSwiNxs6KCrcET47j jp3OviyYE4fDoa4QBgIBv9/PihAAK0JWhKpbEU5OTvLQKAAQQpWGcHR0VBaChBAA CKFKQxi5HCSEAAghIVRLCM/N41mjAEAI0y6EXq+3rKxMo9GUlpa2tLTIFpvNVlBQ IFtKSkpknOKWJCGUVeDY2BgvnwAAQpiOIZT+Wa1WGTidzlWrVsnAZDI1NjbKwGKx VFZWprglbgj9fn8wGOR1hABACNM3hIpQKGS322V5J2OdTif/VDbm5+enuCXSgXm1 tbWBQIAX1AMAIUz3EM7OzkrYcnJy9u/fL//Mzc0Nf0mj0aS4hXeWAQBCmMErQuWP hXl5eTLQarVRq71UthBCACCEGRnC4uJiSaAMhoeHi4qKZGA2m5Xnv9jtdpPJlOIW QggAhDAjQ9je3l5aWqrRaMrKyjo7O2WL1WpVHinV6/XSuRS3EEIAIIQZGUI+fQIA CCEhJIQAQAgJISEEAEJICAkhABBCQkgIAYAQEkJCCACEkBASQgAghISQEAIAISSE hBAACCEhJIQAQAgzIITKe2eHXbx4MfLDIgghABDC7AxhKBRau3ZtTgyNRrNlyxZC CACEUKUrQh4aBQBCqK4Q8jdCACCEqg5hU1OTRqOJeoCUEAIAIVRLCLVa7dGjR1kR AgAhVGkI16xZEwqF0j+EExMTEsLp6em5uTmZAjlmzJgxY/WMJYQVFVeMxstyLMFL NJYQGo2dFRVuCZ4cxx07nX1ZMCcOh2PJQnjw4MEnnnhiZmYmnUMYCAT8fj8rQgCs CFkRLv2KMC8vL/ZFFOkTwsnJSR4aBQBCeAtDmM7PGh0dHZWFICEEAEKo0hBGLgcJ IQBCSAhvSQjT8KHRc/N41igAEMLlXhHOzMzs3r27rq4u0QnkB69du1aj0RQXFx8/ fly2jI+PRxXUZrMVFBTIaUpKSmQcd0uSEMoqcGxsjJdPAAAhXIEQKu9BKtFK9FXp n9PplIFUsLCwUAYNDQ01NTWRpzGZTI2NjTKwWCyVlZVxt8QNod/vDwaDvI4QAAjh SoawqakplXcfvXDhQlFRkQyqqqqMRqNWqzUYDG63W7bodDrlhYlynJ+fH3dLpAPz amtrA4EAL6gHAEK43CGM+huhRqNJ5Y1mqqurJZnKGtHlcslAMiYtlEHkpzjJucXd wjvLAAAhTNMVYSp/R9yxY0fcWCpLSVkdRq3/YrcQQgAghBkZQqfTaTKZBgcHI/9q ODQ0pKwI5UsyMJvNyjNi7HZ7oi2EEAAIYbqEcGpqSlZ4yrM6Zbm2ffv2YDCY6MRF RUVRzxF1uVwGg0G+12g0Sgtli9Vq1el08lW9Xi/li7uFEAIAIUyXEMpyrb6+Xnmv 0VAoZLFYNm7cyKdPAAAhVEsIY58juiKfWU8IAYAQrkwIH3zwwUOHDs3OziorwqNH jyb6Mx4hBABCmIUhVP5GqNfrc3JyCgoKqqurk/yNkBACACHMthCm85tuE0IAIISE kBACACG8lSFsamrSaDRp+8G8hBAACOGtDaFWq03lPdUIIQAQwuwM4erVq5X3PyOE AEAI1RjC5ubmPXv2KC+fIIQAQAhVF0KbzVZeXs7fCAGAEKo0hKtWrVI+a54VIQAQ QpWGcMUfFyWEAEAIVyyE9fX1u3btunDhAiEEAEKoxhBGfUI9fyMEAEKorhDGWpFX UxBCACCEKxxC6Z/dbq+uri4uLk7DEE5MTEgIp6en5+bmZArkmDFjxozVM5YQVlRc MRovy7EEL9FYQmg0dlZUuCV4chx37HT2ZcGcOByOJQvh7OxsY2PjY489ptPpTCZT Q0NDGq4IA4GA3+9nRQiAFSErwqVcEUr/tm3bJv2rrKw8dOiQRqNJwyfLTE5O8tAo ABDCWxLCvLy8b3zjGx988MEKfjB98hCOjo7KQpAQAgAhvCUhPHz4sLIifPjhh19+ +eU0XBFGLgcJIQBCSAiXOIThvxHabLbHHnssPz9/48aNUscV/xvhuXk8axQACOFy hDD2WaN33XVXotPID167dq2sHYuLi5U3ZpOIFhQUyJaSkhIZp7glSQhlFTg2NsbL JwCAEC53CFN5HaH0z+l0ykAqWFhYKAOTydTY2CgDi8VSWVmZ4pa4IfT7/cFgkNcR AgAhXOEQpuLChQtFRUUy0Ol0SjjlOD8/P8UtkQ7Mq62tDQQCvKAeAAhhZoSwurq6 qalJBrm5ueGNytNtUtnCO8sAACHM1BDOzMzs2LHj6NGjyj+1Wm3Uai+VLYQQAAjh yocw7tttJ3/TbafTaTKZBgcHw1vMZrPy/Be73S5fSnELIQQAQpiRK8KioqKoXlqt Vp1OJ2O9Xi+dS3ELIQQAQpiRIeTTJwCAEGZbCC0WS0FBAZ9HCACEUKUhLCwsPHz4 sAwMBsPQ0NDu3buff/55QggAhFAtIQy/1/a2bdvsdnsoFNLr9YQQAAihWkJYVlam vFnanj179u3bNzw8XFJSQggBgBCqJYSdnZ0Gg0EGU1NT5eXlWq1WeTs0QggAhFAV IeRZowBACFUdwqjP47148WLkO6IRQgAghNkZwlAotHbt2tj3lNFoNFu2bCGEAEAI Vboi5KFRACCE6gohfyMEAEKo6hBOTU3t2LFD+QT5/Pz87du3B4NBQggAhFAtITSb zfX19TMzM8ofDi0Wy8aNGwkhABBCtYQw9m+EK/JXQ0IIAIRwZUL44IMPHjp0aHZ2 VlkRHj16NNFHBhJCACCEWRhC5W+Eer0+JyenoKCgurqavxECACFUUQgz5VmjExMT EsLp6em5uTmZAjlmzJgxY/WMJYQVFVeMxstyLMFLNJYQGo2dFRVuCZ4cxx07nX1Z MCcOh2MJQpgmryBMJYSBQMDv97MiBMCKkBXhUq4IMyKEk5OTPDQKAIRQpSEcHR2V hSAhBABCeKtCmJNAmoQwcjlICAEQQkKolhXhuXk8axQACGGahnB4eLiwsFAZj4+P Ry0lbTab8oZtJSUlMo67JUkIZRU4NjbGyycAgBCmaQj379+/fv368MOnDQ0NNTU1 kScwmUzKZ9xbLJbKysq4W+KG0O/3B4NBXkcIAIRwmUJ4Y+rq6kKhUDiEVVVVRqNR q9UaDAa32y1bdDqdnEB5n5r8/Py4WyIdmFdbWxsIBHhBPQAQwnQP4ac/6XoIi4uL XS6XDCRj0kIZRH7AvUajibuFd5YBAEKYJSGMfaxVVodR67/YLYQQAAhh9qwIh4aG lBWh8m7dZrNZeUaM3W5PtIUQAgAhzJIQulwug8Gg0WiMRqO0ULZYrVadTicn0Ov1 Ur64WwghABDCzA4hnz4BAISQEBJCACCEhJAQAgAhJISEEAAIISEkhABACAkhIQQA QkgICSEAEEJCSAgBgBASQkIIAISQEBJCACCEhJAQAgAhJISEEAAIISEkhABACAkh IQQAQkgICSEAEEJCSAgBgBASwoVDODExISGcnp6em5uTKZBjxowZM1bPWEJYUXHF aLwsxxK8RGMJodHYWVHhluDJcdyx09mXBXPicDjUFcJAIOD3+1kRAmBFyIpQdSvC yclJHhoFAEKo0hCOjo7KQpAQAgAhVGkII5eDhBAAISSEagnhuXk8axQACGGahnB4 eLiwsFAZ22y2goICjUZTUlIi4xS3JAmhrALHxsZ4+QQAEMI0DeH+/fvXr1+fk/Pp zzKZTI2NjTKwWCyVlZUpbokbQr/fHwwGeR0hABDCtA5hXV1dKBQKh1Cn08k/ZSDH +fn5KW6JdGBebW1tIBDgBfUAQAjTPYSf/qTrIczNzQ1v1Gg0KW7hnWUAgBBmSQi1 Wm3Uai+VLYQQAAhhloTQbDYrz3+x2+0mkynFLYQQAAhhloTQarXqdDr5p16vl86l uIUQAgAhzOwQ8ukTAEAICSEhBABCSAgJIQAQQkJICAGAEBJCQggAhJAQEkIAIISE kBACACEkhIQQAAghISSEAEAICSEhBABCSAgJIQAQQkJICAGAEBJCQggAhJAQEkIA IISEkBACACEkhIQQAAghIVw4hBMTExLC6enpubk5mQI5ZsyYMWP1jCWEFRVXjMbL cizBSzSWEBqNnRUVbgmeHMcdO519WTAnDodDXSEMBAJ+v58VIQBWhKwIVbcinJyc 5KFRACCEKg3h6OioLAQJIQAQQpWGMHI5SAgBEEJCqJYQnpvHs0YBgBBmQAjHx8dz IsgWm81WUFCg0WhKSkpkHHdLkhDKKnBsbIyXTwAAIcyMEDY0NNTU1ERuMZlMjY2N MrBYLJWVlXG3xA2h3+8PBoO8jhAACGEmhbCqqspoNGq1WoPB4Ha7ZYtOpwuFQjKQ 4/z8/LhbIh2YV1tbGwgEeEE9ABDCDAthcXGxy+WSgWRMWiiD3Nzc8Fc1Gk3cLbyz DAAQwiwJYaS8vDw5ltVh1PovdgshBABCmD0rwqGhIWVFaDKZZGA2m5VnxNjt9kRb CCEAEMIsCaHL5TIYDBqNxmg0Sgtli9Vq1el0OTk5er1eyhd3CyEEAEKYJSHk0ycA gBASQkIIAISQEKojhBcuzLa09B871pv8cOKEr7PTw1UXACEkhNkTwitXrvT0BP7t 33q3bk22h23b1vvaa96jRz1yzFUXACEkhFkSQlkINjUNPP6494EHku1e+/Z5337b 88wzHrO598gRQogFyJ7v8Qx1dZ1e6HDK7WZ3IoSEkBCuUAgXuxB89NFPtxBCJN+v AoGpw4dPL7hfvfqq59gxb3NzL5NGCAkhIVyBEN7AQjC8kRAiyUKwq+vM97/fn3y/ 2rvX+9Zbnp/+1Cf71bFjhJAQEkJCuLwhvOGFICHEEi4E/+mf+pQthBCEkBAuawhv ZiGYJIShD0MdpzvaBtqSH072nezx8kIRFoK+yP2KEIIQEsJlCuHNLwTjhlDO9p2J d+r767f2bk2yf27r3XbEc6TJ2/RG7xtc7VkIRh4IIQghIVymEPb0+E6e9C546O52 J1oIxoZQFoJvD72927f7gd4Hkuyce717nR7nc77nzL3mY73HuNqzECSEIISEcAVC 2NWV0q7mcvkW3NUkhItdCD7a+6iyhRCyECSEIISEMONDeOy4e7ELwfBGQpgd3G5f e7tHbpiSH7q73YkWgoQQhJAQZmQIN2/u3f8z58HW2sUuBAlhlmlrS/GWy7vw/SpC SAgJISHMlBDu/I7n541Hv/arb61tXr/YhWC6hfCD0dH+t97qP3Ei+aG3tdXrdnM7 RQhBCAlhxoRQftzw4OCQzxd7cLb03r/u4/Bh/bqP1637OMUQKgvBp2w/+wfL/9TX r7uned1iF4LpE8L/95e/nHnjDe9zzy349FlvU5PnyJGBri5upwhhEn/95JPxV1/1 Hjmy4MF34oTXwxvWE0JCeMtCeOXKlfPnz4+6XKceeSTuDtLyizfW6c9cO6w6t+6e P8ihvPxPKYawtb3r7S5X+NDW44r7J6Fud3eihWCahFAWgn1Wq/fxx3uTPs3R9+yz HqfTs39/r9l8mhASwsQ+Hh/vP3TIc/Bg8vtVp7Zv977+uttmG/T5ou65dncPLvTA xLVDW1uf1+sjhIQwa0Po9/v7+/tHRkZGR0fPzVvseGhoqLenx71378kNG1zl5Se/ /GXlEDl2WJru++zQfWveLftS6L5/OL/O8IGsCL/85T9LDuVYOci4rc2rfEd5uev6 t578X28el29VDneO33fvh+uU7yj/U/n1b702dnlcyneUu8qvf+t/Gz/e8fhJ78kb +z/e5HjkzBnvr3/d9cwzJzdtijs/yrhj8+Yeu7371Vc7/vEf5Z/uPXt8bvdyXs5M GTscA+F9JnL/iRq7XJ6ofSlqXFPTKbHMxHk4K3vUv/9710svtW/fnuR6J+Pun/yk x+HoePpp909+4vN4lPORa73Hc+o//9P9wAPtSeZHjrds6bTbe9raeiSE2bpf2e2D C+5L87uTO/lcPfVUT3e3b6kum98/5PO91N396wUPPT1vuN3dS/Jzx+YdO3ZMRSEM BoPvzBsYGPjNDRkfHx8cHJQ71Z2bN7dv2JDkcKKhueyeP5TdPVNWNLm+bO7++//3 hg3/J/YgIYz9bovjeME79+nP3PeF96We98f7vmsHCWGiH7+pY9Mr3ld6T/fK7/g3 y+4duQK98krHt77V/pWvJJmi7r17e1paOp9+ut1o7Nq8ue/48dOnT6/IBU5/J06c 3pBoP4g4SAgTzfemTR2vvOLt7c3IGR6RLv38510HDnSYzcn2qEce6TlypPvXv+78 9rdPOZ1yVVX+s3KT53T2/vM/dyXdH68dnnmmu7W1p7PTE/7erPTaa0Op7U7uRBP1 ta/J3YXeJbzCjoy09fT8vKvrQEeHub19Q6JDd/cjPT1HenqcfX2em/+hEoXLly9/ +OGHb7/9tlpCKFeGM2fO+OfdWAjlHE55PJ69e9sXuj51Pf10y5ue++6aXv/F369f /0e5h5VoV0sUwv/xG1lJrpNlX5IdNVEIa7pq2vra5L8p2V7mK9jYuXO+xkZZCCa/ weq8vhDsfOgh+adn3z6Z2BW5wCoJYU1NV1tbXybO8PjZs32HDnXX1S14v6rnuedk ISj3q3zPP3/K51P+s8LnG5SFoNnckTyBW7d2vvaaLATdvb19Wb8r3mQIn3rK3d3d v1SzND5+tq/vUHd3XUfHt9rbv5Kkgj09z0kCPZ62wcGBmwzwxMTEzMzMxx9/rKKH RmUhODQ0JAtB/3WLDWHqC8FrN/Fyn9Rma3n17SQLwSQhlHvu9u43kiwEk4SQhSAh ZCHIQvDWhTANFoLumw+wROHPf/6zLAT/+te/qiWE586dOzPPH2FRIVzUQvDaTfy/ /Mup//qvt97qS7IQTBTC73yn68QJ35unHCnspdEhZCFICKNCyEKQheAShjA7FoK/ //3vP/nkExU9WSZ2IbjYEN7AQrDne98b6OqS72ptHUhlVwuHUO65/8d/eLzea3e1 Tpw+sagQruxCUHja2z1yG7zQoae7m4Xg8oSQhSALwSUMYdYsBGUVeOnSpfBCMFND aLPZCgoKNBpNSUmJjJOEUBaCw8PDcSuYegg9bre7vd0tu8NCh56uLmUheOrUKeUX tqgQKgvB4eFPf9OLCuEKLgT//qBoY2MKl3eDTBQLwWUIYeYuBBVeuVakcLh2pWMh eOtDmB0LwYsXL2bPyydMJlNjY6MMLBZLZWVl3NMcP35cFoJRj4XeWAhPdXSkcvsu h56TJ5WFYPgXlnoIwwvBv9/qpRzClV0ILjqEJ0+yELzVIczcheDfd6df/jLF3Sly ISjc7msvsE3hsQlPd7caF4KLDeHJk+7sWAjOzMzMzs5mTwh1Ol0oFJKBHOfn50d9 9cA8aeTIyMjk5KT88mRdKBMRO+7v7//jH//4ySefyPl88MEHicZDg4NnBgaG+vpO +3zv9PcnGXu93mAw+PHHH4e/9/TpoYGBM319Qz7f6f7+dxKNPR7f1NT5999/P/Ln nn7n9MCZgb6hPt9pX/87/YnGHp9neno66ntXZHy6tzf5/Chjr8cje+SlS5dW6nJm 7nhwcHjBfUnGHk/0fpiJ/99U9iUZ+7zeqP1f7uAmn5/w2Ov1pcl1Z0XG/f2DC+5L MvZ4PO+9995f/vKXpfq5Xq9LOfh8riTjnp5un69dVo9/+MMfbv7nKu9/klUvqM/N zQ2PNRpN+n9CPQBA4ff7UjnIfWbJ2N/+9jfeWSY+rVabZEVICAEAWR5Cs9msPEfG brebTCZCCABQVwitVqtOp8vJydHr9dJCQggAUFcIl+RNt8fHx3sAAOjpeeuttywW i4pCODU1NTg4ODAwEAwG3wUAqNj58+ddLldbW9ulS5fUEsL5F9te+8QQfv0AoHJn z559/fXXJyYmktQkq0LIQhAAkOJCMAtDyEIQAJD6QjCrQshCEACw2IVg9oSQhSAA 4AYWglkSQhaCAIAbWwhmfAhDodDhw4ePHz/uBACoXnNz82IXgtmwIlTejBQAgJuR wxQAAAghAACEEAAAQggAACEEAIAQqnlSrlu1atWhQ4div5r6+Sz4UzQaTVNT0w2c W11d3a5du1pbW9euXStnUlxcfPz4cdkuG+VLzFLkLHm93rKyMjmT0tLSlpaW5Zyl zJqo8D/37NmjnIDdKXaWxsfHcyIwS3Fn6fz581u2bMnLy1uzZk1zc/MyX+kI4dLs bcpAbkC1Wu0Nn0/y71V+iuxqcrJUdrjIvW14ePjee++dnZ2V/jmdTtkiFSwsLPxo /lUlcqM/MjLCLIVnSfpntVpli8yV3IIs5yxl1kSFK/jCCy8oJ2B3ip2lhoaGmpqa yJMxS7GzJNmz2WyyRSp41113LfOVjhAu5d7W29u7c+fO8MaKiorIr9rt9jvvvFPu 8hw8eDB8mq9//euyRdl7Hn74YTk+evSo3LeSXWrHjh1xf4qcWE6gjB0Oh5zn6tWr lbxFfq9y+vr6etnD5FJF3R+8cOFCUVGRMlZOwyxFzZJcD+XClJSULOcsZdxEKRWM PEN2p6hZqqqqMhqNcgKDweB2u5mluLMk9zh/9KMfyQmkgl1dXct8pSOES/n4g5Df ZXhjMBiM3EvkXk8gEJiYmNDpdOHTtLa2Ru49Qm55ZecI392OezcqPJarVu88uVcV 9b1yms7OTrlLJWNZ/P3ud7+LPLfq6urwfTeJouyyzFLkLMlX5QLIV/fv37+cs5RZ ExWuYOSZsDtFzVJxcbHL5ZKBXBL5XmYp7izl5ua++OKLMujv71e+ZTmvdIRwKe92 yZ4k96GiNoYHcp9I7ljJtSL2S5F70tmzZ++55578/HzlypP8bpcMwg/QR32vbNyw YYPsRrIzRe7NMzMzcqdMLkzkmUeegFkK83q94Yu6PLOUWROVE4PdKcnuJJZ5d8qg WYp89DVyZpbnSkcIl3JvkxV9+PHG2D1J7m01Nze3t7cn39sUTz755O233x77U2SX lT0mnDHZd+XGOu73yunlvpWM5X6W3O2ampr6aP6PXiaTaXBwMPL08qXluXOaKbMU Pv3w8HD4oi7PLGXWRMW92OxOsbvT0NCQsiKUax+zFHeWpMQWi0W591leXr7MVzpC uJSPP6xZsya8H8TuSdXV1XLvZvfu3cn3tl27dsl9Nzll1KIt7lOzjh8/rjy4/8Mf /jDqe5XzlB1u/fr1sl152EGuDLH33+vq6qL+mK/yWZJbhNLSUjlBWVlZZ2fncs5S Zk1U3NtcdqeoWZI1kMFgkBMYjUZpIbMUd5bGx8crKirkBPfee+/AwMAyX+kIoSqM jIzILXvsg/vKI6VyLT179iyzxCwxUcwSs0QIs9mhQ4fi3reSu4GxLzBilpglJopZ YpYIIQAAhBAAAEIIAAAhBACAEAIACCFTAAAghAAAEEIAAAghgPQUCoWYBIAQAplz 1cpZ4ivX2rVrb/6c5Xvr6+tv6eUECCGAWxKYJTlD5RNzlI+vI4QAIQSWO4RSoI0b N2o0GpPJFP7ko+Hh4dLS0oKCgsbGRmVLe3v7vffeKycrLCxUPssm8gNGwucc99zk q88+++yaNWuiPj0g/NXW1tZt27bFXs7YH6p8ta6u7vbbb9fr9XLxmpubV69eHXnO 58+ff/DBB2WL2WyWMb93EEIAyUK4Y8cO5ZHJl19+efv27crGhx9++Gc/+5nX673r rruULQaDweFwyMBqtUogo84wPIh7bvLVJ598MhQKRX0ceeT37tq1y263R51boh9a U1MzOzsrCSwvL9+3b1/UOe/cuVP5KCKbzfbEE0/wewchBJAshLKuUp7zMjMzE45N fn5+3I+tSdS/8CDuuclX5Z+JLoOy5cKFC2VlZXKc6HJG/izlZMo4fDnDJ4j8TFdZ SvJ7ByEEkCyEkRvDi6rYU05NTe3Zs2fbtm2ySksSwgXPLVEIlQWc8lk5kZ9En+SH JhpHfi50bm4uv3cQQgDJQijrNmUNJ0srWQgmWhGuX79+3759drt9ZGQkSf/inluK Ifxo/iHZ9vb28JbkPzTROLwSBQghgIVDWF1drfxxLvKveg899FB9fX1/f3/4b4Ra rXZwcFDytnv37vD5yJov6sHMuOeWeggnJiY2bNgQ3hL3hy4Ywp07dyqfOd7Q0FBe Xs7vHYQQwN9TEeWj+ed5mkwmSdrGjRsjnzVaUlJy++23h5+KabPZDAaDLLZ++tOf hpMjvczLy/vovz9rNPbcUg/hR/MfJh75YGnsD10whPJzzWazXIZ77rlnYGCA3zsI IQAAhBAAAEIIAAAhBACAEAIAQAgBACCEAAAQQgAACCEAAIQQAABCCAAAIQQAgBAC AEAIAQAghAAAEEIAAAghAACEEJnuAHATuAaBECIbQngVuCGEEIQQhBCEECCEIIQg hAAhRDaE8OLFq5s2xT98//vLdtv6/PMJL8WZMytzc3/x6sUEl2jT968uz8w8f/Xq pgSHi4QQhBBYohBOT1/NyYl/kNv85fLtbye8FN3dK3NzP311OsElytl0dXlm5ttX r+YkOEwTQhBCYLlD+Kc//amqquozn/nM3XffHQgEUrnFzJEzWeoQ3sDFiLow4cHe vXuXJIQ583JzcwsLC1999dXlD2HOdbfddtsvfvGLRGfndDrz8vJaWloWdSFif4mE EIQQKg3hd7/73dbWVhn86le/uv/++1cqhDdwMRJdmOQXb1EhVAYS5s9+9rMrEkJl MD4+/rnPfS7R2Wk0GmXqCCEIIXAjIbzjjjtCoZAyPnP9D3eyvFi9erWsM1588UVl zSG3trJcq6mpUW5Dd+/eLV9d8PY39RDGvRjygzbNX9quri65PEVFRR6PR/lSc3Oz xOmb3/xm1IowvIpawhDK5bnzzjsXnJmoWVrCEEqJZVWqjKOmIvL/G7s4lsHWrVsj f1Ox80YIQQih9hDKbXfsTfWVK1fef//9mZmZ2267Tf75xS9+UconG8M3r3IrLLet cb/3xkIY96zkB83Ozsrg7rvvPjtv3bp1ype+8IUv9PX1ycWIe+u/hA+NCgnP66+/ vuDMRM3SEj40KsvB8GOzsVORZAZk4PP5In9TsfNGCEEIofYQylpHvlG5iX/ppZfC f3Z69NFH5UZTubl87733vvSlL8lKQm5DU0/OokIY92KEz19ux5UkhG/Qk9/6L+1D o1F/kEs0M1GztFQrwt/+9rdmszm8XL6xqUhllgghCCFUGsLvfe97yvMsnn322c2b NysbZbnT3t5+6tSpyJvLp5566o477rhFIYx7McLnL+EZGRmJPL0sv2RZI8uduDfx SZZlNxnC5DMTNV6qh0ZlObhz585EUxF5d0EWf4nmJNG8EUIQQqg9hPJdDz30UF5e 3n333ff+++8rG+VmV25Vf/CDHyg3l08++aScQLbIeugWhTDuxQifv8vlUv4y9/TT Tytb5Bb/85//fFVVVeyF+epXvyqnvEUhTDIzUbO0hCEUGzZsUPoXOxXhk8mcyPYf //jHSUIYO2+EEIQQWRpCWRJJC+MeLi7fq7YvXUp4KS5fXplXy125eiXBJZq+uEyv Z780H7y4hytXVw4hBCFEdoUQIIQghCCEACEEIQQhBAghCCHUFEKAD+YFIQQAgBAC AJBm/j+uFfLXJWdroQAAAABJRU5ErkJggg==";
        System.out.println("Image data=" + s.substring(22,30) + "...");
        String test = s.substring(22);
        Runtime runtime = Runtime.getRuntime();
        long memoryStart;long start;long time;long memoryUsed;InputStream in;
        
        memoryStart = runtime.totalMemory() - runtime.freeMemory();
        start = System.currentTimeMillis();
        in = new Base64.OldInputStream(new StringBufferInputStream(test));
        while(in.read() > -1) {
            //do nothing
        }
        time = System.currentTimeMillis() - start;
        memoryUsed = ((runtime.totalMemory() - runtime.freeMemory())) / 1024L;
        System.out.println("THEIRS in " + time + " milleseconds using " + memoryUsed + " Kb of memory");
        
        memoryStart = runtime.totalMemory() - runtime.freeMemory();
        start = System.currentTimeMillis();
        in = new Base64DecodingInputStream(new StringBufferInputStream(test));
        while(in.read() > -1) {
            //do nothing
        }
        time = System.currentTimeMillis() - start;
        memoryUsed = ((runtime.totalMemory() - runtime.freeMemory())) / 1024L;
        System.out.println("MINE in " + time + " milleseconds using " + memoryUsed + " Kb of memory");
        
        //IOUtils.copy(new ByteArrayInputStream(Base64.decode(s.substring(22))), new FileOutputStream("C:/TEMP/b64-their-image.png"));
        //IOUtils.copy(new Base64.SmallDecodeInputStream(new StringBufferInputStream(s.substring(22))), new FileOutputStream("C:/TEMP/b64-my-image.png"));
        com.sun.media.jai.codec.FileCacheSeekableStream seekableInput =
            new FileCacheSeekableStream(new URL(s).openStream());
        RenderedOp imageOp = JAI.create("stream", seekableInput);
        System.out.println("Height=" + imageOp.getHeight() + "; Width=" + imageOp.getWidth());

        /*
        Class.forName("org.apache.fop.image.JAIImage");
        System.out.println("START");
        byte b = (byte)130;
        System.out.println("Value " + b + " = " + (int)((256 + b) % 256));
        //System.out.println("Matches=" + Pattern.compile("([^;]*)[;]([^,]*)[,](.*)",Pattern.DOTALL).matcher("image/png;base64,iVBORw0KGgoAAAAN\n\rNAkfd33421l").matches());
        //PrintWriter out = new PrintWriter(URLDecoder.decode(MethodExtractorDoclet.class.getResource(".").getPath() + "/extracted-methods.csv"));
        
        /*
        String[] arr = new String[] {"12345", "67890", "ABCDE", "FGHIJ", "LM"};
        int total = 0;
        byte[] buf = new byte[4];
        int offset = 0;
        int last = 0;
        for(int i = 0; i < arr.length; ) {
            if(arr[i].length() - offset < buf.length - last) {
                arr[i].getBytes(offset, arr[i].length(), buf, last);
                last += arr[i].length() - offset;
                i++;
                offset = 0;
            } else {
                int len = buf.length;
                arr[i].getBytes(offset, len + offset - last, buf, last);
                System.out.write(buf, 0, len);   
                offset += len - last;
                total += len;
                last = 0;
            }
        }
        if(last > 0) {
            System.out.write(buf, 0, last);
            total += last;
        }
        System.out.println();
        System.out.println("Wrote " + total + " bytes");
        //*/
    }
    private static void addDS(Properties props, String name, String driver, String url, String username, String password) {
        props.put(name + "." + BasicDataSourceFactory.PROP_DATABASE, url);
        props.put(name + "." + BasicDataSourceFactory.PROP_DRIVER, driver);
        props.put(name + "." + BasicDataSourceFactory.PROP_USERNAME, username);
        props.put(name + "." + BasicDataSourceFactory.PROP_PASSWORD, password);        
    }
    protected static int[] charTypes = new int[96];
    static {
        charTypes[0] = 1; //whitespace
        charTypes[2] = 5;
        charTypes[6] = 5;
        charTypes[28] = 5;
        charTypes[30] = 5;
    }
    protected static void escapeCharacters(char[] chars, int start, int length) throws SAXException {
        int lastPos = 0;
        int end = start + length;
        for(int i = start; i < end; i++) {
            char ch = chars[i];
            if(ch >= 32 && ch <= 126 && charTypes[ch-32] < 2) continue;
            if(lastPos < i) {
                System.out.print(new String(chars, lastPos, i - lastPos));
            }
            lastPos = i+1; 
            switch(ch) {
                case '\t': case '\n': case '\r': case '\f': 
                case 29: case 30: case 31: case 11:
                    System.out.print(' ');
                    break;
                case '"': 
                    System.out.print("&quot;");
                    break;
                case '&': 
                    System.out.print("&amp;");
                    break;
                case '<':
                    System.out.print("&lt;");
                    break;
                case '>':
                    System.out.print("&gt;");
                    break;
                default:
                    System.out.print("&#" + (int)ch + ";");
            }                
        }
        if(lastPos < end) {
            System.out.print(new String(chars, lastPos, end - lastPos));
        }
        //*/
    }

}
