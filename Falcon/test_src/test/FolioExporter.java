package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class FolioExporter extends MainWithConfig {
	private static final Log log = Log.getLog();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new FolioExporter().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('i', "include", true, true, "include", "The folio ids to include");
		registerCommandLineSwitch('x', "exclude", true, true, "exclude", "The folio ids to exclude");
		registerCommandLineSwitch('o', "output", false, true, "output", "Directory for output");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		int[] include;
		int[] exclude;
		String outputDir;
		try {
			include = ConvertUtils.convert(int[].class, argMap.get("include"));
			exclude = ConvertUtils.convert(int[].class, argMap.get("exclude"));
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
		} catch(ConvertException e) {
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		try {
			Base.configureDataSourceFactory(properties, null);
		} catch(ServiceException e) {
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}
		try {
			Base.configureDataLayer(properties);
		} catch(ServiceException e) {
			finishAndExit(e.getMessage(), 400, true, null);
			return;
		}
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("includeFolioIds", include);
		params.put("excludeFolioIds", exclude);
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_FOLIOS", params);
		} catch(SQLException e) {
			finishAndExit(e.getMessage(), 500, true, null);
			return;
		} catch(DataLayerException e) {
			finishAndExit(e.getMessage(), 600, true, null);
			return;
		}
		while(results.next()) {
			String folioName;
			long folioId;
			try {
				folioName = results.getValue("FOLIO_NAME", String.class);
				folioId = results.getValue("FOLIO_ID", long.class);
				Results subResults;
				try {
					subResults = DataLayerMgr.executeQuery("EXPORT_FOLIO", new Object[] {folioId});
				} catch(SQLException e) {
					finishAndExit(e.getMessage(), 510, true, null);
					return;
				} catch(DataLayerException e) {
					finishAndExit(e.getMessage(), 610, true, null);
					return;
				}
				File file = new File(outputDir+"folio_" + folioName.trim().toLowerCase().replaceAll("[\\W]", "_") + ".sql");
				Writer writer = new FileWriter(file);
				writer.write("SET DEFINE OFF;\r\n");
				try {
					while(subResults.next()) {
						writer.write(subResults.getFormattedValue(1));
						writer.write("\r\n");
					}
					writer.write("/\r\nCOMMIT;\r\n");
					writer.flush();
				} finally {
					writer.close();
				}
				log.info("Wrote folio #" + folioId + " update sql to '" + file.getAbsolutePath() + "'");
			} catch(ConvertException e) {
				finishAndExit(e.getMessage(), 700, true, null);
				return;
			} catch(IOException e) {
				finishAndExit(e.getMessage(), 800, true, null);
				return;
			}
		}
		Log.finish();
	}
}
