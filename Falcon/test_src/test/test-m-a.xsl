<xsl:stylesheet xmlns="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:css="http://www.saf.org/simple/css"
	version="1.0">
	<!-- <xsl:include href="resource:simple/falcon/templates/report/css-pdf-base.xsl" /> -->
	<xsl:template match="/">
		<root>
		<xsl:call-template name="parser">
			<xsl:with-param name="nodes" select="child::node() | attribute::node() | child::text()"/>
		</xsl:call-template>
		</root>
	</xsl:template>
	
	<!-- Parse through and recreate each node -->
	<xsl:template name="parser">
		<xsl:param name="nodes"/>
		<xsl:for-each select="$nodes">
			<xsl:copy>
				<!-- Attach style attributes from selectors matching this node -->
				<xsl:apply-templates mode="stylesheet" select="self::node()" />
				
				<!-- Recurse nodes -->
				<xsl:call-template name="parser">
					<xsl:with-param name="nodes" select="child::node() | attribute::node() | child::text()"/>
				</xsl:call-template>
			</xsl:copy>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template mode="stylesheet" match="node()">
		<xsl:if test="local-name() = 'test'">
			<xsl:attribute name="color">white</xsl:attribute>
		</xsl:if>
		<xsl:if test="local-name() = 'b'">
			<xsl:attribute name="color">red</xsl:attribute>
		</xsl:if>
		<xsl:if test="local-name() = 'a'">
			<xsl:attribute name="color">black</xsl:attribute>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
