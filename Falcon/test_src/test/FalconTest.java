///*
// * Created on Nov 4, 2005
// *
// */
//package test;
//
//import java.beans.IntrospectionException;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.io.PrintWriter;
//import java.io.Writer;
//import java.lang.reflect.InvocationTargetException;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.Locale;
//import java.util.Map;
//import java.util.Properties;
//import java.util.TreeMap;
//
//import javax.servlet.ServletException;
//import javax.xml.transform.Result;
//import javax.xml.transform.stream.StreamResult;
//
//import org.xml.sax.SAXException;
//
//import com.sun.org.apache.xml.internal.serializer.ToXMLStream;
//
//import simple.app.ServiceException;
//import simple.bean.ConvertException;
//import simple.db.BasicDataSourceFactory;
//import simple.db.Call;
//import simple.db.DataLayerException;
//import simple.db.DataLayerMgr;
//import simple.db.DataSourceFactory;
//import simple.db.ParameterException;
//import simple.db.config.ConfigException;
//import simple.db.config.ConfigLoader;
//import simple.db.dbcp.DBCPDataSourceFactory;
//import simple.event.TaskListener;
//import simple.event.WriteCSVTaskListener;
//import simple.falcon.engine.DesignException;
//import simple.falcon.engine.ExecuteEngine;
//import simple.falcon.engine.ExecuteException;
//import simple.falcon.engine.Folio;
//import simple.falcon.engine.Output;
//import simple.falcon.engine.OutputType;
//import simple.falcon.engine.Param;
//import simple.falcon.engine.Report;
//import simple.falcon.engine.ResultsType;
//import simple.falcon.engine.Scene;
//import simple.falcon.engine.standard.StandardDesignEngine;
//import simple.falcon.engine.standard.StandardDesignEngine2;
//import simple.falcon.engine.standard.StandardExecuteEngine;
//import simple.falcon.engine.standard.StandardOutputType;
//import simple.falcon.servlet.FalconUser;
//import simple.io.IOUtils;
//import simple.io.LoadingException;
//import simple.io.Log;
//import simple.net.protocol.AddProtocols;
//import simple.results.BeanException;
//import simple.results.Results;
//import simple.text.StringUtils;
//import simple.translator.TranslatorFactory;
//import simple.xml.XMLBuilder;
//import simple.xml.sax.BeanAdapter1;
//import simple.xml.sax.BeanInputSource;
//
//public class FalconTest {
//    private static Log log;
//    /**
//     * @param args
//     * @throws BeanException
//     * @throws ConvertException
//     * @throws DataLayerException
//     * @throws SQLException
//     * @throws LoadingException
//     * @throws IOException
//     * @throws ConfigException
//     * @throws DesignException
//     * @throws ExecuteException
//     * @throws InstantiationException
//     * @throws InvocationTargetException
//     * @throws IllegalAccessException
//     * @throws IntrospectionException
//     * @throws ServletException
//     */
//    public static void main(String[] args) throws SQLException, DataLayerException, ConvertException, BeanException, ConfigException, IOException, LoadingException, ExecuteException, DesignException, ServletException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException {
//		// System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
//		// final File logFile = File.createTempFile("FalconTest-", ".log");
//		// System.setProperty("simple.io.logging.SimpleBridge.file", logFile.getAbsolutePath());
//        log = Log.getLog();
//        //System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.processor.TransformerFactoryImpl");
//        System.setProperty("javax.xml.transform.TransformerFactory", "com.jclark.xsl.trax.TransformerFactoryImpl");
//        AddProtocols.run();
//        Properties props = new Properties();
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "FOLIO_CONF", "FOLIO_CONF");
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(FAILOVER=TRUE)(LOAD_BALANCE=FALSE)(ADDRESS=(PROTOCOL=TCP)(HOST=10.0.0.45)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=10.0.0.49)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USARDB.world)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC)(RETRIES=1)(DELAY=5))))", "REPORT", "USATECH");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@10.0.0.45:1521:usardb01", "FOLIO_CONF", "fc45popwq");
//		addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "USALIVE_APP_2", "1_PPA_EVILASU");
//		addDS(props, "MAIN", "org.postgresql.Driver", "jdbc:postgresql://devdbs11:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true", "usalive_2", "USALIVE_1");
//		addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "USALIVE_APP_2", "1_PPA_EVILASU");
//		//addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "FOLIO_CONF", "FOLIO_CONF");
//		//addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV03.WORLD)))", "FOLIO_CONF", "FOLIO_CONF");
//		//addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV02.WORLD)))", "FOLIO_CONF", "FOLIO_CONF");
//		//addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))", "SYSTEM", "Pass4ecc");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))","FOLIO_CONF", "fc45popwq");
//        DataSourceFactory dsf = new DBCPDataSourceFactory(props);
//        DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
//        ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml");
//        final File taskTimesFile = File.createTempFile("TaskTimes-", ".csv");
//        PrintWriter pw = new PrintWriter(taskTimesFile);
//		String folder = "local_20131108";
//        //Engine 1
//        /*
//        final StandardDesignEngine sde1 = new StandardDesignEngine();
//        final ExecuteEngine ee1 = new StandardExecuteEngine(sde1);
//        ee1.setDataSourceFactory(dsf);
//        sde1.init();
//        TaskListener tl1 = new WriteCSVTaskListener(pw) {
//			@Override
//			protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
//				super.taskRan(taskName, taskDetails, threadId, "Engine #1", startTime, endTime);
//			}
//        };
//        ee1.setTaskListener(tl1);
//
//        StandardOutputType outputType1 = new StandardOutputType();
//        outputType1.setProtocol("xsl");
//        //outputType1.setContentType("text/xml");
//        //outputType1.setPath("resource:simple/falcon/templates/general/xml.xsl");
//        outputType1.setContentType("text/html");
//        outputType1.setPath("resource:simple/falcon/templates/report/show-report-html.xsl");
//        */
//        //Engine 2
//		final StandardDesignEngine2 sde2 = new StandardDesignEngine2();
//        final ExecuteEngine ee2 = new StandardExecuteEngine(sde2);
//        ee2.setDataSourceFactory(dsf);
//
//        TaskListener tl2 = new WriteCSVTaskListener(pw) {
//			@Override
//			protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
//				super.taskRan(taskName, taskDetails, threadId, "Engine #2", startTime, endTime);
//			}
//        };
//        ee2.setTaskListener(tl2);
//
//        ee2.setCssDirectory("file:///E:/Java Projects/usalive/web/");
//		StandardOutputType outputType = new StandardOutputType();
//		outputType.setProtocol("direct");
//		// outputType1.setContentType("text/xml");
//		// outputType1.setPath("resource:simple/falcon/templates/general/xml.xsl");
//		outputType.setContentType("application/xhtml+xml");
//		outputType.setPath("simple.falcon.engine.standard.DirectXHTMLGenerator");
//
//        /*ee2.registerGenerator("xsl", new XSLGenerator(ee2){
//			@Override
//			protected XMLReader createReader() {
//		        switch(inputSourceType) {
//			        case REPORT: return new ReportAdapter2<ReportInputSource2>();
//			        case CHART: return new ChartAdapter();
//			        default: return null;
//			    }
//			}
//			protected InputSource createInputSource(Report report, Results[] results, Map<String,Object> parameters, Scene scene, OutputType outputType) {
//		        switch(inputSourceType) {
//		            case REPORT: return new ReportInputSource2(report, results, parameters, engine.getDesignEngine().getColumnNamer(), scene, outputType);
//		            case CHART: return new ChartInputSource(report, results, parameters, engine.getDesignEngine().getColumnNamer(), scene, outputType);
//		            default: return null;
//		        }
//		    }
//        });
//        StandardOutputType outputType2 = new StandardOutputType();
//        outputType2.setProtocol("xsl");
//        //outputType2.setContentType("text/xml");
//        //outputType2.setPath("resource:simple/falcon/templates/general/xml.xsl");
//        outputType2.setContentType("text/html");
//        outputType2.setPath("resource:simple/falcon/templates/report/show-report-html2.xsl");
//        */
//
//        final Map<String,Object> parameters = new HashMap<String, Object>();
//        //parameters.put("resultsType", "ARRAY");
//        //parameters.put("StartDate", "01/01/2006");
//        //parameters.put("EndDate","03/01/2006");
//        //parameters.put("AllTerminals","1");
//        parameters.put("promptForParams", "false");
//        //parameters.put("DocId", "16901");
//        //parameters.put("BatchId", "49001");
//        //parameters.put("CurrencyId","1");
//        //parameters.put("Month","05/2006");
//        //parameters.put("CustomerBankId","57310");
//        //parameters.put("DeviceTypeId", "0,1");
//
//        parameters.put("startDate", "01/01/2006");
//        //parameters.put("startDate", "10/01/2006");
//		parameters.put("FeeTypeId", 1);
//		parameters.put("CustomerBankId", 665);
//        parameters.put("TotalCreditAmount", 50);
//        parameters.put("DeviceSerialNumber", "E4070558");
//        parameters.put("DeviceTypeId", "0,1");
//        parameters.put("currencyId", 1);
//        parameters.put("TransactionMonth","9/1/2006");
//        //parameters.put("TransactionMonth","2/1/2006");
//        parameters.put("Month","9/1/2006");
//        //parameters.put("Month","2/1/2006");
//        parameters.put("CustomerId", 805);
//        //parameters.put("TerminalId", 10659);
//        parameters.put("TerminalId", "");
//        parameters.put("TransTypeId", 16);
//        parameters.put("EndDate","03/01/2006");
//        //parameters.put("EndDate","10/02/2006");
//        //parameters.put("DocId", 23148);
//        parameters.put("DocId", 6567);
//        parameters.put("BeginDate", "{*-2592000000}");
//        parameters.put("LocationName", "");
//        parameters.put("StartDate", "10/01/2006 22:00");
//        //parameters.put("StartDate", "01/01/2006");
//        parameters.put("AllTerminals", 1);
//        parameters.put("CallOutcome","Success");
//        parameters.put("searchAmt", 20);
//        parameters.put("BatchId", 234952);
//        parameters.put("CardNumber", "5403247054101406");
//        parameters.put("CurrencyTypeId", 1);
//        parameters.put("CurrencyId", 1);
//        parameters.put("SettleState","settled");
//        parameters.put("BusinessUnitId","1,2,3,4,5");
//        parameters.put("endDate","10/02/2006");
//        parameters.put("resultsType", "CACHE");
//        //parameters.put("endDate","03/01/2006");
//
//        //parameters.put("CustomerId", 805);
//        //parameters.put("CurrencyId", "1,2,3,4");
//        parameters.put("StartDate", "02/19/2006");
//        parameters.put("EndDate","02/27/2006");
//        parameters.put("DeviceTypeId", "0,1,2,3,4,5,6,7,8,9,10,11");
//        //parameters.put("TransTypeId", "20,21");
//
//        //folio.setOutputType(ee.getDesignEngine().getOutputType(22));
//        //custom config
//        /*
//        folio.setOutputType(ee.getDesignEngine().getOutputType(28));
//        parameters.put("chart.cluster", "2,");
//        parameters.put("chart.value", "3.1,4.1,");
//        parameters.put("chartType", "MultiLineChart");
//        parameters.put("chart.groupLevel1", "1,");
//        parameters.put("chart.groupLevel0", "2,3,4");
//
//        FolioStepHelper.chartConfig(folio, new MapInputForm(parameters), ee.getDesignEngine());
//        */
//		Map<Long, Boolean> summary = new TreeMap<Long, Boolean>();
//        try {
//        	//beanXmlTest(ee2, 354);
//        	//paramsTest(ee2, 246);
//	        //sqlTest(ee2, 39);
//	        //singleTest(ee1, 25, parameters, outputType1);
//	        //singleTest(ee2, 25, parameters, outputType2);
//	        //printAllParams(ee1);
//        	//compareEngines(ee1, ee2, parameters, outputType1, outputType2);
//        	//compareEngines(ee2, ee1, parameters, outputType2, outputType1);
//			/*
//			for(int i = 1; i <= 1; i++) {
//				log.info("Test Run #" + i + " ********************************");
//				testEngine(ee2, parameters, outputType);
//				// testDesignEngine(sde2, tl2);
//			}*/// *
//			printFolioSqls(ee2, folder);
//			// sde2.setFullJoinSyntax(false);
//			// printFolioSqls(ee2);
//				//*/
//			// testEngine(ee2, parameters, outputType, summary);
//        } catch(Exception e) {
//        	log.warn("Error occurred", e);
//		} finally {
//			StringBuilder sb = new StringBuilder("Summary:\n");
//			for(Map.Entry<Long, Boolean> entry : summary.entrySet())
//				sb.append('\t').append(entry.getKey()).append("\t--> ").append(entry.getValue() ? "okay" : "ERROR").append('\n');
//			log.warn(sb.toString());
//        }
//        //if(ee1.getTaskListener() != null) ee1.getTaskListener().flush();
//        if(ee2.getTaskListener() != null) ee2.getTaskListener().flush();
//        log.debug("Task Times Written to " + taskTimesFile);
//        Log.finish();
//    }
//
//
//    protected static void beanXmlTest(ExecuteEngine ee, long folioId) throws DesignException, IOException, SAXException, InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException {
//    	final Folio folio = ee.getDesignEngine().getFolio(folioId);
//    	final OutputStream adapterXmlStream = new FileOutputStream(File.createTempFile("BeanXML-", "-Adapter.xml"));
//    	final OutputStream builderXmlStream = new FileOutputStream(File.createTempFile("BeanXML-", "-Builder.xml"));
//    	final String tagName = "folio";
//
//    	BeanAdapter1 adapter = new BeanAdapter1();
//    	ToXMLStream txsA = new ToXMLStream();
//        txsA.setOutputStream(adapterXmlStream);
//        txsA.setIndentAmount(4);
//        txsA.setIndent(true);
//        txsA.setLineSepUse(true);
//    	adapter.setContentHandler(txsA);
//    	adapter.parse(new BeanInputSource(folio, tagName));
//    	txsA.flushPending();
//    	adapterXmlStream.flush();
//
//    	ToXMLStream txsB = new ToXMLStream();
//        txsB.setOutputStream(builderXmlStream);
//        txsB.setIndentAmount(4);
//        txsB.setIndent(true);
//        txsB.setLineSepUse(true);
//    	XMLBuilder builder = new XMLBuilder(txsB);
//    	builder.setNamespace("http://simple/bean/1.0");
//    	builder.put(tagName, folio);
//    	txsB.flushPending();
//    	builderXmlStream.flush();
//    }
//    protected static void printAllParams(ExecuteEngine ee) throws DesignException, ConvertException, SQLException, DataLayerException {
//    	Collection<Param> params = getAllParams(ee);
//    	log.info("Parameters for folio--------");
//    	for(Param p : params) {
//        	log.info("Param '" + p.getName() + "' (default=" + p.getValue() + ") - " + p.getLabel());
//    	}
//    	log.info("End of Parameters--------");
//
//    }
//    protected static Collection<Param> getAllParams(ExecuteEngine ee) throws DesignException, ConvertException, SQLException, DataLayerException {
//    	Map<String,Param> params = new HashMap<String, Param>();
//    	Results results = DataLayerMgr.executeQuery("GET_FOLIOS", null);
//        while(results.next()) {
//        	long folioId = results.getValue("folioId", long.class);
//        	Folio folio = ee.getDesignEngine().getFolio(folioId);
//        	for(Param p : folio.getFilter().gatherParams()) {
//                if(p.getName() != null && p.getName().trim().length() > 0)
//                	params.put(p.getName(), p);
//        	}
//        }
//    	return params.values();
//    }
//    protected static void paramsTest(ExecuteEngine ee, long folioId) throws DesignException {
//    	Folio folio = ee.getDesignEngine().getFolio(folioId);
//    	log.info("Parameters for folio--------");
//    	for(Param p : folio.getFilter().gatherParams()) {
//        	log.info("Param '" + p.getLabel() + "' (default=" + p.getValue() + ")");
//    	}
//    	log.info("End of Parameters--------");
//    }
//    protected static void sqlTest(ExecuteEngine ee, long folioId) throws DesignException {
//    	Folio folio = ee.getDesignEngine().getFolio(folioId);
//    	log.info("SQL= " + ee.getDesignEngine().getCall(folio, new long[] {1}).getSql());
//    }
//
//	protected static void singleTest(final ExecuteEngine ee, long folioId, final Map<String, Object> parameters, OutputType outputType) throws DesignException, ParameterException, IOException, ExecuteException, ServiceException {
//    	runFolio(ee, folioId, parameters, outputType);
//    }
//    protected static void multiThreadTest(final ExecuteEngine ee, long folioId, final Map<String,Object> parameters, OutputType outputType, int threadCount) throws DesignException {
//    	Folio folio = ee.getDesignEngine().getFolio(folioId);
//    	final Report report = ee.getDesignEngine().newReport();
//        report.setFolios(new Folio[] {folio});
//        if(outputType == null)
//        	outputType = folio.getOutputType();
//    	report.setOutputType(outputType);
//
//    	Runnable run = new Runnable() {
//			public void run() {
//				try {
//					runReport(ee, report, parameters);
//				} catch (IOException e) {
//					log.warn(e);
//				} catch (ExecuteException e) {
//					log.warn(e);
//				} catch (DesignException e) {
//					log.warn(e);
//				} catch (ParameterException e) {
//					log.warn(e);
//				} catch(ServiceException e) {
//					log.warn(e);
//				}
//			}
//        };
//        Thread[] threads = new Thread[threadCount];
//        for(int i = 0; i < threads.length; i++)
//        	threads[i] = new Thread(run, "Runner-" + (i+1));
//        for(int i = 0; i < threads.length; i++)
//        	threads[i].start();
//        for(int i = 0; i < threads.length; i++)
//			try {
//				threads[i].join();
//			} catch (InterruptedException e) {
//				log.warn(e);
//			}
//    }
//    private static void addDS(Properties props, String name, String driver, String url, String username, String password) {
//        props.put(name + "." + BasicDataSourceFactory.PROP_DATABASE, url);
//        props.put(name + "." + BasicDataSourceFactory.PROP_DRIVER, driver);
//        props.put(name + "." + BasicDataSourceFactory.PROP_USERNAME, username);
//        props.put(name + "." + BasicDataSourceFactory.PROP_PASSWORD, password);
//    }
//
//	protected static File runReport(ExecuteEngine ee, Report report, Map<String, Object> parameters) throws IOException, ExecuteException, DesignException, ParameterException, ServiceException {
//        FalconUser executor = new FalconUser();
//        executor.setUserId(1);
//        executor.setUserGroupIds(new long[] {1});
//        Scene scene = new Scene();
//        scene.setUserAgent("");
//        String baseUrl = "http://localhost:8580/CustomerReporting/";
//        Locale locale = null;
//        scene.setBaseUrl(baseUrl);
//        //scene.setStylesheets(new String[] {"css/default-style.css"});
//        scene.setStylesheets(new String[] {"css/usalive-general-style.css","css/usalive-report-style.css"});
//        if(locale != null) scene.setLocale(locale);
//        scene.setTranslator(TranslatorFactory.getDefaultFactory().getTranslator(locale, baseUrl));
//
//        final File reportFile = File.createTempFile("FX-", "-" + report.getOutputType().generateFileName(report, parameters));
//        Output out = new Output() {
//            public Writer getWriter() throws IOException {
//                return new FileWriter(reportFile);
//            }
//            public OutputStream getOutputStream() throws IOException {
//                return new FileOutputStream(reportFile);
//            }
//			public Result getResult() throws IOException {
//				return new StreamResult(reportFile);
//			}
//		};
//		try {
//        for(int i = 0; i < 1; i++) {
//            ee.buildReport(report, parameters, executor, report.getOutputType(), scene, out, ResultsType.FORWARD);
//            log.debug("Finished creating report");
//			}
//		} catch(Exception e) {
//			log.warn("Could not build report", e);
//			return null;
//        }
//		log.info("Report written to " + reportFile);
//        return reportFile;
//    }
//
//	protected static File runFolio(ExecuteEngine ee, Folio folio, Map<String, Object> parameters, OutputType outputType) throws IOException, ExecuteException, DesignException, ParameterException, ServiceException {
//        Report report = ee.getDesignEngine().newReport();
//        report.setFolios(new Folio[] {folio});
//        report.setOutputType(outputType);
//        return runReport(ee, report, parameters);
//    }
//
//	protected static void runFolio(ExecuteEngine ee, long folioId, Map<String, Object> parameters, OutputType outputType) throws ParameterException, IOException, ExecuteException, DesignException, ServiceException {
//    	Folio folio = ee.getDesignEngine().getFolio(folioId);
//    	if(outputType == null) outputType = folio.getOutputType();
//    	runFolio(ee, folio, parameters, outputType);
//    }
//
//	protected static void compareEngines(ExecuteEngine ee1, ExecuteEngine ee2, Map<String, Object> parameters, OutputType outputType1, OutputType outputType2) throws IOException, SQLException, DataLayerException, ConvertException, DesignException, ExecuteException, ServiceException {
//        Results results = DataLayerMgr.executeSQL("metadata",
//        		"SELECT FOLIO_ID folioId FROM FOLIO_CONF.FOLIO " +
//        		"WHERE FOLIO_ID NOT IN(250, 146, 242, 244, 31, 108) AND FOLIO_ID NOT IN(222) AND FOLIO_ID > 222 " +
//        		"ORDER BY FOLIO_ID", null, null);
//        while(results.next()) {
//        	long folioId = results.getValue("folioId", long.class);
//        	log.info("Testing Folio " + folioId + ": -----------------------");
//        	if(ee1.getTaskListener() != null) ee1.getTaskListener().taskStarted("get-folio", "Folio Id " + folioId);
//        	Folio folio = ee1.getDesignEngine().getFolio(folioId);
//        	if(ee1.getTaskListener() != null) ee1.getTaskListener().taskEnded("get-folio");
//        	File file1 = runFolio(ee1, folio, parameters, outputType1 == null ? folio.getOutputType() : outputType1);
//        	if(ee2.getTaskListener() != null) ee2.getTaskListener().taskStarted("get-folio", "Folio Id " + folioId);
//        	folio = ee2.getDesignEngine().getFolio(folioId);
//        	if(ee2.getTaskListener() != null) ee2.getTaskListener().taskEnded("get-folio");
//        	File file2 = runFolio(ee2, folio, parameters, outputType2 == null ? folio.getOutputType() : outputType2);
//        	int d = IOUtils.firstDifference(file1, file2);
//        	if(d > -1)
//        		log.warn("The generated files " + file1.getAbsolutePath() + " and " + file2.getAbsolutePath() + " from the two engines do not match at position " + (d+1));
//        }
//    }
//
//	protected static void testEngine(ExecuteEngine ee, Map<String, Object> parameters, OutputType outputType, Map<Long, Boolean> summary) throws IOException, SQLException, DataLayerException, ConvertException, DesignException, ExecuteException {
//        Results results = DataLayerMgr.executeSQL("metadata",
//        		"SELECT FOLIO_ID folioId FROM FOLIO_CONF.FOLIO " +
//        		"WHERE FOLIO_ID > 0 " +
//		        // "WHERE FOLIO_ID IN(246)" +
//        		"ORDER BY FOLIO_ID", null, null);
//        while(results.next()) {
//        	long folioId = results.getValue("folioId", long.class);
//        	log.info("Testing Folio " + folioId + ": -----------------------");
//        	if(ee.getTaskListener() != null) ee.getTaskListener().taskStarted("get-folio", "Folio Id " + folioId);
//        	Folio folio = ee.getDesignEngine().getFolio(folioId);
//        	if(ee.getTaskListener() != null) {
//        		ee.getTaskListener().taskEnded("get-folio");
//        		ee.getTaskListener().taskStarted("get-call", "Folio Id " + folioId);
//        	}
//			try {
//				Call call = ee.getDesignEngine().getCall(folio, new long[] { 1, 100 });
//				log.info("SQL for Folio " + folioId + ":\n" + call.getSql());
//			} catch(DesignException e) {
//				log.warn("Could not get call for folio " + folioId, e);
//				summary.put(folioId, false);
//				continue;
//			} finally {
//				if(ee.getTaskListener() != null)
//					ee.getTaskListener().taskEnded("get-call");
//			}
//			try {
//				File file = runFolio(ee, folio, parameters, outputType == null ? folio.getOutputType() : outputType);
//				summary.put(folioId, file != null);
//			} catch(Exception e) {
//				log.warn("Could not get generate report for folio " + folioId, e);
//				summary.put(folioId, false);
//			}
//        }
//    }
//
//    protected static void testDesignEngine(StandardDesignEngine de, TaskListener taskListener) throws IOException, SQLException, DataLayerException, ConvertException, DesignException, ExecuteException {
//        Results results = DataLayerMgr.executeSQL("metadata",
//        		"SELECT FOLIO_ID folioId FROM FOLIO_CONF.FOLIO " +
//        		"WHERE FOLIO_ID IN(914)" + //NOT IN(222, 344) " +
//        		//"AND FOLIO_ID > 344 " +
//        		"ORDER BY FOLIO_ID", null, null);
//        Connection conn = DataLayerMgr.getConnection("REPORT");
//        try {
//	        while(results.next()) {
//	        	long folioId = results.getValue("folioId", long.class);
//	        	log.info("Testing Folio " + folioId + ": -----------------------");
//	        	if(taskListener != null)
//	        		taskListener.taskStarted("get-folio", "Folio Id " + folioId);
//	        	Folio folio = de.getFolio(folioId);
//	        	if(taskListener != null) {
//	        		taskListener.taskEnded("get-folio");
//	        		taskListener.taskStarted("get-call", "Folio Id " + folioId);
//	        	}
//				try {
//					de.getCall(folio, new long[] { 1, 100 }, conn);
//				} catch(DesignException e) {
//					log.warn("Could not get call for folio " + folioId, e);
//				}
//	        	if(taskListener != null)
//	        		taskListener.taskEnded("get-call");
//	        }
//        } finally {
//        	conn.close();
//        }
//    }
//
//	protected static void printFolioSqls(ExecuteEngine ee, String folder) throws IOException, SQLException, DataLayerException, ConvertException, DesignException {
//		boolean full = ee.getDesignEngine() instanceof StandardDesignEngine2 && ((StandardDesignEngine2) ee.getDesignEngine()).isFullJoinSyntax();
//		File dir = new File("D:\\Development\\Java Projects\\usalive\\folio-sql\\" + folder); // "local_" + (full ? "full" : "old") + "_after_3");
//		dir.delete();
//		dir.mkdirs();
//		Results results = DataLayerMgr.executeSQL("metadata",
//				"SELECT FOLIO_ID folioId FROM FOLIO_CONF.FOLIO "
//				//+ "WHERE FOLIO_ID = 1199 "
//				+ "ORDER BY FOLIO_ID",
//				null, null);
//		while(results.next()) {
//			long folioId = results.getValue("folioId", long.class);
//			log.info("Testing Folio " + folioId + ": -----------------------");
//			Folio folio = ee.getDesignEngine().getFolio(folioId);
//			FileOutputStream out = new FileOutputStream(new File(dir, "folio-" + folioId + ".sql"));
//			try {
//				Call call = ee.getDesignEngine().getCall(folio, new long[] { 8 });
//				out.write(call.getSql().getBytes());
//				log.info("SQL for Folio " + folioId + ":\n" + call.getSql());
//			} catch(DesignException e) {
//				out.write(StringUtils.exceptionToString(e).getBytes());
//				log.warn("Could not get call for folio " + folioId, e);
//			} finally {
//				out.close();
//			}
//		}
//	}
//
//}
