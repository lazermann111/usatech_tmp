//package test;
//
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.Map;
//import java.util.Properties;
//
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.xml.sax.SAXException;
//
//import simple.app.Base;
//import simple.app.MainWithConfig;
//import simple.app.ServiceException;
//import simple.bean.ConvertException;
//import simple.bean.ConvertUtils;
//import simple.falcon.engine.DesignEngine;
//import simple.falcon.engine.DesignException;
//import simple.falcon.engine.Folio;
//import simple.falcon.engine.standard.StandardDesignEngine2;
//import simple.io.Log;
//import simple.xml.ObjectBuilder;
//
//public class FolioImporter extends MainWithConfig {
//	//private static final Log log = Log.getLog();
//
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		new FolioImporter().run(args);
//	}
//
//	@Override
//	protected void registerDefaultCommandLineArguments() {
//		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
//		registerCommandLineSwitch('i', "inputFile", false, true, "inputFile", "The file with the folio xml");
//	}
//
//	@Override
//	protected void registerDefaultActions() {
//		// no actions
//	}
//	@Override
//	public void run(String[] args) {
//		Map<String,Object> argMap;
//		try {
//			argMap = parseArguments(args);
//		} catch(IOException e) {
//			finishAndExit(e.getMessage(), 100, true, e);
//			return;
//		}
//		InputStream inputFile;
//		try {
//			inputFile = new FileInputStream(ConvertUtils.convert(String.class, argMap.get("inputFile")));
//		} catch(ConvertException e) {
//			finishAndExit(e.getMessage(), 200, true, e);
//			return;
//		} catch(FileNotFoundException e) {
//			finishAndExit(e.getMessage(), 201, true, e);
//			return;
//		}
//		Properties properties;
//		try {
//			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
//		} catch(IOException e) {
//			finishAndExit("Could not read properties file", 120, e);
//			return;
//		}
//		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
//			properties.put(entry.getKey(), entry.getValue());
//		}
//		try {
//			Base.configureDataSourceFactory(properties, null);
//		} catch(ServiceException e) {
//			finishAndExit(e.getMessage(), 300, true, e);
//			return;
//		}
//		try {
//			Base.configureDataLayer(properties);
//		} catch(ServiceException e) {
//			finishAndExit(e.getMessage(), 400, true, e);
//			return;
//		}
//		try {
//			ObjectBuilder builder = new ObjectBuilder(inputFile);
//			DesignEngine designEngine = new StandardDesignEngine2();
//			Folio folio = designEngine.newFolio();
//			folio.readXML(builder);
//			designEngine.saveFolio(folio);
//		} catch(DesignException e) {
//			finishAndExit(e.getMessage(), 500, true, e);
//			return;
//		} catch(IOException e) {
//			finishAndExit(e.getMessage(), 501, true, e);
//			return;
//		} catch(SAXException e) {
//			finishAndExit(e.getMessage(), 502, true, e);
//			return;
//		} catch(ParserConfigurationException e) {
//			finishAndExit(e.getMessage(), 503, true, e);
//			return;
//		}
//		Log.finish();
//	}
//}
