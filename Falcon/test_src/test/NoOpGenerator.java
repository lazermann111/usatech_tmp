/*
 * Created on Aug 24, 2005
 *
 */
package test;

import java.util.Map;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Generator;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.results.Results;

public class NoOpGenerator implements Generator {
    protected ExecuteEngine engine;

    public NoOpGenerator(ExecuteEngine engine) {
        this.engine = engine;
    }

	public void generate(Report report, OutputType outputType, Map<String, Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
        // generate the report (just iterator through results
    	/*
    	long roughCount = 0;
        Folio[] folios = report.getFolios();
        for(int i = 0; i < folios.length; i++) {
        	String tmp = folios[i].getTitle(parameters);
        	if(tmp != null)	roughCount += tmp.length();
        	tmp = folios[i].getSubtitle(parameters);
        	if(tmp != null)	roughCount += tmp.length();
        	Pillar[] pillars = folios[i].getPillars();
        	for(int k = 0; k < pillars.length; k++) {
                tmp = pillars[k].getLabel();
                if(tmp != null)	roughCount += tmp.length();
            }
        	while(results[i].next()) {
        		for(int k = 0; k < pillars.length; k++) {
                    tmp = pillars[k].getDisplayText(results[i]);
                    if(tmp != null)	roughCount += tmp.length();
                }
            }
        }
        Log.getLog().debug("Generated Report of rough size " + roughCount);
        */
    }
    public boolean isResetResultsRequired(OutputType outputType) {
    	return false;
    }
}