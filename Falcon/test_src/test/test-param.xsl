<xsl:stylesheet xmlns="http://none"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xalan="http://xml.apache.org/xalan"
	version="1.0">
	<xsl:template match="/">
		<xsl:call-template name="sub">
			<xsl:with-param name="list">
				<item>This is the first item</item>
				<item>This is the second item</item>
				<stuff>And here is some stuff</stuff>				
			</xsl:with-param>
		</xsl:call-template>	
	</xsl:template> 
	
	<xsl:template name="sub">
		<xsl:param name="list"/>
		<xsl:for-each select="xalan:nodeset($list)/*">
	 		<xsl:copy-of select="."/>
	 		---------------------
	 		<xsl:variable name="index" select="position()"/>
	 		#<xsl:value-of select="$index"/> = '<xsl:value-of select="."/>';
		 </xsl:for-each>
 	</xsl:template>			
</xsl:stylesheet>
  