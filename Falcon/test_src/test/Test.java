/*
 * Created on Nov 19, 2004
 *
 */
package test;

import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * @author bkrug
 *
 */
public class Test {
    private static final String xslText = 
        "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" " +
                "xmlns:system=\"xalan://test.Test\" version=\"1.0\">" +
            "<xsl:template match=\"/\">" +
                "<xsl:for-each select=\"symbols/*\">" +
                    "The following should be blank: '<xsl:value-of select=\"system:giveNull()\"/>'.\n" +
                    "But this should NOT: '<xsl:value-of select=\"system:code(string())\"/>'.\n" +
                 "</xsl:for-each>" +
            "</xsl:template>" +
        "</xsl:stylesheet>";
    public static void main(String[] args) throws Throwable {
        testNull("org.apache.xalan.processor.TransformerFactoryImpl", Collections.EMPTY_MAP);
        Map<String,Object> a = new HashMap<String,Object>();
        a.put("generate-translet", true);
        a.put("debug", true);
        a.put("jar-name", "xsltc-translets.jar");
        testNull("org.apache.xalan.xsltc.trax.TransformerFactoryImpl", a);        
        testNull("com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl", a);
    }
    
    public static void testNull(String transformerFactoryClass, Map<String,Object> attributes) throws Throwable {
        System.setProperty("javax.xml.transform.TransformerFactory", transformerFactoryClass);
        String xml = "<symbols><letter>A</letter><letter>B</letter><number>1</number><number>2</number><letter>C</letter><letter>D</letter><number>3</number><letter>E</letter></symbols>";        
        InputStream xsl = new StringBufferInputStream(xslText);
        TransformerFactory tf = TransformerFactory.newInstance();
        for(Map.Entry<String, Object> e : attributes.entrySet()) {
            tf.setAttribute(e.getKey(), e.getValue());
        }
        System.out.println("*** Running a test using TransformerFactory " + tf.getClass().getName());
        Transformer t = tf.newTransformer(new StreamSource(xsl));       
        t.transform(new StreamSource(new StringBufferInputStream(xml)), new StreamResult(System.out));
        System.out.println();
        System.out.println("*** Done running this test");
        System.out.println();
    }
    
    public static Object giveNull() {
        return null;
    }
    public static String code(String s) {
        return "" + (char)(32 + (s.hashCode() % 64));
    }
}
