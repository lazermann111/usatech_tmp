/*
 * Created on Nov 19, 2004
 *
 */
package test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.InputSource;

import simple.net.protocol.AddProtocols;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.xml.TemplatesLoader;
import simple.xml.sax.AggregatingAdapter;
import simple.xml.sax.AggregatingInputSource;
import simple.xml.sax.ParametersAdapter;
import simple.xml.sax.ParametersInputSource;

/**
 * @author bkrug
 *
 */
public class XSLTest {
    public static void main(String[] args) throws Throwable {
        AddProtocols.run();

        /*
        String xslFile = args[0];
        String xmlText = "<symbols><letter>A</letter><letter>B</letter><number>1</number><number>2</number><letter>C</letter><letter>D</letter><number>3</number><letter>E</letter></symbols>";
        InputStream xsl = XSLTest.class.getClassLoader().getResourceAsStream(xslFile);
        InputStream xml = new StringBufferInputStream(xmlText);
        testXSL("org.apache.xalan.processor.TransformerFactoryImpl", xsl, xml);
        xsl = XSLTest.class.getClassLoader().getResourceAsStream(xslFile);
        xml = new StringBufferInputStream(xmlText);
        testXSL("com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl", xsl, xml);
        */
        //URL xsl = XSLTest.class.getClassLoader().getResource("test/css-x.xsl");
        //InputStream xml = XSLTest.class.getClassLoader().getResourceAsStream("test/report.xml");
        //testXSL("org.apache.xalan.processor.TransformerFactoryImpl", xsl, xml);
        //testXSL("com.jclark.xsl.trax.TransformerFactoryImpl", new File("C:\\Java Projects\\usalive\\web\\WEB-INF\\classes\\templates\\general\\frame.xsl").toURL());
        URL xsl = XSLTest.class.getResource("convertXLSTToJava.xsl");
		/*testXSL("com.jclark.xsl.trax.TransformerFactoryImpl",
				xsl, new SAXSource(new InputSource(XSLTest.class.getResourceAsStream("/simple/falcon/templates/general/html-base.xsl"))), "", new FileOutputStream(new File(xsl.toURI().resolve("DirectBaseGenerator.java"))));
		// */
        /*testXSL(//"org.apache.xalan.processor.TransformerFactoryImpl",
        		"com.jclark.xsl.trax.TransformerFactoryImpl",
        		xsl, new SAXSource(new InputSource(XSLTest.class.getResourceAsStream("/simple/falcon/templates/report/show-report-base.xsl"))), "", new FileOutputStream(new File(xsl.toURI().resolve("DirectXHTMLGenerator.java"))));
        		*/
		// InputStream src = new FileInputStream("D:\\Development\\Java Projects\\usalive\\xsl\\templates\\general\\app-base.xsl");
		// InputStream src = XSLTest.class.getResourceAsStream("/simple/falcon/templates/report/show-report-base.xsl");
		// InputStream src = XSLTest.class.getResourceAsStream("/simple/falcon/templates/general/html-base.xsl");
		InputStream src = XSLTest.class.getResourceAsStream("/simple/falcon/templates/report/show-report-html.xsl");
		File targetFile = new File(xsl.toURI().resolve("HtmlReportGenerator.java"));
        Map<String, Object> params = new HashMap<String, Object>();
		params.put("packageName", "test");
		params.put("className", "HtmlReportGenerator");
        
		testXSL("com.jclark.xsl.trax.TransformerFactoryImpl",
        		xsl, new SAXSource(new InputSource(src)), new FileOutputStream(targetFile), params);
        
    }

    public static void testXSL(String transformerFactoryClass, URL xsl, InputStream xml) throws Throwable {
        testXSL(transformerFactoryClass, xsl, new SAXSource(new InputSource(xml)), null, System.out);
    }
    public static void testXSL(String transformerFactoryClass, URL xsl, Source xml, Object context, OutputStream out) throws Throwable {
    	testXSL(transformerFactoryClass, xsl, xml, out, Collections.singletonMap("context", context));
    }
    public static void testXSL(String transformerFactoryClass, URL xsl, Source xml, OutputStream out, Map<String, Object> params) throws Throwable {
        try {
            System.setProperty("javax.xml.transform.TransformerFactory", transformerFactoryClass);
            TransformerFactory tf = TransformerFactory.newInstance();
            System.out.println("*** Running a test using TransformerFactory " + tf.getClass().getName());
            Translator translator = DefaultTranslatorFactory.getTranslatorInstance();
            TemplatesLoader tl = TemplatesLoader.getInstance(translator);
            tl.setFactory(tf);
            Transformer transformer = tl.newTransformer(xsl);
            if(params != null)
	            for(Map.Entry<String, Object> entry : params.entrySet())
	            	transformer.setParameter(entry.getKey(), entry.getValue());
            transformer.transform(xml,  new StreamResult(out));

            System.out.println();
            System.out.println("*** Done running this test");
            System.out.println();
        } catch(Exception e) {
            System.out.println("ERROR: ");
            e.printStackTrace();
        }
    }

    protected static final ParametersAdapter parametersAdapter = new ParametersAdapter(); // only one is needed since it is thread-safe

    public static void testXSL(String transformerFactoryClass, URL xsl) throws Throwable {
        Map<String,Object> context = new HashMap<String, Object>();
        context.put("request.requestURL", new StringBuffer("HA!"));
        context.put("content", "go2somewhere");

        Source[] sources = new Source[1];
        String[] xpaths = new String[1];
        sources[0] = new SAXSource(parametersAdapter, new ParametersInputSource(context));
        xpaths[0] = "parameters";
        Source src = new SAXSource(new AggregatingAdapter(), new AggregatingInputSource(sources, xpaths, null));
        testXSL(transformerFactoryClass, xsl, src, context, System.out);
    }

    public static Object giveNull() {
        return null;
    }
}
