/*
 * Created on Sep 13, 2005
 *
 */
package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class BinaryFileCompare {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception{
        // TODO Auto-generated method stub
        File f1 = new File("C:/TEMP/chart1.png");
        File f2 = new File("C:/TEMP/html-test/Test Chart_files/get_chart.png");
        
        InputStream is1 = new FileInputStream(f1);
        InputStream is2 = new FileInputStream(f2);
        int c1 = 0;
        int c2 = 0;   
        int index = 0;
        byte[] buf1 = new byte[100];
        byte[] buf2 = new byte[100];  
        int pos = 1;
        OUTER: while(true) {
            while(c1 == c2) {
                c1 = is1.read();
                c2 = is2.read();
                if(c1 == -1 || c2 == -1) break OUTER;
                pos++;
            }
            buf1[0] = (byte)c1;
            buf2[0] = (byte)c2;
            for(index = 1; !contains(buf1, c2) && !contains(buf2, c1); index++) {
                System.out.println("Byte " + pos + " - [" + c1 + "] vs [" + c2 + "]");
                c1 = is1.read();
                c2 = is2.read();
                if(c1 == -1 || c2 == -1) break OUTER;
                buf1[index] = (byte)c1;
                buf2[index] = (byte)c2;                
                pos++;
            }
        }
        System.out.println("Compared " + pos + " bytes");
    }

    private static boolean contains(byte[] buf, int c2) {
        for(byte b : buf) if(b == c2) return true;
        return false;
    }

}
