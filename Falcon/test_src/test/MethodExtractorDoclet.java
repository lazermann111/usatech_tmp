/*
 * Created on Sep 23, 2005
 *
 */
package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import simple.text.StringUtils;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doclet;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.RootDoc;

public class MethodExtractorDoclet extends Doclet {

    public MethodExtractorDoclet() {
        super();
        // TODO Auto-generated constructor stub
    }
/*
    public static boolean start(RootDoc rootDoc) {
        try {
            PrintWriter out = new PrintWriter(URLDecoder.decode(MethodExtractorDoclet.class.getResource(".").getPath()) + "/extracted-methods.csv");
            StringUtils.writeCSVLine(new Object[] {
                    "Class",
                    "Method",
                    "Method Description",
                    "Return Type",
                    "Modifiers",
                    "Position",
                    "Parameter Name",
                    "Parameter Type",
                    "Parameter Description"
            }, out);                    
            for(ClassDoc classDoc: rootDoc.classes()) {
                for(MethodDoc methodDoc : classDoc.methods()) {
                    Parameter[] parameters = methodDoc.parameters();
                    ParamTag[] paramTags = methodDoc.paramTags();
                    Map<String,String> paramComments = new HashMap<String,String>();
                    for(ParamTag pt : paramTags) paramComments.put(pt.parameterName(), pt.parameterComment());
                    for(int i = 0; i < parameters.length; i++) {
                        StringUtils.writeCSVLine(new Object[] {
                                classDoc.name(),
                                methodDoc.name(),
                                methodDoc.commentText(),
                                methodDoc.returnType().toString(),
                                methodDoc.modifiers(),
                                i+1,
                                parameters[i].name(),
                                parameters[i].type().toString(),
                                paramComments.get(parameters[i].name())
                        }, out);
                    }
                }
            }
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    */
    public static boolean start(RootDoc rootDoc) {
        try {
            PrintWriter out = new PrintWriter(URLDecoder.decode(MethodExtractorDoclet.class.getResource(".").getPath()) + "/summary-of-methods.csv");
            StringUtils.writeCSVLine(new Object[] {
                    "Class",
                    "Method",
                    "Method Description",
                    "Return Type",
                    "Modifiers",
                    "Parameters...",
            }, out);                    
            for(ClassDoc classDoc: rootDoc.classes()) {
                for(MethodDoc methodDoc : classDoc.methods()) {
                    Parameter[] parameters = methodDoc.parameters();
                    ParamTag[] paramTags = methodDoc.paramTags();
                    Map<String,String> paramComments = new HashMap<String,String>();
                    for(ParamTag pt : paramTags) paramComments.put(pt.parameterName(), pt.parameterComment());
                    Object[] line = new Object[5 + 2 * parameters.length];
                    line[0] = classDoc.name();
                    line[1] = methodDoc.name();
                    line[2] = methodDoc.commentText();
                    line[3] = methodDoc.returnType().toString();
                    line[4] = methodDoc.modifiers();
                    
                    for(int i = 0; i < parameters.length; i++) {
                        line[5 + i * 2] = parameters[i].type().toString();
                        line[6 + i * 2] = parameters[i].name();
                        //paramComments.get(parameters[i].name())
                    }
                    StringUtils.writeCSVLine(line, out);
                }
            }
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
}
