<xsl:stylesheet xmlns="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:css="http://www.saf.org/simple/css"
	version="1.0">
	<xsl:include href="../../src/simple/falcon/templates/report/css-pdf-base.xsl" />
	<xsl:template mode="stylesheet" match="node()">
		<xsl:if test="(@css:class='title')">
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="font-family">Arial , Helvetica , sans-serif</xsl:attribute>
			<xsl:attribute name="color">white</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='table') and (@css:class='report')">
			<xsl:attribute name="border-spacing">0.0pt</xsl:attribute>
			<xsl:attribute name="border-collapse">collapse</xsl:attribute>
			<xsl:attribute name="font-size">small</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='table') and (@css:class='groupingoutput')">
			<xsl:attribute name="border-spacing">0.0pt</xsl:attribute>
			<xsl:attribute name="border-collapse">collapse</xsl:attribute>
			<xsl:attribute name="font-size">small</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(name()='table-cell') and (ancestor::node()[(@css:class='report')]) and (name()='th') and (ancestor::node()[(@css:class='report')])">
			<xsl:attribute name="border">1.0px solid rgb(199 , 199 , 199)</xsl:attribute>
			<xsl:attribute name="font-size">12.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='headerRow')">
			<xsl:attribute name="background-color">rgb(132 , 157 , 170)</xsl:attribute>
			<xsl:attribute name="text-align">center</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(name()='a') and (true or ('defaulted for type' = '10')) and (ancestor::node()[(@css:class='headerRow')]) and (name()='a') and (true or ('defaulted for type' = '10')) and (@css:class='columnHeader') and (ancestor::node()[(@css:class='headerRow')])">
			<xsl:attribute name="color">rgb(192 , 0 , 0)</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(name()='a') and (true or ('defaulted for type' = '10')) and (ancestor::node()[(@css:class='headerRow')]) and (name()='a') and (true or ('defaulted for type' = '10')) and (ancestor::node()[(@css:class='headerRow')])">
			<xsl:attribute name="color">rgb(255 , 204 , 51)</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(name()='a') and (true or ('defaulted for type' = '10')) and (ancestor::node()[(@css:class='headerRow')])">
			<xsl:attribute name="color">rgb(51 , 102 , 153)</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='heading')">
			<xsl:attribute name="font-family">Arial , Helvetica , sans-serif</xsl:attribute>
			<xsl:attribute name="background-color">rgb(51 , 51 , 153)</xsl:attribute>
			<xsl:attribute name="color">white</xsl:attribute>
			<xsl:attribute name="padding">2.0px 2.0px 2.0px 2.0px</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="width">100.0%</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='p') and (@css:class='heading')">
			<xsl:attribute name="margin-bottom">4.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='grouptitle')">
			<xsl:attribute name="font-family">Arial , Helvetica , sans-serif</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="font-style">italic</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='nonAlignedSummary')">
			<xsl:attribute name="font-style">italic</xsl:attribute>
			<xsl:attribute name="font-size">small</xsl:attribute>
		</xsl:if>
		<xsl:if test="(true or ('defaulted for type' = '10'))">
			<xsl:attribute name="color">rgb(51 , 102 , 153)</xsl:attribute>
			<xsl:attribute name="text-decoration">none</xsl:attribute>
			<xsl:attribute name="border-style">none</xsl:attribute>
		</xsl:if>
		<xsl:if test="(true or ('defaulted for type' = '10'))">
			<xsl:attribute name="color">rgb(51 , 102 , 153)</xsl:attribute>
			<xsl:attribute name="text-decoration">none</xsl:attribute>
			<xsl:attribute name="border-style">none</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='body')">
			<xsl:attribute name="font-family">Arial , Helvetica , sans-serif</xsl:attribute>
			<xsl:attribute name="border-top-width">0.0px</xsl:attribute>
			<xsl:attribute name="border-right-width">0.0px</xsl:attribute>
			<xsl:attribute name="border-bottom-width">0.0px</xsl:attribute>
			<xsl:attribute name="scrollbar-arrow-color">rgb(255 , 204 , 51)</xsl:attribute>
			<xsl:attribute name="scrollbar-base-color">rgb(122 , 82 , 41)</xsl:attribute>
			<xsl:attribute name="scrollbar-dark-shadow-color">rgb(255 , 0 , 0)</xsl:attribute>
			<xsl:attribute name="scrollbar-face-color">rgb(132 , 157 , 170)</xsl:attribute>
			<xsl:attribute name="scrollbar-highlight-color">rgb(204 , 204 , 204)</xsl:attribute>
			<xsl:attribute name="scrollbar-shadow-color">rgb(100 , 120 , 131)</xsl:attribute>
			<xsl:attribute name="overflow">auto</xsl:attribute>
			<xsl:attribute name="font-size-0">10.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='hr')">
			<xsl:attribute name="color">rgb(102 , 102 , 102)</xsl:attribute>
			<xsl:attribute name="border-style">solid</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='menuLabel')">
			<xsl:attribute name="padding-top">6.0px</xsl:attribute>
			<xsl:attribute name="padding-left">6.0px</xsl:attribute>
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="font-size">x-small</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='menuItemText')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="font-size">x-small</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='menuItem')">
			<xsl:attribute name="padding-left">18.0px</xsl:attribute>
			<xsl:attribute name="padding-top">0.0px</xsl:attribute>
			<xsl:attribute name="padding-bottom">0.0px</xsl:attribute>
			<xsl:attribute name="border-color">rgb(204 , 204 , 204)</xsl:attribute>
			<xsl:attribute name="border-style">solid</xsl:attribute>
			<xsl:attribute name="border-top-width">0.0px</xsl:attribute>
			<xsl:attribute name="border-right-width">0.0px</xsl:attribute>
			<xsl:attribute name="border-bottom-width">1.0px</xsl:attribute>
			<xsl:attribute name="border-left-width">0.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='unSelectedLink')">
			<xsl:attribute name="font-size">x-small</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='unSelectedLink') and (true or ('defaulted for type' = '10'))">
			<xsl:attribute name="color">rgb(0 , 0 , 0)</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='title1')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="margin-top">0.0px</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="color">rgb(153 , 102 , 51)</xsl:attribute>
			<xsl:attribute name="font-size">140.0%</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='title2')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="color">rgb(44 , 109 , 145)</xsl:attribute>
			<xsl:attribute name="font-size">105.0%</xsl:attribute>
			<xsl:attribute name="margin">2.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='title3')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="color">rgb(44 , 109 , 145)</xsl:attribute>
			<xsl:attribute name="font-size">100.0%</xsl:attribute>
			<xsl:attribute name="margin">2.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='title4')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="color">rgb(44 , 109 , 145)</xsl:attribute>
			<xsl:attribute name="font-size">95.0%</xsl:attribute>
			<xsl:attribute name="margin">2.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='codefrag')">
			<xsl:attribute name="font-family">Courier New , Courier , monospace</xsl:attribute>
			<xsl:attribute name="font-size">smaller</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='table-row') and (@css:class='oddRow')">
			<xsl:attribute name="background-color">rgb(199 , 251 , 164)</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='section')">
			<xsl:attribute name="position">relative</xsl:attribute>
			<xsl:attribute name="overflow">auto</xsl:attribute>
			<xsl:attribute name="border">1.0px solid rgb(208 , 208 , 208)</xsl:attribute>
			<xsl:attribute name="height">100.0%</xsl:attribute>
			<xsl:attribute name="width">100.0%</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='form')">
			<xsl:attribute name="margin">0.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:id='fieldSelects')">
			<xsl:attribute name="width">150.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='input')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="color">rgb(51 , 102 , 153)</xsl:attribute>
			<xsl:attribute name="margin">2.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(name()='select')">
			<xsl:attribute name="font-family">
				Verdana , Arial , Helvetica , sans-serif
			</xsl:attribute>
			<xsl:attribute name="color">rgb(51 , 102 , 153)</xsl:attribute>
			<xsl:attribute name="border">rgb(0 , 0 , 102)</xsl:attribute>
			<xsl:attribute name="border-style">solid</xsl:attribute>
			<xsl:attribute name="border-top-width">1.0px</xsl:attribute>
			<xsl:attribute name="border-right-width">1.0px</xsl:attribute>
			<xsl:attribute name="border-bottom-width">1.0px</xsl:attribute>
			<xsl:attribute name="border-left-width">1.0px</xsl:attribute>
			<xsl:attribute name="margin">2.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='groupHeaderRow0') and (@css:class='groupFooterRow0')">
			<xsl:attribute name="background-color">rgb(224 , 224 , 224)</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="border-top">2.0px solid rgb(0 , 64 , 0)</xsl:attribute>
			<xsl:attribute name="border-bottom">2.0px solid rgb(0 , 64 , 0)</xsl:attribute>
			<xsl:attribute name="border-right">2.0px solid rgb(0 , 64 , 0)</xsl:attribute>
			<xsl:attribute name="border-left">2.0px solid rgb(0 , 64 , 0)</xsl:attribute>
			<xsl:attribute name="font-size">16.0px</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(@css:class='groupHeaderRow1') and (@css:class='groupFooterRow1') and (name()='table-cell') and (ancestor::node()[(@css:class='groupHeaderRow1')])">
			<xsl:attribute name="background-color">transparent</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="border">none</xsl:attribute>
			<xsl:attribute name="color">rgb(0 , 64 , 0)</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(@css:class='groupSpacingRow0') and (name()='table-cell') and (ancestor::node()[(@css:class='groupSpacingRow0')])">
			<xsl:attribute name="border">none</xsl:attribute>
		</xsl:if>
		<xsl:if
			test="(@css:class='groupSpacingRow1') and (name()='table-cell') and (ancestor::node()[(@css:class='groupSpacingRow1')])">
			<xsl:attribute name="border">none</xsl:attribute>
			<xsl:attribute name="height">20.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='groupSpacingRow1')">
			<xsl:attribute name="border-right">none</xsl:attribute>
			<xsl:attribute name="border-left">none</xsl:attribute>
			<xsl:attribute name="height">20.0px</xsl:attribute>
		</xsl:if>
		<xsl:if test="(@css:class='rowcount')">
			<xsl:attribute name="margin-bottom">5.0px</xsl:attribute>
			<xsl:attribute name="font-style">italic</xsl:attribute>
			<xsl:attribute name="font-size">10.0px</xsl:attribute>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>