/*
 * Created on Nov 22, 2004
 *
 */
package test;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Field;
import simple.falcon.engine.Folio;
import simple.falcon.servlet.FalconServlet;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.util.DynaBeanMap;

/**
 * @author bkrug
 *
 */
public class RunChartStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    /**
     * 
     */
    public RunChartStep() {
        super();
    }
    
    /* (non-Javadoc)
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        try {
            Folio folio = (Folio) form.getAttribute("folio");
            //if(folio == null) folio = FolioStepHelper.readFolio(form);
            long[] userGroupIds = (long[])form.getAttribute(SimpleServlet.ATTRIBUTE_USER + ".userGroupIds");
            if(userGroupIds == null || userGroupIds.length == 0) 
                throw new ServletException("You do not have access to run reports");
            Map requestParamValues = new DynaBeanMap(form);
            ExecuteEngine executeEngine = (ExecuteEngine)form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
            String chartType = form.getString("chart.type",true).trim();
            long[] clusterIds = form.getLongArray("chart.clusterIds", true);
            Field[] clusterFields = executeEngine.getDesignEngine().getFields(clusterIds);
            long[] valueIds = form.getLongArray("chart.valueIds", true);
            Field[] valueFields = executeEngine.getDesignEngine().getFields(valueIds);
            requestParamValues.put("chart.title", folio.getTitle(null));
            requestParamValues.put("chart.subtitle", folio.getSubtitle(null));
            requestParamValues.put("chart.axis1", clusterFields[0].getLabel());
            requestParamValues.put("chart.axis2", valueFields[0].getLabel());
            
            //byte[] png = executeEngine.buildChart(chartType, folio, requestParamValues, userGroupIds, clusterFields, valueFields);
	        log.debug("Report built");
	        response.setContentType("image/png");
	        ServletOutputStream out = response.getOutputStream();
	        //out.write(png);
	        out.flush();
        } catch(ServletException se) {
            //log.warn("Couldn't create report", se);
            throw se;
        } catch(Throwable e) {
            //log.warn("Couldn't create report", e);
            throw new ServletException(e);
        }
    }
}
