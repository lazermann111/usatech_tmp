/*
 * Created on Sep 16, 2005
 *
 */
package test;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.fop.apps.Driver;
import org.xml.sax.InputSource;

import simple.io.Log;
import simple.io.logging.avalon.SimpleAvalonLogger;
import simple.net.protocol.AddProtocols;

public class PDFTest {
    private static final Log log = Log.getLog();
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        AddProtocols.run();
        //String fn = "C:\\TEMP\\html-test\\chart-report-fo.xml";
        //String fn = "C:\\Java Projects\\Falcon\\test_src\\FX-56295-Credit Card Summary.xml";
		String fn = "D:\\Development\\Java Projects\\Falcon\\test_src\\test\\Authorization EFT and Substitute W-9 Cert-fo.xml";
		String pdf = "C:\\Users\\bkrug\\Downloads\\Authorization EFT and Substitute W-9 Cert-fo.xml.pdf";
        Driver driver = new Driver();
        driver.setLogger(new SimpleAvalonLogger(log));
        driver.setRenderer(Driver.RENDER_PDF);
		driver.setOutputStream(new FileOutputStream(pdf));
        driver.setInputSource(new InputSource(new FileInputStream(fn)));
        driver.run();
    }

}
