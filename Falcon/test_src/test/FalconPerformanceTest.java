///*
// * Created on Nov 4, 2005
// *
// */
//package test;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.io.PrintStream;
//import java.io.Writer;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Properties;
//
//import javax.xml.transform.Result;
//import javax.xml.transform.stream.StreamResult;
//
//import simple.bean.ConvertException;
//import simple.db.BasicDataSourceFactory;
//import simple.db.DataLayerException;
//import simple.db.DataLayerMgr;
//import simple.db.DataSourceFactory;
//import simple.db.ParameterException;
//import simple.db.config.ConfigException;
//import simple.db.config.ConfigLoader;
//import simple.db.dbcp.DBCPDataSourceFactory;
//import simple.falcon.engine.DesignException;
//import simple.falcon.engine.ExecuteEngine;
//import simple.falcon.engine.ExecuteException;
//import simple.falcon.engine.Folio;
//import simple.falcon.engine.Output;
//import simple.falcon.engine.OutputType;
//import simple.falcon.engine.Report;
//import simple.falcon.engine.ResultsType;
//import simple.falcon.engine.Scene;
//import simple.falcon.engine.standard.StandardExecuteEngine;
//import simple.falcon.servlet.FalconUser;
//import simple.io.LoadingException;
//import simple.io.Log;
//import simple.io.ResourceResolver;
//import simple.io.logging.PerformanceTracker;
//import simple.net.protocol.AddProtocols;
//import simple.results.BeanException;
//import simple.translator.DefaultTranslatorFactory;
//import simple.translator.Translator;
//import simple.xml.TemplatesLoader;
//
//public class FalconPerformanceTest {
//    private static Log log;
//    private static Translator translator = DefaultTranslatorFactory.getTranslatorInstance();
//    protected static class FakeWebResourceResolver implements ResourceResolver {
//        protected String defaultPrefix = "web:/";
//        protected File webDir;
//        /**
//         *
//         */
//        public FakeWebResourceResolver(File webDir) {
//            this.webDir = webDir;
//        }
//        /* (non-Javadoc)
//         * @see simple.falcon.engine.ResourceResolver#getResourceURL(java.lang.String)
//         */
//        public URL getResourceURL(String path) throws MalformedURLException, FileNotFoundException {
//            if(path.startsWith("web:")) {
//                URL url = new File(webDir, path.substring(4)).toURL();
//                if(url == null)
//                    throw new FileNotFoundException("Could not find '" + path.substring(4) + "' in the servlet context (web:)");
//                return url;
//            } else if(simple.text.RegexUtils.matches("^[a-zA-Z]*:.*", path, null)) {
//                return new URL(path);
//            } else if(path.charAt(0) == '/') { //absolute - XXX: what should we do here?
//                URL url = getClass().getClassLoader().getResource(path.substring(1));
//                if(url == null)
//                    throw new FileNotFoundException("Could not find '" + path + "' in the class path");
//                return url;
//            } else if(defaultPrefix != null && defaultPrefix.trim().length() > 0 && !path.startsWith(defaultPrefix)) {
//                return getResourceURL(defaultPrefix + path);
//            } else {
//                return new URL(path);
//            }
//        }
//        /**
//         * @return Returns the defaultPrefix.
//         */
//        public String getDefaultPrefix() {
//            return defaultPrefix;
//        }
//        /**
//         * @param defaultPrefix The defaultPrefix to set.
//         */
//        public void setDefaultPrefix(String defaultPrefix) {
//            this.defaultPrefix = defaultPrefix;
//        }
//
//    }
//
//    /**
//     * @param args
//     * @throws BeanException
//     * @throws ConvertException
//     * @throws DataLayerException
//     * @throws SQLException
//     * @throws LoadingException
//     * @throws IOException
//     * @throws ConfigException
//     * @throws DesignException
//     * @throws ExecuteException
//     * @throws ClassNotFoundException
//     * @throws IllegalAccessException
//     * @throws InstantiationException
//     */
//    public static void main(String[] args) throws SQLException, DataLayerException, ConvertException, BeanException, ConfigException, IOException, LoadingException, ExecuteException, DesignException, InstantiationException, IllegalAccessException, ClassNotFoundException {
//        System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
//        //System.setProperty(".level", "ERROR");
//        System.setProperty("simple.io.logging.SimpleBridge.file", "E:\\Java Projects\\Falcon\\test_src\\FalconPerformanceTest.log");
//        log = Log.getLog();
//        AddProtocols.run();
//        Properties props = new Properties();
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "REPORT", "USATECH");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@KRUGGER.usatech.com:1521:kru01", "FOLIO_CONF", "FOLIO_CONF");
//        addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@usadbd02.usatech.com:1531:usadbd02", "REPORT", "USATECH");
//        addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@usadbd02.usatech.com:1531:usadbd02", "FOLIO_CONF", "FOLIO_CONF");
//        //addDS(props, "REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@10.0.0.45:1521:usardb01", "REPORT", "USATECH");
//        //addDS(props, "metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@10.0.0.45:1521:usardb01", "FOLIO_CONF", "fc45popwq");
//        DataSourceFactory dsf = new DBCPDataSourceFactory(props); //new BasicDataSourceFactory(props);
//        DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
//        ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml");
//        ExecuteEngine ee = StandardExecuteEngine.getInstance();
//        ee.setDataSourceFactory(dsf);
//        ee.setResourceResolver(new FakeWebResourceResolver(new File("C:\\Java Projects\\Falcon\\web")));
//        String directory = "E:\\Java Projects\\Falcon\\test_src";
//        /*
//        int times = 1;
//        long folioId = 25;
//        Map<String,Object> parameters = new HashMap<String, Object>();
//        // parameters.put("resultsType", "ARRAY");
//        parameters.put("StartDate", "03/03/2005");
//        parameters.put("EndDate","03/03/2005");
//        //parameters.put("EndDate","03/07/2005");
//        parameters.put("AllTerminals","1");
//        parameters.put("DeviceTypeId","0,1,2,3,4,5,6,7,8,9,10,11");
//        parameters.put("promptForParams", "false");
//        */
//        int times = 10;
//        long folioId = 25;
//        Map<String,Object> parameters = new HashMap<String, Object>();
//        parameters.put("StartDate", "01/20/2005");
//        parameters.put("EndDate","07/20/2006");
//        parameters.put("AllTerminals","0");
//        parameters.put("DeviceTypeId","0,1,3,11,10,9,6,4,5,8,7");
//        parameters.put("promptForParams", "false");
//        //parameters.put("resultsType", "cache");
//        parameters.put("TerminalId", "11210,11211,11212,11237,11236,11229,11233,11232,11199,11231,10901,10903,10902,12400,12366,10904,9305,8158,11158,12215,12214,12212,12213,12226,12197,12199,12196,12198,9608,10190,10190,13509,9607,9605,13508,9606,9609,9095,12139,12139,11894,11894,13473,12468");
//
//        //outputType = [27]; StartDate = []; EndDate = [07/20/2006]; searchAction = [/selection_main]; resultsType = [cache]; TransTypeId = [16]; y = [8]; x = [9]; AllTerminals = [0]; DeviceTypeId = [0,1,3,11,10,9,6,4,5,8,7]; doChartConfigUpdate = [false]; promptForParams = [false]; folioId = [25]; TerminalId = [11210,11211,11212,11237,11236,11229,11233,11232,11199,11231,10901,10903,10902,12400,12366,10904,9305,8158,11158,12215,12214,12212,12213,12226,12197,12199,12196,12198,9608,10190,10190,13509,9607,9605,13508,9606,9609,9095,12139,12139,11894,11894,13473,12468]]
//        OutputType outputType = ee.getDesignEngine().getOutputType(25);
//        /*
//        StandardOutputType outputType = new StandardOutputType();
//        outputType.setContentType("text/plain");
//        outputType.setLabel("NoOp Report");
//        outputType.setCategory("Report");
//        outputType.setProtocol("noop");
//        outputType.setPath("");
//        ee.registerGenerator("noop", new NoOpGenerator(ee));
//        */
//        testFolioPerformance(ee, directory, folioId, parameters, times, outputType,
//        	"com.jclark.xsl.trax.TransformerFactoryImpl");
//        testFolioPerformance(ee, directory, folioId, parameters, times, outputType,
//    		"org.apache.xalan.processor.TransformerFactoryImpl");
//
//        /*
//        StandardOutputType outputType = new StandardOutputType();
//        outputType.setContentType("text/html");
//        outputType.setLabel("HTML Report");
//        outputType.setCategory("Report");
//        outputType.setProtocol("xsl");
//        outputType.setPath("resource:simple/falcon/templates/report/show-report-html-xt.xsl");
//        */
//        /*
//        StandardOutputType outputType = new StandardOutputType();
//        outputType.setContentType("text/xml");
//        outputType.setLabel("XML-FO Report");
//        outputType.setCategory("Report");
//        outputType.setProtocol("new-fo");
//        outputType.setPath("resource:simple/falcon/templates/report/show-report-pdf-prepare.xsl");
//
//        testFolioPerformance(ee, directory, folioId, parameters, times, ee.getDesignEngine().getOutputType(26),
//                "com.jclark.xsl.trax.TransformerFactoryImpl");
//        /*ee.registerGenerator("old-fo", new OldFOGenerator(ee));
//        outputType.setProtocol("old-fo");
//        outputType.setPath("resource:simple/falcon/templates/report/show-report-pdf-old.xsl");
//        testFolioPerformance(ee, directory, folioId, parameters, times, outputType,
//            "com.jclark.xsl.trax.TransformerFactoryImpl");
//        /*
//        ee.registerGenerator("new-fo", new NewFOGenerator(ee));
//        outputType.setProtocol("new-fo");
//        testFolioPerformance(ee, directory, folioId, parameters, times, outputType,
//            "com.jclark.xsl.trax.TransformerFactoryImpl");
//
//        testFolioPerformance(ee, directory, folioId, parameters, times, ee.getDesignEngine().getOutputType(26),
//            "com.jclark.xsl.trax.TransformerFactoryImpl");
//    */
//        //testFolioPerformance(ee, directory, folioId, parameters, times, ee.getDesignEngine().getOutputType(26),
//         //       "org.apache.xalan.processor.TransformerFactoryImpl");
//
//        Log.finish();
//    }
//
//    private static void testFolioPerformance(ExecuteEngine ee, String directory, long folioId, Map<String,Object> parameters, int times, OutputType outputType, String... transformers) throws DesignException, IOException, ExecuteException, InstantiationException, IllegalAccessException, ClassNotFoundException, ParameterException {
//        Folio folio = ee.getDesignEngine().getFolio(folioId);
//        for(String transformer : transformers) {
//            javax.xml.transform.TransformerFactory factory = (javax.xml.transform.TransformerFactory)Class.forName(transformer).newInstance();
//            TemplatesLoader.getInstance(translator).setFactory(factory);
//            log.debug("Using Transformer '" + transformer + "'");
//            String fileName = directory + "/folio_performance_" + transformer + "-" + outputType.getProtocol() + ".log";
//            log.debug("Logging task times in '" + fileName + "'");
//            PerformanceTracker.getDefaultInstance().getOutput().setOut(new PrintStream(new FileOutputStream(fileName)));
//            for(int i = 0; i < times; i++) {
//            	PerformanceTracker.getDefaultInstance().startTask("run-report", "Iteration #" + (i+1));
//                runFolio(ee, folio, parameters, outputType);
//                PerformanceTracker.getDefaultInstance().endTask("run-report");
//            }
//        }
//    }
//
//    private static void addDS(Properties props, String name, String driver, String url, String username, String password) {
//        props.put(name + "." + BasicDataSourceFactory.PROP_DATABASE, url);
//        props.put(name + "." + BasicDataSourceFactory.PROP_DRIVER, driver);
//        props.put(name + "." + BasicDataSourceFactory.PROP_USERNAME, username);
//        props.put(name + "." + BasicDataSourceFactory.PROP_PASSWORD, password);
//    }
//
//    protected static void runReport(ExecuteEngine ee, Report report, Map<String,Object> parameters, OutputType outputType) throws IOException, ExecuteException, DesignException, ParameterException {
//        FalconUser executor = new FalconUser();
//        executor.setUserId(1);
//        executor.setUserGroupIds(new long[] {1});
//        Scene scene = new Scene();
//        scene.setBaseUrl("http://localhost:8580/usalive/");
//        //scene.setStylesheets(new String[] {"css/default-style.css"});
//        scene.setStylesheets(new String[] {
//                "file:///C:/Java Projects/usalive/web/css/usalive-general-style.css",
//                "file:///C:/Java Projects/usalive/web/css/usalive-report-style.css"
//            });
//
//        //scene.setStylesheets(new String[] {});
//        scene.setTranslator(translator);
//        //scene.setLocale(?);
//        if(outputType == null) outputType = report.getOutputType();
//        final File reportFile = File.createTempFile("FX-", "-" + outputType.generateFileName(report, parameters));
//        Output out = new Output() {
//            public Writer getWriter() throws IOException {
//                return new FileWriter(reportFile);
//            }
//            public OutputStream getOutputStream() throws IOException {
//                return new FileOutputStream(reportFile);
//            }
//			public Result getResult() throws IOException {
//				return new StreamResult(reportFile);
//			}
//        };
//        for(int i = 0; i < 1; i++) {
//            ee.buildReport(report, parameters, executor, outputType, scene, out, ResultsType.FORWARD);
//            log.debug("Finished creating report");
//        }
//    }
//    protected static void runFolio(ExecuteEngine ee, Folio folio, Map<String,Object> parameters, OutputType outputType) throws IOException, ExecuteException, DesignException, ParameterException {
//        Report report = ee.getDesignEngine().newReport();
//        report.setFolios(new Folio[] {folio});
//        report.setOutputType(folio.getOutputType());
//        runReport(ee, report, parameters, outputType);
//    }
//}
