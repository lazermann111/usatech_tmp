/*
 * Created on May 18, 2005
 *
 */
package test;

import java.util.Arrays;
import java.util.Locale;

public class LocaleTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Locale locale = Locale.getDefault();
        System.out.println("Default Locale=" + locale);
        System.out.println("Available Locales=" + Arrays.asList(Locale.getAvailableLocales()));
    }

}
