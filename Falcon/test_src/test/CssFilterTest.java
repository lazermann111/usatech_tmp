///*
// * Created on Mar 21, 2006
// *
// */
//package test;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//
//import javax.xml.parsers.SAXParserFactory;
//
//import org.xml.sax.InputSource;
//import org.xml.sax.XMLReader;
//
//import com.sun.org.apache.xml.internal.serializer.ToXMLStream;
//
//import simple.falcon.engine.css.CssFilter;
//import simple.falcon.engine.css.CssManager;
//
///**
// * @author bkrug
// *
// */
//public class CssFilterTest {
//
//    /**
//     * @param args
//     * @throws Exception
//     */
//    public static void main(String[] args) throws Exception {
//        CssManager mgr = new CssManager(true);
//        CssFilter filter = mgr.getCssSheet(CssFilterTest.class.getResource("css_filter_test.css")).createCssFilter();
//        File input = new File(CssFilterTest.class.getResource("css_filter_test.html").toURI());
//        File output = new File(input.getParentFile(), "css_filter_test-converted.html");
//        ToXMLStream txs = new ToXMLStream();
//        txs.setOutputStream(new FileOutputStream(output));
//        filter.setContentHandler(txs);
//        XMLReader reader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
//        reader.setContentHandler(filter);
//        reader.setDTDHandler(filter);
//        reader.setEntityResolver(filter);
//        reader.setErrorHandler(filter);
//        reader.parse(new InputSource(new FileInputStream(input)));
//        txs.flushPending();
//    }
//
//}
