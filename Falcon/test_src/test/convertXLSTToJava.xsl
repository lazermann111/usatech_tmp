<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:img="http://simple/falcon/image/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:str="http://simple/xml/extensions/java/simple.text.StringUtils"
	exclude-result-prefixes="fr img x2 str">
	<xsl:output method="text" omit-xml-declaration="yes"/>
	<xsl:param name="packageName" select="'test'"/>
    <xsl:param name="className" select="'DirectXHTMLGenerator'"/>
    
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="xsl:stylesheet">
package <xsl:value-of select="$packageName"/>;

import simple.falcon.engine.Generator;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.io.Log;
import simple.results.Results;

import java.util.Map;

public class <xsl:value-of select="$className"/> extends DelegatingXMLHandler implements Generator {
	private static final Log log = Log.getLog();
	protected static class GenerateInfo {
	   public final Report report;
        public final Results[] results;
        public final Map&lt;String, Object&gt; parameters;
        public final ColumnNamer namer;
        public final Scene scene;
        public final OutputType outputType;

        public GenerateInfo(Report report, Results[] results, Map&lt;String, Object&gt; parameters, ColumnNamer namer, Scene scene, OutputType outputType) {
            this.report = report;
            this.results = results;
            this.parameters = parameters;
            this.namer = namer;
            this.scene = scene;
            this.outputType = outputType;
        }
	}  		
	protected ExecuteEngine engine;

    public <xsl:value-of select="$className"/>(ExecuteEngine engine) {
        super();
        this.engine = engine;
    }
    protected AppendableAttributes attr() {
        return new AppendableAttributes(getNameSpaceURI(), getPrefix());
    }
    @Override
    public void generate(Report report, OutputType outputType, Map&lt;String, Object&gt; parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
        GenerateInfo info = new GenerateInfo(report, results, parameters, engine.getDesignEngine().getColumnNamer(), scene, outputType);
        try {
            setupOutput(info, out);
        } catch(IOException e) {
            throw new ExecuteException(e);
        }
        try {
            generateDocument(info);
        } catch(SAXException e) {
            throw new ExecuteException(e);
        }
    }

    protected void setupOutput(GenerateInfo info, Output out) throws IOException {

    }
   	<xsl:apply-templates/>
}	
	</xsl:template>
	
	<xsl:template match="xsl:template[@match='/']">
        public void generateDocument( GenerateInfo info) throws ExecuteException {
            <xsl:apply-templates/>
        }
    </xsl:template>
    
	<xsl:template match="xsl:template[@name]">
		protected void <xsl:value-of select="str:toJavaName(concat('generate-',@name))"/>(GenerateInfo info<xsl:apply-templates mode="params"/>) throws SAXException {
		      <xsl:for-each select="xsl:param[@select]"><!-- TODO: defaults -->
		      if(<xsl:value-of select="str:toJavaName(string(@name))"/> == null)
		          <xsl:value-of select="str:toJavaName(string(@name))"/> = XPATH(<xsl:value-of select="xsl:param[@select]"/>);
		      </xsl:for-each>
		<xsl:apply-templates/>
		}
	</xsl:template>
	
	<xsl:template match="xsl:param" mode="params">
		, String <xsl:value-of select="str:toJavaName(string(@name))"/>
	</xsl:template>
	
	<xsl:template match="xsl:call-template">
	    <xsl:apply-templates mode="setup"/>
		<xsl:value-of select="str:toJavaName(concat('generate-',@name))"/>(info<xsl:apply-templates mode="params"/>);
	</xsl:template>
	
	<xsl:template match="xsl:call-template" mode="in-param">
        <xsl:apply-templates mode="setup"/>
        <xsl:value-of select="str:toJavaName(concat('generate-',@name))"/>(info<xsl:apply-templates mode="params"/>);
    </xsl:template>
    
	<xsl:template match="xsl:with-param" mode="params">
		, <xsl:value-of select="str:toJavaName(string(@name))"/>
	</xsl:template>
	
	<xsl:template match="xsl:with-param" mode="setup">
	    String <xsl:value-of select="str:toJavaName(string(@name))"/> = <xsl:choose>
            <xsl:when test="@select">XPATH(<xsl:value-of select="@select"/>)</xsl:when>
            <xsl:otherwise><xsl:apply-templates mode="in-param"/></xsl:otherwise>
        </xsl:choose>;
    </xsl:template>
    
	<xsl:template match="*" mode="params" priority="-0.5"/>
	
	<xsl:template match="*" mode="in-param" priority="-0.5"/>
    
	<xsl:template match="xsl:variable">
		String <xsl:value-of select="str:toJavaName(string(@name))"/> = <xsl:choose>
            <xsl:when test="@select">XPATH(<xsl:value-of select="@select"/>)</xsl:when>
            <xsl:otherwise><xsl:apply-templates mode="in-param"/></xsl:otherwise>
        </xsl:choose>;
	</xsl:template>
		
	<xsl:template match="xsl:param">
        String <xsl:value-of select="str:toJavaName(string(@name))"/> = <xsl:choose>
            <xsl:when test="@select">XPATH(<xsl:value-of select="@select"/>)</xsl:when>
            <xsl:otherwise><xsl:apply-templates mode="in-param"/></xsl:otherwise>
        </xsl:choose>;
    </xsl:template>
    
	<xsl:template match="xsl:for-each">
		for(Object o : XPATH(<xsl:value-of select="@select"/>)) {
		<xsl:apply-templates/>
		}
	</xsl:template>
	
	<xsl:template match="xsl:for-each" mode="in-param">
        for(Object o : XPATH(<xsl:value-of select="@select"/>)) {
        <xsl:apply-templates mode="in-param"/>
        }
    </xsl:template>
        
	<xsl:template match="xsl:if">
		if(XPATH(<xsl:value-of select="@test"/>)) {
		<xsl:apply-templates/>
		}
	</xsl:template>
	
	<xsl:template match="xsl:choose">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="xsl:when[1]">
		if(XPATH(<xsl:value-of select="@test"/>)) {
		<xsl:apply-templates/>
		}
	</xsl:template>
	<xsl:template match="xsl:when[position() &gt; 1]">
		else if(XPATH(<xsl:value-of select="@test"/>)) {
		<xsl:apply-templates/>
		}
	</xsl:template>
	<xsl:template match="xsl:otherwise">
		else {
		<xsl:apply-templates/>
		}
	</xsl:template>
	
	<xsl:template match="xsl:if" mode="in-param">
        (XPATH(<xsl:value-of select="@test"/>) ? <xsl:apply-templates mode="in-param"/> : "")
    </xsl:template>
    
	<xsl:template match="xsl:choose" mode="in-param">
        <xsl:apply-templates mode="in-param"/>
    </xsl:template>
    <xsl:template match="xsl:when[1]" mode="in-param">
        (XPATH(<xsl:value-of select="@test"/>) ? <xsl:apply-templates mode="in-param"/> <xsl:if test="not(../xsl:otherwise)">: ""</xsl:if>)
    </xsl:template>
    <xsl:template match="xsl:when[position() &gt; 1]" mode="in-param">
        : XPATH(<xsl:value-of select="@test"/>) ? <xsl:apply-templates mode="in-param"/>
    </xsl:template>
    <xsl:template match="xsl:otherwise" mode="in-param">
        : <xsl:apply-templates mode="in-param"/>
    </xsl:template>
    
	<xsl:template match="xsl:value-of">
		characters(XPATH(<xsl:value-of select="@select"/>));
	</xsl:template>
	
	<xsl:template match="xsl:value-of" mode="in-param">
        <xsl:if test="position() &gt; 1"> + </xsl:if>XPATH(<xsl:value-of select="@select"/>)
    </xsl:template>
    
	<xsl:template match="x2:translate">
		characters(translate(info.scene, "<xsl:value-of select="@key"/>", "<xsl:value-of select="str:replace(str:replace(string(.), '\', '\\'), '&#34;', '\&#34;')"/>"));
	</xsl:template>
	
	<xsl:template match="x2:translate" mode="in-param">
        <xsl:if test="position() &gt; 1"> + </xsl:if>translate(info.scene, "<xsl:value-of select="@key"/>", "<xsl:value-of select="str:replace(str:replace(string(.), '\', '\\'), '&#34;', '\&#34;')"/>")
    </xsl:template>
    
	<xsl:template match="*[namespace-uri() = 'http://www.w3.org/1999/XSL/Transform']" priority="-0.5">
		/* &lt;xsl:<xsl:value-of select="local-name()"/><xsl:for-each select="@*"><xsl:text> </xsl:text><xsl:value-of select="local-name()"/>="<xsl:value-of select="str:replace(str:replace(string(.), '\', '\\'), '&#34;', '\&#34;')"/>"</xsl:for-each>&gt;<xsl:apply-templates/> */
	</xsl:template>
	<xsl:template match="*[namespace-uri() = 'http://www.w3.org/1999/XSL/Transform']" mode="in-param"  priority="-0.5">
        /* &lt;xsl:<xsl:value-of select="local-name()"/><xsl:for-each select="@*"><xsl:text> </xsl:text><xsl:value-of select="local-name()"/>="<xsl:value-of select="str:replace(str:replace(string(.), '\', '\\'), '&#34;', '\&#34;')"/>"</xsl:for-each>&gt;<xsl:apply-templates/> */
    </xsl:template>
    
	<xsl:template match="text()"  priority="-0.5">
		<xsl:if test="normalize-space(.)">
			characters("<xsl:value-of select="str:replace(str:replace(string(normalize-space(.)), '\', '\\'), '&#34;', '\&#34;')"/>");
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="text()" mode="in-param" priority="-0.5">
	    <xsl:if test="position() &gt; 1"> + </xsl:if>"<xsl:value-of select="str:replace(str:replace(string(normalize-space(.)), '\', '\\'), '&#34;', '\&#34;')"/>"
    </xsl:template>
    
	<xsl:template match="*[namespace-uri() != 'http://www.w3.org/1999/XSL/Transform']"  priority="-0.5">
		startElement("<xsl:value-of select="local-name()"/>" 
		<xsl:if test="count(@*) &gt; 0">, attr()
				<xsl:for-each select="@*">.add("<xsl:value-of select="local-name()"/>", "<xsl:value-of select="str:replace(str:replace(string(.), '\', '\\'), '&#34;', '\&#34;')"/>")</xsl:for-each>
		</xsl:if>);
		<xsl:apply-templates/>
		endElement("<xsl:value-of select="local-name()"/>");
	</xsl:template>
</xsl:stylesheet>
