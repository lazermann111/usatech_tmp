/*
 * Created on Sep 16, 2005
 *
 */
package test;

import java.io.InputStream;
import java.io.StringBufferInputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import simple.net.protocol.AddProtocols;

public class CSSParserTest {

    /**
     * @param args
     */
    public static void main(String[] args) throws Throwable {
        AddProtocols.run();
        InputStream xsl = CSSParserTest.class.getClassLoader().getResourceAsStream("test/test-m-a.xsl");
        InputStream xml = new StringBufferInputStream("<a><b color=\"green\">" +
                "<test color=\"blue\" class=\"nothing\">stuff</test></b></a>"
                );//CSSParserTest.class.getClassLoader().getResourceAsStream("test/report.xml");
        testXSL("org.apache.xalan.processor.TransformerFactoryImpl", xsl, xml);       
        /*
        Properties tagMap = new Properties();
        tagMap.load(CssToXslParser.class.getResourceAsStream("tagMap.properties"));
        Parser cssParser = new Parser();
        CssToXslDocumentHandler handler = new CssToXslDocumentHandler(System.out, tagMap);
        
        String css = ".one, a.two, #A, td b .three, p.four table.five { font-size: 9pt; }";
        //new FileInputStream("C:\\Java Projects\\Falcon\\web\\css\\default-style.css"));
        cssParser.setDocumentHandler(handler);
        cssParser.parseStyleSheet(new InputSource(new InputStreamReader(new StringBufferInputStream(css))));
        
        int alpha = 255;
        int red = 180;
        int green = 80;
        int blue = 64;
        int color = (alpha << 24) | (red << 16) | (green << 8) | (blue << 0);
        System.out.println(Integer.toHexString(color & 0xFFFFFF));
        //System.out.println(StringUtils.pad(Integer.toHexString(color), "0", 6, StringUtils.JUSTIFY_LEFT));
        //*/

    }


public static void testXSL(String transformerFactoryClass, InputStream xsl, InputStream xml) throws Throwable {
    try {
        System.setProperty("javax.xml.transform.TransformerFactory", transformerFactoryClass);
        TransformerFactory tf = TransformerFactory.newInstance();
        System.out.println("*** Running a test using TransformerFactory " + tf.getClass().getName());
        Transformer t = tf.newTransformer(new StreamSource(xsl));
        t.transform(new StreamSource(xml), new StreamResult(System.out));
        System.out.println();
        System.out.println("*** Done running this test");
        System.out.println();
    } catch(Exception e) {
        System.out.println("ERROR: ");
        e.printStackTrace();
    }
}


}
