/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import simple.db.Parameter;
import simple.db.specific.DbSpecific;
import simple.results.Results.Aggregate;
import simple.sql.Expression;
import simple.sql.SQLType;
import simple.util.CaseInsensitiveComparator;
import simple.util.StaticCollection;
/**
 * @author bkrug
 *
 */
public abstract class Operator {
    protected static final Map<Integer,Operator> operatorMap = new HashMap<Integer,Operator>();
    public static enum Side {NONE, DISPLAY, SORT, BOTH};
    public static final Collection<String> PARAMETER_NAMES = StaticCollection.create(
            new CaseInsensitiveComparator<String>(),
            "display", "sort", "0"
        );

    protected int operatorId;
    protected String name;
    protected String description;

    /**
     *
     */
    public Operator(int operatorId) {
        setOperatorId(operatorId);
    }

    public Operator() {
    }
    public abstract Side getSide() ;

    public abstract String getXml() ;

    public abstract Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) ;

	public abstract int getParameterCount();

	public abstract int getParameterCount(Field field);

	public abstract Parameter[] getParameters(Field field);

    public abstract SQLType[] getApplicableSqlTypes() ;

	public abstract Integer[] getParameterSqlTypes();

	public abstract void setParameterSqlTypes(Integer[] parameterSqlTypes);

    public static Operator getOperator(int operatorId) {
        return operatorMap.get(new Integer(operatorId));
    }

    public static void registerOperator(Operator operator) {
    	if(operator != null && operator.getOperatorId() != 0)
    		operatorMap.put(operator.getOperatorId(), operator);
    }
    public static void deregisterOperator(Operator operator) {
    	if(operator != null && operator.getOperatorId() != 0)
    		operatorMap.remove(operator.getOperatorId());
    }
    /**
     * @return Returns the operatorId.
     */
    public int getOperatorId() {
        return operatorId;
    }
    /**
     * @param operatorId The operatorId to set.
     */
    public void setOperatorId(int operatorId) {
    	this.operatorId = operatorId;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
    	return operatorId;
    }
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
    	if(this == obj) return true;
    	if(!(obj instanceof Operator)) return false;
    	Operator op = (Operator)obj;
    	return operatorId == op.operatorId;
    }
}
