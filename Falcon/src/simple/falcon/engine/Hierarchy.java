/*
 * Created on Jan 3, 2005
 *
 */
package simple.falcon.engine;

import java.util.List;

/**
 * @author bkrug
 *
 */
public interface Hierarchy<E> {
    public String getName() ;
    public String getDescription() ;
    public Object getId() ;
    public List<Hierarchy<E>> getBranches() ;
    public List<E> getLeafs() ;
}
