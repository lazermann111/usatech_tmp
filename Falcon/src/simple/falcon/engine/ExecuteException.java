/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine;

public class ExecuteException extends Exception {
    private static final long serialVersionUID = -8844817501L;

    public ExecuteException() {
        super();
    }

    public ExecuteException(String message) {
        super(message);
    }

    public ExecuteException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExecuteException(Throwable cause) {
        super(cause);
    }

}
