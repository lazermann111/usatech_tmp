/*
 * Created on Feb 15, 2005
 *
 */
package simple.falcon.engine;

import java.util.Map;

/**
 * @author bkrug
 *
 */
public interface OutputType {
    public String getContentType() ;
    public String getLabel() ;
    public String getCategory() ;
    public int getOutputTypeId() ;
    public String getPath() ;
    public void setPath(String path);
    public String getProtocol() ;
    public String generateFileName(Folio folio, Map<String,?> requestParamValues) ;
    public String generateFileName(Report report, Map<String,?> requestParamValues) ;
    public String generateFullTitle(Report report, Map<String,?> requestParamValues) ;
    public String generateFullTitle(Folio folio, Map<String,?> requestParamValues) ;
}
