package simple.falcon.engine;

import java.util.Collections;

import simple.sql.BaseExpression;

public class NoColumnExpression extends BaseExpression {
	protected final String sqlFrag;
	public NoColumnExpression(String sqlFrag) {
		super(Collections.singletonList((Object) sqlFrag));
		this.sqlFrag = sqlFrag;
	}

	@Override
	public NoColumnExpression copy() {
		return new NoColumnExpression(sqlFrag);
	}	
}
