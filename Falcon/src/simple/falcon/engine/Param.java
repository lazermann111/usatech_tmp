package simple.falcon.engine;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.param.ParamEditor;
import simple.sql.SQLType;
import simple.translator.Translator;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public interface Param {

	public Param copy(Translator translator) ;
	public abstract SQLType getSqlType();
	public abstract void setSqlType(SQLType sqlType);

	public boolean isFromField();
	
	public boolean isRequired();
	/**
	 * @return Returns the name.
	 */
	public abstract String getName();
	public abstract void setName(String name);

	/**
	 * @return Returns the prompt.
	 */
	public abstract String getPrompt();
	public abstract void setPrompt(String prompt);

	/**
	 * @return Returns the value.
	 */
	public abstract Object getValue();

	/**
	 * @param value The value to set.
	 */
	public abstract void setValue(Object value);

	/**
	 * @return Returns the label.
	 */
	public abstract String getLabel();
	public abstract void setLabel(String label);

	public abstract ParamEditor getEditor();
	public abstract void setEditor(ParamEditor editor);

	public abstract Class<? extends Object> getJavaType();
	public void writeXML(String tagName, XMLBuilder builder)throws SAXException,ConvertException;
    public void readXML(ObjectBuilder builder) throws DesignException;
    
    public abstract void setRequired(boolean isRequired);

}