/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine;

import java.text.Format;
import java.util.ArrayList;
import java.util.List;

import simple.bean.ConvertUtils;
import simple.db.Parameter;
import simple.db.specific.DbSpecific;
import simple.falcon.engine.standard.StandardLookupParamEditor;
import simple.lang.SystemUtils;
import simple.results.Results.Aggregate;
import simple.sql.MutableExpression;
import simple.sql.SQLType;
import simple.text.NumberAsWordsFormat;
import simple.text.StringUtils;
import simple.translator.Translator;

/**
 * @author bkrug
 *
 */
public abstract class FilterItem implements Filter {
	public static final Param[] EMPTY_PARAMS = new Param[0];
    protected final static Format ordinalFormat = new NumberAsWordsFormat(true);
    protected Field field;
    protected Operator operator;
	protected transient Param[] params;
	protected Param[] operatorParams;
    protected Aggregate aggregateType;
    /**
     *
     */
    public FilterItem() {
        super();
    }

    @Override
	public FilterItem clone() {
    	FilterItem copy;
		try {
			copy = (FilterItem)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError("Cloneable class threw CloneNotSupportedException");
		}
    	return copy;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
		return SystemUtils.addHashCodes(0, field, operator, getParams(), aggregateType);
    }
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
    	if(this == obj) return true;
    	if(!(obj instanceof FilterItem)) return false;
    	FilterItem fi = (FilterItem)obj;
    	return ConvertUtils.areEqual(field, fi.field)
    		&& ConvertUtils.areEqual(operator, fi.operator)
    		&& ConvertUtils.areEqual(getParams(), fi.getParams())
    		&& aggregateType == fi.aggregateType;
    }
    public FilterItem copy(Translator translator) {
    	FilterItem copy = clone();
		if(operatorParams != null) {
			copy.operatorParams = new Param[operatorParams.length];
			for(int i = 0; i < operatorParams.length; i++) {
				String label;
				String prompt;
				if(translator != null) {
					label = translator.translate(operatorParams[i].getLabel());
					prompt = translator.translate(operatorParams[i].getPrompt());
				} else {
					label = operatorParams[i].getLabel();
					prompt = operatorParams[i].getPrompt();
				}
				copy.operatorParams[i] = createParam(operatorParams[i].getName(), label, prompt, operatorParams[i].getSqlType(), operatorParams[i].isFromField());
				copy.operatorParams[i].setEditor(operatorParams[i].getEditor());
				copy.operatorParams[i].setValue(operatorParams[i].getValue());
			}
    	}
		copy.params = null;
    	return copy;
    }
    public Field getField() {
        return field;
    }
    public void setField(Field field) {
		Field old = this.field;
		this.field = field;
		if(ConvertUtils.areEqual(old, field))
			return;
		params = null;
    }
    public Operator getOperator() {
        return operator;
    }
    public void setOperator(Operator operator) {
		Operator old = this.operator;
        this.operator = operator;
		if(ConvertUtils.areEqual(old, operator))
			return;
		if(operator != null && old != null && operator.getParameterSqlTypes() == null && old.getParameterSqlTypes() != null)
			operator.setParameterSqlTypes(old.getParameterSqlTypes());
		operatorParams = null;
		params = null;
    }
    public void setOperatorById(int operatorId) throws IllegalArgumentException {
        Operator op = Operator.getOperator(operatorId);
        if(op == null) throw new IllegalArgumentException("Operator '" + operatorId + "' is not a valid operator");
		setOperator(op);
    }
    public Object[] getValues() {
        Param[] params = getParams();
        Object[] values = new Object[params.length];
        for(int i = 0; i < params.length; i++) {
            values[i] = params[i].getValue();
        }
        return values;
    }

    public void setValues(Object[] values) {
        if(values == null) return;
        Param[] params = getParams();
        for(int i = 0; i < values.length && i < params.length; i++) {
            params[i].setValue(values[i]);
        }
    }

    public SQLType[] getSqlTypes() {
		Param[] params = getParams();
		SQLType[] sqlTypes = new SQLType[params.length];
		for(int i = 0; i < params.length; i++) {
			sqlTypes[i] = params[i].getSqlType();
		}
		return sqlTypes;
    }

	public Param[] getOperatorParams() {
		if(operatorParams == null) {
			if(operator == null)
				operatorParams = EMPTY_PARAMS;
			else {
				Parameter[] parameters = operator.getParameters(field);
				List<Param> paramList = new ArrayList<Param>();
				int k = 0;
				for(int i = 0; i < parameters.length; i++) {
					String name;
					String label;
					String prompt;
					boolean fromField;
					if(StringUtils.isBlank(parameters[i].getPropertyName())) {
						k++;
						name = StringUtils.toJavaName(getField().getLabel()) + (k == 1 ? "" : String.valueOf(k));
						String rank = (k == 1 ? "" : ordinalFormat.format(k) + " ");
						label = StringUtils.capitalizeFirst(rank) + getField().getLabel();
						prompt = "Enter the " + rank + "value for " + getField().getLabel();
						fromField = false;
						paramList.add(createParam(name, label, prompt, parameters[i].getSqlType(), fromField));
					}
				}
				operatorParams = paramList.toArray(new Param[paramList.size()]);
			}
		}
		return operatorParams;
	}

    /**
     * @return Returns the params.
     */
    public Param[] gatherParams() {
        return getParams();
    }

    /**
     * @return Returns the params.
     */
	protected Param[] getParams() {
		if(params == null) {
			if(operator == null){
				if(operatorParams==null){
					params = EMPTY_PARAMS;
				}else{
					return operatorParams;
				}
			}
			else {
				Parameter[] parameters = operator.getParameters(field);
				Param[] tmp = new Param[parameters.length];
				int k = 0;
				for(int i = 0; i < parameters.length; i++) {
					String name;
					String label;
					String prompt;
					boolean fromField;
					if(!StringUtils.isBlank(parameters[i].getPropertyName())) {
						name = parameters[i].getPropertyName();
						label = StringUtils.capitalizeFirst(StringUtils.toWords(name));
						prompt = "Enter the value for " + label;
						fromField = true;
					} else {
						if(operatorParams != null && operatorParams.length > k) {
							tmp[i] = operatorParams[k];
							k++;
							continue;
						}
						k++;
						name = StringUtils.toJavaName(getField().getLabel()) + (k == 1 ? "" : String.valueOf(k));
						String rank = (k == 1 ? "" : ordinalFormat.format(k) + " ");
						label = StringUtils.capitalizeFirst(rank) + getField().getLabel();
						prompt = "Enter the " + rank + "value for " + getField().getLabel();
						fromField = false;
					}
					tmp[i] = createParam(name, label, prompt, parameters[i].getSqlType(), fromField);
				}
				params = tmp;
			}
		}
		return params;
    }

	protected abstract Param createParam(String name, String label, String prompt, SQLType sqlType, boolean fromField);

	protected void updateOperatorParams(Param[] allParams, Param[] operatorParams) {
		int k = 0;
		for(int i = 0; i < allParams.length; i++) {
			if(allParams[i] == null || !allParams[i].isFromField()) {
				if(k >= operatorParams.length)
					break;
				allParams[i] = operatorParams[k++];
				if(allParams[i] instanceof StandardLookupParamEditor) {
					StandardLookupParamEditor slpe = (StandardLookupParamEditor) allParams[i];
					slpe.setFilter(this);
				}
			}
		}
	}
    /**
     * @param params The params to set.
     */
	public void setOperatorParams(Param[] operatorParams) {
		if(operatorParams == null) {
			this.params = null;
			this.operatorParams = null;
		}else if(operator==null) {
			this.operatorParams=operatorParams;
		}else{
			// make sure operator params is correct length
			Param[] old = getOperatorParams();
			for(int i = 0; i < old.length && i < operatorParams.length; i++)
				if(operatorParams[i] != null && old[i] != operatorParams[i]) {
					old[i] = operatorParams[i];
					if(operatorParams[i] instanceof StandardLookupParamEditor) {
						StandardLookupParamEditor slpe = (StandardLookupParamEditor) operatorParams[i];
						slpe.setFilter(this);
					}
					params = null;
				}
		}
    }

    public void buildExpression(MutableExpression appendTo, DbSpecific dbSpecific) {
    	appendTo.append(getOperator().getExpression(getField(), getAggregateType(), dbSpecific));
    }
/*
    public boolean equals(Object o) {
        if(!(o instanceof FilterItem)) return false;
        FilterItem f = (FilterItem)o;
        return field.equals(f.field) && operator.equals(f.operator);
    }

    public int hashCode() {
        return field.hashCode() + operator.hashCode();
    }
*/
    public boolean sameExpression(Filter other) {
        if(!(other instanceof FilterItem)) return false;
        FilterItem f = (FilterItem)other;
        return field.equals(f.field) && operator.equals(f.operator);
    }

	public Aggregate getAggregateType() {
		return aggregateType;
	}

	public void setAggregateType(Aggregate aggregateType) {
		this.aggregateType = aggregateType;
	}
}
