package simple.falcon.engine;

import java.sql.ResultSet;

public enum ResultsType {
	CACHE(ResultSet.TYPE_FORWARD_ONLY, true, true),
	FORWARD(ResultSet.TYPE_FORWARD_ONLY, false, true),
	SCROLL(ResultSet.TYPE_SCROLL_INSENSITIVE, false, true),
	ARRAY(ResultSet.TYPE_FORWARD_ONLY, false, false);

	protected final int resultSetType;
	protected final boolean useCache;
	protected final boolean lazyAccess;
	private ResultsType(int resultSetType, boolean useCache, boolean lazyAccess) {
		this.resultSetType = resultSetType;
		this.useCache = useCache;
		this.lazyAccess = lazyAccess;
	}
	public int getResultSetType() {
		return resultSetType;
	}
	public boolean isUseCache() {
		return useCache;
	}
	public boolean isLazyAccess() {
		return lazyAccess;
	}
}
