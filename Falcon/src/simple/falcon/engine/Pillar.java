package simple.falcon.engine;

import java.text.Format;

import org.xml.sax.SAXException;

import simple.results.DataGenre;
import simple.results.Results;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

/**
 * @author bkrug
 *
 */
public interface Pillar {

	public static enum PillarType {
	    LINK, // text with possible link
	    IMAGE, // an image   
	    NOTE, // a note on a column  
	    CLUSTER, // an axis on a chart    
	    VALUE, // the value on the chart
	    ROW, // row-level style, link etc
	    CHECKBOX,
	    BUTTON,
	    RADIO
	}
	public Format getPercentFormat();
	
	public Format getActionFormat();

	public Format getDisplayFormat();

	public Format getHelpFormat();

	public Format getSortFormat();

	public Format getStyleFormat();

	public PillarType getPillarType();

	public Format getLabelFormat();

	public DataGenre getSortType();

	public String getDescription();

	public FieldOrder[] getFieldOrders();

	public int getGroupingLevel();

	public int getIndex() ;
	
	public void setIndex(int index) ;
	
	public String getWidth() ;
	
	public void setWidth(String width) ;
	
	public void addFieldOrder(Field field, SortOrder sortOrder, Results.Aggregate aggregateType,
			int sortIndex);

	public void removeFieldOrder(int index);

	public void replaceFieldOrders(Field[] fields, SortOrder[] sortOrders,
			Results.Aggregate[] aggregateTypes, int[] sortIndexes);

	/**
	 * @param percentFormat The percentFormat to set.
	 */
	public void setPercentFormat(Format percentFormat);
	/**
	 * @param actionFormat The actionFormat to set.
	 */
	public void setActionFormat(Format actionFormat);

	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description);

	/**
	 * @param displayFormat The displayFormat to set.
	 */
	public void setDisplayFormat(Format displayFormat);

	/**
	 * @param groupingLevel The groupingLevel to set.
	 */
	public void setGroupingLevel(int groupingLevel);

	/**
	 * @param helpFormat The helpFormat to set.
	 */
	public void setHelpFormat(Format helpFormat);

	/**
	 * @param labelFormat The labelFormat to set.
	 */
	public void setLabelFormat(Format labelFormat);

	/**
	 * @param pillarType The pillarType to set.
	 */
	public void setPillarType(PillarType pillarType);

	/**
	 * @param sortFormat The sortFormat to set.
	 */
	public void setSortFormat(Format sortFormat);

	/**
	 * @param sortType The sortType to set.
	 */
	public void setSortType(DataGenre sortType);

	/**
	 * @param styleFormat The styleFormat to set.
	 */
	public void setStyleFormat(Format styleFormat);

	/**
	 * @return Returns the aggregated.
	 */
	public boolean isAggregated();

	public ResultsValueRetriever getDisplayValueRetriever() ;

	public ResultsValueRetriever getSortValueRetriever() ;
	
	/** Notifies the pillar that its field orders or other attributes may have changed
	 * 
	 */
	public void invalidate() ;
	public void writeXML(String tagName, XMLBuilder builder)throws SAXException;
    public void readXML(ObjectBuilder builder)throws DesignException;

}