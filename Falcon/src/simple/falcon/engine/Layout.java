package simple.falcon.engine;

public enum Layout {
	FULL, UNFRAMED, FRAGMENT, OFFLINE;
	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
