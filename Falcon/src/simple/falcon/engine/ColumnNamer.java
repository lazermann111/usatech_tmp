/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine;

public interface ColumnNamer {
    //public String getSortColumnName(Field field) ;
    //public String getDisplayColumnName(Field field) ;
	public String getDisplayColumnName(FieldOrder fieldOrder);
	public String getSortColumnName(FieldOrder fieldOrder);
}
