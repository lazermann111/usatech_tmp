/*
 * Created on Mar 22, 2006
 *
 */
package simple.falcon.engine.css;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;

/** Keeps a list of attributes by priority to make it easy to know whether to overwrite or not
 * @author bkrug
 *
 */
public class PrioritizedAttributes implements Attributes {
    protected static class Attr {
        protected String uri;
        protected String localName;
        protected String qName;
        protected String type;
        protected String value;
        protected int priority;
    }
    protected Map<String, Integer> attrMap = new HashMap<String,Integer>();
    protected List<Attr> attrList = new ArrayList<Attr>();
    
    /**
     * 
     */
    public PrioritizedAttributes() {
        super();
    }
    
    /**
     * 
     */
    public PrioritizedAttributes(Attributes atts) {
        this();
        setAttributes(atts);
    }
    
    public void setAttributes(Attributes atts) {
        attrList.clear();
        attrMap.clear();
        if(atts instanceof PrioritizedAttributes) {
            PrioritizedAttributes pa = (PrioritizedAttributes)atts;
            attrList.addAll(pa.attrList);
            attrMap.putAll(pa.attrMap);
        } else {
            for(int i = 0; i < atts.getLength(); i++) {
                String uri = atts.getURI(i);
                String localName = atts.getLocalName(i);
                String qName = atts.getQName(i);
                String key = makeKey(uri, localName, qName);
                Attr attr =  new Attr();
                attr.uri = uri;
                attr.localName = localName;
                attr.qName = qName;
                attr.priority = 0;
                attr.value = atts.getValue(i);
                attr.type = atts.getType(i);
                attrList.add(attr);
                attrMap.put(key, i);
            }
        }
    }
    
    public void addAttributes(Attributes atts) {
        if(atts instanceof PrioritizedAttributes) {
            PrioritizedAttributes pa = (PrioritizedAttributes)atts;
			for(Attr update : pa.attrList) {
				addAttribute(update.uri, update.localName, update.qName, update.type, update.value, update.priority);
            }
        } else {
            for(int i = 0; i < atts.getLength(); i++) {
                addAttribute(atts.getURI(i), atts.getLocalName(i), atts.getQName(i), atts.getType(i), atts.getValue(i), 0);
            }
        }
    }
    
    public boolean addAttribute(String uri, String localName, String qName, String type, String value, int priority) {
        String key = makeKey(uri, localName, qName);
        Attr attr = getAttr(key);
        if(attr != null) {
			if(attr.priority > priority)
				return false;
            attr.priority = priority;
            attr.value = value;
            attr.type = type;
        } else {
            attr = new Attr();
            attr.uri = uri;
            attr.localName = localName;
            attr.qName = qName;
            attrList.add(attr);
            attrMap.put(key, attrList.size() - 1);
        }
		attr.priority = priority;
		attr.value = value;
		attr.type = type;
		return true;
    }
    
    protected String makeKey(String uri, String localName, String qName) {
        if(uri == null || uri.trim().length() == 0) return qName;
		return localName + "@" + uri;
    }
    protected Attr getAttr(String key) {
        Integer index = attrMap.get(key);
        return index == null ? null : getAttr(index);
    }
    
    protected Attr getAttr(int index) {
        try {
            return attrList.get(index);
        } catch(IndexOutOfBoundsException e) {
            return null;
        }
    }
    /**
     * @see org.xml.sax.Attributes#getLength()
     */
    public int getLength() {
        return attrList.size();
    }

    /**
     * @see org.xml.sax.Attributes#getURI(int)
     */
    public String getURI(int index) {
        Attr attr = getAttr(index);
        return (attr == null ? null : attr.uri);
    }

    /**
     * @see org.xml.sax.Attributes#getLocalName(int)
     */
    public String getLocalName(int index) {
        Attr attr = getAttr(index);
        return (attr == null ? null : attr.localName);
    }

    /**
     * @see org.xml.sax.Attributes#getQName(int)
     */
    public String getQName(int index) {
        Attr attr = getAttr(index);
        return (attr == null ? null : attr.qName);
    }

    /**
     * @see org.xml.sax.Attributes#getType(int)
     */
    public String getType(int index) {
        Attr attr = getAttr(index);
        return (attr == null ? null : attr.type);
    }

    /**
     * @see org.xml.sax.Attributes#getValue(int)
     */
    public String getValue(int index) {
        Attr attr = getAttr(index);
        return (attr == null ? null : attr.value);
    }

    /**
     * @see org.xml.sax.Attributes#getIndex(java.lang.String, java.lang.String)
     */
    public int getIndex(String uri, String localName) {
        Integer index = attrMap.get(makeKey(uri, localName, localName));
        return index == null ? -1 : index;
    }

    /**
     * @see org.xml.sax.Attributes#getIndex(java.lang.String)
     */
    public int getIndex(String qName) {
        Integer index = attrMap.get(makeKey(null, null, qName));
        return index == null ? -1 : index;
    }

    /**
     * @see org.xml.sax.Attributes#getType(java.lang.String, java.lang.String)
     */
    public String getType(String uri, String localName) {
        return getType(getIndex(uri, localName));
    }

    /**
     * @see org.xml.sax.Attributes#getType(java.lang.String)
     */
    public String getType(String qName) {
        return getType(getIndex(qName));
    }

    /**
     * @see org.xml.sax.Attributes#getValue(java.lang.String, java.lang.String)
     */
    public String getValue(String uri, String localName) {
        return getValue(getIndex(uri, localName));
    }

    /**
     * @see org.xml.sax.Attributes#getValue(java.lang.String)
     */
    public String getValue(String qName) {
        return getValue(getIndex(qName));
    }

}
