/*
 * Created on Mar 22, 2006
 *
 */
package simple.falcon.engine.css;

import java.io.InputStream;
import java.io.Reader;

import org.w3c.css.sac.InputSource;

/**
 * @author bkrug
 *
 */
public class DelegateInputSource extends InputSource {
    protected org.xml.sax.InputSource delegate;
    /**
     * 
     */
    public DelegateInputSource(org.xml.sax.InputSource delegate) {
        super();
        this.delegate = delegate;
    }
    @Override
    public String getURI() {
        return delegate.getSystemId();
    }
    @Override
    public InputStream getByteStream() {
        return delegate.getByteStream();
    }
    @Override
    public Reader getCharacterStream() {
        return delegate.getCharacterStream();
    }
}
