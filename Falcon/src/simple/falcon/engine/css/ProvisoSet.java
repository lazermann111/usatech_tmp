/*
 * Created on Mar 15, 2006
 *
 */
package simple.falcon.engine.css;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;

import simple.util.EnhancedBitSet;

public class ProvisoSet {
    protected int lastIndex;
    protected Map<String,Map<String,Integer>> attrProvisos = new HashMap<String, Map<String,Integer>>();
    protected Map<String,Map<String,Integer>> attrSpaceProvisos = new HashMap<String, Map<String,Integer>>();
    protected Map<String,Map<String,Integer>> attrHyphenProvisos = new HashMap<String, Map<String,Integer>>();
    protected Map<String,Integer> idProvisos = new HashMap<String, Integer>();
    protected Map<String,Integer> tagProvisos = new HashMap<String, Integer>();
    protected Map<String,Integer> pseudoProvisos = new HashMap<String, Integer>();
    
    public ProvisoSet() {
        
    }
    protected void markIfFound(String value, Map<String,Integer> provisos, EnhancedBitSet qualifications) {
        Integer index = provisos.get(value);
        if(index != null) qualifications.set(index);             
    }
    public EnhancedBitSet findQualifications(String elementName, Attributes atts) {
        EnhancedBitSet qualifications = new EnhancedBitSet(getProvisoCount());
        markProvisos(elementName, atts, qualifications);
        return qualifications;
    }
    public void markProvisos(String elementName, Attributes atts, EnhancedBitSet qualifications) {
        markIfFound(elementName, tagProvisos, qualifications);
        String id = null;
        for(int i = 0; i < atts.getLength(); i++) {
            String name = atts.getQName(i);
            String value = atts.getValue(i);
            if(name.equals("id")) {
                id = value;
            } else if(id == null && name.equals("name")) {
                id = value;
            }
            Map<String,Integer> attrValues = attrProvisos.get(name);
            if(attrValues != null) {
                markIfFound(value, attrValues, qualifications);
                if(value != null) {
                    markIfFound(null, attrValues, qualifications);
                }
            }
            if(value != null) {
                attrValues = attrSpaceProvisos.get(name);
                if(attrValues != null) {
                    for(String s : value.split("\\s")) {
                        markIfFound(s, attrValues, qualifications);
                    }
                }
                attrValues = attrHyphenProvisos.get(name);
                if(attrValues != null) {
                    for(String s : value.split("-")) {
                        markIfFound(s, attrValues, qualifications);
                    }
                }
            }
        }
        if(id != null) {
            markIfFound(id, idProvisos, qualifications);              
        }
    }
    public int addAttrProviso(String name, String value) {
        Map<String,Integer> attrValues = attrProvisos.get(name);
        if(attrValues == null) {
            attrValues = new HashMap<String, Integer>();
            attrProvisos.put(name, attrValues);
        }
        Integer prev = attrValues.get(value);
        if(prev == null) {
            int index = lastIndex++;
            attrValues.put(value, index);
            return index;
        } else {
            return prev;
        }
    }
    public int addAttrSpaceProviso(String name, String value) {
        Map<String,Integer> attrValues = attrSpaceProvisos.get(name);
        if(attrValues == null) {
            attrValues = new HashMap<String, Integer>();
            attrSpaceProvisos.put(name, attrValues);
        }
        Integer prev = attrValues.get(value);
        if(prev == null) {
            int index = lastIndex++;
            attrValues.put(value, index);
            return index;
        } else {
            return prev;
        }
    }
    public int addAttrHyphenProviso(String name, String value) {
        Map<String,Integer> attrValues = attrHyphenProvisos.get(name);
        if(attrValues == null) {
            attrValues = new HashMap<String, Integer>();
            attrHyphenProvisos.put(name, attrValues);
        }
        Integer prev = attrValues.get(value);
        if(prev == null) {
            int index = lastIndex++;
            attrValues.put(value, index);
            return index;
        } else {
            return prev;
        }
    }
    public int addIDProviso(String id) {
        Integer prev = idProvisos.get(id);
        if(prev == null) {
            int index = lastIndex++;
            idProvisos.put(id, index);
            return index;
        } else {
            return prev;
        }
     }
    public int addTagProviso(String tag) {
        Integer prev = tagProvisos.get(tag);
        if(prev == null) {
            int index = lastIndex++;
            tagProvisos.put(tag, index);
            return index;
        } else {
            return prev;
        }
    }    
    public int addPseudoProviso(String tag) {
        Integer prev = pseudoProvisos.get(tag);
        if(prev == null) {
            int index = lastIndex++;
            pseudoProvisos.put(tag, index);
            return index;
        } else {
            return prev;
        }
    }    
    public int getProvisoCount() {
        return lastIndex;
    }
    public ProvisoSet clone() {
        ProvisoSet ps = new ProvisoSet();
        ps.lastIndex = lastIndex;
        ps.attrHyphenProvisos.putAll(attrHyphenProvisos);
        ps.attrProvisos.putAll(attrProvisos);
        ps.attrSpaceProvisos.putAll(attrSpaceProvisos);
        ps.idProvisos.putAll(idProvisos);
        ps.pseudoProvisos.putAll(pseudoProvisos);
        ps.tagProvisos.putAll(tagProvisos);
        return ps;
    }
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Attributes: ").append(attrProvisos);
        sb.append("; Tags: ").append(tagProvisos);
        sb.append("; IDs: ").append(idProvisos);
        if(!attrHyphenProvisos.isEmpty())
            sb.append("; Hyphen-Attributes: ").append(attrHyphenProvisos);
        if(!attrSpaceProvisos.isEmpty())
            sb.append("; Space-Attributes: ").append(attrSpaceProvisos);
        
        return sb.toString();
    }
}