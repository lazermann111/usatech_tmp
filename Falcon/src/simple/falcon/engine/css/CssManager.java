package simple.falcon.engine.css;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import simple.io.BundledConfigSource;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.io.PropertiesLoader;

public class CssManager implements Loader {
    private static final Log log = Log.getLog();
    protected static final String FO_URI = "http://www.w3.org/1999/XSL/Format";
    protected static final String XHTML_URI = "http://www.w3.org/1999/xhtml";

	protected Map<List<URL>, ConfigSource> configs = new HashMap<List<URL>,ConfigSource>();
	protected Map<ConfigSource,CssSheet> sheets = new HashMap<ConfigSource,CssSheet>();
	protected ConfigSource tagMapCfgSrc;
	protected Properties tagMap;
	protected File tempDirectory = null;
    protected String namespaceURI;

	public CssManager(boolean isHtml){
		tagMap = new Properties();
		if(!isHtml) {
            namespaceURI = FO_URI;
    		try {
    			URL url = CssManager.class.getResource("tagMap.properties");
    			if (url == null){
    				IOException ioe = new IOException("Unable to load config file 'tagMap.properties'");
    				throw ioe;
    			}
				tagMapCfgSrc = ConfigSource.createConfigSource(url);
				tagMapCfgSrc.registerLoader(new PropertiesLoader(tagMap), -1, -1);
    		} catch (IOException ioe) {
    			log.error("Unable to load properties resource '" + CssManager.class.getPackage() + "tagMap.properties'.",ioe);
    		}
        } else {
            namespaceURI = XHTML_URI;
        }
	}

	public void load(ConfigSource src) throws LoadingException {
        if(tagMapCfgSrc != null) tagMapCfgSrc.reload();
		CssParser cssParser = new CssParser(src.getUri());
		CssSheet handler = new CssSheet(tagMap, cssParser);
        handler.setNamespaceURI(namespaceURI);
        try {
            cssParser.setDocumentHandler(handler);
			cssParser.parseStyleSheet(new DelegateInputSource(src.getInputSource()));
		} catch (IOException ioe) {
			throw new LoadingException(ioe);
		}
        sheets.put(src, handler);
	}

	public CssSheet getCssSheet(URL... cssUrls) {
        List<URL> list = Arrays.asList(cssUrls);
        ConfigSource cssCfgSrc = configs.get(list);
        if(cssCfgSrc == null) {
			if(log.isInfoEnabled())
				log.info("Creating CssSheet from " + list);
            ConfigSource[] cfgs = new ConfigSource[cssUrls.length];
            for(int i = 0; i < cssUrls.length; i++) {
                try {
					cfgs[i] = ConfigSource.createConfigSource(cssUrls[i]);
				} catch(IOException e) {
					log.warn("Could not get config source", e);
				}
            }
            cssCfgSrc = new BundledConfigSource(Arrays.asList(cfgs));
            cssCfgSrc.registerLoader(this,-1,-1);
            configs.put(list, cssCfgSrc);
        }

		try {
			cssCfgSrc.reload();
		} catch (LoadingException le) {
			log.error("Unable to reload Config Source '"+cssCfgSrc+"'",le);
		}
        return sheets.get(cssCfgSrc);
	}

}
