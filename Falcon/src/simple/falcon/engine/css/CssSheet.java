/*
 * Created on Mar 14, 2006
 *
 */
package simple.falcon.engine.css;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.batik.css.engine.sac.ExtendedSelector;
import org.w3c.css.sac.AttributeCondition;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CombinatorCondition;
import org.w3c.css.sac.Condition;
import org.w3c.css.sac.ConditionalSelector;
import org.w3c.css.sac.DescendantSelector;
import org.w3c.css.sac.DocumentHandler;
import org.w3c.css.sac.ElementSelector;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.Locator;
import org.w3c.css.sac.Parser;
import org.w3c.css.sac.SACMediaList;
import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;
import org.w3c.css.sac.SiblingSelector;

import simple.io.Log;
import simple.text.StringUtils;
import simple.util.EnhancedBitSet;

/**
 * @author bkrug
 *
 */
public class CssSheet implements DocumentHandler {
    private static final Log log = Log.getLog();
    protected static float DEFAULT_PIXEL_TO_MM = 0.28f;
    protected static float BORDER_PIXEL_TO_MM = 0.14f;
        
    protected PrioritizedAttributes currentAtts;
	protected final ProvisoSet provisoSet;
	protected final Set<Rule> rules;
    protected String namespaceURI;
	protected final List<InputSource> inputSources = new ArrayList<InputSource>();
    protected int specificity = 0;
	protected final Locator locator;
	protected final Properties tagMap;

	public CssSheet(Properties tagMap, Locator locator) {
		this(new ProvisoSet(), new LinkedHashSet<Rule>(), tagMap, locator);
    }

	public CssSheet(ProvisoSet provisoSet, Set<Rule> rules, Properties tagMap, Locator locator) {
        super();
        this.provisoSet = provisoSet;
        this.rules = rules;
        this.tagMap = tagMap;
		this.locator = locator;
    }

    
    public void startDocument(InputSource source) throws CSSException {
        inputSources.add(source); 
    }

    public void endDocument(InputSource source) throws CSSException {
        int index = inputSources.lastIndexOf(source);
        if(index >= 0) inputSources.subList(index, inputSources.size()).clear();
    }

    public void comment(String arg0) throws CSSException {
    }

    public void ignorableAtRule(String arg0) throws CSSException {
    }

    public void namespaceDeclaration(String arg0, String arg1) throws CSSException {
    }

    public void importStyle(String uri, SACMediaList media, String defaultNamespaceURI) throws CSSException {
        //read this sytle sheet in first
        log.debug("Importing stylesheet '" + uri + "'");
        String baseUri = null;
        if(!inputSources.isEmpty()) {
            baseUri = inputSources.get(inputSources.size()).getURI();
        } else {
            log.warn("Start Document was not called before importStyle");
        }
        Parser cssParser = new org.apache.batik.css.parser.Parser();
        try {
            URL url = (baseUri == null || baseUri.trim().length() == 0 ? new URL(uri) : new URL(new URL(baseUri), uri));
            cssParser.setDocumentHandler(this);
            InputSource newSrc = new InputSource();
            newSrc.setByteStream(url.openStream());
            newSrc.setURI(url.toString());
            cssParser.parseStyleSheet(newSrc);
        } catch (IOException ioe) {
            throw new CSSException(ioe);
        }
    }

    public void startMedia(SACMediaList arg0) throws CSSException {
    }

    public void endMedia(SACMediaList arg0) throws CSSException {
    }

    public void startPage(String arg0, String arg1) throws CSSException {
    }

    public void endPage(String arg0, String arg1) throws CSSException {
    }

    public void startFontFace() throws CSSException {
    }

    public void endFontFace() throws CSSException {
    }

    public void startSelector(SelectorList selectors) throws CSSException {
        currentAtts = new PrioritizedAttributes();
        for(int i = 0; i < selectors.getLength(); i++) {
            Selector s = selectors.item(i);
            if(s instanceof ExtendedSelector)
                specificity = ((ExtendedSelector)s).getSpecificity();
            else
                specificity = 0; //For now just set to zero (since selector should always be an ExtendedSelector
            EnhancedBitSet qualifications = new EnhancedBitSet(provisoSet.getProvisoCount());
			Rule rule = new Rule(qualifications, currentAtts, (locator != null ? "'" + locator.getURI() + "', Line " + locator.getLineNumber() : null));
            rule = parseSelector(s, rule);
            rules.add(rule);
			if(log.isDebugEnabled())
				log.debug((locator != null ? "Stylesheet '" + locator.getURI() + "', Line " + locator.getLineNumber() + ": " : "") + "Adding rule " + rule);
        }
    }
    
    protected Rule parseSelector(Selector s, Rule currentRule) {
        switch(s.getSelectorType()) {
            case Selector.SAC_ANY_NODE_SELECTOR:
                //do nothing
                break;
             case Selector.SAC_CONDITIONAL_SELECTOR:
                currentRule = parseSelector(((ConditionalSelector)s).getSimpleSelector(), currentRule);
                parseCondition(((ConditionalSelector)s).getCondition(), currentRule);
                break;
             case Selector.SAC_ELEMENT_NODE_SELECTOR:
                currentRule = parseElementSelector(((ElementSelector)s).getLocalName(), currentRule);
                break;
             case Selector.SAC_DESCENDANT_SELECTOR:
                DescendantSelector desc = (DescendantSelector)s;
                currentRule = parseSelector(desc.getSimpleSelector(), currentRule);
				currentRule = parseSelector(desc.getAncestorSelector(), new Rule(new EnhancedBitSet(), currentRule, Rule.Relation.DESCENDENT, (locator != null ? "'" + locator.getURI() + "', Line " + locator.getLineNumber() : null)));
                break;
             case Selector.SAC_CHILD_SELECTOR:
                 DescendantSelector child = (DescendantSelector)s;
                 currentRule = parseSelector(child.getSimpleSelector(), currentRule);
				currentRule = parseSelector(child.getAncestorSelector(), new Rule(new EnhancedBitSet(), currentRule, Rule.Relation.CHILD, (locator != null ? "'" + locator.getURI() + "', Line " + locator.getLineNumber() : null)));
                 break;
             case Selector.SAC_DIRECT_ADJACENT_SELECTOR: 
                 SiblingSelector sib = (SiblingSelector)s;
                 currentRule = parseSelector(sib.getSiblingSelector(), currentRule);
				currentRule = parseSelector(sib.getSelector(), new Rule(new EnhancedBitSet(), currentRule, Rule.Relation.ADJACENT, (locator != null ? "'" + locator.getURI() + "', Line " + locator.getLineNumber() : null)));
                 break;
            default:
				log.warn((locator != null ? "Stylesheet '" + locator.getURI() + "', Line " + locator.getLineNumber() + ": " : "") + "CSS parser used default Selector for type '" + s.getSelectorType() + "'");
            
        }
        return currentRule;
    }
    
    protected void parseCondition(Condition c, Rule rule){       
        switch(c.getConditionType()){
            case Condition.SAC_ATTRIBUTE_CONDITION:
            case Condition.SAC_CLASS_CONDITION:
                rule.getQualifications().set(
                    provisoSet.addAttrProviso(
                            ((AttributeCondition)c).getLocalName(), 
                            ((AttributeCondition)c).getValue()));
                break;
            case Condition.SAC_ID_CONDITION:
                rule.getQualifications().set(
                        provisoSet.addIDProviso(((AttributeCondition)c).getValue()));
                break;
            case Condition.SAC_AND_CONDITION:
                parseCondition(((CombinatorCondition)c).getFirstCondition(), rule);
                parseCondition(((CombinatorCondition)c).getSecondCondition(), rule);
                break;
            case Condition.SAC_PSEUDO_CLASS_CONDITION:
                //Do this so that rule has quals
                rule.getQualifications().set(
                        provisoSet.addPseudoProviso(((AttributeCondition)c).getValue()));
                break;
            default:
				log.warn((locator != null ? "Stylesheet '" + locator.getURI() + "', Line " + locator.getLineNumber() + ": " : "") + "CSS parser used default Condition for type '" + c.getConditionType() + "'");
            
        }
    }

    public void endSelector(SelectorList selectors) throws CSSException {
		log.debug((locator != null ? "Stylesheet '" + locator.getURI() + "', Line " + locator.getLineNumber() + ": " : "") + "Attached attributes to rule(s): " + Rule.getAttributesString(currentAtts));
        currentAtts = null;
    }

    public static final int IMPORTANT_PRIORITY = 1 << 24;
    public void property(String name, LexicalUnit value, boolean important) throws CSSException {
    	addAttribute(currentAtts, getNamespaceURI(), name, value, important, specificity);
    }

	public static void addAttribute(PrioritizedAttributes atts, String uri, String name, LexicalUnit value, boolean important, int specificity) {
        StringBuffer appendTo = new StringBuffer();
        parseLexicalUnit(value, (name.equals("border") || name.startsWith("border-") ? BORDER_PIXEL_TO_MM : DEFAULT_PIXEL_TO_MM), appendTo);
        atts.addAttribute(uri, name, name, "CDATA", appendTo.toString(), specificity + (important ? IMPORTANT_PRIORITY : 0));    	
    }
    protected static final String[] lexicalUnitOperators = new String[12];
    static {
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_COMMA] = ",";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_EXP] = "^";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_GE] = ">=";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_GT] = ">";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_LE] = "<=";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_LT] = "<";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_MINUS] = "-";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_MOD] = "%";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_MULTIPLY] = "*";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_PLUS] = "+";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_SLASH] = "/";
        lexicalUnitOperators[LexicalUnit.SAC_OPERATOR_TILDE] = "~";
    }

	protected static void parseLexicalUnit(LexicalUnit value, float pixelToMM, StringBuffer appendTo) {
		boolean space = false;
		while(value != null) {
			switch(value.getLexicalUnitType()) {
				case LexicalUnit.SAC_INTEGER:
					if(space)
						appendTo.append(' ');
					appendTo.append(value.getIntegerValue());
					break;
				case LexicalUnit.SAC_URI:
				case LexicalUnit.SAC_ATTR:
				case LexicalUnit.SAC_IDENT:
				case LexicalUnit.SAC_STRING_VALUE:
					if(space)
						appendTo.append(' ');
					appendTo.append(value.getStringValue());
					break;
				case LexicalUnit.SAC_COUNTER_FUNCTION:
					if(space)
						appendTo.append(' ');
					appendTo.append("counter(");
					parseLexicalUnit(value.getParameters(), pixelToMM, appendTo);
					appendTo.append(')');
					break;
				case LexicalUnit.SAC_COUNTERS_FUNCTION:
					if(space)
						appendTo.append(' ');
					appendTo.append("counters(");
					parseLexicalUnit(value.getParameters(), pixelToMM, appendTo);
					appendTo.append(')');
					break;
				case LexicalUnit.SAC_RECT_FUNCTION:
					if(space)
						appendTo.append(' ');
					appendTo.append("rect(");
					parseLexicalUnit(value.getParameters(), pixelToMM, appendTo);
					appendTo.append(')');
					break;
				case LexicalUnit.SAC_RGBCOLOR:
					if(space)
						appendTo.append(' ');
					appendTo.append("rgb(");
					parseLexicalUnit(value.getParameters(), pixelToMM, appendTo);
					appendTo.append(')');
					break;
				case LexicalUnit.SAC_FUNCTION:
					if(space)
						appendTo.append(' ');
					appendTo.append(value.getFunctionName()).append('(');
					parseLexicalUnit(value.getParameters(), pixelToMM, appendTo);
					appendTo.append(')');
					break;
				case LexicalUnit.SAC_PIXEL:
					if(space)
						appendTo.append(' ');
					appendTo.append(value.getFloatValue() * pixelToMM).append("mm");
					break;
				case LexicalUnit.SAC_REAL:
				case LexicalUnit.SAC_DIMENSION:
					if(space)
						appendTo.append(' ');
					// fall-through
				case LexicalUnit.SAC_EM:
				case LexicalUnit.SAC_EX:
				case LexicalUnit.SAC_INCH:
				case LexicalUnit.SAC_CENTIMETER:
				case LexicalUnit.SAC_MILLIMETER:
				case LexicalUnit.SAC_POINT:
				case LexicalUnit.SAC_PICA:
				case LexicalUnit.SAC_PERCENTAGE:
				case LexicalUnit.SAC_DEGREE:
				case LexicalUnit.SAC_GRADIAN:
				case LexicalUnit.SAC_RADIAN:
				case LexicalUnit.SAC_MILLISECOND:
				case LexicalUnit.SAC_SECOND:
				case LexicalUnit.SAC_HERTZ:
				case LexicalUnit.SAC_KILOHERTZ:
					appendTo.append(value.getFloatValue()).append(value.getDimensionUnitText());
					break;
				case LexicalUnit.SAC_SUB_EXPRESSION:
					parseLexicalUnit(value.getSubValues(), pixelToMM, appendTo);
					break;
				case LexicalUnit.SAC_INHERIT:
					if(space)
						appendTo.append(' ');
					appendTo.append("inherit");
					break;
				default:
					if(value.getLexicalUnitType() < lexicalUnitOperators.length) {
						appendTo.append(lexicalUnitOperators[value.getLexicalUnitType()]);
					} else {
						log.warn("CSS parser used default Lexical unit for type '" + value.getLexicalUnitType() + "'; value '" + value + "'");
					}
			}
			space = true;
			value = value.getNextLexicalUnit();
		}
    }
    
    protected Rule parseElementSelector(String htmlTag, Rule currentRule){
        if(htmlTag != null){
            String mapping = tagMap.getProperty(htmlTag);
            if (mapping == null){
                currentRule.getQualifications().set(provisoSet.addTagProviso(htmlTag));
            } else {
                String[] tags = StringUtils.split(mapping,'/');
                currentRule.getQualifications().set(provisoSet.addTagProviso(tags[tags.length-1]));               
                for(int i = (tags.length - 2); i >= 0; i--) {
					currentRule = new Rule(new EnhancedBitSet(), currentRule, Rule.Relation.DESCENDENT, (locator != null ? "'" + locator.getURI() + "', Line " + locator.getLineNumber() : null));
                    currentRule.getQualifications().set(provisoSet.addTagProviso(tags[i]));
                }
            }
        }
        return currentRule;
    }

    public CssFilter createCssFilter() {
        if(log.isDebugEnabled())
            log.debug("Creating CssFilter with ProvisoSet: " + provisoSet + "; and Rules: " + rules);
        return new CssFilter(provisoSet, rules, getNamespaceURI());
    }
    
    public String getNamespaceURI() {
        return namespaceURI;
    }

    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }

}
