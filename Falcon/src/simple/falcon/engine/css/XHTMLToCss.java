/*
 * Created on Mar 23, 2006
 *
 */
package simple.falcon.engine.css;

import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

/**
 * @author bkrug
 *
 */
public class XHTMLToCss extends XMLFilterImpl {

    /**
     * 
     */
    public XHTMLToCss() {
        super();
    }

    /**
     * @param parent
     */
    public XHTMLToCss(XMLReader parent) {
        super(parent);
    }

}
