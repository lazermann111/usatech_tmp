package simple.falcon.engine.css;

import org.apache.batik.css.parser.Parser;
import org.w3c.css.sac.Locator;

public class CssParser extends Parser implements Locator {
	protected final String cssUri;

	public CssParser(String cssUri) {
		super();
		this.cssUri = cssUri;
	}

	public String getURI() {
		return cssUri;
	}

	public int getLineNumber() {
		return scanner.getLine();
	}

	public int getColumnNumber() {
		return scanner.getColumn();
	}
}
