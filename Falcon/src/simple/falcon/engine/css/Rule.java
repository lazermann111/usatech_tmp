/*
 * Created on Mar 15, 2006
 *
 */
package simple.falcon.engine.css;

import org.xml.sax.Attributes;

import simple.util.EnhancedBitSet;

public class Rule {
    public static enum Relation { NONE, DESCENDENT, CHILD, ADJACENT };

	protected final EnhancedBitSet qualifications;
	protected final Rule subRule;
	protected final Attributes atts;
	protected final Relation relation;
	protected final String location;
    
	public Rule(EnhancedBitSet qualifications, Rule subRule, Relation relation, String location) {
        if(subRule == null) throw new NullPointerException("Sub Rule cannot be null");
        if(relation == Relation.NONE) throw new IllegalArgumentException("Relation cannot be NONE");
        this.qualifications = qualifications;
        this.subRule = subRule;
        this.relation = relation;
		this.location = location;
		this.atts = null;
    }
    
	public Rule(EnhancedBitSet qualifications, Attributes atts, String location) {
        if(atts == null) throw new NullPointerException("Attributes cannot be null");
        this.qualifications = qualifications;
        this.atts = atts;
		this.location = location;
		this.relation = Relation.NONE;
		this.subRule = null;
    }
    
    public Attributes getAttributes() {
        return atts;
    }
    
    public Rule getSubRule() {
        return subRule;
    }
    
    public Relation getRelation() {
        return relation;
    }

    public EnhancedBitSet getQualifications() {
        return qualifications;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer(qualifications.toString());
        switch(getRelation()) {
            case DESCENDENT:
                sb.append(" --> ").append(getSubRule().toString());
                break;
            case CHILD:
                sb.append(" > ").append(getSubRule().toString());
                break;
            case ADJACENT:
                sb.append(" + ").append(getSubRule().toString());
                break;
            case NONE:
                sb.append("=(").append(getAttributesString(atts)).append(")");
				if(location != null)
					sb.append('@').append(location);
                break;
        }
        return sb.toString();
    }
    
    public static String getAttributesString(Attributes atts) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < atts.getLength(); i++) {
            if(i != 0) sb.append("; ");
            sb.append(atts.getQName(i));
            sb.append("=");
            sb.append(atts.getValue(i));
        }
        return sb.toString();
    }

}