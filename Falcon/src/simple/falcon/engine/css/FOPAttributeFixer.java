/*
 * Created on Mar 16, 2006
 *
 */
package simple.falcon.engine.css;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLFilterImpl;

import simple.io.Log;
import simple.text.RegexUtils;
import simple.util.EnhancedBitSet;
import simple.util.StaticCollection;
import simple.util.StaticMap;

/**
 * @author bkrug
 *
 */
public class FOPAttributeFixer extends XMLFilterImpl {
    private static final Log log = Log.getLog();
    protected static final String FO_URI = "http://www.w3.org/1999/XSL/Format";
    protected EnhancedBitSet blockDepths = new EnhancedBitSet();
    protected int depth = 0;
    protected static final int TOP = 0;
    protected static final int RIGHT = 1;
    protected static final int BOTTOM = 2;
    protected static final int LEFT = 3;
    protected static final int COMPOSITE = 4;
    protected static final StaticMap<String, Integer> spaceAttNames = StaticMap.create(
            new String[] {"margin", "margin-right", "margin-left", "margin-top", "margin-bottom",
                    "start-indent", "end-indent", 
                    "space-start", "space-end", "space-before", "space-after"},
            new Integer[] {COMPOSITE, RIGHT, LEFT, TOP, BOTTOM,
                    LEFT, RIGHT,
                    LEFT, RIGHT, TOP, BOTTOM});
    protected static final StaticMap<String, Integer> paddingAttNames = StaticMap.create(
            new String[] {"padding", "padding-right", "padding-left", "padding-top", "padding-bottom"},
            new Integer[] {COMPOSITE, RIGHT, LEFT, TOP, BOTTOM});
    protected static final String fontPattern = "^(?:([^\\s/]+)\\s+)?(?:([^\\s/]+)\\s+)?(?:([^\\s/]+)\\s+)?([^\\s/]+)(?:\\s*/\\s*([^\\s/]+))?\\s+([^\\s/].*)$";
    protected static final String[] spacePartNames = new String[] {
        "space-before.optimum", "end-indent", "space-after.optimum", "start-indent"};
    protected static final String[] fontPartNames = new String[] {
        "font-style", "font-variant", "font-weight", "font-size", "line-height", "font-family"};
    protected static final StaticCollection<String> ignoreElementNames = StaticCollection.create(
            "root", "layout-master-set", "simple-page-master", "region-before", "region-body", 
            "region-after", "page-sequence-master", "repeatable-page-master-reference", 
            "page-sequence", "title", "static-content", "flow");
    /**
     * 
     */
    public FOPAttributeFixer() {
        super();
    }

    public FOPAttributeFixer(XMLReader parent) {
        super(parent);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        atts = fixAttributes(localName, atts);
        super.startElement(uri, localName, qName, atts);
    }

    /** Fixes the space and composite attributes that FOP does not understand 
     * @param elementName
     * @param atts
     * @return
     */
    public Attributes fixAttributes(String elementName, Attributes atts) {
        // Don't do this for some elements
        if(ignoreElementNames.contains(elementName)) {
            depth++;
            return atts;
        }
        blockDepths.set(depth, elementName.equals("block"));
        String[] spaceValues = new String[4];
        String[] fontValues = null;
        boolean needsFix = false;
        List<Integer> remove = new ArrayList<Integer>();
        for(int i = 0; i < atts.getLength(); i++) {
            String value = atts.getValue(i);
            if(value != null && value.trim().length() > 0) {
                String name = atts.getLocalName(i);
                Integer type = spaceAttNames.get(name);
                if(type == null && (depth == 0 || blockDepths.get(depth-1))) {
                    type = paddingAttNames.get(name);
                }
                if(type != null) {
                    needsFix = true;
                    remove.add(i);
                    switch(type) {
                        case COMPOSITE:
                            String[] parts = value.split("\\s+");
                            switch(parts.length) {
                                case 4:
                                    addValue(spaceValues, parts[0], TOP);
                                    addValue(spaceValues, parts[1], RIGHT);
                                    addValue(spaceValues, parts[2], BOTTOM);
                                    addValue(spaceValues, parts[3], LEFT);
                                    break;
                                case 3:
                                    addValue(spaceValues, parts[0], TOP);
                                    addValue(spaceValues, parts[1], RIGHT);
                                    addValue(spaceValues, parts[2], BOTTOM);
                                    addValue(spaceValues, parts[1], LEFT);
                                    break;
                                case 2:
                                    addValue(spaceValues, parts[0], TOP);
                                    addValue(spaceValues, parts[1], RIGHT);
                                    addValue(spaceValues, parts[0], BOTTOM);
                                    addValue(spaceValues, parts[1], LEFT);
                                    break;
                                case 1:
                                    addValue(spaceValues, parts[0], TOP);
                                    addValue(spaceValues, parts[0], RIGHT);
                                    addValue(spaceValues, parts[0], BOTTOM);
                                    addValue(spaceValues, parts[0], LEFT);
                                    break;
                            }
                            break;
                        default:
                            addValue(spaceValues, value, type);
                    }
                } else if(name.equals("font")) {
                    fontValues = RegexUtils.match(fontPattern, value, null);
                    if(fontValues == null) log.warn("Font value does not match pattern: '" + value + "'; ignoring font attribute");
                    else {
                        needsFix = true;
                        remove.add(i);
                    }
                }
            }
        }
   
        depth++;
        if(needsFix) {
            AttributesImpl ai;
            if(atts instanceof AttributesImpl) {
                ai = (AttributesImpl)atts;
            } else {
                ai = new AttributesImpl(atts);
            }
            // remove composite properties (backwards so as to avoid modifying what we are removing)
            for(int i = remove.size() - 1; i >= 0; i--) { 
                ai.removeAttribute(remove.get(i));
            }
                
            for(int i = 0; i < spacePartNames.length; i++) {
                if(spaceValues[i] != null) ai.addAttribute(FO_URI, spacePartNames[i], spacePartNames[i], "CDATA", spaceValues[i]);
            }
            
            if(fontValues != null) {
                for(int i = 0; i < fontPartNames.length; i++) {
                    if(fontValues[i+1] != null) ai.addAttribute(FO_URI, fontPartNames[i], fontPartNames[i], "CDATA", fontValues[i+1]);
                }
            }
            if(log.isDebugEnabled())
                log.debug("Fixed [" + Rule.getAttributesString(atts) + "] into [" + Rule.getAttributesString(ai) + "]");
            return ai;
        } else {
            return atts;
        }
    }

    protected void addValue(String[] values, String value, int index) {
        if(values[index] == null) values[index] = value;
        else values[index] = values[index] + " + " + value;
    }
    
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        elementComplete(localName);
        super.endElement(uri, localName, qName);
    }

    public void elementComplete(String elementName) {
        depth--;
    }

}
