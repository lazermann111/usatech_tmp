/*
 * Created on Mar 15, 2006
 *
 */
package simple.falcon.engine.css;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.DocumentHandler;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.Parser;
import org.w3c.css.sac.SACMediaList;
import org.w3c.css.sac.SelectorList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

import simple.util.EnhancedBitSet;
import simple.util.SparseBuckets;

/**
 * @author bkrug
 *
 */
public class CssFilter extends XMLFilterImpl {
    protected ProvisoSet provisoSet;
    protected Set<Rule> ancestorRules = new LinkedHashSet<Rule>();
    protected Set<Rule> baseRules = new LinkedHashSet<Rule>();    
    protected SparseBuckets<Rule> currentDescRules = new SparseBuckets<Rule>();
    protected SparseBuckets<Rule> currentChildRules = new SparseBuckets<Rule>();
    protected SparseBuckets<Rule> currentAdjacentRules = new SparseBuckets<Rule>();
    protected int depth = 0;
    protected String styleUri;
    protected Parser styleValueParser;
    protected String attributesUri;
    protected class StyleValueHandler implements DocumentHandler {
    	protected PrioritizedAttributes attributes;
		public void startDocument(InputSource arg0) throws CSSException {
		}
		public void endDocument(InputSource arg0) throws CSSException {
		}
		public void comment(String arg0) throws CSSException {
		}
		public void ignorableAtRule(String arg0) throws CSSException {
		}
		public void namespaceDeclaration(String arg0, String arg1) throws CSSException {
		}
		public void importStyle(String arg0, SACMediaList arg1, String arg2) throws CSSException {
		}
		public void startMedia(SACMediaList arg0) throws CSSException {
		}
		public void endMedia(SACMediaList arg0) throws CSSException {
		}
		public void startPage(String arg0, String arg1) throws CSSException {
		}
		public void endPage(String arg0, String arg1) throws CSSException {
		}
		public void startFontFace() throws CSSException {
		}
		public void endFontFace() throws CSSException {
		}
		public void startSelector(SelectorList arg0) throws CSSException {
		}
		public void endSelector(SelectorList arg0) throws CSSException {
		}
		public void property(String name, LexicalUnit value, boolean important) throws CSSException {
			CssSheet.addAttribute(attributes, attributesUri, name, value, important, CssSheet.IMPORTANT_PRIORITY - 1);
		}
		public PrioritizedAttributes getAttributes() {
			return attributes;
		}
		public void setAttributes(PrioritizedAttributes attributes) {
			this.attributes = attributes;
		}    	
    }
    protected StyleValueHandler styleValueHandler;
    /**
     * 
     */
    public CssFilter(ProvisoSet provisoSet, Set<Rule> rules, String attributesUri) {
        this(null, provisoSet, rules, attributesUri);
    }
    public CssFilter(XMLReader parent, ProvisoSet provisoSet, Set<Rule> rules, String attributesUri) {
        super(parent);
        this.attributesUri = attributesUri;
        this.provisoSet = provisoSet;
        parseRules(rules);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        //find all quals
        EnhancedBitSet qualifications = provisoSet.findQualifications(qName, atts);
        for(Rule rule : baseRules) {
            if(qualifications.contains(rule.getQualifications())) {
                atts = addAttributes(atts, rule.getAttributes());
            }
        }
        //must do adjacent rules first since we wipe out list at current depth
        List<Rule> rules = currentAdjacentRules.get(depth);
        if(rules != null) {
            List<Rule> savedRules = new ArrayList<Rule>(rules);
            rules.clear();
            for(Rule rule : savedRules) {
                if(qualifications.contains(rule.getQualifications())) {
                    switch(rule.getRelation()) {
                        case DESCENDENT:
                            currentDescRules.add(depth, rule.getSubRule());
                            break;
                        case CHILD:
                            currentChildRules.add(depth, rule.getSubRule());
                            break;
                        case ADJACENT:
                            currentAdjacentRules.add(depth, rule.getSubRule());
                            break;
                        case NONE:
                            atts = addAttributes(atts, rule.getAttributes());
                            break;
                    }
                }
            }
        }
        rules = currentChildRules.get(depth-1);
        if(rules != null)
            for(Rule rule : rules) {
                if(qualifications.contains(rule.getQualifications())) {
                    switch(rule.getRelation()) {
                        case DESCENDENT:
                            currentDescRules.add(depth, rule.getSubRule());
                            break;
                        case CHILD:
                            currentChildRules.add(depth, rule.getSubRule());
                            break;
                        case ADJACENT:
                            currentAdjacentRules.add(depth, rule.getSubRule());
                            break;
                        case NONE:
                            atts = addAttributes(atts, rule.getAttributes());
                            break;
                    }
                }
            }
		List<Rule> descAdditions = new ArrayList<Rule>();
        for(Rule rule : currentDescRules) {
            if(qualifications.contains(rule.getQualifications())) {
                switch(rule.getRelation()) {
                    case DESCENDENT:
						descAdditions.add(rule.getSubRule());
                        break;
                    case CHILD:
                        currentChildRules.add(depth, rule.getSubRule());
                        break;
                    case ADJACENT:
                        currentAdjacentRules.add(depth, rule.getSubRule());
                        break;
                    case NONE:
                        atts = addAttributes(atts, rule.getAttributes());
                        break;
                }
            }
        }
		// avoid concurrent mod exception
		for(Rule rule : descAdditions)
			currentDescRules.add(depth, rule);

        String style = getStyleValue(atts);
        if(style != null && style.trim().length() > 0) {
            Parser parser = getStyleValueParser(atts);
            try {
                InputSource src = new InputSource(new StringReader(style));
                parser.parseRule(src);
            } catch (IOException ioe) {
                throw new SAXException(ioe);
            }

        }
        super.startElement(uri, localName, qName, atts);
        for(Rule rule : ancestorRules) {
            if(qualifications.contains(rule.getQualifications())) {
                switch(rule.getRelation()) {
                    case DESCENDENT:
                        currentDescRules.add(depth, rule.getSubRule());
                        break;
                    case CHILD:
                        currentChildRules.add(depth, rule.getSubRule());
                        break;
                    case ADJACENT:
                        currentAdjacentRules.add(depth, rule.getSubRule());
                        break;
                }
            }
        }
        depth++;
    }    
    protected static String[] possibleStyleUris = new String[] {"", "http://www.w3.org/1999/xhtml"};
    protected String getStyleValue(Attributes atts) {
    	if(styleUri == null) {
    		for(String uri : possibleStyleUris) {
    			String style = atts.getValue(uri, "style");
    			if(style != null) {
    				styleUri = uri;
    				return style;
    			}
    		}
    		return null; // not found
    	}
		return atts.getValue(styleUri, "style");
    }
    protected Parser getStyleValueParser(Attributes atts) {
        if(styleValueParser == null) {
        	styleValueParser = new org.apache.batik.css.parser.Parser();
        	styleValueHandler = new StyleValueHandler();
        	styleValueParser.setDocumentHandler(styleValueHandler);
        }
        PrioritizedAttributes pa;
        if(atts instanceof PrioritizedAttributes) {
            pa = (PrioritizedAttributes)atts;
        } else {
            pa = new PrioritizedAttributes(atts);
        }
        styleValueHandler.setAttributes(pa);
        return styleValueParser;
    }
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        depth--;
        //remove rules at current depth
        List<Rule> rulesAtDepth = currentDescRules.get(depth);
        if(rulesAtDepth != null) rulesAtDepth.clear();
        rulesAtDepth = currentChildRules.get(depth);
        if(rulesAtDepth != null) rulesAtDepth.clear();
    }
    /**
     * @param rules
     */
    protected void parseRules(Set<Rule> rules) {
        for(Rule rule : rules) {
            switch(rule.getRelation()) {
                case DESCENDENT:
                    ancestorRules.add(rule);
                    break;
                case CHILD:
                    ancestorRules.add(rule);
                    break;
                case ADJACENT:
                    ancestorRules.add(rule);
                    break;
                case NONE:
                    baseRules.add(rule);
                    break;
            }
        }
    }
    protected Attributes addAttributes(Attributes atts, Attributes addAtts) {
        PrioritizedAttributes pa;
        if(atts instanceof PrioritizedAttributes) {
            pa = (PrioritizedAttributes)atts;
        } else {
            pa = new PrioritizedAttributes(atts);
        }
        pa.addAttributes(addAtts);
        return pa;
    }
}
