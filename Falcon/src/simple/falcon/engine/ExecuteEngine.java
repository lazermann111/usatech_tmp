/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine;

import java.util.Map;

import simple.db.DataSourceFactory;
import simple.db.ParameterException;
import simple.event.TaskListener;
import simple.io.ResourceResolver;
import simple.results.Results;

/**
 * @author bkrug
 *
 */
public interface ExecuteEngine {
    public void registerGenerator(String protocol, Generator generator) ;

    public void buildReport(Report report, Map<String,Object> parameters, Executor executor, OutputType outputType, Scene scene, Output out, ResultsType resultsType)
        throws ExecuteException, DesignException, ParameterException ;

    public void buildReport(Generator generator, Report report, Map<String,Object> parameters, Executor executor, OutputType outputType, Scene scene, Output out, ResultsType resultsType)
    	throws ExecuteException, DesignException, ParameterException;

    public Results[] buildResults(Report report, Map<String,Object> parameters, Executor executor, Scene scene, ResultsType resultsType)
    	throws ExecuteException, DesignException, ParameterException;

	public DesignEngine getDesignEngine() throws DesignException;

    public ResourceResolver getResourceResolver() ;
    public void setResourceResolver(ResourceResolver resourceResolver) ;

    public DataSourceFactory getDataSourceFactory() ;
    public void setDataSourceFactory(DataSourceFactory dataSourceFactory) throws DesignException ;
    /**
     * @return Returns the dataSourceName.
     */
	public String getDefaultDataSourceName();

    /**
     * @param dataSourceName The dataSourceName to set.
     */
	public void setDefaultDataSourceName(String defaultDataSourceName) throws DesignException;

	public String getDesignDataSourceName();

	public void setDesignDataSourceName(String designDataSourceName);

    /**
     * @return Returns the cssDirectory.
     */
    public String getCssDirectory() ;

    /**
     * @param cssDirectory The cssDirectory to set.
     */
    public void setCssDirectory(String cssDirectory) ;

    public TaskListener getTaskListener() ;
	public void setTaskListener(TaskListener taskListener) ;
}
