/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.db.specific.DbSpecific;
import simple.sql.MutableExpression;
import simple.translator.Translator;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;


/**
 * @author bkrug
 *
 */
public interface Filter extends Cloneable {
    void buildExpression(MutableExpression appendTo, DbSpecific dbSpecific) ;
    simple.falcon.engine.Param[] gatherParams() ;
    boolean sameExpression(Filter other) ;
    Filter clone();
    Filter copy(Translator translator) ;
    void writeXML(String tagName, XMLBuilder builder)throws SAXException,ConvertException;
    void readXML(ObjectBuilder builder) throws DesignException;
}
