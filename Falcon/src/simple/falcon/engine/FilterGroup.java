/*
 * Created on Feb 4, 2005
 *
 */
package simple.falcon.engine;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.specific.DbSpecific;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.lang.SystemUtils;
import simple.sql.Expression;
import simple.sql.MutableExpression;
import simple.translator.Translator;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

/**
 * @author bkrug
 *
 */
public abstract class FilterGroup implements Filter {
	public static final String SEPARATOR_AND = "AND";
	public static final String SEPARATOR_OR = "OR";
	protected static final Expression BEGIN_PARENS_EXP = new NoColumnExpression("(");
	protected static final Expression END_PARENS_EXP = new NoColumnExpression(")");
	protected static final Expression SEP_AND_EXP = new NoColumnExpression("\n  AND ");
	protected static final Expression SEP_OR_EXP =  new NoColumnExpression("\n   OR ");

    protected String separator = SEPARATOR_AND;
    protected Expression sepExp = SEP_AND_EXP;
    protected Set<Filter> childFilters = new LinkedHashSet<Filter>();
    protected Filter whereFilter;
    protected Filter havingFilter;
    protected boolean parsed = false;
    protected StandardDesignEngine designEngine;

    protected static class NoXMLFilterGroup extends FilterGroup {
    	public void readXML(ObjectBuilder builder) {
    		// do nothing
    	}
    	public void writeXML(String tagName, XMLBuilder builder) {
    		// do nothing
    	}
    }
    /**
     *
     */
    public FilterGroup() {
        super();
    }
    public FilterGroup(simple.falcon.engine.standard.StandardDesignEngine designEngine){
    	super();
        this.designEngine = designEngine;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
    	int hc = 0;
    	hc = SystemUtils.addHashCode(hc, separator);
    	hc = SystemUtils.addHashCode(hc, childFilters);
    	return hc;
    }
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
    	if(this == obj) return true;
    	if(!(obj instanceof FilterGroup)) return false;
    	FilterGroup fg = (FilterGroup)obj;
    	return ConvertUtils.areEqual(separator, fg.separator)
    		&& childFilters.equals(fg.childFilters);
    }

    @Override
	public FilterGroup clone() {
    	FilterGroup copy;
		try {
			copy = (FilterGroup)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError("Cloneable class threw CloneNotSupportedException");
		}
    	copy.childFilters = new LinkedHashSet<Filter>();
    	for(Filter f : childFilters) {
    		copy.childFilters.add(f.clone());
    	}
    	return copy;
    }

    public FilterGroup copy(Translator translator) {
    	FilterGroup copy;
		try {
			copy = (FilterGroup)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError("Cloneable class threw CloneNotSupportedException");
		}
    	copy.childFilters = new LinkedHashSet<Filter>();
    	for(Filter f : childFilters) {
    		copy.childFilters.add(f.copy(translator));
    	}
    	return copy;
    }

    public void addFilter(Filter filter) {
        parsed = false;
        childFilters.add(filter);
    }

    public boolean hasFilter(Filter filter) {
        //return childFilters.contains(filter);
        for(Filter f : childFilters) {
            if(f.sameExpression(filter)) return true;
        }
        return false;
    }

    /**
     * @return number of filters in the current group
     * (equivalent to <code>filterCount(false,true)</code>)
     */
    public int filterCount() {
    	return childFilters.size();
    }


    /**
     * @param includeGroups include groups in the count (as opposed to items only)t
     * @return number of filters in the group. non recursive.
     */
    public int filterCount(boolean includeGroups) {
    	return filterCount(false,includeGroups);
    }

    /**
     * @param recurseChildren include children of child groups in the count
     * @param includeGroups include groups in the count (as opposed to items only)
     * @return the number of filters (fitting the parameters) in the group
     */
    public int filterCount(boolean recurseChildren, boolean includeGroups) {
    	int count;

    	if(includeGroups){
    		count = childFilters.size();
    	} else {
    		count = 0;
    		for(Iterator<Filter> fi=childFilters.iterator(); fi.hasNext();){
    			Filter f = fi.next();
    			if(f instanceof FilterItem){
    				count++;
    			}
    		}
    	}

    	if (recurseChildren){
    		for(Iterator<Filter> fi=childFilters.iterator(); fi.hasNext();){
    			Filter f = fi.next();
    			if(f instanceof FilterGroup){
    				count += ((FilterGroup)f).filterCount(recurseChildren,includeGroups);
    			}
    		}
    	}
    	return count;
    }

    Iterator<Filter> filterIterator() {
        return childFilters.iterator();
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        if(separator == null) throw new NullPointerException("Separator cannot be null");
        if(SEPARATOR_AND.equalsIgnoreCase(separator)) {
        	sepExp = SEP_AND_EXP;
        } else if(SEPARATOR_OR.equalsIgnoreCase(separator)) {
        	sepExp = SEP_OR_EXP;
        } else {
        	throw new IllegalArgumentException("Invalid value '" + separator + "' for Separatorl");
        }
        this.separator = separator;
    }

    public Filter buildWhereFilter() {
        ensureParse();
        return whereFilter;
    }

    public Filter buildHavingFilter() {
        ensureParse();
        return havingFilter;
    }

    /**
     *
     */
    protected void ensureParse() {
        if(parsed) return;
        parseFilters();
        parsed = true;
    }

    protected void parseFilters() {
    	FilterGroup fgw = new NoXMLFilterGroup();
        FilterGroup fgh = new NoXMLFilterGroup();
        fgw.setSeparator(getSeparator());
        fgh.setSeparator(getSeparator());
        for(Filter filter : childFilters) {
            if(filter instanceof FilterGroup) {
                FilterGroup fgc = (FilterGroup)filter;
                Filter w = fgc.buildWhereFilter();
                Filter h = fgc.buildHavingFilter();
                if(w != null) fgw.addFilter(w);
                if(h != null) fgh.addFilter(h);
                if(getSeparator().equals(SEPARATOR_OR) && w == null && h != null) {//all true
                	whereFilter = null;
                	havingFilter = this;
                	return;
                }
            } else if(filter instanceof FilterItem) {
            	FilterItem fi = (FilterItem)filter;
            	if(fi.getAggregateType() != null ||(fi.getField()!=null&& fi.getField().containsAggregate())) {
            		if(getSeparator().equals(SEPARATOR_OR)) {
                    	whereFilter = null;
                    	havingFilter = this;
                    	return;
                	}
                    fgh.addFilter(filter);
            	} else {
            		fgw.addFilter(filter);
            	}
            	/*
            	switch(fi.getField().getAdditiveType()) {
                	case NONE:
                    default:
                    	if(fi.getAggregateType() == null) {
	                        fgw.addFilter(filter);
	                		break;
                    	}
                    case FULL:
                    case SEMI:
                    	if(getSeparator().equals(SEPARATOR_OR)) {
                        	whereFilter = null;
                        	havingFilter = this;
                        	return;
                    	}
                        fgh.addFilter(filter);
                    	break;

                }*/
            } else {
                fgw.addFilter(filter);
            }
        }
        if(getSeparator().equals(SEPARATOR_OR) && fgw.filterCount() > 0 && fgh.filterCount() > 0) {
        	whereFilter = fgw;
        	havingFilter = this;
        } else {
	        whereFilter = (fgw.filterCount() == 0 ? null : fgw);
	        havingFilter = (fgh.filterCount() == 0 ? null : fgh);
        }
    }

    /* (non-Javadoc)
     * @see simple.falcon.engine.Filter#buildExpression()
     */
    public void buildExpression(MutableExpression appendTo, DbSpecific dbSpecific) {
    	switch(filterCount()) {
    		case 0:
    			return;
    		case 1:
    			childFilters.iterator().next().buildExpression(appendTo, dbSpecific);
    			break;
    		default:
    			appendTo.append(BEGIN_PARENS_EXP);
    			boolean first = true;
    			for(Filter filter : childFilters) {
                    if(first)
                    	first = false;
                    else
                    	appendTo.append(sepExp);
                    filter.buildExpression(appendTo, dbSpecific);
                }
    			appendTo.append(END_PARENS_EXP);
    	}
    }

    /* (non-Javadoc)
     * @see simple.falcon.engine.Filter#gatherParams()
     */
    public Param[] gatherParams() {
        switch(childFilters.size()) {
            case 0: return new Param[0];
            case 1: return childFilters.iterator().next().gatherParams();
        }
        int cnt = 0;
        Param[][] paramBox = new Param[childFilters.size()][];
        int i = 0;
        for(Iterator<Filter> iter = childFilters.iterator(); iter.hasNext(); i++) {
            Param[] subParams = iter.next().gatherParams();
            cnt += subParams.length;
            paramBox[i] = subParams;
        }
        Param[] params = new Param[cnt];
        cnt = 0;
        for(Param[] subParams : paramBox) {
            System.arraycopy(subParams, 0, params, cnt, subParams.length);
            cnt += subParams.length;
        }
        return params;
    }

    public Set<Filter> getChildFilters() {
        return Collections.unmodifiableSet(childFilters);
    }

    public boolean sameExpression(Filter other) {
        if(!(other instanceof FilterGroup)) return false;
        FilterGroup fg = (FilterGroup)other;
        if(filterCount() == 0 && fg.filterCount() == 0) return true;
        if(!getSeparator().equals(fg.getSeparator()) || childFilters.size() != fg.childFilters.size()) return false;
        for(Filter f : childFilters) {
            if(!fg.hasFilter(f)) return false;
        }
        return true;
    }
}
