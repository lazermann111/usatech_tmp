package simple.falcon.engine;

import simple.text.StringUtils;

public class Version implements Comparable<Version> {
	protected static final char[] DELIMS = "._-".toCharArray();
	protected String versionText;
	protected Comparable<?>[] parts;
	public Version(String versionText) {
		this.versionText = versionText;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && toString().equals(obj.toString());
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return versionText;
	}

	public int compareTo(Version o) {
		if(o == null) return 1;
		ensureParse();
		o.ensureParse();
		for(int i = 0; i < parts.length && i < o.parts.length; i++) {
			if(parts[i] instanceof Number) {
				if(o.parts[i] instanceof Number) {
					int c = ((Number)parts[i]).intValue() - ((Number)o.parts[i]).intValue();
					if(c != 0) return c;
				} else {
					return 1;
				}
			} else if(o.parts[i] instanceof Number) {
				return -1;
			} else {
				int c = parts[i].toString().compareTo(o.parts[i].toString());
				if(c != 0) return c;
			}
		}
		return parts.length - o.parts.length;
	}

	protected void ensureParse() {
		if(parts == null) {
			String[] tmp = StringUtils.split(versionText, DELIMS, false);
			parts = new Comparable[tmp.length];
			for(int i = 0; i < tmp.length; i++) {
				try {
					parts[i] = Integer.parseInt(tmp[i]);
				} catch(NumberFormatException e) {
					parts[i] = tmp[i];
				}
			}
		}
	}

}
