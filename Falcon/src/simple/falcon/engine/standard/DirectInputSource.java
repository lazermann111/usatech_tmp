/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.Map;

import org.xml.sax.InputSource;

import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.results.Results;

/**
 * @author bkrug
 *
 */
public class DirectInputSource extends InputSource {
    protected Report report;
    protected Results[] results;
    protected Map<String,Object> parameters;
    protected ColumnNamer namer;
    protected Scene scene;
    protected OutputType outputType;

    /**
     *
     */
    public DirectInputSource(Report report, Results[] results, Map<String,Object> parameters, ColumnNamer namer, Scene scene, OutputType outputType) {
        this.report = report;
        this.results = results;
        this.parameters = parameters;
        this.namer = namer;
        this.scene = scene;
        this.outputType = outputType;
    }

    /**
     * @return Returns the report.
     */
    public Report getReport() {
        return report;
    }

    /**
     * @param report The report to set.
     */
    public void setReport(Report report) {
        this.report = report;
    }

    /**
     * @return Returns the results.
     */
    public Results[] getResults() {
        return results;
    }
    /**
     * @param results The results to set.
     */
    public void setResults(Results[] results) {
        this.results = results;
    }

    /**
     * @return Returns the parameters.
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * @param parameters The parameters to set.
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return Returns the scene.
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * @param scene The scene to set.
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public ColumnNamer getNamer() {
        return namer;
    }

    public void setNamer(ColumnNamer namer) {
        this.namer = namer;
    }

    /**
     * @return Returns the outputType.
     */
    public OutputType getOutputType() {
        return outputType;
    }

    /**
     * @param outputType The outputType to set.
     */
    public void setOutputType(OutputType outputType) {
        this.outputType = outputType;
    }

}
