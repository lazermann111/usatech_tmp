/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import simple.falcon.engine.ExecuteEngine;

public class ChartFOGenerator extends FOGenerator {
    public ChartFOGenerator(ExecuteEngine engine) {
        super(engine, InputSourceType.CHART);
    }
}