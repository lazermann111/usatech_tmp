package simple.falcon.engine.standard;

import simple.db.Parameter;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Filter;
import simple.falcon.engine.FilterGroup;
import simple.falcon.engine.Param;

public class FilterGroupAwareParameter extends Parameter {
	protected final boolean having;
	protected final int paramIndex;

	public FilterGroupAwareParameter(boolean having, int paramIndex) {
		super();
		this.having = having;
		this.paramIndex = paramIndex;
	}

	public void updateDefaultValue(FilterGroup filter) throws DesignException {
		Filter f = having ? filter.buildHavingFilter() : filter.buildWhereFilter();
		if(f == null)
			throw new DesignException(having ? "Having" : "Where" + " filter is null");
		Param[] p = f.gatherParams();
		if(p == null || p.length <= paramIndex)
			throw new DesignException("Parameter index (" + paramIndex + ") is greater than allowed (" + (p == null ? 0 : (p.length - 1)) + ")");
		Object v = p[paramIndex].getValue();
		setDefaultValue(v);
	}
}
