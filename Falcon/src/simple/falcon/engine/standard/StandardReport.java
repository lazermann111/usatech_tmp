/*
 * Created on Nov 17, 2005
 *
 */
package simple.falcon.engine.standard;

import java.text.Format;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public class StandardReport implements Report {
	public static final int DEFAULT_OUTPUT_TYPE = 22;
    protected Folio[] folios;
    protected OutputType outputType;
    protected Format title;
    protected Format subtitle;
    protected String name;
    protected Long reportId;
    protected final SortedMap<String, String> verbage = new TreeMap<String, String>(); // so that xml is consistent
	protected final Map<String,String> verbageUnmod = Collections.unmodifiableMap(verbage);
    protected final SortedMap<String, String> directives = new TreeMap<String, String>(); // so that xml is consistent
	protected final Set<String> directiveNames = Collections.unmodifiableSet(directives.keySet());
    protected boolean updated;
    protected StandardDesignEngine designEngine;

    public StandardReport() {
    }

    public StandardReport(StandardDesignEngine designEngine){
    	this.designEngine=designEngine;
    }

    /**
	 * @see simple.falcon.engine.Report#getReportId()
	 */
    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getFolios()
	 */
    public Folio[] getFolios() {
        return folios;
    }
    /**
	 * @see simple.falcon.engine.Report#setFolios(simple.falcon.engine.Folio[])
	 */
    public void setFolios(Folio[] folios) {
        this.folios = folios;
        updated = true;
    }
    /**
	 * @see simple.falcon.engine.Report#getOutputType()
	 */
    public OutputType getOutputType() {
        return outputType;
    }
    /**
	 * @see simple.falcon.engine.Report#setOutputType(simple.falcon.engine.OutputType)
	 */
    public void setOutputType(OutputType outputType) {
        this.outputType = outputType;
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getSubtitle()
	 */
    public Format getSubtitle() {
        return subtitle;
    }

    /**
	 * @see simple.falcon.engine.Report#getSubtitle(java.lang.Object)
	 */
    public String getSubtitle(Object args) {
        if(subtitle == null) return "";
        return subtitle.format(args);
    }

    /**
	 * @see simple.falcon.engine.Report#setSubtitle(java.text.Format)
	 */
    public void setSubtitle(Format subtitle) {
        this.subtitle = subtitle;
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getTitle()
	 */
    public Format getTitle() {
        return title;
    }

    /**
	 * @see simple.falcon.engine.Report#getTitle(java.lang.Object)
	 */
    public String getTitle(Object args) {
        if(title == null) return "";
        return title.format(args);
    }

    /**
	 * @see simple.falcon.engine.Report#setTitle(java.text.Format)
	 */
    public void setTitle(Format title) {
        this.title = title;
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getName()
	 */
    public String getName() {
        return name;
    }

    /**
	 * @see simple.falcon.engine.Report#setName(java.lang.String)
	 */
    public void setName(String name) {
        this.name = name;
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getDirective(java.lang.String)
	 */
    public String getDirective(String name) {
        return directives.get(name);
    }

    /**
	 * @see simple.falcon.engine.Report#setDirective(java.lang.String, java.lang.String)
	 */
    public void setDirective(String name, String directive) {
        directives.put(name, directive);
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getDirectiveNames()
	 */
    public Set<String> getDirectiveNames() {
        return directiveNames;
    }

    /**
	 * @see simple.falcon.engine.Report#getVerbage(java.lang.String)
	 */
    public String getVerbage(String text) {
        return getVerbage(text, text);
    }

    /**
	 * @see simple.falcon.engine.Report#getVerbage(java.lang.String, java.lang.String)
	 */
    public String getVerbage(String text, String defaultTranslation) {
        String translation = verbage.get(text);
        return (translation == null ? defaultTranslation : translation);
    }

    /**
	 * @see simple.falcon.engine.Report#setVerbage(java.lang.String, java.lang.String)
	 */
    public void setVerbage(String text, String translation) {
        verbage.put(text, translation);
        updated = true;
    }

    /**
	 * @see simple.falcon.engine.Report#getVerbageMap()
	 */
    public Map<String,String> getVerbageMap() {
        return verbageUnmod;
    }

    public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException{
		 Map<String,String> atts = new LinkedHashMap<String, String>();
		 atts.put("class", Report.class.getName());
		 atts.put("reportId", ConvertUtils.getStringSafely(getReportId()));
		 atts.put("name", getName());
		 atts.put("title",ConvertUtils.getFormatString(getTitle()));
		 atts.put("subtitle", ConvertUtils.getFormatString(getSubtitle()));
		 if(tagName==null) tagName="report";
		 builder.elementStart(tagName, atts);
		 if(getOutputType()!=null){
			 builder.put("outputType", getOutputType());
		 }
		 if(directives.size()>0){
			builder.elementStart("directives");
			 for(Map.Entry<String,String> entry : directives.entrySet()) {
					builder.elementStart("directive", new String[]{"name", "value"},entry.getKey(), entry.getValue());
					builder.elementEnd("directive");
			 }
			 builder.elementEnd("directives");
		 }
		 if(verbage.size()>0){
			builder.elementStart("verbages");
			 for(Map.Entry<String,String> entry : verbage.entrySet()) {
					builder.elementStart("verbage", new String[]{"name", "value"},entry.getKey(), entry.getValue());
					builder.elementEnd("verbage");
			 }
			 builder.elementEnd("verbages");
		 }
		 if(folios != null && folios.length > 0){
			builder.elementStart("folios");
			 for(int i=0; i<folios.length; i++){
				 if(folios[i]!=null){
					 folios[i].writeXML("folio", builder);
				 }
			 }
			 builder.elementEnd("folios");
		 }
		 builder.elementEnd(tagName);
	 }
	 public void readXML(ObjectBuilder builder) throws DesignException {
		if(!StringUtils.isBlank(builder.getAttrValue("reportId"))) {
			setReportId(builder.get("reportId", long.class));
		}
		setName(builder.getAttrValue("name"));
		if(!StringUtils.isBlank(builder.getAttrValue("title")))
			setTitle(ConvertUtils.getFormat(builder.getAttrValue("title")));
		if(!StringUtils.isBlank(builder.getAttrValue("subtitle")))
			setSubtitle(ConvertUtils.getFormat(builder.getAttrValue("subtitle")));
		ObjectBuilder[] directives = builder.getSubNodes("directives");
		if(directives != null && directives.length > 0) {
			ObjectBuilder[] directive = directives[0].getSubNodes("directive");
			if(directive != null) {
				for(int i = 0; i < directive.length; i++) {
					setDirective(directive[i].getAttrValue("name"), directive[i].getAttrValue("value"));
				}
			}
		}
		ObjectBuilder[] verbages = builder.getSubNodes("verbages");
		if(verbages != null && verbages.length > 0) {
			ObjectBuilder[] verbage = verbages[0].getSubNodes("verbage");
			if(verbage != null) {
				for(int i = 0; i < verbage.length; i++) {
					setVerbage(verbage[i].getAttrValue("name"), verbage[i].getAttrValue("value"));
				}
			}
		}
		
		ObjectBuilder[] folios = builder.getSubNodes("folios");
		OutputType folioOutputType = null;
		if(folios != null && folios.length > 0) {
			ObjectBuilder[] folio = folios[0].getSubNodes("folio");
			if(folio.length > 0) {
				Folio[] folioArray = new Folio[folio.length];
				for(int i = 0; i < folio.length; i++) {
					Folio f = designEngine.newFolio();
					f.readXML(folio[i]);
					folioArray[i] = f;
					if(folioOutputType == null)
						folioOutputType = f.getOutputType();
				}
				setFolios(folioArray);
			}
		}
		ObjectBuilder[] outputTypeArray = builder.getSubNodes("outputType");
		if(outputTypeArray != null && outputTypeArray.length > 0) {
			Class<? extends OutputType> clazz;
			try {
				clazz = Class.forName(outputTypeArray[0].getAttrValue("class")).asSubclass(OutputType.class);
			} catch(ClassNotFoundException e) {
				throw new DesignException(e);
			}
			OutputType outputTypeObj;
			try {
				outputTypeObj = outputTypeArray[0].read(clazz);
			} catch(ConvertException e) {
				throw new DesignException(e);
			}
			setOutputType(outputTypeObj);
		} else if(folioOutputType != null) {
			setOutputType(folioOutputType);
		} else {
			setOutputType(designEngine.getOutputType(DEFAULT_OUTPUT_TYPE));
		}

		updated = false;
	 }

	/**
	 * @see simple.falcon.engine.Report#isUpdated()
	 */
	public boolean isUpdated() {
		return updated;
	}
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hc = 0;
		hc = SystemUtils.addHashCode(hc, title);
		hc = SystemUtils.addHashCode(hc, subtitle);
		hc = SystemUtils.addHashCode(hc, folios);
		hc = SystemUtils.addHashCode(hc, outputType);
		return hc;
	}
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof StandardReport)) return false;
		StandardReport r = (StandardReport)obj;
		return ConvertUtils.areEqual(title, r.title)
			&& ConvertUtils.areEqual(subtitle, r.subtitle)
			&& ConvertUtils.areEqual(outputType, r.outputType)
			&& Arrays.deepEquals(folios, r.folios)
			&& directives.equals(r.directives)
			&& verbage.equals(r.verbage);
	}
}
