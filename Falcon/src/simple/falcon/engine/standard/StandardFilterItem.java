/*
 * Created on Oct 18, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.specific.DbSpecific;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Operator;
import simple.falcon.engine.Param;
import simple.io.Log;
import simple.results.Results;
import simple.sql.MutableExpression;
import simple.sql.SQLType;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public class StandardFilterItem extends FilterItem {
	private static final Log log = Log.getLog();
    protected Long filterId;
    protected StandardDesignEngine designEngine;
    public StandardFilterItem(StandardDesignEngine designEngine) {
        super();
        this.designEngine = designEngine;
    }
    public Long getFilterId() {
        return filterId;
    }
    public void setFilterId(Long filterId) {
        this.filterId = filterId;
    }

	/*
	@Override
	public boolean equals(Object obj) {
	    if(!(obj instanceof StandardFilterItem)) return false;
	    if(filterId != null) {
	        StandardFilterItem sfg = (StandardFilterItem)obj;
	        if(sfg.filterId != null)
	            return filterId.equals(sfg.filterId);
	    }
	    return super.equals(obj);
	}
	@Override
	public int hashCode() {
	    if(filterId != null)
	        return filterId.hashCode();
	    return super.hashCode();
	}*/
	@Override
	protected Param createParam(String name, String label, String prompt, SQLType sqlType, boolean fromField) {
		StandardParam param = new StandardParam(designEngine, fromField);
		param.setName(name);
		param.setLabel(label);
		param.setPrompt(prompt);
		param.setSqlType(sqlType);
		try {
			param.setEditor(designEngine.createEditor(designEngine.getDefaultEditorString(sqlType), this));
		} catch(DesignException e) {
			log.warn("Could not get default param editor", e);
		}
		return param;
	}

    @Override
	public void buildExpression(MutableExpression appendTo, DbSpecific dbSpecific) {
    	appendTo.append(getOperator().getExpression(getField(), getAggregateType(), dbSpecific));
    }
    public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException{
		if(tagName==null) tagName="filter";
		Map<String,String> filterAtts = new LinkedHashMap<String, String>(4);
		filterAtts.put("filterId", ConvertUtils.getStringSafely(getFilterId()));
		if(field!=null){
			filterAtts.put("fieldId", ConvertUtils.getStringSafely(field.getId()));
			filterAtts.put("fieldLabel", ConvertUtils.getStringSafely(field.getLabel()));
		}
		if(operator!=null){
			filterAtts.put("operatorId", ConvertUtils.getStringSafely(operator.getOperatorId()));
		}
		if(getAggregateType() != null)
			filterAtts.put("aggregateType", ConvertUtils.getStringSafely(getAggregateType()));
    	builder.elementStart(tagName, filterAtts);
		if(operatorParams != null) {
			for(int i = 0; i < operatorParams.length; i++) {
				operatorParams[i].writeXML("param", builder);
    		}
    	}
    	builder.elementEnd(tagName);
    }

    public void readXML(ObjectBuilder builder) throws DesignException {
		setFilterId(builder.get("filterId", long.class));
		if(!StringUtils.isBlank(builder.getAttrValue("fieldId"))){
			setField(designEngine.getField(builder.get("fieldId", Long.class)));
		}
		if(!StringUtils.isBlank(builder.getAttrValue("operatorId"))){
			Operator operator=designEngine.getOperator(builder.get("operatorId", int.class));
			setOperator(operator);
		}
		setAggregateType(builder.get("aggregateType", Results.Aggregate.class));
		ObjectBuilder[] params = builder.getSubNodes("param");
		if(params!=null){
			List<Param> filterParams = new ArrayList<Param>();
			for(int i=0; i<params.length; i++){
				StandardParam param = new StandardParam(designEngine, false);
				param.readXML(params[i]);
				filterParams.add(param);
			}
			setOperatorParams(filterParams.toArray(new Param[filterParams.size()]));
		}
	 }
}
