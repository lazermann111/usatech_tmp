/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.Map;

import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Generator;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.xml.ChartAdapter;
import simple.falcon.xml.ChartInputSource;
import simple.falcon.xml.ReportAdapter;
import simple.falcon.xml.ReportInputSource;
import simple.results.Results;

public abstract class XMLBaseGenerator implements Generator {
    //private static final Log log = Log.getLog();
    public static enum InputSourceType {REPORT, CHART };
    protected ExecuteEngine engine;
    protected InputSourceType inputSourceType;
    public XMLBaseGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
        this.engine = engine;
        this.inputSourceType = inputSourceType;
    }

	protected SAXSource createXMLSource(Report report, Results[] results, Map<String, Object> parameters, Executor executor, Scene scene, OutputType outputType) throws DesignException {
        // Pipe this directly to the transformer using SAXEvents
        return new SAXSource(createReader(), createInputSource(report, results, parameters, executor, scene, outputType));
    }

	protected InputSource createInputSource(Report report, Results[] results, Map<String, Object> parameters, Executor executor, Scene scene, OutputType outputType) throws DesignException {
        switch(inputSourceType) {
            case REPORT: return new ReportInputSource(report, results, parameters, engine.getDesignEngine().getColumnNamer(), executor, scene, outputType);
            case CHART: return new ChartInputSource(report, results, parameters, engine.getDesignEngine().getColumnNamer(), executor, scene, outputType);
            default: return null;
        }
    }

    protected XMLReader createReader() {
        switch(inputSourceType) {
	        case REPORT: return new ReportAdapter<ReportInputSource>();
	        case CHART: return new ChartAdapter();
	        default: return null;
	    }
    }
/*
    protected ObjectCache<File> getChartCache(Map<String,Object> requestParamValues) {
        try {
            return RequestUtils.getOrCreateAttribute((ServletRequest)requestParamValues.get("request"), "chartCache", SessionFileCache.class, "session");
        } catch (ServletException e) {
            log.warn("Could not get chart cache", e);
        } catch (ConvertException e) {
            log.warn("Could not get chart cache", e);
        } catch (InstantiationException e) {
            log.warn("Could not get chart cache", e);
        } catch (IllegalAccessException e) {
            log.warn("Could not get chart cache", e);
        } catch(NullPointerException e) {
            log.warn("Could not get chart cache", e);
        }
        return null;
    }*/
    public boolean isResetResultsRequired(OutputType outputType) {
    	return false;
    }
}