/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.print.PrintService;
import javax.xml.transform.Result;
import javax.xml.transform.sax.SAXResult;

import org.apache.fop.apps.Driver;
import org.apache.fop.render.awt.AWTRenderer;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.io.Log;
import simple.io.logging.avalon.SimpleAvalonLogger;

/** This generator prints the report to a printer
 * @author bkrug
 *
 */
public class PDFPrintGenerator extends FOGenerator {
    private static final Log log = Log.getLog();
    protected PrintService printService;
    public PDFPrintGenerator(ExecuteEngine engine) {
        super(engine);
    }
    
    public PDFPrintGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
        super(engine, inputSourceType);
    }

    @Override
    protected Result getStreamResult(OutputType outputType, final Output out) throws IOException {
        // render the PDF
        Driver driver = new Driver();
        driver.setLogger(new SimpleAvalonLogger(log));
        final PrinterJob printerJob = PrinterJob.getPrinterJob();
        printerJob.setCopies(1);
        if(printService != null) {
        	try {
        		printerJob.setPrintService(printService);               
            } catch (PrinterException e) {
            	IOException ioe = new IOException("Unable to set print service");
            	ioe.initCause(e);
                throw ioe;
            }
        }
        AWTRenderer renderer = new AWTRenderer(null) {
        	public void stopRenderer(OutputStream outputStream) throws IOException {
                super.stopRenderer(outputStream);
                try {
                    printerJob.print();
                } catch (PrinterException e) {
                	IOException ioe = new IOException("Unable to print");
                	ioe.initCause(e);
                    throw ioe;
                }
                writeSuccessResult(out, getNumberOfPages(), printerJob.getPrintService());
            }
        };
        printerJob.setPageable(renderer);
        driver.setRenderer(renderer);
        return new SAXResult(driver.getContentHandler());
    }
    
    protected void writeSuccessResult(Output out, int pages, PrintService ps) throws IOException {
    	PrintWriter pw = new PrintWriter(out.getWriter());
    	pw.println("Printed " + pages + " to " + ps.getName());   
    	pw.flush();
    }
}