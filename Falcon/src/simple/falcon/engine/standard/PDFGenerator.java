/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;

import javax.xml.transform.Result;
import javax.xml.transform.sax.SAXResult;

import org.apache.fop.apps.Driver;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.io.Log;
import simple.io.logging.avalon.SimpleAvalonLogger;

public class PDFGenerator extends FOGenerator {
    private static final Log log = Log.getLog();
    
    public PDFGenerator(ExecuteEngine engine) {
        super(engine);
    }
    
    public PDFGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
        super(engine, inputSourceType);
    }

    @Override
    protected Result getStreamResult(OutputType outputType, Output out) throws IOException {
        // render the PDF
		//* FOP 0.20.5
		Driver driver = new Driver();
		driver.setLogger(new SimpleAvalonLogger(log));
		driver.setRenderer(Driver.RENDER_PDF);
		driver.setOutputStream(out.getOutputStream());
		return new SAXResult(driver.getContentHandler());
		/*/ FOP 1.0
		FopFactory fopFactory = FopFactory.newInstance();
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent(); 
		// foUserAgent.setBaseURL(info.scene.getBaseUrl());
		Fop fop;
		DefaultHandler fopHandler;
		try {
			fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out.getOutputStream());
			fopHandler = fop.getDefaultHandler();
		} catch(FOPException e) {
			throw new IOException(e);
		}
		return new SAXResult(fopHandler);
		// */
    }
}