/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import simple.falcon.engine.ExecuteEngine;

public class ChartXSLGenerator extends XSLGenerator {
    public ChartXSLGenerator(ExecuteEngine engine) {
        super(engine, InputSourceType.CHART);
    }
}