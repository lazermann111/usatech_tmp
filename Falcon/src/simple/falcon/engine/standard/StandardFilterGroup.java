/*
 * Created on Oct 18, 2005
 *
 */
package simple.falcon.engine.standard;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Filter;
import simple.falcon.engine.FilterGroup;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public class StandardFilterGroup extends FilterGroup {
    protected Long filterGroupId;
    public StandardFilterGroup() {
        super();
    }
    public StandardFilterGroup(StandardDesignEngine designEngine) {
    	super(designEngine);
    }
    public Long getFilterGroupId() {
        return filterGroupId;
    }
    public void setFilterGroupId(Long filterGroupId) {
        this.filterGroupId = filterGroupId;
    }

	/*
	@Override
	public boolean equals(Object obj) {
	    if(!(obj instanceof StandardFilterGroup)) return false;
	    if(filterGroupId != null) {
	        StandardFilterGroup sfg = (StandardFilterGroup)obj;
	        if(sfg.filterGroupId != null)
	            return filterGroupId.equals(sfg.filterGroupId);
	    }
	    return super.equals(obj);
	}
	@Override
	public int hashCode() {
	    if(filterGroupId != null)
	        return filterGroupId.hashCode();
	    return super.hashCode();
	}
	*/
    public void writeXML(String tagName, XMLBuilder builder) throws SAXException,ConvertException{
    	if(tagName==null) tagName="filterGroup";
    	builder.elementStart(tagName, new String[]{"filterGroupId", "separator"}, ConvertUtils.getStringSafely(getFilterGroupId()),getSeparator());
    	for (Filter f : childFilters){
    		f.writeXML(null, builder);
    	}
    	builder.elementEnd(tagName);
    }

    public void readXML(ObjectBuilder builder) throws DesignException {
    	 setFilterGroupId(builder.get("filterGroupId", long.class));
		 setSeparator(builder.getAttrValue("separator"));
		 ObjectBuilder[] filters = builder.getSubNodes("filter");
		 if(filters!=null){
			 for(int i=0; i<filters.length; i++){
				 StandardFilterItem filter = new StandardFilterItem(designEngine);
				 filter.readXML(filters[i]);
				 addFilter(filter);
			 }
		 }
		 ObjectBuilder[] filterGroups = builder.getSubNodes("filterGroup");
		 if(filterGroups!=null){
			 for(int i=0; i<filterGroups.length; i++){
				 StandardFilterGroup filterGroup = new StandardFilterGroup(designEngine);
				 filterGroup.readXML(filterGroups[i]);
				 addFilter(filterGroup);
			 }
		 }
	}
}
