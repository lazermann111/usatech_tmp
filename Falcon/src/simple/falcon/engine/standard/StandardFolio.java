/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine.standard;

import java.sql.Types;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.locks.ReentrantLock;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Filter;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.SortOrder;
import simple.lang.SystemUtils;
import simple.results.DataGenre;
import simple.results.Results;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;
import simple.xml.XMLizable;

/**
 * @author bkrug
 *
 */
public class StandardFolio implements Folio, XMLizable {
	
	protected String dataSourceName;	

	protected String query;

	public String getQuery() {
		return query;
	}

	protected class InnerStandardPillar extends StandardPillar {
		protected int index;

		public InnerStandardPillar() {
			super(StandardFolio.this);
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			movePillar(this.index, index);
		}
	}

	protected Filter filter;
	protected Long folioId;
	protected Format title;
	protected Format subtitle;
	protected FieldOrder[] fieldOrders = null;
	protected final List<InnerStandardPillar> pillars = new ArrayList<InnerStandardPillar>();
	protected OutputType outputType;
	protected String name;
	protected String chartType;
	protected StandardDesignEngine designEngine;
	protected final Map<Integer, Integer> maxRowsPerGroup = new HashMap<Integer, Integer>();
	protected final SortedMap<String, String> directives = new TreeMap<String, String>(); // so that xml is consistent
	protected final ReentrantLock lock = new ReentrantLock();
	protected boolean updated;

	public StandardFolio(StandardDesignEngine designEngine) {
		this.designEngine = designEngine;
	}

	public StandardFolio copy(Translator translator) {
		StandardFolio copy = designEngine.newFolio();
		lock.lock();
		try {
			copy.filter = filter.copy(translator);
			copy.folioId = folioId;
			copy.title = TranslatorFactory.translateFormat(title, translator);
			copy.subtitle = TranslatorFactory.translateFormat(subtitle, translator);
			for(InnerStandardPillar p : pillars) {
				p.copy(translator, copy);
			}
			copy.outputType = outputType;
			copy.name = name;
			copy.chartType = chartType;
			copy.maxRowsPerGroup.putAll(maxRowsPerGroup);
			copy.directives.putAll(directives);
			copy.updated = updated;
		} finally {
			lock.unlock();
		}
		return copy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * simple.falcon.engine.standard.FolioI#setFilter(simple.falcon.engine.standard
	 * .Filter)
	 */
	public void setFilter(Filter filter) {
		this.filter = filter;
		updated = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getSubtitle()
	 */
	public Format getSubtitle() {
		return subtitle;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getSubtitle(java.lang.Object)
	 */
	public String getSubtitle(Object args) {
		if(subtitle == null)
			return "";
		return subtitle.format(args);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#setSubtitle(java.text.Format)
	 */
	public void setSubtitle(Format subtitle) {
		this.subtitle = subtitle;
		updated = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getTitle()
	 */
	public Format getTitle() {
		return title;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getTitle(java.lang.Object)
	 */
	public String getTitle(Object args) {
		if(title == null)
			return ""; // "Custom Report #" + (folioId == null ? "N-" +
						// hashCode() : folioId.toString());
		return title.format(args);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#setTitle(java.text.Format)
	 */
	public void setTitle(Format title) {
		this.title = title;
		updated = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#addLinkPillar(int,
	 * java.lang.String, java.lang.String, simple.falcon.engine.standard.Field,
	 * int)
	 */
	public void addLinkPillar(int groupingLevel, String actionFormat, String helpFormat, Field field, SortOrder sortOrder) {
		addPillar(Pillar.PillarType.LINK, groupingLevel, field.getLabel(), null, null, field.getFormat(), actionFormat, helpFormat, null, getSortType(field), new SortOrder[] { sortOrder },
				new Field[] { field }, null, null);
	}

	void addPillarWithDefaults(Pillar.PillarType pillarType, int groupingLevel, String label, String description, String styleFormat, String displayFormat, String actionFormat, String helpFormat,
			String sortFormat, SortOrder[] sortOrders, Field[] fields) {
		DataGenre sortType = DataGenre.STRING;
		if(fields != null && fields.length == 1) {
			// get any defaults
			if(displayFormat == null)
				displayFormat = fields[0].getFormat();
			sortType = getSortType(fields[0]);
			switch(pillarType) {
				case LINK:
					/*
					 * if(actionFormat == null) { actionFormat =
					 * "message:javascript: handleSelection(" +
					 * fields[0].getId() + ", \"{,prepare,script}\")";
					 * if(helpFormat == null) helpFormat =
					 * "simple.text.LiteralFormat:Click to filter by this value"
					 * ; }
					 */
					if(label == null)
						label = fields[0].getLabel();
					break;
				case NOTE:
					if(actionFormat == null)
						actionFormat = "message:#note" + (pillars.size() + 1);
					if(helpFormat == null && description != null)
						helpFormat = "simple.text.LiteralFormat:" + description;
					break;

			}

		}
		// call addPillar();
		addPillar(pillarType, groupingLevel, label, description, styleFormat, displayFormat, actionFormat, helpFormat, sortFormat, sortType, sortOrders, fields, null, null);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#addBlankPillar()
	 */
	public void addBlankPillar() {
		newPillar();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#addPillar(int, int,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * int[], simple.falcon.engine.standard.Field[], int[])
	 */
	public void addPillar(Pillar.PillarType pillarType, int groupingLevel, String labelFormat, String description, String styleFormat, String displayFormat, String actionFormat, String helpFormat,
			String sortFormat, DataGenre sortType, SortOrder[] sortOrders, Field[] fields, Results.Aggregate[] aggregateTypes, int[] sortIndexes) {
		// add to pillars
		InnerStandardPillar pillar = newPillar();
		pillar.setGroupingLevel(groupingLevel);
		if(actionFormat != null)
			pillar.setActionFormat(ConvertUtils.getFormat(actionFormat));
		pillar.setDescription(description);
		if(displayFormat != null)
			pillar.setDisplayFormat(ConvertUtils.getFormat(displayFormat));
		if(helpFormat != null)
			pillar.setHelpFormat(ConvertUtils.getFormat(helpFormat));
		if(labelFormat != null)
			pillar.setLabelFormat(ConvertUtils.getFormat(labelFormat));
		pillar.setPillarType(pillarType);
		if(sortFormat != null)
			pillar.setSortFormat(ConvertUtils.getFormat(sortFormat));
		pillar.setSortType(sortType);
		if(styleFormat != null)
			pillar.setStyleFormat(ConvertUtils.getFormat(styleFormat));

		// add to fieldorders
		pillar.replaceFieldOrders(fields, sortOrders, aggregateTypes, sortIndexes);
	}

	protected InnerStandardPillar createPillar() {
		return new InnerStandardPillar();
	}

	public InnerStandardPillar newPillar() {
		InnerStandardPillar pillar = createPillar();
		lock.lock();
		try {
			pillar.index = pillars.size() + 1;
			pillars.add(pillar);
			updated = true;
		} finally {
			lock.unlock();
		}
		return pillar;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#addFieldPillar(int,
	 * simple.falcon.engine.standard.Field, int)
	 */
	public void addFieldPillar(int groupingLevel, Field field, SortOrder sortOrder) {
		// addLinkPillar(groupingLevel, "message:javascript: handleSelection(" +
		// field.getId() + ", \"{,prepare,script}\")",
		// "simple.text.LiteralFormat:Click to filter by this value", field,
		// sortOrder);
		addLinkPillar(groupingLevel, null, null, field, sortOrder);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#addNotePillar(java.lang.String,
	 * simple.falcon.engine.standard.Field, int)
	 */
	public void addNotePillar(String note, Field field, SortOrder sortOrder) {
		int index = pillars.size() + 1;
		addPillar(Pillar.PillarType.NOTE, GROUPING_LEVEL_ZERO, null, note, null, field.getFormat(), "message:#note" + index, "simple.text.LiteralFormat:" + note, null, getSortType(field),
				new SortOrder[] { sortOrder }, new Field[] { field }, new Results.Aggregate[] { Results.Aggregate.COUNT }, null);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#removePillar(int)
	 */
	public void removePillar(int pillarIndex) {
		lock.lock();
		try {
			pillars.remove(pillarIndex - 1);
			for(int i = pillarIndex; i <= pillars.size(); i++) {
				pillars.get(i - 1).index = i;
			}
			recalcFieldOrders();
			updated = true;
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#movePillar(int, int)
	 */
	public void movePillar(int oldPillarIndex, int newPillarIndex) {
		if(oldPillarIndex != newPillarIndex) {
			lock.lock();
			try {
				InnerStandardPillar p = pillars.remove(oldPillarIndex - 1);
				while(pillars.size() < newPillarIndex - 1)
					newPillar();
				pillars.add(newPillarIndex - 1, p);
				for(int i = Math.min(oldPillarIndex, newPillarIndex); i <= Math.max(oldPillarIndex, newPillarIndex); i++) {
					pillars.get(i - 1).index = i;
				}
				recalcFieldOrders();
				updated = true;
			} finally {
				lock.unlock();
			}
		}
	}

	protected SortOrder getDefaultSortOrder(Field field) {
		switch(field.getSortSqlType().getTypeCode()) {
			case Types.BIGINT:
			case Types.BIT:
			case Types.DECIMAL:
			case Types.DOUBLE:
			case Types.FLOAT:
			case Types.INTEGER:
			case Types.NUMERIC:
			case Types.REAL:
			case Types.SMALLINT:
			case Types.TINYINT:
				return SortOrder.DESC;
			case Types.TIME:
			case Types.DATE:
			case Types.TIMESTAMP:
				return SortOrder.DESC;
			default:
				return SortOrder.ASC;
		}
	}

	protected static DataGenre getSortType(Field field) {
		switch(field.getSortSqlType().getTypeCode()) {
			case java.sql.Types.BIGINT:
			case java.sql.Types.BIT:
			case java.sql.Types.DECIMAL:
			case java.sql.Types.DOUBLE:
			case java.sql.Types.FLOAT:
			case java.sql.Types.INTEGER:
			case java.sql.Types.NUMERIC:
			case java.sql.Types.REAL:
			case java.sql.Types.SMALLINT:
			case java.sql.Types.TINYINT:
				return DataGenre.NUMBER;
			case java.sql.Types.DATE:
			case java.sql.Types.TIME:
			case java.sql.Types.TIMESTAMP:
				return DataGenre.DATE;
			default:
				return DataGenre.STRING;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getFieldOrders()
	 */
	public FieldOrder[] getFieldOrders() {
		lock.lock();
		try {
			if(fieldOrders == null) {
				SortedMap<Integer, List<FieldOrder>> buckets = new TreeMap<Integer, List<FieldOrder>>(Collections.reverseOrder()); // so
																																	// that
																																	// the
																																	// highest
																																	// grouping
																																	// level
																																	// is
																																	// first
				Set<Field> fields = new HashSet<Field>();
				int cnt = 0;
				for(int i = 0; i < pillars.size(); i++) {
					Pillar p = pillars.get(i);
					Integer key;
					if(p.getGroupingLevel() > GROUPING_LEVEL_ZERO)
						key = new Integer(p.getGroupingLevel());
					else
						key = new Integer(GROUPING_LEVEL_ZERO);
					List<FieldOrder> list = buckets.get(key);
					if(list == null) {
						list = new ArrayList<FieldOrder>();
						buckets.put(key, list);
					}
					FieldOrder[] fos = p.getFieldOrders();
					if(fos != null)
						for(int k = 0; k < fos.length; k++) {
							if(fields.add(fos[k].getField())) {
								list.add(fos[k]);
								cnt++;
							}
						}
				}
				fieldOrders = new FieldOrder[cnt];
				cnt = 0;
				for(Iterator<List<FieldOrder>> iter = buckets.values().iterator(); iter.hasNext();) {
					List<FieldOrder> list = iter.next();
					for(int i = 0; i < list.size(); i++) {
						fieldOrders[cnt++] = list.get(i);
					}
				}
			}
			return fieldOrders;
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getPillars()
	 */
	public Pillar[] getPillars() {
		return pillars.toArray(new Pillar[pillars.size()]);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof StandardFolio))
			return false;
		StandardFolio f = (StandardFolio) o;
		return ConvertUtils.areEqual(query, f.query)
			&& Arrays.equals(getPillars(), f.getPillars())
			&& filter!=null&&filter.equals(f.filter)
			&& directives.equals(f.directives)
			&& ConvertUtils.areEqual(chartType, f.chartType)
			&& ConvertUtils.areEqual(title, f.title)
			&& ConvertUtils.areEqual(subtitle, f.subtitle)
			&& maxRowsPerGroup.equals(f.maxRowsPerGroup);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#hashCode()
	 */
	@Override
	public int hashCode() {
		int hc = 0;
		hc = SystemUtils.addHashCode(hc, getPillars());
		hc = SystemUtils.addHashCode(hc, filter);
		hc = SystemUtils.addHashCode(hc, query);
		return hc;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getFolioId()
	 */
	public Long getFolioId() {
		return folioId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#setFolioId(java.lang.Long)
	 */
	public void setFolioId(Long folioId) {
		this.folioId = folioId;
		updated = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getOutputType()
	 */
	public OutputType getOutputType() {
		return outputType;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * simple.falcon.engine.standard.FolioI#setOutputType(simple.falcon.engine
	 * .standard.OutputType)
	 */
	public void setOutputType(OutputType outputType) {
		this.outputType = outputType;
		updated = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#getName()
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see simple.falcon.engine.standard.FolioI#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
		updated = true;
	}

	public void recalcFieldOrders() {
		fieldOrders = null;
	}

	/**
	 * @return Returns the chartType.
	 */
	public String getChartType() {
		return chartType;
	}

	/**
	 * @param chartType
	 *            The chartType to set.
	 */
	public void setChartType(String chartType) {
		this.chartType = chartType;
		updated = true;
	}

	/*
	 * public void setPillars(Pillar[] pillars) { this.pillars.clear();
	 * for(Pillar p : pillars){ this.pillars.add(p); } recalcFieldOrders(); }
	 */
	public int getMaxRowsForLevel(int groupLevel) {
		Integer rows = maxRowsPerGroup.get(groupLevel);
		return rows == null ? -1 : rows;
	}

	public void setMaxRowsForLevel(int groupLevel, int maxRows) {
		maxRowsPerGroup.put(groupLevel, maxRows);
		updated = true;
	}

	public int getMaxRowsPerSection() {
		return getMaxRowsForLevel(0);
	}

	public void setMaxRowsPerSection(int maxRows) {
		setMaxRowsForLevel(0, maxRows);
	}

	public int getMaxRows() {
		return getMaxRowsForLevel(-1);
	}

	public void setMaxRows(int maxRows) {
		setMaxRowsForLevel(-1, maxRows);
	}

	public String getDirective(String name) {
		return directives.get(name);
	}

	public void setDirective(String name, String directive) {
		directives.put(name, directive);
		updated = true;
	}

	public Set<String> getDirectiveNames() {
		return directives.keySet();
	}

	public void removePillar(Pillar pillar) {
		removePillar(pillar.getIndex());
	}

	public ColumnNamer getColumnNamer() {
		return designEngine.getColumnNamer();
	}

	public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException {
		Map<String, String> atts = new LinkedHashMap<String, String>();
		atts.put("folioId", ConvertUtils.getStringSafely(getFolioId()));
		atts.put("name", getName());
		atts.put("title", ConvertUtils.getFormatString(getTitle()));
		atts.put("subtitle", ConvertUtils.getFormatString(getSubtitle()));
		atts.put("chartType", getChartType());
		atts.put("maxRowsPerSection", ConvertUtils.getStringSafely(getMaxRowsPerSection()));
		atts.put("maxRows", ConvertUtils.getStringSafely(getMaxRows()));
		if(!StringUtils.isBlank(getQuery())){
			atts.put("query", ConvertUtils.getStringSafely(getQuery()));
		}
		if(!StringUtils.isBlank(getDataSourceName())){
			atts.put("dataSourceName", ConvertUtils.getStringSafely(getDataSourceName()));
		}
		if(tagName == null)
			tagName = "folio";
		builder.elementStart(tagName, atts);
		if(getOutputType() != null) {
			builder.put("outputType", getOutputType());
		}
		if(directives.size() > 0) {
			builder.elementStart("directives");
			for(Map.Entry<String, String> entry : directives.entrySet()) {
				builder.elementStart("directive", new String[] { "name", "value" }, entry.getKey(), entry.getValue());
				builder.elementEnd("directive");
			}
			builder.elementEnd("directives");
		}

		if(filter != null) {
			filter.writeXML(null, builder);
		}
		if(pillars.size() > 0) {
			builder.elementStart("pillars");
			for(int i = 0; i < pillars.size(); i++) {
				Pillar p = pillars.get(i);
				p.writeXML("pillar", builder);
			}
			builder.elementEnd("pillars");
		}
		builder.elementEnd(tagName);

	}

	public void readXML(ObjectBuilder builder) throws DesignException {
		if(!StringUtils.isBlank(builder.getAttrValue("folioId"))) {
			setFolioId(builder.get("folioId", long.class));
		}
		setName(builder.getAttrValue("name"));
		if(!StringUtils.isBlank(builder.getAttrValue("title")))
			setTitle(ConvertUtils.getFormat(builder.getAttrValue("title")));
		if(!StringUtils.isBlank(builder.getAttrValue("subtitle")))
			setSubtitle(ConvertUtils.getFormat(builder.getAttrValue("subtitle")));
		if(!StringUtils.isBlank(builder.getAttrValue("chartType")))
			setChartType(builder.getAttrValue("chartType"));
		if(!StringUtils.isBlank(builder.getAttrValue("maxRowsPerSection")))
			setMaxRowsPerSection(builder.get("maxRowsPerSection", int.class));
		if(!StringUtils.isBlank(builder.getAttrValue("maxRows")))
			setMaxRows(builder.get("maxRows", int.class));
		if(!StringUtils.isBlank(builder.getAttrValue("query")))
			setQuery(builder.getAttrValue("query"));
		if(!StringUtils.isBlank(builder.getAttrValue("dataSourceName")))
			setDataSourceName(builder.getAttrValue("dataSourceName"));
		ObjectBuilder[] directives = builder.getSubNodes("directives");
		if(directives != null && directives.length > 0) {
			ObjectBuilder[] directive = directives[0].getSubNodes("directive");
			if(directive != null) {
				for(int i = 0; i < directive.length; i++) {
					setDirective(directive[i].getAttrValue("name"), directive[i].getAttrValue("value"));
				}
			}
		}
		ObjectBuilder[] outputTypeArray = builder.getSubNodes("outputType");
		if(outputTypeArray != null && outputTypeArray.length > 0) {
			Class<? extends OutputType> clazz;
			try {
				clazz = Class.forName(outputTypeArray[0].getAttrValue("class")).asSubclass(OutputType.class);
			} catch(ClassNotFoundException e) {
				throw new DesignException(e);
			}
			OutputType outputTypeObj;
			try {
				outputTypeObj = outputTypeArray[0].read(clazz);
			} catch(ConvertException e) {
				throw new DesignException(e);
			}
			setOutputType(outputTypeObj);
		}
		ObjectBuilder[] filterGroups = builder.getSubNodes("filterGroup");
		if(filterGroups != null && filterGroups.length > 0) {
			StandardFilterGroup fg = new StandardFilterGroup(designEngine);
			fg.readXML(filterGroups[0]);
			setFilter(fg);
		} else {
			ObjectBuilder[] filters = builder.getSubNodes("filter");
			if(filters != null && filters.length > 0) {
				StandardFilterItem fi = new StandardFilterItem(designEngine);
				fi.readXML(filters[0]);
				setFilter(fi);
			}
		}
		ObjectBuilder[] pillars = builder.getSubNodes("pillars");
		if(pillars != null && pillars.length > 0) {
			ObjectBuilder[] pillar = pillars[0].getSubNodes("pillar");
			for(int i = 0; i < pillar.length; i++) {
				Pillar p = newPillar();
				p.readXML(pillar[i]);
			}

		}
		updated = false;
	}

	public boolean isUpdated() {
		return updated;
	}
	
	public Filter getFilter() {
		return filter;
	}
	
	public void setQuery(String query){
		this.query = query;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

}
