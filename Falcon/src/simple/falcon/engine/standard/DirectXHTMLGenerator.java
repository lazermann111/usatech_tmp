package simple.falcon.engine.standard;

import java.awt.Dimension;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import javax.activation.DataSource;
import javax.xml.parsers.ParserConfigurationException;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.w3c.dom.Node;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.SAXException;

import com.lowagie.text.DocumentException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.chart.ChartCreationException;
import simple.chart.ChartFactory;
import simple.chart.ChartMaker;
import simple.chart.ChartType;
import simple.chart.ChartType.Axis;
import simple.chart.ChartUtils;
import simple.chart.GenericChartType.GenericAxis;
import simple.event.LogTimingTaskListener;
import simple.event.NadaTaskListener;
import simple.event.TaskListener;
import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Generator;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Pillar.PillarType;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsValueRetriever;
import simple.falcon.engine.Scene;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.xml.DisplayType;
import simple.io.Base64EncodingOutputStream;
import simple.io.BufferStreamDataSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.QPEncoderWriter;
import simple.results.DataGenre;
import simple.results.MultiColumnResultsReader;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.results.ResultsFilter;
import simple.results.ResultsReader;
import simple.results.SingleColumnResultsReader;
import simple.servlet.RequestInfoUtils;
import simple.servlet.RequestUtils;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.LiteralFormat;
import simple.text.MessageFormat;
import simple.text.MessageFormat.FormatObject;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.translator.Translator;
import simple.util.CaseInsensitiveComparator;
import simple.util.CollectionUtils;
import simple.util.SparseBuckets;
import simple.util.StaticCollection;
import simple.xml.AppendableAttributes;
import simple.xml.Serializer;
import simple.xml.dom.SAX2DOM;
import simple.xml.serializer.XHTMLHandler;

/*
 * Taken from show-report-base.xml v.1.50 (04/23/2012) and ReportAdapter v.1.79 (05/18/2012)
 */

public class DirectXHTMLGenerator implements Generator {
	private static final Log log = Log.getLog();
	protected static final String NBSP = "\u00A0";
	protected static final String ZWNJ = "\u200C";
	protected static final String SP = " ";
	protected static final String UN = "_";
	protected static final String NL = "\n";
	protected static final String CRLF = "\r\n";
	protected static final AtomicInteger boundaryUIDGenerator = new AtomicInteger();
	protected static final String configEntryRE = "(?:(?:(?:\\s*\"[^\"]*(?:\"{2}[^\"]*)*\"\\s*)|(?:\\s*\\'[^\\']*(?:\\'{2}[^\\']*)*\\'\\s*)|[^\\=\\;]*)=(?:(?:\\s*\"[^\"]*(?:\"{2}[^\"]*)*\"\\s*)|(?:\\s*\\'[^\\']*(?:\\'{2}[^\\']*)*\\'\\s*)|[^\\=\\;]*)(?:$|\\;))*";
	protected static final String chartTrueRE = "(?:(?:(?:\\s*\"chart\"\\s*)|(?:\\s*\\'chart\\'\\s*)|chart)=(?:(?:\\s*\"true\"\\s*)|(?:\\s*\\'true\\'\\s*)|true)(?:$|\\;))";
	protected static final Pattern chartConfigPattern = Pattern.compile("[^:]*:" + configEntryRE + chartTrueRE + configEntryRE);
	protected static final int folioRowEquiv = 2;
	protected static final int groupRowEquiv = 1;
	protected static TaskListener taskListener;
	protected static boolean timingLogged = false;
	// protected final CssManager cssManager = new CssManager(true);
	protected class MHtmlSerializer extends XHTMLHandler {
		protected Writer rawWriter;
		protected final String boundary = getUniqueBoundaryValue();
		protected final List<DataSource> mimeParts = new ArrayList<DataSource>(3);
		protected String mainPartEncoding = "quoted-printable";
		protected String subPartEncoding = "base64";
		protected GenerateInfo info;

		@Override
		public void setWriter(Writer writer) {
			this.rawWriter = writer;
			super.setWriter(new QPEncoderWriter(writer));
		}
		@Override
		public void endDocument() throws SAXException {
			try {
				write(newline);
				flushBuffer();
				for(DataSource part : mimeParts) {
					rawWriter.write(CRLF);
					rawWriter.write("--");
					rawWriter.write(boundary);
					rawWriter.write(CRLF);
					rawWriter.write("Content-Type: ");
					rawWriter.write(part.getContentType());
					rawWriter.write(CRLF);
					String partEncoding = "base64";
					rawWriter.write("Content-Transfer-Encoding: ");
					rawWriter.write(partEncoding);
					rawWriter.write(CRLF);
					rawWriter.write("Content-ID: <");
					rawWriter.write(part.getName());
					rawWriter.write('>');
					rawWriter.write(CRLF);
					rawWriter.write(CRLF);
					Base64EncodingOutputStream out = new Base64EncodingOutputStream(rawWriter, CRLF);
					IOUtils.copy(part.getInputStream(), out);
					out.finish(); // must finish() and not just flush() because last '=' are not written in Base64EncodingOutputStream until finish().
				}
				rawWriter.write(CRLF);
				rawWriter.write("--");
				rawWriter.write(boundary);
				rawWriter.write("--");
				rawWriter.write(CRLF);
			} catch(IOException e) {
				throw new SAXException(e);
			}
			super.endDocument();
		}

		@Override
		public void startDocument() throws SAXException {
			try {
				rawWriter.write("From: \"USALive Reports\"");
				rawWriter.write(CRLF);
				rawWriter.write("Subject: ");
				String title = getTitle(info);
				String subtitle = getSubtitle(info);
				if(!StringUtils.isBlank(title)) {
					rawWriter.write(title);
					if(!StringUtils.isBlank(subtitle))
						rawWriter.write(" - ");
				}
				if(!StringUtils.isBlank(subtitle))
					rawWriter.write(subtitle);

				rawWriter.write(CRLF);
				rawWriter.write("Date: ");
				rawWriter.write(new Date().toString());
				rawWriter.write(CRLF);
				rawWriter.write("MIME-Version: 1.0");
				rawWriter.write(CRLF);
				rawWriter.write("Content-Type: multipart/related;");
				rawWriter.write(CRLF);
				rawWriter.write("\ttype=\"text/html\";");
				rawWriter.write(CRLF);
				rawWriter.write("\tboundary=\"");
				rawWriter.write(boundary);
				rawWriter.write("\"");
				rawWriter.write(CRLF);
				rawWriter.write(CRLF);
				rawWriter.write("Multi-part report in MIME format");
				rawWriter.write(CRLF);
				rawWriter.write(CRLF);
				rawWriter.write("--");
				rawWriter.write(boundary);
				rawWriter.write(CRLF);
				rawWriter.write("Content-Type: ");
				rawWriter.write(getMediaType());
				rawWriter.write(";");
				rawWriter.write(CRLF);
				rawWriter.write("\tcharset=\"");
				rawWriter.write(getEncoding());
				rawWriter.write("\"");
				rawWriter.write(CRLF);
				rawWriter.write("Content-Transfer-Encoding: ");
				rawWriter.write(mainPartEncoding);
				rawWriter.write(CRLF);
				rawWriter.write(CRLF);
			} catch(IOException e) {
				throw new SAXException(e);
			}
			super.startDocument();
		}
	};

	protected static class PillarData implements Comparable<PillarData> {
		protected String action;
		public final String sort;
		public final String help;
		public final String display;
		public final String style;
		public final String percent;
		public final PillarInfo pillarInfo;
		public final XTabData xtab;
		public final String label;
		public PillarData note;

		public PillarData(GenerateInfo info, FolioInfo folioInfo, PillarInfo pillarInfo, Results results, int level, XTabData xtab) throws SAXException {
			this.pillarInfo = pillarInfo;
			int groupColumnIndex = folioInfo.getLastGroupColumnIndex(level);
			Object displayData = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, xtab, groupColumnIndex, info.parameters);
			Object sortData = pillarInfo.pillar.getSortValueRetriever().getAggregate(results, xtab, groupColumnIndex, info.parameters);
			if(pillarInfo.pillar.getPercentFormat() != null) {
				Object percentData;
				if(xtab != null) {
					Object prevDisplayData = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, groupColumnIndex, info.parameters);
					try {
						percentData = getPercentData(displayData, prevDisplayData, pillarInfo.pillar.getPercentFormat());
					} catch(ConvertException e) {
						throw new SAXException(e);
					}
				} else if(level != 0) {
					Object prevDisplayData = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, folioInfo.getLastGroupColumnIndex(level + 1), info.parameters);
					try {
						percentData = getPercentData(displayData, prevDisplayData, pillarInfo.pillar.getPercentFormat());
					} catch(ConvertException e) {
						throw new SAXException(e);
					}
				} else { // when level is zero it is always 100%
					if(displayData instanceof MultiValueWithContext) {
						int size = ((MultiValueWithContext) displayData).getValueSize();
						Object[] percentDataArray = new Object[size];
						for(int i = 0; i < size; i++) {
							percentDataArray[i] = new BigDecimal(1.00);
						}
						percentData = percentDataArray;
					} else {
						percentData = new BigDecimal(1.00);
					}
				}
				this.percent = ConvertUtils.formatObject(percentData, pillarInfo.pillar.getPercentFormat());
			} else
				this.percent = null;
			this.display = ConvertUtils.formatObject(displayData, pillarInfo.pillar.getDisplayFormat());
			if(pillarInfo.pillar.getActionFormat() != null)
				this.action = adjustAction(ConvertUtils.formatObject(displayData, pillarInfo.pillar.getActionFormat()), info.scene);
			else
				this.action = null;
			if(pillarInfo.pillar.getSortType() == DataGenre.DATE) {
				String tmp;
				try {
					tmp = getDateSortText(sortData, pillarInfo.pillar.getSortFormat());
				} catch(ConvertException e) {
					tmp = null;
					log.warn("Could not convert sort text to number of milleseconds for date sorting", e);
				}
				this.sort = tmp;
			} else
				this.sort = ConvertUtils.formatObject(sortData, pillarInfo.pillar.getSortFormat());
			if(pillarInfo.pillar.getHelpFormat() != null)
				this.help = ConvertUtils.formatObject(displayData, pillarInfo.pillar.getHelpFormat());
			else
				this.help = null;
			if(pillarInfo.pillar.getStyleFormat() != null)
				this.style = ConvertUtils.formatObject(displayData, pillarInfo.pillar.getStyleFormat());
			else
				this.style = null;
			this.xtab = xtab;
			if(pillarInfo.notePillarInfo != null) {
				note = new PillarData(info, folioInfo, pillarInfo.notePillarInfo, results, level, xtab);
				if(!StringUtils.isBlank(note.display)) {
					folioInfo.noteValues.add(new Note(note));
					int chars = note.display.length();
					if(chars > pillarInfo.notePillarInfo.maxChars)
						pillarInfo.notePillarInfo.maxChars = chars;
				}
			}
			// Handle Dynamic Labels
			if(folioInfo.dynamiclabels && pillarInfo.pillar.getLabelFormat() != null && !(pillarInfo.pillar.getLabelFormat() instanceof LiteralFormat))
				label = ConvertUtils.formatObject(displayData, pillarInfo.pillar.getLabelFormat());
			else
				label = pillarInfo.label;
			initialize(info, displayData, sortData, groupColumnIndex);
		}

		protected void initialize(GenerateInfo info, Object displayData, Object sortData, int groupColumnIndex) {
		}

		public int compareTo(PillarData o) {
			if(o == null)
				return 1;
			if(pillarInfo.pillarIndex < o.pillarInfo.pillarIndex)
				return -1;
			if(pillarInfo.pillarIndex > o.pillarInfo.pillarIndex)
				return 1;
			return comparePairs(sort, o.sort, display, o.display, action, o.action, help, o.help, style, o.style);
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}
	}

	protected static class XTabData extends PillarData implements ResultsFilter {
		protected Object displayData;
		protected Object sortData;
		protected int groupColumnIndex;
		protected Map<String, Object> parameters;

		public XTabData(GenerateInfo info, FolioInfo folioInfo, PillarInfo pillarInfo, Results results, int level) throws SAXException {
			super(info, folioInfo, pillarInfo, results, level, null);
		}

		public boolean isValidRow(int row, Object[] data, Results results) {
			Object currentDisplayData = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, groupColumnIndex, parameters);
			Object currentSortData = pillarInfo.pillar.getSortValueRetriever().getAggregate(results, groupColumnIndex, parameters);
			return CollectionUtils.deepEquals(currentDisplayData, displayData) && CollectionUtils.deepEquals(currentSortData, sortData);
		}

		public void register(Results results, List<PillarInfo> groupZero) {
			// register filter with results
			for(PillarInfo pi : groupZero) {
				pi.pillar.getDisplayValueRetriever().trackAggregates(results, this);
				pi.pillar.getSortValueRetriever().trackAggregates(results, this);
			}
		}
		
		protected void initialize(GenerateInfo info, Object displayData, Object sortData, int groupColumnIndex) {
			this.displayData = displayData;
			this.sortData = sortData;
			this.groupColumnIndex = groupColumnIndex;
			this.parameters = info.parameters;
		}
	}

	protected static class Note implements Comparable<Note> {
		public final String marker;
		public final String description;
		public final String style;
		public final String colId;

		public Note(PillarData data) {
			this(data.display, data.help, data.style, String.valueOf(data.pillarInfo.pillarIndex));
		}

		public Note(String marker, String description, String style, String colId) {
			super();
			this.marker = marker;
			this.description = description;
			this.style = style;
			this.colId = colId;
		}

		public int compareTo(Note o) {
			if(o == null)
				return 1;
			int i = compare(marker, o.marker);
			if(i != 0)
				return i;
			i = compare(description, o.description);
			if(i != 0)
				return i;
			return compare(style, o.style);
		}
	}

	protected static class Image implements Comparable<Image> {
		public final String src;
		public final String description;

		public Image(PillarData data) {
			this(data.display, data.help);
		}

		public Image(String src, String description) {
			super();
			this.src = src;
			this.description = description;
		}

		public int compareTo(Image o) {
			if(o == null)
				return 1;
			int i = compare(src, o.src);
			if(i != 0)
				return i;
			return compare(description, o.description);
		}
	}

	protected static int comparePairs(String... pairs) {
		if(pairs == null)
			return 0;
		if(pairs.length % 2 != 0)
			throw new IllegalArgumentException("Number of arguments must be even");
		int c;
		for(int i = 0; i < pairs.length - 1; i += 2) {
			c = compare(pairs[i], pairs[i + 1]);
			if(c != 0)
				return c;
		}
		return 0;
	}

	protected static int compare(String s1, String s2) {
		if(s1 == null) {
			if(s2 == null)
				return 0;
			return -1;
		}
		if(s2 == null)
			return 1;
		int i = s1.compareToIgnoreCase(s2);
		if(i != 0)
			return i;
		return s1.compareTo(s2);
	}

	protected static enum TargetType {
		HTML("HTML", "html", false, "text/html", "text/xhtml", "application/xhtml+xml"),
		PDF("PDF", "pdf", false, "application/pdf"), 
		XLS("Excel", "excel", false, "application/xls", "application/vnd.ms-excel"), 
		DOC("Word Document", "doc", false, "application/msword"),	
		PPT("PowerPoint", "ppt", false, "application/vnd.ms-powerpoint"),
		CSV("Comma-separated Values", "csv", false, "text/csv"),
		MHTML("IE7 HTML", "mhtml", false, "multipart/related; type=\"text/html\""),
		CHART_HTML("HTML", "html", true, "text/html", "text/xhtml", "application/xhtml+xml"),
		CHART_PDF("PDF", "pdf", true, "application/pdf"), 
		CHART_XLS("Excel", "excel", true, "application/xls", "application/vnd.ms-excel"),
		CHART_DOC("Word Document", "doc", true, "application/msword"),
		CHART_PPT("PowerPoint", "ppt", false, "application/vnd.ms-powerpoint"),
		CHART_MHTML("IE7 HTML", "mhtml", true, "multipart/related; type=\"text/html\""),
		;
		private final String primaryContentType;
		private final SortedSet<String> allContentTypes;
		private final boolean chart;
		private final String description;
		private final String codeSuffix;

		private TargetType(String description, String codeSuffix, boolean chart, String... contentTypes) {
			this.description = description;
			this.codeSuffix = codeSuffix;
			this.chart = chart;
			if(contentTypes == null || contentTypes.length == 0) {
				primaryContentType = null;
				allContentTypes = Collections.unmodifiableSortedSet(new TreeSet<String>());
			} else {
				primaryContentType = contentTypes[0];
				SortedSet<String> tmp = new TreeSet<String>(new CaseInsensitiveComparator<String>());
				for(String contentType : contentTypes)
					tmp.add(contentType);
				allContentTypes = Collections.unmodifiableSortedSet(tmp);
			}
		}

		public String getPrimaryContentType() {
			return primaryContentType;
		}

		public SortedSet<String> getAllContentTypes() {
			return allContentTypes;
		}

		public boolean isChart() {
			return chart;
		}

		protected static final Map<String, TargetType> nonChartTargetTypeMapping = new HashMap<String, TargetType>();
		protected static final Map<String, TargetType> chartTargetTypeMapping = new HashMap<String, TargetType>();
		static {
			for(TargetType targetType : values())
				registerTargetType(targetType);
		}
		protected static void registerTargetType(TargetType targetType) {
			Map<String, TargetType> mapping = (targetType.isChart() ? chartTargetTypeMapping : nonChartTargetTypeMapping);
			for(String contentType : targetType.getAllContentTypes())
				mapping.put(contentType, targetType);
		}

		public static TargetType getTargetType(boolean chart, String contentType) {
			return (chart ? chartTargetTypeMapping : nonChartTargetTypeMapping).get(contentType);
		}

		public String getDescription() {
			return description;
		}

		public String getCodeSuffix() {
			return codeSuffix;
		}
	}

	public static final String DEFAULT_CHART_TYPE = "BarChart3D";

	protected static class ChartInfo {
		public ChartMaker chartMaker;
		public DisplayType displayType;
		public Map<String, Pillar> dynamicLabelClusters;
		public int chartNumber = 1;
	}

	protected class GenerateInfo {
		public final Report report;
		public final Results[] results;
		public final Map<String, Object> parameters;
		public final ColumnNamer namer;
		public final Scene scene;
		public final OutputType outputType;
		public final Executor executor;
		public final TargetType targetType;
		public final Serializer serializer;
		public final Collection<TargetType> altTargetTypes;
		public int pageIndex = 0;
		public final Output output;
		public final boolean isPagable;
		public final FolioInfo[] folioInfos;
		public final DisplayType displayType;
		public int totalPageCount = 1;
		public int maxRowsPerPage;
		public int paginationCount = 0;
		public int reportHeaderSize = 0;
		
		public Output getOutput() {
			return output;
		}

		public GenerateInfo(Report report, Results[] results, Map<String, Object> parameters, ColumnNamer namer, Scene scene, Executor executor, OutputType outputType, TargetType targetType, Serializer serializer, Collection<TargetType> altTargetTypes, Output output) {
			this.report = report;
			this.results = results;
			this.parameters = parameters;
			this.namer = namer;
			this.scene = scene;
			this.executor = executor;
			this.outputType = outputType;
			this.targetType = targetType;
			this.serializer = serializer;
			this.altTargetTypes = altTargetTypes;
			this.output=output;
			if(output instanceof PagableOutput && scene.isLive() && RequestInfoUtils.isDirectlyRendered(outputType.getContentType()) && !targetType.isChart()) {
				this.isPagable=true;
				log.info("isPagable=true");
			}else{
				this.isPagable=false;
			}
			folioInfos = new FolioInfo[report.getFolios() != null ? report.getFolios().length : 0];
			for(int i = 0; i < folioInfos.length; i++)
				folioInfos[i] = new FolioInfo(report.getFolios()[i], results[i], i + 1);
			if(targetType.isChart())
				this.displayType = getDisplayType(parameters, scene, targetType);
			else
				this.displayType = null;
		}
	}

	protected class FolioInfo {
		public final Set<Image> imageValues = new TreeSet<Image>();
		public final SortedSet<Note> noteValues = new TreeSet<Note>();
		public final SparseBuckets<PillarInfo> groups = new SparseBuckets<PillarInfo>();
		public final SortedMap<Integer, Integer> groupColumnIndexs = new TreeMap<Integer, Integer>();
		public final List<String> groupOneColumnNames = new ArrayList<String>();
		// public final List<PillarInfo> groupTableTotals = new ArrayList<PillarInfo>();
		public int maxLevel = Folio.GROUPING_LEVEL_ZERO;
		public final Folio folio;
		public final Results results;
		public final int ordinal;
		public final String folioLabel;

		// ------------
		protected final List<PillarInfo> detailPillars = new ArrayList<PillarInfo>();
		protected final List<PillarInfo> imagePillars = new ArrayList<PillarInfo>();
		protected int tableColumnCount = 0;
		protected int levelZeroCount = 0;
		protected int firstTotalIndex = -1;
		protected int detailRowCount = 0;
		protected int tableCount = 0;
		protected boolean showLegend;
		protected List<PillarInfo> groupZero;
		protected List<PillarInfo> groupOne;
		protected SortedSet<XTabData> xtabData;
		protected int groupColumns;
		protected int summaryGroupCount;
		protected boolean dynamiclabels;
		protected Collection<Integer> noTotals;

		public FolioInfo(Folio folio, Results results, int ordinal) {
			this.folio = folio;
			this.results = results;
			this.ordinal = ordinal;
			this.folioLabel = (folio.getFolioId() == null ? "N" + ordinal : String.valueOf(folio.getFolioId()));
		}

		protected Object getSampleData(FieldOrder fo) {
			if(fo.getAggregateType() != null) {
				switch(fo.getAggregateType()) {
					case COUNT:
					case DISTINCT:
						return 1;
					case ARRAY:
					case SET:
						return new Object[] { getSampleData(fo.getField().getDisplaySqlType()) };
				}
			}
			return getSampleData(fo.getField().getDisplaySqlType());
		}

		protected Object getSampleData(SQLType sqlType) {
			return getSampleData(SQLTypeUtils.getJavaType(sqlType));
		}

		protected Object getSampleData(Class<?> cls) {
			if(Number.class.isAssignableFrom(cls))
				return ConvertUtils.convertSafely(cls, 1, null);
			if(Date.class.isAssignableFrom(cls))
				return new Date();
			if(cls.isArray())
				return new Object[] { getSampleData(cls.getComponentType()) };
			return ConvertUtils.convertSafely(cls, "-", null);
		}

		public void preprocess(GenerateInfo info) throws SAXException {
			showLegend = ConvertUtils.getBooleanSafely(folio.getDirective("show-legend"), true);
			dynamiclabels = ConvertUtils.getBooleanSafely(folio.getDirective("dynamic-labels"), false);
			groupColumns = ConvertUtils.getIntSafely(folio.getDirective("group-sections"), 2);
			if(groupColumns < 1)
				groupColumns = 2;
			final Pillar[] pillars = folio.getPillars(); // allcolumns
			PillarInfo lastPillarInfo = null;
			for(int i = 0; i < pillars.length; i++) {
				try {
					if(shouldProcessPillar(info, pillars[i])) {
						// organize pillars into groups
						int groupingLevel = pillars[i].getGroupingLevel();
						maxLevel = Math.max(maxLevel, groupingLevel);
						PillarInfo pillarInfo = new PillarInfo(pillars[i], i + 1);
						groups.add(groupingLevel, pillarInfo);
						final Object sample;
						if(dynamiclabels && pillars[i].getLabelFormat() != null && !(pillars[i].getLabelFormat() instanceof LiteralFormat)) {
							FieldOrder[] fos = pillars[i].getFieldOrders();
							switch(fos.length) {
								case 0:
									sample = null;
									break;
								case 1:
									sample = getSampleData(fos[0]);
									break;
								default:
									Object[] sampleArray = new Object[fos.length];
									sample = sampleArray;
									for(int k = 0; k < fos.length; k++)
										sampleArray[k] = getSampleData(fos[k]);
							}
						} else
							sample = null;
						pillarInfo.label = ConvertUtils.formatObject(sample, pillars[i].getLabelFormat());
						switch(pillars[i].getPillarType()) {
							case NOTE:
								if(lastPillarInfo != null)
									lastPillarInfo.notePillarInfo = pillarInfo;
								if(info.targetType == TargetType.XLS) {
									switch(groupingLevel) {
										case 0:
											levelZeroCount++;
											// fall-through
										case 2:
											tableColumnCount++;
									}
								}
								break;
							case IMAGE:
								if(showLegend)
									imagePillars.add(pillarInfo);
								// fall-through
							default:
								if(firstTotalIndex < 0 && groupingLevel <= 2 && (groupingLevel != 2 || pillars[i].isAggregated()))
									firstTotalIndex = tableColumnCount;
								switch(groupingLevel) {
									case 0:
										levelZeroCount++;
										// fall-through
									case 2:
										tableColumnCount++;
										// if(groupingLevel == 0 || pillars[i].isAggregated())
										// info.groupTableTotals.add(pillarInfo);

								}
								if(info.targetType == TargetType.XLS && pillars[i].getPercentFormat() != null) {
									tableColumnCount++;
									if(groupingLevel == 0)
										levelZeroCount++;
								}
								// fall-through
							case ROW:
								if(pillars[i].getGroupingLevel() < 3)
									detailPillars.add(pillarInfo);

						}
						lastPillarInfo = pillarInfo;
					}
				} catch(IllegalArgumentException e) {
					if(log.isDebugEnabled())
						log.debug("Excluding pillar " + (i + 1) + " from report because a field is not in the resultset");
				}

			}
			// Add Groups to Results by level and make sure there are no gaps in groupColumnNames
			Set<String> usedGCNs = new HashSet<String>();
			String last = null;
			summaryGroupCount = 0;
			for(int i = maxLevel; i >= Folio.GROUPING_LEVEL_ZERO; i--) {
				List<PillarInfo> group = groups.get(i);
				if(group != null) {
					if(i >= Folio.GROUPING_LEVEL_THREE && !group.isEmpty())
						summaryGroupCount++;
					for(PillarInfo pillarInfo : group) {
						ResultsValueRetriever displayRetreiver = pillarInfo.pillar.getDisplayValueRetriever();
						ResultsValueRetriever sortRetreiver = pillarInfo.pillar.getSortValueRetriever();
						List<String> sortGCNs = sortRetreiver.addGroups(results);
						List<String> displayGCNs = displayRetreiver.addGroups(results);
						for(String gcn : sortGCNs) {
							if(usedGCNs.add(gcn))
								last = gcn;
						}
						for(String gcn : displayGCNs) {
							if(usedGCNs.add(gcn))
								last = gcn;
						}
						displayRetreiver.trackAggregates(results);
						sortRetreiver.trackAggregates(results);
						if(i == Folio.GROUPING_LEVEL_ONE) {
							groupOneColumnNames.addAll(sortGCNs);
							groupOneColumnNames.addAll(displayGCNs);
						}
					}
				}
				groupColumnIndexs.put(i, results.getColumnIndex(last));
			}

			if(info.isPagable && info.maxRowsPerPage > 0) {
				if(info.paginationCount > 0 && info.paginationCount + folioRowEquiv + (groupRowEquiv * (!results.isGroupEnding(0) ? summaryGroupCount : 0)) >= info.maxRowsPerPage) {
					info.paginationCount = 0;
					info.totalPageCount++;
				}
				info.paginationCount += folioRowEquiv;
			}

			Integer ngt[];
			try {
				ngt = ConvertUtils.convert(Integer[].class, folio.getDirective("no-group-totals"));
			} catch(ConvertException e) {
				log.warn("Could not convert '" + folio.getDirective("no-group-totals") + "' to an array of numbers");
				ngt = null;
			}
			if(ngt != null)
				noTotals = StaticCollection.create(ngt);
			else
				noTotals = Collections.emptySet();
			groupZero = groups.get(Folio.GROUPING_LEVEL_ZERO);
			if(groupZero == null)
				groupZero = Collections.emptyList();
			groupOne = groups.get(Folio.GROUPING_LEVEL_ONE);
			if(groupOne == null)
				groupOne = Collections.emptyList();
			else if(groupZero.isEmpty() || noTotals.contains(1))
				groupOne.clear();

			if(!results.isGroupEnding(0)) {
				int groupOneColumnIndex = getLastGroupColumnIndex(1);
				int groupTwoColumnIndex = getLastGroupColumnIndex(2);
				int groupThreeColumnIndex = getLastGroupColumnIndex(3);

				xtabData = new TreeSet<XTabData>();
				// Looks like we need a double pass through the results - first to find all the cross-tab possibilities and to count rows, then to actually process
				while(results.next()) {
					if(!groupOne.isEmpty() && !groupZero.isEmpty() && !noTotals.contains(1) && results.isGroupEnding(groupOneColumnIndex)) {
						for(PillarInfo pi : groupOne) {
							XTabData xtab = new XTabData(info, this, pi, results, 1);
							if(xtabData.add(xtab))
								xtab.register(results, groupZero);
						}
					}
					if((results.getRow()) % 1000 == 0)
						log.info("Pre-processed " + results.getRow() + " rows and continuing");
					if(results.isGroupEnding(groupTwoColumnIndex)) {
						detailRowCount++;
						if(results.isGroupEnding(groupThreeColumnIndex))
							tableCount++;
						if(info.isPagable && info.maxRowsPerPage > 0) {
							info.paginationCount++;
							if(info.paginationCount >= info.maxRowsPerPage) {
								info.totalPageCount++;
								info.paginationCount = folioRowEquiv;
							}
							for(int i = Folio.GROUPING_LEVEL_THREE; i <= maxLevel; i++) {
								List<PillarInfo> group = groups.get(i);
								if(group != null && !group.isEmpty()) {
									if(results.isGroupEnding(getLastGroupColumnIndex(i)))
										info.paginationCount += groupRowEquiv;
									else
										break;
								}
							}
							
						}
					}
					for(PillarInfo pi : imagePillars) {
						if(results.isGroupEnding(getLastGroupColumnIndex(pi.pillar.getGroupingLevel()))) {
							PillarData imageData = new PillarData(info, this, pi, results, pi.pillar.getGroupingLevel(), null);
							imageValues.add(new Image(imageData));
						}
					}
					if(dynamiclabels && results.isGroupEnding(0)) {
						Iterator<PillarInfo> iter = detailPillars.iterator();
						while(iter.hasNext()) {
							PillarInfo pi = iter.next();
							if(pi.pillar.getPillarType() != PillarType.NOTE && pi.pillar.getPillarType() != PillarType.ROW && pi.pillar.getLabelFormat() != null && !(pi.pillar.getLabelFormat() instanceof LiteralFormat)) {
								Object displayData = pi.pillar.getDisplayValueRetriever().getAggregate(results, 0, info.parameters);
								String label = ConvertUtils.formatObject(displayData, pi.pillar.getLabelFormat());
								if(label == null) {
									// remove from various places so it's not output
									iter.remove();
									// info.groupTableTotals.remove(pi);
									groups.get(pi.pillar.getGroupingLevel()).remove(pi);
									tableColumnCount--;
									if(info.targetType == TargetType.XLS && pi.pillar.getPercentFormat() != null) {
										tableColumnCount--;
										if(pi.pillar.getGroupingLevel() == 0)
											levelZeroCount--;
									}
									if(pi.pillar.getGroupingLevel() == 0) {
										levelZeroCount--;
										Iterator<XTabData> xtabIter = xtabData.iterator();
										while(xtabIter.hasNext())
											if(xtabIter.next().pillarInfo.pillar == pi.pillar)
												xtabIter.remove();
									} else if(pi.pillar.getGroupingLevel() == 1) {
										if(groupOne.isEmpty())
											xtabData.clear();
									}
								} else
									pi.label = label;
							}
						}
					}
				}
				log.info("Pre-processed " + results.getRow() + " total rows");
			}
		}

		public void process(GenerateInfo info) throws SAXException {
			ChartInfo chartInfo;
			DisplayType displayType;
			if(info.targetType.isChart()) {
				chartInfo = new ChartInfo();
				prepareChart(info, chartInfo, folio);
				displayType = getDisplayType(info.parameters, info.scene, info.targetType);
			} else {
				chartInfo = null;
				displayType = null;
			}
			if(info.totalPageCount > 1 && info.maxRowsPerPage > 0) {
				if(info.paginationCount > 0 && info.paginationCount + folioRowEquiv + (groupRowEquiv * (!results.isGroupEnding(0) ? summaryGroupCount : 0)) >= info.maxRowsPerPage) {
					info.paginationCount = 0;
					nextPage(info);
				}
				info.paginationCount += folioRowEquiv;
			}
			generateFolioTitle(info, folio);

			results.setRow(0);
			if(!results.isGroupEnding(0)) {
				if(!imageValues.isEmpty()) {
					generateIconLegend(info, imageValues, folioLabel);
				}

				if(info.totalPageCount <= 1 || info.maxRowsPerPage <= 0)
					generateGroupCaption(info, folio, results, detailRowCount, tableCount);

				AppendableAttributes aa = attr().add("class", "folio").add("id", "folio_" + folioLabel);
				String tableWidth = folio.getDirective("table-width");
				if(!StringUtils.isBlank(tableWidth))
					aa.add("style", "width: " + tableWidth + ";");
				info.serializer.startElement("table", aa);

				boolean groupBeginning = true;
				if(chartInfo != null) {
					tableColumnCount = 1;
					int endingLevel = maxLevel;
					Results subResults;
					int groupThreeColumnIndex = getLastGroupColumnIndex(3);
					while((subResults = results.nextSubResult(groupThreeColumnIndex)) != null) {
						for(int level = endingLevel; level > Folio.GROUPING_LEVEL_TWO; level--) {
							List<PillarInfo> group = groups.get(level);
							if(group != null && !group.isEmpty()) {
								generateGroupHeader(info, this, results, folioLabel, group, tableColumnCount, groupColumns, level, noteValues);
							}
						}
						info.serializer.startElement("tbody");
						info.serializer.startElement("tr");
						info.serializer.startElement("td", attr().add("class", "chart"));
						makeChart(info, folio, subResults, folioLabel, chartInfo, displayType);
						info.serializer.endElement("td");
						info.serializer.endElement("tr");
						info.serializer.endElement("tbody");
						boolean checkEnding = true;
						endingLevel = -1;
						for(int level = Folio.GROUPING_LEVEL_THREE; level <= maxLevel; level++) {
							List<PillarInfo> group = groups.get(level);
							if(group != null && !group.isEmpty()) {
								// calc last column name
								if(results.isGroupEnding(getLastGroupColumnIndex(level))) {
									info.serializer.startElement("tbody");
									info.serializer.startElement("tr", attr().add("class", "groupSpacingRow" + (level - 2)));
									info.serializer.putElement("td", attr().add("colspan", String.valueOf(tableColumnCount)));
									info.serializer.endElement("tr");
									info.serializer.endElement("tbody");
									endingLevel = level;
								} else {
									checkEnding = false;
									break;
								}
							}
						}
						if(checkEnding && results.isGroupEnding(0)) {
							info.serializer.startElement("tbody");
							info.serializer.startElement("tr", attr().add("class", "groupSpacingRow0"));
							info.serializer.putElement("td", attr().add("colspan", String.valueOf(tableColumnCount)));
							info.serializer.endElement("tr");
							info.serializer.endElement("tbody");
						}
					}
				} else {
					if(!xtabData.isEmpty() && !groupZero.isEmpty())
						tableColumnCount += xtabData.size() * levelZeroCount;
					generateTableColumns(info, folio, folioLabel, detailPillars, groupZero, xtabData);
					int rowCount = 0;
					int groupTwoColumnIndex = getLastGroupColumnIndex(2);
					TaskListener tl = getTaskListener();
					while(results.next()) {
						if((results.getRow()) % 1000 == 0) {
							log.info("Processed " + results.getRow() + " rows and continuing");
							tl.flush();
						}
						tl.taskStarted("GROUP_HEADER_PROCESSING", null);
						for(int level = maxLevel; level > Folio.GROUPING_LEVEL_TWO; level--) {
							List<PillarInfo> group = groups.get(level);
							if(group != null && !group.isEmpty()) {
								// calc last column name
								if(groupBeginning || results.isGroupBeginning(getLastGroupColumnIndex(level))) {
									groupBeginning = true;
									generateGroupHeader(info, this, results, folioLabel, group, tableColumnCount, groupColumns, level, noteValues);
								}
							}
						}
						tl.taskEnded("GROUP_HEADER_PROCESSING");
						tl.taskStarted("TABLE_HEADER_PROCESSING", null);
						// now do level 2,1,0
						if(groupBeginning && !detailPillars.isEmpty()) { // Do table headers
							info.serializer.startElement("tbody");
							info.serializer.startElement("tr", attr().add("class", "headerRow"));
							generateTableHeader(info, folio, folioLabel, detailPillars, groupZero, xtabData);
							info.serializer.endElement("tr");
							info.serializer.endElement("tbody");
							info.serializer.startElement("tbody");
						}
						tl.taskEnded("TABLE_HEADER_PROCESSING");
						groupBeginning = false;
						if(!detailPillars.isEmpty() && results.isGroupEnding(groupTwoColumnIndex)) {
							rowCount++;
							if(folio.getMaxRows() > 0 && rowCount > folio.getMaxRows()) {
								info.serializer.startElement("tbody");
								info.serializer.startElement("tr");
								info.serializer.startElement("td", attr().add("class", "morerows").add("colspan", String.valueOf(tableColumnCount)));
								info.serializer.characters(String.valueOf(detailRowCount - folio.getMaxRows()));
								info.serializer.characters(SP);
								info.serializer.characters(translate(info.scene, "report-more-rows", "more row(s)..."));
								info.serializer.endElement("td");
								info.serializer.endElement("tr");
								info.serializer.endElement("tbody");
								break;
							}
							tl.taskStarted("TABLE_DATA_PROCESSING", null);
							generateTableData(info, this, folioLabel, detailPillars, groupZero, xtabData, results, (rowCount % 2) == 1);
							tl.taskEnded("TABLE_DATA_PROCESSING");

							if(info.totalPageCount > 1 && info.maxRowsPerPage > 0) {
								info.paginationCount++;
								if(info.paginationCount >= info.maxRowsPerPage) {
									tl.taskStarted("PAGINATION_PROCESSING", null);
									info.serializer.endElement("tbody");
									info.serializer.endElement("table"); // close out current table
									info.paginationCount = folioRowEquiv;
									nextPage(info);
									generateFolioTitle(info, folio);
									aa = attr().add("class", "folio").add("id", "folio_" + folioLabel);
									if(!StringUtils.isBlank(tableWidth))
										aa.add("style", "width: " + tableWidth + ";");
									info.serializer.startElement("table", aa);
									generateTableColumns(info, folio, folioLabel, detailPillars, groupZero, xtabData);

									if(!imageValues.isEmpty()) {
										generateIconLegend(info, imageValues, folioLabel);
									}

									for(int level = Folio.GROUPING_LEVEL_THREE; level <= maxLevel; level++) {
										List<PillarInfo> group = groups.get(level);
										if(group != null && !group.isEmpty()) {
											generateGroupHeader(info, this, results, folioLabel, group, tableColumnCount, groupColumns, level, noteValues);
											break;
										}
									}
									// Do table headers
									info.serializer.startElement("tbody");
									info.serializer.startElement("tr", attr().add("class", "headerRow"));
									generateTableHeader(info, folio, folioLabel, detailPillars, groupZero, xtabData);
									info.serializer.endElement("tr");
									info.serializer.endElement("tbody");
									info.serializer.startElement("tbody");
									tl.taskEnded("PAGINATION_PROCESSING");
								}
								for(int i = Folio.GROUPING_LEVEL_THREE; i <= maxLevel; i++) {
									List<PillarInfo> group = groups.get(i);
									if(group != null && !group.isEmpty()) {
										if(results.isGroupEnding(getLastGroupColumnIndex(i)))
											info.paginationCount += groupRowEquiv;
										else
											break;
									}
								}
							}
							tl.taskStarted("GROUP_FOOTER_PROCESSING", null);
							if(results.isGroupEnding(getLastGroupColumnIndex(Folio.GROUPING_LEVEL_THREE))) {
								if(!detailPillars.isEmpty())
									info.serializer.endElement("tbody");
								int level = Folio.GROUPING_LEVEL_THREE;
								List<PillarInfo> group = groups.get(level);
								do {
									if(group != null && !group.isEmpty()) {
										// calc last column name
										if(!groupZero.isEmpty()&&!noTotals.contains(level)) {
											PillarData firstData;
											if(dynamiclabels) {
												firstData = null;
												for(PillarInfo pi : group) {
													PillarData data = new PillarData(info, this, pi, results, level, null);
													if(!StringUtils.isBlank(data.label) || !StringUtils.isBlank(data.display)) {
														firstData = data;
														break;
													}
												}
											} else
												firstData = new PillarData(info, this, group.get(0), results, level, null);
											if(firstData != null) {
												info.serializer.startElement("tbody");
												aa = attr().add("class", "groupFooterRow" + (level - 2));
												info.serializer.startElement("tr", aa);
												if(firstTotalIndex > 0) {
													info.serializer.startElement("td", attr().add("colspan", String.valueOf(firstTotalIndex)));
													info.serializer.characters(firstData.display);
													info.serializer.characters(SP);
													info.serializer.characters(translate(info.scene, "report-totals-label", "Totals"));
													info.serializer.characters(":");
													info.serializer.endElement("td");
												}
												generateTableTotals(info, this, folioLabel, detailPillars, groupZero, xtabData, results, level, firstTotalIndex);
												info.serializer.endElement("tr");
												info.serializer.endElement("tbody");
											}
										}
										info.serializer.startElement("tbody");
										info.serializer.startElement("tr", attr().add("class", "groupSpacingRow" + (level - 2)));
										aa = attr().add("colspan", String.valueOf(tableColumnCount));
										if(groupZero.isEmpty())
											aa.add("class", "noTotals");
										info.serializer.putElement("td", aa);
										info.serializer.endElement("tr");
										info.serializer.endElement("tbody");
									}
								} while(++level <= maxLevel && ((group = groups.get(level)) == null || group.isEmpty() || results.isGroupEnding(getLastGroupColumnIndex(level))));
								tl.taskEnded("GROUP_FOOTER_PROCESSING");
								tl.taskStarted("REPORT_FOOTER_PROCESSING", null);
								if(level > maxLevel && results.isGroupEnding(0)) {
									if(!groupZero.isEmpty()) {
										info.serializer.startElement("tbody");
										aa = attr().add("class", "groupFooterRow0");
										info.serializer.startElement("tr", aa);
										if(firstTotalIndex > 0) {
											info.serializer.startElement("td", attr().add("colspan", String.valueOf(firstTotalIndex)));
											info.serializer.characters(translate(info.scene, "report-totals-label", "Totals"));
											info.serializer.characters(":");
											info.serializer.endElement("td");
										}
										generateTableTotals(info, this, folioLabel, detailPillars, groupZero, xtabData, results, 0, firstTotalIndex);
										info.serializer.endElement("tr");
										info.serializer.endElement("tbody");
									}
									info.serializer.startElement("tbody");
									info.serializer.startElement("tr", attr().add("class", "groupSpacingRow0"));
									aa = attr().add("colspan", String.valueOf(tableColumnCount));
									if(groupZero.isEmpty())
										aa.add("class", "noTotals");
									info.serializer.putElement("td", aa);
									info.serializer.endElement("tr");
									info.serializer.endElement("tbody");
								}
								tl.taskEnded("REPORT_FOOTER_PROCESSING");
							}
						}
					}
					log.info("Processed " + results.getRow() + " total rows");
					tl.flush();
				}
			} else {
				generateGroupCaption(info, folio, results, 0, 0);
				AppendableAttributes aa = attr().add("class", "folio").add("id", "folio_" + folioLabel);
				String tableWidth = folio.getDirective("table-width");
				if(!StringUtils.isBlank(tableWidth))
					aa.add("style", "width: " + tableWidth + ";");
				info.serializer.startElement("table", aa);
				info.serializer.startElement("tbody");
				info.serializer.startElement("tr");
				String zeroRowsText = folio.getDirective("zero-rows-text");
				if(StringUtils.isBlank(zeroRowsText))
					zeroRowsText = translate(info.scene, "report-no-rows", "No data found");
				info.serializer.putElement("td", attr().add("class", "zerorows"), zeroRowsText);
				info.serializer.endElement("tr");
				info.serializer.endElement("tbody");
			}

			info.serializer.endElement("table");

			if(showLegend && !noteValues.isEmpty()) {
				generateNoteDesc(info, noteValues, folioLabel);
			}
		}

		public Integer getLastGroupColumnIndex(int level) {
			if(level < 1)
				return new Integer(0);
			Integer result=groupColumnIndexs.get(level);
			if(result==null){
				return new Integer(0);
			}
			return result;
		}

		protected void nextPage(GenerateInfo info) throws SAXException {
			info.serializer.putElement("div", attr().add("class", "continued"), "-- (Continued) --");
			generateReportFooter(info, info.report);
			generateAfterContents(info);
			info.serializer.endDocument();

			PagableOutput out = (PagableOutput) info.getOutput();
			try {
				out.next();
			} catch(IOException e) {
				throw new SAXException(e);
			} catch(ServiceException e) {
				throw new SAXException(e);
			}
			try {
				info.serializer.setWriter(out.getWriter());
			} catch(IOException e) {
				throw new SAXException(e);
			}
			info.pageIndex++;

			info.serializer.startDocument();
			info.serializer.startPrefixMapping(null, getNameSpaceURI());
			generatePreprocess(info);
			generateBeforeContents(info);
			generateReportHeader(info, info.report);
			generateReportTitle(info, info.report);
		}
	}
	protected static class PillarInfo {
		public final Pillar pillar;
		public final int pillarIndex;
		public String label;
		public PillarInfo notePillarInfo;
		public int maxChars;

		public PillarInfo(Pillar pillar, int pillarIndex) {
			super();
			this.pillar = pillar;
			this.pillarIndex = pillarIndex;
		}
	}

	protected static String translate(Scene scene, String key, String defaultText) {
		Translator t = scene.getTranslator();
		if(t == null)
			return defaultText;
		return t.translate(key, defaultText);
	}

	protected static final Pattern LAYOUT_PATTERN = Pattern.compile("(^|[&])(unframed|fragment)=");

	protected static String adjustAction(String action, Scene scene) {
		if(action == null || (action = action.trim()).length() == 0)
			return action;
		Layout layout = scene.getLayout();
		if(layout != null && layout != Layout.FULL) {
			URL url;
			try {
				url = new URL(new URL(scene.getBaseUrl()), action);
			} catch(MalformedURLException e) {
				return action;
			}
			if(url.getProtocol() == null || url.getProtocol().equalsIgnoreCase("http") || url.getProtocol().equalsIgnoreCase("https")) {
				String query = url.getQuery();
				if(query == null || (query = query.trim()).length() == 0 || !LAYOUT_PATTERN.matcher(query).find()) {
					int pos = action.lastIndexOf('?');
					if(pos < 0)
						action += '?' + layout.toString() + "=true";
					else if(pos == action.length() - 1)
						action += layout + "=true";
					else
						action += '&' + layout.toString() + "=true";
				}
			}
		}
		return action;
	}

	protected static String getDateSortText(Object sortValue, Format sortFormat) throws ConvertException {
		if(sortValue instanceof Date)
			return String.valueOf(((Date) sortValue).getTime());
		else if(sortValue instanceof Calendar)
			return String.valueOf(((Calendar) sortValue).getTimeInMillis());
		else if(sortValue instanceof Number) {
			return String.valueOf(sortValue);
		} else {
			String sortText = ConvertUtils.formatObject(sortValue, sortFormat);
			if(sortText != null)
				try {
					Date sortDate = ConvertUtils.convert(Date.class, sortText);
					if(sortDate != null)
						return String.valueOf(sortDate.getTime());
				} catch(ConvertException e) {
					if(sortValue instanceof Object[]) {
						for(Object sv : (Object[]) sortValue) {
							try {
								sortText = getDateSortText(sv, null);
								if(sortText != null)
									return sortText;
							} catch(ConvertException e1) {
							}
						}
					}
					throw e;
				}
			return null;
		}
	}

	protected static Object getPercentData(Object displayValue, Object prevDisplayValue, Format format) throws ConvertException {
		if(displayValue == null || prevDisplayValue == null || !displayValue.getClass().equals(prevDisplayValue.getClass()))
			return null;
		if(prevDisplayValue instanceof MultiValueWithContext) {
			Object[] prevDisplayValueArray = ((MultiValueWithContext) prevDisplayValue).convert(Object[].class);
			Object[] returnVal = new Object[prevDisplayValueArray.length];
			Object[] displayValArray = ((MultiValueWithContext) displayValue).convert(Object[].class);
			DecimalFormat[] formats = new DecimalFormat[prevDisplayValueArray.length];
			if(format instanceof DecimalFormat) {
				for(int i = 0; i < returnVal.length; i++) {
					formats[i] = (DecimalFormat) format;
				}
			} else if(format instanceof MessageFormat) {
				FormatObject[] fos = ((MessageFormat) format).getFormatObjects();
				for(int i = 0; i < returnVal.length && i < fos.length; i++) {
					if(fos[i].format instanceof DecimalFormat)
						formats[i] = (DecimalFormat) fos[i].format;
				}
			} else if(format instanceof java.text.MessageFormat) {
				Format[] fos = ((java.text.MessageFormat) format).getFormats();
				for(int i = 0; i < returnVal.length && i < fos.length; i++) {
					if(fos[i] instanceof DecimalFormat)
						formats[i] = (DecimalFormat) fos[i];
				}
			}
			for(int i = 0; i < returnVal.length; i++) {
				returnVal[i] = calcPercent(displayValArray[i], prevDisplayValueArray[i], formats[i]);
			}
			return returnVal;
		} else if(prevDisplayValue instanceof ValueWithContext) {
			prevDisplayValue = ((ValueWithContext) prevDisplayValue).convert(Object.class);
			displayValue = ((ValueWithContext) displayValue).convert(Object.class);
			DecimalFormat dformat = null;
			if(format instanceof DecimalFormat) {
				dformat = (DecimalFormat) format;
			} else if(format instanceof MessageFormat) {
				FormatObject[] fos = ((MessageFormat) format).getFormatObjects();
				if(fos.length > 0) {
					dformat = (DecimalFormat) fos[0].format;
				}
			} else if(format instanceof java.text.MessageFormat) {
				Format[] fos = ((java.text.MessageFormat) format).getFormats();
				if(fos.length > 0) {
					dformat = (DecimalFormat) fos[0];
				}
			}
			return calcPercent(displayValue, prevDisplayValue, dformat);
		} else {
			log.debug("Invalid percent data. Do not know how to process.");
			return null;
		}
	}

	protected static Object calcPercent(Object displayValue, Object prevDisplayValue, DecimalFormat format) throws ConvertException {
		if(displayValue == null || prevDisplayValue == null) {
			return null;
		}
		if(prevDisplayValue instanceof Number) {
			BigDecimal prevDisplayValueNum = ConvertUtils.convert(BigDecimal.class, prevDisplayValue);
			if(prevDisplayValueNum != null && prevDisplayValueNum.signum() != 0) {
				BigDecimal displayValueNum = ConvertUtils.convert(BigDecimal.class, displayValue);
				if(displayValueNum != null) {
					if(format != null)
						return displayValueNum.divide(prevDisplayValueNum, format.getMaximumFractionDigits() + (int) Math.log10(format.getMultiplier()), RoundingMode.HALF_UP);
					return displayValueNum.divide(prevDisplayValueNum, displayValueNum.scale() + prevDisplayValueNum.scale(), RoundingMode.HALF_UP);
				}
				return null;
			}
			return null;
		}
		return null;
	}

	protected final ExecuteEngine engine;
	protected int defaultMaxRowsPerPage = 500;

	public DirectXHTMLGenerator(ExecuteEngine engine) {
		super();
		this.engine = engine;
	}

	protected String getNameSpaceURI() {
		return "http://www.w3.org/1999/xhtml";
	}

	protected AppendableAttributes attr() {
		return new AppendableAttributes(getNameSpaceURI(), "");
	}

	protected Set<TargetType> getDefaultAltTargetTypes(TargetType targetType, Map<String, String> config) {
		Set<TargetType> altTargetTypes = new LinkedHashSet<TargetType>();
		if(ConvertUtils.getBooleanSafely(config.get("extendedOutputTypes"), false)) {
			if(targetType == TargetType.CHART_HTML)
				altTargetTypes.add(TargetType.HTML);
			altTargetTypes.add(targetType.isChart() ? TargetType.CHART_PDF : TargetType.PDF);
		}
		altTargetTypes.add(targetType.isChart() ? TargetType.CHART_DOC : TargetType.DOC);
		altTargetTypes.add(TargetType.XLS);
		return altTargetTypes;
	}
	protected GenerateInfo createGenerateInfo(Report report, OutputType outputType, Map<String, Object> parameters, Results[] results, Executor executor, final Scene scene, final Output out) throws SAXException {
		String configString = StringUtils.substringAfter(outputType.getPath(), ":");
		Map<String, String> config = new HashMap<String, String>();
		try {
			StringUtils.intoMap(config, configString);
		} catch(ParseException e) {
			log.warn("Could not parse output type config '" + configString + "'", e);
		}
		boolean chart = ConvertUtils.getBooleanSafely(config.get("chart"), false);
		TargetType targetType = TargetType.getTargetType(chart, outputType.getContentType());
		if(targetType == null) {
			log.warn("Could not find Target Type for '" + outputType.getContentType() + "'; using '" + TargetType.HTML.getPrimaryContentType() + "'");
			targetType = chart ? TargetType.CHART_HTML : TargetType.HTML;
		}
		Set<TargetType> altTargetTypes;
		if(StringUtils.isBlank(scene.getRunReportAction()))
			altTargetTypes = Collections.emptySet();
		else {
			String alternateOutputTypes = config.get("alternateOutputTypes");
			if(StringUtils.isBlank(alternateOutputTypes)) {
				altTargetTypes = getDefaultAltTargetTypes(targetType, config);
			} else if(alternateOutputTypes.trim().equals("-"))
				altTargetTypes = Collections.emptySet();
			else
				try {
					altTargetTypes = ConvertUtils.convert(LinkedHashSet.class, TargetType.class, alternateOutputTypes);
				} catch(ConvertException e) {
					log.warn("Could not convert alternateOutputTypes '" + alternateOutputTypes + "' to a list of TargetTypes; falling back to defaults", e);
					altTargetTypes = getDefaultAltTargetTypes(targetType, config);
				}
		}
		Serializer serializer;
		XHTMLHandler xhtml;
		switch(targetType) {
			case PDF:
			case CHART_PDF:
				scene.setLayout(Layout.UNFRAMED);
				//NOTE: 12/07/2012 This is for rptgen host not able to retrieve file from usalive https.for pdf, we just retrieve locally
				scene.setBaseUrl(engine.getCssDirectory());
				// remove any ?t= query from style urls
				String[] stylesheets = scene.getStylesheets();
				if(stylesheets != null) {
					for(int i = 0; i < stylesheets.length; i++)
						stylesheets[i] = IOUtils.removeLastModifiedFromUri(stylesheets[i]);
					scene.setStylesheets(stylesheets);
				}
				// FOP
				/*
				FopFactory fopFactory = FopFactory.newInstance();
				fopFactory.setStrictValidation(false);
				FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
				foUserAgent.setBaseURL(info.scene.getBaseUrl());
				Fop fop;
				DefaultHandler fopHandler;
				try {
					fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out.getOutputStream());
					fopHandler = fop.getDefaultHandler();
				} catch(FOPException e) {
					throw new IOException(e);
				}

				// Use XSLT
				List<URL> urls = new ArrayList<URL>();
				for(int i = 0; i < info.scene.getStylesheets().length; i++) {// must be forwards (last one wins)
					String cssPath = info.scene.getStylesheets()[i];
					if(cssPath != null && cssPath.trim().length() > 0) {
						if(log.isDebugEnabled())
							log.debug("Finding url for '" + engine.getCssDirectory() + cssPath + "'");
						urls.add(engine.getResourceResolver().getResourceURL(engine.getCssDirectory() + cssPath));
					} else {
						if(log.isDebugEnabled())
							log.debug("CSS is blank for at index " + (i + 1));
					}
				}

				TemplatesLoader tl = TemplatesLoader.getInstance(info.scene.getTranslator());
				TransformerHandler handler;
				try {
					handler = tl.newTransformerHandler("resource:simple/falcon/templates/report/xhtml-to-pdf.xsl");
				} catch(TransformerConfigurationException e) {
					throw new IOException(e);
				} catch(LoadingException e) {
					throw new IOException(e);
				}
				CssFilter filter = cssManager.getCssSheet(urls.toArray(new URL[urls.size()])).createCssFilter();
				// FOPAttributeFixer fixer = new FOPAttributeFixer();
				// filter.setContentHandler(fixer);
				// fixer.setContentHandler(handler);

				handler.setResult(new SAXResult(fopHandler));

				filter.setContentHandler(handler);
				filter.setDTDHandler(handler);

				setContentHandler(filter);
				setDTDHandler(filter);
				setErrorHandler(filter);
				setEntityResolver(filter);
				// */
				/* A new SAX-based converter - not finished
				DelegatingXMLHandler h = new XHTMLToFOHandler();
				h.setContentHandler(fopHandler);
				h.setEntityResolver(fopHandler);
				h.setErrorHandler(fopHandler);
				h.setDTDHandler(fopHandler);
				setContentHandler(h);
				setLexicalHandler(h);
				setDTDHandler(h);
				setErrorHandler(h);
				setEntityResolver(h);
				 */
				// * FlyingSaucer + iText
				// This works very well (it retains much of the html formatting). The only drawback is that it does not support
				// streaming the file right now. (The whole report is read into a DOM Document then converted to PDF). None of the
				// other approaches worked with streaming so we use this as the best option without extensive work.
				// Also, it currently renders any content of <script> elements.
				// 06-15-2012 - For some reason this stopped working and instead produces blank pages
				try {
					for(Folio folio:report.getFolios()){
						// set folio maxRows if not set for pdf only
						if(folio.getMaxRows()<0){
							try{
								folio.setMaxRows(ConvertUtils.getInt(parameters.get("folioMaxRows"), 0));
							}catch(ConvertException e){
								throw new SAXException(e);
							}
						}
					}
					serializer = new SAX2DOM() {
						@Override
						public void endDocument() {
							super.endDocument();
							ITextRenderer renderer = new ITextRenderer();
							renderer.setDocument((org.w3c.dom.Document) getDOM(), scene.getBaseUrl());
							renderer.layout();
							OutputStream os=null;
							try {
								os = out.getOutputStream();
								renderer.createPDF(os);
								os.flush();
							} catch(DocumentException e) {
								throw new UndeclaredThrowableException(e);
							} catch(IOException e) {
								throw new UndeclaredThrowableException(e);
							}finally{
								try{
									os.close();
								}catch(IOException e) {
										throw new UndeclaredThrowableException(e);
								}
							}
						}

					};
				} catch(ParserConfigurationException e) {
					throw new SAXException(e);
				}
				break;
			case CHART_DOC:
			case CHART_XLS:
				scene.setLayout(Layout.UNFRAMED);
				serializer = new MHtmlSerializer();
				// TODO: set media type
				break;
			case CHART_MHTML:
			case MHTML:
				serializer = new MHtmlSerializer();
				break;
			case XLS:
			case DOC:
			case PPT:
			case CHART_PPT:
				scene.setLayout(Layout.UNFRAMED);
			default:
				serializer = xhtml = new XHTMLHandler();
				xhtml.setMediaType(StringUtils.substringBefore(outputType.getContentType(), ";"));
				String sessionToken = ConvertUtils.getStringSafely(scene.getProperties().get("session-token"));
				if(!StringUtils.isBlank(sessionToken))
					xhtml.setSessionToken(sessionToken);
		}
		try {
			if(!(targetType.equals(TargetType.PDF)||targetType.equals(TargetType.CHART_PDF))){
				serializer.setWriter(out.getWriter());
			}
		} catch(IOException e) {
			throw new SAXException(e);
		}
		ColumnNamer cn;
		try {
			cn = engine.getDesignEngine().getColumnNamer();
		} catch(DesignException e) {
			throw new SAXException(e);
		}
		GenerateInfo info = new GenerateInfo(report, results, parameters, cn, scene, executor, outputType, targetType, serializer, altTargetTypes, out);
		if(serializer instanceof MHtmlSerializer)
			((MHtmlSerializer) serializer).info = info;
		return info;
	}

	
	public void generate(Report report, OutputType outputType, Map<String, Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
		try {
			GenerateInfo info = createGenerateInfo(report, outputType, parameters, results, executor, scene, out);
			if(info.scene.getLayout() == null)
				info.scene.setLayout(Layout.FULL);
			generateDocument(info);
		} catch(SAXException e) {
			throw new ExecuteException(e);
		}
	}
	
	protected String getDisplayName(GenerateInfo info) {
		return "";
	}
	
	/**
	 * @throws SAXException
	 */
	protected void generateMenuLinks(GenerateInfo info, Node linksNode) throws SAXException {
		
	}

	/**
	 * @throws SAXException
	 */
	protected void generateBodyHeader(GenerateInfo info) throws SAXException {
		
	}

	protected void generateReportTitle(GenerateInfo info, Report report) throws SAXException {
		if(ConvertUtils.getBooleanSafely(report.getDirective("show-run-date"), true))
			info.serializer.putElement("div", attr().add("class", "rundate"), ConvertUtils.formatObject(new Date(), report.getVerbage("run-date-format", "MESSAGE:Run Date: {,DATE,MMM dd, yyyy h:mm:ss a z}")));

		OutputType ie7CompatOutputType = null;
		switch(info.targetType) {
			case CHART_HTML:
				if(info.targetType == TargetType.CHART_HTML && info.displayType != null) {
					switch(info.displayType) {
						case INLINE_IMAGE:
						case INLINE_OBJECT:
							ie7CompatOutputType = getOutputTypeFor(TargetType.CHART_MHTML, info.outputType.getProtocol(), StringUtils.substringBefore(info.outputType.getPath(), ":"), false);
					}
				}
				// fall-through
			case HTML:
				if(ConvertUtils.getBooleanSafely(report.getDirective("show-alt-output-types"), true))
					generateOutputTypeMenu(info, false);
				else if(ie7CompatOutputType != null)
					generateOutputTypeMenu(info, true);
				if(ie7CompatOutputType != null) {
					StringBuilder sb = new StringBuilder();
					sb.append("[if lte IE 7]>\n<div class=\"ie-incompat-charts\">").append(translate(info.scene, "report-ie-compat-charts-warning-plain", ""))
                      .append("<a onclick=\"document.forms['changeOutput'].displayType.value='inline-request'; submitOutputForm(document.forms['changeOutput'], null, null, 'true');\">")
                      .append(translate(info.scene, "report-ie-compat-charts-warning-link", "The charts on this report are incompatible with your browser (Internet Explorer 7 and below). Please click here to generate a report whose charts will display in your browser."))
							.append("</a></div>\n<style type=\"text/css\">\n.reportHeader { height: " + (37 + info.reportHeaderSize) + "px; }\n.reportContent { top: " + (37 + info.reportHeaderSize) + "px; }\n</style>\n<![endif]");

					info.serializer.comment(sb.toString());
					sb.setLength(0);
					sb.append("[if IE 8]>\n<div class=\"ie-incompat-charts\">").append(translate(info.scene, "report-ie-compat-charts-warning-plain", ""))
					  .append("<a onclick=\"document.forms['changeOutput'].displayType.value='inline-request'; submitOutputForm(document.forms['changeOutput'], null, null, 'true');\">")
							.append(translate(info.scene, "report-ie8-compat-charts-warning-link", "Some of the charts on this report may be incompatible with your browser (Internet Explorer 8). Please click here to generate a report whose charts will display in your browser."))
							.append("</a></div>\n<style type=\"text/css\">\n.reportHeader { height: " + (37 + info.reportHeaderSize) + "px; }\n.reportContent { top: " + (37 + info.reportHeaderSize) + "px; }\n</style>\n<![endif]");
					info.serializer.comment(sb.toString());
				}
		}

		if(info.totalPageCount > 1)
			generatePageNavigation(info);
		String reportTitle = report.getTitle(info.parameters);
		if(!StringUtils.isBlank(reportTitle)){
			if(info.isPagable && info.totalPageCount > 1) {
				info.serializer.putElement("div", attr().add("class", "title0 title-paginated").add("title", reportTitle), reportTitle);
			}else{
				info.serializer.putElement("div", attr().add("class", "title0").add("title", reportTitle), reportTitle);
			}
		}

		String reportSubtitle = report.getSubtitle(info.parameters);
		if(!StringUtils.isBlank(reportSubtitle)){
			if(info.isPagable && info.totalPageCount > 1) {
				info.serializer.putElement("div", attr().add("class", "title1 title-paginated").add("title", reportSubtitle), reportSubtitle);
			}else{
				info.serializer.putElement("div", attr().add("class", "title1").add("title", reportSubtitle), reportSubtitle);
			}
		}
		if(!StringUtils.isBlank(reportTitle) || !StringUtils.isBlank(reportSubtitle) || info.folioInfos.length > 1) {
			info.serializer.endElement("div");
			info.serializer.startElement("div", attr().add("class", "reportContent"));
		}
	}
	
	protected void generateFolioTitle(GenerateInfo info, Folio folio) throws SAXException {
		boolean showLine = false;
		String folioTitle = folio.getTitle(info.parameters);
		if(!StringUtils.isBlank(folioTitle)) {
			if(info.isPagable && info.totalPageCount > 1) {
				info.serializer.putElement("div", attr().add("class", "title2 title-paginated").add("title", folioTitle), folioTitle);
			}else{
				info.serializer.putElement("div", attr().add("class", "title2").add("title", folioTitle), folioTitle);
			}
			showLine = true;
		}
		String folioSubtitle = folio.getSubtitle(info.parameters);
		if(!StringUtils.isBlank(folioSubtitle)) {
			if(info.isPagable && info.totalPageCount > 1) {
				info.serializer.putElement("div", attr().add("class", "title3 title-paginated").add("title", folioSubtitle), folioSubtitle);
			}else{
				info.serializer.putElement("div", attr().add("class", "title3").add("title", folioSubtitle), folioSubtitle);
			}
			showLine = true;
		}

		if(showLine)
			info.serializer.putElement("div", attr().add("class", "folio-line"), NBSP);
		if(info.folioInfos.length == 1 && StringUtils.isBlank(info.report.getTitle(info.parameters)) && StringUtils.isBlank(info.report.getSubtitle(info.parameters))) {
			info.serializer.endElement("div");
			info.serializer.startElement("div", attr().add("class", "reportContent"));
		}
	}
	
	protected static String getUniqueBoundaryValue() {
		StringBuilder s = new StringBuilder();
		// Unique string is ----=_Part_<part>_<hashcode>.<currentTime>
		s.append("----=_Part_").append(boundaryUIDGenerator.incrementAndGet()).append("_").append(s.hashCode()).append('.').append(System.currentTimeMillis());
		return s.toString();
	}

	protected void generateDocumentHeader(GenerateInfo info) throws SAXException {
		// XXX: for excel should we add something like?:
		// "<html xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>List Of Employees</x:Name><x:WorksheetOptions><x:Selected/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif])-->"
		AppendableAttributes aa = attr().add("lang", "en");
		aa.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "xml:lang", "CDATA", "en");
			info.serializer.startElement("html", aa);
		info.serializer.startElement("head");
		info.serializer.putElement("meta", attr().add("http-equiv", "X-UA-Compatible").add("content", "IE=9; IE=8; IE=7; IE=EDGE"));
		String title = getTitle(info);
		String subtitle = getSubtitle(info);
		info.serializer.startElement("title");
		if(!StringUtils.isBlank(title)) {
			info.serializer.characters(title);
			if(!StringUtils.isBlank(subtitle))
				info.serializer.characters(" - ");
		}
		if(!StringUtils.isBlank(subtitle))
			info.serializer.characters(subtitle);
		info.serializer.endElement("title");
		generateBase(info);
		generateStyles(info);
		switch(info.targetType) {
			case CHART_HTML:
			case HTML:
				generateScripts(info);
		}
		generatePageHeader(info);
		info.serializer.endElement("head");
	}

	protected void generateDocument(GenerateInfo info) throws SAXException {
		prepareReport(info);
		info.serializer.startDocument();
		info.serializer.startPrefixMapping(null, getNameSpaceURI());
		generatePreprocess(info);
		generateBeforeContents(info);
		generateContents(info);
		generateAfterContents(info);
		info.serializer.endDocument();
	}

	protected void generateBeforeContents(GenerateInfo info) throws SAXException {
		generateDocumentHeader(info);
		info.serializer.startElement("body");
		generateBodyHeader(info);
		generateContentHeader(info);
	}

	protected void generateAfterContents(GenerateInfo info) throws SAXException {
		generateContentFooter(info);
		generateBodyFooter(info);
		info.serializer.endElement("body");
		info.serializer.endElement("html");
	}

	protected void generateScripts(GenerateInfo info) throws SAXException {
		if(info.scene.getScripts() != null)
			for(String script : info.scene.getScripts()) {
				if(!StringUtils.isBlank(script))
					info.serializer.putElement("script", attr().add("type", "text/javascript").add("src", script));// info.scene.addLastModifiedToUri() is already called when creating the scene
			}
	}

	protected void prepareReport(GenerateInfo info) throws SAXException {
		if(info.isPagable)
			info.maxRowsPerPage = ConvertUtils.getIntSafely(info.report.getDirective("max-rows-per-page"), getDefaultMaxRowsPerPage());
		// do first pass for each folio here
		for(FolioInfo folioInfo : info.folioInfos)
			folioInfo.preprocess(info);
		if(info.isPagable) {
			PagableOutput output = (PagableOutput) info.getOutput();
			output.setMaxPageId(info.totalPageCount - 1);
		}
		info.paginationCount = 0;
	}

	/**
	 * @throws SAXException
	 */
	protected void generatePreprocess(GenerateInfo info) throws SAXException {
		info.serializer.processingInstruction("client", "user-agent=\"" + info.scene.getUserAgent() + '"');
	}

	/**
	 * @throws SAXException
	 */
	protected String getTitle(GenerateInfo info) {
		return "Falcon Reporting Engine";
	}
	
	/**
	 * @throws SAXException
	 */
	protected void generatePageHeader(GenerateInfo info) throws SAXException {
	      
	}

	protected void generateBody(GenerateInfo info) throws SAXException {
		info.serializer.startElement("body");
		generateBodyHeader(info);
		generateContentHeader(info);
		generateContents(info);
		generateContentFooter(info);
		generateBodyFooter(info);
		info.serializer.endElement("body");
	}

	/**
	 * @param info
	 * @throws SAXException
	 */
	protected void generateBodyFooter(GenerateInfo info) throws SAXException {

	}

	/**
	 * @throws SAXException
	 */
	protected void generateContentHeader(GenerateInfo info) throws SAXException {
	      
	}

	protected void generateContents(GenerateInfo info) throws SAXException {
		generateReport(info, info.report);
	}

	/**
	 * @throws SAXException
	 */
	protected void generateContentFooter(GenerateInfo info) throws SAXException {
	      
	}

	protected OutputType getOutputTypeFor(TargetType targetType, String protocol, String pathPrefix, boolean lenient) throws SAXException {
		OutputType lenientOutputType = null;
		try {
			for(OutputType outputType : engine.getDesignEngine().getOutputTypes()) {
				if(targetType.getAllContentTypes().contains(outputType.getContentType()) && targetType.isChart() == (outputType.getPath() != null && chartConfigPattern.matcher(outputType.getPath()).matches())) {
					if((StringUtils.isBlank(protocol) || StringUtils.isBlank(outputType.getProtocol()) || protocol.equals(outputType.getProtocol())) && (pathPrefix == null || outputType.getPath().startsWith(pathPrefix)))
						return outputType;
					if(lenient) {
						if(lenientOutputType == null)
							lenientOutputType = outputType;
						else if(!ConvertUtils.areEqual(lenientOutputType.getProtocol(), protocol) && ConvertUtils.areEqual(outputType.getProtocol(), protocol))
							lenientOutputType = outputType;
						else if(!lenientOutputType.getPath().startsWith(pathPrefix) && outputType.getPath().startsWith(pathPrefix) && (!ConvertUtils.areEqual(lenientOutputType.getProtocol(), protocol) || ConvertUtils.areEqual(outputType.getProtocol(), protocol)))
							lenientOutputType = outputType;
					}
				}
			}
		} catch(DesignException e) {
			throw new SAXException(e);
		}
		return lenientOutputType;
	}
	protected void generateReport(GenerateInfo info, Report report) throws SAXException {
		generateReportHeader(info, report);
		generateReportTitle(info, report);
		for(FolioInfo folioInfo : info.folioInfos) {
			folioInfo.process(info);
			info.serializer.characters(NBSP);
		}
		generateReportFooter(info, report);
	}

	protected boolean handleScrolling(GenerateInfo info) {
		switch(info.targetType) {
			case CHART_HTML:
			case HTML:
				return true;
			default:
				return false;
		}
	}
	/**
	 * @throws SAXException
	 */
	protected void generateReportHeader(GenerateInfo info, Report report) throws SAXException {
		AppendableAttributes aa = attr().add("class", "reportHeader");
		String tableWidth = report.getDirective("table-width");
		if(!StringUtils.isBlank(tableWidth))
			aa.add("style", "width: " + tableWidth + ';');
		info.serializer.startElement("div", aa);
	}

	protected void generateReportFooter(GenerateInfo info, Report report) throws SAXException {
		info.serializer.endElement("div");
		if(getCopyrightHeight(info) > 0)
			generateCopyright(info, report, null, "report-copyright");
		info.serializer.characters(NBSP);
	}

	protected void generateCopyright(GenerateInfo info, Report report, String cssId, String cssClass) throws SAXException {
		AppendableAttributes aa = attr();
		if(!StringUtils.isBlank(cssId))
			aa.add("id", cssId);
		if(!StringUtils.isBlank(cssClass))
			aa.add("class", cssClass);

		info.serializer.putElement("div", aa, translate(info.scene, "report-copyright", ""));
	}

	/**
	 * @throws SAXException
	 */
	protected void generateOutputTypeMenu(GenerateInfo info, boolean hideButtons) throws SAXException {
		if(!StringUtils.isBlank(info.scene.getRunReportAction()))
			generateOutputTypeMenuContent(info, info.scene.getRunReportAction(), hideButtons);
	}

	protected static final Set<String> EXCLUDE_PARAMS = new HashSet<String>(Arrays.asList("basicReportId", "reportId", "folioIds", "folioId", "title", "subtitle", "query", "max-rows-per-page", "paramNames", "paramTypes", "params.user"));
	protected void generateOutputTypeMenuContent(GenerateInfo info, String runReportAction, boolean hideButtons) throws SAXException {
		info.serializer.startElement("div", attr().add("class", "output-type-menu"));
		Map<String,String> params = new HashMap<String, String>();
		for(Map.Entry<String, Object> entry : info.parameters.entrySet()) {
			if(!StringUtils.isBlank(entry.getKey()) && !entry.getKey().startsWith("~") && entry.getValue() != null) {
				String value = ConvertUtils.getStringSafely(entry.getValue());
				if(!StringUtils.isBlank(value))
					params.put(entry.getKey(), value);
			}
		}
		params.put("outputType", String.valueOf(info.outputType.getOutputTypeId()));
		params.put("resultsType", "cache");
		params.put("promptForParams", "FIX");
		params.put("doChartConfigUpdate", "false");
		if(info.targetType == TargetType.CHART_HTML && StringUtils.isBlank(params.get("displayType")) && info.displayType != null)
			params.put("displayType", info.displayType.toString());
		Layout nextLayout = info.scene.getLayout();
		int pos = runReportAction.indexOf('?');
		if(pos >= 0) {
			for(Map.Entry<String, Object> entry : RequestUtils.parseParameters(runReportAction.substring(pos + 1)).entrySet()) {
				if(!StringUtils.isBlank(entry.getKey()) && !entry.getKey().startsWith("~") && entry.getValue() != null) {
					String value = ConvertUtils.getStringSafely(entry.getValue());
					if(!StringUtils.isBlank(value)) {
						if("fragment".equals(entry.getKey())) {
							Boolean fragment = ConvertUtils.convertSafely(Boolean.class, entry.getValue(), null);
							if(fragment != null) {
								if(fragment)
									nextLayout = Layout.FRAGMENT;
								else if(nextLayout == Layout.FRAGMENT)
									nextLayout = Layout.FULL;
							}
						} else if("unframed".equals(entry.getKey())) {
							Boolean unframed = ConvertUtils.convertSafely(Boolean.class, entry.getValue(), null);
							if(unframed != null) {
								if(unframed)
									nextLayout = Layout.UNFRAMED;
								else if(nextLayout == Layout.UNFRAMED)
									nextLayout = Layout.FULL;
							}
						} else
							params.put(entry.getKey(), value);
					}
				}
			}
			runReportAction = runReportAction.substring(0, pos);
		}
		info.serializer.startElement("form", attr().add("name", "changeOutput").add("action", runReportAction).add("method", "post"));
		if(info.report.getReportId() != null)		
			info.serializer.putElement("input", attr().add("type", "hidden").add("name", "reportId").add("value", info.report.getReportId().toString()));

		// set basicReportId
		String basicReportId = params.get("basicReportId");
		if(!StringUtils.isBlank(basicReportId))
			info.serializer.putElement("input", attr().add("type", "hidden").add("name", "basicReportId").add("value", basicReportId));
		List<Long> folioIds = new ArrayList<Long>();
		for(Folio folio : info.report.getFolios())
			if(folio.getFolioId() != null)
				folioIds.add(folio.getFolioId());
		switch(folioIds.size()) {
			case 1:
				info.serializer.putElement("input", attr().add("type", "hidden").add("name", "folioId").add("value", folioIds.get(0).toString()));
				break;
			case 0:					
				break;
			default:
				for(Long folioId : folioIds)
					info.serializer.putElement("input", attr().add("type", "hidden").add("name", "folioIds").add("value", folioId.toString()));
		}
		
		switch(nextLayout) {
			case FRAGMENT:
				params.put("fragment", "true");
				break;
			case UNFRAMED:
				params.put("unframed", "true");
				break;
		}
		for(Map.Entry<String, String> entry : params.entrySet()) {
			if(!EXCLUDE_PARAMS.contains(entry.getKey()) && !StringUtils.isBlank(entry.getValue()))
				info.serializer.putElement("input", attr().add("type", "hidden").add("name", entry.getKey()).add("value", entry.getValue()));
		}
		if(!hideButtons) {
			Object fileId = info.scene.getProperties().get("fileId");
			Object requestId = info.scene.getProperties().get("requestId");
			if(requestId != null) {
				// info.serializer.putElement("input", attr().add("type", "hidden").add("name", "refreshFileId").add("value", fileId));
				info.serializer.putElement("input", attr().add("type", "hidden").add("name", "refreshFileId").add("value", String.valueOf(fileId)));
				info.serializer.putElement("input", attr().add("type", "hidden").add("name", "requestId").add("value", String.valueOf(requestId)));
				info.serializer.putElement("input", attr().add("type", "hidden").add("name", "profileId").add("value", String.valueOf(info.executor.getUserId())));
				generateAltOutput(info, "refresh_report_data.i", info.outputType, "Refresh Data", false, "report-button-refresh");
			}
			String saveAction = params.get("saveAction");		
			if(!StringUtils.isBlank(saveAction)) {
				if(StringUtils.isBlank(params.get("runReportAction")))
					info.serializer.putElement("input", attr().add("type", "hidden").add("name", "runReportAction").add("value", runReportAction));
				generateAltOutput(info, saveAction, info.outputType, "Save Report", false, "report-button-save");
			}
			String referrer = params.get("referrer");		
			if(!StringUtils.isBlank(referrer))
				generateAltOutput(info, referrer, info.outputType, "Configure Parameters", false, "report-button-config");
			Collection<TargetType> altTargetTypes;
			// add from directive
			TargetType[] directiveTargetTypes = ConvertUtils.convertSafely(TargetType[].class, info.report.getDirective("alt-output-type-list"), null);
			if(directiveTargetTypes != null)
				altTargetTypes = Arrays.asList(directiveTargetTypes);
			else
				altTargetTypes = info.altTargetTypes;

			for(TargetType altTargetType : altTargetTypes)
				if(altTargetType != null)
					generateAltTargetType(info, runReportAction, altTargetType);
					
			if(!info.targetType.isChart() && ConvertUtils.getBooleanSafely(info.report.getDirective("allow-chart-config"), false))
				generateAltOutput(info, "configure_chart.i", getOutputTypeFor(TargetType.CHART_HTML, info.outputType.getProtocol(), StringUtils.substringBefore(info.outputType.getPath(), ":"), false), "Create Chart from Data", true, "report-button-html-chart");
		}
		info.serializer.startElement("script", attr().add("type", "text/javascript"));
		info.serializer.characters("function submitOutputForm(form, action, outputType, doChartConfigUpdate) { if(action) { form.action = action; } if(outputType) { form.outputType.value = outputType; } form.doChartConfigUpdate.value = doChartConfigUpdate; form.submit(); }");
		if(nextLayout == Layout.FRAGMENT) {
			info.serializer.characters("\nwindow.addEvent(\"domready\", function() { new Form.Request(document.changeOutput, $(document.changeOutput).getParent().getParent()); });");
		}
		info.serializer.endElement("script");
		info.serializer.endElement("form");
		info.serializer.endElement("div");
	}

	protected void generateAltTargetType(GenerateInfo info, String runReportAction, TargetType targetType) throws SAXException {
		OutputType outputType = getOutputTypeFor(targetType, info.outputType.getProtocol(), StringUtils.substringBefore(info.outputType.getPath(), ":"), true);
		if(outputType != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("View ");
			if(targetType.isChart())
				sb.append("Chart ");
			sb.append("in ").append(targetType.getDescription());
			String title = sb.toString();
			sb.setLength(0);
			sb.append("report-button-").append(targetType.getCodeSuffix());
			if(targetType.isChart())
				sb.append("-chart");
			String id = sb.toString();
			generateAltOutput(info, runReportAction, outputType, title, targetType.isChart(), id);
		}
	}

	protected void generateAltOutput(GenerateInfo info, String action, OutputType outputType, String title, boolean allowChartConfig, String id) throws SAXException {
		if(outputType == null)
			return;
		StringBuilder sb = new StringBuilder();
		sb.append("submitOutputForm(this.form, '").append(StringUtils.prepareScript(action)).append("', ").append(outputType.getOutputTypeId()).append(", '").append(allowChartConfig).append("');");
		String onclick = sb.toString();

		info.serializer.startElement("button", attr().add("title", title).add("onclick", onclick).add("id", id));
		info.serializer.putElement("div", (String) null);
		info.serializer.endElement("button");
	}

	protected void generateNoteDesc(GenerateInfo info, Set<Note> noteValues, String folioLabel) throws SAXException {
		if(!noteValues.isEmpty()) {
			info.serializer.startElement("div", attr().add("class", "notes"));
			int cnt = 1;
			for(Note note : noteValues) {
				if(!StringUtils.isBlank(note.description)) {
					AppendableAttributes aa = attr().add("class", "note-desc").add("id", "note_desc" + folioLabel + UN + cnt++);
					if(!StringUtils.isBlank(note.style))
						aa.add("style", note.style);
					info.serializer.startElement("div", aa);
					info.serializer.characters(note.marker);
					info.serializer.characters(NBSP);
					info.serializer.characters("-");
					info.serializer.characters(NBSP);
					info.serializer.characters(note.description); // @fr:tip = helpFormat()
					info.serializer.endElement("div");
				}
			}
			info.serializer.endElement("div");
		}
	}

	protected void generateIconLegend(GenerateInfo info, Set<Image> imageValues, String folioLabel) throws SAXException {
		if(!imageValues.isEmpty()) {
			info.serializer.startElement("div", attr().add("class", "icon-legends"));
			for(Image img : imageValues) {
				if(!StringUtils.isBlank(img.description)) {
					info.serializer.startElement("span", attr().add("class", "icon-legend"));
					info.serializer.putElement("img", attr().add("src", img.src).add("alt", img.description));
					info.serializer.characters(NBSP);
					info.serializer.characters("-");
					info.serializer.characters(NBSP);
					info.serializer.characters(img.description);
					info.serializer.endElement("span");
				}
			}
			info.serializer.endElement("div");
		}
	}

	protected void generateGroupCaption(GenerateInfo info, Folio folio, Results results, int detailRowCount, int chartCount) throws SAXException {
		int rows = detailRowCount /*results.getRow()-1*/;
		String total;
		if(folio.getMaxRows() > 0 && results.getRow() > folio.getMaxRows()) {
			total = "?";
			info.serializer.putElement("div", attr().add("class", "incompleteWarning"), translate(info.scene, "not-all-rows-shown", "Warning: only a subset of rows are displayed and included in total calculations"));
		} else {
			total = String.valueOf(rows);
		}
		log.info("Folio " + folio.getFolioId() + " has " + total + " rows");

		if(ConvertUtils.getBooleanSafely(folio.getDirective("show-count"), true)) {
			String content;
			if(info.targetType.isChart())
				content = ConvertUtils.formatObject(new Object[] { chartCount }, info.report.getVerbage("charts-summary-format", "MESSAGE:Created {0} chart{0,CHOICE,0#s|1#|2#s}"));
			else
				content = ConvertUtils.formatObject(new Object[] { 1, rows, total }, info.report.getVerbage("rows-summary-format", "MESSAGE:Showing rows {0} to {1} of {2}"));
			info.serializer.putElement("span", attr().add("class", "rowcount"), content);
		}
	}

	/**
	 * @throws SAXException
	 */
	protected boolean shouldProcessPillar(GenerateInfo info, Pillar pillar) throws SAXException {
		if(!info.targetType.isChart())
			return true;
		switch(pillar.getPillarType()) {
			case CLUSTER:
				return false;
			case VALUE:
				return false;
		}
		return true;
	}

	protected ResultsReader toReader(Pillar pillar, ColumnNamer namer) {
		FieldOrder[] fos = pillar.getFieldOrders();
		if(fos.length == 1) {
			return new SingleColumnResultsReader(namer.getDisplayColumnName(fos[0]), namer.getSortColumnName(fos[0]), pillar.getDisplayFormat(), fos[0].getAggregateType());
		}
		String[] displayColumns = new String[fos.length];
		String[] sortColumns = new String[fos.length];
		Aggregate[] aggregates = new Aggregate[fos.length];
		for(int i = 0; i < fos.length; i++) {
			displayColumns[i] = namer.getDisplayColumnName(fos[i]);
			sortColumns[i] = namer.getSortColumnName(fos[i]);
			aggregates[i] = fos[i].getAggregateType();
		}
		return new MultiColumnResultsReader(displayColumns, sortColumns, pillar.getDisplayFormat(), aggregates);
	}

	protected FieldOrder getFirstNumberField(FieldOrder[] fos) {
		for(int k = 0; k < fos.length; k++) {
			if(isNumberField(fos[k].getField()))
				return fos[k];
		}
		return null;
	}

	protected FieldOrder getFirstField(FieldOrder[] fieldOrders, DataGenre[] acceptedGenres) {
		for(FieldOrder fo : fieldOrders) {
			for(DataGenre genre : acceptedGenres) {
				switch(genre) {
					case NUMBER:
						if(isNumberField(fo.getField()))
							return fo;
						break;
					case DATE:
						if(isDateField(fo.getField()))
							return fo;
						break;
					default:
						return fo;
				}
			}

		}
		return null;
	}

	protected boolean isNumberField(Field f) {
		Class<?> cl = SQLTypeUtils.getJavaType(f.getDisplaySqlType().getTypeCode());
		cl = ConvertUtils.convertToWrapperClass(cl);
		return Number.class.isAssignableFrom(cl);
	}

	protected boolean isDateField(Field f) {
		Class<?> cl = SQLTypeUtils.getJavaType(f.getDisplaySqlType().getTypeCode());
		cl = ConvertUtils.convertToWrapperClass(cl);
		return Date.class.isAssignableFrom(cl) || Calendar.class.isAssignableFrom(cl);
	}

	protected DisplayType getDisplayType(Map<String, Object> parameters, Scene scene, TargetType targetType) {
		try {
			String dis = ConvertUtils.getString(parameters.get("displayType"), false);
			if(dis != null)
				for(DisplayType d : DisplayType.values()) {
					if(d.toString().equalsIgnoreCase(dis))
						return d;
				}
		} catch(ConvertException e) {
		}
		switch(targetType) {
			case HTML:
			case CHART_HTML:
				log.debug("User Agent='" + scene.getUserAgent() + "'");
				if(scene.getUserAgent() != null) {
					if(EngineUtils.isDataUriSupported(scene.getUserAgent()))
						return DisplayType.INLINE_IMAGE;
					return DisplayType.INLINE_REQUEST;
				}
				return DisplayType.INLINE_IMAGE; // DisplayType.INLINE_OBJECT; // DisplayType.UNKNOWN;
			case CHART_DOC:
			case CHART_XLS:
			case CHART_PPT:
			case CHART_MHTML:
			case MHTML:
				return DisplayType.MHTML;
			case CHART_PDF:
				return DisplayType.INLINE_IMAGE;
			default:
				return DisplayType.INLINE_IMAGE;
		}
	}

	protected void prepareChart(GenerateInfo info, ChartInfo chartInfo, Folio folio) throws SAXException {
		String chartType = folio.getChartType();
		if(chartType == null)
			chartType = DEFAULT_CHART_TYPE;
		ChartType ct = ChartFactory.getChartType(chartType);
		chartInfo.chartMaker = ct.newChartMaker();
		String config = folio.getDirective("chart-config-settings");
		if(config != null) {
			Map<String, Object> props = new HashMap<String, Object>();
			try {
				StringUtils.intoMap(props, config);
			} catch(ParseException e) {
				throw new SAXException(e);
			}
			for(Map.Entry<String, Object> entry : props.entrySet()) {
				chartInfo.chartMaker.setProperty(entry.getKey(), entry.getValue());
			}
		}
		List<GenericAxis> axes = new ArrayList<GenericAxis>(ct.getAxes().size());
		for(Axis axis : ct.getAxes()) {
			if(axis instanceof GenericAxis) {
				axes.add((GenericAxis) axis);
			}
		}

		int ci = 0;
		int vi = 0;
		chartInfo.dynamicLabelClusters = new HashMap<String, Pillar>();
		for(Pillar pillar : folio.getPillars()) {
			switch(pillar.getPillarType()) {
				case CLUSTER:
					LOOP: for(; ci < axes.size(); ci++) {
						GenericAxis ga = axes.get(ci);
						switch(ga.getGenericAxisType()) {
							case CLUSTER_AND_VALUE:
							case CLUSTER_LEGEND:
							case CLUSTER:
								String axisName = ga.getName();
								String label;
								if(pillar.getLabelFormat() instanceof LiteralFormat) {
									label = pillar.getLabelFormat().format(null);
								} else if(pillar.getLabelFormat() == null) {
									label = "";
								} else {
									label = null;
									chartInfo.dynamicLabelClusters.put(axisName, pillar);
								}
								chartInfo.chartMaker.setAxisData(axisName, label, toReader(pillar, info.namer), pillar.getDisplayFormat());
								axes.remove(ci);
								if(ci < vi)
									vi--;
								break LOOP;
						}
					}
					break;
				case VALUE:
					LOOP: for(; vi < axes.size(); vi++) {
						GenericAxis ga = axes.get(vi);
						switch(ga.getGenericAxisType()) {
							case CLUSTER_AND_VALUE:
							case VALUE_LEGEND:
							case VALUE:
								// determine which field order to use (first one that is a number)
								if(pillar.getFieldOrders() != null && pillar.getFieldOrders().length > 0) {
									FieldOrder fo = getFirstField(pillar.getFieldOrders(), ga.getAcceptedGenre());
									if(fo != null) {
										chartInfo.chartMaker.setAxisData(ga.getName(), ConvertUtils.formatObject(null, pillar.getLabelFormat()), new SingleColumnResultsReader(info.namer.getDisplayColumnName(fo), info.namer.getSortColumnName(fo), pillar.getDisplayFormat(), fo.getAggregateType()), pillar.getDisplayFormat());
										axes.remove(vi);
										if(vi < ci)
											ci--;
										break LOOP;
									}
								}
								break;
						}
					}
					break;
			}
		}
	}

	protected void generateGroupHeader(GenerateInfo info, FolioInfo folioInfo, Results results, String folioLabel, List<PillarInfo> group, int tableColumnCount, int groupColumns, int level, Set<Note> noteValues) throws SAXException {
		StringBuilder defaultStyle = new StringBuilder();
		String defaultAction = null;
		String defaultHelp = null;
		List<PillarData> groupData = new ArrayList<PillarData>();
		for(PillarInfo pi : group) {
			PillarData data = new PillarData(info, folioInfo, pi, results, level, null);
			if(pi.pillar.getPillarType() == PillarType.ROW) {
				if(!StringUtils.isBlank(data.style)) {
					if(defaultStyle.length() > 0 && !defaultStyle.toString().trim().endsWith(";"))
						defaultStyle.append("; ");
					defaultStyle.append(data.style);
				}
				if(defaultAction == null && !StringUtils.isBlank(data.action)) {
					defaultAction = data.action;
					defaultHelp = data.help;
				}
			} else if(pi.pillar.getPillarType() == PillarType.NOTE) {
				noteValues.add(new Note(data));
			} else if(!folioInfo.dynamiclabels || !StringUtils.isBlank(data.label) || !StringUtils.isBlank(data.display)) {
				groupData.add(data);
			}
		}
		if(groupData.isEmpty())
			return;
		info.serializer.startElement("tbody");
		AppendableAttributes aa = attr().add("class", "groupHeaderRow" + (level - 2));
		if(defaultStyle.length() > 0)
			aa.add("style", defaultStyle.toString());
		info.serializer.startElement("tr", aa);
		if(groupData.size() > 1) {
			info.serializer.startElement("td", attr().add("colspan", String.valueOf(tableColumnCount)));
			info.serializer.startElement("table", attr().add("class", "groupInfo"));
			info.serializer.startElement("tbody");
			int groupRows = (int) Math.ceil(groupData.size() / (double) groupColumns);
			for(int r = 0; r < groupRows; r++) {
				info.serializer.startElement("tr");
				for(int c = 0; c < groupColumns; c++) {
					int groupIndex = (c * groupRows) + r;
					if(groupIndex >= groupData.size())
						break;
					PillarData data = groupData.get(groupIndex);
					aa = attr().add("class", "colId_" + data.pillarInfo.pillarIndex);
					if(!StringUtils.isBlank(data.style))
						aa.add("style", data.style);
					info.serializer.startElement("td", aa);
					info.serializer.startElement("span", attr().add("class", "groupLabel").add("title", data.pillarInfo.pillar.getDescription()));
					if(!StringUtils.isBlank(data.label)) {
						info.serializer.characters(data.label);
						info.serializer.characters(":");
					}
					info.serializer.startElement("span", attr().add("class", "groupValue"));
					generateTableCell(info, data, defaultAction, defaultHelp, folioLabel);
					info.serializer.endElement("span");
					info.serializer.endElement("span");
					info.serializer.endElement("td");

				}
				info.serializer.endElement("tr");
			}
			info.serializer.endElement("tbody");
			info.serializer.endElement("table");
		} else {
			PillarData data = groupData.get(0);
			aa = attr().add("colspan", String.valueOf(tableColumnCount));
			if(!StringUtils.isBlank(data.style))
				aa.add("style", data.style);
			info.serializer.startElement("td", aa);
			info.serializer.startElement("span", attr().add("class", "groupLabel").add("title", data.pillarInfo.pillar.getDescription()));
			if(!StringUtils.isBlank(data.label)) {
				info.serializer.characters(data.label);
				info.serializer.characters(":");
			}
			info.serializer.startElement("span", attr().add("class", "groupValue"));
			generateTableCell(info, data, defaultAction, defaultHelp, folioLabel);
			info.serializer.endElement("span");
			info.serializer.endElement("span");
		}
		info.serializer.endElement("td");
		info.serializer.endElement("tr");
		info.serializer.endElement("tbody");
	}

	/* Much time was spent testing various options for chart display in various browsers. The <img src="data:image/png;base64,..."/> works very well
	 * in FF, IE9, Chrome, Safari. IE8 and IE7 have many issues though:
	 * 1.) Not all PNG images are rendered. Often just a red X is displayed. This is on images that render fine in all other programs. There are many posts
	 *     about this on the Internal with a whole host of suggestions - none of which fixed the issue I saw
	 * 2.) IE8 can not render img's with uri's whose length >= 32768. PNG images are not displayed at all. GIF images are blurred, JPEG images are cropped.
	 * 3.) The iframe with form post to a base64 converter works for all tested browsers, but obviously is less desirable because it involves hitting the server
	 * 4.) Even with the above mentioned method, IE7 cuts off the bottom of the image
	 * 5.) A <object type="image/gif" data="data:image/gif;base64,..."/> in IE8 does not display in the print preview or when the document is printed
	 * 
	 * So, when the chart could display in multiple browsers we use a gif image with some javascript to handle IE 8 and below differently
	 * 
	 */
	protected void makeChart(GenerateInfo info, Folio folio, Results subResults, String folioLabel, ChartInfo chartInfo, DisplayType displayType) throws SAXException {
		if(subResults == null)
			return;
		try {
			for(Map.Entry<String, Pillar> entry : chartInfo.dynamicLabelClusters.entrySet()) {
				Pillar pillar = entry.getValue();
				String label = pillar.getLabelFormat().format(pillar.getDisplayValueRetriever().getAggregate(subResults, 0, info.parameters));
				chartInfo.chartMaker.getAxisData(entry.getKey()).setLabel(label);
			}

			JFreeChart chart = chartInfo.chartMaker.makeChart(subResults);
			// Adjust Look
			ChartTheme chartTheme = new DirectChartTheme();
			chartTheme.apply(chart);

			// Determine size
			Map<String, Object> drawSettings = new HashMap<String, Object>();
			Dimension size = ChartUtils.calculateSize(chart, drawSettings);
			AppendableAttributes aa = attr().add("width", String.valueOf(size.width) + "px").add("height", String.valueOf(size.height) + "px");
			if(info.targetType == TargetType.CHART_XLS)
				info.serializer.addAttribute(getNameSpaceURI(), "style", "style", "height: " + size.height + "px; width: " + size.width + "px;");
			// we'll have to store the img bytes in memory for this
			// String contentType = "image/gif";
			String contentType = "image/png";
			// String contentType = "image/jpeg";
			final OutputStream chartOutputStream;
			final String chartId = String.valueOf(chartInfo.chartNumber++);
			switch(displayType) {
				case INLINE_IMAGE:
					info.serializer.startElement("img", aa);
					Writer dataWriter = info.serializer.addAttributeByStream(null, null, "src");
					dataWriter.append("data:").append(contentType).append(";base64,");
					chartOutputStream = new Base64EncodingOutputStream(dataWriter, null);
					break;
				case INLINE_APPLET:
					info.serializer.startElement("object", aa.add("codetype", "application/java").add("code", "helpers.ImageDisplayer").add("codebase", "code"));
					info.serializer.putElement("param", attr().add("name", "data"));
					dataWriter = info.serializer.addAttributeByStream(null, null, "value");
					dataWriter.append("data:").append(contentType).append(";base64,");
					chartOutputStream = new Base64EncodingOutputStream(dataWriter, null);
					break;
				case MHTML:
					contentType = "image/gif";// since IE 7 has trouble with png's
					String name = "chart_img_" + chartId;
					info.serializer.putElement("img", aa.add("src", "cid:" + name));
					BufferStreamDataSource ds = new BufferStreamDataSource(name, contentType);
					((MHtmlSerializer) info.serializer).mimeParts.add(ds);
					chartOutputStream = ds.getOutputStream();
					break;
				case UNKNOWN:
					info.serializer.putElement("img", aa.add("id", "chart_img_" + chartId));
					info.serializer.startElement("script", attr().add("type", "text/javascript"));
					info.serializer.characters("\nwindow.addEvent('domready', function() {\nvar b64 = \"");
					chartOutputStream = new Base64EncodingOutputStream(info.serializer.getWriter(), null);
					break;
				case INLINE_REQUEST:
					info.serializer.putElement("iframe", aa.add("name", "chart_frame_" + chartId).add("frameborder", "no").add("scrolling", "no").add("marginheight", "0").add("marginwidth", "0"));
					// Use "POST" method because IE limits the length of get requests to 2048 bytes (which includes the entire URL string)
					info.serializer.startElement("form", attr().add("id", "form_chart_frame_" + chartId).add("action", "b64").add("target", "chart_frame_" + chartId).add("method", "post"));
					info.serializer.putElement("input", attr().add("type", "hidden").add("name", "content-type").add("value", contentType));
					info.serializer.startElement("input", attr().add("type", "hidden").add("name", "data"));
					dataWriter = info.serializer.addAttributeByStream(null, null, "value");
					chartOutputStream = new Base64EncodingOutputStream(dataWriter, null);
					break;
				case INLINE_OBJECT:
					info.serializer.startElement("object", aa.add("contentType", contentType));
					dataWriter = info.serializer.addAttributeByStream(null, null, "data");
					dataWriter.append("data:").append(contentType).append(";base64,");
					chartOutputStream = new Base64EncodingOutputStream(dataWriter, null);
					break;
				default:
					info.serializer.characters(translate(info.scene, "report-cannot-display-image", "Cannot display image"));
					chartOutputStream = null;
			}
			if(chartOutputStream != null) {
				long start = System.currentTimeMillis();
				if("image/png".equals(contentType))
					ChartUtilities.writeChartAsPNG(chartOutputStream, chart, size.width, size.height);
				else if("image/jpeg".equals(contentType))
					ChartUtilities.writeChartAsJPEG(chartOutputStream, chart, size.width, size.height);
				else if("image/gif".equals(contentType))
					ChartUtils.writeChartAsGIF(chartOutputStream, chart, size.width, size.height);
				log.debug("Wrote chart in " + (System.currentTimeMillis() - start) + " milliseconds");
				chartOutputStream.flush();
				switch(displayType) {
					case INLINE_IMAGE:
						chartOutputStream.close();
						info.serializer.endElement("img");
						break;
					case INLINE_APPLET:
						chartOutputStream.close();
						info.serializer.endElement("object");
						break;
					case UNKNOWN:
						info.serializer.characters("\";\nvar img = $(\"chart_img_");
						info.serializer.characters(chartId);
						info.serializer.characters("\"); if(Browser.ie && Browser.version <= 8) {\nvar iframe = new Element(\"iframe\", {name: \"chart_frame_");
						info.serializer.characters(chartId);
						info.serializer.characters("\", frameborder: \"no\", scrolling: \"no\", marginheight: \"0\", marginwidth: \"0\", width: img.width, height: img.height});\n");
						info.serializer.characters("var form = new Element(\"form\", {id: \"form_chart_frame_");
						info.serializer.characters(chartId);
						info.serializer.characters("\", action: \"b64\", target: \"chart_frame_");
						info.serializer.characters(chartId);
						info.serializer.characters("\", method: \"post\"});\n");
						info.serializer.characters("form.adopt(new Element(\"input\", {type: \"hidden\", name: \"content-type\", value: \"");
						info.serializer.characters(contentType);
						info.serializer.characters("\"}), new Element(\"input\", {type: \"hidden\", name: \"data\", value: b64}), iframe);\n");
						info.serializer.characters("form.replaces(img); form.submit();\n");
						info.serializer.characters("} else {\n");
						info.serializer.characters("img.src = \"data:");
						info.serializer.characters(contentType);
						info.serializer.characters(";base64,\" + b64;\n}\n});\n");
						info.serializer.endElement("script");
						break;
					case INLINE_REQUEST:
						chartOutputStream.close();
						info.serializer.endElement("input");
						info.serializer.endElement("form");
						info.serializer.putElement("script", attr().add("type", "text/javascript").add("defer", "defer"), "document.getElementById(\"form_chart_frame_" + chartId + "\").submit();");
						break;
					case INLINE_OBJECT:
						chartOutputStream.close();
						info.serializer.characters("This report is not compatible with your browser. Please upgrade.");
						info.serializer.endElement("object");
						break;
				}
			}
		} catch(ChartCreationException e) {
			log.warn("Could not create chart", e);
			throw new SAXException(e);
		} catch(IllegalArgumentException e) {
			log.warn("Could not create chart", e);
			throw new SAXException(e);
		} catch(IOException e) {
			log.warn("Could not create chart", e);
			throw new SAXException(e);
		}
	}

	protected String getJavascriptSortType(DataGenre sortType) {
		if(sortType == null)
			sortType = DataGenre.STRING;
		else if(sortType == DataGenre.DATE)
			sortType = DataGenre.NUMBER;
		return sortType.name();
	}

	protected void generateTableHeader(GenerateInfo info, Folio folio, String folioLabel, List<PillarInfo> tablePillars, List<PillarInfo> groupZero, SortedSet<XTabData> xtabData) throws SAXException {
		boolean hitXtab = (xtabData == null || xtabData.isEmpty());
		for(PillarInfo pi : tablePillars) {
			switch(pi.pillar.getPillarType()) {
				case ROW:
				case NOTE:
					break; // ignore these
				default:
					if(pi.pillar.getGroupingLevel() == 0 && !xtabData.isEmpty()) {
						AppendableAttributes aa = attr().add("class", "colId_" + pi.pillarIndex);
						if(info.targetType == TargetType.XLS && pi.notePillarInfo != null) {
							int c = 2;
							for(PillarInfo npi = pi.notePillarInfo.notePillarInfo; npi != null; npi = npi.notePillarInfo)
								c++;
							aa.add("colspan", String.valueOf(c));
						}
						info.serializer.startElement("th", aa);
						info.serializer.startElement("a", attr().add("data-toggle", "sort").add("data-sort-type", getSortTypeAttributeValue(pi.pillar.getSortType())).add("title", "Sort Rows" + (StringUtils.isBlank(pi.label) ? "" : " by " + pi.label)));
						info.serializer.characters(translate(info.scene, "report-xtab-total-column-label", "Total"));
						info.serializer.characters(SP);
						info.serializer.characters(pi.label);
						info.serializer.endElement("a");
						info.serializer.endElement("th");
						if(pi.pillar.getPercentFormat() != null && info.targetType == TargetType.XLS) {
							info.serializer.startElement("th");
							info.serializer.startElement("a");
							info.serializer.characters(translate(info.scene, "report-xtab-percent-column-label", "%"));
							info.serializer.characters(SP);
							info.serializer.characters(pi.label);
							info.serializer.endElement("a");
							info.serializer.endElement("th");
						}
					} else if(pi.pillar.getGroupingLevel() != 1) {
						AppendableAttributes aa = attr().add("class", "colId_" + pi.pillarIndex);
						if(info.targetType == TargetType.XLS && pi.notePillarInfo != null) {
							int c = 2;
							for(PillarInfo npi = pi.notePillarInfo.notePillarInfo; npi != null; npi = npi.notePillarInfo)
								c++;
							aa.add("colspan", String.valueOf(c));
						}
						info.serializer.startElement("th", aa);
						info.serializer.putElement("a", attr().add("data-toggle", "sort").add("data-sort-type", getSortTypeAttributeValue(pi.pillar.getSortType())).add("title", "Sort Rows" + (StringUtils.isBlank(pi.label) ? "" : " by " + pi.label)), pi.label);
						info.serializer.endElement("th");
						if(pi.pillar.getPercentFormat() != null && info.targetType == TargetType.XLS) {
							info.serializer.startElement("th");
							info.serializer.startElement("a");
							info.serializer.characters(translate(info.scene, "report-xtab-percent-column-label", "%"));
							info.serializer.characters(SP);
							info.serializer.characters(pi.label);
							info.serializer.endElement("a");
							info.serializer.endElement("th");
						}
					} else if(!hitXtab) {
						hitXtab = true;
						for(XTabData xtab : xtabData) {
							for(PillarInfo pillarInfo : groupZero) {
								switch(pillarInfo.pillar.getPillarType()) {
									case ROW:
									case NOTE:
										break; // ignore these
									default:
										AppendableAttributes aa = attr().add("class", "colId_" + pillarInfo.pillarIndex + '_' + xtab.pillarInfo.pillarIndex);
										if(info.targetType == TargetType.XLS && pillarInfo.notePillarInfo != null) {
											int c = 2;
											for(PillarInfo npi = pillarInfo.notePillarInfo.notePillarInfo; npi != null; npi = npi.notePillarInfo)
												c++;
											aa.add("colspan", String.valueOf(c));
										}
										info.serializer.startElement("th", aa);
										info.serializer.startElement("a", attr().add("data-toggle", "sort").add("data-sort-type", getSortTypeAttributeValue(pillarInfo.pillar.getSortType())).add("title", "Sort Rows" + (StringUtils.isBlank(xtab.display) ? "" : " by " + xtab.display + (StringUtils.isBlank(pillarInfo.label) ? "" : " " + pillarInfo.label))));
										if(xtab.pillarInfo.pillar.getPillarType() == PillarType.IMAGE) {
											info.serializer.putElement("img", attr().add("src", xtab.display).add("alt", xtab.help));
										} else {
											info.serializer.characters(xtab.display);
										}
										info.serializer.characters(SP);
										info.serializer.characters(pillarInfo.label);
										info.serializer.endElement("a");
										info.serializer.endElement("th");
										if(pillarInfo.pillar.getPercentFormat() != null && info.targetType == TargetType.XLS) {
											info.serializer.startElement("th");
											info.serializer.startElement("a");
											if(xtab.pillarInfo.pillar.getPillarType() == PillarType.IMAGE) {
												info.serializer.putElement("img", attr().add("src", xtab.display).add("alt", xtab.help));
											} else {
												info.serializer.characters(xtab.display);
											}
											info.serializer.characters(SP);
											info.serializer.characters(pillarInfo.label);
											info.serializer.characters(" %");
											info.serializer.endElement("a");
											info.serializer.endElement("th");
										}
								}
						}
					}
				}
			}
		}
	}

	protected String getSortTypeAttributeValue(DataGenre sortType) {
		if(sortType == null)
			return DataGenre.STRING.toString();
		if(sortType == DataGenre.DATE)
			return DataGenre.NUMBER.toString();
		return sortType.toString();
	}

	protected Justification getColumnAlignment(GenerateInfo info, Pillar pillar) {
		switch(pillar.getPillarType()) {
			case IMAGE:
			case BUTTON:
			case CHECKBOX:
			case RADIO:
				return Justification.CENTER;
		}
		if(pillar.getSortType() == DataGenre.NUMBER)
			return Justification.RIGHT;
		return Justification.LEFT;
	}

	protected void generateTableColumns(GenerateInfo info, Folio folio, String folioLabel, List<PillarInfo> tablePillars, List<PillarInfo> groupZero, SortedSet<XTabData> xtabData) throws SAXException {
		switch(info.targetType) {
			case HTML:
				info.serializer.startElement("style", attr().add("type", "text/css"));
				info.serializer.characters(NL);
				boolean hitXtab = (xtabData == null || xtabData.isEmpty());
				for(PillarInfo pi : tablePillars) {
					if(pi.pillar.getPillarType() == PillarType.NOTE || pi.pillar.getPillarType() == PillarType.ROW)
						continue;
					if(pi.pillar.getGroupingLevel() != 1) {
						info.serializer.characters("#folio_");
						info.serializer.characters(folioLabel);
						info.serializer.characters(" td.colId_");
						info.serializer.characters(String.valueOf(pi.pillarIndex));
						info.serializer.characters(" { text-align:");
						info.serializer.characters(getColumnAlignment(info, pi.pillar).toString().toLowerCase());
						info.serializer.characters("; }\n");
					} else if(!hitXtab) {
						hitXtab = true;
						for(PillarData xtab : xtabData) {
							for(PillarInfo pillarInfo : groupZero) {
								switch(pillarInfo.pillar.getPillarType()) {
									case ROW:
									case NOTE:
										break; // ignore these
									default:
										info.serializer.characters("#folio_");
										info.serializer.characters(folioLabel);
										info.serializer.characters(" td.colId_");
										info.serializer.characters(String.valueOf(pillarInfo.pillarIndex));
										info.serializer.characters(UN);
										info.serializer.characters(String.valueOf(xtab.pillarInfo.pillarIndex));
										info.serializer.characters(" { text-align:");
										info.serializer.characters(getColumnAlignment(info, pillarInfo.pillar).toString().toLowerCase());
										info.serializer.characters("; }\n");
								}
							}
						}
					}
				}
				info.serializer.endElement("style");
				break;
			case XLS:
				info.serializer.startElement("style", attr().add("type", "text/css"));
				info.serializer.characters(NL);
				hitXtab = (xtabData == null || xtabData.isEmpty());
				for(PillarInfo pi : tablePillars) {
					if(pi.pillar.getPillarType() == PillarType.NOTE || pi.pillar.getPillarType() == PillarType.ROW)
						continue;
					// Use mso-number-format:"mm\/dd\/yyyy" to style all date and number fields (See
					// http://cosicimiento.blogspot.com/2008/11/styling-excel-cells-with-mso-number.html)
					// NOTE: setting the width attribute on these seems to have no effect
					if(pi.pillar.getGroupingLevel() != 1) {
						info.serializer.characters("#folio_");
						info.serializer.characters(folioLabel);
						info.serializer.characters(" td.colId_");
						info.serializer.characters(String.valueOf(pi.pillarIndex));
						info.serializer.characters(" { text-align:");
						info.serializer.characters(getColumnAlignment(info, pi.pillar).toString().toLowerCase());
						info.serializer.characters("; ");
						switch(pi.pillar.getSortType()) {
							case NUMBER:
								if(pi.pillar.getDisplayFormat() == null)
									info.serializer.characters("mso-number-format: General;");
								break;
							case STRING:
							case OTHER:
							case ARRAY:
								info.serializer.characters("mso-number-format: \"\\@\"; ");
								break;

						}
						info.serializer.characters("}\n");
						/*
						AppendableAttributes aa = attr().add("align", getColumnAlignment(info, pi.pillar).toString().toLowerCase());
						// if(!StringUtils.isBlank(pi.pillar.getWidth()))
						// aa.add("width", pi.pillar.getWidth());
						info.serializer.putElement("col", aa);
						if(pi.notePillarInfo != null) {
							for(PillarInfo npi = pi.notePillarInfo; npi != null; npi = npi.notePillarInfo)
								info.serializer.putElement("col"); // , attr().add("width", String.valueOf(npi.maxChars) + "em"));
						}
						if(pi.pillar.getPercentFormat() != null)
							info.serializer.putElement("col", attr().add("align", "right"));
						*/
					} else if(!hitXtab) {
						hitXtab = true;
						for(PillarData xtab : xtabData) {
							for(PillarInfo pillarInfo : groupZero) {
								switch(pillarInfo.pillar.getPillarType()) {
									case ROW:
									case NOTE:
										break; // ignore these
									default:
										info.serializer.characters("#folio_");
										info.serializer.characters(folioLabel);
										info.serializer.characters(" td.colId_");
										info.serializer.characters(String.valueOf(pillarInfo.pillarIndex));
										info.serializer.characters(UN);
										info.serializer.characters(String.valueOf(xtab.pillarInfo.pillarIndex));
										info.serializer.characters(" { text-align:");
										info.serializer.characters(getColumnAlignment(info, pillarInfo.pillar).toString().toLowerCase());
										info.serializer.characters("; mso-number-format:");
										switch(pillarInfo.pillar.getSortType()) {
											case NUMBER:
												if(pillarInfo.pillar.getDisplayFormat() == null)
													info.serializer.characters("General");
												break;
											case STRING:
											case OTHER:
											case ARRAY:
												info.serializer.characters("\"\\@\"");
												break;

										}
										info.serializer.characters("; }\n");
										/*
										AppendableAttributes aa = attr().add("align", getColumnAlignment(info, pillarInfo.pillar).toString().toLowerCase());
										// if(!StringUtils.isBlank(pillarInfo.pillar.getWidth()))
										// aa.add("width", pillarInfo.pillar.getWidth());
										info.serializer.putElement("col", aa);
										if(pillarInfo.notePillarInfo != null) {
											for(PillarInfo npi = pillarInfo.notePillarInfo; npi != null; npi = npi.notePillarInfo)
												info.serializer.putElement("col"); // , attr().add("width", String.valueOf(npi.maxChars) + "em"));
										}
										if(pillarInfo.pillar.getPercentFormat() != null)
											info.serializer.putElement("col", attr().add("align", "right"));
											*/
								}
							}
						}
					}
				}
				info.serializer.endElement("style");
				break;
		}
	}

	protected void generateTableTotals(GenerateInfo info, FolioInfo folioInfo, String folioLabel, List<PillarInfo> detailPillars, List<PillarInfo> groupZero, SortedSet<XTabData> xtabData, Results results, int level, int startIndex) throws SAXException {
		boolean hitXtab = (xtabData == null || xtabData.isEmpty());
		StringBuilder defaultStyle = new StringBuilder();
		String defaultAction = null;
		String defaultHelp = null;
		List<PillarData> tableData = new ArrayList<PillarData>();
		int columnIndex = 0;
		for(PillarInfo pi : detailPillars) {
			if(pi.pillar.getGroupingLevel() != 1) {
				if(pi.pillar.getPillarType() == PillarType.ROW) {
					PillarData data = new PillarData(info, folioInfo, pi, results, level, null);
					if(!StringUtils.isBlank(data.style)) {
						if(defaultStyle.length() > 0 && !defaultStyle.toString().trim().endsWith(";"))
							defaultStyle.append("; ");
						defaultStyle.append(data.style);
					}
					if(defaultAction == null && !StringUtils.isBlank(data.action)) {
						defaultAction = data.action;
						defaultHelp = data.help;
					}
				} else if(pi.pillar.getPillarType() == PillarType.NOTE) {
					// PillarData data = new PillarData(info, pi, results, level, null);
					// if(!StringUtils.isBlank(data.display))
					// info.noteValues.add(new Note(data));
				} else if(columnIndex++ >= startIndex) {
					if(pi.pillar.getGroupingLevel() == 0 || pi.pillar.isAggregated()) {
						PillarData data = new PillarData(info, folioInfo, pi, results, level, null);
						String action = data.getAction(); 
						if (action != null) {
							// USAT-587 Transactions in Payment not working bug fix
							int index = action.indexOf("params.PaymentBatchId=");
							if (index > -1) {
								StringBuilder newAction = new StringBuilder(action.substring(0, index)).append("params.PaymentBatchId=");
								int ampIndex = action.indexOf("&", index);
								if (ampIndex > index)
									newAction.append(action.substring(ampIndex));
								data.setAction(newAction.toString());
							}
					}
						tableData.add(data);
					} else
						tableData.add(null);
				}
			} else if(!hitXtab) {
				hitXtab = true;
				for(XTabData xtab : xtabData) {
					for(PillarInfo pillarInfo : groupZero) {
						switch(pillarInfo.pillar.getPillarType()) {
							case ROW:
							case NOTE:
								break; // ignore these
							default:
								if(columnIndex++ >= startIndex) {
									PillarData data = new PillarData(info, folioInfo, pillarInfo, results, level, xtab);
									tableData.add(data);
								}
						}
					}
				}
			}
		}
		AppendableAttributes aa;
		for(PillarData data : tableData) {
			if(data == null)
				info.serializer.putElement("td", attr().add("data-sort-value", ""));
			else {
				String colId;
				if(data.xtab == null)
					colId = String.valueOf(data.pillarInfo.pillarIndex);
				else
					colId = String.valueOf(data.pillarInfo.pillarIndex) + '_' + data.xtab.pillarInfo.pillarIndex;
				aa = attr().add("class", "colId_" + colId).add("data-sort-value", data.sort);
				if(!StringUtils.isBlank(data.style))
					aa.add("style", data.style);
				info.serializer.startElement("td", aa);
				generateTableCell(info, data, defaultAction, defaultHelp, folioLabel);
				if(data.pillarInfo.pillar.getPercentFormat() != null) {
					if(info.targetType == TargetType.XLS) {
						info.serializer.endElement("td");
						info.serializer.startElement("td");
						if(!StringUtils.isBlank(data.percent))
							info.serializer.putElement("span", attr().add("class", "percentage"), data.percent);
					} else if(!StringUtils.isBlank(data.percent)) {
						info.serializer.characters(NBSP);
						info.serializer.startElement("span", attr().add("class", "percentage"));
						info.serializer.characters("(");
						info.serializer.characters(NBSP);
						info.serializer.characters(data.percent);
						info.serializer.characters(NBSP);
						info.serializer.characters(")");
						info.serializer.endElement("span");
					}
				}
				info.serializer.endElement("td");
			}
		}
	}

	protected void generateTableData(GenerateInfo info, FolioInfo folioInfo, String folioLabel, List<PillarInfo> detailPillars, List<PillarInfo> groupZero, SortedSet<XTabData> xtabData, Results results, boolean odd) throws SAXException {
		boolean hitXtab = (xtabData == null || xtabData.isEmpty());
		StringBuilder defaultStyle = new StringBuilder();
		String defaultAction = null;
		String defaultHelp = null;
		List<PillarData> tableData = new ArrayList<PillarData>();
		for(PillarInfo pi : detailPillars) {
			if(pi.pillar.getGroupingLevel() != 1) {
				PillarData data = new PillarData(info, folioInfo, pi, results, 2, null);
				if(pi.pillar.getPillarType() == PillarType.ROW) {
					if(!StringUtils.isBlank(data.style)) {
						if(defaultStyle.length() > 0 && !defaultStyle.toString().trim().endsWith(";"))
							defaultStyle.append("; ");
						defaultStyle.append(data.style);
					}
					if(defaultAction == null && !StringUtils.isBlank(data.action)) {
						defaultAction = data.action;
						defaultHelp = data.help;
					}
				} else if(pi.pillar.getPillarType() == PillarType.NOTE) {
					// if(!StringUtils.isBlank(data.display))
					// info.noteValues.add(new Note(data));
				} else {
					tableData.add(data);
				}
			} else if(!hitXtab) {
				hitXtab = true;
				for(XTabData xtab : xtabData) {
					for(PillarInfo pillarInfo : groupZero) {
						switch(pillarInfo.pillar.getPillarType()) {
							case ROW:
							case NOTE:
								break; // ignore these
							default:
								PillarData data = new PillarData(info, folioInfo, pillarInfo, results, 2, xtab);
								tableData.add(data);
						}
					}
				}
			}
		}
		AppendableAttributes aa = attr();
		if(defaultAction != null)
			aa.add("class", odd ? "oddRowLink" : "evenRowLink").add("onmouseover", "this.className=this.className.replace(/((?:odd|even|total)Row).*/, '$1Hover')").add("onmouseout", "this.className=this.className.replace(/((?:odd|even|total)Row).*/, '$1Link')");
		else
			aa.add("class", odd ? "oddRow" : "evenRow");
		if(defaultStyle.length() > 0)
			aa.add("style", defaultStyle.toString());

		info.serializer.startElement("tr", aa);
		for(PillarData data : tableData) {
			String colId;
			if(data.xtab == null)
				colId = String.valueOf(data.pillarInfo.pillarIndex);
			else
				colId = String.valueOf(data.pillarInfo.pillarIndex) + '_' + data.xtab.pillarInfo.pillarIndex;
			aa = attr().add("class", "colId_" + colId).add("data-sort-value", data.sort);
			if(!StringUtils.isBlank(data.style))
				aa.add("style", data.style);
			info.serializer.startElement("td", aa);
			generateTableCell(info, data, defaultAction, defaultHelp, folioLabel);
			if(data.pillarInfo.pillar.getPercentFormat() != null) {
				if(info.targetType == TargetType.XLS) {
					info.serializer.endElement("td");
					info.serializer.startElement("td");
					if(!StringUtils.isBlank(data.percent))
						info.serializer.putElement("span", attr().add("class", "percentage"), data.percent);
				} else if(!StringUtils.isBlank(data.percent)) {
					info.serializer.characters(NBSP);
					info.serializer.startElement("span", attr().add("class", "percentage"));
					info.serializer.characters("(");
					info.serializer.characters(NBSP);
					info.serializer.characters(data.percent);
					info.serializer.characters(NBSP);
					info.serializer.characters(")");
					info.serializer.endElement("span");
				}
			}
			info.serializer.endElement("td");
		}
		info.serializer.endElement("tr");
	}

	protected void generateTableCell(GenerateInfo info, PillarData data, String defaultAction, String defaultHelp, String folioLabel) throws SAXException {
		AppendableAttributes aa = attr();
		if(info.targetType == TargetType.XLS && !StringUtils.isBlank(data.style))
			aa.add("style", data.style);
		switch(data.pillarInfo.pillar.getPillarType()) {
			case BUTTON:
				aa.add("type", "button").add("value", data.display).add("title", data.help);
				if(!StringUtils.isBlank(data.action)) {
					if(data.action.startsWith("javascript:"))
						aa.add("onclick", data.action.substring("javascript:".length()).trim());
					else
						aa.add("onclick", data.action);
				}
				info.serializer.putElement("input", aa);
				break;
			case CHECKBOX:
			case RADIO:
				aa.add("type", data.pillarInfo.pillar.getPillarType().toString().toLowerCase()).add("name", data.display).add("value", data.sort).add("title", data.help);
				if(!StringUtils.isBlank(data.action)) {
					if(data.action.startsWith("javascript:"))
						aa.add("onclick", data.action.substring("javascript:".length()).trim());
					else
						aa.add("onclick", data.action);
				}
				info.serializer.putElement("input", aa);
				break;
			case ROW:
			case NOTE: // these should not be in the list
				throw new IllegalArgumentException("Table cells not generated for ROW or NOTE pillars at pillar index " + data.pillarInfo.pillarIndex);
			default:
				String action;
				String help;
				if(!StringUtils.isBlank(data.action) || StringUtils.isBlank(defaultAction)) {
					action = data.action;
					help = data.help;
				} else {
					action = defaultAction;
					help = defaultHelp;
				}
				String tag;
				if(!StringUtils.isBlank(help)) {
					switch(info.targetType) {
						case HTML:
						case CHART_HTML:
						case MHTML:
						case CHART_MHTML:
							tag = "span";
							help = StringUtils.prepareHTML(help);
							aa.add("data-toggle", "tip");
							aa.add("data-show-on-click", "true");
							aa.add("data-fixed", "true");
							aa.add("data-offset", "{x: 12, y: 5}");
							aa.add("data-window-padding", "{x: 10, y: 10}");
							aa.add("data-positions", "['centerRight','centerLeft', 'topCenter', 'bottomCenter']");
							break;
						case XLS:
						case CHART_XLS:
							tag = "a";
							if(StringUtils.isBlank(action))
								aa.add("href", "http:"); // this is the least obtrusive link I can find
							break;
						default:
							tag = "a";
							if(StringUtils.isBlank(action))
								aa.add("href", "");
					}

					aa.add("title", help);
				} else
					tag = "span";

				if(StringUtils.isBlank(action)) {
					info.serializer.startElement(tag, aa);
					generateTableCellData(info, data);
					info.serializer.endElement(tag);
				} else {
					if(action.startsWith("javascript:"))
						aa.add("onclick", action.substring("javascript:".length()).trim());
					else if(action.length() > 2048) {
						String[] parts = StringUtils.split(action, '?', 1);
						aa.add("onclick", "Browser.post('" + StringUtils.prepareScript(parts[0]) + "', '" + (parts.length > 1 ? StringUtils.prepareScript(parts[1]) : "") + "');");
					} else
						aa.add("href", action);
					info.serializer.startElement("a", aa);
					generateTableCellData(info, data);
					info.serializer.endElement("a");
				}

		}
		if(data.note != null)
			generateNotes(info, data.note, folioLabel);
	}

	protected void generateTableCellData(GenerateInfo info, PillarData data) throws SAXException {
		if(data.pillarInfo.pillar.getPillarType() == PillarType.IMAGE)
			info.serializer.putElement("img", attr().add("src", data.display).add("alt", data.help));
		else
			info.serializer.characters(data.display);
	}

	protected void generateNotes(GenerateInfo info, PillarData noteData, String folioLabel) throws SAXException {
		if(info.targetType == TargetType.XLS) {
			info.serializer.endElement("td");
			info.serializer.startElement("td");
			if(StringUtils.isBlank(noteData.display))
				info.serializer.characters(ZWNJ);
		}
		if(!StringUtils.isBlank(noteData.display)) {
			if(info.targetType != TargetType.XLS)
				info.serializer.characters(NBSP);
			AppendableAttributes aa = attr().add("title", noteData.help).add("class", "notes").add("id", "note" + folioLabel + UN + noteData.pillarInfo.pillarIndex);
			if(!StringUtils.isBlank(noteData.style))
				aa.add("style", noteData.style);
			info.serializer.putElement("sup", aa, noteData.display);
		}
		if(noteData.note != null)
			generateNotes(info, noteData.note, folioLabel);
	}

	/**
	 * @throws SAXException
	 */
	protected String getSubtitle(GenerateInfo info) {
		Report report = info.report;
		String title = report.getTitle(info.parameters);
		if(StringUtils.isBlank(title)) {
			title = report.getFolios()[0].getTitle(info.parameters);
			if(StringUtils.isBlank(title))
				title = translate(info.scene, "report-default-title", "Custom Report");
		}
		return title;
	}

	protected void generateBase(GenerateInfo info) throws SAXException {
		info.serializer.putElement("base", attr().add("href", info.scene.getBaseUrl()));
	}

	protected void generateStyles(GenerateInfo info) throws SAXException {
		if(info.scene.getStylesheets() != null)
			for(String stylesheet : info.scene.getStylesheets()) {
				if(!StringUtils.isBlank(stylesheet))
					info.serializer.putElement("link", attr().add("type", "text/css").add("rel", "stylesheet").add("href", stylesheet)); // info.scene.addLastModifiedToUri() is already called when
																																			// creating the scene
			}
		switch(info.targetType) {
			case XLS:
				info.serializer.putElement("style", attr().add("type", "text/css"), "td { border: 0.5pt solid #D0D0D0; }\n.groupFooterRow1 { background-color: #D0D0D0; }\n.groupFooterRow0 { background-color: #FFFF99; }\n.note { font-size: 8pt; }\n");
				break;
			case CHART_HTML:
			case HTML:
				switch(info.scene.getLayout()) {
					case FRAGMENT:
						break;
					default:
						if(info.reportHeaderSize == 0) {
							if(showOutputTypeButtons(info))
								info.reportHeaderSize += 26;
							else if(ConvertUtils.getBooleanSafely(info.report.getDirective("show-run-date"), true))
								info.reportHeaderSize += 26;

							if(info.totalPageCount > 1)
								info.reportHeaderSize += 36;
							String reportTitle = info.report.getTitle(info.parameters);
							if(!StringUtils.isBlank(reportTitle))
								info.reportHeaderSize += 15;

							String reportSubtitle = info.report.getSubtitle(info.parameters);
							if(!StringUtils.isBlank(reportSubtitle))
								info.reportHeaderSize += 21;
							if(info.folioInfos.length == 1 && StringUtils.isBlank(reportTitle) && StringUtils.isBlank(reportSubtitle)) {
								String folioTitle = info.folioInfos[0].folio.getTitle(info.parameters);
								String folioSubtitle = info.folioInfos[0].folio.getSubtitle(info.parameters);
								if(!StringUtils.isBlank(folioTitle))
									info.reportHeaderSize += (27 * (1 + folioTitle.length() / 50));
								if(!StringUtils.isBlank(folioSubtitle))
									info.reportHeaderSize += (20 * (1 + folioSubtitle.length() / 100));
								if(!StringUtils.isBlank(folioTitle) || !StringUtils.isBlank(folioSubtitle))
									info.reportHeaderSize += 8;
							}
						}
						StringBuilder sb = new StringBuilder();
						sb.append(".reportHeader { height: ").append(info.reportHeaderSize).append("px; }\n.reportContent { top: ").append(info.reportHeaderSize).append("px; ");
						int h = getCopyrightHeight(info);
						if(h > 0)
							sb.append("bottom: ").append(h).append("px; ");
						sb.append("}\n");
						info.serializer.putElement("style", attr().add("type", "text/css"), sb.toString());
				}
				break;
				// NOTE: 12042012 the following is to prevent first page being empty, also avoid wide columns being cut off
			case PDF:
			case CHART_PDF:
				StringBuilder sb = new StringBuilder();
				sb.append("@page { size : 23.39in 16.54in;}\n.reportHeader { page-break-after: auto !important; }\n.reportContent { page-break-after: auto !important;} ");
				info.serializer.putElement("style", attr().add("type", "text/css"), sb.toString());
				break;
		}
	}

	protected int getCopyrightHeight(GenerateInfo info) {
		return 21;
	}
	protected boolean showOutputTypeButtons(GenerateInfo info) {
		switch(info.targetType) {
			case CHART_HTML:
			case HTML:
				return ConvertUtils.getBooleanSafely(info.report.getDirective("show-alt-output-types"), true);
			default:
				return false;
		}
	}

	public boolean isResetResultsRequired(OutputType outputType) {
		return true;
	}

	protected void generatePageNavigation(GenerateInfo info) throws SAXException {
		long profileId = info.executor.getUserId();
		long requestId = ConvertUtils.getLongSafely(info.scene.getProperties().get("requestId"), -1);

		info.serializer.startElement("script", attr().add("type", "text/javascript"));
		info.serializer
				.characters("window.addEvent(\"domready\", function() {\nvar element = $(\"page-navigation-text\");\nif(element && element.meiomask) {\nelement.meiomask(\"Reverse.Integer\",  {thousands: ''});\nif(element.onchange) {\nvar func = typeof(element.onchange) == 'function' ? element.onchange : new Function(\"event\", String.from(element.onchange));\nelement.addEvent(\"change\", func);\nelement.addEvent(\"keypress\", function(event) {\nif(13 == event.code) {\nelement.blur();\n}\n});\n}\n}\n});");
		info.serializer.characters("\nfunction gotoReportPage(pageNum) {\nif(pageNum < 1) {\npageNum = 1;\n} else if(pageNum > ");
		info.serializer.characters(String.valueOf(info.totalPageCount));
		info.serializer.characters(") {\npageNum = ");
		info.serializer.characters(String.valueOf(info.totalPageCount));
		info.serializer.characters(";\n}\nlocation.href='retrieve_report_page_by_user.i?pollInterval=1000&pollIntervalIndex=1&profileId=");
		info.serializer.characters(String.valueOf(profileId));
		info.serializer.characters("&requestId=");
		info.serializer.characters(String.valueOf(requestId));
		info.serializer.characters("&pageId=' + (pageNum - 1);\n}\n");
		info.serializer.endElement("script");

		info.serializer.startElement("table", attr().add("class", "page-navigation"));
		info.serializer.startElement("tr");
		generatePageNavigationButton(info, "<<", "First", 1, info.pageIndex > 0);
		generatePageNavigationButton(info, "<", "Previous", info.pageIndex, info.pageIndex > 0);
		info.serializer.startElement("td");
		info.serializer.characters("Page");
		info.serializer.characters(NBSP);
		info.serializer.putElement("input", attr().add("id", "page-navigation-text").add("type", "text").add("value", String.valueOf(info.pageIndex + 1)).add("size", "6").add("onchange", "gotoReportPage(parseInt(this.value))"));
		info.serializer.characters(NBSP);
		info.serializer.characters("of");
		info.serializer.characters(NBSP);
		info.serializer.characters(String.valueOf(info.totalPageCount));
		info.serializer.endElement("td");
		generatePageNavigationButton(info, ">", "Next", info.pageIndex + 2, info.pageIndex < info.totalPageCount - 1);
		generatePageNavigationButton(info, ">>", "Last", info.totalPageCount, info.pageIndex < info.totalPageCount - 1);
		info.serializer.endElement("tr");
		info.serializer.endElement("table");
	}

	protected void generatePageNavigationButton(GenerateInfo info, String text, String tip, int pageNum, boolean enabled) throws SAXException {
		info.serializer.startElement("td");
		AppendableAttributes aa = attr().add("class", "page-navigation-button").add("type", "button").add("value", text).add("title", tip).add("onclick", "gotoReportPage(" + pageNum + ")");
		if(!enabled)
			aa.add("disabled", "disabled");
		info.serializer.putElement("input", aa);
		info.serializer.endElement("td");
	}

	public int getDefaultMaxRowsPerPage() {
		return defaultMaxRowsPerPage;
	}

	public void setDefaultMaxRowsPerPage(int defaultMaxRowsPerPage) {
		this.defaultMaxRowsPerPage = defaultMaxRowsPerPage;
	}

	public static TaskListener getTaskListener() {
		if(taskListener == null)
			taskListener = timingLogged ? new LogTimingTaskListener(log) : new NadaTaskListener();
		return taskListener;
	}

	public static boolean isTimingLogged() {
		return timingLogged;
	}

	public static void setTimingLogged(boolean timingLogged) {
		DirectXHTMLGenerator.timingLogged = timingLogged;
		taskListener = null;
	}
}
