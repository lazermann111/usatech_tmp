/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.text.Format;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.xml.sax.SAXException;

import simple.app.DialectResolver;
import simple.bean.ConvertUtils;
import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.ResultsValueRetriever;
import simple.falcon.engine.SortOrder;
import simple.lang.Initializer;
import simple.lang.SystemUtils;
import simple.results.DataGenre;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public abstract class StandardPillar extends Initializer<RuntimeException> implements Pillar {
	protected final static FieldOrder[] EMPTY_FIELD_ORDERS = new FieldOrder[0];
	protected final static String defaultNumericFormat="NUMBER:0.##";
	protected final ReentrantLock lock = new ReentrantLock();
	protected StandardFolio folio;
	protected PillarType pillarType = PillarType.LINK;
	protected Format percentFormat;
	protected Format actionFormat;
	protected Format displayFormat;
	protected Format helpFormat;
	protected Format sortFormat;
	protected Format styleFormat;
	protected Format labelFormat;
	protected String description;
	protected FieldOrder[] fieldOrders = EMPTY_FIELD_ORDERS;
	protected DataGenre sortType;
	protected int groupingLevel;
	protected boolean aggregated;
	protected String width;
	protected ResultsValueRetriever displayValueRetriever;
	protected ResultsValueRetriever sortValueRetriever;

	public StandardPillar(StandardFolio folio) {
		this.folio = folio;
	}

	@Override
	protected void doInitialize() throws RuntimeException {
		ColumnNamer namer = folio.getColumnNamer();
		String[] displayColumns;
		String[] sortColumns;
		Aggregate[] aggregates;
		if(fieldOrders == null || fieldOrders.length == 0) {
			displayColumns = sortColumns = new String[0];
			aggregates = new Aggregate[0];
		} else {
			displayColumns = new String[fieldOrders.length];
			sortColumns = new String[fieldOrders.length];
			aggregates = new Aggregate[fieldOrders.length];
			for(int i = 0; i < fieldOrders.length; i++) {
				displayColumns[i] = namer.getDisplayColumnName(fieldOrders[i]);
				sortColumns[i] = namer.getSortColumnName(fieldOrders[i]);
				aggregates[i] = fieldOrders[i].getAggregateType();
			}
		}
		displayValueRetriever = createValueRetriever(displayColumns, aggregates);
		sortValueRetriever = createValueRetriever(sortColumns, aggregates);
	}
	@Override
	protected void doReset() {
		if(folio.getQuery()==null){
			folio.recalcFieldOrders();
		}
		displayValueRetriever = null;
		sortValueRetriever = null;
	}
	public void invalidate() {
		reset();
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#copy(simple.translator.Translator)
	 */
	public StandardPillar copy(Translator translator, StandardFolio folioCopy) {
		StandardPillar copy = folioCopy.newPillar();
		lock.lock();
		try {
			copy.pillarType = pillarType;
			copy.percentFormat = percentFormat;
			copy.actionFormat = actionFormat;
			copy.displayFormat = TranslatorFactory.translateFormat(displayFormat, translator);
			copy.helpFormat = TranslatorFactory.translateFormat(helpFormat, translator);
			copy.labelFormat = TranslatorFactory.translateFormat(labelFormat, translator);
			copy.sortFormat = sortFormat;
			copy.sortType = sortType;
			if(translator != null) {
				copy.description = translator.translate(description);
			} else {
				copy.description = description;
			}
			copy.styleFormat = styleFormat;
			copy.groupingLevel = groupingLevel;
			copy.fieldOrders = fieldOrders.clone();
			copy.aggregated = aggregated;
			copy.displayValueRetriever = displayValueRetriever;
			copy.sortValueRetriever = sortValueRetriever;
			copy.width = width;
		} finally {
			lock.unlock();
		}
		return copy;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#getActionFormat()
	 */
	public Format getActionFormat() {
		return actionFormat;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getDisplayFormat()
	 */
	public Format getDisplayFormat() {
		return displayFormat;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getHelpFormat()
	 */
	public Format getHelpFormat() {
		return helpFormat;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getSortFormat()
	 */
	public Format getSortFormat() {
		return sortFormat;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getStyleFormat()
	 */
	public Format getStyleFormat() {
		return styleFormat;
	}
	// Other methods
	/**
	 * @see simple.falcon.engine.standard.Pillar#getPillarType()
	 */
	public PillarType getPillarType() {
		return pillarType;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getLabelFormat()
	 */
	public Format getLabelFormat() {
		return labelFormat;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getSortType()
	 */
	public DataGenre getSortType() {
		return sortType;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getDescription()
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @see simple.falcon.engine.standard.Pillar#getFieldOrders()
	 */
	public FieldOrder[] getFieldOrders() {
		return fieldOrders;
	}
	
	public void setFieldOrders(FieldOrder[] fieldOrders) {
		lock.lock();
		try {
			this.fieldOrders = fieldOrders;
			invalidate();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#getGroupingLevel()
	 */
	public int getGroupingLevel() {
		return groupingLevel;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#addFieldOrder(simple.falcon.engine.Field, simple.falcon.engine.SortOrder, simple.results.Results.Aggregate, int)
	 */
	public void addFieldOrder(Field field, SortOrder sortOrder, Aggregate aggregateType, int sortIndex) {
		lock.lock();
		try {
			FieldOrder[] newFieldOrders = new FieldOrder[fieldOrders.length + 1];
			FieldOrder fo = createFieldOrder(getSortType(), field, sortOrder, aggregateType, sortIndex);
			newFieldOrders[fieldOrders.length] = fo;
			System.arraycopy(fieldOrders, 0, newFieldOrders, 0, fieldOrders.length);
			fieldOrders = newFieldOrders;
			invalidate();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#removeFieldOrder(int)
	 */
	public void removeFieldOrder(int index) {
		if(index > -1 && index < fieldOrders.length) {
			lock.lock();
			try {
				FieldOrder[] newFieldOrders = new FieldOrder[fieldOrders.length - 1];
				System.arraycopy(fieldOrders, 0, newFieldOrders, 0, index);
				System.arraycopy(fieldOrders, index + 1, newFieldOrders, index, fieldOrders.length - (index + 1));
				fieldOrders = newFieldOrders;
				invalidate();
			} finally {
				lock.unlock();
			}
		}
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#replaceFieldOrders(simple.falcon.engine.Field[], simple.falcon.engine.SortOrder[], simple.results.Results.Aggregate[], int[])
	 */
	public void replaceFieldOrders(Field[] fields, SortOrder[] sortOrders, Aggregate[] aggregateTypes, int[] sortIndexes) {
		lock.lock();
		try {
			fieldOrders = createFieldOrders(sortType, fields, sortOrders, aggregateTypes, sortIndexes);
			invalidate();
		} finally {
			lock.unlock();
		}
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setActionFormat(java.text.Format)
	 */
	public void setActionFormat(Format actionFormat) {
		this.actionFormat = actionFormat;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setDescription(java.lang.String)
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setDisplayFormat(java.text.Format)
	 */
	public void setDisplayFormat(Format displayFormat) {
		this.displayFormat = displayFormat;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setGroupingLevel(int)
	 */
	public void setGroupingLevel(int groupingLevel) {
		if(groupingLevel < 0) {
			this.groupingLevel = -groupingLevel - 1;
			this.aggregated = true;
		} else {
			this.groupingLevel = groupingLevel;
			this.aggregated = false;
		}
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setHelpFormat(java.text.Format)
	 */
	public void setHelpFormat(Format helpFormat) {
		this.helpFormat = helpFormat;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setLabelFormat(java.text.Format)
	 */
	public void setLabelFormat(Format labelFormat) {
		this.labelFormat = labelFormat;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setPillarType(simple.falcon.engine.Pillar.PillarType)
	 */
	public void setPillarType(PillarType pillarType) {
		this.pillarType = pillarType;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setSortFormat(java.text.Format)
	 */
	public void setSortFormat(Format sortFormat) {
		this.sortFormat = sortFormat;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setSortType(simple.results.DataGenre)
	 */
	public void setSortType(DataGenre sortType) {
		this.sortType = sortType;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#setStyleFormat(java.text.Format)
	 */
	public void setStyleFormat(Format styleFormat) {
		this.styleFormat = styleFormat;
	}

	protected FieldOrder createFieldOrder(DataGenre pillarSortType, Field field, SortOrder sortOrder, Aggregate aggregateType, int sortIndex) {
		FieldOrder fo = new FieldOrder();
		fo.setField(field);
		if(sortOrder == null) {
			if(DataGenre.DATE == pillarSortType
					&& (folio.getOutputType() == null || "report".equalsIgnoreCase(folio.getOutputType().getProtocol()))) {
				fo.setSortOrder(SortOrder.DESC);
			} else if (DataGenre.NUMBER == pillarSortType
					&& (folio.getOutputType() == null || "report".equalsIgnoreCase(folio.getOutputType().getProtocol()))) {
				fo.setSortOrder(SortOrder.DESC);
			} else {
				fo.setSortOrder(SortOrder.ASC);
			}
		} else {
			fo.setSortOrder(sortOrder);
		}
		if (aggregateType == null) {
			/*
            if (fo.getField().getAdditiveType() == Field.AdditiveType.FULL)
                fo.setAggregateType(Results.Aggregate.SUM);
            else
                fo.setAggregateType(Results.Aggregate.MAX);*/
			/*
			 * if("DATE".equals(pillar.sortType)) fo.aggregateType =
			 * Results.AGGREGATE_MAX; else if("NUMBER".equals(pillar.sortType))
			 * fo.aggregateType = Results.AGGREGATE_SUM; else fo.aggregateType =
			 * Results.AGGREGATE_MAX;//XXX: not sure what this should be
			 */
		} else {
			fo.setAggregateType(aggregateType);
		}
		fo.setSortIndex(sortIndex);
		return fo;
	}

	protected FieldOrder[] createFieldOrders(DataGenre pillarSortType, Field[] fields, SortOrder[] sortOrders, Aggregate[] aggregateTypes, int[] sortIndexes) {
		FieldOrder[] fos = new FieldOrder[fields.length];
		for(int i = 0; i < fields.length; i++) {
			fos[i] = createFieldOrder(pillarSortType, fields[i],
					(sortOrders == null || sortOrders.length <= i ? null : sortOrders[i]),
					(aggregateTypes == null || aggregateTypes.length <= i ? null : aggregateTypes[i]),
					(sortIndexes == null || sortIndexes.length <= i ? 0 : sortIndexes[i])) ;
		}
		return fos;
	}

	/**
	 * @see simple.falcon.engine.standard.Pillar#isAggregated()
	 */
	public boolean isAggregated() {
		return aggregated;
	}

	public ResultsValueRetriever getDisplayValueRetriever() {
		try {
			initialize();
		} catch(InterruptedException e) {
		}
		return displayValueRetriever;
	}

	public ResultsValueRetriever getSortValueRetriever() {
		try {
			initialize();
		} catch(InterruptedException e) {
		}
		return sortValueRetriever;
	}

	protected ResultsValueRetriever createValueRetriever(String[] columns, Aggregate[] aggregates) {
		if(columns.length == 1) {
			return new SingleColumnRetriever(columns[0], aggregates[0]);
		}
		return new MultiColumnRetriever(columns, aggregates);
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}
	public void writeXML(String tagName, XMLBuilder builder) throws SAXException{
    	if(tagName==null) tagName="pillar";
    	Map<String,String> pillarAtts = new LinkedHashMap<String, String>(11);
		 pillarAtts.put("pillarType", getPillarType().toString());
		 pillarAtts.put("percentFormat", ConvertUtils.getFormatString(getPercentFormat()));
		 pillarAtts.put("actionFormat", ConvertUtils.getFormatString(getActionFormat()));
		 pillarAtts.put("displayFormat", ConvertUtils.getFormatString(getDisplayFormat()));
		 pillarAtts.put("helpFormat", ConvertUtils.getFormatString(getHelpFormat()));
		 pillarAtts.put("sortFormat", ConvertUtils.getFormatString(getSortFormat()));
		 pillarAtts.put("styleFormat", ConvertUtils.getFormatString(getStyleFormat()));
		 pillarAtts.put("labelFormat", ConvertUtils.getFormatString(getLabelFormat()));
		 pillarAtts.put("description",getDescription());
		 pillarAtts.put("sortType",getSortType()==null?"":getSortType().toString());
		 pillarAtts.put("groupingLevel",ConvertUtils.getStringSafely(getGroupingLevel()));
		 pillarAtts.put("width",ConvertUtils.getStringSafely(getWidth()));
		 pillarAtts.put("aggregated",ConvertUtils.getStringSafely(isAggregated()));
		 builder.elementStart("pillar", pillarAtts);
		 FieldOrder[] fields=getFieldOrders();
		 for(int j=0; j<fields.length; j++){
			 if(fields[j].getDisplayColumnName()!=null){
				 Map<String,String> filedAtts = new LinkedHashMap<String, String>(2);
				 filedAtts.put("displayColumnName", ConvertUtils.getStringSafely(fields[j].getDisplayColumnName()));
				 filedAtts.put("sortColumnName", ConvertUtils.getStringSafely(fields[j].getSortColumnName()));
				 builder.elementStart("fieldOrder", filedAtts);
				 builder.elementEnd("fieldOrder");
			 }
			 if(fields[j].getField()!=null){
				 Map<String,String> filedAtts = new LinkedHashMap<String, String>(4);
				 filedAtts.put("fieldLabel", ConvertUtils.getStringSafely(fields[j].getField().getLabel()));
				 filedAtts.put("fieldId", ConvertUtils.getStringSafely(fields[j].getField().getId()));
				 filedAtts.put("sortOrder",ConvertUtils.getStringSafely(fields[j].getSortOrder()));
				 filedAtts.put("aggregateType",ConvertUtils.getStringSafely(fields[j].getAggregateType()));
				 filedAtts.put("sortIndex", ConvertUtils.getStringSafely(fields[j].getSortIndex()));
				 builder.elementStart("field", filedAtts);
				 builder.elementEnd("field");
			 }
		 }
    	builder.elementEnd(tagName);
    }
	
    public void setAggregated(boolean aggregated) {
		this.aggregated = aggregated;
	}

	public void readXML(ObjectBuilder builder) throws DesignException{
    	setPillarType(builder.get("pillarType", PillarType.class));
    	if(!isBlank(builder.getAttrValue("percentFormat")))setPercentFormat(ConvertUtils.getFormat(builder.getAttrValue("percentFormat")));
    	if(!isBlank(builder.getAttrValue("actionFormat")))setActionFormat(ConvertUtils.getFormat(builder.getAttrValue("actionFormat")));
		if (!isBlank(builder.getAttrValue("displayFormat")))
			setDisplayFormat(ConvertUtils.getFormat(builder.getAttrValue("displayFormat")));
		else if (!DialectResolver.isOracle() && builder.get("sortType", DataGenre.class).equals(DataGenre.NUMBER))
			setDisplayFormat(ConvertUtils.getFormat(defaultNumericFormat));
    	if(!isBlank(builder.getAttrValue("helpFormat")))setHelpFormat(ConvertUtils.getFormat(builder.getAttrValue("helpFormat")));
    	if(!isBlank(builder.getAttrValue("sortFormat")))setSortFormat(ConvertUtils.getFormat(builder.getAttrValue("sortFormat")));
    	if(!isBlank(builder.getAttrValue("styleFormat")))setStyleFormat(ConvertUtils.getFormat(builder.getAttrValue("styleFormat")));
    	if(!isBlank(builder.getAttrValue("labelFormat")))setLabelFormat(ConvertUtils.getFormat(builder.getAttrValue("labelFormat")));
		setDescription(builder.getAttrValue("description"));
		if(!isBlank(builder.getAttrValue("sortType")))setSortType(builder.get("sortType", DataGenre.class));
		setGroupingLevel(builder.get("groupingLevel", int.class));
		setWidth(builder.getAttrValue("width"));
		setAggregated(builder.get("aggregated", boolean.class));
		ObjectBuilder[] fields = builder.getSubNodes("field");
		if(fields!=null){
			for(int i=0; i<fields.length; i++){
				addFieldOrder(folio.designEngine.getField(fields[i].get("fieldId", Long.class)), fields[i].get("sortOrder", SortOrder.class), fields[i].get("aggregateType", Results.Aggregate.class), fields[i].get("sortIndex", int.class));
			}
		}
		ObjectBuilder[] fieldOrder = builder.getSubNodes("fieldOrder");
		if(fieldOrder!=null){
			if(fieldOrder.length==1){
				FieldOrder[] fo1=new FieldOrder[1];
				fo1[0]=new FieldOrder();
				fo1[0].setDisplayColumnName(fieldOrder[0].getAttrValue("displayColumnName"));
				fo1[0].setSortColumnName(fieldOrder[0].getAttrValue("sortColumnName"));
				setFieldOrders(fo1);
			}
		}

	}

    protected boolean isBlank(String s) {
    	return (s == null || s.trim().length() == 0);
    }

	public Format getPercentFormat() {
		return percentFormat;
	}

	public void setPercentFormat(Format percentFormat) {
		this.percentFormat = percentFormat;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hc = 0;
		hc = SystemUtils.addHashCode(hc, pillarType);
		//hc = SystemUtils.addHashCode(hc, percentFormat);
		hc = SystemUtils.addHashCode(hc, actionFormat);
		hc = SystemUtils.addHashCode(hc, displayFormat);
		//hc = SystemUtils.addHashCode(hc, helpFormat);
		//hc = SystemUtils.addHashCode(hc, sortFormat);
		//hc = SystemUtils.addHashCode(hc, styleFormat);
		hc = SystemUtils.addHashCode(hc, labelFormat);
		//hc = SystemUtils.addHashCode(hc, description);
		//hc = SystemUtils.addHashCode(hc, sortType);
		hc = SystemUtils.addHashCode(hc, groupingLevel);
		hc = SystemUtils.addHashCode(hc, fieldOrders);
		return hc;
	}
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof StandardPillar)) return false;
		StandardPillar p = (StandardPillar)obj;
		return ConvertUtils.areEqual(pillarType, p.pillarType)
			&& ConvertUtils.areEqual(percentFormat, p.percentFormat)
			&& ConvertUtils.areEqual(actionFormat, p.actionFormat)
			&& ConvertUtils.areEqual(displayFormat, p.displayFormat)
			&& ConvertUtils.areEqual(helpFormat, p.helpFormat)
			&& ConvertUtils.areEqual(sortFormat, p.sortFormat)
			&& ConvertUtils.areEqual(styleFormat, p.styleFormat)
			&& ConvertUtils.areEqual(labelFormat, p.labelFormat)
			&& ConvertUtils.areEqual(description, p.description)
			&& Arrays.deepEquals(fieldOrders, p.fieldOrders)
			&& ConvertUtils.areEqual(sortType, p.sortType)
			&& groupingLevel == p.groupingLevel
			&& aggregated == p.aggregated
			&& ConvertUtils.areEqual(width, p.width);
	}
}