package simple.falcon.engine.standard;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;
import java.awt.Stroke;

import org.jfree.chart.ChartColor;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.DrawingSupplier;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.ui.RectangleInsets;

public class DirectChartTheme extends StandardChartTheme {
	private static final long serialVersionUID = -5732861705140699375L;
	protected float lineWidth = 3.0f;
	protected static final Color FAINT_GRAY = new Color(237, 237, 237);
	protected static final Paint[] DEFAULT_PAINT_SEQUENCE = new Paint[] {
        ChartColor.DARK_RED,
        ChartColor.DARK_BLUE,
        ChartColor.DARK_GREEN,
        ChartColor.DARK_YELLOW,
        ChartColor.DARK_MAGENTA,
        ChartColor.DARK_CYAN,
        Color.darkGray,
        ChartColor.LIGHT_RED,
        ChartColor.LIGHT_BLUE,
        ChartColor.LIGHT_GREEN,
		new Color(0xFF, 0xC0, 0x40),// ChartColor.LIGHT_YELLOW,
        ChartColor.LIGHT_MAGENTA,
        ChartColor.LIGHT_CYAN,
        Color.lightGray,
        ChartColor.VERY_DARK_RED,
        ChartColor.VERY_DARK_BLUE,
        ChartColor.VERY_DARK_GREEN,
        ChartColor.VERY_DARK_YELLOW,
        ChartColor.VERY_DARK_MAGENTA,
        ChartColor.VERY_DARK_CYAN,
        new Color(0xFF, 0x55, 0x55),
        new Color(0x55, 0x55, 0xFF),
        new Color(0x55, 0xFF, 0x55),
        new Color(0xFF, 0xFF, 0x55),
        new Color(0xFF, 0x55, 0xFF),
        new Color(0x55, 0xFF, 0xFF),
        Color.pink,
        Color.gray,
        ChartColor.VERY_LIGHT_RED,
        ChartColor.VERY_LIGHT_BLUE,
        ChartColor.VERY_LIGHT_GREEN,
        ChartColor.VERY_LIGHT_YELLOW,
        ChartColor.VERY_LIGHT_MAGENTA,
        ChartColor.VERY_LIGHT_CYAN
    };
	public DirectChartTheme() {
		super("direct");
		setChartBackgroundPaint(FAINT_GRAY);
		setPlotBackgroundPaint(Color.WHITE);
		setDomainGridlinePaint(Color.LIGHT_GRAY);
		setRangeGridlinePaint(new Color(108, 108, 108, 128));
		// setPlotOutlinePaint(paint);
		setBaselinePaint(Color.DARK_GRAY);
		// this.crosshairPaint = Color.blue;
		setAxisLabelPaint(Color.BLACK);
		setTickLabelPaint(Color.BLACK);
		setWallPaint(new Color(208, 208, 208));
		setXYBarPainter(new StandardXYBarPainter());
	}

	@Override
	public void apply(JFreeChart chart) {
		chart.setPadding(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		super.apply(chart);
	}

	protected void applyToCategoryPlot(CategoryPlot plot) {
		super.applyToCategoryPlot(plot);
		plot.setRangeZeroBaselineVisible(true);
	}

	@Override
	protected void applyToXYPlot(XYPlot plot) {
		super.applyToXYPlot(plot);
		plot.setRangeZeroBaselineVisible(true);
	}

	@Override
	public DrawingSupplier getDrawingSupplier() {
		return new DefaultDrawingSupplier(
				DEFAULT_PAINT_SEQUENCE, 
				DefaultDrawingSupplier.DEFAULT_FILL_PAINT_SEQUENCE, 
				DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE, 
				new Stroke[] { new BasicStroke(getLineWidth(), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL)}, 
				DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
				DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE);
	}

	public float getLineWidth() {
		return lineWidth;
	}

	public void setLineWidth(float lineWidth) {
		this.lineWidth = lineWidth;
	}
}