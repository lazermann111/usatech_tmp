package simple.falcon.engine.standard;

import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.CacheableCall;
import simple.db.Column;
import simple.db.Cursor;
import simple.db.DataLayerException;
import simple.db.Parameter;
import simple.db.specific.DbSpecific;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Filter;
import simple.falcon.engine.FilterGroup;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Param;
import simple.falcon.engine.Pillar.PillarType;
import simple.falcon.engine.SortOrder;
import simple.io.IOUtils;
import simple.results.BeanException;
import simple.results.DataGenre;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.sql.Aliaser;
import simple.sql.BaseMutableExpression;
import simple.sql.BuildSQLException;
import simple.sql.Expression;
import simple.sql.ExtendedExpression;
import simple.sql.InitialsAliaser;
import simple.sql.Join;
import simple.sql.SQLBuilder;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.sql.Table;

public class StandardDesignEngine3 extends simple.falcon.engine.standard.StandardDesignEngine2 {
	private static final simple.io.Log log = simple.io.Log.getLog();
    protected Map<Field,Aggregate> defaultAggs = new HashMap<Field, Aggregate>();
    protected class SubSelect {
    	public final Set<Table> tables = new TreeSet<Table>(sqlBuilder.getTableComparator());
    	public final Set<FieldOrder> fieldOrders = new HashSet<FieldOrder>();
    	public simple.falcon.engine.standard.SelectStatement select;
    	public Map<String, Join> joins = null;
    	public FilterGroup filter = new simple.falcon.engine.standard.StandardFilterGroup(StandardDesignEngine3.this);
    	public boolean isolate = false;
    	public String alias = null;
    	public Map<Table,String> tableMap;
    	public int processed = 0;
    }

	public StandardDesignEngine3() {
		super();
	}

    @Override
	protected CacheableCall createCall(Long folioId, String effectiveUserGroupIds, Collection<FieldOrder> fieldOrderList, FilterGroup filter, DatabaseMetaData metaData, String hint) throws SQLException, BuildSQLException {
    	//return createCallWAggs(fieldOrderList, filter, metaData);
    	return createCall2(folioId, effectiveUserGroupIds,fieldOrderList, filter, metaData, hint);
	}

    public class InnerFolio3 extends InnerFolio {
    	protected Map<Field,Aggregate> defaultAggs;
		public InnerFolio3() {
			super();
		}
		@Override
		public void addPillar(PillarType pillarType, int groupingLevel, String labelFormat, String description, String styleFormat, String displayFormat, String actionFormat, String helpFormat, String sortFormat, DataGenre sortType, SortOrder[] sortOrders, Field[] fields, Aggregate[] aggregateTypes, int[] sortIndexes) {
			boolean useGiven = true;
			switch(groupingLevel) {
				case 10: //xtab
					groupingLevel = 1;
					break;
				case 0: case 1: case 2: case 3: case 4: case 5:
					groupingLevel += 2;
					useGiven = false;
					break;
				case -1: // totals
					groupingLevel = 0;
					break;
				case -2: case -3: case -4: case -5: case -6: case -7:
					groupingLevel = -groupingLevel;
					break;
			}
			if(aggregateTypes == null)
				aggregateTypes = new Aggregate[fields.length];
			for(int i = 0; i < fields.length && i < aggregateTypes.length; i++) {
	    		if(fields[i].getAdditiveType() != Field.AdditiveType.NONE) {
	    			aggregateTypes[i] = null;
	    		} else if(!useGiven || aggregateTypes[i] == null || !aggregateAllowed(aggregateTypes[i], fields[i])) {
	    			aggregateTypes[i] = defaultAggs.get(fields[i]);
	    		}
	    	}
			super.addPillar(pillarType, groupingLevel, labelFormat, description, styleFormat, displayFormat,
					actionFormat, helpFormat, sortFormat, sortType, sortOrders, fields, aggregateTypes,
					sortIndexes);
		}
		protected boolean aggregateAllowed(Aggregate aggregate, Field field) {
			switch(aggregate) {
				case AVG:
				case SUM:
					Class<? extends Object> cls1 = SQLTypeUtils.getJavaType(field.getDisplaySqlType().getTypeCode());
					Class<? extends Object> cls2 = SQLTypeUtils.getJavaType(field.getSortSqlType().getTypeCode());
					return (Number.class.isAssignableFrom(cls1) && Number.class.isAssignableFrom(cls2));
				case ARRAY:
				case COUNT:
				case DISTINCT:
				case FIRST:
				case LAST:
				case MAX:
				case MIN:
				case SET:
				default:
					return true;
			}
		}
		@Override
		public InnerStandardPillar newPillar() {
			return new InnerStandardPillar2();
		}
	    public class InnerStandardPillar2 extends InnerStandardPillar {
			public InnerStandardPillar2() {
				super();
			}

			/*@Override
			protected Aggregate[] getAggregateTypes() {
				if(aggregateTypes == null) {
		            if(fieldOrders == null) aggregateTypes = new Results.Aggregate[0];
		            else {
		                aggregateTypes = new Results.Aggregate[fieldOrders.length];
		                for(int i = 0; i < fieldOrders.length; i++) {
		                    aggregateTypes[i] = fieldOrders[i].getAggregateType();
		                    if(aggregateTypes[i] == Aggregate.DISTINCT || aggregateTypes[i] == Aggregate.SET)
		                    	aggregateTypes[i] = null;
		                }
		            }
		        }
		        return aggregateTypes;
			}	*/
		}

		@Override
		public void setDirective(String name, String directive) {
			if("no-group-totals".equalsIgnoreCase(name)) {
				// NOTE: This is because we artifically translate the folio pillars in StandardDesignEngine2 to adjust the group levels
				try {
		        	int[] ngt = ConvertUtils.convert(int[].class, directive);
		            if(ngt != null) {
			        	for(int i = 0; i < ngt.length; i++)
			        		if(ngt[i] > 0) ngt[i] += 2;
			            directive = ConvertUtils.convert(String.class, ngt);;
			        }
		        } catch(ConvertException e) {
		        	//do nothing
		        }
			}
			super.setDirective(name, directive);
		}
		@Override
		public void setFilter(Filter filter) {
			fixFilterAggregates(filter);
			super.setFilter(filter);
		}
		protected void fixFilterAggregates(Filter filter) {
			if(filter instanceof FilterGroup) {
				Set<Filter> children = ((FilterGroup)filter).getChildFilters();
				if(children != null)
					for(Filter f : children)
						fixFilterAggregates(f);
			} else if(filter instanceof FilterItem) {
				FilterItem fi = (FilterItem)filter;
				if(fi.getAggregateType() == null)
					fi.setAggregateType(defaultAggs.get(fi.getField()));
			}
		}
    }
	@Override
	public InnerFolio newFolio() {
		InnerFolio3 folio = new InnerFolio3();
		folio.defaultAggs = defaultAggs;
		return folio;
	}

    @Override
	protected Field createField(Results results, Connection conn) throws BeanException, SQLException {
        Field field = new Field();
        results.fillBean(field);
        String displayExp = results.getFormattedValue("displayExp");
        String sortExp = results.getFormattedValue("sortExp");
        DatabaseMetaData metaData = conn.getMetaData();
        ExtendedExpression de = sqlBuilder.createExtendedExpression(displayExp, metaData);
        ExtendedExpression se = sqlBuilder.createExtendedExpression(sortExp, metaData);
        Expression displayExpression;
        Expression sortExpression;
        if(de.getAggregates().isEmpty() && se.getAggregates().isEmpty()) {
        	field.setAdditiveType(Field.AdditiveType.NONE);
			displayExpression = de;
			sortExpression = se;
        } else if(de.getAggregates().size() <= 1 && se.getAggregates().size() <= 1) {
        	Aggregate dAgg, sAgg;
        	Expression dExp, sExp;
        	if(de.getAggregates().isEmpty()) {
        		dAgg = null;
        		dExp = de;
        	} else {
        		dAgg = de.getAggregates().iterator().next();
        		dExp = de.getInnerExpressions().iterator().next();
        	}
        	if(se.getAggregates().isEmpty()) {
        		sAgg = null;
        		sExp = se;
        	} else {
        		sAgg = se.getAggregates().iterator().next();
        		sExp = se.getInnerExpressions().iterator().next();
        	}
        	if((dAgg != null && !startsWith(de, dAgg))
        			|| (sAgg != null && !startsWith(se, dAgg))) {
        		field.setAdditiveType(Field.AdditiveType.FULL);
    			displayExpression = de;
    			sortExpression = se;
        	} else {
        		if(dAgg != null) {
        			defaultAggs.put(field, dAgg);
        		} else if(sAgg != null) {
        			defaultAggs.put(field, sAgg);
        		}
        		displayExpression = dExp;
        		sortExpression = sExp;
        		field.setAdditiveType(Field.AdditiveType.NONE);
        	}
        } else {
    		field.setAdditiveType(Field.AdditiveType.FULL);
			displayExpression = de;
			sortExpression = se;
        }
        field.setDisplayExpression(displayExpression);
        field.setSortExpression(sortExpression);
        fieldCache.put(field.getId(), field);
        return field;
    }
    @Override
	protected boolean startsWith(Expression expression, Aggregate aggregate) {
    	String s = expression.toString().trim();
    	//remove starting paren
    	int functionStart = 0;
    	while(s.charAt(functionStart) == '(') functionStart++;
    	int beginParen = s.indexOf('(', functionStart);
    	if(beginParen < 0) {
    		return false;
    	}
		int functionEnd = beginParen;
		while(Character.isWhitespace(s.charAt(functionEnd - 1)))
			functionEnd--;
		return (sqlBuilder.getAggregateParser().isAggregateFunction(s, functionStart, functionEnd, beginParen, null) != null);
    }
    protected boolean isOuterSelectRequired(Aggregate agg) {
    	if(agg == null)
    		return false;
    	switch(agg) {
    		case AVG:
            case DISTINCT:
    		case FIRST:
    		case LAST:
    				return true;
    		default:
    			return false;
    	}
    }

	@Override
	public Aggregate getRetrieverAggregate(Aggregate original) {
		if(original == null)
			return null;
		switch(original) {
			case COUNT:
				return Aggregate.SUM;
			case AVG:
	    		return null;
			case ARRAY:
			case DISTINCT:
	    	case FIRST:
    		case LAST:
    		case MAX:
    		case MIN:
    		case SET:
    		case SUM:
    			return original;
    	}
		return original;
	}
    /**
     * @param agg
     * @param expression
     * @param sqlType
     * @param appendTo
     * @return true if an agg function was used and no group by is needed, otherwise false
     */
    protected void formatAggFunction(Aggregate agg, String expression, SQLType sqlType, StringBuilder appendTo) {
    	switch(agg) {
    		case DISTINCT:
    			/* handle when outputting
    			appendTo.append("COUNT(DISTINCT ").append(expression).append(')');
    			return;*/
    		case ARRAY:
    			/* handle when outputting
    			appendTo.append("CAST(COLLECT(").append(expression).append(") AS ").append(getArrayTypeName(sqlType.getTypeCode())).append(')');
    			return;*/
    		case SET:
    			/* handle when outputting
    			appendTo.append("CAST(COLLECT(DISTINCT ").append(expression).append(") AS ").append(getArrayTypeName(sqlType.getTypeCode())).append(')');
    			return;*/
    		case FIRST: case LAST: //handle these when outputting values (in xml or csv)
    			appendTo.append(expression);
    			return;
    		default:
    			appendTo.append(agg.toString()).append('(').append(expression).append(')');
				return;
    	}
    }
    /*
     * Agg Type		Expression  		Retrieve Agg	Requires Outer	Requires Isolation
     * -----------	-----------------	-----------		--------------	-----------------
     * AVG			AVG(?)				NULL			TRUE			TRUE				XXX: AVG needs to be revisited
     * ARRAY		?					ARRAY			FALSE			FALSE
     * COUNT		COUNT(?)			SUM				FALSE			TRUE
     * DISTINCT		?					DISTINCT		TRUE			FALSE
     * FIRST		?					FIRST			TRUE			FALSE
     * LAST 		?					LAST			TRUE			FALSE
     * MAX			MAX(?)				MAX				FALSE			FALSE
     * MIN			MIN(?)				MIN				FALSE			FALSE
     * SET			?					SET 			FALSE			FALSE
     * SUM			SUM(?)				SUM				FALSE			TRUE
     */

    /**
     * @param agg
     * @return false if an agg function was used and no group by is needed, otherwise true
     */
    @Override
	protected boolean needsGroupBy(Aggregate agg) {
    	if(agg == null)
    		return true;
    	switch(agg) {
    		case ARRAY:
    		case DISTINCT:
    		case FIRST:
    		case LAST:
    		case SET:
        		// handle these when outputting values (in xml or csv)
    			return true;
    		default:
    			return false;
    	}
    }

    protected String formatAggFunction(Aggregate agg, String expression, SQLType sqlType) {
    	if(agg == null) return expression;
    	StringBuilder sb = new StringBuilder();
    	formatAggFunction(agg, expression, sqlType, sb);
    	return sb.toString();
    }

    protected static final SQLType DECIMAL_SQL_TYPE = new SQLType(Types.DECIMAL);
    protected static final SQLType INTEGER_SQL_TYPE = new SQLType(Types.INTEGER);

    /** Creates a call for the specified list of FieldOrders and Filter.
     *  FieldOrders <strong>MUST</strong> be in correct order of sorting.
     *  (For sql's SORT BY clause)
     * @throws BuildSQLException
     *
     */
    protected CacheableCall createCall2(Long folioId, String effectiveUserGroupIds,Collection<FieldOrder> fieldOrderList, FilterGroup filter, DatabaseMetaData metaData, String hint) throws SQLException, BuildSQLException {
        log.debug("Creating a call");
        log.debug("Processing " + fieldOrderList.size() + " field(s).");
        final SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        final List<Argument> params = new ArrayList<Argument>();
        final Cursor cur = new Cursor();
        final List<Column> cols = new ArrayList<Column>(fieldOrderList.size()*2);
        final Map<Table,String> tableAliases = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
        final SelectStatement select = new SelectStatement();
        if(hint != null)
        	select.setHint(hint);
        DbSpecific dbSpecific = getDbSpecific(metaData);
        params.add(cur);

        //1. Process each field order
		Aliaser aliaser = new InitialsAliaser(tableAliases);
        int lastIndex = 0;
        String exp;
        for(FieldOrder fo : fieldOrderList) {
        	String displayCN = columnNamer.getDisplayColumnName(fo);
			String sortCN = columnNamer.getSortColumnName(fo);
			Column col = new Column();
            col.setIndex(++lastIndex);
            col.setPropertyName(displayCN);
            cols.add(col);

            String displayExp;
			String sortExp;
			for(simple.sql.Column column : fo.getField().getDisplayExpression().getColumns()) {
				if(!tableAliases.containsKey(column.getTable())) {
					String alias = aliaser.getAlias(column.getTable());
					tableAliases.put(column.getTable(), alias);
				}
			}
			displayExp = formatAggFunction(fo.getAggregateType(), fo.getField().getDisplayExpression().format(aliaser), fo.getField().getDisplaySqlType());
	    	if(!fo.getField().getDisplayExpression().equals(fo.getField().getSortExpression())) {
	    		for(simple.sql.Column column : fo.getField().getSortExpression().getColumns()) {
					if(!tableAliases.containsKey(column.getTable())) {
						String alias = aliaser.getAlias(column.getTable());
						tableAliases.put(column.getTable(), alias);
					}
				}
				sortExp = formatAggFunction(fo.getAggregateType(), fo.getField().getSortExpression().format(aliaser), fo.getField().getSortSqlType());
	    		lastIndex++;
	    	} else {
	    		sortExp = null;
	    	}
			if(fo.getField().getAdditiveType() == Field.AdditiveType.NONE && needsGroupBy(fo.getAggregateType())) {
				select.addGroupBy(displayExp);
				if(sortExp != null)
					select.addGroupBy(sortExp);
			}
			select.addColumn(displayExp, displayCN);
			if(sortExp != null) {
				select.addColumn(sortExp, sortCN);
			}
			col = new Column();
            col.setIndex(lastIndex);
            col.setPropertyName(sortCN);
            cols.add(col);
        	switch(getAggregateTypeCode(fo.getAggregateType(), fo.getField().getSortSqlType())) {
        		case Types.ARRAY:
        		case Types.JAVA_OBJECT:
        		case Types.OTHER:
        		case Types.REF:
        		case Types.STRUCT:
        			break;
        		default:
                	select.addOrderBy(sortExp != null ? sortCN : displayCN, fo.getSortOrder());
        	}
        }

        // 2. Add the filters
        if(filter != null && filter.filterCount() > 0) {
    		Filter whereFilter = filter.buildWhereFilter();
    		Filter havingFilter = filter.buildHavingFilter();
    		// add filter to sub selects
			exp = formatFilterExpression(whereFilter, tableAliases, aliaser, params, dbSpecific, false);
        	if(exp != null)
        		select.addWhere(exp);
			exp = formatFilterExpression(havingFilter, tableAliases, aliaser, params, dbSpecific, true);
        	if(exp != null)
        		select.addHaving(exp);
    	}

        // 3. Find all joins
		final Map<String, Join> joins = sqlBuilder.getJoins(tableAliases, aliaser, metaData);
        for(Map.Entry<String, Join> entry : joins.entrySet()) {
    		exp = entry.getKey();
    		Join join = entry.getValue();
			select.addWhere(join.getExpressionObject().format(aliaser));
        }

        // 4. Add tables
        for(Map.Entry<Table,String> entry : tableAliases.entrySet()) {
			select.addTable(entry.getKey().getSchemaName(), entry.getKey().getTableName(), entry.getValue());
		}

        cur.setColumns(cols.toArray(new Column[cols.size()]));
        //cur.setGroups(grpList.toArray());
        cur.setLazyAccess(true); // this is for memory considerations
        String sql = select.getSql();
        log.debug("SQL=\n" + sql);
    	CacheableCall call = new CacheableCall(resultsCache);
    	if(folioId!=null){
    		Map<String, Object> paramMap=new HashMap<String,Object>();
    		paramMap.put("folioId", folioId);
    		paramMap.put("effectiveUserGroupIds", effectiveUserGroupIds);
    		paramMap.put("generatedSql", sql);
    		try{
    			dataLayer.executeCall("UPSERT_FOLIO_SQL", paramMap, true);
    			Object overwrittenSql=paramMap.get("overwrittenSql");
    			if(overwrittenSql!=null){
    				String oSql=IOUtils.readFully(ConvertUtils.convert(Clob.class, overwrittenSql).getAsciiStream());
    				call.setSql(oSql);
    				log.info("SQL Overwritten=\n" + oSql);
    			}else{
    				call.setSql(sql);
    			}
    		}catch(DataLayerException e){
    			throw new BuildSQLException(e);
    		}catch(ConvertException e){
    			throw new BuildSQLException(e);
    		}catch(IOException e){
    			throw new BuildSQLException(e);
    		}
    	}else{
    		call.setSql(sql);
    	}
        call.setSql(sql);
        call.setArguments(params.toArray(new Argument[params.size()]));
        call.setMinElapsedTime(getMinElapsedToCache());
        //call.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE); //needed to allow ResultSetScrollableResults
        return call;
    }

    /** Creates a call for the specified list of FieldOrders and Filter.
     *  FieldOrders <strong>MUST</strong> be in correct order of sorting.
     *  (For sql's SORT BY clause)
     * @throws BuildSQLException
     *
     */
    protected CacheableCall createCallWAggs(Collection<FieldOrder> fieldOrderList, FilterGroup filter, DatabaseMetaData metaData) throws SQLException, BuildSQLException {
        final SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        log.debug("Creating a call");
        Map<Table,SubSelect> subSelects = new TreeMap<Table,SubSelect>(sqlBuilder.getTableComparator());
        List<Argument> topQueryParams = new ArrayList<Argument>();
        List<Argument> queryParams = new ArrayList<Argument>();
        DbSpecific dbSpecific = getDbSpecific(metaData);
        Cursor cur = new Cursor();
        List<Column> cols = new ArrayList<Column>(fieldOrderList.size()*2);
        queryParams.add(cur);
        //List grpList = new ArrayList();
        log.debug("Processing " + fieldOrderList.size() + " field(s).");
        SelectStatement topSelect = new SelectStatement();

        //1. Organize by table
        final Set<Table> tables = new TreeSet<Table>(sqlBuilder.getTableComparator());
        String exp;
        for(FieldOrder fo : fieldOrderList) {
        	tables.clear();
        	gatherTables(fo.getField(), tables);
        	SubSelect sub = findSubSelect(tables, subSelects, fo.getAggregateType());
        	sub.fieldOrders.add(fo);
        	if(!fo.getField().getDisplayExpression().equals(fo.getField().getSortExpression())) {
        		exp = columnNamer.getSortColumnName(fo);
        	} else {
        		exp = columnNamer.getDisplayColumnName(fo);
        	}
        	switch(getAggregateTypeCode(fo.getAggregateType(), fo.getField().getSortSqlType())) {
        		case Types.ARRAY:
        		case Types.JAVA_OBJECT:
        		case Types.OTHER:
        		case Types.REF:
        		case Types.STRUCT:
        			break;
        		default:
                	topSelect.addOrderBy(exp, fo.getSortOrder());
        	}
        }

        // 2. Add the filters into the organized sub-selects
        processFilterSubSelects(filter, subSelects);

        //3. Write sql using sub-selects of tables w/aggs
		int index = 1;
        // find all top-levels first
        int extraColCount = 0;
        final Map<Expression,String> replacements = new HashMap<Expression, String>();
        Map<Table,String> topTableMap = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
        final Map<Table,String> allTableMap = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
		Aliaser aliaser = new InitialsAliaser(allTableMap);
        for(SubSelect sub : subSelects.values()) {
        	if(sub.processed != 0) continue;
        	if(!sub.isolate) { //add to top level select
        		for(Table tab : sub.tables) {
					String alias = aliaser.getAlias(tab);
					topTableMap.put(tab, alias);
					allTableMap.put(tab, alias);
					topSelect.addTable(tab.getSchemaName(), tab.getTableName(), alias);
        		}
        		if(sub.filter != null && sub.filter.filterCount() > 0) {
					BaseMutableExpression be = new BaseMutableExpression();
            		sub.filter.buildExpression(be, dbSpecific);
					exp = be.format(/*TODO: this needs review: topTableMap,*/aliaser);
                    Param[] params = sub.filter.gatherParams();
                    for(int k = 0; k < params.length; k++) {
                        Parameter a = new Parameter();
                        a.setSqlType(dbSpecific.getNormalizedType(params[k].getSqlType()));
                        a.setPropertyName(params[k].getName());
                        a.setDefaultValue(params[k].getValue());
                        a.setRequired(false);
                        topQueryParams.add(a);
                    }
        			topSelect.addWhere(exp);
        		}
        		sub.processed = 10;
        	} else {
        		sub.select.newline = NEWLINE + INDENT;
        		for(Table tab : sub.tables) {
					String alias = aliaser.getAlias(tab);
					sub.tableMap.put(tab, alias);
					allTableMap.put(tab, alias);
					sub.select.addTable(tab.getSchemaName(), tab.getTableName(), alias);
        		}
        		sub.processed = 1;
        	}
    		for(FieldOrder fo : sub.fieldOrders) {
    			String displayCN = columnNamer.getDisplayColumnName(fo);
    			String sortCN = columnNamer.getSortColumnName(fo);
    			Column col = new Column();
                col.setIndex(index);
                col.setPropertyName(displayCN);
                cols.add(col);
                if(!sub.isolate) {
                	String displayExp;
        			String sortExp;
					displayExp = formatAggFunction(fo.getAggregateType(), fo.getField().getDisplayExpression().format(/*TODO: this needs review: topTableMap,*/aliaser), fo.getField().getDisplaySqlType());
    		    	if(!fo.getField().getDisplayExpression().equals(fo.getField().getSortExpression())) {
						sortExp = formatAggFunction(fo.getAggregateType(), fo.getField().getSortExpression().format(/*TODO: this needs review: topTableMap,*/aliaser), fo.getField().getSortSqlType());
    		    		index++;
    		    	} else {
    		    		sortExp = null;
    		    	}
    				if(fo.getField().getAdditiveType() == Field.AdditiveType.NONE && needsGroupBy(fo.getAggregateType())) {
    					topSelect.addGroupBy(displayExp);
    					if(sortExp != null)
    						topSelect.addGroupBy(sortExp);
    				}
					topSelect.addColumn(displayExp, displayCN);
					if(sortExp != null) {
						topSelect.addColumn(sortExp, sortCN);
					}
    			} else {
					extraColCount = appendFieldExpressions(fo, true, sub, topTableMap, topSelect, displayCN, extraColCount, replacements, aliaser);
    				if(!fo.getField().getDisplayExpression().equals(fo.getField().getSortExpression())) {
						extraColCount = appendFieldExpressions(fo, false, sub, topTableMap, topSelect, sortCN, extraColCount, replacements, aliaser);
    					index++;
    				}
    			}
    			col = new Column();
                col.setIndex(index++);
                col.setPropertyName(sortCN);
                cols.add(col);
    		}
        }
        //find all joins then decide where they go
		final Map<String, Join> joins = sqlBuilder.getJoins(allTableMap, aliaser, metaData);
        for(Map.Entry<String, Join> entry : joins.entrySet()) {
    		exp = entry.getKey();
    		Join join = entry.getValue();
    		SubSelect sub;
    		if(join.getTable2() == null) {
    			if(topTableMap.containsKey(join.getTable1())) {
					addJoinToSub(join, allTableMap, topTableMap, topSelect, aliaser);
    			} else {
    				sub = subSelects.get(join.getTable1());
    				if(sub == null) {
    					addTableToSub(join.getTable1(), allTableMap, topTableMap, topSelect);
						addJoinToSub(join, allTableMap, topTableMap, topSelect, aliaser);
    				} else {
						addJoinToSub(join, allTableMap, sub.tableMap, sub.select, aliaser);
    				}
    			}
    		} else if(topTableMap.containsKey(join.getTable1())) {
    			if(topTableMap.containsKey(join.getTable2())) {
					addJoinToSub(join, allTableMap, topTableMap, topSelect, aliaser);
    			} else {
    				sub = subSelects.get(join.getTable2());
    				if(sub == null) {
    					addTableToSub(join.getTable2(), allTableMap, topTableMap, topSelect);
						addJoinToSub(join, allTableMap, topTableMap, topSelect, aliaser);
    				} else {
        				// add additional tables to top
	    				for(Table addTab : join.getAdditionalTables()) {
	    					addTableToSub(addTab, allTableMap, topTableMap, topSelect);
	    				}

	    				// add additional columns to subs
	    				replacements.clear();
	    				for(simple.sql.Column col : join.getExpressionObject().getColumns()) {
	    					if(sub.tableMap.containsKey(col.getTable())) {
	        					String nm = "xcol_" + ++extraColCount;
	        					exp = sub.tableMap.get(col.getTable()) + '.' + col.getColumnName();
	        					sub.select.addColumn(exp, nm);
	        					sub.select.addGroupBy(exp);
	        					replacements.put(col, sub.alias + '.' + nm);
	    					}
	    				}
						topSelect.addWhere(join.getExpressionObject().format(/*TODO: this needs review: allTableMap,*/replacements, aliaser));
    				}
    			}
    		} else if(topTableMap.containsKey(join.getTable2())) {
				sub = subSelects.get(join.getTable1());
				if(sub == null) {
					addTableToSub(join.getTable1(), allTableMap, topTableMap, topSelect);
					addJoinToSub(join, allTableMap, topTableMap, topSelect, aliaser);
				} else {
    				// add additional tables to sub
					for(Table addTab : join.getAdditionalTables()) {
						addTableToSub(addTab, allTableMap, topTableMap, topSelect);
					}

					// add additional columns to subs
					replacements.clear();
					for(simple.sql.Column col : join.getExpressionObject().getColumns()) {
						if(sub.tableMap.containsKey(col.getTable())) {
	    					String nm = "xcol_" + ++extraColCount;
	    					exp = sub.tableMap.get(col.getTable()) + '.' + col.getColumnName();
	    					sub.select.addColumn(exp, nm);
	    					sub.select.addGroupBy(exp);
	    					replacements.put(col, sub.alias + '.' + nm);
						}
					}
					topSelect.addWhere(join.getExpressionObject().format(/*TODO: this needs review: allTableMap,*/replacements, aliaser));
				}
			} else {
				SubSelect sub1 = subSelects.get(join.getTable1());
    			SubSelect sub2 = subSelects.get(join.getTable2());
    			if(sub1 == null) {
    				if(sub2 != null) {
    					addTableToSub(join.getTable1(), allTableMap, sub2.tableMap, sub2.select);
						addJoinToSub(join, allTableMap, sub2.tableMap, sub2.select, aliaser);
    				} else {
    					addTableToSub(join.getTable1(), allTableMap, topTableMap, topSelect);
    					addTableToSub(join.getTable2(), allTableMap, topTableMap, topSelect);
						addJoinToSub(join, allTableMap, topTableMap, topSelect, aliaser);
    				}
    			} else if(sub2 == null) {
    				addTableToSub(join.getTable2(), allTableMap, sub1.tableMap, sub1.select);
					addJoinToSub(join, allTableMap, sub1.tableMap, sub1.select, aliaser);
    			} else if(sub1 == sub2) {
					addJoinToSub(join, allTableMap, sub1.tableMap, sub1.select, aliaser);
    			} else {
    				//add additional tables to top
    				for(Table addTab : join.getAdditionalTables()) {
    					addTableToSub(addTab, allTableMap, topTableMap, topSelect);
    				}
    				// add additional columns to subs
    				replacements.clear();
    				for(simple.sql.Column col : join.getExpressionObject().getColumns()) {
    					if(sub1.tableMap.containsKey(col.getTable())) {
        					String nm = "xcol_" + ++extraColCount;
        					exp = sub1.tableMap.get(col.getTable()) + '.' + col.getColumnName();
        					sub1.select.addColumn(exp, nm);
        					sub1.select.addGroupBy(exp);
        					replacements.put(col, sub1.alias + '.' + nm);
    					} else if(sub2.tableMap.containsKey(col.getTable())) {
        					String nm = "xcol_" + ++extraColCount;
        					exp = sub2.tableMap.get(col.getTable()) + '.' + col.getColumnName();
        					sub2.select.addColumn(exp, nm);
        					sub2.select.addGroupBy(exp);
        					replacements.put(col, sub2.alias + '.' + nm);
    					} else {
    						//how did we get here?
    						log.info("Column " + col + " found in join " + join.getExpression());
    					}
    				}
					topSelect.addWhere(join.getExpressionObject().format(/*TODO: this needs review: allTableMap,*/replacements, aliaser));
    			}
			}
        }
        StringBuilder ss = new StringBuilder();
        ss.append('(');
        for(SubSelect sub : subSelects.values()) {
        	if(sub.processed != 1) continue;
        	if(sub.filter != null && sub.filter.filterCount() > 0) {
        		Filter whereFilter = sub.filter.buildWhereFilter();
        		Filter havingFilter = sub.filter.buildHavingFilter();
        		// add filter to sub selects
				exp = formatFilterExpression(whereFilter, sub.tableMap, aliaser, queryParams, dbSpecific, false);
            	if(exp != null)
            		sub.select.addWhere(exp);
				exp = formatFilterExpression(havingFilter, sub.tableMap, aliaser, queryParams, dbSpecific, true);
            	if(exp != null)
            		sub.select.addHaving(exp);
        	}

        	// add sub-selects into from clause
        	ss.setLength(1);
        	sub.select.getSql(ss);
        	ss.append(')');
        	topSelect.addTable(null, ss.toString(), sub.alias);
        	sub.processed = 10;
        }
        queryParams.addAll(topQueryParams);
        cur.setColumns(cols.toArray(new Column[cols.size()]));
        //cur.setGroups(grpList.toArray());
        cur.setLazyAccess(true); // this is for memory considerations
        String sql = topSelect.getSql();
        log.debug("SQL=\n" + sql);
    	CacheableCall call = new CacheableCall(resultsCache);
        call.setSql(sql);
        call.setArguments(queryParams.toArray(new Argument[queryParams.size()]));
        call.setMinElapsedTime(getMinElapsedToCache());
        //call.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE); //needed to allow ResultSetScrollableResults
        return call;
    }

	protected void addFilter(List<Argument> params, SubSelect sub, Aliaser aliaser, DbSpecific dbSpecific) {
    	if(sub.filter != null && sub.filter.filterCount() > 0) {
    		Filter whereFilter = sub.filter.buildWhereFilter();
    		Filter havingFilter = sub.filter.buildHavingFilter();
    		// add filter to sub selects
			String exp = formatFilterExpression(whereFilter, sub.tableMap, aliaser, params, dbSpecific, false);
        	if(exp != null)
        		sub.select.addWhere(exp);
			exp = formatFilterExpression(havingFilter, sub.tableMap, aliaser, params, dbSpecific, true);
        	if(exp != null)
        		sub.select.addHaving(exp);
    	}
    }

    protected String addTables(SubSelect sub, String lastAlias) {
    	for(Table tab : sub.tables) {
			lastAlias = sqlBuilder.createAlias(lastAlias);
			sub.tableMap.put(tab, lastAlias);
			sub.select.addTable(tab.getSchemaName(), tab.getTableName(), lastAlias);
		}
    	return lastAlias;
    }

	protected int addColumns(Collection<Column> cols, SubSelect sub, Aliaser aliaser, int lastIndex) {
    	for(FieldOrder fo : sub.fieldOrders) {
			String displayCN = columnNamer.getDisplayColumnName(fo);
			String sortCN = columnNamer.getSortColumnName(fo);
			Column col = new Column();
            col.setIndex(++lastIndex);
            col.setPropertyName(displayCN);
            cols.add(col);
            if(!sub.isolate) {
            	String displayExp;
    			String sortExp;
				displayExp = formatAggFunction(fo.getAggregateType(), fo.getField().getDisplayExpression().format(/*TODO: this needs review: sub.tableMap,*/aliaser), fo.getField().getDisplaySqlType());
		    	if(!fo.getField().getDisplayExpression().equals(fo.getField().getSortExpression())) {
					sortExp = formatAggFunction(fo.getAggregateType(), fo.getField().getSortExpression().format(/*TODO: this needs review: sub.tableMap,*/aliaser), fo.getField().getSortSqlType());
		    		lastIndex++;
		    	} else {
		    		sortExp = null;
		    	}
				if(fo.getField().getAdditiveType() == Field.AdditiveType.NONE && needsGroupBy(fo.getAggregateType())) {
					sub.select.addGroupBy(displayExp);
					if(sortExp != null)
						sub.select.addGroupBy(sortExp);
				}
				sub.select.addColumn(displayExp, displayCN);
				if(sortExp != null) {
					sub.select.addColumn(sortExp, sortCN);
				}
			} col = new Column();
            col.setIndex(lastIndex);
            col.setPropertyName(sortCN);
            cols.add(col);
		}
    	return lastIndex;
	}

	@Override
	protected String formatFilterExpression(Filter filter, Map<Table, String> tableAliases, Aliaser aliaser, List<Argument> arguments, DbSpecific dbSpecific, boolean having) {
    	if(filter == null) return null;
    	if(filter instanceof FilterGroup && ((FilterGroup)filter).filterCount() == 0) return null;

		BaseMutableExpression be = new BaseMutableExpression();
		filter.buildExpression(be, dbSpecific);
		String exp = be.format(aliaser);
        Param[] params = filter.gatherParams();
        for(int k = 0; k < params.length; k++) {
			Parameter a = new FilterGroupAwareParameter(having, k);
            a.setSqlType(dbSpecific.getNormalizedType(params[k].getSqlType()));
            a.setPropertyName(params[k].getName());
            a.setDefaultValue(params[k].getValue());
            a.setRequired(false);
            arguments.add(a);
        }
        return exp;
    }

	protected int appendFieldExpressions(FieldOrder fo, boolean display, SubSelect sub, Map<Table, String> topTableMap, SelectStatement topSelect, String columnName, int lastId, Map<Expression, String> replacements, Aliaser aliaser) {
    	boolean groupBy = (fo.getField().getAdditiveType() == Field.AdditiveType.NONE && needsGroupBy(fo.getAggregateType()));
    	if(display)
			return appendFieldExpressions(fo.getField().getDisplayExpression(), fo.getAggregateType(), fo.getField().getDisplaySqlType(), groupBy, sub, topTableMap, topSelect, columnName, lastId, replacements, aliaser);
		return appendFieldExpressions(fo.getField().getSortExpression(), fo.getAggregateType(), fo.getField().getSortSqlType(), groupBy, sub, topTableMap, topSelect, columnName, lastId, replacements, aliaser);
    }

	protected int appendFieldExpressions(Expression expression, Aggregate aggregate, SQLType sqlType, boolean groupBy, SubSelect sub, Map<Table, String> topTableMap, SelectStatement topSelect, String columnName, int lastId, Map<Expression, String> replacements, Aliaser aliaser) {
    	String exp;
    	ExtendedExpression tmp;
    	if(expression instanceof ExtendedExpression
    			&& !(tmp=(ExtendedExpression)expression).getInnerExpressions().isEmpty()) {
    		replacements.clear();
    		for(Expression inner : tmp.getInnerExpressions()) {
    			String nm = "inner_" + ++lastId;
				exp = inner.format(/*TODO: this needs review: sub.tableMap,*/aliaser);
				sub.select.addColumn(exp, nm);
				sub.select.addGroupBy(exp);
				replacements.put(inner, sub.alias + '.' + nm);
    		}
			exp = expression.format(/*TODO: this needs review: topTableMap,*/replacements, aliaser);
    		exp = formatAggFunction(aggregate, exp, sqlType);
    		topSelect.addColumn(exp, columnName);
    		if(groupBy) {
				topSelect.addGroupBy(exp);
    		}
    	} else {
			exp = formatAggFunction(aggregate, expression.format(/*TODO: this needs review: sub.tableMap,*/aliaser), sqlType);
    		if(groupBy)
				sub.select.addGroupBy(exp);
			sub.select.addColumn(exp, columnName);
			topSelect.addColumn(sub.alias + '.' + columnName, columnName);
			topSelect.addGroupBy(sub.alias + '.' + columnName);
    	}
    	return lastId;
    }

    protected void addTableToSub(Table table, Map<Table,String> tableAliases, Map<Table,String> tableMap, SelectStatement select) {
    	if(!tableMap.containsKey(table)) {
			String alias = tableAliases.get(table);
	    	tableMap.put(table, alias);
	    	select.addTable(table.getSchemaName(), table.getTableName(), alias);
    	}
    }

	protected void addJoinToSub(Join join, Map<Table, String> tableAliases, Map<Table, String> tableMap, SelectStatement select, Aliaser aliaser) {
		for(Table addTab : join.getAdditionalTables()) {
			addTableToSub(addTab, tableAliases, tableMap, select);
		}
		select.addWhere(join.getExpressionObject().format(aliaser));
	}

	protected void processFilterSubSelects(FilterGroup filter, Map<Table,SubSelect> subSelects) {
		final Set<Table> tables = new TreeSet<Table>(sqlBuilder.getTableComparator());
        for(Filter f : filter.getChildFilters()) {
			if(f instanceof FilterGroup) {
				FilterGroup fg = (FilterGroup)f;
				if(FilterGroup.SEPARATOR_AND.equalsIgnoreCase(fg.getSeparator())) {
					processFilterSubSelects(fg, subSelects);
				} else {
					tables.clear();
					gatherTables(fg, tables);
					Set<Aggregate> aggs = new HashSet<Aggregate>();
					gatherAggregates(fg, aggs);
					Aggregate agg = null;
					for(Aggregate a : aggs)
						if(isIsolatationRequired(a))
							agg = a;
					SubSelect sub = findSubSelect(tables, subSelects, agg);
		        	sub.filter.addFilter(f);
				}
			} else if(f instanceof FilterItem) {
				FilterItem fi = (FilterItem) f;
				tables.clear();
				gatherTables(fi.getField(), tables);
				SubSelect sub = findSubSelect(tables, subSelects, fi.getAggregateType());
	        	sub.filter.addFilter(f);
			}
		}
	}

	protected void gatherAggregates(FilterGroup filter, Set<Aggregate> aggs) {
		for(Filter f : filter.getChildFilters()) {
			if(f instanceof FilterGroup) {
				gatherAggregates((FilterGroup) f, aggs);
			} else if(f instanceof FilterItem) {
				aggs.add(((FilterItem) f).getAggregateType());
			}
		}
	}

	protected void gatherTables(FilterGroup filter, Set<Table> tables) {
		for(Filter f : filter.getChildFilters()) {
			if(f instanceof FilterGroup) {
				gatherTables((FilterGroup) f, tables);
			} else if(f instanceof FilterItem) {
				gatherTables(((FilterItem) f).getField(), tables);

			}
		}
	}

	protected void gatherTables(Field field, Set<Table> tables) {
		for(simple.sql.Column col : field.getDisplayExpression().getColumns()) {
			tables.add(col.getTable());
		}
		if(!field.getDisplayExpression().equals(field.getSortExpression())) {
			for(simple.sql.Column col : field.getSortExpression().getColumns()) {
				tables.add(col.getTable());
			}
    	}
	}

	protected SubSelect findSubSelect(Set<Table> tables, Map<Table,SubSelect> subSelects, Aggregate aggregate) {
		SubSelect sub;
    	Table table = null;
    	switch(tables.size()) {
    		case 1:
    			//get or find the appropriate subselect
    			table = tables.iterator().next();
    		case 0:
    			sub = subSelects.get(table);
    			if(sub == null) {
    				sub = new SubSelect();
    				subSelects.put(table, sub);
    			}
    			break;
    		default: // must link this to the other tables - ugh!
    			sub = null;
    			for(Table tab : tables) {
    				SubSelect ss = subSelects.get(tab);
        			if(ss != null) {
        				if(sub == null) {
        					sub = ss;
        				} else {
        					//merge
        					sub.fieldOrders.addAll(ss.fieldOrders);
        					if(sub.filter.filterCount() == 0)
        						sub.filter = ss.filter;
        					else if(ss.filter.filterCount() > 0)
        						sub.filter.addFilter(ss.filter);
        					sub.tables.addAll(ss.tables);
        					subSelects.put(tab, sub);
        				}
        			} else if(sub != null) {
        				subSelects.put(tab, sub);
        			} else {
        				sub = new SubSelect();
        				subSelects.put(tab, sub);
        			}
    			}
    	}
		sub.tables.addAll(tables);
		if(!sub.isolate && isIsolatationRequired(aggregate)) {
			sub.isolate = true;
		}
		return sub;
	}

	protected boolean isIsolatationRequired(Aggregate aggregate) {
		if(aggregate == null) return false;
		switch(aggregate) {
			case SUM:
			case COUNT:
			case AVG:
				return true;
			default:
				return false;
		}
	}
    /*
    protected Expression removeAggFunctions(Expression exp, Collection<Aggregate> aggregates, DatabaseMetaData metaData) {
    	return sqlBuilder.createExpression(removeAggFunctions(exp.toString(), aggregates, metaData));
    }
    protected static final Pattern distinctInCountPattern = Pattern.compile("\\s*[(]\\s*DISTINCT\\s+", Pattern.CASE_INSENSITIVE);
    protected static final Pattern castCollectPattern = Pattern.compile("\\s*[(]\\s*COLLECT\\s*[(]\\s*DISTINCT\\s+(.*)[)]\\s+AS\\s+(?:\\w+\\s*[.]\\s*)?\\w+[)]", Pattern.CASE_INSENSITIVE);
    protected String removeAggFunctions(String exp, final Collection<Aggregate> aggregates, DatabaseMetaData metaData) {
    	final StringBuilder sb = new StringBuilder();
		final Holder<Integer> lastPos = new Holder<Integer>();
		final Set<String> aggFunctions = sqlBuilder.getAggregateFunctions(metaData);
		lastPos.setValue(0);
		SQLBuilder.parseDataColumn(exp, new SQLBuilder.TableColumnHandler(){
			public void handleFunction(String s, int wordStart, int lastDot, int wordEnd) {
				String function = s.substring(wordStart, wordEnd).toUpperCase();
                if(aggFunctions.contains(function)) {
                	if(lastPos.getValue() < wordStart) sb.append(s.substring(lastPos.getValue(), wordStart));
                	lastPos.setValue(wordEnd);
                	Aggregate agg;
                	try {
                		agg = ConvertUtils.convert(Aggregate.class, function);
                	} catch(ConvertException e) {
                		agg = null;
                	}
                	if(agg != null) {
                		if(Aggregate.COUNT.equals(agg)) {
                			Matcher matcher = distinctInCountPattern.matcher(s);
                			matcher.region(wordEnd, s.length());
                			if(matcher.lookingAt()) {
                				agg = Aggregate.DISTINCT;
                				sb.append('(');
                				lastPos.setValue(matcher.end());
                			}
                		}
                		aggregates.add(agg);
                	} else if("CAST".equalsIgnoreCase(function)) {
                		Matcher matcher = castCollectPattern.matcher(s);
            			matcher.region(wordEnd, s.length());
            			if(matcher.lookingAt()) {
            				agg = Aggregate.SET;
            				sb.append(matcher.group(1));
            				lastPos.setValue(matcher.end());
            			}
                	}
                }
			}
			public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) {
			}
			public void handleBeginParen(String s, int pos) {
			}
			public void handleEndParen(String s, int pos) {
			}

		});
		if(lastPos.getValue() == 0) return exp;
		sb.append(exp.substring(lastPos.getValue()));
		return sb.toString();
	}
    */

}
