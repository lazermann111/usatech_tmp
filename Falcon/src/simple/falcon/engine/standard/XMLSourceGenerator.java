package simple.falcon.engine.standard;

import java.util.Map;

import javax.xml.transform.sax.SAXSource;

import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.results.Results;

public class XMLSourceGenerator extends XMLBaseGenerator {
	protected SAXSource source;
	public XMLSourceGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
		super(engine, inputSourceType);
	}

	public void generate(Report report, OutputType outputType, Map<String, Object> parameters,
			Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
		try {
			source = createXMLSource(report, results, parameters, executor, scene, outputType);
		} catch(DesignException e) {
			throw new ExecuteException(e);
		}
	}

	public SAXSource getSource() {
		return source;
	}
}
