/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.results.ResultsFilter;

public class SingleColumnRetriever extends AbstractResultsValueRetriever {
    protected String columnName;
    protected Aggregate aggregate;
    protected int columnIndex=-1;
    public SingleColumnRetriever(String columnName, Aggregate aggregate) {
        this.columnName = columnName;
        this.aggregate = aggregate;
    }
    public Object getValue(Results results, Map<String,Object> context) {
        return wrapWithContext(getValue(results, columnName), context);
    }
    public Object getAggregate(Results results, ResultsFilter filter, int groupColumnIndex, Map<String,Object> context) {
    	return wrapWithContext(getAggregate(results, filter, columnIndex, aggregate, groupColumnIndex), context);
    }
	public void trackAggregates(Results results, ResultsFilter filter) {
		if(columnIndex==-1){
    		columnIndex=results.getColumnIndex(columnName);
    	}
		trackAggregate(results, filter, columnIndex, aggregate);
	}
	public List<String> addGroups(Results results) {
		String gcn = addGroup(results, columnName, aggregate);
		if(gcn == null)
			return Collections.emptyList();
		return Collections.singletonList(gcn);
	}
	protected Object wrapWithContext(final Object value, final Map<String,Object> context) {
		return new ValueWithContext(value, context);
	}
}