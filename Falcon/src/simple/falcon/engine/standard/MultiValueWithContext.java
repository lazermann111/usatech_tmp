/**
 *
 */
package simple.falcon.engine.standard;

import java.beans.IntrospectionException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.DynaProperty;

import simple.bean.AbstractDynaBean;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Convertible;
import simple.bean.DynaBean;
import simple.bean.ReflectionUtils;
import simple.lang.SystemUtils;
import simple.util.CollectionUtils;

public class MultiValueWithContext extends AbstractDynaBean implements Convertible, Serializable {
	private static final long serialVersionUID = 3565074821338244937L;
	protected final static Pattern arrayPattern = Pattern.compile("\\s*(?:(\\d+)|(?:\\[(\\d+)\\]))(?:(?:\\.(.+))|(\\[.+)|(\\(.+))?\\s*");
	protected final Object[] value;
	protected final Map<String,Object> context;
	protected transient DynaProperty[] dynaClassProperties;

	public MultiValueWithContext(Object[] value, Map<String, Object> context) {
		this.value = value;
		this.context = context;
	}

	@Override
	public Object get(String name) {
		if(name == null || name.trim().length() == 0) return value;
		Matcher matcher = arrayPattern.matcher(name);
		if(matcher.matches()) {
			int index = Integer.parseInt(SystemUtils.nvl(matcher.group(1), matcher.group(2)));
			if(value == null || index >= value.length) return null;
			String remaining = SystemUtils.nvl(matcher.group(3), matcher.group(4), matcher.group(5));
			if(remaining != null && remaining.length() > 0) {
				try {
					return ReflectionUtils.getProperty(value[index], remaining);
				} catch(IntrospectionException e) {
					return null;
				} catch(IllegalAccessException e) {
					return null;
				} catch(InvocationTargetException e) {
					return null;
				} catch(ParseException e) {
					return null;
				}
			}
			return value[index];
		}
		try {
			return ReflectionUtils.getMapProperty(context, name);
		} catch(IntrospectionException e) {
			return null;
		} catch(IllegalAccessException e) {
			return null;
		} catch(InvocationTargetException e) {
			return null;
		} catch(ParseException e) {
			return null;
		}
	}

	@Override
	public Object get(String name, int index) {
		if(name == null || name.trim().length() == 0)
			return (value != null && index < value.length ? value[index] : null);
		return super.get(name, index);
	}

	@Override
	public void remove(String name, String key) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}

	@Override
	public void set(String name, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}

	@Override
	public void set(String name, int index, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}

	@Override
	public void set(String name, String key, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}

	@Override
	protected String getDynaClassName() {
		return "ArrayWithContext";
	}

	@Override
	protected Object getDynaProperty(String name) {
		return get(name);
	}

	@Override
	protected boolean hasDynaProperty(String name) {
		if(name == null || name.trim().length() == 0) return true;
		Matcher matcher = arrayPattern.matcher(name);
		if(matcher.matches()) {
			int index = Integer.parseInt(SystemUtils.nvl(matcher.group(1), matcher.group(2)));
			if(value == null || index >= value.length) return false;
			String remaining = SystemUtils.nvl(matcher.group(3), matcher.group(4), matcher.group(5));
			if(remaining != null && remaining.length() > 0) {
				try {
					return ReflectionUtils.hasProperty(value[index], remaining);
				} catch(IntrospectionException e) {
					return false;
				} catch(IllegalAccessException e) {
					return false;
				} catch(InvocationTargetException e) {
					return false;
				} catch(ParseException e) {
					return false;
				}
			}
			return true;
		}
		try {
			return ReflectionUtils.hasProperty(context, name);
		} catch(IntrospectionException e) {
			return false;
		} catch(IllegalAccessException e) {
			return false;
		} catch(InvocationTargetException e) {
			return false;
		} catch(ParseException e) {
			return false;
		}
	}

	@Override
	protected void setDynaProperty(String name, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}

	@Override
	protected DynaProperty[] getDynaClassProperties() {
		if(dynaClassProperties == null) {
			DynaProperty[] dps = new DynaProperty[1+(value == null ? 0 : value.length) + context.size()];
			dps[0] = new DynaProperty("");
			int i = 1;
			if(value != null)
				for(; i < value.length; i++)
					dps[i] = new DynaProperty(String.valueOf(i));
			for(String name : context.keySet())
				dps[i++] = new DynaProperty(name);
			dynaClassProperties = dps;
		}
		return dynaClassProperties;
	}

	@Override
	public String toString() {
		return value == null ? "" : CollectionUtils.deepToString(value);
	}

	public <Target> Target convert(Class<Target> toClass) throws ConvertException {
		return ConvertUtils.convert(toClass, value);
	}
	@Override
	public int hashCode() {
		return value == null ? 0 : value.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return CollectionUtils.deepEquals(value, stripValue(obj));
	}
	protected Object stripValue(Object value) {
    	if(value instanceof Convertible)
			try {
				return ((Convertible)value).convert(Object.class);
			} catch(ConvertException e) {
				//ignore
			}
    	Object tmp;
    	if(value instanceof DynaBean && (tmp=((DynaBean)value).get(null)) != null)
    		return tmp;
    	return value;
    }

	public int getValueSize(){
		return value==null?0:value.length;
	}
	
}