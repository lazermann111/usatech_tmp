/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.DynaClass;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.db.Parameter;
import simple.db.specific.DbSpecific;
import simple.falcon.engine.Field;
import simple.falcon.engine.NoColumnExpression;
import simple.falcon.engine.Operator;
import simple.results.Results.Aggregate;
import simple.sql.ArraySQLType;
import simple.sql.BaseMutableExpression;
import simple.sql.Expression;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.MessageFormat;
import simple.text.StringUtils;

public class StandardOperator extends Operator {
	protected static final Expression PARAMETER_EXPRESSION = new NoColumnExpression("?");
	protected MessageFormat expression;
	protected Integer[] parameterSqlTypes;
    protected SQLType[] applicableSqlTypes;
    protected String xml;
    protected String pattern;
    protected Side side;
    protected StandardDesignEngine designEngine;

    protected interface ExpressionMaker {
    	public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) ;

		public void addParameters(Collection<Parameter> parameters, Field field);
    }

    protected ExpressionMaker[] makers;

    public StandardOperator() {
        super();
    }

    public StandardOperator(String pattern, Integer[] parameterSqlTypes) {
        this();
        //make pattern safe and find all ? (params)
        parsePattern(pattern);
        this.parameterSqlTypes = parameterSqlTypes;
    }
    /*
    protected BaseOperator(String pattern, int paramCount, Integer[] sqlTypes) {
        this.expression = new MessageFormat(pattern);
        this.paramCount = paramCount;
        this.sqlTypes = sqlTypes;
    }
    public BaseOperator(int operatorId, String op, String sep, String prefix, String suffix, int paramCount, int fieldIndex, boolean parens) {
        this(toPattern(operatorId, op, sep, prefix, suffix, paramCount, fieldIndex, parens), paramCount, new Integer[paramCount]);
    }

    protected static String toPattern(int operatorId, String op, String sep, String prefix, String suffix, int paramCount, int fieldIndex, boolean parens) {
        if(sep == null || sep.trim().length() == 0) sep = " ";
        else sep = " " + sep.trim() + " ";
        sep = sep == null ? "" : sep;
        String s = "";
        for(int i = 0; i < paramCount + 1; i++) {
            if(prefix != null) s += prefix;
            if(i == fieldIndex) s += "{display}";
            else s += "?";
            if(suffix != null) s += suffix;
            if(i == 0) {
                s += " " + op;
                if(parens) s += "(";
                else if(paramCount != 0) s += " ";
            } else if(i < paramCount) {
                s += sep;
            }
        }
        if(parens) s += ")";
        return s;
    }
    */

    protected void parsePattern(String pattern) {
        //make pattern safe and find all ? (params)
    	List<ExpressionMaker> makerList = new ArrayList<ExpressionMaker>();
        expression = new MessageFormat(pattern);
		int paramCount = 0;
        MessageFormat.FormatObject[] fos = expression.getFormatObjects();
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < fos.length; i++) {
			if(fos[i].parameters == null) {// it's a literal piece
                String s = fos[i].format.format(null);
                boolean inQuote = false;
                int lastParamPos = 0;
                for(int p = 0; p < s.length(); p++) {
                    char ch = s.charAt(p);
                    switch(ch) {
                        case '\'':
                            inQuote = !inQuote;
                            sb.append("&#39;");
                            break;
                        case '?':
                            if(inQuote) sb.append(ch);
                            else {
								String piece = s.substring(lastParamPos, p);
								if(piece.trim().length() > 0) {
									final Expression exp = new NoColumnExpression(piece);
									makerList.add(new ExpressionMaker() {
										public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) {
											return exp;
										}

										public void addParameters(Collection<Parameter> parameters, Field field) {
										}
									});
								}
								lastParamPos = p + 1;
								final int paramIndex = paramCount;
								makerList.add(new ExpressionMaker() {
									public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) {
										if(parameterSqlTypes != null && parameterSqlTypes.length > paramIndex && parameterSqlTypes[paramIndex] != null && parameterSqlTypes[paramIndex] == Types.ARRAY)
											return dbSpecific.createParameterExpression(PARAMETER_EXPRESSION, Aggregate.ARRAY, field.getDisplaySqlType());
										return PARAMETER_EXPRESSION;
									}

									public void addParameters(Collection<Parameter> parameters, Field field) {
										Parameter parameter = new Parameter();
										if(parameterSqlTypes != null && parameterSqlTypes.length > paramIndex && parameterSqlTypes[paramIndex] != null) {
											if(parameterSqlTypes[paramIndex] == Types.ARRAY)
												parameter.setSqlType(new ArraySQLType(field != null ? field.getDisplaySqlType() : new SQLType(Types.OTHER)));
											else
												parameter.setSqlType(new SQLType(parameterSqlTypes[paramIndex]));
										} else
											parameter.setSqlType(field != null ? field.getDisplaySqlType() : new SQLType(Types.OTHER));
										parameters.add(parameter);
									}
								});
								sb.append("<param index=\"");
                                sb.append(paramCount++);
                                sb.append("\"/>");                                
                            }
                            break;
                        case '<':
                            sb.append("&lt;");
                            break;
                        case '&':
                            sb.append("&amp;");
                            break;
                        case '>':
                            sb.append("&gt;");
                            break;
                        case '"':
                            sb.append("&quot;");
                            break;
                        default:
                            sb.append(ch);
                    }
                }
                
                String piece = s.substring(lastParamPos);
                if(piece.trim().length() > 0) {
            		final Expression exp = new NoColumnExpression(piece);
                    makerList.add(new ExpressionMaker() {
    					public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) {
    						return exp;
    					}

						public void addParameters(Collection<Parameter> parameters, Field field) {
						}
                    });
                }
			} else if(fos[i].parameters.length != 1) {
				throw new IllegalArgumentException("Parameter '" + StringUtils.join(fos[i].parameters, "&") + "' is not valid; please use 'display' or 'sort'");
			} else if("display".equalsIgnoreCase(fos[i].parameters[0]) || "0".equals(fos[i].parameters[0])) {
            	sb.append("<field attribute=\"");
				sb.append(fos[i].parameters[0]);
                sb.append("\"/>");
				if(side == Side.SORT)
					side = Side.BOTH;
				else
					side = Side.DISPLAY;
                makerList.add(new ExpressionMaker() {
					public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) {
						return dbSpecific.createAggregateExpression(field.getDisplayExpression(), aggregate, field.getDisplaySqlType());
					}

					public void addParameters(Collection<Parameter> parameters, Field field) {
						if(field != null && field.getDisplayExpression().getParameters() != null)
							parameters.addAll(field.getDisplayExpression().getParameters());
					}
                });
			} else if("sort".equalsIgnoreCase(fos[i].parameters[0])) {
            	sb.append("<field attribute=\"");
				sb.append(fos[i].parameters[0]);
                sb.append("\"/>");
				if(side == Side.DISPLAY)
					side = Side.BOTH;
				else
					side = Side.SORT;
                makerList.add(new ExpressionMaker() {
					public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) {
						return dbSpecific.createAggregateExpression(field.getSortExpression(), aggregate, field.getSortSqlType());
					}

					public void addParameters(Collection<Parameter> parameters, Field field) {
						if(field != null && field.getSortExpression().getParameters() != null)
							parameters.addAll(field.getSortExpression().getParameters());
					}
                });
            } else {
				throw new IllegalArgumentException("Parameter '" + fos[i].parameters[0] + "' is not valid; please use 'display' or 'sort'");
            }
        }
        xml = sb.toString();
        makers = makerList.toArray(new ExpressionMaker[makerList.size()]);
    }

    /**
     * @return Returns the pattern.
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * @param pattern The pattern to set.
     */
    public void setPattern(String pattern) {
        this.pattern = pattern;
        parsePattern(pattern);
    }

	public int getParameterCount() {
		return getParameterCount(null);
	}
    @Override
	public int getParameterCount(Field field) {
		return getParameters(field).length;
    }

    @Override
	public Expression getExpression(Field field, Aggregate aggregate, DbSpecific dbSpecific) {
		BaseMutableExpression be = new BaseMutableExpression();
        for(ExpressionMaker em : makers) {
            be.append(em.getExpression(field, aggregate, dbSpecific));
        }
        return be;
    	/*
        return expression.format(new DynaBean() {
            public boolean contains(String arg0, String arg1) {
                return false;
            }

            public Object get(String key) {
                if("display".equalsIgnoreCase(key))
                    return field.getDisplayExpression();
                else if("sort".equalsIgnoreCase(key))
                    return field.getSortExpression();
                return null;
            }

            public Object get(String arg0, int arg1) {
                return null;
            }

            public Object get(String arg0, String arg1) {
                return null;
            }

            public DynaClass getDynaClass() {
                return null;
            }

            public void remove(String arg0, String arg1) {
            }

            public void set(String arg0, Object arg1) {
            }

            public void set(String arg0, int arg1, Object arg2) {
            }

            public void set(String arg0, String arg1, Object arg2) {
            }

        });*/
    }

    @Override
	public Side getSide() {
        return side;
    }

	public Integer[] getParameterSqlTypes() {
		return parameterSqlTypes;
	}

	@Override
	public Parameter[] getParameters(Field field) {
		List<Parameter> parameters = new ArrayList<Parameter>();
		for(ExpressionMaker em : makers)
			em.addParameters(parameters, field);
		return parameters.toArray(new Parameter[parameters.size()]);
    }

    protected static SQLType findTypeToUse(MessageFormat expression, Field field) {
        switch(findSide(expression)) {
            case DISPLAY: return field.getDisplaySqlType();
            case SORT: return field.getSortSqlType();
            case BOTH: return field.getSortSqlType();
            default: return new SQLType(Types.OTHER); //??? unknown return new SQLType(Types.NULL); //??? unknown
        }
    }
    protected static Side findSide(MessageFormat expression) {
        Side s = Side.NONE;
        for(MessageFormat.FormatObject fo : expression.getFormatObjects()) {
			if(fo.parameters == null || fo.parameters.length != 1)
				continue;
			if("display".equalsIgnoreCase(fo.parameters[0])) {
                switch(s) {
                    case NONE: s = Side.DISPLAY; break;
                    case SORT: s = Side.BOTH; break;
                }
			} else if("sort".equalsIgnoreCase(fo.parameters[0])) {
                switch(s) {
                    case NONE: s = Side.SORT; break;
                    case DISPLAY: s = Side.BOTH; break;
                }
            }
        }
        return s;
    }

    protected final static DynaBean toStringBean = new DynaBean() {
        public boolean contains(String arg0, String arg1) {
            return false;
        }

        public Object get(String key) {
            return "[" + key.toUpperCase() + "]";
        }

        public Object get(String arg0, int arg1) {
            return null;
        }

        public Object get(String arg0, String arg1) {
            return null;
        }

        public DynaClass getDynaClass() {
            return null;
        }

        public void remove(String arg0, String arg1) {
        }

        public void set(String arg0, Object arg1) {
        }

        public void set(String arg0, int arg1, Object arg2) {
        }

        public void set(String arg0, String arg1, Object arg2) {
        }
    };

    @Override
	public String toString() {
        return expression.format(toStringBean);
    }

    @Override
	public String getXml() {
        return xml;
    }

    /**
     * @param parameterSqlTypes The SQL Type Contant for each parameter. A null indicates to use whatever
     *  type the field is.
     */
    public void setParameterSqlTypes(Integer[] parameterSqlTypes) {
        this.parameterSqlTypes = parameterSqlTypes;
    }

    /**
     * @param parameterSqlTypes The SQL Type Contant for each parameter. A null indicates to use whatever
     *  type the field is.
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws SecurityException
     */
    public void setParameterSqlTypeStrings(String[] parameterSqlTypeStrings) throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
    	if(parameterSqlTypeStrings == null)
            this.parameterSqlTypes = null;
        else {
            Integer[] types = new Integer[parameterSqlTypeStrings.length];
            for(int i = 0; i < types.length; i++)
                if(parameterSqlTypeStrings[i] != null && parameterSqlTypeStrings[i].trim().length() > 0)
                    types[i] = SQLTypeUtils.getTypeCode(parameterSqlTypeStrings[i].trim());
            this.parameterSqlTypes = types;
        }
    }

    @Override
    public SQLType[] getApplicableSqlTypes() {
        return applicableSqlTypes;
    }

    /**
     * @param applicableSqlTypes The applicableSqlTypes to set.
     */
    public void setApplicableSqlTypes(SQLType[] applicableSqlTypes) {
        this.applicableSqlTypes = applicableSqlTypes;
    }

    public void setApplicableSqlTypes(int[] applicableSqlTypes) {
        SQLType[] types = new SQLType[applicableSqlTypes.length];
        for(int i = 0; i < types.length; i++) types[i] = new SQLType(applicableSqlTypes[i]);
        this.applicableSqlTypes = types;
    }

    public void setApplicableSqlTypeStrings(String[] applicableSqlTypeStrings) throws ConvertException {
        SQLType[] types = new SQLType[applicableSqlTypes.length];
        for(int i = 0; i < types.length; i++) types[i] = ConvertUtils.convert(SQLType.class, applicableSqlTypeStrings[i]);
        this.applicableSqlTypes = types;
    }

    protected StandardDesignEngine getDesignEngine() {
        return designEngine;
    }

    void setDesignEngine(StandardDesignEngine designEngine) {
        this.designEngine = designEngine;
    }
}