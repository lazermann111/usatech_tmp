/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import simple.bean.ReflectionUtils;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Generator;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.results.Results;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class DirectGenerator implements Generator {
	protected final ExecuteEngine engine;
	protected final Cache<String, Generator, ExecuteException> delegates = new LockSegmentCache<String, Generator, ExecuteException>(10, 0.75f, 4) {
		@Override
		protected Generator createValue(String key, Object... additionalInfo) throws ExecuteException {
			Class<? extends Generator> genClass;
			try {
				genClass = Class.forName(key).asSubclass(Generator.class);
			} catch(ClassNotFoundException e) {
				throw new ExecuteException("Could not find Generator class '" + key + "'", e);
			} catch(ClassCastException e) {
				throw new ExecuteException("Class '" + key + "' does not implement " + Generator.class.getName(), e);
			}
			try {
				return ReflectionUtils.createObject(genClass, engine);
			} catch(IllegalArgumentException e) {
				throw new ExecuteException("Could not create instance of '" + key + "'", e);
			} catch(InstantiationException e) {
				throw new ExecuteException("Could not create instance of '" + key + "'", e);
			} catch(IllegalAccessException e) {
				throw new ExecuteException("Could not create instance of '" + key + "'", e);
			} catch(InvocationTargetException e) {
				throw new ExecuteException("Could not create instance of '" + key + "'", e);
			}
		}
	};

    public DirectGenerator(ExecuteEngine engine) {
        this.engine = engine;
    }

    public void generate(Report report, OutputType outputType, Map<String,Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
		getDelegate(outputType).generate(report, outputType, parameters, results, executor, scene, out);
	}

	protected Generator getDelegate(OutputType outputType) throws ExecuteException {
		String classname = StringUtils.substringBefore(outputType.getPath(), ":");
		return getDelegate(classname);
	}
	protected Generator getDelegate(String classname) throws ExecuteException {
		return delegates.getOrCreate(classname);
    }

    public boolean isResetResultsRequired(OutputType outputType) {
		try {
			return getDelegate(outputType).isResetResultsRequired(outputType);
		} catch(ExecuteException e) {
			return true;
		}
    }
}