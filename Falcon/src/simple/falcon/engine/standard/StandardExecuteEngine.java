/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine.standard;

import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.CacheableCall;
import simple.db.Call;
import simple.db.ConnectionGroup;
import simple.db.Cursor;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.db.PlainConnectionGroup;
import simple.event.TaskListener;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignEngine.InvalidField;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Generator;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsType;
import simple.falcon.engine.Scene;
import simple.falcon.engine.util.EngineUtils;
import simple.io.ResourceResolver;
import simple.results.Results;
import simple.sql.BuildSQLException;
import simple.sql.SQLType;
import simple.xml.TemplatesLoader;

/**
 * @author bkrug
 *
 */
public class StandardExecuteEngine implements ExecuteEngine {
    private static final simple.io.Log log = simple.io.Log.getLog();

    protected DataSourceFactory dataSourceFactory;
	protected String designDataSourceName = "metadata";
	protected String defaultDataSourceName = "report";
    protected final DesignEngine designEngine;
    protected ResourceResolver resourceResolver;
	protected String cssDirectory = "";
    protected TaskListener taskListener;
    protected final Map<String, Generator> generators = new HashMap<String, Generator>();
    protected boolean gcEnabled = false; // it is not usually a good idea to call System.gc() explicitly - see http://java.sun.com/javase/technologies/hotspot/gc/gc_tuning_6.html#cms.oom
    protected static final ReentrantLock gcLock = new ReentrantLock();
    protected static final Condition gcSignal = gcLock.newCondition();
    protected static GCThread gcThread;

    protected static class GCThread extends Thread {
    	protected volatile boolean done = false;
        @Override
		public void run() {
            while(!done) {
                long heapBefore = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
                System.gc();
                long heapAfter = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
                log.debug("Reclaimed " + ((heapBefore-heapAfter)/1024) + " KB of Heap; Now using " + (heapAfter/1024) + " KB of heap");
                gcLock.lock();
                try {
                    gcSignal.awaitUninterruptibly();
                } finally {
                    gcLock.unlock();
                }
            }
            log.debug("Garbarge Collector Thread is stopped");
        }
    }

	public static StandardExecuteEngine getInstance() {
    	StandardDesignEngine sde = new StandardDesignEngine();
        StandardExecuteEngine ee = new StandardExecuteEngine(sde);
    	return ee;
    }

	public StandardExecuteEngine(DesignEngine designEngine) {
    	if(designEngine == null)
			throw new NullPointerException("Design Engine must be provided");
		this.designEngine = designEngine;
		registerDefaultGenerators();
    }

    protected static GCThread initGCThread() {
		GCThread gct = new GCThread();
		gct.setName("Garbage Collector");
		gct.setPriority(2);
		gct.setDaemon(true);
		gct.start();
        return gct;
    }

    protected void signalGarbageCollection() {
    	if(gcEnabled) {
	    	gcLock.lock();
	        try {
	            gcSignal.signalAll();
	            if(gcThread == null) {
	            	gcThread = initGCThread();
	            }
	        } finally {
	            gcLock.unlock();
	        }
    	}
    }
    protected void shutdownGarbageCollection() {
    	gcLock.lock();
        try {
            if(gcThread != null) {
            	gcThread.done = true;
            	gcSignal.signalAll();
            	gcThread = null;
            }
        } finally {
            gcLock.unlock();
        }
    }

    public void registerGenerator(String protocol, Generator generator) {
        generators.put(protocol, generator);
    }

    /**
     *
     */
    protected void registerDefaultGenerators() {
        generators.put("xsl", new XSLGenerator(this));
        generators.put("csv", new CSVGenerator(this));
        generators.put("pdf", new PDFGenerator(this));
        generators.put("fo", new FOGenerator(this));
        generators.put("chart-xsl", new ChartXSLGenerator(this));
        generators.put("chart-fo", new ChartFOGenerator(this));
        generators.put("chart-pdf", new ChartPDFGenerator(this));
        generators.put("fixed", new FixedWidthGenerator(this));
		generators.put("direct", new DirectGenerator(this));
    }

    public void buildReport(Report report, Map<String,Object> parameters, Executor executor, OutputType outputType, Scene scene, Output out, ResultsType resultsType)
            throws ExecuteException, DesignException, ParameterException {
        // determine content-type and report generator
        Generator generator = generators.get(outputType.getProtocol());
        if(generator == null)
            throw new ExecuteException("Could not find generator for '" + outputType.getProtocol() + "'");
        buildReport(generator, report, parameters, executor, outputType, scene, out, resultsType);
    }
   
    public Results[] buildResults(Report report, Map<String,Object> parameters, Executor executor, Scene scene, ResultsType resultsType)
    		throws ExecuteException, DesignException, ParameterException {
		ConnectionGroup connGroup = new PlainConnectionGroup(getDataSourceFactory());
        try {
			return buildResults(report, EngineUtils.createReportParameters(parameters, executor, scene), executor, connGroup, resultsType, false);
		} catch(SQLException e) {
			throw new ExecuteException(e);
		} catch(DataLayerException e) {
			throw new ExecuteException(e);
		} catch(BuildSQLException e) {
			throw new ExecuteException(e);
		} finally {
			connGroup.close(false);
        }
    }
    public void buildReport(Generator generator, Report report, Map<String,Object> parameters, Executor executor, OutputType outputType, Scene scene, Output out, ResultsType resultsType)
        	throws ExecuteException, DesignException, ParameterException {
    	try {
			boolean closed = false;
			ConnectionGroup connGroup = new PlainConnectionGroup(getDataSourceFactory());
            try {
				Results[] results = buildResults(report, EngineUtils.createResultParameters(parameters, executor, scene), executor, connGroup, resultsType, generator.isResetResultsRequired(outputType));
            	try {
					if(!resultsType.isLazyAccess()) {
						connGroup.close(false);
						closed = true;
					}
	                if(taskListener != null) {
	                	StringBuilder sb = new StringBuilder();
	                	if(report.getReportId() != null)
	                		sb.append("ReportId=").append(report.getReportId());
	                	else {
	                		sb.append("FolioId=");
	                		for(Folio folio : report.getFolios()) {
	                			sb.append(folio.getFolioId()).append('.');
	                		}
	                		sb.setLength(sb.length()-1);
	                	}
	                	taskListener.taskStarted("generate-report", sb.append("; OutputType=").append(outputType.getLabel()).toString());
	                }
	                	    
	                generator.generate(report, outputType, EngineUtils.createReportParameters(parameters, executor, scene), results, executor, scene, out);
	                if(taskListener != null) taskListener.taskEnded("generate-report");
            	} finally {
            		for(Results r : results) if(r != null) r.close();
            	}
            } finally {
                // close the results and connection AFTER running the generator in case the Call object uses a scrollable resultset
				if(!closed)
					connGroup.close(false);
                signalGarbageCollection();
            }
        } catch(SQLException e) {
            throw new ExecuteException(e);
        } catch(DataLayerException e) {
            throw new ExecuteException(e);
		} catch(BuildSQLException e) {
			throw new ExecuteException(e);
		}
    }

	protected Results[] buildResults(Report report, Map<String, Object> parameters, Executor executor, ConnectionGroup connGroup, ResultsType resultsType, boolean resettable) throws ExecuteException, DesignException, SQLException, DataLayerException, BuildSQLException {
    	// create Results
	    Folio[] folios = report.getFolios();
	    for(Folio folio : folios)
	        if(folio.getFieldOrders().length == 0 && folio.getQuery()==null)
	            throw new ExecuteException("You are not authorized to view any of the data on this report");
	    // set the executor in the parameters
	    final long[] userGroupIds = (executor == null || executor.getUserGroupIds() == null ? new long[0] : executor.getUserGroupIds());
	    CacheableCall[] calls = new CacheableCall[folios.length];
	    Results[] results = new Results[folios.length];
	    log.debug("Executing " + folios.length + " queries using " + resultsType.isUseCache() + ", " + resultsType.getResultSetType() + ", " + resultsType.isLazyAccess() + " resultsType");
	    for(int i = 0; i < folios.length; i++) {
			Connection conn = connGroup.getConnection(designEngine.findDataSourceName(folios[i]), true);
			if(taskListener != null)
				taskListener.taskStarted("get-call", "FolioId=" + folios[i].getFolioId());
			calls[i] = designEngine.getCall(folios[i], userGroupIds, conn);
			if(taskListener != null)
				taskListener.taskEnded("get-call");
			// set the cursor types
			Cursor cur = (Cursor) calls[i].getArgument(0);
			calls[i].setResultSetType(resultsType.getResultSetType());
			cur.setCaching(resettable);
			cur.setLazyAccess(resultsType.isLazyAccess());
			// set defaults from existing
			for(int k = 1; k < calls[i].getArgumentCount(); k++) {
				Argument a = calls[i].getArgument(k);
				if(a.isIn() && a instanceof Parameter) {
					Parameter p = (Parameter) a;
					if(p.getDefaultValue() != null && p.getPropertyName() != null && !parameters.containsKey(p.getPropertyName()))
						parameters.put(p.getPropertyName(), p.getDefaultValue());
				}
			}

			// NOTE: there may be additional arguments that come from the user-group filters added to the folio
			// execute the call
			if(taskListener != null)
				taskListener.taskStarted("execute-call", "SQL=" + calls[i].getSql());
			try {
				results[i] = calls[i].executeQuery(conn, parameters, resultsType.isUseCache(), null);
			} catch(SQLException e) {
				if(DataLayer.DEFAULT_RETRY.decide(conn, e)) {
					conn.close();
					conn = designEngine.findConnection(folios[i]);
					results[i] = calls[i].executeQuery(conn, parameters, resultsType.isUseCache(), null);
				} else {
					throw e;
				}
			}
            if(taskListener != null) taskListener.taskEnded("execute-call");
        }
        return results;
	}

    protected String getSortType(Class<?> dataClass) {
        if(Number.class.isAssignableFrom(dataClass))
            return "NUMBER";
        else if(java.util.Date.class.isAssignableFrom(dataClass))
            return "DATE";
        else
            return "STRING";
    }

	public DesignEngine getDesignEngine() throws DesignException {
		return designEngine;
    }

    public ResourceResolver getResourceResolver() {
		if(resourceResolver == null)
			setResourceResolver(new URLResourceResolver());
        return resourceResolver;
    }

    public void setResourceResolver(ResourceResolver resourceResolver) {
        this.resourceResolver = resourceResolver;
    }

	protected Call buildCall(Folio folio, long[] userGroupIds) throws DesignException {
		return designEngine.getCall(folio, userGroupIds);
    }

    public DataSourceFactory getDataSourceFactory() {
        if(dataSourceFactory == null) {
			setDataSourceFactory(DataLayerMgr.getDataSourceFactory());
        }
        return dataSourceFactory;
    }

	public void setDataSourceFactory(DataSourceFactory dataSourceFactory) {
    	DataSourceFactory old = this.dataSourceFactory;
    	this.dataSourceFactory = dataSourceFactory;
    	if(designEngine != null && !ConvertUtils.areEqual(old, dataSourceFactory))
			designEngine.setDataSourceFactory(dataSourceFactory);
    }

    /**
     * @return Returns the cssDirectory.
     */
    public String getCssDirectory() {
        return cssDirectory;
    }

    /**
     * @param cssDirectory
     *            The cssDirectory to set.
     */
    public void setCssDirectory(String cssDirectory) {
        if(cssDirectory == null)
            cssDirectory = "";
        else if(cssDirectory.length() > 0 && !cssDirectory.endsWith("/") && !cssDirectory.endsWith("\\"))
        	cssDirectory += "/";
        this.cssDirectory = cssDirectory;
    }

	public InvalidField[] validateFields() throws DesignException {
		return getDesignEngine().validateFields();
    }

    /** Validates that the given expressions can be used as field display and sort expressions
     * @param displayExpression
     * @param sortExpression
     * @throws DesignException
     * @throws ExecuteException
     * @throws SQLException
     * @throws BuildSQLException
     */
	public SQLType[] validateFieldExpressions(String displayExpression, String sortExpression) throws DesignException {
		return getDesignEngine().validateFieldExpressions(displayExpression, sortExpression);
    }

	public TaskListener getTaskListener() {
		return taskListener;
	}

	public void setTaskListener(TaskListener taskListener) {
		this.taskListener = taskListener;
	}

	public void finalize() {
		shutdownGarbageCollection();
	}

	static {
        TemplatesLoader.setFactoryClassAttribute(
                "org.apache.xalan.xsltc.trax.TransformerFactoryImpl", "generate-translet", true);
        TemplatesLoader.setFactoryClassAttribute(
                "org.apache.xalan.xsltc.trax.TransformerFactoryImpl", "jar-name", "xsltc-translets.jar");
        TemplatesLoader.setFactoryClassAttribute(
                "org.apache.xalan.xsltc.trax.TransformerFactoryImpl", "destination-directory", System.getProperty("java.io.tmpdir"));
        TemplatesLoader.setFactoryClassAttribute(
                "org.apache.xalan.xsltc.trax.TransformerFactoryImpl", "enable-inlining", true);
        TemplatesLoader.setFactoryClassAttribute(
                "org.apache.xalan.processor.TransformerFactoryImpl", "http://xml.apache.org/xalan/features/incremental", true);
        /*
        if("org.apache.xalan.xsltc.trax.TransformerFactoryImpl".equals(factoryClassName)) {
            templatesLoader.setAttribute("generate-translet", true);
            //templatesLoader.setAttribute("debug", true);
            templatesLoader.setAttribute("jar-name", "xsltc-translets.jar");
            templatesLoader.setAttribute("destination-directory", System.getProperty("java.io.tmpdir"));
            templatesLoader.setAttribute("enable-inlining", true);
            // "translet-name";
            // "destination-directory";
            // "package-name";
            // "jar-name";
            // "generate-translet";
            // "auto-translet"; -- we may wish to turn this on
            // "use-classpath";
            // "debug";
            // "enable-inlining"; -- we may wish to turn this on
            // "indent-number";
        } else if("org.apache.xalan.processor.TransformerFactoryImpl".equals(factoryClassName)) {
            templatesLoader.setAttribute("http://xml.apache.org/xalan/features/incremental", true);
            // NOTE: testing has shown that for the current show-report-html.xsl template incremental processing does NOT improve speed
            //http://xml.apache.org/xalan/features/optimize - default is true
            //http://xml.apache.org/xalan/properties/source-location - default is false
        }
        */
	}

	public boolean isGcEnabled() {
		return gcEnabled;
	}

	public void setGcEnabled(boolean gcEnabled) {
		this.gcEnabled = gcEnabled;
	}

	public String getDesignDataSourceName() {
		return designDataSourceName;
	}

	public void setDesignDataSourceName(String designDataSourceName) {
		if(designDataSourceName == null)
			designDataSourceName = "METADATA";
		String oldDSN = this.designDataSourceName;
		this.designDataSourceName = designDataSourceName;
		if(!ConvertUtils.areEqual(oldDSN, designDataSourceName))
			designEngine.setDesignDataSourceName(designDataSourceName);
	}

	public String getDefaultDataSourceName() {
		return defaultDataSourceName;
	}

	public void setDefaultDataSourceName(String defaultDataSourceName) {
		if(defaultDataSourceName == null)
			defaultDataSourceName = "REPORT";
		String oldDSN = this.defaultDataSourceName;
		this.defaultDataSourceName = defaultDataSourceName;
		if(!ConvertUtils.areEqual(oldDSN, defaultDataSourceName))
			designEngine.setDefaultDataSourceName(defaultDataSourceName);
	}
}
