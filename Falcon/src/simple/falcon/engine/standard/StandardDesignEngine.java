/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import simple.app.DialectResolver;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.CacheableCall;
import simple.db.CacheableCall.ExecuteKey;
import simple.db.Column;
import simple.db.Cursor;
import simple.db.DBHelper;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.db.DataSourceNotFoundException;
import simple.db.DbUtils;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.db.specific.DbSpecific;
import simple.db.specific.DefaultSpecific;
import simple.db.specific.OracleSpecific;
import simple.db.specific.PostgresSpecific;
import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Filter;
import simple.falcon.engine.FilterGroup;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Folio;
import simple.falcon.engine.GenericHierarchy;
import simple.falcon.engine.Hierarchy;
import simple.falcon.engine.Operator;
import simple.falcon.engine.Operator.Side;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Param;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Pillar.PillarType;
import simple.falcon.engine.Report;
import simple.falcon.engine.SortOrder;
import simple.falcon.run.FolioUpdateUtils;
import simple.io.LoadingException;
import simple.lang.Initializer;
import simple.lang.SystemUtils;
import simple.param.ParamEditor;
import simple.param.ParamEditorFactory;
import simple.results.BeanException;
import simple.results.DataGenre;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.sql.Aliaser;
import simple.sql.ArraySQLType;
import simple.sql.BaseMutableExpression;
import simple.sql.BuildSQLException;
import simple.sql.Expression;
import simple.sql.InitialsAliaser;
import simple.sql.Join;
import simple.sql.JoinCardinality;
import simple.sql.JoinType;
import simple.sql.SQLBuilder;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.sql.Table;
import simple.text.LiteralFormat;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.util.CompositeList;
import simple.util.LRUMap;
import simple.util.concurrent.AbstractFutureCache;
import simple.util.concurrent.Cache;
import simple.util.concurrent.ExpirableIterator;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.LockSegmentFutureCache;

/** The main workhorse of designing, saving, retrieving and executing folios.
 * @author bkrug
 *
 */
public class StandardDesignEngine implements DesignEngine, ColumnNamer {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected static final String INDENT = "    ";
    protected static final String NEWLINE = "\n";
    protected static final Pattern oracleProductPattern = Pattern.compile("Oracle.*");
	protected static final Pattern postgresProductPattern = Pattern.compile("PostgreSQL-.*");
	protected static final String defaultDateFormat="DATE:MM/dd/yyyy hh:mm:ss a";
	protected static final String defaultNumericFormat="NUMBER:0.##";
	
    protected final Cache<String,DbSpecific,SQLException> reportSpecificCache = new LockSegmentCache<String,DbSpecific,SQLException>() {
		@Override
		protected DbSpecific createValue(String key, Object... additionalInfo) throws SQLException {
			//should use metaData product info to get the right DbSpecific
			DatabaseMetaData metaData;
			if(additionalInfo != null && additionalInfo.length > 0) {
				metaData = (DatabaseMetaData)additionalInfo[0];
			} else {
				metaData = null;
			}
			DbSpecific dbSpecific;
			if(key == null) {
				dbSpecific = new DefaultSpecific();
			} else if(oracleProductPattern.matcher(key).matches()) {
				dbSpecific = new OracleSpecific();
			} else if(postgresProductPattern.matcher(key).matches()) {
				dbSpecific = new PostgresSpecific();
			} else {
				dbSpecific = new DefaultSpecific();
			}
			if(metaData != null)
				dbSpecific.initialize(metaData.getConnection());
			return dbSpecific;
		}
	};
    protected int minElapsedToCache = 2000;
	protected final ColumnNamer columnNamer = this;

	public String getDisplayColumnName(FieldOrder fieldOrder) {
		if(fieldOrder.getDisplayColumnName()!=null){
			return fieldOrder.getDisplayColumnName();
		}
		return appendUID(fieldOrder, new StringBuilder("D_")).toString();
	}

	public String getSortColumnName(FieldOrder fieldOrder) {
		if(fieldOrder.getSortColumnName()!=null){
			return fieldOrder.getSortColumnName();
		}
		return appendUID(fieldOrder, new StringBuilder("S_")).toString();
	}

    protected final DataLayer dataLayer = DataLayerMgr.getGlobalDataLayer();

    protected final Comparator<? super String> comparator = new Comparator<String>() {
        public int compare(String s1, String s2) {
            if (s1 == null) {
                if (s2 == null)
                    return 0;
				return -1;
            } else if (s2 == null) {
                return 1;
            } else {
                return caseSensitive ? s1.compareTo(s2) : s1.compareToIgnoreCase(s2);
            }
        }
    };

    public class InnerReport extends StandardReport {
    	public InnerReport() {
			super(StandardDesignEngine.this);
		}
    	protected void setUpdated(boolean updated) {
    		this.updated = updated;
    	}
    }
    public class InnerFolio extends StandardFolio {
    	public InnerFolio() {
			super(StandardDesignEngine.this);
		}
    	protected void setUpdated(boolean updated) {
    		this.updated = updated;
    	}
    }
    protected SQLBuilder sqlBuilder;

    protected boolean caseSensitive = false;

    protected volatile boolean fieldsLoaded = false;

    protected final ConcurrentMap<Long, Field> fieldCache = new ConcurrentHashMap<Long, Field>(300, 0.75f, 16);

    protected final AbstractFutureCache<Long, StandardFolio> folioCache = new LockSegmentFutureCache<Long, StandardFolio>(500, 32, 0.75f, 16) {
    	@Override
		protected StandardFolio createValue(Long folioId, Object... additionalInfo) throws DesignException {
			return createFolio(folioId);
		}
    };
    //protected final Map<Long, StandardFolio> folioCache = new LRUMap<Long, StandardFolio>(500);
    //protected final ReentrantLock folioCacheLock = new ReentrantLock();

    protected final Cache<String,Boolean,RuntimeException> metricCache = new LockSegmentCache<String,Boolean,RuntimeException>() {
		@Override
		protected Boolean createValue(String key, Object... additionalInfo) {
			// TODO: actually hit the database with a test query to
            // determine this
            /*
             * try {
             *  } catch() {
             *  }
             */
            return sqlBuilder.containsAggregate(key, (DatabaseMetaData)additionalInfo[0]);
		}

		@Override
		protected boolean keyEquals(String key1, String key2) {
			return key1.equals(key2);
		}

		@Override
		protected boolean valueEquals(Boolean value1, Boolean value2) {
			return value1.equals(value2);
		}
    };

    //protected final Map<CallKey,CacheableCall> callCache = new LRUMap<CallKey, CacheableCall>(1000, 64, 0.75f);
    protected final AbstractFutureCache<CallKey,CacheableCall> callCache = new LockSegmentFutureCache<CallKey, CacheableCall>(1000, 64, 0.75f, 16) {
    	@Override
		protected CacheableCall createValue(CallKey callKey, Object... additionalInfo) throws SQLException, BuildSQLException {
			CacheableCall call = createCall(callKey.folioId, callKey.effectiveUserGroupIds, callKey.fieldOrderList, callKey.filter, localMetaData.get(),
					additionalInfo == null || additionalInfo.length == 0 ? null : ConvertUtils.getStringSafely(additionalInfo[0]));
            call.setMaxRows(callKey.maxRows);
            return call;
		}
    };
    protected final LRUMap<ExecuteKey,Object[]> resultsCache = new LRUMap<ExecuteKey,Object[]>(100);

    protected Map<Integer,Operator> operatorCache;

    protected Map<Integer,StandardOutputType> outputTypeCache;

    protected static class ParamArrayList<E> extends ArrayList<E> {
        private static final long serialVersionUID = 1L;
    }

    protected final Map<Integer,String> arrayTypeNames = new HashMap<Integer, String>();

    protected static final ThreadLocal<DatabaseMetaData> localMetaData = new ThreadLocal<DatabaseMetaData>();

	protected DataSourceFactory dataSourceFactory;
	protected String defaultDataSourceName = "REPORT";
	protected String designDataSourceName = "METADATA";
	protected final Initializer<DesignException> initializer = new Initializer<DesignException>() {
		@Override
		protected void doInitialize() throws DesignException {
			callCache.setExpireTime(0); // never expire
			folioCache.setExpireTime(0); // never expire
			loadMetadata(true);
		}

		@Override
		protected void doReset() {
			sqlBuilder = null;
			clearOperators();
			clearOutputTypes();
			clearArrayTypeNames();
			clearFields();
			clearCalls();
			clearFolios();
		}
	};

	static {
		ParamEditorFactory.registerEditor("LOOKUP", StandardLookupParamEditor.class);
	}

    public StandardDesignEngine() {
    }

	protected Connection getDesignConnection() throws DataSourceNotFoundException, SQLException {
		return getDataSourceFactory().getDataSource(getDesignDataSourceName()).getConnection();
	}
    protected void loadMetadata(boolean retry) throws DesignException {
    	try {
			Connection conn = getDesignConnection();
            try {
                DatabaseMetaData md = conn.getMetaData();
                caseSensitive = (md.supportsMixedCaseIdentifiers());
                log.debug("Ignore Case=" + !caseSensitive + "; StoresMixed="
                        + md.storesMixedCaseIdentifiers() + "; StoresLower="
                        + md.storesLowerCaseIdentifiers());
                sqlBuilder = new SQLBuilder(comparator);
                loadJoins(sqlBuilder);
                //load operators
                loadOperators(conn);

                //load output types
                loadOutputTypes(conn);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(DataLayerException e) {
        	if(retry) {
        		try {
					ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml", dataLayer);
				} catch(ConfigException e1) {
					log.warn("Could not load design data layer file 'simple/falcon/engine/standard/design-data-layer.xml'", e1);
				} catch(IOException e1) {
					log.warn("Could not load design data layer file 'simple/falcon/engine/standard/design-data-layer.xml'", e1);
				} catch(LoadingException e1) {
					log.warn("Could not load design data layer file 'simple/falcon/engine/standard/design-data-layer.xml'", e1);
				}
				loadMetadata(false);
				return;
        	}
        	throw new DesignException("Could not initialize", e);
		} catch(ConvertException e) {
			throw new DesignException("Could not initialize", e);
		} catch(BeanException e) {
        	throw new DesignException("Could not initialize", e);
		} catch(SQLException e) {
        	throw new DesignException("Could not initialize", e);
        }
    }
    public int getMinElapsedToCache() {
        return minElapsedToCache;
    }

    public void setMinElapsedToCache(int minElapsedToCache) {
        this.minElapsedToCache = minElapsedToCache;
        log.debug("Min Elapsed Time set to " + minElapsedToCache);
    }

    public int getResultsCacheSize() {
        return resultsCache.getMaxSize();
    }

    public void setResultsCacheSize(int cacheSize) {
        resultsCache.setMaxSize(cacheSize);
        log.debug("Results cache size set to " + cacheSize);
    }
    /**
     * @param conn
     * @throws ConvertException
     * @throws DataLayerException
     * @throws SQLException
     * @throws BeanException
     * @throws DesignException
     */
    protected void loadOperators(Connection conn) throws SQLException, DataLayerException, ConvertException,
            BeanException, DesignException {
        Results results = dataLayer.executeQuery(conn, "GET_OPERATORS", null);
        StandardOperator[] operators = results.toBeanArray(StandardOperator.class);
        Map<Integer,Operator> newOpCache = new HashMap<Integer, Operator>();
        //this avoids need for synchronization
        for (int i = 0; i < operators.length; i++) {
            newOpCache.put(operators[i].getOperatorId(), operators[i]);
            operators[i].setDesignEngine(this);
            Operator.registerOperator(operators[i]);
        }
        operatorCache = newOpCache;
    }

	protected void clearOperators() {
		Map<Integer, Operator> operatorCache = this.operatorCache;
		if(operatorCache != null)
			operatorCache.clear();
	}

	protected void loadOutputTypes(Connection conn) throws SQLException, DataLayerException, BeanException {
		Results results = dataLayer.executeQuery(conn, "GET_OUTPUT_TYPES", null);
		StandardOutputType[] outputType = results.toBeanArray(StandardOutputType.class);
        Map<Integer,StandardOutputType> newOTCache = new HashMap<Integer, StandardOutputType>();
        //this avoids need for synchronization
        for (int i = 0; i < outputType.length; i++)
		    newOTCache.put(outputType[i].getOutputTypeId(), outputType[i]);
        outputTypeCache = newOTCache;
	}

	protected void clearOutputTypes() {
		Map<Integer, StandardOutputType> outputTypeCache = this.outputTypeCache;
		if(outputTypeCache != null)
			outputTypeCache.clear();
	}

    public void clearFields() {
        fieldsLoaded = false;
        fieldCache.clear();
    }

    public void clearCalls() {
        callCache.clear();
    }

    public void clearFolios() {
        folioCache.clear();
    }

    public Field[] getFields(long[] fieldIds) throws DesignException {
		initialize();
		Field[] fields = new Field[fieldIds.length];
        try {
			Connection conn = getDesignConnection();
            try {
                for (int i = 0; i < fieldIds.length; i++) {
                    fields[i] = getField(fieldIds[i], conn);
                }
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch (SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (BeanException e) {
            throw new DesignException(e);
        }
        return fields;
    }

	protected Field[] getFields(long[] fieldIds, Connection conn) throws SQLException, DataLayerException, BeanException {
		Field[] fields = new Field[fieldIds.length];
		for(int i = 0; i < fieldIds.length; i++) {
	        fields[i] = getField(fieldIds[i], conn);
	    }
		return fields;
	}

    public Field getField(long fieldId) throws DesignException {
		initialize();
		try {
			Connection conn = getDesignConnection();
            try {
                return getField(fieldId, conn);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch (SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (BeanException e) {
            throw new DesignException(e);
        }
    }

    protected void loadAllFields() throws DesignException {
        try {
			Connection conn = getDesignConnection();
            try {
                loadAllFields(conn);
            } finally {
                conn.close();
            }
        } catch(SQLException e) {
            throw new DesignException(e);
		} catch(DataSourceNotFoundException e) {
			throw new DesignException(e);
		}
    }

    protected void loadAllFields(Connection conn) throws DesignException {
        try {
            Results results = dataLayer.executeQuery(conn, "GET_ALL_FIELDS", null);
            Set<Long> fieldIds = new HashSet<Long>(fieldCache.keySet());
            while(results.next()) {
                Field f = createField(results, conn);
                fieldCache.put(f.getId(), f);
                fieldIds.remove(f.getId());
            }
            fieldCache.keySet().removeAll(fieldIds);
            fieldsLoaded = true;
            // lastly clear the calls
            clearCalls();
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch(DataLayerException e) {
            throw new DesignException(e);
        } catch(BeanException e) {
            throw new DesignException(e);
        }
    }

    protected Field createField(Results results, Connection conn) throws BeanException, SQLException {
        Field field = new Field();
        DatabaseMetaData metaData = conn.getMetaData();
        field.setDisplayExpression(sqlBuilder.createExtendedExpression(results.getFormattedValue("displayExp"), metaData));
        field.setSortExpression(sqlBuilder.createExtendedExpression(results.getFormattedValue("sortExp"), metaData));
        results.fillBean(field);
        if (isMetric(field, metaData))
            field.setAdditiveType(Field.AdditiveType.FULL);
        else
            field.setAdditiveType(Field.AdditiveType.NONE);
        return field;
    }

	protected Field getField(long fieldId, Connection conn) throws SQLException, DataLayerException, BeanException {
        Field field = fieldCache.get(fieldId);
        if(field == null) {
            field = loadField(fieldId, conn);
            fieldCache.put(field.getId(), field);
        }
        return field;
    }

	protected Field loadField(long fieldId, Connection conn) throws SQLException, DataLayerException, BeanException {
        Results results = dataLayer.executeQuery(conn, "GET_FIELD", new Object[] { fieldId });
        if (results.next()) {
        	Field field = createField(results, conn);
        	fieldCache.put(field.getId(), field);
            return field;
        }
		throw new SQLException("Field id " + fieldId + " could not be found in the database");
    }

    protected void loadJoins(SQLBuilder sqlBuilder) throws SQLException, DataLayerException, ConvertException {
		Connection conn = getDesignConnection();
		try {
			conn.setAutoCommit(true);
			Results results = dataLayer.executeQuery(conn, "GET_JOINS", null);
			while(results.next()) {
				String tabExp1 = ConvertUtils.getString(results.getValue("firstTable"), false);
				String tabExp2 = ConvertUtils.getString(results.getValue("secondTable"), false);
				Table tab1 = sqlBuilder.parseTable(tabExp1);
				if(tab1 == null && tabExp1 != null && tabExp1.trim().length() > 0) {
					log.warn("Cannot add join for table '" + tabExp1 + "' because it is an invalid table expression");
				}
				Table tab2 = sqlBuilder.parseTable(tabExp2);
				if(tab2 == null && tabExp2 != null && tabExp2.trim().length() > 0) {
					log.warn("Cannot add join for table '" + tabExp2 + "' because it is an invalid table expression");
				}
				String joinExp = ConvertUtils.getString(results.getValue("joinExpression"), true);
				JoinType joinType = results.getValue("joinType", JoinType.class);
				JoinCardinality joinCardinality = results.getValue("joinCardinality", JoinCardinality.class);
				sqlBuilder.loadJoin(tab1, tab2, joinExp, results.getRow(), joinType, joinCardinality);
			}
			results = dataLayer.executeQuery(conn, "GET_UNIQUE_EXPRESSIONS", null);
			while(results.next()) {
				String tabExp = ConvertUtils.getString(results.getValue("table"), false);
				String uniqueExp = ConvertUtils.getString(results.getValue("unique"), false);
				Table tab = sqlBuilder.parseTable(tabExp);
				if(tab == null) {
					log.warn("Cannot add join for table '" + tabExp + "' because it is an invalid table expression");
				}
				sqlBuilder.loadUniqueExpression(tab, uniqueExp);
			}
		} finally {
			conn.close();
		}
		/*
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		sqlBuilder.printJoinPaths(new PrintStream(baos));
		log.debug("Join Paths:\n" + baos);*/
    }

	protected void clearJoins() {
		sqlBuilder.clearJoins();
		sqlBuilder.clearUniqueExpressions();
	}

	protected void initialize() throws DesignException {
		getDataSourceFactory();
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new DesignException(e);
		}
	}

	protected void initializeSafely() {
		try {
			initialize();
		} catch(DesignException e) {
			log.error("Could not initialize DesignEngine", e);
		}
	}
    public CacheableCall getCall(Folio folio, long[] userGroupIds) throws DesignException {
		try {
			Connection conn = findConnection(folio);
            try {
				// Caching is not necessary at this point (Performance is too good)
				return getCall(folio, userGroupIds, conn);
            } finally {
                conn.close();
            }
        } catch(SQLException e) {
            throw new DesignException(e);
		}
    }
    public List<Argument> getArguments(Folio f) throws DesignException{
			ArrayList<Argument> arList=new ArrayList<Argument>();
			if(f.getQuery()!=null){
			Connection conn = findConnection(f);
			try {
				DbSpecific dbSpecific = getDbSpecific(conn.getMetaData());
				ResultSetMetaData rsmd = dbSpecific.getQueryMetaData(conn, f.getQuery());
					if(rsmd!=null){
						int count = rsmd.getColumnCount();
						Cursor cur=new Cursor();
						List<Column> cols = new ArrayList<Column>(count);
						int lastIndex = 0;
						for(int i=1;i<count+1;i++){
							String displayCN = rsmd.getColumnName(i);
							Column col = new Column();
					        col.setIndex(++lastIndex);
					        col.setPropertyName(displayCN);
					        cols.add(col);
						}
						cur.setColumns(cols.toArray(new Column[cols.size()]));
						arList.add(cur);
				        Param[] paramArray=f.getFilter().gatherParams();
				        for(Param param:paramArray){
							Parameter p=new Parameter(param.getName(), param.getSqlType(), param.isRequired(), true, false); 
							arList.add(p);
				        }
					}
			} catch(SQLException e) {
				throw new DesignException(e);
			} finally {
				DbUtils.closeSafely(conn);
				}
			}
			return arList;
	}
    public CacheableCall getCall(Folio folio, long[] userGroupIds, Connection conn) throws DesignException {
        // Caching is not necessary at this point (Performance is too good)
		initialize();
		if(folio.getQuery()!=null){
			StandardFolio sFolio=(StandardFolio)folio;
			CacheableCall call = new CacheableCall(resultsCache);
	        call.setSql(sFolio.getQuery());
	        List<Argument> arguments=getArguments(sFolio);
	        call.setArguments(arguments.toArray(new Argument[arguments.size()]));
	        call.setMinElapsedTime(getMinElapsedToCache());
	        //call.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE); //needed to allow ResultSetScrollableResults
	        return call;
		}
		return createCall(folio, userGroupIds, conn);
    }

    protected static final Comparator<int[]> intArrayComparator = new Comparator<int[]>(){
        public int compare(int[] o1, int[] o2) {
            int diff = 0;
            for(int i = 0; diff == 0 && i < o1.length; i++) {
                diff = o1[i] - o2[i];
            }
            return diff;
        }
    };

    protected StringBuilder appendUID(FieldOrder fieldOrder, StringBuilder appendTo) {
    	appendTo.append(fieldOrder.getField().getId());
		Aggregate agg = getColumnAggregate(fieldOrder.getAggregateType());
		if(agg != null)
			appendTo.append('_').append(agg);
		return appendTo;
	}
    protected String getUID(FieldOrder fieldOrder) {
		return appendUID(fieldOrder, new StringBuilder()).toString();
	}
    protected CacheableCall createCall(Folio folio, long[] userGroupIds, Connection conn)
            throws DesignException {
        try {
            DatabaseMetaData metaData = conn.getMetaData();
            // we could use simple.util.CollectionComparator - but we'll write our own to optimize
            SortedMap<int[], FieldOrder> sortedFieldOrders = new TreeMap<int[], FieldOrder>(intArrayComparator);
            FilterGroup fg = new StandardFilterGroup(this);
            if(folio.getFilter() != null)
            	fg.addFilter(folio.getFilter());
            Pillar[] pillars = folio.getPillars();
            // get filters all at once to reduce hits against db
        	Set<Long> fieldIds = new HashSet<Long>();
            for(Pillar p : pillars) {
            	for(FieldOrder fo : p.getFieldOrders()) {
            		fieldIds.add(fo.getField().getId());
            	}
            }
            int o = 0;
            Map<String, int[]> alreadyIncluded = new HashMap<String, int[]>();
            FieldWithEffectiveUserGroup fieldWithUserGroup=gatherFilters(fieldIds, userGroupIds, fg);
            Set<Long> includeSet = fieldWithUserGroup.getIncludeSet();
            for(Pillar p : pillars) {
            	for(FieldOrder fo : p.getFieldOrders()) {
            		if(includeSet.contains(fo.getField().getId())) {
            			// Order fieldOrders by group level desc, then by sort index, then by position
                        int l = p.getGroupingLevel();
                        int[] order = new int[] {l < 0 ? 0 : -l, fo.getSortIndex(), o++};
                        String uid = getUID(fo);
            			int[] old = alreadyIncluded.get(uid);
            			if(old == null) {
            				alreadyIncluded.put(uid, order);
                            sortedFieldOrders.put(order, fo);
            			} else if(intArrayComparator.compare(old, order) > 0) {
            				alreadyIncluded.put(uid, order);
            				sortedFieldOrders.remove(old);
            				sortedFieldOrders.put(order, fo);
            			}
            		}
            	}
            }
            if (sortedFieldOrders.size() == 0)
                throw new DesignException("You do not have access to any fields on this report");
            return getCall(folio.getFolioId(), fieldWithUserGroup.getEffectiveUserGroupIds(), sortedFieldOrders.values(), fg, folio.getMaxRows(), metaData, folio.getDirective("query-optimizer-hint"));
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (ConvertException e) {
            throw new DesignException(e);
        } catch (BeanException e) {
            throw new DesignException(e);
        } catch(BuildSQLException e) {
            throw new DesignException(e);
        }
    }

    /** Returns the call for the specified list of FieldOrders and Filter.
     *  FieldOrders <strong>MUST</strong> be in correct order of sorting.
     *  (For sql's SORT BY clause)
     * @throws BuildSQLException
     *
     */
    protected CacheableCall getCall(Long folioId, String effectiveUserGroupIds, Collection<FieldOrder> fieldOrderList, FilterGroup filter, int maxRows, DatabaseMetaData metaData, String hint) throws SQLException, BuildSQLException {
    	// Caching is not necessary at this point (Performance is too good), but it is already implemented so leave it
    	CallKey callKey = createCallKey(folioId, effectiveUserGroupIds,fieldOrderList, filter, maxRows);
        localMetaData.set(metaData);
        CacheableCall call;
		try {
			call = callCache.getOrCreate(callKey, hint);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof BuildSQLException)
				throw (BuildSQLException)e.getCause();
			if(e.getCause() instanceof SQLException)
				throw (SQLException)e.getCause();
			throw new BuildSQLException("Exception while creating call", e.getCause());
		}
        call.setMinElapsedTime(getMinElapsedToCache());
		call = (CacheableCall) call.clone();
		for(int k = 1; k < call.getArgumentCount(); k++) {
			Argument a = call.getArgument(k);
			if(a.isIn() && a instanceof FilterGroupAwareParameter)
				try {
					((FilterGroupAwareParameter) a).updateDefaultValue(filter);
				} catch(DesignException e) {
					throw new BuildSQLException(e);
				}
		}
        return call;
    }

    protected static class CallKey {
        protected Collection<FieldOrder> fieldOrderList;
        protected FilterGroup filter;
        protected int hashCode;
        protected int maxRows;
        protected Long folioId;
        protected String effectiveUserGroupIds;
        protected CallKey(Long folioId, String effectiveUserGroupIds, Collection<FieldOrder> fieldOrderList, FilterGroup filter, int maxRows) {
        	this.folioId=folioId;
        	this.effectiveUserGroupIds=effectiveUserGroupIds;
            this.fieldOrderList = fieldOrderList;
            this.filter = filter;
            this.maxRows = maxRows;
            calcHashCode();
        }

        protected void calcHashCode() {
        	if(folioId==null){
        		hashCode = calcFieldOrdersHashCode() * 31 + calcFilterHashCode(filter);
        	}else{
        		hashCode = 31 * hashCode + folioId.hashCode();
        		hashCode = 31 * hashCode + effectiveUserGroupIds.hashCode();
        	}
            /*for(FieldOrder fo : fieldOrderList) {
                hashCode += fo.hashCode();
            }*/
        }
        protected int calcFieldOrdersHashCode() {
        	int hash = 1;
        	for(FieldOrder fo : fieldOrderList) {
        		hash = 31*hash + fo.hashCode();
        	}
        	return hash;
        }
        protected int calcFilterHashCode(Filter f) {
            if(f instanceof FilterGroup) {
                FilterGroup fg = (FilterGroup)f;
                int hc = fg.getSeparator().hashCode();
                for(Filter f0 : fg.getChildFilters())
                	hc = hc ^ SystemUtils.spreadHash(calcFilterHashCode(f0));
                return hc;
            } else if(f instanceof FilterItem) {
                FilterItem fi = (FilterItem)f;
                return fi.getField().hashCode() + (10000 * fi.getOperator().getOperatorId());
            } else {
                return f.hashCode();
            }
        }
        @Override
		public int hashCode() {
            return hashCode;
        }

        @Override
		public boolean equals(Object o) {
            if(!(o instanceof CallKey))
                return false;
            CallKey ck = (CallKey) o;
            if(hashCode != ck.hashCode)
                return false;
            if(fieldOrderList.size() != ck.fieldOrderList.size())
                return false;
            if(maxRows != ck.maxRows)
                return false;
            Iterator<FieldOrder> iter0 = fieldOrderList.iterator();
            Iterator<FieldOrder> iter1 = ck.fieldOrderList.iterator();
            while(iter0.hasNext()) {
                if(!iter0.next().equals(iter1.next()))
                    return false;
            }
            if (folioId == null) {
    			if (ck.folioId != null)
    				return false;
    		} else if (!folioId.equals(ck.folioId))
    			return false;
            if (effectiveUserGroupIds == null) {
    			if (ck.effectiveUserGroupIds != null)
    				return false;
    		} else if (!effectiveUserGroupIds.equals(ck.effectiveUserGroupIds))
    			return false;
            return filter.sameExpression(ck.filter);
        }
    }

    protected CallKey createCallKey(Long folioId, String effectiveUserGroupIds,Collection<FieldOrder> fieldOrderList, FilterGroup filter, int maxRows) {
        return new CallKey(folioId, effectiveUserGroupIds,fieldOrderList, filter, maxRows);
    }

    /** Creates a call for the specified list of FieldOrders and Filter.
     *  FieldOrders <strong>MUST</strong> be in correct order of sorting.
     *  (For sql's SORT BY clause)
     * @throws BuildSQLException
     *
     */
	protected CacheableCall createCall(Long folioId, String effectiveUserGroupIds, Collection<FieldOrder> fieldOrderList, FilterGroup filter, DatabaseMetaData metaData, String hint) throws SQLException, BuildSQLException {
        SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        log.debug("Creating a call");
        String sql = null;
        String groupBy = null;
        String sortBy = null;
        Map<Table,String> tables = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
        List<Argument> queryParams = new ArrayList<Argument>();
        Cursor cur = new Cursor();
        List<Column> cols = new ArrayList<Column>(fieldOrderList.size()*2);
        DbSpecific dbSpecific = getDbSpecific(metaData);
        //Set<Long> fieldIds = new HashSet<Long>();
        //List grpList = new ArrayList();
		Aliaser aliaser = new InitialsAliaser(tables);
        log.debug("Processing " + fieldOrderList.size() + " field(s).");
        for(FieldOrder fo : fieldOrderList) {
            Field field = fo.getField();
            //if(!fieldIds.add(field.getId())) continue;
            // we can ignore the field if we have already used it
            if(sql == null) {
            	sql = "SELECT";
            	if(hint != null && (hint=hint.trim()).length() > 0) {
        			sql += " /*+ " + hint.replace("*/", "* /") + " */";
        		}
            	sql += NEWLINE;
            }
            else sql += "," + NEWLINE;
			String exp = field.getDisplayExpression().format(aliaser);
            sql += INDENT + exp; //+ " \"" + field.getId() + "\"";
            Column col = new Column();
            cols.add(col);
            col.setIndex(cols.size());
            col.setPropertyName(columnNamer.getDisplayColumnName(fo));
            col = new Column();
            cols.add(col);
            col.setIndex(cols.size());
            col.setPropertyName(columnNamer.getSortColumnName(fo));
            if(!isMetric(field, metaData)) {
                if(groupBy == null) groupBy = "GROUP BY" + NEWLINE;
                else groupBy += "," + NEWLINE;
                groupBy += INDENT + exp;
                //grpList.add(new Integer(cols[colIndex].getIndex()));
            }
            if(!field.getDisplayExpression().equals(field.getSortExpression())) {
				exp = field.getSortExpression().format(aliaser);
	            if(!isSortMetric(field, metaData)) {
	                if(groupBy == null) groupBy = "GROUP BY" + NEWLINE;
	                else groupBy += "," + NEWLINE;
	                groupBy += INDENT + exp;
	                //grpList.add(new Integer(cols[colIndex+1].getIndex()));
	            }
            }
            sql += "," + INDENT + exp; // show sort columns values
            switch(fo.getField().getSortSqlType().getTypeCode()) {
            	case Types.ARRAY: case Types.STRUCT: case Types.BLOB: case Types.CLOB: case Types.JAVA_OBJECT: case Types.OTHER: case Types.REF:
            		break; // do not sort as these are unsortable
            	default:
            		switch(fo.getSortOrder()) {
    	        	case NONE:
    	                break;
                	case DESC:
                	    if(sortBy == null) sortBy = "ORDER BY" + NEWLINE;
                        else sortBy += "," + NEWLINE;
                        sortBy += INDENT + exp + " DESC";
                        break;
                	case ASC: default:
                	    if(sortBy == null) sortBy = "ORDER BY" + NEWLINE;
                        else sortBy += "," + NEWLINE;
                        sortBy += INDENT + exp + " ASC";
                        break;
                }
            }
        }
        cur.setColumns(cols.toArray(new Column[cols.size()]));
        //cur.setGroups(grpList.toArray());
        cur.setLazyAccess(true); // this is for memory considerations
        queryParams.add(cur);

        //filters
        String filterWhere = null;
        String filterHaving = null;
        Filter whereFilter = filter.buildWhereFilter();
        Filter havingFilter = filter.buildHavingFilter();
        if(whereFilter != null) {
			BaseMutableExpression be = new BaseMutableExpression();
        	whereFilter.buildExpression(be, dbSpecific);
			filterWhere = be.format(aliaser);
            Param[] params = whereFilter.gatherParams();
            for(int k = 0; k < params.length; k++) {
				Parameter a = new FilterGroupAwareParameter(false, k);
                a.setSqlType(dbSpecific.getNormalizedType(params[k].getSqlType()));
                a.setPropertyName(params[k].getName());
                a.setDefaultValue(params[k].getValue());
                a.setRequired(false);
                queryParams.add(a);
            }
        }
        if(havingFilter != null) {
			BaseMutableExpression be = new BaseMutableExpression();
        	havingFilter.buildExpression(be, dbSpecific);
			filterHaving = be.format(aliaser);
            Param[] params = havingFilter.gatherParams();
            for(int k = 0; k < params.length; k++) {
				Parameter a = new FilterGroupAwareParameter(true, k);
                a.setSqlType(dbSpecific.getNormalizedType(params[k].getSqlType()));
                a.setPropertyName(params[k].getName());
                a.setDefaultValue(params[k].getValue());
                a.setRequired(false);
                queryParams.add(a);
            }
        }

        // NOTE: must get joins BEFORE writing from clause as extra tables may
        // be needed for joins
		Collection<String> joins = sqlBuilder.getJoins(tables, aliaser, metaData, false).keySet();
        sql += NEWLINE + "FROM" + NEWLINE;
        boolean first = true;
        for(Map.Entry<Table,String> entry : tables.entrySet()) {
            if(first) first = false;
            else sql += "," + NEWLINE;
            sql += INDENT + (entry.getKey().getSchemaName().length() > 0 ? entry.getKey().getSchemaName() + "." : "")
                + entry.getKey().getTableName() + " " + entry.getValue();
        }
        sql += NEWLINE;
        //where
        log.debug("Calculating joins for " + tables.size() + " table(s).");
        String where = null;
        for(String join : joins) {
            if(where == null) where = "WHERE ";
            else where += "  AND ";
            where += join + NEWLINE;
        }
        if(filterWhere != null) {
            if(where == null) where = "WHERE ";
            else where += "  AND ";
            where += filterWhere + NEWLINE;
        }
        if(where != null) sql += where;
        if(groupBy != null) sql += groupBy + NEWLINE;
        if(filterHaving != null) sql += "HAVING " + filterHaving + NEWLINE;
        if(sortBy != null) sql += sortBy + NEWLINE;
        log.debug("SQL=\n" + sql);
    	CacheableCall call = new CacheableCall(resultsCache);
        call.setSql(sql);
        call.setArguments(queryParams.toArray(new Argument[queryParams.size()]));
        call.setMinElapsedTime(getMinElapsedToCache());
        //call.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE); //needed to allow ResultSetScrollableResults
        return call;
    }

    protected boolean gatherFilters(long fieldId, long[] userGroupIds, FilterGroup parentFilter) throws SQLException, DataLayerException, ConvertException,
 BeanException, DesignException {
        Object[] args = new Object[] { new Long(fieldId), userGroupIds };
        boolean include = false;
        FilterGroup topFG = new StandardFilterGroup(this);
        topFG.setSeparator("OR");
		Connection conn = getDesignConnection();
        Results results;
        try {
            results = dataLayer.executeQuery(conn, "GET_USER_FILTERS", args);
            //process rows
            while (results.next()) {
                include = true;
                Long filterGroupId = ConvertUtils.convert(Long.class, results.getValue("filterGroupId"));
                if (filterGroupId != null) {
                    FilterGroup fg = getFilterGroup(filterGroupId, conn);
                    topFG.addFilter(fg);
                    //gatherValues(fg, paramValues); -- No need to do this because defaults are set on the arguments of the call
                }
            }
        } finally {
            try { conn.close(); } catch(SQLException e) {}
        }
        switch(topFG.filterCount()) {
            case 0: //do nothing
                break;
            case 1:
                parentFilter.addFilter(topFG.getChildFilters().iterator().next());
                break;
            default:
                parentFilter.addFilter(topFG);
        }

        return include;
    }
    
    protected class FieldWithEffectiveUserGroup{
    	protected Set<Long> includeSet = new HashSet<Long>();
    	protected String effectiveUserGroupIds;
		private FieldWithEffectiveUserGroup(Set<Long> includeSet,
				String effectiveUserGroupIds) {
			super();
			this.includeSet = includeSet;
			this.effectiveUserGroupIds = effectiveUserGroupIds;
		}
		public Set<Long> getIncludeSet() {
			return includeSet;
		}
		public void setIncludeSet(Set<Long> includeSet) {
			this.includeSet = includeSet;
		}
		public String getEffectiveUserGroupIds() {
			return effectiveUserGroupIds;
		}
		public void setEffectiveUserGroupIds(String effectiveUserGroupIds) {
			this.effectiveUserGroupIds = effectiveUserGroupIds;
		}
		
    }

    protected FieldWithEffectiveUserGroup gatherFilters(Set<Long> fieldIds, long[] userGroupIds, FilterGroup parentFilter) throws SQLException, DataLayerException, ConvertException,
 BeanException, DesignException {
		Object[] args = new Object[] { fieldIds, userGroupIds };
		Set<Long> includeSet = new HashSet<Long>();
		Set<Long> effectiveUserGroupIdSet = new HashSet<Long>();
		Set<Set<Long>> filterGroupSet = new HashSet<Set<Long>>();
		FilterGroup topFG = new StandardFilterGroup(this);
		topFG.setSeparator("AND");
		Connection conn = getDesignConnection();
		Results results;
		try {
		    results = dataLayer.executeQuery(conn, "GET_USER_FILTERS_FOR_FIELDS", args);
		    results.addGroup("fieldId");
		    //process rows
		    boolean any = false;
		    Set<Long> fgIdSet = null;
		    while(results.next()) {
		    	includeSet.add(ConvertUtils.convert(Long.class, results.getValue("fieldId")));
		    	effectiveUserGroupIdSet.add(ConvertUtils.convert(Long.class, results.getValue("effectiveUserGroupId")));
		    	if(!any) {
			    	Long filterGroupId = ConvertUtils.convert(Long.class, results.getValue("filterGroupId"));
			    	if(filterGroupId == null) {
			    		// a null filter group indicates carte blanc for this field
			    		if(!results.isGroupEnding("fieldId")) {
				    		any = true;
			    		}
			    		fgIdSet = null; //reset
			    	} else if(results.isGroupEnding("fieldId")) {
			    		if(fgIdSet == null) {
			    			Set<Long> set = Collections.singleton(filterGroupId);
		    				if(filterGroupSet.add(set)) {
		    					FilterGroup fg = getFilterGroup(filterGroupId, conn);
		    		            topFG.addFilter(fg);
		    				}
			    		} else {
			    			fgIdSet.add(filterGroupId);
				    		if(filterGroupSet.add(fgIdSet)) {
				    			FilterGroup midFG = new StandardFilterGroup(this);
				    			midFG.setSeparator("OR");
				    			for(long l : fgIdSet) {
				    				FilterGroup fg = getFilterGroup(l, conn);
				    				midFG.addFilter(fg);
					    		}
				    			topFG.addFilter(midFG);
				    		}
				    		fgIdSet = null; //reset
			    		}
			    	} else {
			    		fgIdSet = new HashSet<Long>();
			    		fgIdSet.add(filterGroupId);
			    	}
		    	} else if(results.isGroupEnding("fieldId")) {
		    		any = false; // reset
		    	}
		    }
		} finally {
		    try { conn.close(); } catch(SQLException e) {}
		}
		switch(topFG.filterCount()) {
		    case 0: //do nothing
		        break;
		    case 1:
		        parentFilter.addFilter(topFG.getChildFilters().iterator().next());
		        break;
		    default:
		        parentFilter.addFilter(topFG);
		}
		StringBuilder sb=new StringBuilder("");
		for(Long userGroupId:effectiveUserGroupIdSet){
			sb.append(userGroupId).append(",");
		}
		String effectiveUserGroupIdStr=null;
		if(sb.length()>0){
			effectiveUserGroupIdStr=sb.substring(0, sb.length()-1);
		}else{
			effectiveUserGroupIdStr=sb.toString();
		}
		
		return new FieldWithEffectiveUserGroup(includeSet,effectiveUserGroupIdStr);
	}

    protected boolean isMetric(Field field, DatabaseMetaData md) {
        return isMetric(field.getDisplayExpression(), md);
    }

    protected boolean isSortMetric(Field field, DatabaseMetaData md) {
        return isMetric(field.getSortExpression(), md);
    }

    public boolean isMetric(Expression expression, DatabaseMetaData md) {
    	return isMetric(expression.toString(), md);
    }
    public boolean isMetric(String expression, DatabaseMetaData md) {
        return metricCache.getOrCreate(expression, md);
    }

    public OutputType getOutputType(int outputTypeId) {
		initializeSafely();
		StandardOutputType outputType = outputTypeCache.get(outputTypeId);
        return outputType == null ? null : outputType.clone(); // protected against changes
    }

    public OutputType[] getOutputTypes() {
		initializeSafely();
		return outputTypeCache.values().toArray(new OutputType[outputTypeCache.size()]);
    }

    public Operator getOperator(int operatorId) {
		initializeSafely();
		return operatorCache.get(operatorId);
    }

    public Operator[] getOperators() {
		initializeSafely();
		return operatorCache.values().toArray(new Operator[operatorCache.size()]);
    }

    public Long saveFolio(Folio folio) throws DesignException {
        return saveFolio(folio, null);
    }
    public Long saveFolio(Folio folio, Executor executor) throws DesignException {
		initialize();
        try {
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("folio", folio);
            if(executor != null) params.put("userId", executor.getUserId());
			Connection conn = getDesignConnection();
            try {
                //Save Filters first to get top FG Id
                Long filterGroupId = null;
                FilterGroup fg = (FilterGroup)folio.getFilter();
                if (fg != null) {
                    //skip root filter unless it has child FilterItems
                    if(fg.filterCount(false,false) > 0){
                        filterGroupId = saveFilter(null, fg, conn);
                    } else if(fg.getChildFilters().size() == 1){
                        filterGroupId = saveFilter(null, fg.getChildFilters().iterator().next(), conn);
                    } else{
                        filterGroupId = saveFilter(null, fg, conn);
                    }
                    reloadFilterGroup(fg);
                }
                params.put("filterGroupId", filterGroupId);
                dataLayer.executeCall(conn, "SAVE_FOLIO", params);
                Pillar[] pillars = folio.getPillars();
                for (int i = 0; i < pillars.length; i++) {
                    params.put("index", new Integer(i + 1));
                    params.put("pillar", pillars[i]);
                    FieldOrder[] fieldOrders = pillars[i].getFieldOrders();
                    long[] fieldIds;
                    SortOrder[] sortOrders;
                    Results.Aggregate[] aggregateTypes;
                    int[] sortIndexes;
                    if(fieldOrders == null) fieldOrders = new FieldOrder[0];
                    fieldIds = new long[fieldOrders.length];
                    sortOrders = new SortOrder[fieldOrders.length];
                    aggregateTypes = new Results.Aggregate[fieldOrders.length];
                    sortIndexes = new int[fieldOrders.length];
                    for(int k = 0; k < fieldOrders.length; k++) {
                        fieldIds[k] = fieldOrders[k].getField().getId();
                        sortOrders[k] = fieldOrders[k].getSortOrder();
                        aggregateTypes[k] = fieldOrders[k].getAggregateType();
                        sortIndexes[k] = fieldOrders[k].getSortIndex();
                    }
                    params.put("fieldIds", fieldIds);
                    params.put("sortOrders", sortOrders);
                    params.put("aggregateTypes", aggregateTypes);
                    params.put("sortIndexes", sortIndexes);

                    if(pillars[i].isAggregated()){
                    	params.put("aggGroupLevel", -(pillars[i].getGroupingLevel() + 1));
                	} else {
                    	params.put("aggGroupLevel", pillars[i].getGroupingLevel());
                    }
                    dataLayer.executeCall(conn, "SAVE_FOLIO_PILLAR", params);
                }
                // save directives
                params.clear();
                params.put("folioId", folio.getFolioId());
                for(String name : folio.getDirectiveNames()) {
                    params.put("directiveName", name);
                    params.put("directiveValue", folio.getDirective(name));
                    dataLayer.executeCall(conn, "SAVE_FOLIO_DIRECTIVE", params);
                }
                conn.commit();
                if(folio instanceof InnerFolio)
                	((InnerFolio)folio).setUpdated(false);
                folioCache.expire(folio.getFolioId());
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
            return folio.getFolioId();
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (ConvertException e) {
            throw new DesignException(e);
        }
    }

    //WARN: use this method with care, it is designed only to be called from saveFolio()
    protected Long saveFilter(Long parentGroupId, Filter filter, Connection conn) throws SQLException, DataLayerException, ConvertException {
    	Map<String,Object> params = new HashMap<String,Object>();
        params.put("parentGroupId", parentGroupId);
        if(filter instanceof FilterGroup) {
            StandardFilterGroup fg = (StandardFilterGroup)filter;
            Set<Long> childFilterIds = new HashSet<Long>();
            Set<Long> excludeIds = new HashSet<Long>();
            if(fg.filterCount(true,false) > 0) {
	            params.put("filterGroup", fg);
                dataLayer.executeCall(conn, "SAVE_FILTER_GROUP", params);
                for(Filter f : fg.getChildFilters()) {
	                Long childId = saveFilter(fg.getFilterGroupId(), f, conn);
                    if(f instanceof FilterGroup) excludeIds.add(childId);
                    else childFilterIds.add(childId);
	            }
                //remove old children
                params.put("excludeIds", excludeIds);
                params.put("currentChildFilterIds", childFilterIds);
                dataLayer.executeCall(conn, "REMOVE_CHILD_FILTER_GROUPS", params);
                dataLayer.executeCall(conn, "REMOVE_NON_CURRENT_FILTERS", params);
                return fg.getFilterGroupId();
            }
        } else if(filter instanceof FilterItem) {
            StandardFilterItem fi = (StandardFilterItem)filter;
            params.put("filter", fi);
			Param[] filterParams = fi.getOperatorParams();
            params.put("paramCount", filterParams.length);
            dataLayer.executeCall(conn, "SAVE_FILTER", params);
			for(int k = 0; k < filterParams.length; k++) {
                params.put("index", k + 1);
                params.put("param", filterParams[k]);
                dataLayer.executeCall(conn, "SAVE_FILTER_PARAM", params);
            }
            return fi.getFilterId();
        }
        return null;
    }

    public StandardFolio getFolio(long folioId) throws DesignException {
        return getFolio(folioId, null);
    }

    public StandardFolio getFolio(long folioId, Translator translator) throws DesignException {
		initialize();
		StandardFolio folio;
		try {
			folio = folioCache.getOrCreate(folioId);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof DesignException)
				throw (DesignException) e.getCause();
			throw new DesignException("While creating folio", e.getCause());
		}
		if(folio.getFolioId() != folioId)
			throw new DesignException("Cached Folio has wrong folio id! It should have " + folioId + " but instead has " + folio.getFolioId());
		log.debug("Copying folio " + folioId);
        return folio.copy(translator);
    }

    protected InnerFolio createFolio(long folioId) throws DesignException {
    	try {
    		log.debug("Creating folio " + folioId);
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("folioId", folioId);
			Connection conn = getDesignConnection();
            try {
            	InnerFolio folio = newFolio();
                Results results;
                // load directives
                results = dataLayer.executeQuery(conn, "GET_FOLIO_DIRECTIVES", params);
                boolean dynamicLabels = false;
                while (results.next()) {
                    String name = ConvertUtils.getString(results.getValue("directiveName"), true);
                    String value = ConvertUtils.getString(results.getValue("directiveValue"), false);
                    folio.setDirective(name, value);
                    if(name.equalsIgnoreCase("DYNAMIC-LABELS")) {
                    	dynamicLabels = ConvertUtils.getBoolean(value, false);
                    }
                }

                //load folio info and filter
                results = dataLayer.executeQuery(conn, "GET_FOLIO", params);
                if (results.next()) {
                    folio.setFolioId(ConvertUtils.getLong(results.getValue("folioId")));
                    folio.setName(ConvertUtils.getString(results.getValue("name"), false));
                    String title = ConvertUtils.getString(results.getValue("title"), false);
                    String subtitle = ConvertUtils.getString(results.getValue("subtitle"), false);
                    if(title != null)
                        folio.setTitle(ConvertUtils.getFormat(title));
                    if(subtitle != null)
                        folio.setSubtitle(ConvertUtils.getFormat(subtitle));
                    folio.setChartType(ConvertUtils.convert(String.class, results.getValue("chartType")));
                    folio.setMaxRowsPerSection(ConvertUtils.convert(Integer.class, results.getValue("maxRowsPerSection")));
                    folio.setMaxRows(ConvertUtils.convert(Integer.class, results.getValue("maxRows")));

                    int outputType = ConvertUtils.getInt(results.getValue("outputTypeId"), 22);
                	folio.setOutputType(getOutputType(outputType));
                    //load filter groups
                    Long filterGroupId = ConvertUtils.convert(Long.class, results.getValue("filterGroupId"));
                    if(filterGroupId != null) folio.setFilter(getFilterGroup(filterGroupId, null, conn));
                    else folio.setFilter(new StandardFilterGroup());
                } else
                    throw new SQLException("Invalid folio id '" + folioId + "'");

                //load fields
                results = dataLayer.executeQuery(conn, "GET_FOLIO_PILLARS", params);
                while (results.next()) {
                	Pillar pillar = folio.newPillar();
                	results.fillBean(pillar);
                	String label = ConvertUtils.getString(results.getValue("label"), false);
                	if(!dynamicLabels && label != null && !label.regionMatches(true, 0, "LITERAL:", 0, "LITERAL:".length()))
                    	label = "LITERAL:" + label;
                	if(label != null)
                		pillar.setLabelFormat(ConvertUtils.getFormat(label));
                    /*
                    Pillar.PillarType pillarType = ConvertUtils.convert(Pillar.PillarType.class, results.getValue("pillarType"));
                    int groupingLevel = ConvertUtils.getInt(results.getValue("groupingLevel"), Folio.GROUPING_LEVEL_ZERO);
                    String label = ConvertUtils.getString(results.getValue("label"), false);
                    String description = ConvertUtils.getString(results.getValue("description"), false);
                    String styleFormat = ConvertUtils.getString(results.getValue("styleFormat"), false);
                    String displayFormat = ConvertUtils.getString(results.getValue("displayFormat"), false);
                    String actionFormat = ConvertUtils.getString(results.getValue("actionFormat"), false);
                    String helpFormat = ConvertUtils.getString(results.getValue("helpFormat"), false);
                    String sortFormat = ConvertUtils.getString(results.getValue("sortFormat"), false);
                    DataGenre sortType = ConvertUtils.convert(DataGenre.class, results.getValue("sortType"));
                    */
                    SortOrder[] sortOrders = ConvertUtils.convert(SortOrder[].class, results.getValue("sortOrders"));
                    Results.Aggregate[] aggregateTypes = ConvertUtils.convert(Results.Aggregate[].class, results.getValue("aggregateTypes"));
                    long[] fieldIds = ConvertUtils.convert(long[].class, results.getValue("fieldIds"));
                    int[] sortIndexes = ConvertUtils.convert(int[].class, results.getValue("sortIndexes"));
                    if(fieldIds != null)
                    	for(int i = 0; i < fieldIds.length; i++) {
                    		pillar.addFieldOrder(
                    				getField(fieldIds[i], conn),
                    				(sortOrders == null || sortOrders.length <= i ? null : sortOrders[i]),
                    				(aggregateTypes == null || aggregateTypes.length <= i ? null : aggregateTypes[i]),
                    				(sortIndexes == null || sortIndexes.length <= i ? null : sortIndexes[i]));
                    	}
                    /*folio.addPillar(pillarType, groupingLevel, label, description, styleFormat, displayFormat, actionFormat,
                            helpFormat, sortFormat, sortType, sortOrders, getFields(fieldIds, conn), aggregateTypes, sortIndexes);
                    */
                }
                folio.setUpdated(false);
                return folio;
            } catch(DesignException e) {
                throw new DesignException(e);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (ConvertException e) {
            throw new DesignException(e);
        } catch (BeanException e) {
            throw new DesignException(e);
        }
    }

	protected FilterGroup getFilterGroup(long filterGroupId, Connection conn) throws SQLException, DataLayerException, ConvertException, BeanException, DesignException {
        return getFilterGroup(filterGroupId, null, conn);
    }

	protected FilterGroup getFilterGroup(long filterGroupId, Translator translator, Connection conn) throws SQLException, DataLayerException, ConvertException, BeanException, DesignException {
        // load filter groups
        Map<Object,FilterGroup> filterGroups = new HashMap<Object,FilterGroup>();
        Results results = dataLayer.executeQuery(conn, "GET_FILTER_GROUPS", new Object[] {filterGroupId});
        FilterGroup topFG = null;
        while(results.next()) {
            StandardFilterGroup fg = new StandardFilterGroup();
            results.fillBean(fg);
            Object fgId = results.getValue("filterGroupId");
            filterGroups.put(fgId, fg);
            FilterGroup parentFG = filterGroups.get(results.getValue("parentGroupId"));
            if(parentFG == null) {
                assert (topFG == null);
                topFG = fg;
                log.debug("Made filter group " + fg + " the top");
            } else {
                parentFG.addFilter(fg);
                log.debug("Added filter group " + fg + " to parent " + parentFG);
            }
            // load filters
            Results filterResults = dataLayer.executeQuery(conn, "GET_FILTERS", new Object[] {fgId});
            while(filterResults.next()) {
                long fieldId = ConvertUtils.getLong(filterResults.getValue("fieldId"));
                StandardFilterItem filter = new StandardFilterItem(this);
                filterResults.fillBean(filter);
                filter.setField(getField(fieldId, conn));
                fg.addFilter(filter);
                Results paramResults = dataLayer.executeQuery(conn, "GET_FILTER_PARAMS", new Object[] {filter.getFilterId()});
                List<Param> filterParams = new ArrayList<Param>();
                while(paramResults.next()) {
					StandardParam param = new StandardParam(this, false);
                    filterParams.add(param);
                    paramResults.fillBean(param);
                    // let's try to convert the value into the proper type -- XXX: For the designer's sake we need to do this at run time
                    /*Class<? extends Object> targetClass = SQLTypeUtils.getJavaType(param.getSqlType());
                    try {
                        param.setValue(ConvertUtils.convert(targetClass, param.getValue()));
                    } catch(ConvertException e) {
                        log.warn("Could not convert '" + param.getValue() + "' to a " + targetClass.getName(), e);
                    }
                    */
                    if(translator != null) {
                        param.setLabel(translator.translate(param.getLabel()));
                        param.setPrompt(translator.translate(param.getPrompt()));
                    }
                    String editor = paramResults.getFormattedValue("paramEditor");
                    if(editor == null || editor.trim().length() == 0) {
                        editor = getDefaultEditorString(param.getSqlType());
                    }
                    param.setEditor(createEditor(editor, filter));
                }
                filter.setOperatorParams(filterParams.toArray(new Param[filterParams.size()]));
            }
        }
        return topFG;
    }
    /*
    public Param createParam(String name, String label, String prompt, SQLType sqlType, String editor) {

    }*/
    public ParamEditor createEditor(String editorString, FilterItem filterItem) throws DesignException {
    	ParamEditor editor;
		try {
			editor = ParamEditorFactory.createEditor(editorString);
		} catch(ClassCastException e) {
			throw new DesignException(e);
		} catch(IllegalArgumentException e) {
			throw new DesignException(e);
		} catch(ClassNotFoundException e) {
			throw new DesignException(e);
		} catch(InstantiationException e) {
			throw new DesignException(e);
		} catch(IllegalAccessException e) {
			throw new DesignException(e);
		} catch(InvocationTargetException e) {
			throw new DesignException(e);
		}
    	if(editor instanceof StandardLookupParamEditor) {
    		StandardLookupParamEditor slpe = (StandardLookupParamEditor)editor;
    		slpe.setEngine(this);
    		slpe.setFilter(filterItem);
    	}
    	return editor;
    }
    public String getDefaultEditorString(SQLType sqlType) {
        switch(sqlType.getTypeCode()) {
            case Types.DATE: case Types.TIME: case Types.TIMESTAMP:
                return "DATE";
            case Types.BIGINT: case Types.INTEGER: case Types.NUMERIC: case Types.SMALLINT: case Types.TINYINT:
                return "NUMBER";
            case Types.DECIMAL: case Types.DOUBLE: case Types.FLOAT: case Types.REAL:
                return "NUMBER:(20)";
            case Types.BIT: case Types.BOOLEAN:
                return "SELECT:true=yes;false=no;";
            case Types.ARRAY:
            	SQLType compType;
            	if(sqlType instanceof ArraySQLType && (compType=((ArraySQLType)sqlType).getComponentType()) != null) {
            		switch(compType.getTypeCode()) {
                        case Types.BIGINT: case Types.INTEGER: case Types.NUMERIC: case Types.SMALLINT: case Types.TINYINT:
                            return "TEXT:;^(\\d+[,])*\\d+$";
                        case Types.DECIMAL: case Types.DOUBLE: case Types.FLOAT: case Types.REAL:
                            return "TEXT:;^(\\d+([.]\\d+)?[,])*\\d+([.]\\d+)?$";
                        case Types.BIT: case Types.BOOLEAN:
                            return "TEXT:;^((yes|no|true|false|0|1)[,])*(yes|no|true|false|0|1)+$";
                    }
            	}
            default:
                return "TEXT";
        }
    }

    protected GenericHierarchy<Field> getCategory(Map<Object, GenericHierarchy<Field>> categoryCache, Object categoryId, Connection conn) throws SQLException,
                DataLayerException, ConvertException, BeanException {
        GenericHierarchy<Field> category = categoryCache.get(categoryId);
        if (category == null) {// avoid synchronizaton if possible
            if (categoryId == null) {
                category = new GenericHierarchy<Field>();
                category.setDescription("Top level category for all fields");
                category.setName("All");
                categoryCache.put(null, category);
            } else {
                log.debug("Creating category for id = " + categoryId);
                Results results = dataLayer.executeQuery(conn, "GET_CATEGORY", new Object[] { categoryId });
                if (results.next()) {
                    category = new GenericHierarchy<Field>();
                    results.fillBean(category);
                    //add to parent
                    Object parentId = results.getValue("parentCategoryId");
                    log.debug("Parent category id = " + parentId);
                    getCategory(categoryCache, parentId, conn).getBranches().add(category);
                    categoryCache.put(categoryId, category);
                } else {
                    throw new SQLException("Category id " + categoryId
                            + " could not be found in the database");
                }
            }
        }
        return category;
    }

    public Hierarchy<Field> getFieldHierarchy(long[] userGroupIds) throws DesignException {
        return getFieldHierarchy(userGroupIds, null);
    }

    public Hierarchy<Field> getFieldHierarchy(long[] userGroupIds, long[] excludeFieldIds) throws DesignException {
		initialize();
		try {
            Map<Object, GenericHierarchy<Field>> categoryCache = new HashMap<Object, GenericHierarchy<Field>>();
			Connection conn = getDesignConnection();
            try {
            	// Get all categories
            	Results results = dataLayer.executeQuery(conn, "GET_FIELD_CATEGORIES", null);
            	while (results.next()){
                    Object categoryId = results.getValue("categoryId");
                    getCategory(categoryCache, categoryId, conn);
                    log.debug("Added category " + categoryId + " to the hierarchy");
            	}

            	// Add fields
                results = dataLayer.executeQuery(conn, "GET_FIELDS_FOR_USER_GROUPS", new Object[] {userGroupIds, excludeFieldIds});
                while (results.next()) {
                    long fieldId = ConvertUtils.convert(Long.class, results.getValue("fieldId"));
					Field field;
					try {
						field = getField(fieldId, conn);
					} catch(SQLException e) {
						log.error("Could not load field " + fieldId, e);
						continue;
					}
                    Object categoryId = results.getValue("categoryId");
                    List<Field> leaves = getCategory(categoryCache, categoryId, conn).getLeafs();
                    leaves.add(field);
                    log.debug("CategoryId = " + categoryId + " now has " + leaves.size() + " fields");
                }
                return getCategory(categoryCache, null, conn);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (ConvertException e) {
            throw new DesignException(e);
        } catch (BeanException e) {
            throw new DesignException(e);
        }
    }

    public InnerFolio newFolio() {
        return new InnerFolio();
    }

    public ColumnNamer getColumnNamer() {
        return columnNamer;
    }

	public Map<Integer,String> getSqlTypes() {
		return SQLTypeUtils.getTypeCodeNames();
	}

    public void clearArrayTypeNames() {
        arrayTypeNames.clear();
    }

	protected Collection<Field> getFields() throws DesignException {
        if(!fieldsLoaded) loadAllFields();
        return fieldCache.values();
    }

	protected Connection findConnection(String displayExpression, String sortExpression) throws BuildSQLException, DataSourceNotFoundException, SQLException {
		return findConnection(sqlBuilder.createExpression(displayExpression), sqlBuilder.createExpression(sortExpression));
	}
	protected Connection findConnection(Expression displayExpression, Expression sortExpression) throws BuildSQLException, DataSourceNotFoundException, SQLException {
		String dataSourceName = findDataSourceName(displayExpression, sortExpression);
		return getDataSourceFactory().getDataSource(dataSourceName).getConnection();
	}
	protected String findDataSourceName(Expression displayExpression, Expression sortExpression) throws BuildSQLException {
		String dataSourceName = null;
		for(simple.sql.Column column : displayExpression.getColumns()) {
			if(StringUtils.isBlank(dataSourceName))
				dataSourceName = column.getDataSourceName();
			else if(StringUtils.isBlank(column.getDataSourceName()) && !dataSourceName.equalsIgnoreCase(column.getDataSourceName()))
				throw new BuildSQLException("Display expression contains more than one data source");
		}
		for(simple.sql.Column column : sortExpression.getColumns()) {
			if(StringUtils.isBlank(dataSourceName))
				dataSourceName = column.getDataSourceName();
			else if(StringUtils.isBlank(column.getDataSourceName()) && !dataSourceName.equalsIgnoreCase(column.getDataSourceName()))
				throw new BuildSQLException("Sort expression contains more than one data source");
		}
		if(StringUtils.isBlank(dataSourceName))
			dataSourceName = getDefaultDataSourceName();
		return dataSourceName;
	}

	public Connection findConnection(Folio folio) throws DesignException {
		String dataSourceName;
		try {
			dataSourceName = findDataSourceName(folio);
		} catch(BuildSQLException e) {
			throw new DesignException(e);
		}
		try {
			return getDataSourceFactory().getDataSource(dataSourceName).getConnection();
		} catch(DataSourceNotFoundException e) {
			throw new DesignException(e);
		} catch(SQLException e) {
			throw new DesignException(e);
		}
	}

	public String findDataSourceName(Folio folio) throws BuildSQLException {
		try {
			initialize();
		} catch(DesignException e) {
			throw new BuildSQLException(e);
		}
		String dataSourceName = folio.getDataSourceName();
		if(StringUtils.isBlank(dataSourceName)) {
			if(folio.getQuery() == null) {
				for(FieldOrder fo : folio.getFieldOrders()) {
					for(simple.sql.Column column : fo.getField().getDisplayExpression().getColumns()) {
						if(StringUtils.isBlank(dataSourceName))
							dataSourceName = column.getDataSourceName();
						else if(StringUtils.isBlank(column.getDataSourceName()) && !dataSourceName.equalsIgnoreCase(column.getDataSourceName()))
							throw new BuildSQLException("Display expression contains more than one data source");
					}
					for(simple.sql.Column column : fo.getField().getSortExpression().getColumns()) {
						if(StringUtils.isBlank(dataSourceName))
							dataSourceName = column.getDataSourceName();
						else if(StringUtils.isBlank(column.getDataSourceName()) && !dataSourceName.equalsIgnoreCase(column.getDataSourceName()))
							throw new BuildSQLException("Sort expression contains more than one data source");
					}
				}
			}
			if(StringUtils.isBlank(dataSourceName))
				dataSourceName = getDefaultDataSourceName();
		}
		return dataSourceName;
	}

	public SQLType[] validateFieldExpressions(String displayExpression, String sortExpression) throws DesignException {
		try {
			return validateFieldExpressions(sqlBuilder.createExpression(displayExpression), sqlBuilder.createExpression(sortExpression));
		} catch(DataSourceNotFoundException e) {
			throw new DesignException(e);
		} catch(SQLException e) {
			throw new DesignException(e);
		} catch(BuildSQLException e) {
			throw new DesignException(e);
		}
    }

	public SQLType[] validateFieldExpressions(Expression displayExpression, Expression sortExpression) throws SQLException, BuildSQLException, DataSourceNotFoundException, DesignException {
		initialize();
		log.debug("Validating '" + displayExpression + "' and '" + sortExpression + "'");
		Connection conn = findConnection(displayExpression, sortExpression);
		try {
			String sql = buildQuery(displayExpression, sortExpression, conn.getMetaData());
			CompositeList<Parameter> params = new CompositeList<Parameter>();
			params.merge(displayExpression.getParameters());
			params.merge(sortExpression.getParameters());
			return describeQuery(conn, sql, params);
		} catch(SQLException e) {
			throw e;
		} finally {
			try {
				conn.close();
			} catch(SQLException e) {
			}
		}

    }

	public void retrieveDistinctValues(String displayExpression, String sortExpression, Side sortSide, int maxRows, Map<Object, Object> valuesHolder) throws SQLException, BuildSQLException, DataSourceNotFoundException, DesignException {
		initialize();
		SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        log.debug("Finding distinct values of '" + displayExpression + "' and '" + sortExpression + "'");
        Map<Table,String> tables = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
		Aliaser aliaser = new InitialsAliaser(tables);
        String sql;
		Connection conn = findConnection(displayExpression, sortExpression);
		try {
            sql = "SELECT " + sqlBuilder.formatExpression(displayExpression, tables, tables, conn.getMetaData());
            sql += ", " + sqlBuilder.formatExpression(sortExpression, tables, tables, conn.getMetaData());
            sql += NEWLINE + "FROM " + NEWLINE;
			Collection<String> joins = sqlBuilder.getJoins(tables, aliaser, conn.getMetaData(), false).keySet();
            boolean first = true;
            for(Map.Entry<Table,String> entry : tables.entrySet()) {
                if(first) first = false;
                else sql += "," + NEWLINE;
                sql += INDENT + (entry.getKey().getSchemaName().length() > 0 ? entry.getKey().getSchemaName() + "." : "")
                    + entry.getKey().getTableName() + " " + entry.getValue();
            }
            sql += NEWLINE;
            //where
            log.debug("Calculating joins for " + tables.size() + " table(s).");
            first = true;
            for(String join : joins) {
                if(first) {
                    first = false;
                    sql += "WHERE ";
                } else sql += "  AND ";
                sql += join + NEWLINE;
            }
            switch(sortSide) {
                case DISPLAY:
                    sql += "ORDER BY 1, 2" + NEWLINE;
                    break;
                case SORT: case BOTH:
                    sql += "ORDER BY 2, 1" + NEWLINE;
                    break;
            }
            Statement statement = conn.createStatement();
            try {
                statement.setMaxRows(maxRows);
                log.debug("Running SQL: " + sql);
                ResultSet rs = statement.executeQuery(sql);
                try {
                    while(rs.next()) {
                        valuesHolder.put(rs.getObject(2), rs.getObject(1));
                    }
                } finally {
                    rs.close();
                }
            } finally {
                statement.close();
            }
        } finally {
            conn.close();
        }
    }

    //TODO: need to change this method to filter by user groups
    //TODO: need to cache results
	public void retrieveDistinctValues(Expression displayExpression, Expression sortExpression, Side sortSide, int maxRows, Map<Object, Object> valuesHolder) throws SQLException, BuildSQLException, DataSourceNotFoundException, DesignException {
		initialize();
		SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        log.debug("Finding distinct values of '" + displayExpression + "' and '" + sortExpression + "'");
        Map<Table,String> tables = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
        StringBuilder sql = new StringBuilder();
		Aliaser aliaser = new InitialsAliaser(tables);
		Connection conn = findConnection(displayExpression, sortExpression);
		try {
            sql.append("SELECT ");
			displayExpression.format(aliaser, sql);
            sql.append(", ");
			sortExpression.format(aliaser, sql);
            sql.append(NEWLINE).append("FROM ");
			Collection<Join> joins = sqlBuilder.getJoins(tables, aliaser, conn.getMetaData(), false).values();
            for(Map.Entry<Table,String> entry : tables.entrySet()) {
                sql.append(NEWLINE).append(INDENT);
                if(entry.getKey().getSchemaName().length() > 0)
                	sql.append(entry.getKey().getSchemaName()).append('.');
                sql.append(entry.getKey().getTableName()).append(' ').append(entry.getValue()).append(',');
            }
            sql.setLength(sql.length()-1);
            //where
            log.debug("Calculating joins for " + tables.size() + " table(s).");
            if(!joins.isEmpty()) {
            	sql.append(NEWLINE).append("WHERE ");
            	for(Join join : joins) {
					join.getExpressionObject().format(aliaser, sql);
                    sql.append(NEWLINE).append("  AND ");
                }
            	sql.setLength(sql.length()-6);
            } else {
            	sql.append(NEWLINE);
            }

            switch(sortSide) {
                case DISPLAY:
                    sql.append("ORDER BY 1, 2").append(NEWLINE);
                    break;
                case SORT: case BOTH:
                    sql.append("ORDER BY 2, 1").append(NEWLINE);
                    break;
            }
            Statement statement = conn.createStatement();
            try {
                statement.setMaxRows(maxRows);
                String tmp = sql.toString();
                log.debug("Running SQL: " + tmp);
                ResultSet rs = statement.executeQuery(tmp);
                try {
                    while(rs.next()) {
                        valuesHolder.put(rs.getObject(2), rs.getObject(1));
                    }
                } finally {
                    rs.close();
                }
            } finally {
                statement.close();
            }
        } finally {
            conn.close();
        }
    }

    protected String buildQuery(Expression displayExpression, Expression sortExpression, DatabaseMetaData metaData) throws BuildSQLException {
    	SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        Map<Table,String> tables = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
		Aliaser aliaser = new InitialsAliaser(tables);
		StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
		displayExpression.format(aliaser, sql);
        sql.append(" D, ");
		sortExpression.format(aliaser, sql);
		sql.append(" S");
		if(tables.isEmpty()) {
			try {
				if(metaData.getDatabaseProductName().equalsIgnoreCase("oracle"))
					sql.append(NEWLINE).append("FROM DUAL WHERE 0 = 1");
			} catch(SQLException e) {
				throw new BuildSQLException(e);
			}
			return sql.toString();
		}
		sql.append(NEWLINE).append("FROM ");
		Collection<Join> joins = sqlBuilder.getJoins(tables, aliaser, metaData, false).values();
        for(Map.Entry<Table,String> entry : tables.entrySet()) {
            sql.append(NEWLINE).append(INDENT);
            if(entry.getKey().getSchemaName().length() > 0)
            	sql.append(entry.getKey().getSchemaName()).append('.');
			sql.append(entry.getKey().getTableName()).append(' ').append(aliaser.getAlias(entry.getKey())).append(',');
        }
        sql.setLength(sql.length()-1);
        //where
        log.debug("Calculating joins for " + tables.size() + " table(s).");
    	sql.append(NEWLINE).append("WHERE ");
        if(!joins.isEmpty()) {
        	for(Join join : joins) {
				join.getExpressionObject().format(aliaser, sql);
                sql.append(NEWLINE).append("  AND ");
            }
        }
        sql.append("0 = 1");
        return sql.toString();
    }

	protected static final SQLType DEFAULT_SQL_TYPE = new SQLType(Types.VARCHAR);
	protected SQLType[] describeQuery(Connection conn, String sql, List<Parameter> parameters) throws SQLException {
		PreparedStatement statement = conn.prepareStatement(sql);
        try {
        	statement.setMaxRows(1);
			if(parameters != null && !parameters.isEmpty()) {
				DBHelper dbHelper = DataLayerMgr.getDBHelper(conn);
				int index = 1;
				for(Parameter parameter : parameters) {
					try {
						dbHelper.setInParameter(statement, index, parameter.getPropertyName(), 0, (parameter.getSqlType() != null ? parameter.getSqlType() : DEFAULT_SQL_TYPE));
					} catch(ParameterException e) {
						throw new SQLException(e);
					}
					index++;
				}
			}
            log.debug("Running Validation SQL: " + sql);
			ResultSet rs = statement.executeQuery();
            try {
                ResultSetMetaData rsmd = rs.getMetaData();
                return new SQLType[] {
                        makeSQLType(rsmd.getColumnType(1), rsmd.getColumnTypeName(1)),
                        makeSQLType(rsmd.getColumnType(2), rsmd.getColumnTypeName(2))
                };
            } finally {
                try { rs.close(); } catch (SQLException e) {}
            }
        } finally {
            try { statement.close(); } catch (SQLException e) {}
        }
    }

    protected SQLType makeSQLType(int sqlType, String typeName) {
    	return new SQLType(sqlType, typeName);/*
        switch(sqlType) {
            case Types.ARRAY: case Types.JAVA_OBJECT: case Types.OTHER: case Types.REF: case Types.STRUCT:
                return new SQLType(sqlType, typeName);
            default:
                return new SQLType(sqlType);
        }*/
    }

	public InvalidField[] validateFields() throws DesignException {
        Collection<InvalidField> invalids = new ArrayList<InvalidField>();
        log.debug("Validating all fields");
        for(Field field : getFields()) {
            try {
				validateFieldExpressions(field.getDisplayExpression(), field.getSortExpression());
            } catch (SQLException e) {
                invalids.add(new InvalidField(field, e));
            } catch(BuildSQLException e) {
                invalids.add(new InvalidField(field, e));
			} catch(DataSourceNotFoundException e) {
				invalids.add(new InvalidField(field, e));
			}
        }
        log.debug("Found " + invalids.size() + " invalid fields");

        return invalids.toArray(new InvalidField[invalids.size()]);
    }

    public Long saveReport(Report report) throws DesignException {
        return saveReport(report, null);
    }

    public Long saveReport(Report report, Executor executor) throws DesignException {
        throw new DesignException("Method not implemented"); //TODO: implement this
    }

    public Report getReport(long reportId) throws DesignException {
        return getReport(reportId, null);
    }

    public Report getReport(long reportId, Translator translator) throws DesignException {
		initialize();
        try {
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("reportId", reportId);
			Connection conn = getDesignConnection();
            try {
            	InnerReport report = newReport(translator);
                Results results = dataLayer.executeQuery(conn, "GET_REPORT", params);
                if (results.next()) {
                    report.setReportId(ConvertUtils.getLong(results.getValue("reportId")));
                    report.setName(ConvertUtils.getString(results.getValue("name"), false));
                    String title = ConvertUtils.getString(results.getValue("title"), false);
                    String subtitle = ConvertUtils.getString(results.getValue("subtitle"), false);
                    if(translator != null) {
                        title = translator.translate(title);
                        subtitle = translator.translate(subtitle);
                    }
                    if(title != null)
                        report.setTitle(ConvertUtils.getFormat(title));
                    if(subtitle != null)
                        report.setSubtitle(ConvertUtils.getFormat(subtitle));

                    int outputType = ConvertUtils.getInt(results.getValue("outputTypeId"), 22);
                    report.setOutputType(getOutputType(outputType));
                } else {
                	throw new SQLException("Invalid report id '" + reportId + "'");
                }

                // load folios
                results = dataLayer.executeQuery(conn, "GET_REPORT_FOLIOS", params);
                Collection<Folio> folioSet = new ArrayList<Folio>();
                while (results.next()) {
                    long folioId = ConvertUtils.getLong(results.getValue("folioId"));
                    folioSet.add(getFolio(folioId, translator));
                }
                Folio[] folioArray = new Folio[folioSet.size()];
                report.setFolios(folioSet.toArray(folioArray));

                // load directives
                results = dataLayer.executeQuery(conn, "GET_REPORT_DIRECTIVES", params);
                while (results.next()) {
                    String name = ConvertUtils.getString(results.getValue("directiveName"), true);
                    String value = ConvertUtils.getString(results.getValue("directiveValue"), false);
                    report.setDirective(name, value);
                }
                report.setUpdated(false);
                return report;
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch (DataLayerException e) {
            throw new DesignException(e);
        } catch (ConvertException e) {
            throw new DesignException(e);
        }
    }

    public void reloadFolio(long folioId) throws DesignException {
        folioCache.expire(folioId);
    }
    public void reloadFolios() throws DesignException {
        folioCache.clear();
    }
    public void reloadReport(long reportId) throws DesignException {
        // TODO Auto-generated method stub

    }
    public void reloadReports() throws DesignException {
        // TODO Auto-generated method stub

    }
    public void reloadField(long fieldId) throws DesignException {
		initialize();
        //find all folios and calls with this field and update / clear them
        Field field;
        try {
            log.debug("Reloading field " + fieldId);
			Connection conn = getDesignConnection();
            try {
                field = loadField(fieldId, conn);
            } catch(BeanException e) {
                throw new DesignException(e);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch(DataLayerException e) {
            throw new DesignException(e);
        }
        for(Iterator<CallKey> iter = callCache.keySet().iterator(); iter.hasNext(); ) {
            CallKey ck = iter.next();
            for(FieldOrder fo : ck.fieldOrderList) {
                if(fo.getField().equals(field)) {
                	if(iter instanceof ExpirableIterator<?>)
                		((ExpirableIterator<CallKey>)iter).expire();
                	else
                		iter.remove();
                    break;
                }
            }
        }
    }

    public void reloadOutputType(int outputTypeId) throws DesignException {
        reloadOutputTypes();
        //outputTypeCache.put(outputTypeId, outputType);
    }

    protected boolean containsOperator(Filter searchIn, Operator searchFor) {
        if(searchIn instanceof FilterItem) {
            if(((FilterItem)searchIn).getOperator().equals(searchFor)) return true;
			return false;
        } else if(searchIn instanceof FilterGroup) {
            for(Filter f : ((FilterGroup)searchIn).getChildFilters()) {
                if(containsOperator(f, searchFor)) return true;
            }
        }
        return false;
    }

    public void reloadOperator(int operatorId) throws DesignException {
        /*operatorCache.put(operator.getOperatorId(), operator);
        if(operator instanceof StandardOperator)
            ((StandardOperator)operator).setDesignEngine(this);
           */
        reloadOperators();
        Operator operator = getOperator(operatorId);
        // find all calls with this operator and clear them
        for(Iterator<CallKey> iter = callCache.keySet().iterator(); iter.hasNext(); ) {
            CallKey ck = iter.next();
            if(ck.filter != null && containsOperator(ck.filter, operator)) {
            	if(iter instanceof ExpirableIterator<?>)
            		((ExpirableIterator<CallKey>)iter).expire();
            	else
            		iter.remove();
            }
        }
    }

    protected boolean containsFilter(Filter searchIn, Filter searchFor) {
        if(searchIn.equals(searchFor)) return true;
        else if(searchIn instanceof FilterGroup) {
            for(Filter f : ((FilterGroup)searchIn).getChildFilters()) {
                if(containsFilter(f, searchFor)) return true;
            }
        }
        return false;
    }

    public void reloadFilterItem(FilterItem filterItem) throws DesignException {
        // find all calls with this filter and clear them
        for(Iterator<CallKey> iter = callCache.keySet().iterator(); iter.hasNext(); ) {
            CallKey ck = iter.next();
            if(ck.filter != null && containsFilter(ck.filter, filterItem)) {
            	if(iter instanceof ExpirableIterator<?>)
            		((ExpirableIterator<CallKey>)iter).expire();
            	else
            		iter.remove();
            }
        }
        //TODO: should do this with folios also
    }

    public void reloadFilterGroup(FilterGroup filterGroup) throws DesignException {
        // find all calls with this filter and clear them
        for(Iterator<CallKey> iter = callCache.keySet().iterator(); iter.hasNext(); ) {
            CallKey ck = iter.next();
            if(ck.filter != null && containsFilter(ck.filter, filterGroup)) {
            	if(iter instanceof ExpirableIterator<?>)
            		((ExpirableIterator<CallKey>)iter).expire();
            	else
            		iter.remove();
            }
        }
        //TODO: should do this with folios also
    }

    public void reloadFields() throws DesignException {
        // clear fields Cache, calls Cache
        clearFields();
        clearCalls();
        clearFolios();
    }

    public void reloadOutputTypes() throws DesignException {
		initialize();
        try {
			Connection conn = getDesignConnection();
            try {
                loadOutputTypes(conn);
            } catch(BeanException e) {
                throw new DesignException(e);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch(DataLayerException e) {
            throw new DesignException(e);
        }
    }

    public void reloadOperators() throws DesignException {
		initialize();
        clearCalls();
        try {
			Connection conn = getDesignConnection();
            try {
                loadOperators(conn);
            } catch(ConvertException e) {
                throw new DesignException(e);
            } catch(BeanException e) {
                throw new DesignException(e);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch(DataLayerException e) {
            throw new DesignException(e);
        }
    }

    public void reloadFilters() throws DesignException {
        // clear all calls
        clearCalls();
        clearFolios();
    }

    public FilterItem getFilterItem(long filterItemId) throws DesignException {
        throw new DesignException("Not supported");
    }

    public FilterGroup getFilterGroup(long filterGroupId) throws DesignException {
        return getFilterGroup(filterGroupId, (Translator)null);
    }

    public FilterGroup getFilterGroup(long filterGroupId, Translator translator) throws DesignException {
		initialize();
		try {
			Connection conn = getDesignConnection();
            try {
                return getFilterGroup(filterGroupId, translator, conn);
            } catch(ConvertException e) {
                throw new DesignException(e);
            } catch(BeanException e) {
                throw new DesignException(e);
            } catch(DesignException e) {
                throw new DesignException(e);
            } finally {
                try { conn.close(); } catch(SQLException e) {}
            }
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch(DataLayerException e) {
            throw new DesignException(e);
        }
    }

    public Report newReport() throws DesignException {
        return newReport(null);
    }

    public InnerReport newReport(Translator translator) throws DesignException {
    	InnerReport report = new InnerReport();
        report.setVerbage("run-date-format",
                translateIfProvided("MESSAGE:Run Date: {,DATE,MMM dd, yyyy h:mm:ss a z}", translator));
        report.setVerbage("rows-summary-format",
                translateIfProvided("MESSAGE:Showing rows {0} to {1} of {2}", translator));
        report.setVerbage("charts-summary-format",
                translateIfProvided("MESSAGE:Created {0} charts", translator));

        /*
        report.setVerbage("group-totals-label",
                translateIfProvided("Totals:", translator));
        report.setVerbage("no-data-label",
                translateIfProvided("No data found", translator));
        report.setVerbage("error-label",
                translateIfProvided("An error occurred:", translator));
        report.setVerbage("more-rows-label",
                translateIfProvided("more row(s)...", translator));
        */
        return report;
    }

    protected String translateIfProvided(String text, Translator translator) {
        if(translator == null) return text;
		return translator.translate(text, (Object[]) null);
    }

    public void reloadJoins() throws DesignException {
        SQLBuilder newSB = new SQLBuilder(comparator);
        newSB.clearJoins();
        try {
            loadJoins(newSB);
        } catch(SQLException e) {
            throw new DesignException(e);
        } catch(DataLayerException e) {
            throw new DesignException(e);
        } catch(ConvertException e) {
            throw new DesignException(e);
        }
        sqlBuilder = newSB;

        // lastly clear the calls
        clearCalls();
    }

    public Aggregate getColumnAggregate(Aggregate aggregate) {
    	return aggregate;
    }

	protected DbSpecific getDbSpecific(DatabaseMetaData metaData) throws SQLException {
		StringBuilder sb = new StringBuilder();
		sb.append(metaData.getDatabaseProductName()).append('-').append(metaData.getDatabaseMajorVersion()).append('.').append(metaData.getDatabaseMinorVersion());
		return reportSpecificCache.getOrCreate(sb.toString(), metaData);
	}

	public DataSourceFactory getDataSourceFactory() {
		if(dataSourceFactory == null) {
			setDataSourceFactory(DataLayerMgr.getDataSourceFactory());
		}
		return dataSourceFactory;
	}

	public void setDataSourceFactory(DataSourceFactory dataSourceFactory) {
		DataSourceFactory old = this.dataSourceFactory;
		this.dataSourceFactory = dataSourceFactory;
		if(!ConvertUtils.areEqual(old, dataSourceFactory))
			initializer.reset();
	}

	public String getDefaultDataSourceName() {
		return defaultDataSourceName;
	}

	public void setDefaultDataSourceName(String defaultDataSourceName) {
		String old = this.defaultDataSourceName;
		this.defaultDataSourceName = defaultDataSourceName;
		if(!ConvertUtils.areEqual(old, defaultDataSourceName))
			initializer.reset();
	}

	public String getDesignDataSourceName() {
		return designDataSourceName;
	}

	public void setDesignDataSourceName(String designDataSourceName) {
		String old = this.designDataSourceName;
		this.designDataSourceName = designDataSourceName;
		if(!ConvertUtils.areEqual(old, designDataSourceName))
			initializer.reset();
	}
	
	public void createQueryPillars(Report report, Map<String, ?> reportParams) throws DesignException {
		StandardFolio f=((StandardFolio)report.getFolios()[0]);
		if(f.query!=null){
			f.pillars.clear();
			Connection conn = findConnection(f);
			try {
				DbSpecific dbSpecific = getDbSpecific(conn.getMetaData());
				ResultSetMetaData rsmd=dbSpecific.getQueryMetaData(conn, f.query);
				int count = rsmd.getColumnCount();
				for(int i=1;i<count+1;i++){
					StandardPillar pillar1=f.newPillar();
					pillar1.setPillarType(PillarType.LINK);
					pillar1.setLabelFormat(new LiteralFormat(rsmd.getColumnName(i)));
					pillar1.setGroupingLevel(2);
					FieldOrder[] fo1=new FieldOrder[1];
					fo1[0]=new FieldOrder();
					fo1[0].setDisplayColumnName(rsmd.getColumnName(i));
					fo1[0].setSortColumnName(rsmd.getColumnName(i));
					pillar1.setFieldOrders(fo1);
					if(rsmd.isSigned(i)){
						pillar1.setSortType(DataGenre.NUMBER);
						if (!DialectResolver.isOracle())
							pillar1.setDisplayFormat(ConvertUtils.getFormat(defaultNumericFormat)); 
					}else if(rsmd.getColumnClassName(i).equals("java.sql.Date") ||rsmd.getColumnClassName(i).equals("java.sql.Timestamp") ){
						pillar1.setSortType(DataGenre.DATE);
						pillar1.setDisplayFormat(ConvertUtils.getFormat(defaultDateFormat));
						pillar1.setSortFormat(ConvertUtils.getFormat(defaultDateFormat));
					}else{
						pillar1.setSortType(DataGenre.STRING);
					}
				}
			} catch(SQLException e) {
				throw new DesignException(e);
			} finally {
				DbUtils.closeSafely(conn);
			}
			createQueryFolioParams(f, reportParams);
		}
	}
	
	public void createQueryFolioParams(Folio folio, Map<String, ?> reportParams) {
		String[] parameterNames = ConvertUtils.convertSafely(String[].class, reportParams.get("paramNames"), null);
		SQLType[] parameterTypes = ConvertUtils.convertSafely(SQLType[].class, reportParams.get("paramTypes"), null);
		Boolean[] paramRequired = ConvertUtils.convertSafely(Boolean[].class, reportParams.get("paramRequired"), null);
		int n;
		if(parameterNames == null || parameterNames.length == 0) {
			n = 0;
			if(parameterTypes != null)
				n = parameterTypes.length;
			while(reportParams.containsKey("params." + (n + 1)))
				n++;
			parameterNames = new String[n];
			for(int i = 0; i < n; i++) {
				parameterNames[i] = String.valueOf(i + 1);
			}
		} else {
			n = parameterNames.length;
			if(parameterTypes != null && parameterTypes.length > n)
				n = parameterTypes.length;
		}
		Param[] paramArray = new Param[n];
		for(int i=0; i<paramArray.length;i++){
			paramArray[i]=new StandardParam(this, false);
			if(i < parameterNames.length && parameterNames[i] != null) {
				paramArray[i].setName(parameterNames[i]);
				paramArray[i].setValue(reportParams.get("params." + parameterNames[i]));
			}
			if(parameterTypes != null && i < parameterTypes.length && parameterTypes[i] != null){
					paramArray[i].setSqlType(parameterTypes[i]);	
			}
			if(paramRequired!=null&&paramRequired[i]!=null){
				paramArray[i].setRequired(paramRequired[i]);
			}
		}
        StandardFilterGroup filterGroup = new StandardFilterGroup();
        filterGroup.setSeparator("AND");
        StandardFilterItem filter=new StandardFilterItem(this);
        filter.setOperatorParams(paramArray);
        filterGroup.addFilter(filter);
		FolioUpdateUtils.addFilter(folio, filterGroup, "AND");
	}

}