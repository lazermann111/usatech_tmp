/*
 * Created on Feb 7, 2006
 *
 */
package simple.falcon.engine.standard;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataSourceNotFoundException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Operator.Side;
import simple.io.Log;
import simple.param.ParamEditor.SelectParamEditor;
import simple.sql.BuildSQLException;
import simple.sql.Expression;
import simple.util.ConversionMap;

public class StandardLookupParamEditor extends SelectParamEditor {
	private static final Log log = Log.getLog();
    protected boolean multiple;
    protected boolean required;
    protected int maxRows;
    //protected final Map<Object,Object> options = new LinkedHashMap<Object,Object>();
    protected long valueFieldId;
    protected StandardDesignEngine engine;
    protected FilterItem filter;
    public StandardLookupParamEditor(){}
    public StandardLookupParamEditor(String pattern) {
        super(pattern);
    }
    public StandardLookupParamEditor(String pattern, StandardDesignEngine engine, FilterItem filter) {
        super(pattern);
        // load options on each call
        this.engine = engine;
        this.filter = filter;
        //retrieveOptions(engine, filter);
    }

    protected void retrieveOptions(Map<Object,Object> options, StandardDesignEngine engine, FilterItem filter) {
        if(!required) options.put("", "");
        Expression valueExpression;
		try {
			valueExpression = engine.getField(valueFieldId).getDisplayExpression();
		} catch(DesignException e) {
			log.warn("Could not get field for id = " + valueFieldId + " for LOOKUP parameter", e);
			return;
		}
        Expression keyExpression;
        switch(filter.getOperator().getSide()) {
            case SORT: case BOTH: keyExpression = filter.getField().getSortExpression(); break;
            case DISPLAY: case NONE: default: keyExpression = filter.getField().getDisplayExpression(); break;
        }
        try {
			engine.retrieveDistinctValues(valueExpression, keyExpression, Side.DISPLAY, maxRows, options);
		} catch(SQLException e) {
			log.warn("Could not get list of values for field id = " + valueFieldId, e);
		} catch(BuildSQLException e) {
			log.warn("Could not get list of values for field id = " + valueFieldId, e);
		} catch(DataSourceNotFoundException e) {
			log.warn("Could not get list of values for field id = " + valueFieldId, e);
		} catch(DesignException e) {
			log.warn("Could not get list of values for field id = " + valueFieldId, e);
		}
    }
    @Override
	public boolean isMultiple() {
        return multiple;
    }
    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }
    @Override
	public Map<String, Option> getOptions() throws SQLException, BuildSQLException, DataLayerException, DesignException {
		Map<String, Option> options = new LinkedHashMap<String, Option>();
		retrieveOptions(new ConversionMap<String, Object, Option, Object>(options) {
			@Override
			protected Option convertValueTo(Object value1) {
				return makeOption(null, ConvertUtils.getStringSafely(value1), null);
			}

			@Override
			protected Object convertValueFrom(Option value0) {
				return value0 == null ? null : value0.getLabel();
			}

			@Override
			protected String convertKeyTo(Object key1) {
				return ConvertUtils.getStringSafely(key1);
			}

			@Override
			protected Object convertKeyFrom(String key0) {
				return key0;
			}

			@Override
			protected boolean isValueReversible() {
				return true;
			}
		}, engine, filter);
		return options;
    }

    @Override
	protected void parsePattern(String pattern) {
    	maxRows = 100; //set default
        if(pattern != null) {
            pattern = pattern.trim();
            switch(pattern.charAt(0)) {
                case '?':
                    required = false;
                    multiple = false;
                    pattern = pattern.substring(1);
                    break;
                case '+':
                    required = true;
                    multiple = true;
                    pattern = pattern.substring(1);
                    break;
                case '*':
                    required = false;
                    multiple = true;
                    pattern = pattern.substring(1);
                    break;
                default:
                    required = true;
                    multiple = false;
            }
            if(pattern.trim().length() > 0) {
                String[] parts = pattern.trim().split("[<]", 2);
                if(parts == null || parts.length < 1) throw new IllegalArgumentException("Pattern '" + pattern + "' does not conform to required format");
                try {
                    valueFieldId = Long.parseLong(parts[0].trim());
                } catch(NumberFormatException e) {
                    throw new IllegalArgumentException("Pattern '" + pattern + "' does not conform to required format", e);
                }
                if(parts.length > 1 && parts[1].trim().length() > 0) {
                    try {
                        maxRows = Integer.parseInt(parts[1].trim());
                    } catch(NumberFormatException e) {
                        throw new IllegalArgumentException("Pattern '" + pattern + "' does not conform to required format", e);
                    }
                }
            }
        } else {
            required = true;
            multiple = false;
        }
    }

    @Override
	public boolean isRequired() {
        return required;
    }

    @Override
	public String getValidRegex() {
        return null;//No validation necessary
    }

public int getMaxRows() {
	return maxRows;
}

public void setMaxRows(int maxRows) {
	this.maxRows = maxRows;
}

public long getValueFieldId() {
	return valueFieldId;
}

public void setValueFieldId(long valueFieldId) {
	this.valueFieldId = valueFieldId;
}

public StandardDesignEngine getEngine() {
	return engine;
}

public FilterItem getFilter() {
	return filter;
}

public void setRequired(boolean required) {
	this.required = required;
}
public void setEngine(StandardDesignEngine engine) {
	this.engine = engine;
}
public void setFilter(FilterItem filter) {
	this.filter = filter;
}

}
