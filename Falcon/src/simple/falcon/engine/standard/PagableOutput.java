package simple.falcon.engine.standard;

import java.io.IOException;

import simple.app.ServiceException;
import simple.falcon.engine.Output;

public interface PagableOutput extends Output {
	public void next() throws IOException, ServiceException;
	public int getMaxPageId();
	public void setMaxPageId(int maxPageId);
}
