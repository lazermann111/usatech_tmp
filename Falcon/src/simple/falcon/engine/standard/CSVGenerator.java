/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Generator;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsValueRetriever;
import simple.falcon.engine.Scene;
import simple.io.Log;
import simple.results.Results;
import simple.text.LiteralFormat;
import simple.text.StringUtils;

public class CSVGenerator implements Generator {
    protected ExecuteEngine engine;

    public CSVGenerator(ExecuteEngine engine) {
        this.engine = engine;
    }

    public void generate(Report report, OutputType outputType, Map<String,Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
        // generate the report
        try {
            PrintWriter pw = new PrintWriter(out.getWriter());
            Folio[] folios = report.getFolios();
            for(int i = 0; i < folios.length; i++) {
                writeFolio(folios[i], results[i], pw, parameters);
            }
            pw.flush();
        } catch(IOException e) {
            throw new ExecuteException(e);
        }
    }

	protected void writeFolio(Folio folio, Results results, PrintWriter pw, Map<String, Object> parameters) {
        Pillar[] initPillars = folio.getPillars();
		boolean showHeader;
		try {
			showHeader = ConvertUtils.getBoolean(folio.getDirective("show-header"), true);
		} catch (ConvertException e) {
			Log.getLog().debug("Could not convert '" + folio.getDirective("show-header") + "' to a boolean", e);
			showHeader = true;
		}
        List<Pillar> listPillars = new ArrayList<Pillar>();
		for (Pillar pillar : initPillars) {
			if (!showHeader || 
					/* Just to exclude note column in Device Health Report */
					pillar.getLabelFormat() != null || 
					pillar.getGroupingLevel() < 3 /* Level One Group (Table) */) {
				listPillars.add(pillar);
			}
		}
		Pillar[] pillars = new Pillar[listPillars.size()];
		pillars = listPillars.toArray(pillars);

		Object[] row = new Object[pillars.length];

        boolean avoidQuotes;
        try {
        	avoidQuotes = ConvertUtils.getBoolean(folio.getDirective("cvs-avoid-quotes"), false);
        } catch(ConvertException e) {
        	Log.getLog().debug("Could not convert '" + folio.getDirective("cvs-avoid-quotes") + "' to a boolean", e);
        	avoidQuotes = false;
        }
        
        int groupColumnIndex=0;
        for(Pillar pillar : pillars) {
        	ResultsValueRetriever displayRetreiver = pillar.getDisplayValueRetriever();
            ResultsValueRetriever sortRetreiver = pillar.getSortValueRetriever();
            List<String> sortGCNs = sortRetreiver.addGroups(results);
            List<String> displayGCNs = displayRetreiver.addGroups(results);
            if(!displayGCNs.isEmpty()) {
            	groupColumnIndex = results.getColumnIndex(displayGCNs.get(displayGCNs.size()-1));
            } else if(!sortGCNs.isEmpty()) {
            	groupColumnIndex = results.getColumnIndex(sortGCNs.get(sortGCNs.size()-1));
            }
            displayRetreiver.trackAggregates(results);
            sortRetreiver.trackAggregates(results);
        }
        Collection<Integer> rawColumns = new HashSet<Integer>();
        try {
        	int[] arr = ConvertUtils.convert(int[].class, folio.getDirective("use-raw-values"));
        	if(arr != null) {
	        	for(int index : arr) {
	        		rawColumns.add(index);
	        	}
        	} else {
        		// This is an attempt to avoid quoting dates and numbers
        		for(int i = 0; i < pillars.length; i++) {
        			if(pillars[i].getDisplayFormat() == null && pillars[i].getFieldOrders().length == 1)
        				rawColumns.add(i);
        		}
        	}
        } catch(ConvertException e) {
        	Log.getLog().debug("Could not convert '" + folio.getDirective("use-raw-values") + "' to an array of integers", e);
        }
		boolean hasData = results.next();
		if(showHeader) {
			for(int i = 0; i < pillars.length; i++) {
				if(pillars[i].getLabelFormat() instanceof LiteralFormat)
					row[i] = pillars[i].getLabelFormat().format(null);
				else if(pillars[i].getLabelFormat() != null)
					row[i] = ConvertUtils.formatObject(hasData ? pillars[i].getDisplayValueRetriever().getAggregate(results, 0, parameters) : null, pillars[i].getLabelFormat());
			}
			StringUtils.writeCSVLine(row, pw, avoidQuotes);
		}
		for(; hasData; hasData = results.next()) {
        	if(results.isGroupEnding(groupColumnIndex)) {
	            for(int i = 0; i < pillars.length; i++) {
	            	Object value = pillars[i].getDisplayValueRetriever().getAggregate(results, groupColumnIndex, parameters);
	                row[i] = rawColumns.contains(i) ? value : ConvertUtils.formatObject(value, pillars[i].getDisplayFormat());
	            }
	            StringUtils.writeCSVLine(row, pw, avoidQuotes);
        	}
        }
        //pw.println();
    }
    public boolean isResetResultsRequired(OutputType outputType) {
    	return false;
    }
}