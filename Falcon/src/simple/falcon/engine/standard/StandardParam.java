/*
 * Created on Jan 18, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.LinkedHashMap;
import java.util.Map;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Param;
import simple.lang.SystemUtils;
import simple.param.ParamEditor;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.translator.Translator;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

/**
 * @author bkrug
 *
 */
public class StandardParam implements Param {
	protected String name;
	protected Object value;
	protected String label;
	protected String prompt;
	protected SQLType sqlType;
	protected ParamEditor editor;
	protected final StandardDesignEngine designEngine;
	protected final boolean fromField;
	protected boolean isRequired=true;

	public StandardParam(StandardDesignEngine designEngine, boolean fromField) {
		this.designEngine = designEngine;
		this.fromField = fromField;
	}

	/**
	 * @see simple.falcon.engine.Param#copy(simple.translator.Translator)
	 */
	public Param copy(Translator translator) {
		StandardParam p = new StandardParam(designEngine, fromField);
		p.editor = editor;
		p.label = label;
		p.name = name;
		p.prompt = prompt;
		p.sqlType = sqlType.clone();
		p.value = value;
		return p;
	}

	/**
	 * @see simple.falcon.engine.Param#getSqlType()
	 */
	public SQLType getSqlType() {
		if(sqlType == null)
			sqlType = new SQLType();
		return sqlType;
	}

	/**
	 * @param sqlType
	 *            The sqlType to set.
	 */
	public void setSqlType(SQLType sqlType) {
		this.sqlType = sqlType;
	}

	/**
	 * @see simple.falcon.engine.Param#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see simple.falcon.engine.Param#getPrompt()
	 */
	public String getPrompt() {
		return prompt;
	}

	/**
	 * @param prompt
	 *            The prompt to set.
	 */
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	/**
	 * @see simple.falcon.engine.Param#getValue()
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @see simple.falcon.engine.Param#setValue(java.lang.Object)
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @see simple.falcon.engine.Param#getLabel()
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            The label to set.
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @see simple.falcon.engine.Param#getEditor()
	 */
	public ParamEditor getEditor() {
		return editor;
	}

	public void setEditor(ParamEditor editor) {
		this.editor = editor;
		if(editor instanceof StandardLookupParamEditor) {
			StandardLookupParamEditor slpe = (StandardLookupParamEditor) editor;
			slpe.setEngine(designEngine);
		}
	}

	/**
	 * @see simple.falcon.engine.Param#getJavaType()
	 */
	public Class<? extends Object> getJavaType() {
		return SQLTypeUtils.getJavaType(sqlType);
	}

	public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException {
		if(tagName == null)
			tagName = "param";
		Map<String, String> atts = new LinkedHashMap<String, String>(6);
		atts.put("name", getName());
		atts.put("value", ConvertUtils.getStringSafely(getValue()));
		atts.put("label", getLabel());
		atts.put("prompt", getPrompt());
		atts.put("sqlType", getSqlType().toString());
		builder.elementStart(tagName, atts);
		if(getEditor() != null) {
			builder.put("editor", getEditor());
		}
		builder.elementEnd(tagName);
	}

	public void readXML(ObjectBuilder builder) throws DesignException {
		setName(builder.getAttrValue("name"));
		setValue(builder.getAttrValue("value"));
		setLabel(builder.getAttrValue("label"));
		setPrompt(builder.getAttrValue("prompt"));
		setSqlType(builder.get("sqlType", SQLType.class));
		ObjectBuilder[] editor = builder.getSubNodes("editor");
		if(editor != null && editor.length > 0) {
			ParamEditor editorObj;
			try {
				editorObj = ParamEditor.readXML(editor[0]);
			} catch(SAXException e) {
				throw new DesignException(e);
			}
			setEditor(editorObj);
		}
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hc = 0;
		hc = SystemUtils.addHashCode(hc, name);
		hc = SystemUtils.addHashCode(hc, value);
		hc = SystemUtils.addHashCode(hc, label);
		hc = SystemUtils.addHashCode(hc, prompt);
		hc = SystemUtils.addHashCode(hc, sqlType);
		hc = SystemUtils.addHashCode(hc, editor);
		return hc;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof StandardParam))
			return false;
		StandardParam p = (StandardParam) obj;
		return ConvertUtils.areEqual(name, p.name) && ConvertUtils.areEqual(value, p.value) && ConvertUtils.areEqual(label, p.label) && ConvertUtils.areEqual(prompt, p.prompt)
				&& ConvertUtils.areEqual(sqlType, p.sqlType) && ConvertUtils.areEqual(editor, p.editor);
	}

	public boolean isFromField() {
		return fromField;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	@Override
	public boolean isRequired() {
		return isRequired;
	}
}
