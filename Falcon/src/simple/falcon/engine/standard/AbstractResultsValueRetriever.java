/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.Map;

import simple.falcon.engine.ResultsValueRetriever;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.results.ResultsFilter;

public abstract class AbstractResultsValueRetriever implements ResultsValueRetriever {
    protected Object getValue(Results results, String columnName) {
        return results.getValue(columnName);
    }
    public Object getAggregate(Results results, int groupColumnIndex, Map<String,Object> context) {
    	return getAggregate(results, null, groupColumnIndex, context);
    }
    protected Object getAggregate(Results results, ResultsFilter filter, int columnIndex, Aggregate aggregate, int groupColumnIndex) {
    	if(aggregate == null)
    		return results.getValue(columnIndex);
		return results.getAggregate(columnIndex, groupColumnIndex, filter, aggregate);
    }
	public void trackAggregates(Results results) {
		trackAggregates(results, null);
	}
	protected void trackAggregate(Results results, ResultsFilter filter, int columnIndex, Aggregate aggregate) {
		if(aggregate != null)
			results.trackAggregate(columnIndex, filter, aggregate);
	}
	protected String addGroup(Results results, String columnName, Aggregate aggregate) {
		if(aggregate == null) {
			results.addGroup(columnName);
			return columnName;
		}
		return null;
	}
}