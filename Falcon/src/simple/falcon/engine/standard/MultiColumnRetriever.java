/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import simple.results.Results;
import simple.results.ResultsFilter;
import simple.results.Results.Aggregate;

public class MultiColumnRetriever extends AbstractResultsValueRetriever {
    protected String[] columnNames;
    protected Aggregate[] aggregates;
    protected int[] columnIndexs;
    public MultiColumnRetriever(String[] columnNames, Aggregate[] aggregates) {
        this.columnNames = columnNames;
        this.aggregates = aggregates;
    }
    public Object getValue(Results results, Map<String,Object> context) {
        Object[] value = new Object[columnNames.length];
        for(int i = 0; i < value.length; i++) {
            value[i] = getValue(results, columnNames[i]);
        }
        return wrapWithContext(value, context);
    }
    public Object getAggregate(Results results, ResultsFilter filter, int groupColumnIndex, Map<String,Object> context) {
        Object[] value = new Object[columnNames.length];
        for(int i = 0; i < value.length; i++) {
        	value[i] = getAggregate(results, filter, columnIndexs[i], aggregates[i], groupColumnIndex);
        }
        return wrapWithContext(value, context);
    }
	public void trackAggregates(Results results, ResultsFilter filter) {
		if(columnIndexs==null){
        	columnIndexs=new int[columnNames.length];
        	for(int i=0;i<columnNames.length;i++){
        		columnIndexs[i]=results.getColumnIndex(columnNames[i]);
        	}
        }
		for(int i = 0; i < columnIndexs.length; i++) {
            trackAggregate(results, filter, columnIndexs[i], aggregates[i]);
        }
	}
	public List<String> addGroups(Results results) {
		List<String> groupColumnNames = new ArrayList<String>();
		for(int i = 0; i < columnNames.length; i++) {
            String tmp = addGroup(results, columnNames[i], aggregates[i]);
            if(tmp != null)
            	groupColumnNames.add(tmp);
        }
		return groupColumnNames;
	}
	protected Object wrapWithContext(final Object[] value, final Map<String,Object> context) {
		return new MultiValueWithContext(value, context);
	}
}