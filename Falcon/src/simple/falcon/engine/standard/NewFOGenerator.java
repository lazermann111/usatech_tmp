/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.TransformerHandler;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Scene;
import simple.falcon.engine.util.CssToXslParser;
import simple.io.LoadingException;
import simple.io.Log;
import simple.xml.TemplatesLoader;

public class NewFOGenerator extends XSLGenerator {
    private static final Log log = Log.getLog();
    protected CssToXslParser cssToXSLParser = new CssToXslParser();

    public NewFOGenerator(ExecuteEngine engine) {
        super(engine);
    }

    public NewFOGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
        super(engine, inputSourceType);
    }
    @Override
    protected Result createOutputTarget(OutputType outputType, Map<String,Object> parameters, Scene scene, Output out) throws IOException, ExecuteException {
        // chain together the report building xsl and the style XSL from the css
        try {
            Result result = getStreamResult(outputType, out);
            TemplatesLoader tl = TemplatesLoader.getInstance(scene.getTranslator());
            TransformerHandler handler;
            for(int i = scene.getStylesheets().length - 1; i >= 0; i--) {//must be backwards
                String cssPath = scene.getStylesheets()[i];
                if(cssPath != null && cssPath.trim().length() > 0) {
                    URL cssURL = engine.getResourceResolver().getResourceURL(engine.getCssDirectory() + cssPath);
                    URL xslURL = cssToXSLParser.getXslUrl(cssURL);
                    if(log.isDebugEnabled()) log.debug("Chaining the css at index " + (i+1) + "('" + cssURL + "') with XSL '" + xslURL + "' to the transformation");
                    handler = tl.newTransformerHandler(xslURL, refreshInterval);
                    handler.setResult(result);
                    result = new SAXResult(handler);
                } else {
                    if(log.isDebugEnabled()) log.debug("CSS is blank for at index " + (i+1));
                }
            }
            handler = tl.newTransformerHandler("resource:simple/falcon/templates/report/xhtml-to-pdf.xsl");
            handler.setResult(result);
            result = new SAXResult(handler);
            return result;
        } catch(LoadingException e) {
            throw new ExecuteException(e);
        } catch (TransformerConfigurationException e) {
            throw new ExecuteException(e);
        } catch (IOException e) {
            throw new ExecuteException(e);
        }
     }
}