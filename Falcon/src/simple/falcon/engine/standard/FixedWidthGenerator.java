/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Generator;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsValueRetriever;
import simple.falcon.engine.Scene;
import simple.io.Log;
import simple.results.Results;
import simple.text.LiteralFormat;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class FixedWidthGenerator implements Generator {
	private static final Log log = Log.getLog();
    protected ExecuteEngine engine;

    public FixedWidthGenerator(ExecuteEngine engine) {
        this.engine = engine;
    }

    public void generate(Report report, OutputType outputType, Map<String,Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
        // generate the report
        try {
            PrintWriter pw = new PrintWriter(out.getWriter());
            Folio[] folios = report.getFolios();
            for(int i = 0; i < folios.length; i++) {
                writeFolio(folios[i], results[i], pw, parameters);
            }
            pw.flush();
        } catch(IOException e) {
            throw new ExecuteException(e);
        }
    }

	protected void writeFolio(Folio folio, Results results, PrintWriter pw, Map<String, Object> parameters) throws ExecuteException {
        Pillar[] pillars = folio.getPillars();
        int[] widths = new int[pillars.length];
        Justification[] justs = new Justification[pillars.length];
        boolean showHeader;
        try {
        	showHeader = ConvertUtils.getBoolean(folio.getDirective("show-header"), true);
        } catch(ConvertException e) {
        	log.debug("Could not convert '" + folio.getDirective("show-header") + "' to a boolean", e);
        	showHeader = true;
        }
        int groupColumnIndex = 0;
        for(int i = 0; i < pillars.length; i++) {
	        Pillar pillar = pillars[i];
        	ResultsValueRetriever displayRetreiver = pillar.getDisplayValueRetriever();
            ResultsValueRetriever sortRetreiver = pillar.getSortValueRetriever();
            List<String> sortGCNs = sortRetreiver.addGroups(results);
            List<String> displayGCNs = displayRetreiver.addGroups(results);
            if(!displayGCNs.isEmpty()) {
            	groupColumnIndex = results.getColumnIndex(displayGCNs.get(displayGCNs.size()-1));
            } else if(!sortGCNs.isEmpty()) {
            	groupColumnIndex = results.getColumnIndex(sortGCNs.get(sortGCNs.size()-1));
            }
            displayRetreiver.trackAggregates(results);
            sortRetreiver.trackAggregates(results);

            widths[i] = calculateWidth(pillar.getWidth());
            if(widths[i] == 0) throw new ExecuteException("Width not specified for pillar #" + (i+1));
            if(pillar.getSortType() == null)
            	justs[i] = Justification.LEFT;
            else
	            switch(pillar.getSortType()) {
	            	case NUMBER:
	            		justs[i] = Justification.RIGHT;
	            		break;
	            	default:
	            		justs[i] = Justification.LEFT;
	            }
        }
        if(showHeader) {
	        for(int i = 0; i < pillars.length; i++) {
	        	String s;
	        	if(pillars[i].getLabelFormat() instanceof LiteralFormat)
	        		s = pillars[i].getLabelFormat().format(null);
	        	else if(pillars[i].getLabelFormat() != null)
	        		s = ConvertUtils.formatObject(pillars[i].getDisplayValueRetriever().getAggregate(results, 0, parameters), pillars[i].getLabelFormat());
	        	else
	        		s = null;
	        	pw.print(StringUtils.pad(s, StringUtils.SPACE, widths[i], justs[i]));
	        }
	        pw.println();
	        pw.flush();
        }
        Collection<Integer> rawColumns = new HashSet<Integer>();
        try {
        	int[] arr = ConvertUtils.convert(int[].class, folio.getDirective("use-raw-values"));
        	if(arr != null) {
	        	for(int index : arr) {
	        		rawColumns.add(index);
	        	}
        	} else {
        		// This is an attempt to avoid quoting dates and numbers
        		for(int i = 0; i < pillars.length; i++) {
        			if(pillars[i].getDisplayFormat() == null && pillars[i].getFieldOrders().length == 1)
        				rawColumns.add(i);
        		}
        	}
        } catch(ConvertException e) {
        	log.debug("Could not convert '" + folio.getDirective("use-raw-values") + "' to an array of integers", e);
        }
        while(results.next()) {
        	if(results.isGroupEnding(groupColumnIndex)) {
	            for(int i = 0; i < pillars.length; i++) {
	            	Object value = pillars[i].getDisplayValueRetriever().getAggregate(results, groupColumnIndex, parameters);
	                String s;
	                if(!rawColumns.contains(i))
	                	s = ConvertUtils.formatObject(value, pillars[i].getDisplayFormat());
	                else if(value == null)
	                	s = null;
	                else
	                	s = value.toString();
	                pw.print(StringUtils.pad(s, StringUtils.SPACE, widths[i], justs[i]));
	            }
	            pw.println();
	            pw.flush();
        	}
        }
    }
    protected static Pattern WIDTH_PATTERN = Pattern.compile("\\s*(\\d+(?:[.]\\d+)?).*");

    protected int calculateWidth(String widthString) {
    	if(widthString == null) return 0;
    	Matcher matcher = WIDTH_PATTERN.matcher(widthString);
    	if(matcher.matches()) try {
    		return (int)Double.parseDouble(matcher.group(1));
    	} catch(NumberFormatException e) {
    		log.debug("Could not parse number", e);
    	}
    	return 0;
    }
    public boolean isResetResultsRequired(OutputType outputType) {
    	return false;
    }
}