/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import simple.falcon.engine.ExecuteEngine;

public class ChartPDFGenerator extends PDFGenerator {
    public ChartPDFGenerator(ExecuteEngine engine) {
        super(engine, InputSourceType.CHART);
    }
}