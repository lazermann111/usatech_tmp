package simple.falcon.engine.standard;

import java.io.IOException;
import java.net.URL;

import simple.io.ResourceResolver;

public class URLResourceResolver implements ResourceResolver {
	public URL getResourceURL(String path) throws IOException {
		return new URL(path);
	}
}
