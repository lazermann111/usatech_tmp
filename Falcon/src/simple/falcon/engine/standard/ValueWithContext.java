/**
 *
 */
package simple.falcon.engine.standard;

import java.beans.IntrospectionException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Map;

import org.apache.commons.beanutils.DynaProperty;

import simple.bean.AbstractDynaBean;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Convertible;
import simple.bean.DynaBean;
import simple.bean.ReflectionUtils;

public class ValueWithContext extends AbstractDynaBean implements Convertible, Serializable {
	private static final long serialVersionUID = 3565074821338244936L;
	protected final Object value;
	protected final Map<String,Object> context;
	protected transient DynaProperty[] dynaClassProperties;

	public ValueWithContext(Object value, Map<String, Object> context) {
		this.value = value;
		this.context = context;
	}
	@Override
	public Object get(String name) {
		if(name == null || name.trim().length() == 0) return value;
		try {
			if((name=name.trim()).charAt(0) == '.')
				return ReflectionUtils.getProperty(value, name.substring(1));
			return ReflectionUtils.getMapProperty(context, name);
		} catch(IntrospectionException e) {
			return null;
		} catch(IllegalAccessException e) {
			return null;
		} catch(InvocationTargetException e) {
			return null;
		} catch(ParseException e) {
			return null;
		}
	}
	@Override
	public void remove(String name, String key) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}
	@Override
	public void set(String name, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}
	@Override
	public void set(String name, int index, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}
	@Override
	public void set(String name, String key, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}
	@Override
	protected String getDynaClassName() {
		return "ValueWithContext";
	}
	@Override
	protected Object getDynaProperty(String name) {
		return get(name);
	}
	@Override
	protected boolean hasDynaProperty(String name) {
		if(name == null || name.trim().length() == 0) return true;
		try {
			if((name=name.trim()).charAt(0) == '.')
				return ReflectionUtils.hasProperty(value, name.substring(1));
			return ReflectionUtils.hasProperty(context, name);
		} catch(IntrospectionException e) {
			return false;
		} catch(IllegalAccessException e) {
			return false;
		} catch(InvocationTargetException e) {
			return false;
		} catch(ParseException e) {
			return false;
		}
	}
	@Override
	protected void setDynaProperty(String name, Object value) {
		throw new UnsupportedOperationException("This dynabean is not modifiable");
	}
	@Override
	protected DynaProperty[] getDynaClassProperties() {
		if(dynaClassProperties == null) {
			DynaProperty[] dps = new DynaProperty[1 + context.size()];
			dps[0] = new DynaProperty("");
			int i = 1;
			for(String name : context.keySet())
				dps[i++] = new DynaProperty(name);
			dynaClassProperties = dps;
		}
		return dynaClassProperties;
	}
	@Override
	public String toString() {
		return value == null ? "" : value.toString();
	}
	public <Target> Target convert(Class<Target> toClass) throws ConvertException {
		return ConvertUtils.convert(toClass, value);
	}
	@Override
	public int hashCode() {
		return value == null ? 0 : value.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return ConvertUtils.areEqual(value, stripValue(obj));
	}
	protected Object stripValue(Object value) {
    	if(value instanceof Convertible)
			try {
				return ((Convertible)value).convert(Object.class);
			} catch(ConvertException e) {
				//ignore
			}
    	Object tmp;
    	if(value instanceof DynaBean && (tmp=((DynaBean)value).get(null)) != null)
    		return tmp;
    	return value;
    }
}