/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.lang.SystemUtils;
import simple.text.RegexUtils;

public class StandardOutputType implements OutputType, Cloneable {
    protected String protocol;
    protected String path;
    protected String contentType;
    protected String label;
    protected String category;
    protected int outputTypeId;
    private static final int MAX_FILE_NAME_LENGTH=256;

    public StandardOutputType() {
    }
    public StandardOutputType clone() {
    	try {
			return (StandardOutputType)super.clone();
		} catch(CloneNotSupportedException e) {
			throw new UndeclaredThrowableException(e);
		}
    }
    public String getContentType() {
        return contentType;
    }
    public String getLabel() {
        return label;
    }
    public int getOutputTypeId() {
        return outputTypeId;
    }
    public String getPath() {
        return path;
    }
    public String getProtocol() {
        return protocol;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public void setOutputTypeId(int outputTypeId) {
        this.outputTypeId = outputTypeId;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    public void setOutputKey(String key) {
        int index = key.indexOf(':');
        if(index < 0) index = key.length();
        setProtocol(key.substring(0, index));
        if(index+1 < key.length()) setPath(key.substring(index+1));
    }

    public String generateFileName(Folio folio, Map<String,?> requestParamValues) {
		return fixFileName(appendFileExtention(generateFullTitle(new StringBuilder(), folio, requestParamValues), getContentType())).toString();
    }

    public String generateFileName(Report report, Map<String,?> requestParamValues) {
		return fixFileName(appendFileExtention(generateFullTitle(new StringBuilder(), report, requestParamValues), getContentType())).toString();
    }

    public String generateFullTitle(Report report, Map<String,?> requestParamValues) {
    	return generateFullTitle(new StringBuilder(), report, requestParamValues).toString();
    }

    public String generateFullTitle(Folio folio, Map<String,?> requestParamValues) {
    	return generateFullTitle(new StringBuilder(), folio, requestParamValues).toString();
    }

    protected StringBuilder generateFullTitle(StringBuilder appendTo, Report report, Map<String,?> requestParamValues) {
    	String title = report.getTitle(requestParamValues);
    	String subtitle = report.getSubtitle(requestParamValues);
    	if(title != null && (title=title.trim()).length() > 0) {
    		return generateFullTitle(appendTo, title, subtitle);
    	} else if(subtitle != null && (subtitle=subtitle.trim()).length() > 0) {
    		return generateFullTitle(appendTo, subtitle, null);
    	} else if(report.getFolios() != null && report.getFolios().length > 0) {
    		return generateFullTitle(appendTo, report.getFolios()[0], requestParamValues);
    	} else {
    		return generateFullTitle(appendTo, "Unnamed Report", "");
    	}
    }
    protected StringBuilder generateFullTitle(StringBuilder appendTo, Folio folio, Map<String,?> requestParamValues) {
    	return generateFullTitle(appendTo, folio.getTitle(requestParamValues), folio.getSubtitle(requestParamValues));
    }
    protected StringBuilder generateFullTitle(StringBuilder appendTo, String title, String subtitle) {
    	if(title != null && (title=title.trim()).length() > 0) {
        	appendTo.append(title);
        }
        if(subtitle != null && (subtitle=subtitle.trim()).length() > 0) {
        	if(appendTo.length() > 0)
        		appendTo.append(" - ");
        	appendTo.append(subtitle);
        }
        return appendTo;
    }

	public static String fixFileName(String filename) {
		return fixFileName(new StringBuilder(filename)).toString();
	}
	public static StringBuilder fixFileName(StringBuilder filename) {
		for(int i = 0; i < filename.length(); i++) {
			switch(filename.charAt(i)) {
				case '\'':
				case '"':
				case '&':
				case '!':
				case '^':
				case ';':
				case ',':
				case ':':
					filename.deleteCharAt(i);
					i--;
					break;
				case '\\':
				case '/':
				case '=':
					filename.setCharAt(i, '-');
					break;
			}
		}
		return filename;
	}
	protected static final Map<String, String> contentTypeToExt = new HashMap<String, String>();
	static {
		contentTypeToExt.put("text/plain", "txt");
		contentTypeToExt.put("application/msword", "doc");
		contentTypeToExt.put("application/vnd.ms-excel", "xls");
		contentTypeToExt.put("application/vnd.ms-powerpoint", "ppt");
		contentTypeToExt.put("multipart/related; type=\"text/html\"", "mhtml");
		contentTypeToExt.put("application/xhtml+xml", "html");
	}

	public static StringBuilder appendFileExtention(StringBuilder appendTo, String contentType) {
		if(contentType == null)
			return appendTo;
		String ext = contentTypeToExt.get(contentType);
		if(ext == null) {
			String[] ct = RegexUtils.match("([^/]+)[/]([^;]+)(?:[;](.+))?", contentType, null);
			if(ct.length < 3)
				return appendTo; // unexpected
			ext = ct[2];
		}
		if(appendTo.length() + ext.length() + 1 > MAX_FILE_NAME_LENGTH) {
			appendTo.setLength(MAX_FILE_NAME_LENGTH - ext.length() - 1);
		}
		appendTo.append('.').append(ext);
	    return appendTo;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
		return SystemUtils.addHashCodes(0, protocol, path, contentType);
    }
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
    	if(this == obj) return true;
    	if(!(obj instanceof StandardOutputType)) return false;
    	StandardOutputType o = (StandardOutputType) obj;
    	return ConvertUtils.areEqual(protocol, o.protocol)
    		&& ConvertUtils.areEqual(path, o.path)
    		&& ConvertUtils.areEqual(contentType, o.contentType);
    }
}