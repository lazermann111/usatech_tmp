package simple.falcon.engine.standard;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import simple.app.DialectResolver;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.CacheableCall;
import simple.db.Column;
import simple.db.Cursor;
import simple.db.DataLayerException;
import simple.db.DataSourceNotFoundException;
import simple.db.Parameter;
import simple.db.specific.DbSpecific;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Filter;
import simple.falcon.engine.FilterGroup;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Param;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.ResultsValueRetriever;
import simple.falcon.engine.Version;
import simple.io.IOUtils;
import simple.results.AbstractResults;
import simple.results.BeanException;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.results.ResultsFilter;
import simple.sql.Aliaser;
import simple.sql.BaseMutableExpression;
import simple.sql.BuildSQLException;
import simple.sql.Expression;
import simple.sql.ExtendedExpression;
import simple.sql.InitialsAliaser;
import simple.sql.Join;
import simple.sql.JoinStrand;
import simple.sql.SQLBuilder;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.sql.Table;

public class StandardDesignEngine2 extends simple.falcon.engine.standard.StandardDesignEngine {
	private static final simple.io.Log log = simple.io.Log.getLog();
    protected Map<Field,Aggregate> defaultAggs = new HashMap<Field, Aggregate>();
    public static final String ENGINE_VERSION_DIRECTIVE = "engine-version";
    protected static final Version ENGINE_VERSION = new Version("2.0");
	protected boolean fullJoinSyntax = true;
	protected long logExecuteTimeThreshhold;

	public StandardDesignEngine2() {
		super();
	}

	public class SingleColumnRetriever2 extends SingleColumnRetriever {
		public SingleColumnRetriever2(String column, Aggregate aggregate) {
			super(column, aggregate);
		}
		@Override
		protected Object getAggregate(Results results, ResultsFilter filter, int columnName, Aggregate aggregate, int groupColumnIndex) {
			return transformAggregateValue(super.getAggregate(results, filter, columnName, getRetrieverAggregate(aggregate), groupColumnIndex), aggregate);
		}
		@Override
		protected void trackAggregate(Results results, ResultsFilter filter, int columnName, Aggregate aggregate) {
			super.trackAggregate(results, filter, columnName, getRetrieverAggregate(aggregate));
		}
		@Override
		protected String addGroup(Results results, String columnName, Aggregate aggregate) {
			return super.addGroup(results, columnName, getRetrieverAggregate(aggregate));
		}
	}
	public class MultiColumnRetriever2 extends MultiColumnRetriever {
		public MultiColumnRetriever2(String[] columns, Aggregate[] aggregates) {
			super(columns, aggregates);
		}
		@Override
		protected Object getAggregate(Results results, ResultsFilter filter, int columnName, Aggregate aggregate, int groupColumnIndex) {
			return transformAggregateValue(super.getAggregate(results, filter, columnName, getRetrieverAggregate(aggregate), groupColumnIndex), aggregate);
		}
		@Override
		protected void trackAggregate(Results results, ResultsFilter filter, int columnName, Aggregate aggregate) {
			super.trackAggregate(results, filter, columnName, getRetrieverAggregate(aggregate));
		}
		@Override
		protected String addGroup(Results results, String columnName, Aggregate aggregate) {
			return super.addGroup(results, columnName, getRetrieverAggregate(aggregate));
		}
	}

    public class InnerFolio2 extends InnerFolio {
		public InnerFolio2() {
			super();
		}
		@Override
		public void setFilter(Filter filter) {
			fixFilterAggregates(filter);
			super.setFilter(filter);
		}
		protected void fixFilterAggregates(Filter filter) {
			if(filter instanceof FilterGroup) {
				Set<Filter> children = ((FilterGroup)filter).getChildFilters();
				if(children != null)
					for(Filter f : children)
						fixFilterAggregates(f);
			} else if(filter instanceof FilterItem) {
				FilterItem fi = (FilterItem)filter;
				if(fi.getAggregateType() == null)
					fi.setAggregateType(defaultAggs.get(fi.getField()));
			}
		}
		@Override
		protected InnerStandardPillar createPillar() {
			return new InnerStandardPillar2();
		}
		protected class InnerStandardPillar2 extends InnerStandardPillar {
			public InnerStandardPillar2() {
				super();
			}
			@Override
			protected ResultsValueRetriever createValueRetriever(String[] columns, Aggregate[] aggregates) {
			   if(columns.length == 1) {
		           return new SingleColumnRetriever2(columns[0], aggregates[0]);
		       }
			   return new MultiColumnRetriever2(columns, aggregates);
		   }
	    }
    }
    @Override
    protected InnerFolio createFolio(long folioId) throws DesignException {
    	InnerFolio folio = super.createFolio(folioId);
    	convertToVersion2(folio);
    	folio.setUpdated(false);
		return folio;
    }
    protected void convertToVersion2(InnerFolio folio) {
    	String versionText = folio.getDirective(ENGINE_VERSION_DIRECTIVE);
    	if(versionText == null || versionText.trim().length() == 0
    			|| new Version(versionText).compareTo(ENGINE_VERSION) < 0) {
			Pillar[] pillars = folio.getPillars();
			if(pillars != null)
				for(Pillar p : pillars) {
					boolean useGiven = true;
					int groupingLevel = p.getGroupingLevel();
					switch(groupingLevel) {
						case 10: //xtab
							groupingLevel = 1;
							break;
						case 0: case 1: case 2: case 3: case 4: case 5:
							groupingLevel += 2;
							useGiven = false;
							break;
						case -1: // totals
							groupingLevel = 0;
							break;
						case -2: case -3: case -4: case -5: case -6: case -7:
							groupingLevel = -groupingLevel;
							break;
					}
					p.setGroupingLevel(groupingLevel);
					FieldOrder[] fos = p.getFieldOrders();
					if(fos != null)
						for(FieldOrder fo : fos) {
							Aggregate aggregateType = fo.getAggregateType();
							if(fo.getField().getAdditiveType() != Field.AdditiveType.NONE) {
								fo.setAggregateType(null);
				    		} else if(!useGiven || aggregateType == null || !aggregateAllowed(aggregateType, fo.getField())) {
				    			fo.setAggregateType(defaultAggs.get(fo.getField()));
				    		}
						}
					p.invalidate();

				}
			String noGroupTotals = folio.getDirective("no-group-totals");
			if(noGroupTotals != null && noGroupTotals.trim().length() > 0) {
				// NOTE: This is because we artifically translate the folio pillars in StandardDesignEngine2 to adjust the group levels
				try {
		        	int[] ngt = ConvertUtils.convert(int[].class, noGroupTotals);
		            if(ngt != null) {
			        	for(int i = 0; i < ngt.length; i++)
			        		if(ngt[i] > 0) ngt[i] += 2;
			        	folio.setDirective("no-group-totals", ConvertUtils.convert(String.class, ngt));
			        }
		        } catch(ConvertException e) {
		        	//do nothing
		        }
			}
			folio.setDirective(ENGINE_VERSION_DIRECTIVE, ENGINE_VERSION.toString());
		}
	}
	@Override
	public InnerFolio newFolio() {
		return new InnerFolio2();
	}

    @Override
	public Long saveFolio(Folio folio, Executor executor) throws DesignException {
		if(folio.getDirective(ENGINE_VERSION_DIRECTIVE) == null)
			folio.setDirective(ENGINE_VERSION_DIRECTIVE, ENGINE_VERSION.toString());
		return super.saveFolio(folio, executor);
	}
	@Override
	protected Field createField(Results results, Connection conn) throws BeanException, SQLException {
        Field field = new Field();
        results.fillBean(field);
        String dTmp = results.getFormattedValue("displayExp");
        String sTmp = results.getFormattedValue("sortExp");
        DatabaseMetaData metaData = conn.getMetaData();
        ExtendedExpression displayExp = sqlBuilder.createExtendedExpression(dTmp, metaData);
        ExtendedExpression sortExp = sqlBuilder.createExtendedExpression(sTmp, metaData);
        field.setDisplayExpression(displayExp);
        field.setSortExpression(sortExp);
        if(displayExp.getAggregates().isEmpty() && sortExp.getAggregates().isEmpty()) {
        	field.setAdditiveType(Field.AdditiveType.NONE);
		} else if(displayExp.getAggregates().size() <= 1 && sortExp.getAggregates().size() <= 1) {
        	Aggregate dAgg, sAgg;
        	Expression dExp, sExp;
        	if(displayExp.getAggregates().isEmpty()) {
        		dAgg = null;
        		dExp = displayExp;
        	} else {
        		dAgg = displayExp.getAggregates().iterator().next();
        		dExp = displayExp.getInnerExpressions().iterator().next();
        	}
        	if(sortExp.getAggregates().isEmpty()) {
        		sAgg = null;
        		sExp = sortExp;
        	} else {
        		sAgg = sortExp.getAggregates().iterator().next();
        		sExp = sortExp.getInnerExpressions().iterator().next();
        	}
        	if((dAgg != null && !startsWith(displayExp, dAgg))
        			|| (sAgg != null && !startsWith(sortExp, dAgg))) {
        		field.setAdditiveType(Field.AdditiveType.FULL);
        	} else {
        		if(dAgg != null) {
        			defaultAggs.put(field, dAgg);
        		} else if(sAgg != null) {
        			defaultAggs.put(field, sAgg);
        		}
        		field.setAdditiveType(Field.AdditiveType.NONE);
        		//recalc sql types of field
        		SQLType[] types;
				try {
					types = validateFieldExpressions(dExp, sExp);
        		} catch(BuildSQLException e) {
        			throw new SQLException("While getting new sql types for expressions : " + e.getMessage());
				} catch(DataSourceNotFoundException e) {
					throw new SQLException("While getting new sql types for expressions : " + e.getMessage());
				} catch(DesignException e) {
					throw new SQLException("While getting new sql types for expressions : " + e.getMessage());
				}
        		log.info("Changed field #" + field.getId() + " to expressions '" + dExp + "', '" + sExp + "' and sql types " + types[0] + ", " + types[1]);
        		field.setDisplaySqlType(types[0]);
    			field.setSortSqlType(types[1]);
    			field.setDisplayExpression(dExp);
    	        field.setSortExpression(sExp);

        	}
        } else {
    		field.setAdditiveType(Field.AdditiveType.FULL);
        }
        fieldCache.put(field.getId(), field);
        return field;
    }
    protected boolean startsWith(Expression expression, Aggregate aggregate) {
    	String s = expression.toString().trim();
    	//remove starting paren
    	int functionStart = 0;
    	while(s.charAt(functionStart) == '(') functionStart++;
    	int beginParen = s.indexOf('(', functionStart);
    	if(beginParen < 0) {
    		return false;
    	}
		int functionEnd = beginParen;
		while(Character.isWhitespace(s.charAt(functionEnd - 1)))
			functionEnd--;
		return (sqlBuilder.getAggregateParser().isAggregateFunction(s, functionStart, functionEnd, beginParen, null) != null);
    }

	protected boolean aggregateAllowed(Aggregate aggregate, Field field) {
		switch(aggregate) {
			case AVG:
			case SUM:
				Class<? extends Object> cls1 = SQLTypeUtils.getJavaType(field.getDisplaySqlType().getTypeCode());
				Class<? extends Object> cls2 = SQLTypeUtils.getJavaType(field.getSortSqlType().getTypeCode());
				return (Number.class.isAssignableFrom(cls1) && Number.class.isAssignableFrom(cls2));
			case ARRAY:
			case COUNT:
			case DISTINCT:
			case FIRST:
			case LAST:
			case MAX:
			case MIN:
			case SET:
			default:
				return true;
		}
	}

	// These three methods determine how to process each aggregate at three various stages of the report-production process
	/*
     * Agg Type		Column Agg			Expression  								Retrieve Agg
     * -----------	-----------------	-----------------							-----------
     * AVG			ARRAY				CAST(COLLECT(?) AS <array-type>)			avg() of ARRAY
     * ARRAY		ARRAY				CAST(COLLECT(?) AS <array-type>)			ARRAY
     * COUNT		COUNT				COUNT(?)									SUM
     * DISTINCT		SET					CAST(COLLECT(DISTINCT ?) AS <array-type>)	size() of SET
     * FIRST		ARRAY				CAST(COLLECT(?) AS <array-type>)			first() of ARRAY
     * LAST 		ARRAY				CAST(COLLECT(?) AS <array-type>)			last() of ARRAY
     * MAX			MAX					MAX(?)										MAX
     * MIN			MIN					MIN(?)										MIN
     * SET			SET					CAST(COLLECT(DISTINCT ?) AS <array-type>)	SET
     * SUM			SUM					SUM(?)										SUM
     */

    @Override
    public Aggregate getColumnAggregate(Aggregate aggregate) {
		if(aggregate == null)
			return null;
		switch(aggregate) {
			case AVG:
    		case ARRAY:
    		case FIRST:
    		case LAST:
	    			return Aggregate.ARRAY;
    		case DISTINCT:
    		case SET:
    			return Aggregate.SET;
    		default:
    			return aggregate;
    	}
    }

    public Aggregate getRetrieverAggregate(Aggregate aggregate) {
		if(aggregate == null)
			return null;
		switch(aggregate) {
			case AVG:
    		case ARRAY:
    		case FIRST:
    		case LAST:
	    			return Aggregate.ARRAY;
    		case DISTINCT:
    		case SET:
    			return Aggregate.SET;
    		case COUNT:
    			return Aggregate.SUM;
    		default:
    			return aggregate;
    	}
    }
	protected Object transformAggregateValue(Object value, Aggregate aggregate) {
		if(aggregate == null || value == null)
			return value;
		switch(aggregate) {
			case AVG:
				try {
					Collection<?> coll = ConvertUtils.asCollection(value);
					BigDecimal sum = null;
			        int count = 0;
			        for(Object v : coll) {
				        if(v instanceof Number) {
			                sum = AbstractResults.calcSum(sum, (Number)v);
			                count++;
				        }
			        }
			        if(sum == null) return null;
			        return sum.divide(new BigDecimal("" + count), sum.scale() + 4, BigDecimal.ROUND_HALF_UP);
				} catch(ConvertException e) {
					throw new IllegalArgumentException("Value " + value + " could not be converted to a collection", e);
				}
			case DISTINCT:
				try {
					return ConvertUtils.asCollection(value).size();
				} catch(ConvertException e) {
					throw new IllegalArgumentException("Value " + value + " could not be converted to a collection", e);
				}
			case FIRST:
				try {
					Collection<?> coll = ConvertUtils.asCollection(value);
					if(coll.size() > 0) return coll.iterator().next();
				} catch(ConvertException e) {
					throw new IllegalArgumentException("Value " + value + " could not be converted to a collection", e);
				}
			case LAST:
				try {
					Collection<?> coll = ConvertUtils.asCollection(value);
					if(coll.size() > 0) {
						if(coll instanceof List<?>) return ((List<?>)coll).get(coll.size()-1);
						Iterator<?> iter = coll.iterator();
						Object last = null;
						while(iter.hasNext())
							last = iter.next();
						return last;
					}
				} catch(ConvertException e) {
					throw new IllegalArgumentException("Value " + value + " could not be converted to a collection", e);
				}
    		default:
				return value;
		}
	}
     /**
     * @param agg
     * @return false if an agg function was used and no group by is needed, otherwise true
     */
    protected boolean needsGroupBy(Aggregate agg) {
    	if(agg == null)
    		return true;
    	switch(agg) {
    		case ARRAY:
    		case DISTINCT:
    		case FIRST:
    		case LAST:
    		case SET:
        		// handle these when outputting values (in xml or csv)
    			return true;
    		default:
    			return false;
    	}
    }

    protected int getAggregateTypeCode(Aggregate aggregate, SQLType innerType) {
    	if(aggregate == null)
    		return innerType.getTypeCode();
    	switch(aggregate) {
    		case ARRAY:
    		case SET:
        			return Types.ARRAY;
    		case AVG:
    			return Types.DECIMAL;
    		case COUNT:
    		case DISTINCT:
    			return Types.INTEGER;
    		case FIRST:
    		case LAST:
    		case MAX:
    		case MIN:
    		case SUM:
    		default:
    			return innerType.getTypeCode();
    	}
    }

	public String getSortColumnName(FieldOrder fieldOrder) {
		if(fieldOrder.getSortColumnName()!=null){
			return fieldOrder.getSortColumnName();
		}else if(fieldOrder.getField()!=null){
			if(fieldOrder.getField().getDisplayExpression().equals(fieldOrder.getField().getSortExpression()))
				return getDisplayColumnName(fieldOrder);
			return appendUID(fieldOrder, new StringBuilder("S_")).toString();
		}else{
			return null;
		}
	}
    /** Creates a call for the specified list of FieldOrders and Filter.
     *  FieldOrders <strong>MUST</strong> be in correct order of sorting.
     *  (For sql's SORT BY clause)
     * @throws BuildSQLException
     *
     */
    @Override
	protected CacheableCall createCall(Long folioId, String effectiveUserGroupIds,Collection<FieldOrder> fieldOrderList, FilterGroup filter, DatabaseMetaData metaData, String hint) throws SQLException, BuildSQLException {
        log.debug("Creating a call");
        log.debug("Processing " + fieldOrderList.size() + " field(s).");
        final SQLBuilder sqlBuilder = this.sqlBuilder; // this protects us from the changes in reloadFields()
        final List<Argument> params = new ArrayList<Argument>();
        final Cursor cur = new Cursor();
        final List<Column> cols = new ArrayList<Column>(fieldOrderList.size()*2);
        final Map<Table,String> tableAliases = new TreeMap<Table,String>(sqlBuilder.getTableComparator());
		final Map<Table, Set<Column>> colsByTable = new TreeMap<Table, Set<Column>>(sqlBuilder.getTableComparator());
        final SelectStatement select = new SelectStatement();
        if(hint != null)
        	select.setHint(hint);
        final DbSpecific dbSpecific = getDbSpecific(metaData);
        params.add(cur);

        //1. Process each field order
		Aliaser aliaser = new InitialsAliaser(tableAliases);
        int lastIndex = 0;
        String exp;
		final List<Argument> groupByParams = new ArrayList<Argument>();
        for(FieldOrder fo : fieldOrderList) {
        	String displayCN = columnNamer.getDisplayColumnName(fo);
			String sortCN = columnNamer.getSortColumnName(fo);
			Column col = new Column();
            col.setIndex(++lastIndex);
            col.setPropertyName(displayCN);
            cols.add(col);

            String displayExp;
			String sortExp;
			Aggregate columnAgg = getColumnAggregate(fo.getAggregateType());
			boolean needsDcp;
			if(columnAgg != null)
				switch(columnAgg) {
					case COUNT:
					case SUM:
						needsDcp = true;
						break;
					default:
						needsDcp = false;
				}
			else
				needsDcp = false;
			Table lastTable = null;
			for(simple.sql.Column column : fo.getField().getDisplayExpression().getColumns()) {
				if(!tableAliases.containsKey(column.getTable())) {
					String alias = aliaser.getAlias(column.getTable());
					tableAliases.put(column.getTable(), alias);
				}
				if(needsDcp) {
					if(lastTable == null)
						lastTable = column.getTable();
					else if(!lastTable.equals(column.getTable())) {
						log.warn("Field " + fo.getField().getId() + " may need double-count protection but references more than one table");
						needsDcp = false;
						colsByTable.get(lastTable).remove(col);
						continue;
					}
					Set<Column> colsForTable = colsByTable.get(lastTable);
					if(colsForTable == null) {
						colsForTable = new LinkedHashSet<Column>();
						colsByTable.put(column.getTable(), colsForTable);
					}
					colsForTable.add(col);
				}
			}
			displayExp = dbSpecific.createAggregateExpression(fo.getField().getDisplayExpression(), columnAgg, fo.getField().getDisplaySqlType()).format(aliaser);
			select.addColumn(displayExp, displayCN);
			if(fo.getField().getDisplayExpression().getParameters() != null)
				params.addAll(fo.getField().getDisplayExpression().getParameters());

			if(!ConvertUtils.areEqual(displayCN, sortCN)) {
				col = new Column();
				col.setIndex(++lastIndex);
				col.setPropertyName(sortCN);
				cols.add(col);

				lastTable = null;
				for(simple.sql.Column column : fo.getField().getSortExpression().getColumns()) {
					if(!tableAliases.containsKey(column.getTable())) {
						String alias = aliaser.getAlias(column.getTable());
						tableAliases.put(column.getTable(), alias);
					}
					if(needsDcp) {
						if(lastTable == null)
							lastTable = column.getTable();
						else if(!lastTable.equals(column.getTable())) {
							log.warn("Field " + fo.getField().getId() + " may need double-count protection but references more than one table");
							needsDcp = false;
							colsByTable.get(lastTable).remove(col);
							continue;
						}
						Set<Column> colsForTable = colsByTable.get(lastTable);
						if(colsForTable == null) {
							colsForTable = new LinkedHashSet<Column>();
							colsByTable.put(column.getTable(), colsForTable);
						}
						colsForTable.add(col);
					}
				}
				sortExp = dbSpecific.createAggregateExpression(fo.getField().getSortExpression(), columnAgg, fo.getField().getSortSqlType()).format(aliaser);
				select.addColumn(sortExp, sortCN);
				if(fo.getField().getSortExpression().getParameters() != null)
					params.addAll(fo.getField().getSortExpression().getParameters());
	    	} else {
	    		sortExp = null;
	    	}
			if(fo.getField().getAdditiveType() == Field.AdditiveType.NONE && columnAgg == null) {
				select.addGroupBy(displayExp);
				if(fo.getField().getDisplayExpression().getParameters() != null)
					groupByParams.addAll(fo.getField().getDisplayExpression().getParameters());
				if(sortExp != null) {
					select.addGroupBy(sortExp);
					if(fo.getField().getSortExpression().getParameters() != null)
						groupByParams.addAll(fo.getField().getSortExpression().getParameters());

				}
			}

        	switch(getAggregateTypeCode(columnAgg, fo.getField().getSortSqlType())) {
        		case Types.ARRAY:
        		case Types.JAVA_OBJECT:
        		case Types.OTHER:
        		case Types.REF:
        		case Types.STRUCT:
        			break;
        		default:
                	select.addOrderBy(sortExp != null ? sortCN : displayCN, fo.getSortOrder());
        	}
        }

		// 2. Add the filters (Must format this first to get any additional tables; but must add parameters in correct order)
        if(filter != null && filter.filterCount() > 0) {
    		Filter whereFilter = filter.buildWhereFilter();
    		Filter havingFilter = filter.buildHavingFilter();
    		// add filter to sub selects
			// NOTE: parameters must be added in this order: those in the columns, then table joins, then where filter, then group by, then having filter
			exp = formatFilterExpression(whereFilter, tableAliases, aliaser, groupByParams.subList(0, 0), dbSpecific, false);
			if(exp != null)
        		select.addWhere(exp);
			exp = formatFilterExpression(havingFilter, tableAliases, aliaser, groupByParams, dbSpecific, true);
        	if(exp != null)
        		select.addHaving(exp);
		}

		// 3. Find all joins
		if(isFullJoinSyntax()) {
			// add where clause first to get any extra tables
			// final Map<String, Join> joins = sqlBuilder.getExtraJoins(tableAliases, aliaser, metaData);

			// add from clause
			JoinStrand js = sqlBuilder.getJoinWeb(tableAliases, aliaser, metaData, true);
			select.addJoinStrand(js, aliaser, params);

			// Check for double-count scenarios
			int ucnt = 1;
			for(Map.Entry<Table, Set<Column>> entry : colsByTable.entrySet()) {
				if(!js.isLowestGranularity(entry.getKey())) {
					Expression dcpExp = sqlBuilder.getUniqueExpression(entry.getKey());
					if(dcpExp == null)
						continue;
					for(simple.sql.Column col : dcpExp.getColumns()) {
						if(!tableAliases.containsKey(col.getTable()))
							throw new BuildSQLException("Double-count protection unique column '" + dcpExp + "' references a table not in this query");
					}
					// add expression to select with proper column names
					String expText = dcpExp.format(aliaser);
					String unqCN = "U_" + ucnt++;
					select.addColumn(expText, unqCN);
					select.addGroupBy(expText);
					Column col = new Column();
					col.setIndex(++lastIndex);
					col.setPropertyName(unqCN);
					cols.add(col);

					// set results appropriately
					for(Column ccol : entry.getValue())
						ccol.setUniqueColumn(col);
				}
			}

			/*
			// process params from where clause
			for(Map.Entry<String, Join> entry : joins.entrySet()) {
				exp = entry.getKey();
				Join join = entry.getValue();
				Expression joinExp = join.getExpressionObject();
				select.addWhere(joinExp.format(aliaser));
				if(joinExp.getParameters() != null)
					params.addAll(joinExp.getParameters());
			}
			*/
			params.addAll(groupByParams);
		} else {
			final Map<String, Join> joins = sqlBuilder.getJoins(tableAliases, aliaser, metaData);
			for(Map.Entry<String, Join> entry : joins.entrySet()) {
				exp = entry.getKey();
				Join join = entry.getValue();
				Expression joinExp = join.getExpressionObject();
				select.addWhere(joinExp.format(aliaser));
				if(joinExp.getParameters() != null)
					params.addAll(joinExp.getParameters());
			}
			params.addAll(groupByParams);

			// 4. Add tables
			for(Map.Entry<Table, String> entry : tableAliases.entrySet()) {
				select.addTable(entry.getKey().getSchemaName(), entry.getKey().getTableName(), entry.getValue());
			}
		}

		cur.setColumns(cols.toArray(new Column[cols.size()]));
        //cur.setGroups(grpList.toArray());
        cur.setLazyAccess(true); // this is for memory considerations
        String sql = select.getSql();
        log.info("SQL=\n" + sql);
    	CacheableCall call = new CacheableCall(resultsCache);
    	if(folioId!=null){
    		Map<String, Object> paramMap=new HashMap<String,Object>();
    		paramMap.put("folioId", folioId);
    		paramMap.put("effectiveUserGroupIds", effectiveUserGroupIds);
    		paramMap.put("generatedSql", sql);
    		try{
    			dataLayer.executeCall("UPSERT_FOLIO_SQL", paramMap, true);
    			Object overwrittenSql=paramMap.get("overwrittenSql");
    			if(overwrittenSql!=null){
    				String oSql=null;
    				if (DialectResolver.isOracle()) {
    					oSql=IOUtils.readFully(ConvertUtils.convert(Clob.class, overwrittenSql).getAsciiStream());
    				} else {
    				    oSql=(String) overwrittenSql;
    				}
    				call.setSql(oSql);
    				log.info("SQL Overwritten=\n" + oSql);
    			}else{
    				call.setSql(sql);
    			}
    		}catch(DataLayerException e){
    			throw new BuildSQLException(e);
    		}catch(ConvertException e){
    			throw new BuildSQLException(e);
    		}catch(IOException e){
    			throw new BuildSQLException(e);
    		}
    	}else{
    		call.setSql(sql);
    	}
        call.setArguments(params.toArray(new Argument[params.size()]));
        call.setMinElapsedTime(getMinElapsedToCache());
		if(getLogExecuteTimeThreshhold() >= 0)
			call.setLogExecuteTimeThreshhold(getLogExecuteTimeThreshhold());
        //call.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE); //needed to allow ResultSetScrollableResults
        return call;
    }

	protected String formatFilterExpression(Filter filter, Map<Table, String> tableAliases, Aliaser aliaser, List<Argument> arguments, DbSpecific dbSpecific, boolean having) {
    	if(filter == null) return null;
    	if(filter instanceof FilterGroup && ((FilterGroup)filter).filterCount() == 0) return null;

		BaseMutableExpression be = new BaseMutableExpression();
		filter.buildExpression(be, dbSpecific);
		String exp = be.format(aliaser);
        Param[] params = filter.gatherParams();
        for(int k = 0; k < params.length; k++) {
			Parameter a = new FilterGroupAwareParameter(having, k);
            SQLType sqlType = dbSpecific.getNormalizedType(params[k].getSqlType());
            a.setSqlType(sqlType);
            a.setPropertyName(params[k].getName());
            a.setDefaultValue(params[k].getValue());
            a.setRequired(false);
            arguments.add(a);
        }
        return exp;
    }

	public boolean isFullJoinSyntax() {
		return fullJoinSyntax;
	}

	public void setFullJoinSyntax(boolean fullJoinSyntax) {
		boolean old = this.fullJoinSyntax;
		this.fullJoinSyntax = fullJoinSyntax;
		if(old != fullJoinSyntax)
			clearCalls();
	}

	public long getLogExecuteTimeThreshhold() {
		return logExecuteTimeThreshhold;
	}

	public void setLogExecuteTimeThreshhold(long logExecuteTimeThreshhold) {
		this.logExecuteTimeThreshhold = logExecuteTimeThreshhold;
	}

	/*
	protected SQLType[] describeQuery(Connection conn, String sql) throws SQLException {
		return getDbSpecific(conn.getMetaData()).describeQuery(conn, sql);
	}*/
}
