/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.io.LoadingException;
import simple.results.Results;
import simple.xml.TemplatesLoader;
import simple.xml.serializer.XHTMLHandler;

public class XSLGenerator extends XMLBaseGenerator {
    protected int refreshInterval = 15000;

    public XSLGenerator(ExecuteEngine engine) {
        this(engine, InputSourceType.REPORT);
    }

    public XSLGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
        super(engine, inputSourceType);
    }

    public void generate(Report report, OutputType outputType, Map<String,Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException {
        // generate the report
        try {
            Transformer t = TemplatesLoader.getInstance(scene.getTranslator()).newTransformer(engine.getResourceResolver().getResourceURL(outputType.getPath()), refreshInterval);
            t.setParameter("context", scene);
            String token = ConvertUtils.getStringSafely(scene.getProperties().get("session-token"));
        	if(token != null)
        		t.setOutputProperty(XHTMLHandler.SESSION_TOKEN, token);
            Result target = createOutputTarget(outputType, parameters, scene, out);
            t.transform(createXMLSource(report, results, parameters, executor, scene, outputType), target);
        } catch (TransformerConfigurationException e) {
            throw new ExecuteException("at " + getXSLLocation(e), e);
        } catch (IOException e) {
            throw new ExecuteException(e);
        } catch (LoadingException e) {
            throw new ExecuteException(e);
        } catch (TransformerException e) {
            throw new ExecuteException("at " + getXSLLocation(e), e);
		} catch(DesignException e) {
			throw new ExecuteException(e);
		}
    }

    protected String getXSLLocation(Throwable e) {
        if(e instanceof TransformerException) {
            TransformerException te = (TransformerException)e;
            if(te.getLocator() != null)
                return te.getLocationAsString();
        } else if(e instanceof SAXException) {
            Throwable cause = ((SAXException)e).getException();
            if(cause != null) {
            	//we will init the cause on the exception as well for ease of reporting/debugging
            	if(e.getCause() == null)
            		e.initCause(cause);
                return getXSLLocation(cause);
            }
        }
        Throwable cause = e.getCause();
        if(cause != null)
            return getXSLLocation(cause);
        return "unknown";
    }

	/**
	 * @throws ExecuteException
	 */
	protected Result createOutputTarget(OutputType outputType, Map<String, Object> parameters, Scene scene, Output out) throws IOException, ExecuteException {
        return getStreamResult(outputType, out);
    }

    protected Result getStreamResult(OutputType outputType, Output out) throws IOException {
        return out.getResult();
    }

	public int getRefreshInterval() {
		return refreshInterval;
	}

	public void setRefreshInterval(int refreshInterval) {
		this.refreshInterval = refreshInterval;
	}
}