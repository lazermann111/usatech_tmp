package simple.falcon.engine.standard;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.IOUtils;
import simple.io.Log;
import simple.io.ResourceResolver;

public class DirectoryResourceResolver implements ResourceResolver {
	private static final Log log = Log.getLog();
	protected static Pattern PATH_STRIPPER = Pattern.compile("(\\w+:)?([^?]+)([?].*)?");
	protected File baseDirectory = new File(".");
	protected String prefixMatcher;
	protected ResourceResolver fallback;
	
	public DirectoryResourceResolver() {
	}
	
	public DirectoryResourceResolver(ResourceResolver fallback) {
		this();
		this.fallback = fallback;
	}
	
	public URL getResourceURL(String path) throws IOException {
		String prefixMatcher = getPrefixMatcher();
		String relativePath;
		if(prefixMatcher != null && path.startsWith(prefixMatcher)) {
			relativePath = path.substring(prefixMatcher.length());
		} else {
			relativePath = path;
		}
		Matcher matcher = PATH_STRIPPER.matcher(relativePath);
		if(matcher.matches())
			relativePath = matcher.group(2);
		File baseDirectory = getBaseDirectory();
		File file = new File(baseDirectory, relativePath);
		if(file.canRead())
			return file.toURI().toURL();
		ResourceResolver fallback = getFallback();
		if(fallback == null) {
			log.warn("Could not find resource '" + relativePath + "' in directory '" + IOUtils.safeCanonicalPath(baseDirectory) + "'");
			return null;
		}
		log.info("Could not find resource '" + relativePath + "' in directory '" + IOUtils.safeCanonicalPath(baseDirectory) + "' falling back to " + fallback);
		return fallback.getResourceURL(path);
	}

	public File getBaseDirectory() {
		return baseDirectory;
	}

	public void setBaseDirectory(File baseDirectory) {
		if(baseDirectory == null)
			baseDirectory = new File(".");
		this.baseDirectory = baseDirectory;
	}

	public String getPrefixMatcher() {
		return prefixMatcher;
	}

	public void setPrefixMatcher(String prefixMatcher) {
		this.prefixMatcher = prefixMatcher;
	}

	public ResourceResolver getFallback() {
		return fallback;
	}

	public void setFallback(ResourceResolver fallback) {
		this.fallback = fallback;
	}
}
