package simple.falcon.engine.standard;

import java.util.List;

import simple.db.Parameter;
import simple.falcon.engine.SortOrder;
import simple.sql.Aliaser;
import simple.sql.JoinStrand;
import simple.sql.JoinType;
import simple.sql.Table;
import simple.text.PositionedStringBuilder;

public class SelectStatement {
	protected String newline = StandardDesignEngine.NEWLINE;
	protected String indent = StandardDesignEngine.INDENT;
	protected final StringBuilder columns = new StringBuilder();
	protected final StringBuilder from = new StringBuilder();
	protected final StringBuilder where = new StringBuilder();
	protected final StringBuilder groupBy = new StringBuilder();
	protected final StringBuilder having = new StringBuilder();
	protected final StringBuilder orderBy = new StringBuilder();
	protected String hint;

	public void setHint(String hint) {
		this.hint = hint;
	}
	public void addColumn(String expression) {
		columns.append(newline).append(indent).append(expression).append(',');
	}
	public void addColumn(String expression, String alias) {
		columns.append(newline).append(indent).append(expression).append(' ').append(alias).append(',');
	}
	public void addTable(String schema, String table, String alias) {
		if(from.length() > 0)
			from.append(',');
		from.append(newline).append(indent);
		if(schema != null && schema.trim().length() > 0)
			from.append(schema).append('.');
		from.append(table);
		if(alias != null && alias.trim().length() > 0)
			from.append(' ').append(alias);
	}

	public void addJoinStrand(JoinStrand js, Aliaser aliaser, List<? super Parameter> params) {
		if(from.length() > 0)
			from.append(',');
		PositionedStringBuilder psb = new PositionedStringBuilder(from, 0, 0);
		if(js.getTable() == null) {
			if(js.getCount() > 1)
				throw new IllegalArgumentException("And extra join clause can only have one child");
			addWhere(js.getExpression().format(aliaser));
			js = js.getFirst();
		}

		appendTable(js.getTable(), psb, aliaser);
		appendJoinStrand(js, psb, aliaser, params);
	}

	protected void appendJoinStrand(JoinStrand js, PositionedStringBuilder psb, Aliaser aliaser, List<? super Parameter> params) {
		OUTER: for(JoinStrand sub : js.getStrands()) {
			if(sub.getExpression() != null) {
				List<Parameter> joinParams = sub.getExpression().getParameters();
				if(joinParams != null)
					params.addAll(joinParams);
			}
			if(sub.getTable() == null) { // it's an extra join
				psb.append(" AND ").append(sub.getExpression().format(aliaser));
			} else {
				PositionedStringBuilder subPsb;
				JoinType type = sub.getType();
				if(type == null)
					type = JoinType.CROSS;
				switch(type) {
					case INNER:
						psb.append(newline).append(indent).append("JOIN ");
						appendTable(sub.getTable(), psb, aliaser);
						psb.append(" ON ").append(sub.getExpression().format(aliaser));
						subPsb = psb;
						break;
					case LEFT:
					case RIGHT:
					case FULL:
						psb.append(newline).append(indent).append(sub.getType().toString()).append(" OUTER JOIN ");
						if(sub.getCount() == 0) {
							appendTable(sub.getTable(), psb, aliaser);
							psb.append(" ON ").append(sub.getExpression().format(aliaser));
							continue OUTER;
						}
						psb.append('(');
						subPsb = psb.sub();
						appendTable(sub.getTable(), subPsb, aliaser);
						psb.append(") ON ").append(sub.getExpression().format(aliaser));
						break;
					case CROSS:
						psb.append(newline).append(indent).append("CROSS JOIN ");
						if(sub.getCount() == 0) {
							appendTable(sub.getTable(), psb, aliaser);
							continue OUTER;
						}
						psb.append('(');
						subPsb = psb.sub();
						appendTable(sub.getTable(), subPsb, aliaser);
						psb.append(')');
						break;
					default:
						throw new IllegalArgumentException("JoinType " + sub.getType() + " is not supported");
				}
				appendJoinStrand(sub, subPsb, aliaser, params);
			}
		}
	}
	protected void appendTable(Table table, PositionedStringBuilder psb, Aliaser aliaser) {
		String schema = table.getSchemaName();
		if(schema != null && schema.trim().length() > 0)
			psb.append(schema).append('.');
		psb.append(table.getTableName());
		String alias = aliaser.getAlias(table);
		if(alias != null && alias.trim().length() > 0)
			psb.append(' ').append(alias);
	}
	public void addWhere(String expression) {
		if(where.length() == 0)
			where.append("WHERE ");
		else
			where.append("  AND ");
		where.append(expression).append(newline);
	}
	public void addGroupBy(String expression) {
		groupBy.append(newline).append(indent).append(expression).append(',');
	}
	public void addHaving(String expression) {
		if(having.length() == 0)
			having.append("HAVING ");
		else
			having.append("   AND ");
		having.append(expression).append(newline);
	}
	public void addOrderBy(String expression, SortOrder sortOrder) {
		switch(sortOrder) {
			case ASC:
				orderBy.append(newline).append(indent).append(expression).append(" ASC,");
				break;
			case DESC:
				orderBy.append(newline).append(indent).append(expression).append(" DESC,");
				break;
		}
	}
	public String getSql() {
		StringBuilder sql = new StringBuilder();
		getSql(sql);
		return sql.toString();
	}
	public void getSql(StringBuilder sql) {
		sql.append("SELECT ");
		if(hint != null && (hint=hint.trim()).length() > 0) {
			sql.append("/*+ ").append(hint.replace("*/", "* /")).append(" */");
		}
		sql.append(columns);
		sql.setLength(sql.length()-1);
		sql.append(newline);
		sql.append("FROM ");
		sql.append(from);
		sql.append(newline);
		if(where.length() > 0) {
			sql.append(where);
		}
		if(groupBy.length() > 0) {
			sql.append("GROUP BY ").append(groupBy);
			sql.setLength(sql.length()-1);
			sql.append(newline);
    		}
		if(having.length() > 0) {
    		sql.append(having);
		}
		if(orderBy.length() > 0) {
			sql.append("ORDER BY ").append(orderBy);
			sql.setLength(sql.length()-1);
			sql.append(newline);
		}
	}
}