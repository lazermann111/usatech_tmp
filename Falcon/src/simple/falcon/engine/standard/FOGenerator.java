/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine.standard;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.TransformerHandler;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Scene;
import simple.falcon.engine.css.CssFilter;
import simple.falcon.engine.css.CssManager;
import simple.falcon.engine.css.FOPAttributeFixer;
import simple.io.LoadingException;
import simple.io.Log;
import simple.xml.TemplatesLoader;

public class FOGenerator extends XSLGenerator {
    private static final Log log = Log.getLog();
    protected final CssManager cssManager = new CssManager(true);
    
    public FOGenerator(ExecuteEngine engine) {
        super(engine);
    }
    
    public FOGenerator(ExecuteEngine engine, InputSourceType inputSourceType) {
        super(engine, inputSourceType);
    }
    @Override
    protected Result createOutputTarget(OutputType outputType, Map<String,Object> parameters, Scene scene, Output out) throws IOException, ExecuteException {
        // chain together the report building xsl and the style XSL from the css
        try {
            Result result = getStreamResult(outputType, out);
            List<URL> urls = new ArrayList<URL>();
            for(int i = 0; i < scene.getStylesheets().length; i++) {//must be forwards (last one wins)
                String cssPath = scene.getStylesheets()[i];
                if(cssPath != null && cssPath.trim().length() > 0) {
					if(log.isDebugEnabled())
						log.debug("Finding url for '" + engine.getCssDirectory() + cssPath + "'");
                    urls.add(engine.getResourceResolver().getResourceURL(engine.getCssDirectory() + cssPath));                   
                } else {
					if(log.isDebugEnabled())
						log.debug("CSS is blank for at index " + (i + 1));
                }
            }
            
            TemplatesLoader tl = TemplatesLoader.getInstance(scene.getTranslator());
            TransformerHandler handler = tl.newTransformerHandler("resource:simple/falcon/templates/report/xhtml-to-pdf.xsl");
            CssFilter filter = cssManager.getCssSheet(urls.toArray(new URL[urls.size()])).createCssFilter();
            FOPAttributeFixer fixer = new FOPAttributeFixer();
            filter.setContentHandler(fixer);
            //*
            handler.setResult(result);
            fixer.setContentHandler(handler);
            result = new SAXResult(filter);
            /*/
            TransformerHandler ident = tl.newTransformerHandler();
            ident.setResult(result);
            filter.setContentHandler(ident);
            handler.setResult(new SAXResult(filter));
            result = new SAXResult(handler);
            //*/
            return result;
        } catch(LoadingException e) {
            throw new ExecuteException(e);
        } catch (TransformerConfigurationException e) {
            throw new ExecuteException(e);
        } catch (IOException e) {
            throw new ExecuteException(e);
        }
     }
}