package simple.falcon.engine.standard;

import java.io.IOException;
import java.net.URL;

import simple.bean.ConvertUtils;
import simple.translator.Translator;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;
import simple.xml.TemplatesLoader;

public class TemplatesLoader2 extends TemplatesLoader {
	protected static class CacheKey {
		public Translator translator;
		public URLRewriter urlRewriter;
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof CacheKey) {
				CacheKey ck = (CacheKey)obj;
				return ConvertUtils.areEqual(translator, ck.translator) && ConvertUtils.areEqual(urlRewriter, ck.urlRewriter);
			}
			return false;
		}
		@Override
		public int hashCode() {
			return (translator == null ? 0 : translator.hashCode()) ^  (urlRewriter == null ? 0 : urlRewriter.hashCode());
		}
	}
    protected static final Cache<CacheKey,TemplatesLoader2,RuntimeException> instances = new LockSegmentCache<CacheKey,TemplatesLoader2,RuntimeException>(100, 0.75f, 16) {
		@Override
		protected TemplatesLoader2 createValue(CacheKey key, Object... additionalInfo) {
			return new TemplatesLoader2(key.translator, key.urlRewriter);
		}
		@Override
		protected boolean keyEquals(CacheKey key1, CacheKey key2) {
			return key1.equals(key2);
		}
		@Override
		protected boolean valueEquals(TemplatesLoader2 value1, TemplatesLoader2 value2) {
			return value1 == value2; // not important
		}
    };

	public interface URLRewriter {
		public URL rewrite(URL url) ;
		public String rewrite(String path) ;
	}
	protected URLRewriter urlRewriter;

	public TemplatesLoader2(Translator translator, URLRewriter urlRewriter) {
		super(translator);
		this.urlRewriter = urlRewriter;
	}

	public static TemplatesLoader getInstance(Translator translator, URLRewriter urlRewriter) {
		if(urlRewriter == null) return TemplatesLoader.getInstance(translator);
		CacheKey key = new CacheKey();
		key.translator = translator;
		key.urlRewriter = urlRewriter;
        return instances.getOrCreate(key);
    }

	@Override
	protected BundledConfigSource getConfigSource(String path, Long refreshInterval) throws IOException {
		if(urlRewriter != null)
			path = urlRewriter.rewrite(path);
		return super.getConfigSource(path, refreshInterval);
	}

	@Override
	protected BundledConfigSource getConfigSource(URL url, Long refreshInterval) throws IOException {
		if(urlRewriter != null)
			url = urlRewriter.rewrite(url);
		return super.getConfigSource(url, refreshInterval);
	}

}
