/*
 * Created on Jan 3, 2005
 *
 */
package simple.falcon.engine;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bkrug
 *
 */
public class GenericHierarchy<E> implements Hierarchy<E> {
    protected String name;
    protected Object id;
    protected String description;
    protected List<Hierarchy<E>> branches;
    protected List<E> leafs;
    /**
     * 
     */
    public GenericHierarchy() {
        this(new ArrayList<Hierarchy<E>>(), new ArrayList<E>());
    }

    public GenericHierarchy(List<Hierarchy<E>> branches, List<E> leafs) {
        this.leafs = leafs;
        this.branches = branches;
    }
    
    /* (non-Javadoc)
     * @see simple.falcon.engine.Hierarchy#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see simple.falcon.engine.Hierarchy#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /* (non-Javadoc)
     * @see simple.falcon.engine.Hierarchy#getId()
     */
    public Object getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see simple.falcon.engine.Hierarchy#getBranches()
     */
    public List<Hierarchy<E>> getBranches() {
        return branches;
    }

    /* (non-Javadoc)
     * @see simple.falcon.engine.Hierarchy#getLeafs()
     */
    public List<E> getLeafs() {
        return leafs;
    }

    /**
     * @param branches The branches to set.
     */
    public void setBranches(List<Hierarchy<E>> branches) {
        this.branches = branches;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param id The id to set.
     */
    public void setId(Object id) {
        this.id = id;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }
}
