/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine;

import simple.sql.Expression;
import simple.sql.ExtendedExpression;
import simple.sql.SQLType;


/**
 * @author bkrug
 *
 */
public class Field {
    public static enum AdditiveType {FULL, SEMI, NONE};
    protected AdditiveType additiveType = null;
    private long id;
    private String label;
    private String style;
    private String format;
    private Expression displayExpression;
    private Expression sortExpression;
    private SQLType displaySqlType;
    private SQLType sortSqlType;
    private int domain; //currency, percent, text identifier, code, name, etc.
    private int aggregateCount;
    
    public Expression getDisplayExpression() {
        return displayExpression;
    }
    public void setDisplayExpression(Expression displayExpression) {
        this.displayExpression = displayExpression;
        aggregateCount = -1;
    }
    public int getDomain() {
        return domain;
    }
    public void setDomain(int domain) {
        this.domain = domain;
    }
    public String getFormat() {
        return format;
    }
    public void setFormat(String format) {
        this.format = format;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public Expression getSortExpression() {
        return sortExpression;
    }
    public void setSortExpression(Expression sortExpression) {
        this.sortExpression = sortExpression;
        aggregateCount = -1;
    }
    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }
    public AdditiveType getAdditiveType() {
        return additiveType;
    }
    public void setAdditiveType(AdditiveType additiveType) {
        this.additiveType = additiveType;
    }
    /**
     * @return Returns the displaySqlType.
     */
    public SQLType getDisplaySqlType() {
        return displaySqlType;
    }
    /**
     * @param displaySqlType The displaySqlType to set.
     */
    public void setDisplaySqlType(SQLType displaySqlType) {
        this.displaySqlType = displaySqlType;
    }
    /**
     * @return Returns the sortSqlType.
     */
    public SQLType getSortSqlType() {
        return sortSqlType;
    }
    /**
     * @param sortSqlType The sortSqlType to set.
     */
    public void setSortSqlType(SQLType sortSqlType) {
        this.sortSqlType = sortSqlType;
    }
    public boolean containsAggregate() {
    	if(aggregateCount == -1) {
    		aggregateCount = 0;
    		if(displayExpression instanceof ExtendedExpression) {
    			aggregateCount += ((ExtendedExpression)displayExpression).getAggregates().size();
    		}
    		if(sortExpression instanceof ExtendedExpression) {
    			aggregateCount += ((ExtendedExpression)sortExpression).getAggregates().size();
    		}
    	}
    	return aggregateCount > 0;
    }
    /*
    public void setDisplaySqlType(String sqlTypeString) throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
        setDisplaySqlType(new SQLType(sqlTypeString));
    }
    public void setSortSqlTypeName(String sqlTypeString) throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
        setSortSqlType(new SQLType(sqlTypeString));
    }
    */
    public boolean equals(Object o) {
        return o instanceof Field && ((Field)o).id == id;
    }
    public int hashCode() {
        return (int)id;
    }
}
