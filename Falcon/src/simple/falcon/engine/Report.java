/**
 *
 */
package simple.falcon.engine;

import java.text.Format;
import java.util.Map;
import java.util.Set;

import simple.xml.ObjectBuilder;
import simple.xml.XMLizable;

/**
 * @author Brian S. Krug
 *
 */
public interface Report extends XMLizable {

	public Long getReportId();

	public void setReportId(Long reportId) ;

	/**
	 * @return Returns the folios.
	 */
	public Folio[] getFolios();

	/**
	 * @param folios The folios to set.
	 */
	public void setFolios(Folio[] folios);

	/**
	 * @return Returns the outputType.
	 */
	public OutputType getOutputType();

	/**
	 * @param outputType The outputType to set.
	 */
	public void setOutputType(OutputType outputType);

	public Format getSubtitle();

	public String getSubtitle(Object args);

	public void setSubtitle(Format subtitle);

	public Format getTitle();

	public String getTitle(Object args);

	public void setTitle(Format title);

	/**
	 * @return Returns the name.
	 */
	public String getName();

	/**
	 * @param name The name to set.
	 */
	public void setName(String name);

	public String getDirective(String name);

	public void setDirective(String name, String directive);

	public Set<String> getDirectiveNames();

	public String getVerbage(String text);

	public String getVerbage(String text, String defaultTranslation);

	public void setVerbage(String text, String translation);

	public Map<String, String> getVerbageMap();

	public boolean isUpdated();

	public void readXML(ObjectBuilder builder) throws DesignException ;

}