/*
 * Created on Nov 1, 2005
 *
 */
package simple.falcon.engine;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import simple.bean.ConvertUtils;
import simple.io.ConfigSource;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.servlet.RequestInfo;
import simple.servlet.RequestInfoUtils;
import simple.translator.Translator;
import simple.util.concurrent.Cache;

public class Scene implements RequestInfo {
	private static final Log log = Log.getLog();
    protected String userAgent;
    protected String baseUrl;
    protected URL baseUrlObject;
    protected String[] stylesheets;
    protected Locale locale;
    protected Translator translator;
    protected String[] scripts;
    protected String runReportAction;
    protected TimeZone timeZone ;
    protected boolean live ;
	protected Layout layout;
	protected Cache<String, ConfigSource, RuntimeException> fileCache;
    protected final Map<String,Object> properties = new HashMap<String,Object>();
    
    public Scene() {
        super();
    }

    /**
     * @return Returns the baseUrl.
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl The baseUrl to set.
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        try {
			this.baseUrlObject = new URL(baseUrl);
		} catch(MalformedURLException e) {
			log.warn("Invalid baseUrl '" + baseUrl + "'", e);
			this.baseUrlObject = null;
		}
    }

    /**
     * @return Returns the stylesheets.
     */
    public String[] getStylesheets() {
        return stylesheets;
    }

    /**
     * @param stylesheets The stylesheets to set.
     */
    public void setStylesheets(String[] stylesheets) {
        this.stylesheets = stylesheets;
    }

    /**
     * @return Returns the userAgent.
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * @param userAgent The userAgent to set.
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Translator getTranslator() {
        return translator;
    }

    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

	public String[] getScripts() {
		return scripts;
	}

	public void setScripts(String[] scripts) {
		this.scripts = scripts;
	}

	public String getRunReportAction() {
		return runReportAction;
	}

	public void setRunReportAction(String runReportAction) {
		this.runReportAction = runReportAction;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public String getServerName() {
		if(baseUrlObject == null)
			return null;
		return baseUrlObject.getHost();
	}

	public String getContextPath() {
		if(baseUrlObject == null)
			return null;
		String path = baseUrlObject.getPath();
		if(path.length() == 0)
			return ".";
		if(path.charAt(0) == '/')
			return path;
		return "/" + path;
	}

	public String getPathInfo() {
		if(baseUrlObject == null)
			return null;
		return null;
	}

	public String getRealPath(String webUri) {
		return webUri;
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public Map<String, Object> getProperties() {
		return properties;
	}

	public Layout getLayout() {
		return layout;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}
	
	public String addLastModifiedToUri(String path) {
		return RequestInfoUtils.addLastModifiedToUri(this, path, fileCache);
	}

	public Cache<String, ConfigSource, RuntimeException> getFileCache() {
		return fileCache;
	}

	public void setFileCache(Cache<String, ConfigSource, RuntimeException> fileCache) {
		this.fileCache = fileCache;
	}

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, getUserAgent(), getBaseUrl(), getStylesheets(), getLocale(), getScripts(), getRunReportAction(), getTimeZone(), isLive(), getLayout());
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		if(!(obj instanceof Scene))
			return false;
		Scene other = (Scene)obj;
		return ConvertUtils.areEqual(getUserAgent(), other.getUserAgent())
				&& ConvertUtils.areEqual(getBaseUrl(), other.getBaseUrl())
				&& ConvertUtils.areEqual(getStylesheets(), other.getStylesheets())
				&& ConvertUtils.areEqual(getLocale(), other.getLocale())
				&& ConvertUtils.areEqual(getScripts(), other.getScripts())
				&& ConvertUtils.areEqual(getRunReportAction(), other.getRunReportAction())
				&& ConvertUtils.areEqual(getTimeZone(), other.getTimeZone())
				&& isLive() == other.isLive()
				&& ConvertUtils.areEqual(getLayout(), other.getLayout());				
	}
}
