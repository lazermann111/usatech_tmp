/*
 * Created on Sep 16, 2005
 *
 */
package simple.falcon.engine;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import javax.xml.transform.Result;

public interface Output {
    public Writer getWriter() throws IOException ;
    public OutputStream getOutputStream() throws IOException ;
    public Result getResult() throws IOException ;
}
