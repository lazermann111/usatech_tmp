package simple.falcon.engine;

import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.results.Results;

/**
 * Represents an pair of SQL columns
 * (one for the display expression and one for the sort expression).
 */
public class FieldOrder {
    protected Field field;
    protected SortOrder sortOrder;
    protected Results.Aggregate aggregateType;
    //  the order in which to sort rows
    protected int sortIndex = 0;
    
    protected String displayColumnName;
    protected String sortColumnName;
    
    public String getDisplayColumnName() {
		return displayColumnName;
	}
	public void setDisplayColumnName(String displayColumnName) {
		this.displayColumnName = displayColumnName;
	}
	public String getSortColumnName() {
		return sortColumnName;
	}
	public void setSortColumnName(String sortColumnName) {
		this.sortColumnName = sortColumnName;
	}
	public Field getField() {
        return field;
    }
    public SortOrder getSortOrder() {
        return sortOrder;
    }
    @Override
	public int hashCode() {
    	int hc = 0;
    	hc = SystemUtils.addHashCode(hc, field);
    	hc = SystemUtils.addHashCode(hc, sortOrder);
    	hc = SystemUtils.addHashCode(hc, aggregateType);
    	hc = SystemUtils.addHashCode(hc, displayColumnName);
    	hc = SystemUtils.addHashCode(hc, sortColumnName);
    	return hc;
    }
    @Override
	public boolean equals(Object o) {
        if(o instanceof FieldOrder) {
            FieldOrder fo = (FieldOrder) o;
            return sortOrder == fo.sortOrder && ConvertUtils.areEqual(displayColumnName, fo.displayColumnName) &&ConvertUtils.areEqual(sortColumnName, fo.sortColumnName) &&ConvertUtils.areEqual(field, fo.field) && aggregateType == fo.aggregateType && sortIndex == fo.sortIndex;
        }
        return false;
    }
    
    public Results.Aggregate getAggregateType() {
        return aggregateType;
    }
    
	public void setAggregateType(Results.Aggregate aggregateType) {
        this.aggregateType = aggregateType;
    }
    public void setField(Field field) {
        this.field = field;
    }
    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
    public int getSortIndex() {
        return sortIndex;
    }
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }
}