package simple.falcon.engine.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.batik.css.parser.Parser;
import org.w3c.css.sac.InputSource;

import simple.io.CacheFile;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.io.PropertiesLoader;

public class CssToXslParser implements Loader {
    private static final Log log = Log.getLog();
	protected Map<java.net.URL,ConfigSource> configUrlMap;
	protected Map<ConfigSource,File> xslConfMap;
	protected ConfigSource tagMapCfgSrc;
	protected Properties tagMap;
	protected File tempDirectory = null;

	public CssToXslParser(){
		this.configUrlMap = new HashMap<URL,ConfigSource>();
		this.xslConfMap = new HashMap<ConfigSource,File>();
		this.tagMap = new Properties();

		try {
			URL url = CssToXslParser.class.getResource("tagMap.properties");
			if (url == null){
				IOException ioe = new IOException("Unable to load config file 'tagMap.properties'");
				throw ioe;
			}
			this.tagMapCfgSrc = ConfigSource.createConfigSource(url);
		} catch (IOException ioe) {
			log.error("Unable to load properties resource '" + CssToXslParser.class.getPackage() + "tagMap.properties'.",ioe);
		}

		this.tagMapCfgSrc.registerLoader(new PropertiesLoader(tagMap),-1,-1);
	}

	public void load(ConfigSource src) throws LoadingException {
		Parser cssParser;
		InputStreamReader cssStream;
		InputSource cssSource;
		CssToXslDocumentHandler handler;
		File file;
		FileOutputStream fos = null;

		try {
			if(xslConfMap.containsKey(src)){
				file = xslConfMap.get(src);
			} else {
				file = new CacheFile(File.createTempFile("css",".xsl",tempDirectory).getPath());
				xslConfMap.put(src,file);
			}
			fos = new java.io.FileOutputStream(file);
		} catch (FileNotFoundException fnfe) {
			log.error("Could not find temp file.",fnfe);
		} catch (IOException ioe) {
			log.error("Failed to create temp file.",ioe);
		}

		this.tagMapCfgSrc.reload();
		cssParser = new org.apache.batik.css.parser.Parser();
		handler = new CssToXslDocumentHandler(fos,this.tagMap);

		try {
			cssStream = new java.io.InputStreamReader(src.getInputStream());
			cssSource = new org.w3c.css.sac.InputSource(cssStream);
			cssParser.setDocumentHandler(handler);
			cssParser.parseStyleSheet(cssSource);
		} catch (IOException ioe) {
			log.error("Parsing to XSL failed for CSS source '"+src.toString()+"'",ioe);
		}
	}

	public URL getXslUrl(URL cssUrl) {
		URL xslUrl = null;
		ConfigSource cssCfgSrc = configUrlMap.get(cssUrl);

		if(cssCfgSrc == null){
			try {
				cssCfgSrc = ConfigSource.createConfigSource(cssUrl);
			} catch(IOException e) {
				log.error("Could not get Config Source", e);
			}
			cssCfgSrc.registerLoader(this,-1,-1);
			configUrlMap.put(cssUrl,cssCfgSrc);
		}

		try {
			cssCfgSrc.reload();
		} catch (LoadingException le) {
			log.error("Unable to reload Config Source for URL '"+cssUrl.toString()+"'",le);
		}

		try {
            File f = xslConfMap.get(cssCfgSrc);
            if(!f.exists()) cssCfgSrc.getLoader().load(cssCfgSrc);
			xslUrl = f.toURI().toURL();
		} catch (MalformedURLException mue) {
			log.error("Unable to convert XSL temp file to URL",mue);
		} catch (LoadingException e) {
            log.error("Could not load CSS file");
        }

		if(xslUrl == null){
			log.error("No XSL document mapped for Config Source '"+cssCfgSrc.toString()+"'");
		}

		return xslUrl;
	}

	public void setTempDirectory(File dir){
		this.tempDirectory = dir;
	}

	public File getTempDirectory(){
		return this.tempDirectory;
	}

}
