/**
 *
 */
package simple.falcon.engine.util;

import java.beans.IntrospectionException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Parameter;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Executor;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Param;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.engine.standard.StandardDesignEngine2;
import simple.falcon.engine.standard.StandardFolio;
import simple.falcon.engine.standard.StandardParam;
import simple.falcon.xml.DisplayType;
import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.util.AffixedKeyMap;
import simple.util.BeanPropertiesMap;
import simple.util.CompositeMap;
import simple.util.FilterMap;
import simple.util.SiftedMap;

/**
 * @author Brian S. Krug
 *
 */
public class EngineUtils {
	protected static final Pattern userAgentsSupportingDataUri = Pattern.compile("(?:Mozilla/(?:.(?!MSP?IE\\s+[0-8]\\.))*?)|(?:Opera/.*)");
	public static final String PARAMS_PREFIX = "params.";
	public static int maxRowPerPage = 500;
	public static boolean isDataUriSupported(String userAgent) {
		return userAgentsSupportingDataUri.matcher(userAgent).matches();
	}
	public static Map<String,Object> createReportParameters(Map<String,Object> parameters, Executor executor, Scene scene) {
    	CompositeMap<String,Object> compositeParameters = new CompositeMap<String, Object>();
    	Map<String,Object> beanMap;
    	try {
			beanMap = new BeanPropertiesMap(executor);
		} catch(IntrospectionException e) {
			beanMap = Collections.singletonMap("EXCEPTION", (Object)e);
		}
        compositeParameters.merge(new AffixedKeyMap<Object>(beanMap, "~user.", null));
        try {
			beanMap = new BeanPropertiesMap(scene);
		} catch(IntrospectionException e) {
			beanMap = Collections.singletonMap("EXCEPTION", (Object)e);
		}
        compositeParameters.merge(new AffixedKeyMap<Object>(beanMap, "~scene.", null));
        compositeParameters.merge(parameters);
	    return compositeParameters;
    }
	public static Map<String,Object> createResultParameters(Map<String,Object> parameters, Executor executor, Scene scene) {
		Map<String, Object> params = new FilterMap<Object>(parameters, EngineUtils.PARAMS_PREFIX, null);
		params.put("user", executor);
		params = new SiftedMap<String, Object>(params) {
			@Override
			protected boolean siftKey(Object key, boolean update) {
				return key instanceof String && ((update && key.equals("user")) || ((String) key).startsWith("user."));
			}
		};

		/*
		Map<String, Object> params = new HashMap<String, Object>(parameters.size());
		params.put("user", executor);
		for(Map.Entry<String, Object> entry : parameters.entrySet())
			if(entry.getKey() != null && entry.getKey().startsWith(EngineUtils.PARAMS_PREFIX)) {
				String name = entry.getKey().substring(EngineUtils.PARAMS_PREFIX.length());
				if(!name.equals("user") && !name.startsWith("user."))
					params.put(name, entry.getValue());
			}
			*/
		return params;
    }
	public static void extractParamsForAsync(Report report, Scene scene, Map<String, ?> userParams, Map<String, String> resultParams, String... keepParamNames) throws ConvertException {
		extractParamsForAsync(report, scene, userParams, resultParams, Arrays.asList(keepParamNames));
	}
	public static void extractParamsForAsync(Report report, Scene scene, Map<String, ?> userParams, Map<String, String> resultParams, Iterable<String> keepParamNames) throws ConvertException {
		for(Folio folio : report.getFolios()) {
			extractParamsForAsync(folio.getFilter().gatherParams(), userParams, resultParams);
			for(FieldOrder fo : folio.getFieldOrders()) {
				if(fo.getField() != null) {
					if(fo.getField().getDisplayExpression() != null)
						extractParamsForAsync(fo.getField().getDisplayExpression().getParameters(), userParams, resultParams);
					if(fo.getField().getSortExpression() != null)
						extractParamsForAsync(fo.getField().getSortExpression().getParameters(), userParams, resultParams);
				}
			}
        }
		if(report.getOutputType() != null && report.getOutputType().getProtocol().startsWith("chart-")) {
			String dis = ConvertUtils.getStringSafely(userParams.get("displayType"));
			DisplayType displayType;
			if(dis != null)
				displayType = DisplayType.getByValue(dis);
			else
				displayType = null;
			if(displayType == null) {
				if(scene.getUserAgent() != null && isDataUriSupported(scene.getUserAgent())) {
					displayType = DisplayType.INLINE_IMAGE;
		        } else {
		        	displayType = DisplayType.INLINE_REQUEST;
		        }
			}
			resultParams.put("displayType", displayType.toString());
		}
		for(String p : keepParamNames) {
			if(!resultParams.containsKey(p) && userParams.containsKey(p))
				resultParams.put(p, ConvertUtils.getString(userParams.get(p), false));
		}
	}

	protected static void extractParamsForAsync(Param[] params, Map<String, ?> userParams, Map<String, String> resultParams) throws ConvertException {
		if(params != null)
			for(Param param : params)
				if(!StringUtils.isBlank(param.getName())) {
					Class<? extends Object> targetClass = param.getJavaType();
					String paramName = PARAMS_PREFIX + param.getName();
					String stringValue;
					if(userParams.containsKey(paramName)) {
						stringValue = ConvertUtils.getString(ConvertUtils.convert(targetClass, userParams.get(paramName)), false);
					} else {
						stringValue = ConvertUtils.getString(ConvertUtils.convert(targetClass, param.getValue()), false);
					}
					if(stringValue != null)
						resultParams.put(paramName, stringValue);
				}
	}

	protected static void extractParamsForAsync(List<Parameter> params, Map<String, ?> userParams, Map<String, String> resultParams) throws ConvertException {
		if(params != null)
			for(Parameter param : params)
				if(!StringUtils.isBlank(param.getPropertyName())) {
					Class<? extends Object> targetClass = param.getJavaType();
					String paramName = PARAMS_PREFIX + param.getPropertyName();
					String stringValue;
					if(userParams.containsKey(paramName)) {
						stringValue = ConvertUtils.getString(ConvertUtils.convert(targetClass, userParams.get(paramName)), false);
					} else {
						stringValue = ConvertUtils.getString(ConvertUtils.convert(targetClass, param.getDefaultValue()), false);
					}
					if(stringValue != null)
						resultParams.put(paramName, stringValue);
				}
	}

	public static void extractParamsForPrompt(Report report, Scene scene, Map<String, ?> userParams, Map<String, Object> resultParams, Map<String, Param> paramPrompts, String... keepParamNames) throws ConvertException {
		extractParamsForPrompt(report, scene, userParams, resultParams, paramPrompts, Arrays.asList(keepParamNames));
	}

	public static void extractParamsForPrompt(Report report, Scene scene, Map<String, ?> userParams, Map<String, Object> resultParams, Map<String, Param> paramPrompts, Iterable<String> keepParamNames) throws ConvertException {
		for(Folio folio : report.getFolios()) {
			extractParamsForPrompt(folio.getFilter().gatherParams(), userParams, resultParams, paramPrompts);
			for(FieldOrder fo : folio.getFieldOrders()) {
				if(fo.getField() != null) {
					if(fo.getField().getDisplayExpression() != null)
						extractParamsForPrompt(fo.getField().getDisplayExpression().getParameters(), userParams, resultParams, paramPrompts);
					if(fo.getField().getSortExpression() != null)
						extractParamsForPrompt(fo.getField().getSortExpression().getParameters(), userParams, resultParams, paramPrompts);
				}
			}
	    }
		for(String p : keepParamNames) {
			if(!resultParams.containsKey(p) && userParams.containsKey(p))
				resultParams.put(p, ConvertUtils.getString(userParams.get(p), false));
		}
	}

	protected static void extractParamsForPrompt(Param[] params, Map<String, ?> userParams, Map<String, Object> resultParams, Map<String, Param> paramPrompts) throws ConvertException {
		if(params != null)
			for(Param param : params)
				if(!StringUtils.isBlank(param.getName())) {
					Class<? extends Object> targetClass = param.getJavaType();
					String paramName = PARAMS_PREFIX + param.getName();
					if(userParams.containsKey(paramName)) {
						resultParams.put(paramName, ConvertUtils.convert(targetClass, userParams.get(paramName)));
					} else {
						resultParams.put(paramName, ConvertUtils.convert(targetClass, param.getValue()));
					}

					if(param.getLabel() != null && param.getLabel().trim().length() > 0 && !paramPrompts.containsKey(paramName)) {
						paramPrompts.put(paramName, param);
					}
				}
	}

	protected static void extractParamsForPrompt(List<Parameter> params, Map<String, ?> userParams, Map<String, Object> resultParams, Map<String, Param> paramPrompts) throws ConvertException {
		if(params != null)
			for(Parameter param : params)
				if(!StringUtils.isBlank(param.getPropertyName())) {
					Class<? extends Object> targetClass = param.getJavaType();
					String paramName = PARAMS_PREFIX + param.getPropertyName();
					if(userParams.containsKey(paramName)) {
						resultParams.put(paramName, ConvertUtils.convert(targetClass, userParams.get(paramName)));
					} else {
						resultParams.put(paramName, ConvertUtils.convert(targetClass, param.getDefaultValue()));
					}

					if(!paramPrompts.containsKey(paramName)) {
						Param p = new StandardParam(null, false);
						p.setLabel(param.getPropertyName());
						p.setName(param.getPropertyName());
						p.setSqlType(param.getSqlType());
						p.setValue(param.getDefaultValue());
						paramPrompts.put(paramName, p);
					}
				}
	}
	
	public static Report getSQLFolioReport(ExecuteEngine executeEngine, Map<String, ?> reportParams, int outputTypeId) throws DesignException {
		StandardDesignEngine2 designEngine=(StandardDesignEngine2)executeEngine.getDesignEngine();
		StandardFolio folio=new StandardFolio(designEngine);
		String title = ConvertUtils.getStringSafely(reportParams.get("title"));
		if(!StringUtils.isBlank(title)) {
			folio.setTitle(new MessageFormat(title));
		}
		String subtitle = ConvertUtils.getStringSafely(reportParams.get("subtitle"));
		if(!StringUtils.isBlank(subtitle)) {
			folio.setSubtitle(new MessageFormat(subtitle));
		}
		folio.setQuery(reportParams.get("query").toString());
		folio.setDataSourceName(designEngine.getDesignDataSourceName());
		Report report = designEngine.newReport();
		folio.setDirective("max-rows-per-page", ConvertUtils.getStringSafely(reportParams.get("max-rows-per-page"), String.valueOf(maxRowPerPage)));
		String header = ConvertUtils.getStringSafely(reportParams.get("header"));
		if(!StringUtils.isBlank(header)) {
			folio.setDirective("show-header", header);
		}
		report.setDirective("max-rows-per-page",ConvertUtils.getStringSafely(reportParams.get("max-rows-per-page"), String.valueOf(maxRowPerPage)));
		report.setFolios(new Folio[] {folio});
		designEngine.createQueryPillars(report, reportParams);
		if(folio.getOutputType() == null && outputTypeId > 0)
		folio.setOutputType(executeEngine.getDesignEngine().getOutputType(outputTypeId));
        report.setOutputType(folio.getOutputType());
        return report;
	}
}
