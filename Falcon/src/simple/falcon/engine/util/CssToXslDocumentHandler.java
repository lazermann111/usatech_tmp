package simple.falcon.engine.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.batik.css.parser.DefaultAndCondition;
import org.apache.batik.css.parser.DefaultAttributeCondition;
import org.apache.batik.css.parser.DefaultClassCondition;
import org.apache.batik.css.parser.DefaultConditionalSelector;
import org.apache.batik.css.parser.DefaultDescendantSelector;
import org.apache.batik.css.parser.DefaultElementSelector;
import org.apache.batik.css.parser.DefaultIdCondition;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.Condition;
import org.w3c.css.sac.DocumentHandler;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.SACMediaList;
import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;

import simple.io.Log;
import simple.text.StringUtils;

public class CssToXslDocumentHandler implements DocumentHandler {
    private static final Log log = Log.getLog();
    protected static float DEFAULT_PIXEL_TO_MM = 0.28f;
    protected static float BORDER_PIXEL_TO_MM = 0.14f;
    protected static class PieceLexicalUnit implements LexicalUnit {
        protected LexicalUnit delegate;
        public PieceLexicalUnit(LexicalUnit delegate) {
            this.delegate = delegate;
        }
        public String getDimensionUnitText() {
            return delegate.getDimensionUnitText();
        }
        public float getFloatValue() {
            return delegate.getFloatValue();
        }
        public String getFunctionName() {
            return delegate.getFunctionName();
        }
        public int getIntegerValue() {
            return delegate.getIntegerValue();
        }
        public short getLexicalUnitType() {
            return delegate.getLexicalUnitType();
        }
        public LexicalUnit getNextLexicalUnit() {
            return null;
        }
        public LexicalUnit getParameters() {
            return delegate.getParameters();
        }
        public LexicalUnit getPreviousLexicalUnit() {
            return null;
        }
        public String getStringValue() {
            return delegate.getStringValue();
        }
        public LexicalUnit getSubValues() {
            return delegate.getSubValues();
        }
    }
    protected static interface CompositeProperty {
        public Map<String,LexicalUnit> split(LexicalUnit value) ;
    }
    protected static class FontCompositeProperty implements CompositeProperty { 
        public Map<String,LexicalUnit> split(LexicalUnit value) {
            Map<String,LexicalUnit> map = new HashMap<String, LexicalUnit>();
            //figure out if line height is used and how many of the optional pieces are used
//        addCompositeProperty("font", 
//            "(?:([^\\s]+)\\s+)?(?:([^\\s]+)\\s+)?(?:([^\\s]+)\\s+)?([^\\s]+)(?:/([^\\s]+))?\\s+([^\\s]+)", 
//            "font-style", "font-variant", "font-weight", "font-size", "line-height", "font-family");    
            final String[] optionalPropertyNames = new String[] {"font-style", "font-variant", "font-weight"};
            boolean lineHeight = false;
            int optionals = -2;
            while(!lineHeight) {
               optionals++;
               if(value.getLexicalUnitType() == LexicalUnit.SAC_OPERATOR_SLASH) { 
                   lineHeight = true;
                   if(value.getNextLexicalUnit() == null) throw new CSSException("'font' property not valid value. Missing line-height after '/'");
                   value = value.getNextLexicalUnit();
                   break;
               } else if(value.getNextLexicalUnit() == null) {
                   break;
               } else {
                   value = value.getNextLexicalUnit();               
               }
            }
            if(lineHeight) {
                map.put("line-height", new PieceLexicalUnit(value));
                if(value.getNextLexicalUnit() == null) throw new CSSException("'font' property not valid value. Missing font-family after line-height value");
                value = value.getNextLexicalUnit(); 
            }
            if(optionals < 0)
                throw new CSSException("'font' property not valid value. Both font-size and font-family are required");
            else if(optionals > 3)
                throw new CSSException("'font' property not valid value. Too many lexicals provided");
            map.put("font-family", new PieceLexicalUnit(value));
            if(lineHeight)
                value = value.getPreviousLexicalUnit().getPreviousLexicalUnit();
            value = value.getPreviousLexicalUnit();
            map.put("font-size", new PieceLexicalUnit(value));
            for(int i = 0; i < optionals; i++) {
                value = value.getPreviousLexicalUnit();
                map.put(optionalPropertyNames[i], new PieceLexicalUnit(value));
            }
            return map;
        }    
    }
    /*
    protected static class CompositePiece {
        protected Integer lexicalUnitTypePrefix;
        protected boolean optional;
        protected String subPropertyName;
        public CompositePiece(String subPropertyName, boolean optional, Integer lexicalUnitTypePrefix) {
            this.subPropertyName = subPropertyName;
            this.optional = optional;
            this.lexicalUnitTypePrefix = lexicalUnitTypePrefix;
        }
        public Integer getLexicalUnitTypePrefix() {
            return lexicalUnitTypePrefix;
        }
        public boolean isOptional() {
            return optional;
        }
        public String getSubPropertyName() {
            return subPropertyName;
        }       
    }
    protected static class GenericCompositeProperty implements CompositeProperty {
        public GenericCompositeProperty(CompositePiece... pieces) {
            this.pieces = pieces;
            List<Integer> bks = new ArrayList<Integer>();
            List<Integer> mks = new ArrayList<Integer>();
            for(int i = 0; i < pieces.length; i++) {
                if(pieces[i].getLexicalUnitTypePrefix() != null) {
                    bks.add(i);
                    mks.add(pieces[i].getLexicalUnitTypePrefix());
                }
            }
        }
        protected Pattern regex;
        protected CompositePiece[] pieces;
        protected int[] breaks;
        protected int[] markers;
        public Map<String,LexicalUnit> split(LexicalUnit value) {
            Map<String,LexicalUnit> map = new HashMap<String, LexicalUnit>();
            
            LexicalUnit[] lus = new LexicalUnit[pieces.length];
            int k = 0;
            for(int i = 0; i < pieces.length; i++) {
                if(pieces[i].getLexicalUnitTypePrefix() != null) {
                    
                }
                lus[k++] = new PieceLexicalUnit(value);
                value = value.getNextLexicalUnit();
            }
            while(value != null) {
               switch(value.getLexicalUnitType()) {
                   case LexicalUnit.SAC_OPERATOR_SLASH: 
                       
               }
            }
            return map;
        }
    }*/
    
    protected static final Map<String, CompositeProperty> compositeProperties = new HashMap<String, CompositeProperty>();
    static {
        addCompositeProperty("font", new FontCompositeProperty());    
    }
    
    protected java.io.OutputStream out;
	protected Properties tagMap;
	
    public static void addCompositeProperty(String propertyName, CompositeProperty compositeProperty) {
        compositeProperties.put(propertyName, compositeProperty);        
    }

	public CssToXslDocumentHandler(OutputStream outStream, Properties tagMap) {
		this.out = outStream;
		this.tagMap = tagMap;
	}
	
	public void setOutputStream(OutputStream outStream) {
		this.out = outStream;
	}

	public void startDocument(InputSource source) throws CSSException {
		write("<xsl:stylesheet xmlns=\"http://www.w3.org/1999/XSL/Format\" " +
			"xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" " +
			"xmlns:css=\"http://www.saf.org/simple/css\" version=\"1.0\">"+
			"<xsl:include href=\"resource:/simple/falcon/templates/report/css-pdf-base.xsl\"/>" +
			"<xsl:template mode=\"stylesheet\" match=\"node()\">");
	}

	public void endDocument(InputSource source) throws CSSException {
		write("</xsl:template></xsl:stylesheet>");
	}

	public void comment(String arg0) throws CSSException {

	}

	public void ignorableAtRule(String arg0) throws CSSException {

	}

	public void namespaceDeclaration(String arg0, String arg1)
			throws CSSException {

	}

	public void importStyle(String arg0, SACMediaList arg1, String arg2)
			throws CSSException {

	}

	public void startMedia(SACMediaList arg0) throws CSSException {

	}

	public void endMedia(SACMediaList arg0) throws CSSException {

	}

	public void startPage(String arg0, String arg1) throws CSSException {

	}

	public void endPage(String arg0, String arg1) throws CSSException {

	}

	public void startFontFace() throws CSSException {

	}

	public void endFontFace() throws CSSException {

	}

	public void startSelector(SelectorList selectors) throws CSSException {
		write("<xsl:if test=\"");
        boolean first = true;
		for(int i = 0; i < selectors.getLength(); i++) {
            Selector s = selectors.item(i);
            if(s != null) {
                if(first) first = false;
                else write(" or ");
                write("(");
                parseSelector(s);                
                write(")");
            }
		}
		write("\">");
	}
	
	private boolean parseSelector(Selector s){
		boolean wrote = true;
		switch(s.getSelectorType()){
			case(Selector.SAC_ANY_NODE_SELECTOR):{
				write("node()");
			} break;
			case(Selector.SAC_CONDITIONAL_SELECTOR):{
				if (parseSelector(((DefaultConditionalSelector)s).getSimpleSelector())){
					write(" and ");
				}
				parseCondition(((DefaultConditionalSelector)s).getCondition());
			} break;
			case(Selector.SAC_ELEMENT_NODE_SELECTOR):{
				String selector = getElementSelector(((DefaultElementSelector)s).getLocalName());
				if (selector != null){
					write("(" + selector + ")");
				} else {
					wrote = false;
				}
			} break;
			case(Selector.SAC_DESCENDANT_SELECTOR):{
				DefaultDescendantSelector child = (DefaultDescendantSelector)s;
				parseSelector(child.getSimpleSelector());
				write(" and (ancestor::node()[");
				parseSelector(child.getAncestorSelector());
				write("])");
			} break;
			default:{
				log.warn("CSS to XSL parser used default Selector for type '" + s.getSelectorType() + "'");
				write("(true or ('defaulted for type' = '" + s.getSelectorType() + "'))");
			}
		}
		return wrote;
	}
	
	private void parseCondition(Condition c){		
		switch(c.getConditionType()){
			case(Condition.SAC_ATTRIBUTE_CONDITION):{
				write("(" + ((DefaultAttributeCondition)c).getLocalName() + "='" + 
					((DefaultAttributeCondition)c).getValue() + "')");
			} break;
			case(Condition.SAC_CLASS_CONDITION):{
				write("(@css:class='"+((DefaultClassCondition)c).getValue()+"')");
			} break;
			case(Condition.SAC_ID_CONDITION):{
				write("(@css:id='"+((DefaultIdCondition)c).getValue()+"')");
			} break;
			case(Condition.SAC_AND_CONDITION):{
				parseCondition(((DefaultAndCondition)c).getFirstCondition());
				write(" and ");
				parseCondition(((DefaultAndCondition)c).getSecondCondition());
			} break;
			case(Condition.SAC_PSEUDO_CLASS_CONDITION):{
				//Do nothing, pseudo classes don't make sense in PDF do they?
				write("false");
			} break;
			default:{
				log.warn("CSS to XSL parser used default Condition for type '" + c.getConditionType() + "'");
				write("(true or ('defaulted for type' = '" + c.getConditionType() + "'))");
			}
		}
	}

	public void endSelector(SelectorList selectors) throws CSSException {
		write("</xsl:if>");
	}

	public void property(String name, LexicalUnit value, boolean important) throws CSSException {
	    // translate composite properties
        CompositeProperty comp = compositeProperties.get(name);
        if(comp != null) {
            Map<String,LexicalUnit> subProperties = comp.split(value);
            for(Map.Entry<String,LexicalUnit> entry : subProperties.entrySet()) {
                property(entry.getKey(), entry.getValue(), important);
            }
        } else {
            //TODO: obey important somehow
    		write("<xsl:attribute name=\"" + name + "\">");
    		parseLexicalUnit(value, (name.equals("border") || name.startsWith("border-") ? BORDER_PIXEL_TO_MM : DEFAULT_PIXEL_TO_MM));
    		write("</xsl:attribute>");
        }
	}
	
	private void parseLexicalUnit(LexicalUnit value, float pixelToMM){
		while(value != null){
			if(value.getPreviousLexicalUnit() != null){
				write(" ");
			}
			
			switch(value.getLexicalUnitType()){
				case(LexicalUnit.SAC_INTEGER): {
					write(value.getIntegerValue());
				} break;
				case(LexicalUnit.SAC_URI):
				case(LexicalUnit.SAC_ATTR):
				case(LexicalUnit.SAC_IDENT):
				case(LexicalUnit.SAC_STRING_VALUE): {
					write(value.getStringValue());
				} break;
				case(LexicalUnit.SAC_COUNTER_FUNCTION): {
					write("counter(");
					parseLexicalUnit(value.getParameters(), pixelToMM);
					write(")");
				} break;
				case(LexicalUnit.SAC_COUNTERS_FUNCTION): {
					write("counters(");
					parseLexicalUnit(value.getParameters(), pixelToMM);
					write(")");
				} break;
				case(LexicalUnit.SAC_RECT_FUNCTION): {
					write("rect(");
					parseLexicalUnit(value.getParameters(), pixelToMM);
					write(")");
				} break;
				case(LexicalUnit.SAC_RGBCOLOR): {
					write("rgb(");
					parseLexicalUnit(value.getParameters(), pixelToMM);
					write(")");
				} break;
				case(LexicalUnit.SAC_FUNCTION): {
					write(value.getFunctionName()+"(");
					parseLexicalUnit(value.getParameters(), pixelToMM);
					write(")");
				} break;
				case(LexicalUnit.SAC_PIXEL): {
					write("" + (value.getFloatValue() * pixelToMM) + "mm");
				} break;
				case(LexicalUnit.SAC_REAL): 
				case(LexicalUnit.SAC_DIMENSION): 
				case(LexicalUnit.SAC_EM):
				case(LexicalUnit.SAC_EX):
				case(LexicalUnit.SAC_INCH):
				case(LexicalUnit.SAC_CENTIMETER): 
				case(LexicalUnit.SAC_MILLIMETER):
				case(LexicalUnit.SAC_POINT):
				case(LexicalUnit.SAC_PICA):
				case(LexicalUnit.SAC_PERCENTAGE): 
				case(LexicalUnit.SAC_DEGREE):
				case(LexicalUnit.SAC_GRADIAN): 
				case(LexicalUnit.SAC_RADIAN):
				case(LexicalUnit.SAC_MILLISECOND): 
				case(LexicalUnit.SAC_SECOND):
				case(LexicalUnit.SAC_HERTZ):
				case(LexicalUnit.SAC_KILOHERTZ): {
					write("" + value.getFloatValue() + value.getDimensionUnitText());
				} break;
				case(LexicalUnit.SAC_SUB_EXPRESSION): {
					parseLexicalUnit(value.getSubValues(), pixelToMM);
				} break;
				case(LexicalUnit.SAC_OPERATOR_COMMA): {
					write(",");
				} break;
				default: {
					log.warn("CSS to XSL parser used default Lexical unit for type '" + value.getLexicalUnitType() + "'");
					write("(defaulted for type '" + value.getLexicalUnitType() + "')");
				}
			}
			
			value = value.getNextLexicalUnit();
		}
	}
	
	private String getElementSelector(String htmlTag){
		String selector = null;
		if(htmlTag != null){
			String mapping = tagMap.getProperty(htmlTag);
			if (mapping == null){
				selector = "name()='" + htmlTag + "'";//XXX: Does this need to be local-name() or should we use http://www.w3.org/1999/xhtml/ + htmlTag???
			} else {
				String[] tags = StringUtils.split(mapping,'/');
				selector = "";
				String tail = "";
				for(int i = (tags.length - 1); i >= 0; i--){
					if (i < (tags.length - 1)) {
						selector += " and (ancestor::node()[";
						tail += "])";
					}
					selector += "name()='" + tags[i] + "'";//XXX: Does this need to be local-name() or should we use http://www.w3.org/1999/xhtml/ + htmlTag???
				}
				selector += tail;
			}
		}
		return selector;
	}
	
	private void write(int msg) {
		try {
			this.out.write((""+msg).getBytes());
		} catch (IOException ioe) {
			log.error("Unable to write to temp xsl file.",ioe);
		}
	}
	
	private void write(String msg) {
		try {
			this.out.write(msg.getBytes());
		} catch (IOException ioe) {
			log.error("Unable to write to temp xsl file.",ioe);
		}
	}

}
