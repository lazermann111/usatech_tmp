/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine;

import java.util.List;
import java.util.Map;

import simple.results.Results;
import simple.results.ResultsFilter;

public interface ResultsValueRetriever {
    public Object getValue(Results results, Map<String,Object> context) ;
    public Object getAggregate(Results results, int groupColumnIndex, Map<String,Object> context) ;
    public Object getAggregate(Results results, ResultsFilter filter, int groupColumnIndex, Map<String,Object> context) ;
    public void trackAggregates(Results results) ;
    public void trackAggregates(Results results, ResultsFilter filter) ;
    public List<String> addGroups(Results results) ;
}