/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine;
/**
 * Indicates a problem occurred while accessing the DesignEngine functionality
 * @author bkrug
 *
 */
public class DesignException extends Exception {
    private static final long serialVersionUID = 12394817501L;

    public DesignException() {
        super();
    }

    public DesignException(String message) {
        super(message);
    }

    public DesignException(String message, Throwable cause) {
        super(message, cause);
    }

    public DesignException(Throwable cause) {
        super(cause);
    }

}
