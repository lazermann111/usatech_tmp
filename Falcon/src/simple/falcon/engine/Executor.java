/*
 * Created on Nov 1, 2005
 *
 */
package simple.falcon.engine;

import java.util.Map;

public interface Executor {
    public long[] getUserGroupIds() ;
    public long getUserId() ;
    public Map<String, String> getUserProperties() ;

	public String getDisplayName();
}
