package simple.falcon.engine;


public class DirectoryScene extends Scene {
	protected final String baseDir;
	
	public DirectoryScene(String baseDir) {
		super();
		if(baseDir == null || baseDir.length() == 0)
			baseDir = ".";
		int pos = baseDir.length();
		LOOP: while(pos > 0) {
			switch(baseDir.charAt(pos-1)) { 
				case '/': case '\\': 
					break; 
				default: 
					break LOOP;
			}
			pos--;
		}
		baseDir = baseDir.substring(0, pos);
		this.baseDir = baseDir;
	}

	public String getRealPath(String webUri) {
		if(webUri == null) 
			return null;
		if(webUri.length() == 0)
			return baseDir +'/';
		switch(webUri.charAt(0)) {
			case '/': case '\\': 
				return baseDir + webUri; 
			default: 
				return baseDir + '/' + webUri;
		}
	}
}
