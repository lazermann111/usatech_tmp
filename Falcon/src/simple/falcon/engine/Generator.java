/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine;

import java.util.Map;

import simple.results.Results;

public interface Generator {
    public void generate(Report report, OutputType outputType, Map<String,Object> parameters, Results[] results, Executor executor, Scene scene, Output out) throws ExecuteException ;
    public boolean isResetResultsRequired(OutputType outputType) ;
}