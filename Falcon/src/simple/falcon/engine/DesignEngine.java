/*
 * Created on Nov 16, 2004
 *
 */
package simple.falcon.engine;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Map;

import simple.db.CacheableCall;
import simple.db.DataSourceFactory;
import simple.sql.BuildSQLException;
import simple.sql.SQLType;
import simple.translator.Translator;

/** The main workhorse of designing, saving, retrieving and executing folios.
 * @author bkrug
 *
 */
public interface DesignEngine {
	public ColumnNamer getColumnNamer();

    public CacheableCall getCall(Folio folio, long[] userGroupIds) throws DesignException ;

	public CacheableCall getCall(Folio folio, long[] userGroupIds, Connection conn) throws DesignException;

	public Connection findConnection(Folio folio) throws DesignException;

	public String findDataSourceName(Folio folio) throws BuildSQLException;

    public boolean isMetric(String expression, DatabaseMetaData md) throws DesignException ;

    public Field getField(long fieldId) throws DesignException ;
    public OutputType getOutputType(int outputTypeId) throws DesignException ;
    public Operator getOperator(int operatorId) throws DesignException ;
    public FilterItem getFilterItem(long filterItemId) throws DesignException ;
    public FilterGroup getFilterGroup(long filterGroupId) throws DesignException ;
    public Folio getFolio(long folioId) throws DesignException ;
    public Folio getFolio(long folioId, Translator translator) throws DesignException ;
    public Report getReport(long reportId) throws DesignException ;
    public Report getReport(long reportId, Translator translator) throws DesignException;

    public Field[] getFields(long[] fieldIds) throws DesignException ;
    public Hierarchy<Field> getFieldHierarchy(long[] userGroupIds) throws DesignException ;
    public Hierarchy<Field> getFieldHierarchy(long[] userGroupIds, long[] excludeFieldIds) throws DesignException ;
    public OutputType[] getOutputTypes() throws DesignException ;
    public Operator[] getOperators() throws DesignException ;
    //public Folio[] getFolios() throws DesignException ;
    //public Report[] getReports() throws DesignException ;

    public void reloadFolio(long folioId) throws DesignException ;
    public void reloadReport(long reportId) throws DesignException ;
    public void reloadField(long fieldId) throws DesignException ;
    public void reloadOutputType(int outputTypeId) throws DesignException ;
    public void reloadOperator(int operatorId) throws DesignException ;
    public void reloadFilterItem(FilterItem filterItem) throws DesignException ;
    public void reloadFilterGroup(FilterGroup filterGroup) throws DesignException ;

    public void reloadFolios() throws DesignException ;
    public void reloadReports() throws DesignException ;
    public void reloadFields() throws DesignException ;
    public void reloadOutputTypes() throws DesignException ;
    public void reloadOperators() throws DesignException ;
    public void reloadFilters() throws DesignException ;
    public void reloadJoins() throws DesignException ;

    public Folio newFolio() throws DesignException ;
    public Long saveFolio(Folio folio) throws DesignException ;
    public Long saveFolio(Folio folio, Executor executor) throws DesignException ;

    public Report newReport() throws DesignException ;
    public Report newReport(Translator translator) throws DesignException;
    public Long saveReport(Report report) throws DesignException ;
    public Long saveReport(Report report, Executor executor) throws DesignException ;

    public Map<Integer,String> getSqlTypes() ;

    public static class InvalidField {
        protected Field field;
        protected Throwable exception;
        public InvalidField(Field field, Throwable exception) {
            this.field = field;
            this.exception = exception;
        }
        /**
         * @return Returns the exception.
         */
        public Throwable getException() {
            return exception;
        }
        /**
         * @return Returns the field.
         */
        public Field getField() {
            return field;
        }
    }

	public InvalidField[] validateFields() throws DesignException;

	public SQLType[] validateFieldExpressions(String displayExpression, String sortExpression) throws DesignException;

	public DataSourceFactory getDataSourceFactory();

	public void setDataSourceFactory(DataSourceFactory dataSourceFactory);

	public String getDefaultDataSourceName();

	public void setDefaultDataSourceName(String defaultDataSourceName);

	public String getDesignDataSourceName();

	public void setDesignDataSourceName(String designDataSourceName);
	
	public void createQueryPillars(Report report, Map<String, ?> reportParams) throws DesignException;

}