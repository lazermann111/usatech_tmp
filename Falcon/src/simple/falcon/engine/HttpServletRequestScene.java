package simple.falcon.engine;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class HttpServletRequestScene extends Scene {
	protected final HttpServletRequest request;
	
	public HttpServletRequestScene(HttpServletRequest request) {
		super();
		this.request = request;
	}

	public String getRealPath(String webUri) {
		ServletContext servletContext = request.getSession().getServletContext();
		return servletContext.getRealPath(webUri);
		/*
		if(StringUtils.startsWith(webUri, request.getContextPath())) {
			return servletContext.getRealPath(webUri.substring(request.getContextPath().length()));
		} else {
			ServletContext rootServletContext = servletContext.getContext("/");
			if(rootServletContext == null)
				return null;
			return rootServletContext.getRealPath(webUri.toString());
		}
		*/
	}

	public HttpServletRequest getRequest() {
		return request;
	}
}
