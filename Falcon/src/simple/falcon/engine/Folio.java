/*
 * Created on Aug 24, 2005
 *
 */
package simple.falcon.engine;

import java.text.Format;
import java.util.Set;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.translator.Translator;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public interface Folio{
    // The Grouping Levels are an unbounded enum so use int for now
    public static final int GROUPING_LEVEL_AGG_FOUR = -5;

    public static final int GROUPING_LEVEL_AGG_THREE = -4;

    public static final int GROUPING_LEVEL_AGG_TWO = -3;

    public static final int GROUPING_LEVEL_AGG_ONE = -2;

    public static final int GROUPING_LEVEL_AGG = -1;

    public static final int GROUPING_LEVEL_ZERO = 0;

    public static final int GROUPING_LEVEL_ONE = 1;

    public static final int GROUPING_LEVEL_TWO = 2;

    public static final int GROUPING_LEVEL_THREE = 3;

    public static final int GROUPING_LEVEL_FOUR = 4;

    public Folio copy(Translator translator) ;

    public Filter getFilter();

    public void setFilter(Filter filter);

    public Format getSubtitle();

    public String getSubtitle(Object args);

    public void setSubtitle(Format subtitle);

    public Format getTitle();

    public String getTitle(Object args);

    public void setTitle(Format title);

    public void removePillar(Pillar pillar);

    public void removePillar(int pillarIndex);

    public void movePillar(int oldPillarIndex, int newPillarIndex);

    public FieldOrder[] getFieldOrders();

    public Pillar[] getPillars();

    public boolean equals(Object o);

    public int hashCode();

    public Pillar newPillar() ;
    /**
     * @return Returns the folioId.
     */
    public Long getFolioId();

    /**
     * @param folioId The folioId to set.
     */
    public void setFolioId(Long folioId);

    public OutputType getOutputType();

    public void setOutputType(OutputType outputType);
    public String getChartType() ;
    public void setChartType(String chartType) ;

    /**
     * @return Returns the name.
     */
    public String getName();

    /**
     * @param name The name to set.
     */
    public void setName(String name);

    public int getMaxRowsPerSection() ;
    public void setMaxRowsPerSection(int maxRows) ;
    public int getMaxRows() ;
    public void setMaxRows(int maxRows) ;

    public String getDirective(String name) ;
    public void setDirective(String name, String directive) ;
    public Set<String> getDirectiveNames() ;
    //public int getMaxRowsForLevel(int groupLevel) ;

    //public void setMaxRowsForLevel(int groupLevel, int maxRows) ;
    public void recalcFieldOrders() ;

	public boolean isUpdated() ;

    public ColumnNamer getColumnNamer() ;

    public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException;

    public void readXML(ObjectBuilder builder) throws DesignException;
    
    public String getQuery();

	public void setQuery(String query);
    
    public String getDataSourceName();
}