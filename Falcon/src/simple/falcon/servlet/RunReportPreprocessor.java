package simple.falcon.servlet;

import javax.servlet.ServletException;

import simple.falcon.engine.Executor;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.servlet.InputForm;

public interface RunReportPreprocessor {
	public void preprocess(Report report, Scene scene, Executor executor, InputForm form) throws ServletException ;
}
