/*
 * Created on Dec 30, 2004
 *
 */
package simple.falcon.servlet;

import java.io.IOException;

import javax.servlet.ServletException;

import simple.io.LoadingException;
import simple.servlet.SimpleServlet;

/**
 * @author bkrug
 *
 */
public class FalconServlet extends SimpleServlet {
    private static final long serialVersionUID = 941041023488L;
    public static final String ATTRIBUTE_DESIGN_ENGINE = "simple.falcon.engine.DesignEngine";
    public static final String ATTRIBUTE_EXECUTE_ENGINE = "simple.falcon.engine.ExecuteEngine";
    public static final String ATTRIBUTE_DEFAULT_CSS_PATH = "DEFAULT_CSS_PATH";
    public static final String ATTRIBUTE_DEFAULT_SCRIPT_PATH = "DEFAULT_SCRIPT_PATH";
    protected String falconActionsFile = "simple/falcon/servlet/falcon-actions.xml";
    /**
     *
     */
    public FalconServlet() {
        super();
        restrictedParameterNames.add(ATTRIBUTE_DESIGN_ENGINE);
        restrictedParameterNames.add(ATTRIBUTE_EXECUTE_ENGINE);
        restrictedParameterNames.add(ATTRIBUTE_DEFAULT_CSS_PATH);
        restrictedParameterNames.add(ATTRIBUTE_DEFAULT_SCRIPT_PATH);
    }

    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
	public void init() throws ServletException {
        setApplicationAttributeIfNull(ATTRIBUTE_DEFAULT_CSS_PATH, "css/default-style.css");
        setApplicationAttributeIfNull(ATTRIBUTE_DEFAULT_SCRIPT_PATH, "scripts/report.js");
        
        super.init();

        String actionsFile = getFalconActionsFile();
        if(actionsFile.trim().length() > 0)
	        try {
				addActionsFile(actionsFile, refreshInterval);
			} catch(IOException e) {
				throw new ServletException(e);
			} catch(LoadingException e) {
				throw new ServletException(e);
			}
        /*
        String cssDir = getServletConfig().getInitParameter("css-directory");
        if(cssDir == null) cssDir = "web:/";
        ee.setCssDirectory(cssDir);*/
        //ee.setCssDirectory("web:/");

        //ee.getTemplatesLoader().setAttribute("generate-translet", true);
        //ee.getTemplatesLoader().setAttribute("debug", true);
        //ee.getTemplatesLoader().setAttribute("jar-name", "xsltc-translets.jar");
    }

    protected void setApplicationAttributeIfNull(String attributeName, Object defaultValue) {
    	if(getApplicationAttribute(attributeName) == null)
    		setApplicationAttribute(attributeName, defaultValue);
    }
    @Override
    public void destroy() {
    	super.destroy();
		getServletContext().removeAttribute(ATTRIBUTE_EXECUTE_ENGINE);
    	getServletContext().removeAttribute(ATTRIBUTE_DESIGN_ENGINE);
    }

	public String getFalconActionsFile() {
		return falconActionsFile;
	}

	public void setFalconActionsFile(String falconActionsFile) {
		this.falconActionsFile = falconActionsFile;
	}
}
