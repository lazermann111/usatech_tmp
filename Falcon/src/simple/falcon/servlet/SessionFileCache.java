/*
 * Created on Sep 16, 2005
 *
 */
package simple.falcon.servlet;

import java.io.File;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import simple.io.Log;
import simple.util.ObjectCache;

public class SessionFileCache extends ObjectCache<File> implements HttpSessionBindingListener {
    private static final Log log = Log.getLog();
    public SessionFileCache() {
        super();
    }
    public void valueBound(HttpSessionBindingEvent event) {
        // do nothing
    }
    public void valueUnbound(HttpSessionBindingEvent event) {
        //attempt to delete all files
        log.debug("File Cache removed from Session; attempting to delete files");
        for(File file : cache.values()) {
            if(file != null) {
                if(file.delete()) log.debug("Deleted '" + file.getName() + "'");
                else log.warn("Could not delete '" + file.getName() + "'");
            }
        }
    }
}