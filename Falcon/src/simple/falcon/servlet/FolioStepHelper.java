/*
 * Created on Jan 18, 2005
 *
 */
package simple.falcon.servlet;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Field;
import simple.falcon.engine.Filter;
import simple.falcon.engine.Folio;
import simple.falcon.engine.HttpServletRequestScene;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.engine.SortOrder;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterGroup;
import simple.falcon.run.FolioUpdateUtils;
import simple.results.DataGenre;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.NotAuthorizedException;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleServlet;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.translator.Translator;

/**
 * @author bkrug
 *
 */
public class FolioStepHelper {
    private static final simple.io.Log log = simple.io.Log.getLog();
/* NOT USED
    public static Folio readFolio(InputForm form) throws ServletException, SQLException, DataLayerException, ConvertException, BeanException {
        //PerformanceTracker.getDefaultInstance().startTask("read-folio", "");
        log.debug("Reading request and creating folio");
        ExecuteEngine executeEngine = (ExecuteEngine)form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
        DesignEngine designEngine = executeEngine.getDesignEngine();
        Folio folio = new Folio();
        FolioStepHelper.addPillars(folio, form, designEngine);
        FolioStepHelper.addFilters(folio, form, designEngine);
        int outputType = form.getInt("outputType", false, 0);
        if(outputType > 0) folio.setOutputType(designEngine.getOutputType(outputType));
        if(folio.getOutputType() == null) folio.setOutputType(designEngine.getOutputType(22));
        FolioStepHelper.updateTitles(folio, form, designEngine);
        if(log.isDebugEnabled()) log.debug("Created folio with " + folio.getPillars().length + " pillar(s) and filter " + folio.getFilter() + ". Creating report...");
        //PerformanceTracker.getDefaultInstance().endTask("read-folio");
        return folio;
    }
    public static void addPillars(Folio folio, InputForm form, DesignEngine designEngine) throws ServletException, SQLException, DataLayerException, ConvertException, BeanException {
        long[] fieldIds = form.getLongArray("fieldIds", false);
        if(fieldIds != null && fieldIds.length > 0) {
            log.debug("Getting fields...");
            Field[] fields = designEngine.getFields(fieldIds);
            for(int i = 0; i < fields.length; i++) {
                folio.addFieldPillar(Folio.GROUPING_LEVEL_ZERO, fields[i], Folio.SORT_ORDER_DEFAULT);
            }
        }
        //TODO: expand this to allow more sophisticated pillars
        int blankPillars = form.getInt("blankPillars", false, 0);
        for(int i = 0; i < blankPillars; i++) {
            folio.addBlankPillar();
        }
    }
*/
    public static void checkFolioUpdatePrivs(Folio folio, InputForm form) throws SQLException, DataLayerException, NotAuthorizedException, ConvertException {
    	ServletUser user = (ServletUser) form.getAttribute(SimpleServlet.ATTRIBUTE_USER);
    	if(user == null || !(user.hasPrivilege("19") || (user.hasPrivilege("18") && folio.getFolioId() == null))) {
        	Object[] ret = DataLayerMgr.executeCall("TEST_FOLIO_UPDATE_PRIV", form, false);
        	log.debug("Test update Priv returned " + ret[1]);
        	if(!ConvertUtils.getBoolean(ret[1])) {
        		throw new NotAuthorizedException("You are not permitted to update this folio");
        	}
    	}
    }
    public static StandardFilterGroup readFilter(InputForm form, StandardDesignEngine designEngine, Filter oldFilter) throws DesignException, ServletException {
        try {
			return FolioUpdateUtils.readFilter(form, designEngine, oldFilter);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

    public static void addFilter(Folio folio, Filter filter, String separator) {
    	FolioUpdateUtils.addFilter(folio, filter, separator);
    }

    public static void addFilters(Folio folio, InputForm form, StandardDesignEngine designEngine) throws ServletException, DesignException {
    	try {
			FolioUpdateUtils.addFilters(folio, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

    public static void replaceFilters(Folio folio, InputForm form, StandardDesignEngine designEngine) throws ServletException, DesignException {
    	try {
			FolioUpdateUtils.replaceFilters(folio, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

	public static void addBlankPillars(Folio folio, InputForm form, DesignEngine designEngine) throws ServletException {
    	try {
			FolioUpdateUtils.addBlankPillars(folio, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

	public static void updatePillar(Folio folio, InputForm form, DesignEngine designEngine) throws ServletException, DesignException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ParseException {
    	try {
			FolioUpdateUtils.updatePillar(folio, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

	public static void chartConfig(Folio folio, InputForm form, DesignEngine designEngine) throws ServletException {
    	try {
			FolioUpdateUtils.chartConfig(folio, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

    public static void updateTitles(Folio folio, InputForm form, DesignEngine designEngine) throws ServletException {
    	try {
			FolioUpdateUtils.updateTitles(folio, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

    public static void updateTitles(Report report, InputForm form, DesignEngine designEngine) throws ServletException {
    	try {
			FolioUpdateUtils.updateTitles(report, form, designEngine);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }

    public static void addFieldToPillar(Folio folio, int pillarIndex, long fieldId, DesignEngine designEngine)
            throws DesignException {
        FolioUpdateUtils.addFieldToPillar(folio, pillarIndex, fieldId, designEngine);
    }

    public static void addFieldPillar(Folio folio, long fieldId, DesignEngine designEngine) throws DesignException {
    	FolioUpdateUtils.addFieldPillar(folio, fieldId, designEngine);
    }
    public static void addFieldPillar(Folio folio, int groupingLevel, Field field, SortOrder sortOrder) {
    	FolioUpdateUtils.addFieldPillar(folio, groupingLevel, field, sortOrder);
	}
    public static void addLinkPillar(Folio folio, int groupingLevel, String actionFormat, String helpFormat, Field field, SortOrder sortOrder) {
    	FolioUpdateUtils.addLinkPillar(folio, groupingLevel, actionFormat, helpFormat, field, sortOrder);
	}
    public static void addPillar(Folio folio, Pillar.PillarType pillarType, int groupingLevel, String labelFormat, String description, String styleFormat, String displayFormat, String actionFormat, String helpFormat, String sortFormat, DataGenre sortType, SortOrder[] sortOrders, Field[] fields, Results.Aggregate[] aggregateTypes, int[] sortIndexes) {
    	FolioUpdateUtils.addPillar(folio, pillarType, groupingLevel, labelFormat, description, styleFormat, displayFormat, actionFormat, helpFormat, sortFormat, sortType, sortOrders, fields, aggregateTypes, sortIndexes);
	}
	public static void updateDirectives(Folio folio, InputForm form) throws ServletException {
		try {
			FolioUpdateUtils.updateDirectives(folio, form);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
	}
	public static void updateDirectives(Report report, InputForm form) throws ServletException {
		try {
			FolioUpdateUtils.updateDirectives(report, form);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
	}

	public static Layout determineLayout(InputForm form) throws ServletException {
		if(form.getBoolean("fragment", false, false))
			return Layout.FRAGMENT;
		Boolean unframed = form.getBooleanObject("unframed");
		if(unframed == null) 
			return null;
		if(unframed.booleanValue())
			return Layout.UNFRAMED;
		return Layout.FULL;
	}
	public static Scene createScene(InputForm form, HttpServletRequest request, Translator translator) throws ServletException {
		Scene scene = new HttpServletRequestScene(request);
        try {
            scene.setLocale(RequestUtils.getLocale(request));
            if(log.isDebugEnabled()) log.debug("Using locale " + scene.getLocale());
        } catch(ConvertException e) {
            log.warn("Could not convert to Locale", e);
        }
        ServletUser user = RequestUtils.getUser(request);
        if(user instanceof FalconUser)
        	 scene.setTimeZone(((FalconUser)user).getTimeZone());
        scene.setBaseUrl(RegexUtils.substitute("[^/]*$", request.getRequestURL().toString(), "", ""));
        scene.setUserAgent(request.getHeader("user-agent"));
        scene.setTranslator(translator);
        scene.setRunReportAction(request.getContextPath() + request.getServletPath());
        scene.setLive(true);
        String sessionToken = RequestUtils.getSessionTokenIfPresent(form);
		if(sessionToken != null)
			scene.getProperties().put("session-token", sessionToken);
		scene.setLayout(determineLayout(form));

        {
        List<String> stylesheets = new ArrayList<String>();
        String[] cssPaths = new String[4];
        cssPaths[0] = null;
        cssPaths[1] = translator == null ? null : translator.translate("report-stylesheets", (String)null);
        cssPaths[2] = ConvertUtils.getStringSafely(form.getAttribute("simple.servlet.ServletUser.userProperties.CUSTOM_CSS_PATH", "session"));
        cssPaths[3] = ConvertUtils.getStringSafely(form.getAttribute("simple.falcon.servlet.CustomReportStep.CUSTOM_CSS_PATH", "request"));
        for(String cssPath : cssPaths) {
            if(cssPath != null && cssPath.trim().length() > 0)
            for(String cssTok : StringUtils.split(cssPath, ',')) {
                cssTok = cssTok.trim();
                if(cssTok.length() > 0)
                    stylesheets.add(RequestUtils.addLastModifiedToUri(request, null, cssTok));
            }
        }
        scene.setStylesheets(stylesheets.toArray(new String[stylesheets.size()]));
        }
        {
        List<String> scripts = new ArrayList<String>();
        String[] scriptPaths = new String[4];
        scriptPaths[0] = null;
        scriptPaths[1] = translator == null ? null : translator.translate("main-scripts", (String)null);
        scriptPaths[2] = ConvertUtils.getStringSafely(form.getAttribute("simple.servlet.ServletUser.userProperties.CUSTOM_SCRIPT_PATH", "session"));
        scriptPaths[3] = ConvertUtils.getStringSafely(form.getAttribute("simple.falcon.servlet.CustomReportStep.CUSTOM_SCRIPT_PATH", "request"));
        for(String scriptPath : scriptPaths) {
            if(scriptPath != null && scriptPath.trim().length() > 0)
            for(String cssTok : StringUtils.split(scriptPath, ',')) {
                cssTok = cssTok.trim();
                if(cssTok.length() > 0)
                	scripts.add(RequestUtils.addLastModifiedToUri(request, null, cssTok));
            }
        }
        scene.setScripts(scripts.toArray(new String[scripts.size()]));
        }
        return scene;
	}
}
