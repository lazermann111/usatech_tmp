/*
 * Created on Apr 12, 2005
 *
 */
package simple.falcon.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.io.IOUtils;
import simple.io.Log;
import simple.io.Nozzle;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.ObjectCache;

public class GetChartStep extends AbstractStep {
    private static final Log log = Log.getLog();
    public GetChartStep() {
        super();
    }

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        long cacheId = form.getLong("chartReferenceId", true, 0);
        String contentType = form.getString("chartContentType", false);
        ObjectCache<?> chartCache = (ObjectCache<?>)form.getAttribute("chartCache", "session");
        if(chartCache != null) {
            Object o = chartCache.getObject(cacheId);
            if(o != null) {
                if(log.isTraceEnabled()) log.trace("Found session-cached object " + o.getClass().getName() + " with id=" + cacheId + "\n" + o);
                else if(log.isDebugEnabled()) log.debug("Found session-cached object " + o.getClass().getName() + " with id=" + cacheId);
                if(contentType != null) response.setContentType(contentType);
                try {
                    if(o instanceof File) {
                        OutputStream out = response.getOutputStream();
                        InputStream in = new FileInputStream((File)o);
                        long len = IOUtils.copy(in, out);
                        out.flush();
                        log.debug("Copied " + len + " bytes to servlet output stream"); 
                        in.close();
                    } else if(o instanceof Reader) {
                        PrintWriter out = response.getWriter();
                        Reader in = (Reader)o;
                        long len = IOUtils.copy(in, out);
                        out.flush();
                        log.debug("Copied " + len + " chars to servlet output stream");
                        in.close();
                    } else if(o instanceof Nozzle) {
                        PrintWriter out = response.getWriter();
                        int len = ((Nozzle)o).fill(out);
                        log.debug("Filled in " + len + " bytes to servlet output stream");   
                        out.flush();
                    } else if(o instanceof ByteArrayOutputStream) {
                        OutputStream out = response.getOutputStream();
                        byte[] b = ((ByteArrayOutputStream)o).toByteArray();
                        out.write(b);
                        out.flush();
                        log.debug("Wrote " + b.length + " bytes to servlet output stream");                    
                    } else if(o instanceof InputStream) {
                        OutputStream out = response.getOutputStream();
                        InputStream in = (InputStream)o;
                        long len = IOUtils.copy(in, out);
                        out.flush();
                        log.debug("Copied " + len + " bytes to servlet output stream"); 
                        in.close();
                    } else {
                        PrintWriter out = response.getWriter();
                        String s = o.toString();
                        out.write(s);
                        log.debug("Printed " + s.length() + " bytes to servlet output stream");
                        out.flush();
                    }
                } catch(IOException e) {
                    throw new ServletException(e);
                }
            } else {
                log.info("Could not find session-cached object with id=" + cacheId);
            }
        } else {
            log.info("Could not find chartCache in session");
        }
    }

}
