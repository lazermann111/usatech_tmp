/*
 * Created on Sep 14, 2005
 *
 */
package simple.falcon.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import simple.bean.ConvertException;
import simple.io.Base64;
import simple.io.Log;
import simple.servlet.RequestUtils;
import simple.text.RegexUtils;

public class Base64TranslatorServlet extends HttpServlet {
    private static final long serialVersionUID = 18895234L;
	private static final Log log = Log.getLog();
    public static final String PART_CACHE_ATTRIBUTE = "simple.falcon.servlet.Base64TranslatorServlet.PART_CACHE";
    public static final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(3);
    protected static long defaultCacheTimeout = 15 * 1000; // only stay in the session for 15 seconds
    public static class PartCache implements HttpSessionBindingListener {
        protected ScheduledFuture<?> removeTask;
        protected final ReentrantLock lock = new ReentrantLock();
        protected final Condition signal = lock.newCondition();
        protected int partCount = 0;
        protected String[] parts = new String[10];
        protected long timeout;
        public PartCache() {
            this(defaultCacheTimeout);
        }
        public PartCache(long timeout) {
            this.timeout = timeout;
        }
        public void addPart(int part, String data) {
            if(part > partCount) partCount = part;
            try {
                parts[part-1] = data;
            } catch(ArrayIndexOutOfBoundsException aiooe) {
                String[] tmp = new String[part + 5];
                System.arraycopy(parts, 0, tmp, 0, parts.length);
                parts = tmp;
                parts[part-1] = data;
            } 
            if(haveAllParts()) {
                lock.lock();
                try {
                    signal.signalAll();
                } finally {
                    lock.unlock();
                }
            }
        }
        public String getPartsString(long timeout) {
            if(waitForAllParts(timeout)) {
                StringBuffer sb = new StringBuffer();
                for(int i = 0; i < partCount; i++) sb.append(parts[i]);
                return sb.toString();
			}
			return null;
        }
        
        public String[] getPartsArray(long timeout) {
            if(waitForAllParts(timeout)) {
                String[] arr = new String[partCount];
                System.arraycopy(parts, 0, arr, 0, arr.length);
                return arr;
			}
			return null;
        }
        public boolean waitForAllParts(long timeout) {
            if(!haveAllParts()) {
                Date end = new Date(System.currentTimeMillis() + timeout);
                do {
                    lock.lock();
                    try {
                        if(!signal.awaitUntil(end) && !haveAllParts()) return false; //timeout reached
                    } catch (InterruptedException e) {
                        // do nothing
                    } finally {
                        lock.unlock();
                    }
                } while(!haveAllParts());
            }
            return true;
        }
        protected boolean haveAllParts() {
            for(int i = 0; i < partCount; i++) {
                if(parts[i] == null) {
                    log.debug("Missing part " + (i+1) + " of " + partCount);
                    return false;
                }
            }
            return true;
        }
        public void valueBound(HttpSessionBindingEvent event) {
            // wait for timeout, then remove this from session
            if(timeout > 0) {
                final HttpSession session = event.getSession();
                final String name = event.getName();
                Runnable r = new Runnable() {
                    public void run() {
                        session.removeAttribute(name);
                        log.debug("Removed Part Cache #" + name.substring(name.lastIndexOf('.') + 1) + " from the session");
                    }
                };
                removeTask = scheduler.schedule(r, timeout, TimeUnit.MILLISECONDS);
            }
        }
        public void valueUnbound(HttpSessionBindingEvent event) {
            // stop timer
            if(removeTask != null) removeTask.cancel(false);
        }
    }
    public Base64TranslatorServlet() {
        super();
    }
    protected void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String part = request.getParameter("part");
        String refId = request.getParameter("ref-id");
        String contentType = request.getParameter("content-type");
        String data = request.getParameter("data");
        if(data == null) {
            data = request.getParameter("data1");
            if(data == null) {
                log.warn("Got no data for request with params: " + request.getParameterMap());
            } else {
                log.debug("Received data piece 1 (" + data.length() + " bytes)");
                String[] arr = new String[10];
                arr[0] = data;
                int cnt = 1;
                for(; ; cnt++) {
                    data = request.getParameter("data" + (cnt+1));
                    if(data == null) break; 
                    log.debug("Received data piece " +(cnt+1) + " (" + data.length() + " bytes)");
                    try {
                        arr[cnt] = data;
                    } catch(ArrayIndexOutOfBoundsException aiooe) {
                        String[] tmp = new String[cnt + 5];
                        System.arraycopy(arr, 0, tmp, 0, arr.length);
                        arr = tmp;
                        arr[cnt] = data;
                    } 
                }
                decodeToOutput(arr, cnt, contentType, response);  
            }
        } else if(refId != null && refId.length() > 0 && part != null && (part=part.trim()).length() > 0) {
            String[] parts = RegexUtils.match("(\\d+)of(\\d+)", part, "i");
            if(parts != null) {
                int index = Integer.parseInt(parts[1]);
                int count = Integer.parseInt(parts[2]);
                PartCache partCache;
                try {
                    partCache = RequestUtils.getOrCreateAttribute(request, PART_CACHE_ATTRIBUTE + "." + refId, PartCache.class, "session");
                } catch (ConvertException e) {
                    throw new ServletException(e);
                } catch (InstantiationException e) {
                    throw new ServletException(e);
                } catch (IllegalAccessException e) {
                    throw new ServletException(e);
                }
                log.debug("Received part " + index + " of " + count + " for Part Cache #" + refId + " (" + data.length() + " bytes)");
                partCache.addPart(index, data);// store for later
                if(index >= count) { // wait for other parts 
                    String[] arr = partCache.getPartsArray(30 * 1000);
                    decodeToOutput(arr, arr.length, contentType, response);                
                } else {
                    //output blank
                    return;
                }                 
            }
        } else {
            log.debug("Translating base 64 data of length " + data.length() + " for content type " + contentType);
            if(contentType != null && (contentType=contentType.trim()).length() > 0) 
                response.setContentType(contentType);
            response.getOutputStream().write(Base64.decode(data));
            response.getOutputStream().flush();
            //response.getWriter().write(new String(Base64.decode(data)).toCharArray());
            //response.getWriter().flush();
        }
    }
    
    @SuppressWarnings("deprecation")
	protected void decodeToOutput(String[] arr, int length, String contentType, HttpServletResponse response) throws IOException {
        log.debug("Translating base 64 data from " + length + " pieces for content type " + contentType);
        if(contentType != null && (contentType=contentType.trim()).length() > 0) 
            response.setContentType(contentType);
        
        OutputStream out = response.getOutputStream();
        //byte[] buf = new byte[4 * (int)(arr[0].length()/4)];
        int total = 0;
        byte[] buf = new byte[512];
        int offset = 0;
        int last = 0;
        for(int i = 0; i < length; ) {
            if(arr[i].length() - offset < buf.length - last) {
                arr[i].getBytes(offset, arr[i].length(), buf, last);
                last += arr[i].length() - offset;
                i++;
                offset = 0;
            } else {
                int len = buf.length;
                arr[i].getBytes(offset, len + offset - last, buf, last);
                out.write(Base64.decode(buf, 0, len));   
                offset += len - last;
                total += len;
                last = 0;
            }
        }
        if(last > 0) {
            out.write(Base64.decode(buf, 0, last));
            total += last;
        }
        log.debug("Wrote " + total + " bytes for Base 64 Encoded Request");                   
        out.flush();
    }
    /** Process incoming HTTP GET requests
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response from the servlet
     * @throws ServletException If an exception occurs
     * @throws IOException If an IO exception occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }
    /** Process incoming HTTP POST requests
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response from the servlet
     * @throws ServletException If an exception occurs
     * @throws IOException If an IO exception occurs
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }
}
