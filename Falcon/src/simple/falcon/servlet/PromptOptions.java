/**
 *
 */
package simple.falcon.servlet;

import java.util.Map;
import java.util.TreeMap;

import simple.util.CaseInsensitiveComparator;

public enum PromptOptions {
	NEVER, MISSING, FIX, ALWAYS ;

	protected static final Map<String,PromptOptions> promptOptionsMap = new TreeMap<String,PromptOptions>(new CaseInsensitiveComparator<String>());
    static {
        promptOptionsMap.put("never", PromptOptions.NEVER);
        promptOptionsMap.put("no", PromptOptions.NEVER);
        promptOptionsMap.put("false", PromptOptions.NEVER);
        promptOptionsMap.put("missing", PromptOptions.MISSING);
        promptOptionsMap.put("if", PromptOptions.MISSING);
        promptOptionsMap.put("conditional", PromptOptions.MISSING);
        promptOptionsMap.put("always", PromptOptions.ALWAYS);
        promptOptionsMap.put("yes", PromptOptions.ALWAYS);
        promptOptionsMap.put("true", PromptOptions.ALWAYS);
        promptOptionsMap.put("fix", PromptOptions.FIX);
    }

    public static PromptOptions getByValue(String value) {
    	return promptOptionsMap.get(value);
    }
}