package simple.falcon.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.ParameterException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Param;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsType;
import simple.falcon.engine.Scene;
import simple.io.Log;
import simple.servlet.InputForm;
import simple.servlet.RequestInfoUtils;
import simple.text.StringUtils;

/**
 * Base class to run a folio asynchronously
 *
 * @author bkrug
 *
 */
public class InlineRunReportStep extends AbstractRunReportStep {
	private static final Log log = Log.getLog();

	public InlineRunReportStep() {
		super();
	}

	/**
	 * @param report
	 * @param scene
	 * @param form
	 * @param paramPrompts
	 * @param reportParams
	 * @param response
	 * @throws ServletException
	 */
	@Override
	protected void sendReportPage(ExecuteEngine executeEngine, Report report, Scene scene, Executor executor, PromptOptions promptOption, InputForm form, Map<String, Param> paramPrompts, Map<String, Object> parameters, Iterable<String> keepParamNames, HttpServletResponse response) throws ServletException {
		// create report
		OutputType outputType = report.getOutputType();
		form.setAttribute("Content-Type", outputType.getContentType());
		try {
			String filename = outputType.generateFileName(report, parameters);
			// NOTE: this is to accomadate IE7 and IE8 which always prompt for "application/xhtml+xml"
			String contentType = RequestInfoUtils.getContentTypeForLegacyBrowsers(outputType.getContentType());
			response.setContentType(contentType);
			if(!RequestInfoUtils.isDirectlyRendered(contentType)) {
				// NOTE: using "attachment;filename=" causes FireFox to show SaveAs dialog even for text/html
				StringBuilder sb = new StringBuilder();
				sb.append("attachment;filename=\"");
				if(!StringUtils.isBlank(filename))
					sb.append(StringUtils.escape(filename, new char[] { '"' }, '\\'));
				sb.append('"');
				String cd = sb.toString();
				response.setHeader("Content-Disposition", cd);
				form.setAttribute("Content-Disposition", cd);
	        }
	    	String resultsTypeString = (String)form.get("resultsType");
	    	ResultsType resultsType;
	    	if(resultsTypeString == null || (resultsTypeString=resultsTypeString.trim()).length() == 0)
	    		resultsType = ResultsType.FORWARD;
			else
				try {
					resultsType = ConvertUtils.convert(ResultsType.class, resultsTypeString);
				} catch(ConvertException e) {
					throw new ServletException("Invalid resultsType '" + resultsTypeString + "' specified", e);
				}

	        ServletResponseOutput out = new ServletResponseOutput(response);
	        executeEngine.buildReport(report, parameters, executor, outputType, scene, out, resultsType);
			if(log.isDebugEnabled()) log.debug("Report built with content-type: " + outputType.getContentType() + "; filename: " + filename);
	        out.flush();
		} catch(ParameterException e) {
			OUTER: switch(promptOption) {
				case MISSING:
				case FIX:
					switch(e.getType()) {
						case CONVERSION:
						case MISSING:
							if(paramPrompts.size() > 0) {
								log.info("Parameter Exception - forwarding to prompt page", e);
								form.setAttribute("simple.db.ParameterException", e);
								sendPromptPage(executeEngine, report, scene, executor, promptOption, form, paramPrompts, parameters, keepParamNames, response);
								break OUTER;
							}
					}
				default:
					throw new ServletException("Parameter Exception not fixable", e);
			}
		} catch(IOException e) {
			throw new ServletException("Could not write to response stream", e);
		} catch(ExecuteException e) {
			throw new ServletException("Could not build the report", e);
		} catch(DesignException e) {
			throw new ServletException("Could not build the report", e);
		}
	}
}
