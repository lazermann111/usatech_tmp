/*
 * Created on Oct 12, 2006
 *
 */
package simple.falcon.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.falcon.engine.Folio;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

/**
 * @author bkrug
 *
 */
public class CheckFolioUpdatePrivsStep extends AbstractStep {
    public CheckFolioUpdatePrivsStep() {
        super();
    }
    
    /**
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        try {
            Folio folio = (Folio) form.getAttribute("folio");
            if(folio == null) throw new ServletException("No folio to save!");
            FolioStepHelper.checkFolioUpdatePrivs(folio, form);
        } catch(Throwable e) {
            throw new ServletException(e);
        }
    }
}
