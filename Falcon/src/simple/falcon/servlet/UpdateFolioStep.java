/*
 * Created on Nov 22, 2004
 *
 */
package simple.falcon.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

/**
 * @author bkrug
 *
 */
public class UpdateFolioStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    private boolean pillars = true;
    private boolean titles = true;
    private boolean outputType = true;
    private boolean chartConfig = true;
    private boolean directives = true;
    private String filters = "add";
    private boolean checkPrivs = false;
    /**
     * 
     */
    public UpdateFolioStep() {
        super();
    }
    
    /* (non-Javadoc)
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        try {
            Folio folio = (Folio) form.getAttribute("folio");
            ExecuteEngine executeEngine = (ExecuteEngine)form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
            StandardDesignEngine designEngine = (StandardDesignEngine)executeEngine.getDesignEngine();
            if(folio == null) {
                folio = designEngine.newFolio();
                form.setAttribute("folio", folio);
                log.debug("Creating new folio");
            } else if(isCheckPrivs()) {
            	FolioStepHelper.checkFolioUpdatePrivs(folio, form);
            }
            log.debug("Updating folio " + (folio.getFolioId() == null ? "NEW" : String.valueOf(folio.getFolioId())));
            if(pillars) {
                log.debug("Updating pillars");
                FolioStepHelper.addBlankPillars(folio, form, designEngine);
                FolioStepHelper.updatePillar(folio, form, designEngine);
            }
            if("ADD".equalsIgnoreCase(filters)) {
                log.debug("Adding filters");
                FolioStepHelper.addFilters(folio, form, designEngine);
            } else if("REPLACE".equalsIgnoreCase(filters)) {
                log.debug("Replacing filters");
                FolioStepHelper.replaceFilters(folio, form, designEngine);
            }
            if(outputType) {
                int outputType = form.getInt("outputType", false, 0);
                if(outputType > 0) {
                    log.debug("Setting output type");
                    folio.setOutputType(designEngine.getOutputType(outputType));
                }
            }
            if(folio.getOutputType() == null) folio.setOutputType(designEngine.getOutputType(22)); 
            
            if(chartConfig) {
            	if(form.getBoolean("doChartConfigUpdate",false,false)){
                    log.debug("Configuring chart");
                    FolioStepHelper.chartConfig(folio,form,designEngine);
            	}
            }
            
            if(titles) {
                log.debug("Updating titles");
                FolioStepHelper.updateTitles(folio, form, designEngine); 
            }
            if(directives) {
            	log.debug("Updating directives");
                FolioStepHelper.updateDirectives(folio, form);
            }
        } catch(ServletException se) {
            log.warn("Couldn't retrieve folio", se);
            throw se;
        } catch(Throwable e) {
            log.warn("Couldn't retrieve folio", e);
            throw new ServletException(e);
        }
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public boolean isOutputType() {
        return outputType;
    }

    public void setOutputType(boolean outputType) {
        this.outputType = outputType;
    }

    public boolean isPillars() {
        return pillars;
    }

    public void setPillars(boolean pillars) {
        this.pillars = pillars;
    }

    public boolean isTitles() {
        return titles;
    }

    public void setTitles(boolean titles) {
        this.titles = titles;
    }
    
    public void setAll(boolean all) {
        setFilters("add");
        setOutputType(all);
        setPillars(all);
        setTitles(all);
    }

	public boolean isDirectives() {
		return directives;
	}

	public void setDirectives(boolean directives) {
		this.directives = directives;
	}

	public boolean isCheckPrivs() {
		return checkPrivs;
	}

	public void setCheckPrivs(boolean checkPrivs) {
		this.checkPrivs = checkPrivs;
	}
}
