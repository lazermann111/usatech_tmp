/*
 * Created on Jan 17, 2006
 *
 */
package simple.falcon.servlet;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import simple.db.ParameterException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsType;
import simple.falcon.engine.Scene;
import simple.falcon.run.ReportRunner;
import simple.falcon.xml.ReportAdapter;
import simple.falcon.xml.ReportInputSource;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.translator.Translator;
import simple.util.StaticCollection;

public class ReportSourceStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();
    protected static Collection<String> skipParameters = StaticCollection.create("username", "password", "requestHandler", "translator");
    protected static OutputType outputType = new OutputType() {
		public String generateFileName(Folio folio, Map<String,?> requestParamValues) {
			return null;
		}
		public String generateFileName(Report report, Map<String,?> requestParamValues) {
			return null;
		}

		public String getCategory() {
			return "source";
		}

		public String getContentType() {
			return "text/xml";
		}

		public String getLabel() {
			return "Xml Source";
		}

		public int getOutputTypeId() {
			return 0;
		}

		public String getPath() {
			return null;
		}

		public void setPath(String path) {
		}

		public String getProtocol() {
			return "xml-src";
		}
		
		public String generateFullTitle(Report report, Map<String, ?> requestParamValues) {
			return null;
		}
		public String generateFullTitle(Folio folio, Map<String, ?> requestParamValues) {
			return null;
		}
    };
    protected String result;
    protected Long folioId;
    protected Long reportId;

    public ReportSourceStep() {
        super();
    }

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		ExecuteEngine executeEngine = (ExecuteEngine)form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
        //Report report = (Report) form.getAttribute("report");
		Translator translator = form.getTranslator();
        Executor executor = (Executor)form.getAttribute(SimpleServlet.ATTRIBUTE_USER, "session");
        Scene scene = FolioStepHelper.createScene(form, request, translator);
        Map<String,Object> parameters = new HashMap<String,Object>();
        for(Enumeration<?> en = request.getParameterNames(); en.hasMoreElements(); ) {
            String name = en.nextElement().toString();
            if(!skipParameters.contains(name)) {
                String[] values =  request.getParameterValues(name);
                if(values != null) {
                    switch(values.length) {
                        case 0: break;
                        case 1: parameters.put(name, values[0]); break;
                        default: parameters.put(name, values);
                    }
                }
            }
        }
        parameters.put("resultsType", "ARRAY");
		Source source;
		try {
			Report report = getReport(executeEngine, translator);
			ReportRunner.adjustSceneForReport(scene, report);
			Results[] results = executeEngine.buildResults(report, parameters, executor, scene, ResultsType.FORWARD);
			source = new SAXSource(new ReportAdapter<ReportInputSource>(), new ReportInputSource(report, results, parameters, executeEngine.getDesignEngine().getColumnNamer(), executor, scene, outputType));
		} catch(ParameterException e) {
			log.warn("Couldn't create report", e);
            throw new ServletException(e);
		} catch(ExecuteException e) {
			log.warn("Couldn't create report", e);
            throw new ServletException(e);
		} catch(DesignException e) {
			log.warn("Couldn't create report", e);
            throw new ServletException(e);
		}
		form.setAttribute(getResult(), source);
	}

	protected Report getReport(ExecuteEngine executeEngine, Translator translator) throws ServletException, DesignException {
		if(reportId != null) {
			return executeEngine.getDesignEngine().getReport(reportId, translator);
		} else if(folioId != null) {
			Folio folio = executeEngine.getDesignEngine().getFolio(folioId, translator);
            Report report = executeEngine.getDesignEngine().newReport(translator);
            report.setFolios(new Folio[] {folio});
            report.setName(folio.getName());
            report.setOutputType(folio.getOutputType());
            for(String dir : folio.getDirectiveNames())
                report.setDirective(dir, folio.getDirective(dir));
            return report;
		} else {
			throw new ServletException("Neither reportId nor folioId is configured");
		}
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Long getFolioId() {
		return folioId;
	}

	public void setFolioId(Long folioId) {
		this.folioId = folioId;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
}
