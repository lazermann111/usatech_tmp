/*
 * Created on Nov 22, 2004
 *
 */
package simple.falcon.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Report;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.translator.Translator;

/**
 * @author bkrug
 *
 */
public class RetrieveReportStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    private boolean required = true;
    private boolean useCache = false;
    /**
     * 
     */
    public RetrieveReportStep() {
        super();
    }
    
    /* (non-Javadoc)
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        try {
            ExecuteEngine executeEngine = (ExecuteEngine)form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
            DesignEngine designEngine = executeEngine.getDesignEngine();           
            Report report;
            //try reportid
            long reportId = form.getLong("reportId", false, 0);
			Translator translator = form.getTranslator();
            if(reportId != 0) {
                if(useCache) {
                    report = (Report)form.getAttribute("report", "session");
                    if(report != null && report.getReportId() != null && report.getReportId().longValue() == reportId) {
                        log.debug("Found report in session and using it");
                        return;
                    }
                }
                report = designEngine.getReport(reportId, translator);
                form.setAttribute("report", report, (useCache ? "session" : "request"));
                log.debug("Retrieved report from database"); 
            } else {
                long[] folioIds = form.getLongArray("folioIds", false);
                if(folioIds != null && folioIds.length > 0) {
                    report = designEngine.newReport(translator);
                    Folio[] folios = new Folio[folioIds.length];
                    for(int i = 0; i < folioIds.length; i++) {
                        folios[i] = designEngine.getFolio(folioIds[i], translator);
                    }
                    report.setFolios(folios);
                    report.setName(folios[0].getName());
                    report.setOutputType(folios[0].getOutputType());
                    for(String dir : folios[0].getDirectiveNames())
                        report.setDirective(dir, folios[0].getDirective(dir));
                    //report.setSubtitle(folios[0].getSubtitle());
                    //report.setTitle(folios[0].getTitle());
                } else {
                    long folioId = form.getLong("folioId", false, 0);
                    if(folioId != 0) {
                        Folio folio = designEngine.getFolio(folioId, translator);
                        report = designEngine.newReport(translator);
                        report.setFolios(new Folio[] {folio});
                        report.setName(folio.getName());
                        report.setOutputType(folio.getOutputType());
                        for(String dir : folio.getDirectiveNames())
                            report.setDirective(dir, folio.getDirective(dir));
                        //report.setSubtitle(folio.getSubtitle());
                        //report.setTitle(folio.getTitle());                        
                        form.setAttribute("folio", folio, (useCache ? "session" : "request"));
                        log.debug("Retrieved folio from database");  
                    } else if(isRequired()) {
                        throw new ServletException("Parameter, 'reportId', is missing");
                    } else if(useCache) {
                        report = designEngine.newReport();
                    } else {
                        return;
                    }
                }
            }
            
            form.setAttribute("report", report, (useCache ? "session" : "request"));
        } catch(ServletException se) {
            log.warn("Couldn't retrieve report", se);
            throw se;
        } catch(DesignException e) {
            log.warn("Couldn't retrieve report", e);
            throw new ServletException(e);
        }
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @return Returns the useCache.
     */
    public boolean isUseCache() {
        return useCache;
    }

    /**
     * @param useCache The useCache to set.
     */
    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }
}
