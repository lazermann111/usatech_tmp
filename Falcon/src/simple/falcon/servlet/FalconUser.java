/*
 * Created on Jan 21, 2005
 *
 */
package simple.falcon.servlet;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import simple.falcon.engine.Executor;
import simple.servlet.BasicServletUser;

/**
 * @author bkrug
 *
 */
public class FalconUser extends BasicServletUser implements Executor {
    private static final long serialVersionUID = -9073226818316442941L;
    protected long[] userGroupIds;
    protected long userId;
    protected TimeZone timeZone;
    protected Map<String, String> userProperties = new HashMap<String, String>();
    
    /**
     * 
     */
    public FalconUser() {
        super();
    }

    /**
     * @return Returns the userGroupIds.
     */
    public long[] getUserGroupIds() {
        return userGroupIds;
    }
    /**
     * @param userGroupIds The userGroupIds to set.
     */
    public void setUserGroupIds(long[] userGroupIds) {
        this.userGroupIds = userGroupIds;
    }
    /**
     * @return Returns the userId.
     */
    public long getUserId() {
        return userId;
    }
    /**
     * @param userId The userId to set.
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return Returns the userProperties.
     */
    public Map<String, String> getUserProperties() {
        return userProperties;
    }

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}      

	public String getDisplayName() {
		return getFullName();
	}
}
