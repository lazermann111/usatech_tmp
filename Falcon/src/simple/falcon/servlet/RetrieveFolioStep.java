/*
 * Created on Nov 22, 2004
 *
 */
package simple.falcon.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.translator.Translator;

/**
 * @author bkrug
 *
 */
public class RetrieveFolioStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    private boolean required = true;
    private boolean useCache = false;
    /**
     * 
     */
    public RetrieveFolioStep() {
        super();
    }
    
    /* (non-Javadoc)
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        try {
            long folioId = form.getLong("folioId", isRequired(), 0);
            Folio folio;
            if(useCache) {
                folio = (Folio)form.getAttribute("folio", "session");
                if(folio != null && (folio.getFolioId() == null ? 0 : folio.getFolioId().longValue()) == folioId) {
                    log.debug("Found folio in session and using it");
                    return;
                }
            }
            ExecuteEngine executeEngine = (ExecuteEngine)form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
            DesignEngine designEngine = executeEngine.getDesignEngine();   
			Translator translator = form.getTranslator();
            if(folioId != 0) {
                folio = designEngine.getFolio(folioId, translator);
    	        form.setAttribute("folio", folio, (useCache ? "session" : "request"));
                log.debug("Retrieved folio " + folioId  + " from database");               
            } else if(isRequired()) {
                throw new ServletException("Parameter, 'folioId', is missing");
            } else if(useCache) {
                form.setAttribute("folio", designEngine.newFolio(), "session");
            }
            //remove report from session in case it is there
            form.setAttribute("report", null, "session");
        } catch(ServletException se) {
            log.warn("Couldn't retrieve folio", se);
            throw se;
        } catch(Throwable e) {
            log.warn("Couldn't retrieve folio", e);
            throw new ServletException(e);
        }
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @return Returns the useCache.
     */
    public boolean isUseCache() {
        return useCache;
    }

    /**
     * @param useCache The useCache to set.
     */
    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }
}
