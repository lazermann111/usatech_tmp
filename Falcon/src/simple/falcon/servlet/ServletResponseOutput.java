/*
 * Created on Sep 16, 2005
 *
 */
package simple.falcon.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import simple.falcon.engine.Output;

public class ServletResponseOutput implements Output {
    private static enum WriterOrStream { NONE, WRITER, STREAM };
    protected HttpServletResponse response;
    protected WriterOrStream writerOrStream = WriterOrStream.NONE;
    
    public ServletResponseOutput(HttpServletResponse response) {
        this.response = response;
    }
    
    public Writer getWriter() throws IOException {
        Writer w = response.getWriter();
        writerOrStream = WriterOrStream.WRITER;
        return w;
    }

    public OutputStream getOutputStream() throws IOException {
        OutputStream os = response.getOutputStream();
        writerOrStream = WriterOrStream.STREAM;
        return os;
    }

    public void flush() throws IOException {
        switch(writerOrStream) {
            case WRITER: getWriter().flush(); break;
            case STREAM: getOutputStream().flush(); break;
        }
    }

	public Result getResult() throws IOException {
		return new StreamResult(getWriter());
	}

	public void next() throws IOException {
	}
	
}
