package simple.falcon.servlet;

import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Param;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.run.ReportRunner;
import simple.io.LoadingException;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.servlet.steps.TransformStep;
import simple.translator.Translator;
import simple.util.DynaBeanMap;
import simple.util.StaticCollection;
import simple.xml.TemplatesLoader;
import simple.xml.sax.AggregatingAdapter;
import simple.xml.sax.AggregatingInputSource;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanInputSource;
import simple.xml.sax.ParametersAdapter;
import simple.xml.sax.ParametersInputSource;
import simple.xml.serializer.XHTMLHandler;

/**
 * Base class to run a report
 *
 * @author bkrug
 *
 */
public abstract class AbstractRunReportStep extends AbstractStep {
	private static final Log log = Log.getLog();
	protected String promptXSL = "resource:/simple/falcon/templates/report/show-params-plus.xsl";
	protected final Collection<String> skipParameters = StaticCollection.create("username", "password", "requestHandler", "translator");
	protected final Set<String> keepParamNames = new HashSet<String>();
	protected String path;
	protected int refreshInterval = 15000;
	protected int outputTypeId;
	protected static RunReportPreprocessor preprocessor;

	public AbstractRunReportStep() {
		super();
		keepParamNames.add("selectedMenuItem");
	}

	protected Report getReport(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		Report report = (Report) form.getAttribute("report");
		if(report == null) {
			Folio folio = (Folio) form.getAttribute("folio");
			if(folio == null)
				throw new ServletException("You must provide the folio for this step");
			log.debug("Running folio " + (folio.getFolioId() == null ? "NEW" : String.valueOf(folio.getFolioId())));
			try {
				report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
			} catch(DesignException e) {
				throw new ServletException(e);
			}
			report.setFolios(new Folio[] { folio });
			report.setName(folio.getName());
			report.setOutputType(folio.getOutputType());
			for(String dir : folio.getDirectiveNames())
				report.setDirective(dir, folio.getDirective(dir));
			form.setAttribute("report", report);
		} else {
			log.debug("Running report " + (report.getReportId() == null ? "NEW" : String.valueOf(report.getReportId())));
		}
		adjustOutputType(report, form, executeEngine);
		ReportRunner.adjustSceneForReport(scene, report);
		return report;
	}

	protected void adjustOutputType(Report report, InputForm form, ExecuteEngine executeEngine) throws ServletException {
		int outputTypeId = form.getInt("outputType", false, getOutputTypeId());
		if(outputTypeId > 0) {
			OutputType outputType;
			try {
				outputType = executeEngine.getDesignEngine().getOutputType(outputTypeId);
			} catch(DesignException e) {
				throw new ServletException(e);
			}
			if(outputType != null)
				report.setOutputType(outputType);
		}
		if(getPath() != null) {
			report.getOutputType().setPath(getPath());
		}
	}

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
		Executor executor = (Executor) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, "session");
		if(executor.getUserGroupIds() == null || executor.getUserGroupIds().length == 0)
			throw new ServletException("You do not have access to run reports");
		
		int selectedMenuItem = form.getInt("selectedMenuItem", false, 0);
		if (selectedMenuItem <= 0) {
			Integer previousMenuItem = null;
			try {
				previousMenuItem = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_SELECTED_MENU_ITEM, Integer.class, false, "session");
			} catch (ConvertException | ServletException e) {
				log.warn("Error getting simple.servlet.SelectedMenuItem value", e);
			}
	    	selectedMenuItem = previousMenuItem == null ? 0 : previousMenuItem.intValue();
	    	if (selectedMenuItem > 0)
	    		form.set("selectedMenuItem", selectedMenuItem);
		}		

		Translator translator = form.getTranslator();
		Scene scene = FolioStepHelper.createScene(form, request, translator);
		Report report = getReport(executeEngine, form, scene);
		RunReportPreprocessor rrp = getPreprocessor();
		if(rrp != null)
			rrp.preprocess(report, scene, executor, form);
		handleReportRequest(executeEngine, report, scene, executor, new DynaBeanMap(form), form, request, response);
	}

	protected Set<String> getKeepParamNames(InputForm form) {
		return keepParamNames;
	}

	public String[] getKeepParamNames() {
		return keepParamNames.toArray(new String[keepParamNames.size()]);
	}

	public void setKeepParamNames(String[] keepParamNames) {
		this.keepParamNames.clear();
		this.keepParamNames.addAll(Arrays.asList(keepParamNames));
	}

	protected void handleReportRequest(ExecuteEngine executeEngine, Report report, Scene scene, Executor executor, Map<String, Object> reportParams, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// check that necessary parameters are specified
		PromptOptions po = PromptOptions.getByValue(form.getString("promptForParams", false));
		if(po == null)
			po = PromptOptions.MISSING;

		Map<String, Param> paramPrompts = new LinkedHashMap<String, Param>();
		Map<String, Object> resultParams = new HashMap<String, Object>();
		Iterable<String> keepParamNames = getKeepParamNames(form);
		try {
			EngineUtils.extractParamsForPrompt(report, scene, reportParams, resultParams, paramPrompts, keepParamNames);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}

		boolean prompt = false;
		switch(po) {
			case ALWAYS:
				prompt = (paramPrompts.size() > 0);
				break;
			case MISSING:
				if(paramPrompts.size() > 0) {
					for(Param param : paramPrompts.values()) {
						if(param.getValue() == null && reportParams.get(EngineUtils.PARAMS_PREFIX + param.getName()) == null) {
							prompt = true;
							break;
						}
					}
				}
				break;
		}
		
		if(prompt) {
			sendPromptPage(executeEngine, report, scene, executor, po, form, paramPrompts, resultParams, keepParamNames, response);
		} else {
			sendReportPage(executeEngine, report, scene, executor, po, form, paramPrompts, resultParams, keepParamNames, response);
		}
	}

	protected void sendReportPage(Executor executor, InputForm form, Map<String, Object> reportParams, HttpServletResponse response) throws ServletException {
    	sendReportPage(null, null, null, executor, null, form, null,reportParams, null, response);
    }
	/**
	 * @param report
	 * @param scene
	 * @param form
	 * @param paramPrompts
	 * @param reportParams
	 * @param response
	 * @throws ServletException
	 */
	protected abstract void sendReportPage(ExecuteEngine executeEngine, Report report, Scene scene, Executor executor, PromptOptions promptOption, InputForm form, Map<String, Param> paramPrompts, Map<String, Object> parameters, Iterable<String> keepParamNames, HttpServletResponse response) throws ServletException ;

	protected void sendPromptPage(ExecuteEngine executeEngine, Report report, Scene scene, Executor executor, PromptOptions promptOption, InputForm form, Map<String, Param> paramPrompts, Map<String, Object> parameters, Iterable<String> keepParamNames, HttpServletResponse response)
			throws ServletException {
		try {
			URL url = executeEngine.getResourceResolver().getResourceURL(getPromptXSL());
			Transformer transformer = null;
			boolean sourceOnly = form.getBoolean(TransformStep.ATTRIBUTE_SOURCE_ONLY, false, false);
			if(sourceOnly) {
				transformer = TemplatesLoader.getInstance(null).newTransformer();
				response.setContentType("text/xml");
			} else {
				transformer = TemplatesLoader.getInstance(scene.getTranslator()).newTransformer(url, refreshInterval);
				response.setContentType("text/html");
			}
			transformer.setParameter("context", form);
			String token = ConvertUtils.getStringSafely(scene.getProperties().get("session-token"));
        	if(token != null)
        		transformer.setOutputProperty(XHTMLHandler.SESSION_TOKEN, token);
            
			Writer out = response.getWriter();
			Object params = paramPrompts.values();
			Source[] sources = new Source[] { new SAXSource(new ParametersAdapter(), new ParametersInputSource(parameters)),
					new SAXSource(new BeanAdapter1(), new BeanInputSource(params, "params")) };
			String[] xpaths = new String[] { "parameters", "param-prompts" };
			ServletRequest request = form.getRequest();
			request.getRequestDispatcher("include/header.jsp").include(request, response);
			transformer.transform(new SAXSource(new AggregatingAdapter(), new AggregatingInputSource(sources, xpaths, null)), new StreamResult(out));
			request.getRequestDispatcher("include/footer.jsp").include(request, response);
			out.flush();
		} catch(IOException e) {
			throw new ServletException(e);
		} catch(TransformerConfigurationException e) {
			throw new ServletException(e);
		} catch(TransformerException e) {
			throw new ServletException(e);
		} catch(LoadingException e) {
			throw new ServletException(e);
		}
	}

	public String getPromptXSL() {
		return promptXSL;
	}

	public void setPromptXSL(String promptXSL) {
		this.promptXSL = promptXSL;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getRefreshInterval() {
		return refreshInterval;
	}

	public void setRefreshInterval(int refreshInterval) {
		this.refreshInterval = refreshInterval;
	}

	public static RunReportPreprocessor getPreprocessor() {
		return preprocessor;
	}

	public static void setPreprocessor(RunReportPreprocessor preprocessor) {
		AbstractRunReportStep.preprocessor = preprocessor;
	}

	public int getOutputTypeId() {
		return outputTypeId;
	}

	public void setOutputTypeId(int outputTypeId) {
		this.outputTypeId = outputTypeId;
	}
}
