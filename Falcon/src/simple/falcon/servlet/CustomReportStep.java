/*
 * Created on Jan 17, 2006
 *
 */
package simple.falcon.servlet;

import simple.falcon.engine.standard.StandardOutputType;

public class CustomReportStep extends InlineRunReportStep {
    protected StandardOutputType outputType = new StandardOutputType();
    public CustomReportStep() {
        super();
        //set defaults
        setContentType("text/html");
        setPath("resource:simple/falcon/templates/report/show-report-html.xsl");
        setProtocol("xsl");
    }
    public String getContentType() {
        return outputType.getContentType();
    }
    @Override
	public String getPath() {
        return outputType.getPath();
    }
    public String getProtocol() {
        return outputType.getProtocol();
    }
    public void setContentType(String contentType) {
        outputType.setContentType(contentType);
    }
    @Override
	public void setPath(String path) {
        outputType.setPath(path);
    }
    public void setProtocol(String protocol) {
        outputType.setProtocol(protocol);
    }
}
