<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:x="http://simple/translate/1.0"	 
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="b x">
	<xsl:output method="xhtml"/>	
	<xsl:template match="/">
<html><head>
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /><title>Error Occurred on Server</title>
</head>
<body><p style="font-family: Verdana; font-size: 10px; color: red"><x:translate><b>Application Error:</b> An error occurred on the server. We are working to resolve this. Please try again later.</x:translate></p>
<xsl:value-of select="*/b:message"/>
</body>
</html>
	</xsl:template>
</xsl:stylesheet>