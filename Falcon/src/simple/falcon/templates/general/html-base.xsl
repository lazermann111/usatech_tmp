<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestInfo"
	xmlns:str="http://exslt.org/strings"
	xmlns:cmn="http://exslt.org/common"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect p a str cmn x2 req">
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	<xsl:variable name="baseUri" select="string(req:getBaseUrl($context))"/>
	<xsl:template match="/">
		<xsl:call-template name="preprocess"/>
		<xsl:call-template name="document"/>
	</xsl:template>

	<xsl:template name="document">
		<html xml:lang="en" lang="en">
			<head>
				<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
				<xsl:variable name="subtitle"><xsl:call-template name="subtitle"/></xsl:variable>
				<title><xsl:call-template name="title"/><xsl:if test="string-length(normalize-space($subtitle)) &gt; 0"> - <xsl:value-of select="$subtitle"/></xsl:if></title>
				<xsl:call-template name="base"/>
				<xsl:call-template name="styles"/>
				<xsl:call-template name="scripts"/>
				<xsl:call-template name="page-header"/>
			</head>
			<xsl:call-template name="body"/>
		</html>
	</xsl:template>

	<xsl:template name="preprocess"/>

	<xsl:template name="title">Falcon Reporting Engine</xsl:template>
	<xsl:template name="subtitle"/>

	<xsl:template name="uri">
		<xsl:param name="uri"/>
		<xsl:value-of select="req:addLastModifiedToUri($context, string($uri))"/>
	</xsl:template>

	<xsl:template name="base">
		<base href="{$baseUri}" />
	</xsl:template>

	<xsl:template name="styles">
		<xsl:call-template name="stylesheet-x">
			<xsl:with-param name="stylesheet-list"><x2:translate key="app-stylesheets"/></xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="stylesheet">
			<xsl:with-param name="property" select="'simple.servlet.ServletUser.userProperties.CUSTOM_CSS_PATH'"/>
		</xsl:call-template>
		<xsl:call-template name="extra-styles"/>
	</xsl:template>

	<xsl:template name="extra-styles"/>

	<xsl:template name="scripts">
		<script type="text/javascript" src="{req:addLastModifiedToUri($context, 'scripts/common.js')}"/>
		<xsl:call-template name="extra-scripts"/>
	</xsl:template>

	<xsl:template name="extra-scripts"/>

	<xsl:template name="page-header"/>

	<xsl:template name="body">
		<body>
			<xsl:call-template name="content-header"/>
			<xsl:call-template name="contents"/>
			<xsl:call-template name="content-footer"/>
		</body>
	</xsl:template>

	<xsl:template name="content-header"/>
	<xsl:template name="contents"/>
	<xsl:template name="content-footer"/>

	<xsl:template name="set-user-agent">
		<xsl:processing-instruction name="client">
			<xsl:text>user-agent="</xsl:text>
			<xsl:value-of select="req:getUserAgent($context)" />
			<xsl:text>"</xsl:text>
		</xsl:processing-instruction>
	</xsl:template>

	<xsl:template name="get-locale">
		<xsl:value-of select="req:getLocale($context)" />
	</xsl:template>
	
	<xsl:template name="stylesheet">
		<xsl:param name="property"/>
		<xsl:for-each select="str:split(normalize-space(reflect:getProperty($context, $property)), ',')">
			<xsl:if test="string-length(normalize-space(.)) > 0">
				<link href="{req:addLastModifiedToUri($context, normalize-space(.))}" type="text/css" rel="stylesheet"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="stylesheet-x">
		<xsl:param name="stylesheet-list"/>
		<xsl:for-each select="str:split(normalize-space($stylesheet-list), ',')">
			<xsl:if test="string-length(normalize-space(.)) > 0">
				<link href="{req:addLastModifiedToUri($context, normalize-space(.))}" type="text/css" rel="stylesheet"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="script-x">
		<xsl:param name="script-list"/>
		<xsl:for-each select="str:split(normalize-space($script-list), ',')">
			<xsl:if test="string-length(normalize-space(.)) > 0">
				<script type="text/javascript" src="{req:addLastModifiedToUri($context, normalize-space(.))}"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- Useful template to call for getting current request parameters -->
	<xsl:template name="parameters-xml">
		<xsl:copy-of select="*/a:parameters/p:parameters"/>
	</xsl:template>

	<xsl:template name="parameters-form">
		<xsl:param name="overrides"/>
		<xsl:param name="parameters" select="*/a:parameters/p:parameters/p:parameter"/>
		<xsl:variable name="override-set" select="cmn:node-set($overrides)/p:override"/>
		<xsl:variable name="ignore-set" select="cmn:node-set($overrides)/p:ignore"/>
		<xsl:for-each select="$override-set[@value]">
			<input type="hidden" name="{@name}" value="{@value}"/>
		</xsl:for-each>
		<xsl:for-each select="$parameters[string-length(@p:value) > 0]">
			<xsl:variable name="name" select="@p:name"/>
			<xsl:choose>
				<xsl:when test="@p:name = 'session-token'" />
				<xsl:when test="not($override-set[@name = $name]) and not($ignore-set[@name = $name])">
					<input type="hidden" name="{@p:name}" value="{@p:value}"/>
				</xsl:when>
				<xsl:otherwise />
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="parameters-query">
		<xsl:param name="overrides"/>
		<xsl:param name="parameters" select="*/a:parameters/p:parameters/p:parameter"/>
		<xsl:variable name="override-set" select="cmn:node-set($overrides)/*"/>
		<xsl:call-template name="query-string">
			<xsl:with-param name="parameters">
				<xsl:for-each select="$override-set[string-length(@value) > 0]">
					<parameter name="{@name}" value="{@value}"/>
				</xsl:for-each>
				<xsl:for-each select="$parameters[string-length(@p:value) > 0]">
					<xsl:variable name="name" select="@p:name"/>
					<xsl:if test="not($override-set[@name = $name])">
						<parameter name="{@p:name}" value="{@p:value}"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="query-string">
		<xsl:param name="parameters"/>
		<xsl:for-each select="cmn:node-set($parameters)/*"><xsl:if test="position() > 1">&amp;</xsl:if><xsl:value-of select="@name"/>=<xsl:value-of select="@value"/></xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
