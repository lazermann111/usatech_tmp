<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:b="http://simple/bean/1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect b">
	<xsl:output method="xhtml"/>
	<xsl:param name="context"/>

	<xsl:template match="/">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title>Please Log On!</title>
<link rel="StyleSheet" href="css/default-style.css" type="text/css"/>
<base href="{reflect:getProperty($context, 'request.requestURL')}"/>
</head>
<body class="content">
<form name="info" method="POST" action="login.i">
<xsl:comment>Attributes in request: <xsl:value-of select="reflect:toPropertyMap($context)"/></xsl:comment>
		<xsl:for-each select="*/b:parameters/*[local-name() != 'userName' and local-name() != 'password' and local-name() != 'simple.servlet.steps.LogonStep.forward']">
	<input type="hidden" name="{local-name()}" value="{.}"/>
		</xsl:for-each>
		<xsl:variable name="forward" select="*/b:parameters/b:simple.servlet.steps.LogonStep.forward"/>
		<xsl:choose>
			<xsl:when test="$forward='/logout' or $forward='/login' or $forward='/login_prompt' or $forward='' or not($forward)">
				<xsl:comment>Forward is blank or a login/logout value; use search path</xsl:comment>
				<xsl:variable name="forward" select="reflect:getProperty($context, 'simple.servlet.SimpleServlet.SearchPath')"/>
	 			<xsl:choose>
	 				<xsl:when test="$forward='/logout' or $forward='/login' or $forward='/login_prompt' or $forward='' or not($forward)">
						<xsl:comment>Forward is blank or a login/logout value; use default</xsl:comment>
						<xsl:call-template name="forward">
							<xsl:with-param name="forward" select="'/home'"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="forward">
							<xsl:with-param name="forward" select="$forward"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
	 		</xsl:when>
	 		<xsl:otherwise>
 				<xsl:call-template name="forward">
					<xsl:with-param name="forward" select="$forward"/>
				</xsl:call-template>
	 		</xsl:otherwise>
		</xsl:choose>
<!--
for(java.util.Enumeration enum = request.getParameterNames(); enum.hasMoreElements(); ) {
    String s = (String)enum.nextElement();
    if("username".equalsIgnoreCase(s) || "password".equalsIgnoreCase(s) || simple.servlet.steps.AbstractLogonStep.ATTRIBUTE_FORWARD.equalsIgnoreCase(s)) continue;
    String[] v = request.getParameterValues(s);
    for(int i = 0; i < v.length; i++) {
        %><input type=hidden name="<%=s%>" value="<%=v[i]%>"/><%
    }
}
Object forward = request.getAttribute("simple.servlet.steps.LogonStep.forward");
if(forward == null || forward.toString().trim().length() == 0) {
    forward = request.getAttribute("simple.servlet.SimpleServlet.SearchPath");
    if(forward == null || forward.toString().trim().length() == 0 || forward.toString().equals("/logout") || forward.toString().equals("/login")) forward = "/home";
}
String focusTo = "userName";
if(exception instanceof simple.servlet.steps.LoginFailureException) {
String msg = "Log-in Failed";
switch(((simple.servlet.steps.LoginFailureException)exception).getType()) {
    case simple.servlet.steps.LoginFailureException.INVALID_USERNAME: focusTo = "userName"; msg = "User does not exist"; break;
    case simple.servlet.steps.LoginFailureException.INVALID_PASSWORD: focusTo = "password"; msg = "Invalid password"; break;
}
%><p class="loginError"><%=msg%></p><%
}%>
<script type="text/javascript" defer="defer">document.forms[0].<%=focusTo%>.focus();document.forms[0].<%=focusTo%>.select();</script>
 --><h4>Please enter the following information to login to the system:</h4>
<table cellSpacing="1" cellPadding="1" valign="top" class="search">
    <tr>
      <th>User Name</th>
      <td><input type="text" name="userName" value="{reflect:getProperty($context, 'userName')}"/></td></tr>
    <tr>
      <th>Password</th>
      <td><input type="password" name="password" value="{reflect:getProperty($context, 'password')}" autocomplete="off"/></td></tr>
</table>
<!--<p><a href="<%=request.getContextPath()%>/forgotPassword.saf" onmouseover="window.status = 'Forgot User Name or Password'; return true;"
    onmouseout="window.status = ''">Click here if you forgot your User Name or Password</a><br></p>-->
<p><input type="submit" value="Logon"/></p>
</form>
</body>
</html>
	</xsl:template>

	<xsl:template name="forward">
		<xsl:param name="forward"/>
  		<xsl:comment>Forward='<xsl:copy-of select="$forward"/>'</xsl:comment>
		<input type="hidden" name="simple.servlet.steps.LogonStep.forward" value="{$forward}"/>
	</xsl:template>
</xsl:stylesheet>