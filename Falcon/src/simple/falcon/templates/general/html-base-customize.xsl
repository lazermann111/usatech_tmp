<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:import href="html-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:call-template name="extra-styles"/>
</xsl:stylesheet>
