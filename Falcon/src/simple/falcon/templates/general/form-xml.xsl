<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:param name="form"/>
	<xsl:template match="/">
		<xsl:for-each select="$form">
			<xsl:copy-of select="."/>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
