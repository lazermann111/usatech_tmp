<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:b="http://simple/bean/1.0"
	xmlns:str="http://simple/xml/extensions/java/java.lang.String"
	exclude-result-prefixes="b"
	version="1.0">

    <xsl:output method="xhtml"/>
    <xsl:template match="b:fieldHierarchy">
	  	<xsl:call-template name="fieldHierarchy">
	  		<xsl:with-param name="branch" select="."/>
	  	</xsl:call-template>
    </xsl:template>

    <xsl:template name="buildTree">
    	<xsl:param name="tree"/>
		<script type="text/javascript" defer="defer">
			function toggleTree(li) {
				if(li.className == "closedNode") {
					li.className = "openNode";
				} else {
					li.className = "closedNode";
				}
			}
		</script>
		<ul class="categoryNode" id="treeTop">
		  	<xsl:call-template name="fieldHierarchy">
		  		<xsl:with-param name="branch" select="$tree"/>
		  	</xsl:call-template>
	  	</ul>
	</xsl:template>

    <xsl:template name="fieldHierarchy">
	  	<xsl:param name="branch"/>
		<xsl:for-each select="$branch">
			<li class="closedNode"><a class="label" onclick="toggleTree(this.parentNode)"
		    		id="tree_{b:id}" title="{b:description}"><xsl:value-of select="b:name"/></a>
				<xsl:if test="b:id">
					<xsl:call-template name="group-action">
						<xsl:with-param name="groupId" select="b:id"/>
						<xsl:with-param name="groupLabel" select="b:name"/>
					</xsl:call-template>
				</xsl:if>
		        <div class="treeDiv">
		        <ul class="categoryNode">
				<xsl:for-each select="b:branches/b:branch">
				  	<xsl:sort select="str:toUpperCase(string(b:name))"/>
					<xsl:call-template name="fieldHierarchy">
				  		<xsl:with-param name="branch" select="."/>
				  	</xsl:call-template>
				</xsl:for-each>
		    	<xsl:for-each select="b:leafs/b:leaf">
		    	<xsl:sort select="str:toUpperCase(string(b:label))"/>
			    <li class="unchosenField" title="{b:description}"
			        id="field_{b:id}"
			        fieldId="{b:id}"
			        fieldLabel="{b:label}"
			        ><xsl:value-of select="b:label"/>&#160;
			        	<xsl:call-template name="field-action">
			        		<xsl:with-param name="fieldId" select="b:id"/>
			        		<xsl:with-param name="fieldLabel" select="b:label"/>
			        		<xsl:with-param name="fieldDescription" select="b:description"/>
			        	</xsl:call-template>
					</li>
				</xsl:for-each>
				</ul>
		      	</div>
		    	</li>
		</xsl:for-each>
    </xsl:template>

	<xsl:template name="field-action">
  		<a href="javascript:" onclick="selectField(this.parentNode);">display</a>
		<a href="javascript:" onclick="filterField(this.parentNode);">filter</a>
	</xsl:template>

	<xsl:template name="group-action">

	</xsl:template>
</xsl:stylesheet>
