<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:b="http://simple/bean/1.0"
	exclude-result-prefixes="b"
	version="1.0">
	<xsl:import href="design-base.xsl"/>
    <xsl:import href="field-tree.xsl"/>
    <xsl:output method="xhtml"/>

    <xsl:template name="contents">
    	<div id="topTreeDiv">
	  	<xsl:call-template name="buildTree">
	  		<xsl:with-param name="tree" select="*/b:field-hierarchy"/>
	  	</xsl:call-template>
		<a href="./edit_field.i" target="editFieldsFrame">New Field</a><br />
		<a href="./edit_field_category.i" target="editFieldsFrame">New Category</a>
	  	</div>
	  	<script type="text/javascript" defer="defer">
			//expand top level tree by default
	  		function toggleTopNode() {
		  		var ul = document.getElementById("topTreeDiv").getElementsByTagName("ul")[0];
		  		for(var i = 0; i &lt; ul.childNodes.length; i++) {
		  			if(ul.childNodes[i].nodeType == 1 &amp;&amp; ul.childNodes[i].nodeName.toLowerCase() == "li") {
		  				toggleTree(ul.childNodes[i]);
	  				}
  				}
	  		}

	  		toggleTopNode();
		</script>
    </xsl:template>

	<xsl:template name="field-action">
		<xsl:param name="fieldId"/>
		<xsl:param name="fieldLabel"/>
  		<xsl:text> (</xsl:text>
  		<a href="./edit_field.i?unframed=true&amp;fieldId={$fieldId}" target="editFieldsFrame">Edit</a>
		<xsl:text>)</xsl:text>
	</xsl:template>

	<xsl:template name="group-action">
		<xsl:param name="groupId"/>
		<xsl:param name="groupLabel"/>
  		<xsl:text> (</xsl:text>
		<a href="./edit_field_category.i?unframed=true&amp;categoryId={$groupId}" target="editFieldsFrame">Edit</a>
		<xsl:text>)</xsl:text>
	</xsl:template>
</xsl:stylesheet>
