<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:b="http://simple/bean/1.0"
	exclude-result-prefixes="b"
	version="1.0">
	<xsl:import href="design-base.xsl"/>
    <xsl:import href="field-tree.xsl"/>
    <xsl:output method="xhtml"/>

    <xsl:template name="contents">
    	<div id="topTreeDiv">
	  	<xsl:call-template name="buildTree">
	  		<xsl:with-param name="tree" select="*/b:field-hierarchy"/>
	  	</xsl:call-template>
	  	</div>
	  	<script type="text/javascript" defer="defer">
	  		var folioEditor = window.parent.document.getElementsByName("folioDesignFrame").item(0).contentWindow;

	  		function addFieldPillar(fieldId, fieldLabel) {
	  			folioEditor.addFieldPillar(fieldId);
	  		}
	  		function addField(fieldId, fieldLabel) {
	  			folioEditor.addField(fieldId);
	  		}
	  		function addFilter(fieldId, fieldLabel) {
	  			folioEditor.addFilter(fieldId, fieldLabel);
	  		}
	  		//expand top level tree by default
	  		function toggleTopNode() {
		  		var ul = document.getElementById("topTreeDiv").getElementsByTagName("ul")[0];
		  		for(var i = 0; i &lt; ul.childNodes.length; i++) {
		  			if(ul.childNodes[i].nodeType == 1 &amp;&amp; ul.childNodes[i].nodeName.toLowerCase() == "li") {
		  				toggleTree(ul.childNodes[i]);
	  				}
  				}
	  		}

	  		toggleTopNode();
		</script>
		<div><a target="_top" href="edit_fields_frame.i">Edit Fields</a></div>
    </xsl:template>

	<xsl:template name="field-action">
		<xsl:param name="fieldId"/>
		<xsl:param name="fieldLabel"/>
  		<a href="javascript: addFieldPillar({$fieldId}, '{$fieldLabel}')">Add Pillar</a>&#160;
  		<a href="javascript: addField({$fieldId}, '{$fieldLabel}')">Add Field</a>&#160;
  		<a href="javascript: addFilter({$fieldId}, '{$fieldLabel}')">Add Filter</a>
	</xsl:template>
</xsl:stylesheet>
