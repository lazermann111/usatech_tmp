<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils" 
	xmlns:a="http://simple/aggregating/1.0"  
	xmlns:r="http://simple/results/1.0" 
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a">
	<xsl:import href="design-base.xsl"/>
    <xsl:output method="xhtml"/>
    
	<xsl:param name="context" />
	
	<xsl:variable name="sessionToken" select="reflect:getProperty($context, 'simple.servlet.SessionToken')"/>
        
	<xsl:template name="extra-scripts">
	    <style type="text/css">
	        table.folio-list{border-collapse: collapse; width: 100%;}
	        .folio-list caption{border: 1px solid black; background: #DDDDDD; padding: 5px; font-weight: bold;}
	        .folio-list thead td{border: 1px solid black; background: #DDDDDD;}
	        .folio-list tbody td{border: 1px solid black; }
	    </style>
	    <script type="text/javascript">
	        function deleteFolio(folioId){
	            if(confirm("Are you sure you want to delete folio " + folioId + "?")){
	                document.location="delete_folio.i?folioId=" + folioId + "&amp;session-token=<xsl:value-of select="$sessionToken"/>";
	            }
	        }
	
	        function copyFolio(folioId){
	            if(confirm("Are you sure you want to copy folio " + folioId + "?")){
	                document.location="copy_folio.i?folioId=" + folioId + "&amp;session-token=<xsl:value-of select="$sessionToken"/>";
	            }
	        }
	    </script>
	</xsl:template>
    
    <xsl:template name="contents">
        <span class="caption">Folio Designer</span>
        <div class="spacer5"></div>
        <xsl:choose>
		    <xsl:when test="count(/a:base/a:folioList/r:results/r:row) &gt; 0">
		        <table class="sortable-table initializedTable" width="800">
		            <caption class="sectionTitle">Maintain Folios</caption>
		            <thead class="sectionTitle">
		                <tr class="gridHeader">
		                    <td>Report</td>
		                    <td>Edit</td>
		                    <td>Copy</td>
		                    <td>Delete</td>
		                </tr>
		            </thead>
		            <tbody>
		                <xsl:for-each select="/a:base/a:folioList/r:results/r:row">
		                    <tr>
		                        <td>
		                            <a>
		                                <xsl:attribute name="href">
		                                    <xsl:text>run_report_async.i?folioId=</xsl:text>
		                                    <xsl:value-of select="r:folioId" />
		                                </xsl:attribute>
		                                <xsl:choose>
		                                    <xsl:when test="r:name = ''">
		                                        <xsl:text>(No Name)</xsl:text>
		                                    </xsl:when>
		                                    <xsl:otherwise>
		                                        <xsl:value-of select="r:name" />
		                                    </xsl:otherwise>
		                                </xsl:choose>
		                            </a>
		                        </td>
		                        <td>
		                            <a>
		                                <xsl:attribute name="href">
		                                    <xsl:text>design_frame.i?folioId=</xsl:text>
		                                    <xsl:value-of select="r:folioId" />&amp;session-token=<xsl:value-of select="$sessionToken"/>
		                                </xsl:attribute>
		                                Edit
		                            </a>
		                        </td>
		                        <td>
		                            <a>
		                                <xsl:attribute name="href">
		                                    <xsl:text>javascript:copyFolio('</xsl:text>
		                                    <xsl:value-of select="r:folioId" />
		                                    <xsl:text>')</xsl:text>
		                                </xsl:attribute>
		                                Copy
		                            </a>
		                        </td>
		                        <td>
		                            <a>
		                                <xsl:attribute name="href">
		                                    <xsl:text>javascript:deleteFolio('</xsl:text>
		                                    <xsl:value-of select="r:folioId" />
		                                    <xsl:text>')</xsl:text>
		                                </xsl:attribute>
		                                Delete
		                            </a>
		                        </td>
		                    </tr>
		                </xsl:for-each>
		            </tbody>
		        </table>
		    </xsl:when>
		    <xsl:otherwise>
		        <div>No folios found.</div>
		    </xsl:otherwise>
		</xsl:choose>
		<hr />
		<a href="design_frame.i?session-token={$sessionToken}">New Folio</a>
		<a href="edit_fields_frame.i?session-token={$sessionToken}">Edit Fields</a>
    </xsl:template>
</xsl:stylesheet>