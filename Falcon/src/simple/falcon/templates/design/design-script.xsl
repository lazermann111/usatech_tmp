<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:b="http://simple/bean/1.0">
	<xsl:output method="text"/>	
	<xsl:template match="/">
<![CDATA[
var selectedFields = new Array();
var filteredFields = new Array();
var order = 1;
var filterCount = 0;

function toggleTree(li) {
	if(li.className == "closedNode") {
		li.className = "openNode";
		//alert("Toggle On");
	} else {
		li.className = "closedNode";
		//alert("Toggle Off");
	}
}

function selectField(a) {
	if(!selectedFields[a.fieldId]) {
		var index = order++;
		document.reportForm.fieldIds.value = document.reportForm.fieldIds.value + "," + a.fieldId;
		selectedFields[a.fieldId] = index;
		addSelectedField(a);
	}
	a.className = "chosenField";
}

function filterField(a, op, val) {
	var fieldId = a.fieldId;
	var fieldLabel = a.fieldLabel;
	if(!filteredFields[fieldId]) {
		var filterId = ++filterCount;
		filteredFields[fieldId] = filterId;
		var tr = document.getElementById("filterTable").insertRow(-1);
		var td = tr.insertCell();
		td.innerText = fieldLabel;
		var inp = document.createElement("input");
		inp.type = "hidden";
		inp.name = "filter_field_" + filterId;
		inp.value = fieldId;
		td.appendChild(inp);
		td = tr.insertCell();
		var sel = document.createElement("select");
		sel.onchange = new Function("selectOperator(this.options(this.selectedIndex))");
		sel.name = "filter_op_" + filterId;
		sel.filterId = filterId;
  		]]><xsl:for-each select="*/b:operators/b:operator">
			addOperator(sel, "<xsl:value-of select="b:op"/>",
				<xsl:value-of select="b:fieldIndex"/>,
				<xsl:value-of select="b:parameterCount"/>,
				"<xsl:value-of select="b:prefix"/>",
				"<xsl:value-of select="b:suffix"/>",
				"<xsl:value-of select="b:sep"/>",
				<xsl:value-of select="b:parens"/>,
				"<xsl:value-of select="b:operatorId"/>",
				"<xsl:value-of select="."/>");
		</xsl:for-each><![CDATA[
		td.appendChild(sel);
		if(op) setValue(sel, op);
		else setValue(sel, 1); 
		if(sel.selectedIndex == -1) setValue(sel, sel.options(0).value);
		selectOperator(sel.options(sel.selectedIndex), val);
	}
}

function addOption(sel, label, value) {
	var op = document.createElement("option");
	op.innerText = label;
	op.value = value;
	sel.appendChild(op);
	return op;	
}

function addOperator(sel, op, fieldIndex, paramCount, prefix, suffix, sep, parens, key, fullString) {
	var label;
	if(fieldIndex == 0) {
		label = prefix + op + suffix;
	} else {
		label = fullString;
	}
	var option = addOption(sel, label, key);
	option.paramCount = paramCount;
	option.sep = sep;
}

function selectOperator(option, values) {
	var filterId = option.parentNode.filterId;
	var name = "filter_val_" + filterId;
	var inps = new Array(); /*option.form.elements("filter_val_" + filterId);	
	if(!(inps instanceof Array)) inps = new Array(inps); */
	for(var i = 0; i < option.form.elements.length; i++) {
		if(name == option.form.elements(i).name) inps.push(option.form.elements(i));
	}
	
	//hide what we don't need
	for(var i = option.paramCount; i < inps.length; i++) {
		var txt = inps[i].previousSibling;
		if(txt != null &&  "font" == txt.tagName.toLowerCase()) {
			txt.style.display = "none";
		}
		inps[i].disabled = true;
		inps[i].style.display = "none";
	}
	var td = option.parentNode.parentNode;
	for(var i = 0; i < option.paramCount; i++) {
		var inp;
		if(i >= inps.length) {
			//create
			if(i > 0) {
				var txt = document.createElement("font");
				txt.innerHTML = option.sep;
				td.appendChild(txt);
			}
			inp = document.createElement("input");
			inp.type = "text";
			inp.name = "filter_val_" + filterId;
			td.appendChild(inp);
		} else {
			var txt = inps[i].previousSibling;
			if(txt != null && "font" == txt.tagName.toLowerCase()) {
				txt.innerHTML = option.sep;
				txt.style.display = "inline";
			}
			inps[i].disabled = false;
			inps[i].style.display = "inline";
			inp = inps[i];
		}
		if(values) {
			if(i == 0 && !(values instanceof Array)) inp.value = values;
			else if(i < values.length) inp.value = values[i];
		}
	}
}

function setValue(select, value) {
    if(document.layers) {
        for(var i = 0; i < select.options.length; i++) {
            if(select.options(i).value == value) {
                select.selectedIndex = i;
                return;
            }
        }
        select.selectedIndex = -1;
    } else {
        select.value = value;
    }
}

function saveReport() {
	var frm = document.forms['reportForm'];
	var title = window.prompt("Please type a title for this report", frm.title.value);
	if(title == undefined) return;
	frm.title.value = title;
	frm.action='save_report.i';
	frm.submit();
}
/*
function handleSelection(anch) {
	var td = getContainingTag(anch, "TD");
	alert("CP#1");
	var table = getContainingTable(td.parentNode);
	alert("CP#2");
	var th = table.tHead.rows.item(0).cells.item(td.cellIndex);
	//var fieldLabel = th.innerText;
	var fieldId = th.fieldId	
	var a = document.getElementById("field_" + fieldId);
	alert("CP#3");
	filterField(a, 1, td.value);
}*/
function handleSelection(fieldId, value) {
	var a = document.getElementById("field_" + fieldId);
	filterField(a, 1, value);
}
alert("script 04");
]]>
	</xsl:template>
</xsl:stylesheet>
