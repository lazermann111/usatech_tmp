<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:conv="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
	xmlns:javaclass="http://simple/xml/extensions/java/java.lang.Class"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
    xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a b conv javaclass req reflect">
	<xsl:import href="design-base.xsl"/>
	<xsl:import href="design-filters.xsl"/>
	<xsl:import href="design-filter-groups.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />
	
	<xsl:template name="contents">
		<xsl:variable name="folio" select="/a:base/b:folio"/>
		<span class="section">Folio</span>
		<xsl:call-template name="edit-folio">
			<xsl:with-param name="folio" select="$folio"/>
			<xsl:with-param name="outputTypes" select="/a:base/b:outputTypes"/>
		</xsl:call-template>
		<a class="selectedTab" href="javascript: showPane('pillarsPane');" id="pillarsPaneTab">
			Pillars
		</a>
		<a class="unselectedTab" href="javascript: showPane('filtersPane');" id="filtersPaneTab">
			Filters
		</a>
		<a class="unselectedTab" href="javascript: showPane('filterGroupsPane');" id="filterGroupsPaneTab">
			Filter Groups
		</a>
		<div style="">
			<div id="pillarsPane" class="unselectedPane">
				<span class="section">Pillars</span>
				<xsl:call-template name="list-pillars">
					<xsl:with-param name="folio" select="$folio"/>
				</xsl:call-template>
				<span class="section">Pillar Detail</span><br/>
				<iframe id="pillarEditor" width="100%" height="250px"
					marginheight="0"/>
			</div>
			<form name="addFiltersForm" action="replace_filters.i" onsubmit="return checkChanged(this);">
				<div id="filtersPane" class="unselectedPane">
					<span class="section">Filters</span>
					<xsl:call-template name="edit-filters">
						<xsl:with-param name="folio" select="$folio"/>
					</xsl:call-template>
				</div>
				<div id="filterGroupsPane" class="unselectedPane">
					<span class="section">Filter Groups</span>
					<xsl:call-template name="edit-filter-groups">
						<xsl:with-param name="folio" select="$folio"/>
					</xsl:call-template>
				</div>
			</form>
		</div>
	</xsl:template>

	<xsl:template name="edit-folio">
		<xsl:param name="folio"/>
		<xsl:param name="outputTypes"/>
		<xsl:variable name="sessionToken" select="reflect:getProperty($context, 'simple.servlet.SessionToken')"/>
        
		<form name="folioForm" action="save_folio.i">
			<input type="hidden" name="folioId" value="{$folio/@b:folioId}"/>
			<table><tr><td>
				<table class="form-layout">
					<xsl:call-template name="form-1-per-row">
						<xsl:with-param name="fields">
							<text name="name" value="{$folio/@b:name}">Name</text>
							<text name="title" value="{$folio/@b:title}">
								Title</text>
							<text name="subtitle" value="{$folio/@b:subtitle}">
								Subtitle</text>
							<select name="outputType"
								value="{$folio/b:outputType/b:outputTypeId}">
								Default Report Type
								<xsl:for-each select="$outputTypes/b:outputType">
									<option value="{b:outputTypeId}">
											<xsl:value-of select="b:label"/>
										</option>
								</xsl:for-each>
							</select>
						</xsl:with-param>
					</xsl:call-template>
				</table>
				</td><td>
				<input type="button" value="Reload Folio From Database" onclick="window.location.href='reload_folio.i?folioId={$folio/@b:folioId}&amp;session-token={$sessionToken}'"/>
				<br/>
				<input type="button" value="View SQL" onclick="window.location.href='get_sql.i?folioId={$folio/@b:folioId}'"/>
				<br/>
				<input type="button" value="Run This Report" onclick="window.location.href='run_report_async.i?unframed=true&amp;folioId={$folio/@b:folioId}'"/>
				<br/>
				<input id="saveButton" type="submit" value="Save Changes"/></td>
				</tr></table>
		</form>
	</xsl:template>

	<xsl:template name="list-pillars">
		<xsl:param name="folio"/>
		<table class="selectableList" id="pillarTable">
			<thead><tr><th>Pillar Index</th><th>Label</th><th>Pillar Type</th>
				<th>Grouping Level</th><th>Fields</th>
				<th>Display Format</th><th>Action Format</th><th>Tooltip
				Format</th>
				<th>Sort Format</th><th>Style Format</th><th>Percent Format</th><th>Description</th></tr>
				</thead>
			<tbody>
				<xsl:if test="count($folio/child::*) > 0">
					<xsl:for-each select="$folio/b:pillars/b:pillar">
					<xsl:variable name="index" select="position()"/>
					<xsl:variable name="pillarTypeInt" select="conv:getInt(conv:convert(javaclass:forName('simple.falcon.engine.Pillar$PillarType'),string(@b:pillarType)))"/>
						<tr onclick="selectPillar({$index}, this);"><td>
								<xsl:value-of select="$index"/></td><td>
								<xsl:value-of select="@b:labelFormat"/></td>
							<td><xsl:choose>
								<xsl:when test="$pillarTypeInt = 1">Text</xsl:when>
								<xsl:when test="$pillarTypeInt = 2">Image</xsl:when>
								<xsl:when test="$pillarTypeInt = 3">Note</xsl:when>
								<xsl:when test="$pillarTypeInt = 4">Axis (Charts)</xsl:when>
								<xsl:when test="$pillarTypeInt = 5">Value	(Charts)</xsl:when>
								<xsl:when test="$pillarTypeInt = 6">Row-level</xsl:when>
								<xsl:when test="$pillarTypeInt = 7">Checkbox</xsl:when>
								<xsl:when test="$pillarTypeInt = 8">Button</xsl:when>
								<xsl:when test="$pillarTypeInt = 9">Radio</xsl:when>
								</xsl:choose></td>
							<td>
						    <!--
								<xsl:choose>
									<xsl:when test="b:groupingLevel = -1">Show Totals</xsl:when>
					  				<xsl:when test="b:groupingLevel = 0">Show Detail</xsl:when>
					  				<xsl:when test="b:groupingLevel = 1">Level One</xsl:when>
					  				<xsl:when test="b:groupingLevel = 2">Level Two</xsl:when>
					  				<xsl:when test="b:groupingLevel = 3">Level Three</xsl:when>
					  				<xsl:when test="b:groupingLevel = 4">Level Four</xsl:when>
					  				<xsl:when test="b:groupingLevel = -2">Level One, with Totals</xsl:when>
					  				<xsl:when test="b:groupingLevel = -3">Level Two, with Totals</xsl:when>
					  				<xsl:when test="b:groupingLevel = -4">Level Three, with Totals</xsl:when>
					  				<xsl:when test="b:groupingLevel = -5">Level Four, with Totals</xsl:when>

									<xsl:when test="b:aggregated = 'true'">
										<xsl:value-of select="-1 * (b:groupingLevel + 1)" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="b:groupingLevel" />
									</xsl:otherwise>
								</xsl:choose> -->
								<xsl:choose>
									<xsl:when test="@b:groupingLevel = 0">Show Totals at Each Level</xsl:when>
					  				<xsl:when test="@b:groupingLevel = 1">Cross-Tab Column</xsl:when>
					  				<xsl:when test="@b:groupingLevel = 2">Detail Row</xsl:when>
					  				<xsl:when test="@b:groupingLevel = 3">Level One Group (Table)</xsl:when>
					  				<xsl:when test="@b:groupingLevel = 4">Level Two Group</xsl:when>
					  				<xsl:when test="@b:groupingLevel = 5">Level Three Group</xsl:when>
					  				<xsl:otherwise>
										<xsl:value-of select="@b:groupingLevel" />
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td><xsl:for-each
									select="b:field">
								<xsl:if test="position() &gt; 1">,
								</xsl:if>
								<xsl:value-of select="@b:fieldLabel"/>
								<!-- (<xsl:value-of select="b:sortOrder"/>)  -->
								</xsl:for-each></td>
							<td><xsl:value-of select="@b:displayFormat"/></td><td>
								<xsl:value-of select="@b:actionFormat"/>
							</td>
							<td><xsl:value-of select="@b:helpFormat"/></td><td>
								<xsl:value-of select="@b:sortFormat"/>
							</td>
							<td><xsl:value-of select="@b:styleFormat"/></td>
							<td><xsl:value-of select="@b:percentFormat"/></td>
							<td>
								<xsl:value-of select="@b:description"/>
							</td></tr>
					
					</xsl:for-each>
				</xsl:if>
			</tbody>
			<!-- <select name="{concat('sortOrder', $index}" value="{b:field/b:sortOrder}">
			<option value="0">Default</option>
			<option value="1">Ascending</option>
			<option value="2">Descending</option>
			<option value="3">None</option>
			</select>
			</td>
			<xsl:choose>
			<xsl:when test="b:field/b:aggregateType = 1">Sum</xsl:when>
			<xsl:when test="b:field/b:aggregateType = 2">Average</xsl:when>
			<xsl:when test="b:field/b:aggregateType = 3">Maximum</xsl:when>
			<xsl:when test="b:field/b:aggregateType = 4">Minimum</xsl:when>
			<xsl:when test="b:field/b:aggregateType = 5">Count</xsl:when>
			</xsl:choose>
			-->
			<tfoot>
				<tr><td colspan="12">
					<form name="addPillarForm" action="edit_folio.i"
						onsubmit="return checkChanged(this);" class="floatLeft">
						<xsl:if
							test="count($folio/child::*) > 0">
							<input type="hidden" name="folioId"
								value="{$folio/@b:folioId}"/>
						</xsl:if>
						<input type="hidden" name="blankPillars" value="1"/>
						<input id="addButton" type="submit" value="Add Pillar"/>
					</form>
					<form name="addFieldPillarForm" action="add_field_pillar.i"
						onsubmit="return checkChanged(this);" class="floatLeft">
						<xsl:if
							test="count($folio/child::*) > 0">
							<input type="hidden" name="folioId"
								value="{$folio/@b:folioId}"/>
						</xsl:if>
						<input type="hidden" name="fieldId"/>
					</form>
					<form name="deletePillarForm" action="delete_pillar.i"
						onsubmit="return checkDelete();" class="floatLeft">
						<xsl:if
							test="count($folio/child::*) > 0">
							<input type="hidden" name="folioId"
								value="{$folio/@b:folioId}"/>
						</xsl:if>
						<input name="pillarIndex" type="hidden"/>
						<input id="deleteButton" type="submit"
							value="Delete Pillar" disabled="true"/>
					</form>
					</td></tr>
			</tfoot>
		</table>
		<script type="text/javascript" defer="defer">

	  		var selectedPillarIndex = -1;
	  		var tableRows = document.getElementById("pillarTable").tBodies[0].rows;
	  		var deleteButton = document.getElementById("deleteButton");
	  		var saveButton = document.getElementById("saveButton");
	  		var changedArray = new Array();

	  		function selectPillar(index, tr) {
	  			selectedPillarIndex = index;
	  			document.forms["deletePillarForm"].elements["pillarIndex"].value = index;
	  			for(var i = 0; i &lt; tableRows.length; i++) {
	  				tableRows[i].className = "";
	  			}
	  			tr.className = "selectedRow";
	  			<xsl:if test="count($folio/child::*) > 0">
	  			var detailFrame = document.getElementById("pillarEditor");
	  			if(detailFrame) detailFrame.src = "edit_pillar.i?unframed=true&amp;folioId=<xsl:value-of select="$folio/@b:folioId"
	  				/>&amp;session-token=<xsl:value-of select="req:getSessionTokenIfPresent($context)"
	  				/>&amp;pillarIndex=" + index;
	  			</xsl:if>
	  			deleteButton.disabled = (index &lt; 0);
	  		}

	  		function checkDelete() {
	  			var label = tableRows[selectedPillarIndex-1].cells[1];
	  			label.normalize();
	  			var text = (label.firstChild ? label.firstChild.nodeValue : "");
	  			return confirm("Are you sure you want to delete pillar #" + selectedPillarIndex + " (" + text + ")?");
	  		}

	  		function checkChanged(frm) {
	  			for(var i = 0; i &lt; changedArray.length; i++) {
	  				var b = confirm("You have made other changes to this folio [" + changedArray[i] + "]. Would you like to save those?");
	  				if(b) {
	  					//TODO: ??
	  				}
	  			}
	  			return true;
	  		}

	  		function fieldChanged(inp) {
	  			inp.form.changed = true;
	  			/*if(inp.form.name == "folioForm") {
	  				saveButton.disabled = "false";
	  			}*/
	  			changedArray[inp.form.name] = inp.form.name;
	  		}

	  		function addFieldPillar(fieldId) {
	  			var frm = document.forms["addFieldPillarForm"];
	  			frm.elements["fieldId"].value = fieldId;
	  			frm.submit();
	  		}

	  		function addField(fieldId) {
	  			var detailFrame = document.getElementById("pillarEditor");
	  			if(detailFrame) detailFrame.contentWindow.addField(fieldId);
	  		}

	  		function addFilter(fieldId, fieldLabel) {
	  			filterField("", fieldId, fieldLabel);
	  			showPane("filtersPane");
	  		}

	  		var selectedPane = null;
	  		function showPane(paneName) {
	  			var div = document.getElementById(paneName);
	  			if(selectedPane) {
	  				selectedPane.className = "unselectedPane";
	  				var tab = document.getElementById(selectedPane.id + "Tab");
	  				if(tab) tab.className = "unselectedTab";
	  			}
	  			if(div) {
	  				div.className = "selectedPane";
	  				var tab = document.getElementById(paneName + "Tab");
	  				if(tab) tab.className = "selectedTab";
  				}
  				selectedPane = div;
	  		}
	  		showPane("pillarsPane");
	  		<xsl:variable name="pillarIndex" select="reflect:getProperty($context, 'pillarIndex')"/>
            <xsl:if test="$pillarIndex &gt; 0">
            window.addEvent("domready", function() {
                var trs = $$("#pillarTable > tbody > tr");
                if(trs &amp;&amp; trs.length >= <xsl:value-of select="$pillarIndex"/>)
                    selectPillar(<xsl:value-of select="$pillarIndex"/>, trs[<xsl:value-of select="$pillarIndex - 1"/>]);
            });
            </xsl:if>
	  	</script>

	</xsl:template>
</xsl:stylesheet>
