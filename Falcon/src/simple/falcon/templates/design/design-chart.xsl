<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:b="http://simple/bean/1.0" 
	xmlns:r="http://simple/results/1.0"  
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	exclude-result-prefixes="b r a p reflect"
	version="1.0">
	<xsl:import href="design-base.xsl"/>
    <xsl:output method="xhtml"/>
	
	<xsl:param name="context" />
	
	<xsl:template name="contents">
		<xsl:attribute name="onload"><xsl:text>init('selectChartTypes','selectGroupingDiv','chartConfigDiv','chartType','groupLevel0');</xsl:text></xsl:attribute>
		
		<form name="configChartForm" action="run_custom_folio.i">
			<input type="hidden" id="chartType" name="chartType" value="" />
			<input type="hidden" id="groupLevel0" name="chart.groupLevel0" value="" />
			<xsl:call-template name="parameters-form">
				<xsl:with-param name="overrides">
					<p:override name="doChartConfigUpdate" value="true" />
					<p:parameter name="outputType" />
					<p:parameter name="chartType" />
					<p:parameter name="chart.groupLevel0" />
					<p:parameter name="chart.groupLevel1" />
					<p:parameter name="chart.cluster" />
					<p:parameter name="chart.value" />
					<!--  todo - should be ignoring, not including -->
				</xsl:with-param>
			</xsl:call-template>

			<span class="section">Group By:</span>
			<div id="selectGroupingDiv">
				<xsl:text> </xsl:text>
			</div>
			<span class="section">Output Document Type:</span>
			<div>
				<select name="outputType" value="{reflect:getProperty($context,'outputType')}">
					<option value="28">HTML Document</option>
					<option value="31">PDF Document</option>
				</select>
			</div>
			<span class="section">Chart Type:</span>
			<div>
				<select id="selectChartTypes" onchange="changeType(this);">
					<xsl:call-template name="chartTypes" />
				</select>
			</div>
			<span class="section">Configure Chart:</span>
			<div id="chartConfigDiv">
				<xsl:text> </xsl:text>
			</div>
			<input type="submit" value="Show Chart" />
		</form>
		<script type="text/javascript">
var chartElem,groupElem,configElem,pillarSelect,fieldSelect,outputTypeElem,group0;
var pillarIds = new Array();
var group1selects = new Array();
			
function setChartConfig(type,clusters,values,clusterDescriptions,valueDescriptions){
	chartTypeElem.value = type;
			
	clearConfig();
			
	for(var i in clusters){
		addCluster(clusters[i],clusterDescriptions[i]);
	}
			
	for(var i in values){
		addValue(values[i],valueDescriptions[i]);
	}
}
			
function clearConfig(){
	while(configElem.childNodes.length > 0){
		configElem.removeChild(configElem.firstChild);
	}
}
			
function addCluster(name,description,value){
	var select = pillarSelect.cloneNode(true);
	select.setAttribute("name","chart.cluster");
	select.setAttribute("id","fieldSelects."+name);
	select.onchange = new Function("validSelection(this);");
	if((value != null) &amp;&amp; (value != '')){
		select.value=value;
	}
			
	var a = document.createElement("a");
	a.setAttribute("title",description);
	a.appendChild(document.createTextNode(name+": "));

	configElem.appendChild(a);
	configElem.appendChild(select);
	configElem.appendChild(document.createElement("br"));
}
			
function addValue(name,description,value){
	if(name == "") name = "Value";

	var select = fieldSelect.cloneNode(true);
	select.setAttribute("name","chart.value");
	select.setAttribute("id","fieldSelects."+name);
	select.onchange = new Function("validSelection(this);");
	if((value != null) &amp;&amp; (value != '')){
		select.value=value;
	}
			
	var a = document.createElement("a");
	a.setAttribute("title",description);
	a.appendChild(document.createTextNode(name+": "));

	configElem.appendChild(a);
	configElem.appendChild(select);
	configElem.appendChild(document.createElement("br"));
}
			
function validSelection(select){
	var isValid = false;

	/* XXX: I think we want to allow selection of nothing actually...
	if(select.value == "") {
		if(select.getAttribute("oldValue") != null){
			select.value = select.getAttribute("oldValue");
		}
	} else {*/
	if(select.value != ""){
		var selects = document.getElementsByTagName("select");
		var found = false;
		for(var i in selects){
			if(selects[i] != select){
				if(selects[i].value == select.value){
					found = true;
					select.value = select.getAttribute("oldValue");
					alert("You can not use the same pillar or field in more than one place.");
					break;
				}
			}
		}
		if(!found){
			select.setAttribute("oldValue",select.value);
			isValid = true;
		}
	}

	return isValid;
}
			
function setGroup(select){
	if((select.value == "") &amp;&amp; (select.parentNode.lastChild != select)){
		// if they set a previosly selected group to null, drop the menu
		select.parentNode.removeChild(select);
		select.value = null;
		updateGroupings();
	} else {
		if(validSelection(select)){
			updateGroupings();
			if(select.parentNode.lastChild == select){
				addGroupSelect();
			}
		}
	}
}
			
function notInGroupOne(id){
	var found = false;
	for(var i in group1selects){
		if(id == group1selects[i].value){
			found = true;
			break;
		}
	}
	return !found;
}
			
function updateGroupings(){
	group0.value = "";
	for(var i in pillarIds){
		if(notInGroupOne(pillarIds[i])){
			if(group0.value != "") group0.value += ",";
			group0.value += pillarIds[i];
		}
	}
}
			
function addGroupSelect(selected){
	var select = pillarSelect.cloneNode(true);
	select.setAttribute("name","chart.groupLevel1");
	select.onchange = new Function("setGroup(this);");
	if(selected != "") select.value = selected;

	groupElem.appendChild(select);
	group1selects.push(select);
}
		
function changeType(chartSelect){
	var action = new Function(chartSelect.value);
	action();
}
		
function addChartType(name,value){
	var option = document.createElement("option");
	option.setAttribute("value",value);
	option.appendChild(document.createTextNode(name));
		
	chartElem.appendChild(option);
}
			
function addPillar(id,label){
	var option = document.createElement("option");
	option.setAttribute("value",id);
	option.appendChild(document.createTextNode(label));
		
	pillarSelect.appendChild(option);
			
	if(id != ""){
		pillarIds.push(""+id);
	}
}
			
function initPillars(){
	addPillar("","SELECT PILLAR:");
	<xsl:call-template name="pillar-options" />
}
			
function addField(id,label){
	var option = document.createElement("option");
	option.setAttribute("value",id);
	option.appendChild(document.createTextNode(label));
		
	fieldSelect.appendChild(option);		
}
			
function initFields(){
	addField("","SELECT FIELD:");
	<xsl:call-template name="field-options" />
}
			
function initDefaults(){
<xsl:variable name="parameter-set" select="*/a:parameters/p:parameters/p:parameter"/>		
<xsl:text>
	var selects = document.getElementsByTagName("select");
	var clusters = 0;
	var values = 0;
	for(var i in selects){
		if((selects[i].name=='chart.cluster') &amp;&amp; (clusters == 0)){
			selects[i].value='</xsl:text>
			<xsl:value-of select="$parameter-set[@name='chart.cluster'][1]/@value" />
			<xsl:text>';
			clusters++;
			continue;
		}
		if((selects[i].name=='chart.cluster') &amp;&amp; (clusters == 1)){
			selects[i].value='</xsl:text>
			<xsl:value-of select="$parameter-set[@name='chart.cluster'][2]/@value" />
			<xsl:text>';
			clusters++;
			continue;
		}
		if((selects[i].name=='chart.value') &amp;&amp; (values == 0)){
			selects[i].value='</xsl:text>
			<xsl:value-of select="$parameter-set[@name='chart.value'][1]/@value" />
			<xsl:text>';
			values++;
			continue;
		}
		if((selects[i].name=='chart.value') &amp;&amp; (values == 1)){
			selects[i].value='</xsl:text>
			<xsl:value-of select="$parameter-set[@name='chart.value'][2]/@value" />
			<xsl:text>';
			values++;
			continue;
		}
		if((selects[i].name=='chart.value') &amp;&amp; (values == 2)){
			selects[i].value='</xsl:text>
			<xsl:value-of select="$parameter-set[@name='chart.value'][3]/@value" />
			<xsl:text>';
			values++;
			continue;
		}
	}</xsl:text>
	<xsl:for-each select="$parameter-set[@name='chart.groupLevel1']">
		<xsl:text>
	addGroupSelect('</xsl:text>
		<xsl:value-of select="@value" />
		<xsl:text>');</xsl:text>
	</xsl:for-each>
}
		  
function init(chartId,groupId,configId,chartTypeId,group0Id){
	chartElem = document.getElementById(chartId);
	groupElem = document.getElementById(groupId);
	configElem = document.getElementById(configId);
	chartTypeElem = document.getElementById(chartTypeId);
	group0 = document.getElementById(group0Id);

	pillarSelect = document.createElement("select");
	fieldSelect = document.createElement("select");

	initPillars();
	initFields();

	changeType(chartElem);
		
	initDefaults();
	addGroupSelect();
	updateGroupings();
}
		</script>
	</xsl:template>
	
	<xsl:template name="field-options">
		<xsl:for-each select="/*/b:folio/b:pillars/b:pillar">
			<xsl:variable name="pillarIndex" select="position()" />
			<xsl:for-each select="b:fieldOrders/b:fieldOrder/b:field">
				<xsl:if test="(b:sortSqlType/b:sqlType &gt;= 2 and b:sortSqlType/b:sqlType &lt;= 8) or (b:sortSqlType/b:sqlType &gt;= -7 and b:sortSqlType/b:sqlType &lt;= -5)">
					<xsl:text>addField("</xsl:text>
					<xsl:value-of select="$pillarIndex" />
					<xsl:text>.</xsl:text>
					<xsl:value-of select="position()" />
					<xsl:text>","</xsl:text>
					<xsl:value-of select="b:label" />
					<xsl:text>");
<!--		-->	</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="pillar-options">
		<xsl:for-each select="/*/b:folio/b:pillars/b:pillar">
			<xsl:text>addPillar("</xsl:text>
			<xsl:value-of select="position()" />
			<xsl:text>","</xsl:text>
			<xsl:value-of select="b:label" />
			<xsl:text>");
<!-- -->	</xsl:text>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="chartTypes">
		<xsl:for-each select="/*/a:chartConfig/r:results/r:row">
			<xsl:sort select="r:chartTypeLabel" />
			<option>
				<xsl:if test="reflect:getProperty($context,'chartType') = r:chartTypeId">
					<xsl:attribute name="selected">selected</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="value">
					<xsl:text>setChartConfig('</xsl:text>
					<xsl:value-of select="r:chartTypeId" />
					<xsl:text>',new Array('</xsl:text>
					<xsl:value-of select="r:chartClusterOneName" />
					<xsl:text>','</xsl:text>
					<xsl:value-of select="r:chartClusterTwoName" />
					<xsl:text>'),new Array('</xsl:text>
					<xsl:value-of select="r:chartValueOneName" />
					<xsl:if test="string-length(r:chartValueTwoName) &gt; 0">
						<xsl:text>','</xsl:text>
						<xsl:value-of select="r:chartValueTwoName" />
					</xsl:if>
					<xsl:if test="string-length(r:chartValueThreeName) &gt; 0">
						<xsl:text>','</xsl:text>
						<xsl:value-of select="r:chartValueThreeName" />
					</xsl:if>
					<xsl:text>'),new Array('</xsl:text>
					<xsl:value-of select="r:chartClusterOneDescription" />
					<xsl:text>','</xsl:text>
					<xsl:value-of select="r:chartClusterTwoDescription" />
					<xsl:text>'),new Array('</xsl:text>
					<xsl:value-of select="r:chartValueOneDescription" />
					<xsl:text>','</xsl:text>
					<xsl:value-of select="r:chartValueTwoDescription" />
					<xsl:text>','</xsl:text>
					<xsl:value-of select="r:chartValueThreeDescription" />
					<xsl:text>'));</xsl:text>
				</xsl:attribute>
				<xsl:value-of select="r:chartTypeLabel"/>
			</option>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
