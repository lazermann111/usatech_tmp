<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect">
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template match="/">
		<html xml:lang="en" lang="en">
			<head>
				<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
				<title>Report Design</title>
				<base href="{reflect:getProperty($context, 'request.requestURL')}" />
				<link href="css/default-style.css" type="text/css" rel="stylesheet" />
			</head>
			<body>
				<xsl:call-template name="contents"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="contents">
	</xsl:template>


  <xsl:template name="form-1-per-row">
  	<xsl:param name="fields"/>
  	<xsl:for-each select="$fields/*">
  		<xsl:choose>
  			<xsl:when test="local-name() = 'text'">
  				<xsl:call-template name="form-field-text">
		  			<xsl:with-param name="label" select="normalize-space(.)"/>
		  			<xsl:with-param name="name" select="@name"/>
		  			<xsl:with-param name="value" select="@value"/>
		  		</xsl:call-template>
  			</xsl:when>
  			<xsl:when test="local-name() = 'select'">
  				<xsl:call-template name="form-field-select">
		  			<xsl:with-param name="label" select="normalize-space(text())"/>
		  			<xsl:with-param name="name" select="@name"/>
		  			<xsl:with-param name="value" select="@value"/>
		  			<xsl:with-param name="options" select="option"/>
		  		</xsl:call-template>
  			</xsl:when>
  		</xsl:choose>
  	</xsl:for-each>
  </xsl:template>

  <xsl:template name="form-field-text">
  	<xsl:param name="label" />
	<xsl:param name="name" />
	<xsl:param name="value" />
		<tr><th><xsl:value-of select="$label"/>:</th><td>
  		<input type="text" name="{$name}" value="{$value}" onchange="if(fieldChanged) fieldChanged(this);"/>
		</td></tr>
  </xsl:template>

  <xsl:template name="form-field-select">
  	<xsl:param name="label" />
	<xsl:param name="name" />
	<xsl:param name="value" />
	<xsl:param name="type" />
	<xsl:param name="options" />
  		<tr><th><xsl:value-of select="$label"/>:</th><td>
  		<select name="{$name}" value="{$value}" onchange="if(fieldChanged) fieldChanged(this);">
			<xsl:for-each select="$options">
				<option value="@value">
					<xsl:choose>
						<xsl:when test="string-length(normalize-space(.)) > 0"><xsl:value-of select="."/></xsl:when>
						<xsl:otherwise><xsl:value-of select="@value"/></xsl:otherwise>
					</xsl:choose>
				</option>
			</xsl:for-each>
		</select>
 		</td></tr>
  </xsl:template>
</xsl:stylesheet>
