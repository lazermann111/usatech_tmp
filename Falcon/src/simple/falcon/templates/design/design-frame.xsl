<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect">
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />
	
	<xsl:template match="/">
		<xsl:variable name="folioId" select="reflect:getProperty($context, 'folioId')"/>
		<xsl:variable name="sessionToken" select="reflect:getProperty($context, 'simple.servlet.SessionToken')"/>
		<div class="caption">Folio Designer</div>
		<div class="iframeContainer" style="width: 70%">
			<iframe src="edit_folio.i?unframed=true&amp;folioId={$folioId}&amp;session-token={$sessionToken}" name="folioDesignFrame"/>
		</div>
		<div class="iframeContainer" style="width: 29.6%">
			<iframe src="select_fields.i?unframed=true&amp;folioId={$folioId}&amp;session-token={$sessionToken}" name="fieldSelectionFrame"/>
		</div>
	</xsl:template>
</xsl:stylesheet>