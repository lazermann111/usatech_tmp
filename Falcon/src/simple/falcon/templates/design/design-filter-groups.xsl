<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:b="http://simple/bean/1.0" 
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	exclude-result-prefixes="b reflect">
	<xsl:import href="design-base.xsl"/>
    <xsl:output method="xhtml"/>
  <xsl:param name="context"/>
	
  <xsl:template name="edit-filter-groups">
  	<xsl:param name="folio"/>
	<input type="hidden" name="folioId" value="{$folio/@b:folioId}"/>
	<input type="hidden" id="filter_group_max_index" name="filter_group_max_index" value="0"/>
	<div>
		<!--<ul>
			<li>Root (AND)</li>-->
			<!-- Filter is always TopFG (dummy root filter) -->
			<ul>
				<li style="display: none;">
					<a id="addLink-" href="javascript:ignore()" 
						onclick="addFilterGroup('addLink-','',1)">
						<xsl:text>Add Group</xsl:text>
					</a>
				</li>
			</ul>
		<!--</ul>-->
	</div>
	<input id="saveButton" type="submit" value="Save Filters" onclick="setFilterMax(this.form)"/>
	<script type="text/javascript" defer="defer">
var fgIndex = 0;
		
function buildTree(){
	<!--<xsl:for-each select="$folio/b:filter/b:childFilters/b:childFilter[b:separator]">
		<xsl:call-template name="build-tree">
			<xsl:with-param name="filter" select="." />
			<xsl:with-param name="groupName" select="position()" />
		</xsl:call-template>
	</xsl:for-each>-->
	<xsl:call-template name="build-tree">
		<xsl:with-param name="filter" select="$folio/b:filterGroup" />
		<xsl:with-param name="groupName" select="'1'" />
	</xsl:call-template>
}
		
function addToGroupSelects(name,index,isInit){
	var option;
	if(isInit != true) isInit = false;


	option = document.createElement("option");
	option.setAttribute("id","opt-"+index);
	option.setAttribute("value",name);
	option.appendChild(document.createTextNode(name));

	for(var i=1; i &lt;= maxFilterIndex; i++){
		filterGroupSelects[i].appendChild(option.cloneNode(true));
		filterGroupSelects[i].value = filterGroupSelects[i].getAttribute("groupvalue");
	}
	if(!isInit){
		showPane("filtersPane");
		showPane("filterGroupsPane");
	}
}
		
function addFilterGroup(id,parentName,count,separator,fgId,isInit){
	var addLink, oldLi, newLi, select, hidden, text, name, maxIndex, del;
	if(isInit == null) isInit = false;
		
	if(separator == null) separator = "AND";

	fgIndex++;

	addLink = document.getElementById(id);
		
	name = parentName + count;
		
	setTimeout("addToGroupSelects('"+name+"',"+fgIndex+","+isInit+")",1);
		
	oldLi = addLink.parentNode;
	parentList = oldLi.parentNode;
	newLi = document.createElement("li");
	newLi.setAttribute("id","li-"+fgIndex);
	text = document.createTextNode("Group " + name + ": ");
	newLi.appendChild(text);
	
	hidden = document.createElement("input");
	hidden.setAttribute("type","hidden");
	hidden.setAttribute("name", "filter_group_label_" + fgIndex);
	hidden.setAttribute("value", name);
	newLi.appendChild(hidden);
	
	hidden = document.createElement("input");
	hidden.setAttribute("type","hidden");
	hidden.setAttribute("name", "filter_group_id_" + fgIndex);
	hidden.setAttribute("value", fgId);
	newLi.appendChild(hidden);
	if(fgIndex > 1) {	
		del = document.createElement("a");
		del.setAttribute("href","javascript:deleteGroup('"+fgIndex+"')");
		del.appendChild(document.createTextNode("Delete"));
		newLi.appendChild(document.createTextNode("("));
		newLi.appendChild(del);
		newLi.appendChild(document.createTextNode(")"));
	}
	select = addSelect(newLi,"filter_group_separator_" + fgIndex);
	select.value = separator;	
		
	addChildGroupList(newLi, name);
		
	addLink.onclick = new Function("addFilterGroup('addLink-" + 
		parentName + "','" + parentName + "'," + (count + 1) + ")");
		
	parentList.insertBefore(newLi,oldLi);
		
	maxIndex = document.getElementById('filter_group_max_index');
	maxIndex.setAttribute("value",fgIndex);
}
		
function deleteGroup(index){
	if(confirm("Are you sure you want to delete this group?")){
		var li = document.getElementById("li-"+index);
		if(li.getElementsByTagName("li").length > 1){
			alert("This group has children. You must delete the children before deleting the group.");
		} else {
		
			li.parentNode.removeChild(li);

			for(var i=1; i &lt;= maxFilterIndex; i++){
				var opt = document.getElementById("opt-"+index);
				filterGroupSelects[i].removeChild(opt);
			}
			showPane("filtersPane");
			showPane("filterGroupsPane");
		}
	}
}
		
function addChildGroupList(parent,name){
	var childList = document.createElement("ul");
	var addGroupLI = document.createElement("li");
	var addGroupLink = document.createElement("a");
	var addGroupText = document.createTextNode("Add Group");

	addGroupLink.appendChild(addGroupText);
	addGroupLink.setAttribute("id","addLink-" + name + ".");
	addGroupLink.setAttribute("href","javascript:ignore()");
	addGroupLink.onclick =
		new Function("addFilterGroup('addLink-" + name + ".','" + name + ".',1)");
	addGroupLI.appendChild(addGroupLink);
	childList.appendChild(addGroupLI);
		
	parent.appendChild(childList);
}

function addSelect(parent,name){
	var select = document.createElement("select");
	select.setAttribute("name",name);
	addOption(select,"AND","AND");
	addOption(select,"OR","OR");
		
	parent.appendChild(select);
		
	return select;
}
		
function ignore(){}
		
buildTree();
	</script>
  </xsl:template>
	
	<xsl:template name="build-tree">
		<xsl:param name="filter" />
		<xsl:param name="parentName" select="''" />
		<xsl:param name="groupName" select="''" />
		
		<xsl:text>addFilterGroup('addLink-</xsl:text>
		<xsl:value-of select="$parentName" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="$parentName" />
		<xsl:text>',</xsl:text>
		<xsl:value-of select="$groupName" />
		<xsl:text>,'</xsl:text>
		<xsl:value-of select="$filter/@b:separator" />
		<xsl:text>','</xsl:text>
		<xsl:value-of select="$filter/@b:filterGroupId" />
		<xsl:text>',true);
		</xsl:text>
		
		<xsl:for-each select="$filter/b:filterGroup">
			<xsl:call-template name="build-tree">
				<xsl:with-param name="filter" select="." />
				<xsl:with-param name="parentName" select="concat($parentName,$groupName,'.')" />
				<xsl:with-param name="groupName" select="position()" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>