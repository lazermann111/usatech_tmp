<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:b="http://simple/bean/1.0" 
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml" 
	exclude-result-prefixes="b reflect">
	<xsl:import href="design-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />
	
	<xsl:template name="contents">
		<xsl:variable name="fieldId" select="reflect:getProperty($context,'fieldId')" />
		<xsl:variable name="field" select="//b:leaf[b:id = $fieldId]" />
		<xsl:variable name="message" select="reflect:getProperty($context,'message')" />
		<xsl:if test="$message">
			<span class="error">Display and/or Sort Expressions are Invalid: 
				<ul><li class="exception"><xsl:value-of select="$message"/></li></ul>
			</span>
		</xsl:if>
		<span class="section">Edit Field</span>
		<form id="fieldForm" name="editField" action="./save_field.i">
			<input type="hidden" name="fieldId" value="{$field/b:id}" />
			<table>
				<tr>
					<td>Label</td>
					<td>
						<xsl:call-template name="input-text">
							<xsl:with-param name="name" select="'label'" />
							<xsl:with-param name="value" select="$field/b:label" />
							<xsl:with-param name="length" select="'64'" />
							<xsl:with-param name="required" select="'true'" />
						</xsl:call-template>
					</td>
				</tr>
				<tr>
					<td>Display Expression</td>
					<td>
						<xsl:call-template name="input-text">
							<xsl:with-param name="name" select="'displayExpression'" />
							<xsl:with-param name="value" select="$field/b:displayExpression" />
							<xsl:with-param name="length" select="'64'" />
							<xsl:with-param name="required" select="'true'" />
						</xsl:call-template>
					</td>
				</tr>
				<tr>
					<td>Sort Expression</td>
					<td>
						<xsl:call-template name="input-text">
							<xsl:with-param name="name" select="'sortExpression'" />
							<xsl:with-param name="value" select="$field/b:sortExpression" />
							<xsl:with-param name="length" select="'64'" />
							<xsl:with-param name="required" select="'true'" />
						</xsl:call-template>
					</td>
				</tr>
				<tr>
					<td>Display Format</td>
					<td>
						<xsl:call-template name="input-text">
							<xsl:with-param name="name" select="'displayFormat'" />
							<xsl:with-param name="value" select="$field/b:format" />
							<xsl:with-param name="length" select="'64'" />
						</xsl:call-template>
					</td>
				</tr>
				<!-- we will figure this out on new field -->
				<xsl:if test="$field/b:id">
					<tr>
						<td>Display SQL Type</td>
						<td>
							<select name="displaySqlType">
								<xsl:call-template name="sql-types">
									<xsl:with-param name="selected"><!-- 
										<xsl:choose>
											<xsl:when test="string-length(reflect:getProperty($context, 'displaySqlType')) &gt; 0">
												<xsl:value-of select="reflect:getProperty($context, 'displaySqlType')"/>
											</xsl:when>
											<xsl:otherwise> -->
												<xsl:value-of select="//b:sqlTypes/b:entry[b:key = $field/b:displaySqlType/b:typeCode]/b:value" /><!-- 
											</xsl:otherwise>
									 	</xsl:choose> -->
									 </xsl:with-param>
								</xsl:call-template>
							</select>
						</td>
					</tr>
					<tr>
						<td>Sort SQL Type</td>
						<td>
							<select name="sortSqlType">
								<xsl:call-template name="sql-types">
									<xsl:with-param name="selected"><!-- 
										<xsl:choose>
											<xsl:when test="string-length(reflect:getProperty($context, 'sortSqlType')) &gt; 0">
												<xsl:value-of select="reflect:getProperty($context, 'sortSqlType')"/>
											</xsl:when>
											<xsl:otherwise> -->
												<xsl:value-of select="//b:sqlTypes/b:entry[b:key = $field/b:sortSqlType/b:typeCode]/b:value" /><!-- 
											</xsl:otherwise>
									 	</xsl:choose> -->
									 </xsl:with-param>
								</xsl:call-template>
							</select>
						</td>
					</tr>
				</xsl:if>
				<tr>
					<td>Category</td>
					<td>
						<select name="categoryId">	
							<xsl:call-template name="field-categories">
								<xsl:with-param name="selected">
									<xsl:choose>
										<xsl:when test="reflect:getProperty($context, 'categoryId')">
											<xsl:value-of select="reflect:getProperty($context, 'categoryId')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$field/../../b:id" />
										</xsl:otherwise>
								 	</xsl:choose>
								 </xsl:with-param>
								 <xsl:with-param name="branch" select="//b:field-hierarchy/b:branches/b:branch" />
							</xsl:call-template>
						</select>
					</td>
				</tr>
			</table>
			<input type="button" onclick="doSubmit()" value="Save Field" />
		</form>
		<script type="text/javascript">
function doSubmit(){
	var canSubmit = true;
	var inputs = document.getElementsByTagName("input");
	for(var i=0; i &lt; inputs.length; i++){
		if((inputs[i].getAttribute("required") == "true") &amp;&amp; (inputs[i].value == "")){
			canSubmit = false;
			break;
		}
	}
	if(!canSubmit) {
		alert("Label, Display Expression, and Sort Expression are required");
	} else {
		document.getElementById("fieldForm").submit();
	}
}
		<xsl:if test="reflect:getProperty($context,'refresh-tree') = 'true'">
window.parent.frames["editFieldsTreeFrame"].location.reload();
		</xsl:if>
		</script>
	</xsl:template>
	
	<xsl:template name="sql-types">
		<xsl:param name="selected" select="''" />
		<xsl:comment>VALUE='<xsl:value-of select="$selected"/>'</xsl:comment>
		<option value="">
			<xsl:if test="string-length(normalize-space($selected)) = 0">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			&lt;DEFAULT&gt;
		</option>			
		<xsl:for-each select="//b:sqlTypes/b:entry">
			<xsl:sort select="b:value"/>
			<option value="{b:value}">
				<xsl:if test="normalize-space($selected) = b:value">
					<xsl:attribute name="selected">selected</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="b:value" />
			</option>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="input-text">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="length"/>
		<xsl:param name="required"/>
		<input type="text" name="{$name}" required="{$required}">
			<xsl:if test="$length">
				<xsl:attribute name="size">
					<xsl:value-of select="$length"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, $name)">
						<xsl:value-of select="reflect:getProperty($context, $name)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$value"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</input>
	</xsl:template>
	
	<xsl:template name="field-categories">
		<xsl:param name="selected" select="''" />
		<xsl:param name="prefix" select="''" />
		<xsl:param name="branch"/>
		<xsl:for-each select="$branch">
			<option value="{b:id}">
				<xsl:if test="$selected = b:id">
					<xsl:attribute name="selected">selected</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="concat($prefix,b:name)" />
			</option>
			<xsl:call-template name="field-categories">
				<xsl:with-param name="selected" select="$selected" />
				<xsl:with-param name="prefix" select="concat('-',$prefix)" />
				<xsl:with-param name="branch" select="b:branches/b:branch" />
			</xsl:call-template>		        		
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
