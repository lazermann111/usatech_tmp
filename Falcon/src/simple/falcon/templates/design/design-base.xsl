<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestInfo"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="cmn reflect">
	<xsl:import href="../general/html-base-customize.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />

	<xsl:template name="subtitle">Folio Design</xsl:template>
	<xsl:template name="extra-scripts">
		<link href="{req:addLastModifiedToUri($context, 'css/edit-style.css')}" type="text/css" rel="stylesheet"/>
	</xsl:template>

	<xsl:template name="form-1-per-row">
	  	<xsl:param name="fields"/>
	  	<xsl:for-each select="cmn:node-set($fields)/*">
	  		<tr>
	  			<th><xsl:value-of select="normalize-space(text())"/>:</th>
	  			<td><xsl:call-template name="form-field"/></td>
  			</tr>
	  	</xsl:for-each>
	</xsl:template>

	<xsl:template name="form-table">
	  	<xsl:param name="fields"/>
	  	<tr>
		  	<xsl:for-each select="cmn:node-set($fields)/*">
		  		<th><xsl:value-of select="normalize-space(text())"/></th>
		  	</xsl:for-each>
	  	</tr>
	  	<tr>
		  	<xsl:for-each select="cmn:node-set($fields)/*">
		  		<td><xsl:call-template name="form-field"/></td>
		  	</xsl:for-each>
	  	</tr>
	</xsl:template>

	<xsl:template name="form-fields-only">
		<xsl:param name="fields"/>
	  	<xsl:for-each select="cmn:node-set($fields)/*">
	  		<xsl:call-template name="form-field"/>
	  	</xsl:for-each>
	</xsl:template>

	<xsl:template name="form-field">
  		<xsl:choose>
  			<xsl:when test="local-name() = 'text'">
	  			<input type="text" name="{@name}" value="{@value}" onchange="if(window.fieldChanged) fieldChanged(this);"/>
  			</xsl:when>
  			<xsl:when test="local-name() = 'select'">
			  	<xsl:variable name="value" select="@value"/>
				<select name="{@name}" onchange="if(window.fieldChanged) fieldChanged(this);">
					<xsl:comment>Original value = <xsl:value-of select="$value"/></xsl:comment>
					<xsl:for-each select="node()[local-name() = 'option']">
						<option value="{@value}">
							<xsl:if test="$value = @value">
								<xsl:attribute name="selected">true</xsl:attribute>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="string-length(normalize-space(.)) > 0"><xsl:value-of select="."/></xsl:when>
								<xsl:otherwise><xsl:value-of select="@value"/></xsl:otherwise>
							</xsl:choose>
						</option>
					</xsl:for-each>
				</select>
  			</xsl:when>
  		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
