<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:b="http://simple/bean/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	exclude-result-prefixes="a b reflect">
	<xsl:import href="design-base.xsl"/>
    <xsl:output method="xhtml"/>
  <xsl:param name="context"/>

  <xsl:template name="edit-filters">
  	<xsl:param name="folio"/>
  	<input type="hidden" name="filter_max_index" value="0"/>
  	<div id="filterCountText">0 filter(s)</div>
	<table id="filterTable" class="folio">
		<tr><th>Field</th><th>Operator</th><th>Parameters</th><th>Group</th><th></th></tr>
	</table>
	<input id="saveButton" type="submit" value="Save Filters" onclick="setFilterMax(this.form)"/>
<script type="text/javascript" defer="defer">
	var filterCount = 0;
	var maxFilterIndex = filterCount;
	var filterCountText = document.getElementById("filterCountText")
	var filterTable = document.getElementById("filterTable");
	var filterGroupSelects = new Array();

	function setFilterMax(frm) {
		frm.elements["filter_max_index"].value = maxFilterIndex;
	}

	function filterField(filterId, fieldId, fieldLabel, op, params, group) {
		var filterIndex = ++maxFilterIndex;
		var tr = filterTable.insertRow(filterTable.rows.length);

		var paramSetRow = filterTable.insertRow(filterTable.rows.length);
		paramSetRow.id = "filter_set_param_row_" + filterIndex;
		paramSetRow.className = "paramSetRowHidden";
		var paramSetCell = paramSetRow.insertCell(0); //document.createElement("td");
		paramSetCell.colSpan = 4;

		tr.setAttribute("filter_group",group);

		var td = tr.insertCell(0);
		td.appendChild(document.createTextNode(fieldLabel));
    	var inp = document.createElement("input");
		inp.type = "hidden";
		inp.name = "filter_field_" + filterIndex;
		inp.value = fieldId;
		td.appendChild(inp);
		inp = document.createElement("input");
		inp.type = "hidden";
		inp.name = "filter_id_" + filterIndex;
		inp.value = filterId;
		td.appendChild(inp);
		td = tr.insertCell(1);
		var sel = document.createElement("select");
		sel.onchange = new Function("selectOperator(this.options[this.selectedIndex])");
		sel.name = "filter_op_" + filterIndex;
		sel.filterIndex = filterIndex;<!-- TODO: escape values in quotes -->
  		<xsl:for-each select="/a:base/b:operators/b:operator">
			<xsl:text>addOperator(sel,</xsl:text>
			<xsl:value-of select="b:operatorId"/>
			<xsl:text>,"</xsl:text>
			<xsl:value-of select="b:name"/>
			<xsl:text>","</xsl:text><xsl:value-of select="b:description"/>
			<xsl:text>",'</xsl:text>
			<xsl:value-of select="b:xml"/>
			<xsl:text>',</xsl:text>
			<xsl:value-of select="b:parameterCount"/>
			<xsl:text>);
			</xsl:text>
		</xsl:for-each>
		td.appendChild(sel);
		td = tr.insertCell(2);
		td = tr.insertCell(3);

		td.appendChild(addGroupSelectToFilter(filterIndex,group));

		td = tr.insertCell(4);
		var inp = document.createElement("input");
		inp.type = "button";
		inp.value = "Remove Filter";
		inp.onclick = new Function("removeFilter(this)");
		td.appendChild(inp);

		if(op) setValue(sel, op);
		else setValue(sel, 1);
		if(sel.selectedIndex == -1) setValue(sel, sel.options[0].value);
		selectOperator(sel.options[sel.selectedIndex], params);
		filterCount++;
		updateFilterCountText();
	}

	function addGroupSelectToFilter(index,group){
		var prevGroupSelect = null;
		var i = index;

		while((i &gt; 0) &amp;&amp; (prevGroupSelect == null)){
			prevGroupSelect = filterGroupSelects[--i];
		}

		filterGroupSelects[index] = document.createElement("select");
		filterGroupSelects[index].name = "filter_group_" + index;
		filterGroupSelects[index].setAttribute("groupvalue", group);

		if(prevGroupSelect != null){
			for(i=0;i &lt; prevGroupSelect.options.length; i++){
				filterGroupSelects[index].appendChild(prevGroupSelect.options[i].cloneNode(true));
			}
		}

		return filterGroupSelects[index];
	}

	function removeFilter(button) {
		var tr = button.parentNode.parentNode;
		tr.parentNode.removeChild(tr.nextSibling); //param row
		tr.parentNode.removeChild(tr); //filter row
		filterCount--;
		updateFilterCountText();
	}

	function removeContent(elem) {
		// go backwards so as to not screw up the order while removing
		for(var i = elem.childNodes.length-1; i &gt;= 0 ; i--) {
			switch(elem.childNodes[i].nodeType) {
				case 1: //ELEMENT_NODE
				case 3: //TEXT_NODE
				case 4: //CDATA_SECTION_NODE
					elem.removeChild(elem.childNodes[i]);
			}
		}
	}

	function updateFilterCountText() {
		removeContent(filterCountText);
		filterCountText.appendChild(document.createTextNode("" + filterCount + " filter(s)"));
	}

	function addOption(sel, label, value) {
		var op = document.createElement("option");
		op.appendChild(document.createTextNode(label));
		op.value = value;
		sel.appendChild(op);
		return op;
	}

	function addOperator(sel, operatorId, name, description, xml, paramCount) {
		var option = addOption(sel, name, operatorId);
		option.xml = xml;
		option.description = description;
		option.paramCount = paramCount;
	}

	function selectOperator(option, params) {
		var filterIndex = option.parentNode.filterIndex;
		var nameName = "filter_param_name_" + filterIndex;
		var valueName = "filter_param_value_" + filterIndex;
		var td = option.parentNode.parentNode.nextSibling;
		var inputsCell = document.getElementById("filter_set_param_row_" + filterIndex).firstChild;
		if(params == null) params = new Array();
		else if(!(params instanceof Array)) params = new Array(params);
		//get all current values
		var nameElems = document.getElementsByName(nameName);
		var valueElems = document.getElementsByName(valueName);
		for(var i = params.length; i &lt; nameElems.length; i++) {
			params[i] = new Array();
			params[i][0] = nameElems[i].value;
			params[i][1] = valueElems[i].value;
		}

		//clear tds
		removeContent(td);
		removeContent(inputsCell);

		var re = /&lt;(\w*)(?:\s+attribute="(\w+)")?(?:\s+index="(\d+)")?\x2f&gt;?/g;
		var match;
		var pos = 0;
		var paramCount = 0;
		while((match = re.exec(option.xml)) != null) {
			td.appendChild(document.createTextNode(replaceEntities(option.xml.substring(pos, match.index))));
			pos = re.lastIndex;
			if("field" == match[1]) {
				var span = document.createElement("span");
				span.className = "filterField";
				span.appendChild(document.createTextNode(match[2].toUpperCase() + " VALUE"));
				td.appendChild(span);
			} else if("param" == match[1]) {
				paramCount++;
				var elem = document.createElement("span");
				var curParam = new Array("","");
				if(params.length > 0) curParam = params.shift();
				var paramValue = "";
				if(curParam[1] != "") paramValue = "(" + curParam[1] + ")";
				td.appendChild(document.createTextNode(curParam[0]+paramValue));
				var inp = document.createElement("input");
				inp.type = "text";
				inp.id = nameName;
				inp.name = nameName;
				inp.value = curParam[0];
				inp.onchange = new Function("changedParamData(this.parentNode.parentNode);");
				inputsCell.appendChild(document.createTextNode("Parameter " + paramCount + " Name: "));
				inputsCell.appendChild(inp);

				inputsCell.appendChild(document.createTextNode("  Parameter " + paramCount + " Value: "));

				inp = document.createElement("input");
				inp.type = "text";
				inp.id = valueName;
				inp.name = valueName;
				inp.value = curParam[1];
				inp.onchange = new Function("changedParamData(this.parentNode.parentNode);");
				inputsCell.appendChild(inp);
				inputsCell.appendChild(document.createElement("br"));
			} else {
				td.appendChild(document.createTextNode("[INVALID ATTRIBUTE '" + match[1] + "']"));
			}
		}
		td.appendChild(document.createTextNode(replaceEntities(option.xml.substr(pos))));

		var but = document.createElement("a");
		but.appendChild(document.createTextNode("Edit"));
		but.href = "javascript:toggleParamRow('filter_set_param_row_" + filterIndex + "');";
		td.appendChild(document.createTextNode("  ["));
		td.appendChild(but);
		td.appendChild(document.createTextNode("]"));
	}

	function changedParamData(paramRow){
		menu = paramRow.previousSibling.cells[1].firstChild;
		selectOperator(menu.options[menu.selectedIndex]);
	}

	function toggleParamRow(id){
		var row = document.getElementById(id);

		if(row.className == "paramSetRowHidden"){
			row.className = "paramSetRowShown";
			row.previousSibling.cells[0].rowSpan = 2;
		} else {
			row.className = "paramSetRowHidden";
			row.previousSibling.cells[0].rowSpan = 1;
		}
	}

	function replaceEntities(s) {
		var re = /&amp;(\w+);/g;
		var out = "";
		var pos = 0;
		while((match = re.exec(s)) != null) {
			out += s.substring(pos, match.index);
			pos = re.lastIndex;
			if("quot" == match[1]) out += '"';
			else if("lt" == match[1]) out += "&lt;";
			else if("gt" == match[1]) out += "&gt;";
			else if("amp" == match[1]) out += "&amp;";
			else if("#" == match[1].charAt(0)) out += String.fromCharCode(parseInt(match[1].substr(1)));
		}
		out += s.substr(pos);
		return out;
	}

	function setValue(select, value) {
	    if(document.layers) {
	        for(var i = 0; i &lt; select.options.length; i++) {
	            if(select.options(i).value == value) {
	                select.selectedIndex = i;
	                return;
	            }
	        }
	        select.selectedIndex = -1;
	    } else {
	        select.value = value;
	    }
	}

	function loadFilters() {
		<!-- folio/filter is always root filter -->
		<xsl:for-each select="$folio/b:filterGroup">
			<xsl:call-template name="processFilterGroup">
				<xsl:with-param name="group" select="'1'"/>
			</xsl:call-template>
		</xsl:for-each>
	}

	setTimeout("loadFilters();",1);
	//updateFilterCountText();
</script>
	</xsl:template>

	<xsl:template name="processFilterGroup">
		<xsl:param name="filterGroup"/>
		<xsl:param name="group" select="'1'" />
		<xsl:for-each select="b:filterGroup">
			<xsl:variable name="prefix"><xsl:if test="$group"><xsl:value-of select="$group"/>.</xsl:if></xsl:variable>
			<xsl:call-template name="processFilterGroup">
				<xsl:with-param name="group" select="concat($prefix, position())"/>
			</xsl:call-template>
		</xsl:for-each>
		<xsl:for-each select="b:filter">
			<xsl:call-template name="processFilter">
				<xsl:with-param name="group" select="$group"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="processFilter">
		<xsl:param name="group" select="'1'" />
		<xsl:text>filterField("</xsl:text>
		<xsl:value-of select="@b:filterId"/>
		<xsl:text>",</xsl:text>
		<xsl:value-of select="@b:fieldId"/>
		<xsl:text>,"</xsl:text>
		<xsl:value-of select="@b:fieldLabel"/>
		<xsl:text>","</xsl:text>
		<xsl:value-of select="@b:operatorId"/>
		<xsl:text>",new Array(</xsl:text>
		<xsl:for-each select="b:param">
			<xsl:if test="position() &gt; 1">,</xsl:if>
			<xsl:text>new Array("</xsl:text>
			<xsl:value-of select="@b:name"/>
			<xsl:text>","</xsl:text>
			<xsl:value-of select="@b:value"/>
			<xsl:text>")</xsl:text>
		</xsl:for-each>
		<xsl:text>),"</xsl:text>
		<xsl:value-of select="$group"/>
		<xsl:text>");</xsl:text>
	</xsl:template>
</xsl:stylesheet>