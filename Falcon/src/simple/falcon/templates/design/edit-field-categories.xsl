<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:b="http://simple/bean/1.0" 
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml" 
	exclude-result-prefixes="b reflect">
	<xsl:import href="design-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />
	
	<xsl:template name="contents">
		<xsl:variable name="categoryId" select="reflect:getProperty($context,'categoryId')" />
		<xsl:variable name="category" select="//b:branch[b:id = $categoryId]" />
		<span class="section">Edit Category</span>
		<form id="categoryForm" name="editCategory" action="./save_field_category.i" target="_top">
			<input type="hidden" name="id" value="{$category/b:id}" />
			<table>
				<tr>
					<td>Label</td>
					<td><input type="text" name="label" value="{$category/b:name}" size="64" required="true" /></td>
				</tr>
				<tr>
					<td>Parent Category</td>
					<td>
						<select name="parentId">
							<option value="">Top</option>	
							<xsl:call-template name="field-categories">
								<xsl:with-param name="selected" select="$category/../../b:id" />
								<xsl:with-param name="self" select="$categoryId" />
								<xsl:with-param name="prefix" select="'-'" />
								<xsl:with-param name="branch" select="//b:field-hierarchy/b:branches/b:branch" />
							</xsl:call-template>
						</select>
					</td>
				</tr>
			</table>
			<input type="button" onclick="doSubmit()" value="Save Field Category" />
		</form>
		<script type="text/javascript">
function doSubmit(){
	var canSubmit = true;
	var inputs = document.getElementsByTagName("input");
	for(var i=0; i &lt; inputs.length; i++){
		if((inputs[i].getAttribute("required") == "true") &amp;&amp; (inputs[i].value == "")){
			canSubmit = false;
			break;
		}
	}
	if(!canSubmit) {
		alert("Label is required");
	} else {
		document.getElementById("categoryForm").submit();
	}
}
		</script>
	</xsl:template>
	
	<xsl:template name="field-categories">
		<xsl:param name="selected" select="''" />
		<xsl:param name="self" select="''" />
		<xsl:param name="prefix" select="''" />
		<xsl:param name="branch"/>
		<xsl:for-each select="$branch[b:id != $self]">
			<option value="{b:id}">
				<xsl:if test="$selected = b:id">
					<xsl:attribute name="selected">selected</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="concat($prefix,b:name)" />
			</option>
			<xsl:call-template name="field-categories">
				<xsl:with-param name="selected" select="$selected" />
				<xsl:with-param name="prefix" select="concat('-',$prefix)" />
				<xsl:with-param name="branch" select="b:branches/b:branch" />
			</xsl:call-template>		        		
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
