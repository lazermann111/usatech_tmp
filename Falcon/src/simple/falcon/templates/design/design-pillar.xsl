<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:conv="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
	xmlns:javaclass="http://simple/xml/extensions/java/java.lang.Class"
	exclude-result-prefixes="b reflect conv javaclass">
	<xsl:import href="design-base.xsl"/>
    <xsl:output method="xhtml"/>
  <xsl:param name="context"/>

  <xsl:template name="contents">
  	<xsl:variable name="index" select="reflect:getProperty($context, 'pillarIndex')"/>
  	<xsl:call-template name="edit-pillar">
  		<xsl:with-param name="pillarIndex" select="$index"/>
  		<xsl:with-param name="pillars" select="//b:folio/b:pillars"/>
  		<xsl:with-param name="folioId" select="//b:folio/@b:folioId"/>
  	</xsl:call-template>
  </xsl:template>

  <xsl:template name="edit-pillar">
  	<xsl:param name="pillarIndex" />
  	<xsl:param name="pillars" />
  	<xsl:param name="folioId" />
  	<xsl:variable name="pillar" select="$pillars/b:pillar[number($pillarIndex)]" />
  	<form name="pillarForm" action="save_pillar.i" target="_parent">
  		<input type="hidden" name="folioId" value="{$folioId}"/>
  		<input type="hidden" name="pillarIndex" value="{$pillarIndex}"/>
  		<input type="hidden" name="maxFieldIndex" value="{count($pillars/b:pillar)}"/>
  		<table><tr valign="top"><td>
  		<table class="form-layout">
  		<!-- actionFormat, description, displayFormat, groupingLevel, helpFormat, label, pillarType, sortFormat, sortType, style?, fields (fieldId, sortOrder, aggType) -->
  		<xsl:call-template name="form-1-per-row">
  			<xsl:with-param name="fields">
  				<select name="newPillarIndex" value="{$pillarIndex}">Pillar Index
  					<xsl:for-each select="$pillars/b:pillar">
  						<option value="{position()}"><xsl:value-of select="position()"/></option>
  					</xsl:for-each>
 				</select>
 				<xsl:variable name="pillarTypeInt" select="conv:getInt(conv:convert(javaclass:forName('simple.falcon.engine.Pillar$PillarType'),string($pillar/@b:pillarType)))"/>
  				<select name="pillarType" value="{$pillarTypeInt}">Pillar Type
	  				<option value="1">Text</option>
	  				<option value="2">Image</option>
	  				<option value="3">Note</option>
	  				<option value="4">Axis (Charts)</option>
	  				<option value="5">Value (Charts)</option>
	  				<option value="6">Row-level</option>
	  				<option value="7">Checkbox</option>
	  				<option value="8">Button</option>
	  				<option value="9">Radio</option>
    			</select>
  				<text name="labelFormat" value="{$pillar/@b:labelFormat}">Column Label</text>
  				<text name="description" value="{$pillar/@b:description}">Description</text>
  				<select name="groupingLevel"> <!--value="{$pillar/b:groupingLevel}">-->
					<xsl:attribute name="value">
						<xsl:choose>
							<xsl:when test="$pillar/@b:aggregated = 'true'">
								<xsl:value-of select="-1 * ($pillar/@b:groupingLevel + 1)" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$pillar/@b:groupingLevel" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:text>Grouping Level</xsl:text>
					<!--
	  				<option value="-1">Show Totals</option>
	  				<option value="0">Show Detail</option>
	  				<option value="1">Level One</option>
	  				<option value="2">Level Two</option>
	  				<option value="3">Level Three</option>
	  				<option value="4">Level Four</option>
	  				<option value="-2">Level One, with Totals</option>
	  				<option value="-4">Level Three, with Totals</option>
	  				<option value="-5">Level Four, with Totals</option>
	  				 -->
					<option value="0">Show Totals at Each Level</option>
	  				<option value="1">Cross-Tab Column</option>
	  				<option value="2">Detail Row</option>
	  				<option value="3">Level One Group (Table)</option>
	  				<option value="4">Level Two Group</option>
	  				<option value="5">Level Three Group</option>
	  				<option value="-3">Level Two, with Totals</option>
  				</select>
  				<select name="sortType" value="{$pillar/@b:sortType}">Sort Type
	  			  	<option value="STRING">String</option>
	  			  	<option value="DATE">Date</option>
	  			  	<option value="NUMBER">Number</option>
  				</select>
  			</xsl:with-param>
  		</xsl:call-template></table></td>
  		<td><table class="form-layout">
  			<xsl:call-template name="form-1-per-row">
  			<xsl:with-param name="fields">
  				<text name="actionFormat" value="{$pillar/@b:actionFormat}">Action Format</text>
  				<text name="displayFormat" value="{$pillar/@b:displayFormat}">Display Format</text>
  				<text name="helpFormat" value="{$pillar/@b:helpFormat}">Tooltip Format</text>
  				<text name="sortFormat" value="{$pillar/@b:sortFormat}">Sort Format</text>
  				<text name="styleFormat" value="{$pillar/@b:styleFormat}">Style Format</text>
  				<text name="percentFormat" value="{$pillar/@b:percentFormat}">Percent Format</text>
  				<text name="width" value="{$pillar/@b:width}">Width</text>
  			</xsl:with-param>
  		</xsl:call-template>
  		</table></td><td><div><span class="section">Fields</span>
  			<table id="pillarFields" class="selectableList">
  				<thead><tr><th>Field</th><th>Sort Order</th><th>Total Type</th></tr></thead>
  				<tbody>
  				<xsl:for-each select="$pillar/b:field">
  					<xsl:variable name="fieldIndex" select="position()"/>
  					<tr onclick="selectField({$fieldIndex}, this);">
	  					<td><xsl:value-of select="@b:fieldLabel"/>
	  						<input type="hidden" name="{concat('fieldId', $fieldIndex)}" value="{@b:fieldId}"/>
	  					</td>
	  					<td>
	  						<xsl:call-template name="form-fields-only">
								<xsl:with-param name="fields">
	  								<select name="{concat('sortOrder', $fieldIndex)}" value="{@b:sortOrder}">
										<option value="">Default</option>
										<option value="ASC">Ascending</option>
										<option value="DESC">Descending</option>
										<option value="NONE">None</option>
									</select>
	  							</xsl:with-param>
  							</xsl:call-template>
	  					</td>
	  					<td>
	  						<xsl:call-template name="form-fields-only">
								<xsl:with-param name="fields">
								  <xsl:choose>
								  <xsl:when test="string-length(@b:aggregateType) = 0">
								  	<select name="{concat('aggregateType', $fieldIndex)}" value="0">
								  	<option value="0">None</option>
										<option value="1">Sum</option>
										<option value="2">Average</option>
										<option value="3">Maximum</option>
										<option value="4">Minimum</option>
										<option value="5">Count</option>
										<option value="6">Distinct</option>
										<option value="7">Array</option>
										<option value="8">First</option>
										<option value="9">Last</option>
										<option value="10">Set</option>
									</select>	
								  </xsl:when>
								  <xsl:otherwise>
								    <xsl:variable name="aggregateTypeInt" select="conv:getInt(conv:convert(javaclass:forName('simple.results.Results$Aggregate'),string(@b:aggregateType)))"/>
								  	<select name="{concat('aggregateType', $fieldIndex)}" value="{$aggregateTypeInt}">
								  	<option value="0">None</option>
										<option value="1">Sum</option>
										<option value="2">Average</option>
										<option value="3">Maximum</option>
										<option value="4">Minimum</option>
										<option value="5">Count</option>
										<option value="6">Distinct</option>
										<option value="7">Array</option>
										<option value="8">First</option>
										<option value="9">Last</option>
										<option value="10">Set</option>
									</select>	
								  </xsl:otherwise>
									</xsl:choose>
									
	  							</xsl:with-param>
  							</xsl:call-template>
	   					</td>
   					</tr>
  				</xsl:for-each>
  				</tbody>
  			</table>
  			<input type="button" id="deleteFieldButton" onclick="deleteFieldFromPillar();" value="Delete Field" enabled="false"/>
  			</div>
  		</td></tr>
  		<tr><td colspan="3"><input id="savePillarButton" type="button" onclick="savePillar()" value="Save Changes"/></td></tr>
  		</table>
  	</form>
  	<form name="addFieldForm" action="add_field.i">
	  	<input type="hidden" name="unframed" value="true"/>
        <input type="hidden" name="folioId" value="{$folioId}"/>
	  	<input type="hidden" name="pillarIndex" value="{$pillarIndex}"/>
	  	<input type="hidden" name="fieldId"/>
  	</form>
    <script type="text/javascript" defer="defer">
	  		var selectedFieldIndex = -1;
	  		var tableBody = document.getElementById("pillarFields").tBodies[0];
	  		var tableRows = tableBody.rows;
	  		var deleteFieldButton = document.getElementById("deleteFieldButton");
	  		var savePillarButton = document.getElementById("savePillarButton");

	  		function selectField(index, tr) {
	  			selectedFieldIndex = index;
	  			for(var i = 0; i &lt; tableRows.length; i++) {
	  				tableRows[i].className = "";
	  			}
	  			tr.className = "selectedRow";
	  			deleteFieldButton.disabled = (index &lt; 0);
	  		}

	  		function addField(fieldId) {
	  			document.forms["addFieldForm"].elements["fieldId"].value = fieldId;
	  			document.forms["addFieldForm"].submit();
	  		}

	  		function deleteFieldFromPillar() {
	  			if(selectedFieldIndex > 0) {
	  				var tr = tableRows[selectedFieldIndex-1];
	  				if(tr) tr.parentNode.removeChild(tr);
	  			}
	  		}

	  		function savePillar() {
	  			if((document.forms["pillarForm"].elements["folioId"].value == null) ||
	  				(document.forms["pillarForm"].elements["folioId"].value == 0) ||
	  				(document.forms["pillarForm"].elements["folioId"].value == ''))
	  				{
	  				alert('You must save your Folio before you save a Pillar on it.');
	  			} else {
	  				document.forms["pillarForm"].submit();
	  			}
	  		}
  	</script>
  </xsl:template>

</xsl:stylesheet>
