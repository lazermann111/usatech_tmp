<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect">
	<xsl:output method="xhtml"/>
		
	<xsl:template match="/">
		<div class="caption">Field Editor</div>
		<div class="iframeContainer" style="width: 70%">
			<iframe name="editFieldsFrame"/>
		</div>
		<div class="iframeContainer" style="width: 29.6%">
			<iframe src="edit_fields_tree.i?unframed=true" name="editFieldsTreeFrame"/>
		</div>
	</xsl:template>
</xsl:stylesheet>