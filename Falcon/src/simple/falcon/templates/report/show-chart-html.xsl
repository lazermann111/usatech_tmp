<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:xalan="http://xml.apache.org/xalan"
	xmlns:fr="http://simple/falcon/report/1.0" 
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr">	
	<xsl:import href="show-base.xsl"/>
	<xsl:variable name="design-mode" select="false()" />
	<xsl:variable name="group-sections" select="2.0"/>

	<xsl:template name="subtitle"><xsl:value-of select="fr:report/@fr:title"/></xsl:template>
	
	<xsl:template name="contents">
		<xsl:variable name="report" select="fr:report"/>
		<xsl:for-each select="$report">
			<p class="title2">
				<xsl:value-of select="@fr:title" />
			</p>
			<p class="title3">
				<xsl:value-of select="@fr:subtitle" />
			</p>
			<xsl:choose>
				<xsl:when test="fr:error">
					<p>
						<b style="color: red">An error occurred:</b>
					</p>
					<p>
						<xsl:value-of select="fr:error" />
					</p>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="group-layout" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="note-desc">
		<xsl:for-each select="fr:columns/fr:column[@fr:column-type = 'note']">
			<xsl:variable name="note-id" select="concat('note', @fr:id)" />
			<p class="{$note-id}" id="{$note-id}">
				<xsl:value-of select="@fr:description" />
			</p>
			<!-- This isn't really correct HTML (style should be in HEAD) but it works in IE and FireFox so we use it -->
			<style>
				.<xsl:value-of select="$note-id" />{ <xsl:value-of select="@fr:style" /> }
			</style>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="group-layout">
		<xsl:variable name="allcolumns" select="fr:columns/fr:column[@fr:column-type != 'note' and @fr:column-type != 'all']" />
		<xsl:call-template name="note-desc"/>
		<table class="folio" id="reportTable">
			<xsl:call-template name="groupheader">
				<xsl:with-param name="columns" select="$allcolumns"/>
			</xsl:call-template>
		</table>
	</xsl:template>

	<xsl:template name="groupheader">
		<xsl:param name="columns" />
		<xsl:if test="@fr:level >= 1"> 
			<xsl:variable name="group-data" select="fr:group-data/*[local-name()!='note' and local-name()!='all' and local-name()!='axis' and local-name()!='value']" />
			<xsl:if test="$group-data">
				<xsl:variable name="all-data" select="fr:group-data/*[local-name()='all']" />
				<xsl:variable name="all-style" select="$all-data/@fr:style" />
				<xsl:variable name="default-link" select="$all-data/@fr:link[position()=1]" />
				<xsl:variable name="default-tip" select="$default-link/../@fr:tip" />		
				<tbody>
					<tr class="{concat('groupHeaderRow', @fr:level)}">
						<xsl:if test="string-length(normalize-space($all-style)) > 0">
							<xsl:attribute name="style">
								<xsl:for-each select="$all-style[string-length(normalize-space()) > 0]">
									<xsl:value-of select="normalize-space(.)"/>;
								</xsl:for-each>
							</xsl:attribute>
						</xsl:if>
						<td>
						<table class="groupInfo">
						<xsl:variable name="group-half-count" select="round(count($group-data) div $group-sections)" />			 
						<xsl:for-each select="$group-data[position() &lt;= $group-half-count]">
							<tr>
							<xsl:variable name="group-index" select="position() mod $group-sections" />			 						
							<xsl:for-each select="$group-data[position() mod $group-sections = $group-index]">
								<xsl:variable name="colid" select="@fr:id" />
								<xsl:variable name="column" select="$columns[@fr:id = $colid]" />
								<td><span class="groupLabel" title="{@fr:description}">
									<xsl:value-of select="$column" />:
									<span class="groupValue">
									<xsl:call-template name="tablecellcontent">
										<xsl:with-param name="default-link" select="$default-link"/>
										<xsl:with-param name="default-tip" select="$default-tip"/>	
									</xsl:call-template>
									</span>
									</span>
								</td>
							</xsl:for-each>
							</tr>
						</xsl:for-each>
						</table>
						</td>
					</tr>
				</tbody>
			</xsl:if>
		</xsl:if>
		<!-- process sub-elements -->
		<xsl:for-each select="fr:group">
			<xsl:call-template name="groupheader">
				<xsl:with-param name="columns" select="$columns" />
			</xsl:call-template>
		</xsl:for-each>
		<xsl:for-each select="fr:chart">
			<tbody>
				<tr>
					<td class="chart">
					<xsl:choose>
						<xsl:when test="string-length(normalize-space(.)) = 0 and @fr:reference-id">
							<img width="{@fr:width}" height="{@fr:height}">
								<xsl:attribute name="src">get_chart.i?chartContentType=<xsl:value-of select="@fr:content-type"/>&amp;chartReferenceId=<xsl:value-of select="@fr:reference-id"/></xsl:attribute>
							</img>
						</xsl:when>
						<xsl:when test="@fr:display-type='inline-image'">
							<img width="{@fr:width}" height="{@fr:height}">
								<xsl:attribute name="src">data:<xsl:value-of select="@fr:content-type"/>;base64,<xsl:value-of select="."/></xsl:attribute>								
							</img>
						</xsl:when>
						<xsl:otherwise>
							<object width="{@fr:width}" height="{@fr:height}" codetype="application/java"
								code="helpers.ImageDisplayer" codebase="code">
								<param name="data" value="{.}"/>
							</object>
						</xsl:otherwise>
					</xsl:choose>
					</td>
				</tr>
			</tbody>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="tablecellcontent">
		<xsl:param name="default-link"/>
		<xsl:param name="default-tip"/>
		<xsl:variable name="colnr" select="position()" />
		<a>
			<xsl:if test="string-length(normalize-space(.)) > 0">			
				<xsl:if test="string-length(normalize-space(@fr:style)) > 0">
					<xsl:attribute name="style">
						<xsl:value-of select="@fr:style" />
					</xsl:attribute>
				</xsl:if>		
				<xsl:choose>
					<xsl:when test="string-length(normalize-space(@fr:link)) > 0">
						<xsl:attribute name="href">
							<xsl:value-of select="@fr:link" />
						</xsl:attribute>
						<xsl:attribute name="title">
							<xsl:value-of select="@fr:tip" />
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="string-length(normalize-space($default-link)) > 0">
						<xsl:attribute name="href">
							<xsl:value-of select="$default-link" />
						</xsl:attribute>
						<xsl:attribute name="title">
							<xsl:value-of select="$default-tip" />
						</xsl:attribute>
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="local-name()='text'">
						<xsl:value-of select="." />
					</xsl:when>
					<xsl:when test="local-name()='image'">
						<img src="{.}" alt="{@fr:tip}" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="." />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</a>
		<!--  notes -->
		<xsl:call-template name="notes">
			<xsl:with-param name="start" select="." />
			<xsl:with-param name="index" select="$colnr + 1" />
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="notes">
		<xsl:param name="start" />
		<xsl:param name="index" />
		<xsl:for-each select="$start/following-sibling::*[position()=1 and local-name()='note']">
			<xsl:if test="string-length(normalize-space(.)) > 0">
				<sup>
					&#160;
					<a href="{@fr:link}" title="{@fr:tip}" class="{concat('note', $index)}">
						<xsl:value-of select="." />
					</a>
				</sup>
			</xsl:if>
			<xsl:call-template name="notes">
				<xsl:with-param name="start" select="." />
				<xsl:with-param name="index" select="$index + 1" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
