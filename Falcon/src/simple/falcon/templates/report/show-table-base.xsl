<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="http://simple/aggregating/1.0"
    xmlns:b="http://simple/bean/1.0"
    xmlns:cmn="http://exslt.org/common"
    xmlns:r="http://simple/results/1.0"
    xmlns:x="http://simple/xml/extensions/java/simple.xml.XSLUtils"
    xmlns:x2="http://simple/translate/2.0"
    exclude-result-prefixes="a b cmn r x x2">
	<xsl:import href="../general/html-base-customize.xsl"/>

	<xsl:template name="subtitle">
		<xsl:if test="string-length(normalize-space(/a:base/b:title)) > 0">
			<xsl:value-of select="/a:base/b:title"/>
		</xsl:if>
	</xsl:template>

    <xsl:template name="extra-scripts">
    </xsl:template>

	<xsl:template name="contents">
	   <table class="reportTitle">
            <tr>
                <td>
                    <xsl:if test="/a:base/b:rundate">
                        <div class="rundate">Run Date: <xsl:value-of select="/a:base/b:rundate"/></div>
                    </xsl:if>
                    <xsl:if test="string-length(normalize-space(/a:base/b:title)) > 0">
                        <div class="title0">
                            <xsl:value-of select="/a:base/b:title" />
                        </div>
                        <div class="folio-line">&#160;</div>
                    </xsl:if>
	    <xsl:for-each select="/a:base/a:results/r:results">
		    <xsl:variable name="sortTypesRaw">
	           <xsl:for-each select="r:column"><sortType><xsl:call-template name="get-sort-type"/></sortType></xsl:for-each>
	        </xsl:variable>         
	        <xsl:variable name="sortTypes" select="cmn:node-set($sortTypesRaw)"/>	              
		    <table class="folio">
		       <tbody>
	                 <tr class="headerRow">                 
						<xsl:for-each select="r:column">
						     <xsl:variable name="sortType"><xsl:call-template name="get-sort-type"/></xsl:variable>
							 <th colId="{position()}">
							    <a data-toggle="sort" data-sort-type="{$sortType}" title="{@r:name}">
	                                <xsl:value-of select="@r:name" />
	                            </a>
	                       </th>
						</xsl:for-each>
				     </tr>
				</tbody>
				<tbody>
				    <xsl:for-each select="r:row">
	                    <tr>
			                <xsl:attribute name="class">
			                    <xsl:choose>
			                        <xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
			                        <xsl:otherwise>oddRow</xsl:otherwise>
			                    </xsl:choose>
			                </xsl:attribute>
			                <xsl:for-each select="*">
			                     <xsl:variable name="col" select="position()"/>
                                 <xsl:variable name="sortType" select="$sortTypes/sortType[$col]"/>
			                     <td colId="{position()}" data-sort-value="{@r:value}">
			                         <xsl:attribute name="style">text-align: <xsl:choose>
			                             <xsl:when test="$sortType = 'NUMBER'">right</xsl:when>
			                             <xsl:otherwise>left</xsl:otherwise>
			                         </xsl:choose>;</xsl:attribute>
			                     <xsl:choose>
                                         <xsl:when test="string-length(.) &gt; 255"><textarea><xsl:value-of select="."/></textarea></xsl:when>
                                         <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                                 </xsl:choose>
			                     </td>            
			                </xsl:for-each>
			            </tr>            
	                </xsl:for-each>
	            </tbody>
		    </table>
	    </xsl:for-each>
	    <div class="copyright"><x2:translate key="report-copyright"></x2:translate></div>
                </td>
            </tr>
        </table>
	    
	</xsl:template>

    <xsl:template name="get-sort-type">
        <xsl:choose>
            <xsl:when test="x:isSubclass(string(@r:class), 'java.util.Date') or x:isSubclass(string(@r:class), 'java.util.Calendar') or string(@r:class) = 'oracle.sql.TIMESTAMP' or string(@r:class) = 'oracle.sql.DATE' or string(@r:class) = 'oracle.sql.TIMESTAMPTZ' or string(@r:class) = 'oracle.sql.TIMESTAMPLTZ'">DATE</xsl:when>
            <xsl:when test="x:isSubclass(string(@r:class), 'java.lang.Number')">NUMBER</xsl:when>
            <xsl:otherwise>STRING</xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>