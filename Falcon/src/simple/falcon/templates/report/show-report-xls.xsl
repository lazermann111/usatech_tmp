<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr">
	<xsl:import href="show-report-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="extra-styles">
		<style type="text/css">
td {
	border: 0.5pt solid #D0D0D0;
}
.groupFooterRow1 {
	background-color: #D0D0D0;
}
.groupFooterRow0 {
	background-color: #FFFF99;
}
		</style>
	</xsl:template>
    
    <xsl:template name="tablecolumns">
        <xsl:param name="columns"/>
        <xsl:param name="xtabs"/>
        <xsl:call-template name="tablecolumns_xls">
	        <xsl:with-param name="columns" select="$columns"/>
	        <xsl:with-param name="xtabs" select="$xtabs"/>        
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template name="tablecelldata">
        <xsl:if test="string-length(normalize-space(@fr:style)) > 0">
            <xsl:attribute name="style"><xsl:value-of select="@fr:style"/></xsl:attribute>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="local-name()='text'">
                <xsl:value-of select="." />
            </xsl:when>
            <xsl:when test="local-name()='image'">
                <img src="{.}" alt="{@fr:tip}" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="." />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
