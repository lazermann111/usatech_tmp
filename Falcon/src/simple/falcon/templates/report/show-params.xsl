<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect b x2">
	<xsl:import href="../general/html-base-customize.xsl"/>
	<xsl:output method="xhtml"/>
	<xsl:param name="context" />
    <xsl:key name="param-names" match="/b:params/b:param" use="@b:name"/>

	<xsl:template name="contents">
		<div class="caption"><xsl:value-of select="reflect:getProperty($context, 'reportTitle')" /></div>
		<form action="run_report_async.i" class="parameters">
  		<input type="hidden" name="promptForParams" value="false"/>
  		<input type="hidden" name="folioId" value="{reflect:getProperty($context, 'folioId')}"/>
  		<input type="hidden" name="unframed" value="{reflect:getProperty($context, 'unframed')}" />
  		<div class="instructions"><x2:translate key="report-params-prompt">Please enter a value for each of the following parameters</x2:translate>:</div>
  		<table class="params">
  		<xsl:for-each select="b:params/b:param[string-length(normalize-space(@b:name)) > 0]">
  			<xsl:variable name="name" select="b:name"/>
  			<xsl:choose>
  				<xsl:when test="preceding-sibling::b:param[@b:name=$name]"></xsl:when>
  				<xsl:otherwise>
		  			<tr>
		  				<th><xsl:value-of select="@b:prompt"/>:</th>
		  				<td>
			  				<input type="text" name="params.{@b:name}">
								<xsl:attribute name="value">
									<xsl:choose>
										 <xsl:when test="string-length(normalize-space(@b:value)) > 0"><xsl:value-of select="@b:value"/></xsl:when>
										 <xsl:when test="reflect:hasProperty($context, concat('params.', @b:name))"><xsl:value-of select="reflect:getProperty($context, string(concat('params.', @b:name)))"/></xsl:when>
									 </xsl:choose>
								 </xsl:attribute>
						 	</input>
						 </td>
					 </tr>
				</xsl:otherwise>
			</xsl:choose>
  		</xsl:for-each>
  		</table>
		<input type="submit"><xsl:attribute name="value"><x2:translate key="report-params-submit">Run Report</x2:translate></xsl:attribute></input>
  		</form>
    </xsl:template>

    <xsl:template name="subtitle"><x2:translate key="report-params-title">Enter Parameters for Report</x2:translate></xsl:template>

</xsl:stylesheet>
