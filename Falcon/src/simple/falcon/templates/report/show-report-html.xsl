<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:img="http://simple/falcon/image/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr img x2 p">
	<xsl:import href="show-report-base.xsl"/>
	<xsl:import href="show-base.xsl"/>
	<xsl:output method="xhtml"/>
		
	<xsl:template name="output-type-menu-content">
		<xsl:param name="run-report-action" />
		<xsl:variable name="currentType" select="@fr:output-type" />
		<div class="output-type-menu">
			<form name="changeOutput" action="{$run-report-action}" method="post">
				<xsl:choose>
					<xsl:when test="@fr:reportId">
						<input type="hidden" name="reportId" value="{@fr:reportId}"/>
					</xsl:when>
					<xsl:when test="count(fr:folio/@fr:folioId[string-length() > 0]) = 1">
						<input type="hidden" name="folioId" value="{fr:folio[1]/@fr:folioId}"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="fr:folio/@fr:folioId[string-length() > 0]">
							<input type="hidden" name="folioIds" value="{.}"/>
						</xsl:for-each>
					</xsl:otherwise>					
				</xsl:choose>					
				<xsl:call-template name="report-parameters-form">
					<xsl:with-param name="overrides">
						<p:parameter name="outputType" value="{$currentType}" />
						<p:parameter name="resultsType" value="cache" />
						<p:parameter name="promptForParams" value="FIX" />
						<xsl:if test="@fr:chart-type"><p:parameter name="chartType" value="{@fr:chart-type}" /></xsl:if>
						<p:parameter name="doChartConfigUpdate" value="false" />
						<xsl:choose>
						  <xsl:when test="/fr:report/fr:scene/@fr:layout = 'fragment'"><p:parameter name="fragment" value="true" /></xsl:when>
						  <xsl:when test="/fr:report/fr:scene/@fr:layout = 'unframed'"><p:parameter name="unframed" value="true" /></xsl:when>                          
						</xsl:choose>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:variable name="saveAction" select="normalize-space(/fr:report/fr:parameters/fr:parameter[@fr:name='saveAction']/@fr:value)" />
				<xsl:if test="string-length($saveAction) &gt; 0">
					<button title="Save Report" onclick="submitOutputForm(this.form, '{$saveAction}', '{$currentType}', 'false','false');" id="report-button-save"><div/></button>
				</xsl:if>				 
				<xsl:variable name="referrer" select="normalize-space(/fr:report/fr:parameters/fr:parameter[@fr:name='referrer']/@fr:value)" />
                <xsl:if test="string-length($referrer) &gt; 0">
					<button title="Configure Parameters" onclick="submitOutputForm(this.form, '{$referrer}', '{$currentType}');" id="report-button-config"><div/></button>
				</xsl:if>				 
				<xsl:if test="$currentType != 22 and $currentType != 28">
					<button title="View as HTML" onclick="submitOutputForm(this.form, '{$run-report-action}', 22, 'false');" id="report-button-html"><div/></button>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$currentType = 28">
						<button title="View Chart in PDF" onclick="submitOutputForm(this.form, '{$run-report-action}', 31, 'true');" id="report-button-pdf"><div/></button>
					</xsl:when>
					<xsl:otherwise>
						<button title="View as PDF" onclick="submitOutputForm(this.form, '{$run-report-action}', 24, 'false');" id="report-button-pdf"><div/></button>
					</xsl:otherwise>
				</xsl:choose>
				<button title="View as Excel" onclick="submitOutputForm(this.form, '{$run-report-action}', 27, 'false');" id="report-button-excel"><div/></button>
				<xsl:variable name="allow-chart-config" select="fr:directives/fr:directive[@fr:name = 'allow-chart-config']/@fr:value" />
				<xsl:if test="$allow-chart-config and $allow-chart-config = 'true'">
					<button title="Create Chart from Data" src="./images/icons/chart.jpg" onclick="submitOutputForm(this.form, 'configure_chart.i', 28, 'false');"  id="report-button-chart"><div/></button>
				</xsl:if>
				<script type="text/javascript">
function submitOutputForm(form, action, outputType, doChartConfigUpdate) {
	form.action = action;
	form.outputType.value = outputType;
	form.doChartConfigUpdate.value = doChartConfigUpdate;
	form.submit();
}
				</script>
			</form>
		</div>
	</xsl:template>
				
	<xsl:template name="output-type-menu">
		<xsl:variable name="run-report-action" select="/fr:report/fr:scene/@fr:runReportAction"/>
		<xsl:if test="string-length(normalize-space($run-report-action)) &gt; 0">
			<xsl:call-template name="output-type-menu-content">
		      <xsl:with-param name="run-report-action" select="normalize-space($run-report-action)"/>
		    </xsl:call-template>
	    </xsl:if>
	</xsl:template>
</xsl:stylesheet>
