<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:ext="http://exslt.org/common"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="ext fr">
	<!-- Useful template to call for getting current request parameters -->
	<xsl:template name="report-parameters-form">
		<xsl:param name="overrides"/>
		<xsl:variable name="override-set" select="ext:node-set($overrides)/*"/>
		<xsl:variable name="parameter-set" select="/fr:report/fr:parameters/fr:parameter[not(starts-with(@fr:name, '~'))]"/>
		<xsl:for-each select="$override-set[@value]">
			<input type="hidden" name="{@name}" value="{@value}"/>
		</xsl:for-each>
		<xsl:for-each select="$parameter-set[string-length(@fr:value) > 0]">
			<xsl:variable name="name" select="@fr:name"/>
			<xsl:if test="not($override-set[@name = $name])">
				<input type="hidden" name="{@fr:name}" value="{@fr:value}"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="report-parameters-query">
		<xsl:param name="overrides"/>
		<xsl:variable name="override-set" select="ext:node-set($overrides)/*"/>
		<xsl:variable name="parameter-set" select="/fr:report/fr:parameters/fr:parameter[not(starts-with(@fr:name, '~'))]"/>
		<xsl:call-template name="report-query-string">
			<xsl:with-param name="parameters">
				<xsl:for-each select="$override-set[string-length(@value) > 0]">
					<parameter name="{@name}" value="{@value}"/>
				</xsl:for-each>
				<xsl:for-each select="$parameter-set[string-length(@fr:value) > 0]">
					<xsl:variable name="name" select="@fr:name"/>
					<xsl:if test="not($override-set[@fr:name = $name])">
						<parameter name="{@fr:name}" value="{@fr:value}"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="report-query-string">
		<xsl:param name="parameters"/>
		<xsl:for-each select="ext:node-set($parameters)/*"><xsl:if test="position() > 1">&amp;</xsl:if><xsl:value-of select="@name"/>=<xsl:value-of select="@value"/></xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
