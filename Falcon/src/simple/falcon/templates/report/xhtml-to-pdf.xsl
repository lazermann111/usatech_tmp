<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:str="http://exslt.org/strings"
	xmlns:util="http://simple/xml/extensions/java/simple.xml.XSLUtils"
	xmlns:pdf="http://simple/falcon/report/pdf" 
	xmlns:css="http://www.saf.org/simple/css" 
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="pdf str util">
	<xsl:output method="xml" omit-xml-declaration="yes"/>
	
	<xsl:variable name="doc-base" select="//pdf:to-pdf/@pdf:base-url"/>
	
	<xsl:template match="*">
		<xsl:copy>
			<!-- Add attributes -->
			<xsl:for-each select="@*">
				<xsl:copy/>
			</xsl:for-each>
			<!-- Recurse nodes -->
			<xsl:apply-templates select="*|text()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="pdf:to-pdf">
		<xsl:apply-templates mode="to-pdf" select="*|text()"/>
	</xsl:template>
	
	<xsl:template match="text()" mode="to-pdf">
		<xsl:value-of select="translate(., '&#160;', ' ')"/>
	</xsl:template>
	
	<xsl:template match="@xhtml:style|@style" mode="to-pdf">
		<xsl:call-template name="parse-style">
			<xsl:with-param name="style" select="."/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="parse-style">
		<xsl:param name="style" select="."/>
		<xsl:for-each select="str:split($style, ';')">
		 	<xsl:variable name="name" select="normalize-space(substring-before(., ':'))"/>
			<xsl:if test="string-length($name) &gt; 0">
				<xsl:attribute name="{$name}">
					<xsl:value-of select="normalize-space(substring-after(., ':'))"/>
				</xsl:attribute>
			</xsl:if>
		 </xsl:for-each>
	</xsl:template>

	<xsl:template match="@xhtml:class|@class" mode="to-pdf">
		<xsl:attribute name="css:class"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>
	
	<xsl:template match="@xhtml:id|@id" mode="to-pdf">
		<xsl:attribute name="css:id"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="xhtml:div|xhtml:p" mode="to-pdf">
		<block><xsl:apply-templates mode="to-pdf" select="@*|*|text()"/></block>
	</xsl:template>
	
	<xsl:template match="xhtml:span" mode="to-pdf">
		<inline><xsl:apply-templates mode="to-pdf" select="@*|*|text()"/></inline>
	</xsl:template>
	
	<xsl:template match="xhtml:table" mode="to-pdf">
		<table>
			<xsl:apply-templates mode="to-pdf" select="@*"/>
			<xsl:variable name="first-row" select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)[1]"/>
			<xsl:variable name="column-count">
				<xsl:call-template name="column-count">
					<xsl:with-param name="row" select="$first-row"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:call-template name="table-columns">
				<xsl:with-param name="column-count" select="$column-count"/>
				<xsl:with-param name="index" select="'1'"/>
				<xsl:with-param name="col-nodes" select="xhtml:col"/>
			</xsl:call-template>
			<xsl:apply-templates mode="to-pdf" select="*[local-name() != 'col']|text()"/>
		</table>
	</xsl:template>
	
	<xsl:template name="column-count">
		<xsl:param name="row" select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)[1]"/>
		<xsl:param name="index" select="'1'"/>
		<xsl:variable name="colspan">
			<xsl:choose>
				<xsl:when test="$row/*[$index]/@colspan &gt; 1"><xsl:value-of select="$row/*[$index]/@colspan"/></xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="following-count">
			<xsl:choose>
				<xsl:when test="$row/*[$index+1]">
					<xsl:call-template name="column-count">
						<xsl:with-param name="row" select="$row"/>
						<xsl:with-param name="index" select="$index + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$colspan + $following-count"/>
	</xsl:template>
	
	<xsl:template name="table-columns">
		<xsl:param name="column-count" select="1"/>
		<xsl:param name="index" select="1"/>
		<xsl:param name="col-nodes" select="xhtml:col"/>
		<xsl:variable name="col-node" select="$col-nodes[position() = $index]"/>
		<!--  TODO: really need to use 1 + sum(preceding-sibling::*/@colspan + 1)  or something like it -->
		<xsl:variable name="col-width" select="normalize-space($col-node/@width)"/>		
		<xsl:variable name="width">
			<xsl:choose>
				<xsl:when test="substring($col-width,string-length($col-width)) = '*'">
					proportional-column-width(<xsl:value-of select="substring($col-width,1,string-length($col-width)-1)"/>)
				</xsl:when>
				<xsl:when test="string-length(translate($col-width,'0123456789.-+','')) &gt; 0">
					<xsl:value-of select="$col-width"/>
				</xsl:when>
				<xsl:when test="string-length($col-width) &gt; 0">
					<xsl:value-of select="$col-width"/>px
				</xsl:when>
				<xsl:otherwise>
					proportional-column-width(<xsl:call-template name="getMaxLength">
						<xsl:with-param name="nodes" 
							select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)/*[position() = $index and (not(@colspan) or @colspan &lt; 2)]" />
					</xsl:call-template>)
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<table-column column-width="{normalize-space($width)}" index="{$index}">
			<xsl:if test="$col-node/@align" >
				<xsl:attribute name="text-align"><xsl:value-of select="normalize-space($col-node/@align)"/></xsl:attribute>
			</xsl:if>
		</table-column>		
		<xsl:if test="$column-count &gt; $index">
			<xsl:call-template name="table-columns">
				<xsl:with-param name="column-count" select="$column-count"/>
				<xsl:with-param name="index" select="$index + 1"/>
				<xsl:with-param name="col-nodes" select="$col-nodes"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template match="xhtml:col" mode="to-pdf-tc">
		<xsl:variable name="position" select="count(preceding-sibling::xhtml:col) + 1"/>
		<xsl:variable name="col-width" select="normalize-space(@width)"/>		
		<xsl:variable name="width">
			<xsl:choose>
				<xsl:when test="substring($col-width,string-length($col-width)) = '*'">
					proportional-column-width(<xsl:value-of select="substring($col-width,1,string-length($col-width)-1)"/>)
				</xsl:when>
				<xsl:when test="string-length(translate($col-width,'0123456789.-+','')) &gt; 0">
					<xsl:value-of select="$col-width"/>
				</xsl:when>
				<xsl:when test="string-length($col-width) &gt; 0">
					<xsl:value-of select="$col-width"/>px
				</xsl:when>
				<xsl:otherwise>
					proportional-column-width(<xsl:call-template name="getMaxLength">
						<xsl:with-param name="nodes" 
							select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)/*[position() = $index and (not(@colspan) or @colspan &lt; 2)]" />
					</xsl:call-template>)
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<table-column text-align="{normalize-space(@align)}" column-width="{normalize-space($width)}"/>
	</xsl:template>
					
	<xsl:template match="xhtml:tbody" mode="to-pdf">
		<table-body><xsl:apply-templates mode="to-pdf-tr-okay" select="@*|*|text()"/></table-body>
	</xsl:template>

	<xsl:template match="xhtml:thead" mode="to-pdf">
		<table-header><xsl:apply-templates mode="to-pdf-tr-okay" select="@*|*|text()"/></table-header>
	</xsl:template>

	<xsl:template match="xhtml:tfoot" mode="to-pdf">
		<table-footer><xsl:apply-templates mode="to-pdf-tr-okay" select="@*|*|text()"/></table-footer>
	</xsl:template>
	
	<xsl:template match="xhtml:tr[(preceding-sibling::*[1])[local-name()!='tr']]" mode="to-pdf">
		<table-body>
			<xsl:apply-templates mode="to-pdf-tr-next" select="."/>			
		</table-body>
	</xsl:template>
	
	<xsl:template match="xhtml:tr[1]" mode="to-pdf">
		<table-body>
			<xsl:apply-templates mode="to-pdf-tr-next" select="."/>			
		</table-body>
	</xsl:template>

	<xsl:template match="xhtml:tr" mode="to-pdf-tr-next">
		<xsl:apply-templates mode="to-pdf-tr-okay" select="."/>
		<xsl:apply-templates mode="to-pdf-tr-next" select="following-sibling::*[1]"/>			
	</xsl:template>
	
	<xsl:template match="xhtml:tr" mode="to-pdf-tr-okay">
		<table-row><xsl:apply-templates mode="to-pdf" select="@*|*|text()"/></table-row>
	</xsl:template>
	
	<xsl:template match="xhtml:td" mode="to-pdf">
		<table-cell>
			<xsl:apply-templates mode="to-pdf" select="@*[name() != 'colspan' and name() != 'rowspan']"/>
			<xsl:if test="string-length(normalize-space(@colspan)) &gt; 0">
				<xsl:attribute name="number-columns-spanned">
					<xsl:value-of select="normalize-space(@colspan)"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="string-length(normalize-space(@rowspan)) &gt; 0">
				<xsl:attribute name="number-rows-spanned">
					<xsl:value-of select="normalize-space(@rowspan)"/>
				</xsl:attribute>
			</xsl:if>
			<block>
				<xsl:apply-templates mode="to-pdf" select="*|text()"/>
			</block>
		</table-cell>
	</xsl:template>
	
	<xsl:template match="xhtml:th" mode="to-pdf">
		<table-cell font-weight="bold" text-align="center">
			<xsl:apply-templates mode="to-pdf" select="@*[name() != 'colspan' and name() != 'rowspan']"/>
			<xsl:if test="string-length(normalize-space(@colspan)) &gt; 0">
				<xsl:attribute name="number-columns-spanned">
					<xsl:value-of select="normalize-space(@colspan)"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="string-length(normalize-space(@rowspan)) &gt; 0">
				<xsl:attribute name="number-rows-spanned">
					<xsl:value-of select="normalize-space(@rowspan)"/>
				</xsl:attribute>
			</xsl:if>
			<block>
				<xsl:apply-templates mode="to-pdf" select="*|text()"/>
			</block>
		</table-cell>
	</xsl:template>	
		
	<xsl:template match="xhtml:img" mode="to-pdf">
		<external-graphic>	
			<xsl:apply-templates mode="to-pdf" select="@*|*|text()"/>
		</external-graphic>							
	</xsl:template>
	
	<xsl:template match="@xhtml:width[name(..)='xhtml:img' or name(..)='xhtml:iframe']|@width[name(..)='xhtml:img' or name(..)='xhtml:iframe']" mode="to-pdf">
		<xsl:attribute name="width"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
		<xsl:attribute name="content-width"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
	</xsl:template>

	<xsl:template match="@xhtml:height[name(..)='xhtml:img' or name(..)='xhtml:iframe']|@height[name(..)='xhtml:img' or name(..)='xhtml:iframe']" mode="to-pdf">
		<xsl:attribute name="height"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
		<xsl:attribute name="content-height"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
	</xsl:template>

	<xsl:template match="@xhtml:src|@src" mode="to-pdf">
		<xsl:attribute name="src">
			<xsl:if test="not(contains(substring-before(., '/'), ':'))">
				<xsl:value-of select="$doc-base"/>
			</xsl:if>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="xhtml:iframe" mode="to-pdf">
		<xsl:variable name="name" select="normalize-space(@name)"/>
		<xsl:choose>
			<xsl:when test="../xhtml:form[normalize-space(@action) = 'b64' and normalize-space(@target) = $name]">
				<!--  this is handled later, ignore it -->
			</xsl:when>
			<xsl:otherwise>
				<!-- Don't know what to do here -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="xhtml:form[@action = 'b64']" mode="to-pdf">
		<xsl:variable name="target" select="normalize-space(@target)"/>	
		<xsl:variable name="iframe" select="../xhtml:iframe[normalize-space(@name) = $target]"/>
		<external-graphic 
				src="data:{xhtml:input[normalize-space(@name)='content-type']/@value};base64,{xhtml:input[normalize-space(@name)='data']/@value}">
			<xsl:apply-templates mode="to-pdf" select="$iframe/@*[name() != 'src']"/>
		</external-graphic>								
	</xsl:template>
	
	<xsl:template match="xhtml:sup" mode="to-pdf">
		<inline alignment-baseline="middle" font-size="smaller"><xsl:apply-templates mode="to-pdf" select="@*|*|text()"/></inline>
	</xsl:template>	

	<xsl:template match="xhtml:a" mode="to-pdf">
		<xsl:choose>
			<xsl:when test="string-length(normalize-space(@href)) &gt; 0">
				<basic-link>
					<xsl:choose>
						<xsl:when test="starts-with(normalize-space(@href), '#')">
							<xsl:attribute name="internal-destination">
								<xsl:value-of select="substring(normalize-space(@href), 2)"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>				
							<xsl:attribute name="external-destination">
								<xsl:if test="not(contains(substring-before(@href, '/'), ':'))">
									<xsl:value-of select="$doc-base"/>
								</xsl:if>
								<xsl:value-of select="normalize-space(@href)"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:apply-templates mode="to-pdf" select="@*|*|text()"/>
				</basic-link>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates mode="to-pdf" select="@*|*|text()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
			
	<xsl:template match="@*" mode="to-pdf" priority="-1">
		<xsl:attribute name="{name()}"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="*" mode="to-pdf" priority="-2">
		<xsl:comment>start UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
		<xsl:element name="{name()}">
			<xsl:apply-templates mode="to-pdf" select="@*|*|text()"/>	
		</xsl:element>
		<xsl:comment>end UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
	</xsl:template>

	<xsl:template match="@*" mode="to-pdf-tr-okay" priority="-1">
		<xsl:attribute name="{name()}"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="*" mode="to-pdf-tr-okay" priority="-2">
		<xsl:comment>start UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
		<xsl:element name="{name()}">
			<xsl:apply-templates mode="to-pdf-tr-okay" select="@*|*|text()"/>	
		</xsl:element>
		<xsl:comment>end UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
	</xsl:template>
	
		<xsl:template match="@*" mode="to-pdf-tr-next" priority="-1">
		<xsl:attribute name="{name()}"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="*" mode="to-pdf-tr-next" priority="-2">
		<xsl:comment>start UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
		<xsl:element name="{name()}">
			<xsl:apply-templates mode="to-pdf-tr-next" select="@*|*|text()"/>	
		</xsl:element>
		<xsl:comment>end UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
	</xsl:template>
					
	<xsl:template name="getMaxLength-new">
		<xsl:param name="nodes" />
		<xsl:variable name="max" select="util:max($nodes, 'string-length(normalize-space(.))')"/>
		<xsl:value-of select="string-length(normalize-space($max))" />
	</xsl:template>
	
	<xsl:template name="getMaxLength">
		<xsl:param name="nodes" />
		<xsl:for-each select="$nodes">
			<xsl:sort select="string-length(normalize-space(.))" data-type="number" order="descending"/>
			<xsl:if test="position() = 1">
				<xsl:value-of select="string-length(normalize-space(.))" />
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="count($nodes) = 0">1</xsl:if>
	</xsl:template>				
</xsl:stylesheet>
