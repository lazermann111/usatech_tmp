<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:cmn="http://exslt.org/common"
	xmlns:str="http://exslt.org/strings"
	xmlns:util="http://simple/xml/extensions/java/simple.xml.XSLUtils"
	xmlns:fr="http://simple/falcon/report/1.0" 
	xmlns:css="http://www.saf.org/simple/css" 
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr cmn str util">
	<xsl:import href="show-report-base.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="yes"/>
	
	<xsl:variable name="doc-base" select="/fr:report/fr:scene/@fr:base-url"/>
	
	<xsl:template match="/fr:report">
		<root language="en" hyphenate="true" country="US">
			<layout-master-set>
		   		<xsl:variable name="page-width-directive" select="fr:folio[1]/fr:directives/fr:directive[@fr:name = 'page-width']/@fr:value" />
				<xsl:variable name="page-height-directive" select="fr:folio[1]/fr:directives/fr:directive[@fr:name = 'page-height']/@fr:value" />
				<xsl:variable name="page-width">
		   			<xsl:choose>
		   				<xsl:when test="number($page-width-directive) &gt; 0">
		   					<xsl:value-of select="number($page-width-directive)"/>
		   				</xsl:when>
		   				<xsl:when test="count(fr:folio[last()]/fr:columns/fr:column[(@fr:column-type = 'text' or @fr:column-type = 'image') and @fr:level &lt; 1]) &lt; 7">8.5</xsl:when>
		   				<xsl:otherwise>11</xsl:otherwise>
		   			</xsl:choose>
		   		</xsl:variable> 
				<xsl:variable name="page-height">
		   			<xsl:choose>
		   				<xsl:when test="number($page-height-directive) &gt; 0">
		   					<xsl:value-of select="number($page-height-directive)"/>
		   				</xsl:when>
		   				<xsl:when test="number($page-width) &lt; 11">11</xsl:when>
		   				<xsl:otherwise>8.5</xsl:otherwise>
		   			</xsl:choose>
		   		</xsl:variable> 
			   <simple-page-master master-name="master-pages-for-all" 
			   		page-height="{$page-height}in" page-width="{$page-width}in"
			   		margin-top="0.4in" margin-right="0.4in" margin-bottom="0.4in" margin-left="0.4in">
			   		<!-- FOP 0.20.5 does not seem to obey margins here -->
					<region-before extent="0in"/>
			       	<region-body />
					<region-after extent="0in"/>
			   </simple-page-master>

			   <page-sequence-master master-name="sequence-of-pages">
				    <repeatable-page-master-reference master-reference="master-pages-for-all" />
			   </page-sequence-master>
		  	</layout-master-set>

 		  	<page-sequence master-reference="sequence-of-pages">
 		  		<title><xsl:call-template name="title"/></title>
			    <static-content flow-name="xsl-region-before"/>
				<static-content flow-name="xsl-region-after"/>
			    <!--  <page-sequence master-reference="document" initial-page-number="1" format="1">-->
				<flow flow-name="xsl-region-body">
					<block unicode-bidi="embed">
						<xsl:call-template name="translate">
							<xsl:with-param name="content"><xsl:call-template name="report"/></xsl:with-param>
						</xsl:call-template>	
					</block>
				</flow>
			</page-sequence>
		</root>
	</xsl:template>
	
	<xsl:template name="translate">
		<xsl:param name="content"/>
		<xsl:apply-templates mode="translate" select="cmn:node-set($content)"/>
	</xsl:template>
	
	<xsl:template match="text()" mode="translate">
		<xsl:value-of select="translate(., '&#160;', ' ')"/>
	</xsl:template>
	
	<xsl:template match="@xhtml:style|@style" mode="translate">
		<xsl:call-template name="parse-style">
			<xsl:with-param name="style" select="."/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="parse-style">
		<xsl:param name="style" select="."/>
		<!-- 
		<xsl:variable name="name" select="normalize-space(substring-before($style, ':'))"/>
		<xsl:if test="string-length($name) &gt; 0">
			<xsl:variable name="tmp" select="substring-after($style, ':')"/>
			<xsl:variable name="value">
				<xsl:choose>
					<xsl:when test="contains($tmp, ';')"><xsl:value-of select="substring-before($tmp, ';')"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$tmp"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="{$name}"><xsl:value-of select="normalize-space($value)"/></xsl:attribute>
			<xsl:variable name="more-style" select="normalize-space(substring-after($tmp, ';'))"/>
			<xsl:if test="string-length($more-style) &gt; 0">
				<xsl:call-template name="parse-style">
					<xsl:with-param name="style" select="$more-style"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		 -->
		 <xsl:for-each select="str:split($style, ';')">
		 	<xsl:variable name="name" select="normalize-space(substring-before(., ':'))"/>
			<xsl:if test="string-length($name) &gt; 0">
				<xsl:attribute name="{$name}">
					<xsl:value-of select="normalize-space(substring-after(., ':'))"/>
				</xsl:attribute>
			</xsl:if>
		 </xsl:for-each>
	</xsl:template>

	<xsl:template match="@xhtml:class|@class" mode="translate">
		<xsl:attribute name="css:class"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>
	
	<xsl:template match="@xhtml:id|@id" mode="translate">
		<xsl:attribute name="css:id"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="xhtml:div|xhtml:p" mode="translate">
		<block><xsl:apply-templates mode="translate" select="*|@*|text()"/></block>
	</xsl:template>
	
	<xsl:template match="xhtml:span" mode="translate">
		<inline><xsl:apply-templates mode="translate" select="*|@*|text()"/></inline>
	</xsl:template>
	
	<xsl:template match="xhtml:table" mode="translate">
		<table>
			<xsl:apply-templates mode="translate" select="@*"/>
			<xsl:variable name="first-row" select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)[1]"/>
			<xsl:variable name="column-count">
				<xsl:call-template name="column-count">
					<xsl:with-param name="row" select="$first-row"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:call-template name="table-columns">
				<xsl:with-param name="column-count" select="$column-count"/>
				<xsl:with-param name="index" select="'1'"/>
				<xsl:with-param name="col-nodes" select="xhtml:col"/>
			</xsl:call-template>
			<xsl:apply-templates mode="translate" select="*[local-name() != 'col']|text()"/>
		</table>
	</xsl:template>
	
	<xsl:template name="column-count">
		<xsl:param name="row" select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)[1]"/>
		<xsl:param name="index" select="'1'"/>
		<xsl:variable name="colspan">
			<xsl:choose>
				<xsl:when test="$row/*[$index]/@colspan &gt; 1"><xsl:value-of select="$row/*[$index]/@colspan"/></xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="following-count">
			<xsl:choose>
				<xsl:when test="$row/*[$index+1]">
					<xsl:call-template name="column-count">
						<xsl:with-param name="row" select="$row"/>
						<xsl:with-param name="index" select="$index + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$colspan + $following-count"/>
	</xsl:template>
	
	<xsl:template name="table-columns">
		<xsl:param name="column-count" select="'1'"/>
		<xsl:param name="index" select="'1'"/>
		<xsl:param name="col-nodes" select="xhtml:col"/>
		<!--  TODO: really need to use 1 + sum(preceding-sibling::*/@colspan + 1)  or something like it -->
		<xsl:variable name="prop-width">
			<xsl:call-template name="getMaxLength">
				<xsl:with-param name="nodes" 
					select="(xhtml:tr|xhtml:tbody/xhtml:tr|xhtml:thead/xhtml:tr|xhtml:tfoot/xhtml:tr)/*[position() = $index and (not(@colspan) or @colspan &lt; 2)]" />
			</xsl:call-template>
		</xsl:variable>
		<table-column column-width="proportional-column-width({normalize-space($prop-width)})">
			<xsl:if test="$col-nodes[$index]/@align" >
				<xsl:attribute name="text-align"><xsl:value-of select="normalize-space($col-nodes[$index]/@align)"/></xsl:attribute>
			</xsl:if>
		</table-column>		
		<xsl:if test="$column-count &gt; $index">
			<xsl:call-template name="table-columns">
				<xsl:with-param name="column-count" select="$column-count"/>
				<xsl:with-param name="index" select="$index + 1"/>
				<xsl:with-param name="col-nodes" select="$col-nodes"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template match="xhtml:col" mode="translate-tc">
		<xsl:variable name="position" select="count(preceding-sibling::xhtml:col) + 1"/>
		<xsl:variable name="prop-width">
			<xsl:call-template name="getMaxLength">
				<xsl:with-param name="nodes" select="../xhtml:tbody/xhtml:tr/*[position() = $position and (not(@colspan) or @colspan &lt; 2)]" />
			</xsl:call-template>
		</xsl:variable>
		<table-column text-align="{normalize-space(@align)}" column-width="proportional-column-width({normalize-space($prop-width)})"/>
	</xsl:template>
					
	<xsl:template match="xhtml:tbody" mode="translate">
		<table-body><xsl:apply-templates mode="translate-tr-okay" select="*|@*|text()"/></table-body>
	</xsl:template>

	<xsl:template match="xhtml:thead" mode="translate">
		<table-header><xsl:apply-templates mode="translate-tr-okay" select="*|@*|text()"/></table-header>
	</xsl:template>

	<xsl:template match="xhtml:tfoot" mode="translate">
		<table-footer><xsl:apply-templates mode="translate-tr-okay" select="*|@*|text()"/></table-footer>
	</xsl:template>
	
	<xsl:template match="xhtml:tr[(preceding-sibling::*[1])[local-name()!='tr']]" mode="translate">
		<table-body>
			<xsl:apply-templates mode="translate-tr-next" select="."/>			
		</table-body>
	</xsl:template>
	
	<xsl:template match="xhtml:tr[1]" mode="translate">
		<table-body>
			<xsl:apply-templates mode="translate-tr-next" select="."/>			
		</table-body>
	</xsl:template>

	<xsl:template match="xhtml:tr" mode="translate-tr-next">
		<xsl:apply-templates mode="translate-tr-okay" select="."/>
		<xsl:apply-templates mode="translate-tr-next" select="following-sibling::*[1]"/>			
	</xsl:template>
	
	<xsl:template match="xhtml:tr" mode="translate-tr-okay">
		<table-row><xsl:apply-templates mode="translate" select="*|@*|text()"/></table-row>
	</xsl:template>
	
	<xsl:template match="xhtml:td" mode="translate">
		<table-cell>
			<xsl:if test="string-length(normalize-space(@colspan)) &gt; 0">
				<xsl:attribute name="number-columns-spanned">
					<xsl:value-of select="normalize-space(@colspan)"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="string-length(normalize-space(@rowspan)) &gt; 0">
				<xsl:attribute name="number-rows-spanned">
					<xsl:value-of select="normalize-space(@rowspan)"/>
				</xsl:attribute>
			</xsl:if>
			<block>
				<xsl:apply-templates mode="translate" select="*|@*[name() != 'colspan' and name() != 'rowspan']|text()"/>
			</block>
		</table-cell>
	</xsl:template>
	
	<xsl:template match="xhtml:th" mode="translate">
		<table-cell font-weight="bold" text-align="center">
			<xsl:if test="string-length(normalize-space(@colspan)) &gt; 0">
				<xsl:attribute name="number-columns-spanned">
					<xsl:value-of select="normalize-space(@colspan)"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="string-length(normalize-space(@rowspan)) &gt; 0">
				<xsl:attribute name="number-rows-spanned">
					<xsl:value-of select="normalize-space(@rowspan)"/>
				</xsl:attribute>
			</xsl:if>
			<block>
				<xsl:apply-templates mode="translate" select="*|@*[name() != 'colspan' and name() != 'rowspan']|text()"/>
			</block>
		</table-cell>
	</xsl:template>	
		
	<xsl:template match="xhtml:img" mode="translate">
		<external-graphic>	
			<xsl:apply-templates mode="translate" select="*|@*|text()"/>
		</external-graphic>							
	</xsl:template>
	
	<xsl:template match="@xhtml:width[name(..)='xhtml:img' or name(..)='xhtml:iframe']|@width[name(..)='xhtml:img' or name(..)='xhtml:iframe']" mode="translate">
		<xsl:attribute name="width"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
		<xsl:attribute name="content-width"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
	</xsl:template>

	<xsl:template match="@xhtml:height[name(..)='xhtml:img' or name(..)='xhtml:iframe']|@height[name(..)='xhtml:img' or name(..)='xhtml:iframe']" mode="translate">
		<xsl:attribute name="height"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
		<xsl:attribute name="content-height"><xsl:value-of select="normalize-space(.)"/>px</xsl:attribute>
	</xsl:template>

	<xsl:template match="@xhtml:src|@src" mode="translate">
		<xsl:attribute name="src">
			<xsl:if test="not(contains(substring-before(., '/'), ':'))">
				<xsl:value-of select="$doc-base"/>
			</xsl:if>
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="xhtml:iframe" mode="translate">
		<xsl:variable name="name" select="normalize-space(@name)"/>
		<xsl:choose>
			<xsl:when test="../xhtml:form[normalize-space(@action) = 'b64' and normalize-space(@target) = $name]">
				<!--  this is handled later, ignore it -->
			</xsl:when>
			<xsl:otherwise>
				<!-- Don't know what to do here -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="xhtml:form[@action = 'b64']" mode="translate">
		<xsl:variable name="target" select="normalize-space(@target)"/>	
		<xsl:variable name="iframe" select="../xhtml:iframe[normalize-space(@name) = $target]"/>
		<external-graphic 
				src="data:{xhtml:input[normalize-space(@name)='content-type']/@value};base64,{xhtml:input[normalize-space(@name)='data']/@value}">
			<xsl:apply-templates mode="translate" select="$iframe/@*[name() != 'src']"/>
		</external-graphic>								
	</xsl:template>
	
	<xsl:template match="xhtml:sup" mode="translate">
		<inline alignment-baseline="middle" font-size="smaller"><xsl:apply-templates mode="translate" select="*|@*|text()"/></inline>
	</xsl:template>	

	<xsl:template match="xhtml:a" mode="translate">
		<xsl:choose>
			<xsl:when test="string-length(normalize-space(@href)) &gt; 0">
				<basic-link>
					<xsl:choose>
						<xsl:when test="starts-with(normalize-space(@href), '#')">
							<xsl:attribute name="internal-destination">
								<xsl:value-of select="substring(normalize-space(@href), 2)"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>				
							<xsl:attribute name="external-destination">
								<xsl:if test="not(contains(substring-before(@href, '/'), ':'))">
									<xsl:value-of select="$doc-base"/>
								</xsl:if>
								<xsl:value-of select="normalize-space(@href)"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:apply-templates mode="translate" select="*|@*|text()"/>
				</basic-link>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates mode="translate" select="*|@*|text()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
			
	<xsl:template match="@*" mode="translate" priority="-1">
		<xsl:attribute name="{name()}"><xsl:value-of select="normalize-space(.)"/></xsl:attribute>
	</xsl:template>

	<xsl:template match="*" mode="translate" priority="-2">
		<xsl:comment>start UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
		<xsl:element name="{name()}">
			<xsl:apply-templates mode="translate" select="*|@*|text()"/>	
		</xsl:element>
		<xsl:comment>end UNHANDLED ELEMENT - <xsl:value-of select="name()"/></xsl:comment>
	</xsl:template>
				
	<xsl:template name="getMaxLength-new">
		<xsl:param name="nodes" />
		<xsl:variable name="max" select="util:max($nodes, 'string-length(normalize-space(.))')"/>
		<xsl:value-of select="string-length(normalize-space($max))" />
	</xsl:template>
	
	<xsl:template name="getMaxLength">
		<xsl:param name="nodes" />
		<xsl:for-each select="$nodes">
			<xsl:sort select="string-length(normalize-space(.))" data-type="number" order="descending"/>
			<xsl:if test="position() = 1">
				<xsl:value-of select="string-length(normalize-space(.))" />
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="count($nodes) = 0">1</xsl:if>
	</xsl:template>				
</xsl:stylesheet>
