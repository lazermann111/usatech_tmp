<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:css="http://www.saf.org/simple/css"
	xmlns:pdf="http://simple/falcon/report/pdf"
	xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestInfo"
    exclude-result-prefixes="fr req">
	<xsl:import href="show-report-base.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="yes"/>
    <xsl:param name="context" />
    
	<xsl:template name="column-count">
	    <xsl:param name="folio" select="fr:folio[1]"/>
	    <xsl:param name="max-column-count" select="0"/>
        
        <xsl:variable name="allcolumns" select="$folio/fr:columns/fr:column" />
        <xsl:variable name="tabcolumns" select="$allcolumns[@fr:level &lt; 3 and @fr:column-type != 'note' and @fr:column-type != 'all']"/>
        <xsl:variable name="xtabs" select="$folio/fr:xtab-values/fr:xtab-value"/>
        <xsl:variable name="table-column-count" select="count($tabcolumns[@fr:level != 1]) + (count($xtabs) * count($tabcolumns[@fr:level = 0]))"/>
        <xsl:variable name="next-folio" select="($folio/following-sibling::fr:folio)[1]"/>
        <xsl:variable name="new-column-count">
	        <xsl:choose>
	            <xsl:when test="$table-column-count &gt; $max-column-count"><xsl:copy-of select="$table-column-count"/></xsl:when>
	            <xsl:otherwise><xsl:copy-of select="$max-column-count"/></xsl:otherwise>
	        </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$next-folio">
                <xsl:call-template name="column-count">
                    <xsl:with-param name="folio" select="$next-folio"/>
                    <xsl:with-param name="max-column-count" select="$new-column-count"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise><xsl:copy-of select="$new-column-count"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
	<xsl:template match="/">
		<xsl:for-each select="fr:report">
			<root language="en" hyphenate="true" country="US">
				<layout-master-set>
			   		<xsl:variable name="page-width-directive" select="fr:folio[1]/fr:directives/fr:directive[@fr:name = 'page-width']/@fr:value" />
					<xsl:variable name="page-height-directive" select="fr:folio[1]/fr:directives/fr:directive[@fr:name = 'page-height']/@fr:value" />
					<xsl:variable name="column-count">
					   <xsl:call-template name="column-count"/>
					</xsl:variable>
					<xsl:variable name="page-width">
			   			<xsl:choose>
			   				<xsl:when test="number($page-width-directive) &gt; 0">
			   					<xsl:value-of select="number($page-width-directive)"/>
			   				</xsl:when>
			   				<xsl:when test="$column-count &lt; 5">8.5</xsl:when>
			   				<xsl:when test="$column-count &lt; 7">11</xsl:when>
                            <xsl:otherwise>17</xsl:otherwise>
			   			</xsl:choose>
			   		</xsl:variable>
					<xsl:variable name="page-height">
			   			<xsl:choose>
			   				<xsl:when test="number($page-height-directive) &gt; 0">
			   					<xsl:value-of select="number($page-height-directive)"/>
			   				</xsl:when>
			   				<xsl:when test="number($page-width) &gt; 11">11</xsl:when>
			   				<xsl:when test="number($page-width) &lt; 11">11</xsl:when>
                            <xsl:otherwise>8.5</xsl:otherwise>
			   			</xsl:choose>
			   		</xsl:variable>
				   <simple-page-master master-name="master-pages-for-all"
				   		page-height="{$page-height}in" page-width="{$page-width}in"
				   		   margin-top="0.4in" margin-right="0.4in" margin-bottom="0.4in" margin-left="0.4in">
				   		<!-- FOP 0.20.5 does not seem to obey margins here -->
						<region-before extent="0in"/>
				       	<region-body />
						<region-after extent="0in"/>
				   </simple-page-master>

				   <page-sequence-master master-name="sequence-of-pages">
					    <repeatable-page-master-reference master-reference="master-pages-for-all" />
				   </page-sequence-master>
			  	</layout-master-set>

	 		  	<page-sequence master-reference="sequence-of-pages">
	 		  		<title><xsl:call-template name="title"/></title>
				    <static-content flow-name="xsl-region-before"/>
					<static-content flow-name="xsl-region-after"/>
				    <!--  <page-sequence master-reference="document" initial-page-number="1" format="1">-->
					<flow flow-name="xsl-region-body">
						<block unicode-bidi="embed">
							<pdf:to-pdf pdf:base-url="{string(req:getBaseUrl($context))}">
								<xsl:call-template name="report"/>
							</pdf:to-pdf>
						</block>
					</flow>
				</page-sequence>
			</root>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
