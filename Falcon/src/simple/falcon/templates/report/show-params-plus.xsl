<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:conv="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
	xmlns:math="http://simple/xml/extensions/java/java.lang.Math"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a b p x2 math conv">
	<xsl:import href="../general/html-base-customize.xsl"/>
	<xsl:output method="xhtml"/>
	<xsl:param name="context" />
    <xsl:key name="param-names" match="/a:base/a:param-prompts/b:params/b:param" use="@b:name"/>

	<xsl:variable name="parameter-exception-type" select="reflect:getProperty($context, 'simple.db.ParameterException.type')"/>
	<xsl:variable name="parameter-exception-name" select="reflect:getProperty($context, 'simple.db.ParameterException.parameterName')"/>
	<xsl:variable name="fix-param" select="/a:base/a:param-prompts/b:params/b:param[@b:name=$parameter-exception-name]"/>
	
	<!-- TODO: allow other parameters to pass thru -->
	<xsl:template name="contents">
		<form name="promptForm" class="parameters" action="run_report_async.i">
		<input type="hidden" name="unframed" value="{reflect:getProperty($context, 'unframed')}" />
  		<xsl:call-template name="parameters-form">
			<xsl:with-param name="overrides">
				<p:override name="promptForParams" value="fix"/>
				<xsl:for-each select="/a:base/a:param-prompts/b:params/b:param[string-length(normalize-space(@b:name)) > 0]">
		  			<p:ignore name="params.{@b:name}"/>
				</xsl:for-each>
				<xsl:choose>
		  			<xsl:when test="reflect:getProperty($context, 'reportId')">
		  				<p:override name="reportId" value="{reflect:getProperty($context, 'reportId')}"/>
		  				<p:ignore name="folioIds"/>
		  				<p:ignore name="folioId"/>
		  			</xsl:when>
		  			<xsl:when test="reflect:getProperty($context, 'folioIds')">
		  				<p:override name="folioIds" value="{reflect:getProperty($context, 'folioIds')}"/>
		  				<p:ignore name="reportId"/>
		  				<p:ignore name="folioId"/>
		  			</xsl:when>
		  			<xsl:otherwise>
		  				<p:override name="folioId" value="{reflect:getProperty($context, 'folioId')}"/>
		  				<p:ignore name="reportId"/>
		  				<p:ignore name="folioIds"/>
		  			</xsl:otherwise>
		  		</xsl:choose>		
			</xsl:with-param>
			<xsl:with-param name="parameters" select="*/a:parameters/p:parameters/p:parameter"/>
		</xsl:call-template>
  		<xsl:choose>
  			<xsl:when test="$fix-param">
	  			<h4 class="error"><x2:translate key="report-params-prompt-fix">Please fix the following and re-submit:</x2:translate></h4>
  				<ul class="error">
  					<li>'<xsl:value-of select="$fix-param/@b:label"/>'
  						<xsl:choose>
  							<xsl:when test="$parameter-exception-type='CONVERSION'"> could not be converted
  								<xsl:choose>
  									<xsl:when test="$fix-param/b:editor/b:editorType='DATE'">to a date. Please use a standard date format.</xsl:when>
  									<xsl:when test="$fix-param/b:editor/b:editorType='NUMBER'">to a number. Please use a standard number format.</xsl:when>
  									<xsl:otherwise>.</xsl:otherwise>
								</xsl:choose>
  							</xsl:when>
  							<xsl:when test="$parameter-exception-type='MISSING'"> was not provided. Please enter a non-blank value.</xsl:when>
  							<xsl:otherwise>caused an error. Please contact USA Technologies.</xsl:otherwise>
  						</xsl:choose>
  					</li>
  				</ul>
  				<script defer="defer" type="text/javascript">document.getElementsByName("params.<xsl:value-of select="$fix-param/@b:name"/>")[0].focus();</script>
  			</xsl:when>
  			<xsl:otherwise>
  				<div class="caption"><xsl:value-of select="reflect:getProperty($context, 'reportTitle')" /></div>
	  			<div class="instructions"><x2:translate key="report-params-prompt">Please enter a value for each of the following parameters:</x2:translate></div>
	  		</xsl:otherwise>
	  	</xsl:choose>
  		<table class="params">
  		<xsl:for-each select="/a:base/a:param-prompts/b:params/b:param[string-length(normalize-space(@b:name)) > 0]">
  			<xsl:variable name="name" select="@b:name"/>
  			<xsl:choose>
  				<xsl:when test="preceding-sibling::b:param[@b:name=$name]"></xsl:when>
  				<xsl:otherwise>
		  			<tr>
		  				<th><xsl:value-of select="@b:label"/>:</th>
		  				<td>
		  					<xsl:choose>
				 				<xsl:when test="/a:base/a:parameters/p:parameters/p:parameter[@p:name=concat('params.',$name)]">
								 	<xsl:call-template name="input">
						 				<xsl:with-param name="value" select="/a:base/a:parameters/p:parameters/p:parameter[@p:name=concat('params.',$name)]/@p:value"/>
						 			</xsl:call-template>
						 		</xsl:when>
						 		<xsl:otherwise>
						 			<xsl:call-template name="input">
						 				<xsl:with-param name="value" select="b:value"/>
						 			</xsl:call-template>
						 		</xsl:otherwise>
					 		</xsl:choose>
						 </td>
					 </tr>
				</xsl:otherwise>
			</xsl:choose>
  		</xsl:for-each>
  		</table>
		<input type="submit"><xsl:attribute name="value"><x2:translate key="report-params-submit">Run Report</x2:translate></xsl:attribute></input>
  		</form>
    </xsl:template>

    <xsl:template name="subtitle"><x2:translate key="report-params-title">Enter Parameters for Report</x2:translate></xsl:template>

    <xsl:template name="extra-scripts">
	    <script type="text/javascript">
window.addEvent("domready", function() {
	new Form.Validator.Inline.Mask(document.promptForm); 
});
    	</script>
    </xsl:template>

    <xsl:template name="input">
    	<xsl:param name="value" />
    	<xsl:choose>
			<xsl:when test="b:editor/b:editorType='DATE'">
				<xsl:call-template name="date-input">
					<xsl:with-param name="value" select="$value"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="b:editor/b:editorType='NUMBER'">
				<xsl:call-template name="number-input">
					<xsl:with-param name="value" select="$value"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="b:editor/b:editorType='SELECT'">
				<xsl:call-template name="select-input">
					<xsl:with-param name="value" select="$value"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="b:editor/b:editorType='TEXT'">
				<xsl:call-template name="text-input">
					<xsl:with-param name="value" select="$value"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="text-input">
					<xsl:with-param name="value" select="$value"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>

    <xsl:template name="date-input">
    	<xsl:param name="value" />
    	<xsl:variable name="formatted-value">
    		<xsl:choose>
    			<xsl:when test="normalize-space(b:editor/b:defaultFormat)"><xsl:value-of select="conv:formatObject(string($value), concat('DATE:',normalize-space(b:editor/b:defaultFormat)), true())"/></xsl:when>
    	 		<xsl:otherwise><xsl:value-of select="$value"/></xsl:otherwise>
    	 	</xsl:choose>
    	</xsl:variable>
    	<xsl:variable name="size">
    		<xsl:choose>
    			<xsl:when test="starts-with(@b:sqlType, 'TIMESTAMP')">25</xsl:when><!-- TIMESTAMP -->
    			<xsl:when test="starts-with(@b:sqlType, 'TIME')">14</xsl:when><!-- TIME -->
    			<xsl:when test="starts-with(@b:sqlType, 'DATE')">10</xsl:when><!-- DATE -->
    			<xsl:otherwise>22</xsl:otherwise>
   			</xsl:choose>
    	</xsl:variable>
    	<xsl:variable name="dateFormat">
            <xsl:choose>
                <xsl:when test="starts-with(@b:sqlType, 'TIMESTAMP')">%m/%d/%Y %H:%M</xsl:when><!-- TIMESTAMP -->
                <xsl:when test="starts-with(@b:sqlType, 'TIME')">%H:%M</xsl:when><!-- TIME -->
                <xsl:when test="starts-with(@b:sqlType, 'DATE')">%m/%d/%Y</xsl:when><!-- DATE -->
                <xsl:otherwise>22</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
    	<input type="text" name="params.{@b:name}" value="{$formatted-value}" size="{$size}" title="{@b:prompt}" label="{@b:label}" data-toggle="calendar" data-format="{$dateFormat}">
    		<xsl:if test="b:editor/b:required or b:editor/b:validRegex">
 		    	<xsl:attribute name="data-validators">
 		    		<xsl:if test="b:editor/b:required">required</xsl:if>
 		    		<xsl:if test="b:editor/b:validRegex">validate-regex:'<xsl:value-of select="b:editor/b:validRegex"/>'</xsl:if>		    		
    			</xsl:attribute>
    		</xsl:if>
    	</input>
		<!--<img src="./images/icons/calendar.gif" onclick="openCalendar('params.{@b:name}', '{@b:label}')" alt="[c]" />  -->
    </xsl:template>

    <xsl:template name="select-input">
    	<xsl:param name="value" />
    	<xsl:variable name="multiple" select="b:editor/b:multiple='true'"/>
    	<select name="params.{@b:name}" title="{@b:prompt}" label="{@b:label}" onchange="{b:onchange}">
    		<xsl:choose>
    			<xsl:when test="$multiple">
	    			<xsl:attribute name="multiple">true</xsl:attribute>
	    			<!--  This does not make the scroll-bars go away when unnecessary
	    			<xsl:attribute name="style">overflow: auto;</xsl:attribute>
	    			-->
	    			<xsl:attribute name="size"><xsl:value-of select="math:min(8, count(b:editor/b:options/b:entry))"/></xsl:attribute>
    			</xsl:when>
    			<xsl:otherwise>
    				<xsl:attribute name="size">1</xsl:attribute>
	    			<xsl:if test="count($value/b:value-item) &gt; 1">
	    				<xsl:attribute name="class">too-many-values</xsl:attribute>
	    				<option><xsl:value-of select="count($value/b:value-item)"/> different values</option>
	   				</xsl:if>
	   			</xsl:otherwise>
    		</xsl:choose>
    		<xsl:for-each select="b:editor/b:options/b:entry">
    			<option value="{b:key}">
    				<xsl:choose>
    					<xsl:when test="b:key=$value or (count($value/b:value-item) = 1 and b:key=string($value/b:value-item))">
    						<xsl:attribute name="selected">true</xsl:attribute>
    					</xsl:when>
    					<xsl:when test="not($multiple)"></xsl:when>
    					<xsl:when test="contains(concat(',', $value, ','), concat(',', b:key, ','))">
    						<xsl:attribute name="selected">true</xsl:attribute>
    					</xsl:when>
    					<xsl:otherwise>
    						<xsl:variable name="key" select="b:key"/>
    						<xsl:if test="$value[b:value-item=$key]">
    							<xsl:attribute name="selected">true</xsl:attribute>
    						</xsl:if>
    					</xsl:otherwise>
    				</xsl:choose>
    				<xsl:value-of select="b:value/b:label"/>
    			</option>
    		</xsl:for-each>
    	</select>
    </xsl:template>

    <xsl:template name="number-input">
    	<xsl:param name="value" />
    	<xsl:variable name="maxlength" select="math:max(string-length(b:editor/b:minValue), string-length(b:editor/b:maxValue))"/>
    	<input type="text" name="params.{@b:name}" title="{@b:prompt}" label="{@b:label}">
    		<xsl:choose>
    			<xsl:when test="count($value/b:value-item) &gt; 1">
    				<xsl:attribute name="class">too-many-values</xsl:attribute>
    				<xsl:attribute name="value"><xsl:value-of select="count($value/b:value-item)"/> different values</xsl:attribute>
	   			</xsl:when>
	   			<xsl:otherwise>
	   				<xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
	   			</xsl:otherwise>
	   		</xsl:choose>
    		<xsl:if test="b:editor/b:required or b:editor/b:validRegex">
 		    	<xsl:attribute name="data-validators">
 		    		<xsl:if test="b:editor/b:required">required</xsl:if>
 		    		<xsl:if test="b:editor/b:validRegex">validate-regex:'<xsl:value-of select="b:editor/b:validRegex"/>'</xsl:if>		    		
    			</xsl:attribute>
    		</xsl:if>
    		<xsl:if test="$maxlength">
    			<xsl:attribute name="maxlength"><xsl:value-of select="$maxlength"/></xsl:attribute>
    			<xsl:attribute name="size"><xsl:value-of select="$maxlength"/></xsl:attribute>
    			<xsl:if test="string-length(b:editor/b:minValue)">
    				<xsl:attribute name="minValue"><xsl:value-of select="b:editor/b:minValue"/></xsl:attribute>
    			</xsl:if>
    			<xsl:if test="string-length(b:editor/b:maxValue)">
    				<xsl:attribute name="maxValue"><xsl:value-of select="b:editor/b:maxValue"/></xsl:attribute>
    			</xsl:if>
    		</xsl:if>
    	</input>

    </xsl:template>

    <!-- The default (a text field) -->
    <xsl:template name="text-input">
		<xsl:param name="value" />
		<input type="text" name="params.{@b:name}" title="{@b:prompt}" label="{@b:label}">
    		<xsl:attribute name="value">
	    		<xsl:choose>
	    			<xsl:when test="count($value/*) &gt; 0">
	    				<xsl:for-each select="$value/*">
	    					<xsl:if test="position() &gt; 1">,</xsl:if><xsl:value-of select="."/>
	    				</xsl:for-each>
	    			</xsl:when>
	    			<xsl:otherwise><xsl:value-of select="$value"/></xsl:otherwise>
	    		</xsl:choose>
    		</xsl:attribute>
 		    <xsl:if test="b:editor/b:required or b:editor/b:validRegex">
 		    	<xsl:attribute name="data-validators">
 		    		<xsl:if test="b:editor/b:required">required</xsl:if>
 		    		<xsl:if test="b:editor/b:validRegex">validate-regex:'<xsl:value-of select="b:editor/b:validRegex"/>'</xsl:if>		    		
    			</xsl:attribute>
    		</xsl:if>
    		<xsl:if test="b:editor/b:maxLength &gt; 0">
    			<xsl:attribute name="maxlength"><xsl:value-of select="b:editor/b:maxLength"/></xsl:attribute>
    			<xsl:attribute name="size"><xsl:value-of select="b:editor/b:maxLength"/></xsl:attribute>
    		</xsl:if>
    	</input>
    </xsl:template>
</xsl:stylesheet>
