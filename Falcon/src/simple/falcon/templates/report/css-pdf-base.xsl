<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/XSL/Format"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:css="http://www.saf.org/simple/css" 
	xmlns:cmn="http://exslt.org/common"
	xmlns:regex="http://exslt.org/regular-expressions"
	exclude-result-prefixes="cmn regex">
	<xsl:output method="xml" omit-xml-declaration="yes"/>
	
	<xsl:template match="/">
		<xsl:apply-templates mode="traverse" select="*" />		 
	</xsl:template>
	<!-- XXX: I don't understand why this template is not matched -->
	<xsl:template match="root|layout-master-set|simple-page-master|region-before|region-body|region-after|page-sequence-master|repeatable-page-master-reference|page-sequence|title|static-content|flow" mode="traverse">
		<xsl:copy>
			<!-- Add attributes -->
			<xsl:for-each select="@*">
				<xsl:copy/>
			</xsl:for-each>
			<!-- Recurse nodes -->
			<xsl:apply-templates mode="traverse" select="*|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*[local-name()='root' or local-name()='layout-master-set' or local-name()='simple-page-master' or local-name()='region-before' or local-name()='region-body' or local-name()='region-after' or local-name()='page-sequence-master' or local-name()='repeatable-page-master-reference' or local-name()='page-sequence' or local-name()='title' or local-name()='static-content' or local-name()='flow']" mode="traverse">
		<xsl:copy>
			<!-- Add attributes -->
			<xsl:for-each select="@*">
				<xsl:copy/>
			</xsl:for-each>
			<!-- Recurse nodes -->
			<xsl:apply-templates mode="traverse" select="*|text()" />
		</xsl:copy>
	</xsl:template>
	
	
	<xsl:template match="*" mode="traverse" priority="-1">
		<xsl:copy>
			<!-- Get any style attributes from selectors matching this node -->
			<xsl:variable name="style-atts">
				<xsl:element name="{name()}">
					<xsl:apply-templates mode="stylesheet" select="."/>
					<!-- Add pre-existing attributes (last value of each unique attribute name overrides any previous value) -->
					<xsl:copy-of select="@*"/>
				</xsl:element>		
			</xsl:variable>
			<!-- Add style attributes -->
			<xsl:apply-templates mode="fix-atts" select="cmn:node-set($style-atts)/*/@*" />				
			<!-- Add space attributes -->
			<xsl:apply-templates mode="space-atts" select="cmn:node-set($style-atts)/*" />
			<!-- Recurse nodes -->
			<xsl:apply-templates mode="traverse" select="*|text()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="@*" mode="fix-atts" priority="-1">
		<xsl:copy/>
	</xsl:template>

	<!-- FOP 0.20.5 does not support margin, space-end, space-begin, and supports padding only on block elements -->
	<xsl:template match="@margin|@padding[local-name(..) != 'block']" mode="fix-atts"/>
	<xsl:template match="@start-indent|@space-start|@margin-left|@padding-left[local-name(..) != 'block']" mode="fix-atts"/>
	<xsl:template match="@end-indent|@space-end|@margin-right|@padding-right[local-name(..) != 'block']" mode="fix-atts"/>
	<xsl:template match="@space-before|@margin-top|@padding-top[local-name(..) != 'block']" mode="fix-atts"/>
	<xsl:template match="@space-after|@margin-bottom|@padding-bottom[local-name(..) != 'block']" mode="fix-atts"/>
	
	<xsl:template match="*" mode="space-atts">
		<xsl:variable name="start-indent">
			<xsl:for-each select="@start-indent|@space-start|@margin-left|@padding-left[local-name(..) != 'block']">
				<xsl:value-of select="normalize-space(.)"/> +
			</xsl:for-each>
			<xsl:for-each select="@margin[not(../@margin-left)]|@padding[local-name(..) != 'block' and not(../@padding-left)]">
				<xsl:variable name="parts" select="regex:match(., '^\s*(?:(\S+)\s+)?(?:(\S+)\s+)?(?:(\S+)\s+)?(\S+)\s*$', '')"/>
				<xsl:choose>
					<!-- 3 values - left is second one; 1, 2, or 4 values - left is last one -->
					<xsl:when test="count($parts[.]) = 3">
						<xsl:value-of select="$parts[2]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$parts[last()]"/>
					</xsl:otherwise>
				</xsl:choose> +
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="string-length(normalize-space($start-indent)) &gt; 0">
			<xsl:attribute name="start-indent"><xsl:value-of select="substring(normalize-space($start-indent), 1, string-length(normalize-space($start-indent)) - 2)"/></xsl:attribute>		
		</xsl:if>
		<xsl:variable name="end-indent">
			<xsl:for-each select="@end-indent|@space-end|@margin-right|@padding-right[local-name(..) != 'block']">
				<xsl:value-of select="normalize-space(.)"/> +
			</xsl:for-each>
			<xsl:for-each select="@margin[not(../@margin-right)]|@padding[local-name(..) != 'block' and not(../@padding-right)]">
				<xsl:variable name="parts" select="regex:match(., '^\s*(\S+)(?:\s+(\S+))?(?:\s+(\S+))?(?:\s+(\S+))?\s*$', '')"/>
				<xsl:choose>
					<!-- 1 value - right is first one; 4, 3, 2 values - right is second one -->
					<xsl:when test="count($parts[.]) = 1">
						<xsl:value-of select="$parts[1]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$parts[2]"/>
					</xsl:otherwise>
				</xsl:choose> +
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="string-length(normalize-space($end-indent)) &gt; 0">
			<xsl:attribute name="end-indent"><xsl:value-of select="substring(normalize-space($end-indent), 1, string-length(normalize-space($end-indent)) - 2)"/></xsl:attribute>		
		</xsl:if>
		<xsl:variable name="space-before">
			<xsl:for-each select="@space-before|@margin-top|@padding-top[local-name(..) != 'block']">
				<xsl:value-of select="normalize-space(.)"/> +
			</xsl:for-each>
			<xsl:for-each select="@margin[not(../@margin-top)]|@padding[local-name(..) != 'block' and not(../@padding-top)]">
				<xsl:variable name="parts" select="regex:match(., '^\s*(\S+)(?:\s+(\S+))?(?:\s+(\S+))?(?:\s+(\S+))?\s*$', '')"/>
				<!-- 4, 3, 2, or 1 value(s) - top is first one -->
				<xsl:value-of select="$parts[1]"/> +
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="string-length(normalize-space($space-before)) &gt; 0">
			<xsl:attribute name="space-before.optimum"><xsl:value-of select="substring(normalize-space($space-before), 1, string-length(normalize-space($space-before)) - 2)"/></xsl:attribute>		
		</xsl:if>
		<xsl:variable name="space-after">
			<xsl:for-each select="@space-after|@margin-bottom|@padding-bottom[local-name(..) != 'block']">
				<xsl:value-of select="normalize-space(.)"/> +
			</xsl:for-each>
			<xsl:for-each select="@margin[not(../@margin-bottom)]|@padding[local-name(..) != 'block' and not(../@padding-bottom)]">
				<xsl:variable name="parts" select="regex:match(., '^\s*(\S+)(?:\s+(\S+))?(?:\s+(\S+))?(?:\s+(\S+))?\s*$', '')"/>
				<xsl:choose>
					<!-- 1 or 2 values - bottom is first one; 4, 3 values - bottom is third one -->
					<xsl:when test="count($parts[.]) >= 3">
						<xsl:value-of select="$parts[3]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$parts[1]"/>
					</xsl:otherwise>
				</xsl:choose> +
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="string-length(normalize-space($space-after)) &gt; 0">
			<xsl:attribute name="space-after.optimum"><xsl:value-of select="substring(normalize-space($space-after), 1, string-length(normalize-space($space-after)) - 2)"/></xsl:attribute>		
		</xsl:if>
	</xsl:template>	
</xsl:stylesheet>