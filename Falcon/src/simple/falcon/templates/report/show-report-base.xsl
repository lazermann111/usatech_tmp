<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:img="http://simple/falcon/image/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:str="http://exslt.org/strings"
	exclude-result-prefixes="fr img x2 str">

	<xsl:template name="report-header"/>

	<xsl:template name="report-footer">
		<div class="copyright"><x2:translate key="report-copyright" is-markup="true"></x2:translate></div>
	</xsl:template>

	<xsl:template name="report">
		<table class="reportTitle">
			<xsl:variable name="table-width" select="normalize-space(fr:directives/fr:directive[@fr:name = 'table-width']/@fr:value)" />
			<xsl:if test="string-length($table-width) &gt; 0">
				<xsl:attribute name="style">width: <xsl:value-of select="$table-width"/>;</xsl:attribute>
			</xsl:if>
			<tr>
				<td>
					<xsl:call-template name="report-header"/>
		            <xsl:variable name="display-run-date" select="fr:directives/fr:directive[@fr:name = 'show-run-date']/@fr:value" />
					<xsl:if test="not($display-run-date) or $display-run-date != 'false'">
						<!-- <div class="rundate">Run Date: <xsl:value-of select="@fr:rundate"/></div> -->
						<div class="rundate"><xsl:value-of select="@fr:rundate"/></div>
					</xsl:if>
					<xsl:variable name="display-alt-output-types" select="fr:directives/fr:directive[@fr:name = 'show-alt-output-types']/@fr:value" />
					<xsl:if test="not($display-alt-output-types) or $display-alt-output-types != 'false'">
						<xsl:call-template name="output-type-menu"/>
					</xsl:if>
					<xsl:if test="string-length(normalize-space(@fr:title)) > 0">
						<div class="title0">
							<xsl:value-of select="@fr:title" />
						</div>
					</xsl:if>
					<xsl:if test="string-length(normalize-space(@fr:subtitle)) > 0">
						<div class="title1">
							<xsl:value-of select="@fr:subtitle" />
						</div>
					</xsl:if>
					<xsl:for-each select="fr:folio">
						<!-- <xsl:if test="position() &gt; 1">
							<hr class="folio-div"/>
						</xsl:if>
						 -->
						<xsl:if test="string-length(normalize-space(@fr:title)) > 0">
							<div class="title2">
								<xsl:value-of select="@fr:title" />
							</div>
						</xsl:if>
						<xsl:if test="string-length(normalize-space(@fr:subtitle)) > 0">
							<div class="title3">
								<xsl:value-of select="@fr:subtitle" />
							</div>
						</xsl:if>
						<xsl:if test="string-length(normalize-space(@fr:title)) > 0 or string-length(normalize-space(@fr:subtitle)) > 0">
							<div class="folio-line">&#160;</div>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="fr:error">
								<div class="error">
									<span class="errorLabel"><x2:translate key="report-error">An error occurred</x2:translate>:</span> <xsl:value-of select="fr:error" />
								</div>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="folio-content" />
							</xsl:otherwise>
						</xsl:choose>&#160;
					</xsl:for-each>
					<xsl:call-template name="report-footer"/>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="output-type-menu"/>

	<xsl:template name="note-desc">
		<xsl:if test="fr:note-values">
			<div class="notes">
				<xsl:variable name="folioId" select="@fr:folioId"/>
				<xsl:for-each select="fr:note-values/fr:note-value">
					<xsl:sort select="."/>
					<xsl:variable name="value" select="."/>
					<xsl:variable name="note" select="(//fr:note[text() = $value])[1]" />
					<xsl:if test="string-length(normalize-space($note/@fr:tip)) &gt; 0">
						<div class="note-desc" id="note_desc{$folioId}_{$note/@fr:id}">
							<xsl:if test="string-length(normalize-space($note/@fr:style)) > 0">
								<xsl:attribute name="style">
									<xsl:value-of select="$note/@fr:style" />
								</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="$note" />&#160;-&#160;<xsl:value-of select="$note/@fr:tip"/>
						</div>
					</xsl:if>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="icon-legend">
		<xsl:if test="fr:image-values">
			<div class="icon-legends">
				<xsl:for-each select="fr:image-values/fr:image-value">
					<xsl:sort select="."/>
					<xsl:variable name="value" select="."/>
					<xsl:variable name="image" select="(//fr:image[text() = $value])[1]" />
					<xsl:if test="string-length(normalize-space($image/@fr:tip)) &gt; 0">
						<span class="icon-legend">
							<img src="{$image}" alt="{$image/@fr:tip}" />&#160;-&#160;<xsl:value-of select="$image/@fr:tip"/>
						</span>
					</xsl:if>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="group-caption">
		<xsl:if test="fr:chunk-info/@fr:total-row-count = '?'">
			<div class="incompleteWarning"><x2:translate key="not-all-rows-shown">Warning: only a subset of rows are displayed and included in total calculations</x2:translate></div>
		</xsl:if>
		<xsl:variable name="display-count" select="fr:directives/fr:directive[@fr:name = 'show-count']/@fr:value" />
		<xsl:if test="(not($display-count) or $display-count != 'false') and (fr:chunk-info/@fr:total-row-count = '?' or number(fr:chunk-info/@fr:total-row-count) &gt; 0)">
			<span class="rowcount"><xsl:value-of select="fr:chunk-info"/></span>
		</xsl:if>
	</xsl:template>

	<xsl:template name="folio-content">
		<xsl:call-template name="group-layout" />
	</xsl:template>

	<xsl:template name="group-layout">
        <xsl:param name="dynamiclabels" select="fr:directives/fr:directive[@fr:name = 'dynamic-labels']/@fr:value" />
		<xsl:variable name="group-sections">
			<xsl:choose>
				<xsl:when test="fr:directives/fr:directive[@fr:name = 'group-sections']/@fr:value > 0">
					<xsl:value-of select="fr:directives/fr:directive[@fr:name = 'group-sections']/@fr:value"/>
				</xsl:when>
				<xsl:otherwise>2.0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:call-template name="group-caption"/>
		<xsl:variable name="display-legend" select="fr:directives/fr:directive[@fr:name = 'show-legend']/@fr:value" />
		<xsl:if test="(not($display-legend) or $display-legend != 'false') and (fr:image-values)">
			<xsl:call-template name="icon-legend"/>
		</xsl:if>

		<table class="folio" id="folio_{@fr:folioId}">
			<xsl:variable name="table-width" select="normalize-space(fr:directives/fr:directive[@fr:name = 'table-width']/@fr:value)" />
			<xsl:if test="string-length($table-width) &gt; 0">
				<xsl:attribute name="style">width: <xsl:value-of select="$table-width"/>;</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="count(fr:group) &gt; 0">
					<xsl:variable name="allcolumns" select="fr:columns/fr:column" />
					<xsl:variable name="tabcolumns" select="$allcolumns[@fr:level &lt; 3 and @fr:column-type != 'note' and @fr:column-type != 'all']"/>
					<xsl:variable name="xtabs" select="fr:xtab-values/fr:xtab-value"/>
					<xsl:variable name="percentFactor" select="number(boolean(/fr:report/@fr:output-type = 27))"/>                    
					<xsl:variable name="table-column-count" select="count($tabcolumns[@fr:level != 1]) + (count($xtabs) * count($tabcolumns[@fr:level = 0])) + $percentFactor * (count($tabcolumns[@fr:level != 1 and @fr:percent]) + (count($xtabs) * count($tabcolumns[@fr:level = 0 and @fr:percent])))"/>
					<!-- set cols -->
					<xsl:call-template name="tablecolumns">
						<xsl:with-param name="columns" select="$tabcolumns"/>
						<xsl:with-param name="xtabs" select="$xtabs"/>
					</xsl:call-template>

					<xsl:for-each select="fr:group">
						<xsl:call-template name="groupheader">
							<xsl:with-param name="allcolumns" select="$allcolumns" />
							<xsl:with-param name="tabcolumns" select="$tabcolumns" />
							<xsl:with-param name="table-column-count" select="$table-column-count" />
							<xsl:with-param name="xtabs" select="$xtabs" />
							<xsl:with-param name="group-sections" select="$group-sections"/>
                            <xsl:with-param name="dynamiclabels" select="$dynamiclabels"/>
						</xsl:call-template>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<tbody>
						<tr>
							<td class="zerorows">
								<xsl:choose>
									<xsl:when test="fr:directives/fr:directive[@fr:name = 'zero-rows-text']/@fr:value">
										<xsl:value-of select="fr:directives/fr:directive[@fr:name = 'zero-rows-text']/@fr:value"/>
									</xsl:when>
									<xsl:otherwise><x2:translate key="report-no-rows">No data found</x2:translate></xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</tbody>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<xsl:if test="(not($display-legend) or $display-legend != 'false') and (fr:note-values)">
			<xsl:call-template name="note-desc"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="groupheader">
		<xsl:param name="allcolumns" />
		<xsl:param name="tabcolumns" />
		<xsl:param name="table-column-count" />
		<xsl:param name="xtabs" />
		<xsl:param name="group-sections" />
                <xsl:param name="dynamiclabels" />

		<xsl:variable name="group-data" select="fr:group-data/*[local-name()='text' or local-name()='image']" />
		<!-- Group 1 = XTab; Group 2 = Row; Group 3 = Table -->
		<xsl:if test="$group-data">
			<xsl:variable name="all-data" select="fr:group-data/*[local-name()='all']" />
			<xsl:variable name="all-style" select="$all-data/@fr:style" />
			<xsl:variable name="default-link" select="$all-data[position()=1]/@fr:link" />
			<xsl:variable name="default-tip" select="$default-link/../@fr:tip" />
			<tbody>
				<tr class="groupHeaderRow{-2+@fr:level}">
					<xsl:if test="string-length(normalize-space($all-style)) > 0">
						<xsl:attribute name="style">
							<xsl:for-each select="$all-style[string-length(normalize-space()) > 0]">
								<xsl:value-of select="normalize-space(.)"/>;
							</xsl:for-each>
						</xsl:attribute>
					</xsl:if>
					<td colspan="{$table-column-count}">
					    <xsl:variable name="group-count" select="count($group-data)" />
                        <xsl:choose>
                            <xsl:when test="$group-count &gt; 1">
	                            <table class="groupInfo">
		                            <tbody>
		                                <xsl:variable name="group-rows" select="ceiling($group-count div $group-sections)" />
		                                <xsl:for-each select="$group-data[position() &lt;= $group-rows]">
		                                    <tr>
		                                        <xsl:variable name="group-index" select="position() mod $group-rows" />
		                                        <xsl:for-each select="$group-data[position() mod $group-rows = $group-index]">
		                                            <xsl:variable name="colid" select="@fr:id" />
		                                            <xsl:variable name="column" select="$allcolumns[@fr:id = $colid]" />
		                                            <td>
		                                              <xsl:if test="string-length(normalize-space(@fr:style)) > 0">
									                    <xsl:attribute name="style"><xsl:value-of select="@fr:style"/></xsl:attribute>
									                  </xsl:if>	                                            
		                                              <span class="groupLabel" title="{@fr:description}">
		                                                <xsl:if test="string-length(normalize-space($column)) > 0"><xsl:value-of select="$column" />:</xsl:if>
		                                                <span class="groupValue">
		                                                <xsl:call-template name="tablecellcontent">
		                                                    <xsl:with-param name="default-link" select="$default-link"/>
		                                                    <xsl:with-param name="default-tip" select="$default-tip"/>
		                                                </xsl:call-template>
		                                                </span>
		                                                </span>
		                                            </td>
		                                        </xsl:for-each>
		                                    </tr>
		                                </xsl:for-each>
		                            </tbody>
		                        </table>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:for-each select="$group-data">
                                    <xsl:variable name="colid" select="@fr:id" />
                                    <xsl:variable name="column" select="$allcolumns[@fr:id = $colid]" />
                                    <xsl:if test="string-length(normalize-space(@fr:style)) > 0">
					                    <xsl:attribute name="style"><xsl:value-of select="@fr:style"/></xsl:attribute>
					                </xsl:if>                                 
                                    <span class="groupLabel" title="{@fr:description}">
                                        <xsl:if test="string-length(normalize-space($column)) > 0"><xsl:value-of select="$column" />:</xsl:if>
                                        <span class="groupValue">
                                        <xsl:call-template name="tablecellcontent">
                                            <xsl:with-param name="default-link" select="$default-link"/>
                                            <xsl:with-param name="default-tip" select="$default-tip"/>
                                        </xsl:call-template>
                                        </span>
                                    </span>
                                </xsl:for-each>
                            </xsl:otherwise>
                        </xsl:choose>						
					</td>
				</tr>
			</tbody>
		</xsl:if>
		<!-- process sub-elements -->
		<xsl:choose>
			<xsl:when test="fr:group and (@fr:level = 3 or fr:group[1]/@fr:level &lt; 3)">
				<tbody>
					<tr class="headerRow">
						<xsl:call-template name="tableheader">
							<xsl:with-param name="columns" select="$tabcolumns" />
							<xsl:with-param name="xtabs" select="$xtabs"/>
                            <xsl:with-param name="dynamiclabels" select="$dynamiclabels" />
						</xsl:call-template>
					</tr>
				</tbody>
				<tbody>
					<xsl:call-template name="tabledata">
						<xsl:with-param name="columns" select="$tabcolumns" />
						<xsl:with-param name="xtabs" select="$xtabs"/>
						<xsl:with-param name="max-rows" select="@fr:max-rows" />
                                                <xsl:with-param name="dynamiclabels" select="$dynamiclabels" />
					</xsl:call-template>
				</tbody>
				<xsl:variable name="row-count" select="count(fr:group[@fr:level = 2]/fr:group-data)"/>
				<xsl:if test="@fr:max-rows > 0 and $row-count > @fr:max-rows">
					<tbody>
						<tr>
							<td class="morerows" colspan="{$table-column-count}">
								<xsl:value-of select="$row-count - @fr:max-rows"/>
								<xsl:text> </xsl:text>
								<x2:translate key="report-more-rows">more row(s)...</x2:translate>
							</td>
						</tr>
					</tbody>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="fr:group">
					<xsl:call-template name="groupheader">
						<xsl:with-param name="allcolumns" select="$allcolumns" />
						<xsl:with-param name="tabcolumns" select="$tabcolumns" />
						<xsl:with-param name="table-column-count" select="$table-column-count" />
						<xsl:with-param name="xtabs" select="$xtabs" />
						<xsl:with-param name="group-sections" select="$group-sections"/>
                        <xsl:with-param name="dynamiclabels" select="$dynamiclabels"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>

		<!-- Process totals -->
		<xsl:variable name="style-level">
			<xsl:choose>
				<xsl:when test="@fr:level &gt; 2"><xsl:value-of select="-2+@fr:level"/></xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="group-totals" select="fr:group-totals/*[local-name()='text' or local-name()='image']" />
		<xsl:if test="$group-totals">
			<tbody>
				<tr class="groupFooterRow{$style-level}">
					<xsl:variable name="idx" select="$group-totals[1]/@fr:id"/>
					<xsl:variable name="percentFactor" select="number(boolean(/fr:report/@fr:output-type = 27))"/>                    
                    <xsl:variable name="first-total-idx" select="count($tabcolumns[@fr:id &lt; $idx and @fr:level &gt; 1])"/>
					<xsl:variable name="first-total-col" select="count($tabcolumns[@fr:id &lt; $idx and @fr:level &gt; 1]) + $percentFactor * (count($tabcolumns[@fr:id &lt; $idx and @fr:level &gt; 1 and @fr:percent]))"/>
                    <xsl:if test="$first-total-col &gt; 0">
						<td	colspan="{$first-total-col}">
							<xsl:value-of select="$group-data[1]" />
							<xsl:text> </xsl:text>
							<x2:translate key="report-totals-label">Totals</x2:translate>:
						</td>
					</xsl:if>
					<xsl:variable name="group" select="."/>
					<xsl:variable name="all-data" select="fr:group-totals/*[local-name()='all']" />
					<xsl:variable name="all-style" select="$all-data/@fr:style" />
					<xsl:variable name="default-link" select="$all-data[position()=1]/@fr:link" />
					<xsl:variable name="default-tip" select="$default-link/../@fr:tip" />
			
					<xsl:for-each select="$tabcolumns[position() &gt; $first-total-idx]">
						<xsl:variable name="column" select="."/>
						<xsl:choose>
							<xsl:when test="@fr:level != 1">
								<xsl:variable name="group-cell" select="$group-totals[@fr:id = $column/@fr:id]" />
								<xsl:choose>
									<xsl:when test="$group-cell">
										<xsl:for-each select="$group-cell">
											<xsl:call-template name="tablecell">
												<xsl:with-param name="column" select="$column" />
												<xsl:with-param name="default-link" select="$default-link"/>
												<xsl:with-param name="default-tip" select="$default-tip"/>
												<xsl:with-param name="dynamiclabels" select="$dynamiclabels"/>
											</xsl:call-template>
										</xsl:for-each>
									</xsl:when>
									<xsl:otherwise>
										<td/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="@fr:id = ($tabcolumns[@fr:level = 1])[1]/@fr:id">
								<xsl:for-each select="$xtabs">
									<xsl:variable name="xtab-id" select="./fr:xtab-id"/>
									<xsl:variable name="xtab-total" select="$group/fr:xtab-totals[fr:xtab-id = $xtab-id]"/>
									<xsl:choose>
										<xsl:when test="$xtab-total">
											<xsl:for-each select="$xtab-total/*[local-name()!='xtab-id' and local-name()!='note' and local-name()!='all']">
												<xsl:variable name="total-id" select="@fr:id"/>
												<xsl:call-template name="tablecell">
													<xsl:with-param name="column" select="$tabcolumns[@fr:id = $total-id]" />
													<xsl:with-param name="default-link" select="$default-link"/>
													<xsl:with-param name="default-tip" select="$default-tip"/>
													<xsl:with-param name="dynamiclabels" select="$dynamiclabels"/>
												</xsl:call-template>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="$tabcolumns[@fr:level = 0]">
												<td data-sort-value=""/>
												<xsl:if test="@fr:percent = 'true' ">
													<td/>
												</xsl:if>
											</xsl:for-each>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</tbody>
		</xsl:if>
		<tbody>
			<tr class="groupSpacingRow{$style-level}">
				<td colspan="{$table-column-count}"/>
			</tr>
		</tbody>

		<!--  a chart if there is one -->
		<xsl:for-each select="fr:chart">
			<tbody>
				<tr>
					<td class="chart" colspan="{$table-column-count}">
						<xsl:call-template name="chart"/>
					</td>
				</tr>
			</tbody>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="chart">
		<xsl:choose>
			<!-- inline-image -->
			<xsl:when test="@fr:display-type='inline-image'">
				<img width="{@fr:width}px" height="{@fr:height}px">
					<xsl:attribute name="src">data:<xsl:value-of select="@fr:content-type"/>;base64,<xsl:value-of select="."/></xsl:attribute>
				</img>
			</xsl:when>
			<!-- inline-request -->
			<xsl:when test="@fr:display-type='inline-request'">
				<xsl:variable name="object-id" select="generate-id()"/>
				<iframe name="{$object-id}" width="{@fr:width}px" height="{@fr:height}px" frameborder="no" scrolling="no"
					marginheight="0" marginwidth="0"/>
				<!-- Use "POST" method because IE limits the length of get requests
					 to 2048 bytes (which includes the entire URL string)  -->
				<form id="form_{$object-id}" action="b64" target="{$object-id}" method="post">
					<input type="hidden" name="content-type" value="{@fr:content-type}"/>
					<input type="hidden" name="data" value="{translate(normalize-space(.), ' ', '')}"/>
				</form>
				<script type="text/javascript" defer="defer">
					document.getElementById("form_<xsl:value-of select="$object-id"/>").submit();
				</script>
			</xsl:when>
			<!-- reference -->
			<xsl:when test="string-length(normalize-space(.)) = 0 and @fr:reference-id">
				<img width="{@fr:width}px" height="{@fr:height}px">
					<xsl:attribute name="src">get_chart.i?chartContentType=<xsl:value-of select="@fr:content-type"/>&amp;chartReferenceId=<xsl:value-of select="@fr:reference-id"/></xsl:attribute>
				</img>
			</xsl:when>
			<!-- inline-divs -->
			<xsl:when test="@fr:display-type='inline-divs'">
				<div>
					<xsl:attribute name="style"><!--
					 -->position: relative; <!--
					 -->width: <xsl:value-of select="@fr:width"/>px; <!--
					 -->height: <xsl:value-of select="@fr:height"/>px;<!--
				 --></xsl:attribute>
					<xsl:for-each select="img:image/img:rect">
						<img src="images/spacer.gif">
							<xsl:attribute name="style"><!--
						     -->left: <xsl:value-of select="@img:x"/>px; <!--
							 -->top: <xsl:value-of select="@img:y"/>px; <!--
							 -->width: <xsl:value-of select="@img:width"/>px; <!--
							 -->height: <xsl:value-of select="@img:height"/>px; <!--
							 -->background-color: <xsl:value-of select="@img:color"/>; <!--
							 -->position: absolute;<!--
						 --></xsl:attribute>
						</img>
					</xsl:for-each>
				</div>
			</xsl:when>
			<!-- inline-applet -->
			<xsl:when test="@fr:display-type='inline-applet'">
				<object width="{@fr:width}px" height="{@fr:height}px" codetype="application/java"
					code="helpers.ImageDisplayer" codebase="code">
					<param name="data" value="{.}"/>
				</object>
			</xsl:when>
			<xsl:otherwise>
				<x2:translate key="report-cannot-display-image">Cannot display image</x2:translate>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="column-alignment">
	    <xsl:param name="column"  select="."/>
        <xsl:choose>
           <xsl:when test="$column/@fr:column-type='image'">center</xsl:when>
           <xsl:when test="$column/@fr:sort-type='NUMBER'">right</xsl:when>
           <xsl:otherwise>left</xsl:otherwise>
       </xsl:choose>
	</xsl:template>
	
    <xsl:template name="tablecolumns">
        <xsl:param name="columns"/>
        <xsl:param name="xtabs"/>
        <xsl:variable name="folioId" select="@fr:folioId"/>
        <style type="text/css">
            <xsl:for-each select="$columns"> 
	            <xsl:choose>
	                <xsl:when test="@fr:level != 1">
	                    #folio_<xsl:value-of select="$folioId" /> td[colId="<xsl:value-of select="@fr:id" />"] { text-align: <xsl:call-template name="column-alignment"/>;}
	                </xsl:when>
	                <xsl:when test="@fr:id = ($columns[@fr:level = 1])[1]/@fr:id">
	                    <xsl:for-each select="$xtabs">
	                        <xsl:variable name="xtab-index" select="position()"/>
	                        <xsl:for-each select="$columns[@fr:level = 0]">
	                            #folio_<xsl:value-of select="$folioId" /> td[colId="<xsl:value-of select="concat(@fr:id, ' ', $xtab-index)" />"] { text-align: <xsl:call-template name="column-alignment"/>;}
	                        </xsl:for-each>
	                    </xsl:for-each>
	                </xsl:when>
	            </xsl:choose>  
            </xsl:for-each>
        </style>
    </xsl:template>
    
	<xsl:template name="tablecolumns_xls">
		<xsl:param name="columns"/>
        <xsl:param name="xtabs"/>
        <xsl:for-each select="$columns">
			<xsl:choose>
				<xsl:when test="@fr:level != 1">
					<xsl:call-template name="tablecolumn_xls"/>
				</xsl:when>
				<xsl:when test="@fr:id = ($columns[@fr:level = 1])[1]/@fr:id">
					<xsl:for-each select="$xtabs">
						<xsl:for-each select="$columns[@fr:level = 0]">
							<xsl:call-template name="tablecolumn_xls"/>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="tablecolumn_xls">
		<xsl:element name="col">
			<!-- use col elements to specify fixed columns widths. Most common browser will only
			honor this as a minimum width if also the total table width is specified. Therefore, a
			trick with a transparant 1-pixel gif is used (see furtheron). The width specified here
			can still be used as a maximum width. -->
			<xsl:if test="string-length(normalize-space(@fr:width)) &gt; 0">
				<xsl:attribute name="width"><xsl:value-of select="@fr:width"/></xsl:attribute>
			</xsl:if>
			<!-- FireFox does not recognize 'align' on a col element and and it's deprecated -->
			<xsl:attribute name="align"><xsl:call-template name="column-alignment"/></xsl:attribute>
			<!--
			<xsl:if test="string-length(normalize-space(@fr:style)) > 0">
				<xsl:attribute name="style">
					<xsl:value-of select="@fr:style" />
				</xsl:attribute>
			</xsl:if>
			-->
		</xsl:element>
		<xsl:if test="/fr:report/@fr:output-type = 27 and @fr:percent"><col align="right"/></xsl:if>           
	</xsl:template>

	<xsl:template name="tableheader">
		<!-- takes a reference to fr:columns as parameter -->
		<xsl:param name="columns" />
		<xsl:param name="xtabs"/>
        <xsl:param name="dynamiclabels"/>
        <xsl:variable name="group" select="."/>
		<xsl:for-each select="$columns">
            <xsl:variable name="column" select="."/>
			<xsl:choose>
				<xsl:when test="@fr:level = 0 and count($xtabs) &gt; 0">
					<th colId="{@fr:id}">
					    <!--
						<xsl:variable name="minwidth" select="1" />
						<img src="images/spacer.gif" height="1" width="{$minwidth}" />
						<br />
						-->
						<a data-toggle="sort" data-sort-type="{@fr:sort-type}" title="{@fr:description}">
							<x2:translate key="report-xtab-total-column-label">Total</x2:translate> <xsl:value-of select="." />
						</a>
					</th>
					<xsl:if test="/fr:report/@fr:output-type = 27 and @fr:percent"><th><a>
                            <x2:translate key="report-xtab-percent-column-label">%</x2:translate> <xsl:value-of select="." />
                        </a></th></xsl:if>                        
				</xsl:when>
                                <xsl:when test="@fr:level != 1">
                                  <xsl:variable name="columnId" select="@fr:id" />
                                  <xsl:variable name="dyn-labs" select="../../fr:directives/fr:directive[@fr:name = 'dynamic-labels']/@fr:value" />
                                <xsl:choose>
                                   <xsl:when test="$dynamiclabels">
                                    <xsl:for-each select="$group">
                                      <xsl:variable name="IsLabel" select="fr:group/fr:group-data/fr:text[@fr:id=$column/@fr:id]/@fr:label"/>
                                        <xsl:if test="string-length(normalize-space($IsLabel)) > 0">
                                           <th colId="{$columnId}">
                                           <a data-toggle="sort" data-sort-type="{@fr:sort-type}" title="{@fr:description}">
                                           <xsl:value-of select="$IsLabel" /></a></th>
                                           <xsl:if test="/fr:report/@fr:output-type = 27 and $column/@fr:percent"><th><a>
					                            <x2:translate key="report-xtab-percent-column-label">%</x2:translate> <xsl:value-of select="$IsLabel" />
					                        </a></th></xsl:if>
                                        </xsl:if>
                                    </xsl:for-each>
                                   </xsl:when>
                                    <xsl:otherwise>
                                       <th colId="{@fr:id}">
		                        <!--  <xsl:variable name="minwidth" select="1" />
					<img src="images/spacer.gif" 
height="1" width="{$minwidth}"/><br /> -->
						<a data-toggle="sort" data-sort-type="{@fr:sort-type}" title="{@fr:description}">
							<xsl:value-of select="." />
						</a>
				       </th>
				       <xsl:if test="/fr:report/@fr:output-type = 27 and @fr:percent"><th><a>
                            <x2:translate key="report-xtab-percent-column-label">%</x2:translate> <xsl:value-of select="." />
                        </a></th></xsl:if>
                                    </xsl:otherwise>
                                </xsl:choose>
                                </xsl:when>
				<xsl:when test="@fr:id = ($columns[@fr:level = 1])[1]/@fr:id">
					<xsl:for-each select="$xtabs">
						<xsl:variable name="xtab-index" select="position()"/>
						<xsl:variable name="xtab-value">
							<xsl:for-each select="*[local-name()='text' or local-name()='image']">
								<xsl:choose>
									<xsl:when test="local-name()='text'">
										<xsl:value-of select="." />
									</xsl:when>
									<xsl:when test="local-name()='image'">
										<img src="{.}" alt="{@fr:tip}" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="." />
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text> </xsl:text>
							</xsl:for-each>
						</xsl:variable>
						<xsl:for-each select="$columns[@fr:level = 0]">
							<th colId="{@fr:id}_{$xtab-index}">
								<!--
								<xsl:variable name="minwidth" select="1" />
								<img src="images/spacer.gif" height="1" width="{$minwidth}" />
								<br />
								-->
								<a data-toggle="sort" data-sort-type="{@fr:sort-type}" title="{@fr:description} for {$xtab-value}">
									<xsl:copy-of select="$xtab-value" /> <xsl:value-of select="."/>
								</a>
							</th>
							<xsl:if test="/fr:report/@fr:output-type = 27 and @fr:percent"><th><a>
                                <xsl:copy-of select="$xtab-value" />  <xsl:value-of select="."/> %
                            </a></th></xsl:if>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="tabledata">
		<xsl:param name="columns" />
		<xsl:param name="xtabs"/>
		<xsl:param name="max-rows"/>
        <xsl:param name="dynamiclabels"/>

		<xsl:for-each select="(fr:group[@fr:level = 2])[not($max-rows) or $max-rows &lt;= 0 or position() &lt;= $max-rows]">
			<tr>
				<xsl:variable name="class-name">
					<xsl:choose>
						<xsl:when test="local-name() = 'total'">totalRow</xsl:when>
						<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
						<xsl:otherwise>oddRow</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="all-data" select="fr:group-data/*[local-name()='all']" />
				<xsl:variable name="all-style" select="$all-data/@fr:style" />
				<xsl:variable name="default-link" select="$all-data[position()=1]/@fr:link" />
				<xsl:variable name="default-tip" select="$default-link/../@fr:tip" />
				<xsl:if test="string-length(normalize-space($all-style)) > 0">
					<xsl:attribute name="style">
						<xsl:for-each select="$all-style[string-length(normalize-space()) > 0]">
							<xsl:value-of select="normalize-space(.)"/>;
						</xsl:for-each>
					</xsl:attribute>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="string-length(normalize-space($default-link)) > 0">
						<!--
						<xsl:attribute name="onclick">
							<xsl:choose>
								<xsl:when test="starts-with(normalize-space($default-link), 'javascript:')">
									<xsl:value-of select="normalize-space(substring-after($default-link, 'javascript:'))"/>
								</xsl:when>
								<xsl:otherwise>
									window.location='<xsl:value-of select="normalize-space($default-link)"/>';
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						 -->
						<xsl:attribute name="class"><xsl:value-of select="$class-name"/>Link</xsl:attribute>
						<xsl:attribute name="onmouseover">this.className=this.className.replace(/((?:odd|even|total)Row).*/, '$1Hover')</xsl:attribute>
						<xsl:attribute name="onmouseout">this.className=this.className.replace(/((?:odd|even|total)Row).*/, '$1Link')</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class"><xsl:value-of select="$class-name"/></xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:variable name="group" select="."/>
				<xsl:for-each select="$columns">
					<xsl:variable name="column" select="."/>
					<xsl:choose>
						<xsl:when test="@fr:level = 2">
							<xsl:for-each select="$group/fr:group-data/*[@fr:id = $column/@fr:id]">
								<xsl:call-template name="tablecell">
									<xsl:with-param name="column" select="$column" />
									<xsl:with-param name="default-link" select="$default-link"/>
									<xsl:with-param name="default-tip" select="$default-tip"/>
                                    <xsl:with-param name="dynamiclabels" select="$dynamiclabels"/> 
								</xsl:call-template>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="@fr:level = 0">
							<xsl:variable name="xtab-all-data" select="$group/fr:group-totals/*[local-name()='all']" />
							<xsl:variable name="xtab-all-style" select="$xtab-all-data/@fr:style" />
							<xsl:variable name="xtab-default-link" select="$xtab-all-data[position()=1]/@fr:link" />
							<xsl:variable name="xtab-default-tip" select="$xtab-default-link/../@fr:tip" />
				
							<xsl:for-each select="$group/fr:group-totals/*[@fr:id = $column/@fr:id]">
								<xsl:call-template name="tablecell">
									<xsl:with-param name="column" select="$column" />
									<xsl:with-param name="default-link" select="$xtab-default-link"/>
									<xsl:with-param name="default-tip" select="$xtab-default-tip"/>
									<xsl:with-param name="dynamiclabels" select="$dynamiclabels"/>
								</xsl:call-template>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="@fr:id = ($columns[@fr:level = 1])[1]/@fr:id">
							<xsl:for-each select="$xtabs">
								<xsl:variable name="xtab-id" select="./fr:xtab-id"/>
								<xsl:variable name="xtab-total" select="$group/fr:group[@fr:level=1 and fr:xtab-id=$xtab-id]/fr:group-totals"/>
								<xsl:choose>
									<xsl:when test="$xtab-total">
										<xsl:variable name="xtab-all-data" select="$xtab-total/*[local-name()='all']" />
										<xsl:variable name="xtab-all-style" select="$xtab-all-data/@fr:style" />
										<xsl:variable name="xtab-default-link" select="$xtab-all-data[position()=1]/@fr:link" />
										<xsl:variable name="xtab-default-tip" select="$xtab-default-link/../@fr:tip" />
				
										<xsl:for-each select="$xtab-total/*[local-name()!='note' and local-name()!='all']">
											<xsl:variable name="total-id" select="@fr:id"/>
											<xsl:call-template name="tablecell">
												<xsl:with-param name="column" select="$columns[@fr:id = $total-id]" />
												<xsl:with-param name="default-link" select="$xtab-default-link"/>
												<xsl:with-param name="default-tip" select="$xtab-default-tip"/>
												<xsl:with-param name="dynamiclabels" select="$dynamiclabels"/>
											</xsl:call-template>
										</xsl:for-each>
									</xsl:when>
									<xsl:otherwise>
										<xsl:for-each select="$columns[@fr:level = 0]">
											<td data-sort-value=""/>
											<xsl:if test="/fr:report/@fr:output-type = 27 and @fr:percent">
									            <td/>
									        </xsl:if>
										</xsl:for-each>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="tablecell">
		<xsl:param name="column" />
		<xsl:param name="default-link"/>
		<xsl:param name="default-tip"/>
        <xsl:param name="dynamiclabels"/>
        <xsl:variable name="columnId" select="@fr:id" />
      	<xsl:choose>
        	<xsl:when test="$dynamiclabels and @fr:label=''"></xsl:when>
            <xsl:otherwise>                
		    <td colId= "{$columnId}" data-sort-value="{@fr:sort-value}">
			    <xsl:if test="string-length(normalize-space(@fr:style)) > 0">
	                <xsl:attribute name="style"><xsl:value-of select="@fr:style"/></xsl:attribute>
	            </xsl:if>
			<xsl:call-template name="tablecellcontent">
				<xsl:with-param name="default-link" select="$default-link"/>
				<xsl:with-param name="default-tip" select="$default-tip"/>
			</xsl:call-template>
			<xsl:if test="$column/@fr:percent and string-length(normalize-space(@fr:percent)) > 0 and not(/fr:report/@fr:output-type = 27)">&#160;<span class="percentage">(<xsl:value-of select="@fr:percent" />)</span></xsl:if>
			</td>
                 </xsl:otherwise>
              </xsl:choose>
            <xsl:if test="$column/@fr:percent and /fr:report/@fr:output-type = 27">
                <td>
	                <xsl:if test="string-length(normalize-space(@fr:percent)) > 0">
		                <span class="percentage"><xsl:value-of select="@fr:percent" /></span>
		            </xsl:if>  
	            </td>
         </xsl:if>
	</xsl:template>

	<xsl:template name="tablecellcontent">
		<xsl:param name="default-link"/>
		<xsl:param name="default-tip"/>
		<xsl:if test="string-length(normalize-space(.)) > 0">
			<xsl:choose>
				<xsl:when test="local-name()='button'">
					<input type="button" value="{.}">
					    <xsl:choose>
					       <xsl:when test="starts-with(normalize-space(@fr:link), 'javascript:')">
					           <xsl:attribute name="onclick"><xsl:value-of select="substring-after(@fr:link, 'javascript:')"/></xsl:attribute>
					           <xsl:attribute name="title"><xsl:value-of select="@fr:tip"/></xsl:attribute>
		                   </xsl:when>
					       <xsl:when test="string-length(normalize-space(@fr:link)) > 0">
                               <xsl:attribute name="onclick"><xsl:value-of select="@fr:link"/></xsl:attribute>
                               <xsl:attribute name="title"><xsl:value-of select="@fr:tip"/></xsl:attribute>
                           </xsl:when>
                        </xsl:choose>
					</input>
				</xsl:when>
				<xsl:when test="local-name()='checkbox'">
					<input type="checkbox" name="{.}" value="{@fr:sort-value}" title="{@fr:tip}">
						<xsl:choose>
                            <xsl:when test="starts-with(normalize-space(@fr:link), 'javascript:')">
                               <xsl:attribute name="onclick"><xsl:value-of select="substring-after(@fr:link, 'javascript:')"/></xsl:attribute>
                           </xsl:when>
                           <xsl:when test="string-length(normalize-space(@fr:link)) > 0">
                               <xsl:attribute name="onclick"><xsl:value-of select="@fr:link"/></xsl:attribute>
                           </xsl:when>
                        </xsl:choose>
                    </input>
				</xsl:when>
				<xsl:when test="local-name()='radio'">
					<input type="radio" name="{.}" value="{@fr:sort-value}" title="{@fr:tip}">
						<xsl:choose>
                            <xsl:when test="starts-with(normalize-space(@fr:link), 'javascript:')">
                               <xsl:attribute name="onclick"><xsl:value-of select="substring-after(@fr:link, 'javascript:')"/></xsl:attribute>
                           </xsl:when>
                           <xsl:when test="string-length(normalize-space(@fr:link)) > 0">
                               <xsl:attribute name="onclick"><xsl:value-of select="@fr:link"/></xsl:attribute>
                           </xsl:when>
                        </xsl:choose>
					</input>
				</xsl:when>
				<xsl:when test="starts-with(normalize-space(@fr:link), 'javascript:')">
                    <a onclick="{substring-after(@fr:link, 'javascript:')}" title="{@fr:tip}">
                        <xsl:call-template name="tablecelldata"/>
                    </a>
                </xsl:when>
                <xsl:when test="string-length(normalize-space(@fr:link)) > 2048">
                    <a onclick="Browser.post('{substring-before(@fr:link, '?')}', '{substring-after(@fr:link, '?')}');" title="{@fr:tip}">
                        <xsl:call-template name="tablecelldata"/>
                    </a>
                </xsl:when>
                <xsl:when test="string-length(normalize-space(@fr:link)) > 0">
					<a href="{@fr:link}" title="{@fr:tip}">
						<xsl:call-template name="tablecelldata"/>
					</a>
				</xsl:when>
				<xsl:when test="starts-with(normalize-space($default-link), 'javascript:')">
                    <a onclick="{substring-after($default-link, 'javascript:')}" title="{$default-tip}">
                        <xsl:call-template name="tablecelldata"/>
                    </a>
                </xsl:when>
                <xsl:when test="string-length(normalize-space($default-link)) > 2048">
                    <a onclick="Browser.post('{substring-before($default-link, '?')}', '{substring-after($default-link, '?')}');" title="{$default-tip}">
                        <xsl:call-template name="tablecelldata"/>
                    </a>
                </xsl:when>
                <xsl:when test="string-length(normalize-space($default-link)) > 0">
					<a href="{$default-link}" title="{$default-tip}">
						<xsl:call-template name="tablecelldata"/>
					</a>
				</xsl:when>
				<!--
				<xsl:when test="$design-mode">
					<a href="javascript: handleSelection('{@fr:field-id}', &#34;{@fr:value}&#34;))" title="Click to filter by this value">
						<xsl:call-template name="tablecelldata"/>
					</a>
				</xsl:when>
				 -->
				 <xsl:otherwise>
				 		<span>
					 		<xsl:call-template name="tablecelldata"/>
						</span>
				  </xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<!--  notes -->
		<xsl:call-template name="notes"/>
	</xsl:template>

	<xsl:template name="tablecelldata">
		<xsl:choose>
			<xsl:when test="local-name()='text'">
				<xsl:value-of select="." />
			</xsl:when>
			<xsl:when test="local-name()='image'">
				<img src="{.}" alt="{@fr:tip}" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="notes">
		<xsl:processing-instruction name="output">preserve-space="yes"</xsl:processing-instruction>
		<xsl:for-each select="(following-sibling::*[1])[local-name()='note']">
			<xsl:if test="string-length(normalize-space()) > 0"><!--
				--><sup title="{@fr:tip}" class="note" id="note{ancestor::fr:folio/@fr:folioId}_{@fr:id}">
					<xsl:if test="string-length(normalize-space(@fr:style)) > 0">
						<xsl:attribute name="style">
							<xsl:value-of select="@fr:style" />
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="." />
				</sup>
			</xsl:if>
			<xsl:call-template name="notes"/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:include href="../general/html-base-customize.xsl"/>
	
    <xsl:template name="subtitle">
        <xsl:choose>
            <xsl:when test="string-length(normalize-space(fr:report/@fr:title)) > 0">
                <xsl:value-of select="fr:report/@fr:title"/>
            </xsl:when>
            <xsl:when test="string-length(normalize-space(fr:report/fr:folio[1]/@fr:title)) > 0">
                <xsl:value-of select="fr:report/fr:folio[1]/@fr:title"/>
            </xsl:when>
            <xsl:otherwise>
                <x2:translate key="report-default-title">Custom Report</x2:translate>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="base">
        <base href="{fr:report/fr:scene/@fr:base-url}" />
    </xsl:template>

    <xsl:template name="styles">
        <xsl:for-each select="fr:report/fr:scene/fr:style[string-length(normalize-space()) > 0]">
            <link href="{.}" type="text/css" rel="stylesheet"/>
        </xsl:for-each>
        <xsl:variable name="notes" select="fr:report/fr:folio/fr:columns/fr:column[@fr:column-type = 'note']" />
        <xsl:if test="$notes">
            <style>
                <xsl:for-each select="$notes">
                .note<xsl:value-of select="../../@fr:folioId" />_<xsl:value-of select="@fr:id" />{ <xsl:value-of select="@fr:style" /> }
                .note-desc<xsl:value-of select="../../@fr:folioId" />_<xsl:value-of select="@fr:id" />{ <xsl:value-of select="@fr:style" /> }<!--
             --></xsl:for-each>
            </style>
        </xsl:if>
        <xsl:call-template name="extra-styles"/>
    </xsl:template>

    <xsl:template name="extra-scripts">
        <xsl:for-each select="fr:report/fr:scene/fr:script[string-length(normalize-space()) > 0]">
            <script type="text/javascript" src="{.}"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="contents">
        <xsl:for-each select="fr:report">
            <xsl:call-template name="report"/>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>