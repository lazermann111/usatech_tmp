<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:img="http://simple/falcon/image/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr img">
	<xsl:import href="show-report-base.xsl"/>
	<xsl:import href="show-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:template name="output-type-menu-content">
		<xsl:param name="run-report-action" />
		<xsl:variable name="currentType" select="@fr:output-type" />
		<div class="output-type-menu">
			<form name="changeOutput" action="{$run-report-action}">
				<xsl:choose>
					<xsl:when test="@fr:reportId">
						<input type="hidden" name="reportId" value="{@fr:reportId}"/>
					</xsl:when>
					<xsl:when test="count(fr:folio/@fr:folioId) = 1">
						<input type="hidden" name="folioId" value="{fr:folio[1]/@fr:folioId}"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="fr:folio/@fr:folioId">
							<input type="hidden" name="folioIds" value="{.}"/>
						</xsl:for-each>
					</xsl:otherwise>					
				</xsl:choose>					
				<xsl:call-template name="report-parameters-form">
					<xsl:with-param name="overrides">
						<p:parameter name="outputType" value="" />
						<p:parameter name="unframed" value="" />
						<p:parameter name="resultsType" value="cache" />
						<xsl:if test="@fr:chart-type"><p:parameter name="chartType" value="{@fr:chart-type}" /></xsl:if>
						<p:parameter name="doChartConfigUpdate" value="false" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:if test="$currentType != 22">
					<input type="image" class="clearBorder" alt="View as HTML" title="View as HTML" value="22" src="./images/icons/html.gif" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$currentType = 28">
						<input type="image" class="clearBorder" alt="View Chart in PDF" title="View Chart in PDF" value="31" src="./images/icons/pdf.gif" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
					</xsl:when>
					<xsl:otherwise>
						<input type="image" class="clearBorder" alt="View as PDF" title="View as PDF" value="24" src="./images/icons/pdf.gif" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
					</xsl:otherwise>
				</xsl:choose>
				<input type="image" class="clearBorder" alt="View as Excel" title="View as Excel" value="27" src="./images/icons/excel.jpg" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
				<xsl:variable name="allow-chart-config" select="fr:directives/fr:directive[@fr:name = 'allow-chart-config']/@fr:value" />
				<xsl:if test="$allow-chart-config and $allow-chart-config = 'true'">
					<input type="image" class="clearBorder" value="28" alt="Create Chart from Data" title="Create Chart from Data" src="./images/icons/chart.jpg" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
				</xsl:if>
				<script type="text/javascript">
function submitChangeOutput(node){
	node.form.outputType.value = node.value;
	if(node.value == "28"){
		node.form.unframed.value = "false";
		node.form.action = "configure_chart.i";
	} <xsl:if test="$currentType = 28">else if(node.value == "31") {
		node.form.unframed.value = "true";
		node.form.doChartConfigUpdate.value = "true";
		node.form.action = "<xsl:value-of select="$run-report-action"/>";
	} </xsl:if>else {
		node.form.unframed.value = "true";
		node.form.action = "<xsl:value-of select="$run-report-action"/>";
	}
	node.form.submit();
}
				</script>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>
