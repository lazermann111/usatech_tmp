/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.xml;

import java.util.Iterator;
import java.util.Map;

import simple.chart.ChartMaker;
import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.Executor;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.results.Results;

/**
 * @author bkrug
 *
 */
public class ChartInputSource extends ReportInputSource {
    public ChartMaker chartMaker;
    public Iterator<String> clusterAxisNames;
    public Iterator<String> valueAxisNames;
    public DisplayType displayType;
    public Map<String,Pillar> dynamicLabelClusters;

    public ChartInputSource(Report report, Results[] results, Map<String,Object> parameters, ColumnNamer namer, Executor executor, Scene scene, OutputType outputType) {
        super(report, results, parameters, namer, executor, scene, outputType);
    }

}
