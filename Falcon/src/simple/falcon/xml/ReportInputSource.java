/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.xml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.xml.sax.InputSource;

import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.results.Results;
import simple.util.SparseBuckets;

/**
 * @author bkrug
 *
 */
public class ReportInputSource extends InputSource {
    protected Report report;
    protected Results[] results;
    protected final Map<String,Object> parameters;
    protected ColumnNamer namer;
    protected Scene scene;
    protected Executor executor;
    protected OutputType outputType;

    public static class PillarInfo {
        public Pillar pillar;
        public String pillarId;
        public String typeName;
    }
    public final SparseBuckets<PillarInfo> groups = new SparseBuckets<PillarInfo>();
    public final SortedMap<Integer, String> groupColumnNames = new TreeMap<Integer, String>();
    public final List<String> groupOneColumnNames = new ArrayList<String>();
    public int maxLevel = Folio.GROUPING_LEVEL_ZERO;
	public final List<PillarInfo> groupTableTotals = new ArrayList<PillarInfo>();

    public final Set<String> noteValues = new HashSet<String>();
    public final Set<String> imageValues = new HashSet<String>();

    /**
     *
     */
    public ReportInputSource(Report report, Results[] results, Map<String,Object> parameters, ColumnNamer namer, Executor executor, Scene scene, OutputType outputType) {
        this.report = report;
        this.results = results;
        this.parameters = parameters;
        this.namer = namer;
        this.scene = scene;
        this.executor = executor;
        this.outputType = outputType;
    }

    /**
     * @return Returns the report.
     */
    public Report getReport() {
        return report;
    }

    /**
     * @param report The report to set.
     */
    public void setReport(Report report) {
        this.report = report;
    }

    /**
     * @return Returns the results.
     */
    public Results[] getResults() {
        return results;
    }
    /**
     * @param results The results to set.
     */
    public void setResults(Results[] results) {
        this.results = results;
    }

    /**
     * @return Returns the parameters.
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * @return Returns the scene.
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * @param scene The scene to set.
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public ColumnNamer getNamer() {
        return namer;
    }

    public void setNamer(ColumnNamer namer) {
        this.namer = namer;
    }

    /**
     * @return Returns the outputType.
     */
    public OutputType getOutputType() {
        return outputType;
    }

    /**
     * @param outputType The outputType to set.
     */
    public void setOutputType(OutputType outputType) {
        this.outputType = outputType;
    }

	public Executor getExecutor() {
		return executor;
	}

	public void setExecutor(Executor executor) {
		this.executor = executor;
	}

}
