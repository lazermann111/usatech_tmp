/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.xml;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsValueRetriever;
import simple.falcon.engine.Scene;
import simple.falcon.engine.standard.MultiValueWithContext;
import simple.falcon.engine.standard.ValueWithContext;
import simple.falcon.xml.ReportInputSource.PillarInfo;
import simple.io.Log;
import simple.results.DataGenre;
import simple.results.Results;
import simple.results.ResultsFilter;
import simple.text.MessageFormat;
import simple.text.MessageFormat.FormatObject;
import simple.util.StaticCollection;
import simple.xml.AbstractXMLReader;
import simple.xml.ExtendedContentHandler;
import simple.xml.sax.PlaybackInputSource;

/**
 * @author bkrug
 *
 */
public class ReportAdapter<IS extends ReportInputSource> extends AbstractXMLReader<IS> {
    private static final Log log = Log.getLog();
    public static final String NAMESPACE = "http://simple/falcon/report/1.0";
    public static final String REPORT_ELEMENT = "report";
    public static final String FOLIO_ELEMENT = "folio";
    public static final String COLUMNS_ELEMENT = "columns";
    public static final String COLUMN_ELEMENT = "column";
    public static final String ROWS_ELEMENT = "data";
    public static final String ROW_ELEMENT = "row";
    //public static final String CELL_ELEMENT = "field";
    public static final String SUMMARY_ELEMENT = "summary";
    public static final String PARAMETERS_ELEMENT = "parameters";
    public static final String PARAMETER_ELEMENT = "parameter";
    public static final String DIRECTIVES_ELEMENT = "directives";
    public static final String DIRECTIVE_ELEMENT = "directive";
    public static final String SCENE_ELEMENT = "scene";
	public static final String EXECUTOR_ELEMENT = "executor";
    public static final String STYLE_ELEMENT = "style";
    public static final String SCRIPT_ELEMENT = "script";
    public static final String PILLAR_ID_ATT = "id";
    public static final String SORT_TYPE_ATT = "sort-type";
    public static final String VALUE_ATT = "value";
    public static final String SORT_VALUE_ATT = "sort-value";
    public static final String SLICE_ELEMENT = "chunk-info";
    public static final String OFFSET_ATT = "chunk-offset";
    public static final String ROWS_SHOWN_ATT = "chunk-length";
    public static final String TOTAL_COUNT_ATT = "total-row-count";
    public static final String TITLE_ATT = "title";
    public static final String SUBTITLE_ATT = "subtitle";
    public static final String PILLAR_TYPE_ATT = "column-type";
    public static final String DESC_ATT = "description";
    public static final String ACTION_ATT = "link";
    public static final String HELP_ATT = "tip";
    public static final String NOTE_ELEMENT = "note";
    public static final String STYLE_ATT = "style";
    public static final String LABEL_ATT = "label";
    public static final String WIDTH_ATT = "width";
    public static final String GROUPING_LEVEL_ATT = "level";
    public static final String PERCENT_ATT = "percent";
    public static final String GROUP_ELEMENT = "group";
    public static final String GROUP_DATA_ELEMENT = "group-data";
    public static final String GROUP_TOTALS_ELEMENT = "group-totals";
    public static final String VERBAGE_ELEMENT = "verbage";
    public static final String OUTPUT_TYPE_ATT = "output-type";
    public static final String RUN_DATE_ATT = "rundate";
    public static final String CHART_TYPE_ATT = "chart-type";
    public static final String BASE_URL_ATT = "base-url";
    public static final String USER_AGENT_ATT = "user-agent";
    public static final String RUN_REPORT_ACTION_ATT = "runReportAction";
    public static final String LIVE_ATT = "live";
    public static final String LAYOUT_ATT = "layout";
	public static final String USER_ID_ATT = "userId";
	public static final String GROUP_IDS_ATT = "groupIds";
	public static final String DISPLAY_NAME_ATT = "displayName";
    public static final String XTAB_SUMMARY_ELEMENT = "xtab-values";
    public static final String XTAB_VALUE_ELEMENT = "xtab-value";
    public static final String XTAB_ID_ELEMENT = "xtab-id";
    public static final String XTAB_TOTALS_ELEMENT = "xtab-totals";

    public static final String[] REPORT_ATTS = new String[] {TITLE_ATT, SUBTITLE_ATT, OUTPUT_TYPE_ATT, RUN_DATE_ATT };
    public static final String[] FOLIO_ATTS = new String[] {"folioId", TITLE_ATT, SUBTITLE_ATT, OUTPUT_TYPE_ATT, CHART_TYPE_ATT };
    public static final String[] ROWS_ATTS = new String[] {"max-rows"};
    public static final String[] GROUP_ATTS = new String[] { GROUPING_LEVEL_ATT };
    public static final String[] PARAMETERS_ATTS = new String[] {"name", "value"};
    public static final String[] SCENE_ATTS = new String[] { BASE_URL_ATT, USER_AGENT_ATT, RUN_REPORT_ACTION_ATT, LIVE_ATT, LAYOUT_ATT };
    public static final String[] DIRECTIVE_ATTS = PARAMETERS_ATTS; //new String[] {"name", "value"};
    public static final String[] SLICE_ATTS = new String[] {OFFSET_ATT, ROWS_SHOWN_ATT, TOTAL_COUNT_ATT};
	public static final String[] EXECUTOR_ATTS = new String[] { USER_ID_ATT, GROUP_IDS_ATT, DISPLAY_NAME_ATT };

    protected static final Comparator<Object[]> valuesComparator = new Comparator<Object[]>() {
		public int compare(Object[] o1, Object[] o2) {
			int i = compareElements(o1[0], o2[0]);
			if(i == 0)
				i = compareElements(o1[1], o2[1]);
			return i;
		}
		@SuppressWarnings({ "unchecked", "rawtypes" })
		protected int compareElements(Object o1, Object  o2) {
			if(o1 == o2) return 0;
			if(o1 == null) return -1;
			if(o2 == null) return 1;
			return ((Comparable)o1).compareTo(o2);
		}
    };

    protected static class SpecialPlaybackInputSource extends PlaybackInputSource implements ResultsFilter {
    	protected final String id;
    	protected boolean current;
    	public SpecialPlaybackInputSource(String id) {
    		this.id = id;
    	}
    	public boolean isValidRow(int row, Object[] data, Results results) {
			return current;
		}
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof SpecialPlaybackInputSource) {
				return id.equals(((SpecialPlaybackInputSource)obj).id);
			}
			return false;
		}
		@Override
		public int hashCode() {
			return id.hashCode();
		}
		public boolean isCurrent() {
			return current;
		}
		public void setCurrent(boolean current) {
			this.current = current;
		}
		public String getId() {
			return id;
		}
    }

    protected static class XTabHelper {
    	public final SortedMap<Object[],XTabFilterPlayback> groupOneValues = new TreeMap<Object[],XTabFilterPlayback>(valuesComparator);
    	protected final int[] groupOneColumnIndexes;
    	protected int lastId = 0;
    	protected XTabFilterPlayback currentFilter;
    	protected XTabFilterPlayback newFilter;
    	protected final List<PillarInfo> groupZero;
    	protected boolean registered;
    	protected class XTabFilterPlayback extends SpecialPlaybackInputSource {
    		protected XTabFilterPlayback() {
    			super(String.valueOf(++lastId));
    			setNameSpaceURI(NAMESPACE);
    		}
	    	@Override
			public boolean isValidRow(int row, Object[] data, Results results) {
	    		if(currentFilter == null) {
	    			Object[] key = new Object[groupOneColumnIndexes.length];
					for(int i = 0; i < groupOneColumnIndexes.length; i++)
						key[i] = data[groupOneColumnIndexes[i]-1];
					currentFilter = groupOneValues.get(key);
					if(currentFilter == null) {
						currentFilter = newFilter;
						groupOneValues.put(key, newFilter);
						newFilter = new XTabFilterPlayback();
						registered = false;
					}
	    		}
				return currentFilter == this;
	    	}
    	}
    	public XTabHelper(List<PillarInfo> groupOne, List<PillarInfo> groupZero, List<String> groupOneColumnNames, Results results) {
    		groupOneColumnIndexes = new int[groupOneColumnNames.size()];
    		int i = 0;
    		for(String gcn : groupOneColumnNames) {
    			groupOneColumnIndexes[i++] = results.getColumnIndex(gcn);
    		}
    		this.groupZero = groupZero;
    		newFilter = new XTabFilterPlayback();
			registerNewFilter(results);
    	}
    	public void clearCurrent(Results results) {
    		if(!registered) {
    			registerNewFilter(results);
    		}
    		currentFilter = null;
    	}
    	public SpecialPlaybackInputSource getCurrent() {
    		return currentFilter;
    	}
    	protected void registerNewFilter(Results results) {
    		for(PillarInfo pi : groupZero) {
    			ResultsValueRetriever displayRetreiver = pi.pillar.getDisplayValueRetriever();
	            ResultsValueRetriever sortRetreiver = pi.pillar.getSortValueRetriever();
	            displayRetreiver.trackAggregates(results, newFilter);
	            sortRetreiver.trackAggregates(results, newFilter);
    		}
			registered = true;
    	}
    }

    /**
     * @throws org.xml.sax.SAXException
     */
    public ReportAdapter() {
    }

    /** This generates events for the given input source. It is thread-safe.
     * (State info is stored in the inputsource)
     *
     * @param inputSource
     * @throws SAXException
     */
    @Override
	public void generateEvents(IS inputSource) throws SAXException {
        try {
            log.debug("Generating events for " + inputSource);
            generateHeader(inputSource);
            Folio[] folios = inputSource.getReport().getFolios();
            for(int i = 0; i < folios.length; i++) {
                processFolio(folios[i], inputSource.getResults()[i], inputSource);
            }
            generateFooter(inputSource);
            log.debug("Done Generating events for " + inputSource);
        } catch(SAXException e) {
            //We want to know about every exception
            log.warn("While Generating events for " + inputSource, e);
            throw e;
        } catch(RuntimeException e) {
            //We want to know about every exception
            log.warn("While Generating events for " + inputSource, e);
            throw e;
        }
    }

    /** This generates events for the given input source. It is thread-safe.
     * (State info is stored in the inputsource)
     *
     * @param inputSource
     * @throws SAXException
     */
    public void processFolio(Folio folio, Results results, IS inputSource) throws SAXException {
        //reset state
        inputSource.groupColumnNames.clear();
        inputSource.groups.clear();
        inputSource.imageValues.clear();
        inputSource.noteValues.clear();
        inputSource.maxLevel = Folio.GROUPING_LEVEL_ZERO;
		inputSource.groupTableTotals.clear();
		inputSource.groupOneColumnNames.clear();

        log.debug("Generating events for folio " + folio.getFolioId());
        startElement(FOLIO_ELEMENT, FOLIO_ATTS,
        		folio.getFolioId() == null ? null : folio.getFolioId().toString(),
                folio.getTitle(inputSource.getParameters()),
                folio.getSubtitle(inputSource.getParameters()),
                String.valueOf(folio.getOutputType().getOutputTypeId()),
                folio.getChartType());

        // directives
        startElement(DIRECTIVES_ELEMENT);
        for(String name : folio.getDirectiveNames()) {
            startElement(DIRECTIVE_ELEMENT, DIRECTIVE_ATTS, name, folio.getDirective(name));
            endElement(DIRECTIVE_ELEMENT);
        }
        endElement(DIRECTIVES_ELEMENT);

        generateColumns(folio, results, inputSource);

        generateContent(folio, results, inputSource);

        if(!inputSource.noteValues.isEmpty()) {
            startElement("note-values");
            for(String value : inputSource.noteValues) {
                startElement("note-value");
                characters(value);
                endElement("note-value");
            }
            endElement("note-values");
        }
        if(!inputSource.imageValues.isEmpty()) {
            startElement("image-values");
            for(String value : inputSource.imageValues) {
                startElement("image-value");
                characters(value);
                endElement("image-value");
            }
            endElement("image-values");
        }
        endElement(FOLIO_ELEMENT);
    }

    protected void generateColumns(Folio folio, Results results, IS inputSource) throws SAXException {
    	startElement(COLUMNS_ELEMENT);
        final Pillar[] pillars = folio.getPillars();
        for(int i = 0; i < pillars.length; i++) {
            try {
            	AttributesImpl atts = new AttributesImpl();
                String pillarId = String.valueOf(i+1);
                String typeName = getPillarTypeString(pillars[i]);
                atts.addAttribute(NAMESPACE, PILLAR_ID_ATT, PILLAR_ID_ATT, "CDATA", pillarId);
                if(pillars[i].getSortType() == DataGenre.DATE)
                	atts.addAttribute(NAMESPACE, SORT_TYPE_ATT, SORT_TYPE_ATT, "CDATA", DataGenre.NUMBER.name());
                else if(pillars[i].getSortType() != null)
                	atts.addAttribute(NAMESPACE, SORT_TYPE_ATT, SORT_TYPE_ATT, "CDATA", pillars[i].getSortType().name());
                atts.addAttribute(NAMESPACE, PILLAR_TYPE_ATT, PILLAR_TYPE_ATT, "CDATA", typeName);
                atts.addAttribute(NAMESPACE, DESC_ATT, DESC_ATT, "CDATA", pillars[i].getDescription());
                if(pillars[i].getWidth() != null)
                	atts.addAttribute(NAMESPACE, WIDTH_ATT, WIDTH_ATT, "CDATA", pillars[i].getWidth());
                atts.addAttribute(NAMESPACE, GROUPING_LEVEL_ATT, GROUPING_LEVEL_ATT, "CDATA", String.valueOf(pillars[i].getGroupingLevel()));
				if(pillars[i].getPercentFormat() != null)
					atts.addAttribute(NAMESPACE, PERCENT_ATT, PERCENT_ATT, "CDATA", "true");
                PillarInfo pi = new PillarInfo();
                pi.pillarId = pillarId;
                pi.pillar = pillars[i];
                pi.typeName = typeName;

                processPillar(results, pi, atts, inputSource);
                startElement(COLUMN_ELEMENT, atts);
                characters(ConvertUtils.formatObject(null, pillars[i].getLabelFormat()));
                endElement(COLUMN_ELEMENT);
            } catch(IllegalArgumentException e){
				if(log.isDebugEnabled())
					log.debug("Excluding pillar " + (i + 1) + " from report because a field is not in the resultset");
            }

        }
        endElement(COLUMNS_ELEMENT);

        //Add Groups to Results by level and make sure there are no gaps in groupColumnNames
        Set<String> usedGCNs = new HashSet<String>();
        String last = null;
        for(int i = inputSource.maxLevel; i >= Folio.GROUPING_LEVEL_ZERO; i--) {
        	List<PillarInfo> group = inputSource.groups.get(i);
        	if(group != null) {
        		for(PillarInfo pillarInfo : group) {
		            ResultsValueRetriever displayRetreiver = pillarInfo.pillar.getDisplayValueRetriever();
		            ResultsValueRetriever sortRetreiver = pillarInfo.pillar.getSortValueRetriever();
		            List<String> sortGCNs = sortRetreiver.addGroups(results);
		            List<String> displayGCNs = displayRetreiver.addGroups(results);
		            for(String gcn : sortGCNs) {
	            		if(usedGCNs.add(gcn))
	            			last = gcn;
	            	}
		            for(String gcn : displayGCNs) {
	            		if(usedGCNs.add(gcn))
	            			last = gcn;
	            	}
		            displayRetreiver.trackAggregates(results);
		            sortRetreiver.trackAggregates(results);
		            if(i == Folio.GROUPING_LEVEL_ONE) {
		            	inputSource.groupOneColumnNames.addAll(sortGCNs);
		            	inputSource.groupOneColumnNames.addAll(displayGCNs);
		            }
        		}
        	}
        	inputSource.groupColumnNames.put(i, last);
        }
    }

	protected void generateHeader(IS inputSource) throws SAXException {
        startDocument();
        startPrefixMapping(null, NAMESPACE);
        Report report = inputSource.getReport();
        startElement(REPORT_ELEMENT, REPORT_ATTS,
                report.getTitle(inputSource.getParameters()),
                report.getSubtitle(inputSource.getParameters()),
                String.valueOf(inputSource.getOutputType().getOutputTypeId()),
                ConvertUtils.formatObject(new Date(), report.getVerbage("run-date-format", "MESSAGE:Run Date: {,DATE,MMM dd, yyyy h:mm:ss a z}")));
        startElement(VERBAGE_ELEMENT, report.getVerbageMap());
        endElement(VERBAGE_ELEMENT);
        // styles
        Scene scene = inputSource.getScene();
        if(scene != null) {
			startElement(SCENE_ELEMENT, SCENE_ATTS, scene.getBaseUrl(), scene.getUserAgent(), scene.getRunReportAction(), Boolean.toString(scene.isLive()), (scene.getLayout() != null ? scene.getLayout().toString() : ""));
            if(inputSource.getScene().getStylesheets() != null)
                for(String style : inputSource.getScene().getStylesheets()) {
                    if(style != null && style.trim().length() > 0) {
                        startElement(STYLE_ELEMENT, EMPTY_ATTS);
                        characters(style);
                        endElement(STYLE_ELEMENT);
                    }
                }
            if(inputSource.getScene().getScripts() != null)
                for(String script : inputSource.getScene().getScripts()) {
                    if(script != null && script.trim().length() > 0) {
                        startElement(SCRIPT_ELEMENT, EMPTY_ATTS);
                        characters(script);
                        endElement(SCRIPT_ELEMENT);
                    }
                }
            else {
            	startElement(SCRIPT_ELEMENT, EMPTY_ATTS);
                characters("scripts/report.js");
                endElement(SCRIPT_ELEMENT);
            }
            endElement(SCENE_ELEMENT);
        }
		Executor executor = inputSource.getExecutor();
		if(executor != null) {
			startElement(EXECUTOR_ELEMENT, EXECUTOR_ATTS, Long.toString(executor.getUserId()), ConvertUtils.convertSafely(String.class, executor.getUserGroupIds(), null), executor.getDisplayName());
			endElement(EXECUTOR_ELEMENT);
		}
        // parameters
        startElement(PARAMETERS_ELEMENT);
        for(Map.Entry<String,Object> entry : inputSource.getParameters().entrySet()) {
            if("user".equals(entry.getKey())) continue;
            try {
                startElement(PARAMETER_ELEMENT, PARAMETERS_ATTS, entry.getKey(), ConvertUtils.convert(String.class, entry.getValue()));
                endElement(PARAMETER_ELEMENT);
            } catch(ConvertException e) {
                log.warn("Could not convert parameter '" + entry.getKey() + "' to a string: " + entry.getValue());
            }
        }
        endElement(PARAMETERS_ELEMENT);
        // directives
        startElement(DIRECTIVES_ELEMENT);
        for(String name : inputSource.getReport().getDirectiveNames()) {
            startElement(DIRECTIVE_ELEMENT, DIRECTIVE_ATTS, name, inputSource.getReport().getDirective(name));
            endElement(DIRECTIVE_ELEMENT);
        }
        endElement(DIRECTIVES_ELEMENT);
    }
    /*
    protected String getRunDateString(IS inputSource) {
        Locale locale = null;
        if(inputSource.getScene() != null && inputSource.getScene().getLocale() != null) {
            locale = inputSource.getScene().getLocale();
        } else {
            locale = Locale.getDefault();
        }
        return DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG, locale).format(new Date());
    }
    */
    protected boolean shouldProcessPillar(PillarInfo pillarInfo, IS inputSource) {
        return true;
    }

    protected void processPillar(Results results, PillarInfo pillarInfo, AttributesImpl atts, IS inputSource) {
        if(shouldProcessPillar(pillarInfo, inputSource)) {
            //  organize pillars into groups
            int groupingLevel = pillarInfo.pillar.getGroupingLevel();
            inputSource.maxLevel = Math.max(inputSource.maxLevel, groupingLevel);
            inputSource.groups.add(groupingLevel, pillarInfo);
			if(groupingLevel == 0 || (groupingLevel == 2 && pillarInfo.pillar.isAggregated()))
				inputSource.groupTableTotals.add(pillarInfo);
        }
    }

    protected void generateContent(Folio folio, Results results, IS inputSource) throws SAXException {
        Collection<Integer> noTotals;
        Integer ngt[];
        try {
            ngt = ConvertUtils.convert(Integer[].class, folio.getDirective("no-group-totals"));
        } catch(ConvertException e) {
            log.warn("Could not convert '" + folio.getDirective("no-group-totals") + "' to an array of numbers");
            ngt = null;
        }
        if(ngt != null)
        	noTotals = StaticCollection.create(ngt);
        else
            noTotals = Collections.emptySet();
        List<PillarInfo> groupZero = inputSource.groups.get(Folio.GROUPING_LEVEL_ZERO);
        if(groupZero == null)
        	groupZero = Collections.emptyList();
        List<PillarInfo> groupOne = inputSource.groups.get(Folio.GROUPING_LEVEL_ONE);
        if(groupOne == null)
        	groupOne = Collections.emptyList();
        else if(groupZero.isEmpty() || noTotals.contains(1))
        	groupOne.clear();
        XTabHelper xtabHelper;
        if(!groupOne.isEmpty()) {
        	xtabHelper = new XTabHelper(groupOne, groupZero, inputSource.groupOneColumnNames, results);
        } else {
        	xtabHelper = null;
        }
        int detailRowCount = 0;
        boolean groupBeginning = true;
        log.debug("Generating Content for folio " + folio.getFolioId());
        if(results == null) {
            log.debug("No Results were provided");
        } else {
        	while(results.next()) {
	            //if highest (most granular) level of group is beginning
	            if(groupBeginning) {
	                startElement(GROUP_ELEMENT, GROUP_ATTS,  "0");
	            }
	            String groupColumnName = null;
	            for(int level = inputSource.maxLevel; level > Folio.GROUPING_LEVEL_ONE; level--) {
	                List<PillarInfo> group = inputSource.groups.get(level);
	                if(group != null && !group.isEmpty()) {
	                    //calc last column name
	                	groupColumnName = getLastGroupColumnName(level, inputSource);
	                    if(groupBeginning || results.isGroupBeginning(groupColumnName)) {
	                        groupBeginning = true;
	                        startElement(GROUP_ELEMENT, GROUP_ATTS, String.valueOf(level));
	                    	startElement(GROUP_DATA_ELEMENT);
	                        processGroupData(inputSource, group, groupColumnName, level, results);
	                        endElement(GROUP_DATA_ELEMENT);
	                    }
	                }
	            }
	            {
	                if(!groupOne.isEmpty()) {
	                    //calc last column name
	                	groupColumnName = getLastGroupColumnName(1, inputSource);
	                	/*if(groupBeginning || results.isGroupBeginning(groupColumnName))*/ { // this should always be true
	                        groupBeginning = true;
	                        startElement(GROUP_ELEMENT, GROUP_ATTS, "1");
	                        SpecialPlaybackInputSource playback = xtabHelper.getCurrent();
	                        if(playback.getCommandCount() == 0)
								processGroupData(inputSource, groupOne, groupColumnName, 1, results, playback);
	                        startElement(XTAB_ID_ELEMENT);
	                		characters(playback.getId());
	                		endElement(XTAB_ID_ELEMENT);
	                		while(!results.isGroupEnding(groupColumnName)) {
	                        	results.next();
	                        }
	                		startElement(GROUP_TOTALS_ELEMENT);
							processGroupData(inputSource, groupZero, groupColumnName, 1, results);
	                		endElement(GROUP_TOTALS_ELEMENT);
	                        endElement(GROUP_ELEMENT);
	                        xtabHelper.clearCurrent(results);
	                    }
	                }
	            }
	            //check ending
	            boolean checkGroupEnding = true;
	            for(int level = Folio.GROUPING_LEVEL_TWO; level <= inputSource.maxLevel; level++) {
	                List<PillarInfo> group = inputSource.groups.get(level);
	                if(group != null && !group.isEmpty()) {
	                    groupColumnName = getLastGroupColumnName(level, inputSource);
	                    if(results.isGroupEnding(groupColumnName)) {
	                        if(!groupZero.isEmpty() && !noTotals.contains(level)) {
	                        	startElement(GROUP_TOTALS_ELEMENT);
								processGroupTotalsData(level, inputSource, inputSource.groupTableTotals /*groupZero*/, groupColumnName, results);
		                		endElement(GROUP_TOTALS_ELEMENT);
		                        if(level > Folio.GROUPING_LEVEL_TWO && !groupOne.isEmpty()) {
	                            	for(SpecialPlaybackInputSource playback : xtabHelper.groupOneValues.values()) {
	                            	    processXTabTotal(inputSource, groupZero, groupColumnName, results, playback.getId(), playback);
	                            	}
	                            }
	                        }
	                        endElement(GROUP_ELEMENT);
	                        if(level == Folio.GROUPING_LEVEL_TWO)
	                        	detailRowCount++;
	                    } else {
	                        //early exit
	                        checkGroupEnding = false;
	                        break;
	                    }
	                }
	            }
	            if(checkGroupEnding && results.isGroupEnding(0)) {
	                if(!groupZero.isEmpty() && !noTotals.contains(0)) {
	                	startElement(GROUP_TOTALS_ELEMENT);
						processGroupData(inputSource, inputSource.groupTableTotals /*groupZero*/, null, inputSource.maxLevel + 1, results);
                		endElement(GROUP_TOTALS_ELEMENT);
                        if(!groupOne.isEmpty())
                        	for(SpecialPlaybackInputSource playback : xtabHelper.groupOneValues.values()) {
		                	    processXTabTotal(inputSource, groupZero, null, results, playback.getId(), playback);
		                	}
	                }
	                endElement(GROUP_ELEMENT);
	            }
	            groupBeginning = false;
	        }
        }
        //xtab info
        if(!groupOne.isEmpty()) {
        	startElement(XTAB_SUMMARY_ELEMENT);
        	for(SpecialPlaybackInputSource playback : xtabHelper.groupOneValues.values()) {
        		startElement(XTAB_VALUE_ELEMENT);
    			playback.playback(this);
    			startElement(XTAB_ID_ELEMENT);
        		characters(playback.getId());
        		endElement(XTAB_ID_ELEMENT);
        		endElement(XTAB_VALUE_ELEMENT);
    		}
        	endElement(XTAB_SUMMARY_ELEMENT);
        }

        //      size & scrolling info here
        int rows = detailRowCount /*results.getRow()-1*/;
        String total;
        if(folio.getMaxRows() > 0 && results.getRow() > folio.getMaxRows()) {
            total = "?";
        } else {
            total = String.valueOf(rows);
        }
        log.info("Folio " + folio.getFolioId() + " has " + total + " rows");
        startElement(SLICE_ELEMENT, SLICE_ATTS, String.valueOf(1/*startIndex*/), String.valueOf(rows), total);
        characters(ConvertUtils.formatObject(new Object[] {1, rows, total},
                inputSource.getReport().getVerbage("rows-summary-format", "MESSAGE:Showing rows {0} to {1} of {2}")));
        endElement(SLICE_ELEMENT);
    }

	protected void processXTabTotal(IS inputSource, List<PillarInfo> groupZero, String groupColumnName, Results results, String xtabId, ResultsFilter filter) throws SAXException {
        startElement(XTAB_TOTALS_ELEMENT);
        startElement(XTAB_ID_ELEMENT);
		characters(xtabId);
		endElement(XTAB_ID_ELEMENT);
		int groupColumnIndex=results.getColumnIndex(groupColumnName);
        for(PillarInfo pillarInfo : groupZero) {
        	try {
        		Object displayValue = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, filter, groupColumnIndex, inputSource.getParameters());
                Object sortValue = pillarInfo.pillar.getSortValueRetriever().getAggregate(results, filter, groupColumnIndex, inputSource.getParameters());
                Object percentData=null;
                if(pillarInfo.pillar.getPercentFormat()!=null){
            		Object prevDisplayValue = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, null, groupColumnIndex, inputSource.getParameters());
            		percentData =getPercentData(displayValue, prevDisplayValue, pillarInfo.pillar.getPercentFormat());
            	}
                processPillarValues(percentData, inputSource, pillarInfo, displayValue, sortValue, this);
    	    } catch(SAXException e) {
    	        throw e;
    	    } catch(Exception e) {
    	        handleValueRetrievalException(e, results, pillarInfo.pillarId);
    	    }
        }
        endElement(XTAB_TOTALS_ELEMENT);
	}

	protected void generateFooter(IS inputSource) throws SAXException {
        endElement(REPORT_ELEMENT);
        endPrefixMapping(null);
        endDocument(); // NOTE: when "co" async processing is used this ends the thread
    }

    protected void processGroupData(IS inputSource, List<PillarInfo> pillarInfos, String groupColumnName, int level, Results results) throws SAXException {
        processGroupData(inputSource, pillarInfos, groupColumnName, level, results, this);
    }

    protected void processGroupTotalsData(int level, IS inputSource, List<PillarInfo> pillarInfos, String groupColumnName, Results results) throws SAXException {
        processGroupTotalsData(level, inputSource, pillarInfos, groupColumnName, results, this);
    }

    protected void processGroupData(IS inputSource, List<PillarInfo> pillarInfos, String groupColumnName, int level, Results results, ExtendedContentHandler contentHandler) throws SAXException {
        int groupColumnIndex=results.getColumnIndex(groupColumnName);
    	for(PillarInfo pillarInfo : pillarInfos) {
    		try {
                Object displayValue = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, groupColumnIndex, inputSource.getParameters());
                Object sortValue = pillarInfo.pillar.getSortValueRetriever().getAggregate(results, groupColumnIndex, inputSource.getParameters());
				Object percentValue;
				if(pillarInfo.pillar.getPercentFormat() != null) {
					Object prevDisplayValue = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, results.getColumnIndex(getLastGroupColumnName(level + 1, inputSource)), inputSource.getParameters());
					percentValue = getPercentData(displayValue, prevDisplayValue, pillarInfo.pillar.getPercentFormat());
				} else
					percentValue = null;
				processPillarValues(percentValue, inputSource, pillarInfo, displayValue, sortValue, contentHandler);
    	    } catch(SAXException e) {
    	        throw e;
    	    } catch(Exception e) {
    	        handleValueRetrievalException(e, results, pillarInfo.pillarId);
    	    }
        }
    }

	protected Object calcPercent(Object displayValue, Object prevDisplayValue, DecimalFormat format) throws ConvertException {
		if(displayValue == null || prevDisplayValue == null) {
			return null;
		}
		if(prevDisplayValue instanceof Number) {
			BigDecimal prevDisplayValueNum = ConvertUtils.convert(BigDecimal.class, prevDisplayValue);
			if(prevDisplayValueNum != null && !prevDisplayValueNum.equals(BigDecimal.ZERO)) {
				BigDecimal displayValueNum = ConvertUtils.convert(BigDecimal.class, displayValue);
				if(displayValueNum != null) {
					if(format != null)
						return displayValueNum.divide(prevDisplayValueNum, format.getMaximumFractionDigits() + (int) Math.log10(format.getMultiplier()), RoundingMode.HALF_UP);
					return displayValueNum.divide(prevDisplayValueNum, displayValueNum.scale() + prevDisplayValueNum.scale(), RoundingMode.HALF_UP);
				} 
				return null;
			} 
			return null;
		} 
		return null;
	}

	protected Object getPercentData(Object displayValue, Object prevDisplayValue, Format format) throws ConvertException {
		if(displayValue == null || prevDisplayValue == null || !displayValue.getClass().equals(prevDisplayValue.getClass()))
			return null;
		if(prevDisplayValue instanceof MultiValueWithContext) {
			Object[] prevDisplayValueArray = ((MultiValueWithContext) prevDisplayValue).convert(Object[].class);
			Object[] returnVal = new Object[prevDisplayValueArray.length];
			Object[] displayValArray = ((MultiValueWithContext) displayValue).convert(Object[].class);
			DecimalFormat[] formats = new DecimalFormat[prevDisplayValueArray.length];
			if(format instanceof DecimalFormat) {
				for(int i = 0; i < returnVal.length; i++) {
					formats[i] = (DecimalFormat)format;
				}
			} else if(format instanceof MessageFormat) {
				FormatObject[] fos = ((MessageFormat)format).getFormatObjects();
				for(int i = 0; i < returnVal.length && i < fos.length; i++) {
					if(fos[i].format instanceof DecimalFormat)
						formats[i] = (DecimalFormat)fos[i].format;
				}
			} else if(format instanceof java.text.MessageFormat) {
				Format[] fos = ((java.text.MessageFormat)format).getFormats();
				for(int i = 0; i < returnVal.length && i < fos.length; i++) {
					if(fos[i] instanceof DecimalFormat)
						formats[i] = (DecimalFormat)fos[i];
				}
			}
			for(int i = 0; i < returnVal.length; i++) {
				returnVal[i] = calcPercent(displayValArray[i], prevDisplayValueArray[i], formats[i]);
			}
			return returnVal;
		} else if(prevDisplayValue instanceof ValueWithContext) {
			prevDisplayValue = ((ValueWithContext) prevDisplayValue).convert(Object.class);
			displayValue = ((ValueWithContext) displayValue).convert(Object.class);
			DecimalFormat dformat = null;
			if(format instanceof DecimalFormat) {
				dformat = (DecimalFormat)format;
			} else if(format instanceof MessageFormat) {
				FormatObject[] fos = ((MessageFormat)format).getFormatObjects();
				if(fos.length > 0) {
					dformat = (DecimalFormat)fos[0].format;
				}
			} else if(format instanceof java.text.MessageFormat) {
				Format[] fos = ((java.text.MessageFormat)format).getFormats();
				if(fos.length > 0) {
					dformat = (DecimalFormat)fos[0];
				}
			}
			return calcPercent(displayValue, prevDisplayValue, dformat);
		} else {
			log.debug("Invalid percent data. Do not know how to process.");
			return null;
		}
	}
	
    protected void processGroupTotalsData(int level, IS inputSource, List<PillarInfo> pillarInfos, String groupColumnName, Results results, ExtendedContentHandler contentHandler) throws SAXException {
        int groupColumnIndex=results.getColumnIndex(groupColumnName);
    	for(PillarInfo pillarInfo : pillarInfos) {
    		try {
                Object displayValue = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, groupColumnIndex, inputSource.getParameters());
                Object sortValue = pillarInfo.pillar.getSortValueRetriever().getAggregate(results, groupColumnIndex, inputSource.getParameters());
                Object percentData=null;
                if(pillarInfo.pillar.getPercentFormat()!=null){
                	String prevGroupColumnName=getLastGroupColumnName(level+1, inputSource);
            		Object prevDisplayValue = pillarInfo.pillar.getDisplayValueRetriever().getAggregate(results, results.getColumnIndex(prevGroupColumnName), inputSource.getParameters());
            		percentData =getPercentData(displayValue, prevDisplayValue, pillarInfo.pillar.getPercentFormat());
            	}
                processPillarValues(percentData, inputSource, pillarInfo, displayValue, sortValue, contentHandler);
    	    } catch(SAXException e) {
    	        throw e;
    	    } catch(Exception e) {
    	        handleValueRetrievalException(e, results, pillarInfo.pillarId);
    	    }
        }
    }

    protected void handleValueRetrievalException(Exception e, Results results, String pillarId) throws SAXException {
    	String values = "";
        for (int x = 0; x < results.getColumnCount(); x++) {
            if (x > 0)
                values += ",";
            try {
                values += results.getValue(x + 1);
            } catch (Exception e0) {
                values = "<Couldn't retrieve value at column " + (x+1) + ">";
            }
        }
        String msg = "While outputing pillar " + pillarId + " at row " + results.getRow() + " with values=[" + values + "]";
        log.warn(msg, e);
        throw new SAXException(msg, e);
    }

    protected void processPillarValues(Object percentData, IS inputSource, PillarInfo pillarInfo, Object displayValue, Object sortValue, ExtendedContentHandler handler) throws SAXException {
    	String display = ConvertUtils.formatObject(displayValue, pillarInfo.pillar.getDisplayFormat());

    	switch(pillarInfo.pillar.getPillarType()) {
            case NOTE: inputSource.noteValues.add(display); break;
            case IMAGE: inputSource.imageValues.add(display); break;
        }
    	AttributesImpl atts = new AttributesImpl();
        if(pillarInfo.pillar.getActionFormat() != null)
			atts.addAttribute(NAMESPACE, ACTION_ATT, ACTION_ATT, "CDATA", adjustAction(ConvertUtils.formatObject(displayValue, pillarInfo.pillar.getActionFormat()), inputSource.getScene()));
        String sortText;
        if(pillarInfo.pillar.getSortType() == DataGenre.DATE) {
        	try {
        		sortText = getDateSortText(sortValue, pillarInfo.pillar.getSortFormat());
			} catch(ConvertException e) {
				log.warn("Could not convert sort text to number of milleseconds for date sorting", e);
				sortText = null;
			}
        } else {
        	sortText = ConvertUtils.formatObject(sortValue, pillarInfo.pillar.getSortFormat());
        }
    	atts.addAttribute(NAMESPACE, SORT_VALUE_ATT, SORT_VALUE_ATT, "CDATA", sortText);
        if(pillarInfo.pillar.getHelpFormat() != null)
        	atts.addAttribute(NAMESPACE, HELP_ATT, HELP_ATT, "CDATA", ConvertUtils.formatObject(displayValue, pillarInfo.pillar.getHelpFormat()));
        atts.addAttribute(NAMESPACE, VALUE_ATT, VALUE_ATT, "CDATA", ConvertUtils.formatObject(displayValue, (Format)null));
        if(pillarInfo.pillar.getStyleFormat() != null)
        	atts.addAttribute(NAMESPACE, STYLE_ATT, STYLE_ATT, "CDATA", ConvertUtils.formatObject(displayValue, pillarInfo.pillar.getStyleFormat()));
        atts.addAttribute(NAMESPACE, LABEL_ATT, LABEL_ATT, "CDATA", ConvertUtils.formatObject(displayValue, pillarInfo.pillar.getLabelFormat()));
        atts.addAttribute(NAMESPACE, PILLAR_ID_ATT, PILLAR_ID_ATT, "CDATA", pillarInfo.pillarId);
        if(percentData!=null&&pillarInfo.pillar.getPercentFormat()!=null){
    		String percentValStr=ConvertUtils.formatObject(percentData,pillarInfo.pillar.getPercentFormat());
    		atts.addAttribute(NAMESPACE, PERCENT_ATT, PERCENT_ATT, "CDATA", percentValStr);
    	}
        handler.startElement(pillarInfo.typeName, atts);
		handler.characters(display);
		handler.endElement(pillarInfo.typeName);
    }

	protected static final Pattern LAYOUT_PATTERN = Pattern.compile("(^|[&])(unframed|fragment)=");

	protected String adjustAction(String action, Scene scene) {
		if(action == null || (action = action.trim()).length() == 0)
			return action;
		Layout layout = scene.getLayout();
		if(layout != null && layout != Layout.FULL) {
			URL url;
			try {
				url = new URL(new URL(scene.getBaseUrl()), action);
			} catch(MalformedURLException e) {
				return action;
			}
			if(url.getProtocol() == null || url.getProtocol().equalsIgnoreCase("http") || url.getProtocol().equalsIgnoreCase("https")) {
				String query = url.getQuery();
				if(query == null || (query = query.trim()).length() == 0 || !LAYOUT_PATTERN.matcher(query).find()) {
					int pos = action.lastIndexOf('?');
					if(pos < 0)
						action += '?' + layout.toString() + "=true";
					else if(pos == action.length() - 1)
						action += layout + "=true";
					else
						action += '&' + layout.toString() + "=true";
				}
			}
		}
		return action;
	}

	protected String getDateSortText(Object sortValue, Format sortFormat) throws ConvertException {
    	if(sortValue instanceof Date)
    		return String.valueOf(((Date)sortValue).getTime());
    	else if(sortValue instanceof Calendar)
    		return String.valueOf(((Calendar)sortValue).getTimeInMillis());
    	else if(sortValue instanceof Number) {
    		return String.valueOf(sortValue);
    	} else {
        	String sortText = ConvertUtils.formatObject(sortValue, sortFormat);
        	if(sortText != null)
            	try {
            		Date sortDate = ConvertUtils.convert(Date.class, sortText);
            		if(sortDate != null)
            			return String.valueOf(sortDate.getTime());
				} catch(ConvertException e) {
					if(sortValue instanceof Object[]) {
						for(Object sv : (Object[])sortValue) {
							try {
								sortText = getDateSortText(sv, null);
								if(sortText != null)
									return sortText;
							} catch(ConvertException e1) {
							}
						}
					}
					throw e;
				}
			return null;
    	}

    }
    protected String getLastGroupColumnName(int level, IS inputSource) {
    	return inputSource.groupColumnNames.get(level);
    }

    /**
     * @param pillarType
     * @return
     */
    protected String getPillarTypeString(Pillar pillar) {
        switch(pillar.getPillarType()) {
        	case IMAGE: return "image";
        	case LINK: return "text";
        	case NOTE: return "note";
            case ROW: return "all";
            case CHECKBOX: return "checkbox";
            case BUTTON: return "button";
            case RADIO: return "radio";
            default: return "text";
        }
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }
}
