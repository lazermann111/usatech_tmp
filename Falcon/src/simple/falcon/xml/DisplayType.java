package simple.falcon.xml;

public enum DisplayType {
	INLINE_IMAGE, INLINE_APPLET, REFERENCE, INLINE_DIVS, INLINE_REQUEST, MHTML, UNKNOWN, INLINE_OBJECT;
    @Override
	public String toString() {
        return super.toString().replace('_', '-').toLowerCase();
    }
    public static DisplayType getByValue(String value) {
    	for(DisplayType d : DisplayType.values()) {
            if(d.toString().equalsIgnoreCase(value))
            	return d;
        }
    	return null;
    }
}