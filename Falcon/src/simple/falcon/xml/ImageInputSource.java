/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.xml;

import java.awt.image.BufferedImage;

import org.xml.sax.InputSource;

/**
 * @author bkrug
 *
 */
public class ImageInputSource extends InputSource {
    protected BufferedImage image;
    
    public ImageInputSource(BufferedImage image) {
        this.image = image;
    }
    public BufferedImage getImage() {
        return image;
    }
    public void setImage(BufferedImage image) {
        this.image = image;
    }
}
