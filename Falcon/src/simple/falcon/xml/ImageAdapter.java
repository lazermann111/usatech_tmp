/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.xml;


import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.util.Arrays;
import java.util.BitSet;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.io.Log;
import simple.text.StringUtils;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class ImageAdapter extends AbstractXMLReader<ImageInputSource> {
    private static final Log log = Log.getLog();
    public static final String NAMESPACE = "http://simple/falcon/image/1.0";
    public static final String IMAGE_ELEMENT = "image";
    public static final String RECTANGLE_ELEMENT = "rect";
    public static final String COLOR_ATT = "color";
    public static final String X_ATT = "x";
    public static final String Y_ATT = "y";
    public static final String WIDTH_ATT = "width";
    public static final String HEIGHT_ATT = "height";
    protected static enum Direction {NONE, X, Y, BOTH};
    /**
     * @throws org.xml.sax.SAXException
     */
    public ImageAdapter() {
    }
   
    public void generateEvents(ImageInputSource inputSource) throws SAXException {
        BufferedImage image = inputSource.getImage();
        startDocument();
        startPrefixMapping(null, NAMESPACE);
        int width = image.getWidth();
        int height = image.getHeight();
        {
            AttributesImpl atts = new AttributesImpl();
            atts.addAttribute(NAMESPACE, WIDTH_ATT, WIDTH_ATT, "CDATA", "" + width);       
            atts.addAttribute(NAMESPACE, HEIGHT_ATT, HEIGHT_ATT, "CDATA", "" + height);       
            startElement(NAMESPACE, IMAGE_ELEMENT, IMAGE_ELEMENT, atts);
            characters("\n");
        }
        ColorModel cm = image.getColorModel();
        //int transferType = cm.getTransferType();
        Raster raster = image.getRaster();
        BitSet pixelsSet = new BitSet(width * height);
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; ) {
                if(pixelsSet.get((x * height) + y)) {
                    y++;
                    continue;
                }
                Object colorArray = raster.getDataElements(x, y, null);
                int color = cm.getRGB(colorArray);
                int h = 1;
                int w = 1;
                Direction dir = Direction.BOTH;
                while(dir != Direction.NONE) {
                    DIR_Y: switch(dir) {
                        case Y: case BOTH:
                            if(h + y >= height || pixelsSet.get((x * height) + y + h)) {
                                if(dir == Direction.BOTH) dir  =  Direction.X;
                                else dir = Direction.NONE;
                                break DIR_Y;
                            }
                            for(int i = 0; i < w; i++) {
                                Object otherColorArray = raster.getDataElements(i + x, h + y, null);
                                int otherColor = cm.getRGB(otherColorArray);
                                if(color != otherColor) {
                                    if(dir == Direction.BOTH) dir  =  Direction.X;
                                    else dir = Direction.NONE;
                                    break DIR_Y;
                                }
                            }
                            //success
                            h++;
                    }
                    DIR_X: switch(dir) {
                        case X: case BOTH:
                            if(w + x >= width) { // don't think the following will ever return true: || pixelsSet.get(((x + w) * height) + y)) {
                                if(dir == Direction.BOTH) dir  =  Direction.Y;
                                else dir = Direction.NONE;
                                break DIR_X;
                            }
                            for(int i = 0; i < h; i++) {
                                Object otherColorArray = raster.getDataElements(w + x, i + y, null);
                                int otherColor = cm.getRGB(otherColorArray);
                                if(color != otherColor) {
                                    if(dir == Direction.BOTH) dir =  Direction.Y;
                                    else dir = Direction.NONE;
                                    break DIR_X;
                                }
                            }
                            // success
                            w++;
                    }                           
                }
                
                //mark any pixels taken care of that are further along (their x > current x)
                for(int i = 1; i < w; i++) {
                    int p = ((x + i) * height) + y;
                    pixelsSet.set(p, p + h);
                }    
                //create element
                writeRect(x, y, w, h, color);
                y += h;
            }
        }
        
        endElement(NAMESPACE, IMAGE_ELEMENT, IMAGE_ELEMENT); 
        endPrefixMapping(null);
        endDocument();
    }

    protected void writeRect(int x, int y, int w, int h, int color) throws SAXException {
        AttributesImpl atts = new AttributesImpl();
        atts.addAttribute(NAMESPACE, X_ATT, X_ATT, "CDATA", "" + x);       
        atts.addAttribute(NAMESPACE, Y_ATT, Y_ATT, "CDATA", "" + y);       
        atts.addAttribute(NAMESPACE, WIDTH_ATT, WIDTH_ATT, "CDATA", "" + w);       
        atts.addAttribute(NAMESPACE, HEIGHT_ATT, HEIGHT_ATT, "CDATA", "" + h);       
        //atts.addAttribute(NAMESPACE, COLOR_ATT, COLOR_ATT, "CDATA", "rgba(" + red + "," + green + "," + blue + "," + alpha + ")");       
        atts.addAttribute(NAMESPACE, COLOR_ATT, COLOR_ATT, "CDATA", "#" + StringUtils.pad(Integer.toHexString(color & 0xFFFFFF), "0", 6, StringUtils.JUSTIFY_LEFT).toUpperCase());       
        startElement(NAMESPACE, RECTANGLE_ELEMENT, RECTANGLE_ELEMENT, atts);             
        endElement(NAMESPACE, RECTANGLE_ELEMENT, RECTANGLE_ELEMENT);
        characters("\n");        
    }
    
    protected Class<?> getColorArrayClass(int transferType) {
        switch(transferType) {
            case DataBuffer.TYPE_BYTE: return byte[].class;
            case DataBuffer.TYPE_DOUBLE: return double[].class;
            case DataBuffer.TYPE_FLOAT: return float[].class;
            case DataBuffer.TYPE_INT: return int[].class;
            case DataBuffer.TYPE_SHORT: return short[].class;
            case DataBuffer.TYPE_USHORT: return short[].class;
            case DataBuffer.TYPE_UNDEFINED: default: return Object.class;        
        }
    }
    
    protected boolean areArraysEqual(Object a1, Object a2, int transferType) {
        switch(transferType) {
            case DataBuffer.TYPE_BYTE: return Arrays.equals((byte[])a1, (byte[])a2);
            case DataBuffer.TYPE_DOUBLE: return Arrays.equals((double[])a1, (double[])a2);
            case DataBuffer.TYPE_FLOAT: return Arrays.equals((float[])a1, (float[])a2);
            case DataBuffer.TYPE_INT: return Arrays.equals((int[])a1, (int[])a2);
            case DataBuffer.TYPE_SHORT: return Arrays.equals((short[])a1, (short[])a2);
            case DataBuffer.TYPE_USHORT: return Arrays.equals((short[])a1, (short[])a2);
            case DataBuffer.TYPE_UNDEFINED: default: 
                log.warn("Could not find specific equals for transferType " + transferType);
                return a1.equals(a2);        
        }       
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }
}
