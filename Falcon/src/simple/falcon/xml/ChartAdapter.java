/*
 * Created on Jan 14, 2005
 *
 */
package simple.falcon.xml;


import java.awt.Dimension;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.batik.util.Base64EncoderStream;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.chart.ChartCreationException;
import simple.chart.ChartFactory;
import simple.chart.ChartType;
import simple.chart.ChartType.Axis;
import simple.chart.ChartUtils;
import simple.chart.GenericChartType.GenericAxis;
import simple.falcon.engine.ColumnNamer;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Scene;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.xml.ReportInputSource.PillarInfo;
import simple.io.Log;
import simple.results.MultiColumnResultsReader;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.results.ResultsReader;
import simple.results.SingleColumnResultsReader;
import simple.sql.SQLTypeUtils;
import simple.text.LiteralFormat;
import simple.text.StringUtils;

/**
 * @author bkrug
 *
 */
public class ChartAdapter extends ReportAdapter<ChartInputSource> {
    private static final Log log = Log.getLog();
    public static final String CHART_ELEMENT = "chart";
    public static final String DEFAULT_CHART_TYPE = "BarChart3D";
    protected ImageAdapter imageAdapter;
    /**
     * @throws org.xml.sax.SAXException
     */
    public ChartAdapter() {
        imageAdapter = new ImageAdapter() {
            @Override
            public void endDocument() throws SAXException {
                //ignore
            }

            @Override
            public void startDocument() throws SAXException {
                //ignore
            }
        };
        imageAdapter.setContentHandler(this);
    }

    @Override
    public void processFolio(Folio folio, Results results, ChartInputSource inputSource) throws SAXException {
        // determine chartType
        String chartType = folio.getChartType();
        if(chartType == null) chartType = DEFAULT_CHART_TYPE;
        ChartType ct = ChartFactory.getChartType(chartType);
        inputSource.chartMaker = ct.newChartMaker();
        String config = folio.getDirective("chart-config-settings");
        if(config != null) {
        	Map<String,Object> props = new HashMap<String, Object>();
        	try {
				StringUtils.intoMap(props, config);
			} catch(ParseException e) {
				throw new SAXException(e);
			}
        	for(Map.Entry<String, Object> entry : props.entrySet()) {
        		inputSource.chartMaker.setProperty(entry.getKey(), entry.getValue());
        	}
        }
        List<String> clusters = new ArrayList<String>();
        List<String> values = new ArrayList<String>();
        for(Axis axis : ct.getAxes()) {
            if(axis instanceof GenericAxis) {
                GenericAxis ga = (GenericAxis)axis;
                switch(ga.getGenericAxisType()) {
                    case CLUSTER_LEGEND:
                    case CLUSTER:
                        clusters.add(axis.getName());
                        break;
                    case VALUE_LEGEND:
                    case VALUE:
                        values.add(axis.getName());
                        break;
                }
            }
        }
        inputSource.clusterAxisNames = clusters.iterator();
        inputSource.valueAxisNames = values.iterator();
        inputSource.dynamicLabelClusters = new HashMap<String, Pillar>();
        super.processFolio(folio, results, inputSource);
    }

    @Override
    protected boolean shouldProcessPillar(PillarInfo pillarInfo, ChartInputSource inputSource) {
    	switch(pillarInfo.pillar.getPillarType()) {
            case CLUSTER:
                if(inputSource.clusterAxisNames.hasNext()) {
                	String axisName = inputSource.clusterAxisNames.next();
                	String label;
                	if(pillarInfo.pillar.getLabelFormat() instanceof LiteralFormat) {
                		label = pillarInfo.pillar.getLabelFormat().format(null);
                	} else if(pillarInfo.pillar.getLabelFormat() == null) {
                		label = "";
                	} else {
                		label = null;
                		inputSource.dynamicLabelClusters.put(axisName, pillarInfo.pillar);
                	}
                    inputSource.chartMaker.setAxisData(axisName, label, toReader(pillarInfo.pillar, inputSource.getNamer()), pillarInfo.pillar.getDisplayFormat());
                }
                return false;
            case VALUE:
                // determine which field order to use (first one that is a number)
                FieldOrder fo = getFirstNumberField(pillarInfo.pillar.getFieldOrders());
                if(fo != null && inputSource.valueAxisNames.hasNext())
                    inputSource.chartMaker.setAxisData(inputSource.valueAxisNames.next(),  ConvertUtils.formatObject(null, pillarInfo.pillar.getLabelFormat()),
                            new SingleColumnResultsReader(inputSource.getNamer().getDisplayColumnName(fo), inputSource.getNamer().getSortColumnName(fo), pillarInfo.pillar.getDisplayFormat(), fo.getAggregateType()),
                            pillarInfo.pillar.getDisplayFormat());
                return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see simple.falcon.xml.ReportAdapter#generateContent(simple.falcon.engine.Folio, simple.results.Results)
     */
    @Override
    protected void generateContent(Folio folio, Results results, ChartInputSource inputSource) throws SAXException {
        Results subResults;
        DisplayType displayType = getDisplayType(inputSource.getParameters(), inputSource.getScene());
        int groupBeginningLevel = 0;
        int chartCount = 0;
        String groupColumnName = getLastGroupColumnName(Folio.GROUPING_LEVEL_THREE, inputSource);
        if(results != null) while((subResults = results.nextSubResult(groupColumnName)) != null) {
            //if highest (most granular) level of group is beginning
            if(groupBeginningLevel == 0) {
                startElement(GROUP_ELEMENT, GROUP_ATTS, "" + 0);
                groupBeginningLevel = inputSource.maxLevel;
            }
            for(int level = groupBeginningLevel; level >= Folio.GROUPING_LEVEL_THREE; level--) {
                List<PillarInfo> group = inputSource.groups.get(level);
                if(group != null) {
                    startElement(GROUP_ELEMENT, GROUP_ATTS, "" + level);
                    startElement(GROUP_DATA_ELEMENT, EMPTY_ATTS);
                    processGroupData(inputSource, inputSource.groups.get(level), getLastGroupColumnName(level, inputSource), level, results);
                    endElement(GROUP_DATA_ELEMENT);
                }
            }
            try {
            	for(Map.Entry<String,Pillar> entry : inputSource.dynamicLabelClusters.entrySet()) {
            		Pillar pillar = entry.getValue();
            		String label = pillar.getLabelFormat().format(pillar.getDisplayValueRetriever().getAggregate(results, 0, inputSource.getParameters()));
            		inputSource.chartMaker.getAxisData(entry.getKey()).setLabel(label);
            	}

                JFreeChart chart = inputSource.chartMaker.makeChart(subResults);
                //XXX: Format value axes???,
                //Determine size
                Map<String,Object> drawSettings = new HashMap<String,Object>();
                Dimension size = ChartUtils.calculateSize(chart, drawSettings);
                // XXX: other configuration ???
                AttributesImpl atts = new AttributesImpl();
                atts.addAttribute(NAMESPACE, "content-type", "content-type", "CDATA", "image/png");
                atts.addAttribute(NAMESPACE, "width", "width", "CDATA", "" + size.width);
                atts.addAttribute(NAMESPACE, "height", "height", "CDATA", "" + size.height);
                atts.addAttribute(NAMESPACE, "display-type", "display-type", "CDATA", displayType.toString());
                // Two ways to do this: output to byteoutputstream and store in session or put on xml
                OutputStream out;
                //long refId;
                switch(displayType) {
                    case INLINE_IMAGE: case INLINE_APPLET:  case INLINE_REQUEST: default:
                        out = new Base64EncoderStream(new OutputStream() {
                            protected final char[] cb = new char[1];
                            @Override
                            public void write(int b) throws IOException {
                                cb[0] = (char)b; //this decoding should be sufficient
                                try {
                                    characters(cb, 0, 1);
                                } catch(SAXException e) {
                                    IOException ioe = new IOException("SAXException Occurred");
                                    e.initCause(e);
                                    throw ioe;
                                }
                                //char[] ch = new String(b).toCharArray();
                            }
                        });
                        break;
                    case INLINE_DIVS:
                        out = null;
                        break;
                    /*case REFERENCE:
                        final File file = File.createTempFile("chart-", ".png");
                        out = new FileOutputStream(file);
                        refId = chartCache.putObject(file);
                        atts.addAttribute(NAMESPACE, "reference-id", "reference-id", "CDATA", "" + refId);
                        break;*/
                }
                startElement(CHART_ELEMENT, atts);
                long start = System.currentTimeMillis();
                if(out != null) {
                    ChartUtilities.writeChartAsPNG(out, chart, size.width, size.height);
                    out.close();
                } else {
                    imageAdapter.generateEvents(new ImageInputSource(chart.createBufferedImage(size.width, size.height)));
                }
                log.debug("Wrote chart in " + (System.currentTimeMillis() - start) + " milliseconds");
            } catch(ChartCreationException e) {
                log.warn("Could not create chart", e);
                throw new SAXException(e);
            } catch (IllegalArgumentException e) {
                log.warn("Could not create chart", e);
                throw new SAXException(e);
            } catch (IOException e) {
                log.warn("Could not create chart", e);
                throw new SAXException(e);
            }
            chartCount++;
            // create chart with subResults
            endElement(CHART_ELEMENT);
            //--
            // check ending
            boolean checkGroupEnding = true;
            groupBeginningLevel = 1;
            for(int level = Folio.GROUPING_LEVEL_THREE; level <= inputSource.maxLevel; level++) {
                List<PillarInfo> group = inputSource.groups.get(level);
                if(group != null) {
                    String gcn = getLastGroupColumnName(level, inputSource);
                    if(results.isGroupEnding(gcn)) {
                        endElement(GROUP_ELEMENT);
                        groupBeginningLevel = level;
                    } else {
                        //early exit
                        checkGroupEnding = false;
                        break;
                    }
                }
            }
            if(checkGroupEnding && results.isGroupEnding(0)) {
                endElement(GROUP_ELEMENT);
            }
        }
        // size & scrolling info here
        final String[] sliceAtts = new String[] {OFFSET_ATT, ROWS_SHOWN_ATT, TOTAL_COUNT_ATT};
        String total;
        if(folio.getMaxRows() > 0 && results.getRow() > folio.getMaxRows()) {
            total = "?";
        } else {
            total = "" + (results.getRow()-1);
        }
        log.info("Folio " + folio.getFolioId() + " has " + total + " rows");
        startElement(SLICE_ELEMENT, sliceAtts, "" + 1/*startIndex*/, "" + (results.getRow()-1), total);
        characters(ConvertUtils.formatObject(new Object[] {chartCount}, inputSource.getReport().getVerbage("charts-summary-format","MESSAGE:Created {0} chart{0,CHOICE,0#s|1#|2#s}")));
        endElement(SLICE_ELEMENT);
    }


    protected ResultsReader toReader(Pillar pillar, ColumnNamer namer) {
        FieldOrder[] fos = pillar.getFieldOrders();
        if(fos.length == 1) {
            return new SingleColumnResultsReader(namer.getDisplayColumnName(fos[0]), namer.getSortColumnName(fos[0]), pillar.getDisplayFormat(), fos[0].getAggregateType());
        }
		String[] displayColumns = new String[fos.length];
		String[] sortColumns = new String[fos.length];
		Aggregate[] aggregates = new Aggregate[fos.length];
		for(int i = 0; i < fos.length; i++) {
			displayColumns[i] = namer.getDisplayColumnName(fos[i]);
			sortColumns[i] = namer.getSortColumnName(fos[i]);
			aggregates[i] = fos[i].getAggregateType();
		}
		return new MultiColumnResultsReader(displayColumns, sortColumns, pillar.getDisplayFormat(), aggregates);
    }

    protected FieldOrder getFirstNumberField(FieldOrder[] fos) {
        for(int k = 0; k < fos.length; k++) {
            if(isNumberField(fos[k].getField()))
                return fos[k];
        }
        return null;
    }

    protected boolean isNumberField(Field f) {
        Class<?> cl = SQLTypeUtils.getJavaType(f.getDisplaySqlType().getTypeCode());
        cl = ConvertUtils.convertToWrapperClass(cl);
        return Number.class.isAssignableFrom(cl);
    }

    /**
     * @see simple.falcon.xml.ReportAdapter#getPillarTypeString(int)
     */
    @Override
    protected String getPillarTypeString(Pillar pillar) {
        switch(pillar.getPillarType()) {
            case CLUSTER: return "axis";
            case VALUE: return "value";
            default:
                if(pillar.getGroupingLevel() < Folio.GROUPING_LEVEL_ONE) return "hidden";
				return super.getPillarTypeString(pillar);
        }
    }

    protected DisplayType getDisplayType(Map<String,Object> parameters, Scene scene) {
        try {
            String dis = ConvertUtils.getString(parameters.get("displayType"), false);
            if(dis != null)
                for(DisplayType d : DisplayType.values()) {
                    if(d.toString().equalsIgnoreCase(dis)) return d;
                }
        } catch(ConvertException e) {
        }
        log.debug("User Agent='" + scene.getUserAgent() + "'");
        if(scene.getUserAgent() != null && EngineUtils.isDataUriSupported(scene.getUserAgent())) {
            return DisplayType.INLINE_IMAGE;
        }
		return DisplayType.INLINE_REQUEST;
    }
}
