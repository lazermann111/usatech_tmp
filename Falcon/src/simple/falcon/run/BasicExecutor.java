package simple.falcon.run;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import simple.falcon.engine.Executor;

public class BasicExecutor implements Executor {
    protected long[] userGroupIds;
    protected long userId;
    protected Map<String, String> userProperties = new HashMap<String, String>();
    protected TimeZone timeZone;
	protected String displayName;
	
    /**
     * @return Returns the userGroupIds.
     */
    public long[] getUserGroupIds() {
        return userGroupIds;
    }
    /**
     * @param userGroupIds The userGroupIds to set.
     */
    public void setUserGroupIds(long[] userGroupIds) {
        this.userGroupIds = userGroupIds;
    }
    /**
     * @return Returns the userId.
     */
    public long getUserId() {
        return userId;
    }
    /**
     * @param userId The userId to set.
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return Returns the userProperties.
     */
    public Map<String, String> getUserProperties() {
        return userProperties;
    }
	public TimeZone getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
