/*
 * Created on Jan 18, 2005
 *
 */
package simple.falcon.run;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.bean.ReflectionUtils;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Filter;
import simple.falcon.engine.FilterGroup;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Param;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Report;
import simple.falcon.engine.SortOrder;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterGroup;
import simple.falcon.engine.standard.StandardFilterItem;
import simple.results.DataGenre;
import simple.results.Results;
import simple.text.StringUtils;
import simple.xml.XMLBuilder;

/**
 * @author bkrug
 *
 */
public class FolioUpdateUtils {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected static void findFilterParams(Filter filter, Map<Long,Param[]> params) {
        if(filter instanceof FilterGroup) {
            FilterGroup fg = (FilterGroup)filter;
            for(Filter f : fg.getChildFilters())
                findFilterParams(f, params);
        } else if(filter instanceof StandardFilterItem) {
            StandardFilterItem sfi = (StandardFilterItem)filter;
			params.put(sfi.getFilterId(), sfi.getOperatorParams());
        }
    }
    public static StandardFilterGroup readFilter(DynaBean form, StandardDesignEngine designEngine, Filter oldFilter) throws DesignException, ConvertException {
        Map<String,FilterGroup> groups = new HashMap<String,FilterGroup>();
        Map<Long,Param[]> oldParamsMap = new HashMap<Long,Param[]>();
        if(oldFilter != null) {
            findFilterParams(oldFilter, oldParamsMap);
        }
        int maxGroupIndex = ConvertUtils.getInt(form.get("filter_group_max_index"), -1);
        int maxIndex = ConvertUtils.getInt(form.get("filter_max_index"), -1);
        log.debug("Reading " + (maxIndex == -1 ? "?" : "" + maxIndex) + " filters in "  + (maxGroupIndex == -1 ? "?" : "" + maxGroupIndex) + " groups");

        StandardFilterGroup fg = new StandardFilterGroup();
        for(int i = 1; i <= maxGroupIndex; i++) {
        	String label = ConvertUtils.getString(form.get("filter_group_label_" + i), null);
        	if(label != null){
                StandardFilterGroup tempFG;
                int lastDot = label.lastIndexOf('.');
	        	if(lastDot == -1){
                    tempFG = fg;
	        	} else {
	        		String parent = label.substring(0,lastDot);
                    FilterGroup parentFG = groups.get(parent);
                    if(parentFG == null) {
                        throw new DesignException("Parent filter group '" + parent + "' not found");
                    }
					tempFG = new StandardFilterGroup();
					parentFG.addFilter(tempFG);
	        	}
                tempFG.setSeparator(ConvertUtils.getString(form.get("filter_group_separator_" + i),true));
                try {
                    tempFG.setFilterGroupId(ConvertUtils.convert(Long.class, form.get("filter_group_id_" + i)));
                } catch(ConvertException e) {
                    log.warn("Could not convert filter_group_id_" + i + " to Long", e);
                }

	        	groups.put(label,tempFG);
        	}
        }
        for(int i = 1; maxIndex == -1 || i <= maxIndex; i++) {
            long fieldId = ConvertUtils.getLong(form.get("filter_field_" + i), 0);
            if(fieldId != 0) {
                log.debug("Adding filter for field " + fieldId);
                StandardFilterItem filter = new StandardFilterItem(designEngine);
                try {
                    filter.setFilterId(ConvertUtils.convert(Long.class, form.get("filter_id_" + i)));
                } catch(ConvertException e) {
                    log.warn("Could not convert filter_id_" + i + " to Long", e);
                }
                filter.setField(designEngine.getField(fieldId));
                filter.setOperatorById(ConvertUtils.getInt(form.get("filter_op_" + i)));
				if(filter.getFilterId() != null) {
					Param[] oldParams = oldParamsMap.get(filter.getFilterId());
					if(oldParams != null)
						filter.setOperatorParams(oldParams);
				}
                String[] paramNames = ConvertUtils.convert(String[].class, form.get("filter_param_name_" + i));
				String[] paramValues = ConvertUtils.convert(String[].class, form.get("filter_val_" + i));
				Param[] params = filter.getOperatorParams();
            	Object rawParamValues = form.get("filter_param_value_" + i);
				if(rawParamValues != null) {
					if(rawParamValues instanceof String[]) {
						String[] paramValues2 = (String[]) rawParamValues;
						if(paramValues == null)
							paramValues = paramValues2;
						else if(paramValues2.length > paramValues.length) {
							for(int k = 0; k < paramValues.length; k++)
								if(paramValues[k] != null)
									paramValues2[k] = paramValues[k];
							paramValues = paramValues2;
						} else {
							for(int k = 0; k < paramValues2.length; k++)
								if(paramValues[k] == null)
									paramValues[k] = paramValues2[k];
						}
                	} else {
						String value = (String) rawParamValues;
						if(paramValues == null || paramValues.length == 0)
							paramValues = new String[] { value };
						else if(paramValues[0] == null)
							paramValues[0] = value;
                	}
				}

				for(int k = 0; k < params.length; k++) {
					if(paramNames != null && paramNames.length > k && !StringUtils.isBlank(paramNames[k]))
						params[k].setName(paramNames[k]);
					if(paramValues != null && paramValues.length > k)
						params[k].setValue(paramValues[k]);
				}

				String groupIndex = ConvertUtils.getString(form.get("filter_group_" + i), null);
                if(groupIndex == null || groupIndex.trim().length() == 0) fg.addFilter(filter);
                else groups.get(groupIndex).addFilter(filter);
            } else if(maxIndex == -1) {
                break;
            }
        }

        try {
        	log.debug("Filter =\n" + XMLBuilder.toXML(fg));
        } catch(Exception e) {
        	log.debug("Couldn't xmlize the filter group", e);
        }
        return fg;
    }

	/*
	public static Param[] getUpdatedFilterParams(StandardDesignEngine designEngine, Param[] oldParams, String[] paramNames, String[] paramValues, SQLType[] paramTypes){
	    log.debug("Creating " + paramTypes.length + " params to replace " + (oldParams != null ? oldParams.length : "no") + "params");
	    Param[] newParams = new Param[paramTypes.length];

	    if(oldParams != null){
	    	for(int j = 0; j < Math.min(newParams.length,oldParams.length); j++){
	    		newParams[j] = oldParams[j];
	    	}
	    }

		for(int j = 0; j < newParams.length; j++){
			if(newParams[j] == null) {
	            newParams[j] = new StandardParam(designEngine);
	            log.debug("Creating new param at index " + j);
	        } else {
	            log.debug("Using old param at index " + j + " with editor of '" + oldParams[j].getEditor() + "'");
	        }
			if(log.isDebugEnabled())
				log.debug("Setting param at index " + j + " to type " + paramTypes[j]);
			newParams[j].setSqlType(paramTypes[j]);

			if(paramNames != null && j < paramNames.length && paramNames[j] != null) {
				newParams[j].setName(paramNames[j]);
				newParams[j].setLabel(paramNames[j]);
				if(newParams[j].getPrompt() == null)
					newParams[j].setPrompt("Enter the value for " + paramNames[j]);
			}

			if(paramValues != null && j < paramValues.length && paramValues[j] != null) {
				newParams[j].setValue(paramValues[j]);
			}
		}

	    return newParams;
	}
	*/
    public static void addFilter(Folio folio, Filter filter, String separator) {
        Filter orig = folio.getFilter();
        if(orig instanceof FilterGroup) {
            FilterGroup fg = (FilterGroup) orig;
            if(fg.getSeparator().equalsIgnoreCase(separator)) {
                fg.addFilter(filter);
                return;
            } else if(fg.filterCount() == 0) {
                fg.setSeparator(separator);
                fg.addFilter(filter);
                return;
            }
        } else if(orig == null) {
            folio.setFilter(filter);
            return;
        }
        StandardFilterGroup fgNew = new StandardFilterGroup();
        fgNew.setSeparator(separator);
        fgNew.addFilter(filter);
        fgNew.addFilter(orig);
        folio.setFilter(fgNew);
    }

    public static void addFilters(Folio folio, DynaBean form, StandardDesignEngine designEngine) throws ConvertException, DesignException {
        FilterGroup fg = readFilter(form, designEngine, null);
        if(fg != null && fg.filterCount() > 0) addFilter(folio, fg, "AND");
    }

    public static void replaceFilters(Folio folio, DynaBean form, StandardDesignEngine designEngine) throws ConvertException, DesignException {
        StandardFilterGroup fg = readFilter(form, designEngine, folio.getFilter());
        if(folio.getFilter() instanceof StandardFilterGroup) {
            fg.setFilterGroupId(((StandardFilterGroup)folio.getFilter()).getFilterGroupId());
        }
        folio.setFilter(fg);
    }

	public static void addBlankPillars(Folio folio, DynaBean form, DesignEngine designEngine) throws ConvertException {
        int blankPillars = ConvertUtils.getInt(form.get("blankPillars"), 0);
        for(int i = 0; i < blankPillars; i++) {
            folio.newPillar();
        }
        log.debug("Added " + blankPillars + " new blank pillars");
    }

    public static void updatePillar(Folio folio, DynaBean form, DesignEngine designEngine) throws ConvertException, DesignException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        int newPillarIndex = ConvertUtils.getInt(form.get("newPillarIndex"), 0);
        if(newPillarIndex > 0) {
            int pillarIndex = ConvertUtils.getInt(form.get("pillarIndex"));
            if(newPillarIndex >= folio.getPillars().length) {
                newPillarIndex = folio.getPillars().length;
                folio.newPillar();
            }
            Pillar pillar = folio.getPillars()[pillarIndex-1];

            Set<String> propNames = ReflectionUtils.getPropertyNames(pillar);
            if(propNames != null) {
                for(String name : propNames) {
                    Object value = ReflectionUtils.getProperty(form, name);
                    if(value != null)
                    	ReflectionUtils.setProperty(pillar, name, value, true);
                }
            }
            //Field orders
            log.debug("Got fieldIds = " + form.get("fieldIds"));
            long[] fieldIds = ConvertUtils.convert(long[].class, form.get("fieldIds"));
            SortOrder[] sortOrders = ConvertUtils.convert(SortOrder[].class, form.get("sortOrders"));
            Results.Aggregate[] aggTypes = ConvertUtils.convert(Results.Aggregate[].class, form.get("aggregateTypes"));
            Integer[] sortIndexes = ConvertUtils.convert(Integer[].class, form.get("sortIndexes"));
            Field[] fields = designEngine.getFields(fieldIds);
            //set any missing fo attributes from existing fo's
            if(sortOrders == null) sortOrders = new SortOrder[fieldIds.length];
            else if(sortOrders.length != fieldIds.length) {
                SortOrder[] tmp = new SortOrder[fieldIds.length];
                System.arraycopy(sortOrders, 0, tmp, 0, Math.min(sortOrders.length, fieldIds.length));
                sortOrders = tmp;
            }
            if(aggTypes == null) aggTypes = new Results.Aggregate[fieldIds.length];
            else if(aggTypes.length != fieldIds.length) {
                Results.Aggregate[] tmp = new Results.Aggregate[fieldIds.length];
                System.arraycopy(aggTypes, 0, tmp, 0, Math.min(aggTypes.length, fieldIds.length));
                aggTypes = tmp;
            }
            if(sortIndexes == null) sortIndexes = new Integer[fieldIds.length];
            else if(sortIndexes.length != fieldIds.length) {
                Integer[] tmp = new Integer[fieldIds.length];
                System.arraycopy(sortIndexes, 0, tmp, 0, Math.min(sortIndexes.length, fieldIds.length));
                sortIndexes = tmp;
            }
            // this uses the existing values as defaults
            for(FieldOrder fo : pillar.getFieldOrders()) {
                for(int i = 0; i < fieldIds.length; i++) {
                    if(fieldIds[i] == fo.getField().getId()) {
                        if(sortOrders[i] == null) sortOrders[i] = fo.getSortOrder();
                        //if(aggTypes[i] == null) aggTypes[i] = fo.getAggregateType();
                        if(sortIndexes[i] == null) sortIndexes[i] = fo.getSortIndex();
                        break;
                    }
                }
            }

            int[] si = new int[sortIndexes.length];
            for(int i = 0; i < sortIndexes.length; i++)
                si[i] = (sortIndexes[i] == null ? 0 : sortIndexes[i]);
            form.set("sortOrders", sortOrders);
            form.set("aggregateTypes", aggTypes);
            form.set("sortIndexes", si);

            if(log.isDebugEnabled()) log.debug("Replacing field orders with fieldIds="
                    + Arrays.toString(fieldIds) + "; SortOrders=" + Arrays.toString(sortOrders) +
                    "; Agg Types=" + Arrays.toString(aggTypes) + "; Sort Indexes=" + Arrays.toString(sortIndexes));
            pillar.replaceFieldOrders(fields, sortOrders, aggTypes, si);
            //Move pillar if necessary
            if(pillarIndex != newPillarIndex) {
                log.debug("Moving pillar at " + pillarIndex + " to " + newPillarIndex);
                folio.movePillar(pillarIndex, newPillarIndex);
            }
            log.debug("Updated pillar " + pillarIndex);
        }
    }

	public static void chartConfig(Folio folio, DynaBean form, DesignEngine designEngine) throws ConvertException {
    	folio.setChartType(ConvertUtils.getString(form.get("chartType"), null));

    	int[] group0 = ConvertUtils.convert(int[].class, form.get("chart.groupLevel0"));
		int[] group1 = ConvertUtils.convert(int[].class, form.get("chart.groupLevel1"));
		int[] clusters = ConvertUtils.convert(int[].class, form.get("chart.cluster"));
		String[] values = ConvertUtils.convert(String[].class, form.get("chart.value"));
		Pillar[] oldPillars = folio.getPillars();
		int index = 1;
		if(group1 != null)
			for(int i=0; i < group1.length; i++){
				int curGroup = group1[i];
				if(curGroup > 0){
					log.debug("Updating Group1(" + curGroup + ").");
					Pillar p = oldPillars[curGroup-1];
					p.setGroupingLevel(Folio.GROUPING_LEVEL_ONE);
					p.setIndex(index++);
					log.debug("Group1(" + curGroup + ") updated.");
				}
			}

		if(clusters != null)
			for(int i=0; i < clusters.length; i++){
				int curCluster = clusters[i];
				if(curCluster > 0){
					Pillar p = oldPillars[curCluster-1];
					p.setPillarType(Pillar.PillarType.CLUSTER);

					if(p.getGroupingLevel() > Folio.GROUPING_LEVEL_ZERO) p.setGroupingLevel(Folio.GROUPING_LEVEL_ZERO);

					p.setIndex(index++);
					log.debug("Cluster(" + curCluster + ") updated.");
				}
			}

		if(values != null)
			for(int i=0; i < values.length; i++){
				String curValue = values[i];
				if(curValue != null && curValue.trim().length() > 0){
					String[] indexes = curValue.split("\\.");
					// Offset from 1 based to 0 based indexing.
					int pillarIndex = Integer.valueOf(indexes[0]) - 1;
					int fieldIndex = Integer.valueOf(indexes[1]) - 1;
					Pillar p = oldPillars[pillarIndex];

					p.setPillarType(Pillar.PillarType.VALUE);

					if(fieldIndex > 0){
						FieldOrder[] fieldOrders = p.getFieldOrders();
						Field[] fields = new Field[fieldOrders.length];
						SortOrder[] sortOrders = new SortOrder[fieldOrders.length];
						Results.Aggregate[] aggregateTypes = new Results.Aggregate[fieldOrders.length];
	                    int[] sortIndexes = new int[fieldOrders.length];

						fields[0] = fieldOrders[fieldIndex].getField();
						sortOrders[0] = fieldOrders[fieldIndex].getSortOrder();
						aggregateTypes[0] = fieldOrders[fieldIndex].getAggregateType();
	                    sortIndexes[0] = fieldOrders[fieldIndex].getSortIndex();

						for(int j=1; j < fieldOrders.length; j++){
							if(j <= fieldIndex){
								fields[j] = fieldOrders[j-1].getField();
								sortOrders[j] = fieldOrders[j-1].getSortOrder();
								aggregateTypes[j] = fieldOrders[j-1].getAggregateType();
	                            sortIndexes[j] = fieldOrders[j-1].getSortIndex();
							}
							if(j > fieldIndex){
								fields[j] = fieldOrders[j+1].getField();
								sortOrders[j] = fieldOrders[j+1].getSortOrder();
								aggregateTypes[j] = fieldOrders[j+1].getAggregateType();
	                            sortIndexes[j] = fieldOrders[j+1].getSortIndex();
							}
						}
						p.replaceFieldOrders(fields,sortOrders,aggregateTypes, sortIndexes);
					}

					if(p.getGroupingLevel() > Folio.GROUPING_LEVEL_ZERO) p.setGroupingLevel(Folio.GROUPING_LEVEL_ZERO);

					p.setIndex(index++);
					log.debug("Value(" + pillarIndex + "," + fieldIndex + ") updated.");
				}
			}

		if(group0 != null)
			for(int i=0; i < group0.length; i++) {
				try {
					Pillar p = oldPillars[group0[i]-1];
					if(p != null) {
						if((p.getPillarType() != Pillar.PillarType.CLUSTER) && (p.getPillarType() != Pillar.PillarType.VALUE)){
							if(p.getGroupingLevel() > Folio.GROUPING_LEVEL_ZERO) p.setGroupingLevel(Folio.GROUPING_LEVEL_ZERO);
							p.setIndex(index++);
						}
					}
				} catch(Exception e){
					log.debug("Group0(" + group0[i] + ") exception.");
				}
				log.debug("Group0(" + group0[i] + ") updated.");
			}

		for(int i = index; i < oldPillars.length+1; i++)
			folio.removePillar(i);
    }

    public static void updateTitles(Folio folio, DynaBean form, DesignEngine designEngine) throws ConvertException {
        String name = ConvertUtils.getString(form.get("name"), null);
        String title = ConvertUtils.getString(form.get("title"), null);
        String subtitle = ConvertUtils.getString(form.get("subtitle"), null);
        if(name != null) folio.setName(name);
        if(title != null) {
            if(title.trim().length() > 0)
                folio.setTitle(ConvertUtils.getFormat(title));
            else
                folio.setTitle(null);
        }
        if(subtitle != null) {
            if(subtitle.trim().length() > 0)
                folio.setSubtitle(ConvertUtils.getFormat(subtitle));
            else
                folio.setSubtitle(null);
        }
    }

    public static void updateTitles(Report report, DynaBean form, DesignEngine designEngine) throws ConvertException {
        String name = ConvertUtils.getString(form.get("name"), null);
        String title = ConvertUtils.getString(form.get("title"), null);
        String subtitle = ConvertUtils.getString(form.get("subtitle"), null);
        if(name != null) report.setName(name);
        if(title != null) {
            if(title.trim().length() > 0)
                report.setTitle(ConvertUtils.getFormat(title));
            else
                report.setTitle(null);
        }
        if(subtitle != null) {
            if(subtitle.trim().length() > 0)
                report.setSubtitle(ConvertUtils.getFormat(subtitle));
            else
                report.setSubtitle(null);
        }
    }

    public static void addFieldToPillar(Folio folio, int pillarIndex, long fieldId, DesignEngine designEngine)
            throws DesignException {
        Field field = designEngine.getField(fieldId);
        log.debug("Adding field to pillar " + pillarIndex + " on folio with " + folio.getPillars().length + " pillars");
        if(folio.getPillars().length > pillarIndex) {
            Pillar pillar = folio.getPillars()[pillarIndex-1];
            pillar.addFieldOrder(field, null, null, 0);
            log.debug("Pillar " + pillarIndex + " now has " + pillar.getFieldOrders().length + " fields");
        } else {
            addFieldPillar(folio, 0, field, null);
        }

    }

    public static void addFieldPillar(Folio folio, long fieldId, DesignEngine designEngine) throws DesignException {
        Field field = designEngine.getField(fieldId);
        addFieldPillar(folio, Folio.GROUPING_LEVEL_ZERO, field, null);
    }
    protected static DataGenre getSortType(Field field) {
		switch(field.getSortSqlType().getTypeCode()) {
			case java.sql.Types.BIGINT:
			case java.sql.Types.BIT:
			case java.sql.Types.DECIMAL:
			case java.sql.Types.DOUBLE:
			case java.sql.Types.FLOAT:
			case java.sql.Types.INTEGER:
			case java.sql.Types.NUMERIC:
			case java.sql.Types.REAL:
			case java.sql.Types.SMALLINT:
			case java.sql.Types.TINYINT:
				return DataGenre.NUMBER;
			case java.sql.Types.DATE:
			case java.sql.Types.TIME:
			case java.sql.Types.TIMESTAMP:
				return DataGenre.DATE;
			default:
				return DataGenre.STRING;
		}
	}
    public static void addFieldPillar(Folio folio, int groupingLevel, Field field, SortOrder sortOrder) {
		addLinkPillar(folio, groupingLevel, null, null, field, sortOrder);
	}
    public static void addLinkPillar(Folio folio, int groupingLevel, String actionFormat, String helpFormat, Field field, SortOrder sortOrder) {
		addPillar(folio, Pillar.PillarType.LINK, groupingLevel, field.getLabel(), null, null, field.getFormat(), actionFormat, helpFormat, null, getSortType(field), new SortOrder[] {sortOrder},  new Field[] {field}, null, null);
	}
    public static void addPillar(Folio folio, Pillar.PillarType pillarType, int groupingLevel, String labelFormat, String description, String styleFormat, String displayFormat, String actionFormat, String helpFormat, String sortFormat, DataGenre sortType, SortOrder[] sortOrders, Field[] fields, Results.Aggregate[] aggregateTypes, int[] sortIndexes) {
		//add to pillars
		Pillar pillar = folio.newPillar();
		pillar.setGroupingLevel(groupingLevel);
		if(actionFormat != null)
			pillar.setActionFormat(ConvertUtils.getFormat(actionFormat));
		pillar.setDescription(description);
		if(displayFormat != null)
			pillar.setDisplayFormat(ConvertUtils.getFormat(displayFormat));
		if(helpFormat != null)
			pillar.setHelpFormat(ConvertUtils.getFormat(helpFormat));
		if(labelFormat != null)
			pillar.setLabelFormat(ConvertUtils.getFormat(labelFormat));
		pillar.setPillarType(pillarType);
		if(sortFormat != null)
			pillar.setSortFormat(ConvertUtils.getFormat(sortFormat));
		pillar.setSortType(sortType);
		if(styleFormat != null)
			pillar.setStyleFormat(ConvertUtils.getFormat(styleFormat));

		//add to fieldorders
		pillar.replaceFieldOrders(fields, sortOrders, aggregateTypes, sortIndexes);
	}
	public static void updateDirectives(Folio folio, DynaBean form) throws ConvertException {
		String[] names = ConvertUtils.convert(String[].class, form.get("directiveNames"));
		if(names != null && names.length > 0) {
			String[] values = ConvertUtils.convert(String[].class, form.get("directiveValues"));
			for(int i = 0; i < names.length; i++) {
				if(values != null && values.length > i)
					folio.setDirective(names[i], values[i]);
				else
					folio.setDirective(names[i], null);
			}
		} else {
			for(int i = 1; true; i++) {
				String name = ConvertUtils.getString(form.get("directiveName" + i), null);
				if(name == null)
					break;
				String value = ConvertUtils.getString(form.get("directiveValue" + i), null);
				folio.setDirective(name, value);
			}
		}
	}
	public static void updateDirectives(Report report, DynaBean form) throws ConvertException {
		String[] names = ConvertUtils.convert(String[].class, form.get("directiveNames"));
		if(names != null && names.length > 0) {
			String[] values = ConvertUtils.convert(String[].class, form.get("directiveValues"));
			for(int i = 0; i < names.length; i++) {
				if(values != null && values.length > i)
					report.setDirective(names[i], values[i]);
				else
					report.setDirective(names[i], null);
			}
		} else {
			for(int i = 1; true; i++) {
				String name = ConvertUtils.getString(form.get("directiveName" + i), null);
				if(name == null)
					break;
				String value = ConvertUtils.getString(form.get("directiveValue" + i), null);
				report.setDirective(name, value);
			}
		}
	}
}
