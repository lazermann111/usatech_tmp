package simple.falcon.run;


import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.InflaterInputStream;

import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.BasicDataSourceFactory;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.DirectoryScene;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Output;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.ResultsType;
import simple.falcon.engine.Scene;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardDesignEngine2;
import simple.falcon.engine.standard.StandardExecuteEngine;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.LoadingException;
import simple.io.Log;
import simple.io.WriterOutputStream;
import simple.net.protocol.AddProtocols;
import simple.results.BeanException;
import simple.servlet.RequestInfoUtils;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.LockSegmentCache;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public class ReportRunner {
	public static final Charset XML_CHARSET = Charset.forName("UTF-8");

	private static Log log = Log.getLog();
    protected ExecuteEngine executeEngine;
    protected LockSegmentCache<String, ConfigSource, RuntimeException> uriFileCache = new LockSegmentCache<String, ConfigSource, RuntimeException>() {
		@Override
		protected ConfigSource createValue(String key, Object... additionalInfo) {
			try {
				return ConfigSource.createConfigSource(key);
			} catch(IOException e) {
				if(log.isDebugEnabled())
					log.debug("Could not create ConfigSource for '" + key + "'", e);
				return null;
			}
		}
		@Override
		protected boolean keyEquals(String key1, String key2) {
			return key1.equals(key2);
		}
		@Override
		protected boolean valueEquals(ConfigSource value1, ConfigSource value2) {
			return value1.equals(value2);
		}
    };

    public ReportRunner() {
    }
    public ReportRunner(ExecuteEngine executeEngine) {
    	this.executeEngine = executeEngine;
    }
    protected void checkExecuteEngine() throws ExecuteException {
    	if(executeEngine == null)
    		throw new ExecuteException("ExecuteEngine is not set");
    }

	public String runReport(Folio folio, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, OutputStream outputStream) throws ExecuteException, DesignException, ParameterException {
    	Scene scene = createScene(timeZone, locale, baseUrl, runReportAction, live, layout);
    	Report report = getReportFromFolio(folio, scene);
		runReport(report, parameters, executor, scene, createOutput(null, outputStream));
        OutputType output=report.getOutputType();
        return output == null ? "" : output.getContentType();
    }

	public String runReport(Report report, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, OutputStream outputStream) throws ExecuteException, DesignException, ParameterException {
    	Scene scene = createScene(timeZone, locale, baseUrl, runReportAction, live, layout);
		runReport(report, parameters, executor, scene, createOutput(null, outputStream));
        OutputType output=report.getOutputType();
        return output == null ? "" : output.getContentType();
    }

	public String runFolio(long folioId, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, Writer writer) throws ExecuteException, DesignException, ParameterException {
		return runFolio(folioId, parameters, executor, timeZone, locale, baseUrl, runReportAction, live, layout, createOutput(writer, null));
    }

	public String runFolio(long folioId, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, OutputStream outputStream) throws ExecuteException, DesignException, ParameterException {
		return runFolio(folioId, parameters, executor, timeZone, locale, baseUrl, runReportAction, live, layout, createOutput(null, outputStream));
    }

	protected String runFolio(long folioId, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, Output out) throws ExecuteException, DesignException, ParameterException {
    	checkExecuteEngine();
    	Scene scene = createScene(timeZone, locale, baseUrl, runReportAction, live, layout);
    	Report report = getReportFromFolio(folioId, scene);
		runReport(report, parameters, executor, scene, out);
        OutputType output=report.getOutputType();
        return output == null ? "" : output.getContentType();
    }

	public String runReport(long reportId, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, Writer writer) throws ExecuteException, DesignException, ParameterException {
		return runReport(reportId, parameters, executor, timeZone, locale, baseUrl, runReportAction, live, layout, createOutput(writer, null));
    }

	public String runReport(long reportId, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, OutputStream outputStream) throws ExecuteException, DesignException, ParameterException {
		return runReport(reportId, parameters, executor, timeZone, locale, baseUrl, runReportAction, live, layout, createOutput(null, outputStream));
    }

	protected String runReport(long reportId, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, Output out) throws ExecuteException, DesignException, ParameterException {
    	checkExecuteEngine();
    	Scene scene = createScene(timeZone, locale, baseUrl, runReportAction, live, layout);
    	Report report = executeEngine.getDesignEngine().getReport(reportId, scene.getTranslator());
		runReport(report, parameters, executor, scene, out);
    	OutputType output=report.getOutputType();
        return output == null ? "" : output.getContentType();
    }

	public String runFolios(long[] folioIds, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, Writer writer) throws ExecuteException, DesignException, ParameterException {
		return runFolios(folioIds, parameters, executor, timeZone, locale, baseUrl, runReportAction, live, layout, createOutput(writer, null));
    }

	public String runFolios(long[] folioIds, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, OutputStream outputStream) throws ExecuteException, DesignException, ParameterException {
		return runFolios(folioIds, parameters, executor, timeZone, locale, baseUrl, runReportAction, live, layout, createOutput(null, outputStream));
    }

	protected String runFolios(long[] folioIds, Map<String, Object> parameters, Executor executor, TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout, Output out) throws ExecuteException, DesignException, ParameterException {
    	checkExecuteEngine();
    	Scene scene = createScene(timeZone, locale, baseUrl, runReportAction, live, layout);
    	Report report = getReportFromFolios(folioIds, scene);
		runReport(report, parameters, executor, scene, out);
    	OutputType output=report.getOutputType();
        return output == null ? "" : output.getContentType();
    }

	public Scene createScene(TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, boolean live, Layout layout) throws ExecuteException {
    	//calculate baseWebDir using baseUrl
    	String baseWebDir = executeEngine.getCssDirectory();
    	return createScene(timeZone, locale, baseUrl, runReportAction, baseWebDir, live, layout);
    }

	public Scene createScene(TimeZone timeZone, Locale locale, String baseUrl, String runReportAction, String baseWebDirectory, boolean live, Layout layout) throws ExecuteException {
		Scene scene = new DirectoryScene(baseWebDirectory);
		scene.setFileCache(uriFileCache);
		scene.setTimeZone(timeZone);
        scene.setLocale(locale);
        scene.setBaseUrl(baseUrl);
        scene.setRunReportAction(runReportAction);
        scene.setLive(live);
        scene.setLayout(layout);
		String host;
		if(scene.getBaseUrl() == null || scene.getBaseUrl().trim().length() == 0)
			host = null;
		else
			try {
				host = new URL(scene.getBaseUrl()).getHost();
			} catch(MalformedURLException e) {
				log.warn("Could not parse baseUrl '" + scene.getBaseUrl() + "' to find the hostname", e);
				host = scene.getBaseUrl();
			}
		Translator translator;
		try {
			translator = TranslatorFactory.getDefaultFactory().getTranslator(scene.getLocale(), host);
		} catch(ServiceException e) {
			throw new ExecuteException(e);
		}
    	scene.setTranslator(translator);
    	scene.setStylesheets(addLastModifiedToUris(baseWebDirectory, translator.translate("report-stylesheets", (String)null)));
		scene.setScripts(addLastModifiedToUris(baseWebDirectory, translator.translate("main-scripts", (String) null)));
        return scene;
	}

    protected String[] addLastModifiedToUris(String baseWebDirectory, String... paths) {
    	List<String> uris = new ArrayList<String>();
        for(String path : paths) {
            if(path != null && (path=path.trim()).length() > 0)
            	for(String p : StringUtils.split(path, ',')) {
            		p = p.trim();
                    if(p.length() > 0)
                    	uris.add(IOUtils.addLastModifiedToFile(p, baseWebDirectory + p, uriFileCache));
            	}

        }
        return uris.toArray(new String[uris.size()]);

    }/*
    protected Translator getTranslator(Scene scene) {
    	Translator translator = scene.getTranslator();
    	if(translator == null) {
    		translator = TranslatorFactory.getDefaultFactory().getTranslator(scene.getLocale(), scene.getBaseUrl());
    		scene.setTranslator(translator);
    	}
    	return translator;
    }*/

	public void runReport(Report report, Map<String, Object> parameters, Executor executor, Scene scene, Writer writer) throws ExecuteException, DesignException, ParameterException {
		runReport(report, parameters, executor, scene, createOutput(writer, null));
    }

	public void runReport(Report report, Map<String, Object> parameters, Executor executor, Scene scene, OutputStream outputStream) throws ExecuteException, DesignException, ParameterException {
		runReport(report, parameters, executor, scene, createOutput(null, outputStream));
    }

    protected Output createOutput(final Writer writer, final OutputStream outputStream) {
    	return new Output() {
            public Writer getWriter() throws IOException {
                return writer != null ? writer : new OutputStreamWriter(outputStream);
            }
            public OutputStream getOutputStream() throws IOException {
                return outputStream != null ? outputStream : new WriterOutputStream(writer);
            }
			public Result getResult() throws IOException {
				return writer != null ? new StreamResult(writer) : new StreamResult(outputStream);
			}
         };
    }
    
	public void runReport(Report report, Map<String, Object> parameters, Executor executor, Scene scene, Output out) throws ExecuteException, DesignException, ParameterException {
    	checkExecuteEngine();
		adjustSceneForReport(scene, report);
		executeEngine.buildReport(report, parameters, executor, report.getOutputType(), scene, out, ResultsType.FORWARD);
    }

	public static void adjustSceneForReport(Scene scene, Report report) {
		if(scene.getLayout() == null) {
			if(RequestInfoUtils.isDirectlyRendered(report.getOutputType().getContentType()))
				scene.setLayout(Layout.FULL);
			else
				scene.setLayout(Layout.UNFRAMED);
		}
	}
    /** This main method is just a sample of what could be done.
     * @param args
     * @throws BeanException
     * @throws ConvertException
     * @throws DataLayerException
     * @throws SQLException
     * @throws LoadingException
     * @throws IOException
     * @throws ConfigException
     * @throws DesignException
     * @throws ExecuteException
     * @throws InstantiationException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IntrospectionException
     */
    public static void main(String[] args) throws SQLException, DataLayerException, ConvertException, BeanException, ConfigException, IOException, LoadingException, ExecuteException, DesignException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException {
        System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
        System.setProperty("javax.xml.transform.TransformerFactory", "com.jclark.xsl.trax.TransformerFactoryImpl");
        log = Log.getLog();
        AddProtocols.run();
        BasicDataSourceFactory dsf = new BasicDataSourceFactory();
        dsf.addDataSource("REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@usadbd02.usatech.com:1531:usadbd02", "USALIVE_APP", "USALIVE_APP");
        dsf.addDataSource("metadata", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@usadbd02.usatech.com:1531:usadbd02", "FOLIO_CONF", "FOLIO_CONF");
        DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
        ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml");

        //Engine 2
        final StandardDesignEngine sde2 = new StandardDesignEngine2();
        final ExecuteEngine ee2 = new StandardExecuteEngine(sde2);
        ee2.setDataSourceFactory(dsf);
		ee2.setDefaultDataSourceName("REPORT");

        /*
        TaskListener tl2 = new WriteCSVTaskListener(pw) {
			@Override
			protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
				super.taskRan(taskName, taskDetails, threadId, "Engine #2", startTime, endTime);
			}
        };
        ee2.setTaskListener(tl2);
		*/

        ReportRunner rr = new ReportRunner(ee2);
        final Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("promptForParams", "false");
        parameters.put("StartDate", "08/13/2007");
        parameters.put("EndDate","08/20/2007");
        parameters.put("DeviceTypeId", "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14");
        parameters.put("TransTypeId", "16");
        parameters.put("AllTerminals", "1");

        String baseUrl = "http://localhost:8580/CustomerReporting/";
        String runReportAction = "run_report.i";
        //ee2.setCssDirectory("file:///E:/Java Projects/usalive/web/");
        ee2.setCssDirectory(baseUrl);
        Locale locale = null;
        long userId = 1;
        long[] userGroupIds = new long[] {1}; // see com.usatech.usalive.hybrid.HybridServlet
        try {
			rr.runFolio(25, parameters, rr.createExecutor(userId, userGroupIds, null), null, locale, baseUrl, runReportAction, true, Layout.FULL, new OutputStreamWriter(System.out));
        } catch(Exception e) {
        	log.warn("Error occurred", e);
        }
        if(ee2.getTaskListener() != null) ee2.getTaskListener().flush();
        Log.finish();
    }

	public Executor createExecutor(long userId, long[] userGroupIds, String displayName) {
		BasicExecutor executor = new BasicExecutor();
		executor.setUserId(userId);
		executor.setUserGroupIds(userGroupIds);
		executor.setDisplayName(displayName);
		return executor;
	}

	public ExecuteEngine getExecuteEngine() {
		return executeEngine;
	}

	public void setExecuteEngine(ExecuteEngine executeEngine) {
		this.executeEngine = executeEngine;
	}

	public Report getReport(long reportId, Scene scene) throws DesignException {
    	return executeEngine.getDesignEngine().getReport(reportId, scene.getTranslator());
    }

	public Report getReportFromCompressedXml(InputStream compressedXml, Scene scene) throws DesignException, UnsupportedEncodingException {
		InflaterInputStream iis = new InflaterInputStream(compressedXml);
		Reader reader = new InputStreamReader(iis, "UTF-8");
		return getReportFromXml(reader, scene);
	}
	public Report getReportFromXml(InputStream xml, Scene scene) throws DesignException {
		Report report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
        try {
			ObjectBuilder builder = new ObjectBuilder(xml);
			report.readXML(builder);
		} catch(Exception e) {
			throw new DesignException(e);
		}
		return report;
    }
	public Report getReportFromXml(Reader xml, Scene scene) throws DesignException {
		Report report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
        try {
			ObjectBuilder builder = new ObjectBuilder(xml);
			report.readXML(builder);
		} catch(Exception e) {
			throw new DesignException(e);
		}
		return report;
    }
    public Report getReportFromFolios(long[] folioIds, Scene scene) throws DesignException {
    	Report report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
        Folio[] folios = new Folio[folioIds.length];
        for(int i = 0; i < folioIds.length; i++) {
            folios[i] = executeEngine.getDesignEngine().getFolio(folioIds[i], scene.getTranslator());
        }
        report.setFolios(folios);
        report.setName(folios[0].getName());
        report.setOutputType(folios[0].getOutputType());
        for(String dir : folios[0].getDirectiveNames())
            report.setDirective(dir, folios[0].getDirective(dir));
        return report;
    }
    public Report getReportFromFolio(long folioId, Scene scene) throws DesignException {
    	Folio folio = executeEngine.getDesignEngine().getFolio(folioId, scene.getTranslator());
    	return getReportFromFolio(folio, scene);
    }
    public Report getReportFromFolio(Folio folio, Scene scene) throws DesignException {
    	Report report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
        report.setFolios(new Folio[] {folio});
        report.setName(folio.getName());
        report.setOutputType(folio.getOutputType());
        for(String dir : folio.getDirectiveNames())
            report.setDirective(dir, folio.getDirective(dir));
        return report;
    }
    public Report decodeReport(Scene scene, byte[] compressedXml) throws DesignException {
    	return decodeReport(scene, new ByteArrayInputStream(compressedXml));
    }

    public Report decodeReport(Scene scene, InputStream compressedXml) throws DesignException {
    	InflaterInputStream iis = new InflaterInputStream(compressedXml);
    	Reader reader = new InputStreamReader(iis, XML_CHARSET);
    	return getReportFromXml(reader, scene);
    }

    public byte[] encodeReport(Report report) throws IOException {
    	try {
			return XMLBuilder.toCompressedXML(report, XML_CHARSET);
		} catch(InstantiationException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(IllegalAccessException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(ClassNotFoundException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(IntrospectionException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(InvocationTargetException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(ConvertException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(ParseException e) {
			throw new IOException("Could not create XMLBuilder", e);
		} catch(SAXException e) {
			throw new IOException("Could not write the report to xml", e);
		}
    }
}
