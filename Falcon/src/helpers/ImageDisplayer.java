/*
 * Created on Sep 13, 2005
 *
 */
package helpers;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

public class ImageDisplayer extends Applet {
    private static final long serialVersionUID = 3248140650L;
	protected Image image;

    public ImageDisplayer() {
        super();
    }
    
    /* (non-Javadoc)
     * @see java.applet.Applet#start()
     */
    @Override
    public void start() {
        String data = getParameter("data");
        sendMessage("Data: " + data.substring(0, 12) + "...; Length: " + data.length());
        byte[] b = data.getBytes();
        image = Toolkit.getDefaultToolkit().createImage(decode(b, 0, b.length));
        sendMessage("Created image '" + image.getClass().getName() + "' with source '"
                + image.getSource().getClass().getName() + "': " + image.getWidth(this) + " x " + image.getHeight(this));        
        //setPreferredSize(new Dimension(image.getWidth(this), image.getHeight(this)));
    }

    private void sendMessage(String msg) {
        System.err.println(msg);
        //javax.swing.JOptionPane.showConfirmDialog(this, msg);
    }
    /* (non-Javadoc)
     * @see java.awt.Container#paint(java.awt.Graphics)
     */
    @Override
    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, this);
        //super.paint(g);
    }

    private final static byte[] DECODABET = {
        -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9,
        -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9,
        -9, -9, -9, -9, -9, -9, -9, 
        // Decimal 33 - 42
        62, -9, -9, -9, 
        // Decimal 44 - 46
        63, 
        // Slash at decimal 47
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 
        // Decimal 62 - 64
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 
        // Letters 'A' through 'N'
        14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9,
        
        // Decimal 91 - 96
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 
        // Letters 'a' through 'm'
        39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9
    };
    //private final static byte BAD_ENCODING = -9;

    // Indicates error in encoding
    private final static byte white_SPACE_ENC = -5;

    // Indicates white space in encoding
    private final static byte EQUALS_SIGN_ENC = -1;

    private final static byte EQUALS_SIGN = (byte) '=';
    
    public static byte[] decode(byte[] source, int off, int len) {
        int len34 = (len * 3) / 4;
        byte[] outBuff = new byte[len34];

        // Upper limit on size of output
        int outBuffPosn = 0;
        byte[] b4 = new byte[4];
        int b4Posn = 0;
        int i = 0;
        byte sbiCrop = 0;
        byte sbiDecode = 0;

        for (i = 0; i < len; i++) {
            sbiCrop = (byte) (source[i] & 0x7f);

            // Only the low seven bits
            sbiDecode = DECODABET[sbiCrop];

            if (sbiDecode >= white_SPACE_ENC) {
                // White space, Equals sign or better
                if (sbiDecode >= EQUALS_SIGN_ENC) {
                    b4[b4Posn++] = sbiCrop;

                    if (b4Posn > 3) {
                        outBuffPosn += decode4to3(b4, 0, outBuff, outBuffPosn);
                        b4Posn = 0;

                        // If that was the equals sign, break out of 'for' loop
                        if (sbiCrop == EQUALS_SIGN) {
                            break;
                        }
                    }

                    // end if: quartet built
                }

                // end if: equals sign or better
            }
            // end if: white space, equals sign or better
            else {
                System.err.println("Bad Base64 input character at " + i + ": " +
                    source[i] + "(decimal)");

                return null;
            }

            // end else:
        }

        // each input character
        byte[] out = new byte[outBuffPosn];
        System.arraycopy(outBuff, 0, out, 0, outBuffPosn);

        return out;
    }

    private static int decode4to3(byte[] source, int srcOffset,
            byte[] destination, int destOffset) {
            // Example: Dk==
            if (source[srcOffset + 2] == EQUALS_SIGN) {
                int outBuff = ((DECODABET[source[srcOffset]] << 24) >>> 6) |
                    ((DECODABET[source[srcOffset + 1]] << 24) >>> 12);
                destination[destOffset] = (byte) (outBuff >>> 16);

                return 1;
            }
            // Example: DkL=
            else if (source[srcOffset + 3] == EQUALS_SIGN) {
                int outBuff = ((DECODABET[source[srcOffset]] << 24) >>> 6) |
                    ((DECODABET[source[srcOffset + 1]] << 24) >>> 12) |
                    ((DECODABET[source[srcOffset + 2]] << 24) >>> 18);
                destination[destOffset] = (byte) (outBuff >>> 16);
                destination[destOffset + 1] = (byte) (outBuff >>> 8);

                return 2;
            }
            // Example: DkLE
            else {
                int outBuff = ((DECODABET[source[srcOffset]] << 24) >>> 6) |
                    ((DECODABET[source[srcOffset + 1]] << 24) >>> 12) |
                    ((DECODABET[source[srcOffset + 2]] << 24) >>> 18) |
                    ((DECODABET[source[srcOffset + 3]] << 24) >>> 24);
                destination[destOffset] = (byte) (outBuff >> 16);
                destination[destOffset + 1] = (byte) (outBuff >> 8);
                destination[destOffset + 2] = (byte) (outBuff);

                return 3;
            }
        }

}
