/*
---

name: Locale.pt-PT.Date

description: Date messages for Portuguese.

license: MIT-style license

authors:
  - Fabio Miranda Costa

requires:
  - /Locale

provides: [Locale.pt-PT.Date]

...
*/

Locale.define('pt-PT', 'Date', {

	months: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
	months_abbr: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
	days: ['Domingo', 'Segunda-feira', 'Ter�a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'S�bado'],
	days_abbr: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S�b'],

	// Culture's date order: DD-MM-YYYY
	dateOrder: ['date', 'month', 'year'],
	shortDate: '%d-%m-%Y',
	shortTime: '%H:%M',
	AM: 'AM',
	PM: 'PM',
	firstDayOfWeek: 1,

	// Date.Extras
	ordinal: '�',

	lessThanMinuteAgo: 'h� menos de um minuto',
	minuteAgo: 'h� cerca de um minuto',
	minutesAgo: 'h� {delta} minutos',
	hourAgo: 'h� cerca de uma hora',
	hoursAgo: 'h� cerca de {delta} horas',
	dayAgo: 'h� um dia',
	daysAgo: 'h� {delta} dias',
	weekAgo: 'h� uma semana',
	weeksAgo: 'h� {delta} semanas',
	monthAgo: 'h� um m�s',
	monthsAgo: 'h� {delta} meses',
	yearAgo: 'h� um ano',
	yearsAgo: 'h� {delta} anos',

	lessThanMinuteUntil: 'em menos de um minuto',
	minuteUntil: 'em um minuto',
	minutesUntil: 'em {delta} minutos',
	hourUntil: 'em uma hora',
	hoursUntil: 'em {delta} horas',
	dayUntil: 'em um dia',
	daysUntil: 'em {delta} dias',
	weekUntil: 'em uma semana',
	weeksUntil: 'em {delta} semanas',
	monthUntil: 'em um m�s',
	monthsUntil: 'em {delta} meses',
	yearUntil: 'em um ano',
	yearsUntil: 'em {delta} anos'

});

/*
---

name: Locale.pt-PT.Form.Validator

description: Form Validator messages for Portuguese.

license: MIT-style license

authors:
  - Miquel Hudin

requires:
  - /Locale

provides: [Locale.pt-PT.Form.Validator]

...
*/

Locale.define('pt-PT', 'FormValidator', {

	required: 'Este campo � necess�rio.',
	minLength: 'Digite pelo menos{minLength} caracteres (comprimento {length} caracteres).',
	maxLength: 'N�o insira mais de {maxLength} caracteres (comprimento {length} caracteres).',
	integer: 'Digite um n�mero inteiro neste dom�nio. Com n�meros decimais (por exemplo, 1,25), n�o s�o permitidas.',
	numeric: 'Digite apenas valores num�ricos neste dom�nio (p.ex., "1" ou "1.1" ou "-1" ou "-1,1").',
	digits: 'Por favor, use n�meros e pontua��o apenas neste campo (p.ex., um n�mero de telefone com tra�os ou pontos � permitida).',
	alpha: 'Por favor use somente letras (a-z), com nesta �rea. N�o utilize espa�os nem outros caracteres s�o permitidos.',
	alphanum: 'Use somente letras (a-z) ou n�meros (0-9) neste campo. N�o utilize espa�os nem outros caracteres s�o permitidos.',
	dateSuchAs: 'Digite uma data v�lida, como {date}',
	dateInFormatMDY: 'Digite uma data v�lida, como DD/MM/YYYY (p.ex. "31/12/1999")',
	email: 'Digite um endere�o de email v�lido. Por exemplo "fred@domain.com".',
	url: 'Digite uma URL v�lida, como http://www.example.com.',
	currencyDollar: 'Digite um valor v�lido $. Por exemplo $ 100,00. ',
	oneRequired: 'Digite algo para pelo menos um desses insumos.',
	errorPrefix: 'Erro: ',
	warningPrefix: 'Aviso: '

});


/*
---

name: Locale.pt-BR.Date

description: Date messages for Portuguese (Brazil).

license: MIT-style license

authors:
  - Fabio Miranda Costa

requires:
  - /Locale
  - /Locale.pt-PT.Date

provides: [Locale.pt-BR.Date]

...
*/

Locale.define('pt-BR', 'Date', {

	// Culture's date order: DD/MM/YYYY
	shortDate: '%d/%m/%Y'

}).inherit('pt-PT', 'Date');


/*
---

name: Locale.pt-BR.Form.Validator

description: Form Validator messages for Portuguese (Brazil).

license: MIT-style license

authors:
  - F�bio Miranda Costa

requires:
  - /Locale

provides: [Locale.pt-BR.Form.Validator]

...
*/

Locale.define('pt-BR', 'FormValidator', {

	required: 'Este campo � obrigat�rio.',
	minLength: 'Digite pelo menos {minLength} caracteres (tamanho atual: {length}).',
	maxLength: 'N�o digite mais de {maxLength} caracteres (tamanho atual: {length}).',
	integer: 'Por favor digite apenas um n�mero inteiro neste campo. N�o s�o permitidos n�meros decimais (por exemplo, 1,25).',
	numeric: 'Por favor digite apenas valores num�ricos neste campo (por exemplo, "1" ou "1.1" ou "-1" ou "-1,1").',
	digits: 'Por favor use apenas n�meros e pontua��o neste campo (por exemplo, um n�mero de telefone com tra�os ou pontos � permitido).',
	alpha: 'Por favor use somente letras (a-z). Espa�o e outros caracteres n�o s�o permitidos.',
	alphanum: 'Use somente letras (a-z) ou n�meros (0-9) neste campo. Espa�o e outros caracteres n�o s�o permitidos.',
	dateSuchAs: 'Digite uma data v�lida, como {date}',
	dateInFormatMDY: 'Digite uma data v�lida, como DD/MM/YYYY (por exemplo, "31/12/1999")',
	email: 'Digite um endere�o de email v�lido. Por exemplo "nome@dominio.com".',
	url: 'Digite uma URL v�lida. Exemplo: http://www.example.com.',
	currencyDollar: 'Digite um valor em dinheiro v�lido. Exemplo: R$100,00 .',
	oneRequired: 'Digite algo para pelo menos um desses campos.',
	errorPrefix: 'Erro: ',
	warningPrefix: 'Aviso: ',

	// Form.Validator.Extras
	noSpace: 'N�o � poss�vel digitar espa�os neste campo.',
	reqChkByNode: 'N�o foi selecionado nenhum item.',
	requiredChk: 'Este campo � obrigat�rio.',
	reqChkByName: 'Por favor digite um {label}.',
	match: 'Este campo deve ser igual ao campo {matchName}.',
	startDate: 'a data inicial',
	endDate: 'a data final',
	currendDate: 'a data atual',
	afterDate: 'A data deve ser igual ou posterior a {label}.',
	beforeDate: 'A data deve ser igual ou anterior a {label}.',
	startMonth: 'Por favor selecione uma data inicial.',
	sameMonth: 'Estas duas datas devem ter o mesmo m�s - voc� deve modificar uma das duas.',
	creditcard: 'O n�mero do cart�o de cr�dito informado � inv�lido. Por favor verifique o valor e tente novamente. {length} n�meros informados.'

});

Locale.use('pt-BR');