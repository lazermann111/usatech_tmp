function createArray() {
	return arguments;
}
function trim(s) {
    return s.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '');
}

function getContainingTable(tobj) {
	return getContainingTag(tobj, "TABLE");
}

function getContainingTag(tobj, tagName) {
	while(tobj != null && tobj.tagName.toUpperCase() != tagName.toUpperCase()) {
		tobj = tobj.parentNode;
	}
	return tobj;
}

function findIndex(arr, obj) {
	for(var i = 0; i < arr.length; i++) {
		if(arr[i] == obj) return i;
	}
	return -1;
}
function hide(elem) {
    if(document.layers) elem.visibility = "hide";
    else elem.style.display = "none";
}
function show(elem) {
    if(document.layers) elem.visibility = "show";
    else elem.style.display = "block";
}
function hideById(id) {
	hide(document.getElementById(id));
}
function showById(id) {
	show(document.getElementById(id));
}
function validateForm(form) {
	for(var i = 0; i < form.elements.length; i++) {
		var elem = form.elements[i];
		var label = elem.getAttribute("label");
		if(typeof(elem.value) == "string" && trim(elem.value).length > 0) {
			// regex match
			if(typeof(elem.getAttribute("valid")) == "string" && trim(elem.getAttribute("valid")).length > 0) {
				if(!trim(elem.value).match(elem.getAttribute("valid"))) {
					alert(label + " is not valid. Please correct.");
					elem.focus();
					return false;
				}
			}
			//less than or equal to max
			if(typeof(elem.getAttribute("maxValue")) == "string" && trim(elem.getAttribute("maxValue")).length > 0) {
				try {
					if(parseFloat(trim(elem.value)) > parseFloat(elem.getAttribute("maxValue"))) {
						alert(label + " must be less than or equal to " + elem.getAttribute("maxValue") + ". Please correct.");
						elem.focus();
					 	return false;
					}
				} catch(e) {
					alert(label + " is not a number. Please correct.");
					elem.focus();
				 	return false;
				}
			}
			// greater than or equal to min
			if(typeof(elem.getAttribute("minValue")) == "string" && trim(elem.getAttribute("minValue")).length > 0) {
				try {
					if(parseFloat(trim(elem.value)) < parseFloat(elem.getAttribute("minValue"))) {
						alert(label + " must be greater than or equal to " + elem.getAttribute("minValue") + ". Please correct.");
						elem.focus();
					 	return false;
					}
				} catch(e) {
					alert(label + " is not a number. Please correct.");
					elem.focus();
				 	return false;
				}
			}
		} else if(typeof(elem.getAttribute("required")) == "string" && "true" == elem.getAttribute("required")) {
			 alert(label + " is required. Please enter a value.");
			 elem.focus();
			 return false;
		}
	}
	return true;
}

function disableSubmit(form) {
	for(var i = 0; i < form.elements.length; i++) {
		var elem = form.elements[i];
		if((elem.type == "submit" || elem.type == "button") && elem.name == "")
		elem.disabled = true;
	}
	return true;
}

function openCalendar(fieldId){
	showCalendar(document.getElementById(fieldId));
}

function getValue(select) {
	if(document.layers) {
    	if(select.selectedIndex == -1) return "";
    	return select.options.item(select.selectedIndex).value;
    } else {
    	return select.value;
	}
}

function setValue(select, value) {
    if(document.layers) {
        for(var i = 0; i < select.options.length; i++) {
            if(select.options.item(i).value == value) {
                select.selectedIndex = i;
                return;
            }
        }
        select.selectedIndex = -1;
    } else {
        select.value = value;
    }
}

function popUp(name, url, height, width, modal) {
	if(modal) {
		if(window.showModalDialog)
	 		window.showModalDialog(url, null,"status=no;dialogHeight=" + height + "px;dialogWidth="+width+"px;");
	 	else {
		 	var win = window.open(url, name, "modal,status=no,resizable,height="+height+",width="+width);
			win.focus();
		}
	} else {
		var win = window.open(url, name, "resizable,status=no,height="+height+",width="+width);
		win.focus();
	}
}
