/*
 *  This contains various functions for showing "windows" or "dialogs".
 *  Requires MooTools v.1.11.
 */
Window.Dialog = {
	root:false, /* root background */
	box:false,  /* displayed content */
	imagesPath: "images/dialog/",
	helpDialog:null,
	createDialog : function(content, userOptions) {
		var options = {close: true, width:350, height: 200};
		if(userOptions)
			$extend(options, userOptions);
		var box = new Element("div", {
			styles : {
				position:"absolute",
				height: options.height,
				width: options.width,
				zIndex:2,
				top: options.top,
				left: options.left
			}});
		var table = new Element("table", {
			styles: {width:"100%", height:"100%"},
			cellSpacing:"0",cellPadding:"0"
		});
		box.adopt(table);
		var tbody = new Element('tbody');
		table.adopt(tbody);
		var tr1 = new Element("tr", {styles: {verticalAlign: "middle"}});
		var td1 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_top_left.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "left",
				width: 1,
				height: 26
			}
		});
		var bar = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_banner.png)",
				backgroundRepeat: "repeat-x",
				height: 26,
				fontWeight: "bold",
				color: "#fff"
			},
			className: "dialogCaption"
		});

		var caption = new Element("div", {styles:{marginTop: 5}});
		caption.setHTML("&nbsp;");
		if(options.title)
			caption.appendText(options.title);

		var td3 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_top_right.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "right",
				width: 1,
				height: 26
			}
		});

		tr1.adopt(td1,bar,td3);

		var tr2 = new Element("tr");
		var td4 = new Element("td", {
			styles: {
				backgroundColor: "#fff",
				backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_left.png)",
				backgroundRepeat: "repeat-y",
				backgroundPosition: "left",
				width: 1
			}
		});

		var td5 = new Element("td", {styles: { backgroundColor: "#fff",height:"100%"}});
		var container = new Element("div", {styles: {overflowX:"hidden",overflowY:"auto",width:"100%",height:options.height,position:"relative"}});
		td5.adopt(container);

		var td6 = new Element("td", {
			styles: {
				backgroundColor: "#fff",
				backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_right.png)",
				backgroundRepeat: "repeat-y",
				backgroundPosition: "right",
				width: 1
			}
		});

		tr2.adopt(td4,td5,td6);

		var tr3 = new Element("tr");
		var td7 = new Element("td", {
			styles: {
				backgroundColor: "#a9a9a9",
				//backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_bottom_left.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "left",
				width: 6,
				height: 1
			}
		});
		var td8 = new Element("td", {
			styles: {
				backgroundColor: "#a9a9a9",
				//backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_bottom.png)",
				backgroundRepeat: "repeat-x",
				height: 1
			}
		});
		var td9 = new Element("td", {
			styles: {
				backgroundColor: "#a9a9a9",
				//backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_border_bottom_right.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "right",
				width: 6,
				height: 1
			}
		});

		tr3.adopt(td7,td8,td9);

		tbody.adopt(tr1,tr2,tr3);

		container.adopt($(content));

		if(options.movable) {
			new Drag.Move(box, {"handle": bar});
		}

		if (options.close) {
			var closeBtn = new Element("div", {
				styles : {
					"float" : "right",
					fontSize : "1px",
					cursor : "pointer",
					width : "21px",
					height : "100%",
					backgroundImage: "url(" + Window.Dialog.imagesPath + "dialog_close.png)",
					backgroundRepeat: "no-repeat",
					backgroundPosition: "center center"
				}
			});
			var fn = function(img) {
				this.setStyle("backgroundImage", "url(" + Window.Dialog.imagesPath + img + ")");
			};
			closeBtn.addEvents({
				click: Window.Dialog.hide,
				mouseenter: fn.pass("dialog_close_hover.png", closeBtn),
				mouseleave: fn.pass("dialog_close.png", closeBtn),
				mousedown: fn.pass("dialog_close_down.png", closeBtn),
				mouseup: fn.pass("dialog_close.png", closeBtn)
			});
			bar.adopt(closeBtn);
		}
/*
	if (options.max) {
		obj.maxBtn = OAT.Dom.create("div");
		obj.move.appendChild(obj.maxBtn);
	}

	if (options.min) {
		obj.minBtn = OAT.Dom.create("div");
		obj.move.appendChild(obj.minBtn);
	}

	if (options.resize) {
		obj.resize = OAT.Dom.create("div");
 		obj.div.appendChild(obj.resize);
 		OAT.Resize.create(obj.resize,obj.div,OAT.Resize.TYPE_XY);
 		OAT.Resize.create(obj.resize,obj.content,OAT.Resize.TYPE_XY);
 		OAT.Resize.create(obj.resize,obj.move,OAT.Resize.TYPE_X);
	}

*/
		bar.adopt(caption); //must do this after all floting buttons

		return box;
	},
	/**
	 * Creates a "dialog" with inner content pulled from the specified url and then returns the inner content div dom element.
	 */
	open : function(url, data, options) {
		var content = new Element("div"); //,{styles: {width:"100%",height:"100%"}});
		content.setText(options.loadText ? options.loadText : "Loading...");
		var dialog = Window.Dialog.createDialog(content, options);
		Window.Dialog.show(dialog, options);
		new Ajax(url, {
			method : "post",
			async : true,
			update : content,
			evalScripts : true,
			autoCancel : true,
			data : data }).request();
		return content;
	},
/*
	prompt : function(message, title, initial) {
		var content = new Element("form");
		message = message.trim();
		var table = makeTable([
			[message + (message.test(":$") ? "" : ":") + "&nbsp;", new Element("input", {type:"text",value:initial})],
			[new Element("td", {colspan:2,style:{textAlign: "center"}}).adopt(
				new Element("input", {type:"submit",value:"OK",events:{click:function(){

				}}})
			)]]);

	},*/
	makeTable : function(cells, props) {
		var table = new Element("table", props);
		var tbody = new Element("tbody");
		table.adopt(tbody);
		for(var i = 0; i < cells.length; i++) {
			var tr = new Element("tr");
			tbody.adopt(tr);
			for(var k = 0; k < cells[i].length; k++) {
				var td;
				if($type(cells[i][k]) == "element") {
					if(cells[i][k].getTag().test("(td)|(th)")) {
						td = cells[i][k];
						if(td.colspan  && td.colspan > 1) {
							k = k + td.colspan;
						}
					} else {
						td = new Element("td");
						td.adopt(cells[i][k]);
					}
				} else {
					td = new Element("td");
					td.appendText(String.valueOf(cells[i][k]));
				}
				tr.adopt(td);
			}
		}
		return table;
	},
	getViewport : function() {
		if (window.webkit) {
			return {
				"left" : document.body.scrollLeft,
				"top" : document.body.scrollTop,
				"width" : window.innerWidth,
				"height" : window.innerHeight
			};
		} else if(window.opera) {
			return {
				"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
				"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
				"width" : document.body.clientWidth,
				"height" : document.body.clientHeight
			};
		} else if(window.ie && document.compatMode == "BackCompat") {
			return {
				"left" : document.body.scrollLeft,
				"top" : document.body.scrollTop,
				"width" : document.body.clientWidth,
				"height" : document.body.clientHeight
			};
		} else if(document.compatMode == "BackCompat") {
			return {
				"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
				"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
				"width" : document.body.clientWidth,
				"height" : document.body.clientHeight
			};
		} else {
			return {
				"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
				"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
				"width" : document.documentElement.clientWidth,
				"height" : document.documentElement.clientHeight
			};
		}
	},

	update:function(event) {
		if (!Window.Dialog.root) { return; }
		var view = Window.Dialog.getViewport();
		Window.Dialog.root.setStyles(view);
	},

	show:function(something,userOptions) {
		if (Window.Dialog.root) return; /* end if another is displayed */
		var options = {
			color:"#000",
			opacity:0.5,
			popup:false,
			center: true
		}
		if(userOptions)
			$extend(options, userOptions);
		var elm = $(something);
		if (!elm) return;
		elm.oldPosition = elm.getStyle("position");
		elm.setStyle("position","absolute");
		elm.oldZindex = elm.getStyle("zIndex");
		elm.setStyle("zIndex", 1000);
		elm.oldParent = elm.getParent();
		var root = new Element("div", {
			styles : {
				position:"fixed",
				left:"0px",
				top:"0px",
				width:"100%",
				height:"100%",
				zIndex : 1000,
				backgroundColor : "transparent"
			}
		});
		var cover = new Element("div", {
			styles : {
				position:"absolute",
				left:"0px",
				top:"0px",
				width:"100%",
				height:"100%",
				zIndex : 1,
				backgroundColor : options.color,
				opacity : options.opacity
			}
		});
		Window.Dialog.root = root;
		Window.Dialog.box = elm;
		elm.setStyle("display", "");
		root.adopt(elm, cover);
		if(window.ie6) {
			root.setStyle("position","absolute");
			Window.Dialog.update();
		}
		if(window.opera) { //this fixes opera's repaint issues
			var realRemove = root.remove.bind(root);
			root.remove = function() {
				cover.effect("opacity", {onComplete : function() {realRemove();}, duration: 200, transition: Fx.Transitions.linear}).start(0);				
			}; 
		}
		document.body.appendChild(root);
		/*if(options.center) {
			var view = Window.Dialog.getViewport();
			var dims = elm.getSize();
			var pos = {
				left : Math.max(Math.floor((view.width - dims.size.x) / 2), 0),
				top : Math.max(Math.floor((view.height - dims.size.y) / 2), 0)
			}
			elm.setStyles(pos);
		}*/
		if(options.popup) { root.addEvent("click", Window.Dialog.hide); }
	},

	hide:function() {
		if (!Window.Dialog.root) { return; }
		var elm = Window.Dialog.box;
		if(elm) {
			elm.setStyle("display", "none");
			elm.setStyle("zIndex", elm.oldZindex);
			elm.setStyle("position", elm.oldPosition);
			if(elm.oldParent)
				elm.oldParent.adopt(elm);
		}
		Window.Dialog.box = null;
		Window.Dialog.root.remove();
		Window.Dialog.root = false;
	} /* hide */,
	
	showDialog: function(content, options) {
		var dialog = Window.Dialog.createDialog(content, options);
		Window.Dialog.show(dialog, options);
	},
	/** This function shows a help pop-up that tells the user more about a feature. The feature's helpKey
	* is used to look up on the server the title and content for the help topic.
	*/
	showHelp :function(helpKey, dimensions, url) {
		if(!Window.Dialog.helpDialog) {
			var content = new Element("div", {
				styles: {
					position: "absolute",
					float: "right",
					color: "black",
					paddingRight: "15px"
				}
			});
			Window.Dialog.helpDialog = Window.Dialog.createHelpDialog(content, dimensions);
			Window.Dialog.helpDialog.content = content;
			Window.Dialog.helpDialog.setStyle("display", "none");
			document.body.appendChild(Window.Dialog.helpDialog);
		}
		Window.Dialog.helpDialog.content.setText("Please wait...");
		Window.Dialog.helpDialog.setStyle("display", "block");


		new Ajax((url || "help.i"), {
			method : "post",
			async : true,
			update : this.helpDialog.content,
			evalScripts : true,
			autoCancel : true,
			data : {helpKey : helpKey, fragment: true}
		}).request();
	},
	
	createHelpDialog : function(content, userOptions) {
		var options = {width:340, height: 200, left: "40%", top: "40%"};
		
		if(userOptions)
			$extend(options, userOptions);
		var box = new Element("div", {
			styles : {
				position:"absolute",
				height: options.height,
				width: options.width,
				zIndex: 2,
				top: options.top,
				left: options.left
			}
		});
		var table = new Element("table", {
			styles: {
				width: "100%",
				height: "100%"
			},
			cellSpacing: "0",
			cellPadding: "0"
		});
		
		box.adopt(table);
		var tbody = new Element('tbody');
		table.adopt(tbody);
		var tr1 = new Element("tr", {styles: {verticalAlign: "middle"}});
		var td1 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_top_left.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "left",
				width: 4,
				height: 20
			}
		});
		var bar = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_top.png)",
				cursor: "move",
				backgroundRepeat: "repeat-x",
				height: 20
			}
		});

		var closeBtn = new Element("div", {
			styles : {
				float : "right",
				fontSize : "1px",
				cursor : "pointer",
				width : "21px",
				height : "100%",
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_close.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "center center"
			}
		});
		
		var fn = function(img) {
			this.setStyle("backgroundImage", "url(" + Window.Dialog.imagesPath + img + ")");
		};
		
		closeBtn.addEvents({
			click: function() { Window.Dialog.helpDialog.setStyle("display", "none"); },
			mouseenter: fn.pass("help_close_hover.png", closeBtn),
			mouseleave: fn.pass("help_close.png", closeBtn),
			mousedown: fn.pass("help_close_down.png", closeBtn),
			mouseup: fn.pass("help_close.png", closeBtn)
		});
		bar.adopt(closeBtn);

		var td3 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_top_right.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "right",
				width: 4,
				height: 20
			}
		});

		tr1.adopt(td1,bar,td3);

		var tr2 = new Element("tr");
		var td4 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_side.png)",
				backgroundRepeat: "repeat-y",
				backgroundPosition: "left",
				width: 4
			}
		});

		var td5 = new Element("td", {
			styles: {
				height: "100%"
			}
		});
		var container = new Element("div", {
			styles : {
				overflow: "auto",
				width: "100%",
				height: "100%",
				position: "relative"
			}
		});
		container.addClass("help-dialog");
		
		td5.adopt(container);

		var td6 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_side.png)",
				backgroundRepeat: "repeat-y",
				backgroundPosition: "right",
				width: 4
			}
		});

		tr2.adopt(td4,td5,td6);

		var tr3 = new Element("tr");
		var td7 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_bottom_left.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "left",
				width: 4,
				height: 4
			}
		});
		var td8 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_bottom.png)",
				backgroundRepeat: "repeat-x",
				height: 4
			}
		});
		var td9 = new Element("td", {
			styles: {
				backgroundImage: "url(" + Window.Dialog.imagesPath + "help_border_bottom_right.png)",
				backgroundRepeat: "no-repeat",
				backgroundPosition: "right",
				width: 4,
				height: 4
			}
		});

		tr3.adopt(td7,td8,td9);

		tbody.adopt(tr1,tr2,tr3);

		container.adopt(content);

		new Drag.Move(box, {handle: bar});

		return box;
	}
}

if (window.ie6) {
	window.addEvent("resize",Window.Dialog.update);
	window.addEvent("scroll",Window.Dialog.update);
}

function openDialog(url, height, width, title) {
	Window.Dialog.open(url, {fragment : true}, {movable: true, top: 90, left: 185, height: height, width: width, title: title});
}
