/*
 *  This contains various functions for showing "windows" or "dialogs".
 *  Requires MooTools v.1.3.
 */
(function(){
var getViewport = function() {
	if (Browser.safari || Browser.chrome) {
		return {
			"left" : document.body.scrollLeft,
			"top" : document.body.scrollTop,
			"width" : window.innerWidth,
			"height" : window.innerHeight
		};
	} else if(Browser.opera) {
		return {
			"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
			"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
			"width" : document.body.clientWidth,
			"height" : document.body.clientHeight
		};
	} else if(Browser.ie && document.compatMode == "BackCompat") {
		return {
			"left" : document.body.scrollLeft,
			"top" : document.body.scrollTop,
			"width" : document.body.clientWidth,
			"height" : document.body.clientHeight
		};
	} else if(document.compatMode == "BackCompat") {
		return {
			"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
			"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
			"width" : document.body.clientWidth,
			"height" : document.body.clientHeight
		};
	} else {
		return {
			"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
			"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
			"width" : document.documentElement.clientWidth,
			"height" : document.documentElement.clientHeight
		};
	}
};
var Dialog = this.Dialog = new Class({

	Implements: [Options],

	options: {
		content: null,
		title: null,
		movable: true,
		close: true, 
		width: 350, 
		height: 200,
		top: "50%",
		left: "50%",
		coverColor: "#000",
		coverOpacity: 0.5,
		popup: false,
		destroyOnClose: true,
		dialogClassName: "dialog-box"
	},

	initialize: function(options){
		this.setOptions(options);
		var box = new Element("div", {
			styles : {
				position:"absolute",
				height: this.options.height,
				width: this.options.width,
				zIndex:2,
				top: this.options.top,
				left: this.options.left,
				marginTop: -(this.options.height/2),
				marginLeft: -(this.options.width/2)
			},
			'class': this.options.dialogClassName
		});
		var bar = new Element("div", {'class': "box-top", text: this.options.title == null ? "" : this.options.title});
		var container = new Element("div", {'class': "box-container"});
		box.adopt([
		    new Element("div", {'class': "box-top-left"}),
		    bar,
		    new Element("div", {'class': "box-top-right"}),
		    new Element("div", {'class': "box-left"}),
		    container,
		    new Element("div", {'class': "box-right"}),
		    new Element("div", {'class': "box-bottom-left"}),
		    new Element("div", {'class': "box-bottom"}),
		    new Element("div", {'class': "box-bottom-right"})	    
		]);
		container.adopt(this.options.content);
		if(this.options.close) {
			bar.adopt(new Element("div", {
				'class': "close-btn",
				events: {
					click: this.hide.bind(this),/*
					mouseenter: function() { this.style.backgroundImage='url(images/dialog_close_hover.png)'; },
					mouseleave: function() { this.style.backgroundImage='url(images/dialog_close.png)'; },*/
					mousedown: function() {	this.set('class', "close-btn-down"); },
					mouseup: function() { this.set('class', "close-btn"); }
				}
			}));
		}
		var cover = new Element("div", {
			styles : {
				position:"absolute",
				left: 0,
				top: 0,
				width: "100%",
				height: "100%",
				zIndex : 1,
				backgroundColor : this.options.coverColor,
				opacity : this.options.coverOpacity
			}
		});	
		this.root = new Element("div", {
			styles : {
				position: (Browser.ie6 ? "absolute" : "fixed"),
				left: 0,
				top: 0,
				width: "100%",
				height: "100%",
				zIndex: 1000,
				backgroundColor: "transparent"
			}
		});
		if(this.options.movable)
			new Drag.Move(box, {"handle": bar, container: this.root});
		if(this.options.popup)
			this.root.addEvent("click", this.hide.bind(this));
		var updateDimensions = function(event) {
			this.root.setStyles(getViewport());
		};

		if(Browser.ie6) {
			window.addEvent("resize",updateDimensions);
			window.addEvent("scroll",updateDimensions);
			updateDimensions();
		} 
		this.root.adopt(box, cover);
	},
	
	show: function() {
		$(document.body).adopt(this.root);
		this.root.setStyles({display: "block"});
		/*if(this.options.center) {
			var view = getViewport();
			var size = elm.getSize();
			var pos = {
				left : Math.max(Math.floor((view.width - size.x) / 2), 0),
				top : Math.max(Math.floor((view.height - size.y) / 2), 0)
			}
			elm.setStyles(pos);
		}*/
	},

	hide: function() {
		var elm = this.root;
		if(elm != null) {
			elm.setStyles({display: "none"});
			elm.dispose();
			if(this.options.destroyOnClose) {
				this.destroy();
			}
			/*
			if(Browser.opera) { //this fixes opera's repaint issues
				new Fx.Tween(this.cover, {
					property: "opacity", 
					duration: 200, 
					transition: Fx.Transitions.linear,
					events: { complete : function() { root.dispose();} }
				}).start(0);
			} else {
				root.dispose();
			}
			*/
		}
	},
	destroy: function() {
		var elm = this.root;
		if(elm != null) {
			elm.dispose();
			elm.destroy();
			this.root = null;
		}
	}
});

Dialog.Registry = [];

Dialog.Url = new Class({

	Extends: Dialog,

	options: {
		url: null,
		data: null,
		loadText: "Loading...",
		contentClassName: null,
		styles: null //{width: "100%", height: "100%"}
	},

	initialize: function(options) {
		this.setOptions(options);
		if(this.options.content == null) {
			this.options.content = new Element("div", {text: this.options.loadText});
			if(this.options.contentClassName)
				this.options.content.set('class', this.options.contentClassName);
			else if(this.options.styles)
				this.options.content.setStyles(this.options.styles);			
		} else {
			var tmp = $(this.options.content);
			if(tmp == null) {
				this.options.content = new Element("div", {text: this.options.content});
				if(this.options.contentClassName)
					this.options.content.set('class', this.options.contentClassName);
				else if(this.options.styles)
					this.options.content.setStyles(this.options.styles);
			} else
				this.options.content = tmp;
		}
		this.parent(this.options);
		if(this.options.register) {
			var index = this.$registryIndex = Dialog.Registry.push(this);
			switch (typeOf(this.options.data)) {
				case 'element': case 'elements': 
					this.options.data = document.id(this.options.data).toQueryString();
					//fall through
				case 'string': 
					if(this.options.data.length > 0)
						this.options.data = this.options.data + "&";
					this.options.data = this.options.data + "registryIndex=" + index;
					break;
				case 'object': case 'hash': this.options.data.registryIndex = index; break;
				case 'null': this.options.data = { registryIndex: index }; break;
			}
		}
		this.request = new Request.HTML({
			url: this.options.url, 
			update: this.options.content,
			link : "cancel",
			data : this.options.data
		});		
	},
	
	show: function(options) {
		this.parent(options);
		this.request.send(options);
	},
	
	destroy: function() {
		this.parent();
		if(this.$registryIndex) {
			Dialog.Registry[this.$registryIndex-1] = null;
			if(Dialog.Registry.length == this.$registryIndex)
				Dialog.Registry.pop();
			this.$registryIndex = null;
		}
	}
});

Dialog.Help = new Class({

	Extends: Dialog.Url,

	options: {
		url: "help.i",
		loadText: "Please wait...",
		helpKey: '',
		dialogClassName: "help-box",
		contentClassName: "help-content"
	},
	
	initialize: function(options) {
		if(options.helpKey)
			options.data = {helpKey : options.helpKey, fragment: true};
		this.parent(options);
		this.options.content.getParent().addEvent("click", this.hide.bind(this));
	}
});

})();
