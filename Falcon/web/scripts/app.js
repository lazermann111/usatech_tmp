function addReportRunner() {
	var newReportRunner = new SerialTaskRunner();
    window.addEvent('domready', function() {
        var box = $("report-ready-box");
        var container = $("report-ready-content");
        //new Drag.Move(box, {handle: box});
        window.handleNewReport = function(url, reportName) {
            newReportRunner.add(function() {
                container.empty();
                container.appendText("Report \"");
                var link = new Element("a", { href: url, text: reportName});
                container.adopt(link);
                container.appendText("\" is ready for pick-up.");
            }, 1).addFx(new Fx.Tween(box), ["opacity", 0, 1], 1).addFx(new Fx.Tween(box, {duration: 5000}), ["opacity", 1, 0], 5000);
        };
        try {
            new Request.HTML({ 
                url : "report_ajax_poll.i",
                data: {resetAjaxPollCount:true},
                update: box
            }).send();
        } catch(e) {
            box.setText("Error occurred retrieving your Report Requests");
        }
    });
}

/*
 *  This sets up various utility functions mostly for form validation and field edit masks.
 *  Requires MooTools v.1.4 and (optionally) Meio Mask v.2.0.1.0
 */
Element.implement({
	getText: function() { return this.get('text'); },		
	setText: function(text) { return this.set('text', text); },	
	setHTML: function(html) { return this.set('html', html); },		
	getHTML: function() { return this.get('html'); },
	getTag: function() { return this.get('tag'); },			
	getValue: function() { return this.get('value'); },
	setValue: function(value) { return this.set('value', value); },
	fireCustomEvent: function(type, props) {
		var evtProps = {type: type, target: this, returnValue: true};
		if(props)
			evtProps = Object.merge(props, evtProps);			
		var args = [ new DOMEvent(evtProps) ];
		var events = this.retrieve('events');
		if(events && events[type])
			return this.fireEvent(type, args);
		
		var handler = this.get("on" + type); 
		if(!handler) 
			return;
		if(typeOf(handler) != 'function') {
			handler = new Function("event", String.from(handler));
		}
		return handler.apply(this, args);
	}
});
(function() {
	var progressSupport = ('onprogress' in new Browser.Request);
	Request.implement({
		send: function(options){
			if (!this.check(options)) return this;
		
			this.options.isSuccess = this.options.isSuccess || this.isSuccess;
			this.running = true;
		
			var type = typeOf(options);
			if (type == 'string' || type == 'element') options = {data: options};
		
			var old = this.options;
			options = Object.append({data: old.data, url: old.url, method: old.method}, options);
			var data = options.data, url = String(options.url), method = options.method.toLowerCase();
		
			if (!url) url = document.location.pathname;
		
			var trimPosition = url.lastIndexOf('/');
			if (trimPosition > -1 && (trimPosition = url.indexOf('#')) > -1) url = url.substr(0, trimPosition);
		
			if (this.options.noCache)
				url += (url.contains('?') ? '&' : '?') + String.uniqueID();
		
			// Modified to handle <input type=file/> elements
			if(window.FormData && method == 'post' && typeOf(data) == "element" && (data = document.id(data)).getTag() == "form" && data.enctype == "multipart/form-data") {
				data = new FormData(data);
			} else {
				switch (typeOf(data)){
					case 'element': data = document.id(data).toQueryString(); break;
					case 'object': case 'hash': data = Object.toQueryString(data);
				}
		
				if (this.options.format){
					var format = 'format=' + this.options.format;
					data = (data) ? format + '&' + data : format;
				}
		
				if (this.options.emulation && !['get', 'post'].contains(method)){
					var _method = '_method=' + method;
					data = (data) ? _method + '&' + data : _method;
					method = 'post';
				}
		
				if (this.options.urlEncoded && ['post', 'put'].contains(method)){
					var encoding = (this.options.encoding) ? '; charset=' + this.options.encoding : '';
					this.headers['Content-type'] = 'application/x-www-form-urlencoded' + encoding;
				}
		
				if (data && method == 'get'){
					url += (url.contains('?') ? '&' : '?') + data;
					data = null;
				}
			}
			var xhr = this.xhr;
			if(progressSupport){
				xhr.onloadstart = this.loadstart.bind(this);
				xhr.onprogress = this.progress.bind(this);
			}
		
			xhr.open(method.toUpperCase(), url, this.options.async, this.options.user, this.options.password);
			if (this.options.user && 'withCredentials' in xhr) xhr.withCredentials = true;
		
			xhr.onreadystatechange = this.onStateChange.bind(this);
		
			Object.each(this.headers, function(value, key){
				try {
					xhr.setRequestHeader(key, value);
				} catch (e){
					this.fireEvent('exception', [key, value]);
				}
			}, this);
		
			this.fireEvent('request');
			xhr.send(data);
			if (!this.options.async) this.onStateChange();
			else if (this.options.timeout) this.timer = this.timeout.delay(this.options.timeout, this);
			return this;
		}
	});
})();
(function() {
	Browser.post = function(url, data, target) {
		var frm = new Element("form", { method: "post", action: String.from(url), target: target });
		if(typeOf(data) == 'element' || (typeOf(data) == 'elements')) {
			frm.adopt(data);
		} else if(typeOf(data) != 'null') {
			if(typeOf(data) != 'object')
				data = String.from(data).parseQueryString();
			Object.each(data, function(value, key) {
				frm.adopt(new Element("input", {type: "hidden", name: key, value: value}));
			});
		}
		$(document.documentElement).adopt(frm);
		frm.submit();
	};
	var containsString = function(item) {
		return String.from(item) == String.from(this);
	};
	Element.Properties.value = {
			set: function(value){
				if(this.get("tag") == "select" && (!Browser.ie|| this.multiple)) {
					var values = Array.from(value);
					Array.each(this.options, function(option){
						option.selected = values.some(containsString, option.value);
					});
				} else {
					this.value = value;
				}
			},
			get: function(){
				if(this.get("tag") == "select" && (!Browser.ie|| this.multiple)) {
					var values = [];
					Array.each(this.options, function(option){
						if(option.selected && !option.disabled)
							values.push(option.value);
					});
					switch(values.length) {
						case 0: return "";
						case 1: return values[0];
						default: return values;
					}
				} else {
					return this.value;
				}
			}		
	};/*
	if(Browser.ie) {
		Element.Properties.disabled = {
				set: function(value) {
					this.disabled = value;
					if(this.disabled) {
						this.adopt(new Element("div", {styles: { background: "red", border: "1px solid green"}}));
					} else {
						this.empty();
					}
				},
				get: function() {
					return this.disabled;
				}		
		};
	}*/
	Element.Properties.validatorProps.get = function(props){
		if (props) this.set(props);
		if (this.retrieve('$moo:validatorProps')) return this.retrieve('$moo:validatorProps');
		if (this.getProperty('data-validator-properties') || this.getProperty('validatorProps')){
			try {
				this.store('$moo:validatorProps', JSON.decode(this.getProperty('validatorProps') || this.getProperty('data-validator-properties')));
			}catch(e){
				return {};
			}
		} else {
			var vals = this.get('validators').filter(function(cls){
				return cls.test(':');
			});
			if (!vals.length){
				this.store('$moo:validatorProps', {});
			} else {
				props = {};
				vals.each(function(cls){
					var quoteIndex=cls.indexOf(':');
					if(quoteIndex>0 && cls.length>quoteIndex+1){
						var propValue=cls.substring(quoteIndex+1);
						var propName=cls.substring(0,quoteIndex);
						try {
							props[propName] = JSON.decode(propValue);
						} catch(e){}
					}
				});
				this.store('$moo:validatorProps', props);
			}
		}
		return this.retrieve('$moo:validatorProps');
	};
	
	var doSelectFn = function(option) {
		return option.fireCustomEvent("select");
	};
	var onSelectFn = function() {
		return this.getSelected().every(doSelectFn);
	};
	Form.attachOnSelect = function(select) {
		if(select.options && select.options.length && $$(select.options).some(function(option){return option.onselect || option.getAttribute("onselect");})) {					
			select.addEvent('change', onSelectFn);
			if(select.form) {
				var origSelected = select.getSelected();
				select.form.addEvent("reset", function() {origSelected.every(doSelectFn);});
			}
			select.getSelected().each(doSelectFn);
		}
	};
	
	Form.injectSessionToken = function(sessionToken, profileId) {
		var initialize = Request.prototype.initialize;
		var send = Request.prototype.send;
		var checkSessionToken = function(data) {
			switch(typeOf(data)) {
				case 'string': 
					if(!data.match(/(\&|^)session-token=/)) {
						if(data.trim().length > 0)
							data += "&";
						data += "session-token=" + sessionToken;
					}
					if(profileId && !data.match(/(\&|^)profileId=/)) {
						if(data.trim().length > 0)
							data += "&";
						data += "profileId=" + profileId;
					}
					break;
				case 'element':
					if(!data.getElements("input[name='session-token']"))
						data.adopt(new Element("input", {type: "hidden", name: "session-token", value: sessionToken}));
					if(profileId && !data.getElements("input[name='profileId']"))
						data.adopt(new Element("input", {type: "hidden", name: "profileId", value: profileId}));				
					break;
				case 'object': case 'hash': 
					if(!data["session-token"])
						data["session-token"] = sessionToken;
					if(profileId && !data["profileId"])
						data["profileId"] = profileId;
					break;
			}
			return data;
		};
		Request.implement({
			initialize: function(options) {
				if(!options)
					options = { data: {} };
				else if(!options.data) 
					options.data = {};
				options.data = checkSessionToken(options.data);
				initialize.call(this, options);
			},
			send: function(options) {
				switch(typeOf(options)) {
					case 'string': case 'element': 			
						options = checkSessionToken(options);
						break;
					case 'object': case 'hash': 
						options.data = checkSessionToken(options.data);
						break;
				}
				send.call(this, options);
			}
		});
		for(var i=0; i < document.forms.length; i++) {
			if (document.forms[i].elements["session-token"] == undefined) {
				var tokenInput = document.createElement("input");
			    tokenInput.type = "hidden";
			    tokenInput.name = "session-token";
			    tokenInput.value = sessionToken;
			    document.forms[i].appendChild(tokenInput);
			}
		}
	};
	
	Locale.define('en-US', 'FormValidator', {
		reqChkByName: "Please select at least one {label}.",
		minimum: "Please enter a value that is at least {minimum}.",
		maximum: "Please enter a value that is not more than {maximum}."
	});
	
	Form.Validator.add('minLength', {
		errorMsg: function(element, props){
			if (typeOf(props.minLength) != 'null')
				return Form.Validator.getMsg('minLength').substitute({minLength:props.minLength,length:element.get('value').length });
			else return '';
		},
		test: function(element, props){
			var value = element.getValue();
	    	if(value == null || value.length == 0)
		    	return true;
			if (typeOf(props.minLength) != 'null') return (value.length >= (props.minLength || 0));
			else return true;
		}
	});
	
	Form.Validator.add('minimum', {
		errorMsg: function(element, props){
			if (typeOf(props.minimum) != 'null')
				return Form.Validator.getMsg('minimum').substitute({minimum:props.minimum,length:element.get('value').length });
			else return '';
		},
		test: function(element, props){
			var value = element.getValue();
	    	if(value == null || value.length == 0)
		    	return true;
			if (typeOf(props.minimum) == 'null') 
				return true;
			var n = new Number(value);
			var min = new Number(props.minimum);
			return n != Number.NaN && min != Number.NaN && n >= min;
		}
	});
	
	Form.Validator.add('maximum', {
		errorMsg: function(element, props){
			if (typeOf(props.maximum) != 'null')
				return Form.Validator.getMsg('maximum').substitute({maximum:props.maximum,length:element.get('value').length });
			else return '';
		},
		test: function(element, props){
			var value = element.getValue();
	    	if(value == null || value.length == 0)
		    	return true;
			if (typeOf(props.maximum) == 'null') 
				return true;
			var n = new Number(value);
			var max = new Number(props.maximum);
			return n != Number.NaN && max != Number.NaN && n <= max;
		}
	});
	
	Form.Validator.add('validate-email', {
	    errorMsg: function(element){
	    	if(element.getProperty("validation-error-message") != null)
		    	return element.getProperty("validation-error-message");
	    	else
		    	return Form.Validator.getMsg.pass('email');
	    },
	    test: function(element){
	    	var value = element.getValue();
	    	if(value == null || value.length == 0)
		    	return true;
			var emailPat = /^(.+)@(.+)$/;
			var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
			var validChars = "\[^\\s" + specialChars + "\]";
			var quotedUser = "(\"[^\"]*\")";
			var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
			var atom = validChars + '+';
			var word = "(" + atom + "|" + quotedUser + ")";
			var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
			var domainPat = new RegExp("^" + atom + "(\\." + atom +")*$");
			var matchArray = value.match(emailPat);
			if(matchArray == null) {
				element.setProperty("validation-error-message", "The email address is not valid (check @ and .'s)");
				return false;
			}
			var user = matchArray[1];
			var domain = matchArray[2];
			if(user.match(userPat) == null) {
				element.setProperty("validation-error-message", "The email address username is not valid.");
				return false;
			}
			var IPArray = domain.match(ipDomainPat)
			if(IPArray != null) {
				for (var i = 1; i <= 4; i++) {
					if (IPArray[i] > 255) {
						element.setProperty("validation-error-message", "The email address destination IP address is invalid.");
						return false;
					}
				}
			} else {
				var domainArray = domain.match(domainPat);
				if (domainArray == null) {
					element.setProperty("validation-error-message", "The email address domain name is not valid.");
				    return false;
				}
				var atomPat = new RegExp(atom, "g");
				var domArr = domain.match(atomPat);
				var len = domArr.length;
				if (domArr[domArr.length-1].length < 2 || domArr[domArr.length-1].length > 6) {
					element.setProperty("validation-error-message", "The email address must end in a two-letter to six-letter domain.");
					return false;
				}
				if (len < 2) {
					element.setProperty("validation-error-message", "The email address is missing a hostname.");
					return false;
				}
			}
			element.setProperty("validation-error-message", null);				
			return true;
	    }
	});

	Form.Validator.add('validate-password', {
	    errorMsg: function(element){
			element = $(element);
			if(element && element.getProperty("validation-error-message"))
		    	return element.getProperty("validation-error-message");
			else
		    	return "The password does not meet the requirements. Please choose another.";
	    },
	    test: function(element){
	    	element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || value.length == 0)
		    	return true;
			var errorMsg = "";
			if(value.length < 8) {
				errorMsg = "must be at least 8 digits";
			}
		    if(!value.match(/[A-Z]/)) {
		    	if(errorMsg != "") errorMsg = errorMsg + " and ";          	
				errorMsg = errorMsg + "must contain at least 1 uppercase letter";
		    }
			if(!value.match(/[a-z]/)) {
		     	if(errorMsg != "") errorMsg = errorMsg + " and ";          	
				errorMsg = errorMsg + "must contain at least 1 lowercase letter";
			}
			if(!value.match(/[!-@\[-^`{-~]/)) {
			    if(errorMsg != "") errorMsg = errorMsg + " and ";          	
				errorMsg = errorMsg + "must contain at least 1 number or 1 punctuation symbol";
			}
			if(errorMsg != "") {
		    	element.setProperty("validation-error-message", "The password " + errorMsg + ". Please choose another.");
				return false;
			} else {
				element.setProperty("validation-error-message", null);
				return true;
			}
		}
	});

	Form.Validator.add('validate-reqchk-bynode', {
		errorMsg: function(element, props){
			return Form.Validator.getMsg('reqChkByName').substitute({label: props.label || "option"});
		},
		test: function(element, props){
			return ((props.nodeId == null ? element : document.id(props.nodeId)).getElements(props.selector || 'input[type=checkbox], input[type=radio]')).some(function(item){
				return item.checked;
			});
		},
		handlesFieldSets: true
	});
	['validate-reqchk-byname', 'validate-one-required'].each(function(item) { var v = Form.Validator.getValidator(item); if(v && v.options) v.options.handlesFieldSets = true; });
	
	var testRegex = function(regex, element, props, next) {
		element = $(element);
    	if(!element)
	    	return false;
    	var value = element.getValue();		
    	if(value == null || value.length == 0)
	    	return true;
    	var matches = value.trim().match(regex);
    	if(!matches)
    		return false;
    	return !next || next(value, element, props, matches);
	};
	var lookupPostalOptions = {
		country: null,
		cityFieldName: "city",
		stateFieldName: "state",
		countryFieldName: "country",
		timeZoneFieldName: "timeZone",
		resultElementId: "postalLookup",
		foundClass: "postal-found",
		notFoundClass: "postal-not-found",
		url: "postalLookup.i"	
	};
	var lookupPostal = function(value, element, props, matches, overrideCountry) {
		var country;
		if(!props.fieldPrefix)
			props.fieldPrefix = "";
		if(overrideCountry)
			country = overrideCountry;
		else if(props.countryFieldName)
			country = element.form[props.countryFieldName].getValue();
		else if(lookupPostalOptions.country)
			country = lookupPostalOptions.country;
		else if(lookupPostalOptions.countryFieldName)
			country = (element.form[props.fieldPrefix + lookupPostalOptions.countryFieldName] || element.form[lookupPostalOptions.countryFieldName]).getValue();
		else
			country =  null;
		var params = Object.merge({}, lookupPostalOptions, props);
		var postalCd;
		if(matches && matches != null && matches.length > 1) {
			matches.shift();
			postalCd = matches.join("");	
			if(postalCd.length == 0)
				postalCd = value;
		} else
			postalCd = value;
		new Request.JSON({
			url: params.url, 
			method: "get", 
			link: "cancel", 
			async: true, 
			onSuccess: function(result) { 
				var cityElement = element.form[props.fieldPrefix + params.cityFieldName] || element.form[params.cityFieldName];
				var stateElement = element.form[props.fieldPrefix + params.stateFieldName] || element.form[params.stateFieldName];
				var timeZoneElement = element.form[props.fieldPrefix + params.timeZoneFieldName] || element.form[params.timeZoneFieldName];
				var resultElement = $(props.fieldPrefix + params.resultElementId);
				var count = result.length;
				if(stateElement) {
					if(count > 0)
						stateElement.setValue(result[0].state);
					stateElement.readOnly = (count > 0);
					App.revalidate(stateElement);				
				}
				if(cityElement) {
					if(count > 0)
						cityElement.setValue(result[0].city);
					cityElement.readOnly = (count > 0);
					App.revalidate(cityElement);
				}
				if(count > 0 && timeZoneElement && result[0].timeZoneId > -1 && (String.from(element.defaultValue).trim() != String.from(element.value).trim() || String.from(timeZoneElement.getValue()) == "")) {
					timeZoneElement.value = result[0].timeZoneId;
				}
				if(resultElement) {
					resultElement.addClass(count > 0 ? params.foundClass : params.notFoundClass);
					resultElement.removeClass(count <= 0 ? params.foundClass : params.notFoundClass);						
				}
				if(typeof postalCityStateChanged == 'function') {
					postalCityStateChanged();
				}
			}
		}).send({data: {postalCd: postalCd, countryCd: country}});
		return true;
	};
	
	Form.Validator.addPostalValidator = function(countryCd, instructs, postalRegex, postalMask) {
		if(typeof(countryCd) != 'string')
			countryCd = String.from(countryCd);
		if(typeof(postalRegex) != 'regexp')
			postalRegex = new RegExp(postalRegex);
		if(!instructs)
			instructs = "Please enter a valid postal code";
		Form.Validator.add('validate-postalcode-' + countryCd.toLowerCase(), { 
			errorMsg: instructs, 
			test: function(element, props) {
				return testRegex(postalRegex, element, props);
			}
		});
		Form.Validator.add('validate-postalcode-lookup-' + countryCd.toLowerCase(), { 
			errorMsg: instructs, 
			next: function(value, element, props, matches) { return lookupPostal(value, element, props, matches, countryCd); },
			test: function(element, props) {
				return testRegex(postalRegex, element, props, this.next);
			}
		});
		if(postalMask) {
			var maskConf = {};
			maskConf['Postal' + countryCd.toUpperCase()] = {mask: postalMask};
			if(window.Meio) Meio.Mask.createMasks('Fixed', maskConf);
		}
	};
	
	Form.Validator.add('validate-regex', {
		errorMsg: function(element, props){
			return "The data is formatted incorrectly.";
		},
		test: function(element, props){
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || value.length == 0)
		    	return true;
	    	if(!props["validate-regex"])
	    		return true;
	    	return value.match(props["validate-regex"]);
		}
	});
	
	Form.Validator.add('validate-regex-nospace', {
		errorMsg: function(element, props){
			return "The data is formatted incorrectly.";
		},
		test: function(element, props){
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || value.length == 0)
		    	return true;
	    	value=value.replace(/\s/g, "");
	    	value=value.replace(/;/g, ",");
	    	if(!props["validate-regex-nospace"])
	    		return true;
	    	var matchResult=value.match(props["validate-regex-nospace"]);
	    	if(matchResult){
	    		element.set('value', value);
	    	}
	    	return matchResult;
		}
	});
	
	Form.Validator.add('validate-multi-dates', {
		errorMsg: function(element, props){
			if (Date.parse){
				var format = props.dateFormat || '%x';
				return Form.Validator.getMsg('dateSuchAs').substitute({date: new Date().format(format)});
			} else {
				return Form.Validator.getMsg('dateInFormatMDY');
			}
		},
		test: function(element, props){
			if (Form.Validator.getValidator('IsEmpty').test(element)) return true;
			var dateLocale = Locale.getCurrent().sets.Date,
			dateNouns = new RegExp([dateLocale.days, dateLocale.days_abbr, dateLocale.months, dateLocale.months_abbr].flatten().join('|'), 'i'),
			value = element.get('value'),
			wordsInValue = value.match(/[a-z]+/gi);
			if (wordsInValue && !wordsInValue.every(dateNouns.exec, dateNouns)) return false;
		    	value=value.replace(/;/g, ",");
		    	var valueArray = value.split(',');
		    	var formatedResult="";
		    	
		    	for(var i=0;i<valueArray.length;i++){
					var date = Date.parse(valueArray[i]),
						format = props.dateFormat || '%x',
						formatted = date.format(format);
					if (formatted == 'invalid date' || !date.isValid()) {
						return false;
					}
					formatedResult+=formatted+",";
		    	}
		    	if(formatedResult.length>1){
		    		element.set('value', formatedResult.substring(0,formatedResult.length-1));
		    	}
		    	return true;
				
		}
	});
	
	Form.Validator.add('validate-multi-hour-duration', {
		errorMsg: function(element, props){
			return 'Please enter validate hour durations.';
		},
		test: function(element, props){
			if (Form.Validator.getValidator('IsEmpty').test(element)) return true;
				value = element.get('value'),
				value=value.replace(/\s/g, "");
		    	value=value.replace(/;/g, ",");
		    	var valueArray = value.split(',');
		    	var formatedResult="";
		    	for(var i=0;i<valueArray.length;i++){
		    		if(valueArray[i].indexOf('-')==-1){
		    			return false;
		    		}else{
			    		var hourArray = valueArray[i].split('-');
			    		if(hourArray.length!=2||hourArray[0].length==0||hourArray[1].length==0){
			    			return false;
			    		}
			    		
						var date = Date.parse(hourArray[0]),
							format = 'HH:MM',
							formatted1 = date.format(format);
						if (formatted1 == 'invalid date' || !date.isValid()) {
							return false;
						}
						var date2 = Date.parse(hourArray[1]),
						format = 'HH:MM',
						formatted2 = date2.format(format);
						if (formatted2 == 'invalid date' || !date2.isValid()) {
							return false;
						}
						formatedResult+=formatted1+"-"+formatted2+",";
		    		}
		    	}
		    	if(formatedResult.length>1){
		    		element.set('value', formatedResult.substring(0,formatedResult.length-1));
		    	}
		    	return true;
				
		}
	});
	
	Form.Validator.add('validate-unique-byname', {
		errorMsg: function(element, props){
			return "Please select each option only once";
		},
		test: function(element, props){
			element = $(element);
	    	if(!element || !element.form || !element.name)
		    	return false;
	    	return element.form.getElements("[name='"+element.name+"']").every(function(item) {
	    		return (item == element || item.getValue() != element.getValue());
	    	});
		}
	});
	
	Form.Validator.add('validate-aba-us', {
		errorMsg: function(element, props) {
			return "Nine digits are required.";
		},
		test: function(element, props) {
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || (value=value.trim()).length == 0)
		    	return true;
	    	return value.match(/\d{9}/);
		}
	});
	
	Form.Validator.add('validate-aba-ca', {
		errorMsg: function(element, props) {
			return "Nine digits are required.";
		},
		test: function(element, props) {
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || (value=value.trim()).length == 0)
		    	return true;
	    	return value.match(/\d{9}/);
		}
	});
	
	Form.Validator.add('validate-cvv', {
		errorMsg: function(element, props) {
			return "Must be three or four digits.";
		},
		test: function(element, props) {
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || (value=value.trim()).length == 0)
		    	return true;
	    	return value.match(/\d{3,4}/);
		}
	});
	
	Form.Validator.add('validate-card', {
		errorMsg: function(element, props) {
			return "A valid card of 15 to 19 digits.";
		},
		test: function(element, props) {
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || (value=value.trim()).length == 0)
		    	return true;
	    	return value.match(/\s*(\d\s*){15,19}/);
		}
	});
	
	Form.Validator.add('validate-phone', {
		errorMsg: function(element, props){
			return "A telephone number such as 800-555-1212 or (800) 555-1212 or 8005551212.";
		},
		test: function(element, props){
			element = $(element);
	    	if(!element)
		    	return false;
	    	var value = element.getValue();		
	    	if(value == null || value.length == 0)
		    	return true;
	    	return value.match(/^\s*\(?\s*\d{3}\s*\)?\s*-?\s*\d{3}\s*-?\s*\d{4}\s*$/);
		}
	});
	
	Form.Validator.add('validate-positive-integer', {
		errorMsg: function(element, props) {
			return " Please enter a positive integer in this field. Numbers with decimals (e.g. 1.25) are not permitted.";
		},
		test: function(element, props) {
			return Form.Validator.getValidator('validate-integer').test(element) && element.get('value')>0;
		}
	});
	
	Form.Validator.implement({test: function(className, field, warn) {
		field = document.id(field);
		if ((this.options.ignoreHidden && !field.isVisible()) || (this.options.ignoreDisabled && (field.get('disabled') || field.getParents('fieldset').some(function(el) { return el.get('disabled');})))) return true;
		var validator = this.getValidator(className);
		if (warn != null) warn = false;
		if (this.hasValidator(field, 'warnOnly')) warn = true;
		var isValid;
		if(this.hasValidator(field, 'ignoreValidation') || !validator)
			isValid = true;
		else if(!validator.options.handlesFieldSets && "fieldset" == field.getTag().toLowerCase())
			isValid = field.getElements(this.options.fieldSelectors).every(function(childField) {
				if ((this.options.ignoreHidden && !childField.isVisible()) || (this.options.ignoreDisabled && childField.get('disabled'))) return true;
				return validator.test(childField);
			}, this);
		else
			isValid = validator.test(field);
		if (validator && field.isVisible()) this.fireEvent('elementValidate', [isValid, field, className, warn]);
		if (warn) return true;
		return isValid;
	}});
	
	if(window.Meio) {
		Meio.Mask.setRule('U', {regex: /[A-Za-z]/, check: function(value, index, _char) { return _char.toUpperCase(); }});
		
		Meio.Mask.createMasks('Fixed', {
			'TaxId'		: {mask: '99-9999999'},
			'AbaUS'		: {mask: '999999999'},
			'AbaCA'		: {mask: '999999999'}
		});
		
		Meio.Mask.createMasks('Reverse', {
			'CurrencyUS'		: {precision: 2, thousands: ',', decimal: '.'}
		});
	}
	Form.Validator.Inline.Mask = new Class({
		Extends: Form.Validator.Inline,
		options: {
			fieldSelectors: "input[type='text'], input[type='password'], input[type='file'], input[type='checkbox'], input[type='radio'], input[type='email'], input[type='tel'], select, textarea, fieldset"
		},
		initialize: function(form, options) {
			if(options == null)
				options = {serial : false};
			else if(options.serial == null)
				options.serial = false;
			this.parent(form, options);
			this.getFields().each(function(element) {
				if(element.get("tag") == "input" && element.get("type") == "text") {
					var mask = element.get('edit-mask');
					var props = element.get('edit-mask-properties');
					if(element.meiomask && mask != null && mask.length > 0) {
						props = JSON.decode(props) || {};
						if(!props.onInvalid) 
							props.onInvalid = function(){
								element.highlight("#FF0");
							};
						element.meiomask(mask, props);
					}
				} else if(element.get("tag") == "select") {					
					Form.attachOnSelect(element);
				}				 
			}, this);
			if(!form.$submit)
				form.$submit = form.submit;
			form.submit = function(action) {
				if(form.$submitted)
					return false;
				if(action)
					this.action = action;
				var event = {type: "submit", target: this, returnValue: true};
			    this.fireEvent("submit", new DOMEvent(event));
			    if(event.returnValue) {
			    	form.$submit();
			    	form.$submitted = true;
			    	return true;
			    }			
			    return false;
			};
			form.addEvent("reset", this.reset.bind(this));
			if("true" == options.detectchanges) {
				var unchangedButtons = form.getElements("input[type=button][data-unchanged-only=true],button[type=button][data-unchanged-only=true]");
				var changedButtons = form.getElements("input[type=submit],button[type=submit],input[type=reset],button[type=reset]");
				var onResetFn = function() {
					unchangedButtons.each(function(el) { el.disabled=false;});
					changedButtons.each(function(el) { el.disabled=true;});
				};
				var onChangeFn = function() {
					unchangedButtons.each(function(el) { el.disabled=true;});
					changedButtons.each(function(el) { el.disabled=false;});
				};
				form.addEvent("reset", onResetFn);
				form.addEvent("change", onChangeFn);
				if(Browser.ie && Browser.version < 9)
					this.getFields().addEvent("change", onChangeFn);
				else
					this.getFields().filter("select").addEvent("change", onChangeFn);
				
				onResetFn();
				var curr = this;
				var onMouseEnterFn = function(event) {
					if(document.activeElement != null && document.activeElement.form == form && curr.getFields().contains(document.activeElement))
						document.activeElement.blur();
				}
				unchangedButtons.each(function(el) { el.addEvent("mouseenter", onMouseEnterFn);});
				form.getElements("*[data-bluronenter=true]").each(function(el) { el.addEvent("mouseenter", onMouseEnterFn);});
			}
		},
		validateField: function(field, force) {
			if(field.readOnly)
				return true;
			return this.parent(field, force);
		}
	});
})();
(function(){
	this.SerialTaskRunner = new Class({
		$tasks: [],
		$running: false,
		$check: function() {
			if(this.$tasks.length)
				this.$next();
			else 
				this.$running = false;
		},
		initialize: function() {
			var fn = this.$check.bind(this);
			var checkFxFn = this.$checkFx = function() {
				this.removeEvent("complete", checkFxFn);
				this.$inuse = false;
				fn();
			};			
		},
		$next: function() {
			this.$loop.delay(this.$tasks[0].delay > 0 ? this.$tasks[0].delay : 1, this);
		},
		$loop: function() {
			var next = this.$tasks.shift();
			next.task.apply(this);
			if(!next.hasComplete)
				this.$check();
		},
		run: function() {
			if(this.$running || !this.$tasks.length)
				return false;
			this.$running = true;
			this.$next();
		},
		add: function(task, delay) {
			if(typeOf(task) != "function")
				task = new Function(task);
			this.$tasks.push({task: task, delay: delay});
			this.run();
			return this;
		},
		addFx: function(task, parameters, delay) {
			if(!instanceOf(task, Fx))
				task = new Fx(task);
			else if(task.$inuse)
				task = Object.clone(task);
			else
				task.$inuse = true;
			this.$tasks.push({task: task.start.pass(parameters, task), delay: delay, hasComplete: true});
			task.addEvent("complete", this.$checkFx);
			this.run();
			return this;
		}
	});
}).call(this);

(function() {
//This was copied from http://stevenlevithan.com/assets/misc/date.format.js
//and a complete listing of formats can be found @ http://blog.stevenlevithan.com/archives/date-time-format
/*
	Date Format 1.1
	(c) 2007 Steven Levithan <stevenlevithan.com>
	MIT license
	With code by Scott Trenda (Z and o flags, and enhanced brevity)
*/

/*** dateFormat
	Accepts a date, a mask, or a date and a mask.
	Returns a formatted version of the given date.
	The date defaults to the current date/time.
	The mask defaults ``"ddd mmm d yyyy HH:MM:ss"``.
*/
var dateFormat = function () {
	var	token        = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloZ]|"[^"]*"|'[^']*'/g,
		timezone     = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (value, length) {
			value = String(value);
			length = parseInt(length) || 2;
			while (value.length < length)
				value = "0" + value;
			return value;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask) {
		// Treat the first argument as a mask if it doesn't contain any numbers
		if (
			arguments.length == 1 &&
			(typeof date == "string" || date instanceof String) &&
			!/\d/.test(date)
		) {
			mask = date;
			date = undefined;
		}

		date = date ? new Date(date) : new Date();
		if (isNaN(date))
			throw "invalid date";

		var dF = dateFormat;
		mask   = String(dF.masks[mask] || mask || dF.masks["default"]);

		var	d = date.getDate(),
			D = date.getDay(),
			m = date.getMonth(),
			y = date.getFullYear(),
			H = date.getHours(),
			M = date.getMinutes(),
			s = date.getSeconds(),
			L = date.getMilliseconds(),
			o = date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4)
			};

		return mask.replace(token, function ($0) {
			return ($0 in flags) ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

//Some common format strings
dateFormat.masks = {
	"default":       "ddd mmm d yyyy HH:MM:ss",
	shortDate:       "m/d/yy",
	mediumDate:      "mmm d, yyyy",
	longDate:        "mmmm d, yyyy",
	fullDate:        "dddd, mmmm d, yyyy",
	shortTime:       "h:MM TT",
	mediumTime:      "h:MM:ss TT",
	longTime:        "h:MM:ss TT Z",
	isoDate:         "yyyy-mm-dd",
	isoTime:         "HH:MM:ss",
	isoDateTime:     "yyyy-mm-dd'T'HH:MM:ss",
	isoFullDateTime: "yyyy-mm-dd'T'HH:MM:ss.lo"
};

//Internationalization strings
dateFormat.i18n = {
	dayNames: Locale.get("Date.days_abbr").invoke("capitalize").append(Locale.get("Date.days")),
	monthNames: Locale.get("Date.months_abbr").invoke("capitalize").append(Locale.get("Date.months"))
};

//For convenience...
Date.prototype.format = function (mask) {
	return dateFormat(this, mask);
}

if(window.Pikaday)
	Pikaday.prototype.toString = function(format) {
		return typeOf(this._d) != 'date' ? '' : dateFormat(this._d, format || this._o.format);
	};
})();


if (!window.App) window.App = {};

(function() {
	App.updateClientTimestamp = function(form) {
		form = $(form);
		var cts = $(form.elements["clientTimestamp"]);
		if(!cts) {
			cts = new Element("input", { type: "hidden", name: "clientTimestamp"});
			form.adopt(cts);
		}
		var cto = $(form.elements["clientTimezoneOffset"]);
		if(!cto) {
			cto = new Element("input", { type: "hidden", name: "clientTimezoneOffset"});
			form.adopt(cto);
		}
		var d = new Date();
		cts.setValue(d.toString());	
		cto.setValue(d.getTimezoneOffset());
	}

	App.enableForm = function(form, excludeCtls) {
		form.getElements("input, textarea, select").each(function(el){
			if(!el.name || el.type == 'hidden' || (typeof(excludeCtls) == 'array' && excludeCtls.contains(el))) return;
			el.disabled = false;
		});
	}

	App.disableForm = function(form, excludeCtls) {
		form.getElements("input, textarea, select").each(function(el){
			if(!el.name || el.type == 'hidden' || (typeof(excludeCtls) == 'array' && excludeCtls.contains(el))) return;
			el.disabled = true;
		});
	} 

	App.checkPostal = function(countryCd, elem, lookup) { 
	    var validator = elem.form.get("validator");
	    validator.resetField(elem);
	    if(typeof(countryCd) != 'string')
			countryCd = String.from(countryCd);
	    if(elem.meiomask) elem.meiomask("Fixed.Postal" + countryCd.toUpperCase());
	    elem.set("data-validators", "required validate-postalcode-" + (lookup ? "lookup-" : "") + countryCd.toLowerCase());
	    validator.validateField(elem);
	}
	
	App.updateValidation = function(elem, validators, mask) {
		var validator = elem.form.get("validator");
	    validator.resetField(elem);
	    if(elem.meiomask) {
		    if(mask) {
		    	var props = elem.get('edit-mask-properties');
				props = JSON.decode(props) || {};
				if(!props.onInvalid) 
					props.onInvalid = function(){
						elem.highlight("#FF0");
					};
				elem.meiomask(mask, props);
		    } else
		    	elem.erase("meiomask");
	    }
    	if(validators)
    		elem.set("data-validators", validators);
    	else
    		elem.erase("data-validators");
        validator.validateField(elem);
	}
	
	App.revalidate = function(elem) {
		var validator = elem.form.get("validator");
	    validator.validateField(elem, true) && elem.get('validators').each(function(className){
			this.hideAdvice(className, elem);
		}, validator);
	}
})();

/*
 *  This contains various functions for showing "windows" or "dialogs".
 *  Requires MooTools v.1.3.
 */
(function(){
var getViewport = function() {
	if (Browser.safari || Browser.chrome) {
		return {
			"left" : document.body.scrollLeft,
			"top" : document.body.scrollTop,
			"width" : window.innerWidth,
			"height" : window.innerHeight
		};
	} else if(Browser.opera) {
		return {
			"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
			"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
			"width" : document.body.clientWidth,
			"height" : document.body.clientHeight
		};
	} else if(Browser.ie && document.compatMode == "BackCompat") {
		return {
			"left" : document.body.scrollLeft,
			"top" : document.body.scrollTop,
			"width" : document.body.clientWidth,
			"height" : document.body.clientHeight
		};
	} else if(document.compatMode == "BackCompat") {
		return {
			"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
			"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
			"width" : document.body.clientWidth,
			"height" : document.body.clientHeight
		};
	} else {
		return {
			"left" : Math.max(document.documentElement.scrollLeft,document.body.scrollLeft),
			"top" : Math.max(document.documentElement.scrollTop,document.body.scrollTop),
			"width" : document.documentElement.clientWidth,
			"height" : document.documentElement.clientHeight
		};
	}
};
var Dialog = this.Dialog = new Class({

	Implements: [Options],

	options: {
		content: null,
		title: null,
		movable: true,
		close: true, 
		width: 350, 
		height: 200,
		top: "50%",
		left: "50%",
		coverColor: "#000",
		coverOpacity: 0.5,
		popup: false,
		destroyOnClose: true,
		dialogClassName: "dialog-box"
	},

	initialize: function(options){
		this.setOptions(options);
		var box = new Element("div", {
			styles : {
				position:"absolute",
				height: this.options.height,
				width: this.options.width,
				zIndex:2,
				top: this.options.top,
				left: this.options.left,
				marginTop: -(this.options.height/2),
				marginLeft: -(this.options.width/2)
			},
			'class': this.options.dialogClassName
		});
		var bar = new Element("div", {'class': "box-top", text: this.options.title == null ? "" : this.options.title});
		var container = new Element("div", {'class': "box-container"});
		box.adopt([
		    new Element("div", {'class': "box-top-left"}),
		    bar,
		    new Element("div", {'class': "box-top-right"}),
		    new Element("div", {'class': "box-left"}),
		    container,
		    new Element("div", {'class': "box-right"}),
		    new Element("div", {'class': "box-bottom-left"}),
		    new Element("div", {'class': "box-bottom"}),
		    new Element("div", {'class': "box-bottom-right"})	    
		]);
		container.adopt(this.options.content);
		if(this.options.close) {
			var dialog = this;
			bar.adopt(new Element("div", {
				'class': "close-btn",
				events: {
					click: function() {	dialog.hide.apply(dialog); },/*
					mouseenter: function() { this.style.backgroundImage='url(images/dialog_close_hover.png)'; },
					mouseleave: function() { this.style.backgroundImage='url(images/dialog_close.png)'; },*/
					mousedown: function() {	this.set('class', "close-btn-down"); },
					mouseup: function() { this.set('class', "close-btn"); }
				}
			}));
		}
		var cover = new Element("div", {
			styles : {
				position:"absolute",
				left: 0,
				top: 0,
				width: "100%",
				height: "100%",
				zIndex : 1,
				backgroundColor : this.options.coverColor,
				opacity : this.options.coverOpacity
			}
		});	
		this.root = new Element("div", {
			styles : {
				position: (Browser.ie6 ? "absolute" : "fixed"),
				left: 0,
				top: 0,
				width: "100%",
				height: "100%",
				zIndex: 1000,
				backgroundColor: "transparent"
			}
		});
		if(this.options.movable)
			new Drag.Move(box, {"handle": bar, container: this.root});
		if(this.options.popup)
			this.root.addEvent("click", this.hide.bind(this));
		var updateDimensions = function(event) {
			this.root.setStyles(getViewport());
		};

		if(Browser.ie6) {
			window.addEvent("resize",updateDimensions);
			window.addEvent("scroll",updateDimensions);
			updateDimensions();
		} 
		this.root.adopt(box, cover);
	},
	
	show: function() {
		$(document.body).adopt(this.root);
		this.root.setStyles({display: "block"});
		/*if(this.options.center) {
			var view = getViewport();
			var size = elm.getSize();
			var pos = {
				left : Math.max(Math.floor((view.width - size.x) / 2), 0),
				top : Math.max(Math.floor((view.height - size.y) / 2), 0)
			}
			elm.setStyles(pos);
		}*/
	},

	hide: function() {
		var elm = this.root;
		if(elm != null) {
			elm.setStyles({display: "none"});
			elm.dispose();
			if(this.options.destroyOnClose) {
				this.destroy();
			}
			/*
			if(Browser.opera) { //this fixes opera's repaint issues
				new Fx.Tween(this.cover, {
					property: "opacity", 
					duration: 200, 
					transition: Fx.Transitions.linear,
					events: { complete : function() { root.dispose();} }
				}).start(0);
			} else {
				root.dispose();
			}
			*/
		}
	},
	destroy: function() {
		var elm = this.root;
		if(elm != null) {
			elm.dispose();
			elm.destroy();
			this.root = null;
		}
	}
});

Dialog.Registry = [];

Dialog.Url = new Class({

	Extends: Dialog,

	options: {
		url: null,
		data: null,
		loadText: "Loading...",
		contentClassName: null,
		styles: null //{width: "100%", height: "100%"}
	},

	initialize: function(options) {
		this.setOptions(options);
		if(this.options.content == null) {
			this.options.content = new Element("div", {text: this.options.loadText});
			if(this.options.contentClassName)
				this.options.content.set('class', this.options.contentClassName);
			else if(this.options.styles)
				this.options.content.setStyles(this.options.styles);			
		} else {
			var tmp = $(this.options.content);
			if(tmp == null) {
				this.options.content = new Element("div", {text: this.options.content});
				if(this.options.contentClassName)
					this.options.content.set('class', this.options.contentClassName);
				else if(this.options.styles)
					this.options.content.setStyles(this.options.styles);
			} else
				this.options.content = tmp;
		}
		this.parent(this.options);
		if(this.options.register) {
			var index = this.$registryIndex = Dialog.Registry.push(this);
			switch (typeOf(this.options.data)) {
				case 'element': case 'elements': 
					this.options.data = document.id(this.options.data).toQueryString();
					//fall through
				case 'string': 
					if(this.options.data.length > 0)
						this.options.data = this.options.data + "&";
					this.options.data = this.options.data + "registryIndex=" + index;
					break;
				case 'object': case 'hash': this.options.data.registryIndex = index; break;
				case 'null': this.options.data = { registryIndex: index }; break;
			}
		}
		this.request = new Request.HTML({
			url: this.options.url, 
			update: this.options.content,
			link : "cancel",
			data : this.options.data
		});		
	},
	
	show: function(options) {
		if(this.options.content != null && this.options.loadText != null) {
			$(this.options.content).empty();
			$(this.options.content).set("text", this.options.loadText);
		}
		this.parent(options);
		this.request.send(options);
	},
	
	destroy: function() {
		this.parent();
		if(this.$registryIndex) {
			Dialog.Registry[this.$registryIndex-1] = null;
			if(Dialog.Registry.length == this.$registryIndex)
				Dialog.Registry.pop();
			this.$registryIndex = null;
		}
	}
});

Dialog.Help = new Class({

	Extends: Dialog.Url,

	options: {
		url: "help.i",
		loadText: "Please wait...",
		helpKey: '',
		dialogClassName: "help-box",
		contentClassName: "help-content"
	},
	
	initialize: function(options) {
		if(options.helpKey)
			options.data = {helpKey : options.helpKey, fragment: true};
		this.parent(options);
		this.options.content.getParent().addEvent("click", this.hide.bind(this));
	}
});

})();

function promptDialog(titleStr, urlStr){
	new Dialog.Url({title: titleStr, url: urlStr, height: 110, width: 300, register: true}).show();
}

function popUp(name, url, height, width, modal) {
	if(modal) {
		if(window.showModalDialog)
	 		window.showModalDialog(url, null,"status=no;dialogHeight=" + height + "px;dialogWidth="+width+"px;");
	 	else {
		 	var win = window.open(url, name, "modal,status=no,resizable,height="+height+",width="+width);
			win.focus();
		}
	} else {
		var win = window.open(url, name, "resizable,status=no,height="+height+",width="+width);
		win.focus();
	}
}

function trim(s) {
	return s.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '');
}

function invalidInput(elem, msg) {
	alert(msg);
	elem.focus();
	elem.addClass("invalidValue");
	elem.select();
}

function validateUSATForm(form) {
	for(var i = 0; i < form.elements.length; i++) {
		if(form.elements[i].type=='hidden')
			continue;
		var elem = $(form.elements[i]);
		var label = elem.getAttribute("label");
		if(typeof(label) == "string" && label.length > 0 && !elem.disabled) {
			if(form.getAttribute("bulk") == "true") {
				var cb = document.getElementById("cb_" + elem.id);
				if(cb && !cb.checked)
					continue;
			}
			var value = elem.value;
			var required = elem.getAttribute("usatRequired"); 
			if(typeof(required) == "string" && "true" == required && value.length == 0) {
				alert(label + " is required. Please enter a value.");
				elem.focus();
				elem.addClass("invalidValue");
				if (form.elements[i].type == 'text')
					elem.select();
				return false;
			}
			if(typeof(value) == "string") {
				/* regex match */
				var valid = elem.getAttribute("valid");
				if(typeof(valid) == "string" && (valid=trim(valid)).length > 0 && value.length > 0) {
					if(!value.match(valid)) {
						alert(label + " is not valid. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					}
				}
				/*less than or equal to max*/
				var maxValue = elem.getAttribute("maxValue");
				if(value.length > 0 && typeof(maxValue) == "string" && (maxValue=trim(maxValue)).length > 0) {
					if(!value.match(/^\-?\d+(\.\d+)?$/)) {
						alert(label + " is not a number. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					} else if(parseFloat(value) > parseFloat(maxValue)) {
						alert(label + " must be less than or equal to " + maxValue + ". Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					}
				}
				/* greater than or equal to min*/
				var minValue = elem.getAttribute("minValue");
				if(value.length > 0 && typeof(minValue) == "string" && (minValue=trim(minValue)).length > 0) {
					if(!value.match(/^\-?\d+(\.\d+)?$/)) {
						alert(label + " is not a number. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					} else if(parseFloat(value) < parseFloat(minValue)) {
						alert(label + " must be greater than or equal to " + minValue + ". Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					}
				}
			}
			elem.removeClass("invalidValue");
		}
	}
	return true;
}

function checkAll(cbAll, cbNames) {
	var arrCbNames = cbNames.split(",");
	for (var i = 0; i < arrCbNames.length; i++) {
		var cbs = cbAll.form.elements[arrCbNames[i]];
		if (cbs) {
			if(typeof(cbs.length) == "undefined") cbs = new Array(cbs);
			for (var j = 0; j < cbs.length; j++) {
				cbs[j].checked = cbAll.checked;
			}
		}
	}
}

function formatDate(date, format) {
	if (format == "MM/dd/yyyy") {
		var mm = (date.getMonth() + 1).toString();         
	    var dd  = date.getDate().toString();  
		var yyyy = date.getFullYear().toString();
		return (mm[1] ? mm : "0" + mm) + "/" + (dd[1] ? dd : "0" + dd) + "/" + yyyy;
	} else
		return date;
}

//sorting function----
(function() {
	var sortMults = [1, -1];
	var sorting = false;
	window.Sorter = new Class({
		Implements: Options,
	    
		options: {
			sortType: "STRING",
			headerClasses: ["sortASC", "sortDESC"],
			sortValueAttribute: 'data-sort-value',
			compareFunctions: {
					number: function(v1,v2) {
						try {
							return parseFloat(v1) - parseFloat(v2);
						} catch(e) {
							return v1.localeCompare(v2);
						}
					},
					date: function(v1,v2) {
						try {
							return new Date(v1).valueOf() - new Date(v2).valueOf();
						} catch(e) {
							return v1.localeCompare(v2);
						}
					},
					string: function(v1,v2) {
						return v1.localeCompare(v2);
					},
					other: function(v1,v2) {
						return v1.localeCompare(v2);
					}			
			}
		},
		
		initialize: function(el, options) {
			this.setOptions(options);
			var self = this;
			el.addEvent("click", function() {
				if(sorting)
					return;
				sorting = true;
				$(document.body).setStyle("cursor", "wait");
				window.status = "Sorting column. Please wait...";
				setTimeout(self.doSort.bind(self, this), 1);
			});
		},
		//sortOrder: 0 = none, 1 = ASC, 2 = DESC
		styleHeaders: function(tr, sortedTh) {
			Array.each(tr.cells, function(item) { 
				Array.each(this.options.headerClasses, function(cls) {
					$(item).removeClass(cls);
				}); 
			}, this);
			sortedTh.addClass(this.options.headerClasses[sortedTh.sortOrder - 1]);
		},
		
		sortRaw: function(tr1, tr2, columnIndex) {
			var v1 = tr1.cells.item(columnIndex).getAttribute(this.options.sortValueAttribute);
			var v2 = tr2.cells.item(columnIndex).getAttribute(this.options.sortValueAttribute);
			//alert("Value1=" + v1 + " (1cc=" + v1.charCodeAt(0) + "); Value2=" + v2 + " (1cc=" + v2.charCodeAt(0) + ")");
			if(v1 == null || v1.trim().length == 0) return v2 == null ? 0 : -1;
			else if(v2 == null || v2.trim().length == 0) return 1;
			var comp = this.options.compareFunctions[String.from(this.options.sortType).trim().toLowerCase()] || this.options.compareFunctions.other;
			return comp(v1,v2);
		},
		
		doSort: function(el) {
			try {
				var sortHeader = $(el).getParent("th");
				if(!sortHeader.sortOrder) {
					if(sortHeader.hasClass("sortASC"))
						sortHeader.sortOrder = 1;
					else if(sortHeader.hasClass("sortDESC"))
						sortHeader.sortOrder = 2;
					else
						sortHeader.sortOrder = 0;
				}
				var sortOrder = 2 - ((sortHeader.sortOrder + 1) % 2);
				sortHeader.sortOrder = sortOrder;
				var tr = $(sortHeader.parentNode);
				this.styleHeaders(tr, sortHeader);
				var tbody;
				if(tr.getParent().get("tag") == "table")
					tbody = tr.getNext("tbody");
				else
					tbody = tr.getParent().getNext("tbody");
				if(tbody == null)
					return; // nothing to sort
				while(!tbody.isVisible()) {
					tbody = tbody.getNext("tbody");
					if(tbody == null)
						return; // nothing to sort
				}
				var start = 0;
				if(tr.parentNode == tbody) {
					start = tr.sectionRowIndex + 1;
				}
				if(!tbody.sortingArray) {
					tbody.sortingArray = new Array();
					for(var i = start; i < tbody.rows.length; i++) tbody.sortingArray.push(tbody.rows[i]);
				}
				var arr = tbody.sortingArray;
				var sortFn = this.sortRaw.bind(this);
				arr.sort(function(tr1,tr2) {
					return sortMults[sortOrder - 1] * sortFn(tr1, tr2, sortHeader.cellIndex);
				});
				//arrange on table
				var re = /(.*)(?:(?:odd)|(?:even))(.*)/i;
				if(tbody.moveRow) { // for IE - for some reason it is much faster to do this first for IE
					for(var i = start; i - start < arr.length; i++) {
						tbody.removeChild(arr[i-start]);
					}
				}
				for(var i = start; i - start < arr.length; i++) {
					if(arr[i-start].className) {
						var mtch = arr[i-start].className.match(re);
						if(mtch) arr[i-start].className = mtch[1] + ((i-start) % 2 == 1 ? "even" : "odd") + mtch[2];
					}
					tbody.appendChild(arr[i-start]);
				}
			} catch(e) {
				alert("Error while sorting: " + e.message + " - " + (e.number & 0xFFFF) + " - " + e.description);
			} finally {
				$(document.body).setStyle("cursor", "default");
				window.status = "Done";
				sorting = false;
			}
		}
	});
	window.Toggles = new Class({
		mappings: null,
		
		initialize: function(mappings) {
			this.mappings = mappings;	
		},
		
		apply: function(parent) {
			if(!parent)
				parent = document.body;
			var mappings = this.mappings;
			$(parent).getElements("*[data-toggle]").each(function(el) {
				var options = mappings[el.getAttribute("data-toggle")];
				if(!options || !options.applicator)
					return;
				if(!options.getOptions) {
					if(options.map)
						options.getOptions = function(el) {
					    	var ops = {};
					    	Object.each(options.map, function(fn, key) {
					    		var v = fn(el);
					    		if(v != null)
					    			ops[key] = v;
					    	});
					    	return ops;
						};
					else
						options.getOptions = function(el) {
					    	var ops = {};
					    	Array.each(el.attributes, function(att, key) {
					    		if("data-toggle" != att.name && /^data-/.test(att.name) && att.value != null) 
					    			ops[att.name.substr(5).camelCase()] = att.value;
					    	});
					    	return ops;
						};
				}
				var ops = options.getOptions(el);
				options.applicator(el, ops);
			});
		}
	});
	/*
	options: {
		toggle: "value", // the value of the data-toggle attribute; either this or selector is REQUIRED
		map: {}, // attribute to class options
		getOptions: function(el) { }, // to get the options
		applicator: function(el, ops) { }, // to do the creation; REQUIRED
		selector: "*[" + toggle + "]" // to get the elements to apply it to
	},*/
	var elementValidateHandler = function(valid, field, className, warn) {
		if(!valid) {
			var validator = this.getValidator(className);
			var msg = validator.getError(field);
			if(msg) {
				var stored = field.retrieve("validation-error-msg");
				if(!stored) {
					stored = new Object();
					field.store("validation-error-msg", stored);	
				}
				stored[className] = msg;
				var errorFields = field.form.retrieve("validation-error-fields");
				if(!errorFields) {
					errorFields = new Array();
					field.form.store("validation-error-fields", errorFields);
				}
				if(!errorFields.contains(field))
					errorFields.push(field);
			}
		} else {
			var stored = field.retrieve("validation-error-msg");
			if(stored)
				delete stored[className];
			if(!stored || stored.length == 0) {
				var errorFields = field.form.retrieve("validation-error-fields");
				if(errorFields)
					errorFields.erase(field);
			}
		}
	};
	var popupValidateHandler = function(valid, form, event) {
		if(!valid) {
			var errorFields = form.retrieve("validation-error-fields");
			var msg;
			if(!errorFields || errorFields.length == 0) {
				msg = "Please fill out the form correctly";
			} else {
				msg = "";
				errorFields.each(function(item) {
					var stored = item.retrieve("validation-error-msg");
					if(stored) {
						var prefix = item.getAttribute("data-label");
						if(prefix)
							prefix = prefix + ": ";
						else
							prefix = "";
						Object.each(stored, function(item) {
							if(msg)
								msg = msg + '\n';
							msg = msg + prefix + item;
						});
					}
				});
			}
			alert(msg);
			if(event)
				event.stop();
		}
	};
	window.TogglesInstance = new Toggles({
		/*
		popover: {
			applicator: function(el, ops) { 
				if(!ops.location)
					ops.location = ops.placement;
				if(ops.offset) {
					var m;
					if(typeOf(ops.offset) == 'string' && (m=ops.offset.match(/^\s*\{\s*(-?\d+(?:\.\d+)?)\s*,\s*(-?\d+(?:\.\d+)?)\s*\}\s*$/))) {
						ops.offset = {x: parseInt(m[1]), y: parseInt(m[2])};
					} else {
						ops.offset = parseInt(ops.offset);
						if(isNaN(ops.offset))
							delete ops.offset;
					}
				}
				if(ops.title)
					ops.getTitle = function() { return ops.title; };
				delete ops.content;
				new Bootstrap.Popover(el, ops); 
			}
		},
		carousel: {
			applicator: function(el, ops) { 
				ops.container = el;
				["distance","scroll","current"].each(function(item) { if(ops[item]) ops[item] = parseInt(ops[item]);});
				new Carousel.Extra(ops); 
			}
		},*/
		calendar: {
			applicator: function(el, ops) {
				if(typeOf(ops.trigger) == "null") {
					ops.button = new Element("img", { src: "/images/calendar.gif", "class": "calendarIcon", title: "Date selector"});
					ops.button.inject(el, 'after');
				} else if(ops.trigger != "false")
					ops.button = $(ops.trigger);
				delete ops.trigger;
				ops.inputField = el;
				if(!ops.ifFormat && ops.format)
					ops.ifFormat = ops.format;
				if(!ops.daFormat && ops.format) 
					ops.daFormat = ops.format;
				if(ops.format)
					delete ops.format;
				if(!ops.align)
					ops.align = "B2";
				if(ops.ifFormat && ops.ifFormat.match(/%./g).some(function(w) {
					switch(w) {
						case "%H":
					    case "%I":
					    case "%k":
					    case "%l":
					    case "%P":
					    case "%p":
						case "%M":
							return true;
						default:
							return false;
					}
				}))
					ops.showsTime = true;
				
				Calendar.setup(ops);
			}
		},
		expand: {
			applicator: function(el, ops) { 
				["duration"].each(function(item) { if(ops[item]) ops[item] = parseInt(ops[item]);});
				var fx = new Fx.Reveal($(ops.target), ops);
				fx.addEvent("start", function() { 
					if(this.showing) 
						el.addClass("expanded");
					else
						el.removeClass("expanded");
				});
				el.addEvent("click", fx.toggle.bind(fx));
			}
		},
		sort: {
			applicator: function(el, ops) {
				new Sorter(el, ops);
			}
		},
		validate: {
			applicator: function(el, ops) {
				new Form.Validator(el, Object.append({
					evaluateFieldsOnBlur: false,
					evaluateFieldsOnChange: false,
					onFormValidate: popupValidateHandler,
					onElementValidate: elementValidateHandler
				}, ops));
			}
		},
		validateInline: {
			applicator: function(el, ops) {
				new Form.Validator.Inline.Mask(el, ops);
			}
		},
		tip: {
			applicator: function(el, ops) {
				["offset", "windowPadding", "positions"].each(function(key) { if(ops[key]) ops[key] = JSON.decode(ops[key]);});
				var tips = new Tips(el, ops);
				if(ops.showOnClick) {
					var on = false;
					el.addEvent("click", function() { 
						if(on)
							tips.hide(el);
						else
							tips.show(el);
						on = !on;
					});
				}
			}
		}
	});

	Request.HTML.implement({success: function(text){
		var options = this.options, response = this.response;

		response.html = text.stripScripts(function(script){
			response.javascript = script;
		});

		var match = response.html.match(/<body[^>]*>([\s\S]*?)<\/body>/i);
		if (match) response.html = match[1];
		var temp = new Element('div').set('html', response.html);

		response.tree = temp.childNodes;
		response.elements = temp.getElements(options.filter || '*');

		if (options.filter) response.tree = response.elements;
		if (options.update){
			var update = document.id(options.update).empty();
			if (options.filter) update.adopt(response.elements);
			else update.set('html', response.html);
			TogglesInstance.apply(update);
		} else if (options.append){
			var append = document.id(options.append);
			if (options.filter) response.elements.reverse().inject(append);
			else append.adopt(temp.getChildren());
			TogglesInstance.apply(temp);
		}
		if (options.evalScripts) Browser.exec(response.javascript);

		this.onSuccess(response.tree, response.elements, response.html, response.javascript);
	}});
})();
window.addEvent("domready", function() {
	TogglesInstance.apply(document.body);
});
