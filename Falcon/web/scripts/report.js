//functions from common.js - so that no dependency exists
function scrollToById(id) {
	document.getElementById(id).scrollIntoView();
}

function trim(s) {
    return s.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '');
}
function getContainingTable(tobj) {
	return getContainingTag(tobj, "TABLE");
}

function getContainingTag(tobj, tagName) {
	while(tobj != null && tobj.tagName.toUpperCase() != tagName.toUpperCase()) {
		tobj = tobj.parentNode;
	}
	return tobj;
}

function popUp(name, url, height, width, modal) {
	if(modal) {
		if(window.showModalDialog)
	 		window.showModalDialog(url, null,"status=no;dialogHeight=" + height + "px;dialogWidth="+width+"px;");
	 	else {
		 	var win = window.open(url, name, "modal,status=no,resizable,height="+height+",width="+width);
			win.focus();
		}
	} else {
		var win = window.open(url, name, "resizable,status=no,height="+height+",width="+width);
		win.focus();
	}
}

// report column sorting functions
function styleHeaders(tr, sortedTh) {
	for(var i = 0; i < tr.cells.length; i++) {
		var th = tr.cells[i];
		if(th == sortedTh) {
			th.className = "sort" + th.sortOrder;
		} else th.className = "";
	}
}

var sorting = false;
var sortHeader = null;
function sortData(anch, sortType) {
	if(sorting) {
		return;
	}
	sorting = true;
	window.document.body.style.cursor = "wait";
	window.status = "Sorting column. Please wait...";
	sortHeader = getContainingTag(anch, "TH");
	setTimeout("doSort('" + sortType + "');", 1);
}

function doSort(sortType) {
	try {
		var sortOrder = sortHeader.sortOrder;
		if(!sortOrder) sortOrder = "ASC";
		else if(sortOrder == "ASC") sortOrder = "DESC";
		else sortOrder = "ASC";
		sortHeader.sortOrder = sortOrder;
		var tr = sortHeader.parentNode;
		styleHeaders(tr, sortHeader);
		var table = getContainingTable(tr.parentNode);
		if(table.rows.length == tr.rowIndex + 1) return; //if the last row, do nothing
		//get the section of the next row in the table
		var tbody = table.rows[tr.rowIndex + 1].parentNode;
		var start = 0;
		if(tr.parentNode == tbody) {
			start = tr.sectionRowIndex + 1;
		}
		if(!tbody.sortingArray) {
			tbody.sortingArray = new Array();
			for(var i = start; i < tbody.rows.length; i++) tbody.sortingArray.push(tbody.rows[i]);
		}
		var arr = tbody.sortingArray;
		var fn = getSortFunction(sortType, sortHeader.cellIndex, sortOrder);
		arr.sort(fn);
		//arrange on table
		var re = /(.*)(?:(?:odd)|(?:even))(.*)/i;
		if(tbody.moveRow) { // for IE - for some reason it is much faster to do this first for IE
			for(var i = start; i - start < arr.length; i++) {
				tbody.removeChild(arr[i-start]);
			}
		}
		for(var i = start; i - start < arr.length; i++) {
			if(arr[i-start].className) {
				var mtch = arr[i-start].className.match(re);
				if(mtch) arr[i-start].className = mtch[1] + ((i-start) % 2 == 1 ? "even" : "odd") + mtch[2];
			}
			tbody.appendChild(arr[i-start]);
		}
	} catch(e) {
		alert("Error while sorting: " + e.message + " - " + (e.number & 0xFFFF) + " - " + e.description);
	} finally {
		window.document.body.style.cursor = "default";
		window.status = "Done";
		sorting = false;
	}
}

function getSortFunction(sortType, columnIndex, sortOrder) {
	if(!sortOrder) sortOrder = "ASC";
	return new Function("tr1", "tr2", "return " +
		(sortOrder.toUpperCase() == "DESC" ? "-" : "") + "sort(tr1, tr2, '"
		+ sortType + "', " + columnIndex + ");");
}

function sort(tr1, tr2, sortType, columnIndex) {
	var v1 = getSortValue(tr1.cells.item(columnIndex));
	var v2 = getSortValue(tr2.cells.item(columnIndex));
	//alert("Value1=" + v1 + " (1cc=" + v1.charCodeAt(0) + "); Value2=" + v2 + " (1cc=" + v2.charCodeAt(0) + ")");
	if(v1 == null || trim(v1).length == 0) return v2 == null ? 0 : -1;
	else if(v2 == null || trim(v2).length == 0) return 1;
	else if(sortType == "NUMBER") {
		try {
			return parseFloat(v1) - parseFloat(v2);
		} catch(e) {
			return v1.localeCompare(v2);
		}
	} else if(sortType == "DATE") {
		try {
			return new Date(v1).valueOf() - new Date(v2).valueOf();
		} catch(e) {
			return v1.localeCompare(v2);
		}
	} else {
		return v1.localeCompare(v2);
	}
}

function getSortValue(elem) {
	return elem.getAttribute('sortValue');
}
function setupPageNavigation() {
	window.addEvent("domready", function() {
		var element = $("page-navigation-text");
		if(element && element.meiomask) {
			element.meiomask("Reverse.Integer",  {thousands: ''});
			if(element.onchange) {
				var func = typeof(element.onchange) == 'function' ? element.onchange : new Function("event", String.from(element.onchange));
				element.addEvent("change", func);
				element.addEvent("keypress", function(event) {
					if(13 == event.code) {
						element.blur();
					}
				});
			}
		}
	});
}