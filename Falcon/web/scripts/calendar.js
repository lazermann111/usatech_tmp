/* new and improved calendar picker
 * Requires MooTools v.1.11.
 *
 * Author: Bill Leonard, Brian Krug
 * Date: 1/15/2008
 */
/* Functionality - February 5th, 2008
 * Rewritted for MooTools 1.3 - March, 2011 (BSK)
	
 		Firefox 		- displays and behaves correctly
 		Netscape		- displays and behaves correctly
 		IE7				- displays and behaves correctly
		Opera			- displays and behaves correctly
		Safari2			- NOT TESTED
		Safari/Konquer 	- NOT TESTED
		IE6         	- displays and behaves correctly
*/
(function() {	
	Dialog.Calendar = new Class({

		Extends: Dialog,

		options: {
			update: null,
			dateFormat: "mm/dd/yyyy",
			width: (Browser.ie7 ? 250 : Browser.ie ? 210 : Browser.safari || Browser.chrome ? 201 : 210),
			height: (Browser.ie7 ? 260 : Browser.ie ? 255 : Browser.chrome ? 260 : 260),
			destroyOnClose: false
		},

		initialize: function(options) {
			var self = this;
			var controls = this.controls = {};
			controls.year = new Element("input", {
				events: {
					change: function() {
						var year = this.getValue();
						if(year.length == 4) {
							self.showMonth({year: year, month: self.controls.month.getValue()});
						}
					}
				},
				name: "year",
				type: "text",
				size: 4,
				maxLength: 4
			});
			controls.month = new Element("select", {
		    	events: {
		    		change:function() {
		    			self.showMonth({year: self.controls.year.getValue(), month: this.getValue()});
		    		}
		    	},
		    	name: "month"
		    });
			controls.days = new Array(42);
		    var monthNames = Locale.get("Date.months");
		    for(var i = 0; i < monthNames.length; i++) {
		    	controls.month.adopt(new Element("option", {
	    			value: i,
	    			text: monthNames[i]
		    	}));
			}
		    var grid = new HtmlTable({
				properties: {cellspacing: 1, align: "center", 'class': "calendar"}
		    });
			var table = new HtmlTable({
				properties: { 'class': "calendar-form" },
				rows: [
				       [[controls.month,controls.year]],
				       [[
							new Element("input", {
								events: {
									click: function() {
										var year = Number.from(self.controls.year.getValue());
										if(year!= null) {
											self.showMonth({year: year-1, month: self.controls.month.getValue()});
										}
									}
								},
								type: "button",
								name: "previousYear",
								value: "<<",
								title: "Previous Year",
								'class': "calendar-button"
							}),
							new Element("input", {
								events: {
									click: function() {
										var month = Number.from(self.controls.month.getValue());
										var year = Number.from(self.controls.year.getValue());										
										if(month == 0) {
											month = 11;
											year--;
										} else {
											month--;
										}
										self.showMonth({year: year, month: month});
									}
								},
								type: "button",
								name: "previousMonth",
								value: "<",
								title: "Previous Month",
								'class': "calendar-button"
							}),
							new Element("input", {
								events: {
									click: function() {
										self.setDate(new Date());
										self.returnDate();
									}
								},
								type: "button",
								name: "today",
								value: "Today",
								title: "Today",
								'class': "calendar-button"
							}),
							new Element("input", {
								events: {
									click: function() {
										var month = Number.from(self.controls.month.getValue());
										var year = Number.from(self.controls.year.getValue());										
										if(month == 11) {
											month = 0;
											year++;
										} else {
											month++;
										}
										self.showMonth({year: year, month: month});		
									}
								},
								type: "button",
								name: "nextMonth",
								value: ">",
								title: "Next Month",
								'class': "calendar-button"
							}),
							new Element("input", {
								events: {
									click: function() {
										var year = Number.from(self.controls.year.getValue());										
										if(year != null) {
											self.showMonth({year: year+1, month: self.controls.month.getValue()});
										}
									}
								},
								type: "button",
								name: "nextYear",
								value: ">>",
								title: "Next Year",
								'class': "calendar-button"
							})
						]],
						[$(grid)]
					]
			});
		
			var weekdayNames = Locale.get("Date.days");
			var weekdayElements = new Array();
			for(var i = 0; i < 7; i++)
				weekdayElements[i] = {content: weekdayNames[i].substr(0, 2), title: weekdayNames[i]};
			grid.set("headers", weekdayElements);
			var dayElements = new Array(7);
			var clickFn = function() {
				var day = this.get("text");
				if(day && day.length > 0) {
					self.setDate(new Date(self.controls.year.getValue(), self.controls.month.getValue(), day));
					self.returnDate();
				}
			};
			for(var i = 0; i < 6 ; i++) {
				for(var k = 0; k < 7 ; k++) {
					dayElements[k] = controls.days[i*7+k] = new Element("td", {
						events: {
							click: clickFn
						}
					});
				}
				grid.push(dayElements);
			}
			var content = new Element("div");
			content.adopt($(table));
			options.content = content;
						
			this.parent(options);
			this.now = new Date();
			var current;
			if(options.update && options.update.value) {
				current = Date.parse(options.update.value);
				if(!Date.isValid(current))
					current = this.now;
			} else
				current = this.now;
			this.setDate(current);
		},
		
		show: function(options) {
			this.parent();
		},
		
		getDate: function() {
			if(this.date == null) {
				this.setDate();
			}
			return this.date;
		},
		
		setDate: function(date) {
			if(date == null)
				date = this.now;
			this.date = date;
			this.showMonth({year: date.getFullYear(), month: date.getMonth()});
		},
		
		showMonth: function(options) {
			this.controls.year.setValue(options.year);
			this.controls.month.setValue(options.month);
			var d = new Date(options.year, options.month, 1);
			var totalDays = d.get('lastdayofmonth');
			var firstDay = d.getDay();
			
			var day = 0;
			for (var i = 0 ; i < 42 ; i++) {
				if (i >= firstDay && i <= (firstDay + totalDays - 1)) {
					this.controls.days[i].set("text", ++day);
					if(options.year == this.date.getFullYear() && options.month == this.date.getMonth() && day == this.date.getDate()) {
						this.controls.days[i].addClass("calendar-special");
						this.controls.days[i].removeClass("calendar-nodate");
					} else {
						this.controls.days[i].removeClass("calendar-special");
						this.controls.days[i].removeClass("calendar-nodate");
					}

					var title;
					if(options.year == this.now.getFullYear() && options.month == this.now.getMonth() && day == this.now.getDate()) {
						title = "Today";
					} else {
						title = new Date(options.year, options.month, day).format(this.options.dateFormat);
					}
					this.controls.days[i].set("title", title);
				} else {
					this.controls.days[i].removeClass("calendar-special");
					this.controls.days[i].addClass("calendar-nodate");

					this.controls.days[i].set("text","");
					this.controls.days[i].set("title", "");
				}
			}
		},
		
		returnDate: function() {
			if(this.options.update) {
				if(!Browser.ie) this.options.update.setValue(this.date.format(this.options.dateFormat));
				else this.options.update.value = this.date.format(this.options.dateFormat);
				this.options.update.focus();
			}
		    this.hide();
		}
	});
})();

function openCalendar(fieldToUpdate, width, height) {
	var options = {update:document.getElementById(fieldToUpdate)};
	if(width)
		options.width = width;
	if(height)
		options.height = height;
	var cal = new Dialog.Calendar(options);
	cal.show();
}
