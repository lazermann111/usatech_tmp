/*
---

name: Locale.sv-SE.Date

description: Date messages for Swedish.

license: MIT-style license

authors:
  - Martin Lundgren

requires:
  - /Locale

provides: [Locale.sv-SE.Date]

...
*/

Locale.define('sv-SE', 'Date', {

	months: ['januari', 'februari', 'mars', 'april', 'maj', 'juni', 'juli', 'augusti', 'september', 'oktober', 'november', 'december'],
	months_abbr: ['jan', 'feb', 'mar', 'apr', 'maj', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
	days: ['s�ndag', 'm�ndag', 'tisdag', 'onsdag', 'torsdag', 'fredag', 'l�rdag'],
	days_abbr: ['s�n', 'm�n', 'tis', 'ons', 'tor', 'fre', 'l�r'],

	// Culture's date order: YYYY-MM-DD
	dateOrder: ['year', 'month', 'date'],
	shortDate: '%Y-%m-%d',
	shortTime: '%H:%M',
	AM: '',
	PM: '',
	firstDayOfWeek: 1,

	// Date.Extras
	ordinal: '',

	lessThanMinuteAgo: 'mindre �n en minut sedan',
	minuteAgo: 'ungef�r en minut sedan',
	minutesAgo: '{delta} minuter sedan',
	hourAgo: 'ungef�r en timme sedan',
	hoursAgo: 'ungef�r {delta} timmar sedan',
	dayAgo: '1 dag sedan',
	daysAgo: '{delta} dagar sedan',

	lessThanMinuteUntil: 'mindre �n en minut sedan',
	minuteUntil: 'ungef�r en minut sedan',
	minutesUntil: '{delta} minuter sedan',
	hourUntil: 'ungef�r en timme sedan',
	hoursUntil: 'ungef�r {delta} timmar sedan',
	dayUntil: '1 dag sedan',
	daysUntil: '{delta} dagar sedan'

});


/*
---

name: Locale.sv-SE.Form.Validator

description: Form Validator messages for Swedish.

license: MIT-style license

authors:
  - Martin Lundgren

requires:
  - /Locale

provides: [Locale.sv-SE.Form.Validator]

...
*/

Locale.define('sv-SE', 'FormValidator', {

	required: 'F�ltet �r obligatoriskt.',
	minLength: 'Ange minst {minLength} tecken (du angav {length} tecken).',
	maxLength: 'Ange h�gst {maxLength} tecken (du angav {length} tecken). ',
	integer: 'Ange ett heltal i f�ltet. Tal med decimaler (t.ex. 1,25) �r inte till�tna.',
	numeric: 'Ange endast numeriska v�rden i detta f�lt (t.ex. "1" eller "1.1" eller "-1" eller "-1,1").',
	digits: 'Anv�nd endast siffror och skiljetecken i detta f�lt (till exempel ett telefonnummer med bindestreck till�tet).',
	alpha: 'Anv�nd endast bokst�ver (a-�) i detta f�lt. Inga mellanslag eller andra tecken �r till�tna.',
	alphanum: 'Anv�nd endast bokst�ver (a-�) och siffror (0-9) i detta f�lt. Inga mellanslag eller andra tecken �r till�tna.',
	dateSuchAs: 'Ange ett giltigt datum som t.ex. {date}',
	dateInFormatMDY: 'Ange ett giltigt datum som t.ex. YYYY-MM-DD (i.e. "1999-12-31")',
	email: 'Ange en giltig e-postadress. Till exempel "erik@domain.com".',
	url: 'Ange en giltig webbadress som http://www.example.com.',
	currencyDollar: 'Ange en giltig belopp. Exempelvis 100,00.',
	oneRequired: 'V�nligen ange minst ett av dessa alternativ.',
	errorPrefix: 'Fel: ',
	warningPrefix: 'Varning: ',

	// Form.Validator.Extras
	noSpace: 'Det f�r inte finnas n�gra mellanslag i detta f�lt.',
	reqChkByNode: 'Inga objekt �r valda.',
	requiredChk: 'Detta �r ett obligatoriskt f�lt.',
	reqChkByName: 'V�lj en {label}.',
	match: 'Detta f�lt m�ste matcha {matchName}',
	startDate: 'startdatumet',
	endDate: 'slutdatum',
	currendDate: 'dagens datum',
	afterDate: 'Datumet b�r vara samma eller senare �n {label}.',
	beforeDate: 'Datumet b�r vara samma eller tidigare �n {label}.',
	startMonth: 'V�lj en start m�nad',
	sameMonth: 'Dessa tv� datum m�ste vara i samma m�nad - du m�ste �ndra det ena eller det andra.'

});

Locale.use('sv-SE');