/*
---

name: Locale.EU.Number

description: Number messages for Europe.

license: MIT-style license

authors:
  - Arian Stolwijk

requires:
  - /Locale

provides: [Locale.EU.Number]

...
*/

Locale.define('EU', 'Number', {

	decimal: ',',
	group: '.',

	currency: {
		prefix: '� '
	}

});

/*
---

name: Locale.fi-FI.Date

description: Date messages for Finnish.

license: MIT-style license

authors:
  - ksel

requires:
  - /Locale

provides: [Locale.fi-FI.Date]

...
*/

Locale.define('fi-FI', 'Date', {

	// NOTE: months and days are not capitalized in finnish
	months: ['tammikuu', 'helmikuu', 'maaliskuu', 'huhtikuu', 'toukokuu', 'kes�kuu', 'hein�kuu', 'elokuu', 'syyskuu', 'lokakuu', 'marraskuu', 'joulukuu'],

	// these abbreviations are really not much used in finnish because they obviously won't abbreviate very much. ;)
	// NOTE: sometimes one can see forms such as "tammi", "helmi", etc. but that is not proper finnish.
	months_abbr: ['tammik.', 'helmik.', 'maalisk.', 'huhtik.', 'toukok.', 'kes�k.', 'hein�k.', 'elok.', 'syysk.', 'lokak.', 'marrask.', 'jouluk.'],

	days: ['sunnuntai', 'maanantai', 'tiistai', 'keskiviikko', 'torstai', 'perjantai', 'lauantai'],
	days_abbr: ['su', 'ma', 'ti', 'ke', 'to', 'pe', 'la'],

	// Culture's date order: DD/MM/YYYY
	dateOrder: ['date', 'month', 'year'],
	shortDate: '%d.%m.%Y',
	shortTime: '%H:%M',
	AM: 'AM',
	PM: 'PM',
	firstDayOfWeek: 1,

	// Date.Extras
	ordinal: '.',

	lessThanMinuteAgo: 'vajaa minuutti sitten',
	minuteAgo: 'noin minuutti sitten',
	minutesAgo: '{delta} minuuttia sitten',
	hourAgo: 'noin tunti sitten',
	hoursAgo: 'noin {delta} tuntia sitten',
	dayAgo: 'p�iv� sitten',
	daysAgo: '{delta} p�iv�� sitten',
	weekAgo: 'viikko sitten',
	weeksAgo: '{delta} viikkoa sitten',
	monthAgo: 'kuukausi sitten',
	monthsAgo: '{delta} kuukautta sitten',
	yearAgo: 'vuosi sitten',
	yearsAgo: '{delta} vuotta sitten',

	lessThanMinuteUntil: 'vajaan minuutin kuluttua',
	minuteUntil: 'noin minuutin kuluttua',
	minutesUntil: '{delta} minuutin kuluttua',
	hourUntil: 'noin tunnin kuluttua',
	hoursUntil: 'noin {delta} tunnin kuluttua',
	dayUntil: 'p�iv�n kuluttua',
	daysUntil: '{delta} p�iv�n kuluttua',
	weekUntil: 'viikon kuluttua',
	weeksUntil: '{delta} viikon kuluttua',
	monthUntil: 'kuukauden kuluttua',
	monthsUntil: '{delta} kuukauden kuluttua',
	yearUntil: 'vuoden kuluttua',
	yearsUntil: '{delta} vuoden kuluttua'

});


/*
---

name: Locale.fi-FI.Form.Validator

description: Form Validator messages for Finnish.

license: MIT-style license

authors:
  - ksel

requires:
  - /Locale

provides: [Locale.fi-FI.Form.Validator]

...
*/

Locale.define('fi-FI', 'FormValidator', {

	required: 'T�m� kentt� on pakollinen.',
	minLength: 'Ole hyv� ja anna v�hint��n {minLength} merkki� (annoit {length} merkki�).',
	maxLength: '�l� anna enemp�� kuin {maxLength} merkki� (annoit {length} merkki�).',
	integer: 'Ole hyv� ja anna kokonaisluku. Luvut, joissa on desimaaleja (esim. 1.25) eiv�t ole sallittuja.',
	numeric: 'Anna t�h�n kentt��n lukuarvo (kuten "1" tai "1.1" tai "-1" tai "-1.1").',
	digits: 'K�yt� pelk�st��n numeroita ja v�limerkkej� t�ss� kent�ss� (sy�tteet, kuten esim. puhelinnumero, jossa on v�liviivoja, pilkkuja tai pisteit�, kelpaa).',
	alpha: 'Anna t�h�n kentt��n vain kirjaimia (a-z). V�lily�nnit tai muut merkit eiv�t ole sallittuja.',
	alphanum: 'Anna t�h�n kentt��n vain kirjaimia (a-z) tai numeroita (0-9). V�lily�nnit tai muut merkit eiv�t ole sallittuja.',
	dateSuchAs: 'Ole hyv� ja anna kelvollinen p�ivm��r�, kuten esimerkiksi {date}',
	dateInFormatMDY: 'Ole hyv� ja anna kelvollinen p�iv�m��r� muodossa pp/kk/vvvv (kuten "12/31/1999")',
	email: 'Ole hyv� ja anna kelvollinen s�hk�postiosoite (kuten esimerkiksi "matti@meikalainen.com").',
	url: 'Ole hyv� ja anna kelvollinen URL, kuten esimerkiksi http://www.example.com.',
	currencyDollar: 'Ole hyv� ja anna kelvollinen eurosumma (kuten esimerkiksi 100,00 EUR) .',
	oneRequired: 'Ole hyv� ja sy�t� jotakin ainakin johonkin n�ist� kentist�.',
	errorPrefix: 'Virhe: ',
	warningPrefix: 'Varoitus: ',

	// Form.Validator.Extras
	noSpace: 'T�ss� sy�tteess� ei voi olla v�lily�ntej�',
	reqChkByNode: 'Ei valintoja.',
	requiredChk: 'T�m� kentt� on pakollinen.',
	reqChkByName: 'Ole hyv� ja valitse {label}.',
	match: 'T�m�n kent�n tulee vastata kentt�� {matchName}',
	startDate: 'alkup�iv�m��r�',
	endDate: 'loppup�iv�m��r�',
	currendDate: 'nykyinen p�iv�m��r�',
	afterDate: 'P�iv�m��r�n tulisi olla sama tai my�h�isempi ajankohta kuin {label}.',
	beforeDate: 'P�iv�m��r�n tulisi olla sama tai aikaisempi ajankohta kuin {label}.',
	startMonth: 'Ole hyv� ja valitse aloituskuukausi',
	sameMonth: 'N�iden kahden p�iv�m��r�n tulee olla saman kuun sis�ll� -- sinun pit�� muuttaa jompaa kumpaa.',
	creditcard: 'Annettu luottokortin numero ei kelpaa. Ole hyv� ja tarkista numero sek� yrit� uudelleen. {length} numeroa sy�tetty.'

});


/*
---

name: Locale.fi-FI.Number

description: Finnish number messages

license: MIT-style license

authors:
  - ksel

requires:
  - /Locale
  - /Locale.EU.Number

provides: [Locale.fi-FI.Number]

...
*/

Locale.define('fi-FI', 'Number', {

	group: ' ' // grouped by space

}).inherit('EU', 'Number');

Locale.use('fi-FI');