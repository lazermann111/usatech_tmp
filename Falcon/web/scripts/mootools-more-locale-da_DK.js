/*
---

name: Locale.da-DK.Date

description: Date messages for Danish.

license: MIT-style license

authors:
  - Martin Overgaard
  - Henrik Hansen

requires:
  - /Locale

provides: [Locale.da-DK.Date]

...
*/

Locale.define('da-DK', 'Date', {

	months: ['Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'December'],
	months_abbr: ['jan.', 'feb.', 'mar.', 'apr.', 'maj.', 'jun.', 'jul.', 'aug.', 'sep.', 'okt.', 'nov.', 'dec.'],
	days: ['S�ndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'L�rdag'],
	days_abbr: ['s�n', 'man', 'tir', 'ons', 'tor', 'fre', 'l�r'],

	// Culture's date order: DD-MM-YYYY
	dateOrder: ['date', 'month', 'year'],
	shortDate: '%d-%m-%Y',
	shortTime: '%H:%M',
	AM: 'AM',
	PM: 'PM',
	firstDayOfWeek: 1,

	// Date.Extras
	ordinal: '.',

	lessThanMinuteAgo: 'mindre end et minut siden',
	minuteAgo: 'omkring et minut siden',
	minutesAgo: '{delta} minutter siden',
	hourAgo: 'omkring en time siden',
	hoursAgo: 'omkring {delta} timer siden',
	dayAgo: '1 dag siden',
	daysAgo: '{delta} dage siden',
	weekAgo: '1 uge siden',
	weeksAgo: '{delta} uger siden',
	monthAgo: '1 m�ned siden',
	monthsAgo: '{delta} m�neder siden',
	yearAgo: '1 �r siden',
	yearsAgo: '{delta} �r siden',

	lessThanMinuteUntil: 'mindre end et minut fra nu',
	minuteUntil: 'omkring et minut fra nu',
	minutesUntil: '{delta} minutter fra nu',
	hourUntil: 'omkring en time fra nu',
	hoursUntil: 'omkring {delta} timer fra nu',
	dayUntil: '1 dag fra nu',
	daysUntil: '{delta} dage fra nu',
	weekUntil: '1 uge fra nu',
	weeksUntil: '{delta} uger fra nu',
	monthUntil: '1 m�ned fra nu',
	monthsUntil: '{delta} m�neder fra nu',
	yearUntil: '1 �r fra nu',
	yearsUntil: '{delta} �r fra nu'

});


/*
---

name: Locale.da-DK.Form.Validator

description: Form Validator messages for Danish.

license: MIT-style license

authors:
  - Martin Overgaard

requires:
  - /Locale

provides: [Locale.da-DK.Form.Validator]

...
*/

Locale.define('da-DK', 'FormValidator', {

	required: 'Feltet skal udfyldes.',
	minLength: 'Skriv mindst {minLength} tegn (du skrev {length} tegn).',
	maxLength: 'Skriv maksimalt {maxLength} tegn (du skrev {length} tegn).',
	integer: 'Skriv et tal i dette felt. Decimal tal (f.eks. 1.25) er ikke tilladt.',
	numeric: 'Skriv kun tal i dette felt (i.e. "1" eller "1.1" eller "-1" eller "-1.1").',
	digits: 'Skriv kun tal og tegns�tning i dette felt (eksempel, et telefon nummer med bindestreg eller punktum er tilladt).',
	alpha: 'Skriv kun bogstaver (a-z) i dette felt. Mellemrum og andre tegn er ikke tilladt.',
	alphanum: 'Skriv kun bogstaver (a-z) eller tal (0-9) i dette felt. Mellemrum og andre tegn er ikke tilladt.',
	dateSuchAs: 'Skriv en gyldig dato som {date}',
	dateInFormatMDY: 'Skriv dato i formatet DD-MM-YYYY (f.eks. "31-12-1999")',
	email: 'Skriv en gyldig e-mail adresse. F.eks "fred@domain.com".',
	url: 'Skriv en gyldig URL adresse. F.eks "http://www.example.com".',
	currencyDollar: 'Skriv et gldigt bel�b. F.eks Kr.100.00 .',
	oneRequired: 'Et eller flere af felterne i denne formular skal udfyldes.',
	errorPrefix: 'Fejl: ',
	warningPrefix: 'Advarsel: ',

	// Form.Validator.Extras
	noSpace: 'Der m� ikke benyttes mellemrum i dette felt.',
	reqChkByNode: 'Foretag et valg.',
	requiredChk: 'Dette felt skal udfyldes.',
	reqChkByName: 'V�lg en {label}.',
	match: 'Dette felt skal matche {matchName} feltet',
	startDate: 'start dato',
	endDate: 'slut dato',
	currendDate: 'dags dato',
	afterDate: 'Datoen skal v�re st�rre end eller lig med {label}.',
	beforeDate: 'Datoen skal v�re mindre end eller lig med {label}.',
	startMonth: 'V�lg en start m�ned',
	sameMonth: 'De valgte datoer skal v�re i samme m�ned - skift en af dem.'

});


Locale.use('da-DK');