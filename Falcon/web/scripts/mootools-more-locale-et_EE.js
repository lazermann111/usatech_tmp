/*
---

name: Locale.et-EE.Date

description: Date messages for Estonian.

license: MIT-style license

authors:
  - Kevin Valdek

requires:
  - /Locale

provides: [Locale.et-EE.Date]

...
*/

Locale.define('et-EE', 'Date', {

	months: ['jaanuar', 'veebruar', 'm�rts', 'aprill', 'mai', 'juuni', 'juuli', 'august', 'september', 'oktoober', 'november', 'detsember'],
	months_abbr: ['jaan', 'veebr', 'm�rts', 'apr', 'mai', 'juuni', 'juuli', 'aug', 'sept', 'okt', 'nov', 'dets'],
	days: ['p�hap�ev', 'esmasp�ev', 'teisip�ev', 'kolmap�ev', 'neljap�ev', 'reede', 'laup�ev'],
	days_abbr: ['p�hap', 'esmasp', 'teisip', 'kolmap', 'neljap', 'reede', 'laup'],

	// Culture's date order: MM.DD.YYYY
	dateOrder: ['month', 'date', 'year'],
	shortDate: '%m.%d.%Y',
	shortTime: '%H:%M',
	AM: 'AM',
	PM: 'PM',
	firstDayOfWeek: 1,

	// Date.Extras
	ordinal: '',

	lessThanMinuteAgo: 'v�hem kui minut aega tagasi',
	minuteAgo: 'umbes minut aega tagasi',
	minutesAgo: '{delta} minutit tagasi',
	hourAgo: 'umbes tund aega tagasi',
	hoursAgo: 'umbes {delta} tundi tagasi',
	dayAgo: '1 p�ev tagasi',
	daysAgo: '{delta} p�eva tagasi',
	weekAgo: '1 n�dal tagasi',
	weeksAgo: '{delta} n�dalat tagasi',
	monthAgo: '1 kuu tagasi',
	monthsAgo: '{delta} kuud tagasi',
	yearAgo: '1 aasta tagasi',
	yearsAgo: '{delta} aastat tagasi',

	lessThanMinuteUntil: 'v�hem kui minuti aja p�rast',
	minuteUntil: 'umbes minuti aja p�rast',
	minutesUntil: '{delta} minuti p�rast',
	hourUntil: 'umbes tunni aja p�rast',
	hoursUntil: 'umbes {delta} tunni p�rast',
	dayUntil: '1 p�eva p�rast',
	daysUntil: '{delta} p�eva p�rast',
	weekUntil: '1 n�dala p�rast',
	weeksUntil: '{delta} n�dala p�rast',
	monthUntil: '1 kuu p�rast',
	monthsUntil: '{delta} kuu p�rast',
	yearUntil: '1 aasta p�rast',
	yearsUntil: '{delta} aasta p�rast'

});


/*
---

name: Locale.et-EE.Form.Validator

description: Form Validator messages for Estonian.

license: MIT-style license

authors:
  - Kevin Valdek

requires:
  - /Locale

provides: [Locale.et-EE.Form.Validator]

...
*/

Locale.define('et-EE', 'FormValidator', {

	required: 'V�li peab olema t�idetud.',
	minLength: 'Palun sisestage v�hemalt {minLength} t�hte (te sisestasite {length} t�hte).',
	maxLength: 'Palun �rge sisestage rohkem kui {maxLength} t�hte (te sisestasite {length} t�hte).',
	integer: 'Palun sisestage v�ljale t�isarv. K�mnendarvud (n�iteks 1.25) ei ole lubatud.',
	numeric: 'Palun sisestage ainult numbreid v�ljale (n�iteks "1", "1.1", "-1" v�i "-1.1").',
	digits: 'Palun kasutage ainult numbreid ja kirjavahem�rke (telefoninumbri sisestamisel on lubatud kasutada kriipse ja punkte).',
	alpha: 'Palun kasutage ainult t�hti (a-z). T�hikud ja teised s�mbolid on keelatud.',
	alphanum: 'Palun kasutage ainult t�hti (a-z) v�i numbreid (0-9). T�hikud ja teised s�mbolid on keelatud.',
	dateSuchAs: 'Palun sisestage kehtiv kuup�ev kujul {date}',
	dateInFormatMDY: 'Palun sisestage kehtiv kuup�ev kujul MM.DD.YYYY (n�iteks: "12.31.1999").',
	email: 'Palun sisestage kehtiv e-maili aadress (n�iteks: "fred@domain.com").',
	url: 'Palun sisestage kehtiv URL (n�iteks: http://www.example.com).',
	currencyDollar: 'Palun sisestage kehtiv $ summa (n�iteks: $100.00).',
	oneRequired: 'Palun sisestage midagi v�hemalt �hele antud v�ljadest.',
	errorPrefix: 'Viga: ',
	warningPrefix: 'Hoiatus: ',

	// Form.Validator.Extras
	noSpace: 'V�li ei tohi sisaldada t�hikuid.',
	reqChkByNode: '�kski v�ljadest pole valitud.',
	requiredChk: 'V�lja t�itmine on vajalik.',
	reqChkByName: 'Palun valige �ks {label}.',
	match: 'V�li peab sobima {matchName} v�ljaga',
	startDate: 'algkuup�ev',
	endDate: 'l�ppkuup�ev',
	currendDate: 'praegune kuup�ev',
	afterDate: 'Kuup�ev peab olema v�rdne v�i p�rast {label}.',
	beforeDate: 'Kuup�ev peab olema v�rdne v�i enne {label}.',
	startMonth: 'Palun valige algkuup�ev.',
	sameMonth: 'Antud kaks kuup�eva peavad olema samas kuus - peate muutma �hte kuup�eva.'

});

Locale.use('et-EE');