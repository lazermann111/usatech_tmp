/*
 * Created on Feb 10, 2005
 */
package com.usatech.pos.test;

import com.usatech.pos.webservice.*;

/*
import java.util.Properties;
import java.security.Security;

import java.security.cert.Certificate;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import java.security.KeyStore;
import javax.net.ssl.*;
import java.io.FileInputStream;
*/

//import org.apache.axis.*;

/**
 * @author dKouznetsov
 */
public class POSTester {
    
    public static void main(String[] args) throws Exception
    {
      /*
        String[][] props = 
        {
                {"javax.net.ssl.trustStore", "K:\\Development\\SOAPOverSSL\\Keys\\client.ts"},
                {"javax.net.ssl.keyStore", "K:\\Development\\SOAPOverSSL\\Keys\\client.ks"},
                {"javax.net.ssl.keyStorePassword", "bigblue"},
                {"javax.net.ssl.keyStoreType", "JKS"}
        };
        
        Properties systemProps = System.getProperties();
        
        for (int i = 0; i < props.length; i++)
        {
            systemProps.setProperty(props[i][0], props[i][1]);
        }
       
   
        
        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        System.out.println("Connecting to https://dkouznetsov.usatech.com:9300/");
        SSLSocket sock = (SSLSocket) factory.createSocket("dkouznetsov.usatech.com", 9300);
        //SSLSocket sock = (SSLSocket) factory.createSocket();
        
        SSLSocketFactory.
        
        String[] enabledCiphers = {"SSL_RSA_WITH_RC4_128_MD5", "SSL_RSA_WITH_3DES_EDE_CBC_SHA"};
        
        sock.setEnabledCipherSuites(enabledCiphers);
        
        String[] cipherSuiteList = sock.getEnabledCipherSuites();
        String[] protocolList = sock.getSupportedProtocols();
        
        System.out.println("\nEnabled Ciphers:");
        for (int i = 0; i < cipherSuiteList.length; i++) {
            System.out.println(cipherSuiteList[i]);
        }
       
        
        System.out.println("\nSupported Protocols:");
        for (int i = 0; i < protocolList.length; i++) {
            System.out.println(protocolList[i]);
        }
        
        sock.startHandshake();
        SSLSession session = sock.getSession();
        System.out.println(
            "\nCipher used: " + session.getCipherSuite());
        System.out.println("Protocol used: " + session.getProtocol());
        
        sock.close();
        */
        	
        /*
        java.util.Properties sysprops = System.getProperties();
    for ( java.util.Enumeration e = sysprops.propertyNames(); e.hasMoreElements(); )
       {
       String key = (String) e.nextElement();
       String value = sysprops.getProperty(key );
       System.out.println(key + "=" + value);
       }
       */

        /*
        SSLContext ctx;
        KeyStore ks;
        FileInputStream fis;
        
        ks = KeyStore.getInstance("JKS");
        
        String kmfAlgorithm = KeyManagerFactory.getDefaultAlgorithm();
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        
        fis = new FileInputStream("K:\\Development\\SOAPOverSSL\\Keys\\client.ks");
        ks.load(fis, "bigblue".toCharArray());
        fis.close();        
        kmf.init(ks, "bigblue".toCharArray());
        
        
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        fis = new FileInputStream("K:\\Development\\SOAPOverSSL\\Keys\\client.ts");
        ks.load(fis, "bigblue".toCharArray());
        fis.close();
        tmf.init(ks);
        */
          
        /*
        FileInputStream fis=new FileInputStream(trustStore);
        KeyStore ks=KeyStore.getInstance("jks");
        ks.load(fis, trustStorePassword.toCharArray());
        fis.close();

        // Now we initialize the TrustManagerFactory with this KeyStore
        tmFact.init(ks);
        */
        

        /*
        tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);
*/
		
        //SSLContext ctx = SSLContext.getInstance("SSL");
        //ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        //ctx
  

        //HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
                
        //AxisProperties.setProperty("axis.socketSecureFactory", "com.usatech.pos.test.ClientSSLFactory");
        
        POSRequestHandlerServiceLocator locator = new POSRequestHandlerServiceLocator();
		POSRequestHandler prh = locator.getposinterface();
			
		System.out.println("Version: " + prh.getVersion());
		
		//com.usatech.pos.webservice.SettleResponse response = prh.settle(999999999, Calendar.getInstance() , "EV000001", "123456789", "USD", 100, "123456");
    }
}
