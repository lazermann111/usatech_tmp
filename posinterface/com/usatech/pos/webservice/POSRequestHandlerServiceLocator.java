/**
 * POSRequestHandlerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.pos.webservice;

public class POSRequestHandlerServiceLocator extends org.apache.axis.client.Service implements com.usatech.pos.webservice.POSRequestHandlerService {

    public POSRequestHandlerServiceLocator() {
    }


    public POSRequestHandlerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public POSRequestHandlerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for posinterface
    private java.lang.String posinterface_address = "http://localhost:9300/axis/services/posinterface";

    public java.lang.String getposinterfaceAddress() {
        return posinterface_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String posinterfaceWSDDServiceName = "posinterface";

    public java.lang.String getposinterfaceWSDDServiceName() {
        return posinterfaceWSDDServiceName;
    }

    public void setposinterfaceWSDDServiceName(java.lang.String name) {
        posinterfaceWSDDServiceName = name;
    }

    public com.usatech.pos.webservice.POSRequestHandler getposinterface() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(posinterface_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getposinterface(endpoint);
    }

    public com.usatech.pos.webservice.POSRequestHandler getposinterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.pos.webservice.PosinterfaceSoapBindingStub _stub = new com.usatech.pos.webservice.PosinterfaceSoapBindingStub(portAddress, this);
            _stub.setPortName(getposinterfaceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setposinterfaceEndpointAddress(java.lang.String address) {
        posinterface_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.pos.webservice.POSRequestHandler.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.pos.webservice.PosinterfaceSoapBindingStub _stub = new com.usatech.pos.webservice.PosinterfaceSoapBindingStub(new java.net.URL(posinterface_address), this);
                _stub.setPortName(getposinterfaceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("posinterface".equals(inputPortName)) {
            return getposinterface();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:pos.usatech.com", "POSRequestHandlerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:pos.usatech.com", "posinterface"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("posinterface".equals(portName)) {
            setposinterfaceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
