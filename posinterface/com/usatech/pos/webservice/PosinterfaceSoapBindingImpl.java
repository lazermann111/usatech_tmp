/**
 * PosinterfaceSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.pos.webservice;

import com.usatech.util.Log;

public class PosinterfaceSoapBindingImpl implements com.usatech.pos.webservice.POSRequestHandler{
    public java.lang.String getVersion() throws java.rmi.RemoteException {
        return com.usatech.pos.POSRequestHandler.getVersion();
    }
    
    public com.usatech.pos.webservice.AuthorizeResponse authorize(int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount) 
        throws java.rmi.RemoteException, com.usatech.pos.webservice.POSException
    {
        com.usatech.pos.webservice.AuthorizeResponse response;
        try
        {
            Log.info(PosinterfaceSoapBindingImpl.class,"Authorize Request" +
                    ": transactionID=" + transactionID +
                    ", transactionDateTime=" + transactionDateTime.getTime() +
                    ", terminalName=" + terminalName +
                    ", cardNumber=" + Log.maskCard(cardNumber) +
                    ", currency=" + currency +
                    ", amount=" + amount);
            
            com.usatech.pos.AuthorizeResponse serverResponse = com.usatech.pos.POSRequestHandler.authorize(transactionID, transactionDateTime, terminalName, cardNumber, currency, amount);
            response = new com.usatech.pos.webservice.AuthorizeResponse(
                    serverResponse.getApprovalCode(), 
                    serverResponse.getApprovedAmount(), 
                    serverResponse.getResponseMessage(), 
                    serverResponse.getStatusCode(), 
                    serverResponse.getTransactionID()
            );
            
            Log.info(PosinterfaceSoapBindingImpl.class,"Authorize Response" +
                    ": approvalCode=" + response.getApprovalCode() +
                    ", approvedAmount=" + response.getApprovedAmount() +
                    ", responseMessage=" + response.getResponseMessage() +
                    ", statusCode=" + response.getStatusCode() +
                    ", transactionID=" + response.getTransactionID());
        }
        catch (com.usatech.pos.POSException pe)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Authorize POSException: description=" + pe.getDescription() + ", responseMessage=" + pe.getResponseMessage() + ", statusCode=" + pe.getStatusCode());
            throw new com.usatech.pos.webservice.POSException(pe.getDescription(), pe.getResponseMessage(), pe.getStatusCode());
        }
        catch (Exception e)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Authorize Exception: " + e.getMessage());
            throw new com.usatech.pos.webservice.POSException(e.getMessage(), "Error processing Authorize Request", -1);
        }
        return response;
    }
    
    public com.usatech.pos.webservice.SettleResponse settle(int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount, java.lang.String approvalCode) 
        throws java.rmi.RemoteException, com.usatech.pos.webservice.POSException
    {
        com.usatech.pos.webservice.SettleResponse response;
        try
        {
            Log.info(PosinterfaceSoapBindingImpl.class,"Settle Request" +
                    ": transactionID=" + transactionID +
                    ", transactionDateTime=" + transactionDateTime.getTime() +
                    ", terminalName=" + terminalName +
                    ", cardNumber=" + Log.maskCard(cardNumber) +
                    ", currency=" + currency +
                    ", amount=" + amount +
                    ", approvalCode=" + approvalCode);
            
            com.usatech.pos.SettleResponse serverResponse = com.usatech.pos.POSRequestHandler.settle(transactionID, transactionDateTime, terminalName, cardNumber, currency, amount, approvalCode);
            response = new com.usatech.pos.webservice.SettleResponse(
                    serverResponse.getResponseMessage(),
                    serverResponse.getStatusCode(),
                    serverResponse.getTransactionID()
            );
            
            Log.info(PosinterfaceSoapBindingImpl.class,"Settle Response" +
                    ": responseMessage=" + response.getResponseMessage() +
                    ", statusCode=" + response.getStatusCode() +
                    ", transactionID=" + response.getTransactionID());
        }
        catch (com.usatech.pos.POSException pe)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Settle POSException: description=" + pe.getDescription() + ", responseMessage=" + pe.getResponseMessage() + ", statusCode=" + pe.getStatusCode());
            throw new com.usatech.pos.webservice.POSException(pe.getDescription(), pe.getResponseMessage(), pe.getStatusCode());
        }
        catch (Exception e)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Settle Exception: " + e.getMessage());
            throw new com.usatech.pos.webservice.POSException(e.getMessage(), "Error processing Settle Request", -1);
        }
        return response;
    }
    
    public com.usatech.pos.webservice.RefundResponse refund(int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount) 
        throws java.rmi.RemoteException, com.usatech.pos.webservice.POSException
    {
        com.usatech.pos.webservice.RefundResponse response;
        try
        {
            Log.info(PosinterfaceSoapBindingImpl.class,"Refund Request" +
                    ": transactionID=" + transactionID +
                    ", transactionDateTime=" + transactionDateTime.getTime() +
                    ", terminalName=" + terminalName +
                    ", cardNumber=" + Log.maskCard(cardNumber) +
                    ", currency=" + currency +
                    ", amount=" + amount);
            
            com.usatech.pos.RefundResponse serverResponse = com.usatech.pos.POSRequestHandler.refund(transactionID, transactionDateTime, terminalName, cardNumber, currency, amount);
            response = new com.usatech.pos.webservice.RefundResponse(
                    serverResponse.getResponseMessage(),
                    serverResponse.getStatusCode(),
                    serverResponse.getTransactionID()
            );
            
            Log.info(PosinterfaceSoapBindingImpl.class,"Refund Response" +
                    ": responseMessage=" + response.getResponseMessage() +
                    ", statusCode=" + response.getStatusCode() +
                    ", transactionID=" + response.getTransactionID());
            
        }
        catch (com.usatech.pos.POSException pe)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Refund POSException: description=" + pe.getDescription() + ", responseMessage=" + pe.getResponseMessage() + ", statusCode=" + pe.getStatusCode());
            throw new com.usatech.pos.webservice.POSException(pe.getDescription(), pe.getResponseMessage(), pe.getStatusCode());
        }
        catch (Exception e)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Refund Exception: " + e.getMessage());
            throw new com.usatech.pos.webservice.POSException(e.getMessage(), "Error processing Refund Request", -1);
        }
        return response;
    }
}
