/*
 * Created on Feb 10, 2005
 */
package com.usatech.pos;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

/**
 * @author dKouznetsov
 */
public class POSRequestHandler {
	private static final String VERSION = "POS System v1.0.1";
	protected static boolean byAmount = true;

	public static String getVersion() {
		return VERSION;
	}

	private static String formatAmount(int amount) {
		return NumberFormat.getCurrencyInstance(Locale.US).format((double) amount / 100);
	}

	protected static final Random random = new Random();

	protected static String generateApprovalCode() {
		char[] code = new char[6];
		int r = random.nextInt();
		for(int i = 0; i < code.length; i++) {
			int n = Math.abs(r % 36);
			r = r / 36;
			code[i] = (char) (n < 10 ? '0' + n : 'A' - 10 + n);
		}
		return new String(code);
	}

	public static AuthorizeResponse authorize(int transactionID, Calendar transactionDateTime, String terminalName, String cardNumber, String currency, int amount) throws POSException {
		AuthorizeResponse response = new AuthorizeResponse();
		response.setTransactionID(transactionID);
		if(isByAmount()) {
			switch(amount % 10) {
				case 0:
					response.setStatusCode(0);
					response.setApprovalCode(generateApprovalCode());
					response.setApprovedAmount(amount);
					response.setResponseMessage("Approved: " + formatAmount(amount));
					break;
				case 1:
					response.setStatusCode(1);
					response.setResponseMessage("Declined");
					break;
				case 2:
					response.setStatusCode(2);
					response.setResponseMessage("Failed");
					break;
				case 3:
					response.setStatusCode(3);
					response.setApprovalCode(generateApprovalCode());
					int approvedAmount = amount / 2;
					response.setApprovedAmount(approvedAmount);
					response.setResponseMessage("Approved: " + formatAmount(approvedAmount));
					break;
				default:
					response.setStatusCode(2);
					response.setResponseMessage("Failed");
					break;
			}
		} else {
			String cardPrefix = cardNumber.substring(0, 1);
			if(cardPrefix.equalsIgnoreCase("0")) {
				response.setStatusCode(0);
				response.setApprovalCode("123456");
				response.setApprovedAmount(amount);
				response.setResponseMessage("Approved: " + formatAmount(amount));
			} else if(cardPrefix.equalsIgnoreCase("1")) {
				response.setStatusCode(1);
				response.setResponseMessage("Declined");
			} else if(cardPrefix.equalsIgnoreCase("3")) {
				response.setStatusCode(3);
				response.setApprovalCode("123456");
				int approvedAmount = (amount > 0 ? amount - 1 : amount);
				response.setApprovedAmount(approvedAmount);
				response.setResponseMessage("Approved: " + formatAmount(approvedAmount));
			} else {
				response.setStatusCode(2);
				response.setResponseMessage("Failed");
			}
		}
		return response;
	}

	public static SettleResponse settle(int transactionID, Calendar transactionDateTime, String terminalName, String cardNumber, String currency, int amount, String approvalCode) throws POSException {
		SettleResponse response = new SettleResponse();
		response.setTransactionID(transactionID);
		if(isByAmount()) {
			switch(amount % 10) {
				case 0:
				case 3:
					response.setStatusCode(0);
					response.setResponseMessage("Settled: " + formatAmount(amount));
					break;
				case 1:
					response.setStatusCode(1);
					response.setResponseMessage("Declined");
					break;
				case 2:
					response.setStatusCode(2);
					response.setResponseMessage("Failed");
					break;
				default:
					response.setStatusCode(2);
					response.setResponseMessage("Failed");
					break;
			}
		} else {
			String cardPrefix = cardNumber.substring(0, 1);
			if(cardPrefix.equalsIgnoreCase("0") || cardPrefix.equalsIgnoreCase("3")) {
				response.setStatusCode(0);
				response.setResponseMessage("Settled: " + formatAmount(amount));
			} else {
				response.setStatusCode(2);
				response.setResponseMessage("Failed");
			}
		}
		return response;
	}

	public static RefundResponse refund(int transactionID, Calendar transactionDateTime, String terminalName, String cardNumber, String currency, int amount) throws POSException {
		RefundResponse response = new RefundResponse();
		response.setTransactionID(transactionID);

		if(isByAmount()) {
			switch(amount % 10) {
				case 0:
				case 3:
					response.setStatusCode(0);
					response.setResponseMessage("Refunded: " + formatAmount(amount));
					break;
				case 1:
					response.setStatusCode(1);
					response.setResponseMessage("Declined");
					break;
				case 2:
					response.setStatusCode(2);
					response.setResponseMessage("Failed");
					break;
				default:
					response.setStatusCode(2);
					response.setResponseMessage("Failed");
					break;
			}
		} else {
			String cardPrefix = cardNumber.substring(0, 1);
			if(cardPrefix.equalsIgnoreCase("0") || cardPrefix.equalsIgnoreCase("3")) {
				response.setStatusCode(0);
				response.setResponseMessage("Refunded: " + formatAmount(amount));
			} else {
				response.setStatusCode(2);
				response.setResponseMessage("Failed");
			}
		}
		return response;
	}

	public static boolean isByAmount() {
		return byAmount;
	}

	public static void setByAmount(boolean byAmount) {
		POSRequestHandler.byAmount = byAmount;
	}
}
