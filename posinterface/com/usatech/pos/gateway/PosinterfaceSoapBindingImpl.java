/**
 * PosinterfaceSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.pos.gateway;

import com.usatech.util.Log;

public class PosinterfaceSoapBindingImpl implements com.usatech.pos.gateway.POSRequestHandler
{
    private static final String VERSION = "POS Gateway v1.0.1";
    private static com.usatech.pos.webservice.POSRequestHandlerServiceLocator locator;
    
    static
    {
        locator = new com.usatech.pos.webservice.POSRequestHandlerServiceLocator(); 
    }
    
    private com.usatech.pos.webservice.POSRequestHandler locateHandler(java.lang.String remoteServerAddress)
    	throws POSException
    {
        java.net.URL endpoint;
        com.usatech.pos.webservice.POSRequestHandler handler;
        try
        {
            endpoint = new java.net.URL(remoteServerAddress);
        }
        catch (Exception e) 
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Failure to connect to invalid remoteServerAddress: " + remoteServerAddress);
            throw new com.usatech.pos.gateway.POSException(e.getMessage(), "Invalid remoteServerAddress", -1);
        }
        try
        {
            handler = locator.getposinterface(endpoint);
        }
        catch (Exception e) 
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Unable to connect to POS System Server " + remoteServerAddress);
            throw new com.usatech.pos.gateway.POSException(e.getMessage(), "Unable to connect to POS System Server", -1);
        }
        return handler;
    }
    
    public java.lang.String getVersion() throws java.rmi.RemoteException 
    {
        return VERSION;
    }

    public com.usatech.pos.gateway.AuthorizeResponse authorize(java.lang.String remoteServerAddress, int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount) 
    	throws java.rmi.RemoteException, com.usatech.pos.gateway.POSException 
    {
        com.usatech.pos.gateway.AuthorizeResponse response;
        com.usatech.pos.webservice.POSRequestHandler handler = locateHandler(remoteServerAddress);
        try
        {
            Log.info(PosinterfaceSoapBindingImpl.class,"Authorize Request" +
                    ": remoteServerAddress=" + remoteServerAddress +
                    ", transactionID=" + transactionID +
                    ", transactionDateTime=" + transactionDateTime.getTime() +
                    ", terminalName=" + terminalName +
                    ", cardNumber=" + Log.maskCard(cardNumber) +
                    ", currency=" + currency +
                    ", amount=" + amount);
                    
            com.usatech.pos.webservice.AuthorizeResponse serverResponse = handler.authorize(transactionID, transactionDateTime, terminalName, cardNumber, currency, amount);
            response = new com.usatech.pos.gateway.AuthorizeResponse(
                    serverResponse.getApprovalCode(), 
                    serverResponse.getApprovedAmount(), 
                    serverResponse.getResponseMessage(), 
                    serverResponse.getStatusCode(), 
                    serverResponse.getTransactionID()
            );
            
            Log.info(PosinterfaceSoapBindingImpl.class,"Authorize Response" +
                    ": approvalCode=" + response.getApprovalCode() +
                    ", approvedAmount=" + response.getApprovedAmount() +
                    ", responseMessage=" + response.getResponseMessage() +
                    ", statusCode=" + response.getStatusCode() +
                    ", transactionID=" + response.getTransactionID());
        }
        catch (com.usatech.pos.webservice.POSException pe)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Authorize POSException: description=" + pe.getDescription() + ", responseMessage=" + pe.getResponseMessage() + ", statusCode=" + pe.getStatusCode());
            throw new com.usatech.pos.gateway.POSException(pe.getDescription(), pe.getResponseMessage(), pe.getStatusCode());
        }
        catch (Exception e)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Authorize Exception: " + e.getMessage());
            throw new com.usatech.pos.gateway.POSException(e.getMessage(), "Error processing Authorize Request", -1);
        }
        return response;
    }

    public com.usatech.pos.gateway.SettleResponse settle(java.lang.String remoteServerAddress, int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount, java.lang.String approvalCode) 
    	throws java.rmi.RemoteException, com.usatech.pos.gateway.POSException 
    {
        com.usatech.pos.gateway.SettleResponse response;
        com.usatech.pos.webservice.POSRequestHandler handler = locateHandler(remoteServerAddress);
        try
        {
            Log.info(PosinterfaceSoapBindingImpl.class,"Settle Request" +
                    ": remoteServerAddress=" + remoteServerAddress +
                    ", transactionID=" + transactionID +
                    ", transactionDateTime=" + transactionDateTime.getTime() +
                    ", terminalName=" + terminalName +
                    ", cardNumber=" + Log.maskCard(cardNumber) +
                    ", currency=" + currency +
                    ", amount=" + amount +
                    ", approvalCode=" + approvalCode);
            
            com.usatech.pos.webservice.SettleResponse serverResponse = handler.settle(transactionID, transactionDateTime, terminalName, cardNumber, currency, amount, approvalCode);
            response = new com.usatech.pos.gateway.SettleResponse(
                    serverResponse.getResponseMessage(),
                    serverResponse.getStatusCode(),
                    serverResponse.getTransactionID()
            );
            
            Log.info(PosinterfaceSoapBindingImpl.class,"Settle Response" +
                    ": responseMessage=" + response.getResponseMessage() +
                    ", statusCode=" + response.getStatusCode() +
                    ", transactionID=" + response.getTransactionID());
        }
        catch (com.usatech.pos.webservice.POSException pe)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Settle POSException: description=" + pe.getDescription() + ", responseMessage=" + pe.getResponseMessage() + ", statusCode=" + pe.getStatusCode());
            throw new com.usatech.pos.gateway.POSException(pe.getDescription(), pe.getResponseMessage(), pe.getStatusCode());
        }
        catch (Exception e)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Settle Exception: " + e.getMessage());
            throw new com.usatech.pos.gateway.POSException(e.getMessage(), "Error processing Settle Request", -1);
        }
        return response;
    }

    public com.usatech.pos.gateway.RefundResponse refund(java.lang.String remoteServerAddress, int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount) 
    	throws java.rmi.RemoteException, com.usatech.pos.gateway.POSException 
    {
        com.usatech.pos.gateway.RefundResponse response;
        com.usatech.pos.webservice.POSRequestHandler handler = locateHandler(remoteServerAddress);
        try
        {
            Log.info(PosinterfaceSoapBindingImpl.class,"Refund Request" +
                    ": remoteServerAddress=" + remoteServerAddress +
                    ", transactionID=" + transactionID +
                    ", transactionDateTime=" + transactionDateTime.getTime() +
                    ", terminalName=" + terminalName +
                    ", cardNumber=" + Log.maskCard(cardNumber) +
                    ", currency=" + currency +
                    ", amount=" + amount);
            
            com.usatech.pos.webservice.RefundResponse serverResponse = handler.refund(transactionID, transactionDateTime, terminalName, cardNumber, currency, amount);
            response = new com.usatech.pos.gateway.RefundResponse(
                    serverResponse.getResponseMessage(),
                    serverResponse.getStatusCode(),
                    serverResponse.getTransactionID()
            );
            
            Log.info(PosinterfaceSoapBindingImpl.class,"Refund Response" +
                    ": responseMessage=" + response.getResponseMessage() +
                    ", statusCode=" + response.getStatusCode() +
                    ", transactionID=" + response.getTransactionID());
        }
        catch (com.usatech.pos.webservice.POSException pe)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Refund POSException: description=" + pe.getDescription() + ", responseMessage=" + pe.getResponseMessage() + ", statusCode=" + pe.getStatusCode());
            throw new com.usatech.pos.gateway.POSException(pe.getDescription(), pe.getResponseMessage(), pe.getStatusCode());
        }
        catch (Exception e)
        {
            Log.error(PosinterfaceSoapBindingImpl.class, "Refund Exception: " + e.getMessage());
            throw new com.usatech.pos.gateway.POSException(e.getMessage(), "Error processing Refund Request", -1);
        }
        return response;
    }
}
