/**
 * POSRequestHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.pos.gateway;

public interface POSRequestHandler extends java.rmi.Remote {
    public com.usatech.pos.gateway.AuthorizeResponse authorize(java.lang.String remoteServerAddress, int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount) throws java.rmi.RemoteException, com.usatech.pos.gateway.POSException;
    public com.usatech.pos.gateway.SettleResponse settle(java.lang.String remoteServerAddress, int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount, java.lang.String approvalCode) throws java.rmi.RemoteException, com.usatech.pos.gateway.POSException;
    public com.usatech.pos.gateway.RefundResponse refund(java.lang.String remoteServerAddress, int transactionID, java.util.Calendar transactionDateTime, java.lang.String terminalName, java.lang.String cardNumber, java.lang.String currency, int amount) throws java.rmi.RemoteException, com.usatech.pos.gateway.POSException;
    public java.lang.String getVersion() throws java.rmi.RemoteException;
}
