/**
 * POSRequestHandlerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.pos.gateway;

public interface POSRequestHandlerService extends javax.xml.rpc.Service {
    public java.lang.String getposinterfaceAddress();

    public com.usatech.pos.gateway.POSRequestHandler getposinterface() throws javax.xml.rpc.ServiceException;

    public com.usatech.pos.gateway.POSRequestHandler getposinterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
