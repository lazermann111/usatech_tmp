/*
 * Created on Feb 14, 2005
 */
package com.usatech.pos;

/**
 * @author dKouznetsov
 */
public class RefundResponse
{
    protected int TransactionID;
    protected int StatusCode;
    protected String ResponseMessage;
    
    public RefundResponse()
    {
        TransactionID = 0;
        StatusCode = 0;
        ResponseMessage = null;
    }
    
    /**
     * @return Returns the responseMessage.
     */
    public String getResponseMessage() {
        return ResponseMessage;
    }
    /**
     * @param responseMessage The responseMessage to set.
     */
    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }
    /**
     * @return Returns the statusCode.
     */
    public int getStatusCode() {
        return StatusCode;
    }
    /**
     * @param statusCode The statusCode to set.
     */
    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }
    /**
     * @return Returns the transactionID.
     */
    public int getTransactionID() {
        return TransactionID;
    }
    /**
     * @param transactionID The transactionID to set.
     */
    public void setTransactionID(int transactionID) {
        TransactionID = transactionID;
    }
}
