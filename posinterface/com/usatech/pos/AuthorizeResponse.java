/*
 * Created on Feb 14, 2005
 */
package com.usatech.pos;

/**
 * @author dKouznetsov
 */
public class AuthorizeResponse
{
    protected int TransactionID;
    protected int StatusCode;
    protected String ResponseMessage;
    private String ApprovalCode;
    private int ApprovedAmount;
    
    public AuthorizeResponse()
    {
        TransactionID = 0;
        StatusCode = 0;
        ResponseMessage = null;
        ApprovalCode = null;
        ApprovedAmount = 0;
    }
    
    /**
     * @return Returns the responseMessage.
     */
    public String getResponseMessage() {
        return ResponseMessage;
    }
    /**
     * @param responseMessage The responseMessage to set.
     */
    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }
    /**
     * @return Returns the statusCode.
     */
    public int getStatusCode() {
        return StatusCode;
    }
    /**
     * @param statusCode The statusCode to set.
     */
    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }
    /**
     * @return Returns the transactionID.
     */
    public int getTransactionID() {
        return TransactionID;
    }
    /**
     * @param transactionID The transactionID to set.
     */
    public void setTransactionID(int transactionID) {
        TransactionID = transactionID;
    }
    /**
     * @return Returns the approvalCode.
     */
    public String getApprovalCode() {
        return ApprovalCode;
    }
    /**
     * @param approvalCode The approvalCode to set.
     */
    public void setApprovalCode(String approvalCode) {
        ApprovalCode = approvalCode;
    }
    /**
     * @return Returns the approvedAmount.
     */
    public int getApprovedAmount() {
        return ApprovedAmount;
    }
    /**
     * @param approvedAmount The approvedAmount to set.
     */
    public void setApprovedAmount(int approvedAmount) {
        ApprovedAmount = approvedAmount;
    }
}
