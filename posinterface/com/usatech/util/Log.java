package com.usatech.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axis.components.logger.LogFactory;

/**
* Provides a facility for logging information.  All logging takes place to log4j which can be redirected
* to a file if desired.  
*
* @author  dkouznetsov
*/
public class Log
{
	private static Log logInstance = null;
	private static org.apache.commons.logging.Log log = LogFactory.getLog("POS");
	
	private static final String CARD_DATA_REGEX = "([0-9]{7,})";
	private static Pattern cardDataPattern = Pattern.compile(CARD_DATA_REGEX);	

	/**
	 * This class is entirely static, a Log object cannot be instantiated.
	 */	
	private Log()
	{
	}

	/**
	 * Used to log debugging messages
	 * 
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void debug(Class classGeneratingMessage, String message)
	{
	    if (log.isDebugEnabled())
	    {
	        log.debug(classGeneratingMessage.getName() + ": " + message);
	    }
	}
	
	/**
	 * Used to log error messages
	 * 
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void error(Class classGeneratingMessage, String message)
	{
	    if (log.isErrorEnabled())
	    {
	        log.error(classGeneratingMessage.getName() + ": " + message);
	    }
	}
	
	/**
	 * Used to log informational messages
	 * 
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void info(Class classGeneratingMessage, String message)
	{
	    if (log.isInfoEnabled())
	    {
	        log.info(classGeneratingMessage.getName() + ": " + message);
	    }
	}
	
	/**
	 * Used to log warning messages
	 * 
	 * @param classGeneratingMessage class which is logging the message
	 * @param message message to be printed in the application log
	 */
	public static void warn(Class classGeneratingMessage, String message)
	{
	    if (log.isWarnEnabled())
	    {
	        log.warn(classGeneratingMessage.getName() + ": " + message);
	    }
	}
    
    /**
     * Masks card data
     */
    public static String maskCard(String card) {
    	if(card == null) return null;
    	Matcher cardDataMatcher = cardDataPattern.matcher(card);
    	if (cardDataMatcher.find()) {
    		byte[] digits = cardDataMatcher.group(1).getBytes();
			int end = digits.length - 4;
			for(int i = 2; i < end; i++)
				digits[i] = '*';
			return card.length() + " bytes, masked: " + new String(digits);    		
    	} else
			return card;
    }
}
