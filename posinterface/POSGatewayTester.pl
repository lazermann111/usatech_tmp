#!/usr/bin/perl

use strict;
use SOAP::Lite;

my $POSGatewayURL = "http://usadev01.usatech.com:9100/posgateway/services/posgateway";
#Port 12345 is used for tracing SOAP messages in TcpTunnelGui tool
#my $POSGatewayURL = "http://localhost:12345/axis/services/posgateway";
my $tranCode = "AUTHORIZE";
my $remoteServerAddress = "http://usaapd1.usatech.com:9100/posinterface/services/posinterface";
my $transactionID = "123123123";
my $transactionDateTime = "2007-08-06T10:55:00.0000000-05:00";
my $terminalName = "EV000001";
my $cardNumber = "0234123412341234";
my $currency = "USD";
my $amount = "150";
my $approvalCode = "123456";

print "\nConnecting to POS Gateway - $tranCode...\n\n";

my $ws = SOAP::Lite
	#-> outputxml("1")
    #-> uri('urn:gateway.pos.usatech.com')
    -> proxy($POSGatewayURL)
    -> xmlschema(2001);
    
my $response;
my $proceed = 1;
my $processResponse = 1;

if ($tranCode eq 'AUTHORIZE')
{
	$response = $ws->authorize(
    	SOAP::Data->name('remoteServerAddress')->value($remoteServerAddress)->type('string'),
    	SOAP::Data->name('transactionID')->value($transactionID)->type('int'),
    	SOAP::Data->name('transactionDateTime')->value($transactionDateTime)->type('dateTime'),
    	SOAP::Data->name('terminalName')->value($terminalName)->type('string'),
    	SOAP::Data->name('cardNumber')->value($cardNumber)->type('string'),
    	SOAP::Data->name('currency')->value($currency)->type('string'),
    	SOAP::Data->name('amount')->value($amount)->type('int')
    );
}
elsif ($tranCode eq 'SETTLE')
{
	$response = $ws->settle(
    	SOAP::Data->name('remoteServerAddress')->value($remoteServerAddress)->type('string'),
    	SOAP::Data->name('transactionID')->value($transactionID)->type('int'),
    	SOAP::Data->name('transactionDateTime')->value($transactionDateTime)->type('dateTime'),
    	SOAP::Data->name('terminalName')->value($terminalName)->type('string'),
    	SOAP::Data->name('cardNumber')->value($cardNumber)->type('string'),
    	SOAP::Data->name('currency')->value($currency)->type('string'),
    	SOAP::Data->name('amount')->value($amount)->type('int'),
    	SOAP::Data->name('approvalCode')->value($approvalCode)->type('string')
    );
}
elsif ($tranCode eq 'REFUND')
{
	$response = $ws->refund(
    	SOAP::Data->name('remoteServerAddress')->value($remoteServerAddress)->type('string'),
    	SOAP::Data->name('transactionID')->value($transactionID)->type('int'),
    	SOAP::Data->name('transactionDateTime')->value($transactionDateTime)->type('dateTime'),
    	SOAP::Data->name('terminalName')->value($terminalName)->type('string'),
    	SOAP::Data->name('cardNumber')->value($cardNumber)->type('string'),
    	SOAP::Data->name('currency')->value($currency)->type('string'),
    	SOAP::Data->name('amount')->value($amount)->type('int')
    );
}
elsif ($tranCode eq 'GETVERSION')
{
	$response = $ws->getVersion();
	$processResponse = 0;
}
else
{
	print "Unsupported transaction type code\n";
	$proceed = 0;
}

if ($proceed == 1)
{
	unless ($response->fault) 
	{
		if ($processResponse == 1)
		{
		   my $POSResponse = $response->result;
		   
		   print "-------------------- POS $tranCode RESULTS --------------------------\n";
		   print "statusCode = " . $POSResponse->{statusCode} . "\n";
		   print "responseMessage = " . $POSResponse->{responseMessage} . "\n";
		   print "transactionID = " . $POSResponse->{transactionID} . "\n";
		   print "-------------------------------------------------------------------\n";
		}
		elsif ($tranCode eq 'GETVERSION')
		{
			print "POS Gateway Version = " . $response->result . "\n";
		}
	} 
	else 
	{		
	  	my $errstr = "Exception occurred:\nfaultcode=" . $response->faultcode . "\nfaultstring=" . $response->faultstring . "\n";
	    if (ref($response->faultdetail) eq 'HASH')
	    {
	    	my %faultdetail = %{$response->faultdetail};
	    	$errstr .= "exceptionName=" . $faultdetail{exceptionName} . "\n";
	    	if (defined $faultdetail{fault})
	    	{
		    	my %fault = %{$faultdetail{fault}};
		    	foreach my $key (keys %fault)
		    	{
		    		$errstr .= $key . "=" . $fault{$key} . "\n";
		    	}
	    	}
	    }
	    else
	    {
	    	$errstr .= "faultdetail=" . $response->faultdetail;
	    }
	    print $errstr;
	}
}
 
print "\nFinished\n";
