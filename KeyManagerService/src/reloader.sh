#!/bin/bash

# kill old proces if exists
oldPid=`cat logs/KeyManagerReloader.pid`

if [ `ps -o pid= $oldPid` ]; then
    kill $oldPid
fi

# write pid out to a file
echo $$ >logs/KeyManagerReloader.pid

# read STDIN for keystoreValue
keystoreValue=`base64`

function startKeyManager {
    echo "[`date`] Reloading KeyManagerService with current directory=`pwd`" >> logs/KeyManagerService.stdout.log
    echo -e "$keystoreValue"|base64 -d |bin/KeyManagerService.sh load 
}
trap startKeyManager 10

while true
do
	sleep 5 
done
