CREATE SCHEMA KM
  AUTHORIZATION postgres;

GRANT USAGE ON SCHEMA KM TO use_km;
    
CREATE TABLE KM.ENCRYPTED
(
   ENCRYPTED_ID BIGINT NOT NULL,
   KEY_ID BIGINT NOT NULL,
   ENCRYPTED_DATA BYTEA[] NOT NULL,
   EXPIRATION_UTC_TS TIMESTAMP NOT NULL,
   USER_NAME VARCHAR(100),
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_ENCRYPTED PRIMARY KEY(ENCRYPTED_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE SEQUENCE KM.SEQ_ENCRYPTED_ID CYCLE;

CREATE OR REPLACE FUNCTION KM.FTRBU_AUDIT() 
    RETURNS TRIGGER 
    SECURITY DEFINER
AS $$
BEGIN
    NEW.UPDATED_UTC_TS := STATEMENT_TIMESTAMP();
    NEW.UPDATED_BY := SESSION_USER;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
         
CREATE TRIGGER TRBU_ENCRYPTED
BEFORE UPDATE ON KM.ENCRYPTED
    FOR EACH ROW
    EXECUTE PROCEDURE KM.FTRBU_AUDIT();
    
CREATE OR REPLACE FUNCTION KM.GET_ENCRYPTED_PARITION(
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pv_encrypted_partition OUT VARCHAR,
    pn_start_expiration_utc_ts OUT KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pn_end_expiration_utc_ts OUT KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lv_date_part VARCHAR := 'month';
    ln_num INTEGER := 1;
    lv_date_format VARCHAR := 'YYYY_MM'; 
BEGIN
	pn_start_expiration_utc_ts := DATE_TRUNC(lv_date_part, pd_expiration_utc_ts);
	IF ln_num > 1 THEN
	   DECLARE
	       ln_subtract INTEGER;
       BEGIN
	       ln_subtract := (CAST(DATE_PART(lv_date_part, pn_start_expiration_utc_ts) AS INTEGER) - 1) % ln_num;
	       IF ln_subtract > 0 THEN
	           pn_start_expiration_utc_ts := pn_start_expiration_utc_ts - CAST((ln_subtract || ' ' || lv_date_part) AS INTERVAL);
	       END IF;
	   END;
	END IF;
	pn_end_expiration_utc_ts := pn_start_expiration_utc_ts + CAST((ln_num || ' ' || lv_date_part) AS INTERVAL);
	pv_encrypted_partition:= 'ENCRYPTED_' || to_char(pn_start_expiration_utc_ts, lv_date_format);	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.CREATE_ENCRYPTED_PARITION(
    pv_encrypted_partition VARCHAR,
    pn_start_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pn_end_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
    EXECUTE 'CREATE TABLE KM.' || pv_encrypted_partition || '('
        || 'CONSTRAINT CK_' || pv_encrypted_partition || ' CHECK (EXPIRATION_UTC_TS >= TIMESTAMP''' || pn_start_expiration_utc_ts ||  ''' AND EXPIRATION_UTC_TS < TIMESTAMP''' || pn_end_expiration_utc_ts || '''),'
        || 'PRIMARY KEY(ENCRYPTED_ID)'
        || ') INHERITS(KM.ENCRYPTED)';
    --EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_encrypted_partition || ' TO use_km';
    --EXECUTE 'ALTER TABLE MQ.'|| pv_encrypted_partition || ' OWNER TO use_km';
    EXECUTE 'CREATE INDEX IX_' || pv_encrypted_partition || '_KEY_ID ON KM.' || pv_encrypted_partition || '(KEY_ID)';
    EXECUTE 'CREATE INDEX IX_' || pv_encrypted_partition || '_EXPIRATION_UTC_TS ON KM.' || pv_encrypted_partition || '(EXPIRATION_UTC_TS)';
EXCEPTION
    WHEN duplicate_table THEN
       RETURN; 
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.STORE_ENCRYPTED(
    pn_encrypted_id OUT KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pv_user_name KM.ENCRYPTED.USER_NAME%TYPE DEFAULT NULL)
    SECURITY DEFINER
AS $$
DECLARE
    lv_encrypted_partition VARCHAR;
    ln_start_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ln_end_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
BEGIN
    pn_encrypted_id := NEXTVAL('KM.SEQ_ENCRYPTED_ID');
    SELECT a.pv_encrypted_partition, a.pn_start_expiration_utc_ts, a.pn_end_expiration_utc_ts
      INTO lv_encrypted_partition, ln_start_expiration_utc_ts, ln_end_expiration_utc_ts
      FROM KM.GET_ENCRYPTED_PARITION(pd_expiration_utc_ts) a;
    LOOP
        BEGIN
            EXECUTE 'INSERT INTO KM.' || lv_encrypted_partition || '(ENCRYPTED_ID, KEY_ID, ENCRYPTED_DATA, EXPIRATION_UTC_TS, USER_NAME) VALUES($1, $2, $3, $4, $5)'
                USING pn_encrypted_id, pn_key_id, pba_encrypted_data, pd_expiration_utc_ts, pv_user_name; 
            EXIT;
        EXCEPTION
            WHEN undefined_table THEN
                PERFORM KM.CREATE_ENCRYPTED_PARITION(lv_encrypted_partition, ln_start_expiration_utc_ts, ln_end_expiration_utc_ts);      
        END;    
    END LOOP;   
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.STORE_ENCRYPTED(
    pn_key_id KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pv_user_name KM.ENCRYPTED.USER_NAME%TYPE) TO use_km;

CREATE OR REPLACE FUNCTION KM.RETRIEVE_ENCRYPTED(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id OUT KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data OUT KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pv_user_name OUT KM.ENCRYPTED.USER_NAME%TYPE,
    pd_expiration_utc_ts OUT KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    SELECT KEY_ID, ENCRYPTED_DATA, USER_NAME, EXPIRATION_UTC_TS
      INTO pn_key_id, pba_encrypted_data, pv_user_name, pd_expiration_utc_ts
      FROM KM.ENCRYPTED
     WHERE ENCRYPTED_ID = pn_encrypted_id;        
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.RETRIEVE_ENCRYPTED(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE) TO use_km;
    
CREATE OR REPLACE FUNCTION KM.RETRIEVE_ENCRYPTED_WITH_USERNAME(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id OUT KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data OUT KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pv_user_name OUT KM.ENCRYPTED.USER_NAME%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    SELECT KEY_ID, ENCRYPTED_DATA, USER_NAME
      INTO pn_key_id, pba_encrypted_data, pv_user_name
      FROM KM.ENCRYPTED
     WHERE ENCRYPTED_ID = pn_encrypted_id;        
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.RETRIEVE_ENCRYPTED_WITH_USERNAME(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE) TO use_km;
    
CREATE OR REPLACE FUNCTION KM.DROP_OLD_PARTITION(
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    RETURNS INT
    SECURITY DEFINER
AS $$
DECLARE
    lv_encrypted_partition VARCHAR;
    ld_min_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ld_start_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ld_end_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ld_max_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ln_cnt INT := 0;
BEGIN
    SELECT MIN(EXPIRATION_UTC_TS)
      INTO ld_min_expiration_utc_ts
      FROM KM.ENCRYPTED;
    WHILE ld_min_expiration_utc_ts < pd_expiration_utc_ts LOOP
        SELECT a.pv_encrypted_partition, a.pn_start_expiration_utc_ts, a.pn_end_expiration_utc_ts
          INTO lv_encrypted_partition, ld_start_expiration_utc_ts, ld_end_expiration_utc_ts
          FROM KM.GET_ENCRYPTED_PARITION(ld_min_expiration_utc_ts) a;
        EXECUTE 'SELECT MAX(EXPIRATION_UTC_TS) FROM KM.' || lv_encrypted_partition INTO ld_max_expiration_utc_ts;
        IF ld_max_expiration_utc_ts IS NULL OR ld_max_expiration_utc_ts < pd_expiration_utc_ts THEN  
            BEGIN
                EXECUTE 'DROP TABLE KM.' || lv_encrypted_partition;
                ln_cnt := ln_cnt + 1;
            EXCEPTION
                WHEN undefined_table THEN
                    NULL;
            END;
        END IF;
        ld_min_expiration_utc_ts := ld_end_expiration_utc_ts;
    END LOOP;
    RETURN  ln_cnt;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.DROP_OLD_PARTITION()
    RETURNS INT
    SECURITY DEFINER
AS $$
BEGIN
    RETURN KM.DROP_OLD_PARTITION(CURRENT_TIMESTAMP AT TIME ZONE 'UTC');
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.DROP_OLD_PARTITION() TO use_km;

-----------
CREATE TABLE KM.ACCOUNT
(
   ENCRYPTED_ACCOUNT_CD BYTEA NOT NULL,
   GLOBAL_ACCOUNT_ID BIGINT NOT NULL,
   ENCRYPTED_CRC BYTEA,
   SOURCE_INSTANCE SMALLINT NOT NULL,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_ACCOUNT PRIMARY KEY(ENCRYPTED_ACCOUNT_CD, SOURCE_INSTANCE)
) 
WITH (
  OIDS = FALSE
);

CREATE SEQUENCE KM.SEQ_ACCOUNT_ID_BASE INCREMENT BY 10 START WITH 1000000000;

CREATE UNIQUE INDEX UX_ACCOUNT_GLOBAL_ACCOUNT_ID ON KM.ACCOUNT(GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE);

GRANT SELECT, INSERT, UPDATE ON KM.ACCOUNT TO use_km;

GRANT USAGE ON KM.SEQ_ACCOUNT_ID_BASE TO use_km;

CREATE TRIGGER TRBU_ACCOUNT
BEFORE UPDATE ON KM.ACCOUNT
    FOR EACH ROW
    EXECUTE PROCEDURE KM.FTRBU_AUDIT();

CREATE TABLE KM.TOKEN
(
    GLOBAL_ACCOUNT_ID BIGINT NOT NULL,
    ENCRYPTED_TOKEN BYTEA NOT NULL,
    KEY_ID BIGINT NOT NULL,
    ENCRYPTED_DATA BYTEA[] NOT NULL,
    EXPIRATION_UTC_TS TIMESTAMP NOT NULL,
    INITIATING_DEVICE_NAME VARCHAR(100) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
    UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
    INITIATING_DEVICE_TRAN_CD BIGINT DEFAULT 0 NOT NULL,
    CONSTRAINT PK_TOKEN PRIMARY KEY(GLOBAL_ACCOUNT_ID, ENCRYPTED_TOKEN)
) 
WITH (
  OIDS = FALSE
);

CREATE INDEX IX_TOKEN_EXPIRATION_UTC_TS ON KM.TOKEN(EXPIRATION_UTC_TS);
CREATE UNIQUE INDEX UX_TOKEN_COMBO_1 ON KM.TOKEN(GLOBAL_ACCOUNT_ID, INITIATING_DEVICE_NAME);

GRANT SELECT, INSERT, UPDATE, DELETE ON KM.TOKEN TO use_km;

CREATE TRIGGER TRBU_TOKEN
BEFORE UPDATE ON KM.TOKEN
    FOR EACH ROW
    EXECUTE PROCEDURE KM.FTRBU_AUDIT();

CREATE OR REPLACE FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_old_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    ln_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pn_instance, pb_encrypted_crc);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN  
            -- Allow multiple rows (one for each instance) so that even it we override it we can still get the info with the old global_account_id
            --      this will also solve the issue of a TOKEN record without an ACCOUNT record
                SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC
                  INTO pn_old_global_account_id, pn_old_instance, ln_old_encrypted_crc
                  FROM KM.ACCOUNT
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                 ORDER BY SOURCE_INSTANCE;
                 IF pn_old_global_account_id IS NULL THEN
                    -- Didn't find the row which means either it was deleted out from under us or the global_account_id/source_instance already exists but not with this encrypted_account_cd
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd,
                           ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND SOURCE_INSTANCE = pn_instance;
                    IF NOT FOUND THEN
                        CONTINUE; -- Try again
                    END IF;
                    pn_changed := TRUE;
                    pn_old_global_account_id := pn_global_account_id;
                    pn_old_instance := pn_instance;
                    RETURN;
                ELSIF pn_old_instance = 0 AND pn_instance = 0 AND pn_old_global_account_id < pn_global_account_id THEN
                    -- Global Account Id is Updated
                    UPDATE KM.ACCOUNT
                       SET GLOBAL_ACCOUNT_ID = pn_global_account_id,
                           ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)
                     WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                       AND SOURCE_INSTANCE = pn_instance
                       AND GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
                    IF NOT FOUND THEN
                        CONTINUE; -- Try again
                    END IF;
                    pn_changed := TRUE;
                    RETURN;
                ELSIF pn_old_instance < pn_instance OR (pn_old_instance = pn_instance AND pn_old_global_account_id >= pn_global_account_id 
                        AND (pb_encrypted_crc IS NULL OR pb_encrypted_crc IS NOT DISTINCT FROM ln_old_encrypted_crc))                   
                        THEN
                    pn_changed := FALSE;
                    RETURN;
                END IF;
                IF pb_encrypted_crc IS NOT NULL THEN
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_CRC = pb_encrypted_crc
                     WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                       AND SOURCE_INSTANCE = pn_instance
                       AND GLOBAL_ACCOUNT_ID = pn_global_account_id;
                END IF;
                pn_changed := TRUE;
                RETURN;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lb_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC 
          INTO pn_global_account_id, pn_old_instance, lb_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
         ORDER BY SOURCE_INSTANCE;
        IF FOUND THEN
            IF (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM lb_old_encrypted_crc) THEN
                UPDATE KM.ACCOUNT
                   SET ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)                       
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM ENCRYPTED_CRC);
                pn_changed := TRUE; 
            ELSE
                pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
            SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pb_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID_WITH_CRC(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pb_old_encrypted_crc OUT KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC 
          INTO pn_global_account_id, pn_old_instance, pb_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
         ORDER BY SOURCE_INSTANCE;
        IF FOUND THEN
            IF (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM pb_old_encrypted_crc) THEN
                UPDATE KM.ACCOUNT
                   SET ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)                       
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM ENCRYPTED_CRC);
                pn_changed := TRUE; 
            ELSE
                pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
            SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pb_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.GET_OR_UPSERT_TOKEN(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token INOUT KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE,
    pn_initiating_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE,
    pn_changed OUT BOOLEAN)
    SECURITY DEFINER
AS $$
DECLARE
    lv_old_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE;
    lv_old_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE;
    lb_old_encrypted_token KM.TOKEN.ENCRYPTED_TOKEN%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.TOKEN(GLOBAL_ACCOUNT_ID, KEY_ID, ENCRYPTED_TOKEN, ENCRYPTED_DATA, EXPIRATION_UTC_TS, INITIATING_DEVICE_NAME, INITIATING_DEVICE_TRAN_CD)
                VALUES(pn_global_account_id, pn_key_id, pb_encrypted_token, pba_encrypted_data, pd_expiration_utc_ts, pv_initiating_device_name, pn_initiating_device_tran_cd);
        pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN
                UPDATE KM.TOKEN
                   SET KEY_ID = pn_key_id,
                       ENCRYPTED_TOKEN = pb_encrypted_token, 
                       ENCRYPTED_DATA = pba_encrypted_data, 
                       EXPIRATION_UTC_TS = pd_expiration_utc_ts,
                       INITIATING_DEVICE_TRAN_CD = pn_initiating_device_tran_cd
                 WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND INITIATING_DEVICE_NAME = pv_initiating_device_name
                   AND INITIATING_DEVICE_TRAN_CD <= pn_initiating_device_tran_cd;
                IF FOUND THEN
                    pn_changed := TRUE;
            RETURN;
                ELSE
                    SELECT INITIATING_DEVICE_TRAN_CD, ENCRYPTED_TOKEN
                      INTO lv_old_device_tran_cd, lb_old_encrypted_token
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND INITIATING_DEVICE_NAME = pv_initiating_device_name
                       AND INITIATING_DEVICE_TRAN_CD > pn_initiating_device_tran_cd;
                    IF FOUND THEN
                        pb_encrypted_token := lb_old_encrypted_token;    
                        pn_changed := FALSE;
            RETURN;
                    END IF;
                    SELECT INITIATING_DEVICE_NAME
                      INTO lv_old_device_name
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND ENCRYPTED_TOKEN = pb_encrypted_token
                       AND INITIATING_DEVICE_NAME != pv_initiating_device_name;
                    IF FOUND THEN
                        RAISE unique_violation USING MESSAGE = 'This same token was already created for this account but by a different device';
                    END IF;
                END IF;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    TO use_km;
      
GRANT EXECUTE ON FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    TO use_km;

GRANT EXECUTE ON FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID_WITH_CRC(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pb_old_encrypted_crc OUT KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE) 
    TO use_km;
    
GRANT EXECUTE ON FUNCTION KM.GET_OR_UPSERT_TOKEN(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token INOUT KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE,
    pn_initiating_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE,
    pn_changed OUT BOOLEAN)
    TO use_km;
    
GRANT SELECT ON ALL TABLES IN SCHEMA KM TO read_km;
