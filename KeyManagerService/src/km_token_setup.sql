DROP FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_key_id BIGINT,
    pb_encrypted_token BYTEA,
    pba_encrypted_data BYTEA[],
    pd_expiration_utc_ts TIMESTAMP);

DROP FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pba_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_key_id BIGINT,
    pb_encrypted_token BYTEA,
    pba_encrypted_data BYTEA[],
    pd_expiration_utc_ts TIMESTAMP);

ALTER TABLE KM.ACCOUNT 
    DROP KEY_ID,
    DROP ENCRYPTED_TOKEN,
    DROP ENCRYPTED_DATA,
    DROP EXPIRATION_UTC_TS
;
-------------------
    
CREATE TABLE KM.TOKEN
(
    GLOBAL_ACCOUNT_ID BIGINT NOT NULL,
    ENCRYPTED_TOKEN BYTEA NOT NULL,
    KEY_ID BIGINT NOT NULL,
    ENCRYPTED_DATA BYTEA[] NOT NULL,
    EXPIRATION_UTC_TS TIMESTAMP NOT NULL,
    INITIATING_DEVICE_NAME VARCHAR(100) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
    UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
    CONSTRAINT PK_TOKEN PRIMARY KEY(GLOBAL_ACCOUNT_ID, ENCRYPTED_TOKEN)
) 
WITH (
  OIDS = FALSE
);

CREATE INDEX IX_TOKEN_EXPIRATION_UTC_TS ON KM.TOKEN(EXPIRATION_UTC_TS);
CREATE UNIQUE INDEX UX_TOKEN_COMBO_1 ON KM.TOKEN(GLOBAL_ACCOUNT_ID, INITIATING_DEVICE_NAME);

GRANT SELECT, INSERT, UPDATE, DELETE ON KM.TOKEN TO use_km;

CREATE TRIGGER TRBU_TOKEN
BEFORE UPDATE ON KM.TOKEN
    FOR EACH ROW
    EXECUTE PROCEDURE KM.FTRBU_AUDIT();

CREATE OR REPLACE FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_old_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    ln_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
    ln_old_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pn_instance, pb_encrypted_crc);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN
                BEGIN
	                SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC, ENCRYPTED_ACCOUNT_CD
	                  INTO STRICT pn_old_global_account_id, pn_old_instance, ln_old_encrypted_crc, ln_old_encrypted_account_cd
	                  FROM KM.ACCOUNT
	                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
	                    OR GLOBAL_ACCOUNT_ID = pn_global_account_id;
	            EXCEPTION
	               WHEN no_data_found THEN -- row was deleted out form under us, try insert again
	                   CONTINUE;
	               WHEN too_many_rows THEN -- one row matched account cd, one matched global account id
	                   DELETE 
	                     FROM KM.ACCOUNT
                        WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                          AND ENCRYPTED_ACCOUNT_CD != pb_encrypted_account_cd;
                       UPDATE KM.ACCOUNT
		                  SET GLOBAL_ACCOUNT_ID = pn_global_account_id,
		                      ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC),
		                      SOURCE_INSTANCE = pn_instance
		                WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
		                  AND SOURCE_INSTANCE >= pn_instance;
		               IF FOUND THEN
		                   pn_changed := TRUE;
		                   RETURN; 
		               END IF;
		               CONTINUE;
                END;
                IF pn_old_global_account_id = pn_global_account_id AND ln_old_encrypted_account_cd != pb_encrypted_account_cd THEN
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd,
                           ENCRYPTED_CRC = pb_encrypted_crc,
                           SOURCE_INSTANCE = pn_instance
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND SOURCE_INSTANCE >= pn_instance;
                    pn_changed := FOUND;
                    RETURN;
                ELSIF pn_old_instance < pn_instance OR (pn_old_instance = pn_instance AND pn_old_global_account_id >= pn_global_account_id 
                        AND (pb_encrypted_crc IS NULL OR pb_encrypted_crc IS NOT DISTINCT FROM ln_old_encrypted_crc))                   
                        THEN
                    pn_old_global_account_id := NULL;
                    pn_changed := FALSE;
                    RETURN;
                END IF;
                UPDATE KM.ACCOUNT
                   SET GLOBAL_ACCOUNT_ID = pn_global_account_id,
                       ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC),
                       SOURCE_INSTANCE = pn_instance
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
                IF FOUND THEN
                    pn_changed := TRUE;
                    RETURN; 
                END IF;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pba_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE);
    
CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lb_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC 
          INTO pn_global_account_id, pn_old_instance, lb_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd;
        IF FOUND THEN
            IF (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM lb_old_encrypted_crc) THEN
                UPDATE KM.ACCOUNT
                   SET ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)                       
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM ENCRYPTED_CRC);
                pn_changed := TRUE; 
            ELSE
                pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
            SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pb_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.UPSERT_TOKEN(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_old_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.TOKEN(GLOBAL_ACCOUNT_ID, KEY_ID, ENCRYPTED_TOKEN, ENCRYPTED_DATA, EXPIRATION_UTC_TS, INITIATING_DEVICE_NAME)
                VALUES(pn_global_account_id, pn_key_id, pb_encrypted_token, pba_encrypted_data, pd_expiration_utc_ts, pv_initiating_device_name);
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN
                UPDATE KM.TOKEN
                   SET KEY_ID = pn_key_id,
                       ENCRYPTED_TOKEN = pb_encrypted_token, 
                       ENCRYPTED_DATA = pba_encrypted_data, 
                       EXPIRATION_UTC_TS = pd_expiration_utc_ts
                 WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND INITIATING_DEVICE_NAME = pv_initiating_device_name;
                IF FOUND THEN
                    RETURN;
                ELSE
                    SELECT INITIATING_DEVICE_NAME
                      INTO lv_old_device_name
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND ENCRYPTED_TOKEN = pb_encrypted_token
                       AND INITIATING_DEVICE_NAME != pv_initiating_device_name;
                    IF FOUND THEN
                        RAISE unique_violation USING MESSAGE = 'This same token was already created for this account but by a different device';
                    END IF;
                END IF;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    TO use_km;
      
GRANT EXECUTE ON FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    TO use_km;
    
GRANT EXECUTE ON FUNCTION KM.UPSERT_TOKEN(
    pn_global_account_id KM.TOKEN.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE)
    TO use_km;
