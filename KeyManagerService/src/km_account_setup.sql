CREATE TABLE KM.ACCOUNT
(
   ENCRYPTED_ACCOUNT_CD BYTEA NOT NULL,
   GLOBAL_ACCOUNT_ID BIGINT NOT NULL,
   ENCRYPTED_CRC BYTEA,
   SOURCE_INSTANCE SMALLINT NOT NULL,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_ACCOUNT PRIMARY KEY(ENCRYPTED_ACCOUNT_CD)
) 
WITH (
  OIDS = FALSE
);

CREATE SEQUENCE KM.SEQ_ACCOUNT_ID_BASE INCREMENT BY 10 START WITH 1000000000;

CREATE UNIQUE INDEX UX_ACCOUNT_GLOBAL_ACCOUNT_ID ON KM.ACCOUNT(GLOBAL_ACCOUNT_ID);

GRANT SELECT, INSERT, UPDATE ON KM.ACCOUNT TO use_km;

GRANT USAGE ON KM.SEQ_ACCOUNT_ID_BASE TO use_km;

CREATE TRIGGER TRBU_ACCOUNT
BEFORE UPDATE ON KM.ACCOUNT
    FOR EACH ROW
    EXECUTE PROCEDURE KM.FTRBU_AUDIT();

CREATE OR REPLACE FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_old_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pba_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pba_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    ln_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
    ln_old_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pba_encrypted_account_cd, pn_global_account_id, pba_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN
                BEGIN
	                SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC, ENCRYPTED_ACCOUNT_CD
	                  INTO STRICT pn_old_global_account_id, pn_old_instance, ln_old_encrypted_crc, ln_old_encrypted_account_cd
	                  FROM KM.ACCOUNT
	                 WHERE ENCRYPTED_ACCOUNT_CD = pba_encrypted_account_cd
	                    OR GLOBAL_ACCOUNT_ID = pn_global_account_id;
	            EXCEPTION
	               WHEN no_data_found THEN -- row was deleted out form under us, try insert again
	                   CONTINUE;
	               WHEN too_many_rows THEN -- one row matched account cd, one matched global account id
	                   DELETE 
	                     FROM KM.ACCOUNT
                        WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                          AND SOURCE_INSTANCE >= pn_instance;
                       UPDATE KM.ACCOUNT
		                  SET GLOBAL_ACCOUNT_ID = pn_global_account_id,
		                      ENCRYPTED_CRC = COALESCE(pba_encrypted_crc, ENCRYPTED_CRC),
		                      SOURCE_INSTANCE = pn_instance
		                WHERE ENCRYPTED_ACCOUNT_CD = pba_encrypted_account_cd
		                  AND SOURCE_INSTANCE >= pn_instance;
		               IF FOUND THEN
		                   pn_changed := TRUE;
		                   RETURN; 
		               END IF;
		               CONTINUE;
                END;
                IF pn_old_global_account_id = pn_global_account_id AND ln_old_encrypted_account_cd != pba_encrypted_account_cd THEN
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_ACCOUNT_CD = pba_encrypted_account_cd,
                           ENCRYPTED_CRC = pba_encrypted_crc,
                           SOURCE_INSTANCE = pn_instance
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND SOURCE_INSTANCE >= pn_instance;
                    pn_changed := FOUND;
                    RETURN;
                ELSIF pn_old_instance < pn_instance OR (pn_old_instance = pn_instance AND pn_old_global_account_id = pn_global_account_id AND (pba_encrypted_crc IS NULL OR pba_encrypted_crc IS NOT DISTINCT FROM ln_old_encrypted_crc)) THEN
                    pn_old_global_account_id := NULL;
                    pn_changed := FALSE;
                    RETURN;
                END IF;
                UPDATE KM.ACCOUNT
                   SET GLOBAL_ACCOUNT_ID = pn_global_account_id,
                       ENCRYPTED_CRC = COALESCE(pba_encrypted_crc, ENCRYPTED_CRC),
                       SOURCE_INSTANCE = pn_instance
                 WHERE ENCRYPTED_ACCOUNT_CD = pba_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
                IF FOUND THEN
                    pn_changed := TRUE;
                    RETURN; 
                END IF;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pba_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pba_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lba_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC
          INTO pn_global_account_id, pn_old_instance, lba_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pba_encrypted_account_cd;
        IF FOUND THEN
            IF pba_encrypted_crc IS NOT NULL AND pba_encrypted_crc IS DISTINCT FROM lba_old_encrypted_crc THEN
	            UPDATE KM.ACCOUNT
	               SET ENCRYPTED_CRC = pba_encrypted_crc
	             WHERE ENCRYPTED_ACCOUNT_CD = pba_encrypted_account_cd
	               AND SOURCE_INSTANCE = pn_old_instance
	               AND GLOBAL_ACCOUNT_ID = pn_global_account_id
	               AND ENCRYPTED_CRC IS DISTINCT FROM pba_encrypted_crc;
	            pn_changed := TRUE; 
	        ELSE
	            pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
	        SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pba_encrypted_account_cd, pn_global_account_id, pba_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;
  
GRANT EXECUTE ON FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pba_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pba_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    TO use_km;

GRANT EXECUTE ON FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pba_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pba_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    TO use_km;
    