﻿CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID_WITH_CRC(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pb_old_encrypted_crc OUT KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC 
          INTO pn_global_account_id, pn_old_instance, pb_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd;
        IF FOUND THEN
            IF (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM pb_old_encrypted_crc) THEN
                UPDATE KM.ACCOUNT
                   SET ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)                       
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM ENCRYPTED_CRC);
                pn_changed := TRUE; 
            ELSE
                pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
            SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pb_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID_WITH_CRC(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pb_old_ecnrypted_crc OUT KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE) 
    TO use_km;

CREATE OR REPLACE FUNCTION KM.GET_OR_UPSERT_TOKEN(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token INOUT KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE,
    pn_initiating_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE,
    pn_changed OUT BOOLEAN)
    SECURITY DEFINER
AS $$
DECLARE
    lv_old_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE;
    lv_old_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE;
    lb_old_encrypted_token KM.TOKEN.ENCRYPTED_TOKEN%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.TOKEN(GLOBAL_ACCOUNT_ID, KEY_ID, ENCRYPTED_TOKEN, ENCRYPTED_DATA, EXPIRATION_UTC_TS, INITIATING_DEVICE_NAME, INITIATING_DEVICE_TRAN_CD)
                VALUES(pn_global_account_id, pn_key_id, pb_encrypted_token, pba_encrypted_data, pd_expiration_utc_ts, pv_initiating_device_name, pn_initiating_device_tran_cd);
        pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN
                UPDATE KM.TOKEN
                   SET KEY_ID = pn_key_id,
                       ENCRYPTED_TOKEN = pb_encrypted_token, 
                       ENCRYPTED_DATA = pba_encrypted_data, 
                       EXPIRATION_UTC_TS = pd_expiration_utc_ts,
                       INITIATING_DEVICE_TRAN_CD = pn_initiating_device_tran_cd
                 WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND INITIATING_DEVICE_NAME = pv_initiating_device_name
                   AND INITIATING_DEVICE_TRAN_CD <= pn_initiating_device_tran_cd;
                IF FOUND THEN
                    pn_changed := TRUE;
            RETURN;
                ELSE
                    SELECT INITIATING_DEVICE_TRAN_CD, ENCRYPTED_TOKEN
                      INTO lv_old_device_tran_cd, lb_old_encrypted_token
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND INITIATING_DEVICE_NAME = pv_initiating_device_name
                       AND INITIATING_DEVICE_TRAN_CD > pn_initiating_device_tran_cd;
                    IF FOUND THEN
                        pb_encrypted_token := lb_old_encrypted_token;    
                        pn_changed := FALSE;
            RETURN;
                    END IF;
                    SELECT INITIATING_DEVICE_NAME
                      INTO lv_old_device_name
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND ENCRYPTED_TOKEN = pb_encrypted_token
                       AND INITIATING_DEVICE_NAME != pv_initiating_device_name;
                    IF FOUND THEN
                        RAISE unique_violation USING MESSAGE = 'This same token was already created for this account but by a different device';
                    END IF;
                END IF;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.GET_OR_UPSERT_TOKEN(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token INOUT KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE,
    pn_initiating_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE,
    pn_changed OUT BOOLEAN)
    TO use_km;
    
GRANT SELECT ON ALL TABLES IN SCHEMA KM TO read_km;