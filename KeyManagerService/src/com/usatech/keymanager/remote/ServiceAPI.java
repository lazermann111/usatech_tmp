package com.usatech.keymanager.remote;

// NOTE: We must use concrete methods or java2wsdl does not use parameter names
public interface ServiceAPI {

	public StoreResponse storeBytes(byte[][] bytes, long expirationTime);

	// public abstract StoreResponse storeStrings(String[] strings, String charset, long expirationTime) { return null; }

	public RetrieveBytesResponse retrieveBytes(long encryptedId);

	// public abstract RetrieveStringsResponse retrieveStrings(long encryptedId, String charset) { return null; }
}
