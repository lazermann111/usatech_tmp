package com.usatech.keymanager.remote;

public class RetrieveStringsResponse extends Response {
	protected final String[] decryptedStrings;

	protected RetrieveStringsResponse(int resultCode, String resultMessage, String[] decryptedStrings) {
		super(resultCode, resultMessage);
		this.decryptedStrings = decryptedStrings;
	}

	public String[] getDecryptedStrings() {
		return decryptedStrings;
	}

	public static RetrieveStringsResponse createSuccessResponse(String[] decryptedStrings) {
		if(decryptedStrings == null || decryptedStrings.length == 0)
			throw new IllegalArgumentException("DecryptedStrings may not be blank for success");
		return new RetrieveStringsResponse(RESULT_CODE_SUCCESS, null, decryptedStrings);
	}

	public static RetrieveStringsResponse createFailedResponse(int resultCode, String resultMessage) {
		if(resultCode == RESULT_CODE_SUCCESS)
			throw new IllegalArgumentException("Failed result code may not equal " + RESULT_CODE_SUCCESS);
		return new RetrieveStringsResponse(resultCode, resultMessage, null);
	}
}
