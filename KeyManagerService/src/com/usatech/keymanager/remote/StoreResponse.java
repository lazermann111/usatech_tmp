package com.usatech.keymanager.remote;

public class StoreResponse extends Response {
	private static final long serialVersionUID = -6928998473446967280L;
	protected final long encryptedId;

	protected StoreResponse(int resultCode, String resultMessage, long encryptedId) {
		super(resultCode, resultMessage);
		this.encryptedId = encryptedId;
	}

	public long getEncryptedId() {
		return encryptedId;
	}

	public static StoreResponse createSuccessResponse(long encryptedId) {
		if(encryptedId == 0)
			throw new IllegalArgumentException("EncryptedId may not be zero for success");
		return new StoreResponse(RESULT_CODE_SUCCESS, null, encryptedId);
	}

	public static StoreResponse createFailedResponse(int resultCode, String resultMessage) {
		if(resultCode == RESULT_CODE_SUCCESS)
			throw new IllegalArgumentException("Failed result code may not equal " + RESULT_CODE_SUCCESS);
		return new StoreResponse(resultCode, resultMessage, 0);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		appendInfo(sb);
		if(getResultCode() == RESULT_CODE_SUCCESS)
			sb.append("; EncryptedId: ").append(encryptedId);
		return sb.toString();
	}
}
