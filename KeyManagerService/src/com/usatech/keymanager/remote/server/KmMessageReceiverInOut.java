/**
 * KmMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.usatech.keymanager.remote.server;

/**
 * KmMessageReceiverInOut message receiver
 */

public class KmMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver {

	public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext) throws org.apache.axis2.AxisFault {
		try {
			// get the implementation class for the Web Service
			Object obj = getTheImplementationObject(msgContext);

			KmSkeleton skel = (KmSkeleton) obj;
			// Out Envelop
			org.apache.axiom.soap.SOAPEnvelope envelope = null;
			// Find the axisOperation that has been set by the Dispatch phase.
			org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
			if(op == null) {
				throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
			}

			java.lang.String methodName;
			if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)) {

				if("retrieveBytes".equals(methodName)) {
					com.usatech.keymanager.remote.binding.RetrieveBytesResponseE retrieveBytesResponse3 = null;
					com.usatech.keymanager.remote.binding.RetrieveBytes wrappedParam = (com.usatech.keymanager.remote.binding.RetrieveBytes) fromOM(msgContext.getEnvelope().getBody().getFirstElement(), com.usatech.keymanager.remote.binding.RetrieveBytes.class, getEnvelopeNamespaces(msgContext.getEnvelope()));
					retrieveBytesResponse3 = wrapRetrieveBytesResponse_return(skel.retrieveBytes(getEncryptedId(wrappedParam)));
					envelope = toEnvelope(getSOAPFactory(msgContext), retrieveBytesResponse3, false, new javax.xml.namespace.QName("urn:keymgr.usatech.com", "retrieveBytes"));
				} else if("storeBytes".equals(methodName)) {
					com.usatech.keymanager.remote.binding.StoreBytesResponse storeBytesResponse8 = null;
					com.usatech.keymanager.remote.binding.StoreBytes wrappedParam = (com.usatech.keymanager.remote.binding.StoreBytes) fromOM(msgContext.getEnvelope().getBody().getFirstElement(), com.usatech.keymanager.remote.binding.StoreBytes.class, getEnvelopeNamespaces(msgContext.getEnvelope()));
					storeBytesResponse8 = wrapStoreBytesResponse_return(skel.storeBytes(getBytes(wrappedParam), getExpirationTime(wrappedParam)));
					envelope = toEnvelope(getSOAPFactory(msgContext), storeBytesResponse8, false, new javax.xml.namespace.QName("urn:keymgr.usatech.com", "storeBytes"));
				} else {
					throw new java.lang.RuntimeException("method not found");
				}

				newMsgContext.setEnvelope(envelope);
			}
		} catch(java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	//
	private org.apache.axiom.om.OMElement toOM(com.usatech.keymanager.remote.binding.RetrieveBytes param, boolean optimizeContent) throws org.apache.axis2.AxisFault {
		try {
			return param.getOMElement(com.usatech.keymanager.remote.binding.RetrieveBytes.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch(org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(com.usatech.keymanager.remote.binding.RetrieveBytesResponseE param, boolean optimizeContent) throws org.apache.axis2.AxisFault {
		try {
			return param.getOMElement(com.usatech.keymanager.remote.binding.RetrieveBytesResponseE.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch(org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(com.usatech.keymanager.remote.binding.StoreBytes param, boolean optimizeContent) throws org.apache.axis2.AxisFault {
		try {
			return param.getOMElement(com.usatech.keymanager.remote.binding.StoreBytes.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch(org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.om.OMElement toOM(com.usatech.keymanager.remote.binding.StoreBytesResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {
		try {
			return param.getOMElement(com.usatech.keymanager.remote.binding.StoreBytesResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch(org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.keymanager.remote.binding.RetrieveBytesResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName) throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.keymanager.remote.binding.RetrieveBytesResponseE.MY_QNAME, factory));
			return emptyEnvelope;
		} catch(org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private long getEncryptedId(com.usatech.keymanager.remote.binding.RetrieveBytes wrappedType) {
		return wrappedType.getEncryptedId();
	}

	private com.usatech.keymanager.remote.binding.RetrieveBytesResponseE wrapRetrieveBytesResponse_return(com.usatech.keymanager.remote.binding.RetrieveBytesResponse param) {
		com.usatech.keymanager.remote.binding.RetrieveBytesResponseE wrappedElement = new com.usatech.keymanager.remote.binding.RetrieveBytesResponseE();
		wrappedElement.set_return(param);
		return wrappedElement;
	}

	private com.usatech.keymanager.remote.binding.RetrieveBytesResponseE wrapretrieveBytes() {
		com.usatech.keymanager.remote.binding.RetrieveBytesResponseE wrappedElement = new com.usatech.keymanager.remote.binding.RetrieveBytesResponseE();
		return wrappedElement;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.keymanager.remote.binding.StoreBytesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName) throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.keymanager.remote.binding.StoreBytesResponse.MY_QNAME, factory));
			return emptyEnvelope;
		} catch(org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private javax.activation.DataHandler[] getBytes(com.usatech.keymanager.remote.binding.StoreBytes wrappedType) {
		return wrappedType.getBytes();
	}

	private long getExpirationTime(com.usatech.keymanager.remote.binding.StoreBytes wrappedType) {
		return wrappedType.getExpirationTime();
	}

	private com.usatech.keymanager.remote.binding.StoreBytesResponse wrapStoreBytesResponse_return(com.usatech.keymanager.remote.binding.StoreResponse param) {
		com.usatech.keymanager.remote.binding.StoreBytesResponse wrappedElement = new com.usatech.keymanager.remote.binding.StoreBytesResponse();
		wrappedElement.set_return(param);
		return wrappedElement;
	}

	private com.usatech.keymanager.remote.binding.StoreBytesResponse wrapstoreBytes() {
		com.usatech.keymanager.remote.binding.StoreBytesResponse wrappedElement = new com.usatech.keymanager.remote.binding.StoreBytesResponse();
		return wrappedElement;
	}

	/**
	 * get the default envelope
	 */
	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
		return factory.getDefaultEnvelope();
	}

	private java.lang.Object fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type, java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {

		try {

			if(com.usatech.keymanager.remote.binding.RetrieveBytes.class.equals(type)) {

				return com.usatech.keymanager.remote.binding.RetrieveBytes.Factory.parse(param.getXMLStreamReaderWithoutCaching());

			}

			if(com.usatech.keymanager.remote.binding.RetrieveBytesResponseE.class.equals(type)) {

				return com.usatech.keymanager.remote.binding.RetrieveBytesResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());

			}

			if(com.usatech.keymanager.remote.binding.StoreBytes.class.equals(type)) {

				return com.usatech.keymanager.remote.binding.StoreBytes.Factory.parse(param.getXMLStreamReaderWithoutCaching());

			}

			if(com.usatech.keymanager.remote.binding.StoreBytesResponse.class.equals(type)) {

				return com.usatech.keymanager.remote.binding.StoreBytesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());

			}

		} catch(java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
		return null;
	}

	/**
	 * A utility method that copies the namepaces from the SOAPEnvelope
	 */
	private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
		java.util.Map returnMap = new java.util.HashMap();
		java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
		while(namespaceIterator.hasNext()) {
			org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
			returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
		}
		return returnMap;
	}

	private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
		org.apache.axis2.AxisFault f;
		Throwable cause = e.getCause();
		if(cause != null) {
			f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
		} else {
			f = new org.apache.axis2.AxisFault(e.getMessage());
		}

		return f;
	}

}// end of class
