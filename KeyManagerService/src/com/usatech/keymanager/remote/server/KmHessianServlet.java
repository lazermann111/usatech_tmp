package com.usatech.keymanager.remote.server;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.io.Log;
import simple.text.StringUtils;

import com.caucho.hessian.server.HessianServlet;
import com.caucho.services.server.ServiceContext;
import com.usatech.keymanager.remote.RetrieveBytesResponse;
import com.usatech.keymanager.remote.ServiceAPI;
import com.usatech.keymanager.remote.StoreResponse;
import com.usatech.keymanager.service.KeyManagerEngine;

public class KmHessianServlet extends HessianServlet implements ServiceAPI {
	private static final Log log = Log.getLog();
	private static final long serialVersionUID = -7387700313650494889L;
	protected KeyManagerEngine keyManagerEngine;

	@Override
	public StoreResponse storeBytes(byte[][] bytes, long expirationTime) {
		String username = getUserWithRole("STORE_DATA");
		if(username == null)
			return StoreResponse.createFailedResponse(404, "User not authorized for this operation");
		if(keyManagerEngine == null)
			return StoreResponse.createFailedResponse(255, "Service is not bound");
		return ServiceHelper.storeBytes(keyManagerEngine, bytes, expirationTime, username);
	}

	@Override
	public RetrieveBytesResponse retrieveBytes(long encryptedId) {
		String username = getUserWithRole("RETRIEVE_DATA");
		if(username == null)
			return RetrieveBytesResponse.createFailedResponse(404, "User not authorized for this operation");
		if(keyManagerEngine == null)
			return RetrieveBytesResponse.createFailedResponse(255, "Service is not bound");
		return ServiceHelper.retrieveBytes(keyManagerEngine, encryptedId, username);
	}

	protected String getUserWithRole(String roleName) {
		ServletRequest request = ServiceContext.getContextRequest();
		if(!(request instanceof HttpServletRequest))
			return null;
		HttpServletRequest hsr = (HttpServletRequest) request;
		return hsr.isUserInRole(roleName) ? hsr.getRemoteUser() : null;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public void service(ServletRequest request, javax.servlet.ServletResponse response) throws java.io.IOException, javax.servlet.ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		if(log.isInfoEnabled())
			log.info(StringUtils.capitalizeFirst(httpRequest.getMethod().toLowerCase()) + " Request on " + httpRequest.getRequestURI());
		if("GET".equalsIgnoreCase(httpRequest.getMethod()))
			ServiceHelper.writeUsageTerms((HttpServletResponse) response);
		else
			super.service(request, response);
	}
}
