package com.usatech.keymanager.remote.server;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.AxisServlet;

import simple.io.Log;
import simple.text.StringUtils;

public class KmAxisServlet extends AxisServlet {
	private static final Log log = Log.getLog();
	private static final long serialVersionUID = -1892019058610562250L;
	protected static ThreadLocal<HttpServletRequest> localRequest = new ThreadLocal<HttpServletRequest>();
	protected static final Set<String> ALLOWED_QUERY_STRINGS = new HashSet<String>();

	static {
		ALLOWED_QUERY_STRINGS.add("wsdl");
		ALLOWED_QUERY_STRINGS.add("wsdl2");
	}

	public static HttpServletRequest getCurrentRequest() {
		return localRequest.get();
	}

	protected String trimSlash(String pathInfo) {
		if(StringUtils.isBlank(pathInfo))
			return "";
		if(pathInfo.charAt(0) == '/')
			return pathInfo.substring(1);
		return pathInfo;
	}

	protected String getServicePart(String pathInfo) {
		if(axisConfiguration == null)
			return "";
		while(true) {
			try {
				if(axisConfiguration.getService(trimSlash(pathInfo)) != null)
					return pathInfo;
			} catch(AxisFault e) {
				log.warn("Could not get service '" + pathInfo + "'", e);
				return "";
			}
			int pos = pathInfo.lastIndexOf('/');
			if(pos < 0)
				return "";
			pathInfo = pathInfo.substring(0, pos);
		}
	}

	protected boolean isServicePart(String pathInfo) {
		try {
			return axisConfiguration != null && axisConfiguration.getService(trimSlash(pathInfo)) != null;
		} catch(AxisFault e) {
			log.warn("Could not get service '" + pathInfo + "'", e);
			return false;
		}
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(log.isInfoEnabled())
			log.info("Post Request on " + request.getRequestURI());
		if(isServicePart(request.getPathInfo()) && StringUtils.isBlank(request.getQueryString())) {
			localRequest.set(request);
			super.doPost(request, response);
			localRequest.set(null);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(log.isInfoEnabled())
			log.info("Get Request on " + request.getRequestURI());
		if(isServicePart(request.getPathInfo())) {
			if(StringUtils.isBlank(request.getQueryString()))
				ServiceHelper.writeUsageTerms(response);
			else if(ALLOWED_QUERY_STRINGS.contains(request.getQueryString()))
				super.doGet(request, response);
			else
				response.sendRedirect(request.getContextPath() + request.getServletPath() + getServicePart(request.getPathInfo()));
		} else
			response.sendRedirect(request.getContextPath() + request.getServletPath() + getServicePart(request.getPathInfo()));
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
