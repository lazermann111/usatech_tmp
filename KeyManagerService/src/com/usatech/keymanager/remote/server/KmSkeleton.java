/**
 * KmSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.usatech.keymanager.remote.server;





import javax.servlet.http.HttpServletRequest;

import com.usatech.keymanager.remote.RetrieveBytesResponse;
import com.usatech.keymanager.remote.StoreResponse;
import com.usatech.keymanager.remote.binding.BindUtils;
import com.usatech.keymanager.service.KeyManagerEngine;

/**
 * KmSkeleton java skeleton for the axisService
 */
public class KmSkeleton {
	protected static KeyManagerEngine keyManagerEngine;

	/**
	 * Auto generated method signature
	 * 
	 * @param encryptedId
	 * @return retrieveBytesResponse
	 */

	public com.usatech.keymanager.remote.binding.RetrieveBytesResponse retrieveBytes(long encryptedId) {
		final RetrieveBytesResponse resp;
		String username = getUserWithRole("RETRIEVE_DATA");
		if(username == null)
			resp = RetrieveBytesResponse.createFailedResponse(404, "User not authorized for this operation");
		else if(keyManagerEngine == null)
			resp = RetrieveBytesResponse.createFailedResponse(255, "Service is not bound");
		else
			resp = ServiceHelper.retrieveBytes(keyManagerEngine, encryptedId, username);
		return BindUtils.bind(resp);
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param bytes
	 * @param expirationTime
	 * @return storeBytesResponse
	 */
	public com.usatech.keymanager.remote.binding.StoreResponse storeBytes(javax.activation.DataHandler[] bytes, long expirationTime) {
		final StoreResponse resp;
		String username = getUserWithRole("STORE_DATA");
		if(username == null)
			resp = StoreResponse.createFailedResponse(404, "User not authorized for this operation");
		else if(keyManagerEngine == null)
			resp = StoreResponse.createFailedResponse(255, "Service is not bound");
		else
			resp = ServiceHelper.storeBytes(keyManagerEngine, BindUtils.createByteArrays(bytes), expirationTime, username);
		return BindUtils.bind(resp);
	}

	protected String getUserWithRole(String roleName) {
		HttpServletRequest request = KmAxisServlet.getCurrentRequest();
		if(request == null)
			return null;
		return request.isUserInRole(roleName) ? request.getRemoteUser() : null;
	}

	public static KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public static void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		KmSkeleton.keyManagerEngine = keyManagerEngine;
	}
}
