package com.usatech.keymanager.remote.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.sql.SQLException;
import java.util.Calendar;

import javax.security.auth.login.CredentialException;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.io.Log;

import com.usatech.keymanager.remote.RetrieveBytesResponse;
import com.usatech.keymanager.remote.StoreResponse;
import com.usatech.keymanager.service.KeyManagerEngine;

public class ServiceHelper {
	private static final Log log = Log.getLog();

	public static StoreResponse storeBytes(KeyManagerEngine kme, byte[][] bytes, long expirationTime, String username) {
		if(kme == null) {
			log.warn("KeyManagerEngine not provided");
			return StoreResponse.createFailedResponse(1, "KeyManager is not configured properly");
		}
		if(kme.getKekStore() == null) {
			log.warn("KeyManager is not loaded");
			return StoreResponse.createFailedResponse(2, "KeyManager is not loaded");
		}
		long encryptedId;
		try {
			encryptedId = kme.storeBytes(bytes, expirationTime, username);
		} catch(ConvertException e) {
			log.warn("Could not store bytes in KeyManagerEngine", e);
			return StoreResponse.createFailedResponse(3, "KeyManager internal error");
		} catch(SQLException e) {
			log.warn("Could not store bytes in KeyManagerEngine", e);
			return StoreResponse.createFailedResponse(4, "KeyManager internal error");
		} catch(DataLayerException e) {
			log.warn("Could not store bytes in KeyManagerEngine", e);
			return StoreResponse.createFailedResponse(5, "KeyManager internal error");
		} catch(CredentialException e) {
			log.warn("User not authorized to perform this action", e);
			return StoreResponse.createFailedResponse(7, "User not authorized to perform this action");
		} catch(GeneralSecurityException e) {
			log.warn("Could not store bytes in KeyManagerEngine", e);
			return StoreResponse.createFailedResponse(6, "KeyManager failure");
		}

		return StoreResponse.createSuccessResponse(encryptedId);
	}

	public static RetrieveBytesResponse retrieveBytes(KeyManagerEngine kme, long encryptedId, String username) {
		if(kme == null) {
			log.warn("KeyManagerEngine not provided");
			return RetrieveBytesResponse.createFailedResponse(1, "KeyManager is not configured properly");
		}
		if(kme.getKekStore() == null) {
			log.warn("KeyManager is not loaded");
			return RetrieveBytesResponse.createFailedResponse(2, "KeyManager is not loaded");
		}
		byte[][] decryptedData;
		try {
			decryptedData = kme.retrieveBytes(encryptedId, username, null);
		} catch(ConvertException e) {
			log.warn("Could not retrieve bytes in KeyManagerEngine", e);
			return RetrieveBytesResponse.createFailedResponse(3, "KeyManager internal error");
		} catch(SQLException e) {
			log.warn("Could not retrieve bytes in KeyManagerEngine", e);
			return RetrieveBytesResponse.createFailedResponse(4, "KeyManager internal error");
		} catch(DataLayerException e) {
			log.warn("Could not retrieve bytes in KeyManagerEngine", e);
			return RetrieveBytesResponse.createFailedResponse(5, "KeyManager internal error");
		} catch(CredentialException e) {
			log.warn("User not authorized to perform this action", e);
			return RetrieveBytesResponse.createFailedResponse(7, "User not authorized to perform this action");
		} catch(KeyManagementException e) {
			log.warn("Could not retrieve bytes in KeyManagerEngine", e);
			return RetrieveBytesResponse.createFailedResponse(6, "Key Management failure (Key Expired)");
		} catch(GeneralSecurityException e) {
			log.warn("Could not retrieve bytes in KeyManagerEngine", e);
			return RetrieveBytesResponse.createFailedResponse(6, "KeyManager failure");
		}

		return RetrieveBytesResponse.createSuccessResponse(decryptedData);
	}

	public static void writeUsageTerms(HttpServletResponse response) throws IOException {
		response.addHeader("Expires", "-1");
		response.setContentType("text/html; charset=utf-8");
		PrintWriter writer = response.getWriter();
		writer.println("<html>");
		writer.println("<title>USA Technologies Key Manager</title>");
		writer.println("<body>");
		writer.println("<div align=\"center\" style=\"font-family: Arial, Verdana, Tahoma, Sans-Serif;\">");

		writer.println("<h3>USA Technologies Key Manager</h3>");
		writer.println("NOTICE TO USERS<br/><br/>");

		writer.println("This computer system is the private property of USA Technologies, Inc.<br/>");
		writer.println("*** It Is For Authorized Use Only. ***<br/><br/>");

		writer.println("Any or all uses of this system may be intercepted,<br/>");
		writer.println("audited, inspected, and monitored, recorded, disclosed<br/>");
		writer.println("to authorized site, government, and law enforcement personnel, as well<br/>");
		writer.println("as authorized officials of government agencies, both domestic and foreign.<br/>");
		writer.println("By using this system, the user consents to such interception, monitoring,<br/>");
		writer.println("recording, auditing, inspection, and disclosure at the discretion of<br/>");
		writer.println("such personnel or officials. Unauthorized or improper use of this system may<br/>");
		writer.println("result in civil and criminal penalties, and administrative or disciplinary<br/>");
		writer.println("action, as appropriate. By continuing to use this system you indicate your<br/>");
		writer.println("awareness of and consent to these terms and conditions of use.<br/><br/>");

		writer.println("LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning.<br/><br/>");

		writer.print("USA Technologies, Inc. - &copy; Copyright 2003-");
		writer.print(Calendar.getInstance().get(Calendar.YEAR));
		writer.println(" - Confidential");

		writer.println("</div>");
		writer.println("</body>");
		writer.println("</html>");

		writer.flush();
	}
}
