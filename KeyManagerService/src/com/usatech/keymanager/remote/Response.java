package com.usatech.keymanager.remote;

import java.io.Serializable;

public class Response implements Serializable {
	private static final long serialVersionUID = 5483132089697030198L;
	public static final int RESULT_CODE_SUCCESS = 0;
	protected final int resultCode;
	protected final String resultMessage;

	public Response(int resultCode, String resultMessage) {
		this.resultCode = resultCode;
		this.resultMessage = resultMessage;
	}

	public int getResultCode() {
		return resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	protected StringBuilder appendInfo(StringBuilder sb) {
		sb.append("ResultCode: ").append(getResultCode());
		if(getResultMessage() != null)
			sb.append("; ResultMessage: ").append(getResultMessage());
		return sb;
	}

	@Override
	public String toString() {
		return appendInfo(new StringBuilder()).toString();
	}
}
