package com.usatech.keymanager.remote;


public class RetrieveBytesResponse extends Response {
	private static final long serialVersionUID = -8431973982693427153L;
	protected final byte[][] decryptedBytes;

	protected RetrieveBytesResponse(int resultCode, String resultMessage, byte[][] decryptedBytes) {
		super(resultCode, resultMessage);
		this.decryptedBytes = decryptedBytes;
	}

	public byte[][] getDecryptedBytes() {
		return decryptedBytes;
	}

	public static RetrieveBytesResponse createSuccessResponse(byte[][] decryptedBytes) {
		if(decryptedBytes == null || decryptedBytes.length == 0)
			throw new IllegalArgumentException("DecryptedBytes may not be blank for success");
		return new RetrieveBytesResponse(RESULT_CODE_SUCCESS, null, decryptedBytes);
	}

	public static RetrieveBytesResponse createFailedResponse(int resultCode, String resultMessage) {
		if(resultCode == RESULT_CODE_SUCCESS)
			throw new IllegalArgumentException("Failed result code may not equal " + RESULT_CODE_SUCCESS);
		return new RetrieveBytesResponse(resultCode, resultMessage, null);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		appendInfo(sb);
		if(getResultCode() == RESULT_CODE_SUCCESS) {
			sb.append("; DecryptedBytes: [");
			for(int i = 0; i < decryptedBytes.length; i++) {
				if(i > 0)
					sb.append(", ");
				for(int k = 0; k < decryptedBytes[i].length; k++)
					sb.append('*');
			}
			sb.append(']');
		}
		return sb.toString();
	}
}
