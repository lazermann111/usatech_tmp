package com.usatech.keymanager.remote.client;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.Arrays;

import com.caucho.hessian.client.HessianProxyFactory;
import com.usatech.keymanager.remote.RetrieveBytesResponse;
import com.usatech.keymanager.remote.ServiceAPI;
import com.usatech.keymanager.remote.StoreResponse;
import com.usatech.keymanager.remote.binding.BindUtils;
import com.usatech.layers.common.ProcessingConstants;

public class KmTest {

	/**
	 * @param args
	 * @throws RemoteException
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws RemoteException, URISyntaxException, MalformedURLException {
		File keystorePath = new File(KmTest.class.getClassLoader().getResource("km.soap.test.ks").toURI());
		if(keystorePath.exists()) {
			System.setProperty("javax.net.ssl.keyStore", keystorePath.getAbsolutePath());
			System.setProperty("javax.net.ssl.trustStore", keystorePath.getAbsolutePath());
			System.setProperty("javax.net.ssl.keyStorePassword", "usatech");
			System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		}
		// String baseUrl = "https://localhost:8143/";
		String baseUrl = "https://intkls11.usatech.com/";
		// String baseUrl = "https://devkls11.usatech.com/";
		byte[][] data = new byte[][] { "THIS IS A VERY SECRET MESSAGE".getBytes(ProcessingConstants.US_ASCII_CHARSET), "DON'T LET ANYONE ELSE SEE THIS!".getBytes(ProcessingConstants.US_ASCII_CHARSET) };
		long expiration = System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000L);

		testSOAP(baseUrl, data, expiration);
		testHessian(baseUrl, data, expiration);
		// testSOAPRetrieveOther(baseUrl, data, expiration);
	}

	public static void testSOAPRetrieveOther(String baseUrl, byte[][] data, long expiration) throws RemoteException {
		KmStub stub = new KmStub(baseUrl + "soap/km");
		RetrieveBytesResponse rresp = BindUtils.unbind(stub.retrieveBytes(134));
		System.out.println("Retrieved Bytes response: " + rresp.toString());
		if(rresp.getResultCode() == 0) {
			String[] sensitiveData = decode(rresp.getDecryptedBytes(), ProcessingConstants.US_ASCII_CHARSET);
			System.out.println(Arrays.toString(sensitiveData));
		}
	}

	public static void testSOAP(String baseUrl, byte[][] data, long expiration) throws RemoteException {
		KmStub stub = new KmStub(baseUrl + "soap/km");
		StoreResponse sresp = BindUtils.unbind(stub.storeBytes(BindUtils.createDataHandlers(data), expiration));
		System.out.println("Store Response: " + sresp.toString());

		RetrieveBytesResponse rresp = BindUtils.unbind(stub.retrieveBytes(sresp.getEncryptedId()));
		System.out.println("Retrieved Bytes response: " + rresp.toString());
		if(rresp.getResultCode() == 0) {
			String[] sensitiveData = decode(rresp.getDecryptedBytes(), ProcessingConstants.US_ASCII_CHARSET);
			System.out.println(Arrays.toString(sensitiveData));
		}
	}

	public static void testHessian(String baseUrl, byte[][] data, long expiration) throws MalformedURLException {
		HessianProxyFactory hessianProxyFactory = new HessianProxyFactory();
		ServiceAPI service = (ServiceAPI) hessianProxyFactory.create(ServiceAPI.class, baseUrl + "hessian/km");
		StoreResponse sresp = service.storeBytes(data, expiration);
		System.out.println("Hessian Store Response: " + sresp.toString());
		RetrieveBytesResponse rresp = service.retrieveBytes(sresp.getEncryptedId());
		System.out.println("Hessian Retrieved Bytes response: " + rresp.toString());
		if(rresp.getResultCode() == 0) {
			String[] sensitiveData = decode(rresp.getDecryptedBytes(), ProcessingConstants.US_ASCII_CHARSET);
			System.out.println(Arrays.toString(sensitiveData));
		}
	}
	public static String[] decode(byte[][] bytes, Charset charset) {
		String[] strings = new String[bytes.length];
		for(int i = 0; i < bytes.length; i++)
			strings[i] = (bytes[i] == null ? null : new String(bytes[i], charset));
		return strings;
	}

}
