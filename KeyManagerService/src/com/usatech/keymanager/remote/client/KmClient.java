package com.usatech.keymanager.remote.client;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.Arrays;

import com.caucho.hessian.client.HessianProxyFactory;
import com.usatech.keymanager.remote.RetrieveBytesResponse;
import com.usatech.keymanager.remote.ServiceAPI;
import com.usatech.keymanager.remote.StoreResponse;
import com.usatech.keymanager.remote.binding.BindUtils;
import com.usatech.layers.common.ProcessingConstants;

public class KmClient {
	protected static final Charset US_ASCII_CHARSET = Charset.forName("US-ASCII");

	/**
	 * @param args
	 * @throws RemoteException
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws RemoteException, URISyntaxException, MalformedURLException {
		String keyStorePath = System.getProperty("javax.net.ssl.keyStore");
		if(keyStorePath == null || keyStorePath.trim().length() == 0) {
			System.err.println("Please specify the keystore using the 'javax.net.ssl.keyStore' system property (i.e. - add -Djavax.net.ssl.keyStore=... to the command line)");
			System.exit(1);
			return;
		}
		String hostname;
		if(args == null || args.length == 0 || args[0] == null || (hostname = args[0].trim()).length() == 0) {
			System.err.println("Please provide the host name of the web service as the first argument to this process");
			System.exit(2);
			return;
		}
		byte[][] data = new byte[][] { "THIS IS A VERY SECRET MESSAGE".getBytes(ProcessingConstants.US_ASCII_CHARSET), "DON'T LET ANYONE ELSE SEE THIS!".getBytes(ProcessingConstants.US_ASCII_CHARSET) };
		long expiration = System.currentTimeMillis() + (7 * 24 * 60 * 60 * 1000L);

		testSOAP(hostname, data, expiration);
		testHessian(hostname, data, expiration);
	}

	public static void testSOAP(String hostname, byte[][] data, long expiration) throws RemoteException {
		String url = "https://" + hostname + "/soap/km";
		System.out.println("Connecting to SOAP service at '" + url + "'...");
		KmStub stub = new KmStub(url);
		System.out.println("Attempting to store some sensitive data");
		StoreResponse sresp = BindUtils.unbind(stub.storeBytes(BindUtils.createDataHandlers(data), expiration));
		System.out.println("SOAP Store Response: " + sresp.toString());

		System.out.println("Attempting to retrieve that sensitive data");
		RetrieveBytesResponse rresp = BindUtils.unbind(stub.retrieveBytes(sresp.getEncryptedId()));
		System.out.println("SOAP Retrieved Bytes response: " + rresp.toString());
		if(rresp.getResultCode() == 0) {
			String[] sensitiveData = decode(rresp.getDecryptedBytes(), US_ASCII_CHARSET);
			System.out.println("Sensitive Data: " + Arrays.toString(sensitiveData));
		}
		System.out.println();
	}

	public static void testHessian(String hostname, byte[][] data, long expiration) throws MalformedURLException {
		String url = "https://" + hostname + "/hessian/km";
		System.out.println("Connecting to Hessian service at '" + url + "'...");
		HessianProxyFactory hessianProxyFactory = new HessianProxyFactory();
		ServiceAPI service = (ServiceAPI) hessianProxyFactory.create(ServiceAPI.class, url);
		System.out.println("Attempting to store some sensitive data");
		StoreResponse sresp = service.storeBytes(data, expiration);
		System.out.println("Hessian Store Response: " + sresp.toString());

		System.out.println("Attempting to retrieve that sensitive data");
		RetrieveBytesResponse rresp = service.retrieveBytes(sresp.getEncryptedId());
		System.out.println("Hessian Retrieved Bytes response: " + rresp.toString());
		if(rresp.getResultCode() == 0) {
			String[] sensitiveData = decode(rresp.getDecryptedBytes(), US_ASCII_CHARSET);
			System.out.println("Sensitive Data: " + Arrays.toString(sensitiveData));
		}
		System.out.println();
	}
	public static String[] decode(byte[][] bytes, Charset charset) {
		String[] strings = new String[bytes.length];
		for(int i = 0; i < bytes.length; i++)
			strings[i] = (bytes[i] == null ? null : new String(bytes[i], charset));
		return strings;
	}

}
