package com.usatech.keymanager.remote.binding;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;

import javax.activation.DataHandler;

import org.apache.axiom.attachments.ByteArrayDataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

import com.usatech.keymanager.remote.Response;
import com.usatech.keymanager.remote.RetrieveBytesResponse;
import com.usatech.keymanager.remote.StoreResponse;

public class BindUtils {

	protected static DataHandler createDataHandler(byte[] bytes) {
		if(bytes == null)
			return null;
		return new DataHandler(new ByteArrayDataSource(bytes));
	}

	public static DataHandler[] createDataHandlers(byte[][] bytes) {
		if(bytes == null)
			return null;
		DataHandler[] dataHandlers = new DataHandler[bytes.length];
		for(int i = 0; i < bytes.length; i++)
			dataHandlers[i] = createDataHandler(bytes[i]);
		return dataHandlers;
	}

	protected static byte[] createByteArray(DataHandler dataHandler) {
		if(dataHandler == null)
			return null;
		try {
			return ConvertUtils.convert(byte[].class, dataHandler.getContent());
		} catch(ConvertException e) {
			throw new UndeclaredThrowableException(e);
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
	}

	public static byte[][] createByteArrays(DataHandler[] dataHandlers) {
		if(dataHandlers == null)
			return null;
		byte[][] bytes = new byte[dataHandlers.length][];
		for(int i = 0; i < dataHandlers.length; i++)
			bytes[i] = createByteArray(dataHandlers[i]);
		return bytes;
	}

	public static RetrieveBytesResponse unbind(com.usatech.keymanager.remote.binding.RetrieveBytesResponse bound) {
		int resultCode = bound.getResultCode();
		if(resultCode == Response.RESULT_CODE_SUCCESS)
			return RetrieveBytesResponse.createSuccessResponse(BindUtils.createByteArrays(bound.getDecryptedBytes()));
		return RetrieveBytesResponse.createFailedResponse(resultCode, bound.getResultMessage());
	}

	public static RetrieveBytesResponse unbind(com.usatech.keymanager.remote.client.KmStub.RetrieveBytesResponse bound) {
		int resultCode = bound.getResultCode();
		if(resultCode == Response.RESULT_CODE_SUCCESS)
			return RetrieveBytesResponse.createSuccessResponse(BindUtils.createByteArrays(bound.getDecryptedBytes()));
		return RetrieveBytesResponse.createFailedResponse(resultCode, bound.getResultMessage());
	}

	public static com.usatech.keymanager.remote.binding.RetrieveBytesResponse bind(RetrieveBytesResponse resp) {
		com.usatech.keymanager.remote.binding.RetrieveBytesResponse bound = new com.usatech.keymanager.remote.binding.RetrieveBytesResponse();
		bound.setDecryptedBytes(BindUtils.createDataHandlers(resp.getDecryptedBytes()));
		bound.setResultCode(resp.getResultCode());
		bound.setResultMessage(resp.getResultMessage());
		return bound;
	}

	public static StoreResponse unbind(com.usatech.keymanager.remote.binding.StoreResponse bound) {
		int resultCode = bound.getResultCode();
		if(resultCode == Response.RESULT_CODE_SUCCESS)
			return StoreResponse.createSuccessResponse(bound.getEncryptedId());
		return StoreResponse.createFailedResponse(resultCode, bound.getResultMessage());
	}

	public static StoreResponse unbind(com.usatech.keymanager.remote.client.KmStub.StoreResponse bound) {
		int resultCode = bound.getResultCode();
		if(resultCode == Response.RESULT_CODE_SUCCESS)
			return StoreResponse.createSuccessResponse(bound.getEncryptedId());
		return StoreResponse.createFailedResponse(resultCode, bound.getResultMessage());
	}

	public static com.usatech.keymanager.remote.binding.StoreResponse bind(StoreResponse resp) {
		com.usatech.keymanager.remote.binding.StoreResponse bound = new com.usatech.keymanager.remote.binding.StoreResponse();
		bound.setEncryptedId(resp.getEncryptedId());
		bound.setResultCode(resp.getResultCode());
		bound.setResultMessage(resp.getResultMessage());
		return bound;
	}
}
