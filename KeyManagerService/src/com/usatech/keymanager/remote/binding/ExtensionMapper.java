
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package com.usatech.keymanager.remote.binding;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://keymgr.usatech.com".equals(namespaceURI) &&
                  "Response".equals(typeName)){
                   
                            return  com.usatech.keymanager.remote.binding.Response.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://keymgr.usatech.com".equals(namespaceURI) &&
                  "RetrieveBytesResponse".equals(typeName)){
                   
                            return  com.usatech.keymanager.remote.binding.RetrieveBytesResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://keymgr.usatech.com".equals(namespaceURI) &&
                  "StoreResponse".equals(typeName)){
                   
                            return  com.usatech.keymanager.remote.binding.StoreResponse.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    