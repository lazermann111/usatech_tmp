package com.usatech.keymanager.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.io.ByteOutput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.mq.app.AnyTwoInstanceWorkQueue;
import simple.mq.app.MultiInstanceWorkQueue;
import simple.results.DatasetUtils;
import simple.text.StringUtils;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.keymanager.service.RetrieveTask.RetrieveResult;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.Tallier;

public class MassRetrieveTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	protected ResourceFolder resourceFolder;
	protected String instance;
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
			byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);	
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);
			int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
			Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
			
			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.NONBLOCKING_RETRY, true);
			boolean handled;
			String resultResourceKey;
			Tallier<String> retrieveQueues;
			Resource resource = rf.getResource(resourceKey, ResourceMode.READ);
			try {
				Resource resultResource = rf.getResource(resource.getName(), ResourceMode.CREATE);
				try {
					CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, resource.getInputStream());
					try {
						resultResourceKey = resultResource.getKey();
						CipherOutputStream cos = Cryption.createEncryptingOutputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, resultResource.getOutputStream());
						try {
							RetrievingDataGenerator rdg = new RetrievingDataGenerator(new OutputStreamByteOutput(cos), cnt, charset);
							DatasetUtils.parseDataset(new InputStreamByteInput(cis), rdg);
							cos.flush();
							handled = rdg.handled;
							retrieveQueues = rdg.retrieveQueues;
						} catch(IOException e) {
							throw new ServiceException("Could not write decrypted crypto file", e);
						} finally {
							cos.close();
						}
					} finally {
						cis.close();
					}
				} finally {
					resultResource.release();
				}
				DeleteResourceTask.requestResourceDeletion(resource, taskInfo.getPublisher());
			} finally {
				resource.release();
			}
			if(!handled) {
				step.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resultResourceKey);
				String[] excludes = step.getAttributeSafely(AuthorityAttrEnum.ATTR_EXCLUDE_INSTANCES, String[].class, null);
				if(excludes != null)
					retrieveQueues.exclude(excludes);
				if(retrieveQueues.getInstanceCount() == 0) {
					log.error("No instances are left to process encrypted data lists of some entries; leaving data null on them and continuing");
					// we need to fail this transaction so send it on to the next step with nothing set (data will be null)
					step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resultResourceKey);
					return 0;
				}
				if(excludes == null)
					excludes = new String[] { getInstance() };
				else {
					String[] tmp = new String[excludes.length + 1];
					System.arraycopy(excludes, 0, tmp, 1, excludes.length);
					tmp[0] = getInstance();
					excludes = tmp;
				}

				step.setAttribute(AuthorityAttrEnum.ATTR_EXCLUDE_INSTANCES, excludes);
				String baseQueueName = MultiInstanceWorkQueue.getBaseQueueName(step.getQueueKey());
				String queueName = AnyTwoInstanceWorkQueue.constructQueueName(baseQueueName, retrieveQueues.getWinners(2));
				step.setQueueKey(queueName);
				return 1;
			}
			step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resultResourceKey);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException) e.getCause();
			throw new ServiceException(e);
		}
		return 0;
	}

	protected class RetrievingDataGenerator extends AbstractAttributeDatasetHandler<IOException> {
		protected final ByteOutput output;
		protected final int cnt;
		protected final int offset = 1;
		protected final Charset charset;
		protected final Object[] values;
		protected final Tallier<String> retrieveQueues = new Tallier<String>(String.class);
		protected boolean handled = true;

		public RetrievingDataGenerator(ByteOutput output, int cnt, Charset charset) {
			this.output = output;
			this.cnt = cnt;
			this.charset = charset;
			if(cnt == 0)
				values = new Object[1 + offset];
			else
				values = new Object[cnt + offset];
		}

		public void handleDatasetEnd() throws IOException {
			DatasetUtils.writeFooter(output);
		}

		public void handleDatasetStart(String[] columnNames) throws IOException {
			String[] outputColumnNames = new String[values.length];
			outputColumnNames[0] = AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue();
			if(cnt == 0) {
				outputColumnNames[offset] = CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue();
			} else {
				for(int i = 0; i < cnt; i++)
					outputColumnNames[offset + i] = CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + '.' + (i + 1);
			}
			DatasetUtils.writeHeader(output, outputColumnNames);
		}

		public void handleRowEnd() throws IOException {
			try {
				retrieveValues();
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			} catch(GeneralSecurityException e) {
				throw new IOException(e);
			} catch(ServiceException e) {
				throw new IOException(e);
			} catch(SQLException e) {
				throw new IOException(e);
			} catch(DataLayerException e) {
				throw new IOException(e);
			} catch(ConvertException e) {
				throw new IOException(e);
			}
			DatasetUtils.writeRow(output, values);
			data.clear();
		}

		protected void retrieveValues() throws AttributeConversionException, ServiceException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
			String encryptedIdList = getDetailAttribute(AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST, String.class, false);
			values[0] = encryptedIdList;
			if(cnt == 0) {
				Object origDecryptedData = getDetailAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, Object.class, false);
				if(origDecryptedData != null) {
					values[offset] = origDecryptedData;
					return;
				}
			} else {
				Object origDecryptedData = getDetailIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 1, Object.class, false);
				if(origDecryptedData != null) {
					values[offset] = origDecryptedData;
					for(int i = 1; i < cnt; i++)
						values[offset + i] = getDetailIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, i + 1, Object.class, false);
					return;
				}
			}
			for(int i = 0; i < cnt; i++)
				values[offset + i] = null;
			if(StringUtils.isBlank(encryptedIdList))
				return;
			handled &= RetrieveTask.retrieve(encryptedIdList, retrieveQueues, getInstance(), keyManagerEngine, new RetrieveResult() {
				public void setExpirationTime(long expirationTime) {
					// Do nothing
				}

				public void setDecryptedData(byte[][] decryptedData) {
					for(int i = 0; i < decryptedData.length && i < values.length - offset; i++)
						values[offset + i] = (charset == null ? decryptedData[i] : new String(decryptedData[i], charset));
				}

				public void setEncryptedIdList(String encryptedIdList) {
					values[0] = encryptedIdList;
				}
			});
		}
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}
}
