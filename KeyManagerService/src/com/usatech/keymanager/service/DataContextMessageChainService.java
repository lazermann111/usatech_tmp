package com.usatech.keymanager.service;

import org.apache.cayenne.access.DataContext;

import com.usatech.app.MessageChainService;

public class DataContextMessageChainService extends MessageChainService {
	protected String domainName;
	public DataContextMessageChainService(String serviceName) {
		super(serviceName);
	}

	@Override
	protected void threadStarting(WorkQueueServiceThread thread) {
		DataContext.bindThreadDataContext(getDomainName() == null ? DataContext.createDataContext() : DataContext.createDataContext(getDomainName()));
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
}
