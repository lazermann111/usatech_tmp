package com.usatech.keymanager.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.crypto.CipherInputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.BatchUpdateDatasetHandler;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class MassUpdateAccountTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected int maxBatchSize = 100;
	protected short instance;
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
	        byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);	
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);	
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);
			boolean skipSameInstance = step.getAttributeDefault(CommonAttrEnum.ATTR_SKIP_ON_SAME_INSTANCE, Boolean.class, false);
			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.BLOCKING_RETRY, true);
			Resource inputResource = rf.getResource(resourceKey, ResourceMode.READ);
			try {
				CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, inputResource.getInputStream());
	    		try {
					int rows = -1;
					Connection conn = DataLayerMgr.getConnection("KM", false);
					try {
						rows = DatasetUtils.parseDataset(new InputStreamByteInput(cis), new MassAccountUpdate(conn, "BATCH_UPSERT_ACCOUNT_ID", getMaxBatchSize(), skipSameInstance));
						log.info("Processed " + rows + " for account update");
					} finally {
						if(rows < 0 && !conn.getAutoCommit())
							conn.rollback();
						conn.close();
					}
				} finally {
					cis.close();
				}			
			} finally {
				inputResource.release();
			}
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	protected class MassAccountUpdate extends BatchUpdateDatasetHandler {
		protected final boolean skipSameInstance;

		public MassAccountUpdate(Connection conn, String callId, int maxBatchSize, boolean skipSameInstance) throws ParameterException, CallNotFoundException {
			super(conn, callId, maxBatchSize);
			this.skipSameInstance = skipSameInstance;
		}
		@Override
		public void handleRowEnd() throws ServiceException {
			try {
				if(skipSameInstance) {
					Short instance = getDetailAttribute(AuthorityAttrEnum.ATTR_INSTANCE, Short.class, false);
					if(instance != null && instance == getInstance())
						return;
				}
				byte[] accountCdHash = getDetailAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, byte[].class, true);
				Integer trackCrc = getDetailAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, Integer.class, false);
				data.put("encryptedAccountCd", encrypt(accountCdHash));
				if(trackCrc != null)
					data.put("encryptedCrc", encryptCrc(trackCrc));
			} catch(AttributeConversionException e) {
				throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
			} catch(GeneralSecurityException e) {
				throw new ServiceException(e);
			}
			super.handleRowEnd();
		}
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public short getInstance() {
		return instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}
}
