package com.usatech.keymanager.service;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.Tallier;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.mq.app.AnyTwoInstanceWorkQueue;
import simple.mq.app.MultiInstanceWorkQueue;
import simple.results.Results;

public class RetrieveTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	protected String instance;
	protected static final Set<String> otherInstances = new LinkedHashSet<>();

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		boolean fullyHandled;
		MessageChainStep step = taskInfo.getStep();
		Tallier<String> retrieveQueues = new Tallier<String>(String.class);
		try {
			fullyHandled = process(step, retrieveQueues);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		}
		
		if(fullyHandled)
			return 0;
		String[] excludes = step.getAttributeSafely(AuthorityAttrEnum.ATTR_EXCLUDE_INSTANCES, String[].class, null);
		if(excludes == null)
			excludes = new String[] { getInstance() };
		else {
			String[] tmp = new String[excludes.length + 1];
			System.arraycopy(excludes, 0, tmp, 1, excludes.length);
			tmp[0] = getInstance();
			excludes = tmp;
		}
		retrieveQueues.exclude(excludes);
		if(retrieveQueues.getInstanceCount() == 0) {
			log.error("No instances are left to process card lists of some entries; leaving pan and exp date null on them and continuing");
			// we need to fail this transaction so send it on to the next step with nothing set (pan and expDate will be null)
			return 0;
		}

		step.setAttribute(AuthorityAttrEnum.ATTR_EXCLUDE_INSTANCES, excludes);
		String baseQueueName = MultiInstanceWorkQueue.getBaseQueueName(step.getQueueKey());
		String queueName = AnyTwoInstanceWorkQueue.constructQueueName(baseQueueName, retrieveQueues.getWinners(2));
		step.setQueueKey(queueName);
		return 1;
	}

	protected boolean process(final MessageChainStep step, Tallier<String> retrieveQueues) throws AttributeConversionException, ServiceException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
		int rowCnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENTRY_COUNT, Integer.class, 0);
		if(rowCnt > 0) {
			boolean fullyHandled = true;
			for(int index = 1; index <= rowCnt; index++)
				fullyHandled &= process(step, index, retrieveQueues);
			return fullyHandled;
		}
		return process(step, 0, retrieveQueues);
	}

	protected boolean process(final MessageChainStep step, final int index, Tallier<String> retrieveQueues) throws AttributeConversionException, ServiceException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
		final int cnt = step.getOptionallyIndexedAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, index, Integer.class, 0);
		// Do we need to process this?
		final StringBuilder sb = new StringBuilder();
		sb.append(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue());
		if(cnt > 0)
			sb.append(".1");
		if(index > 0)
			sb.append('.').append(index);

		if(step.getResultAttributes().get(sb.toString()) != null)
			return true;

		String encryptedIdList = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, index, String.class, index < 1);
		if(encryptedIdList == null)
			return true;
		final Charset charset = step.getOptionallyIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, index, Charset.class, false);
		return retrieve(encryptedIdList, retrieveQueues, getInstance(), keyManagerEngine, new RetrieveResult() {
			public void setExpirationTime(long expirationTime) {
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, index, expirationTime);
			}

			public void setDecryptedData(byte[][] decryptedData) {
				if(cnt == 0) {
					if(decryptedData.length > 0 && decryptedData[0] != null)
						step.setOptionallyIndexedResultAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, index, charset == null ? decryptedData[0] : new MaskedString(new String(decryptedData[0], charset)));
				} else {
					int end = CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue().length() + 1;
					final Attribute decryptedDataAttribute = new Attribute() {
						public String getValue() {
							return sb.toString();
						}
					};
					for(int i = 0; i < cnt && i < decryptedData.length; i++) {
						if(decryptedData[i] != null) {
							sb.setLength(end);
							sb.append(i + 1);
							step.setOptionallyIndexedResultAttribute(decryptedDataAttribute, index, charset == null ? decryptedData[i] : new MaskedString(new String(decryptedData[i], charset)));
						}
					}
				}
			}

			public void setEncryptedIdList(String encryptedIdList) {
				step.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, index, encryptedIdList);
			}
		});
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public static interface RetrieveResult {
		public void setEncryptedIdList(String encryptedIdList);

		public void setDecryptedData(byte[][] decryptedData);

		public void setExpirationTime(long expirationTime);
	}

	public static boolean retrieve(String encryptedIdList, Tallier<String> retrieveQueues, String currentInstance, KeyManagerEngine keyManagerEngine, RetrieveResult result) throws ServiceException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
		byte[][] decrypted;
		final AtomicLong expirationTime = new AtomicLong();
		if(encryptedIdList.startsWith("A:")) { // tokenizeaccount using km.account table instead
			int pos = encryptedIdList.indexOf(':', 2);
			String initiatingDeviceName;
			long globalAccountId;
			if(pos > 1) {
				initiatingDeviceName = encryptedIdList.substring(2, pos);
				globalAccountId = ConvertUtils.getLong(encryptedIdList.substring(pos + 1));
			} else {
				initiatingDeviceName = null;
				globalAccountId = ConvertUtils.getLong(encryptedIdList.substring(2));
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("globalAccountId", globalAccountId);
			params.put("initiatingDeviceName", initiatingDeviceName);
			log.info("Retrieving card data for globalAccountId " + globalAccountId);
			Results results = DataLayerMgr.executeQuery("GET_ACCOUNT_BY_DEVICE", params);
			if(results.next()) {
				Long keyId = results.getValue("keyId", Long.class);
				if(keyId == null) {
					log.info("Instance " + currentInstance + " does not have KeyId for card data of globalAccountId " + globalAccountId + "; forwarding to next instance");
					if(retrieveQueues.getInstanceCount() == 0)
						for(String otherInstance : otherInstances)
							retrieveQueues.vote(otherInstance);
					String sourceInstance = String.valueOf(globalAccountId % 10);
					if(!currentInstance.equals(sourceInstance) && (otherInstances.isEmpty() || otherInstances.contains(sourceInstance)))
						retrieveQueues.vote(sourceInstance);
					return false;
				}
				byte[][] encryptedData = results.getValue("encryptedData", byte[][].class);
				if(encryptedData == null || encryptedData.length == 0) {
					log.info("Instance " + currentInstance + " does not have card data for globalAccountId " + globalAccountId + "; forwarding to next instance");
					if(retrieveQueues.getInstanceCount() == 0)
						for(String otherInstance : otherInstances)
							retrieveQueues.vote(otherInstance);
					String sourceInstance = String.valueOf(globalAccountId % 10);
					if(!currentInstance.equals(sourceInstance) && (otherInstances.isEmpty() || otherInstances.contains(sourceInstance)))
						retrieveQueues.vote(sourceInstance);
					return false;
				}

				decrypted = new byte[encryptedData.length][];
				for(int i = 0; i < encryptedData.length; i++) {
					if(encryptedData[i] != null)
						decrypted[i] = keyManagerEngine.decrypt(keyId, encryptedData[i]);
				}
			} else {
				log.info("Instance " + currentInstance + " does not have globalAccountId " + globalAccountId + "; forwarding to next instance");
				if(retrieveQueues.getInstanceCount() == 0)
					for(String otherInstance : otherInstances)
						retrieveQueues.vote(otherInstance);
				String sourceInstance = String.valueOf(globalAccountId % 10);
				if(!currentInstance.equals(sourceInstance) && (otherInstances.isEmpty() || otherInstances.contains(sourceInstance)))
					retrieveQueues.vote(sourceInstance);
				return false;
			}
		} else {
			Map<String, Long> parsedMap = new HashMap<String, Long>();
			try {
				InteractionUtils.parseCardIdList(encryptedIdList, parsedMap);
			} catch(ParseException e) {
				throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
			}
			Long cardId = parsedMap.remove(currentInstance);
			if(cardId == null) {// send to next instance
				log.info("Instance " + currentInstance + " does not have this card; forwarding to next instance");
				Set<String> instances = parsedMap.keySet();
				if(!otherInstances.isEmpty())
					instances.retainAll(otherInstances);
				retrieveQueues.vote(instances);
				return false;
			}
			log.info("Retrieving card #" + cardId);
			try {
				decrypted = keyManagerEngine.retrieveBytes(cardId, KeyManagerEngine.INTERNAL_USER_NAME, expirationTime);
			} catch(NotEnoughRowsException e) {
				if(parsedMap.isEmpty()) {
					log.error("No instances can process card list " + encryptedIdList + "; leaving pan and exp date null");
					// we need to fail this transaction so send it on to the next step with nothing set (pan and expDate will be null)
					return true;
				}
				log.error("Instance " + currentInstance + " does not have card #" + cardId + "; forwarding to next instance");
				String newCardIdList = InteractionUtils.buildCardIdList(parsedMap);
				result.setEncryptedIdList(newCardIdList);
				Set<String> instances = parsedMap.keySet();
				if(!otherInstances.isEmpty())
					instances.retainAll(otherInstances);
				retrieveQueues.vote(instances);
				return false;
			} catch(KeyManagementException e) {
				if(parsedMap.isEmpty()) {
					log.error("No instances can process card list " + encryptedIdList + "; leaving pan and exp date null");
					// we need to fail this transaction so send it on to the next step with nothing set (pan and expDate will be null)
					return true;
				}
				log.error("Instance " + currentInstance + " can not process card #" + cardId + "; forwarding to next instance");
				String newCardIdList = InteractionUtils.buildCardIdList(parsedMap);
				result.setEncryptedIdList(newCardIdList);
				Set<String> instances = parsedMap.keySet();
				if(!otherInstances.isEmpty())
					instances.retainAll(otherInstances);
				retrieveQueues.vote(instances);
				return false;
			}
		}
		result.setExpirationTime(expirationTime.longValue());
		result.setDecryptedData(decrypted);
		return true;
	}

	public static String[] getOtherInstances() {
		return otherInstances.isEmpty() ? null : otherInstances.toArray(new String[otherInstances.size()]);
	}

	public static void setOtherInstances(String[] otherInstances) {
		RetrieveTask.otherInstances.clear();
		if(otherInstances != null)
			for(String instance : otherInstances)
				if(instance != null && !(instance = instance.trim()).isEmpty())
					RetrieveTask.otherInstances.add(instance);
	}
}
