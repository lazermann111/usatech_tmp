package com.usatech.keymanager.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.Configuration;

import com.usatech.keymanager.master.AbstractKeyManagerLoader;
import com.usatech.keymanager.master.KeyManagerLoader;

import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.app.ServiceManagerWithConfig;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

public class KeyManagerService extends MainWithConfig {
	private static final Log log = Log.getLog();
	protected static final Set<String> JMX_READABLE_PROPERTIES = Collections.singleton("loaderState");
	protected boolean attemptLoad;
	protected String loadFile;

	public class KeyManagerServiceManager extends ServiceManagerWithConfig {
		protected KeyManagerLoader loader;

		protected KeyManagerServiceManager() {
		}
		@Override
		protected void startup() throws ServiceException {
			Object kml = getNamedReference("com.usatech.keymanager.master.KeyManagerLoader");
			if(kml instanceof KeyManagerLoader)
				loader = (KeyManagerLoader) kml;
			super.startup();
			if(attemptLoad) {
				if(loader instanceof AbstractKeyManagerLoader) {
					InputStream in;
					boolean close;
					if(StringUtils.isBlank(loadFile)) {
						in = System.in;
						close = false;
						log.info("Reloading KEKStore from standard input");
					} else {
						try {
							in = new FileInputStream(loadFile);
						} catch(FileNotFoundException e) {
							throw new ServiceException("Could not open file '" + loadFile + "' for load", e);
						}
						close = true;
						log.info("Reloading KEKStore from file " + loadFile);
					}
					try {
						((AbstractKeyManagerLoader) loader).loadKekStore(in, KeyManagerServiceLoader.getReloaderPassword());
					} finally {
						if(close)
							try {
								in.close();
							} catch(IOException e) {
								// ignore
							}
					}
				} else
					log.warn("Could not reload KEKStore from standard input because KeyManagerLoader is " + kml);
			}
		}

		public String getLoaderState() {
			if(loader == null)
				return "UNKNOWN";
			int state = loader.getState();
			if(state >= 0 && state < KeyManagerLoader.STATE_NAMES.length)
				return KeyManagerLoader.STATE_NAMES[state];
			return "INVALID (" + state + ")";
		}

		@Override
		public Set<String> getJMXReadableProperties() {
			return JMX_READABLE_PROPERTIES;
		}
	}
	public static void main(String[] args) {
		new KeyManagerService().run(args);
	}

	protected ServiceManagerWithConfig getServiceManager(Configuration config) throws ServiceException {
		return new KeyManagerServiceManager();
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineSwitch('f', "load-file", true, true, "loadFile", "The file containing the saved kek store (NOTE: Use of this option is insecure)");
	}

	@Override
	protected void registerDefaultActions() {
		super.registerDefaultActions();
		registerAction("load", new Action() {
			public void perform(Map<String, Object> argumentMap, Configuration config) {
				log.info("Starting service manager before attempting reload");
				attemptLoad = true;
				loadFile = ConvertUtils.getStringSafely(argumentMap.get("loadFile"));
				startServiceManager(argumentMap, config);
			}
		});
	}
}
