package com.usatech.keymanager.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.login.CredentialException;

import org.apache.cayenne.conf.Configuration;
import org.apache.cayenne.conf.DefaultConfiguration;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.keymanager.CryptoException;
import com.usatech.keymanager.apple.vas.data.VasData;
import com.usatech.keymanager.apple.vas.exception.InvalidCipherException;
import com.usatech.keymanager.master.KEKException;
import com.usatech.keymanager.master.KEKStore;
import com.usatech.keymanager.master.PersistentKey;
import com.usatech.keymanager.service.KeyGeneratorFactory.SecretKeyInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.KeyVariantType;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.util.EMVIdtechDecryptValueHandler;
import com.usatech.layers.common.util.Vend3ARParsing;
import com.usatech.layers.common.util.Vend3ARTransactionResponseData;
import com.usatech.layers.common.util.VendXEnhancedEncryptedMSRData;
import com.usatech.layers.common.util.VendXParsing;

import simple.app.RetrySpecifiedServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Base64;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.io.TLVParser;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;

public class KeyManagerEngine implements KEKStorePatron {
	private static final byte INGENICO_TRACK2_BEGIN_DELIMITER = (byte)';';
	private static final byte INGENICO_TRACK2_END_DELIMITER = (byte)'?';
	private static final Log log = Log.getLog();
	private static final byte PADDING = 0x00;
	private static final byte[] NULL8 = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	private static final IvParameterSpec nullIvParamSpec = new IvParameterSpec(NULL8);
	private static final byte [] NULL16 = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	private static final IvParameterSpec nullIvParamSpec16 = new IvParameterSpec(NULL16);
	protected static final TLVParser TLV_PARSER = new TLVParser();

	/*
	protected static final byte[] OTI_TLV_PARTIAL_PAN_TAG = ByteArrayUtils.fromHex("DF8168");
	protected static final byte[] OTI_TLV_PCD_TRANSACTION_RESULT_TAG = ByteArrayUtils.fromHex("DF69");
	protected static final byte[] OTI_TLV_DATA_CRYPTOGRAM_CONFIGURATION_TAG = ByteArrayUtils.fromHex("DF8201");
	protected static final byte[] OTI_TLV_DATA_PADDING_ALGORITHM_TAG = ByteArrayUtils.fromHex("DF8202");	
	*/

	public static final String INTERNAL_USER_NAME = "<INTERNAL>";
	protected final KeyGeneratorFactory keyGeneratorFactory = new KeyGeneratorFactory();
	protected final PersistentKeyCache persistentKeyCache = new PersistentKeyCache();
	protected KEKStore kekStore;
	protected int maxEncryptionsPerKey = 100000;
	protected long maxAgeForKey = 604800000;
	protected long maxExpirationGap = 604800000;
	protected boolean emvEnabled = false;
	protected final ConcurrentNavigableMap<Long, PersistentKey> persistentKeyInfoMap = new ConcurrentSkipListMap<Long, PersistentKey>();
	
	public static class PanAndExpDate {
		public final String pan;
		public final String expDate;

		public PanAndExpDate(String pan, String expDate) {
			this.pan = pan;
			this.expDate = expDate;
		}
	}
	
	public static class ProcessedVasData {
		protected EntryType entryType;
		protected String accountData;		

		public ProcessedVasData(EntryType entryType, String accountData) {
			this.entryType = entryType;
			this.accountData = accountData;
		}

		public EntryType getEntryType() {
			return entryType;
		}

		public void setEntryType(EntryType entryType) {
			this.entryType = entryType;
		}

		public String getAccountData() {
			return accountData;
		}

		public void setAccountData(String accountData) {
			this.accountData = accountData;
		}
	}

	protected final Map<CardReaderType, Long> bdkIndexMap = new EnumMap<>(CardReaderType.class);

	// NOTE: This was copied from src-webapp/com/usatech/keymanager/KeyManagerService.java, then adapted
	public byte[] decrypt(long keyId, byte[] encryptedData) throws GeneralSecurityException {
		PersistentKey key = persistentKeyCache.getOrCreate((int) keyId);

		Provider provider = Security.getProvider(key.getProviderName());
		if(provider == null)
			throw new GeneralSecurityException("Provider not found: " + key.getProviderName());

		int blockSize = key.getIvSizeBytes();
		boolean useCBC = encryptedData.length > blockSize;
		String transformation = key.getTransformation();
		if(!useCBC)
			transformation = transformation.replace("/CBC/", "/ECB/");

		Cipher cipher = Cipher.getInstance(transformation, provider);
		if(cipher == null)
			throw new GeneralSecurityException("Cipher not found: " + transformation);

		byte[] decrypted = null;
		if(useCBC) {
			byte[] encrypted = new byte[encryptedData.length - blockSize];
			System.arraycopy(encryptedData, 0, encrypted, 0, encrypted.length);

			byte[] ivBytes = new byte[blockSize];
			System.arraycopy(encryptedData, encrypted.length, ivBytes, 0, blockSize);
			IvParameterSpec iv = new IvParameterSpec(ivBytes);
			cipher.init(Cipher.DECRYPT_MODE, key.getSecretKey(), iv);
			decrypted = cipher.doFinal(encrypted);
		} else {
			cipher.init(Cipher.DECRYPT_MODE, key.getSecretKey());
			decrypted = cipher.doFinal(encryptedData);
		}

		boolean padded = false;
		byte[] unpadded = null;

		for(int i = decrypted.length - 1; i >= 0; i--) {
			if(decrypted[i] == PADDING)
				padded = true;
			else {
				unpadded = new byte[i + 1];
				System.arraycopy(decrypted, 0, unpadded, 0, unpadded.length);
				break;
			}
		}

		if(log.isInfoEnabled())
			log.info("SERVICE_SUCCESS: decrypt(" + keyId + "," + encryptedData.length + " bytes)");

		return padded ? unpadded : decrypted;
	}

	// NOTE: This was copied from src-webapp/com/usatech/keymanager/KeyManagerService.java, then adapted
	public byte[][] decryptDUKPT(AbstractAccountTask task, MessageChainTaskInfo taskInfo, CardReaderType cardReaderType, EntryType entryType, byte[] keySerialNum, byte[][] encryptedData, int index, Pattern acctPattern, Charset charset) throws GeneralSecurityException, InvalidValueException, RetrySpecifiedServiceException {
		MessageChainStep step = taskInfo.getStep();
		long start;
		String ksnHex;
		if(log.isInfoEnabled()) {
			start = System.currentTimeMillis();
			ksnHex = StringUtils.toHex(keySerialNum);
		} else {
			start = 0;
			ksnHex = null;
		}
		String algorithm;
		Long bdkIndex;
		if(!StringUtils.isBlank(cardReaderType.getCipherName())) {
			String[] cipherParts = StringUtils.split(cardReaderType.getCipherName(), '/');
			switch(cipherParts.length) {
				case 1:
				case 2:
				case 3:
					algorithm = cipherParts[0];
					break;
				default:
					throw new GeneralSecurityException("Invalid cipherName '" + cardReaderType.getCipherName() + "': must be of the form 'algorithm/encoding/padding'");
			}
			bdkIndex = bdkIndexMap.get(cardReaderType);
			if(bdkIndex == null)
				throw new GeneralSecurityException("No BDK configured for Card Reader Type " + cardReaderType + " (" + cardReaderType.getValue() + ")");

			if(kekStore == null) {
				log.warn("SERVICE_FAIL: DUKPTDecrypt() : " + "KeyStore is not loaded");
				throw new GeneralSecurityException("KeyStore is not loaded");
			}
		} else {
			algorithm = "NONE";
			bdkIndex = null;
		}
		byte[][] decrypted = new byte[2 * encryptedData.length][];
		for(int i = 0; i < encryptedData.length; i++) {
			if(log.isDebugEnabled())
				log.debug("SERVICE_START: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + bdkIndex + ", ksn: " + ksnHex + ", " + encryptedData[i].length + " bytes)");
			try {
				byte keyVariantTypeId = cardReaderType.getKeyVariantType().getValue();
				byte[] descriptor = null;
				boolean finished = false;
				switch (cardReaderType) {
					case IDTECH_VENDX_UNPARSED:
						//See IDTech NEO Interface Developers Guide
						try {
							// Raw blob from Vendi including the ViVOtech2\0 header
							if (encryptedData[i] == null)
								throw new GeneralSecurityException("Received null encryptedData[" + i + "]");
							if (encryptedData[i].length < 15)
								throw new GeneralSecurityException("Invalid encryptedData[" + i + "] size: " + encryptedData[i].length);

							byte[] responseData = encryptedData[i];
							byte responseCommand = responseData[11];
							byte attribution = responseData[14];
							if ((attribution & 0xC0) != 0xC0)
								throw new GeneralSecurityException("Reader encryption disabled, received attribution byte value '" + StringUtils.toHex(attribution) + "'");

							int tlvOffset = 15;
							//2-byte TLV data length minus attribution byte
							int tlvLength = ((encryptedData[i][12] & 0xFF) << 8) | ((encryptedData[i][13] & 0xFF) << 0) - 1;
							byte[] tlvData = new byte[tlvLength];
							System.arraycopy(encryptedData[i], tlvOffset, tlvData, 0, tlvLength);
							VendXEnhancedEncryptedMSRData encryptedMsrData = null;
							Map<byte[], byte []> tlvMap = TLV_PARSER.parse(encryptedData[i], tlvOffset, tlvLength);
							byte [] msrTlv = tlvMap.get(TLVTag.IDTECH_MSR_TLV.getValue());
							if (msrTlv == null) 
								keySerialNum = tlvMap.get(TLVTag.IDTECH_VEND_KSN.getValue());
							else {
								encryptedMsrData = VendXParsing.parseIdtechEnhancedEncryptedMSR(msrTlv);
								keySerialNum = encryptedMsrData.getKsn();
							}

							Cipher cipher = null;
							if (keySerialNum != null) {
								if ((attribution & 2) == 2) {
									algorithm = "AES";
									cipher = initAesCipher(cardReaderType, bdkIndex, keySerialNum, algorithm);
								} else {
									algorithm = "DESede";
									cipher = initDesCipher(cardReaderType, bdkIndex, keySerialNum, algorithm);
								}
							}

							byte[] track2;
							Map<String, byte[]> emvTlv = null;
							if (msrTlv == null) {
								emvTlv = new HashMap<String, byte[]>();
								ByteBuffer byteBuffer = ByteBuffer.allocate(tlvLength);
								EMVIdtechDecryptValueHandler valueHandler = new EMVIdtechDecryptValueHandler(byteBuffer, cipher, emvTlv, acctPattern, charset);
								TLV_PARSER.parse(responseData, tlvOffset, tlvLength, valueHandler);

								int position = byteBuffer.position();
								byte []decryptResult = new byte [position];
								byteBuffer.position(0);
								byteBuffer.get(decryptResult, 0, position);

								track2 = valueHandler.getTrackTwoData();
							} else {
								byte [] track2Decrypted = cipher.doFinal(encryptedMsrData.getTrack2());
								track2 = new byte [encryptedMsrData.getTrack2Length()];
								System.arraycopy(track2Decrypted, 0, track2, 0, encryptedMsrData.getTrack2Length());
							}

							if (entryType == EntryType.DYNAMIC) {
								if ((attribution & 0x18) == 0x08)
									entryType = EntryType.SWIPE;
								else {
									switch (attribution & 0x19) {
										case 0x11:
											entryType = EntryType.CONTACTLESS;
											break;
										case 0x01:
											entryType = EntryType.EMV_CONTACTLESS;
											break;
										case 0x00:
											entryType = EntryType.EMV_CONTACT;
											break;
									}
									if (emvTlv != null) {
										String aid = StringUtils.toHex(emvTlv.get(TLVTag.APPLICATION_IDENTIFIER.getHexValue()));
										if (entryType == EntryType.EMV_CONTACTLESS && aid != null && TLVTag.INTERAC_AIDS.contains(aid))											
											entryType = EntryType.INTERAC_FLASH;
										else {
											byte[] value = emvTlv.get(TLVTag.IDTECH_MOBILE_WALLET_INDICATOR.getHexValue());
											if (value != null && value.length > 0) {
												if (value[0] == 0x01)
													entryType = EntryType.APPLE_PAY;
												else if (value[0] == 0x02)
													entryType = EntryType.GOOGLE_PAY;
											}
										}
									}
								}
								
								ProcessedVasData processedVasData = processAvasData(task, step, index, emvTlv, entryType);
								entryType = processedVasData.getEntryType();
								if (StringUtils.isBlank(processedVasData.getAccountData())) {
									if (responseCommand != 0x00 && responseCommand != 0x23)
										throw new GeneralSecurityException("Received invalid reader response command: " + StringUtils.toHex(responseCommand));
									decrypted[i * 2] = track2;
								} else
									decrypted[i * 2] = processedVasData.getAccountData().getBytes();
								decrypted[i * 2 + 1] = tlvData;

								if (!cardReaderType.isEmvCertified() || !emvEnabled) {
									//fall back to MSD processing until reader is EMV certified and EMV processing is enabled
									switch (entryType) {
										case EMV_CONTACTLESS:
											entryType = EntryType.CONTACTLESS;
											break;
										case EMV_CONTACT:
											entryType = EntryType.SWIPE;
											break;
									}
								}
								step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, index, entryType);
							}
						} catch (IOException e) {
							throw new KEKException("Unable to parse EMV data", e);
						}

						finished = true;
						break;
					case IDTECH_VENDX_ENCRYPTED_EMV:
						/*
						 * See NEO v1.0 IDG_REV 73.pdf page 141 Success Transaction-Encrypted data field format for Contactless card 
						 * @param responseData
						 */
						try {
							byte [] ksn = new byte [10];
							int ksnOffset = VendXParsing.ATTRIBUTION_LENGTH + VendXParsing.KSN_TLV_TAG_LENGTH;
							System.arraycopy(encryptedData[i], ksnOffset, ksn, 0, VendXParsing.KSN_LENGTH);
							keySerialNum = ksn;
							Cipher cipher = initAesCipher(cardReaderType, bdkIndex, keySerialNum, algorithm);
							byte [] responseData = encryptedData[i];
							int tlvOffset = 1;
							int tlvLength = responseData.length - tlvOffset;
//							byte attribution = responseData[0];
							
							ByteBuffer byteBuffer = ByteBuffer.allocate(tlvLength);
							Map<String, byte[]> values = new HashMap<String, byte[]>();
							EMVIdtechDecryptValueHandler valueHandler = new EMVIdtechDecryptValueHandler(byteBuffer, cipher, values, acctPattern, charset);
							
							TLV_PARSER.parse(responseData, tlvOffset, tlvLength, valueHandler);
							
							// Good touch point for diagnostics of tlv tags - jms - 1/7/16
//							Map<byte [], byte []>tlvMap = TLV_PARSER.parse(responseData, tlvOffset, tlvDataLength);
//							TLVTag.dumpTlvParseMap(tlvMap);
							
							int position = byteBuffer.position();
							byte []decryptResult = new byte [position];
							byteBuffer.position(0);
							byteBuffer.get(decryptResult, 0, position);
							
							decrypted[i * 2 + 1] = extractChipCardData(decryptResult);
							
							byte [] track2 = valueHandler.getTrackTwoData();
							if (track2 != null && track2.length > 0) {
								decrypted[i * 2] = track2;
							} else {
								throw new IOException("Unable to find track 2 data");
							}
						} catch (IOException e) {
							throw new KEKException("Unable to parse EMV data", e);
						}
						break;
					case IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR:
						try {
							byte [] responseData = encryptedData[i];
							int tlvOffset = 1;
							int tlvLength = responseData.length - tlvOffset;
//							byte attribution = responseData[0];
							
							Map<byte[], byte []> tlvMap = TLV_PARSER.parse(encryptedData[i], tlvOffset, tlvLength);
							byte [] msrTlv = tlvMap.get(TLVTag.IDTECH_MSR_TLV.getValue());
							VendXEnhancedEncryptedMSRData encryptedMsrData = VendXParsing.parseIdtechEnhancedEncryptedMSR(msrTlv);
							Cipher cipher = initAesCipher(cardReaderType, bdkIndex, encryptedMsrData.getKsn(), algorithm);
							
							byte [] track2Decrypted = cipher.doFinal(encryptedMsrData.getTrack2());
							byte [] track2NoPadding = new byte [encryptedMsrData.getTrack2Length()];
							System.arraycopy(track2Decrypted, 0, track2NoPadding, 0, encryptedMsrData.getTrack2Length());

							decrypted[i * 2] = track2NoPadding;
							decrypted[i * 2 + 1] = extractChipCardData(encryptedData[i]);
						} catch (IOException e) {
							throw new KEKException("Unable to parse VendX Enhanced Encrypted MSR data", e);
						}
						break;
					case IDTECH_VEND3AR:
						try {
							Vend3ARTransactionResponseData data = Vend3ARParsing.parseAnyARResponseData(encryptedData[i]);
							byte [] track2 = data.getTrackTwoData();
							track2 = ByteArrayUtils.fromBCD(track2);
							decrypted[i * 2] = track2;						
							decrypted[i * 2 + 1] = extractChipCardData(data.getTlvData());
						} catch (IOException e) {
							throw new KEKException("Unable to parse Vend3AR data", e);
						}
						break;
					case OTI_EMV:
						try {
							Map<byte [], byte []>tlvMap = TLV_PARSER.parse(encryptedData[i], 0, encryptedData[i].length);
							byte [] otiResultData = tlvMap.get(TLVTag.OTI_RESULT_DATA_TEMPLATE.getValue());
							Map<byte [], byte []>resultDataMap = TLV_PARSER.parse(otiResultData, 0, otiResultData.length);
							
							byte ksn [] = resultDataMap.get(TLVTag.OTI_KEY_SERIAL_NUMBER.getValue());
							byte[] unknown2Container = resultDataMap.get(TLVTag.OTI_CONTAINER2.getValue());
							Map<byte [], byte []> containerMap = TLV_PARSER.parse(unknown2Container, 0, unknown2Container.length);
							byte [] dataCryptogramKsn = containerMap.get(TLVTag.OTI_KEY_SERIAL_NUMBER.getValue());
							if (dataCryptogramKsn != null && dataCryptogramKsn.length > 0) {
								ksn = dataCryptogramKsn;
							}
							Cipher cipher = initDesCipher(cardReaderType, bdkIndex, ksn, algorithm);
							byte [] dataCryptogram = containerMap.get(TLVTag.OTI_DATA_CRYPTOGRAM.getValue());
							byte [] decryptedCryptogram = cipher.doFinal(dataCryptogram);
							String dataCryptogramHex = StringUtils.toHex(dataCryptogram);
							log.info("dataCryptogram as hex: " + dataCryptogramHex);
							String decryptedCryptogramHex = StringUtils.toHex(decryptedCryptogram);
							log.info("dataDecryptogram as hex: " + decryptedCryptogramHex);
							
							int lengthStart = 3;
							if (decryptedCryptogram.length >= lengthStart && decryptedCryptogram[0] == TLVTag.OTI_DECRYPTED_CONTAINER.getValue()[0]) {
//								int otiTlvLen = parseTlvLengthForTag(decryptedCryptogram, 1);
								int otiTlvLen = TLVTag.parseTlvLength(decryptedCryptogram, 1);
								byte [] otiTlvData = new byte [otiTlvLen];
								System.arraycopy(decryptedCryptogram, lengthStart, otiTlvData, 0, otiTlvLen);
								Map<byte [], byte []>cryptogramMap = TLV_PARSER.parse(otiTlvData, 0, otiTlvData.length);
								TLVTag.dumpTlvParseMap(cryptogramMap);
								
								byte [] resultTlv = otiTlvData;
								byte [] aid = cryptogramMap.get(TLVTag.APPLICATION_IDENTIFIER.getValue());
								if (aid == null || aid.length == 0) {
									// in the event the 9F06 tagged AID is not configured
									byte [] appIdTag = TLVTag.APPLICATION_IDENTIFIER.getValue();
									byte [] otiAidValue = cryptogramMap.get(TLVTag.OTI_AID.getValue());
									if (otiAidValue != null && otiAidValue.length > 1) {
										int aidLengthLength = 1;
										int resultTlvLength = appIdTag.length + otiAidValue.length + aidLengthLength + otiTlvLen;
										resultTlv = new byte [resultTlvLength];
										System.arraycopy(appIdTag, 0, resultTlv, 0, appIdTag.length);
										resultTlv[appIdTag.length] = (byte)otiAidValue.length;
										System.arraycopy(otiAidValue, 0, resultTlv, appIdTag.length + aidLengthLength, otiAidValue.length);
										System.arraycopy(otiTlvData, 0, resultTlv, appIdTag.length + aidLengthLength + otiAidValue.length, otiTlvData.length);
									} else {
										throw new GeneralSecurityException("AID not available for OTI reader data.");
									}
								}
								
								byte [] track2 = cryptogramMap.get(TLVTag.TRACK2_EQUIVALENT_DATA.getValue());
								track2 = ByteArrayUtils.fromBCD(track2);
								decrypted[i * 2] = track2; 
								decrypted[i * 2 + 1] = extractChipCardData(resultTlv);
							}
						} catch (Exception e) {
							throw new KEKException("Unable to parse OTI_EMV data", e);
						}
						break;
					case OTI_BEZEL: // a whole TLV blob has been passed to us, interpret it
						int offset = 0;
						int limit = encryptedData[i].length;
						if(limit > 4 && encryptedData[i][0] == (byte) 0xFC) {
							offset++;
							int len;
							if((encryptedData[i][offset] & 0xFF) < 0x80)
								len = (encryptedData[i][offset++] & 0xFF);
							else {
								int lengthBytes = (encryptedData[i][offset++] & 0x0F);
								len = 0;
								for(int l = 0; l < lengthBytes; l++)
									len = len * 256 + (encryptedData[i][offset++] & 0xFF);
							}
							limit = offset + len;
							log.info("Encrypted data starts with 0xFC and has length " + len);
						} else
							log.info("Encrypted data does not start with 0xFC");
						if(limit > offset && encryptedData[i][offset] == (byte) 0xE9) {
							offset++;
							int len;
							if((encryptedData[i][offset] & 0xFF) < 0x80)
								len = (encryptedData[i][offset++] & 0xFF);
							else {
								int lengthBytes = (encryptedData[i][offset++] & 0x0F);
								len = 0;
								for(int l = 0; l < lengthBytes; l++)
									len = len * 256 + (encryptedData[i][offset++] & 0xFF);

							}
							log.info("Encrypted data has 0xE9 with length " + len);
							Map<byte[], byte[]> values;
							try {
								values = TLV_PARSER.parse(encryptedData[i], offset, len);
							} catch(IOException e) {
								log.error("Could not pre-process OTI TLV blob", e);
								throw new GeneralSecurityException("Invalid TLV Data", e);
							}
							byte[] cryptogram = values.get(TLVTag.OTI_DATA_CRYPTOGRAM.getValue());
							if(cryptogram != null)
								encryptedData[i] = cryptogram;
							else
								log.warn("Cryptogram not found in TLV blob");
							byte[] keyVariant = values.get(TLVTag.OTI_KEY_VARIANT.getValue());
							if(keyVariant != null) {
								if(keyVariant.length == 1) {
									switch(keyVariant[0]) {
										case 0x01:
											keyVariantTypeId = KeyVariantType.PIN.getValue();
											log.info("Tag 0x" + StringUtils.toHex(TLVTag.OTI_KEY_VARIANT.getValue()) + " = " + keyVariant[0] + "; Using KeyVariantType " + KeyVariantType.PIN);
											break;
										case 0x04:
										case 0x05:
											keyVariantTypeId = KeyVariantType.DATA.getValue();
											log.info("Tag 0x" + StringUtils.toHex(TLVTag.OTI_KEY_VARIANT.getValue()) + " = " + keyVariant[0] + "; Using KeyVariantType " + KeyVariantType.DATA);
											break;
										default:
											log.warn("Unexpected Key Variant Type (" + keyVariant[0] + ")");
									}
								} else
									log.warn("Key Variant length is wrong (" + keyVariant.length + ")");
							}
							descriptor = values.get(TLVTag.OTI_DESCRIPTOR.getValue());
							byte[] ksn = values.get(TLVTag.OTI_KEY_SERIAL_NUMBER.getValue());
							if(ksn != null && ksn.length > 0)
								keySerialNum = ksn;
						} else
							log.info("Encrypted data does not have 0xE9 at byte " + (offset + 1) + " - decrypting the entire blob");
						break;
				}

				if (!finished) {
					if(!StringUtils.isBlank(cardReaderType.getCipherName())) {
						if(keySerialNum == null || keySerialNum.length == 0) {
							log.warn("SERVICE_FAIL: DUKPTDecrypt() : " + "KeySerialNum not provided");
							throw new InvalidValueException("KeySerialNum was not provided");
						}
						Cipher cipher;
						if ("AES".equalsIgnoreCase(algorithm))
							cipher = initAesCipher(cardReaderType, bdkIndex, keySerialNum, algorithm);
						else
							cipher = initDesCipher(cardReaderType, bdkIndex, keySerialNum, algorithm);
						decrypted[i * 2] = cipher.doFinal(encryptedData[i]);
						byte[] track2 = null;
						switch (cardReaderType) {
							case IDTECH_VENDX_PARSED_AES:
							case IDTECH_VENDX_PARSED_3DES:
								if (entryType == EntryType.CONTACTLESS && decrypted[i * 2] != null && decrypted[i * 2].length > 4
										&& decrypted[i * 2][0] == (byte) 0xFF && decrypted[i * 2][1] == (byte) 0xEE) {
									//IDTech Vendi decrypted contactless MSD data starts with tag FFEE14 (track 2) / FFEE13 (track 1) and length in byte #4 
									int length = decrypted[i * 2][3] & 0xFF;
									if (length > 0 && length <= decrypted[i * 2].length - 4) {
										byte[] decryptedData = new byte[length];
										System.arraycopy(decrypted[i * 2], 4, decryptedData, 0, length);
										decrypted[i * 2] = decryptedData;
									}
								}
								break;
							case INGENICO_BEZEL:
							case UIC_BEZEL:
								track2 = extractTrack2(decrypted[0]);
								if (track2 != null) {
									decrypted[0] = track2;
								} else {
									log.warn("Could not find track2");
								}
								break;
						}
					} else {
						decrypted[i * 2] = encryptedData[i];
					}
	
					switch (cardReaderType) {
						case OTI_BEZEL_PARSED_BCD:
							decrypted[i * 2] = ByteArrayUtils.fromBCD(decrypted[i * 2]);
							break;
						case OTI_BEZEL: // a whole TLV blob has been passed to us, interpret it
							if(descriptor != null) {
								if(descriptor.length > 0 && descriptor[0] == (byte) 0x00) { // a whole TLV
									if(decrypted[i * 2].length >= 3 && decrypted[i * 2][0] == (byte) 0xEA) {
										int lengthOffset = 1;
										int tlvLen = 0;
										if((decrypted[i * 2][lengthOffset] & 0xFF) < 0x80)
											tlvLen = (decrypted[i * 2][lengthOffset++] & 0xFF);
										else {
											int lengthBytes = (decrypted[i * 2][lengthOffset++] & 0x0F);
											tlvLen = 0;
											for(int l = 0; l < lengthBytes; l++)
												tlvLen = tlvLen * 256 + (decrypted[i * 2][lengthOffset++] & 0xFF);
	
										}
										log.info("Decrypted data has 0xEA with length " + tlvLen);
										Map<byte[], byte[]> values;
										try {
											values = TLV_PARSER.parse(decrypted[i * 2], lengthOffset, tlvLen);
										} catch(IOException e) {
											log.error("Could not post-process OTI Decrypted TLV blob", e);
											throw new GeneralSecurityException("Invalid Decrypted TLV Data", e);
										}
										byte[] val = values.get(TLVTag.OTI_TRACK2.getValue());
										if(val != null) {
											decrypted[i * 2] = ByteArrayUtils.fromBCD(val);
											log.info("Found value for tag 0x" + StringUtils.toHex(TLVTag.OTI_TRACK2.getValue()) + " - using that as the decrypted data");
										} else {
											log.warn("Track 2 tag not found; checking Track 1");
											val = values.get(TLVTag.OTI_TRACK1.getValue());
											if(val != null) {
												decrypted[i * 2] = val;
												log.info("Found value for tag 0x" + StringUtils.toHex(TLVTag.OTI_TRACK1.getValue()) + " - using that as the decrypted data");
											} else
												log.warn("Track 1 tag not found; using entire decrypted blob");
										}
									} else
										log.info("Decrypted data does not start with 0xEA - using entire decrypted blob");
								} else {
									// one value of TLV
									// We don't really care what tag it is, just leave it as the decrypted data
									log.info("Descriptor is for tag '" + StringUtils.toHex(descriptor) + "' - using entire decrypted blob");
								}
							} else
								log.info("No Descriptor tag provided - using entire decrypted blob");
							break;						
						case ACS_ACR31:							
							// read first byte as length of data, adjust decrypted
							int len = decrypted[i * 2][0] & 0xFF;
							if(len >= decrypted[i * 2].length)
								throw new GeneralSecurityException("First byte indicates length should be " + len + " but there are not enough decrypted bytes (" + decrypted[i * 2].length + ") for that");
							byte[] tmp = new byte[len];
							System.arraycopy(decrypted[i * 2], 1, tmp, 0, len);
							decrypted[i * 2] = tmp;
							break;
					}
				}

				if (decrypted[i * 2] == null)
					throw new GeneralSecurityException("Empty account data");

				if(log.isInfoEnabled())
					log.info("SERVICE_SUCCESS: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + bdkIndex + ", ksn: " + ksnHex + ", " + encryptedData[i].length + " bytes) : " + (System.currentTimeMillis() - start) + " ms");

			} catch(GeneralSecurityException e) {
				log.error("SERVICE_FAIL: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + bdkIndex + ", ksn: " + ksnHex + ", " + encryptedData[i].length + " bytes) : " + (System.currentTimeMillis() - start) + " ms" + " : " + e.getMessage());
				throw e;
			} catch(KEKException e) {
				log.error("SERVICE_FAIL: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + bdkIndex + ", ksn: " + ksnHex + ", " + encryptedData[i].length + " bytes) : " + (System.currentTimeMillis() - start) + " ms" + " : " + e.getMessage());
				throw new GeneralSecurityException(e);
			}
		}
		return decrypted;
	}
	
	protected ProcessedVasData processAvasData(AbstractAccountTask task, MessageChainStep step, int index, Map<String, byte[]> emvTlv, EntryType entryType) {
		ProcessedVasData processedVasData = new ProcessedVasData(entryType, null);
		
		if (emvTlv == null)
			return processedVasData;
		
		byte[] avasMerchantId = emvTlv.get(TLVTag.AVAS_MERCHANT_ID.getHexValue());
		byte[] avasData = emvTlv.get(TLVTag.AVAS_VAS_DATA.getHexValue());
				
		if (avasMerchantId != null && avasData != null) {
			processedVasData.setEntryType(EntryType.VAS);
			try {
				VasData vasData = KeyManagerServiceUtil.decryptVas(this, avasData, avasMerchantId);
				if (!vasData.isDecrypted()) {
					processedVasData.setAccountData(StringUtils.toHex(vasData.getEncryptedData()));
					return processedVasData;
				}
				step.setOptionallyIndexedResultAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, index, vasData.getPassIdentifier());
				if (vasData.getCardId() > 0) {
					step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, index, vasData.getCardId());
					String accountData = KeyManagerServiceUtil.getAccountByToken(this, step, index, vasData.getCardId(), task.encrypt(vasData.getToken()), processedVasData.getEntryType());
					processedVasData.setAccountData(accountData);
				}
			} catch(GeneralSecurityException | RetrySpecifiedServiceException | InvalidCipherException | KEKException e) {
				log.error("Could not encrypt data for token authorization", e);
			}
		}
		
		return processedVasData;
	}
	
	/**
	 * Extracts track2 for INGENICO.
	 * @param decrypted Input raw data.
	 * @return Track2.
	 */
	private byte[] extractTrack2(final byte[] decrypted) {
		int track2Begin = ByteArrayUtils.indexOf(decrypted, new byte[] {INGENICO_TRACK2_BEGIN_DELIMITER});
		if (track2Begin < 0) {
			return null;
		}
		int track2End = ByteArrayUtils.indexOf(decrypted, track2Begin, decrypted.length - track2Begin,
				new byte[] {INGENICO_TRACK2_END_DELIMITER});
		if (track2End < 0) {
			return null;
		}

		int track2Length = track2End - track2Begin - 1;
		byte[] ret = new byte[track2Length];
		System.arraycopy(decrypted, track2Begin + 1, ret, 0, track2Length);
		return ret;
	}

	// it was changing something that AuthorizeTask needed.  revisit at some point - 2/19/16
	protected byte [] extractChipCardData(byte [] tlvData) throws IOException {
//		Map<byte[], byte[]> tlvMap = TLV_PARSER.parse(tlvData, 0, tlvData.length);
//		String chipCardData = VendXParsing.extractChaseChipCardData(tlvMap);
//		log.info("chipCardData: " + chipCardData);
//		byte [] result = chipCardData.getBytes();
//		return result;
		return tlvData;
	}

	protected Cipher initDesCipher(CardReaderType cardReaderType, Long bdkIndex, byte [] ksn, String algorithm) throws KEKException, InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		byte[] key1 = new byte[8];
		byte[] key2 = new byte[8];
		kekStore.getDUKPTKey(cardReaderType.getKeyVariantType().getValue(), bdkIndex, ksn, key1, key2);

		byte[] tdesKey = new byte[24];
		System.arraycopy(key1, 0, tdesKey, 0, 8);
		System.arraycopy(key2, 0, tdesKey, 8, 8);
		System.arraycopy(key1, 0, tdesKey, 16, 8);

		DESedeKeySpec keySpec = new DESedeKeySpec(tdesKey);
		SecretKey secretKey = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
		String cipherName = cardReaderType.getCipherName();
		if (cipherName.startsWith("DYNAMIC"))
			cipherName = cipherName.replace("DYNAMIC", algorithm);
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(Cipher.DECRYPT_MODE, secretKey, nullIvParamSpec);
		return cipher;
	}
	
	protected Cipher initAesCipher(CardReaderType cardReaderType, Long bdkIndex, byte [] ksn, String algorithm) 
			throws KEKException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		byte keyVariantTypeId = cardReaderType.getKeyVariantType().getValue();
		byte[] key1 = new byte[8];
		byte[] key2 = new byte[8];
		kekStore.getDUKPTKey(keyVariantTypeId, bdkIndex, ksn, key1, key2);

		byte [] aesKey = new byte[16];
		System.arraycopy(key1, 0, aesKey, 0, 8);
		System.arraycopy(key2, 0, aesKey, 8, 8);
		
		SecretKeySpec secretKey = new SecretKeySpec(aesKey, algorithm);
		String cipherName = cardReaderType.getCipherName();
		if (cipherName.startsWith("DYNAMIC"))
			cipherName = cipherName.replace("DYNAMIC", algorithm);
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(Cipher.DECRYPT_MODE, secretKey, nullIvParamSpec16);
		
		return cipher;
	}
	
	public byte[] encrypt(long keyId, byte[] unencryptedData) throws GeneralSecurityException {
		PersistentKey key = persistentKeyCache.getOrCreate((int) keyId);
		return encrypt(key, unencryptedData);
	}

	// NOTE: This was copied from src-webapp/com/usatech/keymanager/KeyManagerService.java, then adapted
	public byte[] encrypt(PersistentKey key, byte[] unencryptedData) throws GeneralSecurityException {
		long start;
		if(log.isInfoEnabled()) {
			start = System.currentTimeMillis();
		} else {
			start = 0;
		}

		if(log.isDebugEnabled())
			log.debug("SERVICE_START: encrypt(" + key.getKid() + "," + unencryptedData.length + " bytes)");

		Provider provider = Security.getProvider(key.getProviderName());
		if(provider == null)
			throw new GeneralSecurityException("Provider not found: " + key.getProviderName());

		int blockSize = key.getIvSizeBytes();
		boolean useCBC = unencryptedData.length > blockSize;
		String transformation = key.getTransformation();
		if(!useCBC)
			transformation = transformation.replace("/CBC/", "/ECB/");

		Cipher cipher = Cipher.getInstance(transformation, provider);
		if(cipher == null)
			throw new GeneralSecurityException("Cipher not found: " + transformation);
		cipher.init(Cipher.ENCRYPT_MODE, key.getSecretKey());

		byte[] iv = null;
		if(useCBC) {
			iv = cipher.getIV();
			if(iv.length != blockSize)
				throw new GeneralSecurityException("Initialization vector size " + iv.length + " != " + blockSize);
		}

		byte[] encrypted = null;
		int remainder = unencryptedData.length % blockSize;
		if(remainder > 0) {
			byte[] paddedBytes = new byte[unencryptedData.length + blockSize - remainder];
			System.arraycopy(unencryptedData, 0, paddedBytes, 0, unencryptedData.length);
			for(int i = unencryptedData.length; i < paddedBytes.length; i++)
				paddedBytes[i] = PADDING;
			encrypted = cipher.doFinal(paddedBytes);
		} else
			encrypted = cipher.doFinal(unencryptedData);

		byte[] combined = null;
		if(useCBC) {
			combined = new byte[encrypted.length + iv.length];
			System.arraycopy(encrypted, 0, combined, 0, encrypted.length);
			System.arraycopy(iv, 0, combined, encrypted.length, iv.length);
		}

		if(log.isInfoEnabled())
			log.info("SERVICE_SUCCESS: encrypt(" + key.getKid() + ", " + unencryptedData.length + " bytes): " + (System.currentTimeMillis() - start) + " ms");
		return useCBC ? combined : encrypted;
	}

	// NOTE: This was copied from src-webapp/com/usatech/keymanager/KeyManagerService.java, then adapted
	public PersistentKey generateKey(long expirationTime) throws GeneralSecurityException {
		if(log.isDebugEnabled())
			log.debug("SERVICE_START: generateKey()");

		if(kekStore == null) {
			log.warn("SERVICE_FAIL: generateKey() : " + "KeyStore is not loaded");
			throw new GeneralSecurityException("KeyStore is not loaded");
		}
		SecretKeyInfo secretKeyInfo = keyGeneratorFactory.generateKey();
		if(secretKeyInfo.secretKey == null) {
			log.error("SERVICE_FAIL: generateKey() - KeyGenerator returned null key");
			throw new GeneralSecurityException("generateKey failed due to an internal error!");
		}

		byte[] encryptedKey;
		try {
			encryptedKey = kekStore.wrap(secretKeyInfo.secretKey.getEncoded());
		} catch(CryptoException e) {
			throw new GeneralSecurityException(e);
		}
		String encryptedKeyBase64 = Base64.encodeBytes(encryptedKey, true);

		PersistentKey persistentKey = PersistentKey.newInstance();
		persistentKey.setTransformation(secretKeyInfo.transformation);
		persistentKey.setProviderName(secretKeyInfo.providerName);
		persistentKey.setKeySizeBits(secretKeyInfo.keySize);
		persistentKey.setIvSizeBytes(secretKeyInfo.ivSize);
		persistentKey.setCipherTextBase64(encryptedKeyBase64);
		persistentKey.setKekAlias(kekStore.getCurrentKEKAlias());
		persistentKey.setCreatedDate(new java.util.Date());
		persistentKey.setCreatedByUser(kekStore.getLoadingUser());
		persistentKey.setSecretKey(secretKeyInfo.secretKey);
		persistentKey.setExpirationDate(new java.util.Date(expirationTime));
		persistentKey.save();

		if(log.isInfoEnabled())
			log.info("SERVICE_SUCCESS: generateKey()");
		return persistentKey;
	}

	public PanAndExpDate retrievePanAndExpDate(long encryptedId, String username) throws NotEnoughRowsException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
		String[] strings = retrieveStrings(encryptedId, ProcessingConstants.US_ASCII_CHARSET, username, null);
		return new PanAndExpDate(strings.length > 0 ? strings[0] : null, strings.length > 1 ? strings[1] : null);
	}

	public String[] retrieveStrings(long encryptedId, Charset charset, String username, AtomicLong expirationTimeHolder) throws NotEnoughRowsException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
		byte[][] bytes = retrieveBytes(encryptedId, username, expirationTimeHolder);
		String[] strings = new String[bytes.length];
		for(int i = 0; i < bytes.length; i++)
			strings[i] = (bytes[i] == null ? null : new String(bytes[i], charset));
		return strings;
	}

	public byte[][] retrieveBytes(long encryptedId, String username, AtomicLong expirationTimeHolder) throws NotEnoughRowsException, SQLException, DataLayerException, ConvertException, GeneralSecurityException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("encryptedId", encryptedId);
		DataLayerMgr.executeCall("RETRIEVE_ENCRYPTED", params, true);
		Long keyId = ConvertUtils.convert(Long.class, params.get("keyId"));
		if(keyId == null)
			throw new NotEnoughRowsException("Data for id '" + encryptedId + "' does not exist");
		String storedUsername = ConvertUtils.convert(String.class, params.get("username"));
		if(!StringUtils.isBlank(storedUsername) && !storedUsername.equals(username))
			throw new CredentialException("User '" + username + "' not authorized to retrieve this data");
		byte[][] bytes = ConvertUtils.convertRequired(byte[][].class, params.get("encryptedData"));
		for(int i = 0; i < bytes.length; i++)
			bytes[i] = (bytes[i] == null ? null : decrypt(keyId, bytes[i]));
		if(expirationTimeHolder != null)
			expirationTimeHolder.set(ConvertUtils.getLong(params.get("expirationUtcTs")));
		return bytes;
	}

	public long storePanAndExpDate(String pan, String expDate, long expirationTime, String username) throws ConvertException, SQLException, DataLayerException, GeneralSecurityException {
		return storeStrings(StringUtils.isBlank(expDate) ? new String[] { pan } : new String[] { pan, expDate }, ProcessingConstants.US_ASCII_CHARSET, expirationTime, username);
	}

	public long storeStrings(String[] strings, Charset charset, long expirationTime, String username) throws ConvertException, SQLException, DataLayerException, GeneralSecurityException {
		byte[][] bytes = new byte[strings.length][];
		for(int i = 0; i < strings.length; i++)
			bytes[i] = strings[i] == null ? null : strings[i].getBytes(charset);
		return storeBytes(bytes, expirationTime, username);
	}

	public long storeBytes(byte[][] bytes, long expirationTime, String username) throws ConvertException, SQLException, DataLayerException, GeneralSecurityException {
		PersistentKey key = getPersistentKey(expirationTime);
		byte[][] encrypted = new byte[bytes.length][];
		for(int i = 0; i < bytes.length; i++)
			encrypted[i] = bytes[i] == null ? null : encrypt(key, bytes[i]);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("encryptedData", encrypted);
		params.put("expirationUtcTs", expirationTime);
		params.put("keyId", key.getKid());
		params.put("username", username);
		DataLayerMgr.executeCall("STORE_ENCRYPTED", params, true);
		long encryptedId = ConvertUtils.getLong(params.get("encryptedId"));
		return encryptedId;
	}

	public PersistentKey getPersistentKey(long dataExpirationTime) throws GeneralSecurityException {
		Map.Entry<Long, PersistentKey> entry = persistentKeyInfoMap.ceilingEntry(dataExpirationTime);
		long currentTime = System.currentTimeMillis();
		PersistentKey persistentKey;
		if(entry == null) { // Create new one
			persistentKey = addPersistentKey(currentTime, dataExpirationTime);
		} else if(entry.getValue().getEncryptCount() >= getMaxEncryptionsPerKey()) { // Remove old and create new
			persistentKeyInfoMap.remove(entry.getKey(), entry.getValue());
			persistentKey = addPersistentKey(currentTime, dataExpirationTime);
		} else if(entry.getValue().getExpirationDate() == null) { // Use existing one
			persistentKey = entry.getValue();
		} else if(entry.getValue().getExpirationDate().getTime() < currentTime) {// Remove old and create new
			persistentKeyInfoMap.remove(entry.getKey(), entry.getValue());
			persistentKey = addPersistentKey(currentTime, dataExpirationTime);
		} else if(entry.getValue().getExpirationDate().getTime() > getMaxExpirationGap() + dataExpirationTime) { // Create new one
			persistentKey = addPersistentKey(currentTime, dataExpirationTime);
		} else { // Use existing one
			persistentKey = entry.getValue();
		}
		persistentKey.incrementEncryptCount();
		return persistentKey;
	}

	protected PersistentKey addPersistentKey(long currentTime, long dataExpirationTime) throws GeneralSecurityException {
		long newKeyExpirationTime = Math.max(dataExpirationTime, currentTime) + getMaxAgeForKey();
		PersistentKey persistentKey = generateKey(newKeyExpirationTime);
		persistentKeyInfoMap.put(newKeyExpirationTime, persistentKey);
		return persistentKey;
	}

	public KeyGeneratorFactory getKeyGeneratorFactory() {
		return keyGeneratorFactory;
	}

	public KEKStore getKekStore() {
		return kekStore;
	}

	public void setKekStore(KEKStore kekStore) {
		this.kekStore = kekStore;
		persistentKeyCache.setKekStore(kekStore);
	}

	public PersistentKeyCache getPersistentKeyCache() {
		return persistentKeyCache;
	}

	public int getMaxEncryptionsPerKey() {
		return maxEncryptionsPerKey;
	}

	public void setMaxEncryptionsPerKey(int maxEncryptionsPerKey) {
		if(maxEncryptionsPerKey <= 0)
			throw new IllegalArgumentException("Value must be greater than zero");
		this.maxEncryptionsPerKey = maxEncryptionsPerKey;
	}

	public long getMaxAgeForKey() {
		return maxAgeForKey;
	}

	public void setMaxAgeForKey(long maxAgeForKey) {
		if(maxAgeForKey <= 0)
			throw new IllegalArgumentException("Value must be greater than zero");
		this.maxAgeForKey = maxAgeForKey;
	}

	public long getMaxExpirationGap() {
		return maxExpirationGap;
	}

	public void setMaxExpirationGap(long maxExpirationGap) {
		if(maxExpirationGap <= 0)
			throw new IllegalArgumentException("Value must be greater than zero");
		this.maxExpirationGap = maxExpirationGap;
	}

	public boolean isEmvEnabled() {
		return emvEnabled;
	}

	public void setEmvEnabled(boolean emvEnabled) {
		this.emvEnabled = emvEnabled;
	}

	public static void setCayenneConfigurationFile(String cayenneXmlFile) {
		Configuration.initializeSharedConfiguration(new DefaultConfiguration(cayenneXmlFile));
	}

	public long getBdkIndex(int cardReaderIndex) throws InvalidByteValueException {
		Long val = bdkIndexMap.get(CardReaderType.getByValue((byte) cardReaderIndex));
		return val == null ? 0 : val;
	}

	public void setBdkIndex(int cardReaderIndex, long bdkIndex) throws InvalidByteValueException {
		bdkIndexMap.put(CardReaderType.getByValue((byte) cardReaderIndex), bdkIndex);
	}
	
	public PrivateKey getAVASPrivateKey(String publicKeyID) throws KEKException {
		return kekStore.getAVASPrivateKey(publicKeyID);
	}
}
