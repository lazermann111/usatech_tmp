package com.usatech.keymanager.service;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.crypto.SecretKey;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import simple.app.Prerequisite;
import simple.app.ResetAvailabilityListener;
import simple.app.ResetAvailabilityTrigger;
import simple.io.AsyncProcessLogger;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.security.SecurityUtils;
import simple.text.StringUtils;

import com.usatech.keymanager.master.AbstractKeyManagerLoader;
import com.usatech.keymanager.master.KEKStore;

public class KeyManagerServiceLoader extends AbstractKeyManagerLoader implements Prerequisite, ResetAvailabilityTrigger {
	private static final Log log = Log.getLog();
	protected Set<KEKStorePatron> kekStorePatrons = new HashSet<KEKStorePatron>();
	protected final Set<ResetAvailabilityListener> listeners = new HashSet<ResetAvailabilityListener>();
	protected File reloaderCommand;
	

	@Override
	public File getFileRealPath(String fileName) {
		return new File(getConfiguration().getFile().getParentFile(), fileName);
	}

	public void setConfiguration(PropertiesConfiguration config) {
		synchronized(synchObject) {
			if(this.context != null)
				throw new IllegalStateException(toString() + " was already initialized; configuration cannot be changed");
			this.config = config;
		}
	}

	@Override
	protected PropertiesConfiguration initConfig() throws ConfigurationException {
		return config;
	}

	@Override
	protected boolean handleLoaded() {
		KEKStore kekStore = getKEKStore();
		if(kekStore != null && kekStorePatrons != null) {
			for(KEKStorePatron patron : kekStorePatrons)
				if(patron != null)
					patron.setKekStore(kekStore);
			for(ResetAvailabilityListener listener : listeners)
				if(listener != null)
					listener.resetAvailability(this);
		}
		return true;
	}

	public static char[] getReloaderPassword() {
		String password = SecurityUtils.getProperties().getProperty("javax.net.ssl.keyStorePassword");
		if(password == null)
			return null;
		return password.toCharArray();
	}

	public Set<KEKStorePatron> getKekStorePatrons() {
		return kekStorePatrons;
	}

	public void setKekStorePatrons(Set<KEKStorePatron> kekStorePatrons) {
		this.kekStorePatrons = kekStorePatrons;
	}

	@Override
	public void registerListener(ResetAvailabilityListener listener) {
		listeners.add(listener);
	}

	@Override
	public void deregisterListener(ResetAvailabilityListener listener) {
		listeners.remove(listener);
	}

	@Override
	public boolean isAvailable() {
		return getKEKStore() != null;
	}

	public File getReloaderCommand() {
		return reloaderCommand;
	}

	public void setReloaderCommand(File persistentLoaderCommand) throws IOException {
		if(persistentLoaderCommand == null)
			this.reloaderCommand = persistentLoaderCommand;
		else
			this.reloaderCommand = IOUtils.findExecutableFile(persistentLoaderCommand);
	}
	
	@Override
	public void kekStoreUpdated(Map<String, SecretKey> kekMap) {
		if(kekMap != null) {
			File file = getReloaderCommand();
			if(file != null) {
				String cmd = file.getAbsolutePath();
				List<String> command = new ArrayList<String>();
				command.add(cmd);
				RuntimeMXBean rmx = ManagementFactory.getRuntimeMXBean();
				command.add(System.getProperty("java.home"));
				ProcessBuilder pb = new ProcessBuilder(command);
				pb.environment().put("CLASSPATH", rmx.getClassPath());
				pb.environment().put("ARGUMENTS", StringUtils.join(rmx.getInputArguments(), " "));
				Process p;
				try {
					p = pb.start();
					AsyncProcessLogger.logProcessStreams(p, "KeyManagerReloader", AsyncProcessLogger.LogLevel.WARN, AsyncProcessLogger.LogLevel.INFO);
					try {
						String kekProviderName = getConfiguration().getString("kekStore.providerName");
						String kekStoreType = getConfiguration().getString("kekStore.type");
						provider = Security.getProvider(kekProviderName);
						if(provider == null){
							throw new KeyStoreException("KEK Keystore provider not found: " + kekProviderName);
						}
						char[] reloaderPass=getReloaderPassword();
						KeyStore kekStoreWithServerPass = KeyStore.getInstance(kekStoreType, provider);
						kekStoreWithServerPass.load(null, reloaderPass);
						KeyStore.PasswordProtection keyPasswordProtection = new KeyStore.PasswordProtection(reloaderPass);
						for(String alias : kekMap.keySet()) {
							SecretKey secretKey = kekMap.get(alias);
							KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(secretKey);
							kekStoreWithServerPass.setEntry(alias, secretKeyEntry, keyPasswordProtection);
						}
						kekStoreWithServerPass.store(p.getOutputStream(), getReloaderPassword());
					} catch(KeyStoreException e) {
						log.error("Could not send data to reloader command: '" + cmd + "'", e);
					} catch(NoSuchAlgorithmException e) {
						log.error("Could not send data to reloader command: '" + cmd + "'", e);
					} catch(CertificateException e) {
						log.error("Could not send data to reloader command: '" + cmd + "'", e);
					} catch(IOException e) {
						log.error("Could not send data to reloader command: '" + cmd + "'", e);
					}
					p.getOutputStream().close();
				} catch(IOException e) {
					log.error("Could not start persistent loader command: '" + cmd + "'", e);
				}
			}
		}else{
			log.error("kekMap should not be null. Failed to send data to reloader command.");
		}
	}

	@Override
	public String getVersion() {
		return SystemUtils.getApplicationVersionSafely();
	}
}
