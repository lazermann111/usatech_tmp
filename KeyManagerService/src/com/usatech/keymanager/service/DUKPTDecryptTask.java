package com.usatech.keymanager.service;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;

import simple.app.ServiceException;
import simple.bean.MaskedString;
import simple.io.Log;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class DUKPTDecryptTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	protected AccountProcessor lookupAccountTask;
	protected final Cache<String, Pattern, PatternSyntaxException> regexCache = new LockSegmentCache<String, Pattern, PatternSyntaxException>() {
		@Override
		protected Pattern createValue(String key, Object... additionalInfo) throws PatternSyntaxException {
			return Pattern.compile(key);
		}
	};

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			CardReaderType cardReaderType = step.getAttributeDefault(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID, CardReaderType.class, CardReaderType.MAGTEK_MAGNESAFE);
			byte[] keySerialNum = step.getAttribute(CommonAttrEnum.ATTR_KEY_SERIAL_NUM, byte[].class, false);
			EntryType entryType = step.getAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, false);
			int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
			Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
			if(cnt == 0) {
				processIndex(taskInfo, cardReaderType, entryType, keySerialNum, charset, 0);
			} else
				for(int i = 1; i <= cnt; i++)
					processIndex(taskInfo, cardReaderType, entryType, keySerialNum, charset, i);
		} catch(AttributeConversionException e) {
			log.warn("Could not get attributes for DUKPT decryption", e);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INVALID_INPUT);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "SYSTEM_ERROR");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "System operability issue: " + e.getMessage());
		} catch(GeneralSecurityException e) {
			log.warn("Exception performing DUKPT decryption", e);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DUKPT_DECRYPTION_FAILURE);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "DECRYPTION_ERROR");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Decryption Failure: " + e.getMessage());
		} catch(InvalidValueException e) {
			log.warn("Could not process DUKPT decryption", e);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INVALID_INPUT);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "MISSING_INPUT");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, e.getMessage());
		}
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}

	protected void processIndex(MessageChainTaskInfo taskInfo, CardReaderType cardReaderType, EntryType entryType, byte[] keySerialNum, Charset charset, int index) throws AttributeConversionException, GeneralSecurityException, ServiceException, InvalidValueException {
		MessageChainStep step = taskInfo.getStep();
		byte[] encryptedData = step.getOptionallyIndexedAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, index, byte[].class, true);
		if(encryptedData.length > 0) {
			String regex = step.getOptionallyIndexedAttribute(CommonAttrEnum.ATTR_VALIDATION_REGEX, index, String.class, false);
			Pattern pattern = null;
			if (!StringUtils.isBlank(regex))
				pattern = regexCache.getOrCreate(regex);
			byte[][] decryptedData = keyManagerEngine.decryptDUKPT(this, taskInfo, cardReaderType, entryType, keySerialNum, new byte[][] { encryptedData }, index, pattern, charset);
			String acctData;
			Object value;
			Object additional;
			if(charset == null) {
				value = decryptedData[0];
				acctData = new String(decryptedData[0]);
				additional = (decryptedData.length > 1 && decryptedData[1] != null && decryptedData[1].length > 0 ? decryptedData[1] : null);
			} else {
				acctData = charset.decode(ByteBuffer.wrap(decryptedData[0])).toString();
				value = new MaskedString(acctData);
				additional = (decryptedData.length > 1 && decryptedData[1] != null && decryptedData[1].length > 0 ? decryptedData[1] : null);
			}
			Matcher matcher = null;
			if(pattern != null && !(matcher = pattern.matcher(acctData)).matches()) {
				if(log.isDebugEnabled())
					log.debug("Decrypted value '" + StringUtils.preparePrintable(acctData) + "' (item " + index + ") does not match regex '" + regex + "' from encrypted bytes " + StringUtils.toHex(encryptedData[0]));
				else if(log.isInfoEnabled())
					log.info("Decrypted value '" + StringUtils.preparePrintable(MessageResponseUtils.maskTrackData(acctData)) + "' (item " + index + ") does not match regex '" + regex + "'");
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED_PERMANENT);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.UNPARSEABLE_CARD_DATA);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "INVALID_DECRYPTED_CARD_DATA");
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Decrypted Card Data does not match expected format");
				value = null;
			} else {
				if(matcher != null && matcher.groupCount() > 0) {
					StringBuilder sb = new StringBuilder(acctData.length());
					for(int i = 1; i <= matcher.groupCount(); i++) {
						String g = matcher.group(i);
						if(g != null)
							sb.append(g);
					}
					String filteredAcctData = sb.toString();
					if(!filteredAcctData.equals(acctData)) {
						acctData = filteredAcctData;
						value = new MaskedString(acctData);
					}
				}

				if(lookupAccountTask != null && acctData.length() > 0)
					lookupAccountTask.processIndex(taskInfo, acctData, index);

				step.setOptionallyIndexedResultAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, index, value);
				if(additional != null)
					step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, index, additional);
			}
		}
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public AccountProcessor getLookupAccountTask() {
		return lookupAccountTask;
	}

	public void setLookupAccountTask(AccountProcessor lookupAccountIdTask) {
		this.lookupAccountTask = lookupAccountIdTask;
	}
}
