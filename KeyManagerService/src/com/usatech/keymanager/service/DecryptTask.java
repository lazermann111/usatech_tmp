package com.usatech.keymanager.service;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.util.Map;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class DecryptTask implements MessageChainTask {
	
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String,Object> resultAttributes = step.getResultAttributes();
		try {
			long keyId = step.getAttribute(CommonAttrEnum.ATTR_KEY_ID, Long.class, true);
			int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
			Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
			if(cnt == 0) {
				byte[] encryptedData = step.getAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, byte[].class, true);
				if(encryptedData.length > 0) {
					Object value;
					try {
						byte[] decryptedData = keyManagerEngine.decrypt(keyId, encryptedData);
						if(charset == null)
							value = decryptedData;
						else
							value = charset.decode(ByteBuffer.wrap(decryptedData)).toString();
					} catch(KeyManagementException e) {
						log.warn("Key Management Exception", e);
						value = null;
					}
					resultAttributes.put(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue(), value);
				}
			} else
				for(int i = 1; i <= cnt; i++) {
					byte[] encryptedData = step.getIndexedAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, i, byte[].class, false);
					if(encryptedData != null && encryptedData.length > 0) {
						Object value;
						try {
							byte[] decryptedData = keyManagerEngine.decrypt(keyId, encryptedData);
						if(charset == null)
							value = decryptedData;
						else 
							value = charset.decode(ByteBuffer.wrap(decryptedData)).toString();
						} catch(KeyManagementException e) {
							log.warn("Key Management Exception", e);
							value = null;
						}
						resultAttributes.put(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + '.' + i, value);
					}
				}
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

}
