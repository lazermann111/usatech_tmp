package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.security.KeyManagementException;

import simple.util.concurrent.LockLinkedSegmentCache;

import com.usatech.keymanager.CryptoException;
import com.usatech.keymanager.master.KEKStore;
import com.usatech.keymanager.master.PersistentKey;

public class PersistentKeyCache extends LockLinkedSegmentCache<Integer, PersistentKey, GeneralSecurityException> implements KEKStorePatron {
	protected KEKStore kekStore;
	
	protected PersistentKey createValue(Integer key, Object... additionalInfo) throws GeneralSecurityException {
		KEKStore kekStore = getKekStore();
		if(kekStore == null)
			throw new GeneralSecurityException("KEKStore was not provided");
		PersistentKey persistentKey = PersistentKey.getByKID(key);
		if(persistentKey == null)
			throw new KeyManagementException("Key not found for kid: " + key);
		try {
			kekStore.loadSecretKey(persistentKey);
		} catch(CryptoException e) {
			throw new GeneralSecurityException(e);
		}
		return persistentKey;
	}

	public KEKStore getKekStore() {
		return kekStore;
	}

	public void setKekStore(KEKStore kekStore) {
		this.kekStore = kekStore;
	}
}
