package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;

public class RemoveTokenTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			long globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, true);
			byte[] token = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, byte[].class, true);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("globalAccountId", globalAccountId);
			params.put("encryptedToken", encrypt(token));
			int rc = DataLayerMgr.executeUpdate("REMOVE_ACCOUNT_TOKEN", params, true);
			if(rc == 1) {
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 2);
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Successfully untokenized Card Id (" + globalAccountId + ")");
			} else {
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 3);
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "The token for Card Id (" + globalAccountId + ") has changed or has already been untokenized");
			}
		} catch(AttributeConversionException e) {
			log.error("Could not remove token", e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 0);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "System error");
		} catch(SQLException e) {
			log.error("Could not remove token", e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 0);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "System error");
		} catch(DataLayerException e) {
			log.error("Could not remove token", e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 0);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "System error");
		} catch(GeneralSecurityException e) {
			log.error("Could not remove token", e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 0);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "System error");
		}
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}
}
