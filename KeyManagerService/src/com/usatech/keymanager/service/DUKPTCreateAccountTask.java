package com.usatech.keymanager.service;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;

import simple.app.ServiceException;
import simple.io.Log;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class DUKPTCreateAccountTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	protected CreateAccountTask createAccountTask;
	protected final Cache<String, Pattern, PatternSyntaxException> regexCache = new LockSegmentCache<String, Pattern, PatternSyntaxException>() {
		@Override
		protected Pattern createValue(String key, Object... additionalInfo) throws PatternSyntaxException {
			return Pattern.compile(key);
		}
	};

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			CardReaderType cardReaderType = step.getAttributeDefault(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID, CardReaderType.class, CardReaderType.MAGTEK_MAGNESAFE);
			EntryType entryType = step.getAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, false);
			byte[] keySerialNum = step.getAttribute(CommonAttrEnum.ATTR_KEY_SERIAL_NUM, byte[].class, false);
			Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
			byte[] encryptedData = step.getAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, byte[].class, true);
			if(encryptedData.length > 0) {
				String regex = step.getAttribute(CommonAttrEnum.ATTR_VALIDATION_REGEX, String.class, false);
				Pattern pattern = null;
				if (!StringUtils.isBlank(regex))
					pattern = regexCache.getOrCreate(regex);
				byte[][] decryptedData = keyManagerEngine.decryptDUKPT(this, taskInfo, cardReaderType, entryType, keySerialNum, new byte[][] { encryptedData }, 0, pattern, charset);
				String acctData;
				if(charset == null)
					acctData = new String(decryptedData[0]);
				else
					acctData = charset.decode(ByteBuffer.wrap(decryptedData[0])).toString();

				Matcher matcher = null;
				if(pattern != null && !(matcher = pattern.matcher(acctData)).matches()) {
					log.info("Decrypted value for item does not match regex '" + regex + "'");
					step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
					step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "INVALID_DECRYPTED_CARD_DATA");
					step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Decrypted Card Data does not match expected format");
				} else {
					if(matcher != null && matcher.groupCount() > 0) {
						StringBuilder sb = new StringBuilder(acctData.length());
						for(int i = 1; i <= matcher.groupCount(); i++) {
							String g = matcher.group(i);
							if(g != null)
								sb.append(g);
						}
						acctData = sb.toString();
					}
					getCreateAccountTask().processIndex(taskInfo, acctData, 0);
					if(decryptedData.length > 1 && decryptedData[1] != null && decryptedData[1].length > 0)
						step.setResultAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, decryptedData[1]);
				}
			} else {
				log.warn("Decrypted data is zero-length");
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "INVALID_DECRYPTED_CARD_DATA");
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Decrypted card is zero-length");
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.decrypt.error");
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, new Object[] { "Decrypted card is zero-length" });
			}
		} catch(AttributeConversionException e) {
			log.warn("Could not get attributes for DUKPT decryption", e);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "SYSTEM_ERROR");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "System operability issue: " + e.getMessage());
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.decrypt.error");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, new Object[] { "System operability issue", e.getMessage() });
		} catch(GeneralSecurityException e) {
			log.warn("Exception performing DUKPT decryption", e);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "SYSTEM_ERROR");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Decryption Failure: " + e.getMessage());
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.decrypt.error");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, new Object[] { "Decryption Failure", e.getMessage() });
		} catch(InvalidValueException e) {
			log.warn("Could not process DUKPT decryption", e);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INVALID_INPUT);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "MISSING_INPUT");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, e.getMessage());
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.decrypt.error");
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, new Object[] { "Missing Input", e.getMessage() });
		}
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public CreateAccountTask getCreateAccountTask() {
		return createAccountTask;
	}

	public void setCreateAccountTask(CreateAccountTask createAccountTask) {
		this.createAccountTask = createAccountTask;
	}
}
