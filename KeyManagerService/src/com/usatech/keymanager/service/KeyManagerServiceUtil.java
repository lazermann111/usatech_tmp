package com.usatech.keymanager.service;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.MessageChainStep;
import com.usatech.keymanager.apple.vas.crypto.VasDecryptor;
import com.usatech.keymanager.apple.vas.data.DecryptedVas;
import com.usatech.keymanager.apple.vas.data.Vas;
import com.usatech.keymanager.apple.vas.data.VasBuilder;
import com.usatech.keymanager.apple.vas.data.VasData;
import com.usatech.keymanager.apple.vas.exception.InvalidCipherException;
import com.usatech.keymanager.apple.vas.util.ByteArrayUtils;
import com.usatech.keymanager.master.KEKException;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentMaskBRef;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;

public class KeyManagerServiceUtil {
	private static final Log log = Log.getLog();
	
	protected static final PaymentMaskBRef[] CARD_DATA_ORDER = new PaymentMaskBRef[] {// 1:1|2:3|3:5|4:2|5:18|6:19
			PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER,
			PaymentMaskBRef.EXPIRATION_DATE,
			PaymentMaskBRef.DISCRETIONARY_DATA,
			PaymentMaskBRef.CARD_HOLDER,
			PaymentMaskBRef.ZIP_CODE,
			PaymentMaskBRef.ADDRESS
		};
	
	public static VasData decryptVas(KeyManagerEngine keyManagerEngine, byte[] encryptedVASData, byte[] merchantId) throws InvalidCipherException, KEKException {
		try {      
			Vas vas = new VasBuilder().setVasBytes(encryptedVASData).build();
			String publicKeyID = StringUtils.toHex(vas.getFirst4BytesOfPKSha256());
      
			log.info(new StringBuilder("Decrypting VAS data for Public Key ID ").append(publicKeyID).toString());
			PrivateKey privateKey = keyManagerEngine.getAVASPrivateKey(publicKeyID);
			if (privateKey == null)
				return new VasData(encryptedVASData);
			DecryptedVas decryptedVas = VasDecryptor.decode(vas, merchantId, privateKey);
			String decodedMessage = new String(decryptedVas.getData(), Charset.forName("UTF-8"));
			log.info(new StringBuilder(new StringBuilder("Decrypted VAS message length: ").append(decodedMessage.length()).append(" chars")));
			
			if (decodedMessage.contains("|")) {			
				String[] passData = decodedMessage.split("\\|");
				long cardId = Long.valueOf(passData[0]);
				byte[] token = ByteArrayUtils.fromHex(passData[1]);
				String passIdentifier = passData.length > 2 ? passData[2] : null;
				return new VasData(cardId, token, passIdentifier);
			} else
				return new VasData(decodedMessage);
		} catch (InvalidCipherException e) {
			log.error("Error while decoding encrypted Apple VAS data: " + e.getMessage(), e);
			throw e;
		}
	}

	public static String getAccountByToken(KeyManagerEngine keyManagerEngine, MessageChainStep step, int index, long globalAccountId, byte[] encryptedToken, EntryType entryType) {
		String accountData = null;
		Map<String, Object> params = new HashMap<>();
	    params.put("globalAccountId", globalAccountId);
	    params.put("encryptedToken", encryptedToken);
	    try {
		    Results results = DataLayerMgr.executeQuery("GET_ACCOUNT_BY_TOKEN", params);
		    //TODO: Add support for multiple AVAS passes in message
		    String idxStr = index == 0 ? "" : ('.' + String.valueOf(index));
		    if (results.next()) {
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE.getValue() + idxStr, results.getValue("instance", Integer.class));
		        Long keyId = results.getValue("keyId", Long.class);
		        byte[][] encryptedData = results.getValue("encryptedData", byte[][].class);
		        String initiatingDeviceName = results.getValue("initiatingDeviceName", String.class);
		        // Re-construct the manual data
		        StringBuilder cardData = new StringBuilder();
		        for (PaymentMaskBRef bref : CARD_DATA_ORDER) {
		            if (bref.getStoreIndex() > 0 && bref.getStoreIndex() <= encryptedData.length && encryptedData[bref.getStoreIndex() - 1] != null) {
		                cardData.append(new String(keyManagerEngine.decrypt(keyId, encryptedData[bref.getStoreIndex() - 1]), ProcessingConstants.US_ASCII_CHARSET));
		            }
		            cardData.append('|');
		        }
		        accountData = cardData.substring(0, cardData.length() - 1);
		        if (entryType == EntryType.VAS) {
					String[] data = accountData.split("\\|");
					//fallback to track 2
					if (data.length > 1)
						accountData = new StringBuilder().append(data[0]).append('=').append(data[1]).append("123456789").toString();
		        }
		        step.setSecretResultAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + idxStr, accountData);
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_CARD_KEY.getValue() + idxStr, "A:" + initiatingDeviceName + ':' + globalAccountId);
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue() + idxStr, results.getValue("expirationTime", Long.class));
		    } else {
		        // fail
		        log.warn("GET_ACCOUNT_BY_TOKEN in RetrieveByTokenTask returned NO RESULTS");
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue() + idxStr, AuthResultCode.DECLINED);
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue() + idxStr, "INVALID_TOKEN");
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue() + idxStr, "Invalid token for Card Id (" + globalAccountId + ")");
		    }
		} catch(SQLException e) {
			log.warn("Could not retrieve account for token authorization", e);
		} catch(GeneralSecurityException e) {
			log.warn("Could not decrypt data for token authorization", e);
		} catch(ConvertException e) {
			log.warn("Could not convert data for token authorization", e);
		} catch(DataLayerException e) {
			log.warn("Could not retrieve account for token authorization", e);
		} finally {
			return accountData;
		}
	}
}
