package com.usatech.keymanager.service;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;

public interface AccountProcessor {
	public void processIndex(MessageChainTaskInfo taskInfo, String acctData, int index) throws ServiceException;
}
