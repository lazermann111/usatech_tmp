package com.usatech.keymanager.service;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.cayenne.access.DataContext;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.server.Authentication;

public class DataContextClientCertAuthenticator extends ClientCertAuthenticator {
	protected String domainName;
	protected DataContext context;

	public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
		Authentication a = super.validateRequest(req, res, mandatory);
		DataContext.bindThreadDataContext(context);
		return a;
	}

	public void initialize() {
		context = (getDomainName() == null ? DataContext.createDataContext() : DataContext.createDataContext(getDomainName()));
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
}
