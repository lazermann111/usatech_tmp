package com.usatech.keymanager.service;

import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.crypto.CipherInputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.ByteOutput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetUtils;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class MassCreateAccountTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected int maxBatchSize = 100;
	protected short instance;
	protected static final String[] OUTPUT_HEADER = new String[] { AuthorityAttrEnum.ATTR_TRAN_ID.getValue(), AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH.getValue(), AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), AuthorityAttrEnum.ATTR_INSTANCE.getValue() };
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
	        byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);	
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);	
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);
			
			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.BLOCKING_RETRY, true);
			Resource inputResource = rf.getResource(resourceKey, ResourceMode.READ);
			try {
				CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, inputResource.getInputStream());
	    		try {
					int rows = -1;
					Connection conn = DataLayerMgr.getConnection("KM", false);
					try {
						Resource outputResource = rf.getResource(inputResource.getName() + ".result", ResourceMode.CREATE);
						boolean okay = false;
						try {
							OutputStream out = Cryption.createEncryptingOutputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, outputResource.getOutputStream());
							ByteOutput output = new OutputStreamByteOutput(out);
							DatasetUtils.writeHeader(output, OUTPUT_HEADER);
							rows = DatasetUtils.parseDataset(new InputStreamByteInput(cis), new MassAccountCreate(conn, "GET_OR_CREATE_ACCOUNT_ID", getMaxBatchSize(), output));
							DatasetUtils.writeFooter(output);
							output.flush();
							out.close();
							step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, outputResource.getKey());
							outputResource.release();
							log.info("Processed " + rows + " for account update");
							okay = true;
						} finally {
							if(!okay) {
								outputResource.delete();
								outputResource.release();
							}
						}
					} finally {
						if(rows < 0 && !conn.getAutoCommit())
							conn.rollback();
						conn.close();
					}
				} finally {
					cis.close();
				}			
			} finally {
				inputResource.release();
			}
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	protected class MassAccountCreate extends AbstractAttributeDatasetHandler<ServiceException> {
		protected final Connection conn;
		protected final Call call;
		protected final int maxBatchSize;
		protected final ByteOutput output;
		protected final Object[] values = new Object[OUTPUT_HEADER.length];
		protected int batchCnt = 0;

		public MassAccountCreate(Connection conn, String callId, int maxBatchSize, ByteOutput output) throws CallNotFoundException {
			this.conn = conn;
			this.call = DataLayerMgr.getGlobalDataLayer().findCall(callId);
			this.maxBatchSize = maxBatchSize;
			this.output = output;
		}
		@Override
		public void handleRowEnd() throws ServiceException {
			byte[] accountCdHash;
			try {
				accountCdHash = getDetailAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, byte[].class, true);
				data.put("encryptedAccountCd", encrypt(accountCdHash));
				data.put("instance", getInstance());
			} catch(AttributeConversionException e) {
				throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
			} catch(GeneralSecurityException e) {
				throw new ServiceException(e);
			}
			try {
				call.executeCall(conn, data, null);
			} catch(ParameterException e) {
				throw new ServiceException(e);
			} catch(SQLException e) {
				throw new ServiceException(e);
			}
			checkCommit();
			try {
				values[0] = getDetailAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, Long.class, true);
				values[1] = accountCdHash;
				values[2] = getDetailAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, true);
				Short oldInstance = getDetailAttribute(AuthorityAttrEnum.ATTR_OLD_INSTANCE, Short.class, false);
				values[3] = oldInstance == null ? getInstance() : oldInstance;
			} catch(AttributeConversionException e) {
				throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
			}
			try {
				DatasetUtils.writeRow(output, values);
			} catch(IOException e) {
				throw new ServiceException(e);
			}
		}

		@Override
		public void handleDatasetEnd() throws ServiceException {
			checkCommit();
		}

		@Override
		public void handleDatasetStart(String[] columnNames) throws ServiceException {

		}

		protected void checkCommit() throws ServiceException {
			try {
				if(++batchCnt >= maxBatchSize && !conn.getAutoCommit()) {
					batchCnt = 0;
					conn.commit();
				}
			} catch(SQLException e) {
				throw new ServiceException(e);
			}
		}
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public short getInstance() {
		return instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}
}
