package com.usatech.keymanager.service;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.security.SecurityUtils;
import simple.text.StringUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.keymanager.master.PersistentKey;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;

public class LookupTokenTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected int tokenSize = 16;
	protected KeyManagerEngine keyManagerEngine;
	protected String updateAccountQueueKey;
	protected short[] otherInstances;
	protected short instance;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		long globalAccountId;
		String initiatingDeviceName;
		boolean createOnMissing;
		boolean needToFindAccountById = false;
		try {
			globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, true);
			needToFindAccountById = step.getAttributeDefault(CommonAttrEnum.ATTR_INTERNAL_MESSAGE, Boolean.class, false);
			initiatingDeviceName = step.getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
			createOnMissing = step.getAttributeDefault(CommonAttrEnum.ATTR_CREATE, Boolean.class, Boolean.FALSE);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not get attribute", e, WorkRetryType.NO_RETRY);
		}
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("globalAccountId", globalAccountId);
			params.put("initiatingDeviceName", initiatingDeviceName);
			log.info("Retrieving card data for globalAccountId " + globalAccountId);
			Results results = null;
			if (!needToFindAccountById) {
				results = DataLayerMgr.executeQuery("GET_ACCOUNT_BY_DEVICE", params);
			} else {
				results = DataLayerMgr.executeQuery("GET_ACCOUNT_BY_GLOBAL_ACCT_ID", params);
			}
			if(results.next()) {
				byte[] encryptedToken = results.getValue("encryptedToken", byte[].class);
				if(encryptedToken == null || encryptedToken.length == 0) {
					// not tokenized
					if(createOnMissing) {
						long expirationTime;
						String expDate = step.getIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.EXPIRATION_DATE.getStoreIndex(), String.class, false);
						if(!StringUtils.isBlank(expDate))
							expirationTime = InteractionUtils.expDateToMillis(expDate);
						else
							expirationTime = System.currentTimeMillis() + (365 * 24 * 60 * 60 * 1000L);
						Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
						PersistentKey key = keyManagerEngine.getPersistentKey(expirationTime);
						int decryptedCnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 1);
						byte[][] cardData = new byte[decryptedCnt][];
						byte[][] encryptedData = new byte[decryptedCnt][];
						// get card data
						for(int i = 0; i < encryptedData.length; i++) {
							Object data = step.getIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, i + 1, Object.class, false);
							if(data == null)
								continue;
							if(data instanceof byte[])
								cardData[i] = (byte[]) data;
							if(data instanceof String)
								cardData[i] = charset == null ? ((String) data).getBytes() : ((String) data).getBytes(charset);
							if(data instanceof MaskedString)
								cardData[i] = charset == null ? ((MaskedString) data).getValue().getBytes() : ((MaskedString) data).getValue().getBytes(charset);
							if(charset == null)
								cardData[i] = ConvertUtils.convert(byte[].class, data);
							else {
								String sdata = ConvertUtils.convert(String.class, data);
								if(sdata == null)
									continue;
								cardData[i] = sdata.getBytes(charset);
							}
							if(cardData[i] != null)
								encryptedData[i] = keyManagerEngine.encrypt(key, cardData[i]);
						}
						
						// create token
						byte[] token = new byte[getTokenSize()];
						SecurityUtils.getSecureRandom().nextBytes(token);
						encryptedToken = encrypt(token);
						long deviceTranCd = step.getAttributeDefault(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, long.class, 0L);
						params.clear();
						params.put("globalAccountId", globalAccountId);
						params.put("keyId", key.getKid());
						params.put("encryptedToken", encryptedToken);
						params.put("encryptedData", encryptedData);
						params.put("expirationTime", expirationTime);
						params.put("initiatingDeviceName", initiatingDeviceName);
						params.put("deviceTranCd", deviceTranCd);
						DataLayerMgr.executeUpdate("UPSERT_TOKEN", params, true);
						if(ConvertUtils.getBoolean(params.get("result"))) {
							String baseQueueName = getUpdateAccountQueueKey();
							MessageChain mc = new MessageChainV11();
							MessageChainStep broadcastStep = mc.addStep(baseQueueName);
							if(baseQueueName != null)
								baseQueueName += '.';
								
							broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
							broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, token);
							broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.APPROVED);
							broadcastStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, decryptedCnt);
							broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, expirationTime);
							broadcastStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, initiatingDeviceName);
							broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, deviceTranCd);
							for(int i = 0; i < cardData.length; i++)
								broadcastStep.setIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, i + 1, cardData[i]);
							//publish broadcastStep
							for(short i : getOtherInstances()) {
								if(i != instance) {
									broadcastStep.setQueueKey(baseQueueName + i);
									MessageChainService.publish(broadcastStep.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping());
								}
							}
							step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, token);
							step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.APPROVED.getAuthResponseCodeEC2());
						} else {
							// Existing token already exists - get it and decrypt it and use it
							byte[] existingEncryptedToken = ConvertUtils.convertRequired(byte[].class, params.get("encryptedToken"));
							byte[] existingToken = decrypt(existingEncryptedToken);
							step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, existingToken);
							step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.APPROVED.getAuthResponseCodeEC2());
						}
					} else {
						step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
						step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Account is not tokenized");
					}
				} else {
					byte[] token = decrypt(encryptedToken);
					step.getResultAttributes().put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue(), token);
					step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.APPROVED.getAuthResponseCodeEC2());
				}
			} else {
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Account is not registered");
			}
		} catch(SQLException e) {
			log.warn("Could not retrieve token for account " + globalAccountId, e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error. Please try again.");
		} catch(GeneralSecurityException e) {
			log.warn("Could not decrypt token for account " + globalAccountId, e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error. Please try again.");
		} catch(ConvertException e) {
			log.warn("Could not convert token for account " + globalAccountId, e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error. Please try again.");
		} catch(DataLayerException e) {
			log.warn("Could not retrieve token for account " + globalAccountId, e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error. Please try again.");
		} catch(AttributeConversionException e) {
			log.warn("Could not create token for account " + globalAccountId, e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error. Please try again.");
		}
		return 0;
	}

	public int getTokenSize() {
		return tokenSize;
	}

	public void setTokenSize(int tokenSize) {
		this.tokenSize = tokenSize;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public String getUpdateAccountQueueKey() {
		return updateAccountQueueKey;
	}

	public void setUpdateAccountQueueKey(String updateAccountQueueKey) {
		this.updateAccountQueueKey = updateAccountQueueKey;
	}

	public short[] getOtherInstances() {
		return otherInstances;
	}

	public void setOtherInstances(short[] otherInstances) {
		this.otherInstances = otherInstances;
	}

	public short getInstance() {
		return instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}
}
