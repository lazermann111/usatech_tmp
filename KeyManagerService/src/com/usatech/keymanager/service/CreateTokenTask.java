package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.db.DataLayerException;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;

public class CreateTokenTask extends UpdateAccountTask {
	protected String storeCardQueueKey = "usat.keymanager.store#any";

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		final AuthResultCode authResult = taskInfo.getStep().getAttributeSafely(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, null);
		if((authResult == AuthResultCode.FAILED || authResult == AuthResultCode.DECLINED) && taskInfo.getStep().getAttributeSafely(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class, false)) {
			// setup store task after response
			MessageChainStep[] mcss = taskInfo.getStep().getNextSteps(0);
			MessageChainStep responseStep = (mcss == null || mcss.length == 0 ? null : mcss[0]);
			if(responseStep != null) {
				MessageChainStep storeStep = taskInfo.getStep().copyStep();
				storeStep.setQueueKey(getStoreCardQueueKey());
				for(Attribute a : new Attribute[] { AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, AuthorityAttrEnum.ATTR_INSTANCE, AuthorityAttrEnum.ATTR_CARD_KEY })
					taskInfo.getStep().swapReferences(a, storeStep, a);
				Set<MessageChainStep> afterSteps = new LinkedHashSet<MessageChainStep>();
				for(Integer rc : responseStep.getResultCodes().toArray(new Integer[responseStep.getResultCodes().size()])) {
					if(rc != null) {
						MessageChainStep[] nextSteps = responseStep.getNextSteps(rc);
						if(nextSteps != null)
							for(MessageChainStep nextStep : nextSteps)
								afterSteps.add(nextStep);
						responseStep.setNextSteps(rc, storeStep);
					}
				}
				storeStep.setNextSteps(0, afterSteps.toArray(new MessageChainStep[afterSteps.size()]));
				return 0;
			}
		}
		
		MessageChainStep broadcastStep;
		String baseQueueName = getUpdateAccountQueueKey();
		if(baseQueueName != null) {
			baseQueueName += '.';
			MessageChain mc = new MessageChainV11();
			broadcastStep = mc.addStep(baseQueueName);
		} else
			broadcastStep = null;
		try {
			if(processAccount(taskInfo, true, broadcastStep)) {
				if(broadcastStep != null) {
					for(short i : getOtherInstances()) {
						if(i != instance) {
							broadcastStep.setQueueKey(baseQueueName + i);
							MessageChainService.publish(broadcastStep.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping(), null);
						}
					}
				}
			}
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(GeneralSecurityException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}

	public String getStoreCardQueueKey() {
		return storeCardQueueKey;
	}

	public void setStoreCardQueueKey(String storeCardQueueKey) {
		this.storeCardQueueKey = storeCardQueueKey;
	}
}
