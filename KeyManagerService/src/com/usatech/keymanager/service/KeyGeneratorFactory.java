package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.security.Provider;
import java.security.Security;
import java.util.concurrent.atomic.AtomicReference;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import simple.io.Log;


public class KeyGeneratorFactory {
	
	private static final Log log = Log.getLog();
	protected String algorithm;
	protected String encoding;
	protected String padding;
	protected int keySize;
	protected int ivSize;
	protected String provider;
	protected final AtomicReference<KeyGeneratorInfo> keyGeneratorInfo = new AtomicReference<KeyGeneratorInfo>();

	public static class SecretKeyInfo {
		public final SecretKey secretKey;
		public final String providerName;
		public final String transformation;
		public final int keySize;
		public final int ivSize;

		public SecretKeyInfo(SecretKey secretKey, String providerName, String transformation, int keySize, int ivSize) {
			super();
			this.secretKey = secretKey;
			this.providerName = providerName;
			this.transformation = transformation;
			this.keySize = keySize;
			this.ivSize = ivSize;
		}
	}

	protected static class KeyGeneratorInfo {
		public final KeyGenerator keyGenerator;
		public final String transformation;
		public final int keySize;
		public final int ivSize;

		public KeyGeneratorInfo(KeyGenerator keyGenerator, String transformation, int keySize, int ivSize) {
			this.keyGenerator = keyGenerator;
			this.transformation = transformation;
			this.keySize = keySize;
			this.ivSize = ivSize;
		}
	}

	public SecretKeyInfo generateKey() throws GeneralSecurityException {
		KeyGeneratorInfo kgi = keyGeneratorInfo.get();
		if(kgi == null) {
			String provider = this.getProvider();
			Provider providerObj = Security.getProvider(provider);
			if(provider == null)
				throw new GeneralSecurityException("Provider not found: " + provider);
			if(log.isDebugEnabled())
				log.debug("Loaded security Provider: " + provider + " (" + providerObj.getClass().getName() + ")");
			String algorithm = this.algorithm;
			String transformation = algorithm + "/" + encoding + "/" + padding;
			KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm, providerObj);
			int keySize = this.keySize;
			int ivSize = this.ivSize;
			keyGenerator.init(keySize);
			if(log.isDebugEnabled())
				log.debug("Loaded KeyGenerator: " + keyGenerator.getAlgorithm() + " (" + keyGenerator.getClass().getName() + ")");
			kgi = new KeyGeneratorInfo(keyGenerator, transformation, keySize, ivSize);
			keyGeneratorInfo.compareAndSet(null, kgi);
		}
		return new SecretKeyInfo(kgi.keyGenerator.generateKey(), kgi.keyGenerator.getProvider().getName(), kgi.transformation, kgi.keySize, kgi.ivSize);
	}

	protected void resetKgi() {
		keyGeneratorInfo.set(null);
	}
	
	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
		resetKgi();
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
		resetKgi();
	}

	public int getKeySize() {
		return keySize;
	}

	public void setKeySize(int keySize) {
		this.keySize = keySize;
		resetKgi();
	}

	public int getIvSize() {
		return ivSize;
	}

	public void setIvSize(int ivSize) {
		this.ivSize = ivSize;
		resetKgi();
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
		resetKgi();
	}

	public String getPadding() {
		return padding;
	}

	public void setPadding(String padding) {
		this.padding = padding;
		resetKgi();
	}
}
