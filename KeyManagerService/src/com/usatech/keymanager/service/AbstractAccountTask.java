package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import simple.app.RetrySpecifiedServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;
import simple.security.SecureHash;
import simple.text.StringUtils;

import com.usatech.app.MessageChainTask;
import com.usatech.keymanager.CryptoException;
import com.usatech.keymanager.master.KEKStore;

public abstract class AbstractAccountTask implements MessageChainTask, KEKStorePatron {
	private static final Log log = Log.getLog();
	protected static final Pattern CARD_NUMBER_PATTERN = Pattern.compile("([0-9]{13,})");
	protected Key key;
	protected KEKStore kekStore;
	protected String cipherName = "AES/ECB/NOPADDING";
	
	protected static byte[] hash(String data) throws NoSuchAlgorithmException {
		return SecureHash.getUnsaltedHash(data.getBytes());
	}

	protected int decryptCrc(byte[] encryptedCrc) throws RetrySpecifiedServiceException, GeneralSecurityException {
		Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
		byte[] decrypted = cipher.doFinal(encryptedCrc);
		if(decrypted.length < 2)
			throw new GeneralSecurityException("Resultant bytes is too small (" + decrypted.length + ")");
		return (((decrypted[0] & 0xff) << 8) + ((decrypted[1] & 0xff) << 0));
	}

	protected byte[] encryptCrc(int crc) throws RetrySpecifiedServiceException, GeneralSecurityException {
		Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
		int bs = cipher.getBlockSize();
		byte[] unencrypted = new byte[Math.max(2, bs)];
		unencrypted[0] = (byte) ((crc >>> 8) & 0xFF);
		unencrypted[1] = (byte) ((crc >>> 0) & 0xFF);
		return cipher.doFinal(unencrypted);
	}

	protected byte[] encrypt(byte[] unencrypted) throws RetrySpecifiedServiceException, GeneralSecurityException {
		Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
		int bs = cipher.getBlockSize();
		int m;
		if(bs != 0 && (m = (unencrypted.length % bs)) != 0) {
			byte[] tmp = new byte[unencrypted.length - m + bs];
			System.arraycopy(unencrypted, 0, tmp, 0, unencrypted.length);
			unencrypted = tmp;
		}
		return cipher.doFinal(unencrypted);
	}

	protected byte[] decrypt(byte[] encrypted) throws RetrySpecifiedServiceException, GeneralSecurityException {
		Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
		return cipher.doFinal(encrypted);
	}

	protected Cipher getCipher(int opmode) throws RetrySpecifiedServiceException, GeneralSecurityException {
		if(key == null)
			throw new RetrySpecifiedServiceException(getKekStore() == null ? "KEKStore is not loaded" : "Account Encrypting Key is not found in the KEKStore", WorkRetryType.BLOCKING_RETRY);
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(opmode, key);
		return cipher;
	}

	/*
	protected final EnhancedThreadLocal<Cipher, UndeclaredThrowableException> decryptionCipherLocal = new EnhancedThreadLocal<Cipher, UndeclaredThrowableException>(10, 0.75f, 8) {
		@Override
		protected Cipher initialValue() {
			if(key == null)
				throw new UndeclaredThrowableException(new RetrySpecifiedServiceException(getKekStore() == null ? "KEKStore is not loaded" : keyId == 0 ? "Key Id is not set" : "Key Id " + keyId + " is not found in the KEKStore", WorkRetryType.BLOCKING_RETRY));
			Cipher cipher;
			try {
				cipher = Cipher.getInstance(cipherName);
				cipher.init(Cipher.DECRYPT_MODE, key);
			} catch(GeneralSecurityException e) {
				throw new UndeclaredThrowableException(e);
			}
			return cipher;
		}
	};
	protected final EnhancedThreadLocal<Cipher, UndeclaredThrowableException> encryptionCipherLocal = new EnhancedThreadLocal<Cipher, UndeclaredThrowableException>(10, 0.75f, 8) {
		@Override
		protected Cipher initialValue() {
			if(key == null)
				throw new UndeclaredThrowableException(new RetrySpecifiedServiceException(getKekStore() == null ? "KEKStore is not loaded" : keyId == 0 ? "Key Id is not set" : "Key Id " + keyId + " is not found in the KEKStore", WorkRetryType.BLOCKING_RETRY));
			Cipher cipher;
			try {
				cipher = Cipher.getInstance(cipherName);
				cipher.init(Cipher.ENCRYPT_MODE, key);
			} catch(GeneralSecurityException e) {
				throw new UndeclaredThrowableException(e);
			}
			return cipher;
		}
	};
	protected Cipher getCipherCached(int opmode) throws RetrySpecifiedServiceException, GeneralSecurityException {
		try {
			switch(opmode) {
				case Cipher.DECRYPT_MODE:
					return decryptionCipherLocal.get();
				case Cipher.ENCRYPT_MODE:
					return encryptionCipherLocal.get();
				default:
					throw new RetrySpecifiedServiceException("Invalid opmode " + opmode, WorkRetryType.NO_RETRY);
			}
		} catch(UndeclaredThrowableException e) {
			if(e.getCause() instanceof RetrySpecifiedServiceException)
				throw (RetrySpecifiedServiceException) e.getCause();
			if(e.getCause() instanceof GeneralSecurityException)
				throw (GeneralSecurityException) e.getCause();
			throw e;
		}
	}
	*/

	protected void updateKey() {
		if(kekStore != null)
			try {
				SecretKey tmp = kekStore.getAccountEncryptingSecret();
				String alg = StringUtils.substringBefore(getCipherName(), "/");
				if(tmp.getAlgorithm().equalsIgnoreCase(alg))
					key = tmp;
				else
					key = new SecretKeySpec(tmp.getEncoded(), alg);
			} catch(CryptoException e) {
				log.error("Could not get Account Encrypting Key", e);
				key = null;
			}
		else
			key = null;
	}

	@Override
	public void setKekStore(KEKStore kekStore) {
		this.kekStore = kekStore;
		updateKey();
	}

	public KEKStore getKekStore() {
		return kekStore;
	}

	public String getCipherName() {
		return cipherName;
	}

	public void setCipherName(String cipherName) {
		this.cipherName = cipherName;
	}
}
