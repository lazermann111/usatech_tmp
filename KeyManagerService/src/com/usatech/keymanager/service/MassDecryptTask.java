package com.usatech.keymanager.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.util.Map;

import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.ByteOutput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetUtils;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class MassDecryptTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	protected ResourceFolder resourceFolder;
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String,Object> resultAttributes = step.getResultAttributes();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
	        byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);	
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);	
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);
			int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
			Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
			
			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.NONBLOCKING_RETRY, true);
			Resource inputResource = rf.getResource(resourceKey, ResourceMode.UPDATE);
			boolean okay = false;
			try {
				CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, inputResource.getInputStream());
	    		try {
	            	Resource outputResource = rf.getResource(inputResource.getName() + ".decrypt", ResourceMode.CREATE);
					try {
						resultAttributes.put(CommonAttrEnum.ATTR_CRYPTO_RESOURCE.getValue(), outputResource.getKey());	
						CipherOutputStream cos = Cryption.createEncryptingOutputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, outputResource.getOutputStream());					
			            try {
			            	DatasetUtils.parseDataset(new InputStreamByteInput(cis), new DecryptedDataGenerator(new OutputStreamByteOutput(cos), cnt, charset));
			            	cos.flush();
			            } catch(IOException e) {
			            	throw new ServiceException("Could not write decrypted crypto file", e);
			            } finally {
			            	cos.close();
			            }
			            okay = true;
					} finally {
						if(!okay)
							outputResource.delete();
						outputResource.release();
					}
				} finally {
					cis.close();
				}			
			} finally {
				if(okay)
					inputResource.delete();
				inputResource.release();
			}
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	protected class DecryptedDataGenerator extends AbstractAttributeDatasetHandler<IOException> {
		protected final ByteOutput output;
		protected final int cnt;
		protected final Charset charset;
		protected final Object[] values;
		public DecryptedDataGenerator(ByteOutput output, int cnt, Charset charset) {
			this.output = output;
			this.cnt = cnt;
			this.charset = charset;
			if(cnt == 0)
				values = new Object[1];
			else
				values = new Object[cnt];
		}

		public void handleDatasetEnd() throws IOException {
			DatasetUtils.writeFooter(output);
		}

		public void handleDatasetStart(String[] columnNames) throws IOException {
			String[] outputColumnNames;
			if(cnt == 0) {
				outputColumnNames = new String[] {
						CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue()
				};
			} else {
				outputColumnNames = new String[cnt];
				for(int i = 0; i < cnt; i++)
					outputColumnNames[i] = CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + '.' + (i+1);
			}
			DatasetUtils.writeHeader(output, outputColumnNames);
		}

		public void handleRowEnd() throws IOException {
			try {
				Long keyId = getDetailAttribute(CommonAttrEnum.ATTR_KEY_ID, Long.class, false);
				if(keyId == null) {
					for(int i = 0; i < values.length; i++)
						values[i] = null;
				} else if(cnt == 0) {
					byte[] encryptedData = getDetailAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, byte[].class, false);
					if(encryptedData != null && encryptedData.length > 0) {
						try {
							byte[] decryptedData = keyManagerEngine.decrypt(keyId, encryptedData);
							if(charset == null)
								values[0] = decryptedData;
							else
								values[0] = charset.decode(ByteBuffer.wrap(decryptedData)).toString();
						} catch(KeyManagementException e) {
							log.warn("Key Management Exception", e);
							values[0] = null;
						}
					} else
						values[0] = null;
				} else {
					for(int i = 0; i < cnt; i++) {
						byte[] encryptedData = getDetailIndexedAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, i+1, byte[].class, false);
						if(encryptedData != null && encryptedData.length > 0) {
							byte[] decryptedData = keyManagerEngine.decrypt(keyId, encryptedData);
							if(charset == null)
								values[i] = decryptedData;
							else 
								values[i] = charset.decode(ByteBuffer.wrap(decryptedData)).toString();
						} else
							values[i] = null;
					}
				}
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			} catch(GeneralSecurityException e) {
				throw new IOException(e);
			}
			DatasetUtils.writeRow(output, values);
			data.clear();
		}
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}
}
