package com.usatech.keymanager.service;

import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.crypto.CipherInputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.db.ParameterException;
import simple.io.ByteOutput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.mq.app.AnyTwoInstanceWorkQueue;
import simple.mq.app.MultiInstanceWorkQueue;
import simple.results.DatasetUtils;
import simple.text.StringUtils;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.Tallier;

public class MassLookupAccountTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected int maxBatchSize = 100;
	protected short instance;
	protected KeyManagerEngine keyManagerEngine;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
	        byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);	
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);	
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);
			
			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.BLOCKING_RETRY, true);
			Resource inputResource = rf.getResource(resourceKey, ResourceMode.READ);
			try {
				CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, inputResource.getInputStream());
	    		try {
					int rows = -1;
					Connection conn = DataLayerMgr.getConnection("KM", false);
					try {
						Resource outputResource = rf.getResource(inputResource.getName() + ".result" + getInstance(), ResourceMode.CREATE);
						boolean okay = false;
						try {
							OutputStream out = Cryption.createEncryptingOutputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, outputResource.getOutputStream());
							ByteOutput output = new OutputStreamByteOutput(out);
							Tallier<String> retrieveQueues = new Tallier<String>(String.class);
							MassAccountLookup mal = new MassAccountLookup(conn, "GET_OR_CREATE_ACCOUNT_ID", getMaxBatchSize(), output, retrieveQueues);
							rows = DatasetUtils.parseDataset(new InputStreamByteInput(cis), mal);
							output.flush();
							out.close();
							log.info("Processed " + rows + " for lookup account");
							step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, outputResource.getKey());
							okay = true;
							if(retrieveQueues.getInstanceCount() > 0) {
								step.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, outputResource.getKey());
								String[] excludes = step.getAttributeSafely(AuthorityAttrEnum.ATTR_EXCLUDE_INSTANCES, String[].class, null);
								if(excludes != null)
									retrieveQueues.exclude(excludes);
								if(retrieveQueues.getInstanceCount() == 0) {
									log.error("No instances are left to process encrypted data lists of some entries; leaving data null on them and continuing");
									// we need to fail this transaction so send it on to the next step with nothing set (data will be null)
									return 0;
								}
								if(excludes == null)
									excludes = new String[] { String.valueOf(getInstance()) };
								else {
									String[] tmp = new String[excludes.length + 1];
									System.arraycopy(excludes, 0, tmp, 1, excludes.length);
									tmp[0] = String.valueOf(getInstance());
									excludes = tmp;
								}

								step.setAttribute(AuthorityAttrEnum.ATTR_EXCLUDE_INSTANCES, excludes);
								String baseQueueName = MultiInstanceWorkQueue.getBaseQueueName(step.getQueueKey());
								String queueName = AnyTwoInstanceWorkQueue.constructQueueName(baseQueueName, retrieveQueues.getWinners(2));
								step.setQueueKey(queueName);
								return 1;
							}
						} finally {
							outputResource.release();
							if(!okay)
								outputResource.delete();
						}
					} finally {
						if(rows < 0 && !conn.getAutoCommit())
							conn.rollback();
						conn.close();
					}
				} finally {
					cis.close();
				}			
			} finally {
				inputResource.release();
			}
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	protected class MassAccountLookup extends AbstractAttributeDatasetHandler<ServiceException> {
		protected final Connection conn;
		protected final Call call;
		protected final int maxBatchSize;
		protected final ByteOutput output;
		protected Object[] values;
		protected int batchCnt = 0;
		protected final Tallier<String> retrieveQueues;
		protected String[] columnNames;

		public MassAccountLookup(Connection conn, String callId, int maxBatchSize, ByteOutput output, Tallier<String> retrieveQueues) throws CallNotFoundException {
			this.conn = conn;
			this.call = DataLayerMgr.getGlobalDataLayer().findCall(callId);
			this.maxBatchSize = maxBatchSize;
			this.output = output;
			this.retrieveQueues = retrieveQueues;
		}

		@Override
		public void handleRowEnd() throws ServiceException {
			// check if row needs processing
			Long globalAccountId;
			try {
				globalAccountId = getDetailAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
			} catch(AttributeConversionException e) {
				globalAccountId = null;
			}
			boolean writeRow;
			if(globalAccountId == null || globalAccountId.longValue() == 0L)
				writeRow = processRow();
			else
				writeRow = true;
			if(writeRow && columnNames != null) {
				for(int i = 0; i < columnNames.length; i++)
					values[i] = data.get(columnNames[i]);
				try {
					DatasetUtils.writeRow(output, values);
				} catch(IOException e) {
					throw new ServiceException(e);
				}
			}
		}

		/**
		 * @return Whether to write the row or not
		 * @throws ServiceException
		 */
		protected boolean processRow() throws ServiceException {
			byte[] accountCdHash;
			try {
				String encryptedIdList = getDetailAttribute(AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST, String.class, false);
				if(StringUtils.isBlank(encryptedIdList))
					return false;
				Map<String, Long> parsedMap = new HashMap<String, Long>();
				try {
					InteractionUtils.parseCardIdList(encryptedIdList, parsedMap);
				} catch(ParseException e) {
					log.warn("Cannot parse encrypt id list", e);
					return true;
				}
				Long encryptedId = parsedMap.remove(String.valueOf(getInstance()));
				if(encryptedId == null) {// send to next instance
					log.info("Instance " + getInstance() + " does not have this card; forwarding to next instance");
					retrieveQueues.vote(parsedMap.keySet());
					return true;
				}
				log.info("Retrieving card #" + encryptedId);
				String[] decrypted;
				try {
					decrypted = keyManagerEngine.retrieveStrings(encryptedId, ProcessingConstants.US_ASCII_CHARSET, KeyManagerEngine.INTERNAL_USER_NAME, null);
				} catch(NotEnoughRowsException e) {
					if(parsedMap.isEmpty()) {
						log.error("No instances can process card list " + encryptedIdList + "; leaving null");
						return false;
					}
					log.error("Instance " + getInstance() + " does not have card #" + encryptedId + "; forwarding to next instance");
					String newCardIdList = InteractionUtils.buildCardIdList(parsedMap);
					data.put(AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue(), newCardIdList);
					retrieveQueues.vote(parsedMap.keySet());
					return true;
				} catch(KeyManagementException e) {
					if(parsedMap.isEmpty()) {
						log.error("No instances can process card list " + encryptedIdList + "; leaving pan and exp date null", e);
						return false;
					}
					log.error("Instance " + getInstance() + " can not process card #" + encryptedId + "; forwarding to next instance", e);
					String newCardIdList = InteractionUtils.buildCardIdList(parsedMap);
					data.put(AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue(), newCardIdList);
					retrieveQueues.vote(parsedMap.keySet());
					return true;
				} catch(SQLException | DataLayerException e) {
					throw new ServiceException(e);
				}
				accountCdHash = hash(MessageResponseUtils.getCardNumber(decrypted[0]));
				data.put("encryptedAccountCd", encrypt(accountCdHash));
				data.put("instance", getInstance());
			} catch(AttributeConversionException | ConvertException e) {
				log.warn("Cannot convert for encrypt id list", e);
				return false;
			} catch(GeneralSecurityException e) {
				throw new ServiceException(e);
			}
			try {
				call.executeCall(conn, data, null);
			} catch(ParameterException e) {
				throw new ServiceException(e);
			} catch(SQLException e) {
				throw new ServiceException(e);
			}
			checkCommit();
			data.put("accountCdHash", accountCdHash);
			Short oldInstance;
			try {
				oldInstance = getDetailAttribute(AuthorityAttrEnum.ATTR_OLD_INSTANCE, Short.class, false);
			} catch(AttributeConversionException e) {
				log.warn("Cannot convert for encrypt id list", e);
				return false;
			}
			if(oldInstance != null)
				data.put("instance", oldInstance);
			return true;
		}

		@Override
		public void handleDatasetEnd() throws ServiceException {
			checkCommit();
			try {
				DatasetUtils.writeFooter(output);
			} catch(IOException e) {
				throw new ServiceException(e);
			}
		}

		@Override
		public void handleDatasetStart(String[] columnNames) throws ServiceException {
			Set<String> cols = new LinkedHashSet<>();
			for(String cn : columnNames)
				cols.add(cn);
			cols.add(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
			cols.add(AuthorityAttrEnum.ATTR_INSTANCE.getValue());
			cols.add(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH.getValue());

			this.columnNames = cols.toArray(new String[cols.size()]);
			this.values = new Object[this.columnNames.length];
			try {
				DatasetUtils.writeHeader(output, this.columnNames);
			} catch(IOException e) {
				throw new ServiceException(e);
			}
		}

		protected void checkCommit() throws ServiceException {
			try {
				if(++batchCnt >= maxBatchSize && !conn.getAutoCommit()) {
					batchCnt = 0;
					conn.commit();
				}
			} catch(SQLException e) {
				throw new ServiceException(e);
			}
		}
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public short getInstance() {
		return instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}
}
