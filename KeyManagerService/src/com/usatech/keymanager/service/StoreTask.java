package com.usatech.keymanager.service;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.mq.app.AllOrNotOneInstanceWorkQueue;
import simple.mq.app.MultiInstanceWorkQueue;
import simple.text.StringUtils;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class StoreTask extends UpdateAccountTask {
	protected int cardRetentionDays = 90;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String previousCardId = step.getAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, String.class, false);
			long cardId = 0;
			if(shouldStoreCardData(step)) {
				int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
				Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
				byte[][] unencrypted;
				if(cnt == 0) {
					byte[] tmp = getUnencryptedData(step, charset, 0);
					unencrypted = (tmp == null ? null : new byte[][] { tmp });
				} else {
					unencrypted = new byte[cnt][];
					boolean needsEncryption = false;
					for(int i = 0; i < cnt; i++) {
						unencrypted[i] = getUnencryptedData(step, charset, i + 1);
						if(unencrypted[i] != null)
							needsEncryption = true;
					}
					if(!needsEncryption)
						unencrypted = null;
				}
				if(unencrypted != null) {
					long expirationTime = step.getAttributeDefault(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, Long.class, 0L);
					if(expirationTime <= 0)
						expirationTime = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(cardRetentionDays);
					cardId = keyManagerEngine.storeBytes(unencrypted, expirationTime, KeyManagerEngine.INTERNAL_USER_NAME);
				}
			}
			if(previousCardId != null && (previousCardId = previousCardId.trim()).length() > 0) {
				if(cardId != 0) {
					String newCardId = new StringBuilder(previousCardId.length() + 11).append(previousCardId).append(',').append(instance).append(':').append(cardId).toString();
					step.setResultAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, newCardId);
				}
				// also broadcast account info to other keymgrs
				if(step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false) != null) {
					short previousInstance = (short) ConvertUtils.getIntSafely(StringUtils.substringBefore(previousCardId, ":"), 0);
					MessageChainStep broadcastStep;
					String baseQueueName = getUpdateAccountQueueKey();
					if(baseQueueName != null) {
						baseQueueName += '.';
						MessageChain mc = new MessageChainV11();
						broadcastStep = mc.addStep(baseQueueName);
					} else
						broadcastStep = null;
					if(processAccount(taskInfo, false, broadcastStep)) {
						if(broadcastStep != null) {
							for(short i : getOtherInstances()) {
								if(i != instance && i != previousInstance) {
									broadcastStep.setQueueKey(baseQueueName + i);
									MessageChainService.publish(broadcastStep.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping(), taskInfo.getCurrentQos());
								}
							}
						}
					}
				}
			} else {
				MessageChainStep[] nextSteps = step.getNextSteps(0);
				MessageChainStep backupStep = step.addStep(AllOrNotOneInstanceWorkQueue.getInstanceQueueName(MultiInstanceWorkQueue.getBaseQueueName(step.getQueueKey()), String.valueOf(instance)));
				for(Map.Entry<String, Object> entry : step.getAttributes().entrySet())
					backupStep.addLiteralAttribute(entry.getKey(), entry.getValue());
				for(Attribute a : new Attribute[] {AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, AuthorityAttrEnum.ATTR_INSTANCE, AuthorityAttrEnum.ATTR_CARD_KEY})
					step.swapReferences(a, backupStep, a);
				backupStep.setAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, new StringBuilder(10).append(instance).append(':').append(cardId).toString());
				processAccount(taskInfo, false, backupStep);
				for(Map.Entry<String, Object> entry : step.getResultAttributes().entrySet())
					backupStep.addLiteralAttribute(entry.getKey(), entry.getValue());				
				backupStep.setNextSteps(0, nextSteps);
				step.setNextSteps(0, backupStep);
			}
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	protected boolean shouldStoreCardData(MessageChainStep step) throws AttributeConversionException {
		final AuthResultCode authResult = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, false);
		if(authResult == null)
			return true;
		switch(authResult) {
			case APPROVED:
			case PARTIAL:
				return true;
			case FAILED:
			case DECLINED:
				boolean authHoldUsed = step.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, Boolean.class, Boolean.FALSE);
				return authHoldUsed;
			case AVS_MISMATCH:
			case CVV_MISMATCH:
			case CVV_AND_AVS_MISMATCH:
				return true;
			default:
				return false;
		}
	}

	public int getCardRetentionDays() {
		return cardRetentionDays;
	}

	public void setCardRetentionDays(int cardRetentionDays) {
		if(cardRetentionDays <= 0)
			throw new IllegalArgumentException("Value must be greater than zero");
		this.cardRetentionDays = cardRetentionDays;
	}
}
