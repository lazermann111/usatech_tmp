package com.usatech.keymanager.service;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.security.SecurityUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.keymanager.master.PersistentKey;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;

public class UpdateAccountTask extends AbstractAccountTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected short instance;
	protected String resolveAccountQueueKey;
	protected int tokenSize = 16;
	protected KeyManagerEngine keyManagerEngine;
	protected String updateAccountQueueKey;
	protected short[] otherInstances;
	
	protected boolean processAccount(MessageChainTaskInfo taskInfo, boolean tokenize, MessageChainStep broadcastStep) throws AttributeConversionException, SQLException, DataLayerException, GeneralSecurityException, ConvertException, ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		Long globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
		final byte[] accountCdHash = step.getAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, byte[].class, false);
		Short sourceInstance = step.getAttribute(AuthorityAttrEnum.ATTR_INSTANCE, Short.class, false);
		AuthResultCode authResult = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, false);
		Boolean forceTokenization = step.getAttribute(AuthorityAttrEnum.ATTR_FORCE_TOKENIZATION, Boolean.class, false);
		if (forceTokenization == null) {
			forceTokenization = false;
		}
		Integer trackCrc;
		Integer decryptedCnt;
		if(authResult != null && (authResult == AuthResultCode.APPROVED || authResult == AuthResultCode.PARTIAL)) {
			trackCrc = step.getAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, Integer.class, false);
			decryptedCnt = step.getAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, false);
		} else {
			trackCrc = null;
			decryptedCnt = null;
		}
		if (forceTokenization) {
			tokenize = true;
			if (decryptedCnt == null)
				decryptedCnt = step.getAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, false);
			if (trackCrc == null)
				trackCrc = step.getAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, Integer.class, false);
			authResult = AuthResultCode.APPROVED;
		}
		boolean broadcast = true;
		Map<String, Object> params = new HashMap<String, Object>();
		if(globalAccountId != null) {
			if(sourceInstance != null && accountCdHash != null) {
				params.put("globalAccountId", globalAccountId);
				params.put("encryptedAccountCd", encrypt(accountCdHash));
				if(trackCrc != null)
					params.put("encryptedCrc", encryptCrc(trackCrc));
				params.put("instance", sourceInstance);
				DataLayerMgr.executeUpdate("UPSERT_ACCOUNT_ID", params, true);
				Long oldGlobalAccountId = ConvertUtils.convert(Long.class, params.get("oldGlobalAccountId"));
				boolean changed = ConvertUtils.convert(Boolean.class, params.get("changed"));
				if(!changed) {
					Short oldInstance = ConvertUtils.convert(Short.class, params.get("oldInstance"));
					step.setResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
					step.setResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, sourceInstance);
					log.info("Ignoring account update from " + oldGlobalAccountId + " to global id " + globalAccountId + " because new source instance is " + sourceInstance);
					// tell orig to change
					if(oldGlobalAccountId != null && oldInstance != null) {
						if(getOtherInstances() != null && CollectionUtils.iterativeSearch(getOtherInstances(), sourceInstance) >= 0) {
							MessageChain mc = new MessageChainV11();
							MessageChainStep fixStep = mc.addStep(getUpdateAccountQueueKey() + '.' + sourceInstance);
							fixStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, accountCdHash);
							fixStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, oldGlobalAccountId);
							fixStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, oldInstance);
							MessageChainService.publish(fixStep.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping(), null);
						}
						globalAccountId = oldGlobalAccountId;
						sourceInstance = oldInstance;
					}
					broadcast = false; // not broadcast
				} else if(oldGlobalAccountId != null && oldGlobalAccountId.longValue() != globalAccountId.longValue()) {
					log.info("Updated account " + oldGlobalAccountId + " to new global id " + globalAccountId);
					// Must tell loader
					MessageChain mc = new MessageChainV11();
					MessageChainStep resolveStep = mc.addStep(getResolveAccountQueueKey());
					resolveStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
					resolveStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, sourceInstance);
					resolveStep.setAttribute(AuthorityAttrEnum.ATTR_OLD_GLOBAL_ACCOUNT_ID, oldGlobalAccountId);
					MessageChainService.publish(mc, taskInfo.getPublisher());
				} else {
					log.info("Inserted account " + globalAccountId + " from " + sourceInstance);
				}
			} else if(trackCrc != null) {
				params.put("globalAccountId", globalAccountId);
				params.put("encryptedCrc", encryptCrc(trackCrc));
				int r = DataLayerMgr.executeUpdate("UPDATE_TRACK_CRC", params, true);
				if(r > 0)
					log.info("Updated track crc for global id " + globalAccountId);
				else {
					log.info("Global id " + globalAccountId + " not found or track crc not different");
					step.setResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
					step.setResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, sourceInstance);
					broadcast = false;
				}
			} else {
				step.setResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, sourceInstance);
				broadcast = false;
			}
		} else if(accountCdHash != null) { // this will only happen at the first keymgr to process the message
			params.put("encryptedAccountCd", encrypt(accountCdHash));
			if(trackCrc != null)
				params.put("encryptedCrc", encryptCrc(trackCrc));
			params.put("instance", getInstance());
			DataLayerMgr.executeUpdate("GET_OR_CREATE_ACCOUNT_ID", params, true);
			globalAccountId = ConvertUtils.convert(Long.class, params.get("globalAccountId"));
			Short oldInstance = ConvertUtils.convert(Short.class, params.get("oldInstance"));
			boolean changed = ConvertUtils.convert(Boolean.class, params.get("changed"));
			if(!changed) {
				step.setResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, oldInstance);
				log.info("Found account " + globalAccountId);
				broadcast = false;
			} else if(oldInstance == null) {
				sourceInstance = instance;
				log.info("Created new account " + globalAccountId);
			} else {
				log.info("Found account " + globalAccountId);
				sourceInstance = oldInstance;
			}
		} else
			return false;
		if(decryptedCnt != null && decryptedCnt > 0) {
			final Long keyId;
			final byte[][] encryptedData;
			Long expirationTime;
			final byte[] encryptedToken;
			final byte[] token;
			if(tokenize) {
				token = new byte[getTokenSize()];
				SecurityUtils.getSecureRandom().nextBytes(token);
			} else {
				token = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, byte[].class, false);
			}
			if(token != null && token.length > 0) {
				encryptedToken = encrypt(token);
				expirationTime = step.getAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, Long.class, false);
				if(expirationTime == null) {
					String expDate = step.getIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.EXPIRATION_DATE.getStoreIndex(), String.class, false);
					if(!StringUtils.isBlank(expDate))
						expirationTime = InteractionUtils.expDateToMillis(expDate);
					else
						expirationTime = System.currentTimeMillis() + (365 * 24 * 60 * 60 * 1000L);
				}
				Charset charset = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, Charset.class, false);
				PersistentKey key = keyManagerEngine.getPersistentKey(expirationTime);
				byte[][] cardData = new byte[decryptedCnt][];
				encryptedData = new byte[decryptedCnt][];
				// get card data
				for(int i = 0; i < decryptedCnt; i++) {
					cardData[i] = getUnencryptedData(step, charset, i + 1);
					if(cardData[i] != null)
						encryptedData[i] = keyManagerEngine.encrypt(key, cardData[i]);
				}
				keyId = (long) key.getKid();
				String initiatingDeviceName = step.getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
				long deviceTranCd = step.getAttributeDefault(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, long.class, 0L);
				params.clear();
				params.put("globalAccountId", globalAccountId);
				params.put("keyId", keyId);
				params.put("encryptedToken", encryptedToken);
				params.put("encryptedData", encryptedData);
				params.put("expirationTime", expirationTime);
				params.put("initiatingDeviceName", initiatingDeviceName);
				params.put("deviceTranCd", deviceTranCd);
				DataLayerMgr.executeUpdate("UPSERT_TOKEN", params, true);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, "A:" + initiatingDeviceName + ':' + globalAccountId);
				if(ConvertUtils.getBoolean(params.get("result"))) {
					broadcast = true;
					if(broadcastStep != null) {
						broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, token);
						broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResult);
						broadcastStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, decryptedCnt);
						broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, expirationTime);
						broadcastStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, initiatingDeviceName);
						broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, deviceTranCd);
						for(int i = 0; i < cardData.length; i++)
							broadcastStep.setIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, i + 1, cardData[i]);
					}
					if(tokenize)
						step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, token);
				} else if(tokenize) {
					// Existing token already exists - get it and decrypt it and use it
					byte[] existingEncryptedToken = ConvertUtils.convertRequired(byte[].class, params.get("encryptedToken"));
					byte[] existingToken = decrypt(existingEncryptedToken);
					step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, existingToken);
				}
			}
		}
		if(broadcast) {
			updateBroadcast(broadcastStep, globalAccountId, accountCdHash, trackCrc, sourceInstance);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, sourceInstance);
			return true;
		}
		return false;
	}

	protected byte[] getUnencryptedData(MessageChainStep step, Charset charset, int index) throws AttributeConversionException {
		Object data;
		if(index == 0) {
			data = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA_OVERRIDE, Object.class, false);
			if(data == null)
				data = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, Object.class, false);
		} else {
			data = step.getIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA_OVERRIDE, index, Object.class, false);
			if(data == null)
				data = step.getIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, index, Object.class, false);
		}
		if(data == null)
			return null;
		if(data instanceof byte[])
			return (byte[]) data;
		if(data instanceof String)
			return charset == null ? ((String) data).getBytes() : ((String) data).getBytes(charset);
		if(data instanceof MaskedString)
			return charset == null ? ((MaskedString) data).getValue().getBytes() : ((MaskedString) data).getValue().getBytes(charset);
		try {
			if(charset == null)
				return ConvertUtils.convert(byte[].class, data);
			String sdata = ConvertUtils.convert(String.class, data);
			if(sdata == null)
				return null;
			return sdata.getBytes(charset);
		} catch(ConvertException e) {
			throw new AttributeConversionException(CommonAttrEnum.ATTR_DECRYPTED_DATA, e);
		}
	}

	protected void updateBroadcast(MessageChainStep broadcastStep, Long globalAccountId, byte[] accountCdHash, Integer trackCrc, Short sourceInstance) {
		if(globalAccountId != null && broadcastStep != null) {
			broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
			broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, trackCrc);
			broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, sourceInstance);
			if(accountCdHash != null)
				broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, accountCdHash);
		}
	}

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		try {
			processAccount(taskInfo, false, null);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	public short getInstance() {
		return instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}

	public String getResolveAccountQueueKey() {
		return resolveAccountQueueKey;
	}

	public void setResolveAccountQueueKey(String resolveAccountQueueKey) {
		this.resolveAccountQueueKey = resolveAccountQueueKey;
	}

	public int getTokenSize() {
		return tokenSize;
	}

	public void setTokenSize(int tokenSize) {
		this.tokenSize = tokenSize;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	public String getUpdateAccountQueueKey() {
		return updateAccountQueueKey;
	}

	public void setUpdateAccountQueueKey(String updateAccountQueueKey) {
		this.updateAccountQueueKey = updateAccountQueueKey;
	}

	public short[] getOtherInstances() {
		return otherInstances;
	}

	public void setOtherInstances(short[] otherInstances) {
		this.otherInstances = otherInstances;
	}
}
