package com.usatech.keymanager.service;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.apache.cayenne.access.DataContext;
import org.eclipse.jetty.jaas.callback.ObjectCallback;

import com.usatech.keymanager.auth.KeyManagerRole;
import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.util.CertUtil;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

public class KeyManagerLoginModule implements LoginModule {
	private static Log log = Log.getLog();

	private Subject subject;
	private CallbackHandler callbackHandler;

	// the authentication status
	private boolean loginSucceeded = false;
	private boolean commitSucceeded = false;

	private KeyManagerUser user;

	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
		log.debug("Login initializing");
		this.subject = subject;
		this.callbackHandler = callbackHandler;

		/*if(subject != null)
			log.debug("Initialized with subject (" + subject.getClass().getName() + "): " + subject);
		else
			log.debug("Initialized with null subject");*/

		DataContext.bindThreadDataContext(DataContext.createDataContext());
	}

	public boolean login() throws LoginException {
		if(callbackHandler == null)
			throw new LoginException("Error: No CallbackHandler(s) available!");
		log.debug("Login begun");

		NameCallback nameCallback = new NameCallback("certThumbprint");
		ObjectCallback passwordCallback = new ObjectCallback();

		Callback[] callbacks = new Callback[2];
		callbacks[0] = nameCallback;
		callbacks[1] = passwordCallback;

		String userInfo, certThumbprint, certBase64, subjectDN;
		X509Certificate cert = null;
		try {
			callbackHandler.handle(callbacks);
			userInfo = nameCallback.getName();
			cert = ConvertUtils.convertRequired(X509Certificate.class, passwordCallback.getObject());
			try {
				certThumbprint = CertUtil.getCertThumbprint(cert);
			} catch(Exception e) {
				throw new LoginException("Failed to extract thumbprint from certificate: " + e.getMessage());
			}
			try {
				certBase64 = CertUtil.getCertBase64(cert);
			} catch(Exception e) {
				throw new LoginException("Failed to extract thumbprint from certificate: " + e.getMessage());
			}
			subjectDN = cert.getSubjectDN().getName();
			passwordCallback.setObject(null);
		} catch(java.io.IOException ioe) {
			throw new LoginException(ioe.toString());
		} catch(UnsupportedCallbackException uce) {
			throw new LoginException("Error: " + uce.getCallback().toString() + " not available!");
		} catch(ConvertException e) {
			throw new LoginException(e.toString());
		}

		// simply getting this far means the cert was valid
		user = KeyManagerUser.getByCertThumbprint(certThumbprint);
		if(user == null) {
			log.info("Storing new KeyManagerUser: " + userInfo);

			// new user
			user = KeyManagerUser.newInstance();
			user.setSubjectName(subjectDN);
			user.setCertThumbprint(certThumbprint);
			user.setCertValidFrom(cert.getNotBefore().toString());
			user.setCertValidTo(cert.getNotAfter().toString());
			user.setCertSerialNumber(cert.getSerialNumber().toString(16));
			user.setCertBase64(certBase64);
			user.save();
		}

		log.debug("Adding default role ROLE_VALID_CERT");
		user.addToRoles(KeyManagerRole.getByName(KeyManagerRole.ROLE_VALID_CERT));
		user.setCertificate(cert);
		loginSucceeded = true;
		log.debug("Login complete");
		return true;
	}

	public boolean commit() throws LoginException {
		if(loginSucceeded == false)
			return false;

		List<KeyManagerRole> roles = user.getRoles();
		log.debug("user has " + roles.size() + " roles...");

		for(KeyManagerRole role : roles) {
			log.debug("role=" + role);
			if(!subject.getPrincipals().contains(role)) {
				subject.getPrincipals().add(role);
				log.debug("added role");
			}
		}

		if(!subject.getPrincipals().contains(user)) {
			log.debug("added user");
			subject.getPrincipals().add(user);
		}

		// in any case, clean out state
		user = null;

		commitSucceeded = true;

		return true;
	}

	public boolean abort() throws LoginException {
		if(loginSucceeded == false) {
			return false;
		} else if(loginSucceeded == true && commitSucceeded == false) {
			// login succeeded but overall authentication failed
			loginSucceeded = false;
			user = null;
		} else {
			// overall authentication succeeded and commit succeeded,
			// but someone else's commit failed
			logout();
		}
		return true;
	}

	public boolean logout() throws LoginException {
		subject.getPrincipals().remove(user);
		loginSucceeded = false;
		user = null;
		return true;
	}
}
