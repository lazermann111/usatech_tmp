package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.keymanager.apple.vas.data.VasData;
import com.usatech.keymanager.apple.vas.exception.InvalidCipherException;
import com.usatech.keymanager.master.KEKException;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.EntryType;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;
import simple.text.StringUtils;

public class RetrieveByTokenTask extends AbstractAccountTask {
	private static final Log log = Log.getLog();
	protected KeyManagerEngine keyManagerEngine;
	protected DUKPTDecryptTask dukptDecryptTask;
	protected LookupAccountTask lookupAccountTask;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		List<VasData> vasData = null;
		try {
			vasData = getVASData(step);
		} catch (Exception e) {
			log.error("Error getting VAS data", e);
		}
		List<Long> globalAccountIds = new ArrayList<>();
		List<byte[]> tokens = new ArrayList<>();
		EntryType entryType = null;
		try {
			entryType = step.getAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, false);
		} catch (AttributeConversionException e) {
			log.warn("Error getting entryType", e);
		}
		int accountCount = 1;
		if (vasData == null || vasData.size() < 1) {
			try {
				globalAccountIds.add(step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, true));
				tokens.add(step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, byte[].class, true));
			} catch (AttributeConversionException e) {
				throw new RetrySpecifiedServiceException("Could not get attribute", e, WorkRetryType.NO_RETRY);
			}
		} else {
			accountCount = vasData.size();
		    for (int i = 0; i < accountCount; i++) {
		    	long cardId = vasData.get(i).getCardId();
		    	byte[] token = vasData.get(i).getToken();
		    	String idxStr = i == 0 ? "" : ('.' + String.valueOf(i));
		    	if (vasData.get(i).isDecrypted() && cardId > 0) {
		    		globalAccountIds.add(cardId);
		    		tokens.add(token);
		    		String accountData = null;
		    		try {
						accountData = KeyManagerServiceUtil.getAccountByToken(keyManagerEngine, step, i, cardId, encrypt(token), entryType);
					} catch (GeneralSecurityException e) {
						log.warn("Error getting account by token", e);
					}
		    		if (!StringUtils.isBlank(accountData)) {
						step.setResultAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + idxStr, accountData);
						taskInfo.setNextQos(taskInfo.getCurrentQos());
						return 0;
					}
		    	} else {
		    		accountCount--;
		    		step.setResultAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + idxStr, StringUtils.toHex(vasData.get(i).getEncryptedData()));
		    	}
		    }		    

	    	int encryptedDataCount = 0;
			try {
				encryptedDataCount = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
			} catch (AttributeConversionException e) {}
			if (encryptedDataCount > 0) {
				step.setResultAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, encryptedDataCount);
				return dukptDecryptTask.process(taskInfo);
			}
			
			int lookupDataCount = 0;
			try {
				lookupDataCount = step.getAttributeDefault(CommonAttrEnum.ATTR_LOOKUP_DATA_COUNT, Integer.class, 0);
			} catch (AttributeConversionException e) {}
			if (lookupDataCount > 0) {
				step.setResultAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, lookupDataCount);
				return lookupAccountTask.process(taskInfo);
			}
			
			if (accountCount > 0)
		    	step.setResultAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, accountCount);
		}		
		try {			
		    for (int i = 0; i < accountCount; i++) {
		      KeyManagerServiceUtil.getAccountByToken(keyManagerEngine, step, i, globalAccountIds.get(i), encrypt(tokens.get(i)), entryType);
		    }
		} catch(GeneralSecurityException e) {
			log.warn("Error getting account by token", e);
		}
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}

	public KeyManagerEngine getKeyManagerEngine() {
		return keyManagerEngine;
	}

	public void setKeyManagerEngine(KeyManagerEngine keyManagerEngine) {
		this.keyManagerEngine = keyManagerEngine;
	}

	protected List<VasData> getVASData(MessageChainStep step) throws AttributeConversionException, InvalidCipherException, KEKException {
		List<VasData> rv = null;
		int accountCount = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_VAS_DATA_COUNT, Integer.class, 0);
		if (accountCount > 0)
			 rv = new ArrayList<>();
	    for (int i = 0; i < accountCount; i++) {
	        String idxStr = i == 0 ? "" : ('.' + String.valueOf(i));
	        byte[] encryptedVasBytes = step.getAttribute(AuthorityAttrEnum.ATTR_VAS_ENCRYPTED_DATA.getValue() + idxStr, byte[].class, false);
	        byte[] merchantId = step.getAttribute(AuthorityAttrEnum.ATTR_VAS_PASS_MERCHANT_ID.getValue() + idxStr, byte[].class, false);
	        if (encryptedVasBytes == null || merchantId == null)
	            continue;
	        VasData vd = KeyManagerServiceUtil.decryptVas(keyManagerEngine, encryptedVasBytes, merchantId);
	        if (vd.isDecrypted()) {
	        	step.setResultAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER.getValue() + idxStr, vd.getPassIdentifier());
		        step.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue() + idxStr, vd.getCardId());
		        step.setAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue() + idxStr, vd.getToken());
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue() + idxStr, vd.getCardId());
		        step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue() + idxStr, vd.getToken());
	        }
	        rv.add(vd);
	    }
		return rv;
	}

	public DUKPTDecryptTask getDukptDecryptTask() {
		return dukptDecryptTask;
	}

	public void setDukptDecryptTask(DUKPTDecryptTask dukptDecryptTask) {
		this.dukptDecryptTask = dukptDecryptTask;
	}

	public LookupAccountTask getLookupAccountTask() {
		return lookupAccountTask;
	}

	public void setLookupAccountTask(LookupAccountTask lookupAccountTask) {
		this.lookupAccountTask = lookupAccountTask;
	}
}
