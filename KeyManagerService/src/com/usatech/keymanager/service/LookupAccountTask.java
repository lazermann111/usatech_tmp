package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Collections;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;

public class LookupAccountTask extends AbstractAccountTask implements AccountProcessor {
	private static final Log log = Log.getLog();

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		int cnt;
		try {
			cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_LOOKUP_DATA_COUNT, Integer.class, 0);
			if (cnt == 0)
				cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);			
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not get attribute", e, WorkRetryType.NO_RETRY);
		}
		if(cnt == 0)
			processIndex(taskInfo, 0);
		else
			for(int i = 1; i <= cnt; i++)
				processIndex(taskInfo, i);
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}

	public void processIndex(MessageChainTaskInfo taskInfo, String acctData, int index) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			// parse, then hash acctData
			byte[] hashedCard = hash(MessageResponseUtils.getCardNumber(acctData));
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, index, hashedCard);
			int calcCrc = InteractionUtils.getCRC16(acctData);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, index, calcCrc);
			byte[] encryptedAccountCd = encrypt(hashedCard);
			Results results = DataLayerMgr.executeQuery("GET_ACCOUNT", Collections.singletonMap("encryptedAccountCd", (Object) encryptedAccountCd));
			if(results.next()) {
				long globalAccountId = results.getValue("globalAccountId", Long.class);
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, index, globalAccountId);
				int instance = results.getValue("instance", Integer.class);
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, index, instance);
				byte[] encryptedCrc = results.getValue("encryptedCrc", byte[].class);
				if(encryptedCrc != null) {
					int storedCrc = decryptCrc(encryptedCrc);
					boolean crcMatches = (storedCrc == calcCrc);
					step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, index, crcMatches);
					if(log.isInfoEnabled())
						log.info("Found global account id: " + globalAccountId + "; CRC of data " + (crcMatches ? "matches" : "does NOT match"));
				} else if(log.isInfoEnabled())
					log.info("Found global account id: " + globalAccountId + "; no CRC is stored");
			}
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Could not lookup account id", e, WorkRetryType.BLOCKING_RETRY);
		} catch(ConvertException e) {
			log.warn("Could not convert results to lookup account id; skipping...", e);
		} catch(NoSuchAlgorithmException e) {
			log.warn("Could not hash card to lookup account id; skipping...", e);
		} catch(GeneralSecurityException e) {
			log.warn("Could not encrypt card or decrypt crc to lookup account id; skipping...", e);
		}
	}

	protected void processIndex(MessageChainTaskInfo taskInfo, int index) throws ServiceException {
		try {
			String acctData = taskInfo.getStep().getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, index, String.class, false);
			if(!StringUtils.isBlank(acctData)) {
				processIndex(taskInfo, acctData, index);
			}
		} catch(AttributeConversionException e) {
			log.warn("Could not convert attributes to lookup account id; skipping...", e);
		}
	}
}
