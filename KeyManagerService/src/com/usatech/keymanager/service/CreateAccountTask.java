package com.usatech.keymanager.service;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;

public class CreateAccountTask extends AbstractAccountTask implements AccountProcessor {
	private static final Log log = Log.getLog();
	protected short instance;
	protected String updateAccountQueueKey;
	protected short[] otherInstances;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		int cnt;
		try {
			cnt = taskInfo.getStep().getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not get attribute", e, WorkRetryType.NO_RETRY);
		}
		if(cnt == 0)
			processIndex(taskInfo, 0);
		else
			for(int i = 1; i <= cnt; i++)
				processIndex(taskInfo, i);
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		return 0;
	}

	protected void processIndex(MessageChainTaskInfo taskInfo, int index) throws ServiceException {
		String acctData;
		try {
			acctData = taskInfo.getStep().getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, index, String.class, false);
		} catch(AttributeConversionException e) {
			log.warn("Could not convert attributes to lookup account id; skipping...", e);
			return;
		}
		if(StringUtils.isBlank(acctData))
			return;
		processIndex(taskInfo, acctData, index);
	}

	public void processIndex(MessageChainTaskInfo taskInfo, String acctData, int index) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			// parse, then hash acctData
			byte[] hashedCard = hash(MessageResponseUtils.getCardNumber(acctData));
			byte[] encryptedAccountCd = encrypt(hashedCard);
			Map<String, Object> params = new HashMap<String, Object>(3);
			params.put("encryptedAccountCd", encryptedAccountCd);
			short instance = getInstance();
			params.put("instance", instance);

			DataLayerMgr.executeCall("GET_OR_CREATE_ACCOUNT_ID", params, true);
			long globalAccountId = ConvertUtils.convertRequired(Long.class, params.get("globalAccountId"));
			Short oldInstance = ConvertUtils.convert(Short.class, params.get("oldInstance"));
			boolean changed = ConvertUtils.getBoolean(params.get("changed"));
			int calcCrc = InteractionUtils.getCRC16(acctData);
			if(changed) {
				log.info("Created new global account id: " + globalAccountId + "; sending to other instances");
				// update others
				String baseQueueName = getUpdateAccountQueueKey();
				baseQueueName += '.';
				MessageChain mc = new MessageChainV11();
				MessageChainStep broadcastStep = mc.addStep(baseQueueName);
				broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, hashedCard);
				broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
				broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, oldInstance == null ? instance : oldInstance);
				for(short i : getOtherInstances()) {
					if(i != instance) {
						broadcastStep.setQueueKey(baseQueueName + i);
						MessageChainService.publish(broadcastStep.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping(), null);
					}
				}
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.created");
			} else {
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.found");
				byte[] encryptedCrc = ConvertUtils.convert(byte[].class, params.get("oldEncryptedCrc"));
				if(encryptedCrc != null) {
					int storedCrc = decryptCrc(encryptedCrc);
					boolean crcMatches = (storedCrc == calcCrc);
					step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, index, crcMatches);
					if(log.isInfoEnabled())
						log.info("Found global account id: " + globalAccountId + "; CRC of data " + (crcMatches ? "matches" : "does NOT match"));
				} else if(log.isInfoEnabled())
					log.info("Found global account id: " + globalAccountId + "; no CRC is stored");
			}
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, index, globalAccountId);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_INSTANCE, index, oldInstance == null ? instance : oldInstance);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, index, calcCrc);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, 1);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Could not get or create account id", e, WorkRetryType.BLOCKING_RETRY);
		} catch(ConvertException e) {
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.error");
			log.warn("Could not convert results to get or create account id; skipping...", e);
		} catch(NoSuchAlgorithmException e) {
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.error");
			log.warn("Could not hash card to get or create account id; skipping...", e);
		} catch(GeneralSecurityException e) {
			step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, "client.message.get-card-id.error");
			log.warn("Could not encrypt card or decrypt crc to get or create account id; skipping...", e);
		}
	}

	public short getInstance() {
		return instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}

	public String getUpdateAccountQueueKey() {
		return updateAccountQueueKey;
	}

	public void setUpdateAccountQueueKey(String updateAccountQueueKey) {
		this.updateAccountQueueKey = updateAccountQueueKey;
	}

	public short[] getOtherInstances() {
		return otherInstances;
	}

	public void setOtherInstances(short[] otherInstances) {
		this.otherInstances = otherInstances;
	}
}
