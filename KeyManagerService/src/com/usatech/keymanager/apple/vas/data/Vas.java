package com.usatech.keymanager.apple.vas.data;

import org.bouncycastle.util.Arrays;

import java.io.Serializable;

/**
 * Structure to represent encrypted message payload
 */
public class Vas implements Serializable {
    private static final long serialVersionUID = -2033611109956151168L;

    private byte[] first4BytesOfPKSha256;
    private byte[] theirPublicKey;
    private byte[] encTimeInSecondsSince1Jan2001;
    private byte[] encMessage;
    private byte[] aesGcmAuthTag;

    public Vas() {
    }

    public byte[] getFirst4BytesOfPKSha256() {
        return first4BytesOfPKSha256;
    }

    public void setFirst4BytesOfPKSha256(byte[] first4BytesOfPKSha256) {
        this.first4BytesOfPKSha256 = first4BytesOfPKSha256;
    }

    public byte[] getTheirPublicKey() {
        return theirPublicKey;
    }

    public void setTheirPublicKey(byte[] theirPublicKey) {
        this.theirPublicKey = theirPublicKey;
    }

    public byte[] getEncTimeInSecondsSince1Jan2001() {
        return encTimeInSecondsSince1Jan2001;
    }

    public void setEncTimeInSecondsSince1Jan2001(byte[] encTimeInSecondsSince1Jan2001) {
        this.encTimeInSecondsSince1Jan2001 = encTimeInSecondsSince1Jan2001;
    }

    public byte[] getEncMessage() {
        return encMessage;
    }

    public void setEncMessage(byte[] encMessage) {
        this.encMessage = encMessage;
    }

    public byte[] getAesGcmAuthTag() {
        return aesGcmAuthTag;
    }

    public void setAesGcmAuthTag(byte[] aesGcmAuthTag) {
        this.aesGcmAuthTag = aesGcmAuthTag;
    }

    public byte[] getMessageWithTag() {
        byte[] msgWithTag = new byte[encMessage.length + aesGcmAuthTag.length];
        System.arraycopy(encMessage,0, msgWithTag,0, encMessage.length);
        System.arraycopy(aesGcmAuthTag,0, msgWithTag, encMessage.length, aesGcmAuthTag.length);
        return msgWithTag;
    }

    public byte[] getTimeWithTag() {
        byte[] msgWithTag = new byte[encTimeInSecondsSince1Jan2001.length + aesGcmAuthTag.length];
        System.arraycopy(encTimeInSecondsSince1Jan2001,0, msgWithTag,0, encTimeInSecondsSince1Jan2001.length);
        System.arraycopy(aesGcmAuthTag,0, msgWithTag, encTimeInSecondsSince1Jan2001.length, aesGcmAuthTag.length);
        return msgWithTag;
    }

    public byte[] getTimeMessageWithTag() {
        return Arrays.concatenate(encTimeInSecondsSince1Jan2001, encMessage, aesGcmAuthTag);
    }
}
