package com.usatech.keymanager.apple.vas.data;

import java.nio.ByteBuffer;

/**
 * Decrypted VAS data
 */
public class DecryptedVas {
    private final Integer timeInSecondsSince1Jan2001;
    private final byte[] data;

    public DecryptedVas(byte[] wholeMessage) {
        byte[] timeArr = new byte[4];
        System.arraycopy(wholeMessage, 0, timeArr, 0, 4);
        ByteBuffer wrapped = ByteBuffer.wrap(timeArr); // big-endian by default
        timeInSecondsSince1Jan2001 = wrapped.getInt();
        byte[] dataArr = new byte[wholeMessage.length - 4];
        System.arraycopy(wholeMessage, 4, dataArr, 0, wholeMessage.length - 4);
        data = dataArr;
    }

    public Integer getTimeInSecondsSince1Jan2001() {
        return timeInSecondsSince1Jan2001;
    }

    public byte[] getData() {
        return data;
    }
}
