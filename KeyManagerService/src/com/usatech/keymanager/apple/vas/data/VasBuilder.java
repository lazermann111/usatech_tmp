package com.usatech.keymanager.apple.vas.data;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;

/**
 * VAS object builder
 */
public class VasBuilder {
    private static final int PKEY_START_IDX = 4;
    private static final int SHA_START_IDX = 0;
    private static final int TIME_START_IDX = 36;
    private static final int MSG_START_IDX = 40;
    private static final int AUTH_TAG_LEN = 16;

    private byte[] vasBytes;

    public VasBuilder() {
    }

    public VasBuilder setVasBytes(@NotNull byte[] vasBytes) {
        this.vasBytes = vasBytes;
        return this;
    }

    public Vas build() {
        Vas vas = new Vas();
        vas.setFirst4BytesOfPKSha256(Arrays.copyOfRange(vasBytes, SHA_START_IDX, PKEY_START_IDX));
        vas.setTheirPublicKey(Arrays.copyOfRange(vasBytes, PKEY_START_IDX, TIME_START_IDX));
        vas.setEncTimeInSecondsSince1Jan2001(Arrays.copyOfRange(vasBytes, TIME_START_IDX, MSG_START_IDX));
        vas.setEncMessage(Arrays.copyOfRange(vasBytes, MSG_START_IDX, vasBytes.length - AUTH_TAG_LEN));
        vas.setAesGcmAuthTag(Arrays.copyOfRange(vasBytes, vasBytes.length - AUTH_TAG_LEN, vasBytes.length));
        return vas;
    }
}
