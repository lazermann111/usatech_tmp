package com.usatech.keymanager.apple.vas.data;

/**
 * Data of VAS
 */
public class VasData {
    private long cardId;
    private byte[] token;
    private String passIdentifier;
    private byte[] encryptedData;
    private boolean decrypted;

    public VasData(long cardId, byte[] token, String passIdentifier) {
        this.cardId = cardId;
        this.token = token;
        this.passIdentifier = passIdentifier;
        this.decrypted = true;
    }

    public VasData(String passIdentifier) {
        this.passIdentifier = passIdentifier;
        this.decrypted = true;
    }
    
    public VasData(byte[] encryptedData) {
    	this.encryptedData = encryptedData;
    	this.decrypted = false;
    }
    
    public long getCardId() {
        return cardId;
    }

    public byte[] getToken() {
        return token;
    }

	public String getPassIdentifier() {
		return passIdentifier;
	}

	public byte[] getEncryptedData() {
		return encryptedData;
	}

	public boolean isDecrypted() {
		return decrypted;
	}
}
