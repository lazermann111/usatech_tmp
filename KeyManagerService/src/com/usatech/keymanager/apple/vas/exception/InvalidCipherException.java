package com.usatech.keymanager.apple.vas.exception;

/**
 * Exception to indicate unrecoverable error while trying to decode encrypted message
 */
public class InvalidCipherException extends Exception {
    public InvalidCipherException() {
    }

    public InvalidCipherException(String message) {
        super(message);
    }

    public InvalidCipherException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCipherException(Throwable cause) {
        super(cause);
    }

    public InvalidCipherException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
