package com.usatech.keymanager.apple.vas.patch;

import org.bouncycastle.jcajce.provider.digest.SHA256;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import java.nio.ByteBuffer;
import java.security.DigestException;
import java.security.MessageDigest;

/**
 * X9.63 with SHA256 as digest.
 * Actually this code is copy/paste from X963 class PATCHED according this doc:
 * https://www.bsi.bund.de/SharedDocs/Downloads/EN/BSI/Publications/TechGuidelines/TR03111/BSI-TR-03111_pdf.pdf?__blob=publicationFile
 * Algorithm on page 27
 */
public class X963Sha256Patched {
    private MessageDigest md = new SHA256.Digest();
    private byte[] z;
    private byte[] sharedInfo;

    public X963Sha256Patched() {
    }

    /**
     * Init KDF
     *
     * @param secret    Shared secret bytes
     * @param sharedInfo    Additional shared info
     */
    public void init(byte[] secret, byte[] sharedInfo) {
        if(secret == null) {
            throw new NullPointerException("null");
        } else {
            this.z = ByteUtils.clone(secret);
            if(sharedInfo != null) {
                this.sharedInfo = ByteUtils.clone(sharedInfo);
            }
        }
    }

    /**
     * Derive X9.63 KDF key
     *
     * @param k    Key len in BITs
     * @return     Derived key as bytes
     */
    public byte[] deriveKey(int k) {
        int el = this.md.getDigestLength() * Byte.SIZE;
        int j = k / el;
        byte[] result = new byte[k / Byte.SIZE];
        Integer counter = 1;
        try {
            for(int i = 1; i <= j - 1; i++) {
                this.md.update(this.z);
                this.md.update(ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(counter).array());
                this.md.update(this.sharedInfo);
                this.md.digest(result, (i - 1) * (el / Byte.SIZE), el / Byte.SIZE);
                counter++;
            }
        } catch (DigestException var8) {
            throw new RuntimeException("internal error", var8);
        }
        int l = k - (el * j);
        this.md.update(this.z);
        this.md.update(ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(counter).array());
        this.md.update(this.sharedInfo);
        try {
            this.md.digest(result, (l / Byte.SIZE) * (el / Byte.SIZE), el / Byte.SIZE);
        } catch (DigestException e) {
            throw new RuntimeException("internal error", e);
        }
        return result;
    }
}
