package com.usatech.keymanager.apple.vas.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.bouncycastle.util.Arrays;

import com.sun.istack.internal.NotNull;

import com.usatech.keymanager.apple.vas.data.DecryptedVas;
import com.usatech.keymanager.apple.vas.data.Vas;
import com.usatech.keymanager.apple.vas.exception.InvalidCipherException;
import com.usatech.keymanager.apple.vas.util.ByteArrayUtils;
import com.usatech.keymanager.apple.vas.util.SecureHash;

import sun.security.ec.ECPublicKeyImpl;

/**
 * Class to decrypt VAS message payload
 */
public class VasDecryptor {
    protected static final String KDF_SHARED_INFO_1 = "ApplePay encrypted VAS data";
    protected static final String KDF_SHARED_INFO_2 = "id-aes256-GCM";
    public static final int KDF_SCHEME_2_SHARED_INFO_PREFIX = 0x0D;
    public static final int AES_KEY_LEN_BYTES = 32;
    public static final int ECDH_KEY_LEN_BYTES = 32;
    public static final long APPLE_TO_EPOCH_TIME_DIFF_MS = 978307200000L;
    public static final long MESSAGE_VALIDITY_TIME_MS = 30000;
    public static boolean validateMessageTime = true;

    @NotNull
    public static DecryptedVas decode(@NotNull Vas vas, @NotNull byte[] merchantId, @NotNull PrivateKey privateKey) throws InvalidCipherException {
        byte[] sharedSecretBytes;
        try {
            EcdhDecryptor ecdhDecryptor = new EcdhDecryptor();
            PublicKey theirPubKey = ecdhDecryptor.restorePublicKey(vas.getTheirPublicKey());
            sharedSecretBytes = ecdhDecryptor.buildECDHSecretBytes(privateKey, theirPubKey);
        } catch (Exception e) {
            throw new InvalidCipherException(e);
        }
        DecryptedVas decryptedVas;
        try {
            // Load as Scheme2 first. Most devices will use it
            decryptedVas = decodeAsScheme2(vas, sharedSecretBytes, merchantId);
        } catch (InvalidCipherException e) {
            // If Scheme2 won't work try Scheme1. Some old devices may use it.
            decryptedVas = decodeAsScheme1(vas, sharedSecretBytes, merchantId);
        }
        if (validateMessageTime)
        	validateMessageTime(decryptedVas);
        return decryptedVas;
    }

    public static DecryptedVas decodeAsScheme1(Vas vas, byte[] sharedSecretBytes, byte[] merchantId) throws InvalidCipherException {
        try {
            X963AesGcmKdf kdf = new X963AesGcmKdf(AES_KEY_LEN_BYTES);
            byte[] aesKey = kdf.deriveAes256Key(sharedSecretBytes, buildScheme1SharedInfo());
            AesDecryptor aesDecryptor = new AesDecryptor(aesKey);
            byte[] decodedBytes = aesDecryptor.decode(vas.getTimeMessageWithTag(), merchantId);
            return new DecryptedVas(decodedBytes);
        } catch (Exception e) {
            throw new InvalidCipherException(e);
        }
    }

    public static DecryptedVas decodeAsScheme2(Vas vas, byte[] sharedSecretBytes, byte[] merchantId) throws InvalidCipherException {
        try {
            X963AesGcmKdf kdf = new X963AesGcmKdf(AES_KEY_LEN_BYTES);
            byte[] aesKey = kdf.deriveAes256Key(sharedSecretBytes, buildScheme2SharedInfo(merchantId));
            AesDecryptor aesDecryptor = new AesDecryptor(aesKey);
            byte[] decodedBytes = aesDecryptor.decode(vas.getTimeMessageWithTag(), null);
            return new DecryptedVas(decodedBytes);
        } catch (Exception e) {
            throw new InvalidCipherException(e);
        }
    }

    protected static void validateMessageTime(DecryptedVas decryptedVas) throws InvalidCipherException {
        long curTimeMs = System.currentTimeMillis();
        long appleTimeSec = decryptedVas.getTimeInSecondsSince1Jan2001().longValue();
        long appleTimeMs = appleTimeSec * 1000L + APPLE_TO_EPOCH_TIME_DIFF_MS;
        long timeDiffMs = Math.abs(curTimeMs - appleTimeMs);
        
        if (timeDiffMs > MESSAGE_VALIDITY_TIME_MS) {
            throw new InvalidCipherException("Message is valid but obsolete. So drop it. Time diff in ms: " + timeDiffMs);
        }
    }

    protected static void validatePKSha256(@NotNull byte[] their4BytesOfOurPubSha256, @NotNull PublicKey ourPubKey) throws IllegalStateException, NoSuchAlgorithmException, InvalidKeyException {
    	ECPublicKeyImpl bcecPublicKey = (ECPublicKeyImpl) ourPubKey;
        byte[] x = new byte[ECDH_KEY_LEN_BYTES];
        System.arraycopy(bcecPublicKey.getEncodedPublicValue(), 1, x, 0, ECDH_KEY_LEN_BYTES);
        byte[] xCoordinateSha256 = SecureHash.getUnsaltedHash(x);
        for (int i = 0; i < their4BytesOfOurPubSha256.length; i++) {
            if (their4BytesOfOurPubSha256[i] != xCoordinateSha256[i]) {
                throw new IllegalStateException("Message has encrypted with unknown public key that does't match our pub key.");
            }
        }
    }

    @NotNull
    protected static byte[] buildScheme1SharedInfo() {
        return ByteArrayUtils.str2AscIIBytes(KDF_SHARED_INFO_1);
    }

    @NotNull
    protected static byte[] buildScheme2SharedInfo(@NotNull byte[] merchantId) {
        byte[] sharedTextBytes = Arrays.concatenate(ByteArrayUtils.str2AscIIBytes(KDF_SHARED_INFO_2),
                                                    ByteArrayUtils.str2AscIIBytes(KDF_SHARED_INFO_1),
                                                    merchantId);
        byte[] sharedInfoBytes = new byte[sharedTextBytes.length + 1];
        sharedInfoBytes[0] = KDF_SCHEME_2_SHARED_INFO_PREFIX; // prefix as in Apple doc mentioned
        System.arraycopy(sharedTextBytes, 0, sharedInfoBytes, 1, sharedTextBytes.length);
        return sharedInfoBytes;
    }

		public static boolean isValidateMessageTime() {
			return validateMessageTime;
		}

		public static void setValidateMessageTime(boolean validateMessageTime) {
			VasDecryptor.validateMessageTime = validateMessageTime;
		}
    
}
