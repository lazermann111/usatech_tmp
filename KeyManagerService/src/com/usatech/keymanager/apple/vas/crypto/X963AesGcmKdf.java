package com.usatech.keymanager.apple.vas.crypto;

import com.sun.istack.internal.NotNull;
import com.usatech.keymanager.apple.vas.patch.X963Sha256Patched;

/**
 * Generate strong secret from potentially weak secret data using KDF ANSI X9.63-2011
 */
public class X963AesGcmKdf {
    private final int keyLenBits;

    public X963AesGcmKdf(int keyLenBytes) {
        keyLenBits = keyLenBytes * Byte.SIZE;
    }

    @NotNull
    public byte[] deriveAes256Key(@NotNull byte[] secret, @NotNull byte[] sharedInfo) {
        X963Sha256Patched kdf = new X963Sha256Patched();
        kdf.init(secret, sharedInfo);
        return kdf.deriveKey(keyLenBits);
    }
}
