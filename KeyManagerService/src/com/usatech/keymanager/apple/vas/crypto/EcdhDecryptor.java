package com.usatech.keymanager.apple.vas.crypto;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

/**
 * ECDH cipher decryptor
 */
public class EcdhDecryptor {
    private static final String ECDH = "ECDH";
    private static final String CURVE_NAME = "prime256v1";
    private static final int PKEY_LEN_BYTES = 32;
    private static final int COMPRESSED_PKEY_POSITIVE_Y_FLAG = 0x02;
    private static final int COMPRESSED_PKEY_NEGATIVE_Y_FLAG = 0x03;

    public EcdhDecryptor() {
    }

    public byte[] buildECDHSecretBytes(PrivateKey privateKey, PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        KeyAgreement keyAgreement = buildKeyAgreement(privateKey, publicKey);
        return keyAgreement.generateSecret();
    }

    public SecretKey buildECDHSecretKey(PrivateKey privateKey, PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        KeyAgreement keyAgreement = buildKeyAgreement(privateKey, publicKey);
        return keyAgreement.generateSecret(ECDH);
    }

    public PublicKey restorePublicKey(byte[] encodedKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {
        ECParameterSpec spec = ECNamedCurveTable.getParameterSpec(CURVE_NAME);
        ECCurve curve = spec.getCurve();
        byte[] compressed = new byte[PKEY_LEN_BYTES + 1];
        compressed[0] = COMPRESSED_PKEY_POSITIVE_Y_FLAG;
        System.arraycopy(encodedKey, 0, compressed, 1, compressed.length - 1);
        ECPoint q = curve.decodePoint(compressed);
        ECPublicKeySpec pubKeySpec = new ECPublicKeySpec(q, spec);
        KeyFactory keyFactory = KeyFactory.getInstance(ECDH, BouncyCastleProvider.PROVIDER_NAME);
        return keyFactory.generatePublic(pubKeySpec);
    }

    private KeyAgreement buildKeyAgreement(PrivateKey privateKey, PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        KeyAgreement keyAgreement = KeyAgreement.getInstance(ECDH, BouncyCastleProvider.PROVIDER_NAME);
        keyAgreement.init(privateKey);
        keyAgreement.doPhase(publicKey, true);
        return keyAgreement;
    }
}
