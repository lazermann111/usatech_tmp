package com.usatech.keymanager.apple.vas.crypto;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * Class to decrypt AES cipher
 */
public class AesDecryptor {
    private static final String AES = "AES";
    private static final String AES_ALGORITHM = "AES/GCM/NoPadding";
    private static final int IV_LEN_BYTES = 16;

    private final byte[] key;

    public AesDecryptor(byte[] key) {
        this.key = key;
    }

    public byte[] decode(@NotNull byte[] encData, @Nullable byte[] aad) throws BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException {
        SecretKeySpec aesKey = new SecretKeySpec(key, AES);
        Cipher cipher = Cipher.getInstance(AES_ALGORITHM, BouncyCastleProvider.PROVIDER_NAME);
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(IV_LEN_BYTES * 8, new byte[IV_LEN_BYTES]);
        cipher.init(Cipher.DECRYPT_MODE, aesKey, gcmParameterSpec);
        if (aad != null) {
            cipher.updateAAD(aad);
        }
        return cipher.doFinal(encData);
    }
}
