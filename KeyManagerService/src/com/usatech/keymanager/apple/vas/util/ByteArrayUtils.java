package com.usatech.keymanager.apple.vas.util;

import com.sun.istack.internal.NotNull;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;

public class ByteArrayUtils {
    public static int hexValue(char ch) throws IllegalArgumentException {
        switch (ch) {
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case 'A':
            case 'a':
                return 10;
            case 'B':
            case 'b':
                return 11;
            case 'C':
            case 'c':
                return 12;
            case 'D':
            case 'd':
                return 13;
            case 'E':
            case 'e':
                return 14;
            case 'F':
            case 'f':
                return 15;
            default:
                throw new IllegalArgumentException("Character '" + ch + "' (" + (int) ch + ") is not a hex character");
        }
    }

    public static byte hexValue(char ch1, char ch2) throws IllegalArgumentException {
        return (byte) (16 * hexValue(ch1) + hexValue(ch2));
    }

    public static byte[] fromHex(String hex) throws IllegalArgumentException {
        if (hex == null)
            return null;
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++)
            bytes[i] = hexValue(hex.charAt(i * 2), hex.charAt(i * 2 + 1));
        return bytes;
    }

    /**
     * Correctly converts 16-bit Unicode String object to 8-bit US-ASCII bytes
     *
     * @param str   16-bit per char Unicode String object
     * @return      byte array with ASCII representation of {@code str}
     */
    public static byte[] str2AscIIBytes(@NotNull String str) {
        try {
            CharsetEncoder encoder = StandardCharsets.US_ASCII.newEncoder();
            ByteBuffer byteBuffer = encoder.encode(CharBuffer.wrap(str.toCharArray()));
            return byteBuffer.array();
        } catch (CharacterCodingException e) {
            throw new RuntimeException("Failed to encode string to bytes in ASC-II, str = " + str, e);
        }
    }
}
