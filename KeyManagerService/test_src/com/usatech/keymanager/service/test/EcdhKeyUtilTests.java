package com.usatech.keymanager.service.test;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.math.ec.ECMultiplier;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.WNafUtil;
import org.junit.Test;

import simple.io.Base64;
import simple.text.StringUtils;

import com.usatech.keymanager.apple.vas.crypto.EcdhDecryptor;
import com.usatech.keymanager.apple.vas.crypto.VasDecryptor;
import com.usatech.keymanager.apple.vas.data.DecryptedVas;
import com.usatech.keymanager.apple.vas.data.Vas;
import com.usatech.keymanager.apple.vas.data.VasBuilder;
import com.usatech.keymanager.apple.vas.exception.InvalidCipherException;
import com.usatech.keymanager.apple.vas.util.ByteArrayUtils;
import com.usatech.keymanager.apple.vas.util.SecureHash;
import com.usatech.keymanager.util.KeyManagerUtil;

public class EcdhKeyUtilTests {
	
	static {
		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null)
      Security.addProvider(new BouncyCastleProvider());
	}
	
	public static byte[] iv = new SecureRandom().generateSeed(16);

	private static final String PASS_TYPE_ID = "pass.com.usatech.more.pass2";
	private static final String CORRECT_PLAIN_MESSAGE = "126695|7DA79F079E90FF04F79BC54C992B714A";
	private static final String ENCODED_DATA_AS_HEX_STRING = "9CF1F02DA14B0717417FBCA3FB1D8457D9172C15D39C4815274443140382A06B91907DBEEC1B34BBCCDE7C8A54C0766C2C08B1DE12DE76A8649452D9281DA4475A5C98CB62F98D41171B4CD31E89DEA610323052034DA51B629DCC62B5DE0B";
	private static final BigInteger TEST_PRIVATE_KEY_NUMBER = new BigInteger("114923324624668162018415549190138384633962803764000444854494142938157794088282");  
  
  @Test
  public void testAVASKeyPair() throws Exception {
  	ProviderConfiguration configuration = BouncyCastleProvider.CONFIGURATION;  	
  	SecureRandom random = new SecureRandom();   	
  	X9ECParameters x9EcParams = SECNamedCurves.getByName(KeyManagerUtil.EC_PRIME256_CURVE_NAME);
    ECDomainParameters params = new ECDomainParameters(x9EcParams.getCurve(), x9EcParams.getG(), x9EcParams.getN(), x9EcParams.getH(), x9EcParams.getSeed());
  	
  	BigInteger n = params.getN();
  	int nBitLength = n.bitLength();
    int minWeight = nBitLength >>> 2;
  	
  	BigInteger r = null;
  	for (int i = 0; i < 3; i++) {  	
  		BigInteger d;
	    for (;;)
	    {
	        d = new BigInteger(nBitLength, random);	
	        if (d.compareTo(BigInteger.valueOf(2)) < 0  || (d.compareTo(n) >= 0))
	            continue;
	        if (WNafUtil.getNafWeight(d) < minWeight)
	            continue;
	        break;
	    }
	    System.out.println("Private Key Component " + i + ": " + d);
	    if (r == null)
	    	r = d;	    	
	    else
	    	r = r.xor(d);
  	}
  	System.out.println("Private Key Big Integer: " + r);
  	
  	ECMultiplier multiplier = new FixedPointCombMultiplier();  	
  	ECPoint Q = multiplier.multiply(params.getG(), r);
  	AsymmetricCipherKeyPair pair = new AsymmetricCipherKeyPair(new ECPublicKeyParameters(Q, params), new ECPrivateKeyParameters(r, params));

  	ECPublicKeyParameters pub = (ECPublicKeyParameters)pair.getPublic();
    ECPrivateKeyParameters priv = (ECPrivateKeyParameters)pair.getPrivate();
    
    ECParameterSpec ecParamSpec = ECNamedCurveTable.getParameterSpec(KeyManagerUtil.EC_PRIME256_CURVE_NAME);
    BCECPublicKey pubKey = new BCECPublicKey(KeyManagerUtil.EC_ALGORITHM, pub, ecParamSpec, configuration);
    pubKey.setPointFormat("COMPRESSED");
    KeyPair keyPair = new KeyPair(pubKey, new BCECPrivateKey(KeyManagerUtil.EC_ALGORITHM, priv, pubKey, ecParamSpec, configuration));
    
    byte[] compressedPubKey = Q.getEncoded(true);
    
    byte[] x = new byte[KeyManagerUtil.ECDH_KEY_LEN_BYTES];
    System.arraycopy(compressedPubKey, 1, x, 0, KeyManagerUtil.ECDH_KEY_LEN_BYTES);
    byte[] xCoordinateSha256 = SecureHash.getUnsaltedHash(x);
    
    System.out.println("Private Key Hex:                          " + StringUtils.toHex(keyPair.getPrivate().getEncoded()));
    System.out.println("Public Key Hex:                           " + StringUtils.toHex(keyPair.getPublic().getEncoded()));
    System.out.println("Public Key Base64:                        " + Base64.encodeBytes(keyPair.getPublic().getEncoded(), true));
    System.out.println("pubKey Base64:                            " + Base64.encodeBytes(pubKey.getEncoded(), true));
    System.out.println("Compressed Public Key Hex:                " +  StringUtils.toHex(compressedPubKey));
    System.out.println("Compressed Public Key with Header Hex:    " + "3039301306072A8648CE3D020106082A8648CE3D030107032200" + StringUtils.toHex(compressedPubKey));
    System.out.println("Compressed Public Key with Header Base64: " + Base64.encodeBytes(ByteArrayUtils.fromHex("3039301306072A8648CE3D020106082A8648CE3D030107032200" + StringUtils.toHex(compressedPubKey)), true));
    System.out.println("Public Keys Match:                        " + StringUtils.toHex(keyPair.getPublic().getEncoded()).equalsIgnoreCase("3039301306072A8648CE3D020106082A8648CE3D030107032200" + StringUtils.toHex(compressedPubKey)));
    System.out.println("xCoordinate:                              " + StringUtils.toHex(x));
    System.out.println("xCoordinateSha256:                        " + StringUtils.toHex(xCoordinateSha256));
  }  
  
  @Test 
  public void testAVASDecryption() throws Exception {
  	byte[] encryptedVas = ByteArrayUtils.fromHex("9CF1F02DA14B0717417FBCA3FB1D8457D9172C15D39C4815274443140382A06B91907DBEEC1B34BBCCDE7C8A54C0766C2C08B1DE12DE76A8649452D9281DA4475A5C98CB62F98D41171B4CD31E89DEA610323052034DA51B629DCC62B5DE0B");
  	byte[] merchantId = ByteArrayUtils.fromHex("B6D9438CDCB8F87C4AB036402FB65D7F8DC95DC871DF5472E9BE523D8A1A1F5B");
  	BigInteger r = new BigInteger("114923324624668162018415549190138384633962803764000444854494142938157794088282");
  	String xCoordinateSha256Hex = "9CF1F02D2CF37ED205C96EBA82E25B7324B38E61FAC876839724CCE14024641F";  	
    
    ECParameterSpec params = ECNamedCurveTable.getParameterSpec(KeyManagerUtil.EC_PRIME256_CURVE_NAME);
		ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(r, params);
		KeyFactory kf = KeyFactory.getInstance(KeyManagerUtil.ECDH, BouncyCastleProvider.PROVIDER_NAME);
		PrivateKey privateKey = kf.generatePrivate(privateKeySpec);		
    	
		Vas vas = new VasBuilder().setVasBytes(encryptedVas).build();
    EcdhDecryptor ecdhDecryptor = new EcdhDecryptor();
    PublicKey theirPubKey = ecdhDecryptor.restorePublicKey(vas.getTheirPublicKey());        
    byte[] sharedSecretBytes = ecdhDecryptor.buildECDHSecretBytes(privateKey, theirPubKey);
            
    DecryptedVas decryptedVas;
    try {
        // Load as Scheme2 first. Most devices will use it
        decryptedVas = VasDecryptor.decodeAsScheme2(vas, sharedSecretBytes, merchantId);
    } catch (InvalidCipherException e) {
        // If Scheme2 won't work try Scheme1. Some old devices may use it.
        decryptedVas = VasDecryptor.decodeAsScheme1(vas, sharedSecretBytes, merchantId);
    }

    String decodedMessage = new String(decryptedVas.getData(), Charset.forName("UTF-8"));
		
    String vasFirst4BytesOfPKSha256Hex = StringUtils.toHex(vas.getFirst4BytesOfPKSha256());
		System.out.println("our First 4 Bytes of PK Sha256 Hex   : " + xCoordinateSha256Hex.substring(0, 8));
		System.out.println("vas First 4 Bytes of PK Sha256 Hex   : " + vasFirst4BytesOfPKSha256Hex);
		System.out.println("First 4 bytes of PK SHA-256 Match    : " + vasFirst4BytesOfPKSha256Hex.equalsIgnoreCase(xCoordinateSha256Hex.substring(0, 8)));    
    System.out.println("Decrypted Message                    : " + decodedMessage);
  }

	@Test
	public void testAVASDecryption2() {
    try {
    	ECParameterSpec params = ECNamedCurveTable.getParameterSpec(KeyManagerUtil.EC_PRIME256_CURVE_NAME);
  		ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(TEST_PRIVATE_KEY_NUMBER, params);
  		KeyFactory kf = KeyFactory.getInstance(KeyManagerUtil.ECDH, BouncyCastleProvider.PROVIDER_NAME);
  		PrivateKey privateKey = kf.generatePrivate(privateKeySpec);

  		byte[] encryptedVASData = ByteArrayUtils.fromHex(ENCODED_DATA_AS_HEX_STRING);
      Vas vas = new VasBuilder().setVasBytes(encryptedVASData).build();

      byte[] merchantId = SecureHash.getUnsaltedHash(ByteArrayUtils.str2AscIIBytes(PASS_TYPE_ID));
      VasDecryptor.setValidateMessageTime(false);
      DecryptedVas decryptedVas = VasDecryptor.decode(vas, merchantId, privateKey);
      String decodedMessage = new String(decryptedVas.getData(), Charset.forName("UTF-8"));
      if (CORRECT_PLAIN_MESSAGE.equals(decodedMessage)) {
          System.out.println("Congratulations! Message was correctly decoded!");
      } else {
          System.out.println("Sorry, decoded message not equal to original one ;(");
          System.out.println("EXPECTED: " + CORRECT_PLAIN_MESSAGE);
          System.out.println("ACTUAL: " + decodedMessage);
      }
    } catch (Exception e) {
        System.out.println("FATAL: Error while decoding: " + e.getMessage());
        e.printStackTrace();
    }
	}

}
