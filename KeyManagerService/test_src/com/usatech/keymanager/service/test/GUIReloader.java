package com.usatech.keymanager.service.test;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;

import simple.io.BufferStream;
import simple.io.GuiInteraction;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.io.Interaction;
import simple.text.StringUtils;

public class GUIReloader {

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final JFrame frame = new JFrame("Key Manager Service Reloader");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		final JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		frame.getContentPane().add(mainPanel);
		final JButton reloadButton = new JButton("Reload");
		reloadButton.setEnabled(false);
		final JButton outputButton = new JButton("Write to File");
		outputButton.setEnabled(false);
		final JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(reloadButton);
		buttonPanel.add(outputButton);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		final JPanel interactionPanel = new JPanel();
		interactionPanel.setLayout(new BorderLayout());
		mainPanel.add(interactionPanel, BorderLayout.CENTER);
		PrintWriter cw;
		try {
			cw = new PrintWriter("GuiReloader.log");
		} catch(FileNotFoundException e) {
			e.printStackTrace();
			cw = new PrintWriter(System.out);
		}
		final PrintWriter copyWriter = cw;
		final DateFormat copyDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				copyWriter.print(copyDateFormat.format(new Date()));
				copyWriter.print(' ');
				copyWriter.println("Closing Key Manager Service Reloader");
				copyWriter.flush();
				copyWriter.close();
				System.exit(333);
			}
		});
		try {
			copyWriter.print(copyDateFormat.format(new Date()));
			copyWriter.print(' ');
			copyWriter.println("Starting Key Manager Service Reloader");
			final Interaction interaction = new GuiInteraction(interactionPanel) {
				@Override
				protected JTextComponent addMessage(String text) {
					copyWriter.print(copyDateFormat.format(new Date()));
					copyWriter.print(' ');
					copyWriter.println(text);
					copyWriter.flush();
					return super.addMessage(text);
				}
			};
			final String classpath = System.getenv("CLASSPATH");
			final String arguments = System.getenv("ARGUMENTS");
			copyWriter.print(copyDateFormat.format(new Date()));
			copyWriter.print(' ');
			copyWriter.println("CLASSPATH: " + classpath);
			copyWriter.print(copyDateFormat.format(new Date()));
			copyWriter.print(' ');
			copyWriter.println("ARGUMENTS: " + arguments);
			final BufferStream buffer = new HeapBufferStream();
			final Runnable reloadAction = new Runnable() {
				@Override
				public void run() {
					interaction.printf("Starting reload");
					File java;
					try {
						java = IOUtils.findExecutableFile(new File(System.getProperty("java.home") + "/bin/java"));
					} catch(IOException e) {
						interaction.printf("Could not find java executable");
						e.printStackTrace(interaction.getWriter());
						return;
					}
					String cmd = java.getAbsolutePath();
					List<String> command = new ArrayList<String>();
					command.add(cmd);
					String[] args = StringUtils.split(arguments, " -");
					interaction.printf("Split arguments into: " + Arrays.toString(args));
					for(int i = 0; i < args.length; i++) {
						if(args[i] == null)
							continue;
						if(i > 0)
							args[i] = '-' + args[i];
						if(args[i].startsWith("-agentlib:jdwp"))
							continue;
						command.add(args[i]);
					}
					command.add("com.usatech.keymanager.service.KeyManagerService");
					command.add("-p");
					command.add("KeyManagerService.properties");
					command.add("load");
					interaction.printf("Starting up KeyManagerService using command: " + StringUtils.join(command, " "));
					ProcessBuilder pb = new ProcessBuilder(command);
					pb.environment().put("CLASSPATH", classpath);
					Process p;
					try {
						p = pb.start();
						interaction.printf("Writing to KMS stdin");
						long n = IOUtils.copy(buffer.getInputStream(), p.getOutputStream());
						interaction.printf("Wrote %1$s to KMS stdin", n);
						p.getOutputStream().close();
					} catch(IOException e) {
						interaction.printf("Could not start KeyManagerService with command: '%1$s'", cmd);
						e.printStackTrace(interaction.getWriter());
						return;
					}
					// System.exit(0);
				}
			};
			final Runnable outputAction = new Runnable() {
				@Override
				public void run() {
					interaction.printf("Starting output");
					String filename = "KMS-" + System.currentTimeMillis() + "-kek.store";
					try {
						OutputStream out = new FileOutputStream(filename);
						long n = IOUtils.copy(buffer.getInputStream(), out);
						interaction.printf("Wrote %1$s to '%2$s'", n, filename);
						out.close();
					} catch(IOException e) {
						interaction.printf("Could not write to output file '%1$s'", filename);
						e.printStackTrace(interaction.getWriter());
						return;
					}
					// System.exit(0);
				}
			};
			final Executor executor = new ThreadPoolExecutor(1, 1, Integer.MAX_VALUE, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
			reloadButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent evt) {
					executor.execute(reloadAction);
				}
			});
			outputButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent evt) {
					executor.execute(outputAction);
				}
			});
			frame.setVisible(true);
			interaction.printf("Key Manager Service Reloader is starting up...");
			interaction.printf("Reading from stdin");
			long n;
			try {
				n = IOUtils.copy(System.in, buffer.getOutputStream());
				interaction.printf("Read %1$s from stdin; waiting for reload command", n);
				reloadButton.setEnabled(true);
				outputButton.setEnabled(true);
			} catch(IOException e) {
				interaction.printf("Error while reading from stdin:");
				e.printStackTrace(interaction.getWriter());
			}
		} finally {
			copyWriter.flush();
		}
	}

}
