package com.usatech.ec2.test;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.FileDataSource;
import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Message;
import javax.wsdl.PortType;
import javax.wsdl.Service;

import org.apache.axis.Constants;
import org.apache.axis.encoding.TypeMapping;
import org.apache.axis.encoding.TypeMappingDelegate;
import org.apache.axis.encoding.TypeMappingRegistry;
import org.apache.axis.encoding.TypeMappingRegistryImpl;
import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.gen.NoopFactory;
import org.apache.axis.wsdl.gen.Parser;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.Parameter;
import org.apache.axis.wsdl.symbolTable.Parameters;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.TypeEntry;

import simple.bean.ReflectionUtils;
import simple.text.StringUtils;
import simple.util.ConversionSet;
import simple.util.MapBackedSet;

public class ClientTestGenerator extends NoopFactory {
	protected class StubGenerator implements Generator {
		protected final Object o;

		public StubGenerator(Object o) {
			this.o = o;
		}

		@Override
		public void generate() throws IOException {
			System.out.println("Processing " + o);
		}
	}

	@Override
	public Generator getGenerator(Definition definition, SymbolTable symbolTable) {
		return new StubGenerator(definition);
	}

	@Override
	public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {
		return new StubGenerator(type);
	}

	@Override
	public Generator getGenerator(Service service, SymbolTable symbolTable) {
		return new StubGenerator(service);
	}

	@Override
	public Generator getGenerator(Binding binding, SymbolTable symbolTable) {
		return new StubGenerator(binding);
	}

	@Override
	public Generator getGenerator(PortType portType, SymbolTable symbolTable) {
		return new StubGenerator(portType);
	}

	@Override
	public Generator getGenerator(Message message, SymbolTable symbolTable) {
		return new StubGenerator(message);
	}

	@Override
	public void generatorPass(Definition def, SymbolTable symbolTable) {
		// TODO Auto-generated method stub

	}

	protected static final Pattern replacePrefixPattern = Pattern.compile("^EC\\d*");
	public static void main(String[] args) throws Exception {
		File wsdlFile = new File(args[0]);
		// String suffix = "2";
		// File wsdlFile = new File("../NetLayer/resources/wsdl/ePortConnect" + suffix + ".wsdl");
		Parser parser = new Parser();
		// parser.setFactory(new WsdlToMessageData());
		parser.run("file:" + wsdlFile.getAbsolutePath());
		/*
		// Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(wsdlFile);
		// parser.run("file:" + wsdlFile.getAbsolutePath(), doc);

		for(Object k : parser.getSymbolTable().getHashMap().keySet()) {
			BindingEntry be = parser.getSymbolTable().getBindingEntry((QName) k);
			System.out.println(be);
		}*/
		File baseDirectory = new File("../ePortConnectClient/src/");
		if(!baseDirectory.exists()) {
			throw new Exception("File " + baseDirectory.getAbsolutePath() + " does not exist");
		}
		Definition def = parser.getCurrentDefinition();
		String packageName;
		if(def.getTargetNamespace() != null && def.getTargetNamespace().startsWith("urn:"))
			packageName = reversePieces(def.getTargetNamespace().substring(4), '.');
		else
			packageName = "com.usatech.ec2";
		String prefix = "EportConnect";

		File soapTestFile = new File(baseDirectory, packageName.replace('.', '/') + '/' + prefix + "SOAPTest.java");
		soapTestFile.getParentFile().mkdirs();
		File hessianTestFile = new File(baseDirectory, packageName.replace('.', '/') + '/' + prefix + "HessianTest.java");
		hessianTestFile.getParentFile().mkdirs();
		StringBuilder functionSwitch = new StringBuilder();
		StringBuilder soapFunctions = new StringBuilder();
		StringBuilder hessianFunctions = new StringBuilder();
		StringBuilder printUsageFunctions = new StringBuilder();
		StringBuilder printUsageExamples = new StringBuilder();
		StringBuilder defaultFieldMethods = new StringBuilder();
		StringBuilder junitTests = new StringBuilder();
		Set<Class<?>> imports = new LinkedHashSet<Class<?>>();
		functionSwitch.append("\t\t");
		Set<String> genList = new HashSet<String>();
		Set<String> fieldNameList = new HashSet<String>();
		for(Object b : def.getBindings().values()) {
			Binding bnd = (Binding) b;
			BindingEntry bindingEntry = parser.getSymbolTable().getBindingEntry(bnd.getQName());
			for(Object o : bnd.getBindingOperations()) {
				BindingOperation bop = (BindingOperation) o;
				if(!genList.add(bop.getName()))
					continue;
				Parameters parameters = parser.getSymbolTable().getOperationParameters(bop.getOperation(), "", bindingEntry);
				functionSwitch.append("if(function.equalsIgnoreCase(\"").append(bop.getName()).append("\")) {\n");
				soapFunctions.append("\tprotected void test").append(StringUtils.capitalizeFirst(bop.getName())).append("(String url");
				hessianFunctions.append("\tprotected void test").append(StringUtils.capitalizeFirst(bop.getName())).append("(String url");
				junitTests.append("\t@Test\n\tpublic void test").append(StringUtils.capitalizeFirst(bop.getName())).append("() throws Exception {\n\t\ttest").append(StringUtils.capitalizeFirst(bop.getName())).append("(getDefaultUrl()");
				printUsageFunctions.append("\t\tSystem.out.println(\"").append(bop.getName()).append(" [<url>]");
				printUsageExamples.append("\t\tSystem.out.print(\"").append(bop.getName()).append(" \");\n\t\tSystem.out.print(getDefaultUrl());\n");
				// set properties
				int i = 0;
				StringBuilder arguments = new StringBuilder();
				StringBuilder functionBody = new StringBuilder();
				functionBody.append("\t\tEc2Stub.").append(StringUtils.capitalizeFirst(bop.getName())).append(" request = new Ec2Stub.").append(StringUtils.capitalizeFirst(bop.getName())).append("();\n");
				StringBuilder hessianArguments = new StringBuilder();
				for(Object l : parameters.list) {
					Parameter p = (Parameter) l;
					Class<?> cls = tm.getClassForQName(p.getType().getQName());
					if(cls == null)
						cls = String.class;
					imports.add(cls);
					String fieldName = StringUtils.toJavaName(p.getName());
					if(fieldName.equals("fileName")) {
						functionBody.append("\t\trequest.setFileName(filePath.getName());\n");
						hessianArguments.append(", filePath.getName()");
						continue;
					} else if(fieldName.equals("fileSize")) {
						hessianArguments.append(", filePath.length()");
						functionBody.append("\t\trequest.setFileSize(filePath.length());\n");
						continue;
					} else if(fieldName.equals("fileContent")) {
						hessianArguments.append(", new EC2DataHandler(new FileDataSource(filePath))");
						functionBody.append("\t\trequest.setFileContent(new EC2DataHandler(new FileDataSource(filePath)));\n");
						cls = File.class;
						fieldName = "filePath";
					} else
						hessianArguments.append(", ").append(fieldName);
					functionSwitch.append("\t\t\t").append(cls.getSimpleName()).append(' ').append(fieldName).append(" = (args.length <= offset + ").append(i).append(" ? getDefault").append(StringUtils.capitalizeFirst(fieldName)).append("() : ");
					if(cls.equals(long.class))
						functionSwitch.append("parseLong(");
					else if(cls.equals(int.class))
						functionSwitch.append("parseInt(");
					else if(cls.equals(byte[].class))
						functionSwitch.append("parseByteArray(");
					else if(!cls.equals(String.class))
						functionSwitch.append("new ").append(cls.getSimpleName()).append('(');
					functionSwitch.append("args[offset + ").append(i++).append("]");
					if(!cls.equals(String.class))
						functionSwitch.append(')');
					functionSwitch.append(");\n");
					arguments.append(", ").append(fieldName);
					soapFunctions.append(", ").append(cls.getSimpleName()).append(' ').append(fieldName);
					hessianFunctions.append(", ").append(cls.getSimpleName()).append(' ').append(fieldName);
					junitTests.append(", getDefault").append(StringUtils.capitalizeFirst(fieldName)).append("()");
					if(fieldNameList.add(fieldName))
						defaultFieldMethods.append("\tprotected static ").append(cls.getSimpleName()).append(" getDefault").append(StringUtils.capitalizeFirst(fieldName)).append("() {\n\t\treturn null;\n\t}\n\n");
					if(fieldName.equals("filePath") && cls.equals(File.class)) {
						imports.add(FileDataSource.class);
						functionBody.append("\t\trequest.setFileName(").append(fieldName).append(".getName());\n\t\trequest.setFileSize(").append(fieldName).append(".length());\n\t\trequest.setFileContent(new EC2DataHandler(new FileDataSource(").append(fieldName).append(")));\n");
					} else
						functionBody.append("\t\trequest.set").append(StringUtils.capitalizeFirst(fieldName)).append('(').append(fieldName).append(");\n");
					printUsageFunctions.append(" <").append(fieldName).append('>');
					printUsageExamples.append("\t\tSystem.out.print(\" \");\n\t\tSystem.out.print(getDefault").append(StringUtils.capitalizeFirst(fieldName)).append("());\n");
				}

				functionSwitch.append("\t\t\ttest").append(StringUtils.capitalizeFirst(bop.getName())).append("(url");
				functionSwitch.append(arguments).append(");\n\t\t} else ");
				soapFunctions.append(") throws Exception {\n").append(functionBody);
				soapFunctions.append("\t\tEc2Stub ePortConnect = getService(url);\n\t\tEc2Stub.").append(StringUtils.capitalizeFirst(bop.getName())).append("Response response;\n\t\ttry {\n\t\t\tresponse = ePortConnect.");
				soapFunctions.append(bop.getName()).append("(request);\n\t\t} finally {\n\t\t\tePortConnect._getServiceClient().cleanupTransport();\n\t\t}\n");
				soapFunctions.append("\t\tif (response == null || response.get_return() == null)\n\t\t\tthrow new Exception(\"Received null response\");\n");
				soapFunctions.append("\t\thandleResponse(response.get_return());\n");
				soapFunctions.append("\t}\n\n");

				hessianFunctions.append(") throws Exception {\n");
				hessianFunctions.append("\t\tObject response = getService(url).").append(bop.getName()).append('(').append(hessianArguments, 2, hessianArguments.length()).append(");\n");
				hessianFunctions.append("\t\thandleResponse(response);\n");
				hessianFunctions.append("\t}\n\n");

				junitTests.append(");\n\t}\n\n");
				printUsageFunctions.append("\");\n");
				printUsageExamples.append("\t\tSystem.out.println();\n");
			}
		}
		functionSwitch.append(" {\n\t\t\tSystem.out.println(\"ERROR: Invalid operation '\" + function);\n\t\t\tprintUsage();\n\t\t}\n");

		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder importString = new StringBuilder();
		for(Class<?> clazz : imports) {
			if(shouldImport(clazz)) {
				String cn = clazz.getCanonicalName();
				if(cn != null)
					importString.append("import ").append(cn).append(";\n");
			}
		}
		params.put("imports", importString.toString());
		params.put("functionSwitch", functionSwitch.toString());
		params.put("functions", soapFunctions.toString());
		params.put("printUsageFunctions", printUsageFunctions.toString());
		params.put("printUsageExamples", printUsageExamples.toString());
		params.put("junitTests", junitTests.toString());
		params.put("className", prefix + "SOAPTest");
		params.put("defaultFieldMethods", defaultFieldMethods.toString());
		PrintWriter writer = new PrintWriter(new FileWriter(soapTestFile));
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(ClientTestGenerator.class.getResourceAsStream("SOAPTestTemplate.txt")));
			Pattern replacePattern = Pattern.compile("\\$\\{([A-Za-z0-9_.\\(\\)\\[\\]]+)\\}");
			String line;
			while((line = reader.readLine()) != null) {
				Matcher matcher = replacePattern.matcher(line);
				int pos = 0;
				while(matcher.find()) {
					writer.print(line.substring(pos, matcher.start()));
					writer.print(ReflectionUtils.getProperty(params, matcher.group(1)));
					pos = matcher.end();
				}
				writer.println(line.substring(pos));
			}
			writer.flush();
		} finally {
			writer.close();
		}
		params.put("className", prefix + "HessianTest");
		params.put("functions", hessianFunctions.toString());
		writer = new PrintWriter(new FileWriter(hessianTestFile));
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(ClientTestGenerator.class.getResourceAsStream("HessianTestTemplate.txt")));
			Pattern replacePattern = Pattern.compile("\\$\\{([A-Za-z0-9_.\\(\\)\\[\\]]+)\\}");
			String line;
			while((line = reader.readLine()) != null) {
				Matcher matcher = replacePattern.matcher(line);
				int pos = 0;
				while(matcher.find()) {
					writer.print(line.substring(pos, matcher.start()));
					writer.print(ReflectionUtils.getProperty(params, matcher.group(1)));
					pos = matcher.end();
				}
				writer.println(line.substring(pos));
			}
			writer.flush();
		} finally {
			writer.close();
		}
		System.out.println("Generation Complete");
	}

	protected static String reversePieces(String orig, char delimiter) {
		StringBuilder sb = new StringBuilder();
		int p = orig.length();
		while(true) {
			int next = orig.lastIndexOf(delimiter, p - 1);
			if(next < 0) {
				sb.append(orig, 0, p);
				return sb.toString();
			}
			sb.append(orig, next + 1, p).append(delimiter);
			p = next;
			next = orig.lastIndexOf(delimiter, p - 1);
		}
	}

	protected static TypeMappingRegistry tmr = new TypeMappingRegistryImpl();
	protected static TypeMapping tm = (TypeMapping) tmr.getTypeMapping(Constants.URI_SOAP11_ENC);
	protected static final AtomicInteger[] lastMessageTypeValues = new AtomicInteger[256];
	protected final static Set<String> SENSITIVE_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	protected final static Set<String> CARD_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	static {
		SENSITIVE_PROPERTIES.add("PASSWORD");
		SENSITIVE_PROPERTIES.add("NEWPASSWORD");
		SENSITIVE_PROPERTIES.add("CREDENTIAL");
		SENSITIVE_PROPERTIES.add("NEWCREDENTIAL");
		SENSITIVE_PROPERTIES.add("PASSCODE");
		SENSITIVE_PROPERTIES.add("NEWPASSCODE");
		CARD_PROPERTIES.add("CARDDATA");
		((TypeMappingDelegate) tm).setDoAutoTypes(true);
	}

	protected static boolean isSale(Parameter[] parameters) {
		for(Parameter p : parameters)
			if("tranId".equalsIgnoreCase(p.getName()))
				return true;
		return false;
	}

	protected static boolean shouldImport(Class<?> clazz) {
		if(clazz.getPackage() != null && !clazz.getPackage().getName().equals("java.lang") && !clazz.getPackage().getName().equals("com.usatech.ec2"))
			return true;
		return false;
	}
	protected static String fromJavaName(String name) {
		StringBuilder sb = new StringBuilder();
		char last = '_';
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(Character.isDigit(ch)) {
				if(last != '_' && !Character.isDigit(last))
					sb.append('_');
				sb.append(ch);
				last = ch;
			} else if(Character.isLetter(ch)) {
				if(last != '_' && (Character.isDigit(last) || (Character.isUpperCase(ch) && Character.isLowerCase(last))))
					sb.append('_');
				sb.append(Character.toUpperCase(ch));
				last = ch;
			} else {
				sb.append('_');
				last = '_';
			}
		}
		return sb.toString();
	}

	protected static Map<Class<?>, String> bufferOpSuffixes = new HashMap<Class<?>, String>();
	protected static Map<Class<?>, String> utilsOpSuffixes = new HashMap<Class<?>, String>();
	static {
		bufferOpSuffixes.put(byte.class, "");
		bufferOpSuffixes.put(short.class, "Short");
		bufferOpSuffixes.put(int.class, "Int");
		bufferOpSuffixes.put(long.class, "Long");
		bufferOpSuffixes.put(float.class, "Float");
		bufferOpSuffixes.put(double.class, "Double");
		bufferOpSuffixes.put(char.class, "Char");

		bufferOpSuffixes.put(Byte.class, "");
		bufferOpSuffixes.put(Short.class, "Short");
		bufferOpSuffixes.put(Integer.class, "Int");
		bufferOpSuffixes.put(Long.class, "Long");
		bufferOpSuffixes.put(Float.class, "Float");
		bufferOpSuffixes.put(Double.class, "Double");
		bufferOpSuffixes.put(Character.class, "Char");
		
		utilsOpSuffixes.put(byte[].class, "LongBytes");
		utilsOpSuffixes.put(BigDecimal.class, "StringAmount");
		utilsOpSuffixes.put(BigInteger.class, "StringInt");
		utilsOpSuffixes.put(Date.class, "Date");
		utilsOpSuffixes.put(Calendar.class, "Timestamp");
		utilsOpSuffixes.put(Currency.class, "Currency");
		utilsOpSuffixes.put(Boolean.class, "Boolean");
	}

}
