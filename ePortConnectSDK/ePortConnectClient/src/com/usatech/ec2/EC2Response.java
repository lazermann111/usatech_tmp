package com.usatech.ec2;

import java.io.Serializable;

public class EC2Response implements Serializable {
	private static final long serialVersionUID = 8461988464556634296L;
	public static final int ACT_NOTHING = 0;
	public static final int ACT_UPDATE_TRAN_ID = 1;
	public static final int ACT_SERVICE_EVENT = 2;
	
	protected String serialNumber;
	protected int returnCode = 0;
	protected int actionCode = 0;
	protected String returnMessage = "Failed";
	protected String newUsername = "";
	protected String newPassword = "";
	protected long newTranId = 0;
	protected String attributes = "";
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public int getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	public int getActionCode() {
		return actionCode;
	}
	public void setActionCode(int actionCode) {
		this.actionCode = actionCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	public String getNewUsername() {
		return newUsername;
	}
	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public long getNewTranId() {
		return newTranId;
	}
	public void setNewTranId(long newTranId) {
		this.newTranId = newTranId;
	}
	public String getAttributes() {
		return attributes;
	}
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}
}
