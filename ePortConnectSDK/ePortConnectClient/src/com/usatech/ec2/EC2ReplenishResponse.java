package com.usatech.ec2;

import java.io.Serializable;

public class EC2ReplenishResponse extends EC2AuthResponse implements Serializable {
	private static final long serialVersionUID = -8736815613966992928L;
	
	protected long replenishBalanceAmount = 0;
	
	public long getReplenishBalanceAmount() {
		return replenishBalanceAmount;
	}
	public void setReplenishBalanceAmount(long replenishBalanceAmount) {
		this.replenishBalanceAmount = replenishBalanceAmount;
	}
}
