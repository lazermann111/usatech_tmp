package com.usatech.ec2;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.activation.FileDataSource;

import org.junit.Test;

public class EportConnectSOAPTest extends AbstractEportConnectTest {
    public static final String EPORT_CONNECT_SOAP_URL = "https://ec-ecc.usatech.com:9443/soap/ec2";
    protected final Map<String, Ec2Stub> apiMap = new HashMap<String, Ec2Stub>();
    
    public static void main(String[] args) throws Exception {
        new EportConnectSOAPTest().process(args);
    }
    
	protected void process(String[] args) throws Exception {
        if(args.length < 1) {
            printUsage();
            return;
        }
        int offset;
        String url;
        if(args.length > 1 && args[1].startsWith("https://") || args[1].startsWith("http://")) {
            url = args[1];
            offset = 2;
        } else {
            url = getDefaultUrl();
            offset = 1;
        }
        String function = args[0];
		if(function.equalsIgnoreCase("replenishPlain")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			String cardData = (args.length <= offset + 5 ? getDefaultCardData() : args[offset + 5]);
			String entryType = (args.length <= offset + 6 ? getDefaultEntryType() : args[offset + 6]);
			long replenishCardId = (args.length <= offset + 7 ? getDefaultReplenishCardId() : parseLong(args[offset + 7]));
			long replenishConsumerId = (args.length <= offset + 8 ? getDefaultReplenishConsumerId() : parseLong(args[offset + 8]));
			String attributes = unescapeBackslash(args.length <= offset + 9 ? getDefaultAttributes() : args[offset + 9]);
			testReplenishPlain(url, username, password, serialNumber, tranId, amount, cardData, entryType, replenishCardId, replenishConsumerId, attributes);
		} else if(function.equalsIgnoreCase("replenishEncrypted")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			int cardReaderType = (args.length <= offset + 5 ? getDefaultCardReaderType() : parseInt(args[offset + 5]));
			int decryptedCardDataLen = (args.length <= offset + 6 ? getDefaultDecryptedCardDataLen() : parseInt(args[offset + 6]));
			String encryptedCardDataHex = (args.length <= offset + 7 ? getDefaultEncryptedCardDataHex() : args[offset + 7]);
			String ksnHex = (args.length <= offset + 8 ? getDefaultKsnHex() : args[offset + 8]);
			String entryType = (args.length <= offset + 9 ? getDefaultEntryType() : args[offset + 9]);
			long replenishCardId = (args.length <= offset + 10 ? getDefaultReplenishCardId() : parseLong(args[offset + 10]));
			long replenishConsumerId = (args.length <= offset + 11 ? getDefaultReplenishConsumerId() : parseLong(args[offset + 11]));
			String attributes = unescapeBackslash(args.length <= offset + 12 ? getDefaultAttributes() : args[offset + 12]);
			testReplenishEncrypted(url, username, password, serialNumber, tranId, amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, replenishCardId, replenishConsumerId, attributes);
		} else if(function.equalsIgnoreCase("batch")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			String tranResult = (args.length <= offset + 5 ? getDefaultTranResult() : args[offset + 5]);
			String tranDetails = (args.length <= offset + 6 ? getDefaultTranDetails() : args[offset + 6]);
			String attributes = unescapeBackslash(args.length <= offset + 7 ? getDefaultAttributes() : args[offset + 7]);
			testBatch(url, username, password, serialNumber, tranId, amount, tranResult, tranDetails, attributes);
		} else if(function.equalsIgnoreCase("getCardInfoEncrypted")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			int cardReaderType = (args.length <= offset + 3 ? getDefaultCardReaderType() : parseInt(args[offset + 3]));
			int decryptedCardDataLen = (args.length <= offset + 4 ? getDefaultDecryptedCardDataLen() : parseInt(args[offset + 4]));
			String encryptedCardDataHex = (args.length <= offset + 5 ? getDefaultEncryptedCardDataHex() : args[offset + 5]);
			String ksnHex = (args.length <= offset + 6 ? getDefaultKsnHex() : args[offset + 6]);
			String entryType = (args.length <= offset + 7 ? getDefaultEntryType() : args[offset + 7]);
			String attributes = unescapeBackslash(args.length <= offset + 8 ? getDefaultAttributes() : args[offset + 8]);
			testGetCardInfoEncrypted(url, username, password, serialNumber, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, attributes);
		} else if(function.equalsIgnoreCase("getCardInfoPlain")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			String cardData = (args.length <= offset + 3 ? getDefaultCardData() : args[offset + 3]);
			String entryType = (args.length <= offset + 4 ? getDefaultEntryType() : args[offset + 4]);
			String attributes = unescapeBackslash(args.length <= offset + 5 ? getDefaultAttributes() : args[offset + 5]);
			testGetCardInfoPlain(url, username, password, serialNumber, cardData, entryType, attributes);
		} else if(function.equalsIgnoreCase("tokenizeEncrypted")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			int cardReaderType = (args.length <= offset + 4 ? getDefaultCardReaderType() : parseInt(args[offset + 4]));
			int decryptedCardDataLen = (args.length <= offset + 5 ? getDefaultDecryptedCardDataLen() : parseInt(args[offset + 5]));
			String encryptedCardDataHex = (args.length <= offset + 6 ? getDefaultEncryptedCardDataHex() : args[offset + 6]);
			String ksnHex = (args.length <= offset + 7 ? getDefaultKsnHex() : args[offset + 7]);
			String billingPostalCode = (args.length <= offset + 8 ? getDefaultBillingPostalCode() : args[offset + 8]);
			String billingAddress = (args.length <= offset + 9 ? getDefaultBillingAddress() : args[offset + 9]);
			String attributes = unescapeBackslash(args.length <= offset + 10 ? getDefaultAttributes() : args[offset + 10]);
			testTokenizeEncrypted(url, username, password, serialNumber, tranId, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingPostalCode, billingAddress, attributes);
		} else if(function.equalsIgnoreCase("tokenizePlain")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			String cardNumber = (args.length <= offset + 4 ? getDefaultCardNumber() : args[offset + 4]);
			String expirationDate = (args.length <= offset + 5 ? getDefaultExpirationDate() : args[offset + 5]);
			String securityCode = (args.length <= offset + 6 ? getDefaultSecurityCode() : args[offset + 6]);
			String cardHolder = (args.length <= offset + 7 ? getDefaultCardHolder() : args[offset + 7]);
			String billingPostalCode = (args.length <= offset + 8 ? getDefaultBillingPostalCode() : args[offset + 8]);
			String billingAddress = (args.length <= offset + 9 ? getDefaultBillingAddress() : args[offset + 9]);
			String attributes = unescapeBackslash(args.length <= offset + 10 ? getDefaultAttributes() : args[offset + 10]);
			testTokenizePlain(url, username, password, serialNumber, tranId, cardNumber, expirationDate, securityCode, cardHolder, billingPostalCode, billingAddress, attributes);
		} else if(function.equalsIgnoreCase("processUpdates")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			int updateStatus = (args.length <= offset + 3 ? getDefaultUpdateStatus() : parseInt(args[offset + 3]));
			int protocolVersion = (args.length <= offset + 4 ? getDefaultProtocolVersion() : parseInt(args[offset + 4]));
			String appType = (args.length <= offset + 5 ? getDefaultAppType() : args[offset + 5]);
			String appVersion = (args.length <= offset + 6 ? getDefaultAppVersion() : args[offset + 6]);
			String attributes = unescapeBackslash(args.length <= offset + 7 ? getDefaultAttributes() : args[offset + 7]);
			testProcessUpdates(url, username, password, serialNumber, updateStatus, protocolVersion, appType, appVersion, attributes);
		} else if(function.equalsIgnoreCase("replenishCash")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			long replenishCardId = (args.length <= offset + 5 ? getDefaultReplenishCardId() : parseLong(args[offset + 5]));
			long replenishConsumerId = (args.length <= offset + 6 ? getDefaultReplenishConsumerId() : parseLong(args[offset + 6]));
			String attributes = unescapeBackslash(args.length <= offset + 7 ? getDefaultAttributes() : args[offset + 7]);
			testReplenishCash(url, username, password, serialNumber, tranId, amount, replenishCardId, replenishConsumerId, attributes);
		} else if(function.equalsIgnoreCase("getCardIdPlain")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			String cardData = (args.length <= offset + 3 ? getDefaultCardData() : args[offset + 3]);
			String entryType = (args.length <= offset + 4 ? getDefaultEntryType() : args[offset + 4]);
			String attributes = unescapeBackslash(args.length <= offset + 5 ? getDefaultAttributes() : args[offset + 5]);
			testGetCardIdPlain(url, username, password, serialNumber, cardData, entryType, attributes);
		} else if(function.equalsIgnoreCase("getCardIdEncrypted")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			int cardReaderType = (args.length <= offset + 3 ? getDefaultCardReaderType() : parseInt(args[offset + 3]));
			int decryptedCardDataLen = (args.length <= offset + 4 ? getDefaultDecryptedCardDataLen() : parseInt(args[offset + 4]));
			String encryptedCardDataHex = (args.length <= offset + 5 ? getDefaultEncryptedCardDataHex() : args[offset + 5]);
			String ksnHex = (args.length <= offset + 6 ? getDefaultKsnHex() : args[offset + 6]);
			String entryType = (args.length <= offset + 7 ? getDefaultEntryType() : args[offset + 7]);
			String attributes = unescapeBackslash(args.length <= offset + 8 ? getDefaultAttributes() : args[offset + 8]);
			testGetCardIdEncrypted(url, username, password, serialNumber, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, attributes);
		} else if(function.equalsIgnoreCase("cash")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			long tranUTCTimeMs = (args.length <= offset + 5 ? getDefaultTranUTCTimeMs() : parseLong(args[offset + 5]));
			int tranUTCOffsetMs = (args.length <= offset + 6 ? getDefaultTranUTCOffsetMs() : parseInt(args[offset + 6]));
			String tranResult = (args.length <= offset + 7 ? getDefaultTranResult() : args[offset + 7]);
			String tranDetails = (args.length <= offset + 8 ? getDefaultTranDetails() : args[offset + 8]);
			String attributes = unescapeBackslash(args.length <= offset + 9 ? getDefaultAttributes() : args[offset + 9]);
			testCash(url, username, password, serialNumber, tranId, amount, tranUTCTimeMs, tranUTCOffsetMs, tranResult, tranDetails, attributes);
		} else if(function.equalsIgnoreCase("chargePlain")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			String cardData = (args.length <= offset + 5 ? getDefaultCardData() : args[offset + 5]);
			String entryType = (args.length <= offset + 6 ? getDefaultEntryType() : args[offset + 6]);
			String tranResult = (args.length <= offset + 7 ? getDefaultTranResult() : args[offset + 7]);
			String tranDetails = (args.length <= offset + 8 ? getDefaultTranDetails() : args[offset + 8]);
			String attributes = unescapeBackslash(args.length <= offset + 9 ? getDefaultAttributes() : args[offset + 9]);
			testChargePlain(url, username, password, serialNumber, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		} else if(function.equalsIgnoreCase("chargeEncrypted")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			int cardReaderType = (args.length <= offset + 5 ? getDefaultCardReaderType() : parseInt(args[offset + 5]));
			int decryptedCardDataLen = (args.length <= offset + 6 ? getDefaultDecryptedCardDataLen() : parseInt(args[offset + 6]));
			String encryptedCardDataHex = (args.length <= offset + 7 ? getDefaultEncryptedCardDataHex() : args[offset + 7]);
			String ksnHex = (args.length <= offset + 8 ? getDefaultKsnHex() : args[offset + 8]);
			String entryType = (args.length <= offset + 9 ? getDefaultEntryType() : args[offset + 9]);
			String tranResult = (args.length <= offset + 10 ? getDefaultTranResult() : args[offset + 10]);
			String tranDetails = (args.length <= offset + 11 ? getDefaultTranDetails() : args[offset + 11]);
			String attributes = unescapeBackslash(args.length <= offset + 12 ? getDefaultAttributes() : args[offset + 12]);
			testChargeEncrypted(url, username, password, serialNumber, tranId, amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		} else if(function.equalsIgnoreCase("authEncrypted")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			int cardReaderType = (args.length <= offset + 5 ? getDefaultCardReaderType() : parseInt(args[offset + 5]));
			int decryptedCardDataLen = (args.length <= offset + 6 ? getDefaultDecryptedCardDataLen() : parseInt(args[offset + 6]));
			String encryptedCardDataHex = (args.length <= offset + 7 ? getDefaultEncryptedCardDataHex() : args[offset + 7]);
			String ksnHex = (args.length <= offset + 8 ? getDefaultKsnHex() : args[offset + 8]);
			String entryType = (args.length <= offset + 9 ? getDefaultEntryType() : args[offset + 9]);
			String attributes = unescapeBackslash(args.length <= offset + 10 ? getDefaultAttributes() : args[offset + 10]);
			testAuthEncrypted(url, username, password, serialNumber, tranId, amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, attributes);
		} else if(function.equalsIgnoreCase("uploadDeviceInfo")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			String attributes = unescapeBackslash(args.length <= offset + 3 ? getDefaultAttributes() : args[offset + 3]);
			testUploadDeviceInfo(url, username, password, serialNumber, attributes);
		} else if(function.equalsIgnoreCase("uploadFile")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			int fileType = (args.length <= offset + 3 ? getDefaultFileType() : parseInt(args[offset + 3]));
			File filePath = (args.length <= offset + 4 ? getDefaultFilePath() : new File(args[offset + 4]));
			String attributes = unescapeBackslash(args.length <= offset + 5 ? getDefaultAttributes() : args[offset + 5]);
			testUploadFile(url, username, password, serialNumber, fileType, filePath, attributes);
		} else if(function.equalsIgnoreCase("untokenize")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long cardId = (args.length <= offset + 3 ? getDefaultCardId() : parseLong(args[offset + 3]));
			String tokenHex = (args.length <= offset + 4 ? getDefaultTokenHex() : args[offset + 4]);
			String attributes = unescapeBackslash(args.length <= offset + 5 ? getDefaultAttributes() : args[offset + 5]);
			testUntokenize(url, username, password, serialNumber, cardId, tokenHex, attributes);
		} else if(function.equalsIgnoreCase("authPlain")) {
			String username = (args.length <= offset + 0 ? getDefaultUsername() : args[offset + 0]);
			String password = (args.length <= offset + 1 ? getDefaultPassword() : args[offset + 1]);
			String serialNumber = (args.length <= offset + 2 ? getDefaultSerialNumber() : args[offset + 2]);
			long tranId = (args.length <= offset + 3 ? getDefaultTranId() : parseLong(args[offset + 3]));
			long amount = (args.length <= offset + 4 ? getDefaultAmount() : parseLong(args[offset + 4]));
			String cardData = (args.length <= offset + 5 ? getDefaultCardData() : args[offset + 5]);
			String entryType = (args.length <= offset + 6 ? getDefaultEntryType() : args[offset + 6]);
			String attributes = unescapeBackslash(args.length <= offset + 7 ? getDefaultAttributes() : args[offset + 7]);
			testAuthPlain(url, username, password, serialNumber, tranId, amount, cardData, entryType, attributes);
		} else  {
			System.out.println("ERROR: Invalid operation '" + function);
			printUsage();
		}

    }
    
    protected String getDefaultUrl() {
        return EPORT_CONNECT_SOAP_URL;
    }
    
    protected Ec2Stub getService(String url) throws Exception {
        Ec2Stub api = apiMap.get(url);
        if(api == null) {
            api = new Ec2Stub(url);
            apiMap.put(url, api);
        }
        return api;
    }

    protected void printUsage() {
        System.out.println("Please provide arguments to use one of the functions below. You may need to change the classpath for your environment.");
        System.out.println();
		System.out.println("replenishPlain [<url>] <username> <password> <serialNumber> <tranId> <amount> <cardData> <entryType> <replenishCardId> <replenishConsumerId> <attributes>");
		System.out.println("replenishEncrypted [<url>] <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <entryType> <replenishCardId> <replenishConsumerId> <attributes>");
		System.out.println("batch [<url>] <username> <password> <serialNumber> <tranId> <amount> <tranResult> <tranDetails> <attributes>");
		System.out.println("getCardInfoEncrypted [<url>] <username> <password> <serialNumber> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <entryType> <attributes>");
		System.out.println("getCardInfoPlain [<url>] <username> <password> <serialNumber> <cardData> <entryType> <attributes>");
		System.out.println("tokenizeEncrypted [<url>] <username> <password> <serialNumber> <tranId> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <billingPostalCode> <billingAddress> <attributes>");
		System.out.println("tokenizePlain [<url>] <username> <password> <serialNumber> <tranId> <cardNumber> <expirationDate> <securityCode> <cardHolder> <billingPostalCode> <billingAddress> <attributes>");
		System.out.println("processUpdates [<url>] <username> <password> <serialNumber> <updateStatus> <protocolVersion> <appType> <appVersion> <attributes>");
		System.out.println("replenishCash [<url>] <username> <password> <serialNumber> <tranId> <amount> <replenishCardId> <replenishConsumerId> <attributes>");
		System.out.println("getCardIdPlain [<url>] <username> <password> <serialNumber> <cardData> <entryType> <attributes>");
		System.out.println("getCardIdEncrypted [<url>] <username> <password> <serialNumber> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <entryType> <attributes>");
		System.out.println("cash [<url>] <username> <password> <serialNumber> <tranId> <amount> <tranUTCTimeMs> <tranUTCOffsetMs> <tranResult> <tranDetails> <attributes>");
		System.out.println("chargePlain [<url>] <username> <password> <serialNumber> <tranId> <amount> <cardData> <entryType> <tranResult> <tranDetails> <attributes>");
		System.out.println("chargeEncrypted [<url>] <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <entryType> <tranResult> <tranDetails> <attributes>");
		System.out.println("authEncrypted [<url>] <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <entryType> <attributes>");
		System.out.println("uploadDeviceInfo [<url>] <username> <password> <serialNumber> <attributes>");
		System.out.println("uploadFile [<url>] <username> <password> <serialNumber> <fileType> <filePath> <attributes>");
		System.out.println("untokenize [<url>] <username> <password> <serialNumber> <cardId> <tokenHex> <attributes>");
		System.out.println("authPlain [<url>] <username> <password> <serialNumber> <tranId> <amount> <cardData> <entryType> <attributes>");

        System.out.println("Examples:");
        System.out.println();
		System.out.print("replenishPlain ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultCardData());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultReplenishCardId());
		System.out.print(" ");
		System.out.print(getDefaultReplenishConsumerId());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("replenishEncrypted ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultCardReaderType());
		System.out.print(" ");
		System.out.print(getDefaultDecryptedCardDataLen());
		System.out.print(" ");
		System.out.print(getDefaultEncryptedCardDataHex());
		System.out.print(" ");
		System.out.print(getDefaultKsnHex());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultReplenishCardId());
		System.out.print(" ");
		System.out.print(getDefaultReplenishConsumerId());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("batch ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultTranResult());
		System.out.print(" ");
		System.out.print(getDefaultTranDetails());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("getCardInfoEncrypted ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultCardReaderType());
		System.out.print(" ");
		System.out.print(getDefaultDecryptedCardDataLen());
		System.out.print(" ");
		System.out.print(getDefaultEncryptedCardDataHex());
		System.out.print(" ");
		System.out.print(getDefaultKsnHex());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("getCardInfoPlain ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultCardData());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("tokenizeEncrypted ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultCardReaderType());
		System.out.print(" ");
		System.out.print(getDefaultDecryptedCardDataLen());
		System.out.print(" ");
		System.out.print(getDefaultEncryptedCardDataHex());
		System.out.print(" ");
		System.out.print(getDefaultKsnHex());
		System.out.print(" ");
		System.out.print(getDefaultBillingPostalCode());
		System.out.print(" ");
		System.out.print(getDefaultBillingAddress());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("tokenizePlain ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultCardNumber());
		System.out.print(" ");
		System.out.print(getDefaultExpirationDate());
		System.out.print(" ");
		System.out.print(getDefaultSecurityCode());
		System.out.print(" ");
		System.out.print(getDefaultCardHolder());
		System.out.print(" ");
		System.out.print(getDefaultBillingPostalCode());
		System.out.print(" ");
		System.out.print(getDefaultBillingAddress());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("processUpdates ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultUpdateStatus());
		System.out.print(" ");
		System.out.print(getDefaultProtocolVersion());
		System.out.print(" ");
		System.out.print(getDefaultAppType());
		System.out.print(" ");
		System.out.print(getDefaultAppVersion());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("replenishCash ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultReplenishCardId());
		System.out.print(" ");
		System.out.print(getDefaultReplenishConsumerId());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("getCardIdPlain ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultCardData());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("getCardIdEncrypted ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultCardReaderType());
		System.out.print(" ");
		System.out.print(getDefaultDecryptedCardDataLen());
		System.out.print(" ");
		System.out.print(getDefaultEncryptedCardDataHex());
		System.out.print(" ");
		System.out.print(getDefaultKsnHex());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("cash ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultTranUTCTimeMs());
		System.out.print(" ");
		System.out.print(getDefaultTranUTCOffsetMs());
		System.out.print(" ");
		System.out.print(getDefaultTranResult());
		System.out.print(" ");
		System.out.print(getDefaultTranDetails());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("chargePlain ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultCardData());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultTranResult());
		System.out.print(" ");
		System.out.print(getDefaultTranDetails());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("chargeEncrypted ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultCardReaderType());
		System.out.print(" ");
		System.out.print(getDefaultDecryptedCardDataLen());
		System.out.print(" ");
		System.out.print(getDefaultEncryptedCardDataHex());
		System.out.print(" ");
		System.out.print(getDefaultKsnHex());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultTranResult());
		System.out.print(" ");
		System.out.print(getDefaultTranDetails());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("authEncrypted ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultCardReaderType());
		System.out.print(" ");
		System.out.print(getDefaultDecryptedCardDataLen());
		System.out.print(" ");
		System.out.print(getDefaultEncryptedCardDataHex());
		System.out.print(" ");
		System.out.print(getDefaultKsnHex());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("uploadDeviceInfo ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("uploadFile ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultFileType());
		System.out.print(" ");
		System.out.print(getDefaultFilePath());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("untokenize ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultCardId());
		System.out.print(" ");
		System.out.print(getDefaultTokenHex());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
		System.out.print("authPlain ");
		System.out.print(getDefaultUrl());
		System.out.print(" ");
		System.out.print(getDefaultUsername());
		System.out.print(" ");
		System.out.print(getDefaultPassword());
		System.out.print(" ");
		System.out.print(getDefaultSerialNumber());
		System.out.print(" ");
		System.out.print(getDefaultTranId());
		System.out.print(" ");
		System.out.print(getDefaultAmount());
		System.out.print(" ");
		System.out.print(getDefaultCardData());
		System.out.print(" ");
		System.out.print(getDefaultEntryType());
		System.out.print(" ");
		System.out.print(getDefaultAttributes());
		System.out.println();
       
    }
        
	protected void testReplenishPlain(String url, String username, String password, String serialNumber, long tranId, long amount, String cardData, String entryType, long replenishCardId, long replenishConsumerId, String attributes) throws Exception {
		Ec2Stub.ReplenishPlain request = new Ec2Stub.ReplenishPlain();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardData(cardData);
		request.setEntryType(entryType);
		request.setReplenishCardId(replenishCardId);
		request.setReplenishConsumerId(replenishConsumerId);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.ReplenishPlainResponse response;
		try {
			response = ePortConnect.replenishPlain(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testReplenishEncrypted(String url, String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, long replenishCardId, long replenishConsumerId, String attributes) throws Exception {
		Ec2Stub.ReplenishEncrypted request = new Ec2Stub.ReplenishEncrypted();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setEntryType(entryType);
		request.setReplenishCardId(replenishCardId);
		request.setReplenishConsumerId(replenishConsumerId);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.ReplenishEncryptedResponse response;
		try {
			response = ePortConnect.replenishEncrypted(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testBatch(String url, String username, String password, String serialNumber, long tranId, long amount, String tranResult, String tranDetails, String attributes) throws Exception {
		Ec2Stub.Batch request = new Ec2Stub.Batch();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.BatchResponse response;
		try {
			response = ePortConnect.batch(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testGetCardInfoEncrypted(String url, String username, String password, String serialNumber, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) throws Exception {
		Ec2Stub.GetCardInfoEncrypted request = new Ec2Stub.GetCardInfoEncrypted();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setEntryType(entryType);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.GetCardInfoEncryptedResponse response;
		try {
			response = ePortConnect.getCardInfoEncrypted(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testGetCardInfoPlain(String url, String username, String password, String serialNumber, String cardData, String entryType, String attributes) throws Exception {
		Ec2Stub.GetCardInfoPlain request = new Ec2Stub.GetCardInfoPlain();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setCardData(cardData);
		request.setEntryType(entryType);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.GetCardInfoPlainResponse response;
		try {
			response = ePortConnect.getCardInfoPlain(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testTokenizeEncrypted(String url, String username, String password, String serialNumber, long tranId, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingPostalCode, String billingAddress, String attributes) throws Exception {
		Ec2Stub.TokenizeEncrypted request = new Ec2Stub.TokenizeEncrypted();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setBillingPostalCode(billingPostalCode);
		request.setBillingAddress(billingAddress);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.TokenizeEncryptedResponse response;
		try {
			response = ePortConnect.tokenizeEncrypted(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testTokenizePlain(String url, String username, String password, String serialNumber, long tranId, String cardNumber, String expirationDate, String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes) throws Exception {
		Ec2Stub.TokenizePlain request = new Ec2Stub.TokenizePlain();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setCardNumber(cardNumber);
		request.setExpirationDate(expirationDate);
		request.setSecurityCode(securityCode);
		request.setCardHolder(cardHolder);
		request.setBillingPostalCode(billingPostalCode);
		request.setBillingAddress(billingAddress);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.TokenizePlainResponse response;
		try {
			response = ePortConnect.tokenizePlain(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testProcessUpdates(String url, String username, String password, String serialNumber, int updateStatus, int protocolVersion, String appType, String appVersion, String attributes) throws Exception {
		Ec2Stub.ProcessUpdates request = new Ec2Stub.ProcessUpdates();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setUpdateStatus(updateStatus);
		request.setProtocolVersion(protocolVersion);
		request.setAppType(appType);
		request.setAppVersion(appVersion);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.ProcessUpdatesResponse response;
		try {
			response = ePortConnect.processUpdates(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testReplenishCash(String url, String username, String password, String serialNumber, long tranId, long amount, long replenishCardId, long replenishConsumerId, String attributes) throws Exception {
		Ec2Stub.ReplenishCash request = new Ec2Stub.ReplenishCash();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setReplenishCardId(replenishCardId);
		request.setReplenishConsumerId(replenishConsumerId);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.ReplenishCashResponse response;
		try {
			response = ePortConnect.replenishCash(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testGetCardIdPlain(String url, String username, String password, String serialNumber, String cardData, String entryType, String attributes) throws Exception {
		Ec2Stub.GetCardIdPlain request = new Ec2Stub.GetCardIdPlain();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setCardData(cardData);
		request.setEntryType(entryType);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.GetCardIdPlainResponse response;
		try {
			response = ePortConnect.getCardIdPlain(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testGetCardIdEncrypted(String url, String username, String password, String serialNumber, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) throws Exception {
		Ec2Stub.GetCardIdEncrypted request = new Ec2Stub.GetCardIdEncrypted();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setEntryType(entryType);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.GetCardIdEncryptedResponse response;
		try {
			response = ePortConnect.getCardIdEncrypted(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testCash(String url, String username, String password, String serialNumber, long tranId, long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails, String attributes) throws Exception {
		Ec2Stub.Cash request = new Ec2Stub.Cash();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setTranUTCTimeMs(tranUTCTimeMs);
		request.setTranUTCOffsetMs(tranUTCOffsetMs);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.CashResponse response;
		try {
			response = ePortConnect.cash(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testChargePlain(String url, String username, String password, String serialNumber, long tranId, long amount, String cardData, String entryType, String tranResult, String tranDetails, String attributes) throws Exception {
		Ec2Stub.ChargePlain request = new Ec2Stub.ChargePlain();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardData(cardData);
		request.setEntryType(entryType);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.ChargePlainResponse response;
		try {
			response = ePortConnect.chargePlain(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testChargeEncrypted(String url, String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String tranResult, String tranDetails, String attributes) throws Exception {
		Ec2Stub.ChargeEncrypted request = new Ec2Stub.ChargeEncrypted();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setEntryType(entryType);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.ChargeEncryptedResponse response;
		try {
			response = ePortConnect.chargeEncrypted(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testAuthEncrypted(String url, String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) throws Exception {
		Ec2Stub.AuthEncrypted request = new Ec2Stub.AuthEncrypted();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setEntryType(entryType);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.AuthEncryptedResponse response;
		try {
			response = ePortConnect.authEncrypted(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testUploadDeviceInfo(String url, String username, String password, String serialNumber, String attributes) throws Exception {
		Ec2Stub.UploadDeviceInfo request = new Ec2Stub.UploadDeviceInfo();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.UploadDeviceInfoResponse response;
		try {
			response = ePortConnect.uploadDeviceInfo(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testUploadFile(String url, String username, String password, String serialNumber, int fileType, File filePath, String attributes) throws Exception {
		Ec2Stub.UploadFile request = new Ec2Stub.UploadFile();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setFileName(filePath.getName());
		request.setFileType(fileType);
		request.setFileSize(filePath.length());
		request.setFileContent(new EC2DataHandler(new FileDataSource(filePath)));
		request.setFileName(filePath.getName());
		request.setFileSize(filePath.length());
		request.setFileContent(new EC2DataHandler(new FileDataSource(filePath)));
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.UploadFileResponse response;
		try {
			response = ePortConnect.uploadFile(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testUntokenize(String url, String username, String password, String serialNumber, long cardId, String tokenHex, String attributes) throws Exception {
		Ec2Stub.Untokenize request = new Ec2Stub.Untokenize();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setCardId(cardId);
		request.setTokenHex(tokenHex);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.UntokenizeResponse response;
		try {
			response = ePortConnect.untokenize(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}

	protected void testAuthPlain(String url, String username, String password, String serialNumber, long tranId, long amount, String cardData, String entryType, String attributes) throws Exception {
		Ec2Stub.AuthPlain request = new Ec2Stub.AuthPlain();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardData(cardData);
		request.setEntryType(entryType);
		request.setAttributes(attributes);
		Ec2Stub ePortConnect = getService(url);
		Ec2Stub.AuthPlainResponse response;
		try {
			response = ePortConnect.authPlain(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (response == null || response.get_return() == null)
			throw new Exception("Received null response");
		handleResponse(response.get_return());
	}


    
	@Test
	public void testReplenishPlain() throws Exception {
		testReplenishPlain(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultCardData(), getDefaultEntryType(), getDefaultReplenishCardId(), getDefaultReplenishConsumerId(), getDefaultAttributes());
	}

	@Test
	public void testReplenishEncrypted() throws Exception {
		testReplenishEncrypted(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultCardReaderType(), getDefaultDecryptedCardDataLen(), getDefaultEncryptedCardDataHex(), getDefaultKsnHex(), getDefaultEntryType(), getDefaultReplenishCardId(), getDefaultReplenishConsumerId(), getDefaultAttributes());
	}

	@Test
	public void testBatch() throws Exception {
		testBatch(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultTranResult(), getDefaultTranDetails(), getDefaultAttributes());
	}

	@Test
	public void testGetCardInfoEncrypted() throws Exception {
		testGetCardInfoEncrypted(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultCardReaderType(), getDefaultDecryptedCardDataLen(), getDefaultEncryptedCardDataHex(), getDefaultKsnHex(), getDefaultEntryType(), getDefaultAttributes());
	}

	@Test
	public void testGetCardInfoPlain() throws Exception {
		testGetCardInfoPlain(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultCardData(), getDefaultEntryType(), getDefaultAttributes());
	}

	@Test
	public void testTokenizeEncrypted() throws Exception {
		testTokenizeEncrypted(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultCardReaderType(), getDefaultDecryptedCardDataLen(), getDefaultEncryptedCardDataHex(), getDefaultKsnHex(), getDefaultBillingPostalCode(), getDefaultBillingAddress(), getDefaultAttributes());
	}

	@Test
	public void testTokenizePlain() throws Exception {
		testTokenizePlain(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultCardNumber(), getDefaultExpirationDate(), getDefaultSecurityCode(), getDefaultCardHolder(), getDefaultBillingPostalCode(), getDefaultBillingAddress(), getDefaultAttributes());
	}

	@Test
	public void testProcessUpdates() throws Exception {
		testProcessUpdates(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultUpdateStatus(), getDefaultProtocolVersion(), getDefaultAppType(), getDefaultAppVersion(), getDefaultAttributes());
	}

	@Test
	public void testReplenishCash() throws Exception {
		testReplenishCash(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultReplenishCardId(), getDefaultReplenishConsumerId(), getDefaultAttributes());
	}

	@Test
	public void testGetCardIdPlain() throws Exception {
		testGetCardIdPlain(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultCardData(), getDefaultEntryType(), getDefaultAttributes());
	}

	@Test
	public void testGetCardIdEncrypted() throws Exception {
		testGetCardIdEncrypted(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultCardReaderType(), getDefaultDecryptedCardDataLen(), getDefaultEncryptedCardDataHex(), getDefaultKsnHex(), getDefaultEntryType(), getDefaultAttributes());
	}

	@Test
	public void testCash() throws Exception {
		testCash(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultTranUTCTimeMs(), getDefaultTranUTCOffsetMs(), getDefaultTranResult(), getDefaultTranDetails(), getDefaultAttributes());
	}

	@Test
	public void testChargePlain() throws Exception {
		testChargePlain(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultCardData(), getDefaultEntryType(), getDefaultTranResult(), getDefaultTranDetails(), getDefaultAttributes());
	}

	@Test
	public void testChargeEncrypted() throws Exception {
		testChargeEncrypted(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultCardReaderType(), getDefaultDecryptedCardDataLen(), getDefaultEncryptedCardDataHex(), getDefaultKsnHex(), getDefaultEntryType(), getDefaultTranResult(), getDefaultTranDetails(), getDefaultAttributes());
	}

	@Test
	public void testAuthEncrypted() throws Exception {
		testAuthEncrypted(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultCardReaderType(), getDefaultDecryptedCardDataLen(), getDefaultEncryptedCardDataHex(), getDefaultKsnHex(), getDefaultEntryType(), getDefaultAttributes());
	}

	@Test
	public void testUploadDeviceInfo() throws Exception {
		testUploadDeviceInfo(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultAttributes());
	}

	@Test
	public void testUploadFile() throws Exception {
		testUploadFile(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultFileType(), getDefaultFilePath(), getDefaultAttributes());
	}

	@Test
	public void testUntokenize() throws Exception {
		testUntokenize(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultCardId(), getDefaultTokenHex(), getDefaultAttributes());
	}

	@Test
	public void testAuthPlain() throws Exception {
		testAuthPlain(getDefaultUrl(), getDefaultUsername(), getDefaultPassword(), getDefaultSerialNumber(), getDefaultTranId(), getDefaultAmount(), getDefaultCardData(), getDefaultEntryType(), getDefaultAttributes());
	}

}
