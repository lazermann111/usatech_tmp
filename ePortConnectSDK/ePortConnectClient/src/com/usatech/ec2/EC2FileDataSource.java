package com.usatech.ec2;

import java.io.File;
import java.io.Serializable;

import javax.activation.FileDataSource;

public class EC2FileDataSource extends FileDataSource implements Serializable {
	private static final long serialVersionUID = -3248702697942606093L;

	public EC2FileDataSource(File file) {
		super(file);
	}
}
