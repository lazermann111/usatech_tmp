package com.usatech.ec2;

import java.util.concurrent.ExecutionException;

public class EC2ExecutionException extends ExecutionException {
	private static final long serialVersionUID = -563966781328250625L;

	public EC2ExecutionException(String message) {
		super(message);
	}

	public EC2ExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

}
