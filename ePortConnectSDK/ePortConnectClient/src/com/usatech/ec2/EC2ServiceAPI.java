package com.usatech.ec2;

public interface EC2ServiceAPI {
	// Process a card authorization with card data from an encrypting card reader
	public EC2AuthResponse authEncrypted(String username, String password, String serialNumber, long tranId, long amount, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, 
			String attributes);
	
	// Process a card authorization with card data from a non-encrypting card reader 
	public EC2AuthResponse authPlain(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String entryType, String attributes);
	
	// Settle a previously authorized transaction
	public EC2Response batch(String username, String password, String serialNumber, long tranId, 
			long amount, String tranResult, String tranDetails, String attributes);
	
	// Upload a cash transaction
	public EC2Response cash(String username, String password, String serialNumber, long tranId, 
			long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails, String attributes);
	
	// Authorize and settle a transaction with card data from an encrypting card reader
	public EC2AuthResponse chargeEncrypted(String username, String password, String serialNumber, long tranId, 
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String entryType, String tranResult, String tranDetails, String attributes);
	
	// Authorize and settle a transaction with card data from a non-encrypting card reader 
	public EC2AuthResponse chargePlain(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String entryType, String tranResult, String tranDetails, String attributes);
	
	// Upload a file to the Server
	public EC2Response uploadFile(String username, String password, String serialNumber, String fileName, 
			int fileType, long fileSize, EC2DataHandler fileContent, String attributes);
	
	// Download updates from the Server
	public EC2ProcessUpdatesResponse processUpdates(String username, String password, String serialNumber, int updateStatus, 
			int protocolVersion, String appType, String appVersion, String attributes);
	
	// Upload device information to the Server
	public EC2Response uploadDeviceInfo(String username, String password, String serialNumber, String attributes);
	
	// Get MORE card Id for card data from an encrypting card reader
	public EC2CardId getCardIdEncrypted(String username, String password, String serialNumber, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, 
			String attributes);
	
	// Get MORE card Id for card data from a non-encrypting card reader 
	public EC2CardId getCardIdPlain(String username, String password, String serialNumber, 
			String cardData, String entryType, String attributes);
	
	// Get MORE card information for card data from an encrypting card reader
	public EC2CardInfo getCardInfoEncrypted(String username, String password, String serialNumber, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, 
			String attributes);
	
	// Get MORE card information for card data from a non-encrypting card reader 
	public EC2CardInfo getCardInfoPlain(String username, String password, String serialNumber, 
			String cardData, String entryType, String attributes);
	
	// Replenish MORE card identified by replenishCardId and replenishConsumerId with card data from an encrypting card reader
	public EC2ReplenishResponse replenishEncrypted(String username, String password, String serialNumber, long tranId, long amount, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, 
			long replenishCardId, long replenishConsumerId, String attributes);
	
	// Replenish MORE card identified by replenishCardId and replenishConsumerId with card data from a non-encrypting card reader 
	public EC2ReplenishResponse replenishPlain(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String entryType, long replenishCardId, long replenishConsumerId, String attributes);
	
	// Replenish MORE card identified by replenishCardId and replenishConsumerId with cash 
	public EC2ReplenishResponse replenishCash(String username, String password, String serialNumber, long tranId, 
			long amount, long replenishCardId, long replenishConsumerId, String attributes);

	// Get token for card data for use in future authorizations
	public EC2TokenResponse tokenizePlain(String username, String password, String serialNumber, long tranId, String cardNumber, String expirationDate, String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes);

	// Get token for card data for use in future authorizations
	public EC2TokenResponse tokenizeEncrypted(String username, String password, String serialNumber, long tranId, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingPostalCode, String billingAddress, String attributes);

	// Remove the token so it may not be used anymore
	public EC2Response untokenize(String username, String password, String serialNumber, long cardId, String tokenHex, String attributes);

	// Get pre-existing token for card data for use in future authorizations using the cardId
	public EC2TokenResponse retokenize(String username, String password, String serialNumber, long cardId, String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes);
}
