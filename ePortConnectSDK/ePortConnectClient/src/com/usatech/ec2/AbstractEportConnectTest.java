package com.usatech.ec2;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.util.TimeZone;

import org.apache.axis2.databinding.types.HexBinary;

public abstract class AbstractEportConnectTest {
	protected long tranId = System.currentTimeMillis() / 1000;
	protected abstract String getDefaultUrl();

	protected long getDefaultTranId() {
		return tranId++;
	}

	protected String getDefaultUsername() {
		return "SEA_ECC_acc1";
	}

	protected String getDefaultPassword() {
		return "U$nJqxfyHX43cfK!UyA5"; //ecc
	//	return "gVPkyP6hVB6c8TtKbM3j"; //int
	}

	protected String getDefaultSerialNumber() {
		return "K3MTB000007";
	}

	protected long getDefaultAmount() {
		return 250;
	}

	protected String getDefaultCardData() {
		return "4055011111111111"; 
	}

	protected String getDefaultEntryType() {
		return "S";
	}

	protected long getDefaultReplenishCardId() {
		return 126835; //my ecc
	//    return 744702; //my int
	}

	protected long getDefaultReplenishConsumerId() {
	//	return 45310; //my int
	  return 1123257; // my ecc
	}

	protected String getDefaultAttributes() {
		return "";
	}

	protected int getDefaultCardReaderType() {
		return 3;
	}

	protected int getDefaultDecryptedCardDataLen() {
		return getDefaultCardData().length();
	}

	protected String getDefaultEncryptedCardDataHex() {
		return "26ACC27A12665A929CF36DAE47BC49939A3591895B43ACA39D7969BA6981F1F26FDC50EE159C34BD";
	}

	protected String getDefaultKsnHex() {
		return "62994996340010200090";
	}

	protected String getDefaultTranResult() {
		return "Q";
	}

	protected String getDefaultTranDetails() {
		return "A0|200|220|1|Thing|406|30|1|Tip";
	}

	protected String getDefaultBillingPostalCode() {
		return "66666";
	}

	protected String getDefaultBillingAddress() {
		return "";
	}

	protected String getDefaultCardNumber() {
		return "4055011111111111";
	}

	protected String getDefaultExpirationDate() {
		return "2212";
	}

	protected String getDefaultSecurityCode() {
		return "1987";
	}

	protected String getDefaultCardHolder() {
		return "";
	}

	protected int getDefaultUpdateStatus() {
		return 0;
	}

	protected int getDefaultProtocolVersion() {
		return 2;
	}

	protected String getDefaultAppType() {
		return "ECTest";
	}

	protected String getDefaultAppVersion() {
		return "1.0.0";
	}

	protected long getDefaultTranUTCTimeMs() {
		return System.currentTimeMillis();
	}

	protected int getDefaultTranUTCOffsetMs() {
		return TimeZone.getDefault().getOffset(getDefaultTranUTCTimeMs());
	}

	protected File getDefaultFilePath() {
		return new File("bin/log4j.properties");
	}

	protected int getDefaultFileType() {
		return 16;
	}

	protected long getDefaultCardId() {
		return 1000454331;
	}

	protected String getDefaultTokenHex() {
		return "2E48D6C1400328450482B1BCD757671B";
	}

	protected long parseLong(String value) {
		try {
			return Long.parseLong(value);
		} catch(NumberFormatException e) {
			System.out.println("ERROR: Could not convert '" + value + "' to a long");
			return 0L;
		}
	}

	protected int parseInt(String value) {
		try {
			return Integer.parseInt(value);
		} catch(NumberFormatException e) {
			System.out.println("ERROR: Could not convert '" + value + "' to an integer");
			return 0;
		}
	}

	protected byte[] parseByteArray(String value) {
		try {
			return HexBinary.decode(value);
		} catch(IllegalArgumentException e) {
			System.out.println("ERROR: Could not convert '" + value + "' to a byte array");
			return new byte[0];
		}
	}

	protected void handleResponse(Object response) throws Exception {
		if(response == null)
			throw new Exception("Received null response");
		StringBuilder sb = new StringBuilder();
		sb.append(response.getClass().getSimpleName()).append(' ');
		boolean first = true;
		int returnCode = 0;
		for(PropertyDescriptor pd : Introspector.getBeanInfo(response.getClass()).getPropertyDescriptors()) {
			if(pd.getReadMethod() != null && !"class".equals(pd.getName()) && pd.getReadMethod().getParameterTypes().length == 0) {
				if(first)
					first = false;
				else
					sb.append(", ");
				Object value = pd.getReadMethod().invoke(response);
				sb.append(pd.getName()).append(": ").append(value);
				if(pd.getName().equals("returnCode") && value instanceof Number)
					returnCode = ((Number) value).intValue();
			}
		}
		switch(returnCode) {
			case 0:
				throw new Exception(sb.toString());
			default:
				System.out.println(sb.toString());
		}
	}

	protected String unescapeBackslash(String s) {
		if(s == null || s.length() == 0)
			return s;
		int pos = s.indexOf('\\');
		if(pos < 0)
			return s;
		StringBuilder sb = new StringBuilder(s.length());
		int start = 0;
		OUTER: while(true) {
			sb.append(s, start, pos);
			if(pos + 1 >= s.length())
				break;
			char ch = s.charAt(pos + 1);
			start = pos + 2;
			switch(ch) {
				case 't':
					sb.append('\t');
					break;
				case 'b':
					sb.append('\b');
					break;
				case 'n':
					sb.append('\n');
					break;
				case 'f':
					sb.append('\f');
					break;
				case 'r':
					sb.append('\r');
					break;
				case 'u':
					char[] unicode = new char[4];
					for(int i = 0; i < 4; i++) {
						if(start >= s.length()) {
							sb.append("\\u");
							for(int k = 0; k < i; k++)
								sb.append(unicode[k]);
							break OUTER;
						}
						unicode[i] = s.charAt(start++);
					}
					sb.append((char) Integer.parseInt(new String(unicode), 16));
					break;
				default:
					sb.append(ch);
					break;
			}
			pos = s.indexOf('\\', start);
			if(pos < 0) {
				sb.append(s, start, s.length());
				break;
			}
		}
		return sb.toString();
	}
}
