package com.usatech.ec2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import javax.activation.FileDataSource;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class EC2JSONClient implements EC2ServiceAPI {
	private String fullPath;
	private Gson gson;
	
	public EC2JSONClient(String fullPath) {
		this.fullPath = addSuffixIfAbsent(fullPath, "/");
		this.gson = new Gson();
	}
	
	private String addSuffixIfAbsent(String object, String suffix) {
		if (!object.endsWith(suffix)) {
			return object + suffix;
		}
		return object;
	}

	public String post(String function, String request) {
		HttpURLConnection conn;
		BufferedReader bufferedReader = null;
		StringBuilder response = new StringBuilder();
		try {
			URL url = new URL(this.fullPath + function);
			conn = (HttpURLConnection)url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestMethod("POST");

			OutputStream outputStream = conn.getOutputStream();
			outputStream.write(request == null ? "".getBytes() : request.getBytes());
			outputStream.flush();
			bufferedReader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			while ((output = bufferedReader.readLine()) != null) {
				if (response.length() > 0) {
					response.append("\n");
				}
				response.append(output);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ignored) {
				}
			}
			// Commented to allow Java to use connection pooling when call to openConnection(..)
			//if (conn != null) {
			//conn.disconnect();
			//}
		}
		return response.toString();
	}

	@Override
	public EC2AuthResponse authEncrypted(String username, String password, String serialNumber, long tranId, long amount,
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType,
			String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("cardReaderType", cardReaderType);
		request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
		request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
		request.addProperty("ksnHex", ksnHex);
		request.addProperty("entryType", entryType);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("authEncrypted", gson.toJson(request)), EC2AuthResponse.class);
	}

	@Override
	public EC2AuthResponse authPlain(String username, String password, String serialNumber, long tranId, long amount,
			String cardData, String entryType, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("cardData", cardData);
		request.addProperty("entryType", entryType);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("authPlain", gson.toJson(request)), EC2AuthResponse.class);
	}

	@Override
	public EC2Response batch(String username, String password, String serialNumber, long tranId, long amount,
			String tranResult, String tranDetails, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("tranResult", tranResult);
		request.addProperty("tranDetails", tranDetails);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("batch", gson.toJson(request)), EC2Response.class);
	}

	@Override
	public EC2Response cash(String username, String password, String serialNumber, long tranId, long amount,
			long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("tranUTCTimeMs", tranUTCTimeMs);
		request.addProperty("tranUTCOffsetMs", tranUTCOffsetMs);
		request.addProperty("tranResult", tranResult);
		request.addProperty("tranDetails", tranDetails);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("cash", gson.toJson(request)), EC2Response.class);
	}

	@Override
	public EC2AuthResponse chargeEncrypted(String username, String password, String serialNumber, long tranId,
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String entryType, String tranResult, String tranDetails, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("cardReaderType", cardReaderType);
		request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
		request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
		request.addProperty("ksnHex", ksnHex);
		request.addProperty("entryType", entryType);
		request.addProperty("tranResult", tranResult);
		request.addProperty("tranDetails", tranDetails);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("chargeEncrypted", gson.toJson(request)), EC2AuthResponse.class);
	}

	@Override
	public EC2AuthResponse chargePlain(String username, String password, String serialNumber, long tranId, long amount,
			String cardData, String entryType, String tranResult, String tranDetails, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("cardData", cardData);
		request.addProperty("entryType", entryType);
		request.addProperty("tranResult", tranResult);
		request.addProperty("tranDetails", tranDetails);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("chargePlain", gson.toJson(request)), EC2AuthResponse.class);
	}

	@Override
	public EC2Response uploadFile(String username, String password, String serialNumber, String fileName, int fileType,
			long fileSize, EC2DataHandler fileContent, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("fileName", fileName);
		request.addProperty("fileType", fileType);
		request.addProperty("fileSize", fileSize);
		request.addProperty("fileContent", Base64.getEncoder().encodeToString(fileContent.getFileBytes()));
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("uploadFile", gson.toJson(request)), EC2Response.class);
	}

	@Override
	public EC2ProcessUpdatesResponse processUpdates(String username, String password, String serialNumber,
			int updateStatus, int protocolVersion, String appType, String appVersion, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("updateStatus", updateStatus);
		request.addProperty("protocolVersion", protocolVersion);
		request.addProperty("appType", appType);
		request.addProperty("appVersion", appVersion);
		request.addProperty("attributes", attributes);
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(post("processUpdates", gson.toJson(request))).getAsJsonObject();

		EC2ProcessUpdatesResponse response = new EC2ProcessUpdatesResponse();
		response.setActionCode(jsonObject.get("actionCode").getAsInt());
		response.setAttributes(jsonObject.get("attributes").getAsString());
		populateFileInfo(jsonObject, response);
		response.setFileName(jsonObject.get("fileName").getAsString());
		response.setFileSize(jsonObject.get("fileSize").getAsLong());
		response.setFileType(jsonObject.get("fileType").getAsInt());
		response.setNewPassword(jsonObject.get("newPassword").getAsString());
		response.setNewTranId(jsonObject.get("newTranId").getAsLong());
		response.setNewUsername(jsonObject.get("newUsername").getAsString());
		response.setReturnCode(jsonObject.get("returnCode").getAsInt());
		response.setReturnMessage(jsonObject.get("returnMessage").getAsString());
		response.setSerialNumber(jsonObject.get("serialNumber").getAsString());

		return response;
	}

	@Override
	public EC2Response uploadDeviceInfo(String username, String password, String serialNumber, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("uploadDeviceInfo", gson.toJson(request)), EC2Response.class);
	}

	@Override
	public EC2CardId getCardIdEncrypted(String username, String password, String serialNumber, int cardReaderType,
			int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("cardReaderType", cardReaderType);
		request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
		request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
		request.addProperty("ksnHex", ksnHex);
		request.addProperty("entryType", entryType);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("getCardIdEncrypted", gson.toJson(request)), EC2CardId.class);
	}

	@Override
	public EC2CardId getCardIdPlain(String username, String password, String serialNumber, String cardData,
			String entryType, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("cardData", cardData);
		request.addProperty("entryType", entryType);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("getCardIdPlain", gson.toJson(request)), EC2CardId.class);
	}

	@Override
	public EC2CardInfo getCardInfoEncrypted(String username, String password, String serialNumber, int cardReaderType,
			int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("cardReaderType", cardReaderType);
		request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
		request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
		request.addProperty("ksnHex", ksnHex);
		request.addProperty("entryType", entryType);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("getCardInfoEncrypted", gson.toJson(request)), EC2CardInfo.class);
	}

	@Override
	public EC2CardInfo getCardInfoPlain(String username, String password, String serialNumber, String cardData,
			String entryType, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("cardData", cardData);
		request.addProperty("entryType", entryType);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("getCardInfoPlain", gson.toJson(request)), EC2CardInfo.class);
	}

	@Override
	public EC2ReplenishResponse replenishEncrypted(String username, String password, String serialNumber, long tranId,
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String entryType, long replenishCardId, long replenishConsumerId, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("cardReaderType", cardReaderType);
		request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
		request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
		request.addProperty("ksnHex", ksnHex);
		request.addProperty("entryType", entryType);
		request.addProperty("replenishCardId", replenishCardId);
		request.addProperty("replenishConsumerId", replenishConsumerId);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("replenishEncrypted", gson.toJson(request)), EC2ReplenishResponse.class);
	}

	@Override
	public EC2ReplenishResponse replenishPlain(String username, String password, String serialNumber, long tranId,
			long amount, String cardData, String entryType, long replenishCardId, long replenishConsumerId,
			String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("cardData", cardData);
		request.addProperty("entryType", entryType);
		request.addProperty("replenishCardId", replenishCardId);
		request.addProperty("replenishConsumerId", replenishConsumerId);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("replenishPlain", gson.toJson(request)), EC2ReplenishResponse.class);
	}

	@Override
	public EC2ReplenishResponse replenishCash(String username, String password, String serialNumber, long tranId,
			long amount, long replenishCardId, long replenishConsumerId, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("amount", amount);
		request.addProperty("replenishCardId", replenishCardId);
		request.addProperty("replenishConsumerId", replenishConsumerId);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("replenishCash", gson.toJson(request)), EC2ReplenishResponse.class);
	}

	@Override
	public EC2TokenResponse tokenizePlain(String username, String password, String serialNumber, long tranId,
			String cardNumber, String expirationDate, String securityCode, String cardHolder, String billingPostalCode,
			String billingAddress, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("cardNumber", cardNumber);
		request.addProperty("expirationDate", expirationDate);
		request.addProperty("securityCode", securityCode);
		request.addProperty("cardHolder", cardHolder);
		request.addProperty("billingPostalCode", billingPostalCode);
		request.addProperty("billingAddress", billingAddress);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("tokenizePlain", gson.toJson(request)), EC2TokenResponse.class);
	}

	@Override
	public EC2TokenResponse tokenizeEncrypted(String username, String password, String serialNumber, long tranId,
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String billingPostalCode, String billingAddress, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("tranId", tranId);
		request.addProperty("cardReaderType", cardReaderType);
		request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
		request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
		request.addProperty("ksnHex", ksnHex);
		request.addProperty("billingPostalCode", billingPostalCode);
		request.addProperty("billingAddress", billingAddress);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("tokenizeEncrypted", gson.toJson(request)), EC2TokenResponse.class);
	}

	@Override
	public EC2Response untokenize(String username, String password, String serialNumber, long cardId, String tokenHex,
			String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("cardId", cardId);
		request.addProperty("tokenHex", tokenHex);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("untokenize", gson.toJson(request)), EC2Response.class);
	}

	@Override
	public EC2TokenResponse retokenize(String username, String password, String serialNumber, long cardId,
			String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes) {
		JsonObject request = new JsonObject();
		request.addProperty("username", username);
		request.addProperty("password", password);
		request.addProperty("serialNumber", serialNumber);
		request.addProperty("cardId", cardId);
		request.addProperty("securityCode", securityCode);
		request.addProperty("cardHolder", cardHolder);
		request.addProperty("billingPostalCode", billingPostalCode);
		request.addProperty("billingAddress", billingAddress);
		request.addProperty("attributes", attributes);
		return gson.fromJson(post("retokenize", gson.toJson(request)), EC2TokenResponse.class);
	}

	private void populateFileInfo(JsonObject jsonObject, EC2ProcessUpdatesResponse response) {
		if (jsonObject.get("fileSize").getAsLong() > 0) {
			try {
				File file = File.createTempFile(jsonObject.get("fileName").getAsString(), null);
				byte[] fileContent = Base64.getDecoder().decode(jsonObject.get("fileContent").getAsString());
				Files.write(Paths.get(file.getAbsolutePath()), fileContent);
				response.setFileContent(new EC2DataHandler(new FileDataSource(file)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
