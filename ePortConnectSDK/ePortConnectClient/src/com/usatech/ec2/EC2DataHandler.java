package com.usatech.ec2;

import java.io.Serializable;
import java.net.URL;

import javax.activation.DataHandler;
import javax.activation.DataSource;

public class EC2DataHandler extends DataHandler implements Serializable {
	private static final long serialVersionUID = 3712723634066364459L;
	
	protected byte[] fileBytes;

	public EC2DataHandler(DataSource ds) {
		super(ds);
	}

	public EC2DataHandler(Object obj, String mimeType) {
		super(obj, mimeType);
	}
	
	public EC2DataHandler(URL url) {
		super(url);
	}

	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
}
