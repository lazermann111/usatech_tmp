package com.usatech.ec2;

import java.io.Serializable;

public class EC2AuthResponse extends EC2Response implements Serializable {
	private static final long serialVersionUID = -2642793573612251255L;
	
	protected long approvedAmount = 0;
	protected long balanceAmount = 0;
	protected long cardId = 0;
	protected String cardType = "";
	protected long consumerId = 0;
	
	public long getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(long approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	public long getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
}
