package com.usatech.ec2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.activation.DataSource;

public class EC2DataSource implements DataSource, Serializable {
	private static final long serialVersionUID = 19423499548334125L;
	
	protected InputStream inputStream;
	
	public EC2DataSource(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return null;
	}

	@Override
	public String getContentType() {
		return "application/octet-stream";
	}

	@Override
	public String getName() {
		return "EC2DataSource";
	}
}
