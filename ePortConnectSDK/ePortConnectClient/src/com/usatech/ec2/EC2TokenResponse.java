package com.usatech.ec2;

import java.io.Serializable;

public class EC2TokenResponse extends EC2Response implements Serializable {
	private static final long serialVersionUID = -2689182373612251255L;
	
	protected long cardId = 0;
	protected String cardType = "";
	protected long consumerId = 0;
	protected String tokenHex = "";
	
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	public String getTokenHex() {
		return tokenHex;
	}

	public void setTokenHex(String tokenHex) {
		this.tokenHex = tokenHex;
	}
}
