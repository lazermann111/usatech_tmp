package com.usatech.ec2;

import java.io.Serializable;

public class EC2CardId extends EC2Response implements Serializable {
	private static final long serialVersionUID = 6022011835713212492L;
	
	protected long cardId = 0;
	
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}	
}
