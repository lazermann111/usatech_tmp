package com.usatech.ec2;

import java.net.MalformedURLException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.usatech.ec.ECResponse;

public class EC2ClientUtils {
	protected static EC2ServiceAPI eportConnect;
	protected static String eportConnectUrl;
	protected static String virtualDeviceSerialCd;

	public static String getEportConnectUrl() {
		return eportConnectUrl;
	}

	public static void setEportConnectUrl(String eportConnectUrl) throws MalformedURLException {
		String old = EC2ClientUtils.eportConnectUrl;
		if(eportConnectUrl == null || (eportConnectUrl = eportConnectUrl.trim()).isEmpty())
			eportConnect = null;
		else if(old == null || !old.equals(eportConnectUrl))
			eportConnect = (EC2ServiceAPI) new HessianProxyFactory().create(EC2ServiceAPI.class, eportConnectUrl);
		EC2ClientUtils.eportConnectUrl = eportConnectUrl;
	}

	public static long getGlobalAccountId(String cardNum) throws EC2ExecutionException {
		EC2CardId response = eportConnect.getCardIdPlain(null, null, getVirtualDeviceSerialCd(), cardNum, "N", null);
		if(response.getReturnCode() == ECResponse.RES_OK)
			return response.getCardId();
		throw new EC2ExecutionException("ePort Connect call did not succeed: " + response.getReturnMessage());
	}

	public static String getVirtualDeviceSerialCd() {
		return virtualDeviceSerialCd;
	}

	public static void setVirtualDeviceSerialCd(String virtualDeviceSerialCd) {
		EC2ClientUtils.virtualDeviceSerialCd = virtualDeviceSerialCd;
	}

	public static EC2ServiceAPI getEportConnect() {
		return eportConnect;
	}

}
