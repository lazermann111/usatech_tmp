package com.usatech.ec2;

import java.io.Serializable;

public class EC2CardInfo extends EC2Response implements Serializable {
	private static final long serialVersionUID = 6478015464179795934L;
	
	protected long cardId = 0;
	protected String cardType = "";
	protected long consumerId = 0;
	protected long balanceAmount = 0;
	
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}
	public long getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}	
}
