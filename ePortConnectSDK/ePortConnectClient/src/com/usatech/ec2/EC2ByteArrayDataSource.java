package com.usatech.ec2;

import java.io.Serializable;

import org.apache.axiom.attachments.ByteArrayDataSource;

public class EC2ByteArrayDataSource extends ByteArrayDataSource implements Serializable {
	private static final long serialVersionUID = 7948509700229737243L;

	public EC2ByteArrayDataSource(byte[] data, String type) {
        super(data, type);
	}
	
	public EC2ByteArrayDataSource(byte[] data) {
		super(data);
	}

}
