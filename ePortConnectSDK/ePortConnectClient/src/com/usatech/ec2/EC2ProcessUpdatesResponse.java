package com.usatech.ec2;


public class EC2ProcessUpdatesResponse extends EC2Response {
	private static final long serialVersionUID = -2325765467755764824L;
	private static final byte[] EMPTY_BYTES = new byte[0];
	public static final EC2DataHandler EMPTY_DATA_HANDLER = new EC2DataHandler(new EC2ByteArrayDataSource(EMPTY_BYTES, "application/octet-stream"));

	protected String fileName = "";
	protected int fileType = -1;
	protected long fileSize = 0;
	protected EC2DataHandler fileContent = EMPTY_DATA_HANDLER;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getFileType() {
		return fileType;
	}
	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public EC2DataHandler getFileContent() {
		return fileContent;
	}
	public void setFileContent(EC2DataHandler fileContent) {
		this.fileContent = fileContent;
	}
}
