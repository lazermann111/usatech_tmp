package com.usatech.ec;

import java.io.Serializable;

public class ECResponse implements Serializable {
	private static final long serialVersionUID = 7253899317276956836L;
	public static final int RES_FAILED = 0;
	public static final int RES_OK = 1;
	public static final int RES_APPROVED = 2;
	public static final int RES_DECLINED = 3;
	public static final int RES_PARTIALLY_APPROVED = 4;
	public static final int RES_OK_NO_UPDATE = 5;
	public static final int RES_OK_UPLOAD_FILE = 6;
	
	public static final int RES_RESTRICTED_DEBIT_CARD_TYPE = 7;
	public static final int RES_RESTRICTED_PAYMENT_METHOD = 8;
	public static final int RES_CVV_MISMATCH = 9;
	public static final int RES_AVS_MISMATCH = 10;
	public static final int RES_CVV_AND_AVS_MISMATCH = 11;

	protected String serialNumber;
	protected int returnCode = 0;
	protected String returnMessage = "Failed";
	protected String newUsername = "";
	protected String newPassword = "";
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public int getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	public String getNewUsername() {
		return newUsername;
	}
	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
