package com.usatech.ec;

import java.io.Serializable;

import org.apache.axiom.attachments.ByteArrayDataSource;

public class ECByteArrayDataSource extends ByteArrayDataSource implements Serializable {
	private static final long serialVersionUID = -4102854872322557230L;

	public ECByteArrayDataSource(byte[] data, String type) {
        super(data, type);
	}
	
	public ECByteArrayDataSource(byte[] data) {
		super(data);
	}

}
