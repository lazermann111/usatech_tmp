package com.usatech.ec;

import java.io.Serializable;

public class ECAuthResponse extends ECResponse implements Serializable {
	private static final long serialVersionUID = -4711738838689329947L;
	
	protected long approvedAmount = 0;
	
	public long getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(long approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
}
