package com.usatech.ec;


public class ECProcessUpdatesResponse extends ECResponse {
	private static final long serialVersionUID = -3275326082142493403L;
	private static final byte[] EMPTY_BYTES = new byte[0];
	public static final ECDataHandler EMPTY_DATA_HANDLER = new ECDataHandler(new ECByteArrayDataSource(EMPTY_BYTES, "application/octet-stream"));

	protected String fileName = "";
	protected int fileType = -1;
	protected long fileSize = 0;
	protected ECDataHandler fileContent = EMPTY_DATA_HANDLER;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getFileType() {
		return fileType;
	}
	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public ECDataHandler getFileContent() {
		return fileContent;
	}
	public void setFileContent(ECDataHandler fileContent) {
		this.fileContent = fileContent;
	}
}
