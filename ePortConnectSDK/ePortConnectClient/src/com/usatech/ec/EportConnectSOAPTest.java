package com.usatech.ec;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.junit.Test;

public class EportConnectSOAPTest {
	public static final String EPORT_CONNECT_SOAP_URL = "https://ec-ecc.usatech.com:9443/soap/ec";
	public static final String LIB_DIRECTORY = "lib";
	public static final String SERIAL_NUMBER = "K3MTB000001";
	public static final String USERNAME = "testaccount1";
	public static final String PASSWORD = "GnB24@+RwG#FvffG5ArZ";
	public static final String CARD_DATA = "6011000020000000=10120000000000000000";
	public static final String ENCRYPTED_CARD_DATA = "3A4AD6DF49B8B045D3ECCF9AE6B3C159B4A622929A5F77CB3FA3E7BF098EC061549E742EC8A701B2";
	public static final String KEY_SERIAL_NUMBER = "9011150B014759000009";
	public static final long TRANSACTION_ID = System.currentTimeMillis() / 1000;
	public static final String FILE_NAME = "lib/axis2-transport-local-1.6.2.jar";
	public static final int FILE_TYPE = 16;
	
	public static final int RES_FAILED = 0; //Transaction failed
	public static final int RES_OK = 1; //Transaction was successful
	public static final int RES_APPROVED = 2; //Auth or Charge was approved and transaction was submitted for settlement
	public static final int RES_DECLINED = 3; //Auth or Charge was declined
	public static final int RES_PARTIALLY_APPROVED = 4; //Auth was partially approved
	public static final int RES_OK_NO_UPDATE = 5; //Function succeeded, no updates pending
	public static final int RES_OK_UPLOAD_FILE = 6; //Function succeeded, received request to upload a file to the server
	
	public static final String TRAN_RESULT_SUCCESS = "S"; //Transaction was successful and receipt printed
	public static final String TRAN_RESULT_SUCCESS_NO_USE_PRINTER = "Q"; //Transaction was successful, receipt printer is not used
	public static final String TRAN_RESULT_SUCCESS_RECEIPT_PROBLEM = "R"; //Transaction was successful but there was a receipt printing problem
	public static final String TRAN_RESULT_SUCCESS_NO_RECEIPT_REQUESTED = "N"; //Transaction was successful and no receipt was requested
	public static final String TRAN_RESULT_FAILED = "F"; //Transaction failed
	public static final String TRAN_RESULT_CANCELLED = "C"; //Transaction cancelled
	public static final String TRAN_RESULT_TIMED_OUT = "T"; //Transaction timed out
	public static final String TRAN_RESULT_UNABLE_TO_AUTH = "U"; //Unable to authorize
	
	public static final String CARD_TYPE_SWIPED = "C"; //Swiped track 2 data
	public static final String CARD_TYPE_RFID = "R"; //Contactless track 2 data
	
	public static final int CARD_READER_MAGTEK_MAGNESAFE = 1; //MagTek encrypting card reader
	public static final int CARD_READER_IDTECH_SECUREMAG = 2; //ID TECH encrypting card reader
	
	public static final int FILE_TYPE_APPLICATION_UPGRADE = 5;
	public static final int FILE_TYPE_EXECUTABLE_FILE = 7;
	public static final int FILE_TYPE_KIOSK_GENERIC_FILE = 16;
	public static final int FILE_TYPE_LOG_FILE = 9;
	
	public static final int UPDATE_STATUS_NORMAL = 0;
	public static final int UPDATE_STATUS_RETRY = 1;
	
	protected static EcStub ePortConnect = null;
	
	static {
		System.setProperty("javax.net.ssl.trustStore", "resources/ssl/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "eportconnect");
	}

	// java -cp bin com.usatech.ec.EportConnectSOAPTest
	public static void main(String[] args) throws Exception {
		processMain(args, EPORT_CONNECT_SOAP_URL, EportConnectSOAPTest.class.getName());
	}
	
	public static String getUsage(String url, String className) {
		String separator = System.getProperty("path.separator");
		String commandPrefix = new StringBuilder("java -cp ").append("bin").append(separator).append(LIB_DIRECTORY).append("/*")
				.append(" ").append(className).toString();
		Calendar calendar = Calendar.getInstance();
		
		return new StringBuilder()
			.append("\nPlease provide arguments to use one of the functions below. You may need to change the classpath for your environment.\n\n")
			.append("authV3 <url> <username> <password> <serialNumber> <tranId> <amount> <cardData> <cardType>\n")
			.append("authV3_1 <url> <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <cardType>\n")
			.append("batchV3 <url> <username> <password> <serialNumber> <tranId> <amount> <tranResult> <tranDetails>\n")
			.append("cashV3 <url> <username> <password> <serialNumber> <tranId> <amount> <tranUTCTimeMs> <tranUTCOffsetMs> <tranResult> <tranDetails>\n")
			.append("chargeV3 <url> <username> <password> <serialNumber> <tranId> <amount> <cardData> <cardType> <tranResult> <tranDetails>\n")
			.append("chargeV3_1 <url> <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <cardType> <tranResult> <tranDetails>\n")
			.append("uploadFile <url> <username> <password> <serialNumber> <fileName> <fileType>\n")
			.append("processUpdates <url> <username> <password> <serialNumber> <updateStatus>\n")
			.append("\nExamples:\n\n")
			.append(commandPrefix).append(" authV3 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID).append(" 100 ").append(CARD_DATA).append(" ").append(CARD_TYPE_SWIPED).append("\n")
			.append(commandPrefix).append(" batchV3 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID).append(" 60 ").append(TRAN_RESULT_SUCCESS).append(" \"A0|302|10|1|1|305|10|5|1\"").append("\n")
			.append(commandPrefix).append(" authV3_1 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID + 1).append(" 100 ").append(CARD_READER_MAGTEK_MAGNESAFE).append(" 37 ").append(ENCRYPTED_CARD_DATA).append(" ").append(KEY_SERIAL_NUMBER).append(" ").append(CARD_TYPE_SWIPED).append("\n")
			.append(commandPrefix).append(" batchV3 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID + 1).append(" 50 ").append(TRAN_RESULT_SUCCESS).append(" \"A0|302|10|1|1|305|10|4|1\"").append("\n")
			.append(commandPrefix).append(" cashV3 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID + 2).append(" 80 ").append(calendar.getTimeInMillis()).append(" ").append(calendar.getTimeZone().getOffset(calendar.getTimeInMillis())).append(" ").append(TRAN_RESULT_SUCCESS).append(" \"A0|302|10|3|1|305|10|5|1\"").append("\n")
			.append(commandPrefix).append(" chargeV3 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID + 3).append(" 110 ").append(CARD_DATA).append(" ").append(CARD_TYPE_SWIPED).append(" ").append(TRAN_RESULT_SUCCESS).append(" \"A0|302|10|6|1|305|10|5|1\"").append("\n")
			.append(commandPrefix).append(" chargeV3_1 ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(TRANSACTION_ID + 4).append(" 120 ").append(CARD_READER_MAGTEK_MAGNESAFE).append(" 37 ").append(ENCRYPTED_CARD_DATA).append(" ").append(KEY_SERIAL_NUMBER).append(" ").append(CARD_TYPE_SWIPED).append(" ").append(TRAN_RESULT_SUCCESS).append(" \"A0|302|10|7|1|305|10|5|1\"").append("\n")
			.append(commandPrefix).append(" uploadFile ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" \"").append(FILE_NAME).append("\" ").append(FILE_TYPE).append("\n")
			.append(commandPrefix).append(" processUpdates ").append(url).append(" \"").append(USERNAME).append("\" \"").append(PASSWORD).append("\" ").append(SERIAL_NUMBER).append(" ").append(UPDATE_STATUS_NORMAL).append("\n")
			.toString();
	}
	
	public static void processMain(String[] args, String url, String className) throws Exception {
		if (args.length < 1) {
			System.out.print(getUsage(url, className));
			return;
		}
		String function = args[0];
		if ("authV3".equals(function)) {
			if (args.length < 9)
				System.out.print(getUsage(url, className));
			else
				testAuthV3(args[1], args[2], args[3], args[4], Long.valueOf(args[5]), Long.valueOf(args[6]), args[7], args[8]);		
		} else if ("authV3_1".equals(function)) {
			if (args.length < 12)
				System.out.print(getUsage(url, className));
			else
				testAuthV3_1(args[1], args[2], args[3], args[4], Long.valueOf(args[5]), Long.valueOf(args[6]), Integer.valueOf(args[7]), Integer.valueOf(args[8]), args[9], args[10], args[11]);
		} else if ("batchV3".equals(function)) {
			if (args.length < 9)
				System.out.print(getUsage(url, className));
			else
				testBatchV3(args[1], args[2], args[3], args[4], Long.valueOf(args[5]), Long.valueOf(args[6]), args[7], args[8]);
		} else if ("cashV3".equals(function)) {
			if (args.length < 11)
				System.out.print(getUsage(url, className));
			else
				testCashV3(args[1], args[2], args[3], args[4], Long.valueOf(args[5]), Long.valueOf(args[6]), Long.valueOf(args[7]), Integer.valueOf(args[8]), args[9], args[10]);
		} else if ("chargeV3".equals(function)) {
			if (args.length < 11)
				System.out.print(getUsage(url, className));
			else
				testChargeV3(args[1], args[2], args[3], args[4], Long.valueOf(args[5]), Long.valueOf(args[6]), args[7], args[8], args[9], args[10]);
		} else if ("chargeV3_1".equals(function)) {
			if (args.length < 14)
				System.out.print(getUsage(url, className));
			else
				testChargeV3_1(args[1], args[2], args[3], args[4], Long.valueOf(args[5]), Long.valueOf(args[6]), Integer.valueOf(args[7]), Integer.valueOf(args[8]), args[9], args[10], args[11], args[12], args[13]);
		} else if ("uploadFile".equals(function)) {
			if (args.length < 7)
				System.out.print(getUsage(url, className));
			else
				testUploadFile(args[1], args[2], args[3], args[4], args[5], Integer.valueOf(args[6]));
		} else if ("processUpdates".equals(function)) {
			if (args.length < 5)
				System.out.print(getUsage(url, className));
			else
				testProcessUpdates(args[1], args[2], args[3], args[4], Integer.valueOf(args[5]));
		} else
			System.out.print(getUsage(url, className));
	}
	
	protected static String getBasicResponseMessage(EcStub.ECResponse response) {
		String message = "Response returnCode: " + response.getReturnCode() + ", returnMessage: " + response.getReturnMessage();
		if (response.getNewUsername().length() > 0)
			message += ", newUsername: " + response.getNewUsername();
		if (response.getNewPassword().length() > 0)
			message += ", newPassword: " + response.getNewPassword();
		return message;
	}

	protected static void testAuthV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType) throws Exception {		
		EcStub.AuthV3 request = new EcStub.AuthV3();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardData(cardData);
		request.setCardType(cardType);
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		EcStub.AuthV3Response responseMessage;
		try {
			responseMessage = ePortConnect.authV3(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECAuthResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == RES_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}	
	
	@Test
	public void testAuthV3() throws Exception {		
		testAuthV3(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID, 100, CARD_DATA, CARD_TYPE_SWIPED);
	}
	
	protected static void testBatchV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, String tranResult, String tranDetails) throws Exception {
		EcStub.BatchV3 request = new EcStub.BatchV3();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);		
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		EcStub.BatchV3Response responseMessage;
		try {
			responseMessage = ePortConnect.batchV3(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response);
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testBatchV3() throws Exception {
		testBatchV3(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID, 60, TRAN_RESULT_SUCCESS, "A0|302|10|1|1|305|10|5|1");
	}
	
	protected static void testAuthV3_1(String url, String username, String password, String serialNumber, long tranId, long amount, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String cardType) throws Exception {
		EcStub.AuthV3_1 request = new EcStub.AuthV3_1();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setCardType(cardType);
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		EcStub.AuthV3_1Response responseMessage;
		try {
			responseMessage = ePortConnect.authV3_1(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECAuthResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == RES_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testAuthV3_1() throws Exception {
		testAuthV3_1(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID + 1, 100, CARD_READER_MAGTEK_MAGNESAFE, CARD_DATA.length(), ENCRYPTED_CARD_DATA, KEY_SERIAL_NUMBER, CARD_TYPE_SWIPED);
	}
	
	@Test
	public void testBatchV3_1() throws Exception {
		testBatchV3(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID + 1, 50, TRAN_RESULT_SUCCESS, "A0|302|10|1|1|305|10|4|1");
	}
	
	protected static void testCashV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails) throws Exception {
		EcStub.CashV3 request = new EcStub.CashV3();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setTranUTCTimeMs(tranUTCTimeMs);
		request.setTranUTCOffsetMs(tranUTCOffsetMs);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		EcStub.CashV3Response responseMessage;
		try {
			responseMessage = ePortConnect.cashV3(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response);
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testCashV3() throws Exception {
		Calendar calendar = Calendar.getInstance();
		testCashV3(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID + 2, 80, calendar.getTimeInMillis(), 
				calendar.getTimeZone().getOffset(calendar.getTimeInMillis()), TRAN_RESULT_SUCCESS, "A0|302|10|3|1|305|10|5|1");
	}

	protected static void testChargeV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType, String tranResult, String tranDetails) throws Exception {
		EcStub.ChargeV3 request = new EcStub.ChargeV3();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardData(cardData);
		request.setCardType(cardType);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		EcStub.ChargeV3Response responseMessage;
		try {
			responseMessage = ePortConnect.chargeV3(request);		
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECAuthResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == RES_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testChargeV3() throws Exception {
		testChargeV3(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID + 3, 110, CARD_DATA, CARD_TYPE_SWIPED, TRAN_RESULT_SUCCESS, "A0|302|10|6|1|305|10|5|1");
	}
	
	protected static void testChargeV3_1(String url, String username, String password, String serialNumber, long tranId, 
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String cardType, String tranResult, String tranDetails) throws Exception {
		EcStub.ChargeV3_1 request = new EcStub.ChargeV3_1();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(decryptedCardDataLen);
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setCardType(cardType);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		EcStub.ChargeV3_1Response responseMessage;
		try {
			responseMessage = ePortConnect.chargeV3_1(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECAuthResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == RES_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testChargeV3_1() throws Exception {
		testChargeV3_1(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, TRANSACTION_ID + 4, 120, CARD_READER_MAGTEK_MAGNESAFE, CARD_DATA.length(), ENCRYPTED_CARD_DATA,
				KEY_SERIAL_NUMBER, CARD_TYPE_SWIPED, TRAN_RESULT_SUCCESS, "A0|302|10|7|1|305|10|5|1");
	}
	
	protected static void testUploadFile(String url, String username, String password, String serialNumber, String fileName, int fileType) throws Exception {		
		FileDataSource fds = new FileDataSource(fileName);
		DataHandler dh = new DataHandler(fds);
		File file = new File(fileName);
		EcStub.UploadFile request = new EcStub.UploadFile();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setFileName(fileName);
		request.setFileType(fileType);
		request.setFileSize(file.length());
		request.setFileContent(dh);		
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		Options options = ePortConnect._getServiceClient().getOptions();
		options.setProperty(Constants.Configuration.ENABLE_MTOM, Constants.VALUE_TRUE);
		options.setTimeOutInMilliSeconds(10*60*1000);
		EcStub.UploadFileResponse responseMessage;
		try {
			responseMessage = ePortConnect.uploadFile(request);		
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			EcStub.ECResponse response = responseMessage.get_return();
			String message = getBasicResponseMessage(response);
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUploadFile() throws Exception {
		testUploadFile(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, FILE_NAME, FILE_TYPE_KIOSK_GENERIC_FILE);
	}
	
	protected static void testProcessUpdates(String url, String username, String password, String serialNumber, int updateStatus) throws Exception {		
		EcStub.ProcessUpdates request = new EcStub.ProcessUpdates();
		request.setUsername(username);
		request.setPassword(password);
		request.setSerialNumber(serialNumber);
		request.setUpdateStatus(updateStatus);
		if (ePortConnect == null)
			ePortConnect = new EcStub(url);
		Options options = ePortConnect._getServiceClient().getOptions();
		options.setProperty(Constants.Configuration.ENABLE_MTOM, Constants.VALUE_TRUE);
		options.setProperty(Constants.Configuration.CACHE_ATTACHMENTS, Constants.VALUE_FALSE);
		options.setTimeOutInMilliSeconds(10*60*1000);
		
		try {
			EcStub.ProcessUpdatesResponse responseMessage = ePortConnect.processUpdates(request);		
			if (responseMessage == null || responseMessage.get_return() == null)
				throw new Exception("Received null response");
			else {
				EcStub.ECProcessUpdatesResponse response = responseMessage.get_return();
				String message = getBasicResponseMessage(response)
						+ ", fileName: " + response.getFileName() + ", fileType: " + response.getFileType() + ", fileSize: " + response.getFileSize();
				if (response.getReturnCode() == RES_OK || response.getReturnCode() == RES_OK_NO_UPDATE
						|| response.getReturnCode() == RES_OK_UPLOAD_FILE) {
					if (response.getReturnCode() == RES_OK && response.getFileContent() != null) {
						DataHandler dh = response.getFileContent();
						File file = new File(response.getFileName());
						FileOutputStream os = new FileOutputStream(file);
						try {
							dh.writeTo(os);
							os.flush();
						} finally {
							os.close();
						}
					} else if (response.getReturnCode() == RES_OK_UPLOAD_FILE && response.getFileName() != null && response.getFileName().length() > 0)
						testUploadFile(url, username, password, serialNumber, response.getFileName(), FILE_TYPE_KIOSK_GENERIC_FILE);
					System.out.println(message);
				} else
					throw new Exception(message);
			}
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
	}
	
	@Test
	public void testProcessUpdates() throws Exception {
		testProcessUpdates(EPORT_CONNECT_SOAP_URL, USERNAME, PASSWORD, SERIAL_NUMBER, UPDATE_STATUS_NORMAL);
	}
}
