package com.usatech.ec;

import java.io.Serializable;
import java.net.URL;

import javax.activation.DataHandler;
import javax.activation.DataSource;

public class ECDataHandler extends DataHandler implements Serializable {
	private static final long serialVersionUID = 5679526979202794319L;
	protected byte[] fileBytes;

	public ECDataHandler(DataSource ds) {
		super(ds);
	}

	public ECDataHandler(Object obj, String mimeType) {
		super(obj, mimeType);
	}
	
	public ECDataHandler(URL url) {
		super(url);
	}

	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
}
