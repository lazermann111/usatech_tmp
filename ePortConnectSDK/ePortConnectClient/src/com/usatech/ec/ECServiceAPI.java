package com.usatech.ec;

public interface ECServiceAPI {
	// Process a card authorization with card data from a non-encrypting card reader 
	public ECAuthResponse authV3(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType);
	
	// Process a card authorization with card data from an encrypting card reader
	public ECAuthResponse authV3_1(String username, String password, String serialNumber, long tranId, long amount, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String cardType);
	
	// Settle a previously authorized transaction
	public ECResponse batchV3(String username, String password, String serialNumber, long tranId, 
			long amount, String tranResult, String tranDetails);
	
	// Upload a cash transaction
	public ECResponse cashV3(String username, String password, String serialNumber, long tranId, 
			long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails);
	
	// Authorize and settle a transaction with card data from a non-encrypting card reader 
	public ECAuthResponse chargeV3(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType, String tranResult, String tranDetails);
	
	// Authorize and settle a transaction with card data from an encrypting card reader
	public ECAuthResponse chargeV3_1(String username, String password, String serialNumber, long tranId, 
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String cardType, String tranResult, String tranDetails);
	
	// Upload a file to the Server
	public ECResponse uploadFile(String username, String password, String serialNumber, String fileName, 
			int fileType, long fileSize, ECDataHandler fileContent);
	
	// Download updates from the Server
	public ECProcessUpdatesResponse processUpdates(String username, String password, String serialNumber, int updateStatus);
}
