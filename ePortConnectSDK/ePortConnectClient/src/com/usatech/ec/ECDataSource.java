package com.usatech.ec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.activation.DataSource;

public class ECDataSource implements DataSource, Serializable {
	private static final long serialVersionUID = 1166394429813732192L;
	protected InputStream inputStream;
	
	public ECDataSource(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return null;
	}

	@Override
	public String getContentType() {
		return "application/octet-stream";
	}

	@Override
	public String getName() {
		return "ECDataSource";
	}
}
