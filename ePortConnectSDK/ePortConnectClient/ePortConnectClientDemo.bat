@echo off
REM This script starts the ePortConnectClient Demo Program
REM JAVA_HOME JAVA_OPTS
cd "%~dp0"
if exist "bin\setenv.bat" call "bin\setenv.bat"
if not "%JAVA_HOME%" == "" set JAVA=%JAVA_HOME%\bin\java
if not "%JAVA%" == "" goto gotJava
set JAVA=java
:gotJava
SET CLASSPATH=bin
FOR /f "tokens=*" %%G IN ('dir /b lib\*.jar') DO (CALL :setclasspath lib\%%G)
      
echo [%DATE% %TIME%] USING CLASSPATH: %CLASSPATH%
echo [%DATE% %TIME%] USING JAVA_HOME: %JAVA_HOME%
echo [%DATE% %TIME%] USING JAVA_OPTS: %JAVA_OPTS%    
echo [%DATE% %TIME%] ePortConnectClientDemo Starting...
"%JAVA%" %JAVA_OPTS% -cp "%CLASSPATH%" com.usatech.ec2.EportConnectHessianTest %*
echo [%DATE% %TIME%] ePortConnectClientDemo Finished
EXIT /B

:setclasspath
SET CLASSPATH=%CLASSPATH%;%1