/**
 * 
 */
package SerialMain;

import java.io.*;

import java.util.Enumeration;
import java.util.HashMap;

import javax.comm.CommPort;
import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
//import javax.comm.SerialPortEventListener;
import javax.comm.UnsupportedCommOperationException;

// This is some code that I found online.  I implemented it
// and tweaked it to work the way I wanted it to.  It is not
// too clean, but seems to work fine so I am leaving it 'as is'.
/**
 * @author aroyce
 *
 */
public abstract class OpenCommPort {

	protected DataInputStream is;
	protected PrintStream os;
	protected CommPort thePort;
	@SuppressWarnings("rawtypes")
	protected Enumeration portIdentifiers;
	private SerialEdgeControl SECont;
		
	public OpenCommPort (SerialEdgeControl SEContObj) {
		
		// Set the communication port
		// This needs to be externally passed in.
		String commpt;  // = "COM3";
		Integer baudrate;  // = 9600;
		Integer databits;  // = 8;
		Integer parity;  // = 0; // N
		Integer stopbits;  // = 1;
		
		commpt = SEContObj.cPort;
		baudrate = Integer.parseInt(SEContObj.bRate);
		databits = Integer.parseInt(SEContObj.dBits);
		parity = Integer.parseInt(SEContObj.pArity);
		stopbits = Integer.parseInt(SEContObj.sBits);
		
		Integer portduration = 20;
		SECont = SEContObj;
		
		CommPortIdentifier thePortID;
		thePort = null;
		HashMap<String, CommPortIdentifier> map = new HashMap<String, CommPortIdentifier>();
		
		portIdentifiers = CommPortIdentifier.getPortIdentifiers();
		@SuppressWarnings("unchecked")
		Enumeration<CommPortIdentifier> pList = portIdentifiers;
		while (pList.hasMoreElements())
		{
			CommPortIdentifier cpi = (CommPortIdentifier)pList.nextElement();
			map.put(cpi.getName(), cpi);
		}
		
		thePortID = map.get(commpt);
		try {
			thePort = thePortID.open("Comm Test Sys", portduration * 1000);
			SerialPort myPort = (SerialPort) thePort;
			myPort.setInputBufferSize(1024);
			myPort.setOutputBufferSize(1024);
			myPort.setSerialPortParams(baudrate, databits, stopbits, parity);
		} catch (PortInUseException e) {
			SECont.writeGUI("The serial port is in use! " + e);
		} catch (UnsupportedCommOperationException e) {
			SECont.writeGUI("Unsupported Comm Operation.   Wow, you really blew it! " + e);
		}
		
		try{
			is = new DataInputStream(thePort.getInputStream());
		}
		catch (IOException e) {
			SECont.writeGUI("Can't open input stream: write-only");
			is = null;
		}
		catch (NullPointerException n) {
			SECont.writeGUI("Can't open input stream: NPE -Just go home!");
			is = null;
		}
		
		try {
			os = new PrintStream(thePort.getOutputStream(), true);			
		}
		catch (IOException e) {
			SECont.writeGUI("Can't open output stream: you are screwed");
			os = null;			
		}
		catch (NullPointerException n) {
			SECont.writeGUI("Can't open output stream: NPE - Just go home!");
			is = null;
		}
	}
	
}
