/**
 * 
 */
package SerialMain;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//import javax.comm.SerialPortEvent;
//import java.io.PrintStream;
import java.util.*;

/**
 * @author aroyce
 *
 */
public final class ProcessCmd extends OpenCommPort {
	
	private SerialEdgeControl SECont;
	
	public ProcessCmd (String str, SerialEdgeControl SEContObj) {
		
		// Some examples to understand the code and serial interface better.
		// 32321E311E311E3330301E3939391E7072696E741DC48A0D
		// CRC = C48A
		// RS = |, GS = ~
		// 22|1|1|250|999|Mountain Dew~
		// 22|1|1|500|1|Water~
		// 23|4|2|150|999|Coca Cola~|2|150|998|Pepsi Cola~
		// 23|2|2|150|999|Coca Cola~
		// 23|3|3|150|999|Coca Cola~
		// 26|1|1|QA Testing|In Progress
		// 21|750
		
		// Call the parent class constructor
		super(SEContObj);
		
		SECont = SEContObj;
		
		if ( str.contains("|"))
		{
			int valCRC;
			String tmpstr;
			CalculateCRC newCRC;
			byte[] ByteArray;
			char rs = '\u001E';
			char gs = '\u001D';
			int bytelen = str.length();
			
			// Create a new byte array
			ByteArray = new byte[bytelen];
			// Populate the byte array with the string contents.
			// Substitute the record and group separators.
			for(int j=0; j<bytelen; j++)
			{
				if (j < str.length()) 
				{
					ByteArray[j] = (byte) str.charAt(j);
					if (ByteArray[j] == '|')
					{
						ByteArray[j] = (byte) rs;
					}
					else if (ByteArray[j] == '~')
					{
						ByteArray[j] = (byte) gs;
					}
				}
			}
			
			newCRC = new CalculateCRC();
			valCRC = newCRC.getCRC(ByteArray);
			
			byte[] CRCByte = new byte[2];
			CRCByte[0] = (byte) (valCRC >> 8);
			CRCByte[1] = (byte) valCRC;
			
			// Convert the byte arrays into string objects.
			str = new String(ByteArray);
			tmpstr = new String(CRCByte);

			// Send the converted string, CRC and a CR to the serial port.
			send (str + tmpstr + "\r");
		}
		else
		{
			//  Small file transfer
			if (str.equalsIgnoreCase("24") || str.equalsIgnoreCase("25"))
			{
				SmallFileXfer fileTrans = new SmallFileXfer(str);
				send (fileTrans.returnCommand() + "\r");
			}
			//  Large file transfer
			else if (str.equalsIgnoreCase("24a") || str.equalsIgnoreCase("25a") || str.equalsIgnoreCase("25b") || str.equalsIgnoreCase("25c"))
			{
				LargeFileXfer fileTrans = new LargeFileXfer(str);
				send (fileTrans.returnCommand() + "\r");
			}
			else
			{
				// To send an acknowledgment.
				if (str.equalsIgnoreCase("Ack")){
					send('\u0006' + "\r");
				}
				else
				{
					// It is not a special case so simply send the string to the device.
					send (str + "\r");  // Request status
				}
			}
		}
	}
	
		
	protected void send(String s)
	{
		Thread writeout, readout;

		Integer MaxDelay = SECont.timeDelay; // Cut the thread if it lasts too long.  This is in milliseconds.

		writeout = new Thread(new SerialWriter(os.printf(s)));
		
		try {
			writeout.start();
			writeout.join(MaxDelay); // I still need some sort of time delay kludge :(
		}
		catch (UnknownFormatConversionException e) {
			SECont.writeGUI("An IO exception occurred in your crappy Java program.");
			SECont.writeGUI(e.toString());
			SECont.repaint();
			// Now we need to clean-up the communication channels.
			thePort.close();
			thePort = null;
		}
		catch (InterruptedException e) {
			SECont.writeGUI("An IO exception occurred in the serial interface.  You are screwed.");
			SECont.writeGUI(e.toString());
			SECont.repaint();
			// Now we need to clean-up the communication channels.
			thePort.close();
			thePort = null;
		}
		catch (NullPointerException e) {
			SECont.writeGUI("An IO exception occurred in the serial interface.  You are screwed.");
			SECont.writeGUI(e.toString());
			SECont.repaint();			
			// Now we need to clean-up the communication channels.
			thePort.close();
			thePort = null;
		}
		
		// Threads seemed like a good idea, but I have to write and read separately.
		timeDelay(MaxDelay); // Time delay kludge to ensure thread is dead

		readout = new Thread(new SerialReader(is));

		try {
			readout.start();
			readout.join(MaxDelay); // I still need some sort of time delay kludge :(
		}
		catch (InterruptedException e)
		{
			SECont.writeGUI("An IO exception occurred in the serial interface.  You are screwed.");
			SECont.writeGUI(e.toString());
			SECont.repaint();
			// Now we need to clean-up the communication channels.
			thePort.close();
			thePort = null;
		}
	}
	
	
	// Test using a thread to write to the serial port.
	public class SerialWriter implements Runnable 
	{
		
		OutputStream out;
		
		public SerialWriter (OutputStream out)
		{
			this.out = out;
		}
		
		public void run()
		{
			try
			{
				int c = 0;
				while (( c = System.in.read()) > -1)
				{
					this.out.write(c);
				}
				
				out.close();
				out = null;
				
			}
			catch (IOException e)
			{
				SECont.writeGUI("An IO exception occurred in the serial interface.  You are screwed.");
				SECont.writeGUI(e.toString());
				SECont.repaint();
			}
			// Now we need to clean-up the communication channels.
			thePort.close();
			thePort = null;
		}
		
	}
	
	
	// Test using a thread to read from the serial port.
	public class SerialReader implements Runnable
	{
		
		private InputStream in;
		private Integer exitStream;
		private Integer buffSize;
		private String readString;
		
		public SerialReader (InputStream in) //, SerialEdgeControl SEContObj)
		{
			this.in = in;
		}
		
		public void run()
		{
			byte[] readBuffer = new byte[1024];
			int len = -1;
			try
			{
				// while ((len = this.in.read(readBuffer)) != -1)
				if ((len = this.in.read(readBuffer)) != -1)
				{
					readString = new String(readBuffer,0,len);
					//System.out.print(new String(readBuffer,0,len));
				}
				buffSize = readString.length();
				// Let's convert some of the Hex characters back
				// into ASCII characters so that we can read the output.
				// 15 is a "nack"
				exitStream = 0;
				SECont.strBuffer = "";
				for (int i=0; i < buffSize; i++)
				{
					if (readString.charAt(i) == 30)
					{
						SECont.strBuffer = SECont.strBuffer + "|";
						exitStream = 0;
					}
					else if (readString.charAt(i) == 06)
					{
						SECont.strBuffer = SECont.strBuffer + "Ack";
						exitStream = 0;
					}
					else if (readString.charAt(i) != 0)
					{
						SECont.strBuffer = SECont.strBuffer + (char) readString.charAt(i);
						exitStream = 0;
					}
					else if ((readString.charAt(i) == 0) || (readString.charAt(i) == 15))
					{
						exitStream++;
						if (exitStream > 5)
							break;
					}
				}
				
				SECont.writeGUI("The command line response is: " + SECont.strBuffer);
				SECont.repaint();
				
				this.in.close();
				this.in = null;

			}
			catch (IOException e)
			{
				SECont.writeGUI("An IO exception occurred in the serial interface.  You are screwed.");
				SECont.writeGUI(e.toString());
				SECont.repaint();
			}
			// Now we need to clean-up the communication channels.
			// This facilitates a clean opening of the port the next time.
			thePort.close();
			thePort = null;
		}
	}
	
	
	public String response ()
	{
		return SECont.strBuffer;
	}

	
	public static void timeDelay (int msec)
	{
		long t0, t1;
		t0 =  System.currentTimeMillis();
		do
		{
			t1 = System.currentTimeMillis();
		} while (t1 - t0 < msec);
	}


}