
package SerialMain;

import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JFrame;

final class SerialEdge extends JFrame {
	
	static final long serialVersionUID = 42L;
	
	public static void main(String[] args) throws IOException, NoSuchPortException,
    			PortInUseException, UnsupportedCommOperationException {

    	// This instantiates the GUI used to control the program.
    	// The GUI sets the serial parameters and command line input.
    	// The output is displayed on the GUI screen for review.
    	new SerialEdgeControl();
    	
    }
}
    
class SerialEdgeControl extends JFrame implements ActionListener {
    	
	static final long serialVersionUID = 42L;  // Some arbitrary number.
	
	private TextField DataInput;
	private TextArea UserOutput;
	private Button Execute, Clear, Exit, SetComm, TDelay, selfTest;
	private JComboBox<String> ComPort, Baud, DBits, Parity, SBits;
	
	public String cPort;
	public String bRate;
	public String dBits;
	public String pArity;
	public String sBits;
	public Integer timeDelay;

	public String strBuffer;

	public SerialEdgeControl()
    {
		// These are values that can be selected.
		String[] portSelect = {"COM1","COM2","COM3","COM4","COM5","COM6","COM7","COM8"};
		String[] baudRate = {"4800","9600","14400","19200","24000"};
		String[] dataBits = {"7","8"};
		String[] parity = {"0","1"};
		String[] stopBits = {"0","1"};
		
		timeDelay = 800; // Set a default of 800 msec.
		
		// The default integers are array selections starting from 0 (not 1).
		Panel comm = new Panel();
			ComPort = new JComboBox<String>(portSelect);
			ComPort.setSelectedIndex(0);

			Baud = new JComboBox<String>(baudRate);
			Baud.setSelectedIndex(1);

			DBits = new JComboBox<String>(dataBits);
			DBits.setSelectedIndex(1);
			
			Parity = new JComboBox<String>(parity);
			Parity.setSelectedIndex(0);
			
			SBits = new JComboBox<String>(stopBits);
			SBits.setSelectedIndex(1);
			
			SetComm = new Button ("Set Params");
			SetComm.addActionListener(this);
			
			TDelay = new Button (" Set Delay");
			TDelay.addActionListener(this);
			
			comm.add (new Label("Comm Port"));
			comm.add(ComPort);
			comm.add (new Label("Baud"));
			comm.add(Baud);
			comm.add (new Label("Data Bits"));
			comm.add(DBits);
			comm.add (new Label("Parity"));
			comm.add(Parity);
			comm.add (new Label("Stop Bits"));
			comm.add(SBits);
			comm.add(SetComm);
			comm.add(TDelay);
			
			cPort = (String) ComPort.getSelectedItem();
			bRate = (String) Baud.getSelectedItem();
			dBits = (String) DBits.getSelectedItem();
			pArity = (String) Parity.getSelectedItem();
			sBits = (String) SBits.getSelectedItem();

			add("North", comm);
		Panel interact = new Panel();
			DataInput = new TextField("",90);
			Execute = new Button("Execute");
			UserOutput = new TextArea(28,100);
			UserOutput.setEditable(false);
			Execute.addActionListener(this);
			
			interact.add("West",DataInput);
			interact.add("East",Execute);
			interact.add("South",UserOutput);
			
			add("Center",interact);
		Panel controls = new Panel();
			controls.setLayout( new GridLayout(2,6)); // 2 rows and 6 columns
			Clear = new Button("Clear");
			Exit = new Button("Exit");
			selfTest = new Button("Self Test");
			Clear.addActionListener(this);
			Exit.addActionListener(this);
			selfTest.addActionListener(this);

			controls.add(selfTest);
			controls.add (new Label("24 - short file upload"));
			controls.add (new Label("241 - long file upload"));
			controls.add (new Label("71 - file download"));
			controls.add(Clear);
			controls.add(Exit);
			
			controls.add (new Label("21|700"));
			controls.add (new Label("22|1|1|250|9|Pepsi~"));
			controls.add (new Label("Credit requires"));
			controls.add (new Label("manual testing"));
			controls.add (new Label("22|1|1|150|1|Water~"));
			controls.add (new Label("Cmd 13 for Tran Id"));
			
			add("South", controls);
		
		// Render the test harness GUI.
		setTitle ("Serial Edge Test Harness Window");
		setSize (820, 620);
		setVisible (true);

    }
    	
	@Override
	// When a button is pushed this routine is executed.
	public void actionPerformed(ActionEvent e) throws NumberFormatException {
		Object source = e.getSource();

		// Set Communication Parameters
		if (source == SetComm)
		{
			// Set communication parameters
			cPort = (String) ComPort.getSelectedItem();
			bRate = (String) Baud.getSelectedItem();
			dBits = (String) DBits.getSelectedItem();
			pArity = (String) Parity.getSelectedItem();
			sBits = (String) SBits.getSelectedItem();
			
			writeGUI("Serial port parameters are now set.");
			writeGUI("The current thread time delay is: " + timeDelay.toString() + " msec.");
		}
		// Set the time delay for the thread interaction.
		else if (source == TDelay)
		{
			String dataIn;
			
			dataIn = DataInput.getText();
			
			try
			{
				Integer.parseInt(dataIn);
			
				if (dataIn.length() > 2)
				{
					timeDelay = Integer.valueOf(dataIn);
					writeGUI("Thread time delay set to: " + dataIn + " msec.");
				}
				else
				{
					writeGUI("The input must be an integer value greater than 99.");				
					timeDelay = 500;
				}
				DataInput.setText("");
			}
			catch (Exception e1)
			{
				writeGUI("Data input error you dummy.  Time delay set to 500 msec.");								
				timeDelay = 500;
			}
		}
		// Execute the string typed into the command window.
		else if (source == Execute)
		{
			// Execute the command string in the text box
	    	String CmdStr = DataInput.getText();
	    	
	    	manageInput(CmdStr);
	    	
		}
		else if (source == selfTest)
		{
			// Begin a series of controlled tests.  Put a time delay in between each test.
			
			String[] command = {
					"1", // Status
					"5", // Enable ePort
					"3", // Reset
					"4", // Process Update
					"1", // Status
					"5", // Enable
					"9", // Acquire Signal Quality
					"1", // Status
					"10", // Acquire Time and Date
					"1", // Status
					"12", // Acquire Config Data
					"1", // Status
					"23|4|2|150|999|Coca Cola~|2|150|998|Pepsi Cola~", // Cash Sale
					"1", // Status
					"23|3|3|150|999|Coca Cola~", // Cash Sale
					"1", // Status
					"26|1|1|File Transfer|Being Tested", // Set Display
					"1", // Status
					"24", // Short File Transfer
					"1", // Status
					"241", // Long File Transfer
					"1", // Status
					"26|1|1|QA Testing|In Progress", // Set Display
					"1", // Status
					"6", // Disable ePort
					"1", // Status
					"5" // Enable
					// "2" // Reboot
					};

			Integer cmdDelay = 2000; // 2 second delay between all commands.
			Integer maxCtr = command.length;
			
			for (Integer i = 0; i < maxCtr; i++)
			{
				manageInput(command[i]);
				try
				{
					Thread.sleep(cmdDelay);
				}
					catch (InterruptedException e1) {
					// This should not trigger.
				}
			}
		}
		else if (source == Clear)
		{
			// Clear the text box
			UserOutput.setText("");
			DataInput.setText("");
		}
		else if (source == Exit)
		{
			// Exit the entire application
			System.exit(0);
		}
	}
	
	
	public void manageInput(String CmdStr)
	{
		Integer ctr;
		Boolean continueOn;
		
    	if (CmdStr.length() == 0)
    	{
    		writeGUI("You entered too few arguments.  The general format is:\n");
			writeGUI("command|data|data|...\n");
    	}
    	else
    	{
    		ProcessCmd cmdresult;
    		writeGUI("You entered the following command line string: " + CmdStr);
    		// Erase command input string.
    		DataInput.setText("");
    		
    		ctr = 0;
    		Integer ctrMAX = 20;  //10;
    			
    		// Establish logic and results from the listener
    		// Start with small file transfer and automate.
    		
    		// Entering '24' will execute the automated short file transfer.
    		if (CmdStr.equalsIgnoreCase("24"))
    		{
    			continueOn = true;
    			cmdresult = new ProcessCmd (CmdStr, this);
    			// The device needs to acknowledge the command input.
		    	if (strBuffer.equalsIgnoreCase("Ack"))
	    		{
	    			// Enter a status command (1) until the response of "12" is received.
	    			// This indicates that the short file header information has been
	    			// received and processed.  Now the device waits for the file payload.
	    			writeGUI("You entered the following command line string: " + "1");
	    			cmdresult = new ProcessCmd ("1", this);
		    		while (! strBuffer.contains("12"))
	    			{
	    				
		    			cmdresult = new ProcessCmd ("1", this);
	    				ctr++;
	    				// Loop exit counter if something fails.
		    			if (ctr > ctrMAX)
		    			{
		    				writeGUI("Something failed.  Loop Counter Limit Reached.");
		    				continueOn = false;
		    				break;
		    			}
	    			}
    				//  The '25' command initiates the transfer of the file payload.
		    		if (continueOn)
		    		{
		    			writeGUI("You entered the following command line string: " + "25");
		    			cmdresult = new ProcessCmd ("25", this);
		    		}
	    		}
    		}
    		// Entering '241' will execute the automated long file transfer.
    		else if (CmdStr.equalsIgnoreCase("241"))
    		{
				ctr = 0;
    			continueOn = true;
    			cmdresult = new ProcessCmd ("24a", this);
	    		if (strBuffer.equalsIgnoreCase("Ack"))
	    		{
	    			// Enter a status command (1) until the response of "12" is received.
	    			// This indicates that the long file header information has been
	    			// received and processed.  Now the device waits for each file payload.
	    			writeGUI("You entered the following command line string: " + "1");
	    			cmdresult = new ProcessCmd ("1", this);
	    			while (! strBuffer.contains("12"))
	    			{
	    				
	    				// Loop exit counter if something fails.
		    			if ((ctr > ctrMAX) | (strBuffer.contains("4")))
		    			{
		    				writeGUI("Execution failure.  Final counter: " + ctr.toString());
		    				continueOn = false;
		    				break;
		    			}
		    			cmdresult = new ProcessCmd ("1", this);
	    				ctr++;
	    			}
    				ctr = 0;
	    			// Transfer the first "piece" of the long file payload.
		    		if (continueOn)
		    		{
		    			writeGUI("You entered the following command line string: " + "25a");
		    			cmdresult = new ProcessCmd ("25a", this);
		    			if (strBuffer.equalsIgnoreCase("Ack"))
		    			{
		    				// After acknowledgment request a status until a '12' code is returned.
		    				writeGUI("You entered the following command line string: " + "1");
		    				cmdresult = new ProcessCmd ("1", this);
		    				while (! strBuffer.contains("12"))
		    				{
		    					// Loop exit counter if something fails.
		    					if ((ctr > ctrMAX) | (strBuffer.trim().contains("4")))
		    					{
				    				writeGUI("Execution failure.  Final counter: " + ctr.toString());
		    						continueOn = false;
		    						break;
		    					}
		    					cmdresult = new ProcessCmd ("1", this);
		    					ctr++;
		    				}
		    				ctr = 0;
		    				// Transfer the second "piece" of the long file payload.
				    		if (continueOn)
				    		{
				    			writeGUI("You entered the following command line string: " + "25b");
				    			cmdresult = new ProcessCmd ("25b", this);
				    			if (strBuffer.equalsIgnoreCase("Ack"))
				    			{
				    				// After acknowledgment request a status until a '12' code is returned.
				    				writeGUI("You entered the following command line string: " + "1");
				    				cmdresult = new ProcessCmd ("1", this);
				    				while (! strBuffer.contains("12"))
				    				{
				    					// Loop exit counter if something fails.
				    					if ((ctr > ctrMAX) | (strBuffer.contains("4")))
				    					{
						    				writeGUI("Execution failure.  Final counter: " + ctr.toString());
				    						continueOn = false;
				    						break;
				    					}
				    					cmdresult = new ProcessCmd ("1", this);
				    					ctr++;
				    				}
				    				// Transfer the third "piece" of the long file payload.
				    				// This is the last piece so we do not do anything as this is a test.
						    		if (continueOn)
						    		{
						    			writeGUI("You entered the following command line string: " + "25c");
						    			cmdresult = new ProcessCmd ("25c", this);
						    		}
				    			}
		    				}
		    			}
		    		}
				}
    		}
    		// This is a file download test from the server.  This is an automated test.
    		// You must queue a file for download before executing this routine.
    		else if (CmdStr.equalsIgnoreCase("71"))
    		{
    			
    			// This may end-up in another class.
    			
    			// Poll the Edge until I receive an "11" which means there
    			// is a file ready on the server AND the header information
    			// has been sent down to me.  Then send a "7" to kick-off
    			// the file transfer from the server.
    			

    			// Enter a status command of "1" and wait for a response of "11"
    			// which means that the server has a file available.
    			// I need to capture the data coming from the server which will
    			// represent the file header.
    			
    			String fileHdr = "";
    			String fileComp = "";
    			
    			// The following values can be determined from the response string.
    			Integer recNum = 1;
    			Integer rsNum;
    			
    			Integer recSize = 0;
    			Integer recType = 0;
    			Integer recTotal = 1;
    			
    			ctr = 0;
    			
    			writeGUI("You entered the following command line string: " + "4");
    			cmdresult = new ProcessCmd ("4", this);
    			// The file header has been sent and I receive an "11" back.
    			// cmdresult.response().contains("11")
    			while ((!strBuffer.contains("11")) && (ctr < 10))
    			{
	    			cmdresult = new ProcessCmd ("1", this);
	    			ctr++;
    			}
    			//writeGUI("You entered the following command line string: " + "7");
    			//cmdresult = new ProcessCmd ("7", this);
    			fileHdr = cmdresult.response();
    			
    			// This is to parse the header so that large file transfers can take place.
    			ctr = 0;
    			rsNum = 0;
    			String tmpStr = "";
    			while (ctr < fileHdr.length())
    			{
    				if (fileHdr.charAt(ctr) == '|')
    				{
    					rsNum++; // Keep track of record separators.
    					ctr++; // Advance beyond the record separator.
    					if (rsNum == 3)
    					{
    						recType = Integer.parseInt(tmpStr.trim());
	    					tmpStr = "";
    					}
    					else if (rsNum == 4)
    					{
    						recTotal = Integer.parseInt(tmpStr.trim());
	    					tmpStr = "";
    					}
    					else if (rsNum == 5)
    					{
    						recNum = Integer.parseInt(tmpStr.trim());	    						
	    					tmpStr = "";
    					}
    				}
					if (rsNum == 2)
					{
    					tmpStr = tmpStr + fileHdr.charAt(ctr);
					}
					else if (rsNum == 3)
					{
    					tmpStr = tmpStr + fileHdr.charAt(ctr);
					}
					else if (rsNum == 4)
					{
    					tmpStr = tmpStr + fileHdr.charAt(ctr);		
					}
					else if (rsNum == 5)
					{
						if ( Character.isDigit(fileHdr.charAt(ctr)) )
							tmpStr = tmpStr + fileHdr.charAt(ctr);
						else
							if (tmpStr.length() > 1)
							{
								recSize = Integer.parseInt(tmpStr.trim());
								break;
							}
					}
					ctr++;
    			}
    			writeGUI("The calculated parameters for the download file are:");
    			writeGUI("recSize: " + recSize.toString() + ", recType: " + recType.toString() + ", recTotal: " + recTotal.toString());
    			
    			if (fileHdr.length() > 5) // 5 is arbitrary, but confirms that something came through.
    			{
    				
	    			// Now send the "7" to received the file bits an d process in ServerDonwload.
	    			writeGUI("You entered the following command line string: " + "7"); // Ready to accept file transfer.
	    			
					// Set up a loop for large file transfers to grab one record at a time.
	    			// This is not working too reliably.  I suspect timing and that the use
	    			// of a separate thread for reading and writing would work better.
	    			
	    			for (Integer i=0; i < recNum; i++)
	    			{
	    				cmdresult = new ProcessCmd ("7", this);
	    				fileComp = fileComp + cmdresult.response();
	    				cmdresult = new ProcessCmd ("Ack" , this);
	    			}
					
					// End of loop for grabbing multiple records.
					
		    		cmdresult = new ProcessCmd ("Ack" , this);
    				// Write the string to the file system for Debugging.
		    		//writeGUI("The file downloaded is:" + "\n" + fileComp);
    				System.out.println("The file downloaded is:" + "\n" + fileComp);
    			}
    			else
    			{
    				writeGUI("The file header was too short.");
    			}
    		}
    		else
    		{
    			// This is not a special automated command so just
    			// process this file string 'as is'.
    			cmdresult = new ProcessCmd (CmdStr, this);
    		}	    		
    	}		
	}
			
			
	public void writeGUI (String str)
	{
		// Add a time stamp to each entry in the output log.
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		String timeStamp = sdf.format(new Date());
		
		UserOutput.append(timeStamp + ": " + str + "\n");
	}
	
	
}


