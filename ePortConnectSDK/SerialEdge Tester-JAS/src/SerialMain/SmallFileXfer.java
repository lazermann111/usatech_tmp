package SerialMain;

public final class SmallFileXfer {

	private String serialString;
	
	public SmallFileXfer(String str)
	{
		
		char rs = '\u001E';
		char gs = '\u001D';
		CalculateCRC newCRC = new CalculateCRC();
		byte[] ByteArray;
		String inpString = "";
		String tmpstr;
		String hexstr;

		// DEX file representing a small file to be transferred.
		String fData = 
			"DEX-EE100010974-002301-071112-SCHEDULED--SC00\n" +
			"DXS*6422550153*VA*V1/1*1\n" +
			"ST*001*0001\n" +
			"ID1*D9208550*VEC 15.3*161 ***\n" +
			"ID4*2*001*5\n" +
			"VA1*46600*1389*46600*1389*0*0\n" +
			"VA2*275*8*275*8\n" +
			"CA1*MEI025980306660*TRC6512_V003*0101**\n" +
			"CA9*1025*1025\n" +
			"CA17*0*5*7*0*1\n" +
			"CA17*1*10*0*0*2\n" +
			"CA17*2*25*9*0*24\n" +
			"DA2*41925*1236*41925*1236\n" +
			"TA2*0*0*0*0\n" +
			"LS*0100\n" +
			"PA1*10*25*\n" +
			"PA2*23*900*23*900*0*0\n" +
			"PA1*11*25*\n" +
			"PA2*9*300*9*300*0*0\n" +
			"SD1*123456\n" +
			"G85*67A5\n" +
			"SE*55*0001\n" +
			"DXE*1*1  ";
			//"DXE*1*1";
		// Test for YogurtBot
		// fData = "Test Data.";


		// The '24' code sets-up the header.
		if (str.equalsIgnoreCase("24"))
		{
			// Test for YogurtBot
			//String fName = "YogurtBot";

			String fName = "Short File Test"; // A descriptive name.
			// String fType = "0"; // Code for the file type DEX.  This varies depending on what you send up.
			String fType = "26"; // Code for the generic file type.
			String fSize = Integer.toString(fData.length()); // Total file size
			String fCRC;
			String fRecords = "1"; // Number of records.
			String fRSize = fSize; // Size of each record.
						
			// Code to calculate the file CRC
			// Convert the input string into a Hex string.
			tmpstr = convHex2Str (fData);
					
			// Calculate the CRC.
			int valCRC = newCRC.getCRC(tmpstr);
			
			// Ensure the CRC is four characters long.  Pad with zeros if necessary.
			fCRC = Integer.toHexString(valCRC).toUpperCase();
	        for(int j = 0; j < 4 - fCRC.length(); j++)
	        {
	            fCRC = "0" + fCRC;
	        }
					
	        // Build the raw input string.  This is the header string.
			inpString = str + '|' + fName + '|' + fType + '|' +fSize + '|' + fCRC + '|' + fRecords + '|' + fRSize;
			
			int bytelen = inpString.length();
			
			// Create a new byte array
			ByteArray = new byte[bytelen];

			// Populate the byte array with the string contents.
			// Substitute the record and group separators.
			for(int j=0; j<bytelen; j++)
			{
				if (j < inpString.length()) 
				{
					ByteArray[j] = (byte) inpString.charAt(j);
					if (ByteArray[j] == '|')
					{
						ByteArray[j] = (byte) rs;
					}
					else if (ByteArray[j] == '~')
					{
						ByteArray[j] = (byte) gs;
					}
				}
			}
		}
		else  // 25
		{
			// After the header has been processed send up each file part.
			// Here we build the part of the file needed to be sent up
			// based on previous processing parameters.
			String fRecordNum = "1";
			String fNumRecords = "1";
			
			// Setup string with header information.
			inpString = str + "|" + fRecordNum + "|" + fNumRecords + "|" + fData;
			
			int bytelen = inpString.length();
			
			// Create a new byte array
			ByteArray = new byte[bytelen];

			// Populate the byte array with the string contents.
			// Substitute the record and group separators.
			for(int j=0; j<bytelen; j++)
			{
				if (j < inpString.length()) 
				{
					ByteArray[j] = (byte) inpString.charAt(j);
					if (ByteArray[j] == '|')
					{
						ByteArray[j] = (byte) rs;
					}
					else if (ByteArray[j] == '~')
					{
						ByteArray[j] = (byte) gs;
					}
				}
			}
		}

		// Convert the input string into a Hex string.
		tmpstr = convHex2Str (inpString);
				
		// Calculate the CRC of this part.
		int valCRC = newCRC.getCRC(tmpstr);
		hexstr = Integer.toHexString(valCRC).toUpperCase();
		byte[] CRCByte = hex2Byte(hexstr);
				
		// Convert the byte arrays into string objects.
		tmpstr = new String(CRCByte);
		str = new String(ByteArray);
		
		// Set the serial string to a readable format.
		serialString = str + tmpstr;
	}
	
	
	// Convert Hex pairs to bytes
    private byte[] hex2Byte(String str)
    {
       byte[] bytes = new byte[str.length() / 2];
       for (int i = 0; i < bytes.length; i++)
       {
          bytes[i] = (byte) Integer.parseInt(str.substring(2 * i, 2 * i + 2), 16);
       }
       return bytes;
    }
	
    
    // Convert a string to a string of Hex pairs.
	private static String convHex2Str (String str)
	{
		char[] CArray = str.toCharArray();
		String strout = "";
		
		for (int i=0; i < CArray.length; i++)
		{
			if (CArray[i] == '|')
			{
				strout = strout + "1E"; // rs
			}
			else if (CArray[i] == '~')
			{
				strout = strout + "1D"; // gs
			}
			else
			{
				strout = strout + convHex2Char (CArray[i]);
			}
		}
			
		return (strout);
	}
	
	
	// Convert the character to a Hex pair.
	private static String convHex2Char (char str)
	{
        StringBuffer ostr = new StringBuffer();
        String hex = Integer.toHexString(str & 0xFF);  
        for(int j = 0; j < 2 - hex.length(); j++)
        {
            ostr.append("0");
        }

        ostr.append(hex.toUpperCase());

        return (new String(ostr));      
	}
	
	
	public String returnCommand () {
		return (serialString);
	}

}
