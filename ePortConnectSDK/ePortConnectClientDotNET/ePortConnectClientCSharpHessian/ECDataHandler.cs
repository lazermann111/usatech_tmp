namespace com.usatech.ec
{
    public class ECDataHandler
    {
        protected ECDataSource dataSource;
        protected byte[] fileBytes;

        public ECDataSource getDataSource() {
            return dataSource;
        }

        public void setDataSource(ECDataSource dataSource)
        {
            this.dataSource = dataSource;
        }

        public byte[] getFileBytes()
        {
            return fileBytes;
        }

        public void setFileBytes(byte[] fileBytes)
        {
            this.fileBytes = fileBytes;
        }
    }
}