namespace com.usatech.ec
{
    public class ECProcessUpdatesResponse : ECResponse
    {
        protected string fileName = "";
        protected int fileType = -1;
        protected long fileSize = 0;
        protected ECDataHandler fileContent = new ECDataHandler();

        public string getFileName()
        {
            return fileName;
        }
        public void setFileName(string fileName)
        {
            this.fileName = fileName;
        }
        public int getFileType()
        {
            return fileType;
        }
        public void setFileType(int fileType)
        {
            this.fileType = fileType;
        }
        public long getFileSize()
        {
            return fileSize;
        }
        public void setFileSize(long fileSize)
        {
            this.fileSize = fileSize;
        }
        public ECDataHandler getFileContent()
        {
            return fileContent;
        }
        public void setFileContent(ECDataHandler fileContent)
        {
            this.fileContent = fileContent;
        }
    }
}