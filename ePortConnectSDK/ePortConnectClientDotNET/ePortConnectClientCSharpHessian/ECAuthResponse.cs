namespace com.usatech.ec
{
    public class ECAuthResponse : ECResponse
    {

        protected long approvedAmount = 0;

        public long getApprovedAmount()
        {
            return approvedAmount;
        }
        public void setApprovedAmount(long approvedAmount)
        {
            this.approvedAmount = approvedAmount;
        }
    }
}
