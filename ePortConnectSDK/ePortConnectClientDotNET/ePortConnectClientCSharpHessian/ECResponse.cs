namespace com.usatech.ec
{
    public class ECResponse
    {
        protected string serialNumber;
        protected int returnCode = 0;
        protected string returnMessage = "Failed";
        protected string newUsername = "";
        protected string newPassword = "";

        public string getSerialNumber()
        {
            return serialNumber;
        }
        public void setSerialNumber(string serialNumber)
        {
            this.serialNumber = serialNumber;
        }
        public int getReturnCode()
        {
            return returnCode;
        }
        public void setReturnCode(int returnCode)
        {
            this.returnCode = returnCode;
        }
        public string getReturnMessage()
        {
            return returnMessage;
        }
        public void setReturnMessage(string returnMessage)
        {
            this.returnMessage = returnMessage;
        }
        public string getNewUsername()
        {
            return newUsername;
        }
        public void setNewUsername(string newUsername)
        {
            this.newUsername = newUsername;
        }
        public string getNewPassword()
        {
            return newPassword;
        }
        public void setNewPassword(string newPassword)
        {
            this.newPassword = newPassword;
        }
    }
}