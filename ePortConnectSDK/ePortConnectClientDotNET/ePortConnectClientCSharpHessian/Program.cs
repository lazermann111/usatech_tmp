﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using hessiancsharp.client;

using com.usatech.ec;

namespace ePortConnectClientCSharpHessian
{
    class Program
    {
        public static readonly string EPORT_CONNECT_HESSIAN_URL = "https://ec-ecc.usatech.com:9443/hessian/ec";
        public static readonly string SERIAL_NUMBER = "K3MTB000001";
	    public static readonly string USERNAME = "testaccount1";
        public static readonly string PASSWORD = "B28K474FRNRzgN#7Mk@Y";
	    public static readonly string CARD_DATA = "371449635398431=20121015432112345678";
	    public static readonly string ENCRYPTED_CARD_DATA = "09E1695E7F487243DEFD6679DCC099EA46BCE53A157D01E691656B83A3A5DFC42691B984A31472E0";
	    public static readonly string KEY_SERIAL_NUMBER = "9011040B005166000014";
        public static readonly long TRANSACTION_ID = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
        public static readonly string FILE_NAME = "ePortConnectClientCSharpHessian.exe.config";
	    public static readonly int FILE_TYPE = 16;

        public static readonly int RES_FAILED = 0; //Transaction failed
	    public static readonly int RES_OK = 1; //Transaction was successful
	    public static readonly int RES_APPROVED = 2; //Auth or Charge was approved and transaction was submitted for settlement
	    public static readonly int RES_DECLINED = 3; //Auth or Charge was declined
	    public static readonly int RES_PARTIALLY_APPROVED = 4; //Auth was partially approved
	    public static readonly int RES_OK_NO_UPDATE = 5; //Function succeeded, no updates pending
	    public static readonly int RES_OK_UPLOAD_FILE = 6; //Function succeeded, received request to upload a file to the server   
	
	    public static readonly string TRAN_RESULT_SUCCESS = "S"; //Transaction was successful and receipt printed
	    public static readonly string TRAN_RESULT_SUCCESS_NO_USE_PRINTER = "Q"; //Transaction was successful, receipt printer is not used
	    public static readonly string TRAN_RESULT_SUCCESS_RECEIPT_PROBLEM = "R"; //Transaction was successful but there was a receipt printing problem
	    public static readonly string TRAN_RESULT_SUCCESS_NO_RECEIPT_REQUESTED = "N"; //Transaction was successful and no receipt was requested
	    public static readonly string TRAN_RESULT_FAILED = "F"; //Transaction failed
	    public static readonly string TRAN_RESULT_CANCELLED = "C"; //Transaction cancelled
	    public static readonly string TRAN_RESULT_TIMED_OUT = "T"; //Transaction timed out
	    public static readonly string TRAN_RESULT_UNABLE_TO_AUTH = "U"; //Unable to authorize
	
	    public static readonly string CARD_TYPE_SWIPED = "C"; //Swiped track 2 data
	    public static readonly string CARD_TYPE_RFID = "R"; //Contactless track 2 data
	
	    public static readonly int CARD_READER_MAGTEK_MAGNESAFE = 1; //MagTek encrypting card reader
	    public static readonly int CARD_READER_IDTECH_SECUREMAG = 2; //ID TECH encrypting card reader
	
	    public static readonly int FILE_TYPE_APPLICATION_UPGRADE = 5;
	    public static readonly int FILE_TYPE_EXECUTABLE_FILE = 7;
	    public static readonly int FILE_TYPE_KIOSK_GENERIC_FILE = 16;
	    public static readonly int FILE_TYPE_LOG_FILE = 9;

        public static readonly int UPDATE_STATUS_NORMAL = 0;
        public static readonly int UPDATE_STATUS_RETRY = 1;

	    protected static CHessianProxyFactory hessianProxyFactory = null;
	    protected static ECServiceAPI ePortConnect = null;

        static void Main(string[] args)
        {
            processMain(args, EPORT_CONNECT_HESSIAN_URL);
        }

        public static long getCurrentUtcMillis() {
            return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds / 1000;
        }

        public static string getUsage(string url)
        {
            string commandPrefix = "ePortConnectClientCSharpHessian.exe";
            long tranUTCTimeMs = getCurrentUtcMillis();
            long tranUTCOffsetMs = (long) TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).TotalMilliseconds;

            return new StringBuilder()
                .Append("\nPlease provide arguments to use one of the functions below. You may need to change the classpath for your environment.\n\n")
                .Append("authV3 <url> <username> <password> <serialNumber> <tranId> <amount> <cardData> <cardType>\n")
                .Append("authV3_1 <url> <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <cardType>\n")
                .Append("batchV3 <url> <username> <password> <serialNumber> <tranId> <amount> <tranResult> <tranDetails>\n")
                .Append("cashV3 <url> <username> <password> <serialNumber> <tranId> <amount> <tranUTCTimeMs> <tranUTCOffsetMs> <tranResult> <tranDetails>\n")
                .Append("chargeV3 <url> <username> <password> <serialNumber> <tranId> <amount> <cardData> <cardType> <tranResult> <tranDetails>\n")
                .Append("chargeV3_1 <url> <username> <password> <serialNumber> <tranId> <amount> <cardReaderType> <decryptedCardDataLen> <encryptedCardDataHex> <ksnHex> <cardType> <tranResult> <tranDetails>\n")
                .Append("uploadFile <url> <username> <password> <serialNumber> <fileName> <fileType>\n")
                .Append("processUpdates <url> <username> <password> <serialNumber> <updateStatus>\n")
                .Append("\nExamples:\n\n")
                .Append(commandPrefix).Append(" authV3 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID).Append(" 100 ").Append(CARD_DATA).Append(" ").Append(CARD_TYPE_SWIPED).Append("\n")
                .Append(commandPrefix).Append(" batchV3 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID).Append(" 60 ").Append(TRAN_RESULT_SUCCESS).Append(" \"A0|302|10|1|1|305|10|5|1\"").Append("\n")
                .Append(commandPrefix).Append(" authV3_1 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID + 1).Append(" 100 ").Append(CARD_READER_MAGTEK_MAGNESAFE).Append(" 37 ").Append(ENCRYPTED_CARD_DATA).Append(" ").Append(KEY_SERIAL_NUMBER).Append(" ").Append(CARD_TYPE_SWIPED).Append("\n")
                .Append(commandPrefix).Append(" batchV3 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID + 1).Append(" 50 ").Append(TRAN_RESULT_SUCCESS).Append(" \"A0|302|10|1|1|305|10|4|1\"").Append("\n")
                .Append(commandPrefix).Append(" cashV3 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID + 2).Append(" 80 ").Append(tranUTCTimeMs).Append(" ").Append(tranUTCOffsetMs).Append(" ").Append(TRAN_RESULT_SUCCESS).Append(" \"A0|302|10|3|1|305|10|5|1\"").Append("\n")
                .Append(commandPrefix).Append(" chargeV3 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID + 3).Append(" 110 ").Append(CARD_DATA).Append(" ").Append(CARD_TYPE_SWIPED).Append(" ").Append(TRAN_RESULT_SUCCESS).Append(" \"A0|302|10|6|1|305|10|5|1\"").Append("\n")
                .Append(commandPrefix).Append(" chargeV3_1 ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(TRANSACTION_ID + 4).Append(" 120 ").Append(CARD_READER_MAGTEK_MAGNESAFE).Append(" 37 ").Append(ENCRYPTED_CARD_DATA).Append(" ").Append(KEY_SERIAL_NUMBER).Append(" ").Append(CARD_TYPE_SWIPED).Append(" ").Append(TRAN_RESULT_SUCCESS).Append(" \"A0|302|10|7|1|305|10|5|1\"").Append("\n")
                .Append(commandPrefix).Append(" uploadFile ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" \"").Append(FILE_NAME).Append("\" ").Append(FILE_TYPE).Append("\n")
                .Append(commandPrefix).Append(" processUpdates ").Append(url).Append(" \"").Append(USERNAME).Append("\" \"").Append(PASSWORD).Append("\" ").Append(SERIAL_NUMBER).Append(" ").Append(UPDATE_STATUS_NORMAL).Append("\n")
                .ToString();
        }

        public static void processMain(string[] args, string url) {
		    if (args.Length < 1) {
			    Console.WriteLine(getUsage(url));
			    return;
		    }
		    string function = args[0];
		    if ("authV3".Equals(function)) {
			    if (args.Length < 9)
				    Console.WriteLine(getUsage(url));
			    else
				    testAuthV3(args[1], args[2], args[3], args[4], long.Parse(args[5]), long.Parse(args[6]), args[7], args[8]);		
		    } else if ("authV3_1".Equals(function)) {
			    if (args.Length < 12)
				    Console.WriteLine(getUsage(url));
			    else
				    testAuthV3_1(args[1], args[2], args[3], args[4], long.Parse(args[5]), long.Parse(args[6]), int.Parse(args[7]), int.Parse(args[8]), args[9], args[10], args[11]);
		    } else if ("batchV3".Equals(function)) {
			    if (args.Length < 9)
				    Console.WriteLine(getUsage(url));
			    else
				    testBatchV3(args[1], args[2], args[3], args[4], long.Parse(args[5]), long.Parse(args[6]), args[7], args[8]);
		    } else if ("cashV3".Equals(function)) {
			    if (args.Length < 11)
				    Console.WriteLine(getUsage(url));
			    else
				    testCashV3(args[1], args[2], args[3], args[4], long.Parse(args[5]), long.Parse(args[6]), long.Parse(args[7]), int.Parse(args[8]), args[9], args[10]);
		    } else if ("chargeV3".Equals(function)) {
			    if (args.Length < 11)
				    Console.WriteLine(getUsage(url));
			    else
				    testChargeV3(args[1], args[2], args[3], args[4], long.Parse(args[5]), long.Parse(args[6]), args[7], args[8], args[9], args[10]);
		    } else if ("chargeV3_1".Equals(function)) {
			    if (args.Length < 14)
				    Console.WriteLine(getUsage(url));
			    else
				    testChargeV3_1(args[1], args[2], args[3], args[4], long.Parse(args[5]), long.Parse(args[6]), int.Parse(args[7]), int.Parse(args[8]), args[9], args[10], args[11], args[12], args[13]);
		    } else if ("uploadFile".Equals(function)) {
			    if (args.Length < 7)
				    Console.WriteLine(getUsage(url));
			    else
				    testUploadFile(args[1], args[2], args[3], args[4], args[5], int.Parse(args[6]));
		    } else if ("processUpdates".Equals(function)) {
			    if (args.Length < 5)
				    Console.WriteLine(getUsage(url));
			    else
                    testProcessUpdates(args[1], args[2], args[3], args[4], int.Parse(args[5]));
		    } else
			    Console.WriteLine(getUsage(url));
	    }
	
	    protected static string getBasicResponseMessage(ECResponse response) {
		    string message = "Response returnCode: " + response.getReturnCode() + ", returnMessage: " + response.getReturnMessage();
		    if (response.getNewUsername().Length > 0)
			    message += ", newUsername: " + response.getNewUsername();
		    if (response.getNewPassword().Length > 0)
			    message += ", newPassword: " + response.getNewPassword();
		    return message;
	    }

	    protected static void testAuthV3(string url, string username, string password, string serialNumber, long tranId, 
			    long amount, string cardData, string cardType) {
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECAuthResponse response = ePortConnect.authV3(username, password, serialNumber, 
				    tranId, amount, cardData, cardType);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response) 
					    + ", approvedAmount: " + response.getApprovedAmount();
			    if (response.getReturnCode() == RES_APPROVED)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }
	
	    protected static void testBatchV3(string url, string username, string password, string serialNumber, long tranId, 
			    long amount, string tranResult, string tranDetails) {
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECResponse response = ePortConnect.batchV3(username, password, serialNumber, tranId, amount, tranResult, tranDetails);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response);
			    if (response.getReturnCode() == RES_OK)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }
	
	    protected static void testAuthV3_1(string url, string username, string password, string serialNumber, long tranId, long amount, 
			    int cardReaderType, int decryptedCardDataLen, string encryptedCardDataHex, string ksnHex, string cardType) {
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECAuthResponse response = ePortConnect.authV3_1(username, password, serialNumber, tranId, amount, 
				    cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, cardType);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response) 
					    + ", approvedAmount: " + response.getApprovedAmount();
			    if (response.getReturnCode() == RES_APPROVED)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }
	
	    protected static void testCashV3(string url, string username, string password, string serialNumber, long tranId, 
			    long amount, long tranUTCTimeMs, int tranUTCOffsetMs, string tranResult, string tranDetails) {
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECResponse response = ePortConnect.cashV3(username, password, serialNumber, tranId, 
				    amount, tranUTCTimeMs, tranUTCOffsetMs, tranResult, tranDetails);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response);
			    if (response.getReturnCode() == RES_OK)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }

	    protected static void testChargeV3(string url, string username, string password, string serialNumber, long tranId, 
			    long amount, string cardData, string cardType, string tranResult, string tranDetails) {
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECAuthResponse response = ePortConnect.chargeV3(username, password, serialNumber, tranId, 
				    amount, cardData, cardType, tranResult, tranDetails);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response) 
					    + ", approvedAmount: " + response.getApprovedAmount();
			    if (response.getReturnCode() == RES_APPROVED)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }
	
	    protected static void testChargeV3_1(string url, string username, string password, string serialNumber, long tranId, 
			    long amount, int cardReaderType, int decryptedCardDataLen, string encryptedCardDataHex, string ksnHex,
			    string cardType, string tranResult, string tranDetails) {
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECAuthResponse response = ePortConnect.chargeV3_1(username, password, serialNumber, tranId, 
				    amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex,
				    cardType, tranResult, tranDetails);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response) 
					    + ", approvedAmount: " + response.getApprovedAmount();
			    if (response.getReturnCode() == RES_APPROVED)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }
	
	    protected static void testUploadFile(string url, string username, string password, string serialNumber, string fileName, int fileType) {		
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);		
		    byte[] fileBytes = File.ReadAllBytes(fileName);
            ECDataHandler dh = new ECDataHandler();
            dh.setFileBytes(fileBytes);
            ECResponse response = ePortConnect.uploadFile(username, password, serialNumber, fileName, fileType, fileBytes.LongLength, dh);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response);
			    if (response.getReturnCode() == RES_OK)
				    Console.WriteLine(message);
			    else
				    throw new Exception(message);
		    }
	    }

        protected static void testProcessUpdates(string url, string username, string password, string serialNumber, int updateStatus)
        {		
		    if (hessianProxyFactory == null)
			    hessianProxyFactory = new CHessianProxyFactory();
		    if (ePortConnect == null)
			    ePortConnect = (ECServiceAPI) hessianProxyFactory.Create(Type.GetType("com.usatech.ec.ECServiceAPI"), url);
		    ECProcessUpdatesResponse response = ePortConnect.processUpdates(username, password, serialNumber, updateStatus);
		    if (response == null)
			    throw new Exception("Received null response");
		    else {
			    string message = getBasicResponseMessage(response)
					    + ", fileName: " + response.getFileName() + ", fileType: " + response.getFileType() + ", fileSize: " + response.getFileSize();
			    if (response.getReturnCode() == RES_OK || response.getReturnCode() == RES_OK_NO_UPDATE
					    || response.getReturnCode() == RES_OK_UPLOAD_FILE) {
                    if (response.getReturnCode() == RES_OK && response.getFileContent() != null && response.getFileContent().getDataSource() != null
                        && response.getFileContent().getDataSource().getInputStream() != null && response.getFileContent().getDataSource().getInputStream().LongLength > 0)
                        File.WriteAllBytes(response.getFileName(), response.getFileContent().getDataSource().getInputStream());
				    else if (response.getReturnCode() == RES_OK_UPLOAD_FILE && response.getFileName() != null && response.getFileName().Length > 0)
					    testUploadFile(url, username, password, serialNumber, response.getFileName(), FILE_TYPE_KIOSK_GENERIC_FILE);
				    Console.WriteLine(message);
			    } else
				    throw new Exception(message);
		    }
	    }
    }
}
