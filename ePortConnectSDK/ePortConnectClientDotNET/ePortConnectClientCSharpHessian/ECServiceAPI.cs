namespace com.usatech.ec
{
    public interface ECServiceAPI
    {
        // Process a card authorization with card data from a non-encrypting card reader 
        ECAuthResponse authV3(string username, string password, string serialNumber, long tranId,
                long amount, string cardData, string cardType);

        // Process a card authorization with card data from an encrypting card reader
        ECAuthResponse authV3_1(string username, string password, string serialNumber, long tranId, long amount,
                int cardReaderType, int decryptedCardDataLen, string encryptedCardDataHex, string ksnHex, string cardType);

        // Settle a previously authorized transaction
        ECResponse batchV3(string username, string password, string serialNumber, long tranId,
                long amount, string tranResult, string tranDetails);

        // Upload a cash transaction
        ECResponse cashV3(string username, string password, string serialNumber, long tranId,
                long amount, long tranUTCTimeMs, int tranUTCOffsetMs, string tranResult, string tranDetails);

        // Authorize and settle a transaction with card data from a non-encrypting card reader 
        ECAuthResponse chargeV3(string username, string password, string serialNumber, long tranId,
                long amount, string cardData, string cardType, string tranResult, string tranDetails);

        // Authorize and settle a transaction with card data from an encrypting card reader
        ECAuthResponse chargeV3_1(string username, string password, string serialNumber, long tranId,
                long amount, int cardReaderType, int decryptedCardDataLen, string encryptedCardDataHex, string ksnHex,
                string cardType, string tranResult, string tranDetails);

        // Upload a file to the Server
        ECResponse uploadFile(string username, string password, string serialNumber, string fileName,
                int fileType, long fileSize, ECDataHandler fileContent);

        // Download updates from the Server
        ECProcessUpdatesResponse processUpdates(string username, string password, string serialNumber, int updateStatus);
    }
}
