namespace com.usatech.ec
{
    public class ECDataSource {
	    protected byte[] inputStream;

	    public byte[] getInputStream() {
		    return inputStream;
	    }

        public void setInputStream(byte[] inputStream) {
            this.inputStream = inputStream;
        }
    }
}
