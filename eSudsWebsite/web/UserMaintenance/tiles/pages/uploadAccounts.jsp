<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script language="javascript">
function downloadStudentAccounts(ext) {
	var frm = document.forms["uploadAccountsForm"];
	window.location = frm.action + "?method=get&campusId=" + frm.elements["campusId"].value + "&ext=" + ext;
}
</script>
<span class="title">Student Account Upload</span>
<html:form action="/uploadAccounts" enctype="multipart/form-data">
<html:hidden property="method" value="update"/>
<ol class="instructions">
<bean:size id="campusCount" name="campuses"/>
<logic:notEqual name="campusCount" value="1">
	<li>Select the Campus to which you want to add/update accounts:<br/>
	<html:select property="campusId">
		<html:options collection="campuses" labelProperty="name" property="campusId"/>
	</html:select></li>
</logic:notEqual>
<logic:equal name="campusCount" value="1">
	<logic:iterate id="campus" name="campuses" length="1">
	<li>Verify that you want to add/update accounts for the Campus:<br/>
	<html:hidden property="campusId" name="campus"/>
	<input type="text"  readonly="true" size="80"
	value="<bean:write name="campus" property="schoolName" /> &#187; <bean:write name="campus" property="name" />"/>
	</li>
	</logic:iterate>
</logic:equal>
<li>Download currently active students as an
<html:link styleClass="filelinks" href="javascript: downloadStudentAccounts('xls');" title="Download Student Accounts Excel File">Excel file</html:link> or
<html:link styleClass="filelinks" href="javascript: downloadStudentAccounts('csv');" title="Download Student Accounts CSV File">CSV file</html:link>. These files contain a header row with the following columns:
<ul>
<li>RECORD_ACTION_CODE (C&nbsp;-&nbsp;Create, U&nbsp;-&nbsp;Update, D&nbsp;-&nbsp;Deactivate, N&nbsp;-&nbsp;Ignore)<span class="required">*</span></li>
<li>STUDENT_EMAIL_ADDR<span class="required">*</span></li>
<li>STUDENT_ACCOUNT_CD<span class="required">*</span></li>
<li>STUDENT_ACCOUNT_BALANCE<span class="note1">&#135;</span></li>
<li>STUDENT_FNAME</li>
<li>STUDENT_LNAME</li>
<li>STUDENT_ADDR1</li>
<li>STUDENT_ADDR2</li>
<li>STUDENT_CITY</li>
<li>STUDENT_STATE_CD</li>
<li>STUDENT_POSTAL_CD</li>
<li>AUTO_NOTIFY_ON (Y&nbsp;-&nbsp;Yes, N&nbsp;-&nbsp;No)</li>
<li>ACCOUNT_TYPE (Student,&nbsp;Staff,&nbsp;or&nbsp;Custodial)</li>
</ul>
<p class="required">* - Required.</p>
<p class="note1">&#135; - Only provide if eSuds manages account balances.</p>
</li>
<li>Add a row to your file for each student with the appropriate information in each column.</li>
<li>Click the Browse button and select the file you just created:<br/>
<html:file property="excelFile" size="68" /></li>
<li>Click the following check box if you want to deactivate any accounts not listed in your file:<br/>
<html:checkbox property="deactivateOthers" /> Deactivate Accounts Not Listed</li>
<li>Click the Submit button and wait while your file is uploaded and processed:<br/>
<html:submit property="submit" value="Submit" styleClass="button2" /></li>
</ol>
</html:form>