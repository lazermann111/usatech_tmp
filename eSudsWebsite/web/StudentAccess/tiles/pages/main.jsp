<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<logic:notPresent scope="session" name="subject">
	<tiles:insert definition=".login.form"  flush="true" />
</logic:notPresent>
<logic:present scope="session" name="subject">
</logic:present>


