<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="title">Notification Configuration</font>
<br/><font class="instructions">Please enter your school email address and click Submit.<br/>
An email will be sent to you which will allow you to<br/>
register to receive automatic email notification<br/>
when your laundry wash and dry cycles are completed.</font>
<table>
<tr><td>
<html:form action="notifyConfig">
<html:hidden property="action" value="request" />
<table border="0" align="left" cellpadding="2">
<tr>
 <th align="right">School email address:</th>
 <td align="left"><html:text property="primaryEmailAddr" styleClass="input_style" size="30"/></td>
</tr>
<tr>
 <td align="center" colspan="2"><html:submit property="submit"  value="Submit" styleClass="button2" /></td>
</tr>
</table>
</html:form>
</td></tr>
</table>