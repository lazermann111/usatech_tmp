<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="title">Notification Configuration</font>
<table>
<tr><td>
<html:form action="notifyConfig">
<html:hidden property="action" value="update" />
<html:hidden property="primaryEmailAddr" />
<html:hidden property="passCode" />
<html:hidden property="hostStatusNotifyTypeId" />
<table border="0" align="left" cellpadding="2">
<tr>
 <th align="right">School Email Address:</th>
 <td align="left"><bean:write name="notifyConfigForm" property="primaryEmailAddr" /></td>
</tr>
<tr>
 <th align="right">Cycle Completed Notification:</th>
 <td align="left">
 	<html:radio property="notifyOn" value="true"/>On&nbsp;&nbsp;&nbsp;
 	<html:radio property="notifyOn" value="false"/>Off
 </td>
</tr>
<tr>
 <th align="right">Notify Email Address:</th>
 <td align="left"><html:text property="notifyEmailAddr" styleClass="input_style" size="30"/></td>
</tr>
<tr>
 <td align="center" colspan="2"><html:submit property="submit"  value="Update" styleClass="button2" /></td>
</tr>
</table>
</html:form>
</td></tr>
</table>
<logic:present name="student">
<hr class="separator"/>
<font class="title">Usage</font>
<table>
<tr><td>
<div class="criterion">
<html:form action="notifyConfig">
<html:hidden property="action" value="view" />
<html:hidden property="primaryEmailAddr" />
<html:hidden property="passCode" />
<html:hidden property="hostStatusNotifyTypeId" />
		Show Dates: <html:text size="10" property="startDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('startDate')"><img src="../../images/calendar.gif"  border="0"/></html:link>
 to <html:text size="10" property="endDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('endDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
 	<html:submit property="submit" value="Show" styleClass="button2" />
 	</html:form>
</div>
<table class="report" align="center">
<tr valign="top">
		<TH>Date</TH>
		<TH>Time</TH>
		<TH>Description</TH>
		<TH>Price</TH>
</tr>
		<!-- iterate through transactions -->
		<logic:notEmpty name="tranList" property="transactions">
		<% int i=1; %>
		<logic:iterate name="tranList" property="transactions" id="tran">
			<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
					<td><bean:write name="tran" property="tranStartTs" format="MM/dd/yyyy" /></td>
					<td><bean:write name="tran" property="tranStartTs" format="hh:mm aa" /></td>
					<td><pre><bean:write name="tran" property="tranLineItemDesc" /></pre></td>
					<td><logic:notEqual name="tran" property="authServiceType" value="1">**Included**</logic:notEqual>
						<logic:equal name="tran" property="authServiceType" value="1"><bean:write name="tran" property="tranSettlementAmount" format="0.00" /></logic:equal></td>
			</tr>
		<% i++; %>			
	</logic:iterate>
<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
	<td></td><td></td><td>Total Amount:</td><td><bean:write name="tranList" property="totalAmount" format="0.00" /></td>
</tr>
</logic:notEmpty>
<logic:empty  name="tranList" property="transactions">
<tr class="odd"><td colspan="4" align="center">No Transactions Found.</td></tr>
</logic:empty>
<tr class="footer"><td colspan="4"></td></tr>
</table>
</td></tr>
</table>
</logic:present>