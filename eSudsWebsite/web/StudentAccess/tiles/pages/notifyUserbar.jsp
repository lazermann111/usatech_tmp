<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="user_button">
<logic:present name="roomStatusParams">
	<html:link action="/switch.do?page=/showRoomStatus.do&amp;prefix=/RoomStatus" name="roomStatusParams">View Room Status</html:link>
</logic:present>
<logic:notPresent name="roomStatusParams">
	<html:link action="/switch.do?page=/showRoomStatus.do&amp;prefix=/RoomStatus">View Room Status</html:link>
</logic:notPresent>
</font>