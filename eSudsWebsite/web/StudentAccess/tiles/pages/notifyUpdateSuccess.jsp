<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="title">Update Successful!</font><br/>
<font>
<logic:equal name="notifyConfigForm" property="notifyOn" value="true">
From now on, an email will be sent to '<bean:write name="notifyConfigForm" property="notifyEmailAddr"/>'<br/>
each time your laundry wash and dry cycles are completed!
</logic:equal>
<logic:equal name="notifyConfigForm" property="notifyOn" value="false">
You will no longer receive automatic email notification when your laundry wash and dry cycles are completed.<br/>
If you wish to re-enable notification, click 
<a href="../../RoomStatus/notifyConfig.do?action=request&primaryEmailAddr=<bean:write name="notifyConfigForm" property="primaryEmailAddr"/>">
Request e-mail notification</a>.
</logic:equal>
</font>