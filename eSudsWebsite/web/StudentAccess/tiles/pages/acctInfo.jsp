<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<SCRIPT language="JavaScript" src="<html:rewrite page='/scripts/calendar.js' />" >
</SCRIPT>
<script language="JavaScript" src="<html:rewrite page='/scripts/calendar_helper.js' />">
</script>
<script language="JavaScript">
var calendar = "<html:rewrite page='/tiles/pages/calendar.html' />";
</script>
<font class="title">Account Information</font>
<table>
<tr><td>
<html:form action="/updateAcctInfo">
<html:hidden property="studentId" />
<table border="0" align="left" cellpadding="2">
<tr>
 <th align="right">Status</th>
 <td align="left"><logic:equal name="acctInfoForm" property="active" value="true">Active</logic:equal> <logic:notEqual name="acctInfoForm" property="active" value="true" >Inactive</logic:notEqual></td>
</tr>
<tr>
 <th align="right">Name</th>
 <td align="left"><bean:write  name="student" property="firstName" />
 <bean:write  name="student"property="middleName" /> <bean:write  name="student" property="lastName" /></td>
</tr>
<tr>
 <th align="right">Notify Email Address</th>
 <td align="left"><html:text property="notifyEmailAddr" styleClass="input_style" size="30"/></td>
</tr>
<tr>
 <th align="right">Email Address</th>
 <td align="left"><bean:write name="student" property="emailAddr" /></td>
</tr>
<tr>
 <td align="center"><html:submit property="submit"  value="Update" styleClass="button2" /></td>
 <td align="center"><html:reset styleClass="button2" /></td>
</tr>
</table>
</html:form>
</td></tr>
</table>
<hr class="separator"/>
<font class="title">Usage</font>
<table>
<tr><td>
<p>
<html:form action="/showAcctInfo">
		Show Dates: <html:text size="10" property="startDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('startDate')"><img src="../../images/calendar.gif"  border="0"/></html:link>
 to <html:text size="10" property="endDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('endDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
 	<html:submit value="Show" styleClass="button2" />
 	</html:form>
</p>
<table class="report" align="center">
<tr valign="top">
		<TH>Date</TH>
		<TH>Time</TH>
		<TH>Description</TH>
		<TH>Price</TH>
</tr>
		<!-- iterate through transactions -->
		<logic:notEmpty name="tranList" property="transactions">
		<% int i=1; %>
		<logic:iterate name="tranList" property="transactions" id="tran">
			<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
					<td><bean:write name="tran" property="tranStartTs" format="MM/dd/yyyy" /></td>
					<td><bean:write name="tran" property="tranStartTs" format="hh:mm aa" /></td>
					<td><pre><bean:write name="tran" property="tranLineItemDesc" /></pre></td>
					<td><logic:notEqual name="tran" property="authServiceType" value="1">**Included**</logic:notEqual>
						<logic:equal name="tran" property="authServiceType" value="1"><bean:write name="tran" property="tranSettlementAmount" format="0.00" /></logic:equal></td>
			</tr>
		<% i++; %>			
	</logic:iterate>
<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
	<td></td><td></td><td>Total Amount:</td><td><bean:write name="tranList" property="totalAmount" format="0.00" /></td>
</tr>
</logic:notEmpty>
<logic:empty  name="tranList" property="transactions">
<tr class="odd"><td colspan="4" align="center">No Transactions Found.</td></tr>
</logic:empty>
<tr class="footer"><td colspan="4"></td></tr>
</table>
</td></tr>
</table>