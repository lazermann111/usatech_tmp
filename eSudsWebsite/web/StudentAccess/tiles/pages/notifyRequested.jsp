<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="title">Registration Email Sent!</font><br/>
<font class="instructions">An email has been sent to '<bean:write name="notifyConfigForm" property="primaryEmailAddr"/>'<br/> 
which will allow you to register for automatic laundry email notification.<br/>
The email will contain instructions on how to proceed.
</font>