<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="navigation_left">
<logic:present cookie="operatorId">
<html:link action="/showRoomStatus" paramId="operatorId" paramName="result" paramProperty="operator.operatorId"><bean:write name="result" property="operator.name" /></html:link>&#187; 
</logic:present>
<html:link action="/showRoomStatus" paramId="schoolId" paramName="result" paramProperty="campus.schoolBean.schoolId"><bean:write name="result" property="campus.schoolBean.name" /></html:link>&#187; 
<html:link action="/showRoomStatus" paramId="campusId" paramName="result" paramProperty="campus.campusId"><bean:write name="result" property="campus.name" /></html:link>
</font>
<font class="navigation_right">
<font class="refreshNote">This screen will refresh in <span id="refreshCountdown">60</span> seconds</font>
</font>
