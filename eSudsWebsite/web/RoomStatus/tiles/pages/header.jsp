<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<div class="header_left">
<html:link action="/eSudsHome"><img class="logo" src="../../images/esuds_logo.jpg" alt="esuds logo" height="100" width="560" border="0" /></html:link>
</div>
<div class="header_right">
<form>
<logic:equal name="CUSTOM_FEATURES" property="STUDENT_ACCESS" value="FULL">
<html:link href="../../StudentAccess/home.do">
<input type="button" value="Access Your Account" onclick="this.parentNode.click()"/>
</html:link><br/>
</logic:equal>
<logic:equal name="CUSTOM_FEATURES" property="STUDENT_ACCESS" value="NOTIFY">
<html:link action="/notifyConfig.do?action=start">
<input type="button" value="Register for Auto-Notification" onclick="this.parentNode.click()"/>
</html:link><br/>
</logic:equal>
<logic:equal name="CUSTOM_FEATURES" property="STUDENT_ACCESS" value="NOTIFY_USAGE">
<html:link action="/notifyConfig.do?action=start">
<input type="button" value="View Usage / Register for Auto-Notification" onclick="this.parentNode.click()"/>
</html:link><br/>
</logic:equal>
<html:link action="/switch.do?page=/faq.do&amp;prefix=">
<input type="button" value="FAQs" onclick="this.parentNode.click()"/>
</html:link>
</form>
</div>

