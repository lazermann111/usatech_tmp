<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<logic:notEmpty name="result" property="selectedRoom">
<font class="title"><bean:write name="result" property="selectedRoom.laundryRoom" /></font>
<p>
<font class="subtitle">Summary</font>
<br><bean:write name="result" property="selectedRoom.availableWashers" format=""/> out of <bean:write name="result" property="selectedRoom.totalWashers" format=""/> washers available.
<br><bean:write name="result" property="selectedRoom.availableDryers" format=""/> out of <bean:write name="result" property="selectedRoom.totalDryers" format=""/> dryers available.
</p>
<html:form action="/requestNotification">
<html:hidden property="roomId" name="result" />
<p>To be notified when a machine becomes available:
select the box next to the machine for which you want to be notified, enter your e-mail address, and click "Notify Me".</p>
<table class="form">
<tr>
<td class="label">
Email Address: 
</td>
<td class="field">
<html:text property="emailAddress" styleClass="input_style"/> 
</td>
<td align="left">
<html:submit value="Notify Me" styleClass="button2"/>
</td>
</tr>
</table>
<table class="room_status">	
	<tr>
		<th></th>
		<th>Washer #</th>
		<th>Washer Type</th>
		<th>Availability</th>
		<th>Est. Min. Remaining</th>
	</tr>
<logic:empty name="result" property="selectedRoom.washers"><tr align="center">
			<td colspan="5">NO WASHERS FOUND</td>
		</tr></logic:empty>
<% int i=1; %>
<logic:iterate name="result" property="selectedRoom.washers" id="machine">
		<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>' >
			<bean:define id="machineId" name="machine" property="machineId" />
			<td>
			<logic:notEqual  name="machine" property="availability" value="Available">
					<html:multibox styleClass="checkbox" property="selectedMachines" value='<%="" + machineId %>' />
			</logic:notEqual>
			</td>
			<td><bean:write name="machine" property="label" /></td>
			<td><bean:write name="machine" property="name" /> <bean:write name="machine" property="position" /></TD>
			<td>
			<logic:equal name="machine" property="availability" value="Available">
					<font color="green"><bean:write name="machine" property="availability" /></font>
				</logic:equal>
				<logic:notEqual  name="machine" property="availability" value="Available">
					<font color="red"><bean:write name="machine" property="availability" /></font>
				</logic:notEqual>
			</td>
			<td><bean:write name="machine" property="timeRemaining" /></td>
		</tr>
		<% i++; %>
</logic:iterate>
	<tr class ="footer">
		<td colspan="5"></td>
	</tr>
	<tr><td colspan="5" height="2">&nbsp;</td></tr>
<tr>
		<th></th>
		<th>Dryer #</th>
		<th>Dryer Type</th>
		<th>Availability</th>
		<th>Est. Min. Remaining</th>
	</TR>
<logic:empty name="result" property="selectedRoom.dryers"><tr align="center">
			<td colspan="5">NO DRYERS FOUND</td>
		</tr></logic:empty>
<% int j=1; %>
<logic:iterate name="result" property="selectedRoom.dryers" id="machine">
		<tr class='<%= (j % 2 == 1) ? "odd" : "even" %>' >
			<bean:define id="machineId" name="machine" property="machineId" />
			<td><logic:notEqual  name="machine" property="availability" value="Available">
					<html:multibox styleClass="checkbox" property="selectedMachines" value='<%="" + machineId %>' />
			</logic:notEqual>
			</td>
			<td><bean:write name="machine" property="label" /></td>
			<td><bean:write name="machine" property="name" /> <bean:write name="machine" property="position" /></td>
			<td>
				<logic:equal name="machine" property="availability" value="Available">
					<FONT color="green"><bean:write name="machine" property="availability" /></FONT>
				</logic:equal>
				<logic:notEqual  name="machine" property="availability" value="Available">
					<FONT color="red"><bean:write name="machine" property="availability" /></FONT>
				</logic:notEqual>
			</TD>
			<TD><bean:write name="machine" property="timeRemaining" /></TD>
		</tr>
		<% j++; %>
</logic:iterate>
	<tr class ="footer">
		<td colspan="5"></td>
	</tr>
</table>
<logic:notEmpty name="result" property="selectedRoom.roomImageUrl">
<img src="<bean:write name="result" property="selectedRoom.roomImageUrl"/>"/>
</logic:notEmpty>
</html:form>
</logic:notEmpty>
<logic:empty name="result" property="selectedRoom">
<font class="title">No Laundry Rooms Found</font>
</logic:empty>
<script language="javascript">
var refreshSeconds = 120;
window.setTimeout('document.location.reload()', refreshSeconds * 1000);
var startTime = new Date().getTime();
window.setInterval('setCountdown()', 1000);

function setCountdown() {
	document.getElementById("refreshCountdown").innerHTML = Math.max(1, refreshSeconds - Math.round((new Date().getTime() - startTime)/1000));
}
</script>
