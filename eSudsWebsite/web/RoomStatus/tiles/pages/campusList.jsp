<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="title">School - Campus</font>
<p>
<logic:empty name="campuses">
No campuses found.
</logic:empty>
<logic:notEmpty name="campuses">
<logic:iterate name="campuses" id="campus">
<html:link action="/showRoomStatus" paramId="campusId" paramName="campus" paramProperty="campusId"><bean:write name="campus" property="schoolName" /> - <bean:write name="campus" property="name" />
</html:link>
<br />
</logic:iterate>
</logic:notEmpty>
</p>
