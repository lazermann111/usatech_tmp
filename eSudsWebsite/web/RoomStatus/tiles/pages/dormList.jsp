<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<font class="title">Residence Halls</font>
<br /><br /><%/*
<logic:present cookie="operatorId"><html:form action="/showRoomStatus">
&#187;<html:select name="result" property="roomId" onchange="this.form.submit();">
<html:options collection="allRoomsResult" labelProperty="name" property="laundryRoomId"/>	 
</html:select></html:form>
</logic:present>
<logic:present cookie="operatorId">
<a class="selectBoxText" href="javascript:" onclick="openBox('roomList')">Room Goes Here</a>
<div id="roomList" class="selectBoxList">
<logic:iterate property="allRoomsResult" id="room">
<img height="22" width="18" alt="wm" src="../../images/washericon.gif" />
		<span class="laundrylinks"  onmouseover = "this.className = 'hoverlaundrylinks'" onmouseout="this.className = 'laundrylinks'">
		<html:link action="/showRoomStatus" paramId="roomId" paramName="room" paramProperty="laundryRoomId"><bean:write name="room" property="name" /></html:link>
	</span><br />
</logic:iterate>
</logic:present>
<logic:notPresent cookie="operatorId">*/%>
<bean:define id="dormId" name="result" property="dormId" />
<logic:iterate name="result" property="dorms" id="dorm">
<span class="dormlinks" onmouseover = "this.className = 'hoverdormlinks'" onmouseout="this.className = 'dormlinks'">
<html:link action="/showRoomStatus" paramId="dormId" paramName="dorm" paramProperty="dormId"><bean:write name="dorm" property="name" /></html:link></span>
<br/>
<c:if test="${dorm.dormId==result.dormId}" >
	<logic:iterate name="result" property="laundryRooms" id="room">
		<img height = "22" width="18" alt="wm" src="../../images/washericon.gif" />
		<span class="laundrylinks"  onmouseover = "this.className = 'hoverlaundrylinks'" onmouseout="this.className = 'laundrylinks'">
		<html:link action="/showRoomStatus" paramId="roomId" paramName="room" paramProperty="laundryRoomId"><bean:write name="room" property="name" /></html:link>
	</span><br />
	</logic:iterate>
	<br />
</c:if>
</logic:iterate>
<%/*</logic:notPresent>*/%>
<br /><br />

