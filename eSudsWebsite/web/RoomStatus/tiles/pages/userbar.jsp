<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="user_button">
<logic:equal name="CUSTOM_FEATURES" property="STUDENT_ACCESS" value="FULL">
<html:link action="/../StudentAccess/home.do">Access Your Account</html:link> |
</logic:equal>
<logic:equal name="CUSTOM_FEATURES" property="STUDENT_ACCESS" value="NOTIFY">
<html:link action="notifyConfig.do?action=start">Register for Auto-Notification</html:link> |
</logic:equal>
<html:link action="/../faq">FAQs</html:link>
</font>