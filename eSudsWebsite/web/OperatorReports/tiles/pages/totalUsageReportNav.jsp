<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="navigation_left">
<html:link action="/totalUsageReport?action=showDetail&type=operator" >All Schools</html:link>
<logic:notEqual name="navigate" property="schoolName" value="" >
 &#187; <html:link action="/totalUsageReport?action=showDetail&type=school"><bean:write name="navigate" property="schoolName" /></html:link>
</logic:notEqual>
<logic:notEqual name="navigate" property="campusName" value="" >
 &#187; <html:link action="totalUsageReport?action=showDetail&type=campus"><bean:write name="navigate" property="campusName" /></html:link>
</logic:notEqual>
<logic:notEqual name="navigate" property="dormName" value="" >
 &#187; <html:link action="totalUsageReport?action=showDetail&type=dorm"><bean:write name="navigate" property="dormName" /></html:link> 
</logic:notEqual>
</font>
<font class="navigation_right">
<html:link action="/totalUsageReport?action=buildSearch">Search Again</html:link> | <html:link action="/home" >Reports Home</html:link> 
</font>


