
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<logic:notPresent scope="session" name="subject">
	<tiles:insert name="login"  flush="true" />
</logic:notPresent>
<logic:present scope="session" name="subject">
	<tiles:insert name="menu" flush="true" />
</logic:present>


