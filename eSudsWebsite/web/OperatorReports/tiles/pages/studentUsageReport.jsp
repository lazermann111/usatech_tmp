<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<font class="title">Student Usage Report for <bean:write name="dates" property="startDate" /> to <bean:write name="dates" property="endDate" /></font>
<table class="report">
<logic:notEmpty name="students">
<logic:iterate name="students" id="acct">
	<tr>
			<th align="center" colspan="4"><font class="subtitle"><bean:write name="acct" property="studentName" />, <bean:write name="acct" property="activeText" /></font></th>
	</tr>
	<tr>
			<th>Date</th>
			<th>Time</th>
			<th>Description</th>
			<th>Total Amount</th>
	</tr>
	<% int i=1; %>
<logic:notEmpty name="acct" property="trans">
<logic:iterate name="acct" property="trans" id="tran">
	<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>	
		<td><bean:write name="tran" property="tranStartTs" format="MM/dd/yyyy" /></td>
		<td><bean:write name="tran" property="tranStartTs" format="hh:mm aa" /></td>
		<td><pre><bean:write name="tran" property="tranLineItemDesc" /></pre></td>
		<c:choose>
			<c:when test="${tran.authServiceType == 2 || tran.authServiceType == 3}">
		<td>**Included**</td></c:when>
			<c:otherwise>				
		<td><bean:write name="tran" property="tranSettlementAmount" format="$0.00" /></td>
			</c:otherwise>
		</c:choose></tr>
	</tr>
	<% i++; %>
</logic:iterate>
</logic:notEmpty>
<logic:empty name="acct" property="trans">
	<TR class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
	<TD colspan="4"> No Transactions Found </TD>
	</TR>
</logic:empty>
	<TR class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
			<TD align="right">Total Transactions:</TD>
			<TD align="left"><bean:size name="acct" property="trans" id="numTrans" /><bean:write name="numTrans" /></TD>
			<TD align="right">Total Amount:</TD>
			<TD align="left"><bean:write name="acct" property="totalAmount" format="$0.00" /></TD>
	</TR>
		<TR>
			<TD></TD>
			<TD></TD>
			<TD></TD>
			<TD></TD>
		</TR>
</logic:iterate>
<tr class ="footer"><td colspan="4" /></tr>
</logic:notEmpty>
<logic:empty name="students">
<TR>
<TD colspan="4"> No Students Found </TD>
</TR>
</logic:empty>
</TABLE>

