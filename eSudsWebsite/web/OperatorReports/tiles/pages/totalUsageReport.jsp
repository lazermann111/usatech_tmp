
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Total usage for <bean:write name="navigate" property="startDate" /> - <bean:write name="navigate" property="endDate" /></font>
<TABLE class="report">
	<tr><th>Location</th><th>Wash Cycles</th><th>Wash Amount</th><th>Dry Cycles</th><th>Dry Amount</th><th>Total Cycles</th><th>Total Amount</th></tr>
	<logic:empty name="results"><tr><td colspan="7">NO RESULTS</td></tr></logic:empty>
	<logic:iterate name="results" id="lineItem" scope="request">
		<tr>
			<logic:notPresent name="lineItem" property="params">
			<td><bean:write name="lineItem" property="name" /></td>
			</logic:notPresent>
			<logic:present name="lineItem" property="params">
			<td><html:link action="/totalUsageReport" name="lineItem" property="params"><bean:write name="lineItem" property="name" /></html:link></td>
			</logic:present>
			<td><bean:write name="lineItem" property="totWashCycles" /></td>
			<td><bean:write name="lineItem" property="totWashAmount" format="$0.00" /></td>
			<td><bean:write name="lineItem" property="totDryCycles" /></td>
			<td><bean:write name="lineItem" property="totDryAmount" format="$0.00" /></td>
			<td><bean:write name="lineItem" property="totCycles" /></td>
			<td><bean:write name="lineItem" property="totAmount" format="$0.00" /></td>
		</tr>
	</logic:iterate>
</TABLE>


