<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<p class="title">
Room Usage Summary Report for <br/>
<bean:write name="campusSummary" property="schoolName" /> - <bean:write name="campusSummary" property="campusName" />
<br/>
for <bean:write name="dates" property="startDate" format="MM/dd/yyyy" /> to <bean:write name="dates" property="endDate" format="MM/dd/yyyy"/>
</p>

<logic:empty name="campusSummary" property="rooms">
<p class="subtitle">No Activity Found.</p>
</logic:empty>
<logic:iterate name="campusSummary" property="rooms" id="room">
<div id="location" style="page-break-inside: avoid">
<p class="title">Location: <bean:write name="room" property="dormName" />, <bean:write name="room" property="roomName" /></p>
<br/>
<font class="subtitle">Machine Detail</font>
<table class="report">
<tr><th>Label</th><th>Type</th><th>Cycles</th><th>Topoffs</th><th>Cash Amount</th><th>Card Amount</th><th>Total Amount</th></tr>
<logic:empty name="room" property="machines">
<tr><td colspan="7">No Activity Found</td></tr>
</logic:empty>
<% int i=1; %>
<logic:iterate name="room" property="machines" id="machine">
	<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
		<td><bean:write name="machine" property="label" /></td>
		<td><bean:write name="machine" property="machineType" /></td>
		<td><bean:write name="machine" property="numCycles" /></td>
		<td><bean:write name="machine" property="numTopOffs" /></td>
		<td><bean:write name="machine" property="cashAmount" format="$0.00" /></td>
		<td><bean:write name="machine" property="cardAmount" format="$0.00" /></td>
		<td><bean:write name="machine" property="totalAmount" format="$0.00" /></td>
	</tr>
	<% i++; %>
</logic:iterate>
<tr class ="footer"><td colspan="7" /></tr>
</table>

<p class="subtitle">Summary</p>

<table class="report">
<tr><th>Description</th><th>Cycles</th><th>Topoffs</th><th>Total Amount</th></tr>
<logic:empty name="room" property="machineTypeTotals">
<tr><td colspan="4">No Activity Found</td></tr>
</logic:empty>
<% int j=1; %>
<logic:iterate name="room" property="machineTypeTotals" id="machineTotal">
	<tr class='<%= (j % 2 == 1) ? "odd" : "even" %>'>
		<td><bean:write name="machineTotal" property="description" /></td>
		<td><bean:write name="machineTotal" property="totalCycles" /></td>
		<td><bean:write name="machineTotal" property="totalTopOffs" /></td>
		<td><bean:write name="machineTotal" property="totalAmount" format="$0.00" /></td>
	</tr>
	<% j++; %>
</logic:iterate>
<tr class ="footer"><td colspan="4" /></tr>
<logic:iterate name="room" property="paymentTypeTotals" id="paymentTotal">
	<tr class='<%= (j % 2 == 1) ? "odd" : "even" %>'>
		<td><bean:write name="paymentTotal" property="description" /></td>
		<td><bean:write name="paymentTotal" property="totalCycles" /></td>
		<td><bean:write name="paymentTotal" property="totalTopOffs" /></td>
		<td><bean:write name="paymentTotal" property="totalAmount" format="$0.00" /></td>
	</tr>
	<% j++; %>
</logic:iterate>
<tr class ="footer"><td colspan="4" /></tr>
<tr>
		<td><bean:write name="room" property="grandTotal.description" /></td>
		<td><bean:write name="room" property="grandTotal.totalCycles" /></td>
		<td><bean:write name="room" property="grandTotal.totalTopOffs" /></td>
		<td><bean:write name="room" property="grandTotal.totalAmount" format="$0.00" /></td>
</tr>
<tr class ="footer"><td colspan="4" /></tr>
</table>
<br />
<br />
<br />
</div>
</logic:iterate>

