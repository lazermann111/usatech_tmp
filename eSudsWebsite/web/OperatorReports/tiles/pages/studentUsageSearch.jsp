<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script language="JavaScript" src="<html:rewrite page='/scripts/calendar.js' />" >
</script>
<script language="JavaScript" src="<html:rewrite page='/scripts/calendar_helper.js' />">
</script>
<script language="JavaScript">
var calendar = "<html:rewrite page='/tiles/pages/calendar.html' />";
</script>

<font class="title">Student Usage Report</font>
<BR>
<html:form action="/studentUsageReport">
<html:hidden property="action" value="runReport" />
	<TABLE border="0" cellspacing="10" width="100%">
		<TR valign="top">
			<td><font class="subtitle">Campus (Required)</font></td>
			<td><font class="subtitle">Date Range (Required)</font></td>
		</TR>
		<TR valign="top">
			<td>
			<logic:empty name="campuses">
			No campuses found.
			</logic:empty>
			<logic:notEmpty name="campuses">
				<logic:iterate name="campuses" id="campus">
				<html:radio property="campusId" idName="campus" value="campusId" />
				<bean:write name="campus" property="name" />
				<br />
				</logic:iterate>
			</logic:notEmpty>
			</td>
			<td>
<html:radio property="dateChoice" value="today" /> Today
<BR>
<html:radio property="dateChoice" value="yesterday" /> Yesterday
<BR>
<html:radio property="dateChoice" value="since" /> Since <html:text size="10" property="sinceDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('sinceDate')"><img src="../../images/calendar.gif"  border="0"/></html:link>
<BR>
<html:radio property="dateChoice" value="on" /> On <html:text size="10" property="onDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('onDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
<BR>
<html:radio property="dateChoice" value="fromto" /> From <html:text size="10" property="fromDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('fromDate')"><img src="../../images/calendar.gif"  border="0"/></html:link>
 to <html:text size="10" property="toDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('toDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
</td>
		</TR>
		<TR>
			<td></td>
			<td></td>
		</TR>
		<TR>
			<td colspan="2" align="center"><html:submit styleClass="button2" value="Search">Search</html:submit></td>
		</TR>
	</TABLE>

</html:form>