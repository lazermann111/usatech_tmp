<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<font class="title"><bean:write name="report" property="title" /></font>

<table class="report">
<tr>
<jsp:useBean id="report" class="com.usatech.server.report.Report" scope="request" />
<% for (int c = 0; c < report.getColumnCount(); c++) { %>
<th><%=report.getColumnDisplayName(c)%></th>
<%}%>
<% if (report.getRowCount() == 0) { %>
<tr><td colspan=<%=report.getColumnCount()%>>No Results Found</td></tr>
<% } %>
<% for (int r = 0; r < report.getRowCount(); r++) { %>
<tr class='<%= (r % 2 == 0) ? "odd" : "even" %>'>
	<% for (int c = 0; c < report.getColumnCount(); c++) { %>
	<td><%=report.getValueAt(r,c)%></td>
	<%}%>
</tr>
<%}%>
</table>

