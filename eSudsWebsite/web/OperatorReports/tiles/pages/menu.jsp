<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="action" value="buildSearch" scope="page" />

<P class="title">Available Reports</P>
<P class="subtitle"><html:link action="/totalUsageReport" paramId="action" paramName="action" paramScope="page">Total Usage By Location Report</html:link></P>
<p class="subtitle"><html:link action="/roomUsageReport" paramId="action" paramName="action" paramScope="page">Room Usage Summary Report</html:link><p>
<P class="subtitle"><html:link action="/studentUsageReport" paramId="action" paramName="action" paramScope="page">Student Usage Report</html:link></P>
<p class="subtitle"><html:link action="/cashUsageReport" paramId="action" paramName="action" paramScope="page">Cash Usage Report</html:link><p>


