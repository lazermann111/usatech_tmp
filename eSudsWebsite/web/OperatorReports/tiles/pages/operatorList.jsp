<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Operators</font>
<br/>
<br/>
<logic:iterate name="operators" id="operator">
<html:link action="/operator?action=show" paramName="operator" paramProperty="operatorId" paramId="operatorId">
<bean:write name="operator" property="name" />
</html:link>
<br/>
</logic:iterate>
