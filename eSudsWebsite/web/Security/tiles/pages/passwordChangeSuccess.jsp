<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Password Changed Successfully.</font>

<P>
<html:link action="/eSudsHome">eSuds Home</html:link>
<P>
<logic:present cookie="home">
<logic:equal cookie="home" value="StudentAccess">
<html:link action="/switch?page=/home.do&prefix=/StudentAccess">Student Access</html:link>
</logic:equal>
<logic:equal cookie="home" value="OperatorAccess">
<html:link action="/switch?page=/home.do&prefix=/OperatorAccess">Operator Access</html:link>
</logic:equal>
<logic:equal cookie="home" value="OperatorReports">
<html:link action="/switch?page=/home.do&prefix=/OperatorReports">Operator Reports</html:link>
</logic:equal>
</logic:present>