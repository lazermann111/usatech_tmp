<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type="text/javascript">

function addAutoComplete(){
	var allForms = document.getElementsByTagName("form");
	for(var i=0;i<allForms.length;i++){
	   allForms[i].setAttribute( "autocomplete", "off" );
	}
}
window.onload=addAutoComplete;
</script>
<font class="title">Login</font>
<html:form action="/loginUser?action=login" focus="user">
<html:hidden property="application" />
<table cellspacing="5">
<tr><td align="right">
<font class="subtitle">Username: </font></td>
<td>
<html:text property="user" styleClass="input_style" size="15" tabindex="1"/>
</td></tr>
<tr><td align="left">
<font class="subtitle">Password: </font></td>
<td>
<html:password property="password" styleClass="input_style" size="15" tabindex="2"/>
</td></tr>
<tr><td colspan="2" align="center">
<html:submit styleClass="button2" value="Login" tabindex="3"/>
</td></tr>
</table>
</html:form>
<html:link action="/resetPassword?action=showForm" tabindex="4" >Lost Password?</html:link>
