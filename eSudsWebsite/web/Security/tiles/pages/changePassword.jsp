<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<script type="text/javascript">

function addAutoComplete(){
	var allForms = document.getElementsByTagName("form");
	for(var i=0;i<allForms.length;i++){
	   allForms[i].setAttribute( "autocomplete", "off" );
	}
}
window.onload=addAutoComplete;
</script>

<font class="title">Change Password</font>

<html:form action="/changePassword" >
<html:hidden property="action" />
<font size="-1">
	Password should:<ul>
	<li>use alphanumeric characters including <br>at least 1 number</li>
	<li>be between 6 and 10 characters</li></ul></font>
<table>
<tr>
<th align="right">Current Password</th><td align="left"><html:password property="currPassword" styleClass="input_style" /></td>
</tr>
<tr>
<th align="right">New Password</th><td align="left"><html:password property="newPassword" styleClass="input_style" /></td>
</tr>
<tr>
<th align="right">Confirm New Password</th><td align="left"><html:password property="confirmPassword" styleClass="input_style" /></td>
</tr>
<tr>
<td colspan="2" align="center"><html:submit value="Change Password" styleClass="button2"/></td>
</tr>
</table>
</html:form>

