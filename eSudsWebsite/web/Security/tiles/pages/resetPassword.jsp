<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type="text/javascript">

function addAutoComplete(){
	var allForms = document.getElementsByTagName("form");
	for(var i=0;i<allForms.length;i++){
	   allForms[i].setAttribute( "autocomplete", "off" );
	}
}
window.onload=addAutoComplete;
</script>
<font class="title">Reset Password</font>
<html:form action="/resetPassword" >
<html:hidden property="action" />
<table>
<tr>
<th align="right">Username: </th><td align="left"><html:text property="username" styleClass="input_style" /></td>
</tr>
<tr>
<td colspan="2" align="center"><html:submit value="Reset Password" styleClass="button2"/></td>
</tr>
</table>
</html:form>

