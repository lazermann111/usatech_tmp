<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<font class="title">Reset Password Successful</font>
<p>Your password has been reset.  An e-mail has been sent with the temporary password.</p>