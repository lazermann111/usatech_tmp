

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<font class="title">Login Failed</font>
<P>
<html:errors />
</P>
<P>
<font class="subtitle">Please try again.</font>
</P>
<html:link action="/showloginForm">Return to Login Screen</html:link>


