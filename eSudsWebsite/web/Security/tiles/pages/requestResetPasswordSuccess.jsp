<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<font class="title">Password Reset Requested</font>
<p>An e-mail has been sent to your primary account with instructions on how to proceed.</p>