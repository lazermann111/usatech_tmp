function confirmChoice(action, name, form)
{
	var confirmed = window.confirm("Are you sure you want to " + action + " " + name + "?");
	
	if (confirmed)
	{
		form.action.value = action;
		form.submit();
	}
}