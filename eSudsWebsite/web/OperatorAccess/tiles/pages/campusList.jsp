
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Campuses</font>
<logic:notEmpty name="campuses">
<table>
<logic:iterate name="campuses" id="campus">
	<tr><td>
	<html:link action="/campus" name="campus" property="actionMap"><bean:write name="campus" property="name" /></html:link>
	</td></tr>
</logic:iterate>
<tr><td></td></tr>
<tr><td><html:form action="/campus"><html:hidden property="action" value="create" />
			<html:submit value="Add New" styleClass="button2"/></html:form></td></tr>
</table>
</logic:notEmpty>
<logic:empty name="campuses">
<table>
<tr><td>
None found.
</td></tr>
<tr><td><html:form action="/campus">
			<html:hidden property="action" value="create" />
			<html:submit value="Add New" styleClass="button2"/></html:form></td></tr>
</table>
</logic:empty>
