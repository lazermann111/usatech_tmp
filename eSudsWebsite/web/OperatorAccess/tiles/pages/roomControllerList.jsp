

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Room Controllers</font>
<logic:notEmpty name="roomControllers">
<table>
<logic:iterate name="roomControllers" id="roomController">
	<tr><td>
	<html:link action="/roomController" name="roomController" property="actionMap"><bean:write name="roomController" property="serialNumber" /></html:link>
	</td></tr>
</logic:iterate>
<tr><td></td></tr>
</table>
</logic:notEmpty>
<logic:empty name="roomControllers">
<table>
<tr><td>
None found.
</td></tr>
</table>
</logic:empty>
