
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="unassignedAction" value="retrieveList" scope="page"/>
<bean:define id="assignedAction" value="retrieveList" scope="page"/>

<font class="title">Room Controllers</font>

<p>
<html:link action="/roomController" paramId="action" paramName="unassignedAction" paramScope="page">
Assign Free Room Controller
</html:link>
</p>
<!---
<p>
<html:link action="/showAssignedRoomControllers" paramName="operatorId" paramId="operatorId" paramScope="session">
View Assigned Room Controller
</html:link>
</p>
-->