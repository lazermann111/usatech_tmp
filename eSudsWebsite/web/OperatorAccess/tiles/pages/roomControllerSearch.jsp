

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<script language="javascript">

 function submitForm (c)  {
 	document.forms[1].changed.value = c;
	document.forms[1].submit();
 }
</script>

<font class="title">Search for Room Controller</font>
<p><html:errors/></p>

<html:form action="/searchRoomControllers">
<html:hidden property="changed" />
<html:hidden property="operatorId" />
<center>
<table cellpadding="5" style="position: absolute">
<tr><td>Campus</td><td>Residence Hall</td><td>Laundry Room</td><td>Room Controllers</td></tr>
<tr>
<td> 
<html:select property="campus" size="12"  onclick="submitForm('campus')" styleClass="pulldown"><html:optionsCollection name="campuses" value="id" label="name" /></html:select>
</td><td>
<html:select property="dorm" size="12" onclick="submitForm('dorm')" styleClass="pulldown"><html:optionsCollection name="dorms" value="id" label="name" /></html:select>
</td><td>
<html:select property="laundryRoom" size="12" onclick="submitForm('laundryRoom')" styleClass="pulldown"><html:optionsCollection name="laundryRooms" value="id" label="name" /></html:select>
</td><td>
<html:select property="roomController" size="12" styleClass="pulldown"><html:optionsCollection name="roomControllers" value="id" label="name" /></html:select>
</td></tr>

</table>
<html:submit styleClass="button2" value="GO" />
</center>
</html:form>

