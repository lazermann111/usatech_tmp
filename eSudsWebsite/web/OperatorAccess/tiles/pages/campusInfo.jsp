<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script language="javascript" src="<html:rewrite page='/scripts/form_helper.js' />"></script>
<font class="title">Campus Information</font>
<table border="0" cellpadding="16" cellspacing="1">
 <tr><td>
<html:form action="/campus">
<html:hidden property="action" value="save" />
<html:hidden property="campusId" />
	<table border="0" cellpadding="2">
		<tr>
			<td align="right">Name*</td>
			<td align="left"><html:text property="name" styleClass="input_style" size="60"/></td>
		</tr>	
		<tr>
			<td align="right">Address 1</td>
			<td align="left"><html:text property="addr1" styleClass="input_style" size="60"/></td>
		</tr>
		<tr>
			<td align="right">Address 2</td>
			<td align="left"><html:text property="addr2" styleClass="input_style"  size="60"/></td>
		</tr>
		<tr>
			<td align ="right">City</td>
			<td align="left"><html:text property="city" styleClass="input_style"  size="28"/></td>
		</tr>
		<tr>
			<td align ="right">State</td>
			<td align="left"><html:select property="stateCd" styleClass="pulldown"><html:optionsCollection property="states" label="stateName" value="stateCd" /></html:select></td>
		</tr>	
		<tr>
			<td align="right">Country</td>
			<td align="left"><html:select property="countryCd" styleClass="pulldown"><html:optionsCollection property="countries"  label="countryName" value="countryCd"/></html:select></td>
		</tr>	
		<tr>
			<td align ="right">Postal Code</td>
			<td align="left"><html:text property="postalCd" styleClass="input_style" size="10"/></td>
		</tr>	
		<tr>
			<td align ="right">Time Zone</td>
			<td align="left"><html:select property="timeZoneCd" styleClass="pulldown"><html:optionsCollection property="timeZones"  label="timeZoneName" value="timeZoneCd"/></html:select></td>
		</tr>		
		<tr>
			<td align ="right">School</td>
			<td align="left" valign="middle"><html:select property="schoolId" styleClass="pulldown"><html:optionsCollection name="schools"  label="name" value="schoolId"/></html:select>
			</td>
		</tr>
		<tr><td>*<font size="-1">=required</font></td><td></td></tr>
		<tr>
			<td colspan="2" align="center">
				<html:button property="Save" value="Save" styleClass="button2" onclick="confirmChoice('save', form.name.value, form)"/>
			    <logic:equal name="campusForm" property="campusId" value="-1">
			    <html:button property="Delete" value="Delete" styleClass="button2" disabled="true" />
			    </logic:equal>
			    <logic:notEqual name="campusForm" property="campusId" value="-1">
			    <logic:notEmpty name="dorms">
			    <html:button property="Delete" value="Delete" styleClass="button2" disabled="true" />
			    </logic:notEmpty>
			    <logic:empty name="dorms">
			    <html:button property="Delete" value="Delete" styleClass="button2" onclick="confirmChoice('delete', form.name.value, form)"/>
			    </logic:empty>
			    </logic:notEqual>
			    <html:reset styleClass="button2" />
			</td>
		</tr>
		<tr>
			<td colspan="2" class="delay-note">Please allow up to 10 minutes for changes to be reflected on Room Status pages.</td>
		</tr>		
	</table>
	</html:form>
</td>
	<td align="right" valign="top" >
		<table border="0" cellpadding="2">
			<tr><th><font style=".title">Residence Halls</font></th></tr>
			<logic:iterate name="dorms" id="dorm">
			<tr><td align="left">
			<html:link action="/dorm" name="dorm" property="actionMap"><bean:write name="dorm" property="name" /></html:link>
			</td></tr>
			</logic:iterate>
			<tr><td></td></tr>
			<tr><td>
			<logic:equal name="campusForm" property="campusId" value="-1">
			<html:form action="/dorm"><html:hidden name="campusForm" property="campusId" /><html:hidden property="action" value="create" />
			<html:submit value="Add New" styleClass="button2" disabled="true" /></html:form>
			</logic:equal>
			<logic:notEqual name="campusForm" property="campusId" value="-1">
			<html:form action="/dorm"><html:hidden name="campusForm" property="campusId" /><html:hidden property="action" value="create" />
			<html:submit value="Add New" styleClass="button2"/></html:form>
			</logic:notEqual>
			</td></tr>
		</table>
	</td></tr>
</table>




