<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<bean:define id="operatorAction" value="show" scope="page" />
<bean:define id="campusAction" value="retrieveList" scope="page"/>

<p><html:link action="/operator" paramId="action" paramName="operatorAction" paramScope="page">Contact Information</html:link></p>
<p><html:link action="/campus" paramId="action" paramName="campusAction" paramScope="page">Locations</html:link></p>
<p><html:link action="/selectRoomControllers">Room Controllers</html:link></p>

