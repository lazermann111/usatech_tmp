<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Room Controllers</font>
<html:form action="/roomController">
<html:hidden property="action" />
<table cellpadding="2" style="position: absolute">
<tr><td>S/N</td>
	<td><bean:write name="roomControllerForm" property="serialNumber" /></td>
</tr>
<tr><td>Campus</td>
	<td>
	<html:select property="campusId" size="1"  onchange="form.action.value = 'show';form.submit();" styleClass="pulldown">
	<logic:notEmpty name="roomControllerForm" property="campuses">
	<logic:equal name="roomControllerForm" property="campusId" value="-1">
	<html:option value="1">Unknown Campus</html:option>
	</logic:equal>
	<html:optionsCollection name="roomControllerForm" property="campuses" value="campusId" label="name" />
	</logic:notEmpty>
	</html:select>
	</td>
</tr>
<tr><td>Residence Hall</td>
	<td>
	<html:select property="dormId" size="1" onchange="form.action.value = 'show';form.submit();" styleClass="pulldown">
	<logic:notEmpty name="roomControllerForm" property="dorms">
	<logic:equal name="roomControllerForm" property="dormId" value="-1">
	<html:option value="1">Unknown Dorm</html:option>
	</logic:equal>
	<html:optionsCollection name="roomControllerForm" property="dorms" value="dormId" label="name" />
	</logic:notEmpty>
	</html:select>
	</td>
</tr>
<tr><td>Laundry Room</td>
	<td>
	<html:select property="laundryRoomId" size="1" styleClass="pulldown">
	<logic:notEmpty name="roomControllerForm" property="laundryRooms">
	<logic:lessEqual name="roomControllerForm" property="laundryRoomId" value="1">
	<html:option value="1">Unknown Laundry Room</html:option>
	</logic:lessEqual>
	<html:optionsCollection name="roomControllerForm" property="laundryRooms" value="laundryRoomId" label="name" />
	</logic:notEmpty>
	</html:select>
	</td>
</tr>
<tr>
<td colspan="2">
<html:hidden property="serialNumber" />
<html:hidden property="posId" />
<html:hidden property="locationAssigned" />
<html:submit value="Save" styleClass="button2" onclick="form.action.value = 'save';"/> 
<html:reset value="Reset" styleClass="button2" />
</td>
</tr>
</table>
</html:form>



