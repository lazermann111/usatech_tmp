
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Assigned Room Controllers</font>

<table class="report">
<tr>
<th>S/N</th><th>Campus</th><th>Residence Hall</th><th>Laundry Room</th>
</tr>
<% int i=1; %>
<logic:iterate name="roomControllers" id="roomController">
<tr class='<%= (i % 2 == 1) ? "odd" : "even" %>'>
<td><bean:write name="roomController" property="serialNumber" /></td>
<td><bean:write name="roomController" property="campusName" /></td>
<td><bean:write name="roomController" property="dormName" /></td>
<td><bean:write name="roomController" property="laundryRoomName" /></td>
</tr>
<% i++; %>
</logic:iterate>
</table>

