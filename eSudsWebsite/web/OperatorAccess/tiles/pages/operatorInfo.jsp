
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<font class="title">Operator Information</font>
<html:errors />
<html:form action="/operator">
<html:hidden property="operatorId" />
<html:hidden property="action" value="save" />
	<table border="0"  cellpadding="1" style="position: absolute">
	<tr>
			<td align ="right">Name*</td>
			<td align="left"><html:text property="name" styleClass="input_style"/></td>
			</tr>
			
	<tr>
			<td align ="right">Address 1</td>
			<td align="left"><html:text property="addr1" styleClass="input_style"/></td>
	</tr>

			<tr>
			<td align ="right">Address 2</td>
			<td align="left"><html:text property="addr2" styleClass="input_style" /></td>
		</tr>

		<tr>
			<td align ="right">City</td>
			<td align="left"><html:text property="city" styleClass="input_style" /></td>
		</tr>

		<tr>
			<td align ="right">County</td>
			<td align="left"><html:text property="county" styleClass="input_style" /></td>
		</tr>

		<tr>
			<td align ="right">State</td>
			<td align="left"><html:select property="stateCd" styleClass="pulldown"><html:optionsCollection property="states" label="stateName" value="stateCd" /></html:select></td>
		</tr>
		
		<tr>
			<td align ="right">Country</td>
			<td align="left"><html:select property="countryCd" styleClass="pulldown"><html:optionsCollection property="countries"  label="countryName" value="countryCd"/></html:select></td>
		</tr>
			<tr>
			<td align ="right">Postal Code</td>
			<td align="left"><html:text property="postalCd" styleClass="input_style" /></td>
		</tr>
		<tr><td>*=required</td><td></td></tr>
		<tr>
		<td align="right"><html:submit property="submit" value="Save" styleClass="button2" /></td>
		<td align="left"><html:reset styleClass="button2" /></td>
		</tr>
	</table>
</html:form>
