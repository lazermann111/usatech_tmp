<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<logic:present name="dormId">
<html:link action="/dorm?action=show" paramId="dormId" paramName="dormId">
Return to Residence Hall
</html:link>
</logic:present>

<logic:present name="campusId">
<html:link action="/campus?action=show" paramId="campusId" paramName="campusId">
Return to Campus
</html:link>
</logic:present>

<logic:notPresent name="campusId">
<logic:notPresent name="dormId">
<html:link action="/campus?action=retrieveList">
Return to Campus List
</html:link>
</logic:notPresent>
</logic:notPresent>

