<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script language="javascript" src="<html:rewrite page='/scripts/form_helper.js' />"></script>
<font class="title">Laundry Room Information</font>
<html:form action="/laundryRoom">
<html:hidden property="action" value="save" />
<html:hidden property="laundryRoomId" />
<html:hidden property="dormId" />
	<table border="0"  cellpadding="1" border="0"  cellpadding="2">
		<tr>
			<td align ="right">Name*</td>
			<td align="left"><html:text property="name" styleClass="input_style" size="60"/></td>
		</tr>
		<tr><td>*<font size="-1">=required</font></td><td></td></tr>
		<tr>
			<td align="right">
		<tr>
		<td align="center" colspan="2">
		    <html:button property="Save" value="Save" styleClass="button2" onclick="confirmChoice('save', form.name.value, form)"/>
		    <logic:equal name="laundryRoomForm" property="laundryRoomId" value="-1">
		    <html:button property="Delete" value="Delete" styleClass="button2" disabled="true" />
		    </logic:equal>
		    <logic:notEqual name="laundryRoomForm" property="laundryRoomId" value="-1">
		    <html:button property="Delete" value="Delete" styleClass="button2" onclick="confirmChoice('delete', form.name.value, form)"/>
		    </logic:notEqual>
		<html:reset styleClass="button2" /></td>
		</tr>
		<tr>
			<td colspan="2" class="delay-note">Please allow up to 10 minutes for changes to be reflected on Room Status pages.</td>
		</tr>		
	</table>
</html:form>
