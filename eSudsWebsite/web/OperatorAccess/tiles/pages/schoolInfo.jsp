

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">School Information</font>
<html:form action="/saveSchool">
<html:hidden property="schoolId" />
	<table border="0"  cellpadding="1" border="0"  cellpadding="2">
		<tr>
			<td align ="right">Name</td>
			<td align="left"><html:text property="name" styleClass="input_style" size="60"/></td>
		</tr>
		<tr>
			<td align="right">
		<tr>
		<td align="right"><html:submit property="submit" value="Save" styleClass="button2" /></td>
		<td align="left"><html:reset styleClass="button2" /></td>
		</tr>
		<tr>
			<td colspan="2" class="delay-note">Please allow up to 10 minutes for changes to be reflected on Room Status pages.</td>
		</tr>		
	</table>
</html:form>
