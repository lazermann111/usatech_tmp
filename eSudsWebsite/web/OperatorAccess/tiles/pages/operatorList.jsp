<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Operators</font>
<table>
<logic:iterate name="operators" id="operator">
	<tr><td>
	<html:link action="/operator?action=show" paramId="operatorId" paramName="operator" paramProperty="id"><bean:write name="operator" property="name" /></html:link>
	</td></tr>
</logic:iterate>
<logic:present name="Add">
<tr><td>
<html:form action="/operator"><html:hidden property="action" value="create" />
<html:submit value="Add New" styleClass="button2"/></html:form>
</td></tr>
</logic:present>
</table>
