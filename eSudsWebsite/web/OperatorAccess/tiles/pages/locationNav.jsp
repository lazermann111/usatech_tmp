<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="navigation_left">
<logic:present name="campusBean">
<html:link action="/campus?action=retrieveList">
All Campuses
</html:link>
</logic:present>

<logic:present name="dormBean">
<html:link action="/campus?action=show" paramId="campusId" paramName="dormBean" paramProperty="campusBean.campusId">
<bean:write name="dormBean" property="campusBean.name" />
</html:link>
&#187; 
<html:link action="/dorm?action=show" paramId="dormId" paramName="dormBean" paramProperty="dormId">
<bean:write name="dormBean" property="name" />
</html:link>
</logic:present>

<logic:present name="laundryRoomBean">
<html:link action="/campus?action=show" paramId="campusId" paramName="laundryRoomBean" paramProperty="dormBean.campusBean.campusId">
<bean:write name="laundryRoomBean" property="dormBean.campusBean.name" />
</html:link>
&#187; 
<html:link action="/dorm?action=show" paramId="dormId" paramName="laundryRoomBean" paramProperty="dormBean.dormId">
<bean:write name="laundryRoomBean" property="dormBean.name" />
</html:link>
&#187; 
<html:link action="/laundryRoom?action=show" paramId="laundryRoomId" paramName="laundryRoomBean" paramProperty="laundryRoomId">
<bean:write name="laundryRoomBean" property="name" />
</html:link>
</logic:present>
</font>
