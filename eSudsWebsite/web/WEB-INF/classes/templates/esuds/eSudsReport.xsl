<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect fr">
	<xsl:import href="../../simple/falcon/templates/report/show-report-base.xsl"/>
	<xsl:output method="xhtml" omit-dtd="true"/>

	<xsl:param name="context" />

	<xsl:template match="/fr:report">
		<xsl:call-template name="report"/>
	</xsl:template>

	<xsl:template name="output-type-menu">
		<xsl:variable name="currentType" select="@fr:output-type" />
		<xsl:if test="$currentType = 22">
			<div class="output-type-menu">
				<form name="changeOutput" action="/eSudsReport.i">
					<xsl:call-template name="parameters-form">
						<xsl:with-param name="overrides">
							<parameter name="outputType" value="" />
							<parameter name="resultsType" value="cache" />
							<xsl:if test="@fr:chart-type"><parameter name="chartType" value="{@fr:chart-type}" /></xsl:if>
							<parameter name="doChartConfigUpdate" value="false" />
						</xsl:with-param>
					</xsl:call-template>
					<xsl:if test="$currentType != 22">
						<input type="image" class="clearBorder" alt="View as HTML" title="View as HTML" value="22" src="./images/icons/html.gif" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$currentType = 28">
							<input type="image" class="clearBorder" alt="View Chart in PDF" title="View Chart in PDF" value="31" src="./images/icons/pdf.gif" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
						</xsl:when>
						<xsl:otherwise>
							<input type="image" class="clearBorder" alt="View as PDF" title="View as PDF" value="24" src="./images/icons/pdf.gif" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
						</xsl:otherwise>
					</xsl:choose>
					<input type="image" class="clearBorder" alt="View as Excel" title="View as Excel" value="27" src="./images/icons/excel.jpg" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
					<xsl:variable name="allow-chart-config" select="fr:directives/fr:directive[@fr:name = 'allow-chart-config']/@fr:value" />
					<xsl:if test="$allow-chart-config and $allow-chart-config = 'true'">
						<input type="image" class="clearBorder" value="28" alt="Create Chart from Data" title="Create Chart from Data" src="./images/icons/chart.jpg" onclick="submitChangeOutput(this);" onmouseover="this.className='hoverBorder';" onmouseout="this.className='clearBorder';" />
					</xsl:if>
					<script type="text/javascript">
	function submitChangeOutput(node){
		node.form.outputType.value = node.value;
		if(node.value == "28"){
			node.form.action = "configure_chart.i";
		} <xsl:if test="$currentType = 28">else if(node.value == "31") {
			node.form.doChartConfigUpdate.value = "true";
			node.form.action = "<xsl:value-of select="$run-report-action"/>";
		} </xsl:if>else {
			node.form.action = "<xsl:value-of select="$run-report-action"/>";
		}
		node.form.submit();
	}
					</script>
				</form>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>