<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect b p">
	<xsl:import href="resource:/templates/esuds/menuBase.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="title">eSuds.net :: Login</xsl:template>

	<xsl:template name="body">
		<font class="title">Login</font>
		<form name="info" method="POST" action="logon.i" autocomplete="off">
			<xsl:call-template name="parameters-form">
				<xsl:with-param name="overrides">
					<parameter name="username"/>
					<parameter name="password"/>
					<parameter name="simple.servlet.steps.LogonStep.forward"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:variable name="forward" select="*/b:parameters/b:simple.servlet.steps.LogonStep.forward"/>
			<xsl:choose>
				<xsl:when test="$forward='/logout' or $forward='/logon' or $forward='/login' or $forward='' or not($forward)">
					<xsl:comment>Forward is blank or a login/logout value; use search path</xsl:comment>
					<xsl:variable name="forward" select="reflect:getProperty($context, 'simple.servlet.SimpleServlet.SearchPath')"/>
		 			<xsl:choose>
		 				<xsl:when test="$forward='/logout' or $forward='/logon' or $forward='/login' or $forward='' or not($forward)">
							<xsl:comment>Forward is blank or a login/logout value; use default</xsl:comment>
							<xsl:call-template name="forward">
								<xsl:with-param name="forward" select="'/home'"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="forward">
								<xsl:with-param name="forward" select="$forward"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
		 		</xsl:when>
		 		<xsl:otherwise>
	 				<xsl:call-template name="forward">
						<xsl:with-param name="forward" select="$forward"/>
					</xsl:call-template>
		 		</xsl:otherwise>
			</xsl:choose>
			<table cellspacing="5">
				<tr><td align="right">
				<font class="subtitle">Username: </font></td>
				<td>
				<input id="username" type="text" name="username" class="input_style" size="15" tabindex="1" value="{reflect:getProperty($context, 'username')}"/>
				</td></tr>
				<tr><td align="left">
				<font class="subtitle">Password: </font></td>
				<td>
				<input type="password" name="password" class="input_style" size="15" tabindex="2" autocomplete="off"/>
				</td></tr>
				<tr><td colspan="2" align="center">
				<input type="submit" class="button2" value="Login" tabindex="3"/>
				</td></tr>
			</table>
		</form>
		<a action="/resetPassword?action=showForm" tabindex="4">Lost Password?</a>
		<script type="text/javascript" defer="defer">
			$("username").focus();
		</script>
	</xsl:template>

	<xsl:template name="forward">
		<xsl:param name="forward"/>
  		<xsl:comment>Forward='<xsl:copy-of select="$forward"/>'</xsl:comment>
		<input type="hidden" name="simple.servlet.steps.LogonStep.forward" value="{$forward}"/>
	</xsl:template>
</xsl:stylesheet>
