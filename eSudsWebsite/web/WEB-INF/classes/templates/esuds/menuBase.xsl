<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:struts="http://simple/xml/extensions/java/com.usatech.esuds.utils.StrutsHelper"
	xmlns:cmn="http://exslt.org/common"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect struts cmn a p">
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template match="/">
		<html xml:lang="en" lang="en">
			<head>
				<title><xsl:call-template name="title"/></title>
				<base href="{reflect:getProperty($context, 'request.requestURL')}" />
				<link href="../css/esuds3.css" rel="stylesheet" type="text/css"/>
				<script type="text/javascript">
			 		<xsl:attribute name="src">../scripts/mootools.v1.11<xsl:if test="boolean(reflect:getProperty($context, 'debug'))">-debug</xsl:if>.js</xsl:attribute>
			 	</script>
				<xsl:call-template name="head"/>
			</head>
			<body>
				<xsl:call-template name="contents"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="title">eSuds.net</xsl:template>

	<xsl:template name="head">
		<link href="../css/esuds3.css" rel="stylesheet" type="text/css"/>
		<meta http-equiv="Pragma" content="no-cache"/>
	 	<meta http-equiv="Expires" content="-1"/>
	</xsl:template>

	<xsl:template name="contents">
		<xsl:attribute name="class">menuLayout</xsl:attribute>
		<div id="userbar"><xsl:call-template name="userbar"/></div>
		<div id="header"><xsl:call-template name="header"/></div>
		<div id="navigation"><xsl:call-template name="navigation"/></div>
		<div id="center">
			<div id="menu"><xsl:call-template name="menu"/></div>
			<div id="content">
				<div id="messages"><xsl:call-template name="messages"/></div>
				<xsl:call-template name="body"/>
			</div>
			<div id="footer"><xsl:call-template name="footer"/></div>
		</div>
	</xsl:template>

	<xsl:template name="userbar">
		<xsl:variable name="subject" select="reflect:getProperty($context, 'subject')"/>
		<xsl:choose>
			 <xsl:when test="$subject">
			 	<font class="user_button">
					<xsl:variable name="home" select="reflect:getProperty($context, 'cookie:home.value')"/>
					<xsl:choose>
						 <xsl:when test="$home='StudentAccess'">
						 	<xsl:variable name="roomStatusParams" select="translate(reflect:getProperty($context, 'roomStatusParams'),',{}','&amp;')"/>
							<a href="switch.do?page=/showRoomStatus.i&amp;prefix=/RoomStatus&amp;{$roomStatusParams}">View Room Status</a>
							|
							<a href="switch.do?page=/home.do&amp;prefix=/StudentAccess">Student Access</a>
							|
						</xsl:when>
						<xsl:when test="$home='OperatorAccess'">
							<a href="switch.do?page=/home.do&amp;prefix=/OperatorAccess">Operator Access</a>
							|
						</xsl:when>
						<xsl:when test="$home='OperatorReports'">
							<a href="switch.do?page=/home.do&amp;prefix=/OperatorReports">Operator Reports</a>
							|
						</xsl:when>
					</xsl:choose>

					<a href="logout.do">Logout</a>
					|
					<a href="changePassword.do">Change Password</a>
				</font>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="header">
		<a href="eSudsHome.do"><img class="logo" src="../images/esuds_logo.jpg" alt="esuds logo" height="100" width="560" border="0" /></a>
	</xsl:template>

	<xsl:template name="navigation"/>

	<xsl:template name="menu">
		<p>
		</p>
	</xsl:template>

	<xsl:template name="messages">
		<xsl:variable name="messages" select="cmn:node-set(struts:getMessages($context, 'org.apache.struts.action.ACTION_MESSAGE'))"/>
		<xsl:if test="count($messages[string-length(normalize-space()) &gt; 0]) &gt; 0">
			<font class="success">
				<xsl:for-each select="$messages[string-length(normalize-space()) &gt; 0]">
					<span id="success"><xsl:value-of select="."/></span><br/>
				</xsl:for-each>
				<br/>
			</font>
		</xsl:if>
		<xsl:variable name="errors" select="cmn:node-set(struts:getMessages($context, 'org.apache.struts.action.ERROR'))"/>
		<xsl:if test="count($errors[string-length(normalize-space()) &gt; 0]) &gt; 0">
			<font class="error">
				<b>Please correct the following errors or contact the System Administrator:</b>
				<ul>
				<xsl:for-each select="$errors[string-length(normalize-space()) &gt; 0]">
					<li><xsl:value-of select="."/></li>
				</xsl:for-each>
				</ul>
			</font>
		</xsl:if>
	</xsl:template>

	<xsl:template name="body"/>

	<xsl:template name="footer">
		<br />
		<hr color="#bccaf0" width="96%" />
		<p align ="center"><font size="-2">&#169; 2007 by USA Technologies</font></p>
	</xsl:template>

	<xsl:template name="parameters-form">
		<xsl:param name="overrides"/>
		<xsl:param name="parameters" select="*/a:parameters/p:parameters/p:parameter"/>
		<xsl:variable name="override-set" select="cmn:node-set($overrides)/*"/>
		<xsl:for-each select="$override-set[@value]">
			<input type="hidden" name="{@name}" value="{@value}"/>
		</xsl:for-each>
		<xsl:for-each select="$parameters[string-length(@p:value) > 0]">
			<xsl:variable name="name" select="@p:name"/>
			<xsl:if test="not($override-set[@name = $name])">
				<input type="hidden" name="{@p:name}" value="{@p:value}"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>