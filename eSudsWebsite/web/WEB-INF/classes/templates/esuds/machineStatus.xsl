<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:str="http://exslt.org/strings"
	xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r p str">
	<xsl:output method="xhtml" omit-dtd="true"/>

	<xsl:param name="context" />

	<xsl:template match="/">
		<xsl:variable name="selectedMachines" select="str:split(normalize-space(/a:base/a:parameters/p:parameters/p:parameter[@p:name='selectedMachines']/@p:value), ',')"/>
		<table class="room_status">
			<tr>
				<th></th>
				<th>Washer #</th>
				<th>Washer Type</th>
				<th>Availability</th>
				<th>Est. Min. Remaining</th>
			</tr>
			<xsl:choose>
				<xsl:when test="count(/a:base/a:washers/r:results/r:row) = 0">
					<tr align="center">
						<td colspan="5">NO WASHERS FOUND</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
				<xsl:for-each select="/a:base/a:washers/r:results/r:row">
				<tr>
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="(position() mod 2) = 0">even</xsl:when>
							<xsl:otherwise>odd</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<td>
						<xsl:if test="r:statusCd != 1">
							<input type="checkbox" class="checkbox" name="selectedMachines" value="{r:hostId}">
								<xsl:variable name="hostId" select="r:hostId"/>
								<xsl:if test="$selectedMachines[.=$hostId]"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
							</input>
						</xsl:if>
					</td>
					<td>
						<xsl:value-of select="r:label" />
					</td>
					<td>
						<xsl:value-of select="r:hostGroup" />
						<!-- this was never set in the prvious implementation
						<xsl:value-of select="r:position" />
						-->
					</td>
					<td>
						<font>
							<xsl:attribute name="color">
								<xsl:choose>
									<xsl:when test="r:statusCd=1">green</xsl:when>
									<xsl:otherwise>red</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
							<xsl:choose>
								<xsl:when test="r:statusCd=1">Available</xsl:when>
								<xsl:when test="r:statusCd=2 or r:statusCd=7 or r:statusCd=8">
									<xsl:choose>
										<xsl:when test="r:runMinutes &gt;= 120">Cycle Complete</xsl:when>
										<xsl:otherwise>In Use</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="r:statusCd=5">Cycle Complete</xsl:when>
								<xsl:when test="r:statusCd=-1 or r:statusCd=0 or r:statusCd=3 or r:statusCd=4 or r:statusCd=6">Unavailable</xsl:when>
								<xsl:otherwise>Unknown</xsl:otherwise>
							</xsl:choose>
						</font>
					</td>
					<td>
						<xsl:if test="(r:statusCd=2 or r:statusCd=7 or r:statusCd=8) and r:runMinutes &lt; 120">
							<xsl:choose>
								<xsl:when test="r:remainingMinutes &gt; 0.0"><xsl:value-of select="ceiling(r:remainingMinutes)"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="9 + (ceiling(r:remainingMinutes) mod 9)"/></xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</td>
				</tr>
				</xsl:for-each>
			</xsl:otherwise>
			</xsl:choose>
			<tr class="footer">
				<td colspan="5"></td>
			</tr>
			<tr>
				<td colspan="5" height="2">
					&#160;
				</td>
			</tr>
			<tr>
				<th></th>
				<th>Dryer #</th>
				<th>Dryer Type</th>
				<th>Availability</th>
				<th>Est. Min. Remaining</th>
			</tr>
			<xsl:choose>
				<xsl:when test="count(/a:base/a:dryers/r:results/r:row) = 0">
					<tr align="center">
						<td colspan="5">NO DRYERS FOUND</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
				<xsl:for-each select="/a:base/a:dryers/r:results/r:row">
				<tr>
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="(position() mod 2) = 0">even</xsl:when>
							<xsl:otherwise>odd</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<td>
						<xsl:if test="r:statusCd != 1">
							<input type="checkbox" class="checkbox" name="selectedMachines" value="{r:hostId}">
								<xsl:variable name="hostId" select="r:hostId"/>
								<xsl:if test="$selectedMachines[.=$hostId]"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
							</input>
						</xsl:if>
					</td>
					<td>
						<xsl:value-of select="r:label" />
					</td>
					<td>
						<xsl:value-of select="r:hostGroup" />
						<!-- this was never set in the prvious implementation
						<xsl:value-of select="r:position" />
						-->
					</td>
					<td>
						<font>
							<xsl:attribute name="color">
								<xsl:choose>
									<xsl:when test="r:statusCd=1">green</xsl:when>
									<xsl:otherwise>red</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
							<xsl:choose>
								<xsl:when test="r:statusCd=1">Available</xsl:when>
								<xsl:when test="r:statusCd=2 or r:statusCd=7 or r:statusCd=8">
									<xsl:choose>
										<xsl:when test="r:runMinutes &gt;= 120">Cycle Complete</xsl:when>
										<xsl:otherwise>In Use</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="r:statusCd=5">Cycle Complete</xsl:when>
								<xsl:when test="r:statusCd=-1 or r:statusCd=0 or r:statusCd=3 or r:statusCd=4 or r:statusCd=6">Unavailable</xsl:when>
								<xsl:otherwise>Unknown</xsl:otherwise>
							</xsl:choose>
						</font>
					</td>
					<td>
						<xsl:if test="(r:statusCd=2 or r:statusCd=7 or r:statusCd=8) and r:runMinutes &lt; 120">
							<xsl:choose>
								<xsl:when test="r:remainingMinutes &gt; 0.0"><xsl:value-of select="ceiling(r:remainingMinutes)"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="9 + (ceiling(r:remainingMinutes) mod 9)"/></xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</td>
				</tr>
				</xsl:for-each>
			</xsl:otherwise>
			</xsl:choose>
			<tr class="footer">
				<td colspan="5"></td>
			</tr>
			<tr><td colspan="5"><div id="refreshTime"></div></td></tr>
		</table>

		<script type="text/javascript">
			$("washersAvailCount").setHTML("<xsl:value-of select="count(/a:base/a:washers/r:results/r:row[r:statusCd=1])" />");
			$("washersTotalCount").setHTML("<xsl:value-of select="count(/a:base/a:washers/r:results/r:row)" />");
			$("dryersAvailCount").setHTML("<xsl:value-of select="count(/a:base/a:dryers/r:results/r:row[r:statusCd=1])" />");
			$("dryersTotalCount").setHTML("<xsl:value-of select="count(/a:base/a:dryers/r:results/r:row)" />");
		 	$("refreshTime").setHTML("Last Refreshed " + new Date().toString());
		</script>
	</xsl:template>
</xsl:stylesheet>
