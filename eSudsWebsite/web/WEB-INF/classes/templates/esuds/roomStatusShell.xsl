<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:str="http://exslt.org/strings"
	xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r p str">
	<xsl:import href="resource:/templates/esuds/menuBase.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	<xsl:variable name="bottomLocation" select="(/a:base/a:locations/r:results/r:group[r:level=1]/r:group/r:row[r:selected='Y'])[1]"/>
	<xsl:variable name="locationId" select="reflect:getProperty($context, 'locationId')"/>
	<xsl:variable name="selectedLevel" select="$bottomLocation/../../r:level"/>
	<xsl:variable name="operator" select="/a:base/a:operator/r:results/r:row[1]"/>
	<xsl:variable name="operatorParam"><xsl:if test="$operator/r:operatorId">&amp;operatorId=<xsl:value-of select="$operator/r:operatorId"/></xsl:if></xsl:variable>
	<xsl:variable name="maxLevel" select="/a:base/a:locations/r:results/r:group[1]/r:level"/>
	<xsl:variable name="minLevel" select="/a:base/a:locations/r:results/r:group[last()]/r:level"/>

	<xsl:template name="title">eSuds.net :: Room Status</xsl:template>

	<xsl:template name="head">
		<meta http-equiv="Pragma" content="no-cache"/>
	 	<meta http-equiv="Expires" content="-1"/>
	</xsl:template>

	<xsl:template name="header">
		<div class="header_left">
			<a href="eSudsHome.do">
				<img class="logo" src="../images/esuds_logo.jpg" alt="esuds logo" height="100" width="560" border="0"/>
			</a>
		</div>
		<div class="header_right">
			<form>
				<xsl:variable name="studentAccess" select="reflect:getProperty($context, 'CUSTOM_FEATURES.STUDENT_ACCESS')"/>
				<table class="layout">
				<xsl:choose>
					<xsl:when test="$studentAccess='FULL'">
						<tr class="first"><td><a href="../StudentAccess/home.do">
							<input type="button" value="Access Your Account" onclick="this.parentNode.click()" />
						</a>
						</td></tr>
					</xsl:when>
					<xsl:when test="$studentAccess='NOTIFY'">
						<tr class="first"><td><a href="notifyConfig.do?action=start">
							<input type="button" value="Register for Auto-Notification" onclick="this.parentNode.click()" />
						</a>
						</td></tr>
					</xsl:when>
					<xsl:when test="$studentAccess='NOTIFY_USAGE'">
						<tr class="first"><td><a href="notifyConfig.do?action=start">
							<input type="button" value="View Usage / &#13;Register for Auto-Notification" onclick="this.parentNode.click()" />
						</a>
						</td></tr>
					</xsl:when>
				</xsl:choose>
				<tr class="second"><td>
				<a href="switch.do?page=/faq.do&amp;prefix=">
					<input type="button" value="FAQs" onclick="this.parentNode.click()" />
				</a>
				</td></tr>
				<xsl:variable name="customLinkTitle" select="reflect:getProperty($context, 'CUSTOM_FEATURES.CUSTOM_LINK_TITLE')"/>
				<xsl:variable name="customLinkHref" select="reflect:getProperty($context, 'CUSTOM_FEATURES.CUSTOM_LINK_HREF')"/>
				<xsl:if test="string-length(normalize-space($customLinkTitle)) &gt; 0 and string-length(normalize-space($customLinkHref)) &gt; 0">
					<tr class="customRoomStatusLink"><td><a href="{$customLinkHref}"><xsl:value-of select="$customLinkTitle"/></a></td></tr>
				</xsl:if>
				</table>
			</form>
		</div>
	</xsl:template>

	<xsl:template name="navigation">
		<xsl:if test="$selectedLevel &lt;= 3 and $selectedLevel &gt; 0">
			<font class="navigation_left">
				<xsl:if test="$operator/r:operatorName">
					<a href="showRoomStatus.i?operatorId={$operator/r:operatorId}">
						<xsl:value-of select="$operator/r:operatorName"/>
					</a>
				</xsl:if>
				<xsl:for-each select="/a:base/a:locations/r:results/r:group[r:level &gt; 2]/r:group/r:row">
					<xsl:if test="position() &gt; 1 or $operator/r:operatorName">&#160;&#187;</xsl:if>
					<xsl:choose>
						<xsl:when test="r:selectable = 'Y'">
							<a href="showRoomStatus.i?locationId={r:locationId}{$operatorParam}">
								<xsl:value-of select="r:locationName"/>
							</a>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="r:locationName"/></xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</font>
			<font class="navigation_right">
				<font class="refreshNote">
					<span id="refreshCountdown">
					Machine status will refresh in 120 seconds
					</span>
				</font>
			</font>
		</xsl:if>
	</xsl:template>

	<xsl:template name="menu">
		<xsl:if test="$selectedLevel &lt;= 3 and $selectedLevel &gt; 0">
			<!-- TODO: iterate through each distinct locationType -->
			<xsl:for-each select="/a:base/a:locations/r:results/r:group[r:level = 2]/r:group">
				<font class="title">
					<xsl:choose>
						<xsl:when test="r:locationTypeId = 17">Residence Halls</xsl:when>
						<xsl:otherwise>Locations</xsl:otherwise>
					</xsl:choose>
				</font>
				<br /><br />
				<xsl:for-each select="r:row">
					<span class="dormlinks" onmouseover="this.className = 'hoverdormlinks'" onmouseout="this.className = 'dormlinks'">
						<xsl:choose>
							<xsl:when test="r:selectable = 'Y'">
								<a href="showRoomStatus.i?locationId={r:locationId}{$operatorParam}">
									<xsl:value-of select="r:locationName"/>
								</a>
							</xsl:when>
							<xsl:otherwise><xsl:value-of select="r:locationName"/></xsl:otherwise>
						</xsl:choose>
					</span>
					<br/>
					<xsl:if test="r:locationId = $bottomLocation/r:parentLocationId">
						<xsl:for-each select="/a:base/a:locations/r:results/r:group[r:level = 1]/r:group/r:row[r:parentLocationId = $bottomLocation/r:parentLocationId]">
							<img height="22" width="18" alt="wm" src="../images/washericon.gif" />
							<span class="laundrylinks" onmouseover="this.className = 'hoverlaundrylinks'" onmouseout="this.className = 'laundrylinks'">
								<a href="showRoomStatus.i?locationId={r:locationId}{$operatorParam}">
									<xsl:value-of select="r:locationName"/>
								</a>
							</span>
							<br />
						</xsl:for-each>
						<br/>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<xsl:template name="body">
		<xsl:choose>
			<xsl:when test="not($selectedLevel &lt;= 3 and $selectedLevel &gt; 0)">
				<xsl:for-each select="/a:base/a:locations/r:results/r:group[r:level = $maxLevel]/r:group">
					<font class="title">
						<xsl:choose>
							<xsl:when test="r:locationTypeId = 6">School - Campus</xsl:when>
							<xsl:otherwise>Location</xsl:otherwise>
						</xsl:choose>
					</font>
					<xsl:call-template name="locationTree">
						<xsl:with-param name="rows" select="r:row"/>
						<xsl:with-param name="level" select="number(../r:level)"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="$bottomLocation">
				<!--  show room status -->
				<font class="title">
					<xsl:value-of select="$bottomLocation/r:locationName" />
				</font>
				<p>
					<font class="subtitle">Summary</font>
					<br />
					<span id="washersAvailCount">?</span>&#160;out of&#160;<span id="washersTotalCount">?</span>&#160;washers available.
					<br />
					<span id="dryersAvailCount">?</span>&#160;out of&#160;<span id="dryersTotalCount">?</span>&#160;dryers available.
				</p>
				<form id="notifyForm" action="requestNotification.do" method="post">
					<input type="hidden" name="roomId" value="{$bottomLocation/r:locationId}"/>
					<p class="notifiedInstructs">
						To be notified when a machine becomes available or when your wash or dry cycles are complete: select the box next to the machine for which you want to be notified, enter
						your e-mail address, and click "Notify Me".
					</p>
					<table class="form">
						<tr>
							<td class="label">Email Address:</td>
							<td class="field">
								<input type="text" name="emailAddress" class="input_style" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='emailAddress']/@p:value}"/>
							</td>
							<td align="left">
								<input type="submit" value="Notify Me" class="button2" />
							</td>
						</tr>
					</table>
					<div id="machineStatus">Loading...</div>
					<xsl:if test="string-length(normalize-space($bottomLocation/r:imageUrl)) &gt; 0">
						<img src="{normalize-space($bottomLocation/r:imageUrl)}"/>
					</xsl:if>
					<xsl:variable name="contact" select="/a:base/a:contact/r:results/r:row"/>
					<xsl:if test="string-length(normalize-space($contact/r:company)) + string-length(normalize-space($contact/r:fullName))
						+ string-length(normalize-space($contact/r:email)) + string-length(normalize-space($contact/r:phone))
						+ string-length(normalize-space($contact/r:cell)) + string-length(normalize-space($contact/r:url))
						&gt; 0">
						<table class="contactInfo"><tr><td>For laundry room or equipment service requests, please
							<xsl:choose>
								<xsl:when test="string-length(normalize-space($contact/r:company)) &gt; 0">
									contact <xsl:value-of select="$contact/r:company"/>
									<xsl:if test="string-length(normalize-space($contact/r:fullName)) &gt; 0"> - <xsl:value-of select="$contact/r:fullName"/></xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:phone)) &gt; 0"> - <xsl:value-of select="$contact/r:phone"/></xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:cell)) &gt; 0"> - <xsl:value-of select="$contact/r:cell"/> (cell)</xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:email)) &gt; 0"> - <a href="mailto:{$contact/r:email}"><xsl:value-of select="$contact/r:email"/></a></xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:url)) &gt; 0"> - <a href="{$contact/r:url}" target="_blank"><xsl:value-of select="$contact/r:url"/></a></xsl:if>
								</xsl:when>
								<xsl:when test="string-length(normalize-space($contact/r:fullName))">
									contact <xsl:value-of select="$contact/r:fullName"/>
									<xsl:if test="string-length(normalize-space($contact/r:phone)) &gt; 0"> - <xsl:value-of select="$contact/r:phone"/></xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:cell)) &gt; 0"> - <xsl:value-of select="$contact/r:cell"/> (cell)</xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:email)) &gt; 0"> - <a href="mailto:{$contact/r:email}"><xsl:value-of select="$contact/r:email"/></a></xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:url)) &gt; 0"> - <a href="{$contact/r:url}" target="_blank"><xsl:value-of select="$contact/r:url"/></a></xsl:if>
								</xsl:when>
								<xsl:when test="string-length(normalize-space($contact/r:url))&gt; 0">
									goto <a href="{$contact/r:url}" target="_blank"><xsl:value-of select="$contact/r:url"/></a>
									<xsl:if test="string-length(normalize-space($contact/r:phone)) &gt; 0"> or call <xsl:value-of select="$contact/r:phone"/></xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:cell)) &gt; 0">
										or <xsl:if test="string-length(normalize-space($contact/r:phone)) = 0">call </xsl:if>
										<xsl:value-of select="$contact/r:cell"/> (cell)</xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:email)) &gt; 0"> or email <a href="mailto:{$contact/r:email}"><xsl:value-of select="$contact/r:email"/></a></xsl:if>
								</xsl:when>
								<xsl:when test="string-length(normalize-space($contact/r:phone))&gt; 0">
									call <xsl:value-of select="$contact/r:phone"/>
									<xsl:if test="string-length(normalize-space($contact/r:cell)) &gt; 0"> or <xsl:value-of select="$contact/r:cell"/> (cell)</xsl:if>
									<xsl:if test="string-length(normalize-space($contact/r:email)) &gt; 0"> or email <a href="mailto:{$contact/r:email}"><xsl:value-of select="$contact/r:email"/></a></xsl:if>
								</xsl:when>
								<xsl:when test="string-length(normalize-space($contact/r:cell)) &gt; 0">
									call <xsl:value-of select="$contact/r:cell"/>
									<xsl:if test="string-length(normalize-space($contact/r:email)) &gt; 0"> or email <a href="mailto:{$contact/r:email}"><xsl:value-of select="$contact/r:email"/></a></xsl:if>
								</xsl:when>
								<xsl:when test="string-length(normalize-space($contact/r:email)) &gt; 0">
									email <a href="mailto:{$contact/r:email}"><xsl:value-of select="$contact/r:email"/></a>
								</xsl:when>
							</xsl:choose>
						</td></tr></table>
					</xsl:if>
				</form>
				<script type="text/javascript">
				var startTime = new Date().getTime();
				var loadMachineStatusRequest = new Ajax("machineStatus.i?bottomLocationId=<xsl:value-of select="$bottomLocation/r:locationId"/>",
					{update : $("machineStatus"), evalScripts : true});
				loadMachineStatusRequest.request();
				<xsl:variable name="refreshInterval" select="number(/a:base/a:parameters/p:parameters/p:parameter[@p:name='refreshInterval']/@p:value)"/>
				var refreshSeconds = <xsl:choose>
						<xsl:when test="$refreshInterval &gt; 0"><xsl:value-of select="$refreshInterval" /></xsl:when>
						<xsl:otherwise>120</xsl:otherwise>
					</xsl:choose>;
				var refreshTimesCount=15;
				var refreshTimes=refreshTimesCount;
				var refreshIntervalId=0;
				function countdown() {
					if(refreshTimes==0){
							var confirmResult = confirm("Click 'OK' if you would like to continue to update this information.")
							if(confirmResult){
								refreshTimes=refreshTimesCount;
							}else{
								$("refreshCountdown").setHTML("Please click browser's refresh button to restart machine status refresh.");
								window.clearInterval(refreshIntervalId);
							}
					}
					if(refreshTimes &gt; 0){
						var remaining = refreshSeconds - Math.round((new Date().getTime() - startTime)/1000);
						if(remaining &lt;= 1) {
							loadMachineStatusRequest.request($("machineStatus").toQueryString());
							startTime = new Date().getTime();
							refreshTimes--;
						}
						$("refreshCountdown").setHTML("Machine status will refresh in "+Math.max(1, remaining)+" seconds");
					}
				};
				refreshIntervalId=window.setInterval("countdown()", 1000);
				</script>

			</xsl:when>
			<xsl:otherwise>
				<font class="title">No Laundry Rooms Found</font>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="locationTree">
		<xsl:param name="rows"/>
		<xsl:param name="level"/>
		<ul class="treeLevel{$level}">
			<xsl:for-each select="$rows">
				<li>
					<xsl:choose>
						<xsl:when test="r:selectable = 'Y'">
							<a href="showRoomStatus.i?locationId={r:locationId}{$operatorParam}">
								<xsl:value-of select="r:locationName"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="r:locationName"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$level &gt; $minLevel">
						<xsl:variable name="locationId" select="r:locationId"/>
						<xsl:call-template name="locationTree">
							<xsl:with-param name="rows" select="/a:base/a:locations/r:results/r:group[r:level = $level - 1]/r:group/r:row[r:parentLocationId=$locationId]"/>
							<xsl:with-param name="level" select="$level - 1"/>
						</xsl:call-template>
					</xsl:if>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>
</xsl:stylesheet>
