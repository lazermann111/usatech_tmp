
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<font class="title">Reconciliation Summary</font>
<br/>
<font class="subtitle">Summary of Transactions Settled<br/>
between <bean:write name="startDate" format="MM/dd/yyyy hh:mm aa"/>
<br/>and <bean:write name="endDate"  format="MM/dd/yyyy hh:mm aa"/></font>
<table class="report">
	<tr><th>Campus</th><th>Authority</th><th>Wash Cycles</th><th>Wash Amount</th><th>Dry Cycles</th><th>Dry Amount</th><th>Total Cycles</th><th>Total Amount</th></tr>
	<logic:empty name="results"><tr><td colspan="8">NO RESULTS</td></tr></logic:empty>
	<%int i = 1;%>
	<logic:iterate name="results" id="lineItem" scope="request">
		<tr class='<%= (i++ % 2 == 1) ? "odd" : "even" %>'>
			<td><html:link action="reconciliationDetail" name="lineItem" property="parameters"><bean:write name="lineItem" property="campusName" /></html:link></td>
			<td><html:link action="reconciliationDetail" name="lineItem" property="parameters"><bean:write name="lineItem" property="authName" /></html:link></td>
			<td><bean:write name="lineItem" property="totalWashCycles" /></td>
			<td><bean:write name="lineItem" property="totalWashAmount" format="$0.00" /></td>
			<td><bean:write name="lineItem" property="totalDryCycles" /></td>
			<td><bean:write name="lineItem" property="totalDryAmount" format="$0.00" /></td>
			<td><bean:write name="lineItem" property="totalCycles" /></td>
			<td><bean:write name="lineItem" property="totalAmount" format="$0.00" /></td>
		</tr>
	</logic:iterate>
	<tr class="footer"><td colspan="8"/></tr>	
</table>


