<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<font class="title">Reconciliation Detail</font>
<br/>
<font class="subtitle">Transactions Settled<br/>
between <bean:write name="startDate" format="MM/dd/yyyy hh:mm aa"/><br/> 
and <bean:write name="endDate"  format="MM/dd/yyyy hh:mm aa"/><br/><br/></font>
<font class="report_caption">Campus: <bean:write name="campusName"/><br/>
Authority: <bean:write name="authName"/><br/></font>
<table><tr><td align="right">
<span class="note2">* - No configuration information exists for this account</span>
</td></tr>
<tr><td>
<table class="report">
	<tr><th>Residence Hall</th><th>Laundry Room</th><th>Settled Time</th><th>Account Number</th><th>Description</th><th>Transaction Time</th><th>Total Amount</th></tr>
	<logic:empty name="results"><tr><td colspan="7">NO RESULTS</td></tr></logic:empty>
	<%int i = 1;%>
	<logic:iterate name="results" id="lineItem" scope="request">
		<tr class='<%= (i++ % 2 == 1) ? "odd" : "even" %>'>
			<td><bean:write name="lineItem" property="dormName" /></td>
			<td><bean:write name="lineItem" property="roomName" /></td>
			<td><bean:write name="lineItem" property="settledTs" format="MM/dd/yyyy hh:mm aa"/></td>
			<td><bean:write name="lineItem" property="accountNumber"/>
				<logic:equal name="lineItem" property="consumerMissing" value="true">
				<span class="note2">*</span>
				</logic:equal>
			</td>
			<td><span class="lineItem"><%String details = ((com.usatech.esuds.server.adminreports.beans.ReconciliationDetailBean)lineItem).getTranLineItemDesc();%>
			<%=details == null ? "" : org.apache.commons.lang.StringUtils.replace(details, "\r", "</span>\n<span class=\"lineItem\">")%></span></td>
			<td><bean:write name="lineItem" property="tranStartTs" format="MM/dd/yyyy hh:mm aa"/></td>
			<td><bean:write name="lineItem" property="initialAmount" format="$0.00" /></td>
		</tr>
		<logic:present name="lineItem" property="refundAmount">
		<tr class='<%= (i % 2 == 0) ? "oddCaveat" : "evenCaveat" %>'>
			<td><bean:write name="lineItem" property="dormName" /></td>
			<td><bean:write name="lineItem" property="roomName" /></td>
			<td><bean:write name="lineItem" property="refundSettledTs" format="MM/dd/yyyy hh:mm aa"/></td>
			<td><bean:write name="lineItem" property="accountNumber"/>
				<logic:equal name="lineItem" property="consumerMissing" value="true">
				<span class="note2">*</span>
				</logic:equal>
			</td>
			<td>*** 
				<logic:greaterThan name="lineItem" property="totalAmount" value="0">Partially</logic:greaterThan>
				Refunded (<bean:write name="lineItem" property="refundDesc" />) ***
			</td>
			<td><bean:write name="lineItem" property="tranStartTs" format="MM/dd/yyyy hh:mm aa"/></td>
			<td><bean:write name="lineItem" property="refundAmount" format="$0.00" /></td>
		</tr>
		</logic:present>
	</logic:iterate>
	<tr class="footer"><td colspan="7"/></tr>	
</table>
</td></tr></table>

