<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script language="JavaScript" src="<html:rewrite page='/scripts/calendar.js' />" >
</script>
<script language="JavaScript" src="<html:rewrite page='/scripts/calendar_helper.js' />">
</script>
<script language="JavaScript">
var calendar = "<html:rewrite page='/tiles/pages/calendar.html' />";
</script>

<html:form action="/reconciliationReport">
<html:hidden property="action" value="runReport" />
<font class="title">Reconciliation Report</font>
<br/>
	<TABLE border="0" cellspacing="10" width="100%">
		<TR valign="top">
			<TD><font class="subtitle">Location (Required)</font></TD>
			<TD><font class="subtitle">Date Range (Required)</font></TD>
		</TR>
		<TR valign="top">
			<TD><logic:iterate name="locations" id="location">
				<html:radio property="locationId" idName="location" value="locationId" />
				<bean:write name="location" property="locationName" />
				<BR>
			</logic:iterate></TD>
			<TD>
<html:radio property="dateChoice" value="today" /> Today
<BR>
<html:radio property="dateChoice" value="yesterday" /> Yesterday
<BR>
<html:radio property="dateChoice" value="since" /> Since <html:text size="10" property="sinceDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('sinceDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
<BR>
<html:radio property="dateChoice" value="on" /> On <html:text size="10" property="onDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('onDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
<BR>
<html:radio property="dateChoice" value="fromto" /> From <html:text size="10" property="fromDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('fromDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
 to <html:text size="10" property="toDate" /> <html:link href="javascript:doNothing()" onclick="showCalendar('toDate')"><img src="../../images/calendar.gif" border="0"/></html:link>
 </TD>
<TD>
</TD>
</TR>
<TR>
<TD colspan="2" align="center"><html:submit styleClass="button2" value="Search">Search</html:submit></TD>
</TR>
</TABLE>
</html:form>

