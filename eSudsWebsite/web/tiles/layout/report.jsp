<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
         "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<html:base />
	<link href="../../css/usalive-general-style.css" rel="stylesheet" type="text/css"/>
	<link href="../../css/usalive-report-style.css" rel="stylesheet" type="text/css"/>
	<link href="../../css/esuds3.css" rel="stylesheet" type="text/css"/>
	<link href="../../css/esuds-report-style.css" rel="stylesheet" type="text/css"/>
	<logic:present name="CUSTOM_CSS_PATH">
	<link href="../../<bean:write name="CUSTOM_CSS_PATH"/>" rel="stylesheet" type="text/css"/>
	</logic:present>
	<meta http-equiv="Pragma" content="no-cache" />
 	<meta http-equiv="Expires" content="-1" />
<title><tiles:getAsString name="title" /> :: <tiles:getAsString name="subtitle" /></title>
</head>
<body class="layout">
<div id="userbar"><tiles:insert attribute="userbar" /></div>
<div id="header"><tiles:insert attribute="header" /></div>
<div id="navigation"><tiles:insert attribute="navigation" /></div>
<div id="center">
	<div id="content_only">
	<div id="messages"><tiles:insert attribute="messages" /></div>
	<jsp:include flush="true" page="/eSudsReport.i"></jsp:include>
	</div>
</div>
</body>
</html>
