<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<p class="title">Overview</p>
<p>
<b>e-Suds.net</b>&#x2122; is a comprehensive online laundry payment, operating and service system, provided by USA Technologies. USA Technologies is able to connect washing machines and dryers to the Internet. As a laundry operator, this gives you the ability to go online to monitor service conditions, usage and sales, utilize alternative payment methods and offer a number of value-added services to your customers. The system ultimately creates the competitive advantage that you need to succeed in the highly competitive commercial laundry market.
</p>
<p> 
e-Suds.net&#x2122; gives laundry operators the ability to:</p>
<ul>
<li>effectively conduct coin-free transactions</li> 
<li>sell injected detergent and fabric softener as part of a wash</li> 
<li>provide your customers with online machine availability and status</li> 
<li>hold service employees accountable for cash and inventory</li> 
<li>service machines on an as-needed basis, reducing service costs and machine down time</li>
</ul>

<p> 
By putting your system online, you also gain greater control over creating, changing, and modifying your sales and marketing programs to both maximize profits and enhance service. 
</p>
