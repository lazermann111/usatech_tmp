<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%/*
<html:link action="/overview">Overview</html:link><br/>
<html:link action="/paymentSolutions">Payment Solutions</html:link><br/>
<html:link action="/valueAdd">Value-added Services</html:link><br/>
<html:link action="/university">University Programs</html:link><br/>
*/%>
<html:link action="/faq">FAQs</html:link><br/>
<br/>
<logic:present cookie="roomId">
<html:link action="/RoomStatus/roomStatus.do">Room Status</html:link>
</logic:present>
<br/>
