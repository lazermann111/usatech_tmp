<div class="faqs">
<span class="question">What is e-Suds<sup>TM</sup>?</span>
<span class="answer">e-Suds<sup>TM</sup> is an innovative online laundry system that gives students the ability to:
<ul><li>Go online to check if washers and dryers are available in specific laundry rooms.</li>
<li>Use their student ID or PIN code to activate and pay for the wash and dry cycles.</li>
<li>Receive notification that their wash and dry cycles are complete via email, cell phone or PDA.</li>
</ul>
</span>
<hr/>
<span class="question">What does "Available", "In Use" and "Cycle Complete" mean?</span>
<span class="answer">e-Suds<sup>TM</sup> reports the following statuses for washers and dryers:
<ul><li>"Available" means that another user has not started a particular machine.</li>
<li>"In use" means a user has started the machine and it is in use.</li>
<li>"Cycle Complete" means the user has not opened the machine's door to remove their laundry, but the cycle is complete.</li>
</ul>
</span>
<hr/>
<span class="question">How do the notifications work?</span> 
<span class="answer">Notifications work two ways:
<ol><li>If the laundry room is busy, you can request to be notified when a specified washer and/or dryer become available.</li>
<li>If you have laundry in a washer or dryer, you can request to be notified when your load of laundry has completed the washing or drying cycle.</li> 
</ol>
</span>
<hr/>
<span class="question">What if I can't find my laundry room?</span>
<span class="answer">If you are able to successfully find your location and view the list of laundry rooms, it is possible that the specific laundry room you are looking for is not connected to the e-Suds<sup>TM</sup> system.</span>
<hr/>
<span class="question">Why does the e-Suds<sup>TM</sup> Web site occasionally indicate that there is no more time remaining and that my dryer is in use?</span>
<span class="answer">For some types of dryers, the e-Suds<sup>TM</sup> Web site reports average time of the dryer cycle.  This occasionally results in the Web site reporting that there is no time remaining and that the machine is in use.
</span>
</div>