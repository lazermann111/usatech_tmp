<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<p class="title">Payment Solutions</p>
<p>
You, as a laundry operator, understand the challenges of coin-only machines - you face them everyday. Coin jams are inevitable (often rendering machines out of service), stops to collect coins are required (losing time and money in labor costs), change machines are a necessity (increasing the threat of theft). 
Eliminating the use of coins opens new revenue streams, decreases maintenance and labor issues, increases customer satisfaction, increases customer loyalty, and, ultimately, increases ROI.</p>

<p> USA Technologies offers several non-cash options to users. Through its e-Suds.net&#x2122; system you can use RFID (Radio Frequency Identification) tags, student IDs with personal identification numbers (PINs), and Pre-paid Wash Cards. eSuds.net&#x2122; <!-- <img align="right" src="images/esuds_keyfob.gif" hspace="8" vspace="8" alt="keyfob" />-->enables the customer to charge their laundry costs back to an already existing account or to their credit card. This technology works particularly well in a university program and in multi-housing facilities such as apartments and condominiums.</p>

