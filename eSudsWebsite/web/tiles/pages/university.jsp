<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<p class="title">University Programs</p>
<div class="bodycontent">
<p>The university market presents both unique opportunities and unique challenges for laundry operators.</p>
<p> 

University operators have access to a captive community of consumers while both their buying and personal habits are still being formed. However, university operators also have to keep up with student demand for increased technology, the streamlining of services and value-added features.</p>
<p> 
USA Technologies developed a comprehensive package for the university operator. The service includes our online reporting capabilities, plus:</p>
<ul>
<li>Coin-free transaction options - Purchases made with Student ID or PIN card and charged to their student account</li>
<li>Email or pager notification - An email or pager system notifies students when their wash is done</li> 
<li>A virtual view of the laundry room - A website gives students access to machine availability over the internet right from their personal PC</li> 
<li>Detergent and fabric softener injectables - Students can purchase these items to be injected directly into their wash at an additional cost</li>
</ul>

</div>
