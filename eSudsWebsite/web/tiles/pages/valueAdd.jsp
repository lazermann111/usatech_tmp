<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<p class="title">Value-added Services</p>
<p>
eSuds.net&#x2122; enables laundry operators to offer their customers several added benefits, including:</p>

<p> 
<b>Machine Availability and Monitoring</b><br /> 
Access to an interactive website capable of displaying a "virtual view" of a laundry room. Each washer and dryer is marked as either "available" or "in-use". If equipment is "in-use" the remaining cycle time is displayed.</p>
<p> 
<b>Email and Pager Notification</b><br /> 
Consumers can choose to receive email or pager notification upon completion of their wash and dry cycles.</p>
<p> 
<b>Injectable Detergent and Fabric Softener</b><br /> 
Any machine equipped with the e-Suds.net&#x2122; non-cash payment technology can also be fitted with pay-injection hardware. This hardware offers consumers the convenience of purchasing detergent and fabric softener as part of the wash cycle.</p>

<p>
The detergent and fabric softener are housed in one central location and pumped across as many as 8 washers.  The pumping system is airless and dripless - therefore mess free.  With a simple push of a button detergent and fabric sotener are injected into the wash cycle. The products are monitored using USA Technologies' services and a "ticket" is automatically generated to let the operator know when its time to easily refill.</p>
<p> 
Pricing for both laundry detergent and fabric softener is customized to the location, representing a significant profit margin opportunity for the operator.  
</p>
