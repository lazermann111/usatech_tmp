<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<p class="title">Carnegie Mellon Getting Started</p>

<p class="subtitle">Step 1</p>
<p>
Go to <html:link href="http://carnegiemellon.esuds.net/StudentAccess">http://carnegiemellon.esuds.net/StudentAccess</html:link>.
</p>
<p class="subtitle">Step 2 </p>
<p>
Enter in username and password to login.
<br /> <br />
Username: your complete CMU andrew email address.
<br />
Temorary Password: last four digits of your card number.
<br />
(You can get your card number by logging into SIO, Student Information On-Line at <html:link href="http://www.cmu.edu/hub/" >http://www.cmu.edu/hub/</html:link>.)
<br /> <br />
Select [Login] button.
</p>
<p class="subtitle">Step 3</p>
<p>
Click on [Change Password] link in upper right hand corner to create your own unique password.  Fill in the form.
<br /> <br />
Password must be at least 8 characters and contain letters and at least one number.
<br /> <br />
Select [Change Password] button and your new password will be saved.
</p>
<p class="subtitle">Step 4</p>
<p>
If you would like to be notified when your wash or dry cycles are done: In the [Notify Email Address] field, type in the complete email address where you'd like to be notified.
<br />
Select [Update] button.
</p>
<p class="subtitle">Step 5</p>
<p>
To view machine availability and laundry room status, go to <html:link href="http://carnegiemellon.esuds.net/RoomStatus">http://carnegiemellon.esuds.net/RoomStatus</html:link>
<br/>
(We suggest you bookmark this webpage in your browser for future visits.)
<br />
<br />
Take your laundry to the laundry room and follow the instructions on the wall mounted room controller.
</p>
<br />
<p>
Questions or Comments? Please send email to Carnegie Mellon Housing Services at <html:link href="mailto:univhous@andrew.cmu.edu">univhous@andrew.cmu.edu</html:link>
</p>
<p>
Technical Questions? Call customer care at 800-633-0340.
</p>