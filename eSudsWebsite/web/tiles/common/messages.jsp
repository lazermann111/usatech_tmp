<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<font class="success">
<html:messages id="message" message="true">
<span id="success"><bean:write name="message" /></span><br/>
</html:messages>
<logic:present name="message">
<br>
</logic:present>
</font>
<font class="error">
<html:errors />
<logic:present name="org.apache.struts.action.ERROR">
<br>
</logic:present>
</font>