<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<logic:present name="subject" scope="session">
<font class="user_button">
<logic:present cookie="home">
<logic:equal cookie="home" value="StudentAccess">
<logic:present name="roomStatusParams">
	<html:link action="/switch.do?page=/showRoomStatus.do&amp;prefix=/RoomStatus" name="roomStatusParams">View Room Status</html:link> |
</logic:present>
<logic:notPresent name="roomStatusParams">
	<html:link action="/switch.do?page=/showRoomStatus.do&amp;prefix=/RoomStatus">View Room Status</html:link> |
</logic:notPresent>
<html:link action="/switch?page=/home.do&prefix=/StudentAccess">Student Access</html:link> |
</logic:equal>
<logic:equal cookie="home" value="OperatorAccess">
<html:link action="/switch?page=/home.do&prefix=/OperatorAccess">Operator Access</html:link> |
</logic:equal>
<logic:equal cookie="home" value="OperatorReports">
<html:link action="/switch?page=/home.do&prefix=/OperatorReports">Operator Reports</html:link> |
</logic:equal>
</logic:present>
<html:link action="/logout">Logout</html:link>  |
<html:link action="/changePassword" >Change Password</html:link>
</font>
</logic:present>
<logic:notPresent name="subject" scope="session">
<!-- <font class="user_button"><html:link action="/login">Login</html:link></font> -->
</logic:notPresent>
