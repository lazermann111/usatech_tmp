package com.usatech.esuds.utils;

import java.util.Iterator;
import java.util.Locale;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.ErrorMessages;
import org.apache.struts.util.MessageResources;

import simple.bean.ConvertException;
import simple.io.Log;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;

public class StrutsHelper {
	private static final Log log = Log.getLog();
	
	public static String[] getMessages(InputForm inputForm, String attributeName) throws ConvertException {
		String path;
		if(inputForm.getRequest() instanceof HttpServletRequest) {
			path = ((HttpServletRequest)inputForm.getRequest()).getServletPath();
			int pos = path.lastIndexOf('/');
			if(pos < 0) path = "";
			else path = path.substring(0, pos);
		} else {
			path = "";
		}
		MessageResources resources = null;
		while(true) {
			log.debug("Searching for resource bundle at " + Globals.MESSAGES_KEY + path);
			resources = (MessageResources)inputForm.getAttribute(Globals.MESSAGES_KEY + path);
			if(resources != null) break;
			int pos = path.lastIndexOf('/');
			if(pos < 0) break;
			path = path.substring(0, pos);
		}
		if(resources == null) log.warn("Could not find message resources");
		ServletRequest req = inputForm.getRequest();
       	Locale userLocale;
       	if(req instanceof HttpServletRequest)
			userLocale = RequestUtils.getLocale((HttpServletRequest) req);
       	else
       		userLocale = Locale.getDefault();
       	
       	Object value = inputForm.getAttribute(attributeName); //<html:messages name="_________">

       	String[] messages;
        if(value == null) {
            messages = new String[0];
        } else if(value instanceof String) {
        	messages = new String[] {resources == null ? (String)value : resources.getMessage(userLocale, (String)value)};
        } else if(value instanceof String[]) {
            String[] keys = (String[]) value;
            messages = new String[keys.length];
            for (int i = 0; i < keys.length; i++)
            	messages[i] = resources == null ? keys[i] : resources.getMessage(userLocale, keys[i]);
        } else if (value instanceof ErrorMessages) {
            String keys[] = ((ErrorMessages) value).getErrors();
            if (keys == null) {
                keys = new String[0];
            }
            messages = new String[keys.length];
            for (int i = 0; i < keys.length; i++)
            	messages[i] = resources == null ? keys[i] : resources.getMessage(userLocale, keys[i]);
        } else if(value instanceof ActionMessages) {
        	ActionMessages ams = (ActionMessages)value;
        	messages = new String[ams.size()];
        	Iterator iter = ams.get();
            for(int i = 0; iter.hasNext(); i++) {
            	ActionMessage am = (ActionMessage)iter.next();
            	if(resources == null)
            		messages[i] = am.getKey();
            	else if(am.getValues() == null)
            		messages[i] = resources.getMessage(userLocale, am.getKey());
            	else 
            		messages[i] = resources.getMessage(userLocale, am.getKey(), am.getValues());
            }
        } else {
            throw new ConvertException("Could not make '" + value.getClass() + "' into a message",String.class, value);
        }
        if(log.isDebugEnabled())
        	log.debug("Messages for '" + attributeName + "': Turned " + value + " into " + messages.length + " messages");
       	
        return messages;
	}
}
