package com.usatech.esuds.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.usatech.esuds.server.security.exceptions.FailedLogonException;
import com.usatech.esuds.server.security.exceptions.UserException;
import com.usatech.esuds.server.security.services.SecurityService;

import simple.bean.MaskedString;
import simple.servlet.InputForm;
import simple.servlet.ServletUser;
import simple.servlet.steps.AbstractLogonStep;
import simple.servlet.steps.LoginFailureException;

public class EsudsLogonStep extends AbstractLogonStep {

	public EsudsLogonStep() {
	}

	@Override
	public ServletUser authenticateUser(InputForm form) throws ServletException {
		try {
			Object password = form.get("password");
			SecurityService.login((HttpServletRequest)form.getRequest(), form.getString("username", true),
					password instanceof MaskedString ? ((MaskedString)password).getValue() : password == null ? null : password.toString(),
					form.getString("application", false));
		} catch(FailedLogonException e) {
			throw new LoginFailureException(e.getMessage(), 0, e);
		} catch(UserException e) {
			throw new LoginFailureException(e.getMessage(), 0, e);
		}
		return null;
	}

}
