package com.usatech.esuds.servlet;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.falcon.servlet.FalconUser;
import simple.io.Log;
import simple.text.StringUtils;

public class EsudsFalconUser extends FalconUser {
	private static final Log log = Log.getLog();
	private static final long serialVersionUID = 901237101951045610L;
	public static enum Action { CREATE, READ, MODIFY, DELETE };
	protected static final Set<String> APPS = new HashSet<String>();
	protected static final Set<String> OBJECT_TYPES = new HashSet<String>();
    protected static final char[] DELIMITERS = "[](),|/.:".toCharArray();
	protected static class CredentialKey implements Serializable {
		private static final long serialVersionUID = 99847746104146L;
		protected final String app;
		protected final String type;
		protected final Action action;
		protected final int hash;
		public CredentialKey(final String app, final String type, final Action action) {
			super();
			this.app = app;
			this.type = type;
			this.action = action;
			int h = 0;
			if(type != null) h += 31 * 31 * type.hashCode();
			if(action != null) h += + 31 * action.hashCode();
			if(app != null) h += app.hashCode();
			this.hash = h;
		}
		public Action getAction() {
			return action;
		}
		public String getApp() {
			return app;
		}
		public String getType() {
			return type;
		}
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof CredentialKey)) return false;
			CredentialKey ck = (CredentialKey) obj;
			return hash == ck.hash && ConvertUtils.areEqual(type, ck.type) && ConvertUtils.areEqual(action, ck.action) && ConvertUtils.areEqual(app, ck.app);
		}
		@Override
		public int hashCode() {
			return hash;
		}
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			if(app != null) sb.append(app);
			if(type != null) {
				if(sb.length() > 0) sb.append(':');
				sb.append(type);
			}
			if(action != null) {
				if(sb.length() > 0) sb.append(':');
				sb.append(action);
			}
			return sb.toString();
		}
	}
	protected final transient DynaClass dynaClass = new DynaClass() {
		public DynaProperty[] getDynaProperties() {
			CredentialKey[] cks = new CredentialKey[credentials.size()];
			cks = credentials.keySet().toArray(cks);
			DynaProperty[] dynaProperties = new DynaProperty[cks.length];
			for(int i = 0; i < cks.length; i++) {
				dynaProperties[i] = new DynaProperty(cks[i].toString(), Set.class);
			}
			return dynaProperties;
		}

		public DynaProperty getDynaProperty(String name) {
			if(credentialDynaBean.get(name) != null)
				return new DynaProperty(name, Set.class);
			else
				return null;
		}

		public String getName() {
			return "com.usatech.esuds.servlet.EsudsFalconUser.Credentials";
		}

		public org.apache.commons.beanutils.DynaBean newInstance() throws IllegalAccessException, InstantiationException {
			return credentialDynaBean; // credentialDynaBean is immutable so we can just return the singleton
		}
	};
	static {
		APPS.add("OperatorAccess".toUpperCase());
		APPS.add("OperatorReports".toUpperCase());
		APPS.add("StudentAccess".toUpperCase());
		APPS.add("UserMaintenance".toUpperCase());
		APPS.add("AdminReports".toUpperCase());

		OBJECT_TYPES.add("STUDENT_RECORD");
		OBJECT_TYPES.add("OPERATOR_RECORD");
		//OBJECT_TYPES.add("ROOM_STATUS_RECORD");
		//OBJECT_TYPES.add("PERMISSION_RECORD");
		//OBJECT_TYPES.add("CAMPUS_RECORD");
		OBJECT_TYPES.add("LOCATION_RECORD");
		OBJECT_TYPES.add("LOCATION_ACCOUNTS");
	}
	protected final Map<CredentialKey,Set<Long>> credentials = new HashMap<CredentialKey,Set<Long>>();
	protected final transient DynaBean credentialDynaBean = new DynaBean() {
		public boolean contains(String name, String key) {
			Set<Long> ids = get(name);
			try {
				return ids != null && ids.contains(Long.parseLong(key));
			} catch(NumberFormatException e) {
				return false;
			}
		}

		public Set<Long> get(String name) {
			String[] parts = StringUtils.split(name, DELIMITERS, false);
			if(parts.length < 2) {
				log.warn("Could not parse '" + name + "' into two or three parts");
				return null;
			}
			Action action = null;
			String type = null;
			String app = null;
			for(String part : parts) {
				part = part.trim().toUpperCase();
				if(app == null && APPS.contains(part)) {
					app = part;
					continue;
				}
				if(type == null && OBJECT_TYPES.contains(part)) {
					type = part;
					continue;
				}
				if(action == null) {
					try {
						action = Action.valueOf(part);
					} catch(IllegalArgumentException e) {
						log.warn("Credential part '" + part + "' does not match an app, object type, or action");
						return null;
					}
				} else {
					log.warn("Credential part '" + part + "' does not match an app, or object type");
					return null;
				}
			}
			if(action == null) {
				log.warn("Credential '" + name + "' does not contain an action");
				return null;
			} else if(type == null) {
				log.warn("Credential '" + name + "' does not contain an object type");
				return null;
			}

			return credentials.get(new CredentialKey(app, type, action));
		}

		public Long get(String name, int index) {
			Set<Long> ids = get(name);
			if(ids == null || ids.size() >= index)
				return null;
			Iterator<Long> iter = ids.iterator();
			Long id = null;
			for(int i = 0; i < index; i++) {
				id = iter.next();
			}
			return id;
		}

		public Object get(String name, String key) {
			if(name == null || name.length() == 0)
				return get(key);
			else
				return contains(name, key);
		}

		public DynaClass getDynaClass() {
			return dynaClass;
		}

		public void remove(String name, String key) {
			throw new UnsupportedOperationException("Credentials DynaBean is not directly modifiable");
		}

		public void set(String name, Object value) {
			throw new UnsupportedOperationException("Credentials DynaBean is not directly modifiable");
		}

		public void set(String name, int index, Object value) {
			throw new UnsupportedOperationException("Credentials DynaBean is not directly modifiable");
		}

		public void set(String name, String key, Object value) {
			throw new UnsupportedOperationException("Credentials DynaBean is not directly modifiable");
		}
	};
	public EsudsFalconUser() {
	}

	public boolean addCredential(String app, String type, Action action, long id) {
		CredentialKey key = new CredentialKey(app, type, action);
		Set<Long> ids = credentials.get(key);
		if(ids == null) {
			ids = new HashSet<Long>();
			credentials.put(key, ids);
		}
		boolean added = ids.add(id);
		if(app != null)
			addCredential(null, type, action, id);
		return added;
	}

	public DynaBean getCredentials() {
		return credentialDynaBean;
	}
}
