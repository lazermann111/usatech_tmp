package com.usatech.esuds.servlet;


import java.util.Set;

import javax.security.auth.Subject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import simple.falcon.servlet.FalconServlet;
import simple.servlet.ServletUser;
import simple.text.StringUtils;
import simple.translator.DBFullTranslatorFactory;
import simple.translator.RecordMissingDBFullTranslatorFactory;
import simple.translator.TranslatorFactory;

import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.servlet.EsudsFalconUser.Action;
import com.usatech.security.jaas.AppCredential;
import com.usatech.security.jaas.NamePrincipal;
import com.usatech.security.jaas.UserPrincipal;

public class EsudsFalconServlet extends FalconServlet {
    private static final long serialVersionUID = 2394111245025815L;
    protected static final long[] ESUDS_USER_GROUPS = new long[] {10};
    protected String recordMissingTranslations;
    
    public EsudsFalconServlet() {
        super();
        restrictedParameterNames.add("subject");
    }

    protected ServletUser login(HttpServletRequest request) throws ServletException {
    	Subject subject = SecurityService.getSubject(request);
    	if(subject != null) {
    		Set<UserPrincipal> set = subject.getPrincipals(UserPrincipal.class);

            //there should only be one user principal
            if (set.size() != 1) {
                //handle this by throwing exception or logging out or something
                log.error("More than one user logged in.");
                return null;
            }

            UserPrincipal up = set.iterator().next();
            EsudsFalconUser user = new EsudsFalconUser();
            user.setUserId(Long.parseLong(up.getName()));
            StringBuilder userName = new StringBuilder();
            for(NamePrincipal np : subject.getPrincipals(NamePrincipal.class)) {
            	if(userName.length() > 0) userName.append('/');
            	userName.append(np.getName());
            }
            user.setUserName(userName.toString());
            for(AppCredential ac : subject.getPublicCredentials(AppCredential.class)) {
            	for(String action : StringUtils.split(ac.getActions(), ',')) {
            		Action act;
            		try {
            			act = Action.valueOf(action.trim().toUpperCase());
            		} catch(IllegalArgumentException e) {
            			throw new ServletException(e);
            		}
            		user.addCredential(ac.getApp(), ac.getObjectType(), act, ac.getObjectId());
            	}

            }
            user.setUserGroupIds(ESUDS_USER_GROUPS);

            /*
            int[] privs = user.getPrivs();
            String[] fprivs = new String[privs.length];
            for(int i = 0; i < privs.length; i++) fprivs[i] = "" + privs[i];
            fuser.addPrivileges(fprivs);
            */
            request.getSession().setAttribute(ATTRIBUTE_USER, user);
            return user;
        }
        return null;
    }

    public void init() throws ServletException {
    	try {
    	super.init();
    	String recordMissingRegex = getRecordMissingTranslations();
        DBFullTranslatorFactory xf;
        if(recordMissingRegex != null && (recordMissingRegex=recordMissingRegex.trim()).length() > 0) {
        	RecordMissingDBFullTranslatorFactory rmxf = new RecordMissingDBFullTranslatorFactory();
        	rmxf.setFilter(recordMissingRegex);
        	rmxf.setOnMissingCallId("SAVE_PLACEHOLDER_TRANSLATION");
        	xf = rmxf;
        } else {
        	xf = new DBFullTranslatorFactory();
        }
        xf.setCallId("GET_ALL_TRANSLATIONS");
        TranslatorFactory.setDefaultFactory(xf);
    	} catch(ServletException e) {
    		log.error("Could not fully initialize servlet, continuing anyway", e);
    	}
    }

	public String getRecordMissingTranslations() {
		return recordMissingTranslations;
	}

	public void setRecordMissingTranslations(String recordMissingTranslations) {
		this.recordMissingTranslations = recordMissingTranslations;
	}    
}
