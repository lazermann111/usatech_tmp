/*
 * Created on May 9, 2005
 *
 */
package com.usatech.esuds.server.filters;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.util.SecurityUtil;
import com.usatech.esuds.server.util.SecurityUtil.DomainSubdomain;
import com.usatech.server.persistence.SubdomainName;
import com.usatech.server.persistence.SubdomainNamePeer;

public class SubdomainFeatureFilter implements Filter {
    private static Log log = LogFactory.getLog(SubdomainFeatureFilter.class);
    public static final String CSS_PATH_ATTRIBUTE = "CUSTOM_CSS_PATH";
    public static final String FEATURES_ATTRIBUTE = "CUSTOM_FEATURES";   
    
    protected static class FeatureCache {
        public Map features;
        public String cssPath;
    }
    
    public SubdomainFeatureFilter() {
        super();
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        if(log.isDebugEnabled()) log.debug("Filtered request with parameters: " + request.getParameterMap());
        
        //get the css path from the subdomain
        if(request instanceof HttpServletRequest) {  
            try {
                SecurityUtil.DomainSubdomain ds = SecurityUtil.parseUrl((HttpServletRequest)request);
                HttpSession session = ((HttpServletRequest)request).getSession();
                String key = ds.domain + "." + ds.subdomain;
                FeatureCache cache = (FeatureCache)session.getAttribute(key);
                if(cache == null) {
                    cache = new FeatureCache();
                    session.setAttribute(key, cache);
                    cache.cssPath = getCSSPath(ds);
                    cache.features = getFeatures(ds);
                }               
                request.setAttribute(CSS_PATH_ATTRIBUTE, cache.cssPath);
                request.setAttribute(FEATURES_ATTRIBUTE, cache.features);
            } catch(Exception e) {
                log.warn("Could not parse url", e);
            }
        }
        chain.doFilter(request, response);
    }

    private Map getFeatures(DomainSubdomain ds) {
        Map features = new HashMap();
        try {           
            Connection conn = Torque.getConnection();
            try {
                PreparedStatement ps = conn.prepareStatement(
                        "SELECT F.FEATURE_CD, SF.FEATURE_VALUE " +
                        "FROM APP_USER.SUBDOMAIN_FEATURE SF, APP_USER.FEATURE F " +
                        "WHERE SF.FEATURE_ID = F.FEATURE_ID " +
                        "AND SF.DOMAIN_NAME_URL = ? " +
                        "AND SF.SUBDOMAIN_NAME_CD = ? ");
                ps.setString(1, ds.domain);
                ps.setString(2, ds.subdomain);
                ResultSet rs = ps.executeQuery();
                while(rs.next()) features.put(rs.getString(1), rs.getString(2));
            } finally {
                conn.close();
            }
        } catch(Exception e) {
            log.error("While loading features", e);
        }
        return features;
    }

    protected String getCSSPath(SecurityUtil.DomainSubdomain ds) {
        SubdomainName subdomain = null;
        try {
            subdomain = SubdomainNamePeer.retrieveByPK(ds.subdomain, ds.domain);
        } catch (TorqueException te) {
            log.info("Error retrieving sub domain css path: " + te);
            return null;
        }
        return subdomain.getSubdomainCssUrl();
    }
    
    public void destroy() {
    }

}
