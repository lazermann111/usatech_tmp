package com.usatech.esuds.server.studentaccess.forms;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class AcctInfoForm extends ActionForm
{
	long studentId = -1;
	long accountId;
	boolean active = false;
	String notifyEmailAddr;
	String startDate;
	String endDate;
		
	/**
	* Constructor
	*/
	public AcctInfoForm()
	{

		super();

	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		studentId = -1;
		accountId = 0;
		boolean active = false;
		notifyEmailAddr = "";
		startDate = "";
		endDate="";

	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		
		if (notifyEmailAddr != null 
			&& !notifyEmailAddr.equals("") 
		    && (notifyEmailAddr.indexOf('@' ) == -1
			|| notifyEmailAddr.indexOf('.') == -1  
			|| notifyEmailAddr.indexOf(' ') != -1
			|| notifyEmailAddr.length() < 7))
		{
			errors.add("emailAddress", new ActionError("error.email.address.invalid"));
		}
		
		if (startDate != null && !startDate.equals(""))
		{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
			try
			{
				format.parse(startDate);
			}
			catch (ParseException e)
			{
				errors.add("startDate", new ActionError("error.start.date.invalid"));
			}
		}
		
		if (endDate != null && !endDate.equals(""))
		{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
			try
			{
				format.parse(endDate);
			}
			catch (ParseException e)
			{
				errors.add("endDate", new ActionError("error.end.date.invalid"));
			}
		}
		
		return errors;
	}
	
	/**
	 * Returns the notifyEmailAddr.
	 * @return String
	 */
	public String getNotifyEmailAddr()
	{
		return notifyEmailAddr;
	}

	/**
	 * Sets the notifyEmailAddr.
	 * @param notifyEmailAddr The notifyEmailAddr to set
	 */
	public void setNotifyEmailAddr(String notifyEmailAddr)
	{
		this.notifyEmailAddr = notifyEmailAddr;
	}

	/**
	 * Returns the studentId.
	 * @return long
	 */
	public long getStudentId()
	{
		return studentId;
	}

	/**
	 * Sets the studentId.
	 * @param id The id to set
	 */
	public void setStudentId(long id)
	{
		studentId = id;
	}
	
	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string)
	{
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string)
	{
		startDate = string;
	}

	/**
	 * @return
	 */
	public long getAccountId()
	{
		return accountId;
	}

	/**
	 * @param l
	 */
	public void setAccountId(long l)
	{
		accountId = l;
	}

	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}

}
