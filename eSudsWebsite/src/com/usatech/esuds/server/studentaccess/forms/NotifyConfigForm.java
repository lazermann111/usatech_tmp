package com.usatech.esuds.server.studentaccess.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * 
 * @version 1.0
 * @author
 */
public class NotifyConfigForm extends ActionForm {
    protected String primaryEmailAddr;

    protected String passCode;

    protected String notifyEmailAddr;

    protected boolean notifyOn;

    protected long hostStatusNotifyTypeId;

    protected long studentId;
    
    protected String startDate;
    
    protected String endDate;
    
    
    /**
     * Constructor
     */
    public NotifyConfigForm() {
        super();
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        primaryEmailAddr = "";
        passCode = "";
        hostStatusNotifyTypeId = 0;
        notifyEmailAddr = "";
        notifyOn = true;
        studentId = -1;
		startDate = "";
		endDate="";
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (notifyEmailAddr != null
                && !notifyEmailAddr.equals("")
                && (notifyEmailAddr.indexOf('@') == -1 || notifyEmailAddr.indexOf('.') == -1
                        || notifyEmailAddr.indexOf(' ') != -1 || notifyEmailAddr.length() < 7)) {
            errors.add("notifyEmailAddr", new ActionError("error.email.address.invalid"));
        }
        return errors;
    }

    /**
     * Returns the notifyEmailAddr.
     * 
     * @return String
     */
    public String getNotifyEmailAddr() {
        return notifyEmailAddr;
    }

    /**
     * Sets the notifyEmailAddr.
     * 
     * @param notifyEmailAddr
     *            The notifyEmailAddr to set
     */
    public void setNotifyEmailAddr(String notifyEmailAddr) {
        this.notifyEmailAddr = notifyEmailAddr;
    }

    /**
     * @return Returns the passCode.
     */
    public String getPassCode() {
        return passCode;
    }

    /**
     * @param passCode The passCode to set.
     */
    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    /**
     * @return Returns the primaryEmailAddr.
     */
    public String getPrimaryEmailAddr() {
        return primaryEmailAddr;
    }

    /**
     * @param primaryEmailAddr The primaryEmailAddr to set.
     */
    public void setPrimaryEmailAddr(String primaryEmailAddr) {
        this.primaryEmailAddr = primaryEmailAddr;
    }

    /**
     * @return Returns the notifyOn.
     */
    public boolean isNotifyOn() {
        return notifyOn;
    }

    /**
     * @param notifyOn The notifyOn to set.
     */
    public void setNotifyOn(boolean notifyOn) {
        this.notifyOn = notifyOn;
    }

    /**
     * @return Returns the hostStatusNotifyTypeId.
     */
    public long getHostStatusNotifyTypeId() {
        return hostStatusNotifyTypeId;
    }

    /**
     * @param hostStatusNotifyTypeId The hostStatusNotifyTypeId to set.
     */
    public void setHostStatusNotifyTypeId(long hostStatusNotifyTypeId) {
        this.hostStatusNotifyTypeId = hostStatusNotifyTypeId;
    }

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

}
