package com.usatech.esuds.server.studentaccess.actions;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.TransactionListBean;
import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.School;
import com.usatech.esuds.server.model.Student;
import com.usatech.esuds.server.model.StudentAccount;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.studentaccess.forms.AcctInfoForm;
import com.usatech.esuds.server.util.DateUtil;
import com.usatech.server.persistence.Location;

/**
 * @version 1.0
 * @author
 */
public class ShowAcctInfoAction extends Action {
    private static Log log = LogFactory.getLog(ShowAcctInfoAction.class);

    private static final String OBJECT_TYPE = SecurityService.STUDENT;

    private static final String ACTION = SecurityService.READ;

    /**
     * Constructor
     */
    public ShowAcctInfoAction() {

        super();

    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        AcctInfoForm acctForm = (AcctInfoForm) form;
        long studentId = acctForm.getStudentId();

        try {
            if (studentId == -1) {
                // get studentIds for user
                List ids = SecurityService.getAppObjectIds(request, OBJECT_TYPE);

                // right now, we don't have a list, so get the first one
                if(ids.isEmpty()) {
                    log.warn("User does not have an active student account");
                    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.no.active.account"));               
                } else {
                    studentId = Long.parseLong((String) ids.get(0));
                }
            }
            if (studentId != -1) {               
                // check credentials
                if (SecurityService.checkCredentials(request, OBJECT_TYPE, studentId, ACTION)) {
                    // get student
                    Student student = Student.retrieveStudent(studentId);
                    BeanUtils.copyProperties(acctForm, student);
                    request.getSession().setAttribute("student", student);
                    processShowAccounts(student, acctForm, request, errors);
                } else {
                    throw new NotPermittedException("Not permitted to access this resource.");
                }
            }
        } catch (UnknownObjectException uoe) {
            log.warn("Getting student account info", uoe);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown.student"));
        } catch (DataAccessException de) {
            log.warn("Getting student account info", de);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
        } catch (ParseException pe) {
            log.warn("Getting student account info", pe);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.parse.date"));
        } catch (Exception e) {
            log.warn("Getting student account info", e);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);

            // Forward control to the appropriate 'failure' URI (change name as
            // desired)
            forward = mapping.findForward("failure");

        } else {
            // Forward control to the appropriate 'success' URI (change name as
            // desired)
            forward = mapping.findForward("success");
        }

        // Finish with
        return (forward);

    }
    
    public static void processShowAccounts(Student student, AcctInfoForm acctForm, 
            HttpServletRequest request, ActionErrors errors) 
    		throws UnknownObjectException, DataException, ParseException, 
    		IllegalAccessException, InvocationTargetException {
        String startString = acctForm.getStartDate();
        String endString = acctForm.getEndDate();
        // get account information
        //CHANGED this to handle multiple accounts per student (BSK - 11/19/04)
        List accounts = student.getStudentAccounts();
        acctForm.setActive(false);
        // get transactions for this account
        if (accounts.size() > 0) {
            TransactionListBean tranList = new TransactionListBean();
            List trans = new ArrayList();
            if (startString == null || startString.equals("")) {
                for (Iterator iter = accounts.iterator(); iter.hasNext();) {
                    StudentAccount account = (StudentAccount)iter.next();
                    trans.addAll(account.retrieveAccountActivity());
                    if(account.isActive()) acctForm.setActive(true);
                }
            } else {
                GregorianCalendar from = DateUtil.parseDate(acctForm.getStartDate());
                from.set(GregorianCalendar.HOUR_OF_DAY, 0);
                from.set(GregorianCalendar.MINUTE, 0);
                from.set(GregorianCalendar.SECOND, 0);
                Date startDate = from.getTime();

                Date endDate = null;
                if (endString == null || endString.equals("")) {
                    // if endString is empty, assume now
                    endDate = new Date();
                } else {
                    GregorianCalendar to = DateUtil.parseDate(acctForm.getEndDate());
                    to.set(GregorianCalendar.HOUR_OF_DAY, 23);
                    to.set(GregorianCalendar.MINUTE, 59);
                    to.set(GregorianCalendar.SECOND, 59);
                    endDate = to.getTime();
                }

                for (Iterator iter = accounts.iterator(); iter.hasNext();) {
                    StudentAccount account = (StudentAccount)iter.next();
                    trans.addAll(account.retrieveAccountActivity(startDate, endDate));
                    if(account.isActive()) acctForm.setActive(true);
                }
            }
            tranList.setTransactions(trans);
            tranList.calculateTotalAmount();
            request.getSession().setAttribute("tranList", tranList);
            
            Map roomStatusParams = new HashMap();
            request.getSession().setAttribute("roomStatusParams", roomStatusParams);
            try {
                //Figure out the location for the room status link - BSK 03/11/2005
                Location location = ((StudentAccount)accounts.get(0)).getLocation();
                String typeDesc = location.getLocationType().getLocationTypeDesc();
                if(Campus.LOCATION_TYPE_DESC.equals(typeDesc)) {
                    roomStatusParams.put("campusId", "" + location.getLocationId());
                } else if(School.LOCATION_TYPE_DESC.equals(typeDesc)) {
                    roomStatusParams.put("schoolId", "" + location.getLocationId());
                } else if(Dorm.LOCATION_TYPE_DESC.equals(typeDesc)) {
                    roomStatusParams.put("dormId", "" + location.getLocationId());
                } else if(LaundryRoom.LOCATION_TYPE_DESC.equals(typeDesc)) {
                    roomStatusParams.put("roomId", "" + location.getLocationId());
                }
            } catch (TorqueException te) {
                log.warn("Could not get the account location for student '" + student.getEmailAddr() + "'", te);
            }            
        } else {
            errors.add("no.active.account", new ActionError("error.no.active.account"));
        }
    }
}