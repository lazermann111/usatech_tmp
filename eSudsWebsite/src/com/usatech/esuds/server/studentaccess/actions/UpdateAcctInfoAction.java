package com.usatech.esuds.server.studentaccess.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatech.esuds.server.model.Student;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.studentaccess.forms.AcctInfoForm;

/**
 * @version 1.0
 * @author
 */
public class UpdateAcctInfoAction extends Action {
    private static Log log = LogFactory.getLog(UpdateAcctInfoAction.class);

    private static final String OBJECT_TYPE = SecurityService.STUDENT;

    private static final String ACTION = SecurityService.MODIFY;

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        AcctInfoForm acctForm = (AcctInfoForm) form;
        long studentId = acctForm.getStudentId();

        if (SecurityService.checkCredentials(request, OBJECT_TYPE, studentId, ACTION)) {
            try {
                // get student
                Student student = Student.retrieveStudent(studentId);
                // copy properties from form
                BeanUtils.copyProperties(student, acctForm);
                // save changes
                student.save();
                ShowAcctInfoAction.processShowAccounts(student, acctForm, request, errors);
            } catch (UnknownObjectException uoe) {
                errors.add("unknown.student", new ActionError("error.unknown.student"));
            } catch (DataAccessException de) {
                errors.add("data.access", new ActionError("error.data.access"));
            } catch (Exception e) {
                errors.add("unknown.error", new ActionError("error.unknown"));
            }

            if (!errors.isEmpty()) {
                saveErrors(request, errors);
                // Forward control to the appropriate 'failure' URI (change name as desired)
                forward = mapping.findForward("failure");
            } else {
                // Forward control to the appropriate 'success' URI (change name as desired)
                forward = mapping.findForward("success");
            }
        } else {
            throw new NotPermittedException("Not permitted to access this resource.");
        }

        return (forward);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.usatech.esuds.server.security.actions.ProtectedAction#getName()
     */
    protected String getName() {
        return "STUDENT_RECORD";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.usatech.esuds.server.security.actions.ProtectedAction#getAction()
     */
    protected String getAction() {
        return "read, modify";
    }
}