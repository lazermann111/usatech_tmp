package com.usatech.esuds.server.studentaccess.actions;

import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.util.MessageResources;
import org.apache.torque.Torque;

import com.usatech.esuds.server.filters.SubdomainFeatureFilter;
import com.usatech.esuds.server.model.Student;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.studentaccess.forms.AcctInfoForm;
import com.usatech.esuds.server.studentaccess.forms.NotifyConfigForm;
import com.usatech.esuds.server.util.DateUtil;
import com.usatech.security.jaas.AppCredential;

/**
 * @version 1.0
 * @author
 */
public class NotifyConfigAction extends DispatchAction {
    private static Log log = LogFactory.getLog(NotifyConfigAction.class);

    /**
     * Constructor
     */
    public NotifyConfigAction() {
        super();
    }

    public ActionForward start(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionForward forward;
        
        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            forward = mapping.findForward("start");
        }

        return (forward);
    }
    
    public ActionForward get(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        NotifyConfigForm ncForm = (NotifyConfigForm) form;
        // calculate the current values of notif email and notif on from database based on primary email address & pass code
        Connection conn = Torque.getConnection(); 
        long studentId = -1;
        try {
            CallableStatement cs = conn.prepareCall("{call PSS.PKG_CONSUMER_MAINT.GET_CONSUMER_NOTIF(?,?,?,?,?,?,?,?)}");
            try {
                cs.setString(1, ncForm.getPrimaryEmailAddr());
                cs.setLong(2, ncForm.getHostStatusNotifyTypeId());
                cs.setString(3, ncForm.getPassCode());
                cs.setString(4, "NOTIF_CONFIG");
                cs.registerOutParameter(5, Types.NUMERIC); //consumer_id
                cs.registerOutParameter(6, Types.CHAR); //l_expired
                cs.registerOutParameter(7, Types.VARCHAR); //l_notify_email_addr 
                cs.registerOutParameter(8, Types.CHAR); //l_notify_on 
                cs.execute();
                studentId = cs.getLong(5);
                String expired = cs.getString(6);
                if(expired != null && "Y".equals(expired.trim())) {
                    log.warn("Pass Code '" + ncForm.getPassCode() + "' for account '" + ncForm.getPrimaryEmailAddr() + "' has expired");
                    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.expired.passcode", 
                            getHrefBase(request) + "/notifyConfig.do?" +
                                "action=request&" +
                                "primaryEmailAddr=" + URLEncoder.encode(ncForm.getPrimaryEmailAddr())));                                                   
                } else {
                    ncForm.setNotifyEmailAddr(cs.getString(7));
                    String notifyOn = cs.getString(8);
                    ncForm.setNotifyOn(notifyOn != null && "Y".equals(notifyOn.trim()));
                    log.debug("NOFITY_ON=" + ncForm.isNotifyOn() + "; from db=" + notifyOn.trim());
                    if(ncForm.getNotifyEmailAddr() == null || ncForm.getNotifyEmailAddr().trim().length() == 0) {
                        ncForm.setNotifyEmailAddr(ncForm.getPrimaryEmailAddr());
                    }
                    ncForm.setStudentId(studentId);
                }
            } catch(SQLException e) {
                if(e.getErrorCode() == 1403) { // no data found
                    log.warn("Pass Code '" + ncForm.getPassCode() + "' for account '" + ncForm.getPrimaryEmailAddr() + "' is not valid");
                    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalid.passcode"));                                                   
                } else {
                	log.warn("Got Error Code " + e.getErrorCode());
                    throw e;
                }
            } finally {
                try { cs.close(); } catch(SQLException sqle) {}
            }
        } catch(SQLException e) {
            log.warn("While finding notif config data", e);
            errors.add("data.access", new ActionError("error.data.access"));
        } finally {
            try { conn.close(); } catch(SQLException sqle) {}
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            return mapping.findForward("failure");
        } else {
        	Map features = (Map)request.getAttribute(SubdomainFeatureFilter.FEATURES_ATTRIBUTE);
        	if(features != null && features.get("STUDENT_ACCESS") != null && features.get("STUDENT_ACCESS").equals("NOTIFY_USAGE")) {
        		// half login with only student credential
        		Subject subject = new Subject();
        		AppCredential credential = new AppCredential();
        		credential.setApp(SecurityService.getApp(request));
        		credential.setObjectId(studentId);
        		credential.setObjectType(SecurityService.STUDENT);
        		credential.setActions(SecurityService.READ);
        		subject.getPublicCredentials().add(credential);
        		request.getSession().setAttribute("subject", subject);
        		//DatabaseLoginModule.populateSubject(subject, userId, null);
        		populateAcctInfo(studentId, ncForm, request, errors);
        		if (!errors.isEmpty()) {
    	            saveErrors(request, errors);
    	            return mapping.findForward("failure");
    	        } else {
    	        	return mapping.findForward("edit");
    	        }
        	}
            return mapping.findForward("edit");
        }
    }

    public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	Subject subject = (Subject)request.getSession().getAttribute("subject");
    	NotifyConfigForm ncForm = (NotifyConfigForm) form;
        long studentId = ncForm.getStudentId();
        if(subject == null || studentId == -1) {
    		return get(mapping, form, request, response);
    	} else {
    		if (SecurityService.checkCredentials(request, SecurityService.STUDENT, studentId, SecurityService.READ)) {
    			ActionErrors errors = new ActionErrors();
    	        populateAcctInfo(studentId, ncForm, request, errors);
    	        if (!errors.isEmpty()) {
    	            saveErrors(request, errors);
    	            return mapping.findForward("failure");
    	        } else {
    	        	return mapping.findForward("edit");
    	        }
    		} else {
    			throw new NotPermittedException("Not permitted to access this resource.");
    		}    		
    	}
    }
    
    protected void populateAcctInfo(long studentId, NotifyConfigForm ncForm, HttpServletRequest request, ActionErrors errors) {
    	AcctInfoForm acctForm = new AcctInfoForm(); 
    	try {
    		if(ncForm.getStartDate() == null || ncForm.getStartDate().trim().length() == 0) {
        		Calendar calendar = Calendar.getInstance();
        		if(ncForm.getEndDate() != null && ncForm.getEndDate().trim().length() > 0) {
        			calendar.setTime(DateUtil.parseDate(ncForm.getEndDate()).getTime());
        		} 
        		calendar.add(Calendar.MONTH, -1);
        		ncForm.setStartDate(DateUtil.convertDate(calendar.getTime()));
        	}
    		acctForm.setStartDate(ncForm.getStartDate());
    		acctForm.setEndDate(ncForm.getEndDate());
			Student student = Student.retrieveStudent(studentId);
            BeanUtils.copyProperties(acctForm, student);
            ShowAcctInfoAction.processShowAccounts(student, acctForm, request, errors);
    		request.getSession().setAttribute("student", student);
        } catch (UnknownObjectException uoe) {
            log.warn("Getting student account info", uoe);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown.student"));
        } catch (DataAccessException de) {
            log.warn("Getting student account info", de);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
        } catch (ParseException pe) {
            log.warn("Getting student account info", pe);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.parse.date"));
        } catch (Exception e) {
            log.warn("Getting student account info", e);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
        }
	}

	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        //ActionMessages messages = new ActionMessages();
        ActionForward forward;
        NotifyConfigForm ncForm = (NotifyConfigForm) form;
        Connection conn = Torque.getConnection();        
        try {
            CallableStatement cs = conn.prepareCall("{call PSS.PKG_CONSUMER_MAINT.GET_CONSUMER_NOTIF(?,?,?,?,?,?,?,?)}");
            try {
                cs.setString(1, ncForm.getPrimaryEmailAddr());
                cs.setLong(2, ncForm.getHostStatusNotifyTypeId());
                cs.setString(3, ncForm.getPassCode());
                cs.setString(4, "NOTIF_CONFIG");
                cs.registerOutParameter(5, Types.NUMERIC); //consumer_id
                cs.registerOutParameter(6, Types.CHAR); //l_expired
                cs.registerOutParameter(7, Types.VARCHAR); //l_notify_email_addr 
                cs.registerOutParameter(8, Types.CHAR); //l_notify_on 
                cs.execute();
                long studentId = cs.getLong(5);
                String expired = cs.getString(6);
                if(expired != null && "Y".equals(expired.trim())) {
                    log.warn("Pass Code '" + ncForm.getPassCode() + "' for account '" + ncForm.getPrimaryEmailAddr() + "' has expired");
                    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.expired.passcode", 
                            getHrefBase(request) + "/notifyConfig.do?" +
                                "action=request&" +
                                "primaryEmailAddr=" + URLEncoder.encode(ncForm.getPrimaryEmailAddr())));                                                   
                } else {
                    PreparedStatement ps1 = conn.prepareStatement("{call PSS.PKG_CONSUMER_MAINT.CONSUMER_NOTIF_UPD(?,?,?,?)}");
                    try {
                        ps1.setLong(1, studentId);
                        ps1.setLong(2, ncForm.getHostStatusNotifyTypeId());
                        ps1.setString(3, ncForm.isNotifyOn() ? "Y" : "N");
                        ps1.setString(4, ncForm.getNotifyEmailAddr());
                        ps1.executeUpdate();
                    } catch(SQLException e) {
                        log.warn("While updating notif config data", e);
                        errors.add("data.access", new ActionError("error.data.access"));
                    } finally {
                        try { ps1.close(); } catch(SQLException sqle) {}
                    }
                    if(errors.isEmpty()) {
                        ps1 = conn.prepareStatement("{call PSS.PKG_CONSUMER_MAINT.EXPIRE_CONSUMER_PASSCODE(?)}");
                        try {
                            ps1.setString(1, ncForm.getPassCode());
                            ps1.executeUpdate();
                        } catch(SQLException e) {
                            log.warn("While updating passcode expiration", e);
                            errors.add("data.access", new ActionError("error.data.access"));
                        } finally {
                            try { ps1.close(); } catch(SQLException sqle) {}
                        }
                    }
                    if(errors.isEmpty()) {
                        conn.commit();
                    } else {
                        conn.rollback();                            
                    }
                }
            } catch(SQLException e) {
                if(e.getErrorCode() == 100) { // no data found
                    log.warn("Pass Code '" + ncForm.getPassCode() + "' for account '" + ncForm.getPrimaryEmailAddr() + "' is not valid");
                    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalid.passcode"));                                                   
                } else {
                    throw e;
                }
            } finally {
                try { cs.close(); } catch(SQLException sqle) {}
            }
        } catch(SQLException e) {
            log.warn("While finding notif config data", e);
            errors.add("data.access", new ActionError("error.data.access"));
        } finally {
            try { conn.close(); } catch(SQLException sqle) {}
        }
        
        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            forward = mapping.findForward("success");
        }

        return (forward);
    }
    
    public ActionForward request(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward;
        NotifyConfigForm ncForm = (NotifyConfigForm) form;
        // calculate the current values of notif email and notif on from database based on primary email address & pass code
        Connection conn = Torque.getConnection();        
        try {
            Student student = Student.retrieveByEmail(ncForm.getPrimaryEmailAddr());
            if(student == null) { //no data found
                log.warn("Account '" + ncForm.getPrimaryEmailAddr() + "' is not valid");
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalid.account"));                                                      
            } else {
                CallableStatement cs = conn.prepareCall("{? = call PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(?,?)}");
                String passcode;
                try {
                    cs.registerOutParameter(1, Types.VARCHAR);
                    cs.setLong(2, student.getStudentId());
                    cs.setString(3, "NOTIF_CONFIG");
                    cs.executeUpdate();
                    passcode = cs.getString(1);
                } finally {
                    try { cs.close(); } catch(SQLException sqle) {}
                }
                //create email
                String href = getHrefBase(request) + "/notifyConfig.do?" +
                        "action=get&" +
                        "primaryEmailAddr=" + URLEncoder.encode(ncForm.getPrimaryEmailAddr()) + "&" +
                        "hostStatusNotifyTypeId=2&" + 
                        "passCode=" + URLEncoder.encode(passcode);
                //String toName = student.getFirstName() + " " + student.getLastName();
                try {
                    sendNotifRequestEmail(conn, request, href, student);
                } catch(SQLException e) {
                    log.warn("While adding email to queue", e);
                    errors.add("data.access", new ActionError("error.data.access"));                        
                }
                if(errors.isEmpty()) {
                    conn.commit();
                } else {
                    conn.rollback();                            
                }
            }
        } catch(SQLException e) {
            log.warn("While creating passcode", e);
            errors.add("data.access", new ActionError("error.data.access"));
        } finally {
            try { conn.close(); } catch(SQLException sqle) {}
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            forward = mapping.findForward("requested");
        }

        return (forward);
    }

    protected String getHrefBase(HttpServletRequest request) {
        StringBuffer sb = request.getRequestURL();
        int pos = sb.toString().lastIndexOf("/");
        if(pos >= 0) sb.setLength(pos);
        return sb.toString();
    }
    
    protected void sendNotifRequestEmail(Connection conn, HttpServletRequest request, String href, Student student) throws SQLException {
        HttpSession session = request.getSession();
        /*MessageResources resources = (MessageResources) request.getAttribute(Globals.MESSAGES_KEY);
        if(resources == null) {
            resources = (MessageResources) session.getAttribute(Globals.MESSAGES_KEY);
            if(resources == null) resources = (MessageResources) session.getServletContext().getAttribute(Globals.MESSAGES_KEY);
        }
        //*/
        MessageResources resources = (MessageResources) session.getServletContext().getAttribute(Globals.MESSAGES_KEY);        
        Locale userLocale = (Locale) session.getAttribute(Globals.LOCALE_KEY);
        if(log.isDebugEnabled()) log.debug("Got resources " + resources.getConfig() + " (" + resources + ") and locale=" + userLocale);
        String body = resources.getMessage(userLocale, "msg.notify.request.email.body", href, student.getEmailAddr(), student.getFirstName(), student.getLastName());
        String subject = resources.getMessage(userLocale, "msg.notify.request.email.subject");
        String fromAddr = resources.getMessage(userLocale, "msg.notify.request.email.from.addr");
        String fromName = resources.getMessage(userLocale, "msg.notify.request.email.from.name");
        sendEmail(conn, body, (student.getFirstName() + " " + student.getLastName()).trim(), student.getEmailAddr(), subject, fromAddr, fromName);       
    }
    
    protected void sendEmail(Connection conn, String body, String toName, String toAddr, String subject, String fromAddr, String fromName) throws SQLException {
        if(log.isDebugEnabled()) {
            log.debug("Creating email with to=" + toAddr + " (" + toName + "); from=" + fromAddr + " (" + fromName + "); subject=" + subject + "; body=" + body);
        }
        PreparedStatement ps = conn.prepareStatement(
                "INSERT INTO ob_email_queue(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG) " +
                "VALUES(?,?,?,?,?,?)");
        try {
            ps.setString(1, fromAddr);
            ps.setString(2, fromName);
            ps.setString(3, toAddr);
            ps.setString(4, toName);
            ps.setString(5, subject);
            ps.setString(6, body);
            ps.executeUpdate();
        } finally {
            try { ps.close(); } catch(SQLException e) {}
        }
    }
}