package com.usatech.esuds.server.beans;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

/**
 * @author rothma
 *
 */
public class TotalUsageReportBean
{
	private long id;
	private String name;
	private BigDecimal totAmount;
	private long totCycles;
	private long totDeterg;
	private long totFabricSoft;
	private long totDryCycles;
	private long totWashCycles;
	private BigDecimal totWashAmount;
	private BigDecimal totDryAmount;
	private Date transTS; 
	
	//For Navigation
	private HashMap params = null;

	/**
	 * Constructor for TotalUsageReportBean.
	 */
	public TotalUsageReportBean()
	{
		super();
	}

	/**
	 * Returns the id.
	 * @return int
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Returns the name.
	 * @return String
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Returns the totAmount.
	 * @return BigDecimal
	 */
	public BigDecimal getTotAmount()
	{
		return totAmount;
	}

	/**
	 * Returns the totCycles.
	 * @return long
	 */
	public long getTotCycles()
	{
		return totCycles;
	}

	/**
	 * Returns the totDetrg.
	 * @return long
	 */
	public long getTotDeterg()
	{
		return totDeterg;
	}

	/**
	 * Returns the totFabricSoft.
	 * @return long
	 */
	public long getTotFabricSoft()
	{
		return totFabricSoft;
	}

	/**
	 * Returns the transTS.
	 * @return Date
	 */
	public Date getTransTS()
	{
		return transTS;
	}

	/**
	 * Sets the id.
	 * @param id The id to set
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * Sets the name.
	 * @param name The name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the totAmount.
	 * @param totAmount The totAmount to set
	 */
	public void setTotAmount(BigDecimal totAmount)
	{
		this.totAmount = totAmount;
	}

	/**
	 * Sets the totCycles.
	 * @param totCycles The totCycles to set
	 */
	public void setTotCycles(long totCycles)
	{
		this.totCycles = totCycles;
	}

	/**
	 * Sets the totDetrg.
	 * @param totDetrg The totDetrg to set
	 */
	public void setTotDeterg(long totDeterg)
	{
		this.totDeterg = totDeterg;
	}

	/**
	 * Sets the totFabricSoft.
	 * @param totFabricSoft The totFabricSoft to set
	 */
	public void setTotFabricSoft(long totFabricSoft)
	{
		this.totFabricSoft = totFabricSoft;
	}

	/**
	 * Sets the transTS.
	 * @param transTS The transTS to set
	 */
	public void setTransTS(Date transTS)
	{
		this.transTS = transTS;
	}

	/**
	 * Returns the params.
	 * @return HashMap
	 */
	public HashMap getParams()
	{
		return params;
	}

	/**
	 * Sets the params.
	 * @param params The params to set
	 */
	public void setParams(HashMap params)
	{
		this.params = params;
	}

	/**
	 * @return
	 */
	public BigDecimal getTotDryAmount()
	{
		return totDryAmount;
	}

	/**
	 * @return
	 */
	public long getTotDryCycles()
	{
		return totDryCycles;
	}

	/**
	 * @return
	 */
	public BigDecimal getTotWashAmount()
	{
		return totWashAmount;
	}

	/**
	 * @return
	 */
	public long getTotWashCycles()
	{
		return totWashCycles;
	}

	/**
	 * @param decimal
	 */
	public void setTotDryAmount(BigDecimal decimal)
	{
		totDryAmount = decimal;
	}

	/**
	 * @param l
	 */
	public void setTotDryCycles(long l)
	{
		totDryCycles = l;
	}

	/**
	 * @param decimal
	 */
	public void setTotWashAmount(BigDecimal decimal)
	{
		totWashAmount = decimal;
	}

	/**
	 * @param l
	 */
	public void setTotWashCycles(long l)
	{
		totWashCycles = l;
	}

}
