/*
 * *******************************************************************
 * 
 * File TransactionListBean.java
 * 
 * Created on Jun 18, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

import java.math.BigDecimal;
import java.util.List;

import com.usatech.esuds.server.util.AccountUtil;

/**
 * @author melissa
 *  
 */
public class TransactionListBean {
    private List transactions = null;

    private BigDecimal totalAmount = null;

    /**
     * @return
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @return
     */
    public List getTransactions() {
        return transactions;
    }

    /**
     * @param decimal
     */
    public void setTotalAmount(BigDecimal decimal) {
        totalAmount = decimal;
    }

    /**
     * @param list
     */
    public void setTransactions(List list) {
        transactions = list;
    }

     public void calculateTotalAmount() {
        totalAmount = AccountUtil.getTotalAmount(transactions, true);
    }

}