/*
 * Created on Sep 8, 2004
 *
 */
package com.usatech.esuds.server.beans;

import java.math.BigDecimal;

/**
 * @author melissa
 *
 */
public class MachineUsageBean
{
	private String label;
	private String machineType;
	private long numCycles;
	private long numTopOffs;
	private BigDecimal cashAmount = BigDecimal.ZERO;
	private BigDecimal cardAmount = BigDecimal.ZERO;
	private BigDecimal totalAmount = BigDecimal.ZERO;

	/**
	 * @return
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @return
	 */
	public String getMachineType()
	{
		return machineType;
	}

	/**
	 * @return
	 */
	public long getNumCycles()
	{
		return numCycles;
	}

	/**
	 * @return
	 */
	public long getNumTopOffs()
	{
		return numTopOffs;
	}

	/**
	 * @return
	 */
	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}

	/**
	 * @param string
	 */
	public void setLabel(String string)
	{
		label = string;
	}

	/**
	 * @param string
	 */
	public void setMachineType(String string)
	{
		machineType = string;
	}

	/**
	 * @param l
	 */
	public void setNumCycles(long l)
	{
		numCycles = l;
	}

	/**
	 * @param l
	 */
	public void setNumTopOffs(long l)
	{
		numTopOffs = l;
	}

	/**
	 * @param decimal
	 */
	public void setTotalAmount(BigDecimal decimal)
	{
		totalAmount = decimal;
	}

	/**
	 * @return
	 */
	public BigDecimal getCardAmount()
	{
		return cardAmount;
	}

	/**
	 * @return
	 */
	public BigDecimal getCashAmount()
	{
		return cashAmount;
	}

	/**
	 * @param decimal
	 */
	public void setCardAmount(BigDecimal decimal)
	{
		cardAmount = decimal;
	}

	/**
	 * @param decimal
	 */
	public void setCashAmount(BigDecimal decimal)
	{
		cashAmount = decimal;
	}

}
