/*
 * Created on Mar 25, 2004
 *
 */
package com.usatech.esuds.server.beans;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author melissa
 *
 */
public class TransactionBean
{
	private Date date = null;
	private String description = null;
	private BigDecimal price = null;
 
	/**
	 * @return
	 */
	public Date getDate()
	{
		return date;
	}

	/**
	 * @return
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return
	 */
	public BigDecimal getPrice()
	{
		return price;
	}

	/**
	 * @param date
	 */
	public void setDate(Date date)
	{
		this.date = date;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string)
	{
		description = string;
	}

	/**
	 * @param string
	 */
	public void setPrice(BigDecimal _price)
	{
		price = _price;
	}

}
