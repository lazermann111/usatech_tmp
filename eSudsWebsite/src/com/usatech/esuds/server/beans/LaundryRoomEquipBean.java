/*
 * *******************************************************************
 * 
 * File LaundryRoomEquipBean.java
 * 
 * Created on Mar 29, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

public class LaundryRoomEquipBean
{
	private long laundryRoomEquipId;
	private String serialCd = null;
	private int portNum;
	private String position = null;
	private String label = null;
	private int statusIndicator;
	private int cycleMins; 
	
	/**
	 * @return
	 */
	public int getCycleMins()
	{
		return cycleMins;
	}

	/**
	 * @return
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @return
	 */
	public long getLaundryRoomEquipId()
	{
		return laundryRoomEquipId;
	}

	/**
	 * @return
	 */
	public int getPortNum()
	{
		return portNum;
	}

	/**
	 * @return
	 */
	public String getPosition()
	{
		return position;
	}

	/**
	 * @return
	 */
	public String getSerialCd()
	{
		return serialCd;
	}

	/**
	 * @return
	 */
	public int getStatusIndicator()
	{
		return statusIndicator;
	}

	/**
	 * @param i
	 */
	public void setCycleMins(int i)
	{
		cycleMins = i;
	}

	/**
	 * @param string
	 */
	public void setLabel(String string)
	{
		label = string;
	}

	/**
	 * @param l
	 */
	public void setLaundryRoomEquipId(long l)
	{
		laundryRoomEquipId = l;
	}

	/**
	 * @param i
	 */
	public void setPortNum(int i)
	{
		portNum = i;
	}

	/**
	 * @param string
	 */
	public void setPosition(String string)
	{
		position = string;
	}

	/**
	 * @param string
	 */
	public void setSerialCd(String string)
	{
		serialCd = string;
	}

	/**
	 * @param i
	 */
	public void setStatusIndicator(int i)
	{
		statusIndicator = i;
	}

}
