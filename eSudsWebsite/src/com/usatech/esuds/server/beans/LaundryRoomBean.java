/*
 * *******************************************************************
 * 
 * File LaundryRoomBean.java
 * 
 * Created on Mar 29, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

import java.util.HashMap;
import java.util.Map;

public class LaundryRoomBean
{
	private long laundryRoomId;
	private String name = null;
	private DormBean dormBean = null;
	private String action = null;
	
	/**
	 * @return
	 */
	public DormBean getDormBean()
	{
		return dormBean;
	}

	/**
	 * @return
	 */
	public long getLaundryRoomId()
	{
		return laundryRoomId;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param bean
	 */
	public void setDormBean(DormBean bean)
	{
		dormBean = bean;
	}

	/**
	 * @param l
	 */
	public void setLaundryRoomId(long l)
	{
		laundryRoomId = l;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}
	
	public Map getActionMap()
	{
		HashMap map = new HashMap();
		map.put("laundryRoomId", new Long(laundryRoomId));
		map.put("action", action);
		
		return map;
	}

}
