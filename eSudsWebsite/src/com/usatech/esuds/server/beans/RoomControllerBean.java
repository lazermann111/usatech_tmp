/*
 * Created on Feb 27, 2004
 * Created by melissa
 *
 */
package com.usatech.esuds.server.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * @author melissa
 *
 * 
 */
public class RoomControllerBean
{
	private String serialNumber = null;
	private long roomControllerId;
	private long posId;
	private String action = "show";
	
	/**
	 * @return
	 */
	public long getPosId()
	{
		return posId;
	}

	/**
	 * @return
	 */
	public long getRoomControllerId()
	{
		return roomControllerId;
	}

	/**
	 * @return
	 */
	public String getSerialNumber()
	{
		return serialNumber;
	}

	/**
	 * @param l
	 */
	public void setPosId(long l)
	{
		posId = l;
	}

	/**
	 * @param l
	 */
	public void setRoomControllerId(long l)
	{
		roomControllerId = l;
	}

	/**
	 * @param string
	 */
	public void setSerialNumber(String string)
	{
		serialNumber = string;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	public Map getActionMap()
	{
		HashMap map = new HashMap();
		map.put("posId", new Long(posId));
		map.put("action", action);
		
		return map;
	}
}
