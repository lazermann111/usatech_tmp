/*
 * *******************************************************************
 * 
 * File StudentBean.java
 * 
 * Created on March 24, 2004
 *
 * Create by Melissa Klein
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StudentBean
{
	long studentId;
	String firstName;
	String middleName;
	String lastName;
	String emailAddr ;
	String notifyEmailAddr;
	/**
	 * @return
	 */
	public String getEmailAddr()
	{
		return emailAddr;
	}

	/**
	 * @return
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @return
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @return
	 */
	public String getMiddleName()
	{
		return middleName;
	}

	/**
	 * @return
	 */
	public String getNotifyEmailAddr()
	{
		return notifyEmailAddr;
	}

	/**
	 * @return
	 */
	public long getStudentId()
	{
		return studentId;
	}

	/**
	 * @param string
	 */
	public void setEmailAddr(String string)
	{
		emailAddr = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string)
	{
		firstName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string)
	{
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setMiddleName(String string)
	{
		middleName = string;
	}

	/**
	 * @param string
	 */
	public void setNotifyEmailAddr(String string)
	{
		notifyEmailAddr = string;
	}

	/**
	 * @param l
	 */
	public void setStudentId(long l)
	{
		studentId = l;
	}

}
