package com.usatech.esuds.server.beans;
import java.util.List;

/*
 * Created on Sep 13, 2004
 *
 */

/**
 * @author melissa
 *
 */
public class CampusSummaryBean
{
	private String campusName;
	private String schoolName;
	private List rooms;

	/**
	 * @return
	 */
	public String getCampusName()
	{
		return campusName;
	}

	/**
	 * @return
	 */
	public List getRooms()
	{
		return rooms;
	}

	/**
	 * @return
	 */
	public String getSchoolName()
	{
		return schoolName;
	}

	/**
	 * @param string
	 */
	public void setCampusName(String string)
	{
		campusName = string;
	}

	/**
	 * @param list
	 */
	public void setRooms(List list)
	{
		rooms = list;
	}

	/**
	 * @param string
	 */
	public void setSchoolName(String string)
	{
		schoolName = string;
	}

}
