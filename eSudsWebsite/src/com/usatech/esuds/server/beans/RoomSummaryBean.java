/*
 * Created on Sep 13, 2004
 *
 */
package com.usatech.esuds.server.beans;

import java.util.List;

/**
 * @author melissa
 *
 */
public class RoomSummaryBean
{
	private String roomName;
	private String dormName;
	private List machines;
	private List machineTypeTotals;
	private List paymentTypeTotals;
	private UsageTotalBean grandTotal;
	
	/**
	 * @return
	 */
	public String getDormName()
	{
		return dormName;
	}

	/**
	 * @return
	 */
	public List getMachines()
	{
		return machines;
	}

	/**
	 * @return
	 */
	public String getRoomName()
	{
		return roomName;
	}

	/**
	 * @return
	 */
	public List getMachineTypeTotals()
	{
		return machineTypeTotals;
	}

	/**
	 * @param string
	 */
	public void setDormName(String string)
	{
		dormName = string;
	}

	/**
	 * @param list
	 */
	public void setMachines(List list)
	{
		machines = list;
	}

	/**
	 * @param string
	 */
	public void setRoomName(String string)
	{
		roomName = string;
	}

	/**
	 * @param list
	 */
	public void setMachineTypeTotals(List list)
	{
		machineTypeTotals = list;
	}

	/**
	 * @return
	 */
	public List getPaymentTypeTotals()
	{
		return paymentTypeTotals;
	}

	/**
	 * @param list
	 */
	public void setPaymentTypeTotals(List list)
	{
		paymentTypeTotals = list;
	}

	/**
	 * @return
	 */
	public UsageTotalBean getGrandTotal()
	{
		return grandTotal;
	}

	/**
	 * @param bean
	 */
	public void setGrandTotal(UsageTotalBean bean)
	{
		grandTotal = bean;
	}

}
