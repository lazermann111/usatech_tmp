/*
 * *******************************************************************
 * 
 * File CountryBean.java
 * 
 * Created on Apr 5, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

public class TimeZoneBean
{
	private String timeZoneCd = null;
	private String timeZoneName = null;
	
	public TimeZoneBean()
	{
	}
	/**
	 * @return
	 */
	public String getTimeZoneCd()
	{
		return timeZoneCd;
	}

	/**
	 * @return
	 */
	public String getTimeZoneName()
	{
		return timeZoneName;
	}

	/**
	 * @param string
	 */
	public void setTimeZoneCd(String string)
	{
		timeZoneCd = string;
	}

	/**
	 * @param string
	 */
	public void setTimeZoneName(String string)
	{
		timeZoneName = string;
	}

}
