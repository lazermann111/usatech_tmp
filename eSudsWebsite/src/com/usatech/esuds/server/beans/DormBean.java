/*
 * *******************************************************************
 * 
 * File DormBean.java
 * 
 * Created on Mar 29, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

import java.util.HashMap;
import java.util.Map;

public class DormBean
{
	private long dormId;
	private String name = null;
	private CampusBean campusBean = null;
	private String action = null;
	/**
	 * @return
	 */
	public CampusBean getCampusBean()
	{
		return campusBean;
	}

	/**
	 * @return
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param bean
	 */
	public void setCampusBean(CampusBean bean)
	{
		campusBean = bean;
	}

	/**
	 * @param l
	 */
	public void setDormId(long l)
	{
		dormId = l;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}
	
	public Map getActionMap()
	{
		HashMap map = new HashMap();
		map.put("dormId", new Long(dormId));
		map.put("action", action);
		
		return map;
	}

}
