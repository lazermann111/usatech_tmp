/*
 * *******************************************************************
 * 
 * File CountryBean.java
 * 
 * Created on Apr 5, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

public class CountryBean
{
	private String countryCd = null;
	private String countryName = null;
	
	public CountryBean()
	{
	}
	/**
	 * @return
	 */
	public String getCountryCd()
	{
		return countryCd;
	}

	/**
	 * @return
	 */
	public String getCountryName()
	{
		return countryName;
	}

	/**
	 * @param string
	 */
	public void setCountryCd(String string)
	{
		countryCd = string;
	}

	/**
	 * @param string
	 */
	public void setCountryName(String string)
	{
		countryName = string;
	}

}
