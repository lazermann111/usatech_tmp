/*
 * *******************************************************************
 * 
 * File StudentAccountBean.java
 * 
 * Created on March 25, 2004
 *
 * Create by Melissa Klein
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

public class StudentAccountBean
{
	long studentId;
	long accountId;
	boolean active = false;
	/**
	 * @return
	 */
	public long getAccountId()
	{
		return accountId;
	}

	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param l
	 */
	public void setAccountId(long l)
	{
		accountId = l;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}

	/**
	 * @return
	 */
	public long getStudentId()
	{
		return studentId;
	}

	/**
	 * @param l
	 */
	public void setStudentId(long l)
	{
		studentId = l;
	}

}
