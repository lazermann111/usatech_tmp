/*
 * *******************************************************************
 * 
 * File CountryBean.java
 * 
 * Created on Apr 5, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

public class StateBean
{
	private String stateCd = null;
	private String stateName = null;
	private String countryCd = null;
	
	public StateBean()
	{
	}
	/**
	 * @return
	 */
	public String getStateCd()
	{
		return stateCd;
	}

	/**
	 * @return
	 */
	public String getStateName()
	{
		return stateName;
	}

	/**
	 * @param string
	 */
	public void setStateCd(String string)
	{
		stateCd = string;
	}

	/**
	 * @param string
	 */
	public void setStateName(String string)
	{
		stateName = string;
	}

	/**
	 * @return
	 */
	public String getCountryCd()
	{
		return countryCd;
	}

	/**
	 * @param string
	 */
	public void setCountryCd(String string)
	{
		countryCd = string;
	}

}
