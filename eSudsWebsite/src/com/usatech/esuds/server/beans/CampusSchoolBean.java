/*
 * *******************************************************************
 * 
 * File CampusSchoolBean.java
 * 
 * Created on Apr 16, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

/**
 * @author melissa
 *
 * Bean for Campus with School Name
 */
public class CampusSchoolBean
{

	private long campusId;
	private String name = null;
	private String schoolName = null;
	private String action = null;
	
	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public String getSchoolName()
	{
		return schoolName;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param string
	 */
	public void setSchoolName(String string)
	{
		schoolName = string;
	}

}
