/*
 * Created on Sep 8, 2004
 *
 */
package com.usatech.esuds.server.beans;

import java.math.BigDecimal;

/**
 * @author melissa
 *
 */
public class UsageTotalBean
{
	private String description;
	private long totalCycles;
	private long totalTopOffs;
	private BigDecimal totalAmount;

	/**
	 * @return
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return
	 */
	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}

	/**
	 * @return
	 */
	public long getTotalCycles()
	{
		return totalCycles;
	}

	/**
	 * @return
	 */
	public long getTotalTopOffs()
	{
		return totalTopOffs;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string)
	{
		description = string;
	}

	/**
	 * @param decimal
	 */
	public void setTotalAmount(BigDecimal decimal)
	{
		totalAmount = decimal;
	}

	/**
	 * @param l
	 */
	public void setTotalCycles(long l)
	{
		totalCycles = l;
	}

	/**
	 * @param l
	 */
	public void setTotalTopOffs(long l)
	{
		totalTopOffs = l;
	}

}
