/*
 * *******************************************************************
 * 
 * File OperatorBean.java
 * 
 * Created on Apr 5, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

public class OperatorBean
{
	long operatorId;
	String name = null;
	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public long getOperatorId()
	{
		return operatorId;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param l
	 */
	public void setOperatorId(long l)
	{
		operatorId = l;
	}

}
