/*
 * *******************************************************************
 * 
 * File CampusBean.java
 * 
 * Created on Mar 26, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

import java.util.HashMap;
import java.util.Map;

public class CampusBean
{
	private long campusId;
	private String name = null;
	private SchoolBean schoolBean = null;
	private String action = null;
	
	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public SchoolBean getSchoolBean()
	{
		return schoolBean;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param bean
	 */
	public void setSchoolBean(SchoolBean bean)
	{
		schoolBean = bean;
	}
	
	public Map getActionMap()
	{
		HashMap map = new HashMap();
		map.put("campusId", new Long(campusId));
		map.put("action", action);
		return map;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

}
