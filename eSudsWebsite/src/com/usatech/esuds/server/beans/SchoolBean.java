/*
 * *******************************************************************
 * 
 * File SchoolBean.java
 * 
 * Created on Mar 26, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.beans;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SchoolBean
{
	private long schoolId;
	private String name = null;
	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public long getSchoolId()
	{
		return schoolId;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param l
	 */
	public void setSchoolId(long l)
	{
		schoolId = l;
	}

}
