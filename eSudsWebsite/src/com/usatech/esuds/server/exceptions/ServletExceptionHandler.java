/*
 * *******************************************************************
 * 
 * File ServletExceptionHandler.java
 * 
 * Created on Jun 22, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.exceptions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

/**
 * @author melissa
 *
 */
public class ServletExceptionHandler extends ExceptionHandler
{
	/**
	 * 
	 */
	public ServletExceptionHandler()
	{
		super();
	}
	
	public ActionForward execute(
		Exception e,
		ExceptionConfig ec,
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException
	{
		ActionErrors errors = new ActionErrors();
		
		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.generic"));
		
		return mapping.findForward("error");
	}
}
