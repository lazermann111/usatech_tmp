package com.usatech.esuds.server.security.services;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.security.auth.Policy;
import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.School;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.security.exceptions.FailedLogonException;
import com.usatech.esuds.server.security.exceptions.UserException;
import com.usatech.esuds.server.util.LocationUtil;
import com.usatech.esuds.server.util.SecurityUtil;
import com.usatech.security.jaas.AppCredential;
import com.usatech.security.jaas.NamePrincipal;
import com.usatech.security.jaas.UserPrincipal;
import com.usatech.security.jaas.auth.FormCallbackHandler;
import com.usatech.server.persistence.App;
import com.usatech.server.persistence.AppObjectType;
import com.usatech.server.persistence.AppObjectTypePeer;
import com.usatech.server.persistence.AppPeer;
import com.usatech.server.persistence.AppUser;
import com.usatech.server.persistence.AppUserObjectPermission;
import com.usatech.server.persistence.AppUserPeer;
import com.usatech.server.persistence.EmailQueuePeer;
import com.usatech.server.persistence.LocationPeer;
import com.usatech.server.persistence.SubdomainName;
import com.usatech.server.persistence.SubdomainNamePeer;

public class SecurityService {
    public static final String MODIFY = "modify";

    public static final String CREATE = "create";

    public static final String DELETE = "delete";

    public static final String READ = "read";

    public static final String STUDENT = "STUDENT_RECORD";

    public static final String OPERATOR = "OPERATOR_RECORD";

    public static final String LOCATION = "LOCATION_RECORD";

    public static final String LOCATION_ACCOUNTS = "LOCATION_ACCOUNTS";

    private static final String[] STUDENT_APPS = { "Student Access",
            "Room Status" };

    private static final String[] OPERATOR_APPS = { "Operator Access",
            "Operator Reports" };

    private static final String[] PROTECTED_APPS = { "OperatorAccess",
            "OperatorReports", "StudentAccess", "UserMaintenance", "AdminReports" };

    private static Log log = LogFactory.getLog(SecurityService.class);

    public static void login(HttpServletRequest request, String username,
            String password, String application) throws FailedLogonException,
            UserException {
        // check to see if user is already logged in
        if (userLoggedIn(request)) {
            throw new UserException("Already logged in");
        }
        
        if (!isUserPermitted(username)) {
        	log.warn(String.format("User %s tried to login but was soft-locked or did not exist.", username));
          throw new FailedLogonException(String.format("Could not logon user %s because of soft-lock.", username));
        }

        FormCallbackHandler handler = new FormCallbackHandler(username,
                password, application);
        LoginContext lc;

        try {
            lc = new LoginContext("DatabaseLogin", handler);
            log.debug("Created LoginContext.");
            lc.login();
            updateLogin(username);
            log.info("Logged in user " + username);

        } catch (LoginException le) {
            log.warn("Caught LoginException " + le.getMessage(), le);
            updateLoginFail(username);
            throw new FailedLogonException("Could not logon user " + username);
        } catch (SecurityException se) {
            log.error("Caught SecurityException", se);
            throw new FailedLogonException("Error Accessing Information");
        } catch (Exception e) {
            log.error("Unknown error while logging in", e);
            throw new FailedLogonException("Error Accessing Information");
        }

        // put subject in session
        Subject subject = lc.getSubject();

        HttpSession curSession = request.getSession(false);

        // invalidate current session
        if (curSession != null) {
            curSession.invalidate();
        }

        HttpSession session = request.getSession(true);
        session.setAttribute("subject", subject);
        //put the user id in the session for Record Request Filter
        session.setAttribute("appUserId", getUserId(subject));
        session.setAttribute("remoteUser", username);
    }

    private static boolean logout(Subject subject) {
        LoginContext lc;

        try {
            lc = new LoginContext("DatabaseLogin", subject);
            lc.logout();
        } catch (LoginException le) {
            return false;
        }

        return true;

    }

    public static String getUserName(HttpServletRequest request) {
        Subject subject = getSubject(request);
        if (subject != null) {
            String s = "";
            for (Iterator iter = subject.getPrincipals(NamePrincipal.class)
                    .iterator(); iter.hasNext();) {
                NamePrincipal np = (NamePrincipal) iter.next();
                s += np.getName();
                if (iter.hasNext())
                    s += "/";
            }
            return s;
        }
        return "<UNKNOWN>";
    }

    /**
     * Method getSubject.
     *
     * @param request
     * @return Subject
     */
    public static Subject getSubject(HttpServletRequest request) {
        HttpSession session = null;

        try {
            session = request.getSession(false);
        } catch (Exception e) {
            return null;
        }

        if (session == null) {
            return null;
        }

        return (Subject) session.getAttribute("subject");
    }

    /**
     * Method logout.
     *
     * @param request
     * @param response
     */
    public static boolean logout(HttpServletRequest request)
            throws ServletException, IOException {
        Subject subject = getSubject(request);
        if (subject == null) {
            request.getSession().invalidate();
            return false;
        } else {
            removeSubject(request);
            request.getSession().invalidate();
            return logout(subject);
        }
    }

    private static void removeSubject(HttpServletRequest request) {
        HttpSession session = null;

        try {
            session = request.getSession(false);
        } catch (Exception e) {
            return;
        }

        session.removeAttribute("subject");
        session.removeAttribute("appUserId");
    }

    protected static Long getUserId(Subject subject) {
        if (subject == null) {
            return null;
        } else {
            return Long.valueOf(getUser(subject).getName());
        }
    }

    public static long getUserId(HttpServletRequest request) {
    	Long userId = getUserId(getSubject(request));
        if (userId == null) {
            return -1;
        } else {
            return userId.longValue();
        }
    }

    public static List getAppObjectIds(HttpServletRequest request,
            String objectType) {
        Subject subject = getSubject(request);
        Set credentials = subject.getPublicCredentials(AppCredential.class);
        String app = SecurityService.getApp(request);

        ArrayList objectIds = new ArrayList();
        int index = 0;

        for (Iterator i = credentials.iterator(); i.hasNext();) {
            AppCredential credential = (AppCredential) i.next();

            if (credential.getObjectType().equals(objectType)
                    && credential.getApp().equals(app)) {
                objectIds.add("" + credential.getObjectId());
            }
        }

        return objectIds;
    }

    private static UserPrincipal getUser(Subject subject) {
        Set set = subject.getPrincipals(UserPrincipal.class);

        //there should only be one user principal
        if (set.size() > 1 || set.isEmpty()) {
            //handle this by throwing exception or logging out or something
            log.error("More than one user logged in.");
            return null;
        }

        return (UserPrincipal) set.iterator().next();
    }

    public static boolean userLoggedIn(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return false;
        }

        Subject subject = getSubject(request);
        if (subject == null) {
            return false;
        }

        return true;
    }

    public static String getApp(HttpServletRequest request) {
        String app = request.getParameter("application");
        if(app != null) return app;
        String path = request.getServletPath();
        if (path == null) {
            return "";
        } else {
            StringTokenizer token = new StringTokenizer(path, "/");
            if (token.hasMoreTokens()) {
                return token.nextToken();
            } else {
                return "";
            }
        }

    }

    public static boolean isProtectedApp(HttpServletRequest request) {
        if (request.getRequestURI() != null) {
            for (int i = 0; i < PROTECTED_APPS.length; i++) {
                if (request.getRequestURI().indexOf(PROTECTED_APPS[i]) > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    public static void refreshPolicy() {
        Policy policy = Policy.getPolicy();
        policy.refresh();
    }

    public static void createUser(String username, String password,
            String userType, long id) throws UserException {
        // valid username and password?
        if (!SecurityUtil.isValidUsername(username)) {
            throw new UserException("Username is not valid.");
        }

        if (!SecurityUtil.isValidPassword(password)) {
            throw new UserException("Password is not valid.");
        }

        // does username exist?
        try {
            if (AppUserPeer.doesUserExist(username)) {
                log.debug("Username exists");
                throw new UserException("Username exists.");
            }
        } catch (TorqueException te) {
            log.error("Error checking username existence: " + username);
            throw new UserException("Can not check username existence");
        }

        // create user
        AppUser user = new AppUser();
        user.setAppUserName(username);
        user.setAppUserPassword(password);
        user.setAppUserActiveYnFlag("Y");

        try {
            user.save();
        } catch (TorqueException te) {
            log.error("Error saving user " + username);
            throw new UserException("Could not create user.");
        } catch (Exception e) {
            log.error("Error saving user " + username + " " + e);
            throw new UserException("Could not create user.");
        }

        // add permission for themselves and apps

        // look up app_object_type
        AppObjectType type = null;
        try {
            type = AppObjectTypePeer.retrieveByName(userType);
        } catch (TorqueException te) {
            log.error("Error retrieving object type " + userType);
            throw new UserException("Could not create User Permissions.");
        }

        App[] apps = new App[0];

        if (userType.equals(STUDENT)) {
            apps = new App[STUDENT_APPS.length];
            //look up apps
            for (int i = 0; i < STUDENT_APPS.length; i++) {
                try {
                    apps[i] = AppPeer.retrieveByName(STUDENT_APPS[i]);
                } catch (TorqueException te) {
                    log.error("Error retrieving app " + STUDENT_APPS[i]);
                    throw new UserException(
                            "Could not create User Permissions.");
                }
            }
        } else if (userType.equals(OPERATOR)) {
            apps = new App[OPERATOR_APPS.length];
            //look up apps
            for (int i = 0; i < OPERATOR_APPS.length; i++) {
                try {
                    apps[i] = AppPeer.retrieveByName(OPERATOR_APPS[i]);
                } catch (TorqueException te) {
                    log.error("Error retrieving app " + OPERATOR_APPS[i]);
                    throw new UserException(
                            "Could not create User Permissions.");
                }
            }
        }

        for (int i = 0; i < apps.length; i++) {
            //Create Permission
            AppUserObjectPermission permission = new AppUserObjectPermission();

            // Read & Modify Access for the user's id
            permission.setObjectCd("" + id);
            permission.setAllowObjectReadYnFlag("Y");
            permission.setAllowObjectModifyYnFlag("Y");
            permission.setAllowObjectCreateYnFlag("N");
            permission.setAllowObjectDeleteYnFlag("N");

            try {
                permission.setAppId(apps[i].getAppId());
                permission.setAppUserId(user.getAppUserId());
                permission.setAppObjectTypeId(type.getAppObjectTypeId());
                permission.save();
            } catch (TorqueException te) {
                log.error("Error creating permission");
                throw new UserException("Could not create permission.");
            } catch (Exception e) {
                log.error("Error creating permission " + e);
                throw new UserException("Could not create permission.");
            }

        }

        // if operator, add permissions for campus

        refreshPolicy();
    }

    private static String getPermissionSql(String userId, String app,
            String objType) {
        return "SELECT auop.object_cd FROM app_user_object_permission auop, "
                + "app_object_type aot, app a WHERE auop.app_user_id = "
                + userId + " AND auop.app_id = a.app_id AND a.app_cd = \'"
                + app + "\'"
                + " AND aot.app_object_type_id = auop.app_object_type_id "
                + " AND aot.app_object_cd = \'" + objType + "\'";
    }

    public static void changePassword(String username, String currPass,
            String newPass) throws UserException {
        if (!SecurityUtil.isValidPassword(newPass)) {
            throw new UserException("Password is not valid");
        }

        AppUser user = null;

        try {
            user = AppUserPeer.retrieveByUsernameIgnoreCase(username);
        } catch (TorqueException te) {
            log.error("Error retrieving user with username " + username + ", "
                    + te.getMessage());
            throw new UserException("Could not find user");
        }

        if (user.getAppUserPassword().equals(currPass)) {
            user.setAppUserPassword(newPass);

            try {
                user.save();
            } catch (TorqueException te) {
                log.error("Error saving user with username " + username
                        + "with new password, " + te.getMessage());
                throw new UserException("Could not save new password");
            } catch (Exception e) {
                log.error("Error saving user with username " + username
                        + "with new password, " + e.getMessage());
                throw new UserException("Could not save new password");
            }
        } else {
            throw new UserException("Current Password does not match.");
        }

        // If we got this far, change was successful
        String email = user.getAppUserEmailAddr();
        String subject = "Password Change";
        String msg = "Your password for eSuds.net has been changed. "
                + "If you were not the one who changed it, please contact the administrator.";

        try {
            EmailQueuePeer.createAndSendEmail(email, "eSuds User",
                    "admin@esuds.net", "eSuds Administrator", subject, msg);
        } catch (Exception e) {
            log.error("Error sending e-mail to confirm password change.");
        }
    }

    public static void changePassword(HttpServletRequest request,
            String currPass, String newPass) throws UserException {
        if (!SecurityUtil.isValidPassword(newPass)) {
            throw new UserException("New password is not valid");
        }

        long userId = getUserId(request);
        AppUser user = null;

        try {
            user = AppUserPeer.retrieveByPK(userId);
        } catch (TorqueException te) {
            log.error("Error retrieving user with id " + userId + ", "
                    + te.getMessage());
            throw new UserException("Could not find user");
        }

        if (user.getAppUserPassword().equals(currPass)) {
            user.setAppUserPassword(newPass);

            try {
                user.save();
            } catch (TorqueException te) {
                log.error("Error saving user with id " + userId
                        + "with new password, " + te.getMessage());
                throw new UserException("Could not save new password");
            } catch (Exception e) {
                log.error("Error saving user with id " + userId
                        + "with new password, " + e.getMessage());
                throw new UserException("Could not save new password");
            }
        } else {
            throw new UserException("Current password is incorrect");
        }

        // If we got this far, change was successful
        String email = user.getAppUserEmailAddr();
        String subject = "Password Change";
        String msg = "Your password for eSuds.net has been changed. "
                + "If you were not the one who changed it, please contact the administrator.";

        try {
            EmailQueuePeer.createAndSendEmail(email, "eSuds User",
                    "admin@esuds.net", "eSuds Administrator", subject, msg);
        } catch (Exception e) {
            log.error("Error sending e-mail to confirm password change.");
        }

    }

    public static boolean checkCredentials(HttpServletRequest request,
            String objectType, long objectId, String action) {
        AppCredential credential = new AppCredential();
        credential.setObjectType(objectType);
        credential.setObjectId(objectId);
        credential.setActions(action);
        credential.setApp(SecurityService.getApp(request));

        Subject subject = getSubject(request);

        Set credentials = subject.getPublicCredentials(AppCredential.class);

        for (Iterator i = credentials.iterator(); i.hasNext();) {
            AppCredential c = (AppCredential) i.next();

            if (c.implies(credential)) {
                return true;
            }
        }
        //Locations are a hierarchy so let's check up the chain
        if(LOCATION.equals(objectType)) {
            long parentObjectId;
            try {
                parentObjectId = LocationPeer.retrieveByPK(objectId).getParentLocationId();
                if(parentObjectId != 0)
                    return checkCredentials(request, objectType, parentObjectId, action);
                else
                    return false;
            } catch(TorqueException te) {
                log.warn("While getting parent location", te);
                return false;
            }
        }

        return false;

    }

    public static Hashtable getObjectForSubdomain(HttpServletRequest request) {
        String sub = null;
        String domain = null;
        try {
            sub = SecurityUtil.parseSubdomain(request);
            domain = SecurityUtil.parseDomain(request);
        } catch (Exception e) {
            log.info("Error parsing subdomain and domain " + e);
            return null;
        }

        SubdomainName subdomain = null;

        try {
            subdomain = SubdomainNamePeer.retrieveByPK(sub, domain);
        } catch (TorqueException te) {
            log.info("Error retrieving sub domain assignment: " + te);
            return null;
        }

        if (subdomain != null) {
            try {
                Hashtable result = new Hashtable();
                String type = subdomain.getAppObjectType().getAppObjectCd();
                long id = Long.parseLong(subdomain.getObjectCd());

                if (type.equals(OPERATOR)) {
                    result.put("operatorId", new Long(id));
                } else if (type.equals(LOCATION)) {
                    try {
                        type = LocationUtil.getLocationType(id);
                    } catch (DataException de) {
                        log.error("Error getting location type: " + de);
                        return null;
                    } catch (UnknownObjectException uoe) {
                        log.error("Could not find location with id + " + id);
                        return null;
                    }

                    if (type.equals(School.LOCATION_TYPE_DESC)) {
                        result.put("schoolId", new Long(id));
                    } else if (type.equals(Campus.LOCATION_TYPE_DESC)) {
                        result.put("campusId", new Long(id));
                    } else if (type.equals(Dorm.LOCATION_TYPE_DESC)) {
                        result.put("dormId", new Long(id));
                    } else if (type.equals(LaundryRoom.LOCATION_TYPE_DESC)) {
                        result.put("roomId", new Long(id));
                    }
                }

                return result;
            } catch (TorqueException te) {
                log.error("Error retrieve app object type: " + te);
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param username
     * the username of the user whose password we want to reset @ param url the
     *            url which will allow the user to reset the password
     * @throws UserException
     *             if no user with username is found
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     */
    public static void requestResetPassword(String username, String url)
            throws UserException, NoSuchAlgorithmException, NoSuchProviderException {
        AppUser user = null;

        try {
            user = AppUserPeer.retrieveByUsernameIgnoreCase(username);
        } catch (TorqueException te) {
            log.error("Error retrieving user with username " + username + ", "
                    + te.getMessage());
            throw new UserException("Could not find user");
        }

        // user must have e-mail address to reset password
        if (user.getAppUserEmailAddr() == null
                || user.getAppUserEmailAddr().equals("")) {
            throw new UserException("No e-mail address for user");
        }

        // Set token for reset
        String token = SecurityUtil.generateToken(new Date().toString(), 8);

        user.setAppUserPasswordResetToken(token);

        try {
            user.save();
        } catch (TorqueException te) {
            log.error("Error saving token for reset: " + te.getMessage());
            throw new UserException("Error processing reset request.");
        } catch (Exception e) {
            log.error("Error saving token for reset: " + e.getMessage());
            throw new UserException("Error processing reset request.");
        }

        // If we got this far, change was successful
        String email = user.getAppUserEmailAddr();
        String subject = "Password Reset Request";
        String msg = "A request to reset your password for eSuds.net has been received. "
                + "If you were not the one who requested the change it, please ignore this message.\n"
                + "Please go to " + url + token + " to reset your password.";

        try {
            EmailQueuePeer.createAndSendEmail(email, "eSuds User",
                    "admin@esuds.net", "eSuds Administrator", subject, msg);
        } catch (Exception e) {
            log.error("Error sending e-mail to confirm password reset: "
                    + e.getMessage(), e);
            throw new UserException("Could not send e-mail.");
        }

    }

    public static void resetPassword(String username, String token)
            throws UserException, NoSuchAlgorithmException, NoSuchProviderException {
        AppUser user = null;

        try {
            user = AppUserPeer.retrieveByUsernameIgnoreCase(username);
        } catch (TorqueException te) {
            log.error("Error retrieving user with username " + username + ", "
                    + te.getMessage());
            throw new UserException("Could not find user");
        }

		if(user.getAppUserPasswordResetToken() != null && user.getAppUserPasswordResetToken().equals(token)) {
            String passwd = SecurityUtil.createTemporaryPassword();
            user.setAppUserPassword(passwd);
            user.setForcePwChangeYnFlag("Y");
            user.setAppUserPasswordResetToken("");
            try {
                user.save();
            } catch (TorqueException te) {
                log.error("Error setting password and resetting token: "
                        + te.getMessage());
            } catch (Exception e) {
                log.error("Error setting password and resetting token: "
                        + e.getMessage());
            }

            // send e-mail
            String email = user.getAppUserEmailAddr();
            String subject = "Password Reset";
            String msg = "Your password for eSuds.net has been reset. "
                    + "Your temporary password is " + passwd + ".";

            try {
                EmailQueuePeer.createAndSendEmail(email, "eSuds User",
                        "admin@esuds.net", "eSuds Administrator", subject, msg);
            } catch (Exception e) {
                log.error("Error sending e-mail for reset password: "
                        + e.getMessage());
                throw new UserException("Could not send e-mail.");
            }
        } else {
            throw new UserException("Invalid token");
        }
    }

  	private static void updateLogin(String username) {
  		try {
  			Map<String, Object> params = new HashMap<>();
  			params.put("username", username);
  			DataLayerMgr.executeCall("RECORD_LOGIN", params, true);
  		} catch(SQLException e) {
  			log.warn("Could not record login for user '" + username + "'", e);
  		} catch(DataLayerException e) {
  			log.warn("Could not record login for user '" + username + "'", e);
  		}
  	}

  	private static void updateLoginFail(String username) {
  		try {
  			Map<String, Object> params = new HashMap<>();
  			params.put("username", username);
  			DataLayerMgr.executeCall("RECORD_FAILED_LOGIN", params, true);
  		} catch(SQLException e) {
  			log.warn("Could not record login failure for user '" + username + "'", e);
  		} catch(DataLayerException e) {
  			log.warn("Could not record login failure for user '" + username + "'", e);
  		}
  	}
  	
  	private static boolean isUserPermitted(String username) {
  		Results results = null;
  		try{
  			Map<String, Object> params = new HashMap<>();
  			params.put("username", username);
  			results=DataLayerMgr.executeQuery("IS_USER_PERMITTED", params);
  			if(results.next()){
  				return ConvertUtils.getInt(results.get("cnt"), 0) == 1;
  			}
  		}catch(Exception e){
  				log.error("Failed to retrieve consumer card count", e);
  		}
  		return false;
  	}
}

