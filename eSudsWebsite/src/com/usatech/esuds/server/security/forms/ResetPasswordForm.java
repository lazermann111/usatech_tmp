package com.usatech.esuds.server.security.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
/**
 * Form bean for a Struts application.
 * Users may access 1 field on this form:
 * <ul>
 * <li>username - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class ResetPasswordForm extends ActionForm
{
	private String username = null;
	private String action = null;
	private String token = null;
	
	/**
	 * Get username
	 * @return String
	 */
	public String getUsername()
	{
		return username;
	}
	/**
	 * Set username
	 * @param <code>String</code>
	 */
	public void setUsername(String u)
	{
		this.username = u;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		// Reset values are provided as samples only. Change as appropriate.
		username = null;
	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		
		if (action.equals("requestReset"))
		{
			if (username == null || username.equals(""))
			{
				errors.add("username", new ActionError("error.username.required"));
			}
		}
		
		return errors;
	}
	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	/**
	 * @return
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @param string
	 */
	public void setToken(String string)
	{
		token = string;
	}

}
