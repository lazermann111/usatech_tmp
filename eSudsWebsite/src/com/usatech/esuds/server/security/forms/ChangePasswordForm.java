package com.usatech.esuds.server.security.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 4 fields on this form:
 * <ul>
 * <li>confirmPassword - [your comment here]
 * <li>currPassword - [your comment here]
 * <li>newPassword - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class ChangePasswordForm extends ActionForm
{

	private String currPassword = null;
	private String confirmPassword = null;
	private String newPassword = null;
	private String action = null;
	
	/**
	 * Get currPassword
	 * @return String
	 */
	public String getCurrPassword()
	{
		return currPassword;
	}

	/**
	 * Set currPassword
	 * @param <code>String</code>
	 */
	public void setCurrPassword(String c)
	{
		this.currPassword = c;
	}

	/**
	 * Get confirmPassword
	 * @return String
	 */
	public String getConfirmPassword()
	{
		return confirmPassword;
	}

	/**
	 * Set confirmPassword
	 * @param <code>String</code>
	 */
	public void setConfirmPassword(String c)
	{
		this.confirmPassword = c;
	}

	/**
	 * Get newPassword
	 * @return String
	 */
	public String getNewPassword()
	{
		return newPassword;
	}

	/**
	 * Set newPassword
	 * @param <code>String</code>
	 */
	public void setNewPassword(String n)
	{
		this.newPassword = n;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		// Reset values are provided as samples only. Change as appropriate.

		currPassword = null;
		confirmPassword = null;
		newPassword = null;

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();
		
		if (action.equals("showForm"))
		{
			return errors;
		}
		
		if ((currPassword == null) || (currPassword.length() == 0))
		{
			errors.add("currPassword", new org.apache.struts.action.ActionError("error.currPassword.required"));
		}
		
		if ((newPassword == null) || (newPassword.length() == 0))
		{
			errors.add("newPassword", new org.apache.struts.action.ActionError("error.newPassword.required"));
		}		

		if ((confirmPassword == null) || (confirmPassword.length() == 0))
		{
			errors.add("confirmPassword", new org.apache.struts.action.ActionError("error.confirmPassword.required"));
		}
		
		if (!newPassword.equals(confirmPassword))
		{
			errors.add("currPassword", new org.apache.struts.action.ActionError("error.confirmPassword.nomatch"));
		}		
		return errors;

	}
	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

}
