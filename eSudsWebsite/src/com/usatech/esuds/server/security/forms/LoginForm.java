package com.usatech.esuds.server.security.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
*
 * @version 	1.0
 * @author
 */
public class LoginForm extends ActionForm
{

	private String user = "";
	private String password = "";
	private String application = "";
	private String action = "";
	
	/**
	 * Get user
	 * @return String
	 */
	public String getUser()
	{
		return user;
	}

	/**
	 * Set user
	 * @param <code>String</code>
	 */
	public void setUser(String u)
	{
		user = u;
	}
	/**
	 * Get password
	 * @return String
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * Set password
	 * @param <code>String</code>
	 */
	public void setPassword(String p)
	{
		password = p;
	}
	/**
	* Constructor
	*/
	public LoginForm()
	{

		super();

	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		user = "";
		password = "";
		application = "";
		action = "";
	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();

		if (action.equals("login") && ((user == null) || (user.length() == 0)))
		{
		   errors.add("username", new ActionError("error.username.required"));
		}
		
		if (action.equals("login")  && ((password == null) || (password.length() == 0)))
		{
		   errors.add("password", new ActionError("error.password.required"));
		}
		
		return errors;

	}
	/**
	 * @return
	 */
	public String getApplication()
	{
		return application;
	}

	/**
	 * @param string
	 */
	public void setApplication(String string)
	{
		application = string;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

}
