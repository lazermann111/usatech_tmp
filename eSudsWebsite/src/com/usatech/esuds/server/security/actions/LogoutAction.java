package com.usatech.esuds.server.security.actions;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.SecurityUtil;

/**
 * @version 	1.0
 * @author
 */
public class LogoutAction extends Action
{
	private static Log log = LogFactory.getLog(LogoutAction.class);
	private static final String COOKIE_NAME = "home"; 

	/**
	* Constructor
	*/
	public LogoutAction()
	{

		super();

	}
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
		// return value

		boolean success = false;

		try
		{
			success = SecurityService.logout(request);

			// remove Cookie
			Cookie cookie = SecurityUtil.getCookie(request, COOKIE_NAME);
			
			if (cookie != null)
			{
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
			
			if (success)
			{
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logout.successful"));
			}
		}
		catch (Exception e)
		{

			// Report the error using the appropriate name and ID.
			errors.add("login.failed", new ActionError("error.login.failed"));

		}

		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.

		if (!errors.isEmpty())
		{
			saveErrors(request, errors);

			// Forward control to the appropriate 'failure' URI (change name as desired)
			forward = mapping.findForward("failure");

		}
		else
		{
			saveMessages(request, messages);
			// Forward control to the appropriate 'success' URI (change name as desired)
			forward = mapping.findForward("success");

		}

		// Finish with
		return (forward);

	}
}
