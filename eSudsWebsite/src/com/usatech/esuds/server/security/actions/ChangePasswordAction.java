package com.usatech.esuds.server.security.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.security.exceptions.UserException;
import com.usatech.esuds.server.security.forms.ChangePasswordForm;
import com.usatech.esuds.server.security.services.SecurityService;

/**
 * @version 1.0
 * @author
 */
public class ChangePasswordAction extends DispatchAction {

    public ActionForward showForm(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ChangePasswordForm chgPassForm = (ChangePasswordForm) form;
        chgPassForm.setAction("change");
        return mapping.findForward("form");
    }

    public ActionForward change(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        // return value

        ChangePasswordForm chgPassForm = (ChangePasswordForm) form;
        String currPass = chgPassForm.getCurrPassword();
        String newPass = chgPassForm.getNewPassword();

        try {

            SecurityService.changePassword(request, currPass, newPass);

        } catch (UserException ue) {
            log.warn("Error changing password", ue);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.change.password", ue.getMessage() + "."));
        } catch (Exception e) {
            log.warn("Error changing password", e);
            // Report the error using the appropriate name and ID.
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.unknown"));

        }

        // If a message is required, save the specified key(s)
        // into the request for use by the <struts:errors> tag.
        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            // Forward control to the appropriate 'failure' URI (change name as
            // desired)
            forward = mapping.findForward("failure");
        } else {
            // Forward control to the appropriate 'success' URI (change name as
            // desired)
            forward = mapping.findForward("success");
        }

        // Finish with
        return (forward);

    }
}