package com.usatech.esuds.server.security.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import simple.text.StringUtils;

import com.usatech.esuds.server.security.exceptions.FailedLogonException;
import com.usatech.esuds.server.security.exceptions.UserException;
import com.usatech.esuds.server.security.forms.LoginForm;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.SecurityUtil;

/**
 * @version 	1.0
 * @author
 */
public class LoginAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(LoginAction.class);
	
	private static final String COOKIE_NAME = "home"; 
	
	public ActionForward showForm(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();

		LoginForm loginForm = (LoginForm) form;
		String application = loginForm.getApplication();
		
		try
		{
			if (loginForm.getApplication() == null || loginForm.getApplication().equals(""))
			{
				log.error("No application");
			}
			
			Cookie cookie = SecurityUtil.getCookie(request, COOKIE_NAME);
			
			if (cookie != null)
			{
				// remove Cookie
				cookie.setMaxAge(0);
				response.addCookie(cookie);
							
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("login.session.expired"));
			}
			
			loginForm.setAction("login");
		}
		catch (Exception e)
		{
			// Report the error using the appropriate name and ID.
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
		}

		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("loginForm");
		}

		return (forward);
	}

	/**
	* Constructor
	*/
	public LoginAction()
	{
		super();
	}
	public ActionForward login(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException
		{

		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		// return value
		
		LoginForm login = (LoginForm) form;
		String appName = StringUtils.removeCRLF(login.getApplication());

		try
		{
			SecurityService.login(request, login.getUser(), login.getPassword(), appName);
			log.debug("Logged in.");
			
			Cookie cookie = new Cookie("home", appName);
			cookie.setPath("/");
			cookie.setSecure(true);
			response.addCookie(cookie);
			
		}
		catch (FailedLogonException fle)
		{
			// Report the error using the appropriate name and ID.
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.login.failed"));
		}
		catch (UserException ue)
		{
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.login.user.logged.in"));
		}
		catch (Exception e)
		{
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.login.failed"));
			log.error("Caught Exception", e);
		}

		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.

		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			login.setPassword("");
			forward = mapping.findForward("failure");

		}
		else
		{
			// forward to home.do of app
			forward.setPath("/" + appName + "/home.do");

			forward.setContextRelative(true);
			forward.setRedirect(true);

		}

		// Finish with
		return (forward);

	}
}
