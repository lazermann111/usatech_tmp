package com.usatech.esuds.server.security.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.security.exceptions.UserException;
import com.usatech.esuds.server.security.forms.ResetPasswordForm;
import com.usatech.esuds.server.security.services.SecurityService;

/**
 * @version 1.0
 * @author
 */
public class ResetPasswordAction extends DispatchAction {
    public ActionForward requestReset(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();

        ResetPasswordForm resetPasswordForm = (ResetPasswordForm) form;

        String username = resetPasswordForm.getUsername();
        String url = "http://" + request.getServerName()
                + "/Security/resetPassword.do?action=confirm&username="
                + username + "&token=";

        try {
            SecurityService.requestResetPassword(username, url);
        } catch (UserException ue) {
            log.warn("Error requesting password reset", ue);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.reset.password", ue.getMessage() + "."));
        } catch (Exception e) {
            log.warn("Error requesting password reset", e);
            // Report the error using the appropriate name and ID.
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.unknown"));
        }
        // If a message is required, save the specified key(s)
        // into the request for use by the <struts:errors> tag.
        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            // Forward control to the appropriate 'failure' URI (change name as
            // desired)
            forward = mapping.findForward("failure");
        } else {
            // Forward control to the appropriate 'success' URI (change name as
            // desired)
            forward = mapping.findForward("requestSuccess");
        }
        // Finish with
        return (forward);
    }

    public ActionForward showForm(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        ResetPasswordForm resetPasswordForm = (ResetPasswordForm) form;
        resetPasswordForm.setAction("requestReset");
        return (mapping.findForward("form"));
    }

    public ActionForward confirm(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();

        ResetPasswordForm resetPasswordForm = (ResetPasswordForm) form;

        String username = resetPasswordForm.getUsername();
        String token = resetPasswordForm.getToken();

        try {
            SecurityService.resetPassword(username, token);
        } catch (UserException ue) {
            log.warn("Error reseting password", ue);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.reset.password", ue.getMessage() + "."));
        } catch (Exception e) {
            log.warn("Error reseting password", e);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "unknown.error"));
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            // Forward control to the appropriate 'failure' URI (change name as
            // desired)
            forward = mapping.findForward("failure");
        } else {
            // Forward control to the appropriate 'success' URI (change name as
            // desired)
            forward = mapping.findForward("success");
        }

        return forward;
    }
}