package com.usatech.esuds.server.security.exceptions;

/**
 * @author melissa
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class NotLoggedInException extends Exception
{

	/**
	 * Constructor for NotLoggedInException.
	 */
	public NotLoggedInException()
	{
		super();
	}

	/**
	 * Constructor for NotLoggedInException.
	 * @param s
	 */
	public NotLoggedInException(String s)
	{
		super(s);
	}

}
