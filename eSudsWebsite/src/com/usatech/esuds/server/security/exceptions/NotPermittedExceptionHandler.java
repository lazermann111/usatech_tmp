/*
 * Created on Oct 27, 2003
 *
 */
package com.usatech.esuds.server.security.exceptions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

/**
 * @author melissa
 *
 */
public class NotPermittedExceptionHandler extends ExceptionHandler
{

	/**
	 * 
	 */
	public NotPermittedExceptionHandler()
	{
		super();
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.ExceptionHandler#execute(java.lang.Exception, org.apache.struts.config.ExceptionConfig, org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward execute(
		Exception e,
		ExceptionConfig ec,
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException
	{
		
		return mapping.findForward("notPermitted");
	}

	/* (non-Javadoc)
	 * @see org.apache.struts.action.ExceptionHandler#storeException(javax.servlet.http.HttpServletRequest, java.lang.String, org.apache.struts.action.ActionError, org.apache.struts.action.ActionForward, java.lang.String)
	 */
	protected void storeException(
		HttpServletRequest request,
		String string1,
		ActionError error,
		ActionForward forward,
		String string2)
	{
		super.storeException(request,string1, error, forward, string2);
	}

}
