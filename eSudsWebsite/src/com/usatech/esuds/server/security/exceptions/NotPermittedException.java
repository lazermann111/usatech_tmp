package com.usatech.esuds.server.security.exceptions;

/**
 * @author melissa
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class NotPermittedException extends Exception
{

	/**
	 * Constructor for NotPermittedException.
	 */
	public NotPermittedException()
	{
		super();
	}

	/**
	 * Constructor for NotPermittedException.
	 * @param s
	 */
	public NotPermittedException(String s)
	{
		super(s);
	}

}
