/*
 * Created on Oct 30, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.security.exceptions;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UserException extends Exception
{

	/**
	 * 
	 */
	public UserException()
	{
		super();
	}

	/**
	 * @param s
	 */
	public UserException(String s)
	{
		super(s);
	}

}
