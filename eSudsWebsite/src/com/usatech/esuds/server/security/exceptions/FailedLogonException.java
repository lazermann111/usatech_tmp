package com.usatech.esuds.server.security.exceptions;

/**
 * @author melissa
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class FailedLogonException extends Exception
{

	/**
	 * Constructor for FailedLogonException.
	 */
	public FailedLogonException()
	{
		super();
	}

	/**
	 * Constructor for FailedLogonException.
	 * @param s
	 */
	public FailedLogonException(String s)
	{
		super(s);
	}

}
