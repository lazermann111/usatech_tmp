package com.usatech.esuds.server.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.esuds.server.security.services.SecurityService;

/**
 * @version 	1.0
 * @author
 */
public class JAASFilter implements Filter
{
	private static Log log = LogFactory.getLog(JAASFilter.class);
	private String loginForm = null;
	private String login = null;
	private String logout = null;
	private String notPermitted = null;
	private String error = null;

	
	
	/**
	* @see javax.servlet.Filter#void ()
	*/
	public void destroy()
	{

	}

	/**
	* @see javax.servlet.Filter#void (javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	*/
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
		throws ServletException, IOException
	{		
		// If request is looking for login or logout, let it through
		// may want to make this more robust
		if (req instanceof HttpServletRequest)
		{
			HttpServletRequest request = (HttpServletRequest) req;
			String app = SecurityService.getApp(request);
			log.debug("App is " + app);
			
			if (!SecurityService.isProtectedApp(request))
			{
				// allow
				log.debug("Allowing access to " + request.getRequestURI());
				chain.doFilter(req,resp);
			}
			// Check if user is logged in
			else if (SecurityService.userLoggedIn(request))
			{
				log.debug("User is logged in. Allowing access to " + request.getRequestURI());
				chain.doFilter(req,resp);
			}
			// Allow access to logout
			else if (request.getRequestURI().endsWith("logout.do"))
			{
				log.debug("Allowing access to logout");
				chain.doFilter(req,resp);
			}
			else
			{	
				log.debug("User is not logged in.");
				
				// User is not logged in! - Forward to login page					
				// Send application
				String url = null;
				if (loginForm.indexOf('?') == -1)
				{
					url = loginForm + "?application=" + app;
				}
				else
				{
					url = loginForm + "&application=" + app;
				}
				request.getRequestDispatcher(url).forward(req, resp);				
			}
		}
		else
		{
			// Calls next Filter in Chain
			chain.doFilter(req, resp);
		}

	}
	
	/**
	* Method init.
	* @param config
	* @throws javax.servlet.ServletException
	*/
	public void init(FilterConfig config) throws ServletException
	{
		loginForm = config.getInitParameter("loginForm");
		log.debug("Initialized Filter");
	}

}
