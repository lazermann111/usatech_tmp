package com.usatech.esuds.server.adminreports.actions;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.adminreports.beans.ReconciliationSummaryBean;
import com.usatech.esuds.server.adminreports.forms.ReconciliationSummaryForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.DateUtil;
import com.usatech.server.persistence.Location;
import com.usatech.server.persistence.LocationPeer;

import simple.app.DialectResolver;

/**
 * @version 1.0
 * @author
 */
public class ReconciliationSummaryAction extends DispatchAction {
    private static Log log = LogFactory.getLog(ReconciliationSummaryAction.class);
    private static final String OBJECT_TYPE = SecurityService.LOCATION;
    private static final int APP_ID = 6;
    
    public ActionForward buildSearch(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();

        ReconciliationSummaryForm reportForm = (ReconciliationSummaryForm) form;
        
        List locations = LocationPeer.retrieveByUserPrivilege(SecurityService.getUserId(request), APP_ID, OBJECT_TYPE, SecurityService.READ);
        if(locations.isEmpty()) {
            throw new NotPermittedException("Not permitted to view this resource for any location.");          
        } else {
            /*long[] locationIds = new long[locations.size()];
            for(int i = 0; i < locationIds.length; i++) locationIds[i] = ((Location)locations.get(i)).getLocationId();
            reportForm.setLocationIds(locationIds);
            */
            request.setAttribute("locations", locations);
            if(reportForm.getLocationId() == 0) reportForm.setLocationId(((Location)locations.get(0)).getLocationId());
            if (reportForm.getDateChoice() == null || reportForm.getDateChoice().equals("")) {
                reportForm.setDateChoice("today");
            }
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            forward = mapping.findForward("search");
        }

        return forward;
    }

    public ActionForward runReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();

        ReconciliationSummaryForm reportForm = (ReconciliationSummaryForm) form;
        long locationId = reportForm.getLocationId();
        if (SecurityService.checkCredentials(request, OBJECT_TYPE, locationId, SecurityService.READ)) {
            try {
                Date startDate = null;
                Date endDate = null;
                GregorianCalendar date = new GregorianCalendar();

                if (reportForm.getDateChoice().equalsIgnoreCase("today")) {
                    date.set(GregorianCalendar.HOUR_OF_DAY, 0);
                    date.set(GregorianCalendar.MINUTE, 0);
                    date.set(GregorianCalendar.SECOND, 0);

                    startDate = date.getTime();

                    date.set(GregorianCalendar.HOUR_OF_DAY, 23);
                    date.set(GregorianCalendar.MINUTE, 59);
                    date.set(GregorianCalendar.SECOND, 59);

                    endDate = date.getTime();
                } else if (reportForm.getDateChoice().equalsIgnoreCase("yesterday")) {
                    date.roll(GregorianCalendar.DAY_OF_YEAR, false);

                    date.set(GregorianCalendar.HOUR_OF_DAY, 0);
                    date.set(GregorianCalendar.MINUTE, 0);
                    date.set(GregorianCalendar.SECOND, 0);

                    startDate = date.getTime();

                    date.set(GregorianCalendar.HOUR_OF_DAY, 23);
                    date.set(GregorianCalendar.MINUTE, 59);
                    date.set(GregorianCalendar.SECOND, 59);

                    endDate = date.getTime();

                } else if (reportForm.getDateChoice().equalsIgnoreCase("since")) {
                    GregorianCalendar since = DateUtil.parseDate(reportForm.getSinceDate());
                    //since.set(GregorianCalendar.HOUR_OF_DAY, 0);
                    //since.set(GregorianCalendar.MINUTE, 0);
                    //since.set(GregorianCalendar.SECOND, 0);

                    startDate = since.getTime();

                    date.set(GregorianCalendar.HOUR_OF_DAY, 23);
                    date.set(GregorianCalendar.MINUTE, 59);
                    date.set(GregorianCalendar.SECOND, 59);

                    endDate = date.getTime();
                } else if (reportForm.getDateChoice().equalsIgnoreCase("on")) {
                    GregorianCalendar on = DateUtil.parseDate(reportForm.getOnDate());
                    on.set(GregorianCalendar.HOUR_OF_DAY, 0);
                    on.set(GregorianCalendar.MINUTE, 0);
                    on.set(GregorianCalendar.SECOND, 0);

                    startDate = on.getTime();
                    on.set(GregorianCalendar.HOUR_OF_DAY, 23);
                    on.set(GregorianCalendar.MINUTE, 59);
                    on.set(GregorianCalendar.SECOND, 59);

                    endDate = on.getTime();
                } else if (reportForm.getDateChoice().equalsIgnoreCase("fromto")) {
                    GregorianCalendar from = DateUtil.parseDate(reportForm.getFromDate());
                    from.set(GregorianCalendar.HOUR_OF_DAY, 0);
                    from.set(GregorianCalendar.MINUTE, 0);
                    from.set(GregorianCalendar.SECOND, 0);

                    startDate = from.getTime();

                    GregorianCalendar to = DateUtil.parseDate(reportForm.getToDate());
                    to.set(GregorianCalendar.HOUR_OF_DAY, 23);
                    to.set(GregorianCalendar.MINUTE, 59);
                    to.set(GregorianCalendar.SECOND, 59);

                    endDate = to.getTime();
                }

                // Retrieve Schools with this operator
                request.setAttribute("startDate", startDate);
                request.setAttribute("endDate", endDate);             
                List results = retrieveSummaryResults(locationId, startDate.getTime(), endDate.getTime());
                request.setAttribute("results", results);
            } catch (Exception e) {
                log.error("Unknown error occurred: ", e);
                errors.add("unknown.error", new ActionError("error.unknown"));
            }
        } else {
            throw new NotPermittedException("Not permitted to view this resource.");
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            forward = mapping.findForward("report");
        }

        return forward;
    }

    //  Torque is a horrible way to access data - it does not allow group by's, so we'll do this
    // using plain old jdbc
    private List retrieveSummaryResults(long locationId, long startMillis, long endMillis) throws TorqueException, SQLException {
        String sql = null;
        if (DialectResolver.isOracle())
        	sql = "{?=call PSS.PKG_REPORTS.GET_RECONCILIATION_SUMMARY(?,?,?)}";
        else
        	sql = "{?=call PKG_REPORTS.GET_RECONCILIATION_SUMMARY(?,?,?)}";
        Connection conn = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        List results = new ArrayList();
        try {
            conn = Torque.getConnection();
            cs = conn.prepareCall(sql);
            if (DialectResolver.isOracle())
            	cs.registerOutParameter(1, -10 /*oracle.jdbc.driver.OracleTypes.CURSOR*/);
            else
            	cs.registerOutParameter(1, Types.OTHER);
            cs.setLong(2, locationId);
            cs.setTimestamp(3, new Timestamp(startMillis));
            cs.setTimestamp(4, new Timestamp(endMillis));
            cs.executeUpdate();
            rs = (ResultSet)cs.getObject(1);
            while(rs.next()) {
                ReconciliationSummaryBean bean = new ReconciliationSummaryBean();
                bean.setAuthName(rs.getString("AUTH_NAME"));
                bean.setAuthServiceTypeId(rs.getInt("AUTH_SERVICE_TYPE_ID"));
                bean.setCampusId(rs.getLong("CAMPUS_ID"));
                bean.setCampusName(rs.getString("CAMPUS_NAME"));
                bean.setPaymentSubtypeKeyId(rs.getLong("PAYMENT_SUBTYPE_KEY_ID"));
                bean.setPaymentSubtypeKeyName(rs.getString("PAYMENT_SUBTYPE_KEY_NAME"));
                bean.setTotalAmount(rs.getBigDecimal("TOT_AMOUNT"));
                bean.setTotalCycles(rs.getLong("TOT_CYCLES"));
                bean.setTotalDryAmount(rs.getBigDecimal("TOT_DRY_AMOUNT"));
                bean.setTotalDryCycles(rs.getLong("TOT_DRY_CYCLES"));
                bean.setTotalWashAmount(rs.getBigDecimal("TOT_WASH_AMOUNT"));
                bean.setTotalWashCycles(rs.getLong("TOT_WASH_CYCLES"));
                bean.setStartMillis(startMillis);
                bean.setEndMillis(endMillis);
                results.add(bean);
            }
        } finally {
            if(rs != null) try { rs.close(); } catch(SQLException e) {}
            if(cs != null) try { cs.close(); } catch(SQLException e) {}
            if(conn != null)try { conn.close(); } catch(SQLException e) {}
        }
            
        return results;
    }

}
