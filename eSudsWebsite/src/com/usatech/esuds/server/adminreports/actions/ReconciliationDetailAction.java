package com.usatech.esuds.server.adminreports.actions;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.adminreports.beans.ReconciliationDetailBean;
import com.usatech.esuds.server.adminreports.forms.ReconciliationDetailForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;

import simple.app.DialectResolver;

/**
 * @version 1.0
 * @author
 */
public class ReconciliationDetailAction extends Action {
    private static Log log = LogFactory.getLog(ReconciliationDetailAction.class);
    private static final String OBJECT_TYPE = SecurityService.LOCATION;
    private static final int APP_ID = 6;
    
    //  Torque is a horrible way to access data - it does not allow group by's, so we'll do this
    // using plain old jdbc
    private List retrieveDetailResults(long locationId, long startMillis, long endMillis, long paymentSubtypeKeyId, String paymentSubtypeKeyName) throws TorqueException, SQLException {
    	String sql = null;
    	if (DialectResolver.isOracle())
    		sql = "{?=call PSS.PKG_REPORTS.GET_RECONCILIATION_DETAIL(?,?,?,?,?)}";
    	else
    		sql = "{?=call PKG_REPORTS.GET_RECONCILIATION_DETAIL(?,?,?,?,?)}";
        Connection conn = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        List results = new ArrayList();
        try {
            conn = Torque.getConnection();
            cs = conn.prepareCall(sql);
            if (DialectResolver.isOracle())
            	cs.registerOutParameter(1, -10 /*oracle.jdbc.driver.OracleTypes.CURSOR*/);
            else
            	cs.registerOutParameter(1, Types.OTHER);
            cs.setLong(2, locationId);
            cs.setTimestamp(3, new Timestamp(startMillis));
            cs.setTimestamp(4, new Timestamp(endMillis));
            cs.setLong(5, paymentSubtypeKeyId);
            cs.setString(6, paymentSubtypeKeyName);
            cs.executeUpdate();
            rs = (ResultSet)cs.getObject(1);
            while(rs.next()) {
                ReconciliationDetailBean bean = new ReconciliationDetailBean();
                bean.setAccountNumber(rs.getString("CONSUMER_ACCT_CD"));
                bean.setAuthName(rs.getString("AUTH_NAME"));
                bean.setAuthServiceTypeId(rs.getInt("AUTH_SERVICE_TYPE_ID"));
                bean.setCampusId(rs.getLong("CAMPUS_ID"));
                bean.setCampusName(rs.getString("CAMPUS_NAME"));
                bean.setConsumerMissing("Y".equalsIgnoreCase(rs.getString("CONSUMER_MISSING")));
                bean.setDormId(rs.getLong("DORM_ID"));
                bean.setDormName(rs.getString("DORM_NAME"));
                bean.setRoomId(rs.getLong("ROOM_ID"));
                bean.setRoomName(rs.getString("ROOM_NAME"));
                bean.setSettledTs(new Date(rs.getTimestamp("SETTLED_TS").getTime()));
                bean.setInitialAmount(rs.getBigDecimal("INITIAL_AMOUNT"));
                bean.setRefundAmount(rs.getBigDecimal("REFUND_AMOUNT"));
                bean.setTotalAmount(rs.getBigDecimal("TOT_AMOUNT"));
                bean.setTranLineItemDesc(rs.getString("TRAN_LINE_ITEM_DESC"));
                bean.setTranStartTs(new Date(rs.getTimestamp("TRAN_START_TS").getTime()));
                bean.setRefundDesc(rs.getString("TRAN_REFUND_DESC"));
                Timestamp refundSettledTs = rs.getTimestamp("TRAN_REFUND_SETTLEMENT_TS");
                if(refundSettledTs != null)
                    bean.setRefundSettledTs(new Date(refundSettledTs.getTime()));
                results.add(bean);
            }
        } finally {
            if(rs != null) try { rs.close(); } catch(SQLException e) {}
            if(cs != null) try { cs.close(); } catch(SQLException e) {}
            if(conn != null)try { conn.close(); } catch(SQLException e) {}
        }
            
        return results;
    }
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();

        ReconciliationDetailForm reportForm = (ReconciliationDetailForm) form;

        long locationId = reportForm.getLocationId();
        log.debug("Reconciliation Detail requested for locationId=" + locationId + "; from " 
                + new Date(reportForm.getStartMillis()) + " to " + new Date(reportForm.getEndMillis()) + " for " 
                + reportForm.getPaymentSubtypeKeyName() + " - " + reportForm.getPaymentSubtypeKeyId());
        
        if (SecurityService.checkCredentials(request, OBJECT_TYPE, locationId, SecurityService.READ)) {
            try {
                List results = retrieveDetailResults(locationId, reportForm.getStartMillis(), reportForm.getEndMillis(), reportForm.getPaymentSubtypeKeyId(), reportForm.getPaymentSubtypeKeyName());
                request.setAttribute("results", results);
                request.setAttribute("startDate", new Date(reportForm.getStartMillis()));
                request.setAttribute("endDate", new Date(reportForm.getEndMillis()));
                request.setAttribute("campusName", reportForm.getCampusName());
                request.setAttribute("authName", reportForm.getAuthName());
            } catch (Exception e) {
                log.error("Unknown error occurred: ", e);
                errors.add("unknown.error", new ActionError("error.unknown"));
            }
        } else {
            throw new NotPermittedException("Not permitted to view this resource.");
        }

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            forward = mapping.findForward("detail");
        }

        return forward;
    }

}
