/*
 * Created on May 6, 2005
 *
 */
package com.usatech.esuds.server.adminreports.beans;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ReconciliationSummaryBean {
    private long campusId;
    private String campusName;
    private int authServiceTypeId;
    private String authName;
    private BigDecimal totalAmount;
    private long totalCycles;
    private BigDecimal totalWashAmount;
    private long totalWashCycles;
    private BigDecimal totalDryAmount;
    private long totalDryCycles;
    private long paymentSubtypeKeyId;
    private String paymentSubtypeKeyName;
    private long startMillis;
    private long endMillis;
    private Map parameters = new HashMap();
    
    public ReconciliationSummaryBean() {
    }

    /**
     * @return Returns the authName.
     */
    public String getAuthName() {
        return authName;
    }

    /**
     * @param authName The authName to set.
     */
    public void setAuthName(String authName) {
        this.authName = authName;
    }

    /**
     * @return Returns the authServiceTypeId.
     */
    public int getAuthServiceTypeId() {
        return authServiceTypeId;
    }

    /**
     * @param authServiceTypeId The authServiceTypeId to set.
     */
    public void setAuthServiceTypeId(int authServiceTypeId) {
        this.authServiceTypeId = authServiceTypeId;
    }

    /**
     * @return Returns the campusId.
     */
    public long getCampusId() {
        return campusId;
    }

    /**
     * @param campusId The campusId to set.
     */
    public void setCampusId(long campusId) {
        this.campusId = campusId;
    }

    /**
     * @return Returns the campusName.
     */
    public String getCampusName() {
        return campusName;
    }

    /**
     * @param campusName The campusName to set.
     */
    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    /**
     * @return Returns the totalAmount.
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount The totalAmount to set.
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return Returns the totalCycles.
     */
    public long getTotalCycles() {
        return totalCycles;
    }

    /**
     * @param totalCycles The totalCycles to set.
     */
    public void setTotalCycles(long totalCycles) {
        this.totalCycles = totalCycles;
    }

    /**
     * @return Returns the totalDryAmount.
     */
    public BigDecimal getTotalDryAmount() {
        return totalDryAmount;
    }

    /**
     * @param totalDryAmount The totalDryAmount to set.
     */
    public void setTotalDryAmount(BigDecimal totalDryAmount) {
        this.totalDryAmount = totalDryAmount;
    }

    /**
     * @return Returns the totalDryCycles.
     */
    public long getTotalDryCycles() {
        return totalDryCycles;
    }

    /**
     * @param totalDryCycles The totalDryCycles to set.
     */
    public void setTotalDryCycles(long totalDryCycles) {
        this.totalDryCycles = totalDryCycles;
    }

    /**
     * @return Returns the totalWashAmount.
     */
    public BigDecimal getTotalWashAmount() {
        return totalWashAmount;
    }

    /**
     * @param totalWashAmount The totalWashAmount to set.
     */
    public void setTotalWashAmount(BigDecimal totalWashAmount) {
        this.totalWashAmount = totalWashAmount;
    }

    /**
     * @return Returns the totalWashCycles.
     */
    public long getTotalWashCycles() {
        return totalWashCycles;
    }

    /**
     * @param totalWashCycles The totalWashCycles to set.
     */
    public void setTotalWashCycles(long totalWashCycles) {
        this.totalWashCycles = totalWashCycles;
    }

    /**
     * @return Returns the paymentSubtypeKeyId.
     */
    public long getPaymentSubtypeKeyId() {
        return paymentSubtypeKeyId;
    }

    /**
     * @param paymentSubtypeKeyId The paymentSubtypeKeyId to set.
     */
    public void setPaymentSubtypeKeyId(long paymentSubtypeKeyId) {
        this.paymentSubtypeKeyId = paymentSubtypeKeyId;
    }

    /**
     * @return Returns the paymentSubtypeKeyName.
     */
    public String getPaymentSubtypeKeyName() {
        return paymentSubtypeKeyName;
    }

    /**
     * @param paymentSubtypeKeyName The paymentSubtypeKeyName to set.
     */
    public void setPaymentSubtypeKeyName(String paymentSubtypeKeyName) {
        this.paymentSubtypeKeyName = paymentSubtypeKeyName;
    }

    public Map getParameters() {
        parameters.put("locationId", new Long(getCampusId()));
        parameters.put("paymentSubtypeKeyId", new Long(getPaymentSubtypeKeyId()));
        parameters.put("paymentSubtypeKeyName", getPaymentSubtypeKeyName());
        parameters.put("startMillis", new Long(getStartMillis()));
        parameters.put("endMillis", new Long(getEndMillis()));
        parameters.put("campusName", getCampusName());
        parameters.put("authName", getAuthName());
        
        return parameters;
    }

    /**
     * @return Returns the endMillis.
     */
    public long getEndMillis() {
        return endMillis;
    }

    /**
     * @param endMillis The endMillis to set.
     */
    public void setEndMillis(long endMillis) {
        this.endMillis = endMillis;
    }

    /**
     * @return Returns the startMillis.
     */
    public long getStartMillis() {
        return startMillis;
    }

    /**
     * @param startMillis The startMillis to set.
     */
    public void setStartMillis(long startMillis) {
        this.startMillis = startMillis;
    }

}
