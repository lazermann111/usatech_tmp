/*
 * Created on May 6, 2005
 *
 */
package com.usatech.esuds.server.adminreports.beans;

import java.math.BigDecimal;
import java.util.Date;

public class ReconciliationDetailBean {
    private long campusId;
    private String campusName;
    private long dormId;
    private String dormName;
    private long roomId;
    private String roomName;
    private int authServiceTypeId;
    private String authName;
    private BigDecimal initialAmount;
    private BigDecimal refundAmount;
    private BigDecimal totalAmount;
    private Date settledTs;
    private Date tranStartTs;
    private String tranLineItemDesc;
    private String accountNumber;
    private String refundDesc;
    private Date refundSettledTs;
    private boolean consumerMissing;
       
    public ReconciliationDetailBean() {
    }

    /**
     * @return Returns the authName.
     */
    public String getAuthName() {
        return authName;
    }

    /**
     * @param authName The authName to set.
     */
    public void setAuthName(String authName) {
        this.authName = authName;
    }

    /**
     * @return Returns the authServiceTypeId.
     */
    public int getAuthServiceTypeId() {
        return authServiceTypeId;
    }

    /**
     * @param authServiceTypeId The authServiceTypeId to set.
     */
    public void setAuthServiceTypeId(int authServiceTypeId) {
        this.authServiceTypeId = authServiceTypeId;
    }

    /**
     * @return Returns the campusId.
     */
    public long getCampusId() {
        return campusId;
    }

    /**
     * @param campusId The campusId to set.
     */
    public void setCampusId(long campusId) {
        this.campusId = campusId;
    }

    /**
     * @return Returns the campusName.
     */
    public String getCampusName() {
        return campusName;
    }

    /**
     * @param campusName The campusName to set.
     */
    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    /**
     * @return Returns the totalAmount.
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount The totalAmount to set.
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return Returns the accountNumber.
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber The accountNumber to set.
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return Returns the settledTs.
     */
    public Date getSettledTs() {
        return settledTs;
    }

    /**
     * @param settledTs The settledTs to set.
     */
    public void setSettledTs(Date settledTs) {
        this.settledTs = settledTs;
    }

    /**
     * @return Returns the tranLineItemDesc.
     */
    public String getTranLineItemDesc() {
        return tranLineItemDesc;
    }

    /**
     * @param tranLineItemDesc The tranLineItemDesc to set.
     */
    public void setTranLineItemDesc(String tranLineItemDesc) {
        this.tranLineItemDesc = tranLineItemDesc;
    }

    /**
     * @return Returns the tranStartTs.
     */
    public Date getTranStartTs() {
        return tranStartTs;
    }

    /**
     * @param tranStartTs The tranStartTs to set.
     */
    public void setTranStartTs(Date tranStartTs) {
        this.tranStartTs = tranStartTs;
    }

    /**
     * @return Returns the dormId.
     */
    public long getDormId() {
        return dormId;
    }

    /**
     * @param dormId The dormId to set.
     */
    public void setDormId(long dormId) {
        this.dormId = dormId;
    }

    /**
     * @return Returns the dormName.
     */
    public String getDormName() {
        return dormName;
    }

    /**
     * @param dormName The dormName to set.
     */
    public void setDormName(String dormName) {
        this.dormName = dormName;
    }

    /**
     * @return Returns the roomId.
     */
    public long getRoomId() {
        return roomId;
    }

    /**
     * @param roomId The roomId to set.
     */
    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    /**
     * @return Returns the roomName.
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * @param roomName The roomName to set.
     */
    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    /**
     * @return Returns the initialAmount.
     */
    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    /**
     * @param initialAmount The initialAmount to set.
     */
    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    /**
     * @return Returns the refundAmount.
     */
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    /**
     * @param refundAmount The refundAmount to set.
     */
    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    /**
     * @return Returns the refundDesc.
     */
    public String getRefundDesc() {
        return refundDesc;
    }

    /**
     * @param refundDesc The refundDesc to set.
     */
    public void setRefundDesc(String refundDesc) {
        this.refundDesc = refundDesc;
    }

    /**
     * @return Returns the refundSettledTs.
     */
    public Date getRefundSettledTs() {
        return refundSettledTs;
    }

    /**
     * @param refundSettledTs The refundSettledTs to set.
     */
    public void setRefundSettledTs(Date refundSettledTs) {
        this.refundSettledTs = refundSettledTs;
    }

    public boolean isConsumerMissing() {
        return consumerMissing;
    }

    public void setConsumerMissing(boolean consumerMissing) {
        this.consumerMissing = consumerMissing;
    }

}
