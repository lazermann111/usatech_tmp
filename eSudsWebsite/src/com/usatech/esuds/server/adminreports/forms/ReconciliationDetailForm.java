package com.usatech.esuds.server.adminreports.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReconciliationDetailForm extends ActionForm
{
	private long startMillis = 0;
	private long endMillis = 0;
	private long paymentSubtypeKeyId = 0;
	private String paymentSubtypeKeyName = null;
	private long locationId = 0;
    private String action = null;
    private String campusName;
    private String authName;
    
	/**
	* Constructor
	*/
	public ReconciliationDetailForm()
	{

		super();

	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		// Reset values are provided as samples only. Change as appropriate.

		startMillis = 0;
		endMillis = 0;
        paymentSubtypeKeyId = 0;
        paymentSubtypeKeyName = null;
        locationId = 0;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

    /**
     * @return Returns the locationId.
     */
    public long getLocationId() {
        return locationId;
    }

    /**
     * @param locationId The locationId to set.
     */
    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    /**
     * @return Returns the paymentSubtypeKeyId.
     */
    public long getPaymentSubtypeKeyId() {
        return paymentSubtypeKeyId;
    }
    /**
     * @param paymentSubtypeKeyId The paymentSubtypeKeyId to set.
     */
    public void setPaymentSubtypeKeyId(long paymentSubtypeKeyId) {
        this.paymentSubtypeKeyId = paymentSubtypeKeyId;
    }
    /**
     * @return Returns the paymentSubtypeKeyName.
     */
    public String getPaymentSubtypeKeyName() {
        return paymentSubtypeKeyName;
    }
    /**
     * @param paymentSubtypeKeyName The paymentSubtypeKeyName to set.
     */
    public void setPaymentSubtypeKeyName(String paymentSubtypeKeyName) {
        this.paymentSubtypeKeyName = paymentSubtypeKeyName;
    }
    /**
     * @return Returns the endMillis.
     */
    public long getEndMillis() {
        return endMillis;
    }
    /**
     * @param endMillis The endMillis to set.
     */
    public void setEndMillis(long endMillis) {
        this.endMillis = endMillis;
    }
    /**
     * @return Returns the startMillis.
     */
    public long getStartMillis() {
        return startMillis;
    }
    /**
     * @param startMillis The startMillis to set.
     */
    public void setStartMillis(long startMillis) {
        this.startMillis = startMillis;
    }
    /**
     * @return Returns the authName.
     */
    public String getAuthName() {
        return authName;
    }
    /**
     * @param authName The authName to set.
     */
    public void setAuthName(String authName) {
        this.authName = authName;
    }
    /**
     * @return Returns the campusName.
     */
    public String getCampusName() {
        return campusName;
    }
    /**
     * @param campusName The campusName to set.
     */
    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

}
