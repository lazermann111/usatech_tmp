/*
 * Created on Dec 19, 2003
 * Created by melissa
 *
 */
package com.usatech.esuds.server.operatoraccess.beans;

/**
 * @author melissa
 *
 * 
 */
public class NavigationBean
{
	private boolean contactInfoSelected = false;
	private boolean locationsSelected = false;
	private boolean roomControllersSelected = false;

	public NavigationBean()
	{
	}
	
	public void reset()
	{
		contactInfoSelected = false;
		locationsSelected = false;
		roomControllersSelected = false;
	}

	/**
	 * @return
	 */
	public boolean isContactInfoSelected()
	{
		return contactInfoSelected;
	}

	/**
	 * @return
	 */
	public boolean isLocationsSelected()
	{
		return locationsSelected;
	}

	/**
	 * @return
	 */
	public boolean isRoomControllersSelected()
	{
		return roomControllersSelected;
	}

	/**
	 * @param b
	 */
	public void setContactInfoSelected(boolean b)
	{
		contactInfoSelected = b;
	}

	/**
	 * @param b
	 */
	public void setLocationsSelected(boolean b)
	{
		locationsSelected = b;
	}

	/**
	 * @param b
	 */
	public void setRoomControllersSelected(boolean b)
	{
		roomControllersSelected = b;
	}

}
