/*
 * Created on Jan 19, 2004
 * Created by melissa
 *
 */
package com.usatech.esuds.server.operatoraccess.beans;

/**
 * @author melissa
 *
 * 
 */
public class LocationNavigationBean
{
	private long campusId;
	private String campusName;
	private long dormId;
	private String dormName;
	private long laundryRoomId;
	private String LaundryRoomName;
	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public String getCampusName()
	{
		return campusName;
	}

	/**
	 * @return
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * @return
	 */
	public String getDormName()
	{
		return dormName;
	}

	/**
	 * @return
	 */
	public long getLaundryRoomId()
	{
		return laundryRoomId;
	}

	/**
	 * @return
	 */
	public String getLaundryRoomName()
	{
		return LaundryRoomName;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
	}

	/**
	 * @param string
	 */
	public void setCampusName(String string)
	{
		campusName = string;
	}

	/**
	 * @param l
	 */
	public void setDormId(long l)
	{
		dormId = l;
	}

	/**
	 * @param string
	 */
	public void setDormName(String string)
	{
		dormName = string;
	}

	/**
	 * @param l
	 */
	public void setLaundryRoomId(long l)
	{
		laundryRoomId = l;
	}

	/**
	 * @param string
	 */
	public void setLaundryRoomName(String string)
	{
		LaundryRoomName = string;
	}

}
