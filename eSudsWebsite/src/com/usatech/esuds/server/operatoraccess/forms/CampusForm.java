package com.usatech.esuds.server.operatoraccess.forms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.util.LocationUtil;

/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class CampusForm extends ActionForm
{
	private long campusId = -1;
	private long schoolId = -1;
	private String name = null;
	private String addr1 = null;
	private String addr2 = null;
	private String city = null;
	private String stateCd = null;
	private String postalCd= null;
	private String countryCd = null;
	private String timeZoneCd = null;
	private boolean newCampus = true;
	private boolean active = true;
	private List states = null;
	private List countries = null;
	private List timeZones = null;
	//private List schools = null;
	private String action = "show";
	
	public CampusForm()
	{
		try
		{
			states = LocationUtil.getStates();
			countries = LocationUtil.getCountries();
			timeZones = LocationUtil.getTimeZones();
		}
		catch (DataException e)
		{
			// Ignore - this shouldn't happen unless we have database problems
		}
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		campusId = -1;
		schoolId = -1;
		name = "";
		addr1 = "";
		addr2 = "";
		city = "";
		stateCd = "";
		postalCd= "";
		countryCd = "";
		timeZoneCd = "";
		newCampus = true;
		action = "show";
		
		try
		{
			states = LocationUtil.getStates();
			countries = LocationUtil.getCountries();
			timeZones = LocationUtil.getTimeZones();
			//schools = LocationUtil.getSchools();
		}
		catch (DataException e)
		{
			// Ignore - this shouldn't happen unless we have database problems
		}
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();

		if (action.equalsIgnoreCase("save") && ((name == null) || (name.length() == 0)))
		{
			errors.add("name", new org.apache.struts.action.ActionError("error.name.required"));
		}
		
		if (campusId != -1)
		{
			setNewCampus(false);
		}

		return errors;

	}
	/**
	 * @return
	 */
	public String getAddr1()
	{
		return addr1;
	}

	/**
	 * @return
	 */
	public String getAddr2()
	{
		return addr2;
	}

	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @return
	 */
	public String getCountryCd()
	{
		return countryCd;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public String getPostalCd()
	{
		return postalCd;
	}

	/**
	 * @return
	 */
	public long getSchoolId()
	{
		return schoolId;
	}

	/**
	 * @return
	 */
	public String getStateCd()
	{
		return stateCd;
	}

	/**
	 * @return
	 */
	public String getTimeZoneCd()
	{
		return timeZoneCd;
	}

	/**
	 * @param string
	 */
	public void setAddr1(String string)
	{
		addr1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddr2(String string)
	{
		addr2 = string;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
		
		if (campusId != -1)
		{
			newCampus = false;
		}
	}

	/**
	 * @param string
	 */
	public void setCity(String string)
	{
		city = string;
	}

	/**
	 * @param string
	 */
	public void setCountryCd(String string)
	{
		countryCd = string;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param string
	 */
	public void setPostalCd(String string)
	{
		postalCd = string;
	}

	/**
	 * @param l
	 */
	public void setSchoolId(long l)
	{
		schoolId = l;
	}

	/**
	 * @param string
	 */
	public void setStateCd(String string)
	{
		stateCd = string;
	}

	/**
	 * @param string
	 */
	public void setTimeZoneCd(String string)
	{
		timeZoneCd = string;
	}

	/**
	 * @return
	 */
	public boolean isNewCampus()
	{
		return newCampus;
	}

	/**
	 * @param b
	 */
	public void setNewCampus(boolean b)
	{
		newCampus = b;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @return
	 */
	public List getCountries()
	{
		return countries;
	}

	/**
	 * @return
	 */
	public List getStates()
	{
		return states;
	}

	/**
	 * @return
	 */
	public List getTimeZones()
	{
		return timeZones;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	/**
	 * @return
	 *//*
	public List getSchools()
	{
		return schools;
	}
*/
	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}

}
