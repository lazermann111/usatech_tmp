package com.usatech.esuds.server.operatoraccess.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 2 fields on this form:
 * <ul>
 * <li>type - [your comment here]
 * <li>laundryRoomId - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class LaundryRoomForm extends ActionForm
{

	private String name = null;
	private long laundryRoomId = -1;
	private long dormId = -1;
	private String action ="show";
	private boolean newLaundryRoom = true;
	private boolean active = true;
	/**
	 * Get type
	 * @return String
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Set type
	 * @param <code>String</code>
	 */
	public void setName(String n)
	{
		this.name = n;
	}

	/**
	 * Get laundryRoomId
	 * @return long
	 */
	public long getLaundryRoomId()
	{
		return laundryRoomId;
	}

	/**
	 * Set laundryRoomId
	 * @param <code>long</code>
	 */
	public void setLaundryRoomId(long i)
	{
		this.laundryRoomId = i;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		laundryRoomId = -1;
		name = "";
		dormId = -1;
		newLaundryRoom = true;
		action = "show";

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();

		if (action.equalsIgnoreCase("save") && ((name == null) || (name.length() == 0)))
		{
			errors.add("name", new org.apache.struts.action.ActionError("error.name.required"));
		}
		
		if (laundryRoomId != -1)
		{
			newLaundryRoom = false;
		}
		
		return errors;

	}

	/**
	 * @return
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * @param l
	 */
	public void setDormId(long l)
	{
		dormId = l;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @return
	 */
	public boolean isNewLaundryRoom()
	{
		return newLaundryRoom;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	/**
	 * @param b
	 */
	public void setNewLaundryRoom(boolean b)
	{
		newLaundryRoom = b;
	}

	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}

}
