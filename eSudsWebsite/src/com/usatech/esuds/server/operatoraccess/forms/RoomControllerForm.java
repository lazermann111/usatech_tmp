package com.usatech.esuds.server.operatoraccess.forms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author
 */
public class RoomControllerForm extends ActionForm
{

	private long roomControllerId;
	private long posId;
	private long dormId =-1;
	private List dorms = null;
	private long campusId = -1;
	private List campuses = null;
	private String serialNumber = null;
	private long laundryRoomId = -1;
	private List laundryRooms = null;
	private boolean locationAssigned = false;
	private String action = null;

	/**
	 * 
	 */
	public RoomControllerForm()
	{
		super();
	}

	public long getDormId()
	{
		return dormId;
	}

	public void setDormId(long d)
	{
		dormId = d;
	}
	
	public long getCampusId()
	{
		return campusId;
	}

	public void setCampusId(long c)
	{
		campusId = c;
	}

	/**
	 * Get serialNumber
	 * @return String
	 */
	public String getSerialNumber()
	{
		return serialNumber;
	}

	/**
	 * Set serialNumber
	 * @param <code>String</code>
	 */
	public void setSerialNumber(String s)
	{
		this.serialNumber = s;
	}

	/**
	 * Get laundryRoom
	 * @return String[]
	 */
	public long getLaundryRoomId()
	{
		return laundryRoomId;
	}

	/**
	 * Set laundryRoom
	 * @param <code>String[]</code>
	 */
	public void setLaundryRoomId(long l)
	{
		laundryRoomId = l;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		dormId= -1;
		dorms = null;
		roomControllerId = -1;
		campusId = -1;
		campuses = null;
		serialNumber = null;
		laundryRoomId = -1;
		laundryRooms = null;
		locationAssigned = false;
		action="show";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();
		
		if (posId > 0)
		{
		}
		
		if (action != null && action.equals("save"))
		{
			if (campusId == 1)
			{
				errors.add("campus", new ActionError("error.campus.required"));
			}
			
			if (dormId == 1)
			{
				errors.add("dorm", new ActionError("error.dorm.required"));
			}
			
			if (laundryRoomId == 1)
			{
				errors.add("laundryRoom", new ActionError("error.laundry.room.required"));
			}
		}
		
		return errors;

	}
	/**
	 * @return
	 */
	public List getCampuses()
	{
		return campuses;
	}

	/**
	 * @return
	 */
	public List getDorms()
	{
		return dorms;
	}

	/**
	 * @return
	 */
	public List getLaundryRooms()
	{
		return laundryRooms;
	}

	/**
	 * @param list
	 */
	public void setCampuses(List list)
	{
		campuses = list;
	}

	/**
	 * @param list
	 */
	public void setDorms(List list)
	{
		dorms = list;
	}

	/**
	 * @param list
	 */
	public void setLaundryRooms(List list)
	{
		laundryRooms = list;
	}

	/**
	 * @return
	 */
	public long getRoomControllerId()
	{
		return roomControllerId;
	}

	/**
	 * @param l
	 */
	public void setRoomControllerId(long l)
	{
		roomControllerId = l;
	}

	/**
	 * @return
	 */
	public long getPosId()
	{
		return posId;
	}

	/**
	 * @param l
	 */
	public void setPosId(long l)
	{
		posId = l;
	}

	/**
	 * @return
	 */
	public boolean isLocationAssigned()
	{
		return locationAssigned;
	}

	/**
	 * @param b
	 */
	public void setLocationAssigned(boolean b)
	{
		locationAssigned = b;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String _action)
	{
		action = _action;
	}

}
