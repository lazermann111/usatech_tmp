package com.usatech.esuds.server.operatoraccess.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class DormForm extends ActionForm
{
	private long dormId = -1;
	private long campusId = -1;
	private String campusName;
	private String name = null;
	private String addr1 = null;
	private String addr2 = null;
	private String city = null;
	private String county = null;
	private String stateCd = null;
	private String postalCd= null;
	private String countryCd = null;
	private String action = "show";
	private boolean newDorm = true;
	private boolean active = true;

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		dormId = -1;
		name = "";
		addr1 = "";
		addr2 = "";
		city = "";
		county = "";
		stateCd = "";
		postalCd= "";
		countryCd = "";
		action = "show";
		newDorm = true;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();

		if (action.equalsIgnoreCase("save") && ((name == null) || (name.length() == 0)))
		{
			errors.add("name", new org.apache.struts.action.ActionError("error.name.required"));
		}
		
		if (dormId != -1)
		{
			setNewDorm(false);
		}

		return errors;

	}
	/**
	 * @return
	 */
	public String getAddr1()
	{
		return addr1;
	}

	/**
	 * @return
	 */
	public String getAddr2()
	{
		return addr2;
	}

	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @return
	 */
	public String getCountryCd()
	{
		return countryCd;
	}

	/**
	 * @return
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public String getPostalCd()
	{
		return postalCd;
	}

	/**
	 * @return
	 */
	public String getStateCd()
	{
		return stateCd;
	}

	/**
	 * @param string
	 */
	public void setAddr1(String string)
	{
		addr1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddr2(String string)
	{
		addr2 = string;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
	}

	/**
	 * @param string
	 */
	public void setCity(String string)
	{
		city = string;
	}

	/**
	 * @param string
	 */
	public void setCountryCd(String string)
	{
		countryCd = string;
	}

	/**
	 * @param l
	 */
	public void setDormId(long l)
	{
		dormId = l;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param string
	 */
	public void setPostalCd(String string)
	{
		postalCd = string;
	}


	/**
	 * @param string
	 */
	public void setStateCd(String string)
	{
		stateCd = string;
	}

	/**
	 * @return
	 */
	public String getCounty()
	{
		return county;
	}

	/**
	 * @param string
	 */
	public void setCounty(String string)
	{
		county = string;
	}

	/**
	 * @return
	 */
	public String getCampusName()
	{
		return campusName;
	}

	/**
	 * @param string
	 */
	public void setCampusName(String string)
	{
		campusName = string;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @return
	 */
	public boolean isNewDorm()
	{
		return newDorm;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	/**
	 * @param b
	 */
	public void setNewDorm(boolean b)
	{
		newDorm = b;
	}

	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}

}
