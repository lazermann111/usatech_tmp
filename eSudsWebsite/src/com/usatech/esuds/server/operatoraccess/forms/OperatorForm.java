package com.usatech.esuds.server.operatoraccess.forms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.util.LocationUtil;

/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class OperatorForm extends ActionForm
{
	private long operatorId = -1;
	private String name = null;
	private String addr1 = null;
	private String addr2 = null;
	private String city = null;
	private String county = null;
	private String stateCd = null;
	private String postalCd= null;
	private String countryCd = null;
	private boolean newOperator = true;
	private List states = null;
	private List countries = null;
	private List timeZones = null;
	private String action = "show";
	
	public OperatorForm()
	{
		try
		{
			states = LocationUtil.getStates();
			countries = LocationUtil.getCountries();
			timeZones = LocationUtil.getTimeZones();
		}
		catch (DataException e)
		{
			// Ignore - this shouldn't happen unless we have database problems
		}
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		// Reset field values here.
		operatorId = -1;
		name = null;
		addr1 = null;
		addr2 = null;
		city = null;
		county = null;
		stateCd = null;
		postalCd= null;
		countryCd = null;
		newOperator = true;
		action = "show";
		
		/*
		try
		{
			states = LocationUtil.getStates();
			countries = LocationUtil.getCountries();
			timeZones = LocationUtil.getTimeZones();
		}
		catch (DataException e)
		{
			// Ignore - this shouldn't happen unless we have database problems
		}
		*/
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();

		if (action.equalsIgnoreCase("save") && ((name == null) || (name.length() == 0)))
		{
			errors.add("name", new org.apache.struts.action.ActionError("error.name.required"));
		}
		
		if (operatorId != -1)
		{
			setNewOperator(false);
		}
		
		return errors;

	}

	/**
	 * @return
	 */
	public String getAddr1()
	{
		return addr1;
	}

	/**
	 * @return
	 */
	public String getAddr2()
	{
		return addr2;
	}

	/**
	 * @return
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @return
	 */
	public String getCountryCd()
	{
		return countryCd;
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return
	 */
	public long getOperatorId()
	{
		return operatorId;
	}

	/**
	 * @return
	 */
	public String getPostalCd()
	{
		return postalCd;
	}

	/**
	 * @return
	 */
	public String getStateCd()
	{
		return stateCd;
	}

	/**
	 * @param string
	 */
	public void setAddr1(String string)
	{
		addr1 = string;
	}

	/**
	 * @param string
	 */
	public void setAddr2(String string)
	{
		addr2 = string;
	}

	/**
	 * @param string
	 */
	public void setCity(String string)
	{
		city = string;
	}

	/**
	 * @param string
	 */
	public void setCountryCd(String string)
	{
		countryCd = string;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	/**
	 * @param l
	 */
	public void setOperatorId(long l)
	{
		operatorId = l;
	}

	/**
	 * @param string
	 */
	public void setPostalCd(String string)
	{
		postalCd = string;
	}

	/**
	 * @param string
	 */
	public void setStateCd(String string)
	{
		stateCd = string;
	}

	/**
	 * @return
	 */
	public String getCounty()
	{
		return county;
	}

	/**
	 * @param string
	 */
	public void setCounty(String string)
	{
		county = string;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @return
	 */
	public List getCountries()
	{
		return countries;
	}

	/**
	 * @return
	 */
	public List getStates()
	{
		return states;
	}

	/**
	 * @return
	 */
	public List getTimeZones()
	{
		return timeZones;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

	/**
	 * @return
	 */
	public boolean isNewOperator()
	{
		return newOperator;
	}

	/**
	 * @param b
	 */
	public void setNewOperator(boolean b)
	{
		newOperator = b;
	}

}
