package com.usatech.esuds.server.operatoraccess.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class RoomControllerSearchForm extends ActionForm
{
	private long operatorId;
	private String[] campus = null;
	private String[] dorm = null;
	private String[] laundryRoom = null;
	private String[] roomController = null;
	
	private String changed = null;
	
	/**
	 * Get laundryRoom
	 * @return String[]
	 */
	public String[] getLaundryRoom()
	{
		return laundryRoom;
	}

	/**
	 * Set laundryRoom
	 * @param <code>String[]</code>
	 */
	public void setLaundryRoom(String[] s)
	{
		laundryRoom = s;
	}
	/**
	 * Get campus
	 * @return String[]
	 */
	public String[] getCampus()
	{
		return campus;
	}

	/**
	 * Set campus
	 * @param <code>String[]</code>
	 */
	public void setCampus(String[] c)
	{
		campus = c;
	}
	/**
	 * Get dorm
	 * @return String[]
	 */
	public String[] getDorm()
	{
		return dorm;
	}

	/**
	 * Set dorm
	 * @param <code>String[]</code>
	 */
	public void setDorm(String[] d)
	{
		dorm = d;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		roomController = null;
		laundryRoom = null;
		dorm = null;
		campus = null;
		changed = null;

	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{	
		if (changed != null && changed.equals("campus"))
		{
			dorm = null;
			laundryRoom = null;
			roomController = null;
			changed = null;						
		}
		
		if (changed != null && changed.equals("dorm"))
		{
			laundryRoom = null;
			roomController = null;
			changed = null;
		}
		
		if (changed != null && changed.equals("laundryRoom"))
		{
			roomController = null;
			changed = null;
		}
		
		return null;
	}
	/**
	 * @return
	 */
	public String getChanged()
	{
		return changed;
	}

	/**
	 * @param string
	 */
	public void setChanged(String string)
	{
		changed = string;
	}

	/**
	 * @return
	 */
	public String[] getRoomController()
	{
		return roomController;
	}

	/**
	 * @param strings
	 */
	public void setRoomController(String[] strings)
	{
		roomController = strings;
	}

	/**
	 * @return
	 */
	public long getOperatorId()
	{
		return operatorId;
	}

	/**
	 * @param l
	 */
	public void setOperatorId(long l)
	{
		operatorId = l;
	}

}
