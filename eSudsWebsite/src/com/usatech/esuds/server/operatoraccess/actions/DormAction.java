package com.usatech.esuds.server.operatoraccess.actions;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.beans.LaundryRoomBean;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.operatoraccess.forms.DormForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
/**
 * @version 	1.0
 * @author
 */
public class DormAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(DormAction.class);
	
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward save(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
			
		DormForm dormForm = (DormForm) form;
		long dormId = dormForm.getDormId();
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Dorm dorm = null;
				if (dormForm.isNewDorm())
				{
					dorm = new Dorm();
				}
				else
				{
					dorm = Dorm.retrieveDorm(dormId);
				}
				
				BeanUtils.copyProperties(dorm, dormForm);
				
				dorm.save();
				
				// copy properties back to form
				BeanUtils.copyProperties(dormForm, dorm);
				
				List laundryRooms = dorm.getLaundryRooms();
				List laundryRoomBeans = BeanUtil.convertList(laundryRooms, LaundryRoomBean.class, "show");
				request.setAttribute("laundryRooms", laundryRoomBeans);
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("save.successful", "Residence Hall", dorm.getName()));

				// for navigation
				DormBean dormBean = getNavigation(dorm);
				request.setAttribute("dormBean", dormBean);
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.dorm"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			if (dormForm.isNewDorm())
			{
				throw new NotPermittedException("Not permitted to create this resource.");
			}
			else
			{
				throw new NotPermittedException("Not permitted to modify this resource.");
			}
		}
			
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("success");
		}
			
		return forward;
	}
	
	public ActionForward create(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		DormForm dormForm = (DormForm) form;
		
		// get the operatorId out of session
		long operatorId = getOperatorId(request);
		
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				// set dormId to -1 for New
				dormForm.setDormId(-1);
				dormForm.setNewDorm(true);
				dormForm.setActive(true);
				request.setAttribute("laundryRooms", new ArrayList());
			
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			// handle not permitted
			throw new NotPermittedException("Not permitted to create this resource.");
		}
	
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
		
	public ActionForward show(
				ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		DormForm dormForm = (DormForm) form;
		long campusId = dormForm.getCampusId();
		long dormId = dormForm.getDormId();
		long operatorId = getOperatorId(request);
		
		// If no operatorId in form, check session
		if (operatorId < 1)
		{
			operatorId = Long.parseLong((String) request.getSession(false).getAttribute("operatorId"));
		}
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Dorm dorm = Dorm.retrieveDorm(dormId);
				
				if (dorm.getOperatorId() == operatorId)
				{
					BeanUtils.copyProperties(dormForm, dorm);
					List laundryRooms = dorm.getLaundryRooms();
					List laundryRoomBeans = BeanUtil.convertList(laundryRooms, LaundryRoomBean.class, "show");
					request.setAttribute("laundryRooms", laundryRoomBeans);
					
					// for navigation
					DormBean dormBean = getNavigation(dorm);
					request.setAttribute("dormBean", dormBean);
				}
				else
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.dorm"));
				}
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.dorm"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward setInactive(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
			
		DormForm dormForm = (DormForm) form;
		long dormId = dormForm.getDormId();
		long campusId = dormForm.getCampusId();
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Dorm dorm = Dorm.retrieveDorm(dormId);
				List laundryRooms = dorm.getLaundryRooms();
				if (laundryRooms.isEmpty() || laundryRooms == null)
				{
					dorm.setActive(false);							
					dorm.save();
					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("delete.successful", "Residence Hall", dorm.getName()));
					request.setAttribute("campusId", new Long(campusId));
				}
				else
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.dependent.locations"));
				}
				
				// for navigation
				DormBean dormBean = getNavigation(dorm);
				request.setAttribute("dormBean", dormBean);
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.dorm"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to change this dorm.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("deleted");
		}
		
		return forward;
	}
	
	public ActionForward delete(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		// delete is the same as set inactive
		return setInactive(mapping, form, request, response);
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session != null && session.getAttribute("operatorId") != null)
		{
			return Long.parseLong((String) session.getAttribute("operatorId"));
		}
		else return (new Long(0)).longValue();
	}
	
	private static DormBean getNavigation(Dorm dorm) throws IllegalAccessException, InvocationTargetException
	{
		DormBean dormBean = new DormBean();
		BeanUtils.copyProperties(dormBean, dorm);
		CampusBean campusBean = new CampusBean();
		BeanUtils.copyProperties(campusBean, dorm.getCampus());
		dormBean.setCampusBean(campusBean);
		return dormBean;
	}
}
