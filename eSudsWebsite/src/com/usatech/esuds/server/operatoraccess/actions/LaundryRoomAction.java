/*
 * *******************************************************************
 * 
 * File LaundryRoomAction.java
 * 
 * Created on Apr 7, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.operatoraccess.actions;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.beans.LaundryRoomBean;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.operatoraccess.forms.LaundryRoomForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;

public class LaundryRoomAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(LaundryRoomAction.class);
	
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward create(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		LaundryRoomForm laundryRoomForm = (LaundryRoomForm) form;
		
		long operatorId = getOperatorId(request);
	
		try
		{
			if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
			{		
				// set laundryRoomId to -1 for New
				laundryRoomForm.setLaundryRoomId(-1);
				laundryRoomForm.setNewLaundryRoom(true);
				laundryRoomForm.setActive(true);
			}
			else
			{
				// handle not permitted
				throw new NotPermittedException("Not permitted to create this resource.");
			}
		}
		// Add more exception here?
		catch (Exception e)
		{
			log.error("An unexpected error has occurred: " + e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
		}
	
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
		

	public ActionForward save(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
			
		LaundryRoomForm laundryRoomForm = (LaundryRoomForm) form;
		long laundryRoomId = laundryRoomForm.getLaundryRoomId();
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				LaundryRoom laundryRoom = null;
				if (laundryRoomForm.isNewLaundryRoom())
				{
					laundryRoom = new LaundryRoom();
				}
				else
				{
					laundryRoom = LaundryRoom.retrieveLaundryRoom(laundryRoomId);
				}
				
				BeanUtils.copyProperties(laundryRoom, laundryRoomForm);
				
				laundryRoom.save();
				BeanUtils.copyProperties(laundryRoomForm, laundryRoom);
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("save.successful", "Laundry Room", laundryRoom.getName()));
				
				// for navigation
				LaundryRoomBean laundryRoomBean = getNavigation(laundryRoom);
				request.setAttribute("laundryRoomBean", laundryRoomBean);
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.laundryRoom"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			if (laundryRoomForm.isNewLaundryRoom())
			{
				throw new NotPermittedException("Not permitted to create this resource.");
			}
			else
			{
				throw new NotPermittedException("Not permitted to modify this resource.");
			}
		}
			
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("success");
		}
			
		return forward;
	}
	

	public ActionForward show(
				ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		LaundryRoomForm laundryRoomForm = (LaundryRoomForm) form;
		long laundryRoomId = laundryRoomForm.getLaundryRoomId();
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				LaundryRoom laundryRoom = LaundryRoom.retrieveLaundryRoom(laundryRoomId);
				if (laundryRoom.getOperatorId() == operatorId)
				{
					BeanUtils.copyProperties(laundryRoomForm, laundryRoom);
					
					// for navigation
					LaundryRoomBean laundryRoomBean = getNavigation(laundryRoom);
					request.setAttribute("laundryRoomBean", laundryRoomBean);
				}
				else
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.laundryRoom"));
				}
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.laundryRoom"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward setInactive(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();

		LaundryRoomForm laundryRoomForm = (LaundryRoomForm) form;
		long laundryRoomId = laundryRoomForm.getLaundryRoomId();
		long operatorId = getOperatorId(request);
		long dormId = laundryRoomForm.getDormId();

		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			LaundryRoom laundryRoom = null;
			
			try
			{
				laundryRoom = LaundryRoom.retrieveLaundryRoom(laundryRoomId);
				laundryRoom.setActive(false);
				laundryRoom.save();
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("delete.successful", "Laundry Room", laundryRoom.getName()));
				request.setAttribute("dormId", new Long(dormId));
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.laundryRoom"));
			}
			catch (DataAccessException dae)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (DataException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to change this laundry room.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("deleted");
		}
	  
		return forward;
	}
	
	public ActionForward delete(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		// delete is the same as set inactive
		return setInactive(mapping, form, request, response);
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session != null && session.getAttribute("operatorId") != null)
		{
			return Long.parseLong((String) session.getAttribute("operatorId"));
		}
		else return (new Long(0)).longValue();
	}
	
	private static LaundryRoomBean getNavigation(LaundryRoom laundryRoom) throws IllegalAccessException, InvocationTargetException
	{
		LaundryRoomBean laundryRoomBean = new LaundryRoomBean();
		BeanUtils.copyProperties(laundryRoomBean, laundryRoom);
		DormBean dormBean = new DormBean();
		BeanUtils.copyProperties(dormBean, laundryRoom.getDorm());
		CampusBean campusBean = new CampusBean();
		BeanUtils.copyProperties(campusBean, laundryRoom.getDorm().getCampus());
		dormBean.setCampusBean(campusBean);
		laundryRoomBean.setDormBean(dormBean);
		return laundryRoomBean;
	}
}
