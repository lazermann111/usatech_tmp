package com.usatech.esuds.server.operatoraccess.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.beans.RoomControllerBean;
import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.POS;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.operatoraccess.forms.RoomControllerForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
/**
 * @version 	1.0
 * @author
 */
public class RoomControllerAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(CampusAction.class);
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward save(
				ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response)
				throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
	
		RoomControllerForm roomControllerForm = (RoomControllerForm) form;
		long operatorId = getOperatorId(request);
		long posId = roomControllerForm.getPosId();
		long laundryRoomId = roomControllerForm.getLaundryRoomId();
		boolean assigned = roomControllerForm.isLocationAssigned();
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				if (!assigned && laundryRoomId != POS.UNKNOWN_LOCATION_ID)
				{
					POS pos = POS.retrievePOS(posId);
					pos.setLaundryRoomId(laundryRoomId);
					pos.save();
					roomControllerForm.setLocationAssigned(true);
				}
				else
				{
					// not implemented
				}
			}
			catch (DataAccessException dae)
			{
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (DataException de)
			{
				errors.add("data.error", new ActionError("error.data"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: " + e.getMessage());
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("saveSuccess");
		}
			
		return forward;
	}
	
	public ActionForward show(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		RoomControllerForm roomControllerForm = (RoomControllerForm) form;
		long operatorId = getOperatorId(request);
		long posId = roomControllerForm.getPosId();
		long campusId = roomControllerForm.getCampusId();
		long dormId = roomControllerForm.getDormId();
		long laundryRoomId = roomControllerForm.getLaundryRoomId();
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				POS pos = POS.retrievePOS(posId);
				BeanUtils.copyProperties(roomControllerForm, pos);
				boolean assigned = roomControllerForm.isLocationAssigned();
				
				if (!assigned)
				{	
					// check for valid locations
					if (laundryRoomId > 1)
					{
						LaundryRoom room = LaundryRoom.retrieveLaundryRoom(laundryRoomId);
						if (room.getDorm().getCampusId() != campusId)
						{
							roomControllerForm.setLaundryRoomId(-1);
							roomControllerForm.setLaundryRooms(null); 
						}
						
					}
					
					if (dormId > 1)
					{
						Dorm dorm = Dorm.retrieveDorm(dormId);
						if (dorm.getCampusId() != campusId)
						{
							roomControllerForm.setDormId(-1);
							roomControllerForm.setDorms(null);
						}
						else
						{
							roomControllerForm.setLaundryRooms(LaundryRoom.retrieveByDormId(dormId));
						}
					}
					
					if (campusId > 0)
					{
						roomControllerForm.setDorms(BeanUtil.convertList(Dorm.retrieveByCampusId(campusId), DormBean.class));
					}
					
					roomControllerForm.setCampuses(BeanUtil.convertList(Campus.retrieveByOperatorId(operatorId), CampusBean.class));
				}
				else
				{
					// not implemented
				}
			}
			catch (DataAccessException dae)
			{
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (DataException de)
			{
				errors.add("data.error", new ActionError("error.data"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: " + e.getMessage());
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("info");
		}
		
		return forward;
	}
	
	public ActionForward retrieveList(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		RoomControllerForm roomControllerForm = (RoomControllerForm) form;
		long operatorId = getOperatorId(request);
		boolean assigned = roomControllerForm.isLocationAssigned();

		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				if (!assigned)
				{
					List list = POS.retrieveActivePOS(operatorId, POS.UNKNOWN_LOCATION_ID);
					request.setAttribute("roomControllers", BeanUtil.convertList(list, RoomControllerBean.class, "show"));
				}
				else
				{
					// not implemented
				}
			}
			catch (DataAccessException dae)
			{
				log.error("Date Access Exception occurred: ", dae);
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: " + e.getMessage(), e);
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
			
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("list");
		}
		
		return forward;
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session != null && session.getAttribute("operatorId") != null)
		{
			return Long.parseLong((String) session.getAttribute("operatorId"));
		}
		else return (new Long(0)).longValue();
	}
}
