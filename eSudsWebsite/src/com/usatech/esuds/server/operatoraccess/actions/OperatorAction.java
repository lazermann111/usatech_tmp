package com.usatech.esuds.server.operatoraccess.actions;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.OperatorBean;
import com.usatech.esuds.server.model.Operator;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.operatoraccess.forms.OperatorForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
/**
 * @version 	1.0
 * @author
 */
public class OperatorAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(OperatorAction.class);
	
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward save(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		OperatorForm operatorForm = (OperatorForm) form;
		long operatorId = operatorForm.getOperatorId();
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Operator operator = null;
				if (operatorForm.isNewOperator())
				{
					operator = new Operator();
				}
				else
				{
					operator = Operator.retrieveOperator(operatorId);
				}
				
				BeanUtils.copyProperties(operator, operatorForm);
				
				operator.save();
			}
			catch (UnknownObjectException uoe)
			{
                log.warn("", uoe);
				errors.add("unknown.operator", new  ActionError("error.unknown.operator"));
			}
			catch (DataAccessException de)
			{
                log.warn("", de);
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.warn("", e);
                errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			if (operatorForm.isNewOperator())
			{
				throw new NotPermittedException("Not permitted to create this resource.");
			}
			else
			{
				throw new NotPermittedException("Not permitted to modify this resource.");
			}
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward show(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		OperatorForm operatorForm = (OperatorForm) form;
		long operatorId = operatorForm.getOperatorId();
		
		// If no operatorId in form, check session
		if (operatorId < 1)
		{
			operatorId = Long.parseLong((String) request.getSession(false).getAttribute("operatorId"));
		}
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Operator operator = Operator.retrieveOperator(operatorId);
				BeanUtils.copyProperties(operatorForm, operator);
			}
			catch (UnknownObjectException uoe)
			{
                log.warn("", uoe);
				errors.add("unknown.operator", new  ActionError("error.unknown.operator"));
			}
			catch (DataAccessException de)
			{
                log.warn("", de);
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
                log.warn("", e);
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward retrieveList(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		int num = 0;
		
		try
		{
			List ids = SecurityService.getAppObjectIds(request, SecurityService.OPERATOR);
			num = ids.size();
			
			if (num == 0)
			{
				// user does not have access to any operators
				throw new NotPermittedException();
			}
			else if (num == 1 && ((String) ids.get(0)).equals("-1"))
			{
				List operators = Operator.retrieveAll();
				num = operators.size();
				
				List beans = BeanUtil.convertList(operators, OperatorBean.class);
								
				request.setAttribute("operators", beans);				
			}
			else if (num == 1)
			{
					// only one, retrieve
					long id = Long.parseLong((String)ids.get(0));
					OperatorForm operatorForm = (OperatorForm) form;
					request.getSession().setAttribute("operatorId", "" + id);
					operatorForm.setOperatorId(id);
					return show(mapping, operatorForm, request, response);
			}
			else
			{
				List beans = new ArrayList();
				for (int i = 0; i < ids.size(); i++)
				{
					long id = Long.parseLong((String)ids.get(i));
					Operator operator = Operator.retrieveOperator(id);
					OperatorBean operatorBean = new OperatorBean();
					BeanUtils.copyProperties(operatorBean, operator);
					beans.add(operatorBean);
				}

				request.setAttribute("operators", beans);	
			}
		}
		catch (UnknownObjectException uoe)
		{
            log.warn("", uoe);
			errors.add("unknown.operator", new  ActionError("error.unknown.operator"));
		}
		catch (DataAccessException de)
		{
            log.warn("", de);
			errors.add("data.access", new ActionError("error.data.access"));
		}
		catch (Exception e)
		{
			log.error("Unknown error occurred: " + e.getMessage(), e);
			errors.add("unknown.error", new ActionError("error.unknown"));
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			if (num > 1)
			{
				forward = mapping.findForward("list");
			}
		}
		
		return forward;
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		return Long.parseLong((String) request.getSession(false).getAttribute("operatorId"));
	}
}
