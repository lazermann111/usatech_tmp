package com.usatech.esuds.server.operatoraccess.actions;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Operator;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.operatoraccess.forms.CampusForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
import com.usatech.esuds.server.util.LocationUtil;
/**
 * @version 	1.0
 * @author
 */
public class CampusAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(CampusAction.class);
	
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward save(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
			
		CampusForm campusForm = (CampusForm) form;
		long campusId = campusForm.getCampusId();
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Campus campus = null;
				if (campusForm.isNewCampus())
				{
					campus = new Campus();
					campus.setOperatorId(operatorId);
				}
				else
				{
					campus = Campus.retrieveCampus(campusId);
				}
				
				BeanUtils.copyProperties(campus, campusForm);
				
				campus.save();
				
				// copy properties back to form
				BeanUtils.copyProperties(campusForm, campus);
				
				List dorms = campus.getDorms();
				List dormBeans = BeanUtil.convertList(dorms, DormBean.class, "show");
				request.setAttribute("dorms", dormBeans);
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("save.successful", "Campus", campus.getName()));
				
				// for navigation
				CampusBean campusBean = getNavigation(campus);
				request.setAttribute("campusBean", campusBean);
                
                // add only schools that operator can see
                List schoolBeans = LocationUtil.getSchoolsByOperatorUser(SecurityService.getUserId(request));
                request.setAttribute("schools", schoolBeans);                   
			}
			catch (UnknownObjectException uoe)
			{
                log.error("An unknown object error occurred: ", uoe);
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.campus"));
			}
			catch (DataAccessException de)
			{
                log.error("A data access error occurred: ", de);
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (DataException de)
			{
                log.error("An data error occurred: ", de);
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error occurred: ", e);
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			if (campusForm.isNewCampus())
			{
				throw new NotPermittedException("Not permitted to create this resource.");
			}
			else
			{
				throw new NotPermittedException("Not permitted to modify this resource.");
			}
		}
			
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("success");
		}
			
		return forward;
	}
	
	public ActionForward create(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		CampusForm campusForm = (CampusForm) form;
		
		// get the operatorId out of session
		HttpSession session = request.getSession(false);
		
		long operatorId;
		
		try
		{
			operatorId = Long.parseLong((String) session.getAttribute("operatorId"));
			if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
			{
				// set campusId to -1 for New
				campusForm.setCampusId(-1);
				campusForm.setNewCampus(true);
				campusForm.setActive(true);
				request.setAttribute("dorms", new ArrayList());
                
                // add only schools that operator can see
                List schoolBeans = LocationUtil.getSchoolsByOperatorUser(SecurityService.getUserId(request));
                request.setAttribute("schools", schoolBeans);                   
			}
			else
			{
				throw new NotPermittedException("Not permitted to create a campus");
			}
		}
		// Add more exception here?
		catch (Exception e)
		{
			log.error("Unknown error occurred: "+ e.getMessage());
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward show(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		CampusForm campusForm = (CampusForm) form;
		long campusId = campusForm.getCampusId();
		long operatorId = getOperatorId(request);
		
		// If no operatorId in form, check session
		if (operatorId < 1)
		{
			operatorId = getOperatorId(request);
		}
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Campus campus = Campus.retrieveCampus(campusId);
				if (campus.getOperatorId() == operatorId)
				{
					BeanUtils.copyProperties(campusForm, campus);
					List dorms = campus.getDorms();
					List dormBeans = BeanUtil.convertList(dorms, DormBean.class, "show");
					request.setAttribute("dorms", dormBeans);
					
					// for navigation
					CampusBean campusBean = getNavigation(campus);
					request.setAttribute("campusBean", campusBean);
                    
                    // add only schools that operator can see
                    List schoolBeans = LocationUtil.getSchoolsByOperatorUser(SecurityService.getUserId(request));
                    request.setAttribute("schools", schoolBeans);                   
				}
				else
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.campus"));
				}
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.campus"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward retrieveList(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		CampusForm campusForm = (CampusForm) form;
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Operator operator = Operator.retrieveOperator(operatorId);
				List campuses = operator.getCampuses();
				List beans = BeanUtil.convertList(campuses, CampusBean.class, "show");
				request.setAttribute("campuses", beans);
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.campus"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: "+ e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("list");
		}
		
		return forward;
	}
		
	public ActionForward setInactive(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
			
		CampusForm campusForm = (CampusForm) form;
		long campusId = campusForm.getCampusId();
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.MODIFY))
		{
			try
			{
				Campus campus = Campus.retrieveCampus(campusId);
				List dorms = campus.getDorms();
				if (dorms.isEmpty() || dorms == null)
				{
					campus.setActive(false);							
					campus.save();
					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("delete.successful", "Campus", campus.getName()));
					request.setAttribute("operatorId", new Long(operatorId));
				}
				else
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.dependent.locations"));
				}
				
				// for navigation
				CampusBean campusBean = getNavigation(campus);
				request.setAttribute("campusBean", campusBean);
                
                // add only schools that operator can see
                List schoolBeans = LocationUtil.getSchoolsByOperatorUser(SecurityService.getUserId(request));
                request.setAttribute("schools", schoolBeans);                   
			}
			catch (UnknownObjectException uoe)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new  ActionError("error.unknown.campus"));
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("An unexpected error has occurred: " + e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to change this campus.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			saveMessages(request, messages);
			forward = mapping.findForward("deleted");
		}
		
		return forward;
	}
	public ActionForward delete(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		// delete is the same as set inactive
		return setInactive(mapping, form, request, response);
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session != null && session.getAttribute("operatorId") != null)
		{
			return Long.parseLong((String) session.getAttribute("operatorId"));
		}
		else return (new Long(0)).longValue();
	}
	
	private static CampusBean getNavigation(Campus campus) throws IllegalAccessException, InvocationTargetException
	{
		CampusBean campusBean = new CampusBean();
		BeanUtils.copyProperties(campusBean, campus);
		return campusBean;
	}
}
