package com.usatech.esuds.server.roomstatus.exceptions;

/**
 * @author rothma
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class DataAccessException extends Exception
{

	/**
	 * Constructor for DataAccessException.
	 */
	public DataAccessException()
	{
		super();
	}

	/**
	 * Constructor for DataAccessException.
	 * @param arg0
	 */
	public DataAccessException(String arg0)
	{
		super(arg0);
	}

}
