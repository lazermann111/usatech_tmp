package com.usatech.esuds.server.roomstatus.exceptions;

/**
 * @author melissa
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class LaundryRoomNotFoundException extends Exception
{

	/**
	 * Constructor for LaundryRoomNotFoundException.
	 */
	public LaundryRoomNotFoundException()
	{
		super();
	}

	/**
	 * Constructor for LaundryRoomNotFoundException.
	 * @param s
	 */
	public LaundryRoomNotFoundException(String s)
	{
		super(s);
	}

}
