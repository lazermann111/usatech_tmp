package com.usatech.esuds.server.roomstatus.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.beans.SchoolBean;

/**
 * @author melissa
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class DormStatusResult implements Collection
{
	private ArrayList laundryRooms = new ArrayList();
	private CampusBean campus = null;
	private SchoolBean school = null;
	private DormBean dorm = null;
	private LaundryRoomStatusResultBean selectedRoom = null;

	/**
	 * Constructor for DormStatusResult.
	 */
	public DormStatusResult()
	{
		super();
	}

	/**
	 * @see java.util.Collection#size()
	 */
	public int size()
	{
		return laundryRooms.size();
	}

	/**
	 * @see java.util.Collection#isEmpty()
	 */
	public boolean isEmpty()
	{
		return laundryRooms.isEmpty();
	}

	/**
	 * @see java.util.Collection#contains(Object)
	 */
	public boolean contains(Object o)
	{
		return laundryRooms.contains(o);
	}

	/**
	 * @see java.util.Collection#iterator()
	 */
	public Iterator iterator()
	{
		return laundryRooms.iterator();
	}

	/**
	 * @see java.util.Collection#toArray()
	 */
	public Object[] toArray()
	{
		return laundryRooms.toArray();
	}

	/**
	 * @see java.util.Collection#toArray(Object[])
	 */
	public Object[] toArray(Object[] a)
	{
		return laundryRooms.toArray(a);
	}

	/**
	 * @see java.util.Collection#add(Object)
	 */
	public boolean add(Object o)
	{
		return laundryRooms.add(o);
	}

	/**
	 * @see java.util.Collection#remove(Object)
	 */
	public boolean remove(Object o)
	{
		return laundryRooms.remove(o);
	}

	/**
	 * @see java.util.Collection#containsAll(Collection)
	 */
	public boolean containsAll(Collection c)
	{
		return laundryRooms.containsAll(c);
	}

	/**
	 * @see java.util.Collection#addAll(Collection)
	 */
	public boolean addAll(Collection c)
	{
		return laundryRooms.addAll(c);
	}

	/**
	 * @see java.util.Collection#removeAll(Collection)
	 */
	public boolean removeAll(Collection c)
	{
		return laundryRooms.removeAll(c);
	}

	/**
	 * @see java.util.Collection#retainAll(Collection)
	 */
	public boolean retainAll(Collection c)
	{
		return laundryRooms.retainAll(c);
	}

	/**
	 * @see java.util.Collection#clear()
	 */
	public void clear()
	{
		laundryRooms.clear();
	}

	/**
	 * Returns the campus.
	 * @return CampusViewBean
	 */
	public CampusBean getCampus()
	{
		return campus;
	}

	/**
	 * Returns the dorm.
	 * @return DormViewBean
	 */
	public DormBean getDorm()
	{
		return dorm;
	}

	/**
	 * Returns the school.
	 * @return SchoolViewBean
	 */
	public SchoolBean getSchool()
	{
		return school;
	}

	/**
	 * Sets the campus.
	 * @param campus The campus to set
	 */
	public void setCampus(CampusBean campus)
	{
		this.campus = campus;
	}

	/**
	 * Sets the dorm.
	 * @param dorm The dorm to set
	 */
	public void setDorm(DormBean dorm)
	{
		this.dorm = dorm;
	}

	/**
	 * Sets the school.
	 * @param school The school to set
	 */
	public void setSchool(SchoolBean school)
	{
		this.school = school;
	}

	/**
	 * Returns the selectedRoom.
	 * @return LaundryRoomStatusResultBean
	 */
	public LaundryRoomStatusResultBean getSelectedRoom()
	{
		return selectedRoom;
	}

	/**
	 * Sets the selectedRoom.
	 * @param selectedRoom The selectedRoom to set
	 */
	public void setSelectedRoom(LaundryRoomStatusResultBean selectedRoom)
	{
		this.selectedRoom = selectedRoom;
	}

}
