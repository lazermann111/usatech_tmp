package com.usatech.esuds.server.roomstatus.beans;

import java.util.List;

public class LaundryRoomStatusResultBean
{
	private List washers;
	private List dryers;
	private String laundryRoom;
	private long id;
	private int totalWashers;
	private int totalDryers;
	private int availableWashers;
	private int availableDryers;
	private String roomImageUrl;

    public String getRoomImageUrl() {
        return roomImageUrl;
    }
    public void setRoomImageUrl(String roomImageUrl) {
        this.roomImageUrl = roomImageUrl;
    }
	/**
	 * Constructor for LaundryRoomStatusResultBean.
	 */
	public LaundryRoomStatusResultBean(long _id, String _laundryRoom)
	{
		super();
		id = _id;
		laundryRoom = _laundryRoom;
	}

	/**
	 * Constructor LaundryRoomStatusResultBean.
	 */
	public LaundryRoomStatusResultBean()
	{
	}

	/**
	 * Sets the laundryRoom.
	 * @param laundryRoom The laundryRoom to set
	 */
	public void setLaundryRoom(String _laundryRoom)
	{
		laundryRoom = _laundryRoom;
	}

	/**
	 * Returns the id.
	 * @return long
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Method setId.
	 * @param id
	 */
	public void setId(long _id)
	{
		id = _id;
	}

	/**
	 * Sets the machines.
	 * @param machines The machines to set
	 */
	public void setWashers(List _washers)
	{
		washers = _washers;
	}
	
	public void setDryers(List _dryers)
	{
		dryers = _dryers;
	}

	/**
	 * @return
	 */
	public List getDryers()
	{
		return dryers;
	}

	/**
	 * @return
	 */
	public String getLaundryRoom()
	{
		return laundryRoom;
	}

	/**
	 * @return
	 */
	public List getWashers()
	{
		return washers;
	}

	/**
	 * @return
	 */
	public int getAvailableDryers()
	{
		return availableDryers;
	}

	/**
	 * @return
	 */
	public int getAvailableWashers()
	{
		return availableWashers;
	}

	/**
	 * @return
	 */
	public int getTotalDryers()
	{
		return totalDryers;
	}

	/**
	 * @return
	 */
	public int getTotalWashers()
	{
		return totalWashers;
	}

	/**
	 * @param i
	 */
	public void setAvailableDryers(int i)
	{
		availableDryers = i;
	}

	/**
	 * @param i
	 */
	public void setAvailableWashers(int i)
	{
		availableWashers = i;
	}

	/**
	 * @param i
	 */
	public void setTotalDryers(int i)
	{
		totalDryers = i;
	}

	/**
	 * @param i
	 */
	public void setTotalWashers(int i)
	{
		totalWashers = i;
	}

}
