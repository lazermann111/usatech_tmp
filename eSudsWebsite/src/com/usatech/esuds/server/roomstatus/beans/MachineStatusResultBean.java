package com.usatech.esuds.server.roomstatus.beans;

public class MachineStatusResultBean
{
	private long machineId;
	private String port = null;
	private String name = null;
	private String availability = null;
	private String timeRemaining = null;
	private String cycleTime = null;
	private String position = null;
	private String label = null;
	
	// out of 100
	private int progress = 0;
	private boolean isStack = false;

	/**
	 * Constructor for MachineStatusResultBean.
	 */
	public MachineStatusResultBean()
	{
		super();
	}

	/**
	 * Returns the availability.
	 * @return String
	 */
	public String getAvailability()
	{
		return availability;
	}

	/**
	 * Returns the name.
	 * @return String
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Returns the port.
	 * @return String
	 */
	public String getPort()
	{
		return port;
	}

	/**
	 * Returns the timeRemaining.
	 * @return String
	 */
	public String getTimeRemaining()
	{
		if (timeRemaining == null)
		{
			return "";
		}
		else
		{
			return timeRemaining;
		}		
	}

	/**
	 * Sets the availability.
	 * @param availability The availability to set
	 */
	public void setAvailability(String _availability)
	{
		availability = _availability;
	}

	/**
	 * Sets the name.
	 * @param name The name to set
	 */
	public void setName(String _name)
	{
		name = _name;
	}

	/**
	 * Sets the port.
	 * @param port The port to set
	 */
	public void setPort(String _port)
	{
		port = _port;
	}

	/**
	 * Sets the timeRemaining.
	 * @param timeRemaining The timeRemaining to set
	 */
	public void setTimeRemaining(String _timeRemaining)
	{
		timeRemaining = _timeRemaining;
	}

	/**
	 * @return
	 */
	public boolean isStack()
	{
		return isStack;
	}

	/**
	 * @param b
	 */
	public void setStack(boolean b)
	{
		isStack = b;
	}

	/**
	 * @return
	 */
	public String getPosition()
	{
		return position;
	}

	/**
	 * @param string
	 */
	public void setPosition(String string)
	{
		position = string;
	}

	/**
	 * @return
	 */
	public String getCycleTime()
	{
		return cycleTime;
	}

	/**
	 * @param string
	 */
	public void setCycleTime(String string)
	{
		cycleTime = string;
	}

	/**
	 * @return
	 */
	public int getProgress()
	{
		return progress;
	}

	/**
	 * @param string
	 */
	public void setProgress(int _progress)
	{
		progress = _progress;
	}

	/**
	 * @return
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @param string
	 */
	public void setLabel(String string)
	{
		label = string;
	}

	/**
	 * @return
	 */
	public long getMachineId()
	{
		return machineId;
	}

	/**
	 * @param l
	 */
	public void setMachineId(long l)
	{
		machineId = l;
	}

}
