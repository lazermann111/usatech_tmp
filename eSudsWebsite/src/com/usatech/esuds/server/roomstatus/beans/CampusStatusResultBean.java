/*
 * *******************************************************************
 * 
 * File CampusStatusResultBean.java
 * 
 * Created on Apr 14, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.roomstatus.beans;

import java.util.List;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.OperatorBean;

/**
 * @author melissa
 *
 */
public class CampusStatusResultBean
{
	private CampusBean campus = null;
	private OperatorBean operator = null;
	private List dorms = null;
	private long dormId;
	private long roomId;
	private List laundryRooms = null;
	private LaundryRoomStatusResultBean selectedRoom = null;
	
	/**
	 * 
	 */
	public CampusStatusResultBean()
	{
		super();
	}
	/**
	 * @return
	 */
	public CampusBean getCampus()
	{
		return campus;
	}

	/**
	 * @return
	 */
	public List getDorms()
	{
		return dorms;
	}

	/**
	 * @return
	 */
	public List getLaundryRooms()
	{
		return laundryRooms;
	}

	/**
	 * @return
	 */
	public LaundryRoomStatusResultBean getSelectedRoom()
	{
		return selectedRoom;
	}

	/**
	 * @param bean
	 */
	public void setCampus(CampusBean bean)
	{
		campus = bean;
	}

	/**
	 * @param list
	 */
	public void setDorms(List list)
	{
		dorms = list;
	}

	/**
	 * @param list
	 */
	public void setLaundryRooms(List list)
	{
		laundryRooms = list;
	}

	/**
	 * @param result
	 */
	public void setSelectedRoom(LaundryRoomStatusResultBean result)
	{
		selectedRoom = result;
	}

	/**
	 * @return
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * @return
	 */
	public long getRoomId()
	{
		return roomId;
	}

	/**
	 * @param l
	 */
	public void setDormId(long l)
	{
		dormId = l;
	}

	/**
	 * @param l
	 */
	public void setRoomId(long l)
	{
		roomId = l;
	}

	/**
	 * @return
	 */
	public OperatorBean getOperator()
	{
		return operator;
	}

	/**
	 * @param bean
	 */
	public void setOperator(OperatorBean bean)
	{
		operator = bean;
	}

}
