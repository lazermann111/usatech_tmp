package com.usatech.esuds.server.roomstatus.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.beans.LaundryRoomBean;
import com.usatech.esuds.server.beans.SchoolBean;
import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.LaundryEquip;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.LaundryRoomEquip;
import com.usatech.esuds.server.model.School;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.roomstatus.beans.DormStatusResult;
import com.usatech.esuds.server.roomstatus.beans.LaundryRoomStatusResultBean;
import com.usatech.esuds.server.roomstatus.beans.MachineStatusResultBean;
import com.usatech.esuds.server.roomstatus.exceptions.DataAccessException;
import com.usatech.esuds.server.roomstatus.exceptions.LaundryRoomNotFoundException;

/**
 * @author melissa
 * 
 */
public class LaundryRoomStatusService {
    public final static String UNAVAILABLE = "Unavailable";

    public final static String AVAILABLE = "Available";

    public final static String IN_USE = "In Use";

    public final static String CYCLE_COMPELTE = "Cycle Complete";

    public final static String NO_STATUS = "No Status Available";

    public static long reset = 9; // Number of minutes to go back to if
                                    // time-remaining is negative or zero

    private static Log log = LogFactory.getLog(LaundryRoomStatusService.class);

    public static boolean verifyDormChoice(long dormId, long campusId, long schoolId)
            throws UnknownObjectException, DataException {
        Dorm dorm = null;

        dorm = Dorm.retrieveDorm(dormId);
        if(dorm.getCampusId() == campusId && dorm.getCampus().getSchoolId() == schoolId) {
            return true;
        } else {
            return false;
        }
    }

    public static Collection getLaundryRoomStatusforDorm(long dormId) throws DataAccessException,
            LaundryRoomNotFoundException, DataException, UnknownObjectException {

        DormStatusResult results = new DormStatusResult();
        List rooms = Dorm.retrieveDorm(dormId).getLaundryRooms();

        if(rooms.size() < 1) {
            throw new LaundryRoomNotFoundException("Found no Laundry Rooms for Dorm with id " + dormId);
        }

        for(Iterator i = rooms.iterator(); i.hasNext();) {
            LaundryRoom room = (LaundryRoom) i.next();
            LaundryRoomBean bean = new LaundryRoomBean();
            bean.setLaundryRoomId(room.getLaundryRoomId());
            bean.setName(room.getName());
            results.add(bean);

            // If there is no room specified in the parameters, and there is no
            // selected room, use this one
            if(results.getSelectedRoom() == null) {
                LaundryRoomStatusResultBean result = new LaundryRoomStatusResultBean();
                result.setLaundryRoom(room.getName());
                result.setId(room.getLaundryRoomId());

                List washers = getWasherStatusForRoom(room.getLaundryRoomId());
                result.setWashers(washers);
                result.setTotalWashers(washers.size());
                result.setAvailableWashers(findNumAvailMachines(washers));

                List dryers = getDryerStatusForRoom(room.getLaundryRoomId());
                result.setDryers(dryers);
                result.setTotalDryers(dryers.size());
                result.setAvailableDryers(findNumAvailMachines(dryers));
                results.setSelectedRoom(result);
            }
        }

        // Set the School,Campus, and Dorm
        setNavigationForResults(dormId, results);
        return results;

    }

    /**
     * @param l
     * @return
     */
    private static List getDryerStatusForRoom(long id) throws UnknownObjectException, DataException {
        ArrayList result = new ArrayList();
        LaundryRoom room = null;
        List laundryRoomEquipList = null;

        room = LaundryRoom.retrieveLaundryRoom(id);
        laundryRoomEquipList = room.retrieveLaundryRoomEquips(LaundryEquip.DRYER);
        laundryRoomEquipList.addAll(room.retrieveLaundryRoomEquips(LaundryEquip.STACK_DRYER));
        laundryRoomEquipList.addAll(room.retrieveLaundryRoomEquips(LaundryEquip.STACK_WASHER_DRYER,
                LaundryRoomEquip.TOP_POSITION));

        if(laundryRoomEquipList != null) {
            for(Iterator i = laundryRoomEquipList.iterator(); i.hasNext();) {
                LaundryRoomEquip machine = (LaundryRoomEquip) i.next();
                result.add(createMachineStatusBean(machine));
            }
        }

        return orderMachinesByLabel(result);
    }

    /**
     * @param l
     * @return
     */
    private static List getWasherStatusForRoom(long id) throws UnknownObjectException, DataException {
        ArrayList result = new ArrayList();
        LaundryRoom room = null;
        List laundryRoomEquipList = null;

        room = LaundryRoom.retrieveLaundryRoom(id);
        laundryRoomEquipList = room.retrieveLaundryRoomEquips(LaundryEquip.WASHER);
        laundryRoomEquipList.addAll(room.retrieveLaundryRoomEquips(LaundryEquip.STACK_WASHER_DRYER,
                LaundryRoomEquip.BOTTOM_POSITION));

        if(laundryRoomEquipList != null) {
            for(Iterator i = laundryRoomEquipList.iterator(); i.hasNext();) {
                LaundryRoomEquip machine = (LaundryRoomEquip) i.next();
                result.add(createMachineStatusBean(machine));
            }
        }

        return orderMachinesByLabel(result);
    }

    public static Collection getLaundryRoomStatusforDorm(long dormId, long roomId) throws DataException,
            UnknownObjectException {
        DormStatusResult results = new DormStatusResult();
        List rooms = Dorm.retrieveDorm(dormId).getLaundryRooms();

        for(Iterator i = rooms.iterator(); i.hasNext();) {
            LaundryRoom room = (LaundryRoom) i.next();
            LaundryRoomBean bean = new LaundryRoomBean();

            bean.setLaundryRoomId(room.getLaundryRoomId());
            bean.setName(room.getName());
            results.add(bean);

            // Check if this room was the default supplied
            if(bean.getLaundryRoomId() == roomId) {
                LaundryRoomStatusResultBean result = new LaundryRoomStatusResultBean();
                result.setLaundryRoom(room.getName());
                result.setId(room.getLaundryRoomId());

                List washers = getWasherStatusForRoom(room.getLaundryRoomId());
                result.setWashers(washers);
                result.setTotalWashers(washers.size());
                result.setAvailableWashers(findNumAvailMachines(washers));

                List dryers = getDryerStatusForRoom(room.getLaundryRoomId());
                result.setDryers(dryers);
                result.setTotalDryers(dryers.size());
                result.setAvailableDryers(findNumAvailMachines(dryers));

                results.setSelectedRoom(result);
            }
        }

        // Set the School,Campus, and Dorm
        setNavigationForResults(dormId, results);
        return results;

    }

    private static String findAvailability(int status) {
        switch(status) {
            case LaundryRoomEquip.INVALID:
                return "Unavailable";
            case LaundryRoomEquip.NO_STATUS_AVAILABLE:
                return "Unavailable";
            case LaundryRoomEquip.IDLE_AVAILABLE:
                return "Available";
            case LaundryRoomEquip.IN_1ST_CYCLE:
                return "In Use";
            case LaundryRoomEquip.OUT_OF_SERVICE:
                return "Unavailable";
            case LaundryRoomEquip.NOTHING_ON_PORT:
                return "Unavailable";
            case LaundryRoomEquip.IDLE_NOT_AVAILABLE:
                return "Cycle Complete";
            case LaundryRoomEquip.IN_MANUAL_SERVICE_MODE:
                return "Unavailable";
            case LaundryRoomEquip.IN_2ND_CYCLE:
                return "In Use";
            case LaundryRoomEquip.TRANSACTION_IN_PROGRESS:
                return "In Use";
            default:
                return "Unknown";
        }
    }

    private static void setNavigationForResults(long id, DormStatusResult results)
            throws UnknownObjectException, DataException {
        Dorm dorm = Dorm.retrieveDorm(id);
        DormBean dormBean = new DormBean();

        Campus campus = dorm.getCampus();
        CampusBean campusBean = new CampusBean();

        School school = campus.getSchool();
        SchoolBean schoolBean = new SchoolBean();

        try {
            BeanUtils.copyProperties(dormBean, dorm);
            BeanUtils.copyProperties(campusBean, campus);
            BeanUtils.copyProperties(schoolBean, school);
        } catch(Exception e) {
            log.error("Error copying properties into beans.");
        }

        results.setDorm(dormBean);
        results.setCampus(campusBean);
        results.setSchool(schoolBean);
    }

    private static int findNumAvailMachines(List machines) {
        int num = 0;
        Iterator it = machines.iterator();

        while(it.hasNext()) {
            MachineStatusResultBean bean = (MachineStatusResultBean) it.next();
            if(bean.getAvailability().equals(AVAILABLE)) {
                num++;
            }
        }

        return num;
    }

    private static final Comparator machineComparator = new Comparator() {
        public int compare(Object o1, Object o2) {
            MachineStatusResultBean m1 = (MachineStatusResultBean) o1;
            MachineStatusResultBean m2 = (MachineStatusResultBean) o2;
            Object v1 = getLabelValue(m1.getLabel());
            Object v2 = getLabelValue(m2.getLabel());
            if(v1 == null) {
                if(v2 == null)
                    return 0;
                else
                    return 1; // o1 goes after o2
            } else if(v2 == null) {
                return -1; // o1 goes before o2
            } else if(v1.getClass().equals(v2.getClass())) {
                return ((Comparable) v1).compareTo(v2);
            } else if(v1 instanceof Number) {
                return -1;// o1 goes before o2
            } else {
                return 1; // o1 goes after o2
            }
        }

        private Object getLabelValue(String label) {
            if(label == null || (label = label.trim()).length() == 0 || label.equals("000")) {
                return null; // put these at the end of the list
            } else
                try {
                    return new Integer(Integer.parseInt(label));
                } catch(NumberFormatException nfe) {
                    log.debug("Could not parse label '" + label + "' to int.");
                    return label; // these go after the numbers
                }
        }
    };

    private static List orderMachinesByLabel(List beans) {
        Collections.sort(beans, machineComparator);
        return beans;
    }

    private static MachineStatusResultBean createMachineStatusBean(LaundryRoomEquip machine) {
        MachineStatusResultBean bean = new MachineStatusResultBean();
        bean.setMachineId(machine.getLaundryRoomEquipId());
        bean.setPort("" + machine.getPortNum());

        if(machine.getLabel() != null) {
            bean.setLabel("" + machine.getLabel());
        } else {
            bean.setLabel("unlabeled");
        }

        try {
			bean.setName(machine.getLaundryEquip().getGroupNames());
		} catch (TorqueException e) {
			log.warn("Could not set laundry equip name", e);
			bean.setName("Unknown");
		}
        bean.setAvailability(findAvailability(machine.getStatusIndicator()));

        long cycle = machine.getCycleMins();
        bean.setCycleTime("" + cycle);

        if(machine.getStartTS() != null && bean.getAvailability().equals(IN_USE)) {
            long timePassed = (((new Date().getTime()) - (machine.getStartTS().getTime())) / 60000);
            long min = cycle - timePassed;

            // check for negative numbers
            if(timePassed > getMaxCycleMinutes()) {
                bean.setAvailability(CYCLE_COMPELTE);
                bean.setTimeRemaining("0");
                bean.setProgress(0);
            } else if(min > 0) {
                bean.setTimeRemaining("" + min);
                log.debug("minutes remaining is " + min);

                // progress is out of 100, decreases
                int p = 100 - (int) (Math.round(((float) timePassed / (float) cycle) * 100));

                if(p > 0 && p < 100) {
                    bean.setProgress(p);
                } else if(p < 0) {
                    bean.setProgress(0);
                } else {
                    bean.setProgress(100);
                }

                log.debug("Progress is " + p);
            } else {
                long oldmin = min;
                min = reset + (min % reset);
                log.debug("minutes remaining is " + oldmin + " but I'm reporting " + min);
                bean.setTimeRemaining("" + min);

                // progress is out of 100, decreases
                bean.setProgress(1);
                log.debug("Progress is " + 1);
            }
        } else {
            bean.setTimeRemaining("0");
            bean.setProgress(0);
        }

        return bean;

    }

    /**
     * @return
     */
    public static long getMaxCycleMinutes() {
        return 120;
    }

    public static LaundryRoomStatusResultBean getRoomStatusBean(LaundryRoom room)
            throws UnknownObjectException, DataException {
        LaundryRoomStatusResultBean result = new LaundryRoomStatusResultBean();
        result.setLaundryRoom(room.getName());
        result.setId(room.getLaundryRoomId());
        result.setRoomImageUrl(room.getRoomImageUrl());

        List washers = getWasherStatusForRoom(room.getLaundryRoomId());
        result.setWashers(washers);
        result.setTotalWashers(washers.size());
        result.setAvailableWashers(findNumAvailMachines(washers));

        List dryers = getDryerStatusForRoom(room.getLaundryRoomId());
        result.setDryers(dryers);
        result.setTotalDryers(dryers.size());
        result.setAvailableDryers(findNumAvailMachines(dryers));

        return result;
    }
}
