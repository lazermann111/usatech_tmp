package com.usatech.esuds.server.roomstatus.forms;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
/**
 * Form bean for a Struts application.
 * Users may access 2 fields on this form:
 * <ul>
 * <li>emailAddress - [your comment here]
 * <li>machineId - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class RequestNotificationForm extends ActionForm
{
	private String emailAddress = null;
	private long roomId;
	private long[] selectedMachines;
	/**
	 * Get emailAddress
	 * @return String
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}
	/**
	 * Set emailAddress
	 * @param <code>String</code>
	 */
	public void setEmailAddress(String e)
	{
		this.emailAddress = e;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		emailAddress = null;
		roomId =0;
		selectedMachines = new long[0];
	}
	
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		
		if ((emailAddress == null) || (emailAddress.length() == 0))
		{
		  errors.add("emailAddress", new ActionError("error.email.address.required"));
		}
		
		if (selectedMachines == null || selectedMachines.length == 0)
		{
			errors.add("selectedMachines", new ActionError("error.no.machines.selected"));
;		}
		
		if (roomId < 1)
		{
			Cookie[] cookies = request.getCookies();
			
			if (cookies != null)
			{
				for (int i = 0; i < cookies.length; i++)
				{
					if (cookies[i].getName().equals("roomId") && roomId == -1)
					{
						roomId = Integer.parseInt(cookies[i].getValue());
					}
				}
			}
		}
		
		emailAddress = emailAddress.trim();
		
		if (emailAddress.indexOf('@' ) == -1
			|| emailAddress.indexOf('.') == -1  
			|| emailAddress.indexOf(' ') != -1
			|| emailAddress.length() < 7)
		{
			errors.add("emailAddress", new ActionError("error.email.address.invalid"));
		}
		
		return errors;
	}
	/**
	 * @return
	 */
	public long getRoomId()
	{
		return roomId;
	}

	/**
	 * @param l
	 */
	public void setRoomId(long l)
	{
		roomId = l;
	}

	/**
	 * @return
	 */
	public long[] getSelectedMachines()
	{
		return selectedMachines;
	}

	/**
	 * @param ls
	 */
	public void setSelectedMachines(long[] ls)
	{
		selectedMachines = ls;
	}

}
