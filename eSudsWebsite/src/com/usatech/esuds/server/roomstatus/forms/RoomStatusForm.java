package com.usatech.esuds.server.roomstatus.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorActionForm;
/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class RoomStatusForm extends ActionForm
{
	private long schoolId = -1;
	private long campusId = -1;
	private long dormId = -1;
	private long roomId = -1;
	private long operatorId = -1;
	private boolean fromNotification = false;
	
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		campusId = -1;
		dormId = -1;
		roomId = -1;
		operatorId = -1;
		fromNotification = false;
	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();

		return errors;
	}
	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * @return
	 */
	public long getRoomId()
	{
		return roomId;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
	}

	/**
	 * @param l
	 */
	public void setDormId(long l)
	{
		dormId = l;
	}

	/**
	 * @param l
	 */
	public void setRoomId(long l)
	{
		roomId = l;
	}

	/**
	 * @return
	 */
	public long getOperatorId()
	{
		return operatorId;
	}

	/**
	 * @param l
	 */
	public void setOperatorId(long l)
	{
		operatorId = l;
	}

	/**
	 * @return
	 */
	public boolean isFromNotification()
	{
		return fromNotification;
	}

	/**
	 * @param b
	 */
	public void setFromNotification(boolean b)
	{
		fromNotification = b;
	}

	/**
	 * @return
	 */
	public long getSchoolId()
	{
		return schoolId;
	}

	/**
	 * @param l
	 */
	public void setSchoolId(long l)
	{
		schoolId = l;
	}

}
