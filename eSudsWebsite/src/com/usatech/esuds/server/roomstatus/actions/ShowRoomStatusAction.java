package com.usatech.esuds.server.roomstatus.actions;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.beans.CampusSchoolBean;
import com.usatech.esuds.server.beans.DormBean;
import com.usatech.esuds.server.beans.LaundryRoomBean;
import com.usatech.esuds.server.beans.OperatorBean;
import com.usatech.esuds.server.beans.SchoolBean;
import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.Operator;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.roomstatus.beans.CampusStatusResultBean;
import com.usatech.esuds.server.roomstatus.forms.RoomStatusForm;
import com.usatech.esuds.server.roomstatus.services.LaundryRoomStatusService;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
import com.usatech.esuds.server.util.SecurityUtil;
/**
 * @version 	1.0
 * @author
 */
public class ShowRoomStatusAction extends Action
{
	private static Log log = LogFactory.getLog(ShowRoomStatusAction.class);
	
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		ActionMessages messages = new ActionMessages();
		
		RoomStatusForm roomStatusForm = (RoomStatusForm) form;
		
		// form data
		long roomId = roomStatusForm.getRoomId();
		long dormId = roomStatusForm.getDormId();
		long campusId = roomStatusForm.getCampusId();
		long schoolId = roomStatusForm.getSchoolId();
		long operatorId = roomStatusForm.getOperatorId();
		boolean formDataPresent = (schoolId > 0 || campusId > 0 || dormId > 0 || roomId > 0 || operatorId > 0);
		
		// cookie values
		long cookieRoomId = getRoomIdCookieValue(request);
		
		// Create result bean
		CampusStatusResultBean resultBean = new CampusStatusResultBean();
		
		// Locations
		Campus campus = null;
		Dorm dorm = null;
		LaundryRoom room = null;
		
		// Lists
		List dorms = null;
		List rooms = null;
		
		try
		{
			
			if (!formDataPresent)
			{
				// No form data
				log.debug("Form data not present.");
				Hashtable subDomain = SecurityService.getObjectForSubdomain(request);
				if (log.isDebugEnabled() && subDomain == null)
				{
					log.debug("subDomain is null");
				}
					
				if (subDomain != null && subDomain.get("schoolId") != null)
				{
					log.debug("Subdomain points to school, showing campuses.");
					schoolId = ((Long) subDomain.get("schoolId")).longValue();
				
					// Look up campuses for school
					List campuses = Campus.retrieveBySchoolId(schoolId);
					request.setAttribute("campuses", BeanUtil.convertList(campuses, CampusSchoolBean.class));
				
					return mapping.findForward("campusList");
				}
				else if (subDomain != null && subDomain.get("campusId") != null)
				{
					log.debug("Found campus in subdomain, showing this campus.");
					campusId = ((Long) subDomain.get("campusId")).longValue();
					campus = Campus.retrieveCampus(campusId);
					dorms = campus.getDorms();
				}
				// look for operator in subdomain
				else if (subDomain != null && subDomain.get("operatorId") != null)
				{
					log.debug("Found operator in subdomain, show list of campuses.");		
					operatorId = ((Long) subDomain.get("operatorId")).longValue();
				
					// Look up campuses for operator
					List campuses = Campus.retrieveByOperatorId(operatorId);
					request.setAttribute("campuses", BeanUtil.convertList(campuses, CampusSchoolBean.class));
				
					Cookie cookie = new Cookie("operatorId", "" + operatorId);
					cookie.setMaxAge(-1);
					cookie.setDomain(SecurityUtil.parseDomain(request));
					cookie.setPath("/");
					response.addCookie(cookie);
				
					return mapping.findForward("campusList");
				}
				else if (cookieRoomId > 0)
				{
					log.debug("Found room cookie.");
					roomId = cookieRoomId;
					
					// look up this laundry room
					room = LaundryRoom.retrieveLaundryRoom(roomId);
					
					// verify it is active
					if (!room.isActive())
					{
						// room is inactive
						log.debug("Room is inactive, showing homepage");
						
						// remove Cookie
						Cookie cookie = SecurityUtil.getCookie(request, "roomId");
			
						if (cookie != null)
						{
							cookie.setMaxAge(0);
							response.addCookie(cookie);
						}
						
						return mapping.findForward("homepage");
					}
					
					// get dormId
					dormId = room.getDormId();
					
					// Set laundry rooms
					rooms = LaundryRoom.retrieveByDormId(dormId);
					campus = room.getDorm().getCampus();
					dorms = campus.getDorms();
				}
				else
				{
					// found nothing, show information page
					log.debug("Found nothing, forward to homepage.");
					return mapping.findForward("homepage");
				}
			}
			// we have form data check
			else 
			{
				log.debug("Found form data.");
				if (operatorId > 0)
				{
					log.debug("Found operator, showing list of campuses");
					// Look up campuses for school
					List campuses = Campus.retrieveByOperatorId(operatorId);
					request.setAttribute("campuses", BeanUtil.convertList(campuses, CampusSchoolBean.class));
				
					return mapping.findForward("campusList");
				}
				if (schoolId > 0)
				{
					log.debug("Found school in form, showing list of campuses");
					// Look up campuses for school
					List campuses = Campus.retrieveBySchoolId(schoolId);
					request.setAttribute("campuses", BeanUtil.convertList(campuses, CampusSchoolBean.class));
				
					return mapping.findForward("campusList");
				}
				if (campusId > 0)
				{
					log.debug("Found campus in form.");
					campus = Campus.retrieveCampus(campusId);
					dorms = campus.getDorms();
				}
				else if (dormId > 0)
				{
					log.debug("Found dorm in form.");
					dorm = Dorm.retrieveDorm(dormId);
				
					// Set laundry rooms
					rooms = LaundryRoom.retrieveByDormId(dormId);

					// Pick first laundry room in list
					if (rooms.size() > 0)
					{
						room = (LaundryRoom) dorm.getLaundryRooms().get(0);
						roomId = room.getLaundryRoomId();
					}
					
					campus = dorm.getCampus();
					dorms = campus.getDorms();
				}
				else if (roomId > 0)
				{
					log.debug("Found room in form.");
					// look up this laundry room
					room = LaundryRoom.retrieveLaundryRoom(roomId);
					
					// get dormId
					dormId = room.getDormId();
					
					// Set laundry rooms
					rooms = LaundryRoom.retrieveByDormId(dormId);

					campus = room.getDorm().getCampus();
					dorms = campus.getDorms();

				}
				else
				{
					// found nothing, show information page
					log.debug("Found nothing, forward to homepage.");
					return mapping.findForward("homepage");
				}
			}
					
			if (room == null)
			{
				// we have dormId
				if (dormId > 0)
				{
					dorm = Dorm.retrieveDorm(dormId);
					
					// Set laundry rooms
					rooms = LaundryRoom.retrieveByDormId(dormId);
	
					// Pick first laundry room in list
					if (rooms.size() > 0)
					{
						room = (LaundryRoom) dorm.getLaundryRooms().get(0);
						roomId = room.getLaundryRoomId();
					}
				}
				else if (roomId > 0)
				{
					// look up this laundry room
					room = LaundryRoom.retrieveLaundryRoom(roomId);
			
					// get dormId
					dormId = room.getDormId();
			
					// Set laundry rooms
					rooms = LaundryRoom.retrieveByDormId(dormId);
				}
				else
				{
					// Pick first Dorm in list
					if (dorms.size() > 0)
					{
						dorm = (Dorm) dorms.get(0);
						dormId = dorm.getDormId();
					}
				}
				
				// Set laundry rooms
				rooms = LaundryRoom.retrieveByDormId(dormId);
				
				// Pick first laundry room in list
				if (rooms.size() > 0)
				{
					room = (LaundryRoom) dorm.getLaundryRooms().get(0);
					roomId = room.getLaundryRoomId();
				}
			}

			// Set Campus and School
			CampusBean campusBean = new CampusBean();
			BeanUtils.copyProperties(campusBean, campus);
			SchoolBean schoolBean = new SchoolBean();
			BeanUtils.copyProperties(schoolBean, campus.getSchool());
			campusBean.setSchoolBean(schoolBean);
			resultBean.setCampus(campusBean);
			
			// Set operator
			OperatorBean operatorBean = new OperatorBean();
			Operator operator = campus.getOperator();
			BeanUtils.copyProperties(operatorBean, operator);
			resultBean.setOperator(operatorBean);
			
			// Set dormId and dorm list
			resultBean.setDormId(dormId);
			resultBean.setDorms(BeanUtil.convertList(dorms, DormBean.class));
			
			// Set room
			resultBean.setLaundryRooms(BeanUtil.convertList(rooms, LaundryRoomBean.class));
			if (room != null)
				resultBean.setSelectedRoom(LaundryRoomStatusService.getRoomStatusBean(room));
			resultBean.setRoomId(roomId);
			
			//Added to display the drop down for c&g on the nav bar
			//if(operatorId > 0) {
			/*    log.debug("Adding list of all rooms for the operator");
				List allRooms = new ArrayList();
				for(Iterator iter = dorms.iterator(); iter.hasNext(); ) {
				    allRooms.addAll(((Dorm)iter.next()).getLaundryRooms(true));
				}
				request.getSession(true).setAttribute("allRoomsResult", BeanUtil.convertList(allRooms, LaundryRoomBean.class));
			*/
			//}
			
			// In order for request notification to flow correctly, we will put this in the session
			// request.setAttribute("result", resultBean);
			request.getSession(true).setAttribute("result", resultBean);
				
			// add cookie
			if (room != null)
			{
				Cookie cookie = new Cookie("roomId", "" + room.getLaundryRoomId());
				cookie.setMaxAge(31536000);
				cookie.setDomain(SecurityUtil.parseDomain(request));
				cookie.setPath("/");
				response.addCookie(cookie);
			}
				
			return mapping.findForward("roomStatus");
		}
		catch (UnknownObjectException uoe)
		{
			log.error("Unknown Object Error occurred", uoe);
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown.location"));
		}
		catch (DataAccessException dae)
		{
			log.error("Data Access Error occurred", dae);
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));			
		}
		catch (DataException de)
		{
			log.error("Data Error occurred", de);
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data"));
		}
		catch (Exception e)
		{
			log.error("Unknown Error occurred: " + e.getMessage(), e);
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			// Forward control to the appropriate 'failure' URI (change name as desired)
			forward = mapping.findForward("failure");
		}
		return (forward);
	}
	
	private static long getRoomIdCookieValue(HttpServletRequest request)
	{
		Cookie[] cookies = request.getCookies();
		
		if (cookies != null)
		{
			for (int i = 0; i < cookies.length; i++)
			{
				// if no locations were selected, go to the cookie value
				if (cookies[i].getName().equals("roomId"))
				{
					try
					{
						return Long.parseLong(cookies[i].getValue());
					}
					catch (NumberFormatException nfe)
					{
						return 0;
					}
				}
			}
		}
		
		return 0;
	}
}