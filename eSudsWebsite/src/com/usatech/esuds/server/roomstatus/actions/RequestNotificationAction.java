package com.usatech.esuds.server.roomstatus.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import com.usatech.esuds.server.model.LaundryRoomEquip;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.roomstatus.forms.RequestNotificationForm;

/**
 * @version 1.0
 * @author
 */
public class RequestNotificationAction extends Action {
    private static Log log = LogFactory.getLog(RequestNotificationAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ActionErrors errors = new ActionErrors();
        ActionMessages messages = new ActionMessages();
        ActionForward forward = new ActionForward();

        RequestNotificationForm notifyForm = (RequestNotificationForm) form;

        String emailAddr = notifyForm.getEmailAddress();
        long roomId = notifyForm.getRoomId();
        long[] selectedMachines = notifyForm.getSelectedMachines();
        boolean success = false;
        try {
            LaundryRoomEquip.notifyUserOnCycleComplete(emailAddr, selectedMachines, roomId, messages, errors);
            success = true;
            // null out values so they don't show on screen
            notifyForm.setEmailAddress(null);
            notifyForm.setSelectedMachines(new long[0]);
            //messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("notification.success"));
        } catch (DataException dae) {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data.access"));
        } catch (Exception e) {
            log.error("Unknown Error occurred: " + e.getMessage());
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
        }
        // If a message is required, save the specified key(s)
        // into the request for use by the <struts:errors> tag.
        if (!success) {
            saveErrors(request, errors);
            forward = mapping.findForward("failure");
        } else {
            saveMessages(request, messages);
            saveErrors(request, errors);
            forward = mapping.findForward("success");
        }

        return (forward);
    }
}
