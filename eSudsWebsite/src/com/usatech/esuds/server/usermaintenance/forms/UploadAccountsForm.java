/*
 * Created on Oct 8, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.usermaintenance.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
/**
 * @author bkrug
 *
 */
public class UploadAccountsForm extends ActionForm
{
	private Long campusId = null;
	private FormFile excelFile = null;
	private boolean deactivateOthers = false;
	private String columnNames = null;
	/**
	 *
	 */
	public UploadAccountsForm()
	{
		super();
	}

	public Long getCampusId() {
		return campusId;
	}
	public void setCampusId(Long campusId) {
		this.campusId = campusId;
	}
	public FormFile getExcelFile() {
		return excelFile;
	}
	public void setExcelFile(FormFile excelFile) {
		this.excelFile = excelFile;
	}

	public boolean isDeactivateOthers() {
		return deactivateOthers;
	}

	public void setDeactivateOthers(boolean deactivateOthers) {
		this.deactivateOthers = deactivateOthers;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		campusId = null;
		excelFile = null;
		deactivateOthers = false;
		columnNames = null;
	}

	public String getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String columnNames) {
		this.columnNames = columnNames;
	}
}
