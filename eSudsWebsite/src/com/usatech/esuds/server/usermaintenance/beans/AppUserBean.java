/*
 * Created on Oct 7, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.usermaintenance.beans;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AppUserBean
{
	private long appUserId;
	private boolean active;
	private String userName;

	public AppUserBean()
	{
		super();
	}

	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @return
	 */
	public String getUsername()
	{
		return userName;
	}

	/**
	 * @return
	 */
	public long getAppUserId()
	{
		return appUserId;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}

	/**
	 * @param string
	 */
	public void setUserName(String string)
	{
		userName = string;
	}

	/**
	 * @param i
	 */
	public void setAppUserId(long l)
	{
		appUserId = l;
	}

}
