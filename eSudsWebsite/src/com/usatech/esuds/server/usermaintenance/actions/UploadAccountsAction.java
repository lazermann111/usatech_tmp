package com.usatech.esuds.server.usermaintenance.actions;


import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.Dorm;
import com.usatech.esuds.server.model.LaundryRoom;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.ProcessingException;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.usermaintenance.forms.UploadAccountsForm;
import com.usatech.esuds.server.util.AccountUtil;

/**
 * @version 	1.0
 * @author bkrug
 */
public class UploadAccountsAction extends DispatchAction {
	private static Log log = LogFactory.getLog(UploadAccountsAction.class);
	private static final String OBJECT_TYPE =  SecurityService.LOCATION_ACCOUNTS;
	private static final String ACTION = SecurityService.CREATE + "," + SecurityService.MODIFY;

	public ActionForward init(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
			return processAction(mapping, form, request, response, true);
		}

	public ActionForward update(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
			return processAction(mapping, form, request, response, false);
		}

	public ActionForward get(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
	    UploadAccountsForm uaForm = (UploadAccountsForm) form;
		long campusId = 0;
		//figure out campus id (student accounts must be related to a campus level location)
		if(uaForm.getCampusId() != null) {
			campusId = uaForm.getCampusId().longValue();
			Campus campus = Campus.retrieveCampus(campusId);
			if(!SecurityService.checkCredentials(request, OBJECT_TYPE, campusId, SecurityService.READ) &&
			        !SecurityService.checkCredentials(request, OBJECT_TYPE, campus.getSchoolId(), SecurityService.READ))
				throw new NotPermittedException("Not permitted to access this resource.");
		} else {
		    List campuses = Campus.retrieveByUserPrivilege(SecurityService.getUserId(request), 3, OBJECT_TYPE, ACTION);
			request.setAttribute("campuses", campuses);
		    ActionErrors errors = new ActionErrors();
			//ActionMessages messages = new ActionMessages();
			errors.add("campus.required", new ActionError("error.campus.required"));
			saveErrors(request, errors);
		    return mapping.findForward("missing");
		}
        Connection conn = DataLayerMgr.getConnection("esuds-main");
        if("XLS".equalsIgnoreCase(request.getParameter("ext"))) {
    		response.setContentType("application/xls");
            response.setHeader("Content-Disposition","attachment;filename=StudentAccounts.xls");
            //create excel file
            try {
                AccountUtil.outputAccountsAsExcel(campusId, response.getOutputStream(), conn);
            } finally {
                if(conn != null) try { conn.close(); } catch(SQLException e) {}
            }
        } else {
            response.setContentType("application/csv");
            response.setHeader("Content-Disposition","attachment;filename=StudentAccounts.csv");
            //create csv file
            try {
                AccountUtil.outputAccountsAsCSV(campusId, response.getOutputStream(), conn);
            } finally {
                if(conn != null) try { conn.close(); } catch(SQLException e) {}
            }
        }
		return null; //mapping.findForward("file");
	}
	/*
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
			return processAction(mapping, form, request, response, true);
		}
		//*/
	public ActionForward processAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		boolean initOnly)
		throws Exception {
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ActionForward forward = new ActionForward();
		UploadAccountsForm uaForm = (UploadAccountsForm) form;
		long campusId = 0;
		List campuses = null;
		//figure out campus id (student accounts must be related to a campus level location)
		// 1. Look in the form for explicit campusId
		if(uaForm.getCampusId() != null) {
			campusId = uaForm.getCampusId().longValue();
			//set cookie???
		}
		FormFile ff = uaForm.getExcelFile();
		java.sql.Connection conn = null;
		try {
			if(campusId == 0) {
				//2. Look at subdomain (if more than one campus, then present list to select)
				Hashtable presets = SecurityService.getObjectForSubdomain(request);
				/*--Don't use cookie --
				if(presets == null || (presets.get("campusId") == null &&
						presets.get("dormId") == null && presets.get("roomId") == null &&
						presets.get("schoolId") == null)) {
					//3. Look for cookies
					Cookie[] cookies = request.getCookies();
					if(cookies != null) {
						presets = new Hashtable();
						for (int i = 0; i < cookies.length; i++) try {
							presets.put(cookies[i].getName(), Long.getLong(cookies[i].getValue()));
						} catch (NumberFormatException nfe) {
						} catch(NullPointerException npe) {
						}
					}
				}//*/
				if(presets != null) {
					if(presets.get("campusId") != null) {
						campusId = ((Long) presets.get("campusId")).longValue();
					} else if(presets.get("dormId") != null) {
						//find parent campusId
						long dormId = ((Long) presets.get("dormId")).longValue();
						campusId = Dorm.retrieveDorm(dormId).getCampusId();
					} else if(presets.get("roomId") != null) {
						//find parent campusId
						long roomId = ((Long) presets.get("roomId")).longValue();
						campusId = LaundryRoom.retrieveLaundryRoom(roomId).getDorm().getCampusId();
					} else if(presets.get("schoolId") != null) {
						//find school
						long schoolId = ((Long) presets.get("schoolId")).longValue();
						campuses = Campus.retrieveBySchoolId(schoolId);
					}
				}
			}

			// retrieve all campuses changable by user
			if(campuses == null)
			    campuses = Campus.retrieveByUserPrivilege(SecurityService.getUserId(request), 3, OBJECT_TYPE, ACTION);
			request.setAttribute("campuses", campuses);
			if(campusId == 0) {
			    if(campuses.size() == 1) {
					campusId = ((Campus)campuses.get(0)).getCampusId();
				} else {
					//send to jsp that lists campuses to select
					if(initOnly) {
						messages.add("campus.required", new ActionMessage("error.campus.required"));
						saveMessages(request, messages);
					} else {
						errors.add("campus.required", new ActionError("error.campus.required"));
						saveErrors(request, errors);
					}
					return mapping.findForward("missing");
				}
			}

			//check credentials
			Campus campus = Campus.retrieveCampus(campusId);
			if(!SecurityService.checkCredentials(request, OBJECT_TYPE, campusId, ACTION) &&
			        !SecurityService.checkCredentials(request, OBJECT_TYPE, campus.getSchoolId(), ACTION))
				throw new NotPermittedException("Not permitted to access this resource.");
			request.setAttribute("campusId", new Long(campusId));
			//request.setAttribute("campus", campus);
			//early exit for init only
			if(initOnly) return mapping.findForward("missing");
			//check file exists
			if(ff == null) {
				errors.add("file.required", new ActionError("error.file.required"));
				saveErrors(request, errors);
				return mapping.findForward("missing");
			}
			//get db connection
			conn = DataLayerMgr.getConnection("esuds-main");
			String[] columnNames;
			try {
				columnNames = ConvertUtils.convert(String[].class, uaForm.getColumnNames());
			} catch(ConvertException e) {
				throw new ProcessingException("Could not convert column names to a list", 0, 0, e);
			}
			//process excel file
			int[] users = uploadAccounts(campusId, SecurityService.getUserId(request), ff.getInputStream(), ff.getContentType(), uaForm.isDeactivateOthers(), columnNames, conn);
			messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("success.accounts.upload", new Integer(users[0]), new Integer(users[1]), new Integer(users[2])));
		} catch (java.io.IOException ioe) {
			log.warn("Reading excel file", ioe);
			errors.add("upload.io", new ActionError("error.excel.read"));
		} catch(DataException de) {
			log.warn("Retrieving location", de);
			errors.add("data.access", new ActionError("error.data.access"));
		} catch(SQLException sqle) {
			log.warn("Updating account", sqle);
			errors.add("data.access", new ActionError("error.data.access"));
		} catch(ProcessingException pe) {
			log.warn("Processing excel file", pe);
			errors.add("file.processing", new ActionError("error.excel.processing", pe.getMessage(), new Integer(pe.getPosition()), new Integer(pe.getAlreadyProcessed()), pe.getRootCause()));
		/*
		} catch(Exception e) {
			log.warn("Unknown exception", e);
			errors.add("unknown", new ActionError("error.unknown"));
		//*/
		} finally {
			if(conn != null) try { conn.close(); } catch(SQLException e) {}
			if(ff != null) ff.destroy();
		}

		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.
		if (!errors.isEmpty()) {
			saveErrors(request, errors);
			// Forward control to the appropriate 'failure' URI (change name as desired)
			forward = mapping.findForward("failure");
		} else {
			saveMessages(request, messages);
			// Forward control to the appropriate 'success' URI (change name as desired)
			forward = mapping.findForward("success");
		}

		// Finish with
		return (forward);
	}

	protected String getName() {
		return OBJECT_TYPE;
	}

	protected String getAction() {
		return ACTION;
	}

    /**
     * Uploads student account data from an Excel spreadsheet into the database.
     * Returns the number of accounts processed. The Excel spreadsheet should
     * have the following columns at the top with each row below containing the
     * appropriate information: RECORD_ACTION_CODE STUDENT_EMAIL_ADDR
     * STUDENT_ACCOUNT_CD STUDENT_ACCOUNT_BALANCE STUDENT_FNAME STUDENT_LNAME
     * STUDENT_ADDR1 STUDENT_ADDR2 STUDENT_CITY STUDENT_STATE_CD
     * STUDENT_POSTAL_CD
     *
     * @author bkrug
     *
     */
    public static int[] uploadAccounts(long locationId, long appUserId, java.io.InputStream dataStream, String contentType, boolean deactivateOthers, String[] columnNames,
            java.sql.Connection conn) throws IOException, SQLException, ProcessingException {
        log.debug("ContentType is " + contentType);
        RowValuesIterator rowIter;
        if(columnNames == null || columnNames.length == 0)
        	rowIter = SheetUtils.getExcelRowValuesIterator(dataStream);
        else
        	rowIter = SheetUtils.getExcelRowValuesIterator(dataStream, columnNames);
        Set<String> accountCodes;
        if(deactivateOthers)
        	accountCodes = new HashSet<String>();
        else
        	accountCodes = null;
        CallableStatement cs = conn.prepareCall("BEGIN PKG_CONSUMER_MAINT.sp_add_consumer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;");
        int updatedCnt = 0;
        int ignoredCnt = 0;
        int deactivatedCnt = 0;
        try {
            //get all the values for this row
            while(rowIter.hasNext()) {
                try {
                	Map<String,Object> columnValues = rowIter.next();
                    if(columnValues.size() > 0) { // don't process blank rows
	                    log.debug("Processing row with values=" + columnValues);
	                    //validations
	                    String rac = asString(columnValues.get("RECORD_ACTION_CODE"));
	                    boolean ignoreRow = false;
	                    if(rac == null) {
	                    	if(deactivateOthers)
	                    		rac = "Y";
	                    	else
	                    		throw new ProcessingException("An 'RECORD_ACTION_CODE' was not provided for row #"
	                                + (rowIter.getCurrentRowNum() + 1)
	                                + ". Please specify 'D' to Deactivate, 'C' to Create, or 'U' to Update this row. "
	                                + (updatedCnt + ignoredCnt + deactivatedCnt)
	                                + " account(s) were processed before this error. "
	                                + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), null);
	                    } else {
	                        switch(rac.trim().charAt(0)) {
	                            case 'D': case 'd': rac = "N"; break;
	                            case 'U': case 'u': rac = "Y"; break;
	                            case 'C': case 'c': rac = "Y"; break;
	                            case 'N': case 'n': ignoreRow = true; break;
	                            default: throw new ProcessingException("An invalid 'RECORD_ACTION_CODE' of '" + rac + "' was provided for row #"
	                                + (rowIter.getCurrentRowNum() + 1)
	                                + ". Please specify 'D' to Deactivate, 'C' to Create, or 'U' to Update this row. "
	                                + (updatedCnt + ignoredCnt + deactivatedCnt)
	                                + " account(s) were processed before this error. "
	                                + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), null);
	                        }
	                    }
	                    String acctCd = asString(columnValues.get("STUDENT_ACCOUNT_CD"));
                        if(deactivateOthers)
                        	accountCodes.add(acctCd);
	                    if(ignoreRow) {
	                        ignoredCnt++;
	                    } else {
	                        if(acctCd == null || acctCd.trim().length() == 0) {
	                            throw new ProcessingException("A 'STUDENT_ACCOUNT_CD' was not provided for row #"
	                                    + (rowIter.getCurrentRowNum() + 1)
	                                    + ". Please include the 'STUDENT_ACCOUNT_CD' column with the appropriate value for each row. "
	                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), null);
	                        }
	                        String email = asString(columnValues.get("STUDENT_EMAIL_ADDR"));
	                        if(email == null || email.trim().length() == 0) {
	                            throw new ProcessingException("A 'STUDENT_EMAIL_ADDR' was not provided for row #"
	                                    + (rowIter.getCurrentRowNum() + 1)
	                                    + ". Please include the 'STUDENT_EMAIL_ADDR' column with the appropriate value for each row. "
	                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), null);
	                        }
	                        BigDecimal balance;
	                        try {
	                            Object tmp = columnValues.get("STUDENT_ACCOUNT_BALANCE");
	                            if(tmp instanceof BigDecimal)
	                                balance = (BigDecimal) tmp;
	                            else if(tmp == null || tmp.toString().trim().length() == 0)
	                                balance = null;
	                            else
	                                balance = new BigDecimal(tmp.toString().trim());
	                        } catch(NumberFormatException nfe) {
	                            throw new ProcessingException("The value for 'STUDENT_ACCOUNT_BALANCE' in row #"
	                                    + (rowIter.getCurrentRowNum() + 1)
	                                    + " cannot be interpreted as a number. Please change it to a number or leave it blank. "
	                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), nfe);
	                        }
	                        Boolean notifyOn;
	                        try {
	                        	notifyOn = ConvertUtils.convert(Boolean.class, columnValues.get("AUTO_NOTIFY_ON"));
	                        } catch(ConvertException e) {
	                            throw new ProcessingException("The value for 'AUTO_NOTIFY_ON' in row #"
	                                    + (rowIter.getCurrentRowNum() + 1)
	                                    + " cannot be interpreted as a flag. Please change it to 'Y' or 'N' or leave it blank. "
	                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e);
	                        }
	                        String accountType = asString(columnValues.get("ACCOUNT_TYPE"));
	                        int accountTypeId;
	                        if(accountType == null || accountType.length() == 0)
	                        	accountTypeId = 1;
	                        else {
	                        	Object[] ret;
	                        	try {
	                				ret = DataLayerMgr.executeCall(conn, "GET_ACCOUNT_TYPE", new Object[] {accountType});
	                				accountTypeId = ConvertUtils.convert(Integer.class, ret[1]);
	                        	} catch(DataLayerException e1) {
	                				throw new ProcessingException("An error occurred while retrieving the account type in row #"
		                                    + (rowIter.getCurrentRowNum() + 1)  + ". " + (updatedCnt + ignoredCnt + deactivatedCnt)
		                                    + " account(s) were processed before this error. "
		                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
		                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e1);
	                			} catch(ConvertException e1) {
	                				throw new ProcessingException("An error occurred while retrieving the account type in row #"
                                    + (rowIter.getCurrentRowNum() + 1)  + ". " + (updatedCnt + ignoredCnt + deactivatedCnt)
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e1);
	                			} catch(SQLException e1) {
	                				if(e1.getErrorCode() == 1403)
	                					throw new ProcessingException("The value for 'ACCOUNT_TYPE' in row #"
	    	                                    + (rowIter.getCurrentRowNum() + 1)  + " is not recognized. Please use a correct account type name or number. "
	    	                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
	    	                                    + " account(s) were processed before this error. "
	    	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	    	                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e1);
	                				else
	                					throw new ProcessingException("An error occurred while retrieving the account type in row #"
	    	                                    + (rowIter.getCurrentRowNum() + 1)  + ". " + (updatedCnt + ignoredCnt + deactivatedCnt)
	    	                                    + " account(s) were processed before this error. "
	    	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	    	                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e1);
	                			}
		                    }
		                    log.debug("Using account type id " + accountTypeId);
	                        cs.setString(1, asString(columnValues.get("STUDENT_FNAME")));//pv_consumer_fname
	                        cs.setString(2, asString(columnValues.get("STUDENT_LNAME")));//pv_consumer_lname
	                        cs.setString(3, asString(columnValues.get("STUDENT_ADDR1")));//pv_consumer_addr1
	                        cs.setString(4, asString(columnValues.get("STUDENT_ADDR2")));//pv_consumer_addr2
	                        cs.setString(5, asString(columnValues.get("STUDENT_CITY")));//pv_consumer_city
	                        cs.setString(6, asString(columnValues.get("STUDENT_STATE_CD")));//pv_consumer_state
	                        cs.setString(7, asString(columnValues.get("STUDENT_POSTAL_CD")));//pv_consumer_postal_cd
	                        cs.setString(8, email);//pv_consumer_email_addr1
	                        cs.setInt(9, accountTypeId);//pn_consumer_type_id
	                        cs.setLong(10, locationId);//pn_location_id
	                        cs.setString(11, acctCd);//pv_consumer_account_cd
	                        cs.setString(12, rac);//pv_consumer_accnt_actv_yn_flag
	                        cs.setBigDecimal(13, balance);//pv_consumer_account_balance
	                        cs.registerOutParameter(14, Types.NUMERIC);//pn_return_code
	                        cs.registerOutParameter(15, Types.VARCHAR);//pv_error_message
	                        if(notifyOn == null)
	                        	cs.setNull(16, Types.CHAR);//pc_auto_notify_on
	                        else
	                        	cs.setString(16, notifyOn ? "Y" : "N");//pc_auto_notify_on
	                        cs.execute();
	                        int errCode = cs.getInt(14);
	                        String errMsg = cs.getString(15);
	                        if (errCode != 0) {
	                            throw new SQLException(errMsg, null, errCode);
	                        }
	                        conn.commit();
	                        if("Y".equals(rac)) updatedCnt++;
	                        else deactivatedCnt++;
	                        cs.clearParameters();
	                    }
                    }
               } catch (SQLException e) {
                    conn.rollback();
                    log.warn("While processing row #" + (rowIter.getCurrentRowNum() + 1), e);
                    AccountUtil.logAccountUpload(locationId, appUserId, updatedCnt + deactivatedCnt, ignoredCnt, rowIter.getCurrentRowNum()+1, e.getMessage(), conn);
                    throw new ProcessingException(
                            "An error occurred while processing row #"
                                    + (rowIter.getCurrentRowNum() + 1)
                                    + ". "
                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e);
                } catch (ProcessingException e) {
                	log.warn("While processing row #" + (rowIter.getCurrentRowNum() + 1), e);
                    AccountUtil.logAccountUpload(locationId, appUserId,  updatedCnt + deactivatedCnt, ignoredCnt, rowIter.getCurrentRowNum()+1, e.getMessage(), conn);
                    throw e;
                } catch (Exception e) {
                    conn.rollback();
                    log.warn("While processing row #" + (rowIter.getCurrentRowNum() + 1), e);
                    AccountUtil.logAccountUpload(locationId, appUserId,  updatedCnt + deactivatedCnt, ignoredCnt, rowIter.getCurrentRowNum()+1, e.getMessage(), conn);
                    throw new ProcessingException(
                            "An error occurred while processing row #"
                                    + (rowIter.getCurrentRowNum() + 1)
                                    + ". "
                                    + (updatedCnt + ignoredCnt + deactivatedCnt)
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                                    rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e);
                }
            }
        } finally {
            cs.close();
        }
        if(deactivateOthers) {
        	Object[] ret;
        	try {
				ret = DataLayerMgr.executeCall(conn, "DEACTIVATE_NOT_LISTED_ACCTS", new Object[] {accountCodes, locationId});
				deactivatedCnt += ConvertUtils.convert(Integer.class, ret[1]);
        	} catch(DataLayerException e) {
        		AccountUtil.logAccountUpload(locationId, appUserId,  updatedCnt + deactivatedCnt, ignoredCnt, rowIter.getCurrentRowNum()+1, e.getMessage(), conn);
                throw new ProcessingException("An error occurred while deactivating accounts not in the current file. Please contact USA Technologies for assistance.", rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e);
			} catch(ConvertException e) {
				AccountUtil.logAccountUpload(locationId, appUserId,  updatedCnt + deactivatedCnt, ignoredCnt, rowIter.getCurrentRowNum()+1, e.getMessage(), conn);
                throw new ProcessingException("An error occurred while deactivating accounts not in the current file. Please contact USA Technologies for assistance.", rowIter.getCurrentRowNum() + 1, (updatedCnt + ignoredCnt + deactivatedCnt), e);
			}
        	log.debug("Deactivated " + deactivatedCnt + " accounts");
        }
        AccountUtil.logAccountUpload(locationId, appUserId,  updatedCnt + deactivatedCnt, ignoredCnt, 0, null, conn);
        return new int[] {updatedCnt, ignoredCnt, deactivatedCnt};
    }

    protected static String asString(Object obj) {
        if(obj == null) return null;
        else return ConvertUtils.getStringSafely(obj).trim();
    }
}
