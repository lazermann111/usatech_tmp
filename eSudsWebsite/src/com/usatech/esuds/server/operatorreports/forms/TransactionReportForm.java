package com.usatech.esuds.server.operatorreports.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
/**
 * Form bean for a Struts application.
 * Users may access 4 fields on this form:
 * <ul>
 * <li>startDate - [your comment here]
 * <li>operatorId - [your comment here]
 * <li>laundryRoomId - [your comment here]
 * <li>endDate - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class TransactionReportForm extends ActionForm
{
	private String startDate = null;
	private long operatorId;
	private long laundryRoomId;
	private String endDate = null;
	private long paymentTypeId;
	/**
	 * Get startDate
	 * @return String
	 */
	public String getStartDate()
	{
		return startDate;
	}
	/**
	 * Set startDate
	 * @param <code>String</code>
	 */
	public void setStartDate(String s)
	{
		this.startDate = s;
	}
	/**
	 * Get operatorId
	 * @return long
	 */
	public long getOperatorId()
	{
		return operatorId;
	}
	/**
	 * Set operatorId
	 * @param <code>long</code>
	 */
	public void setOperatorId(long o)
	{
		this.operatorId = o;
	}
	/**
	 * Get laundryRoomId
	 * @return long
	 */
	public long getLaundryRoomId()
	{
		return laundryRoomId;
	}
	/**
	 * Set laundryRoomId
	 * @param <code>long</code>
	 */
	public void setLaundryRoomId(long l)
	{
		this.laundryRoomId = l;
	}
	/**
	 * Get endDate
	 * @return String
	 */
	public String getEndDate()
	{
		return endDate;
	}
	/**
	 * Set endDate
	 * @param <code>String</code>
	 */
	public void setEndDate(String e)
	{
		this.endDate = e;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		// Reset values are provided as samples only. Change as appropriate.
		startDate = "";
		operatorId = 0;
		laundryRoomId = 0;
		endDate = "";
		paymentTypeId = 0;
	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		return errors;
	}
	/**
	 * @return
	 */
	public long getPaymentTypeId()
	{
		return paymentTypeId;
	}

	/**
	 * @param l
	 */
	public void setPaymentTypeId(long l)
	{
		paymentTypeId = l;
	}

}
