package com.usatech.esuds.server.operatorreports.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 3 fields on this form:
 * <ul>
 * <li>id - [your comment here]
 * <li>type - [your comment here]
 * <li>name - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class DetailForm extends ActionForm
{

	private long id = -1;
	private String type = null;
	private String name = null;
	private long protectedId = -1;


	/**
	 * Get id
	 * @return String
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Set id
	 * @param <code>String</code>
	 */
	public void setId(long i)
	{
		id = i;
	}
	/**
	 * Get type
	 * @return String
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Set type
	 * @param <code>String</code>
	 */
	public void setType(String t)
	{
		type = t;
	}
	/**
	 * Get name
	 * @return String
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Set name
	 * @param <code>String</code>
	 */
	public void setName(String n)
	{
		name = n;
	}
	/**
	* Constructor
	*/
	public DetailForm()
	{

		super();

	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		// Reset values are provided as samples only. Change as appropriate.

		long id = -1;
		type = null;
		name = null;

	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();
		
		return errors;

	}
	
	public void setProtectedId(long i)
	{
		protectedId = i;
	}
}
