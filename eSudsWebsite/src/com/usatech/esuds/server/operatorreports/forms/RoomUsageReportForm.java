package com.usatech.esuds.server.operatorreports.forms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 5 fields on this form:
 * <ul>
 * <li>dateChoice - [your comment here]
 * <li>sinceDate - [your comment here]
 * <li>onDate - [your comment here]
 * <li>fromDate - [your comment here]
 * <li>toDate - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class RoomUsageReportForm extends ActionForm
{
	private long campusId = -1;
	private String dateChoice = null;
	private String sinceDate = null;
	private String onDate = null;
	private String fromDate = null;
	private String toDate = null;
	private List campuses = null;
	private String action = "buildSearch";

	/**
	 * Get dateChoice
	 * @return String
	 */
	public String getDateChoice()
	{
		return dateChoice;
	}

	/**
	 * Set dateChoice
	 * @param <code>String</code>
	 */
	public void setDateChoice(String d)
	{
		dateChoice = d;
	}
	/**
	 * Get sinceDate
	 * @return String
	 */
	public String getSinceDate()
	{
		return sinceDate;
	}

	/**
	 * Set sinceDate
	 * @param <code>String</code>
	 */
	public void setSinceDate(String s)
	{
		sinceDate = s;
	}
	/**
	 * Get onDate
	 * @return String
	 */
	public String getOnDate()
	{
		return onDate;
	}

	/**
	 * Set onDate
	 * @param <code>String</code>
	 */
	public void setOnDate(String o)
	{
		onDate = o;
	}
	/**
	 * Get fromDate
	 * @return String
	 */
	public String getFromDate()
	{
		return fromDate;
	}

	/**
	 * Set fromDate
	 * @param <code>String</code>
	 */
	public void setFromDate(String f)
	{
		fromDate = f;
	}
	/**
	 * Get toDate
	 * @return String
	 */
	public String getToDate()
	{
		return toDate;
	}

	/**
	 * Set toDate
	 * @param <code>String</code>
	 */
	public void setToDate(String t)
	{
		toDate = t;
	}
	/**
	* Constructor
	*/
	public RoomUsageReportForm()
	{

		super();

	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{

		// Reset values are provided as samples only. Change as appropriate.

		dateChoice = null;
		sinceDate = null;
		onDate = null;
		fromDate = null;
		toDate = null;

	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		if (action.equals("runReport") && campusId <= 0)
		{
		   errors.add("campusId", new ActionError("error.campusId.required"));
		}
		
		if (action.equals("runReport"))
		{
			if (dateChoice != null)
			{
				if (dateChoice.equalsIgnoreCase("since"))
				{
					if (sinceDate == null || sinceDate.length() == 0)
					{
						errors.add("sinceDate", new ActionError("error.sinceDate.required"));
					}
					else
					{
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
						try
						{
							format.parse(sinceDate);
						}
						catch (ParseException e)
						{
							errors.add("sinceDate", new ActionError("error.since.date.invalid"));
						}
					}
				}
				else if (dateChoice.equalsIgnoreCase("on"))
				{
					if (onDate == null || onDate.length() == 0)
					{
						errors.add("onDate", new ActionError("error.onDate.required"));
					}
					else
					{
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
						try
						{
							format.parse(onDate);
						}
						catch (ParseException e)
						{
							errors.add("onDate", new ActionError("error.on.date.invalid"));
						}
					}
				}
				else if (dateChoice.equalsIgnoreCase("fromTo"))
				{
					if (fromDate == null || fromDate.length() == 0)
					{
						errors.add("fromDate", new ActionError("error.fromDate.required"));
					}
					else
					{
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
						try
						{
							format.parse(fromDate);
						}
						catch (ParseException e)
						{
							errors.add("fromDate", new ActionError("error.from.date.invalid"));
						}
					}
					
					if (toDate == null || toDate.length() == 0)
					{
						errors.add("toDate", new ActionError("error.toDate.required"));
					}
					else
					{
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
						try
						{
							format.parse(toDate);
						}
						catch (ParseException e)
						{
							errors.add("toDate", new ActionError("error.to.date.invalid"));
						}
					}
				}
			}
			else
			{
				errors.add("dateChoice", new ActionError("error.dateChoice.required"));
			}
		}
		return errors;
	}
	/**
	 * Returns the campusId.
	 * @return long
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * Sets the campusId.
	 * @param campusId The campusId to set
	 */
	public void setCampusId(long _campusId)
	{
		this.campusId = _campusId;
	}

	/**
	 * @return
	 */
	public List getCampuses()
	{
		return campuses;
	}

	/**
	 * @param list
	 */
	public void setCampuses(List list)
	{
		campuses = list;
	}

	/**
	 * @return
	 */
	public String getAction()
	{
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string)
	{
		action = string;
	}

}
