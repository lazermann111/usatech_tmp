package com.usatech.esuds.server.operatorreports.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
/**
 * Form bean for a Struts application.
 * Users may access 2 fields on this form:
 * <ul>
 * <li>operatorId - [your comment here]
 * <li>action - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class OperatorForm extends ActionForm
{
	private long operatorId;
	private String action = null;
	/**
	 * Get operatorId
	 * @return long
	 */
	public long getOperatorId()
	{
		return operatorId;
	}
	/**
	 * Set operatorId
	 * @param <code>long</code>
	 */
	public void setOperatorId(long o)
	{
		this.operatorId = o;
	}
	/**
	 * Get action
	 * @return String
	 */
	public String getAction()
	{
		return action;
	}
	/**
	 * Set action
	 * @param <code>String</code>
	 */
	public void setAction(String a)
	{
		this.action = a;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		// Reset values are provided as samples only. Change as appropriate.
		operatorId = 0;
		action = null;
	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.
		// if ((field == null) || (field.length() == 0)) {
		//   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;
	}
}
