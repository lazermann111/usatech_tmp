/*
 * Created on Oct 20, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.operatorreports.beans;

import java.math.BigDecimal;
import java.util.List;

public class StudentAcctActivityBean
{
	private String studentName = null;
	private boolean active = false;
	private List trans = null;
	private BigDecimal totalAmount = null;

	/**
	 * 
	 */
	public StudentAcctActivityBean()
	{
		super();
	}

	/**
	 * @return
	 */
	public String getStudentName()
	{
		if (studentName != null)
		{
			return studentName;
		}
		else
		{
			return "";
		}
	}

	/**
	 * @return
	 */
	public BigDecimal getTotalAmount()
	{
		if (totalAmount != null)
		{
			return totalAmount;
		}
		else
		{
			return new BigDecimal(0);
		}
	}

	/**
	 * @return
	 */
	public List getTrans()
	{
		return trans;
	}

	/**
	 * @param string
	 */
	public void setStudentName(String string)
	{
		studentName = string;
	}

	/**
	 * @param string
	 */
	public void setTotalAmount(BigDecimal amt)
	{
		totalAmount = amt;
	}

	/**
	 * @param list
	 */
	public void setTrans(List list)
	{
		trans = list;
	}

	/**
	 * @return
	 */
	public boolean isActive()
	{
		return active;
	}

	/**
	 * @param b
	 */
	public void setActive(boolean b)
	{
		active = b;
	}
	
	public String getActiveText()
		{
			if (active)
			{
				return "active";
			}
			else
			{
				return "inactive";
			}
		}

}
