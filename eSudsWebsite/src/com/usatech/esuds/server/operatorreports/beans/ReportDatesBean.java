/*
 * Created on Oct 24, 2003
 *
 */
package com.usatech.esuds.server.operatorreports.beans;



/**
 * @author melissa
 *
 */
public class ReportDatesBean
{
	private String startDate = null;
	private String endDate = null;
	

	/**
	 * 
	 */
	public ReportDatesBean()
	{
		super();
	}

	/**
	 * @return
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string)
	{
		endDate = string;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string)
	{
		startDate = string;
	}

}
