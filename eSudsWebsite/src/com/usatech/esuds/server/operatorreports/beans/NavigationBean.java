package com.usatech.esuds.server.operatorreports.beans;

import com.usatech.esuds.server.model.UsageTotal;

/**
 * @author rothma
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class NavigationBean
{
	
	private long operatorId;
	private String schoolName = "";
	private long schoolId;
	private String campusName = "";
	private long campusId;
	private String dormName = "";
	private long dormId;
	private String startDate;
	private String endDate;
	private String type;

	/**
	 * Constructor for NavigationViewBean.
	 */
	public NavigationBean()
	{
		super();
	}

	/**
	 * Constructor NavigationViewBean.
	 * @param id
	 * @param type
	 */
	public NavigationBean(long _operatorId)
	{
		operatorId = _operatorId;
		type = UsageTotal.OPERATOR;
	}


	/**
	 * Returns the campusId.
	 * @return long
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * Returns the campusName.
	 * @return String
	 */
	public String getCampusName()
	{
		return campusName;
	}

	/**
	 * Returns the dormId.
	 * @return long
	 */
	public long getDormId()
	{
		return dormId;
	}

	/**
	 * Returns the dormName.
	 * @return String
	 */
	public String getDormName()
	{
		return dormName;
	}

	/**
	 * Returns the schoolId.
	 * @return long
	 */
	public long getSchoolId()
	{
		return schoolId;
	}

	/**
	 * Returns the schoolName.
	 * @return String
	 */
	public String getSchoolName()
	{
		return schoolName;
	}

	/**
	 * Sets the campusId.
	 * @param campusId The campusId to set
	 */
	public void setCampusId(long campusId)
	{
		this.campusId = campusId;
	}

	/**
	 * Sets the campusName.
	 * @param campusName The campusName to set
	 */
	public void setCampusName(String campusName)
	{
		this.campusName = campusName;
	}

	/**
	 * Sets the dormId.
	 * @param dormId The dormId to set
	 */
	public void setDormId(long dormId)
	{
		this.dormId = dormId;
	}

	/**
	 * Sets the dormName.
	 * @param dormName The dormName to set
	 */
	public void setDormName(String dormName)
	{
		this.dormName = dormName;
	}

	/**
	 * Sets the schoolId.
	 * @param schoolId The schoolId to set
	 */
	public void setSchoolId(long schoolId)
	{
		this.schoolId = schoolId;
	}

	/**
	 * Sets the schoolName.
	 * @param schoolName The schoolName to set
	 */
	public void setSchoolName(String schoolName)
	{
		this.schoolName = schoolName;
	}

	/**
	 * Returns the operatorId.
	 * @return long
	 */
	public long getOperatorId()
	{
		return operatorId;
	}

	/**
	 * Sets the operatorId.
	 * @param operatorId The operatorId to set
	 */
	public void setOperatorId(long operatorId)
	{
		this.operatorId = operatorId;
	}
	
	public void setFields(String type, long id, String name)
	{
		if (type.equals(UsageTotal.SCHOOL))
		{
			schoolId = id;
			schoolName = name;
		}
		else if (type.equals(UsageTotal.DORM));
		{
			dormId = id;
			
		}
	}

	/**
	 * Returns the endDate.
	 * @return String
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * Returns the startDate.
	 * @return String
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * Sets the endDate.
	 * @param endDate The endDate to set
	 */
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	/**
	 * Sets the startDate.
	 * @param startDate The startDate to set
	 */
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}
	
	public void resetNavigation(String level)
	{
		if (level.equals(UsageTotal.OPERATOR))
		{
			schoolId = 0;
			schoolName = null;
			campusId = 0;
			campusName = null;
			dormId = 0;
			dormName = null;
			
		}
		if (level.equals(UsageTotal.SCHOOL))
		{
			campusId = 0;
			campusName = null;
			dormId = 0;
			dormName = null;
		}
		else if (level.equals(UsageTotal.CAMPUS))
		{
			dormId = 0;
			dormName = null;
		}
		
		type = level;
	}

	/**
	 * Returns the type.
	 * @return String
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Sets the type.
	 * @param type The type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

}
