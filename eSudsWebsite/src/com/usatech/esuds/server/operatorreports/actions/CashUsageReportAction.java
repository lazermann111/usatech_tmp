package com.usatech.esuds.server.operatorreports.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.CampusBean;
import com.usatech.esuds.server.model.Campus;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.reports.EsudsTransactionReport;
import com.usatech.esuds.server.operatorreports.beans.ReportDatesBean;
import com.usatech.esuds.server.operatorreports.forms.CashUsageReportForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
import com.usatech.esuds.server.util.DateUtil;
import com.usatech.server.report.Report;
/**
 * @version 	1.0
 * @author
 */
public class CashUsageReportAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(CashUsageReportAction.class);
	
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward buildSearch(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		CashUsageReportForm reportForm = (CashUsageReportForm) form;
		
		long operatorId = getOperatorId(request);
		
		if (operatorId < 1)
		{
			return mapping.findForward("home");
		}
		else if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.READ))
		{
			try
			{
				List campuses = Campus.retrieveByOperatorId(operatorId);
				
				if (campuses.isEmpty())
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.no.campuses"));
				}
				else
				{
					List beans = BeanUtil.convertList(campuses, CampusBean.class);
					// reportForm.setCampuses(beans);
					request.getSession().setAttribute("campuses", beans);
				
					// set default values if not set before
					if (reportForm.getCampusId() <= 0)
					{
						reportForm.setCampusId(((CampusBean) beans.get(0)).getCampusId());
					}
			
					if (reportForm.getDateChoice() == null || reportForm.getDateChoice().equals(""))
					{
						reportForm.setDateChoice("today");
					}
				}
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: "+ e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("search");
		}
		
		return forward;
	}
	
	public ActionForward runReport(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		CashUsageReportForm reportForm = (CashUsageReportForm) form;
		
		long operatorId = getOperatorId(request);
		long campusId = reportForm.getCampusId();
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.READ))
		{
			try
			{
				Date startDate = null;
				Date endDate = null;
		
				GregorianCalendar date = new GregorianCalendar();
				if (reportForm.getDateChoice().equalsIgnoreCase("today"))
				{
					date.set(GregorianCalendar.HOUR_OF_DAY, 0);
					date.set(GregorianCalendar.MINUTE, 0);
					date.set(GregorianCalendar.SECOND, 0);
			
					startDate = date.getTime();
			
					date.set(GregorianCalendar.HOUR_OF_DAY, 23);
					date.set(GregorianCalendar.MINUTE, 59);
					date.set(GregorianCalendar.SECOND, 59);
			
					endDate = date.getTime();
								
				}
				else if (reportForm.getDateChoice().equalsIgnoreCase("yesterday"))
				{
					//set to yesterday
					date.roll(GregorianCalendar.DAY_OF_YEAR, false);
			
					date.set(GregorianCalendar.HOUR_OF_DAY, 0);
					date.set(GregorianCalendar.MINUTE, 0);
					date.set(GregorianCalendar.SECOND, 0);
		
					startDate = date.getTime();
				
					date.set(GregorianCalendar.HOUR_OF_DAY, 23);
					date.set(GregorianCalendar.MINUTE, 59);
					date.set(GregorianCalendar.SECOND, 59);
		
					endDate = date.getTime();
								
				}
				else if (reportForm.getDateChoice().equalsIgnoreCase("since"))
				{
					GregorianCalendar since = DateUtil.parseDate(reportForm.getSinceDate());
					since.set(GregorianCalendar.HOUR_OF_DAY, 0);
					since.set(GregorianCalendar.MINUTE, 0);
					since.set(GregorianCalendar.SECOND, 0);
		
					startDate = since.getTime();
					
					date.set(GregorianCalendar.HOUR_OF_DAY, 23);
					date.set(GregorianCalendar.MINUTE, 59);
					date.set(GregorianCalendar.SECOND, 59);
		
					endDate = date.getTime();
				}
				else if (reportForm.getDateChoice().equalsIgnoreCase("on"))
				{
					GregorianCalendar on = DateUtil.parseDate(reportForm.getOnDate());
					on.set(GregorianCalendar.HOUR_OF_DAY, 0);
					on.set(GregorianCalendar.MINUTE, 0);
					on.set(GregorianCalendar.SECOND, 0);
					startDate = on.getTime();
			
					on.set(GregorianCalendar.HOUR_OF_DAY, 23);
					on.set(GregorianCalendar.MINUTE, 59);
					on.set(GregorianCalendar.SECOND, 59);
		
					endDate = on.getTime();
				}
				else if(reportForm.getDateChoice().equalsIgnoreCase("fromto"))
				{
					GregorianCalendar from = DateUtil.parseDate(reportForm.getFromDate());
					from.set(GregorianCalendar.HOUR_OF_DAY, 0);
					from.set(GregorianCalendar.MINUTE, 0);
					from.set(GregorianCalendar.SECOND, 0);
		
					startDate = from.getTime();
			
					GregorianCalendar to = DateUtil.parseDate(reportForm.getToDate());
					to.set(GregorianCalendar.HOUR_OF_DAY, 23);
					to.set(GregorianCalendar.MINUTE, 59);
					to.set(GregorianCalendar.SECOND, 59);
		
					endDate = to.getTime();
				}
		
				ReportDatesBean dates = new ReportDatesBean();
				dates.setStartDate(DateUtil.convertDate(startDate));
				dates.setEndDate(DateUtil.convertDate(endDate));
				request.setAttribute("dates", dates);
				
				EsudsTransactionReport generator = new EsudsTransactionReport();
				
				generator.setStartDate(startDate);
				generator.setEndDate(endDate);
				generator.setCampusId(campusId);
				generator.setPaymentType(EsudsTransactionReport.CASH);
				
				Report report = generator.generateReport();
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				report.setTitle("Cash Usage Report for " +  format.format(startDate) + " to " + format.format(endDate));
				
				request.setAttribute("report", report);

				request.getSession().removeAttribute("campuses");
				
			}
			catch (DataAccessException de)
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.data"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: "+ e.getMessage());
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
			if (!errors.isEmpty())
			{
				saveErrors(request, errors);
				forward = mapping.findForward("failure");
			}
			else
			{
				forward = mapping.findForward("report");
			}
		
		return forward;
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		String operatorId = (String) request.getSession(false).getAttribute("operatorId");
		
		if (operatorId != null)
		{ 
			return Long.parseLong(operatorId);
		}
		else
		{
			return 0;
		}
	}
}
