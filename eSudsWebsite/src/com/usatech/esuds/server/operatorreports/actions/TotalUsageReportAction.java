package com.usatech.esuds.server.operatorreports.actions;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.model.UsageTotal;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.operatorreports.beans.NavigationBean;
import com.usatech.esuds.server.operatorreports.forms.TotalUsageReportForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.DateUtil;
/**
 * @version 	1.0
 * @author
 */
public class TotalUsageReportAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(TotalUsageReportAction.class);
	
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward buildSearch(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		TotalUsageReportForm reportForm = (TotalUsageReportForm) form;
		
		long operatorId = getOperatorId(request);
		
		if (operatorId < 1)
		{
			return mapping.findForward("home");
		}
		else if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.READ))
		{
			try
			{
				if (reportForm.getDateChoice() == null || reportForm.getDateChoice().equals(""))
				{
					reportForm.setDateChoice("today");
				}
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: "+ e.getMessage());
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("search");
		}
		
		return forward;
	}
	
	public ActionForward runReport(
				ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response)
				throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		TotalUsageReportForm reportForm = (TotalUsageReportForm) form;
		
		long operatorId = getOperatorId(request);
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.READ))
		{
			try
			{
				Date startDate = null;
				Date endDate = null;
				GregorianCalendar date = new GregorianCalendar();
		
				if (reportForm.getDateChoice().equalsIgnoreCase("today"))
				{
					date.set(GregorianCalendar.HOUR_OF_DAY, 0);
					date.set(GregorianCalendar.MINUTE, 0);
					date.set(GregorianCalendar.SECOND, 0);
			
					startDate = date.getTime();
				
					date.set(GregorianCalendar.HOUR_OF_DAY, 23);
					date.set(GregorianCalendar.MINUTE, 59);
					date.set(GregorianCalendar.SECOND, 59);
			
					endDate = date.getTime();
				}
				else if (reportForm.getDateChoice().equalsIgnoreCase("yesterday"))
				{
					date.roll(GregorianCalendar.DAY_OF_YEAR, false);
			
					date.set(GregorianCalendar.HOUR_OF_DAY, 0);
					date.set(GregorianCalendar.MINUTE, 0);
					date.set(GregorianCalendar.SECOND, 0);
			
					startDate = date.getTime();
				
					date.set(GregorianCalendar.HOUR_OF_DAY, 23);
					date.set(GregorianCalendar.MINUTE, 59);
					date.set(GregorianCalendar.SECOND, 59);
			
					endDate = date.getTime();
								
				}
				else if (reportForm.getDateChoice().equalsIgnoreCase("since"))
				{
					GregorianCalendar since = DateUtil.parseDate(reportForm.getSinceDate());
					since.set(GregorianCalendar.HOUR_OF_DAY, 0);
					since.set(GregorianCalendar.MINUTE, 0);
					since.set(GregorianCalendar.SECOND, 0);
				
					startDate = since.getTime();
							
					date.set(GregorianCalendar.HOUR_OF_DAY, 23);
					date.set(GregorianCalendar.MINUTE, 59);
					date.set(GregorianCalendar.SECOND, 59);
			
					endDate = date.getTime();
				}
				else if (reportForm.getDateChoice().equalsIgnoreCase("on"))
				{
					GregorianCalendar on = DateUtil.parseDate(reportForm.getOnDate());
					on.set(GregorianCalendar.HOUR_OF_DAY, 0);
					on.set(GregorianCalendar.MINUTE, 0);
					on.set(GregorianCalendar.SECOND, 0);
		
					startDate = on.getTime();
					on.set(GregorianCalendar.HOUR_OF_DAY, 23);
					on.set(GregorianCalendar.MINUTE, 59);
					on.set(GregorianCalendar.SECOND, 59);
		
					endDate = on.getTime();
				}
				else if(reportForm.getDateChoice().equalsIgnoreCase("fromto"))
				{
					GregorianCalendar from = DateUtil.parseDate(reportForm.getFromDate());
					from.set(GregorianCalendar.HOUR_OF_DAY, 0);
					from.set(GregorianCalendar.MINUTE, 0);
					from.set(GregorianCalendar.SECOND, 0);
		
					startDate = from.getTime();
			
					GregorianCalendar to = DateUtil.parseDate(reportForm.getToDate());
					to.set(GregorianCalendar.HOUR_OF_DAY, 23);
					to.set(GregorianCalendar.MINUTE, 59);
					to.set(GregorianCalendar.SECOND, 59);
		
					endDate = to.getTime();
				}
				
				// Retrieve Schools with this operator
				List results = UsageTotal.retrieveTotalsBySchool(operatorId, startDate, endDate);
				request.setAttribute("results", results);
					
				NavigationBean nav = new NavigationBean(operatorId);
				nav.setStartDate(DateUtil.convertDate(startDate));
				nav.setEndDate(DateUtil.convertDate(endDate));
		
				request.getSession(true).setAttribute("navigate", nav);
			}
			catch (DataAccessException dae)
			{
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: "+ e.getMessage());
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("report");
		}
		
		return forward;
	}
	
	public ActionForward showDetail(
				ActionMapping mapping,
				ActionForm form,
				HttpServletRequest request,
				HttpServletResponse response)
				throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		TotalUsageReportForm reportForm = (TotalUsageReportForm) form;
		
		NavigationBean navigate = (NavigationBean) request.getSession().getAttribute("navigate");
		
		// Get dates
		Date startDate = DateUtil.parseDate(navigate.getStartDate()).getTime();
		Date endDate = DateUtil.parseDate(navigate.getEndDate()).getTime();
		
		// Get operatorId
		long operatorId = navigate.getOperatorId();
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.READ))
		{
			try
			{
				long id;
				String name;
			
				if (reportForm.getType().equals(UsageTotal.OPERATOR))
				{
					log.debug("Type is Operator");
					log.debug("came from navigation");
				
					//came from navigation
					navigate.resetNavigation(UsageTotal.OPERATOR);
				
					//Retrieve Campuses with this school
					List results = UsageTotal.retrieveTotalsBySchool(operatorId, startDate, endDate);
					request.setAttribute("results", results);
				
				
				}
			
				else if (reportForm.getType().equals(UsageTotal.SCHOOL))
				{
					log.debug("Type is School");
				
					if (reportForm.getId() < 1)
					{
						log.debug("came from navigation");
					
						// came from navigation
						id = navigate.getSchoolId();
						log.debug("schoolId = " + id);
						navigate.resetNavigation(UsageTotal.SCHOOL);
					}
					else
					{
						id = reportForm.getId();
						log.debug("schoolId = " + id);
						navigate.setSchoolId(id);
						navigate.setSchoolName(reportForm.getName());
						navigate.setType(UsageTotal.SCHOOL);
					}
				
					//Retrieve Campuses with this school
					List results = UsageTotal.retrieveTotalsByCampus(id, operatorId, startDate, endDate);
					request.setAttribute("results", results);
				

					
				}
				else if (reportForm.getType().equals(UsageTotal.CAMPUS))
				{
					log.debug("Type is Campus");
				
					if (reportForm.getId() < 1)
					{
						log.debug("came from navigation");
					
						//came from navigation
						id = navigate.getCampusId();
						log.debug("campusId = " + id);
						navigate.resetNavigation(UsageTotal.CAMPUS);
					}
					else
					{
						id = reportForm.getId();
						navigate.setCampusId(id);
						log.debug("campusId = " + id);
						navigate.setCampusName(reportForm.getName());
						navigate.setType(UsageTotal.CAMPUS);
					}
				
					//Retrieve Dorms with this campus
					List results = UsageTotal.retrieveTotalsByDorm(id, operatorId, startDate, endDate);
					request.setAttribute("results", results);

				}
				else if (reportForm.getType().equals(UsageTotal.DORM))
				{
					log.debug("Type is Dorm");
				
					if (reportForm.getId() < 1)
					{
						log.debug("came from navigation");
					
						// came from navigation
						id = navigate.getDormId();
						log.debug("dormId = " + id);
						navigate.resetNavigation(UsageTotal.DORM);
					}
					else
					{
						id = reportForm.getId();
						navigate.setDormId(id);
						log.debug("dormId = " + id);
						navigate.setDormName(reportForm.getName());
						navigate.setType(UsageTotal.DORM);
					}
				
					//Retrieve Laundry Rooms with this dorm
					List results = UsageTotal.retrieveTotalsByLaundryRoom(id, operatorId, startDate, endDate);
					request.setAttribute("results", results);
					request.getSession().setAttribute("navigate", navigate);
				
				}
			}
			catch (DataAccessException dae)
			{
				errors.add("data.access", new ActionError("error.data.access"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: "+ e.getMessage());
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("report");
		}
		
		return forward;
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		String id  = (String) request.getSession(false).getAttribute("operatorId");
		
		if (id != null)
		{
			return Long.parseLong(id);
		}
		else
		{
			return 0;
		}
	}

}
