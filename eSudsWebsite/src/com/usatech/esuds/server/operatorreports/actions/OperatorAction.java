package com.usatech.esuds.server.operatorreports.actions;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.beans.OperatorBean;
import com.usatech.esuds.server.model.Operator;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.operatorreports.forms.OperatorForm;
import com.usatech.esuds.server.security.exceptions.NotPermittedException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.esuds.server.util.BeanUtil;
/**
 * @version 	1.0
 * @author
 */
public class OperatorAction extends DispatchAction
{
	private static Log log = LogFactory.getLog(OperatorAction.class);
	private static final String OBJECT_TYPE = SecurityService.OPERATOR;
	
	public ActionForward show(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		OperatorForm operatorForm = (OperatorForm) form;
		long operatorId = operatorForm.getOperatorId();
		
		if (operatorId < 1)
		{
			operatorId = getOperatorId(request);
		}
		
		if (SecurityService.checkCredentials(request, OBJECT_TYPE, operatorId, SecurityService.READ))
		{
			try
			{
				Operator operator = Operator.retrieveOperator(operatorId);
				BeanUtils.copyProperties(operatorForm, operator);
				request.getSession(false).setAttribute("operatorId", "" + operatorId);
			}
			catch (UnknownObjectException uoe)
			{
				errors.add("unknown.operator", new  ActionError("error.unknown.operator"));
			}
			catch (Exception e)
			{
				log.error("Unknown error occurred: " + e.getMessage());
				errors.add("unknown.error", new ActionError("error.unknown"));
			}
		}
		else
		{
			throw new NotPermittedException("Not permitted to view this resource.");
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			forward = mapping.findForward("success");
		}
		
		return forward;
	}
	
	public ActionForward retrieveList(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{	
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		int num = 0;
		
		try
		{
			List ids = SecurityService.getAppObjectIds(request, SecurityService.OPERATOR);
			num = ids.size();
			
			if (num == 0)
			{
				// user does not have access to any operators
				throw new NotPermittedException();
			}
			else if (num == 1 && ((String) ids.get(0)).equals("-1"))
			{
				List operators = Operator.retrieveAll();
				num = operators.size();
				
				List beans = BeanUtil.convertList(operators, OperatorBean.class);
								
				request.setAttribute("operators", beans);				
			}
			else if (num == 1)
			{
					// only one, retrieve
					request.getSession().setAttribute("operatorId", "" + (String) ids.get(0));
					return mapping.findForward("single");
			}
			else
			{
				List beans = new ArrayList();
				for (int i = 0; i < ids.size(); i++)
				{
					long id = Long.parseLong((String)ids.get(i));
					Operator operator = Operator.retrieveOperator(id);
					OperatorBean operatorBean = new OperatorBean();
					BeanUtils.copyProperties(operatorBean, operator);
					beans.add(operatorBean);
				}

				request.setAttribute("operators", beans);	
			}
		}
		catch (UnknownObjectException uoe)
		{
			errors.add("unknown.operator", new  ActionError("error.unknown.operator"));
		}
		catch (Exception e)
		{
			log.error("Unknown error occurred: " + e.getMessage());
			errors.add("unknown.error", new ActionError("error.unknown"));
		}
		
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			forward = mapping.findForward("failure");
		}
		else
		{
			if (num > 1)
			{
				forward = mapping.findForward("multiple");
			}
			else
			{
				forward = mapping.findForward("single");
			}
		}
		
		return forward;
	}
	
	private static long getOperatorId(HttpServletRequest request)
	{
		String operatorId = (String) request.getSession(false).getAttribute("operatorId");
		
		if (operatorId != null)
		{
			return Long.parseLong(operatorId);
		}
		else
		{
			return 0;
		}
	}
}
