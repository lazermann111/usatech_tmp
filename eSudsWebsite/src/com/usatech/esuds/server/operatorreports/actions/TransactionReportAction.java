package com.usatech.esuds.server.operatorreports.actions;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.usatech.esuds.server.model.reports.EsudsTransactionReport;
import com.usatech.esuds.server.operatorreports.forms.TransactionReportForm;
import com.usatech.esuds.server.util.DateUtil;
import com.usatech.server.report.Report;
/**
 * @version 	1.0
 * @author
 */
public class TransactionReportAction extends DispatchAction
{
	public ActionForward runReport(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ActionForward forward = new ActionForward();
		
		TransactionReportForm transactionReportForm = (TransactionReportForm) form;
		long operatorId = transactionReportForm.getOperatorId();
		long laundryRoomId = transactionReportForm.getLaundryRoomId();
		long paymentTypeId = transactionReportForm.getPaymentTypeId();
		Date startDate = DateUtil.parseDate(transactionReportForm.getStartDate()).getTime();
		Date endDate = DateUtil.parseDate(transactionReportForm.getEndDate()).getTime();
		
		EsudsTransactionReport reportGenerator = new EsudsTransactionReport();
		
		Report report = null;
		
		try
		{
			reportGenerator.setOperatorId(operatorId);
			reportGenerator.setStartDate(startDate);
			reportGenerator.setEndDate(endDate);
			report = reportGenerator.generateReport();
			
			request.setAttribute("report", report);
		}
		catch (Exception e)
		{
			// Report the error using the appropriate name and ID.
			errors.add("name", new ActionError("id"));
		}
		// If a message is required, save the specified key(s)
		// into the request for use by the <struts:errors> tag.
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
			// Forward control to the appropriate 'failure' URI (change name as desired)
			forward = mapping.findForward("failure");
		}
		else
		{
			// Forward control to the appropriate 'success' URI (change name as desired)
			forward = mapping.findForward("success");
		}
		// Finish with
		return (forward);
	}
}
