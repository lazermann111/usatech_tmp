package com.usatech.esuds.server.init;

import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 * @version 1.0
 * @author
 */
public class InitServlet extends HttpServlet {

    /**
     * @see javax.servlet.GenericServlet#void ()
     */
    public void init() throws ServletException {
        try {
            //set the jaas login config
            System.out.println("~~~ Got log4j class: " + Class.forName("org.apache.commons.logging.impl.Log4JLogger"));
            Log log = LogFactory.getLog(InitServlet.class);
            System.setProperty("java.security.auth.login.config", new java.io.File(new java.net.URI(getClass().getClassLoader().getResource("jaas.properties").toString())).getAbsolutePath());
            System.out.println("~~~ Logging for InitServlet: Log=" + log
            		+ "; b/c org.apache.commons.logging.Log="
            		+ LogFactory.getFactory().getAttribute("org.apache.commons.logging.Log") + " (attribute), "
            		+ System.getProperty("org.apache.commons.logging.Log") + "(system)"

            );

            Configuration conf = null;
            URL file = null;
            try {
                file = getClass().getClassLoader().getResource("Torque.properties");
                if (file != null) {
                    conf = new PropertiesConfiguration(file);
                    Torque.init(conf);
                } else {
                    log.fatal("Could not find Torque.properties ");
                }

            } catch (TorqueException te) {
                log.fatal("TORQUE COULD NOT INITIALIZE!", te);
            }
        } catch (Throwable t) {
            System.err.println("!!! Could not initialize InitServlet");
            t.printStackTrace();
        }
    }

	public void destroy() {
		super.destroy();
		Torque.shutdown();
	}
}
