package com.usatech.esuds.server.model;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.Device;
import com.usatech.server.persistence.DevicePeer;
import com.usatech.server.persistence.DeviceType;
import com.usatech.server.persistence.DeviceTypePeer;
import com.usatech.server.persistence.Host;

public  class RoomController
{
	private Device device = null;
	
	private static final String DEVICE_TYPE_DESC = "eSuds Room Controller";
	private static Log log = LogFactory.getLog(RoomController.class);
	
	public RoomController() throws DataException
	{
		device = new Device();
		device.setNew(true);
		
		DeviceType type = null;
		try
		{
			type = DeviceTypePeer.getDeviceTypeByDesc(DEVICE_TYPE_DESC);
		}
		catch (TorqueException e)
		{
			log.error("Error setting type to " + DEVICE_TYPE_DESC + ": " + e.getMessage());
			device = null;
			throw new DataException("Could not create r oom controller.");
		}
	}
	
	protected RoomController(Device d) throws DataException
	{
		String typeDesc = null;
		try
		{
			typeDesc = d.getDeviceType().getDeviceTypeDesc();
		}
		catch (TorqueException e)
		{
			log.error("Could not get device type for device: " + e.getMessage());
			throw new DataException("Could not create room controller.");
		}
		
		if (typeDesc.equals(DEVICE_TYPE_DESC))
		{
			device = d;
		}
		else
		{
			throw new DataException("Device " + d + " is wrong type.");
		}
	}
	
	public long getId()
	{
		return device.getDeviceId();
	}
	
	public String getName()
	{
		return device.getDeviceName();
	}
	
	public void setName(String name)
	{
		device.setDeviceName(name);
	}
	
	public String getSerialNumber()
	{
		return device.getDeviceSerialCd();
	}
	
	public String getSerialCd()
	{
		return device.getDeviceSerialCd();
	}
	
	public void setSerialCd(String cd)
	{
		device.setDeviceSerialCd(cd);
	}
	
	public boolean isActive()
	{
		if (device.getDeviceActiveYnFlag().equalsIgnoreCase("Y"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setActive(boolean active)
	{
		if (active)
		{
			device.setDeviceActiveYnFlag("Y");
		}
		else
		{
			device.setDeviceActiveYnFlag("N");
		}
	}
	
	public List retrieveLaundryRoomEquips() throws DataAccessException
	{
		List hosts = null;
		
		try
		{
			hosts = device.getHosts();
		}
		catch (TorqueException e)
		{
			log.error("Error getting hosts " + e.getMessage());
			throw new DataAccessException();
		}
		
		List equips = new ArrayList();
		for (Iterator i = hosts.iterator(); i.hasNext();)
		{
			Host host = (Host) i.next();
			equips.add(new LaundryRoomEquip(host));
		}
		
		return equips;
	}
	
	/**
	 * @param type
	 * @return List of LaundryRoomEquips for this room controller of type type ordered by label
	 * @throws DataException
	 */
	public List retrieveLaundryRoomEquips(String type) throws DataException
	{
		List hosts = null;
		try
		{
			hosts = device.getHosts(type);
		}
		catch (TorqueException e)
		{
			log.error("Error getting hosts of type " + type + " " + e.getMessage());
			throw new DataAccessException();
		}
		
		List equips = new ArrayList();
		for (Iterator i = hosts.iterator(); i.hasNext();)
		{
			Host host = (Host) i.next();
			equips.add(new LaundryRoomEquip(host));
		}
		
		return equips;
	}

	/**
	 * @param type
	 * @param position
	 * @return
	 */
	public List retrieveLaundryRoomEquips(String type, int position) throws DataException
	{
		List hosts = null;
		try
		{
			hosts = device.getHosts(type, position);
		}
		catch (TorqueException e)
		{
			log.error("Error getting hosts of type " + type + " at position "+ position + " " + e.getMessage());
			throw new DataAccessException();
		}
		
		List equips = new ArrayList();
		for (Iterator i = hosts.iterator(); i.hasNext();)
		{
			Host host = (Host) i.next();
			equips.add(new LaundryRoomEquip(host));
		}
		
		return equips;
	}
	
	public void validate() throws DataException
	{
		if (!device.isNew() && device.getDeviceId() < 1)
		{
			throw new DataException("Id is not valid.");
		}
		
		if (device.getDeviceName() == null || device.getDeviceName().equals(""))
		{
			throw new DataException("Name is not valid.");
		}
		
		if (device.getDeviceSerialCd() == null || device.getDeviceSerialCd().equals(""))
		{
			throw new DataException("SerialCd is not valid.");
		}
		
		if (device.getDeviceActiveYnFlag() == null || device.getDeviceActiveYnFlag().equals(""))
		{
			throw new DataException("ActiveFlag is not valid.");
		}
		
	}
	
	public void save() throws DataException
	{
		validate();
		
		try
		{
			device.save();
		}
		catch (Exception e)
		{
			log.error("Error saving device " + e.getMessage());
			throw new DataException("Could not save room controller.");
		}
	}
	
	public static RoomController retrieveRoomController(long roomControllerId) throws DataException, UnknownObjectException
	{
		Device device = null;
		try
		{
			device = DevicePeer.retrieveByPK(roomControllerId);
		}
		catch (NoRowsException e)
		{
			log.error("Device with id " + roomControllerId + " not found.");
			throw new UnknownObjectException("Room controller not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving device with id " + roomControllerId + ": " + tmre.getMessage());
			throw new DataException("More than one room controller found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving device with id " + roomControllerId + ": " + te.getMessage());
			throw new DataAccessException();
		}
		
		return new RoomController(device);
	}
}
