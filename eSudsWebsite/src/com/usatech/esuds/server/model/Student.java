/*
 * *******************************************************************
 * 
 * File Student.java
 * 
 * Created on March 25, 2004
 *
 * Create by Melissa Klein
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.StudentAccountBean;
import com.usatech.esuds.server.beans.StudentBean;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.Consumer;
import com.usatech.server.persistence.ConsumerAcct;
import com.usatech.server.persistence.ConsumerPeer;
import com.usatech.server.persistence.ConsumerType;
import com.usatech.server.persistence.ConsumerTypePeer;

public class Student {
    private static Log log = LogFactory.getLog(Student.class);

    private static final String CONSUMER_TYPE_DESC = "Student";

    private Consumer student = null;

    public Student() throws DataException {
        student = new Consumer();
        student.setNew(true);

        ConsumerType type = null;
        try {
            type = ConsumerTypePeer.retrieveByName(CONSUMER_TYPE_DESC);
        } catch (TorqueException e) {
            log.error("Error setting type to " + CONSUMER_TYPE_DESC + ": " + e.getMessage());
            student = null;
            throw new DataException("Could not create Student.");
        }
    }

    protected Student(Consumer c) throws DataException {
        String typeDesc = null;

        try {
            typeDesc = c.getConsumerType().getConsumerTypeDesc();
        } catch (TorqueException te) {
            log.error("Could not get location type for consumer. " + te.getMessage());
            throw new DataException("Could not create student.");
        }

        if (typeDesc.equals(CONSUMER_TYPE_DESC)) {
            student = c;
        } else {
            throw new DataException("Consumer is wrong type.");
        }

    }

    /**
     * @return
     */
    public long getStudentId() {
        return student.getConsumerId();
    }

    public String getFirstName() {
        return student.getConsumerFname();
    }

    public void setFirstName(String fName) {
        student.setConsumerFname(fName);
    }

    public String getMiddleName() {
        return student.getConsumerMname();
    }

    public void setMiddleName(String mName) {
        student.setConsumerMname(mName);
    }

    public String getLastName() {
        return student.getConsumerLname();
    }

    public void setLastName(String lName) {
        student.setConsumerLname(lName);
    }

    public String getEmailAddr() {
        return student.getConsumerEmailAddr1();
    }

    public void setEmailAddr(String emailAddr) {
        student.setConsumerEmailAddr1(emailAddr);
    }

    public String getNotifyEmailAddr() {
        return student.getConsumerEmailAddr2();
    }

    public void setNotifyEmailAddr(String emailAddr) {
        student.setConsumerEmailAddr2(emailAddr);
    }

    public List getStudentAccounts() throws DataException {
        List accts;
        ArrayList list = new ArrayList();

        try {
            accts = student.getConsumerAccts();
        } catch (TorqueException e) {
            log.error("Error getting student accounts: " + e.getMessage());
            throw new DataAccessException();
        }

        for (Iterator i = accts.iterator(); i.hasNext();) {
            list.add(new StudentAccount((ConsumerAcct) i.next()));
        }

        return list;
    }

    public StudentAccount getActiveStudentAccount() throws DataException {
        List list;
        try {
            list = student.getActiveConsumerAccounts();
        } catch (TorqueException e) {
            log.error("Error getting student accounts: " + e.getMessage());
            throw new DataAccessException();
        }

        if (list.size() > 1) {
            log.error("More than one active student accounts found.");
            throw new DataException("Too many active accounts.");
        } else if (list.size() == 0) {
            return null;
        } else {
            return new StudentAccount((ConsumerAcct) list.get(0));
        }
    }

    public StudentAccountBean retrieveActiveAccountInfo() throws DataException, UnknownObjectException {
        List activeAccounts = null;

        try {
            activeAccounts = student.getActiveConsumerAccounts();
        } catch (TorqueException te) {
            log.error("Error retrieving accounts for student with id " + student.getConsumerId() + ": "
                    + te.getMessage());
            throw new DataAccessException();
        }

        // should be only one active account
        if (activeAccounts.size() > 1) {
            throw new DataException("Too many active accounts");
        } else if (activeAccounts.size() == 0) {
            return null;
        } else {
            ConsumerAcct account = (ConsumerAcct) activeAccounts.get(0);
            StudentAccountBean studentAccountBean = new StudentAccountBean();

            // set studentId
            studentAccountBean.setStudentId(student.getConsumerId());

            // get info out of account
            studentAccountBean.setAccountId(account.getConsumerAcctId());
            studentAccountBean.setActive(account.isActive());

            return studentAccountBean;
        }
    }

    public StudentBean retrieveStudentInformation() throws UnknownObjectException, DataException {
        StudentBean studentBean = new StudentBean();
        studentBean.setFirstName(student.getConsumerFname());
        studentBean.setMiddleName(student.getConsumerMname());
        studentBean.setLastName(student.getConsumerLname());
        studentBean.setEmailAddr(student.getConsumerEmailAddr1());
        studentBean.setNotifyEmailAddr(student.getConsumerEmailAddr2());

        return studentBean;

    }

    public void save() throws DataException {
        try {
            student.save();
        } catch (Exception e) {
            log.error("Error saving student: " + e.getMessage());
            throw new DataException("Could not save student.");
        }
    }

    public void reset() {
        student = null;
    }

    public void validate() throws DataException {
        if (!student.isNew() && student.getConsumerId() < 1) {
            throw new DataException("Student id is not valid.");
        }
        /*
         * if (student.getLocationId() < 1) { throw new DataException("Campus id
         * is not valid."); }
         */
    }

    public static Student retrieveStudent(long studentId) throws UnknownObjectException, DataException {
        Consumer student = null;

        try {
            student = ConsumerPeer.retrieveByPK(studentId);

            // check type
            if (student.getConsumerType().getConsumerTypeDesc().equals(CONSUMER_TYPE_DESC)) {
                return new Student(student);
            } else {
                throw new UnknownObjectException("Student not found.");
            }
        } catch (NoRowsException e) {
            log.error("Student with id " + studentId + " not found.");
            throw new UnknownObjectException("Student not found.");
        } catch (TooManyRowsException tmre) {
            log.error("Error retrieving student with id: " + studentId + " " + tmre.getMessage());
            throw new DataException("More than one student found.");
        } catch (TorqueException te) {
            log.error("Error retrieving student with id: " + studentId + " " + te.getMessage());
            throw new DataAccessException();
        }
    }

    public static List retrieveAll() throws DataException {
        List consumers = null;

        try {
            consumers = ConsumerPeer.retrieveByConsumerTypeDesc(CONSUMER_TYPE_DESC);
        } catch (TorqueException te) {
            log.error("Error retrieving all students: " + te.getMessage());
            throw new DataAccessException();
        }

        List students = new ArrayList();

        for (Iterator i = consumers.iterator(); i.hasNext();) {
            Consumer l = (Consumer) i.next();
            students.add(new Student(l));
        }

        return students;
    }

    public static Student retrieveByEmail(String primaryEmailAddr) throws DataException {
         try {
            List list = ConsumerPeer.retrieveByEmailAndConsumerTypeDesc(primaryEmailAddr, CONSUMER_TYPE_DESC);
            if(list.size() == 0) return null;
            if(list.size() > 1) throw new DataException("More than one student found with email = '" + primaryEmailAddr + "'");
            return new Student((Consumer)list.get(0));
        } catch (TorqueException te) {
            log.error("Error retrieving student with email = '" + primaryEmailAddr + "'", te);
            throw new DataAccessException();
        }
    }
    /*
     * public static List retrieveByCampusId(long campusId) throws DataException {
     * List consumers = null;
     * 
     * try { consumers =
     * ConsumerPeer.retrieveByConsumerTypeAndLocation(CONSUMER_TYPE_DESC,
     * campusId); } catch (TorqueException te) { log.error("Error retrieving
     * students for campus " + campusId + ": " + te.getMessage()); throw new
     * DataAccessException(); }
     * 
     * List students = new ArrayList();
     * 
     * for (Iterator i = consumers.iterator(); i.hasNext();) { Consumer c =
     * (Consumer) i.next(); students.add(new Student(c)); }
     * 
     * return students; }
     */
}
