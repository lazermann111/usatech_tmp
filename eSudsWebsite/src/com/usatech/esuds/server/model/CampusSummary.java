/*
 * Created on Sep 14, 2004
 *
 */
package com.usatech.esuds.server.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.CampusSummaryBean;
import com.usatech.esuds.server.beans.MachineUsageBean;
import com.usatech.esuds.server.beans.RoomSummaryBean;
import com.usatech.esuds.server.beans.UsageTotalBean;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.persistence.VwTotalSalesByHost;
import com.usatech.esuds.server.persistence.VwTotalSalesByHostPeer;
import com.usatech.esuds.server.util.AccountUtil;

/**
 * @author melissa
 *
 */
public class CampusSummary
{
	private static Log log = LogFactory.getLog(CampusSummary.class);
	
	public static CampusSummaryBean retrieveSummaryForCampus(long campusId, Date startDate, Date endDate) throws UnknownObjectException, DataException
	{
		List totalSalesByHost = null;
		
		try
		{
			totalSalesByHost = VwTotalSalesByHostPeer.retrieveTotalsForCampus(campusId, startDate, endDate);
		}
		catch (TorqueException te)
		{
			log.error("Error getting Summary for Campus: " + te.getMessage());
			throw new DataAccessException();
		}
		
		CampusSummaryBean campusSummary = new CampusSummaryBean();
		
		List laundryRooms = new ArrayList();
		
		if (totalSalesByHost.isEmpty())
		{
			log.debug("No records found.");
			Campus campus = Campus.retrieveCampus(campusId);
			campusSummary.setCampusName(campus.getName());
			campusSummary.setSchoolName(campus.getSchoolName());
			campusSummary.setRooms(laundryRooms);
			return campusSummary;
		}
		else
		{
			log.debug("Found " + totalSalesByHost.size() + " records.");
			campusSummary.setCampusName(((VwTotalSalesByHost) totalSalesByHost.get(0)).getCampusName());
			campusSummary.setSchoolName(((VwTotalSalesByHost) totalSalesByHost.get(0)).getSchoolName());
		}
		
		boolean roomFound = false;
		boolean machineFound = false;
		
		for (Iterator i = totalSalesByHost.iterator(); i.hasNext();)
		{
			VwTotalSalesByHost sales = (VwTotalSalesByHost) i.next();
			RoomSummaryBean room = null;
			
			// find laundry room
			roomFound = false;
				
			for (Iterator j = laundryRooms.iterator(); (j.hasNext() && !roomFound);)
			{	
				room = (RoomSummaryBean) j.next();
					
				if (room.getDormName().equals(sales.getDormName()) && room.getRoomName().equals(sales.getLaundryRoomName()))
				{
					roomFound = true;
					log.debug("Found a match for room " + sales.getDormName() + " - " + sales.getLaundryRoomName());
					machineFound = false;
					//	find machine, add to machine total
					for (Iterator k = room.getMachines().iterator(); k.hasNext() && !machineFound;)
					{
						MachineUsageBean machine = (MachineUsageBean) k.next();
						if (machine.getLabel() != null && machine.getLabel().equals(sales.getHostLabelCd()) && machine.getMachineType().equals(sales.getHostTypeDesc()))
						{
							machineFound = true;
							log.debug("Found match for label " + sales.getHostLabelCd() + " and type " + sales.getHostTypeDesc());
							machine.setNumCycles(machine.getNumCycles() + sales.getTotCycles());
							machine.setNumTopOffs(machine.getNumTopOffs() + sales.getTotTopOffs());
							//Added 01-24-05 BSK to only consider certain authority service types
							if(AccountUtil.showAmountToOperator(sales.getAuthServiceType())) {
							    machine.setTotalAmount(machine.getTotalAmount().add(sales.getTotAmount()));
							
								if (sales.getClientPaymentTypeDesc().equalsIgnoreCase("cash"))
								{
									machine.setCashAmount(machine.getCashAmount().add(sales.getTotAmount()));
								}
								else
								{
									machine.setCardAmount(machine.getCardAmount().add(sales.getTotAmount()));
								}
							}
						}
					}
					
					if (!machineFound)
					{
						log.debug("No match found for label " + sales.getHostLabelCd() + " and type " + sales.getHostTypeDesc());
						MachineUsageBean machine = new MachineUsageBean();
						machine.setLabel(sales.getHostLabelCd());
						machine.setMachineType(sales.getHostTypeDesc());
						machine.setNumCycles(sales.getTotCycles());
						machine.setNumTopOffs(sales.getTotTopOffs());
                        // Added 02-15-05 BSK to only consider certain authority service types
						if(AccountUtil.showAmountToOperator(sales.getAuthServiceType())) {
							machine.setTotalAmount(sales.getTotAmount());
							
							if (sales.getClientPaymentTypeDesc().equalsIgnoreCase("cash"))
							{
								machine.setCashAmount(sales.getTotAmount());
								machine.setCardAmount(new BigDecimal(0.00));
							}
							else
							{
								machine.setCardAmount(sales.getTotAmount());
								machine.setCashAmount(new BigDecimal(0.00));
							}
						}
						log.debug("Created machine for label " + sales.getHostLabelCd() + " and type " + sales.getHostTypeDesc());
						room.getMachines().add(machine);
					}
				}
			}
			
			if (!roomFound)
			{
				
				log.debug("No match found for room " + sales.getDormName() + " - " + sales.getLaundryRoomName());
				
				room = new RoomSummaryBean();
				room.setDormName(sales.getDormName());
				room.setRoomName(sales.getLaundryRoomName());

				MachineUsageBean machine = new MachineUsageBean();
				machine.setLabel(sales.getHostLabelCd());
				machine.setMachineType(sales.getHostTypeDesc());
				machine.setNumCycles(sales.getTotCycles());
				machine.setNumTopOffs(sales.getTotTopOffs());
				machine.setTotalAmount(sales.getTotAmount());
				
				if (sales.getClientPaymentTypeDesc().equalsIgnoreCase("cash"))
				{
					machine.setCashAmount(sales.getTotAmount());
					machine.setCardAmount(new BigDecimal(0.00));
				}
				else
				{
					machine.setCardAmount(sales.getTotAmount());
					machine.setCashAmount(new BigDecimal(0.00));
				}
				
				List machines = new ArrayList();
				machines.add(machine);
				room.setMachines(machines);
				log.debug("Created room " + sales.getDormName() + " - " + sales.getLaundryRoomName());
				laundryRooms.add(room);
			}
			
			//	find machine type and payment type in totals, add to total
			String type = sales.getHostTypeDesc();
			String payment = sales.getClientPaymentTypeDesc();
			boolean typeFound = false;
			boolean paymentFound = false;
			
			List machineTypeTotals = room.getMachineTypeTotals();
			
			if (machineTypeTotals == null)
			{
				machineTypeTotals = new ArrayList();
				room.setMachineTypeTotals(machineTypeTotals);
			}
			
			for (Iterator l = machineTypeTotals.iterator(); l.hasNext() && !typeFound;)
			{
				UsageTotalBean total = (UsageTotalBean) l.next();
				if (total.getDescription().equals(type))
				{
					typeFound = true;
					total.setTotalCycles(total.getTotalCycles() + sales.getTotCycles());
					total.setTotalTopOffs(total.getTotalTopOffs() + sales.getTotTopOffs());
					total.setTotalAmount(total.getTotalAmount().add(sales.getTotAmount()));
				}
			}
			
			if (!typeFound)
			{
				UsageTotalBean total = new UsageTotalBean();
				total.setDescription(sales.getHostTypeDesc());
				total.setTotalCycles(sales.getTotCycles());
				total.setTotalTopOffs(sales.getTotTopOffs());
				total.setTotalAmount(sales.getTotAmount());
				
				machineTypeTotals.add(total);
			}
				
			List paymentTypeTotals = room.getPaymentTypeTotals();
			
			if (paymentTypeTotals == null)
			{
				paymentTypeTotals = new ArrayList();
				room.setPaymentTypeTotals(paymentTypeTotals);
			}
			
			for (Iterator m = paymentTypeTotals.iterator(); m.hasNext() && !paymentFound;)
			{
				UsageTotalBean total = (UsageTotalBean) m.next();
					
				if (total.getDescription().equals(payment))
				{
					paymentFound = true;
					total.setTotalCycles(total.getTotalCycles() + sales.getTotCycles());
					total.setTotalTopOffs(total.getTotalTopOffs() + sales.getTotTopOffs());
					total.setTotalAmount(total.getTotalAmount().add(sales.getTotAmount()));
				}
			}
			
			if (!paymentFound)
			{
				UsageTotalBean total = new UsageTotalBean();
				total.setDescription(sales.getClientPaymentTypeDesc());
				total.setTotalCycles(sales.getTotCycles());
				total.setTotalTopOffs(sales.getTotTopOffs());
				total.setTotalAmount(sales.getTotAmount());
				
				paymentTypeTotals.add(total);
			}
			
			UsageTotalBean grandTotal = room.getGrandTotal();
			
			if (grandTotal == null)
			{
				grandTotal = new UsageTotalBean();
				grandTotal.setDescription("Grand Total");
				grandTotal.setTotalCycles(sales.getTotCycles());
				grandTotal.setTotalTopOffs(sales.getTotTopOffs());
				grandTotal.setTotalAmount(sales.getTotAmount());
				room.setGrandTotal(grandTotal);
			}
			else
			{
				grandTotal.setTotalCycles(grandTotal.getTotalCycles() + sales.getTotCycles());
				grandTotal.setTotalTopOffs(grandTotal.getTotalTopOffs() + sales.getTotTopOffs());
				grandTotal.setTotalAmount(grandTotal.getTotalAmount().add(sales.getTotAmount()));
			}
		}
		
		// order machines by label
		// there is a bug in torque that prevents us from doing this in the query
		// see VwTotalSalesByHostPeer for comments on this
		for (Iterator it = laundryRooms.iterator(); it.hasNext();)
		{
			RoomSummaryBean room = (RoomSummaryBean) it.next();
			room.getMachines();
			
			MachineUsageBean[] ordered = new MachineUsageBean[1000];
			ArrayList noLabel = new ArrayList();
		
			for (Iterator i = room.getMachines().iterator(); i.hasNext();)
			{
						MachineUsageBean bean = (MachineUsageBean) i.next();
		
						if (!"000".equals(bean.getLabel()))
						{
							try
							{
								int label = Integer.parseInt(bean.getLabel());
								ordered[label] = bean;
							}
							catch (NumberFormatException e)
							{
								log.debug("Could not parse label" + bean.getLabel() + " to int.");
								noLabel.add(bean);
							}
						}
						else
						{
							log.debug("Label was 000, putting in no label list.");
							noLabel.add(bean);
						}
					}
		
					ArrayList result = new ArrayList();
					result.addAll(noLabel);
		
					for (int i = 0; i < ordered.length; i++)
					{
						if (ordered[i] != null)
						{
							result.add(ordered[i]);
						}		
					}
	
					room.setMachines(result);
			
		}

		 campusSummary.setRooms(laundryRooms);
		 return campusSummary;
	}
}
