/*
 * *******************************************************************
 * 
 * File Operator.java
 * 
 * Created on Mar 26, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.CustLoc;
import com.usatech.server.persistence.Customer;
import com.usatech.server.persistence.CustomerPeer;
import com.usatech.server.persistence.CustomerType;
import com.usatech.server.persistence.CustomerTypePeer;
import com.usatech.server.persistence.Location;

public class Operator
{
	private static Log log = LogFactory.getLog(Operator.class);
	
	private static final String CUSTOMER_TYPE_DESC = "eSuds Operator";
	public static final long UNKNOWN_OPERATOR_ID = 1;
	
	private Customer customer = null;
	
	public Operator() throws DataException
	{
		customer = new Customer();
		CustomerType type;
		
		try
		{
			type = CustomerTypePeer.retrieveByName(CUSTOMER_TYPE_DESC);
			customer.setCustomerType(type);
		}
		catch (TorqueException e)
		{
			log.error("Error setting type to " + CUSTOMER_TYPE_DESC + " " + e.getMessage());
			customer = null;
			throw new DataException("Could not create operator.");
		}
	}

	/**
	 * @param customer
	 */
	protected Operator(Customer c) 
	{
		customer = c;
	}
		
	public long getOperatorId()
	{
		return customer.getCustomerId();
	}
	
	public String getName()
	{
		return customer.getCustomerName();
	}
	
	public void setName(String name)
	{
		customer.setCustomerName(name);
	}
	
	public String getAddr1()
	{
		return customer.getCustomerAddr1();
	}
	
	public void setAddr1(String addr1)
	{
		customer.setCustomerAddr1(addr1);
	}
	
	public String getAddr2()
	{
		return customer.getCustomerAddr2();
	}
	
	public void setAddr2(String addr2)
	{
		customer.setCustomerAddr2(addr2);
	}
	
	public String getCity()
	{
		return customer.getCustomerCity();
	}
	
	public void setCity(String city)
	{
		customer.setCustomerCity(city);
	}
	
	public String getCounty()
	{
		return customer.getCustomerCounty();
	}
	
	public void setCounty(String county)
	{
		customer.setCustomerCounty(county);
	}
	
	public String getStateCd()
	{
		return customer.getCustomerStateCd();
	}
	
	public void setStateCd(String cd) throws DataException
	{
		try
		{
			customer.setCustomerStateCd(cd);
		}
		catch (TorqueException e)
		{
			log.error("Error setting state cd " + e.getMessage());
			throw new DataException("Could not set StateCd");
		}
	}
	
	public String getPostalCd()
	{
		return customer.getCustomerPostalCd();
	}
	
	public void setPostalCd(String cd)
	{
		customer.setCustomerPostalCd(cd);
	}
	
	public String getCountryCd()
	{
		return customer.getCustomerCountryCd();
	}
	
	public void setCountryCd(String cd) throws DataException
	{
		try
		{
			customer.setCustomerCountryCd(cd);
		}
		catch (TorqueException e)
		{
			log.error("Error setting country cd " + e.getMessage());
			throw new DataException("Could not set CountryCd");
		}
	}	
	
	public List getCampuses() throws DataException
	{
		//	we only want to return active ones unless we explicitly ask for all
		return getCampuses(true);
	}

	/**
	 * Gets a List of all Campuses associated with an Operator
	 * 
	 * @return a List of Campus
	 */
	public List getCampuses(boolean activeOnly) throws DataException
	{
		ArrayList campuses = new ArrayList();
		List list = null;
		try
		{
			if (activeOnly)
			{
				list = customer.getActiveCustLocs();
			}
			else
			{
				list = customer.getCustLocs();
			}
			
		}
		catch (TorqueException e)
		{
			log.error("Error getting customer locations " + e.getMessage());
			throw new DataAccessException();
		}
	
		for (Iterator i = list.iterator(); i.hasNext();)
		{
			Location l = null;
			try
			{
				l = ((CustLoc) i.next()).getLocation();
				campuses.add(new Campus(l));
			}
			catch (TorqueException te)
			{
				log.error("Error getting location " + te.getMessage());
			}
			
		}
		
		return campuses;
	}
	
	public List getSchools() throws DataException
	{
		Set set = new TreeSet();
		List campuses = getCampuses();
		for (Iterator i = campuses.iterator(); i.hasNext();)
		{
			Campus c = (Campus) i.next();
			set.add(c.getSchool());
		}
		
		ArrayList list = new ArrayList();
		list.addAll(set);
		return list;
	}
	
	public boolean isActive()
	{
		if (customer.isActive())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setActive(boolean active)
	{
		if (active)
		{
			customer.setCustomerActiveYnFlag("Y");
		}
		else
		{
			customer.setCustomerActiveYnFlag("N");
		}
	}
	
	public void validate() throws DataException
	{
		if (!customer.isNew() && customer.getCustomerId() < 1)
		{
			throw new DataException("Id is not valid.");
		}
	
		if (customer.getCustomerName() == null || customer.getCustomerName().equals(""))
		{
			throw new DataException("Name is not valid.");
		}
	}
	
	public void save() throws DataException
	{
		validate();
		
		try
		{
			customer.save();
		}
		catch (Exception e)
		{
			log.error("Error saving customer " + e.getMessage());
			throw new DataException("Could not save operator.");
		}
	}
	
	public static Operator retrieveOperator(long operatorId) throws UnknownObjectException, DataException
	{
		Customer operator = null;
		
		try
		{
			operator = CustomerPeer.retrieveByPK(operatorId);
			
			// check type
			if (operator.getCustomerType().getCustomerTypeDesc().equals(CUSTOMER_TYPE_DESC))
			{
				return new Operator(operator);
			}
			else
			{
				throw new UnknownObjectException("Operator not found.");
			}
		}
		catch (NoRowsException e)
		{
			log.error("Operator with id " + operatorId + " not found.");
			throw new UnknownObjectException("Operator not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving operator with id " + operatorId + " " + tmre.getMessage());
			throw new DataException("More than one operator found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving operator with id " + operatorId + " " + te.getMessage());
			throw new DataAccessException();
		}
	}
	
	public static List retrieveAll() throws DataException
	{
		List customers = null;
		
		try
		{
			customers =CustomerPeer.retrieveByTypeDesc(CUSTOMER_TYPE_DESC);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving all operators " + te.getMessage());
			throw new DataAccessException();
		}
		
		List operators = new ArrayList();
		
		for (Iterator i = customers.iterator(); i.hasNext();)
		{
			Customer c = (Customer) i.next();
			operators.add(new Operator(c));
		}
		
		return operators;
	}

	/**
	 * @param operatorName
	 * @return
	 */
	public static Operator retrieveByName(String operatorName) throws DataException, UnknownObjectException
	{
		List operators = null;
		
		try
		{
			operators = CustomerPeer.retrieveByNameAndTypeDesc(operatorName, CUSTOMER_TYPE_DESC);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving operator " + operatorName + " " + te.getMessage());
			throw new DataAccessException();
		}
		
		if (operators.size() > 1)
		{
			throw new DataException("More than one operator found.");
		}
		else if (operators.size() == 0)
		{
			throw new UnknownObjectException("Operator not found.");
		}
		else
		{
			Customer c = (Customer) operators.get(0);
			return new Operator(c);
		}
		
	}
}
