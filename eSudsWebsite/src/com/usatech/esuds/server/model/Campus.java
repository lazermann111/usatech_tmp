/*
 * *******************************************************************
 * 
 * File Campus.java
 * 
 * Created on Mar 25, 2004
 * 
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates:
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;
import org.apache.torque.util.Criteria;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.server.persistence.AppObjectTypePeer;
import com.usatech.server.persistence.AppUserObjectPermissionPeer;
import com.usatech.server.persistence.CustLoc;
import com.usatech.server.persistence.Location;
import com.usatech.server.persistence.LocationPeer;
import com.usatech.server.persistence.LocationType;
import com.usatech.server.persistence.LocationTypePeer;

public class Campus {
    private static Log log = LogFactory.getLog(Campus.class);

    public static final String LOCATION_TYPE_DESC = "Education - Secondary Campus";

    private static final String CUSTOMER_TYPE_DESC = "eSuds Operator";

    private Location location = null;

    private CustLoc custLocation = null;

    public Campus() throws DataException {
        location = new Location();
        location.setNew(true);

        LocationType type;
        try {
            type = LocationTypePeer.getLocationTypeByDesc(LOCATION_TYPE_DESC);
            location.setLocationType(type);
        } catch (TorqueException e) {
            log.error("Error setting type to " + LOCATION_TYPE_DESC + ": "
                    + e.getMessage());
            location = null;
            throw new DataException("Could not create campus.");
        }

        custLocation = new CustLoc();
        custLocation.setNew(true);

    }

    protected Campus(Location l) throws DataException {
        String typeDesc = null;

        try {
            typeDesc = l.getLocationType().getLocationTypeDesc();
        } catch (TorqueException e) {
            log.error("Could not get location type for location: "
                    + e.getMessage());
            throw new DataException("Could not create campus.");
        }

        if (typeDesc.equals(LOCATION_TYPE_DESC)) {
            location = l;
        } else {
            throw new DataException("Location is wrong type.");
        }

        retrieveCustLoc();
    }

    private void retrieveCustLoc() throws DataException {
        if(custLocation == null) {
            List list = null;
            try {
                list = location.getCustLocsForCustomerType(CUSTOMER_TYPE_DESC);
            } catch (TorqueException e) {
                log
                        .error("Error getting customer locations: "
                                + e.getMessage());
                throw new DataAccessException();
            }

            if (list.size() > 1) {
                log.error("More than one customer found for location with id: "
                        + location.getLocationId() + ".");
                location = null;
                throw new DataException("More than one operator found.");
            } else if (list.size() == 0) {
                log.error("No customer found for location with id: "
                        + location.getLocationId() + ".");
                custLocation = new CustLoc();
                custLocation.setNew(true);
                //location = null;
                //throw new DataException("No operator found.");
            } else {
                custLocation = (CustLoc) list.get(0);
            }
        }
    }

    public long getCampusId() {
        return location.getLocationId();
    }

    public long getSchoolId() {
        return location.getParentLocationId();
    }

    public void setSchoolId(long id) throws DataException {
        try {
            location.setParentLocationId(id);
        } catch (TorqueException e) {
            log.error("Error setting parent location id: " + e.getMessage());
            throw new DataException("Could not set schoolId.");
        }
    }

    public School getSchool() {
        try {
            return new School(location.getLocationRelatedByParentLocationId());
        } catch (TorqueException e) {
            log.warn("Error getting Parent Location: " + e.getMessage());
            return null;
        } catch (DataException de) {
            log.error("Error creating school: " + de.getMessage());
            return null;
        }
    }

    public String getSchoolName() {
        if (getSchool() != null) {
            return getSchool().getName();
        } else {
            return "";
        }
    }

    public void setSchool(School s) throws DataException {
        long id = 0;
        if (s != null) {
            id = s.getSchoolId();
        }

        setSchoolId(id);
    }

    public long getOperatorId() throws DataException {
        if (custLocation == null) {
            retrieveCustLoc();
        }

        if (custLocation != null) {
            return custLocation.getCustomerId();
        } else {
            throw new DataException("No operator found.");
        }
    }

    public void setOperatorId(long id) throws DataException {
        try {
            custLocation.setCustomerId(id);
        } catch (TorqueException te) {
            log.error("Error setting customer: " + te.getMessage());
            throw new DataException("Could not set operatorId.");
        }
    }

    public Operator getOperator() throws DataException {
        if (custLocation == null) {
            retrieveCustLoc();
        }

        try {
            return new Operator(custLocation.getCustomer());
        } catch (TorqueException e) {
            log.error("Error getting customer: " + e.getMessage());
            throw new DataAccessException();
        }
    }

    public void setOperator(Operator o) throws DataException {
        if (o != null) {
            try {
                custLocation.setCustomerId(o.getOperatorId());
            } catch (TorqueException e) {
                log.error("Error setting Customer: " + e.getMessage());
                throw new DataException("Could not set operator.");
            }
        } else {
            throw new DataException("Operator can not be null.");
        }
    }

    public String getName() {
        return location.getLocationName();
    }

    public void setName(String name) {
        location.setLocationName(name);
    }

    public String getAddr1() {
        return location.getLocationAddr1();
    }

    public void setAddr1(String addr1) {
        location.setLocationAddr1(addr1);
    }

    public String getAddr2() {
        return location.getLocationAddr2();
    }

    public void setAddr2(String addr2) {
        location.setLocationAddr2(addr2);
    }

    public String getCity() {
        return location.getLocationCity();
    }

    public void setCity(String city) {
        location.setLocationCity(city);
    }

    public String getCounty() {
        return location.getLocationCounty();
    }

    public void setCounty(String county) {
        location.setLocationCounty(county);
    }

    public String getStateCd() {
        return location.getLocationStateCd();
    }

    public void setStateCd(String cd) throws DataException {
        try {
            location.setLocationStateCd(cd);
        } catch (TorqueException e) {
            log.error("Error setting state cd: " + e.getMessage());
            throw new DataException("Could not set StateCd");
        }
    }

    public String getCountryCd() {
        return location.getLocationCountryCd();
    }

    public void setCountryCd(String cd) throws DataException {
        try {
            location.setLocationCountryCd(cd);
        } catch (TorqueException e) {
            log.error("Error setting country cd: " + e.getMessage());
            throw new DataException("Could not set CountryCd");
        }
    }

    public String getPostalCd() {
        return location.getLocationPostalCd();
    }

    public void setPostalCd(String cd) {
        location.setLocationPostalCd(cd);
    }

    public String getTimeZoneCd() {
        return location.getLocationTimeZoneCd();
    }

    public void setTimeZoneCd(String cd) throws DataException {
        try {
            location.setLocationTimeZoneCd(cd);
        } catch (TorqueException e) {
            log.error("Error setting time zone cd: " + e.getMessage());
            throw new DataException("Could not set TimeZoneCd");
        }
    }

    /**
     * Gets a List of all Dorms associated with this Campus
     * 
     * @return a List of Dorms
     */
    public List getDorms() throws DataException {
        // we only want to return active ones unless we explicitly ask for all
        return getDorms(true);
    }

    public List getDorms(boolean activeOnly) throws DataException {
        List list = null;
        ArrayList dorms = new ArrayList();

        try {
            if (activeOnly) {
                list = location.getActiveDependentLocations(true, new int[]{17});
            } else {
                list = location.getDependentLocations(true, new int[]{17, 28});
            }
        } catch (NoRowsException nre) {
            return dorms;
        } catch (TorqueException e) {
            log.error("Error getting dorms: " + e.getMessage());
            throw new DataAccessException();
        }

        for (Iterator i = list.iterator(); i.hasNext();) {
            Location l = (Location) i.next();
            dorms.add(new Dorm(l));
        }

        return dorms;
    }

    public boolean isActive() {
        if (location.isActive()) {
            return true;
        } else {
            return false;
        }
    }

    public void setActive(boolean active) {
        if (active) {
            location.setLocationActiveYnFlag("Y");
        } else {
            location.setLocationActiveYnFlag("N");
        }
    }

    public void validate() throws DataException {
        if (!location.isNew() && location.getLocationId() < 1) {
            throw new DataException("Id is not valid.");
        }

        if (custLocation.getCustomerId() < 1) {
            throw new DataException("OperatorId is not valid.");
        }

        if (location.getLocationName() == null
                || location.getLocationName().equals("")) {
            throw new DataException("Name is not valid.");
        }
    }

    public void save() throws DataException {
        validate();

        try {
            location.save();
        } catch (Exception e) {
            log.error("Error saving location: " , e);
            throw new DataException("Could not save campus.");
        }

        try {
            // set the locationId in the custLocation - needed if new
            custLocation.setLocationId(location.getLocationId());

            custLocation.save();
        } catch (Exception e) {
            // So campus saved correctly but customer location did not
            log.error("Error saving customer location: ", e);
            throw new DataException("Could not save campus.");
        }
    }

    public static Campus retrieveCampus(long campusId)
            throws UnknownObjectException, DataException {
        return retrieveCampus(campusId, false);
    }

    public static Campus retrieveCampus(long campusId, boolean allowInactive)
            throws UnknownObjectException, DataException {
        Location location = null;

        try {
            location = LocationPeer.retrieveByPK(campusId);

            // check type
            if (location.getLocationType().getLocationTypeDesc().equals(
                    LOCATION_TYPE_DESC)) {
                if (allowInactive || location.isActive()) {
                    return new Campus(location);
                } else {
                    throw new UnknownObjectException("Campus not found.");
                }
            } else {
                throw new UnknownObjectException("Campus not found.");
            }
        } catch (NoRowsException e) {
            log.error("Campus with id " + campusId + " not found.");
            throw new UnknownObjectException("Campus not found.");
        } catch (TooManyRowsException tmre) {
            log.error("Error retrieving campus with id " + campusId + ": "
                    + tmre.getMessage());
            throw new DataException("More than one campus found.");
        } catch (TorqueException te) {
            log.error("Error retrieving campus with id " + campusId + ": "
                    + te.getMessage());
            throw new DataAccessException();
        }
    }

    public static List retrieveAll() throws DataException {
        List locations = null;

        try {
            locations = LocationPeer.retrieveByLocationTypeDesc(
                    LOCATION_TYPE_DESC, true);
        } catch (TorqueException te) {
            log.error("Error retrieving all campuses: " + te.getMessage());
            throw new DataAccessException();
        }

        return convertLocationsToCampuses(locations);
    }

    public static List retrieveByUserPrivilege(long userId, int appId, String objectType, String action) throws DataException {
        List locations = null;
        try {
            Criteria criteria = new Criteria();
            criteria.addJoin(LocationPeer.LOCATION_TYPE_ID, LocationTypePeer.LOCATION_TYPE_ID);
    		criteria.add(LocationTypePeer.LOCATION_TYPE_DESC, LOCATION_TYPE_DESC);
    		criteria.addAscendingOrderByColumn(LocationPeer.LOCATION_NAME);
            criteria.add(LocationPeer.LOCATION_ACTIVE_YN_FLAG, "Y");
            criteria.addJoin(LocationPeer.LOCATION_ID, "VW_LOCATION_HIERARCHY.DESCENDENT_LOCATION_ID");
    		criteria.addJoin("VW_LOCATION_HIERARCHY.ANCESTOR_LOCATION_ID", AppUserObjectPermissionPeer.OBJECT_ID);
    		criteria.add(AppUserObjectPermissionPeer.APP_ID, appId);
    		criteria.addJoin(AppUserObjectPermissionPeer.APP_OBJECT_TYPE_ID, AppObjectTypePeer.APP_OBJECT_TYPE_ID);
    		criteria.add(AppObjectTypePeer.APP_OBJECT_CD, objectType);
    		criteria.add(AppUserObjectPermissionPeer.APP_USER_ID, userId);
    		if(action.indexOf(SecurityService.READ) >= 0) {
    		    criteria.add(AppUserObjectPermissionPeer.ALLOW_OBJECT_READ_YN_FLAG, "Y");        		
    		}
    		if(action.indexOf(SecurityService.CREATE) >= 0) {
    		    criteria.add(AppUserObjectPermissionPeer.ALLOW_OBJECT_CREATE_YN_FLAG, "Y");        		
    		}
    		if(action.indexOf(SecurityService.MODIFY) >= 0) {
    		    criteria.add(AppUserObjectPermissionPeer.ALLOW_OBJECT_MODIFY_YN_FLAG, "Y");        		
    		}
    		if(action.indexOf(SecurityService.DELETE) >= 0) {
    		    criteria.add(AppUserObjectPermissionPeer.ALLOW_OBJECT_DELETE_YN_FLAG, "Y");        		
    		}
    		locations = LocationPeer.doSelect(criteria);
        } catch (TorqueException te) {
            log.error("Error retrieving campuses for user: " + te.getMessage());
            throw new DataAccessException();
        }
        return convertLocationsToCampuses(locations);
    }
    
    public static List convertLocationsToCampuses(List locations) throws DataException {
        List campuses = new ArrayList();
        for (Iterator i = locations.iterator(); i.hasNext();) {
            Location l = (Location) i.next();
            campuses.add(new Campus(l));
        }
        return campuses;
    }

    public static List retrieveBySchoolId(long schoolId) throws DataException {
        List locations = null;

        try {
            // active locations only
            locations = LocationPeer
                    .retrieveByLocationTypeDescAndParentLocationId(
                            LOCATION_TYPE_DESC, schoolId, true, false);
        } catch (TorqueException te) {
            log.error("Error retrieving campuses for school " + schoolId + ": "
                    + te.getMessage());
            throw new DataAccessException();
        }

        return convertLocationsToCampuses(locations);
    }

    public static List retrieveByOperatorId(long operatorId)
            throws DataException {
        List locations = null;

        try {
            locations = LocationPeer.retrieveByLocationTypeAndCustomerId(
                    LOCATION_TYPE_DESC, operatorId);
        } catch (TorqueException te) {
            log.error("Error retrieving campuses for operator " + operatorId
                    + ": " + te.getMessage(), te);
            throw new DataAccessException();
        }

        return convertLocationsToCampuses(locations);
    }

    public static List retrieveBySchoolIdAndOperatorId(long schoolId,
            long operatorId) throws DataException {
        List locations = null;

        try {
            locations = LocationPeer.retrieveByTypeAndParentAndCustomer(
                    LOCATION_TYPE_DESC, schoolId, operatorId);
        } catch (TorqueException te) {
            log.error("Error retrieving all campuses: " + te.getMessage());
            throw new DataAccessException();
        }

        return convertLocationsToCampuses(locations);
    }
}