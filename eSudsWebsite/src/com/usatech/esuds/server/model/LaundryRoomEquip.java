package com.usatech.esuds.server.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.server.persistence.Host;
import com.usatech.server.persistence.HostPeer;
import com.usatech.server.persistence.HostStatusNotifQueue;

import simple.app.DialectResolver;

public class LaundryRoomEquip {
    public final static int INVALID = -1;

    public final static int NO_STATUS_AVAILABLE = 0;

    public final static int IDLE_AVAILABLE = 1;

    public final static int IN_1ST_CYCLE = 2;

    public final static int OUT_OF_SERVICE = 3;

    public final static int NOTHING_ON_PORT = 4;

    public final static int IDLE_NOT_AVAILABLE = 5;

    public final static int IN_MANUAL_SERVICE_MODE = 6;

    public final static int IN_2ND_CYCLE = 7;

    public final static int TRANSACTION_IN_PROGRESS = 8;

    public final static int BOTTOM_POSITION = 0;

    public final static int TOP_POSITION = 1;

    private static Log log = LogFactory.getLog(LaundryRoomEquip.class);

    private Host host = null;

    public LaundryRoomEquip() {
        host = new Host();
    }

    protected LaundryRoomEquip(Host h) {
        host = h;
    }

    public long getLaundryRoomEquipId() {
        return host.getHostId();
    }

    public long getRoomControllerId() {
        return host.getDeviceId();
    }

    public void setRoomControllerId(long id) throws DataException {
        try {
            host.setDeviceId(id);
        } catch (TorqueException e) {
            log.error("Error setting device " + e.getMessage());
            throw new DataException("Could not set roomControllerId.");
        }
    }

    public RoomController getRoomController() throws DataException {
        try {
            return new RoomController(host.getDevice());
        } catch (TorqueException e) {
            log.error("Error getting device " + e.getMessage());
            throw new DataAccessException();
        }
    }

    public long getLaundryEquipId() {
        return host.getHostTypeId();
    }

    public void setLaundryEquipId(long id) throws DataException {
        try {
            host.setHostTypeId(id);
        } catch (TorqueException e) {
            log.error("Error setting hostType " + e.getMessage());
            throw new DataException("Could not set laundryEquipId.");
        }
    }

    public int getStatusIndicator() {
        String status = host.getHostStatusCd();
        return Integer.parseInt(status);
    }

    public void setStatusIndicator(int status) {
        host.setHostStatusCd("" + status);
    }

    public Date getStartTS() {
        return host.getHostLastStartTs();
    }

    public void setStartTS(Date date) {
        host.setHostLastStartTs(date);
    }

    public int getCycleMins() {
        return host.getHostEstCompleteMinut();
    }

    public void setCycleMins(int mins) {
        host.setHostEstCompleteMinut(mins);
    }

    public String getSerialCd() {
        return host.getHostSerialCd();
    }

    public void setSerialCd(String cd) {
        host.setHostSerialCd(cd);
    }

    public int getPortNum() {
        return host.getHostPortNum();
    }

    public void setPortNum(int port) {
        host.setHostPortNum(port);
    }

    public String getPosition() {
        if (host.getHostPositionNum() == BOTTOM_POSITION) {
            return "bottom";
        } else if (host.getHostPositionNum() == TOP_POSITION) {
            return "top";
        } else {
            return "unknown";
        }
    }

    public void setPosition(String position) throws DataException {
        if (position.equalsIgnoreCase("bottom")) {
            host.setHostPortNum(BOTTOM_POSITION);
        } else if (position.equalsIgnoreCase("top")) {
            host.setHostPositionNum(TOP_POSITION);
        } else {
            throw new DataException("Unkown position.");
        }
    }

    public String getLabel() {
        return host.getHostLabelCd();
    }

    public void setLabel(String label) {
        host.setHostLabelCd(label);
    }

    public List getTranLineItems() throws DataException {
        try {
            return host.getTranLineItems();
        } catch (TorqueException e) {
            log.error("Error getting TranLineItems " + e.getMessage());
            throw new DataAccessException();
        }
    }

    public LaundryEquip getLaundryEquip() {
        try {
            return new LaundryEquip(host.getHostType());
        } catch (TorqueException te) {
            log.error("Error getting host type " + te.getMessage());
            return null;
        }
    }

    public boolean isActive() {
        if (host.getHostActiveYnFlag().equalsIgnoreCase("Y")) {
            return true;
        } else {
            return false;
        }
    }

    public void setActive(boolean active) {
        if (active) {
            host.setHostActiveYnFlag("Y");
        } else {
            host.setHostActiveYnFlag("N");
        }
    }

    public void validate() throws DataException {
        if (!host.isNew() && host.getHostId() < 1) {
            throw new DataException("Id is not valid.");
        }

        if (host.getDeviceId() < 1) {
            throw new DataException("RoomController is not valid.");
        }

        if (host.getHostTypeId() < 1) {
            throw new DataException("LaundryEquip is not valid.");
        }

        if (host.getHostStatusCd() == null || host.getHostStatusCd().equals("")) {
            throw new DataException("StatusFlag is not valid.");
        }

        if (host.getHostEstCompleteMinut() < 0) {
            throw new DataException("CycleMins is not valid.");
        }

        if (host.getHostPortNum() < 0) {
            throw new DataException("PortNum is not valid.");
        }

        if (host.getHostPositionNum() != 0 && host.getHostPositionNum() != 1) {
            throw new DataException("PostionNum is not valid.");
        }
    }

    public void save() throws DataException {
        validate();

        try {
            host.save();
        } catch (Exception e) {
            log.error("Error saving host " + e.getMessage());
            throw new DataException("Could not save laundry room equip.");
        }
    }

    public static void notifyUserOnCycleComplete(String emailAddr, long laundryRoomEquipId)
            throws DataException {
        long typeId = 2;

        HostStatusNotifQueue msg = new HostStatusNotifQueue();

        try {
            msg.setHostStatusNotifTypeId(2);
        } catch (TorqueException te) {
            log.error("Error setting typeId " + te.getMessage());
            throw new DataException("Could not create notification.");
        }
        try {
            msg.setHostId(laundryRoomEquipId);
        } catch (TorqueException te) {
            log.error("Error setting hostId " + te.getMessage());
            throw new DataException("Could not set laundry room equip id.");
        }

        msg.setHostStatusNotifQEmailAddr(emailAddr);

        try {
            msg.save();
        } catch (Exception e) {
            log.error("Error saving host status notif queue " + e.getMessage(), e);
            throw new DataException("Could not save notification.");
        }
    }

    /**
     * This is the new more restrictive was to do it, so as to avoid over
     * loading of the system. NOTE: Unfortunately, I had to pass in ActionErrors
     * and ActionMessages so that I could record the individual errors of each notification.
     * 
     */
    public static void notifyUserOnCycleComplete(String emailAddr, long[] laundryRoomEquipIds, long roomId, ActionMessages messages, ActionErrors errors)
            throws DataException {
        Connection conn = null;
        PreparedStatement ps = null;
        int cnt = 0;
        try {
            conn = Torque.getConnection();
            ps = conn.prepareCall("{call PKG_HOST_NOTIF.REGISTER_CYCLE_COMPLETE_NOTIF(?,?,?)}");
            ps.setString(1, emailAddr);
            ps.setLong(3, roomId);
            for(int i = 0; i < laundryRoomEquipIds.length; i++) {
                ps.setLong(2, laundryRoomEquipIds[i]);
                try {
                    ps.executeUpdate();
                    conn.commit();
                    cnt++;
                    Host host = HostPeer.retrieveByPK(laundryRoomEquipIds[i]);
                    messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("notification.success", new Long(laundryRoomEquipIds[i]), host.getHostLabelCd(), host.getHostType().getHostTypeDesc(), emailAddr));
                } catch (SQLException e) {
                    Host host;
                    if (!DialectResolver.isOracle()) 
                    	conn.rollback();
                    switch(e.getErrorCode()) {
                        case 20801: //    INVALID_LOCATION_FOR_HOST 
                            log.info("Location ID " + roomId + " is invalid for host ID " + laundryRoomEquipIds[i]);
                            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.invalid-location-for-host", new Long(roomId), new Long(laundryRoomEquipIds[i])));
                            break;
                        case 20802: //    INVALID_HOST_STATUS_FOR_NOTIF
                            log.info("Host ID " + laundryRoomEquipIds[i] + " is not running");
                            host = HostPeer.retrieveByPK(laundryRoomEquipIds[i]);
                            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.host-not-running", new Long(laundryRoomEquipIds[i]), host.getHostLabelCd(), host.getHostType().getHostTypeDesc()));
                            break;
                        case 20803: //    NOTIFICATION_ALREADY_EXISTS
                            log.info("Notification to '" + emailAddr + "' for host ID " + laundryRoomEquipIds[i] + " already exists");
                            host = HostPeer.retrieveByPK(laundryRoomEquipIds[i]);
                            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.already-notified", new Long(laundryRoomEquipIds[i]), host.getHostLabelCd(), host.getHostType().getHostTypeDesc(), emailAddr));
                            break;
                        case 20804: //    EXCEEDS_MAX_PER_HOST
                            log.info("Host ID " + laundryRoomEquipIds[i] + " is already being monitored by the maximum number of people");
                            host = HostPeer.retrieveByPK(laundryRoomEquipIds[i]);
                            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.max-per-host", new Long(laundryRoomEquipIds[i]), host.getHostLabelCd(), host.getHostType().getHostTypeDesc()));
                            break;
                        case 20805: //    EXCEEDS_MAX_PER_EMAIL EXCEPTION;
                            log.info("Email '" + emailAddr + "' is already monitoring the maximum number of machines");
                            host = HostPeer.retrieveByPK(laundryRoomEquipIds[i]);
                            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.max-per-email", new Long(laundryRoomEquipIds[i]), host.getHostLabelCd(), host.getHostType().getHostTypeDesc(), emailAddr));
                            break;
                        default:
                            throw e;
                    }
                } 
            }
        } catch (SQLException e) {
            log.error("Error saving host status notif queue " + e.getMessage(), e);
            throw new DataException("Could not save notification.");
        } catch (TorqueException e) {
            log.error("Error saving host status notif queue " + e.getMessage(), e);
            throw new DataException("Could not save notification.");
        } finally {
            if(ps != null) try { ps.close(); } catch(SQLException e) {}
            if(conn != null)try { conn.close(); } catch(SQLException e) {}
        }
    }

}
