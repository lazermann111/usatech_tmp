/*
 * Created on Feb 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.model.exceptions;

/** Represents an error that occurred while processing a list of things.
 * 
 * @author bkrug
 *
 * 
 */
public class ProcessingException extends Exception {
	private int position;
	private int alreadyProcessed;
	private Exception rootCause;
	
	/**
	 * 
	 */
	public ProcessingException(String msg, int position, int alreadyProcessed, Exception rootCause) {
		super(msg);
		this.position = position;
		this.alreadyProcessed = alreadyProcessed;
		this.rootCause = rootCause;
	}

	public int getAlreadyProcessed() {
		return alreadyProcessed;
	}
	public int getPosition() {
		return position;
	}
	public Exception getRootCause() {
		return rootCause;
	}
}
