/*
 * Created on Feb 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.model.exceptions;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DataException extends Exception
{

	/**
	 * 
	 */
	public DataException()
	{
		super();
	}

	/**
	 * @param string
	 */
	public DataException(String msg)
	{
		super(msg);
	}
}
