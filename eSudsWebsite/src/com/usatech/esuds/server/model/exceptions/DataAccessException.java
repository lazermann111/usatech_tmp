/*
 * Created on Feb 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.esuds.server.model.exceptions;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DataAccessException extends DataException
{
	
	private static String default_msg = "Error access data.";
	/**
	 * 
	 */
	public DataAccessException()
	{
		super(default_msg);
	}
	/**
	 * @param msg
	 */
	public DataAccessException(String msg)
	{
		super(msg);
	}
}
