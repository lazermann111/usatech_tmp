/*
 * *******************************************************************
 * 
 * File School.java
 * 
 * Created on Mar 26, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.security.services.SecurityService;
import com.usatech.server.persistence.App;
import com.usatech.server.persistence.AppPeer;
import com.usatech.server.persistence.Location;
import com.usatech.server.persistence.LocationPeer;
import com.usatech.server.persistence.LocationType;
import com.usatech.server.persistence.LocationTypePeer;


public class School
{
	private static Log log = LogFactory.getLog(School.class);
	
	public static final String LOCATION_TYPE_DESC = "Education - Secondary";
	
	private Location location = null;
	
	public School()
	{
		location = new Location();
		
		try
		{
			LocationType type = LocationTypePeer.getLocationTypeByDesc(LOCATION_TYPE_DESC);
			location.setLocationType(type);
		}
		catch (TorqueException te)
		{
			// should not happen, but do something here
		}
	}
	
	protected School(Location l) throws DataException
	{
		String typeDesc = null;
		
		try
		{
			typeDesc = l.getLocationType().getLocationTypeDesc();
		}
		catch (TorqueException e)
		{
			log.error("Could not get location type for location. " + e.getMessage());
			throw new DataException("Could not create school.");
		}
		
		if (typeDesc.equals(LOCATION_TYPE_DESC))
		{
			location = l;
		}
		else
		{
			throw new DataException("Location is wrong type.");
		}
	}
	
	public long getSchoolId()
	{
		return location.getLocationId();
	}
	
	public String getName()
	{
		return location.getLocationName();
	}
	
	public void setName(String name)
	{
		location.setLocationName(name);
	}
	
	public List getCampuses() throws DataException
	{
		List list = null;
		ArrayList campuses = new ArrayList();
	
		try
		{
			list = location.getDependentLocations(true);
		}
		catch (NoRowsException nre)
		{
			return campuses;
		}
		catch (TorqueException e)
		{
			log.error("Error getting campuses " + e.getMessage());
			throw new DataAccessException();
		}
	
		for (Iterator i = list.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			campuses.add(new Campus(l));
		}
		
		return campuses;
	}
	
	public List getCampusesForOperator(long operatorId) throws DataException
	{
		List list = null;
		ArrayList campuses = new ArrayList();
	
		try
		{
			list = location.getActiveDependentLocationsForCustomer(operatorId);
		}
		catch (NoRowsException nre)
		{
			return campuses;
		}
		catch (TorqueException e)
		{
			log.error("Error getting campuses " + e.getMessage());
			throw new DataAccessException();
		}
	
		for (Iterator i = list.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			campuses.add(new Campus(l));
		}
		
		return campuses;
	}
	
	public boolean isActive()
	{
		if (location.isActive())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setActive(boolean active)
	{
		if (active)
		{
			location.setLocationActiveYnFlag("Y");
		}
		else
		{
			location.setLocationActiveYnFlag("N");
		}
	}
	
	public void validate() throws DataException
	{
		if (!location.isNew() && location.getLocationId() < 1)
		{
			throw new DataException("Id is not valid.");
		}
		
		if (location.getLocationName() == null || location.getLocationName().equals(""))
		{
			throw new DataException("Name is not valid.");
		}
	}
	
	public void save() throws DataException
	{
		
		validate();
		
		try
		{
			location.save();
		}
		catch (Exception e)
		{
			log.error("Error saving location " + e.getMessage());
			throw new DataException("Could not save school.");
		}
	}
	
	public static School retrieveSchool(long schoolId) throws UnknownObjectException, DataException
	{
		Location location = null;
		
		try
		{
			location = LocationPeer.retrieveByPK(schoolId);
			
			// check type
			if (location.getLocationType().getLocationTypeDesc().equals(LOCATION_TYPE_DESC))
			{
				return new School(location);
			}
			else
			{
				throw new UnknownObjectException("School not found.");
			}
		}
		catch (NoRowsException e)
		{
			log.error("School with id " + schoolId + " not found.");
			throw new UnknownObjectException("School not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving school with id " + schoolId + " " + tmre.getMessage());
			throw new DataException("More than one school found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving school with id " + schoolId + " " + te.getMessage());
			throw new DataAccessException();
		}
	}
	
	public static List retrieveAll() throws DataException
	{
		List locations = null;
		
		try
		{
			locations = LocationPeer.retrieveByLocationTypeDesc(LOCATION_TYPE_DESC, true);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving all schools " + te.getMessage());
			throw new DataAccessException();
		}
		
		List schools = new ArrayList();
		
		for (Iterator i = locations.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			schools.add(new School(l));
		}
		
		return schools;
	}
    
    public static List retrieveByOperatorUser(long userId) throws DataException
    {
        List locations = null;
        
        try
        {
            App app = AppPeer.retrieveByName("Operator Access");
            locations = LocationPeer.retrieveByLocationTypeAndUserPrivilege(LOCATION_TYPE_DESC, userId, app.getAppId(), SecurityService.LOCATION, SecurityService.READ);
        }
        catch (TorqueException te)
        {
            log.error("Error retrieving all schools " + te.getMessage());
            throw new DataAccessException();
        }
        
        List schools = new ArrayList();
        
        for (Iterator i = locations.iterator(); i.hasNext();)
        {
            Location l = (Location) i.next();
            schools.add(new School(l));
        }
        
        return schools;
    }
}
