package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.Pos;
import com.usatech.server.persistence.PosPeer;

public class POS
{
	private static Log log = LogFactory.getLog(POS.class);
	private Pos pos = null;
	
	public final static long UNKNOWN_LOCATION_ID = 1;
	public final static long UNKNOWN_OPERATOR_ID = 1;		 
	
	public POS()
	{
		pos = new Pos();
		pos.setNew(true);
		
	}
	
	protected POS(Pos p)
	{
		pos = p;
	}
	
	public long getPosId()
	{
		return pos.getPosId();
	}
	
	public long getOperatorId()
	{
		return pos.getCustomerId();
	}
	
	public Operator getOperator() throws DataException
	{
		try
		{
			return new Operator(pos.getCustomer());
		}
		catch (TorqueException e)
		{
			log.error("Error getting customer " + e.getMessage());
			throw new DataAccessException();
		}
	}
	
	public long getLaundryRoomId()
	{
		return pos.getLocationId();
	}
	
	public void setLaundryRoomId(long laundryRoomId) throws DataException
	{
		try
		{
			pos.setLocationId(laundryRoomId);
		}
		catch (TorqueException e)
		{
			log.error("Error setting location id " + e.getMessage());
			throw new DataException("Could not set laundry room id.");
		}
	}
	
	public LaundryRoom getLaundryRoom() throws DataException
	{
		try
		{
			return new LaundryRoom(pos.getLocation());
		}
		catch (TorqueException e)
		{
			log.error("Error getting location " + e.getMessage());
			throw new DataAccessException();
		}
	}
	
	public long getRoomControllerId()
	{
		return pos.getDeviceId();
	}
	
	public RoomController getRoomController() throws DataException
	{
		try
		{
			return new RoomController(pos.getDevice());
		}
		catch (TorqueException e)
		{
			log.error("Error getting device " + e.getMessage());
			throw new DataAccessException();
		}
	}
	
	public String getSerialNumber() throws DataException
	{
		return getRoomController().getSerialCd();
	}
	
	public boolean isLocationAssigned()
	{
		if (pos.getLocationId() == UNKNOWN_LOCATION_ID)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean isOperatorAssigned()
	{
		if (pos.getLocationId() == UNKNOWN_OPERATOR_ID)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean isActive()
	{
		if (pos.getPosActiveYnFlag().equalsIgnoreCase("Y"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void validate() throws DataException
	{
		
	}
	
	public void save() throws DataException
	{
		validate();
		
		try
		{
			pos.save();
		}
		catch (Exception e)
		{
			log.error("Error saving pos " + e.getMessage());
			throw new DataException("Could not save POS.");
		}
	}
	
	public static List retrieveByOperatorAndLocation(long operatorId, long locationId) throws DataException
	{
		List poses = null;
		try
		{
			poses = PosPeer.retrieveByCustomerIdAndLocationId(operatorId, locationId);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving poses for operator " + operatorId + " and location " + locationId + ": "+ te.getMessage());
			throw new DataAccessException();
		}
		
		List list = new ArrayList();
		for(Iterator i = poses.iterator(); i.hasNext();)
		{
			Pos pos = (Pos) i.next();
			list.add(new POS(pos));
		}
		
		return list;
	}
	
	public static List retrieveByOperatorAndLocation(long operatorId, long locationId, boolean active) throws DataException
	{
		List poses = null;
		try
		{
			poses = PosPeer.retrieveByCustomerIdAndLocationId(operatorId, locationId, active);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving poses for operator " + operatorId + " and location " + locationId + ": "+ te.getMessage());
			throw new DataAccessException();
		}
		
		List list = new ArrayList();
		for(Iterator i = poses.iterator(); i.hasNext();)
		{
			Pos pos = (Pos) i.next();
			list.add(new POS(pos));
		}
		
		return list;
	}
	
	public static List retrieveActivePOS(long operatorId, long locationId) throws DataAccessException
	{
		List poses = null;
		try
		{
			poses = PosPeer.retrieveActivePos(operatorId, locationId);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving active poses for operator " + operatorId + " and location " + locationId + ": "+ te.getMessage());
			throw new DataAccessException();
		}
		
		List list = new ArrayList();
		for(Iterator i = poses.iterator(); i.hasNext();)
		{
			Pos pos = (Pos) i.next();
			list.add(new POS(pos));
		}
		
		return list;
	}

	/**
	 * @param posId
	 * @return
	 */
	public static POS retrievePOS(long posId) throws UnknownObjectException, DataException
	{
		Pos pos = null;
		try
		{
			pos = PosPeer.retrieveByPK(posId);
		}
		catch (NoRowsException e)
		{
			log.error("Pos with id " + posId + " not found.");
			throw new UnknownObjectException("Pos not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving pos with id " + posId + ": " + tmre.getMessage());
			throw new DataException("More than one pos found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving pos with id " + posId + ": " + te.getMessage());
			throw new DataAccessException();
		}
		
		return new POS(pos);
	}
}
