/*
 * *******************************************************************
 * 
 * File EsudsTransactionReport.java
 * 
 * Created on May 17, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.model.reports;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.persistence.EsudsTranLineItemDetail;
import com.usatech.esuds.server.persistence.EsudsTranLineItemDetailPeer;
import com.usatech.server.report.Column;
import com.usatech.server.report.Report;
import com.usatech.server.report.Row;
import com.usatech.server.report.RowClass;

/**
 * @author melissa
 *
 */
public class EsudsTransactionReport
{
	public static String CASH = "M";
	public static String STUDENT_ACCOUNT="S";
	
	private static Log log = LogFactory.getLog(EsudsTransactionReport.class);
	
	protected long operatorId = 0;
	protected long campusId = 0;
	protected String paymentType = null;
	protected Date startDate = null;
	protected Date endDate = null;
	protected Report report = null;
	
	public EsudsTransactionReport()
	{
		report = new Report();
		
		// in the future, this maybe passed in
		ArrayList columns = new ArrayList();
		columns.add(new Column("DATE", EsudsTranLineItemDetailPeer.TRAN_START_TS, "tranDate", java.util.Date.class, new SimpleDateFormat("MM/dd/yyyy hh:mm a")));
		columns.add(new Column("RESIDENCE HALL", EsudsTranLineItemDetailPeer.DORM_NAME, "dormName", java.lang.String.class));
		columns.add(new Column("LAUNDRY ROOM", EsudsTranLineItemDetailPeer.ROOM_NAME, "roomName", java.lang.String.class));
		columns.add(new Column("DESCRIPTION", EsudsTranLineItemDetailPeer.TRAN_LINE_ITEM_DESC, "description", java.lang.String.class));
		//columns.add(new Column("QUANTITY", EsudsTranLineItemDetailPeer.TRAN_LINE_ITEM_QUANTITY, "quantity", java.lang.Integer.class));
		//columns.add(new Column("ITEM AMOUNT", EsudsTranLineItemDetailPeer.TRAN_LINE_ITEM_AMOUNT, "itemAmount", java.math.BigDecimal.class, NumberFormat.getCurrencyInstance(Locale.US)));
		columns.add(new Column("TOTAL AMOUNT", EsudsTranLineItemDetailPeer.TRAN_LINE_ITEM_TOT_AMOUNT, "totalAmount", java.math.BigDecimal.class, NumberFormat.getCurrencyInstance(Locale.US)));
		//columns.add(new Column("AUTH SERVICE TYPE", EsudsTranLineItemDetailPeer.AUTHORITY_SERVICE_TYPE_ID, "authServiceType", java.lang.Long.class, NumberFormat.getNumberInstance(Locale.US)));
		
		report.setColumns(columns);
	}
	
		
	public Report generateReport() throws DataAccessException
	{
		List trans = null;
		
		try
		{
			trans = EsudsTranLineItemDetailPeer.retrieveTransactions(startDate, endDate, campusId, paymentType);
		}
		catch (TorqueException te)
		{
			log.error("Error retireving transactions: " + te.getMessage());
			throw new DataAccessException();
		}
		
		report.setRowClass(RowClass.getRowClassForColumns(report.getColumns()));
		
		for (Iterator i = trans.iterator(); i.hasNext();)
		{
			Row row = new Row(report.getRowClass());
			EsudsTranLineItemDetail tran = (EsudsTranLineItemDetail) i.next();
			
			for (Iterator j = report.getColumns().iterator(); j.hasNext();)
			{
				Column column = (Column) j.next();
				log.debug("Setting " + column.getDbColumnName());
				Object obj = null;
				obj = tran.getByPeerName(column.getDbColumnName());
				if (obj != null)
				{
					row.set(column.getPropertyName(), obj);
				}
				else
				{
					log.debug(column.getPropertyName() + " is null.");
				}
			}
			
			report.addRow(row);
		}
		
		return report;
	}
	
	/**
	 * @return
	 */
	public Date getEndDate()
	{
		return endDate;
	}

	/**
	 * @return
	 */
	public long getOperatorId()
	{
		return operatorId;
	}

	/**
	 * @return
	 */
	public Date getStartDate()
	{
		return startDate;
	}

	/**
	 * @param date
	 */
	public void setEndDate(Date date)
	{
		endDate = date;
	}

	/**
	 * @param l
	 */
	public void setOperatorId(long l)
	{
		operatorId = l;
	}

	/**
	 * @param date
	 */
	public void setStartDate(Date date)
	{
		startDate = date;
	}

	/**
	 * @return
	 */
	public long getCampusId()
	{
		return campusId;
	}

	/**
	 * @return
	 */
	public String getPaymentType()
	{
		return paymentType;
	}

	/**
	 * @param l
	 */
	public void setCampusId(long l)
	{
		campusId = l;
	}

	/**
	 * @param string
	 */
	public void setPaymentType(String string)
	{
		paymentType = string;
	}

}
