package com.usatech.esuds.server.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.TotalUsageReportBean;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.persistence.TotSalesByCampus;
import com.usatech.esuds.server.persistence.TotSalesByCampusPeer;
import com.usatech.esuds.server.persistence.TotSalesByDorm;
import com.usatech.esuds.server.persistence.TotSalesByDormPeer;
import com.usatech.esuds.server.persistence.TotSalesByLaundryRoom;
import com.usatech.esuds.server.persistence.TotSalesByLaundryRoomPeer;
import com.usatech.esuds.server.persistence.TotSalesBySchool;
import com.usatech.esuds.server.persistence.TotSalesBySchoolPeer;
import com.usatech.esuds.server.util.AccountUtil;

public class UsageTotal
{
	public final static String OPERATOR = "operator";
	public final static String SCHOOL = "school";
	public final static String CAMPUS = "campus";
	public final static String DORM = "dorm";
	protected final static BigDecimal ZERO = new BigDecimal(0).setScale(2);
	
	private static Log log = LogFactory.getLog(UsageTotal.class);

	public static List retrieveTotalsBySchool(long operatorId, Date startDate, Date endDate) throws DataAccessException
	{
		ArrayList results = new ArrayList();

		//Look up totals by School for this operator with dates
		List list = null;
		try
		{
			list = TotSalesBySchoolPeer.getTotForOperator(operatorId, startDate, endDate);
		}
		catch (TorqueException te)
		{
			log.error("Error getting Totals for Operator: " + te.getMessage());
			throw new DataAccessException();
		}

		//add up for each School
		Object[] array = list.toArray();

		for (int i = 0; i < array.length; i++)
		{
			TotSalesBySchool tot = (TotSalesBySchool) array[i];

			//loop through the current results looking for school id
			Iterator iterator = results.iterator();
			boolean found = false;

			while (iterator.hasNext())
			{
				TotalUsageReportBean bean = (TotalUsageReportBean) iterator.next();

				if (bean.getId() == tot.getSchoolId())
				{
					//Already have this school, add totals
					bean.setTotCycles(bean.getTotCycles() + tot.getTotCycles());
					bean.setTotDeterg(bean.getTotDeterg() + tot.getTotDeterg());
					bean.setTotFabricSoft(bean.getTotFabricSoft() + tot.getTotFabricSoft());
					bean.setTotDryCycles(bean.getTotDryCycles() + tot.getTotDryCycles());
					bean.setTotWashCycles(bean.getTotWashCycles() + tot.getTotWashCycles());
					if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
						bean.setTotAmount(bean.getTotAmount().add(tot.getTotAmount()));
						bean.setTotDryAmount(bean.getTotDryAmount().add(tot.getTotDryAmount()));
						bean.setTotWashAmount(bean.getTotWashAmount().add(tot.getTotWashAmount()));
					}
					//we found it
					found = true;
				}
			}

			if (!found)
			{
				//Not found, create a new one, and add it to the results
				TotalUsageReportBean bean = new TotalUsageReportBean();
				bean.setId(tot.getSchoolId());
				bean.setName(tot.getSchoolName());
				bean.setTotCycles(tot.getTotCycles());
				bean.setTotDeterg(tot.getTotDeterg());
				bean.setTotFabricSoft(tot.getTotFabricSoft());
				bean.setTotDryCycles(tot.getTotDryCycles());
				bean.setTotWashCycles(tot.getTotWashCycles());
				if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
					bean.setTotAmount(tot.getTotAmount());
					bean.setTotDryAmount(tot.getTotDryAmount());
					bean.setTotWashAmount(tot.getTotWashAmount());
				} else {				    
					bean.setTotAmount(ZERO);
					bean.setTotDryAmount(ZERO);
					bean.setTotWashAmount(ZERO);				    
				}
				//set for navigation
				HashMap params = new HashMap();
				params.put("id", "" + bean.getId());
				params.put("type", SCHOOL);
				params.put("name", bean.getName());
				params.put("action", "showDetail");

				bean.setParams(params);

				results.add(bean);
			}

		}

		//set into TotalUsageReportBean, and add to results

		return results;
	}

	public static List retrieveTotalsByCampus(long schoolId, long operatorId, Date startDate, Date endDate) throws DataAccessException
	{
		ArrayList results = new ArrayList();

		//Look up totals by Campus for this operator with dates
		List list = null;
		try
		{
			list = TotSalesByCampusPeer.getTotForSchoolAndOperator(schoolId, operatorId, startDate, endDate);
		}
		catch (TorqueException te)
		{
			log.error("Error getting Totals for Operator: " + te.getMessage());
			throw new DataAccessException();
		}

		//add up for each Campus
		Object[] array = list.toArray();

		for (int i = 0; i < array.length; i++)
		{
			TotSalesByCampus tot = (TotSalesByCampus) array[i];

			//loop through the current results looking for school id
			Iterator iterator = results.iterator();
			boolean found = false;

			while (iterator.hasNext())
			{
				TotalUsageReportBean bean = (TotalUsageReportBean) iterator.next();

				if (bean.getId() == tot.getCampusId())
				{
					//Already have this school, add totals
					bean.setTotCycles(bean.getTotCycles() + tot.getTotCycles());
					bean.setTotDeterg(bean.getTotDeterg() + tot.getTotDeterg());
					bean.setTotFabricSoft(bean.getTotFabricSoft() + tot.getTotFabricSoft());
					bean.setTotDryCycles(bean.getTotDryCycles() + tot.getTotDryCycles());
					bean.setTotWashCycles(bean.getTotWashCycles() + tot.getTotWashCycles());
					if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
						bean.setTotDryAmount(bean.getTotDryAmount().add(tot.getTotDryAmount()));
						bean.setTotWashAmount(bean.getTotWashAmount().add(tot.getTotWashAmount()));
						bean.setTotAmount(bean.getTotAmount().add(tot.getTotAmount()));
					}
					//we found it
					found = true;
				}
			}

			if (!found)
			{
				//Not found, create a new one, and add it to the results
				TotalUsageReportBean bean = new TotalUsageReportBean();
				bean.setId(tot.getCampusId());
				bean.setName(tot.getCampusName());
				bean.setTotCycles(tot.getTotCycles());
				bean.setTotDeterg(tot.getTotDeterg());
				bean.setTotFabricSoft(tot.getTotFabricSoft());
				bean.setTotDryCycles(tot.getTotDryCycles());
				bean.setTotWashCycles(tot.getTotWashCycles());
				if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
					bean.setTotAmount(tot.getTotAmount());
					bean.setTotDryAmount(tot.getTotDryAmount());
					bean.setTotWashAmount(tot.getTotWashAmount());
				} else {				    
					bean.setTotAmount(ZERO);
					bean.setTotDryAmount(ZERO);
					bean.setTotWashAmount(ZERO);				    
				}

				//set for navigation
				HashMap params = new HashMap();
				params.put("id", "" + bean.getId());
				params.put("type", CAMPUS);
				params.put("name", bean.getName());
				params.put("action", "showDetail");

				bean.setParams(params);

				results.add(bean);
			}

		}

		//set into TotalUsageReportBean, and add to results

		return results;
	}

	public static List retrieveTotalsByDorm(
		long campusId,
		long operatorId,
		Date startDate,
		Date endDate) throws DataAccessException
	{
		ArrayList results = new ArrayList();

		//Look up totals by Dorm for this operator with dates
		List list = null;
		try
		{
			list = TotSalesByDormPeer.getTotForCampus(campusId, operatorId, startDate, endDate);
		}
		catch (TorqueException te)
		{
			log.error("Error getting Totals for Campus: " + te.getMessage());
			throw new DataAccessException();
		}

		//add up for each Dorm
		Object[] array = list.toArray();

		for (int i = 0; i < array.length; i++)
		{
			TotSalesByDorm tot = (TotSalesByDorm) array[i];

			//loop through the current results looking for school id
			Iterator iterator = results.iterator();
			boolean found = false;

			while (iterator.hasNext())
			{
				TotalUsageReportBean bean = (TotalUsageReportBean) iterator.next();

				if (bean.getId() == tot.getDormId())
				{
					//Already have this school, add totals
					bean.setTotCycles(bean.getTotCycles() + tot.getTotCycles());
					bean.setTotDeterg(bean.getTotDeterg() + tot.getTotDeterg());
					bean.setTotFabricSoft(bean.getTotFabricSoft() + tot.getTotFabricSoft());
					bean.setTotDryCycles(bean.getTotDryCycles() + tot.getTotDryCycles());
					bean.setTotWashCycles(bean.getTotWashCycles() + tot.getTotWashCycles());
					if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
						bean.setTotAmount(bean.getTotAmount().add(tot.getTotAmount()));
						bean.setTotDryAmount(bean.getTotDryAmount().add(tot.getTotDryAmount()));
						bean.setTotWashAmount(bean.getTotWashAmount().add(tot.getTotWashAmount()));
					}

					//we found it
					found = true;
				}
			}

			if (!found)
			{
				//Not found, create a new one, and add it to the results
				TotalUsageReportBean bean = new TotalUsageReportBean();
				bean.setId(tot.getDormId());
				bean.setName(tot.getDormName());
				bean.setTotCycles(tot.getTotCycles());
				bean.setTotDeterg(tot.getTotDeterg());
				bean.setTotFabricSoft(tot.getTotFabricSoft());
				bean.setTotDryCycles(tot.getTotDryCycles());
				bean.setTotWashCycles(tot.getTotWashCycles());
				if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
					bean.setTotAmount(tot.getTotAmount());
					bean.setTotDryAmount(tot.getTotDryAmount());
					bean.setTotWashAmount(tot.getTotWashAmount());
				} else {				    
					bean.setTotAmount(ZERO);
					bean.setTotDryAmount(ZERO);
					bean.setTotWashAmount(ZERO);				    
				}

				//set for navigation
				HashMap params = new HashMap();
				params.put("id", "" + bean.getId());
				params.put("type", DORM);
				params.put("name", bean.getName());
				params.put("action", "showDetail");

				bean.setParams(params);

				results.add(bean);
			}

		}

		//set into TotalUsageReportBean, and add to results

		return results;
	}

	public static List retrieveTotalsByLaundryRoom(
		long dormId,
		long operatorId, 
		Date startDate,
		Date endDate) throws DataAccessException
	{
		ArrayList results = new ArrayList();

		//Look up totals by LaundryRoom for this operator with dates
		List list = null;
		try
		{
			list = TotSalesByLaundryRoomPeer.getTotForDorm(dormId, operatorId, startDate, endDate);
		}
		catch (TorqueException te)
		{
			log.error("Error getting Totals for Operator: " + te.getMessage());
			throw new DataAccessException();
		}

		//add up for each LaundryRoom
		Object[] array = list.toArray();

		for (int i = 0; i < array.length; i++)
		{
			TotSalesByLaundryRoom tot = (TotSalesByLaundryRoom) array[i];

			//loop through the current results looking for school id
			Iterator iterator = results.iterator();
			boolean found = false;

			while (iterator.hasNext())
			{
				TotalUsageReportBean bean = (TotalUsageReportBean) iterator.next();

				if (bean.getId() == tot.getLaundryRoomId())
				{
					//Already have this school, add totals
					bean.setTotCycles(bean.getTotCycles() + tot.getTotCycles());
					bean.setTotDeterg(bean.getTotDeterg() + tot.getTotDeterg());
					bean.setTotFabricSoft(bean.getTotFabricSoft() + tot.getTotFabricSoft());
					bean.setTotDryCycles(bean.getTotDryCycles() + tot.getTotDryCycles());
					bean.setTotWashCycles(bean.getTotWashCycles() + tot.getTotWashCycles());
					if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
						bean.setTotAmount(bean.getTotAmount().add(tot.getTotAmount()));
						bean.setTotDryAmount(bean.getTotDryAmount().add(tot.getTotDryAmount()));
						bean.setTotWashAmount(bean.getTotWashAmount().add(tot.getTotWashAmount()));
					}

					//we found it
					found = true;
				}
			}

			if (!found)
			{
				//Not found, create a new one, and add it to the results
				TotalUsageReportBean bean = new TotalUsageReportBean();
				bean.setId(tot.getLaundryRoomId());
				bean.setName(tot.getLaundryRoomName());
				bean.setTotCycles(tot.getTotCycles());
				bean.setTotDeterg(tot.getTotDeterg());
				bean.setTotFabricSoft(tot.getTotFabricSoft());
				bean.setTotDryCycles(tot.getTotDryCycles());
				bean.setTotWashCycles(tot.getTotWashCycles());
				if(AccountUtil.showAmountToOperator(tot.getAuthServiceType())) {
					bean.setTotAmount(tot.getTotAmount());
					bean.setTotDryAmount(tot.getTotDryAmount());
					bean.setTotWashAmount(tot.getTotWashAmount());
				} else {				    
					bean.setTotAmount(ZERO);
					bean.setTotDryAmount(ZERO);
					bean.setTotWashAmount(ZERO);				    
				}

				//no navigation to set, lowest level

				results.add(bean);
			}

		}

		//set into TotalUsageReportBean, and add to results

		return results;
	}
}