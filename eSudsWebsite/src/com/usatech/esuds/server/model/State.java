package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.server.persistence.StatePeer;

public class State
{
	private static Log log = LogFactory.getLog(State.class);
	
	private com.usatech.server.persistence.State state = null;
	
	protected State(com.usatech.server.persistence.State s)
	{
		state = s;
	}
	
	public String getCd()
	{
		return state.getStateCd();
	}
	
	public String getName()
	{
		return state.getStateName();
	}
	
	public static List retrieveAll() throws DataException
	{
		List states = null;
		
		try
		{
			states = StatePeer.retrieveAll();
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving states " + te);
			throw new DataAccessException();
		}
		
		List result = new ArrayList();
		
		for (Iterator i = states.iterator(); i.hasNext();)
		{
			com.usatech.server.persistence.State c = (com.usatech.server.persistence.State) i.next();
			result.add(new State(c));
		}
		
		return result;
	}
}
