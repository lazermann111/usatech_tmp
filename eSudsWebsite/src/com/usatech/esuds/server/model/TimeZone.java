package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.server.persistence.TimeZonePeer;

public  class TimeZone
{
	private static Log log = LogFactory.getLog(TimeZone.class);
	
	private com.usatech.server.persistence.TimeZone timeZone = null;
	
	protected TimeZone(com.usatech.server.persistence.TimeZone tz)
	{
		timeZone = tz;
	}
	
	public String getCd()
	{
		return timeZone.getTimeZoneCd();
	}
	
	public String getName()
	{
		return timeZone.getTimeZoneName();
	}
	
	public static List retrieveAll() throws DataException
	{
		List timeZones = null;
		
		try
		{
			timeZones = TimeZonePeer.retrieveAll();
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving states " + te);
			throw new DataAccessException();
		}
		
		List result = new ArrayList();
		
		for (Iterator i = timeZones.iterator(); i.hasNext();)
		{
			com.usatech.server.persistence.TimeZone c = (com.usatech.server.persistence.TimeZone) i.next();
			result.add(new TimeZone(c));
		}
		
		return result;
	}
}
