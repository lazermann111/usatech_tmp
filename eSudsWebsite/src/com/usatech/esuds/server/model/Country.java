package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.server.persistence.CountryPeer;

public class Country
{
	private static Log log = LogFactory.getLog(Country.class);
	private com.usatech.server.persistence.Country country = null;
	
	protected Country(com.usatech.server.persistence.Country c)
	{
		country = c;
	}
	
	public String getCd()
	{
		return country.getCountryCd();
	}
	
	public String getName()
	{
		return country.getCountryName();
	}

	public static List retrieveAll() throws DataException
	{
		List countries = null;
		
		try
		{
			countries = CountryPeer.retrieveAll();
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving countries " + te.getMessage());
			throw new DataAccessException();
		}
		
		List result = new ArrayList();
		
		for (Iterator i = countries.iterator(); i.hasNext();)
		{
			com.usatech.server.persistence.Country c = (com.usatech.server.persistence.Country) i.next();
			result.add(new Country(c));
		}
		
		return result;
	}
}
