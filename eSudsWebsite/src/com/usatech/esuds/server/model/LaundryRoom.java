package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.Device;
import com.usatech.server.persistence.Location;
import com.usatech.server.persistence.LocationPeer;
import com.usatech.server.persistence.LocationType;
import com.usatech.server.persistence.LocationTypePeer;

public class LaundryRoom
{
	private static Log log = LogFactory.getLog(LaundryRoom.class);
	
	public static final String LOCATION_TYPE_DESC = "Education - Secondary Room";
	public static final long UNKNOWN_LAUNDRY_ROOM_ID = 1;
	
	private Location location = null;
	
	public LaundryRoom()
	{
		location = new Location();
		location.setNew(true);
		
		try
		{
			LocationType type = LocationTypePeer.getLocationTypeByDesc(LOCATION_TYPE_DESC);
			location.setLocationType(type);
		}
		catch (TorqueException te)
		{
			// should not happen, but do something here
		}		
	}
	
	protected LaundryRoom(Location l) throws DataException
	{
		String typeDesc = null;
		
		try
		{
			typeDesc = l.getLocationType().getLocationTypeDesc();
		}
		catch (TorqueException e)
		{
			log.error("Could not get location type for location: " + e.getMessage());
			throw new DataException("Could not create laundry room.");
		}
		
		if (typeDesc.equals(LOCATION_TYPE_DESC))
		{
			location = l;
		}
		else
		{
			throw new DataException("Location is wrong type.");
		}
	}

	public long getLaundryRoomId()
	{
		return location.getLocationId();
	}
	
	public long getDormId()
	{
		return location.getParentLocationId();
	}
	
	public long getOperatorId() throws DataException
	{
		return getDorm().getCampus().getOperatorId();
	}
	
	public void setDormId(long id) throws DataException
	{
		try
		{
			location.setParentLocationId(id);
		}
		catch (TorqueException e)
		{
			log.error("Error setting parent location id: " + e.getMessage());
			throw new DataException("Could not set dormId.");
		}
	}
	
	public Dorm getDorm()
	{
		try
		{
			return new Dorm(location.getLocationRelatedByParentLocationId());
		}
		catch (TorqueException e)
		{
			log.error("Error getting parent location: " + e.getMessage());
			return null;
		}
		catch (DataException de)
		{
			log.error("Error creating dorm: " + de.getMessage());
			return null;
		}
	}
	
	public String getName()
	{
		return location.getLocationName();
	}
	
	public void setName(String name)
	{
		location.setLocationName(name);
	}
	
	public String getRoomImageUrl() {
	    return location.getLocationImageUrl();
	}
	
	public void setRoomImageUrl(String roomImageUrl) {
	    location.setLocationImageUrl(roomImageUrl);
	}
	
	/**
	 * Gets a List of all active or inactive Room Controllers associated with a LaundryRoom
	 * 
	 * @param active true to retrieve list of active Room Controllers, false to retrieve list of inactive RCs
	 * @return a List of Device
	 */
	public List getRoomControllers(boolean active) throws DataException
	{
		List devices = null;
		try
		{
			devices = location.getDevices(active);
		}
		catch (TorqueException e)
		{
			log.error("Error getting Devices: " + e.getMessage());
			throw new DataAccessException();
		}
		
		List roomControllers = new ArrayList();
		for (Iterator i = devices.iterator(); i.hasNext();)
		{
			Device device = (Device) i.next();
			roomControllers.add(new RoomController(device));
		}
		
		return roomControllers;

	}

	public List retrieveLaundryRoomEquips() throws DataException
	{
		ArrayList list = new ArrayList();
		List roomControllers = getRoomControllers(true);
		Iterator iterator = roomControllers.iterator();
		while (iterator.hasNext())
		{
			List roomControllerEquipList = ((RoomController) iterator.next()).retrieveLaundryRoomEquips();
			list.addAll(roomControllerEquipList);
			
		}
		return list;
	}
	
	public List retrieveLaundryRoomEquips(String type) throws DataException
	{
		List roomControllers = getRoomControllers(true);
		
		List roomControllerEquipList = null;
		List list  = new ArrayList();
		
		for (Iterator i =roomControllers.iterator(); i.hasNext();)
		{
			roomControllerEquipList = ((RoomController) i.next()).retrieveLaundryRoomEquips(type);
			list.addAll(roomControllerEquipList);
		}
		
		return list;
	}
	
	public List retrieveLaundryRoomEquips(String type, int position) throws DataException
	{
		
		List roomControllers = getRoomControllers(true);
		
		ArrayList list = new ArrayList();
		for (Iterator i = roomControllers.iterator(); i.hasNext();)
		{
			List roomControllerEquipList = ((RoomController) i.next()).retrieveLaundryRoomEquips(type, position);
			list.addAll(roomControllerEquipList);
		}
		return list;
	}
	
	public boolean isActive()
	{
		if (location.isActive())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setActive(boolean active)
	{
		if (active)
		{
			location.setLocationActiveYnFlag("Y");
		}
		else
		{
			location.setLocationActiveYnFlag("N");
		}
	}
	
	public void validate() throws DataException
	{
		if (!location.isNew() && location.getLocationId() < 1)
		{
			throw new DataException("Id is not valid.");
		}
		
		if (location.getLocationName() == null || location.getLocationName().equals(""))
		{
			throw new DataException("Name is not valid.");
		}
	}
	
	public void save() throws DataException
	{
		validate();
		
		try
		{
			location.save();
		}
		catch (Exception e)
		{
			log.error("Error saving location: " + e.getMessage());
			throw new DataException("Could not save laundry room.");
		}
	}
	
	public static LaundryRoom retrieveLaundryRoom(long laundryRoomId) throws UnknownObjectException, DataException
	{
		return retrieveLaundryRoom(laundryRoomId, false);
	}
	
	public static LaundryRoom retrieveLaundryRoom(long laundryRoomId, boolean allowInactive) throws UnknownObjectException, DataException
	{
		Location location = null;
		
		try
		{
			location = LocationPeer.retrieveByPK(laundryRoomId);
		
			// check type
			if (location.getLocationType().getLocationTypeDesc().equals(LOCATION_TYPE_DESC))
			{
				if (allowInactive || location.isActive())
				{
					return new LaundryRoom(location);
				}
				else
				{
					throw new UnknownObjectException("Laundry room not found.");
				}
			}
			else
			{
				throw new UnknownObjectException("Laundry room not found.");
			}
		}
		catch (NoRowsException e)
		{
			log.error("Laundry room with id " + laundryRoomId + " not found.", e);
			throw new UnknownObjectException("Laundry room not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving laundryRoom with id " + laundryRoomId + ": " + tmre.getMessage(), tmre);
			throw new DataException("More than one laundryRoom found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving laundryRoom with id " + laundryRoomId + ": " + te.getMessage(), te);
			throw new DataAccessException();
		}
	}
	
	public static List retrieveAll() throws DataException
	{
		List locations = null;
	
		try
		{
			locations = LocationPeer.retrieveByLocationTypeDesc(LOCATION_TYPE_DESC);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving all laundry rooms: " + te.getMessage());
			throw new DataAccessException();
		}
	
		List laundryRooms = new ArrayList();
	
		for (Iterator i = locations.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			laundryRooms.add(new LaundryRoom(l));
		}
		
		return laundryRooms;
	}

	/**
	 * @param dormId
	 * @return
	 */
	public static List retrieveByDormId(long dormId) throws DataException
	{
		List locations = null;
	
		try
		{
			// active locations only
			locations = LocationPeer.retrieveByLocationTypeDescAndParentLocationId(LOCATION_TYPE_DESC, dormId, true, false);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving laundry rooms associated with dorm " + dormId + ": " + te.getMessage());
			throw new DataAccessException();
		}
	
		List laundryRooms = new ArrayList();
	
		for (Iterator i = locations.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			laundryRooms.add(new LaundryRoom(l));
		}
	
		return laundryRooms;
	}
		
		
	
}
