package com.usatech.esuds.server.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.Location;
import com.usatech.server.persistence.LocationPeer;
import com.usatech.server.persistence.LocationType;
import com.usatech.server.persistence.LocationTypePeer;


public class Dorm
{
	private static Log log = LogFactory.getLog(Dorm.class);
	
	public static final String LOCATION_TYPE_DESC = "Education - Secondary Dorm";
	
	private Location location = null;
	
	public Dorm()
	{
		location = new Location();
		location.setNew(true);
		
		try
		{
			LocationType type = LocationTypePeer.getLocationTypeByDesc(LOCATION_TYPE_DESC);
			location.setLocationType(type);
		}
		catch (TorqueException te)
		{
			// should not happen, but do something here
		}
	}
	
	protected Dorm(Location l) throws DataException
	{
		String typeDesc = null;
		
		try
		{
			typeDesc = l.getLocationType().getLocationTypeDesc();
		}
		catch (TorqueException e)
		{
			log.error("Could not get location type for location: " + e.getMessage());
			throw new DataException("Could not create dorm.");
		}
		/*
		if (typeDesc.equals(LOCATION_TYPE_DESC))
		{
			*/location = l;/*
		}
		else
		{
			throw new DataException("Location is wrong type.");
		}*/
	}
	
	public long getDormId()
	{
		return location.getLocationId();
	}
	
	public long getCampusId()
	{
		return location.getParentLocationId();
	}
	
	public long getOperatorId() throws DataException
	{
		return getCampus().getOperatorId();
	}
	
	public void setCampusId(long id) throws DataException
	{
		try
		{
			location.setParentLocationId(id);
		}
		catch (TorqueException e)
		{
			log.error("Error setting parent location id: " + e.getMessage());
			throw new DataException("Could not set campus id.");
		}
	}
	
	public Campus getCampus()
	{
		try
		{
			return new Campus(location.getLocationRelatedByParentLocationId());
		}
		catch (TorqueException e)
		{
			log.error("Error getting parent location: " + e.getMessage());
			return null;
		}
		catch (DataException de)
		{
			log.error("Error creating campus: " + de.getMessage());
			return null;
		}
	}
	
	public String getName()
	{
		return location.getLocationName();
	}
	
	public void setName(String name)
	{
		location.setLocationName(name);
	}
	
	public String getAddr1()
	{
		return location.getLocationAddr1();
	}
	
	public void setAddr1(String addr1)
	{
		location.setLocationAddr1(addr1);
	}
	
	public String getAddr2()
	{
		return location.getLocationAddr2();
	}
	
	public void setAddr2(String addr2)
	{
		location.setLocationAddr1(addr2);
	}
	
	public String getCity()
	{
		return location.getLocationCity();
	}
	
	public void setCity(String city)
	{
		location.setLocationCity(city);
	}
	 
	public String getCounty()
		{
			return location.getLocationCounty();
		}
	
		public void setCounty(String county)
		{
			location.setLocationCounty(county);
		}
	
		public String getStateCd()
		{
			return location.getLocationStateCd();
		}
	
		public void setStateCd(String cd) throws DataException
		{
			try
			{
				location.setLocationStateCd(cd);
			}
			catch (TorqueException e)
			{
				log.error("Error setting state cd: " + e.getMessage());
				throw new DataException("Could not set StateCd");
			}
		}
	
		public String getCountryCd() 
		{
			return location.getLocationCountryCd();
		}
	
		public void setCountryCd(String cd) throws DataException
		{
			try
			{
				location.setLocationCountryCd(cd);
			}
			catch (TorqueException e)
			{
				log.error("Error setting country cd: " + e.getMessage());
				throw new DataException("Could not set CountryCd");
			}
		}
	
		public String getPostalCd()
		{
			return location.getLocationPostalCd();
		}
	
		public void setPostalCd(String cd)
		{
			location.setLocationPostalCd(cd);
		}
		
	public List getLaundryRooms() throws DataException
	{
		// we only want to return active ones unless we explicitly ask for all
		return getLaundryRooms(true);
	}
		
	public List getLaundryRooms(boolean activeOnly) throws DataException
		{
			List list = null;
			ArrayList laundryRooms = new ArrayList();
		
			try
			{
				if (activeOnly)
				{
					list = location.getActiveDependentLocations(true);
				}
				else
				{
					list = location.getDependentLocations(true);
				}
			}
			catch (NoRowsException nre)
			{
				return laundryRooms;
			}
			catch (TorqueException e)
			{
				log.error("Error getting dependent locations: " + e.getMessage());
				throw new DataAccessException();
			}
		
			for (Iterator i = list.iterator(); i.hasNext();)
			{
				Location l = (Location) i.next();
				laundryRooms.add(new LaundryRoom(l));
			}
		
			return laundryRooms;
		}

	public boolean isActive()
	{
		if (location.isActive())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void setActive(boolean active)
	{
		if (active)
		{
			location.setLocationActiveYnFlag("Y");
		}
		else
		{
			location.setLocationActiveYnFlag("N");
		}
	}
			
	public void validate() throws DataException
		{
			if (!location.isNew() && location.getLocationId() < 1)
			{
				throw new DataException("Id is not valid.");
			}
		
			if (location.getLocationName() == null || location.getLocationName().equals(""))
			{
				throw new DataException("Name is not valid.");
			}
		}
	
		public void save() throws DataException
		{
			validate();
		
			try
			{
				location.save();
			}
			catch (Exception e)
			{
				log.error("Error saving location: " + e.getMessage());
				throw new DataException("Could not save dorm.");
			}
		}
		
	public static Dorm retrieveDorm (long dormId) throws UnknownObjectException, DataException
	{
		return retrieveDorm(dormId, false);
	}
		
	public static Dorm retrieveDorm(long dormId, boolean allowInactive) throws UnknownObjectException, DataException
	{
		Location location = null;
		
		try
		{
			location = LocationPeer.retrieveByPK(dormId);
			
			// check type
			if (location.getLocationType().getLocationTypeDesc().equals(LOCATION_TYPE_DESC))
			{
				if (allowInactive || location.isActive())
				{
					return new Dorm(location);
				}
				else
				{
					throw new UnknownObjectException("Dorm not found.");
				}
			}
			else
			{
				throw new UnknownObjectException("Dorm not found.");
			}
		}
		catch (NoRowsException e)
		{
			log.error("Dorm with id " + dormId + " not found.");
			throw new UnknownObjectException("Dorm not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving dorm with id " + dormId + ": " + tmre.getMessage());
			throw new DataException("More than one dorm found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving dorm with id " + dormId + ": " + te.getMessage());
			throw new DataAccessException();
		}
	}
	
	public static List retrieveAll() throws DataException
	{
		List locations = null;
		
		try
		{
			locations = LocationPeer.retrieveByLocationTypeDesc(LOCATION_TYPE_DESC, true);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving all dorms: " + te.getMessage());
			throw new DataAccessException();
		}
		
		List dorms = new ArrayList();
	
		for (Iterator i = locations.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			dorms.add(new Dorm(l));
		}
		
		return dorms;
	}
	
	public static List retrieveByCampusId(long campusId) throws DataException
	{
		List locations = null;
		
		try
		{
			// active locations only
			locations = LocationPeer.retrieveByLocationTypeDescAndParentLocationId(LOCATION_TYPE_DESC, campusId, true, true);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving dorms associated with campus " + campusId + ": " + te.getMessage());
			throw new DataAccessException();
		}
		
		List dorms = new ArrayList();
	
		for (Iterator i = locations.iterator(); i.hasNext();)
		{
			Location l = (Location) i.next();
			dorms.add(new Dorm(l));
		}
		
		return dorms;
	}
}
