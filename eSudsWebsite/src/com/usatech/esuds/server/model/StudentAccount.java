/*
 * *******************************************************************
 * 
 * File StudentAccount.java
 * 
 * Created on March 25, 2004
 *
 * Create by Melissa Klein
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.model;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.StudentAccountBean;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.esuds.server.persistence.VwTranSummaryPeer;
import com.usatech.esuds.server.util.AccountUtil;
import com.usatech.server.persistence.ConsumerAcct;
import com.usatech.server.persistence.ConsumerAcctPeer;
import com.usatech.server.persistence.Location;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StudentAccount
{
	private static Log log = LogFactory.getLog(StudentAccount.class);
	
	private ConsumerAcct account = null;
	
	private static final String CONSUMER_TYPE_DESC = "Student";
	
	public StudentAccount()
	{
		account = new ConsumerAcct();
		account.setNew(true);
	}
	
	protected StudentAccount(ConsumerAcct a)
	{
		account = a;
	}
	
	public long getAccountId()
	{
			return account.getConsumerAcctId();
	}
	
	public String getStudentAccountCd()
	{
		return account.getConsumerAcctCd();
	}
	
	public void setStudentAccountCd(String cd)
	{
		account.setConsumerAcctCd(cd);
	}
	
	public BigDecimal getAccountBalance()
	{
		return account.getConsumerAcctBalance();
	}
		

	public String getAccountConfirmationCd()
	{
		return account.getConsumerAcctConfirmationCd();
	}

	public Student getStudent() throws DataException
	{
		try
		{
			return new Student(account.getConsumer());
		}
		catch (TorqueException e)
		{
			log.error("Error getting consumer " + e.getMessage());
			throw new DataAccessException();
		}
	}	

	public long getStudentId()
	{
		return account.getConsumerId();
	}
	

	public boolean isActive()
	{
		if (account.getConsumerAcctActiveYnFlag().equals("Y"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public String getStudentName() throws DataException
	{
		Student s = getStudent();
		
		String firstName = s.getFirstName();
		String middleName = s.getMiddleName();
		String lastName = s.getLastName();

		String name = new String();		
		if (firstName != null)
		{
			name += firstName + " ";
		}
		
		if (middleName != null)
		{
			name += middleName + " ";
		}
		
		if (lastName != null)
		{
			name += lastName;
		}
		
		return name;
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @return List of TransactionBeans - right now this is at the Transaction Line Item Level
	 * @throws UnknownObjectException
	 * @throws DataAccessException
	 */
	public List retrieveAccountActivity(Date startDate, Date endDate) throws UnknownObjectException, DataException
	{		
		// get Transactions
		List transactions = null;
		try
		{
			transactions = VwTranSummaryPeer.retrieveTransactions(startDate, endDate, account.getConsumerAcctId());
		}
		catch (TorqueException te)
		{
			log.error("Error Getting transactions for account with id " +  account.getConsumerAcctId() + " " + te.getMessage());
			throw new DataAccessException();
		}
		
		/* why do we do this??? Can we simply use the VwTranSummary object?
		ArrayList transList = new ArrayList();
		for (Iterator i = transactions.iterator(); i.hasNext();)
		{
			VwTranSummary tran = (VwTranSummary) i.next();
			TransactionBean bean = new TransactionBean();

			bean.setDate(tran.getTranStartTs());		
			bean.setDescription(tran.getTranLineItemDesc());
			if(tran.getTranSettlementAmount() != null) 
			    bean.setPrice(tran.getTranSettlementAmount().setScale(2));
			transList.add(bean);
		}
		return transList;	
		//*/
		return transactions;
	}
	
	public List retrieveAccountActivity() throws UnknownObjectException, DataException
	{	
		// get Transactions
		List transactions = null;
		try
		{
			transactions = VwTranSummaryPeer.retrieveTransactions(account.getConsumerAcctId());
		}
		catch (TorqueException te)
		{
			log.error("Error Getting transactions for account with id " + account.getConsumerAcctId() + " " + te.getMessage());
			throw new DataAccessException();
		}
			
		/* why do we do this??? Can we simply use the VwTranSummary object?
		ArrayList transList = new ArrayList();

		for (Iterator i = transactions.iterator(); i.hasNext();)
		{
			VwTranSummary tran = (VwTranSummary) i.next();
			TransactionBean bean = new TransactionBean();

			bean.setDate(tran.getTranStartTs());		
			bean.setDescription(tran.getTranLineItemDesc());
			if(tran.getTranSettlementAmount() != null)
			    bean.setPrice(tran.getTranSettlementAmount().setScale(2));
			transList.add(bean);
		}

		return transList;	
		//*/
		return transactions;
	}

	public StudentAccountBean retrieveAccountInfo() throws UnknownObjectException, DataException
	{	
		StudentAccountBean accountBean = new StudentAccountBean();
		accountBean.setAccountId(account.getConsumerAcctId());
		accountBean.setStudentId(account.getConsumerId());
		accountBean.setActive(account.isActive());
		
		return accountBean;
	}
	
	public boolean activateAccount(String code) throws UnknownObjectException, DataException
	{
		if (account.getConsumerAcctConfirmationCd().equals(code))
		{
			account.setConsumerAcctActiveYnFlag("Y");
			try
			{
				account.save();
			}
			catch (Exception e)
			{
				log.error("Error saving account " + e.getMessage());
				throw new DataException("Could not save account.");
			}
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public String generateCode(long accountId) throws DataException, NoSuchAlgorithmException, NoSuchProviderException
	{	
		String code = AccountUtil.generateCode(account.getConsumerAcctCd(), 8);
		account.setConsumerAcctConfirmationCd(code);
		
		save();
	
		return code;
	}
	
	public void reset()
	{
		account = null;
	}
	
	public void save() throws DataException
	{
		try
		{
			account.save();
		}
		catch (Exception e)
		{
			log.error("Error saving student account " + e.getMessage());
			throw new DataException("Could not save student account.");
		}
	}
	
	public static StudentAccount retrieveStudentAccount(long studentAcountId) throws UnknownObjectException, DataException
	{
		ConsumerAcct account = null;
		
		try
		{
			account = ConsumerAcctPeer.retrieveByPK(studentAcountId);
		}
		catch (NoRowsException e)
		{
			log.error("Student with id " + studentAcountId + " not found.");
			throw new UnknownObjectException("Student not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving student with id " + studentAcountId + " " + tmre.getMessage());
			throw new DataException("More than one student found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving student with id " + studentAcountId + " " + te.getMessage());
			throw new DataAccessException();
		}
		
		return new StudentAccount(account);
	}
	
	public static StudentAccount retrieveStudentAccount(String accountNum, long campusId) throws DataException, UnknownObjectException
	{
		List accounts = null; 
		try
		{
			accounts = ConsumerAcctPeer.retrieveByConsumerAcctCdAndLocation(accountNum, campusId);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving account for account number " + accountNum + "  with campus id " + campusId + " " + te.getMessage());
			throw new DataAccessException();
		}
		
		if (accounts.size() > 1)
		{
			log.warn("More than one account with account number " + accountNum + " associated with campus with id " + campusId);
			throw new DataException("Too Many Accounts Found.");
		}
		else if (accounts.size() == 0)
		{
			log.debug("No account with account number " + accountNum + " associated with campus with id " + campusId);
			throw new UnknownObjectException("No account found.");
		}
		else
		{
			ConsumerAcct account = (ConsumerAcct) accounts.get(0);
			return new StudentAccount(account);
		}
	}
	
	public static List retrieveAll() throws DataException
	{
		List accts = null;
		
		try
		{
			accts = ConsumerAcctPeer.retrieveByConsumerTypeName(CONSUMER_TYPE_DESC);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving all student accounts: " + te.getMessage());
			throw new DataAccessException();
		}
		
		List accounts = new ArrayList();
		
		for (Iterator i = accts.iterator(); i.hasNext();)
		{
			ConsumerAcct acct = (ConsumerAcct) i.next();
			accounts.add(new StudentAccount(acct));
		}
		
		return accounts;
	}
	
	public static List retrieveByCampusId(long campusId) throws DataException
	{
		List accts = null;
		
		try
		{
			accts = ConsumerAcctPeer.retrieveByConsumerTypeAndLocation(CONSUMER_TYPE_DESC, campusId);
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving student accounts for campus " + campusId + ": " + te.getMessage());
			throw new DataAccessException();
		}
		
		List accounts = new ArrayList();
		
		for (Iterator i = accts.iterator(); i.hasNext();)
		{
			ConsumerAcct acct = (ConsumerAcct) i.next();
			accounts.add(new StudentAccount(acct));
		}
		
		return accounts;
	}
    public Location getLocation() throws TorqueException {
        return account.getLocation();
    }
}
