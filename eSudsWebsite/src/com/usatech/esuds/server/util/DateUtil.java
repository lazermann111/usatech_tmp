/*
 * Created on Jan 13, 2004
 * Created by melissa
 *
 */
package com.usatech.esuds.server.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author melissa
 *
 * 
 */
public class DateUtil
{

	public static GregorianCalendar parseDate(String str) throws ParseException
	{
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		GregorianCalendar result = new GregorianCalendar();
		
		result.setTime(format.parse(str));
				
		return result;
	}

	public static String convertDate(Date date)
	{
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		return format.format(date);
	}
}
