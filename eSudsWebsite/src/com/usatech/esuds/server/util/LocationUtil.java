/*
 * *******************************************************************
 * 
 * File LocationUtil.java
 * 
 * Created on Apr 5, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import com.usatech.esuds.server.beans.CountryBean;
import com.usatech.esuds.server.beans.SchoolBean;
import com.usatech.esuds.server.beans.StateBean;
import com.usatech.esuds.server.beans.TimeZoneBean;
import com.usatech.esuds.server.model.School;
import com.usatech.esuds.server.model.exceptions.DataAccessException;
import com.usatech.esuds.server.model.exceptions.DataException;
import com.usatech.esuds.server.model.exceptions.UnknownObjectException;
import com.usatech.server.persistence.Country;
import com.usatech.server.persistence.CountryPeer;
import com.usatech.server.persistence.Location;
import com.usatech.server.persistence.LocationPeer;
import com.usatech.server.persistence.State;
import com.usatech.server.persistence.StatePeer;
import com.usatech.server.persistence.TimeZone;
import com.usatech.server.persistence.TimeZonePeer;


public class LocationUtil
{
	private static Log log = LogFactory.getLog(LocationUtil.class);
	
	public static List getCountries() throws DataException
	{
		List countries = null;
		
		try
		{
			countries = CountryPeer.retrieveAll();
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving countries " + te.getMessage());
			throw new DataAccessException();
		}
		
		ArrayList list = new ArrayList();
		
		for (Iterator i = countries.iterator(); i.hasNext();)
		{
			Country c = (Country) i.next();
			CountryBean bean = new CountryBean();
			try
			{
				BeanUtils.copyProperties(bean, c);
			}
			catch (IllegalAccessException e)
			{
				log.error("Error copying properties " + e.getMessage());
			}
			catch (InvocationTargetException e)
			{
				log.error("Error copying properties " + e.getMessage());
			}
			
			list.add(bean);
		}
		
		return list;
	}
	
	public static List getStates() throws DataException
	{
		List states = null;
		
		try
		{
			states = StatePeer.retrieveAll();
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving states " + te.getMessage());
			throw new DataAccessException();
		}
		
		ArrayList list = new ArrayList();
		
		for (Iterator i = states.iterator(); i.hasNext();)
		{
			State state = (State) i.next();
			StateBean bean = new StateBean();
			try
			{
				BeanUtils.copyProperties(bean, state);
			}
			catch (IllegalAccessException e)
			{
				log.error("Error copying properties " + e.getMessage());
			}
			catch (InvocationTargetException e)
			{
				log.error("Error copying properties " + e.getMessage());
			}
			
			list.add(bean);
		}
		
		return list;
	}
	
	public static List getTimeZones() throws DataException
	{
		List timeZones = null;
		
		try
		{
			timeZones = TimeZonePeer.retrieveAll();
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving states " + te.getMessage());
			throw new DataAccessException();
		}
		
		ArrayList list = new ArrayList();
		
		for (Iterator i = timeZones.iterator(); i.hasNext();)
		{
			TimeZone timeZone = (TimeZone) i.next();
			TimeZoneBean bean = new TimeZoneBean();
			try
			{
				BeanUtils.copyProperties(bean, timeZone);
			}
			catch (IllegalAccessException e)
			{
				log.error("Error copying properties " + e.getMessage());
			}
			catch (InvocationTargetException e)
			{
				log.error("Error copying properties " + e.getMessage());
			}
			
			list.add(bean);
		}
		
		return list;
	}

	/**
	 * @return
	 */
	public static List getSchools() throws DataException
	{
		List schools = School.retrieveAll();
		try
		{
			return BeanUtil.convertList(schools, SchoolBean.class);
		}
		catch (InstantiationException e)
		{
			log.error("Error copying properties " + e.getMessage());
			return null;
		}
		catch (IllegalAccessException e)
		{
			log.error("Error copying properties " + e.getMessage());
			return null;
		}
		catch (InvocationTargetException e)
		{
			log.error("Error copying properties " + e.getMessage());
			return null;
		}
	}
	
    /**
     * @return
     */
    public static List getSchoolsByOperatorUser(long userId) throws DataException
    {
        List schools = School.retrieveByOperatorUser(userId);
        try
        {
            return BeanUtil.convertList(schools, SchoolBean.class);
        }
        catch (InstantiationException e)
        {
            log.error("Error copying properties " + e.getMessage(), e);
            return null;
        }
        catch (IllegalAccessException e)
        {
            log.error("Error copying properties " + e.getMessage(), e);
            return null;
        }
        catch (InvocationTargetException e)
        {
            log.error("Error copying properties " + e.getMessage(), e);
            return null;
        }
    }
    
	public static String getLocationType(long locationId) throws DataException, UnknownObjectException
	{
		Location location = null;
		
		try
		{
			location = LocationPeer.retrieveByPK(locationId);
		}
		catch (NoRowsException e)
		{
			log.error("Location with id " + locationId + " not found.");
			throw new UnknownObjectException("Location not found.");
		}
		catch (TooManyRowsException tmre)
		{
			log.error("Error retrieving location with id " + locationId + ": " + tmre.getMessage());
			throw new DataException("More than one location found.");
		}
		catch (TorqueException te)
		{
			log.error("Error retrieving location with id " + locationId + ": " + te.getMessage());
			throw new DataAccessException();
		}
		
		if (location != null)
		{
			try
			{
				return location.getLocationType().getLocationTypeDesc();
			}
			catch (TorqueException te)
			{
				log.error("Error getting location type: " + te);
				throw new DataException("Could not get Location Type");
			}
		}
		else
		{
			throw new DataAccessException();
		}
		 
	}
}
