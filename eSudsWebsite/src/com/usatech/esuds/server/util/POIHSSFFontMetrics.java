/*
 * Created on Aug 2, 2005
 *
 */
package com.usatech.esuds.server.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.FontDetails;

public class POIHSSFFontMetrics {
    private static Properties fontMetricsProps;

    private static Map fontDetailsMap = new HashMap();

    /** Calculates the width of a given string in units of a standard character
     * 
     */
    public static float getStringWidth(String text, String fontName, int fontPoint, boolean bold) {
        FontDetails fd = getFontDetails(fontName);
        float fudge = (bold ? 5.4f : 5.8f); /*a fudge factor*/
        return (fd.getStringWidth(text) * (fontPoint / 10.0f)) / fudge ;
    }
    
    /**
     * Retrieves the fake font details for a given font.
     * 
     * @param font
     *            the font to lookup.
     * @return the fake font.
     */
    public static FontDetails getFontDetails(String fontName) {
        if (fontMetricsProps == null) {
            InputStream metricsIn = null;
            try {
                fontMetricsProps = new Properties();
                metricsIn = FontDetails.class.getResourceAsStream("/font_metrics.properties");
                if (metricsIn == null)
                    throw new FileNotFoundException("font_metrics.properties not found in classpath");
                fontMetricsProps.load(metricsIn);
            } catch (IOException e) {
                throw new RuntimeException("Could not load font metrics: " + e.getMessage());
            } finally {
                if (metricsIn != null) {
                    try {
                        metricsIn.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }

        if (fontDetailsMap.get(fontName) == null) {
            FontDetails fontDetails = FontDetails.create(fontName, fontMetricsProps);
            fontDetailsMap.put(fontName, fontDetails);
            return fontDetails;
        } else {
            return (FontDetails) fontDetailsMap.get(fontName);
        }
    }

}
