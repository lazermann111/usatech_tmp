/*
 * *******************************************************************
 *
 * File AccountUtil.java
 *
 * Created on Mar 26, 2004
 *
 * Create by melissa
 *
 * *******************************************************************
 *
 * Updates:
 *
 * *******************************************************************
 */
package com.usatech.esuds.server.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;

import com.usatech.esuds.server.model.exceptions.ProcessingException;
import com.usatech.server.persistence.EmailQueuePeer;

import simple.app.DialectResolver;
import simple.security.SecurityUtils;

public class AccountUtil {
    private static Log log = LogFactory.getLog(AccountUtil.class);

    private static final String CHARACTER_DELIMITER = "-";

    private static final String TICKET_DELIMITER = "/";

    public static String generateCode(String k, int size) throws NoSuchAlgorithmException, NoSuchProviderException {
        // fill out to size
        if (k.length() < size) {
            while (size > k.length()) {
                k = k + k.charAt(k.length() / 2);
            }
        }

        byte[] b = k.getBytes();
        String key = "";
        SecureRandom random = SecurityUtils.getSecureRandom();

        for (int i = 0; i < b.length; i++) {
            byte r = (byte) (size * random.nextDouble());
            key = key + (char) (b[i] + r);
        }

        return key;
    }

    public static String generateTicket(long accountId, String code) {
        String ticket = "";
        String all = accountId + TICKET_DELIMITER + code;
        char[] a = all.toCharArray();

        for (int i = 0; i < a.length; i++) {
            ticket = ticket + (int) a[i] + CHARACTER_DELIMITER;
        }

        log.debug("Generated ticket is " + ticket);
        return ticket;
    }

    public static Hashtable decodeTicket(String ticket) {
        Hashtable params = new Hashtable();
        String all = "";

        StringTokenizer chars = new StringTokenizer(ticket, CHARACTER_DELIMITER);

        while (chars.hasMoreTokens()) {
            all = all + ((char) (Integer.parseInt(chars.nextToken())));
        }

        StringTokenizer tokenizer = new StringTokenizer(all, TICKET_DELIMITER);

        long accountId = Long.parseLong(tokenizer.nextToken());
        log.debug("parsed accountId = " + accountId);
        params.put("accountId", new Long(accountId));
        String code = tokenizer.nextToken();
        log.debug("parsed code = " + code);
        params.put("code", code);

        return params;
    }

    public static void sendConfirmEmail(String name, String email, String ticket, String server) {
        String url = server + "/confirmUser.do?ticket=" + ticket;
        String msg = "Thank you for registering with eSuds.net. " + "Please go to " + url
                + " to complete your registration.";

        log.debug("Message is " + msg);

        try {
            EmailQueuePeer.createAndSendEmail(email, name, "admin@esuds.net", "eSuds Administrator",
                    "Confirm Account", msg);
        } catch (TorqueException te) {
            log.error("Error sending e-mail.", te);
        } catch (Exception e) {
            log.error("Unknown error sending e-mail.", e);
        }
    }

    public static BigDecimal getTotalAmount(List trans, boolean forStudent) {
        BigDecimal total = new BigDecimal(0).setScale(2);
        for (Iterator i = trans.iterator(); i.hasNext();) {
            BaseObject tran = (BaseObject)i.next();
            BigDecimal amt = (BigDecimal)tran.getByName("TranSettlementAmount");
            Long authServiceType = (Long)tran.getByName("AuthServiceType");
            if(amt != null && (forStudent ? showAmountToStudent(authServiceType.longValue()) : showAmountToOperator(authServiceType.longValue())))
                total = total.add(amt);
        }

        return total;
    }

    private static class ExcelRowIterator implements Iterator {
        private HSSFSheet sheet;

        private int rowIndex = 0;

        public ExcelRowIterator(HSSFSheet _sheet) {
            sheet = _sheet;
        }

        public boolean hasNext() {
            return rowIndex <= sheet.getLastRowNum();
        }

        public Object next() {
            return new ExcelCellIterator(sheet.getRow(rowIndex++));
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class ExcelCellIterator implements Iterator {
        private HSSFRow row;

        private short columnIndex = 0;

        public ExcelCellIterator(HSSFRow _row) {
            row = _row;
            columnIndex = row.getFirstCellNum();
        }

        public boolean hasNext() {
            return columnIndex <= row.getLastCellNum(); //row.getCell(columnIndex) != null;
        }

        public Object next() {
            HSSFCell cell = row.getCell(columnIndex++);
            if(cell == null) return null;
            Object value = null;
            switch (cell.getCellType()) {
            case HSSFCell.CELL_TYPE_BOOLEAN:
                value = new Boolean(cell.getBooleanCellValue());
                break;
            case HSSFCell.CELL_TYPE_NUMERIC:
                value = new BigDecimal(cell.getNumericCellValue());
                break;
            case HSSFCell.CELL_TYPE_STRING:
                value = cell.getStringCellValue();
                break;
            /*
             * case HSSFCell.CELL_TYPE_Date: value = cell.getDateCellValue();
             * break;
             */
            }
            return value;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class CSVRowIterator implements Iterator {
        private BufferedReader br;

        private String next;

        public CSVRowIterator(java.io.InputStream in) throws IOException {
            br = new BufferedReader(new InputStreamReader(in));
            next = br.readLine();
        }

        public boolean hasNext() {
            return next != null;
        }

        public Object next() {
            Iterator iter = new CSVCellIterator(next);
            try {
                next = br.readLine();
            } catch (IOException ioe) {
                throw new RuntimeException(ioe.getMessage());
            }
            return iter;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class CSVCellIterator implements Iterator {
        private String[] values;

        private short columnIndex = 0;

        public CSVCellIterator(String _line) {
            values = split(_line, ',', '"');
        }

        public boolean hasNext() {
            return values.length > columnIndex;
        }

        public Object next() {
            return values[columnIndex++];
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Replaces each occurence of the specified String with the given String.
     *
     * @param orig
     *            The string to search
     * @param find
     *            The string to find
     * @param replace
     *            The string to insert at each occurance of the <CODE>find
     *            </CODE> parameter
     * @return The modified result
     */
    public static String replace(String orig, String find, String replace) {
        StringBuffer result = new StringBuffer();
        int last = 0;
        while (true) {
            int next = orig.indexOf(find, last);
            if (next < 0)
                break;
            result.append(orig.substring(last, next));
            result.append(replace);
            last = next + find.length();
        }
        result.append(orig.substring(last));
        return result.toString();
    }

    /**
     * Splits the given String into an array of Strings using the given
     * delimiter but ignoring the delimiter if it is between the given quote
     * char.
     *
     * @param original
     * @param delimiter
     * @param quote
     * @return
     */
    public static String[] split(String original, char delimiter, char quote) {
        List l = new ArrayList();
        for (int start = 0, end = 0; start < original.length(); start = end + 1) {
            String word = null;
            if (original.charAt(start) == quote) {
                start++;
                for (end = start; end < original.length(); end++) {
                    if (original.charAt(end) == quote)
                        if (end + 1 == original.length())
                            continue;
                        else if (original.charAt(end + 1) == quote)
                            end++;
                        else if (original.charAt(end + 1) == delimiter) {
                            word = replace(original.substring(start, end), "" + quote + quote, "" + quote);
                            end++;
                            break;
                        } else {
                            int pos = end;
                            end = original.indexOf(delimiter, end + 1);
                            if (end < 0)
                                end = original.length();
                            word = replace(original.substring(start, pos), "" + quote + quote, "" + quote)
                                    + original.substring(pos, end);
                            break;
                        }
                }
                if (word == null)
                    word = replace(original.substring(start, end), "" + quote + quote, "" + quote);
            } else {
                end = original.indexOf(delimiter, start);
                if (end < 0)
                    end = original.length();
                word = original.substring(start, end);
            }
            l.add(word);
            if (end == original.length() - 1)
                l.add(""); // delimiter is last character so add an empty word
        }
        String[] retVal = (String[]) l.toArray(new String[l.size()]);
        return retVal;
    }

    /**
     * Uploads student account data from an Excel spreadsheet into the database.
     * Returns the number of accounts processed. The Excel spreadsheet should
     * have the following columns at the top with each row below containing the
     * appropriate information: RECORD_ACTION_CODE STUDENT_EMAIL_ADDR
     * STUDENT_ACCOUNT_CD STUDENT_ACCOUNT_BALANCE STUDENT_FNAME STUDENT_LNAME
     * STUDENT_ADDR1 STUDENT_ADDR2 STUDENT_CITY STUDENT_STATE_CD
     * STUDENT_POSTAL_CD
     *
     * @author bkrug
     *
     */
    public static int uploadAccounts(long locationId, long appUserId, java.io.InputStream dataStream, String contentType,
            java.sql.Connection conn) throws IOException, SQLException, ProcessingException {
        log.debug("ContentType is " + contentType);
        Iterator rowIter;
        //      must buffer dataStream so we can reset on error
        BufferedInputStream bif = new BufferedInputStream(dataStream);
        bif.mark(2050);
        try {
            HSSFWorkbook book = new HSSFWorkbook(bif);
            HSSFSheet sheet = book.getSheetAt(0);
            rowIter = new ExcelRowIterator(sheet);
        } catch (IOException ioe) {
            bif.reset();
            rowIter = new CSVRowIterator(bif);
        }
        List columnNames = new ArrayList();
        Map columnValues = new HashMap();
        boolean isHeader = false;
        boolean processRow = false;
        CallableStatement cs = conn
                .prepareCall("BEGIN PKG_CONSUMER_MAINT.sp_add_consumer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;");
        int cnt = 0;
        int ignored = 0;
        try {
            //get all the values for this row
            for (int i = 0; rowIter.hasNext(); i++) {
                try {
                    Iterator cellIter = (Iterator) rowIter.next();
                    for (int k = 0; cellIter.hasNext(); k++) {
                        Object value = cellIter.next();
                        if(value != null && value.toString().trim().length() > 0) {
                            if (columnNames.size() == 0) {
                                isHeader = true;
                            }
                            if (isHeader) {
                                while (columnNames.size() < k)
                                    columnNames.add(null);
                                value = value.toString().trim().toUpperCase();
                                columnNames.add(k, value);
                            } else if (k < columnNames.size()) {
                                processRow = true;
                                columnValues.put(columnNames.get(k), value);
                            }
                        }
                    }
                    //insert into db
                    if (processRow) {
                        log.debug("Processing row with values=" + columnValues);
                        //validations
                        String rac = asString(columnValues.get("RECORD_ACTION_CODE"));
                        boolean ignoreRow = false;
                        if(rac == null) {
                            throw new ProcessingException("An 'RECORD_ACTION_CODE' was not provided for row #"
                                    + (i + 1)
                                    + ". Please specify 'D' to Deactivate, 'C' to Create, or 'U' to Update this row. "
                                    + cnt
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                            i + 1, cnt, null);
                        } else {
                            switch(rac.trim().charAt(0)) {
                                case 'D': case 'd': rac = "N"; break;
                                case 'U': case 'u': rac = "Y"; break;
                                case 'C': case 'c': rac = "Y"; break;
                                case 'N': case 'n': ignoreRow = true; break;
                                default: throw new ProcessingException("An invalid 'RECORD_ACTION_CODE' of '" + rac + "' was provided for row #"
                                    + (i + 1)
                                    + ". Please specify 'D' to Deactivate, 'C' to Create, or 'U' to Update this row. "
                                    + cnt
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                                    i + 1, cnt, null);
                            }
                        }
                        if(ignoreRow) {
                            ignored++;
                        } else {
	                        String acctCd = asString(columnValues.get("STUDENT_ACCOUNT_CD"));
	                        if(acctCd == null || acctCd.trim().length() == 0) {
	                            throw new ProcessingException("A 'STUDENT_ACCOUNT_CD' was not provided for row #"
	                                    + (i + 1)
	                                    + ". Please include the 'STUDENT_ACCOUNT_CD' column with the appropriate value for each row. "
	                                    + cnt
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    i + 1, cnt, null);
	                        }
	                        String email = asString(columnValues.get("STUDENT_EMAIL_ADDR"));
	                        if(email == null || email.trim().length() == 0) {
	                            throw new ProcessingException("A 'STUDENT_EMAIL_ADDR' was not provided for row #"
	                                    + (i + 1)
	                                    + ". Please include the 'STUDENT_EMAIL_ADDR' column with the appropriate value for each row. "
	                                    + cnt
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    i + 1, cnt, null);
	                        }
	                        BigDecimal balance;
	                        try {
	                            Object tmp = columnValues.get("STUDENT_ACCOUNT_BALANCE");
	                            if(tmp instanceof BigDecimal)
	                                balance = (BigDecimal) tmp;
	                            else if(tmp == null || tmp.toString().trim().length() == 0)
	                                balance = null;
	                            else
	                                balance = new BigDecimal(tmp.toString().trim());
	                        } catch(NumberFormatException nfe) {
	                            throw new ProcessingException("The value for 'STUDENT_ACCOUNT_BALANCE' in row #"
	                                    + (i + 1)
	                                    + " cannot be interpreted as a number. Please change it to a number or leave it blank. "
	                                    + cnt
	                                    + " account(s) were processed before this error. "
	                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
	                                    i + 1, cnt, nfe);
	                        }
	                        cs.setString(1, asString(columnValues.get("STUDENT_FNAME")));//pv_consumer_fname
	                        cs.setString(2, asString(columnValues.get("STUDENT_LNAME")));//pv_consumer_lname
	                        cs.setString(3, asString(columnValues.get("STUDENT_ADDR1")));//pv_consumer_addr1
	                        cs.setString(4, asString(columnValues.get("STUDENT_ADDR2")));//pv_consumer_addr2
	                        cs.setString(5, asString(columnValues.get("STUDENT_CITY")));//pv_consumer_city
	                        cs.setString(6, asString(columnValues.get("STUDENT_STATE_CD")));//pv_consumer_state
	                        cs.setString(7, asString(columnValues.get("STUDENT_POSTAL_CD")));//pv_consumer_postal_cd
	                        cs.setString(8, email);//pv_consumer_email_addr1
	                        cs.setInt(9, 1);//pn_consumer_type_id
	                        cs.setLong(10, locationId);//pn_location_id
	                        cs.setString(11, acctCd);//pv_consumer_account_cd
	                        cs.setString(12, rac);//pv_consumer_accnt_actv_yn_flag
	                        cs.setBigDecimal(13, balance);//pv_consumer_account_balance
	                        cs.registerOutParameter(14, Types.NUMERIC);//pn_return_code
	                        cs.registerOutParameter(15, Types.VARCHAR);//pv_error_message
	                        cs.execute();
	                        int errCode = cs.getInt(14);
	                        String errMsg = cs.getString(15);
	                        if (errCode != 0) {
	                            throw new SQLException(errMsg, null, errCode);
	                        }
	                        conn.commit();
	                        cnt++;
	                        cs.clearParameters();
                        }
                    }
                    //clear values
                    columnValues.clear();
                    isHeader = false;
                    processRow = false;
                } catch (SQLException e) {
                    conn.rollback();
                    log.warn("While processing row #" + (i + 1), e);
                    logAccountUpload(locationId, appUserId, cnt, ignored, i+1, e.getMessage(), conn);
                    throw new ProcessingException(
                            "An error occurred while processing row #"
                                    + (i + 1)
                                    + ". "
                                    + cnt
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                            i + 1, cnt, e);
                } catch (Exception e) {
                    conn.rollback();
                    log.warn("While processing row #" + (i + 1), e);
                    logAccountUpload(locationId, appUserId, cnt, ignored, i+1, e.getMessage(), conn);
                    throw new ProcessingException(
                            "An error occurred while processing row #"
                                    + (i + 1)
                                    + ". "
                                    + cnt
                                    + " account(s) were processed before this error. "
                                    + "Please correct the problem and resubmit or contact USA Technologies for assistance.",
                            i + 1, cnt, e);
                }
            }
        } finally {
            cs.close();
        }
        logAccountUpload(locationId, appUserId, cnt, ignored, 0, null, conn);
        return cnt;
    }

    public static void logAccountUpload(long locationId, long appUserId, int processedCnt, int ignoredCnt, int errorRow, String errorMsg, java.sql.Connection conn){
        PreparedStatement ps;
        try {
            if (DialectResolver.isOracle()) 
            	ps = conn.prepareStatement("BEGIN PKG_CONSUMER_MAINT.SP_LOG_UPLOAD(?,?,?,?,?,?,?); END;");
            else
            	ps = conn.prepareStatement("SELECT PKG_CONSUMER_MAINT.SP_LOG_UPLOAD(?,?,?,?,?,?,?);");
            try {
                ps.setLong(1, locationId);
                ps.setLong(2, appUserId);
                ps.setInt(3, processedCnt);
                ps.setInt(4, ignoredCnt);
                if(errorRow == 0 && errorMsg == null) {
                    ps.setInt(5, 0); //errorCnt
                    ps.setNull(6, Types.NUMERIC);
                    ps.setNull(7, Types.VARCHAR);
                } else {
                    ps.setInt(5, 1); //errorCnt
                    ps.setInt(6, errorRow);
                    ps.setString(7, errorMsg);
                }
                ps.execute();
                conn.commit();
            } finally {
                ps.close();
            }
        } catch (SQLException e) {
            log.error("Could not log account upload", e);
        }
    }

    protected static final String[] ACCOUNTS_SHEET_HEADERS = new String[] {
        "RECORD_ACTION_CODE",
        "STUDENT_EMAIL_ADDR",
        "STUDENT_ACCOUNT_CD",
        "STUDENT_ACCOUNT_BALANCE",
        "STUDENT_FNAME",
        "STUDENT_LNAME",
        "STUDENT_ADDR1",
        "STUDENT_ADDR2",
        "STUDENT_CITY",
        "STUDENT_STATE_CD",
        "STUDENT_POSTAL_CD",
        "AUTO_NOTIFY_ON",
        "ACCOUNT_TYPE"};
    /**
     * Outputs student account data into an Excel spreadsheet from the database.
     * Returns the number of accounts found. The Excel spreadsheet will have the following columns at the top with each row below containing the
     * appropriate information: RECORD_ACTION_CODE STUDENT_EMAIL_ADDR
     * STUDENT_ACCOUNT_CD STUDENT_ACCOUNT_BALANCE STUDENT_FNAME STUDENT_LNAME
     * STUDENT_ADDR1 STUDENT_ADDR2 STUDENT_CITY STUDENT_STATE_CD
     * STUDENT_POSTAL_CD
     *
     * @author bkrug
     *
     */
    public static int outputAccountsAsExcel(long locationId, java.io.OutputStream dataStream, java.sql.Connection conn) throws IOException, SQLException {
        HSSFWorkbook book = new HSSFWorkbook();
        HSSFSheet sheet = book.createSheet();
        HSSFRow row = sheet.createRow(0);
        HSSFCellStyle style = book.createCellStyle();
        HSSFFont font = book.createFont();
        font.setBoldweight((short)700);
        style.setFont(font);
        style.setFillForegroundColor((short)22);
        style.setFillPattern((short)1);
        style.setAlignment((short)2);

        style.setBorderBottom((short)1);
        style.setBorderTop((short)1);
        style.setBorderRight((short)1);
        style.setBorderLeft((short)1);
        style.setBottomBorderColor((short)8);
        style.setTopBorderColor((short)8);
        style.setRightBorderColor((short)8);
        style.setLeftBorderColor((short)8);
        float[] widths = new float[ACCOUNTS_SHEET_HEADERS.length];
        for(int i = 0; i < ACCOUNTS_SHEET_HEADERS.length; i++) {
            HSSFCell cell = row.createCell((short)i);
            cell.setCellValue(ACCOUNTS_SHEET_HEADERS[i]);
            cell.setCellStyle(style);
            widths[i] = Math.max(widths[i], calcCellWidth(book, sheet, cell));
        }
        int cnt = 0;
        HSSFCellStyle textStyle = book.createCellStyle();
        textStyle.setDataFormat((short)0x31); // "Text"
        HSSFCellStyle moneyStyle = book.createCellStyle();
        moneyStyle.setDataFormat((short)8); //"($#,##0.00_);[Red]($#,##0.00)"
        HSSFCellStyle[] styles = new HSSFCellStyle[] {
                textStyle,
                textStyle,
                textStyle,
                moneyStyle,
                textStyle,
                textStyle,
                textStyle,
                textStyle,
                textStyle,
                textStyle,
                textStyle,
                textStyle,
                textStyle
        };

        PreparedStatement ps = conn
                .prepareCall("SELECT 'N', STUDENT_EMAIL_ADDR, STUDENT_ACCOUNT_CD, " +
                		"STUDENT_ACCOUNT_BALANCE, STUDENT_FNAME, STUDENT_LNAME, STUDENT_ADDR1, " +
                		"STUDENT_ADDR2, STUDENT_CITY, STUDENT_STATE_CD, STUDENT_POSTAL_CD, CYCLE_COMPLETE_NOTIFY_ON, ACCOUNT_TYPE " +
                		"FROM VW_STUDENT_ACCOUNT " +
                		"WHERE LOCATION_ID = ? " +
                		"ORDER BY UPPER(STUDENT_EMAIL_ADDR)");
        try {
	        ps.setLong(1, locationId);
	        ResultSet rs = ps.executeQuery();
            while(rs.next()) {
	            row = sheet.createRow(sheet.getLastRowNum()+1);
	            for(short i = 0; i < ACCOUNTS_SHEET_HEADERS.length; i++) {
	                HSSFCell cell = row.createCell(i);
	                Object value = rs.getObject(i+1); //results are 1-based
	                if(value instanceof Boolean) cell.setCellValue(((Boolean)value).booleanValue());
	                else if(value instanceof java.util.Calendar) cell.setCellValue((java.util.Calendar)value);
	                else if(value instanceof java.util.Date) cell.setCellValue((java.util.Date)value);
	                else if(value instanceof Number) cell.setCellValue(((Number)value).doubleValue());
	                else if(value != null) cell.setCellValue(value.toString());
                    cell.setCellStyle(styles[i]);
                    widths[i] = Math.max(widths[i], calcCellWidth(book, sheet, cell));
	            }
                cnt++;
	        }
            //add extra formatted rows
            for(int k = 0; k < 10; k++) {
                row = sheet.createRow(sheet.getLastRowNum()+1);
                for(short i = 0; i < ACCOUNTS_SHEET_HEADERS.length; i++) {
                    row.createCell(i).setCellStyle(styles[i]);
                }
            }
            //set widths
            for(short i = 0; i < ACCOUNTS_SHEET_HEADERS.length; i++) {
                sheet.setColumnWidth(i, (short)(256 * widths[i]));
            }
            log.debug("Created report with " + cnt + " rows");
        } finally {
            ps.close();
        }
        book.write(dataStream);
        return cnt;
    }

    private static float calcCellWidth(HSSFWorkbook book, HSSFSheet sheet, HSSFCell cell) {
        //String format = book.createDataFormat().getFormat(cell.getCellStyle().getDataFormat());
        String text;
        switch(cell.getCellType()) {
            case HSSFCell.CELL_TYPE_BLANK:
                return 0;
            case HSSFCell.CELL_TYPE_BOOLEAN:
                text = "" + cell.getBooleanCellValue();
                break;
            case HSSFCell.CELL_TYPE_ERROR:
                return 10; //XXX: Not sure what to use here
            case HSSFCell.CELL_TYPE_FORMULA:
                return 30; //XXX: Not sure what to use here
            case HSSFCell.CELL_TYPE_NUMERIC:
                text = "" + cell.getNumericCellValue();
                break;
            case HSSFCell.CELL_TYPE_STRING:
                text = cell.getStringCellValue();
                break;
            default:
                return 10;
        }
        // XXX: real limited: just report the length of the text value and hope it's a good estimate
        HSSFFont font = book.getFontAt(cell.getCellStyle().getFontIndex());
        float width = POIHSSFFontMetrics.getStringWidth(text, font.getFontName(), font.getFontHeightInPoints(), font.getBoldweight() == HSSFFont.BOLDWEIGHT_BOLD);
        //log.debug("Calculated width of '" + text + "' for " + font.getFontName() + "- " + font.getFontHeightInPoints() + " pt = " + width);
        return width;
        /*
        //  XXX: I'm going to hack this for now. I'll just count the "extra" characters
        //in the format and add that to the text length. However, for dates, I need to be smarter.
        int n = 0;
        if(!format.equalsIgnoreCase("GENERAL")) {
            if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {

            } else {
                int index = 0;
                for(int i = 0; i < format.length(); i++) {
                    char ch = format.charAt(i);
                    switch(ch) {
                        case '@': case '#':
                            //do nothing
                            break;
                        case '0': case '?':
                            parts[index]++;
                            break;
                        case '_': case '.':
                            n++;
                            break;
                        case ';': case '[':
                            //done
                            i = format.length();
                            break;
                        default:
                            n++;

                    }
                }
            }
        }
        return 0;*/
    }

    /**
     * Outputs student account data into an Excel spreadsheet from the database.
     * Returns the number of accounts found. The Excel spreadsheet will have the following columns at the top with each row below containing the
     * appropriate information: RECORD_ACTION_CODE STUDENT_EMAIL_ADDR
     * STUDENT_ACCOUNT_CD STUDENT_ACCOUNT_BALANCE STUDENT_FNAME STUDENT_LNAME
     * STUDENT_ADDR1 STUDENT_ADDR2 STUDENT_CITY STUDENT_STATE_CD
     * STUDENT_POSTAL_CD
     *
     * @author bkrug
     *
     */
    public static int outputAccountsAsCSV(long locationId, java.io.OutputStream dataStream, java.sql.Connection conn) throws IOException, SQLException {
        PrintWriter pw = new PrintWriter(dataStream);
        printCSVLine(ACCOUNTS_SHEET_HEADERS, pw);
        int cnt = 0;

        PreparedStatement ps = conn
                .prepareCall("SELECT 'N', STUDENT_EMAIL_ADDR, STUDENT_ACCOUNT_CD, " +
                        "STUDENT_ACCOUNT_BALANCE, STUDENT_FNAME, STUDENT_LNAME, STUDENT_ADDR1, " +
                        "STUDENT_ADDR2, STUDENT_CITY, STUDENT_STATE_CD, STUDENT_POSTAL_CD, CYCLE_COMPLETE_NOTIFY_ON, ACCOUNT_TYPE " +
                        "FROM VW_STUDENT_ACCOUNT " +
                        "WHERE LOCATION_ID = ? " +
                        "ORDER BY UPPER(STUDENT_EMAIL_ADDR)");
        try {
            ps.setLong(1, locationId);
            ResultSet rs = ps.executeQuery();
            Object[] data = new Object[ACCOUNTS_SHEET_HEADERS.length];
            while(rs.next()) {
                for(int i = 0; i < data.length; i++) {
                    data[i] = rs.getObject(i+1);
                }
                printCSVLine(data, pw);
                cnt++;
            }
            log.debug("Created report with " + cnt + " rows");
        } finally {
            ps.close();
        }
        pw.flush();
        return cnt;
    }

    protected static void printCSVLine(Object[] data, PrintWriter out) {
        for(int i = 0; i < data.length; i++) {
            if(i > 0) out.print(',');
            if(data[i] instanceof java.util.Date
                || data[i] instanceof Number
                || data[i] instanceof Boolean) out.print(data[i]);
            else if(data[i] != null) out.print("\"" + replace(data[i].toString(), "\"", "\"\"") + "\"");
        }
        out.println();
    }

    protected static String asString(Object obj) {
        if(obj == null) return null;
        else return obj.toString().trim();
    }

    //  We will (eek!) hard code the auth service types that count toward total for now
    public static boolean showAmountToStudent(long authServiceType) {
        switch((int)authServiceType) {
        	case 0: case 1: return true;
        	default: return false;
        }
    }

    //  We will (eek!) hard code the auth service types that count toward total for now
    public static boolean showAmountToOperator(long authServiceType) {
        switch((int)authServiceType) {
	    	case 0: case 1: case 4: return true;
	    	default: return false;
	    }
    }
}