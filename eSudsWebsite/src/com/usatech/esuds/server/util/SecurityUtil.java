/*
 * *******************************************************************
 * 
 * File SecurityUtil.java
 * 
 * Created on Jun 11, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.util;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.stream.DoubleStream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import simple.security.SecurityUtils;

/**
 * @author melissa
 * 
 */
public class SecurityUtil {
    public static class DomainSubdomain {
        public String domain;

        public String subdomain;
    }

    private static char[] ALPHA_NUM = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    public static String generateToken(String k, int size) throws NoSuchAlgorithmException, NoSuchProviderException {
        // fill out to size
        if (k.length() < size) {
            while (size > k.length()) {
                k = k + k.charAt(k.length() / 2);
            }
        } else if (k.length() > size) {
            k = k.substring(k.length() - size);
        }

        byte[] b = k.getBytes();
        char[] key = new char[b.length];
        SecureRandom random = SecurityUtils.getSecureRandom();

        for (int i = 0; i < b.length; i++) {
            byte r = (byte) (size * random.nextDouble());
            //key = key + (char) (b[i] + r);
            //fixed by BSK 8-11-2006 to contain only alpha numerics
            key[i] = ALPHA_NUM[(b[i] + r) % ALPHA_NUM.length];
        }

        return new String(key);
    }

    public static DomainSubdomain parseUrl(HttpServletRequest request) {
        String serverName = request.getServerName();
        int pos = serverName.indexOf('.');
        DomainSubdomain ds = new DomainSubdomain();
        if(pos < 0) {
            ds.subdomain = " ";
            ds.domain = serverName.toLowerCase();
        } else {
            ds.subdomain = serverName.substring(0, pos).toLowerCase();
            ds.domain = serverName.substring(pos + 1).toLowerCase();
        }
        return ds;
    }

    public static String parseSubdomain(HttpServletRequest request) {
        String serverName = request.getServerName();
        String subDomain = serverName.substring(0, serverName.indexOf("."));

        if (subDomain != null) {
            return subDomain.toLowerCase();
        } else {
            return "";
        }
    }

    public static String parseDomain(HttpServletRequest request) {
        String serverName = request.getServerName();
        String domain = serverName.substring((serverName.indexOf(".") + 1));

        if (domain != null) {
            return domain.toLowerCase();
        } else {
            return "";
        }
    }

    /**
     * @return
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     */
    public static String createTemporaryPassword() throws NoSuchAlgorithmException, NoSuchProviderException {
        StringBuilder password = new StringBuilder();
        SecureRandom random = SecurityUtils.getSecureRandom();
        for (int i = 0; i < 10; i++) {
        	password.append(ALPHA_NUM[(int) Math.floor(ALPHA_NUM.length * random.nextDouble())]);
        }
        return password.toString();
    }

    public static boolean isValidPassword(String password) {
        if (password.length() < 6 || password.length() > 10) {
            return false;
        }

        byte[] chars = password.getBytes();
        boolean foundNumber = false;
        for (int i = 0; i < chars.length; i++) {
            char c = (char) chars[i];
            if (!Character.isLetterOrDigit(c)) {
                return false;
            } else if (Character.isDigit(c)) {
                foundNumber = true;
            }
        }

        return foundNumber;
    }

    public static boolean isValidUsername(String username) {
        if (username.length() <= 6 || username.length() >= 60) {
            return false;
        }

        byte[] chars = username.getBytes();
        for (int i = 0; i < chars.length; i++) {
            Character c = new Character((char) chars[i]);
            if (!Character.isLetterOrDigit(c.charValue()) && !c.equals(new Character('_'))) {
                return false;
            }
        }

        return true;
    }

    public static Cookie getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();

        if (cookies == null) {
            return null;
        }

        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals(name)) {
                return cookies[i];
            }
        }

        return null;
    }
}
