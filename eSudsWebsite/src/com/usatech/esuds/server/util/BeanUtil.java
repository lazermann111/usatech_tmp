/*
 * *******************************************************************
 * 
 * File BeanUtils.java
 * 
 * Created on Mar 29, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.esuds.server.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

public class BeanUtil
{
	public static List convertList(List list, Class destinationClass) throws InstantiationException, IllegalAccessException, InvocationTargetException
	{
		List result = new ArrayList();
		
		for (Iterator i = list.iterator(); i.hasNext();)
		{
			Object obj =  i.next();
			Object beanObj = destinationClass.newInstance();
			BeanUtils.copyProperties(beanObj, obj);
			
			result.add(beanObj);
		}
		
		return result;
	}
	
	public static List convertList(List list, Class destinationClass, String action) throws InstantiationException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException
	{
		List result = new ArrayList();
		
		for (Iterator i = list.iterator(); i.hasNext();)
		{
			Object obj =  i.next();
			Object beanObj = destinationClass.newInstance();
			BeanUtils.copyProperties(beanObj, obj);
			Class[] args = {String.class};
			Method method = destinationClass.getMethod("setAction", args);
			Object[] actionArgs = {action};
			method.invoke(beanObj, actionArgs);
			
			result.add(beanObj);
		}
		
		return result;
	}
}
