/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.server.report.exceptions;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InvalidRowException extends Exception
{
	/**
	 * @param string
	 */
	public InvalidRowException(String string)
	{
		super(string);
	}
}
