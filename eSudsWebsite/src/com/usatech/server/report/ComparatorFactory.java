/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatech.server.report;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ComparatorFactory
{
	/**
	 * 
	 */
	private ComparatorFactory()
	{
		
	}
	
	public static Comparator getComparator(final String propertyName)
	{
		return getComparator(propertyName, ComparatorUtils.naturalComparator());
	}
	
	public static Comparator getComparator(final String propertyName, final Comparator propertyComparator)
		{
			return new Comparator() {
			
				public int compare(Object o1, Object o2)
				{
					Object field1 = null;
					Object field2 = null;
					try
					{
						field1 = BeanUtils.getProperty(o1, propertyName);
						field2 = BeanUtils.getProperty(o1, propertyName);
					}
					catch (IllegalAccessException e)
					{
						throw new RuntimeException("Can not access property " + propertyName);
					}
					catch (InvocationTargetException e)
					{
						throw new RuntimeException("Property " + propertyName + " does not exist.");
					}
					catch (NoSuchMethodException e)
					{
						throw new RuntimeException("Property " + propertyName + " does not exist.");
					}

					int result = propertyComparator.compare(field1, field2);
				
					if (result != 0)
					{
						return result;
					}
					else if (o1.equals(o2))
					{
						return 0;
					}
					else
					{
						return propertyComparator.compare(o1, o2);
					}	
				}
			};
		}
	
	
}
