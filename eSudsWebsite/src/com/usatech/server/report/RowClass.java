/*
 * *******************************************************************
 * 
 * File RowClass.java
 * 
 * Created on May 18, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.server.report;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaProperty;

/**
 * @author melissa
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RowClass extends BasicDynaClass
{
	/**
	 * 
	 */
	public RowClass()
	{
		super();
	}
	/**
	 * @param name
	 * @param dynaBeanClass
	 */
	public RowClass(String name, Class dynaBeanClass)
	{
		super(name, dynaBeanClass);
	}
	/**
	 * @param name
	 * @param dynaBeanClass
	 * @param properties
	 */
	public RowClass(
		String name,
		Class dynaBeanClass,
		DynaProperty[] properties)
	{
		super(name, dynaBeanClass, properties);
	}
	
	public static RowClass getRowClassForColumns(List columns)
	{
		DynaProperty[] properties = new DynaProperty[columns.size()];
		
		int j = 0;
		
		for (Iterator i = columns.iterator(); i.hasNext();)
		{
			Column column = (Column) i.next();
			properties[j++] = new DynaProperty(column.getPropertyName(), column.getPropertyClass());
		}
		
		return new RowClass("row", null, properties);
	}
}
