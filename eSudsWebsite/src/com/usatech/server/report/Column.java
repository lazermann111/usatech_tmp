/*
 * Created on Mar 24, 2004
 *
 */
package com.usatech.server.report;

import java.text.Format;

/**
 * @author melissa
 *
 * A column in a report
 */
public class Column
{
	private String displayName = null;
	private String propertyName = null;
	private String dbColumnName = null;
	private Class propertyClass = null;
	private Format format = null;
	private boolean hidden = false;

	public Column(String _displayName, String _dbColumnName, String _propertyName, Class _propertyClass)
	{
		displayName = _displayName;
		dbColumnName = _dbColumnName;
		propertyName = _propertyName;
		propertyClass = _propertyClass;
		format = null;
	}
	
	public Column(String _displayName, String _dbColumnName, String _propertyName, Class _propertyClass, Format _format)
	{
		displayName = _displayName;
		dbColumnName = _dbColumnName;
		propertyName = _propertyName;
		propertyClass = _propertyClass;
		format = _format;
	}

	/**
	 * @return
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @return
	 */
	public Class getPropertyClass()
	{
		return propertyClass;
	}

	/**
	 * @return
	 */
	public String getPropertyName()
	{
		return propertyName;
	}

	/**
	 * @param string
	 */
	public void setDisplayName(String string)
	{
		displayName = string;
	}

	/**
	 * @param class1
	 */
	public void setPropertyClass(Class class1)
	{
		propertyClass = class1;
	}

	/**
	 * @param string
	 */
	public void setPropertyName(String string)
	{
		propertyName = string;
	}

	/**
	 * @return
	 */
	public String getDbColumnName()
	{
		return dbColumnName;
	}

	/**
	 * @param string
	 */
	public void setDbColumnName(String string)
	{
		dbColumnName = string;
	}

	/**
	 * @return
	 */
	public boolean isHidden()
	{
		return hidden;
	}

	/**
	 * @param b
	 */
	public void setHidden(boolean b)
	{
		hidden = b;
	}

	/**
	 * @return
	 */
	public Format getFormat()
	{
		return format;
	}

	/**
	 * @param format
	 */
	public void setFormat(Format format)
	{
		this.format = format;
	}

}
