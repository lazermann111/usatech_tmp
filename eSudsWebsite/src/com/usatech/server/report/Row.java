/*
 * *******************************************************************
 * 
 * File Row.java
 * 
 * Created on Apr 28, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.server.report;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

/**
 * @author melissa
 *
 */
public class Row implements DynaBean
{
	protected DynaClass dynaClass = null;
	protected HashMap properties = null;
	
	/**
	 * 
	 */
	public Row(DynaClass dynaClass)
	{
		super();
		this.dynaClass = dynaClass;
		properties = new HashMap();
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#contains(java.lang.String, java.lang.String)
	 */
	public boolean contains(String name, String key)
	{
		Object property = properties.get(name);
		if (property == null)
		{
			throw new NullPointerException("No property named " + name);
		}
		else if (property instanceof Map)
		{
			Map map = (Map) property;
			return map.containsKey(key);				
		}
		else
		{
			throw new IllegalArgumentException(name + " is not a mapped property");
		}
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#get(java.lang.String)
	 */
	public Object get(String name)
	{
		if (name != null)
		{
			return properties.get(name);
		}
		else
		{
			throw new NullPointerException("name can not be null");
		}
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#get(java.lang.String, int)
	 */
	public Object get(String name, int index)
	{
		Object property = properties.get(name);
		if (property == null)
		{
			throw new NullPointerException("No property named " + name);
		}
		else if (property.getClass().isArray())
		{
			return (Array.get(property, index));		
		}
		else if (property instanceof List)
		{
			return ((List) property).get(index);
		}
		else
		{
			throw new IllegalArgumentException(name + " is not an indexed property");
		}
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#get(java.lang.String, java.lang.String)
	 */
	public Object get(String name, String key)
	{
		Object property = properties.get(name);
		if (property == null)
		{
			throw new NullPointerException("No property named " + name);
		}
		else if (property instanceof Map)
		{
			Map map = (Map) property;
			return map.get(key);				
		}
		else
		{
			throw new IllegalArgumentException(name + " is not a mapped property");
		}
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#getDynaClass()
	 */
	public DynaClass getDynaClass()
	{
		return dynaClass;
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#remove(java.lang.String, java.lang.String)
	 */
	public void remove(String name, String key)
	{
		Object property = properties.get(name);
		if (property == null)
		{
			throw new NullPointerException("No property named " + name);
		}
		else if (property instanceof Map)
		{
			Map map = (Map) property;
			map.remove(key);			
		}
		else
		{
			throw new IllegalArgumentException(name + " is not a mapped property");
		}
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#set(java.lang.String, java.lang.Object)
	 */
	public void set(String name, Object value)
	{
		DynaProperty descriptor = getDynaProperty(name);
		
		if (value == null && descriptor.getType().isPrimitive())
		{
			throw new NullPointerException("value can not be null");
		}
		else if (value != null && !isAssignable(value.getClass(), descriptor.getType()))
		{
			throw new ConversionException("Can not assign value of type '" +
				value.getClass().getName() + "' to property '" + name + "' of type '" +
				descriptor.getType().getName() + "'");
		}
		
		properties.put(name, value);
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#set(java.lang.String, int, java.lang.Object)
	 */
	public void set(String name, int index, Object value)
	{		
		Object property = properties.get(name);
		if (property == null)
		{
			throw new NullPointerException("No property named " + name);
		}
		else if (property.getClass().isArray())
		{
			Array.set(property, index, value);		
		}
		else if (property instanceof List)
		{
			((List) property).set(index, value);
		}
		else
		{
			throw new IllegalArgumentException(name + " is not an indexed property");
		}
	}
	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.DynaBean#set(java.lang.String, java.lang.String, java.lang.Object)
	 */
	public void set(String name, String key, Object value)
	{
		Object property = properties.get(name);
		if (property == null)
		{
			throw new NullPointerException("No property named " + name);
		}
		else if (property instanceof Map)
		{
			Map map = (Map) property;
			map.put(key, value);				
		}
		else
		{
			throw new IllegalArgumentException(name + " is not a mapped property");
		}
	}
	
	protected DynaProperty getDynaProperty(String name)
	{
		DynaProperty descriptor = getDynaClass().getDynaProperty(name);
		if (descriptor == null)
		{
			throw new IllegalArgumentException(name + " is not a property");
		}
		else
		{
			return (descriptor);
		}
	}
	
	protected boolean isAssignable(Class source, Class dest)
	{
		if (dest.isAssignableFrom(source))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
