/*
 * Created on Mar 24, 2004
 *
 */
package com.usatech.server.report;

import java.lang.reflect.InvocationTargetException;

import java.text.Format;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.server.report.exceptions.InvalidColumnException;
import com.usatech.server.report.exceptions.InvalidRowException;

/**
 * @author melissa
 *
 *  Used to display a report
 *  Note: There are no checks for duplicate columns or rows
 */
public class Report
{
	private static Log log = LogFactory.getLog(Report.class);
	
	private String title = null;
	private List rows = null;
	private List columns = null;
	private RowClass rowClass = null;
	
	public Report()
	{
		columns = new ArrayList();
		rows = new ArrayList();
	}
	
	public void addColumn(Column c)
	{
		columns.add(c);
	}
	
	public void addColumns(List _columns)
	{
		columns.addAll(_columns);
	}
	
	/**
	 * @return
	 */
	public List getColumns()
	{
		return columns;
	}
	
	public List getColumnDisplayNames()
	{
		ArrayList list = new ArrayList();
		for (Iterator i = columns.iterator(); i.hasNext();)
		{
			list.add(((Column) i.next()).getDisplayName());
		}
		
		return list;
	}
	
	public List getColumnDbColumnNames()
	{
		ArrayList list = new ArrayList();
		for (Iterator i = columns.iterator(); i.hasNext();)
		{
			list.add(((Column) i.next()).getDbColumnName());
		}
		
		return list;
	}
	
	public void addRow(Row r)
	{
		rows.add(r);
	}
	
	public void addRows(List _rows)
	{
		rows.add(_rows);
	}

	/**
	 * @return rows
	 */
	public List getRows()
	{
		return rows;
	}

	/**
	 * @param list columns
	 */
	public void setColumns(List _columns)
	{
		columns = _columns;
	}

	/**
	 * @param list rows
	 */
	public void setRows(List _rows)
	{
		rows = _rows;
	}
	
	public String getColumnDisplayName(int columnIndex)
	{
		return ((Column) columns.get(columnIndex)).getDisplayName(); 
	}
	
	public Class getColumnClass(int columnIndex)
	{
		return ((Column) columns.get(columnIndex)).getPropertyClass();
	}
	
	public int getColumnCount()
	{
		return columns.size();
	}
	
	public int getRowCount()
	{
		return rows.size();
	}
	
	public Object getValueAt(int rowIndex, int columnIndex) throws InvalidColumnException, InvalidRowException
	{
		// test that the indexes are valid
		if (rowIndex < 0 || rowIndex > rows.size())
		{
			log.error("Row index " + rowIndex + " is out of bounds");
			throw new InvalidRowException("Index for rows is out of bounds.");
		}
		if (columnIndex < 0 || columnIndex > columns.size())
		{
			log.error("Column index " + columnIndex + " is out of bounds");
			throw new InvalidColumnException("Index for columns is out of bounds.");
		}
		
		Column column = (Column) columns.get(columnIndex);
		String property = column.getPropertyName();
		Row row = (Row) rows.get(rowIndex);
		if (column.getFormat() != null)
		{
			return column.getFormat().format(row.get(property));
		}
		else
		{
			return row.get(property);
		}
	}
		
	public void sortByColumnIndex(int columnIndex) throws InvalidColumnException
	{
		if (columnIndex < 0 || columnIndex > columns.size())
		{
			log.error("Column index " + columnIndex + " is out of bounds");
			throw new InvalidColumnException("Index for columns is out of bounds.");
		}
		
		Column column = (Column) columns.get(columnIndex);
		String property = column.getPropertyName();
		
		Collections.sort(rows, ComparatorFactory.getComparator(property));
	}
	
	public void sortByColumnDisplayName(String columnDisplayName) throws InvalidColumnException
	{	
		Column column = getColumnByDisplayName(columnDisplayName);
		String property = column.getPropertyName();
		
		Collections.sort(rows, ComparatorFactory.getComparator(property));
	}
	
	public void sortByColumnPropertyName(String columnPropertyName) throws InvalidColumnException
	{	
		Column column = getColumnByPropertyName(columnPropertyName);
		String property = column.getPropertyName();
		
		Collections.sort(rows, ComparatorFactory.getComparator(property));
	}
	
	public void sortByColumnIndexes(int[] columnIndexes) throws InvalidColumnException
	{
		List comparators = new ArrayList();
		for (int i = 0; i < columnIndexes.length; i++)
		{
			int columnIndex = columnIndexes[i];
			
			if (columnIndex < 0 || columnIndex > columns.size())
			{
				log.error("Column index " + columnIndex + " is out of bounds");
				throw new InvalidColumnException("Index for columns is out of bounds.");
			}
		
			Column column = (Column) columns.get(columnIndex);
			String property = column.getPropertyName();
			comparators.add(ComparatorFactory.getComparator(property));
		}
		
		Collections.sort(rows, new ComparatorChain(comparators));
	}
	
	public void sortByColumnDisplayNames(String[] columnDisplayNames) throws InvalidColumnException
	{
		List comparators = new ArrayList();
		
		for (int i = 0; i < columnDisplayNames.length; i++)
		{
			String columnDisplayName = columnDisplayNames[i];
			Column column = getColumnByDisplayName(columnDisplayName);
			String property = column.getPropertyName();
			comparators.add(ComparatorFactory.getComparator(property));
		}
		
		Collections.sort(rows, new ComparatorChain(comparators));
	}
	
	public void sortByColumnPropertyNames(String[] columnPropertyNames) throws InvalidColumnException
	{
		List comparators = new ArrayList();
	
		for (int i = 0; i < columnPropertyNames.length; i++)
		{
			String columnPropertyName = columnPropertyNames[i];
			Column column = getColumnByPropertyName(columnPropertyName);
			String property = column.getPropertyName();
			comparators.add(ComparatorFactory.getComparator(property));
		}
		
		Collections.sort(rows, new ComparatorChain(comparators));
	}
	
	private Column getColumnByDisplayName(String name) throws InvalidColumnException
	{
		for (Iterator i = columns.iterator(); i.hasNext();)
		{
			Column column = (Column) i.next();
			if (column.getDisplayName().equals(name))
			{
				return column;
			}
		}
		throw new InvalidColumnException("No column with display name " + name);
	}
	
	private Column getColumnByPropertyName(String name) throws InvalidColumnException
	{
		for (Iterator i = columns.iterator(); i.hasNext();)
		{
			Column column = (Column) i.next();
			if (column.getPropertyName().equals(name))
			{
				return column;
			}
		}
		throw new InvalidColumnException("No column with property name " + name);
	}
	/**
	 * @return
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param _title
	 */
	public void setTitle(String _title)
	{
		title = _title;
	}

	/**
	 * @return
	 */
	public RowClass getRowClass()
	{
		return rowClass;
	}

	/**
	 * @param clazz
	 */
	public void setRowClass(RowClass clazz)
	{
		rowClass = clazz;
	}
	
	public void changeDisplayName(String currentName, String newName) throws InvalidColumnException
	{
		Column column = getColumnByDisplayName(currentName);
		column.setDisplayName(newName);
		
	}
	

}
