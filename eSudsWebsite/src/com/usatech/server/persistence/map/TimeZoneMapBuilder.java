package com.usatech.server.persistence.map;

import java.util.Date;
import java.math.BigDecimal;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.TableMap;

/**
  *  This class was autogenerated by Torque on:
  *
  * [Fri Aug 27 17:49:47 EDT 2004]
  *
  */
public class TimeZoneMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.usatech.server.persistence.map.TimeZoneMapBuilder";


    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("DB");

        dbMap.addTable("TIME_ZONE");
        TableMap tMap = dbMap.getTable("TIME_ZONE");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("TIME_ZONE_SEQ");

              tMap.addPrimaryKey("TIME_ZONE.TIME_ZONE_CD", new String());
                    tMap.addColumn("TIME_ZONE.TIME_ZONE_NAME", new String());
                    tMap.addColumn("TIME_ZONE.UTC_OFFSET", new BigDecimal(0));
                    tMap.addColumn("TIME_ZONE.EXAMPLE", new String());
          }
}
