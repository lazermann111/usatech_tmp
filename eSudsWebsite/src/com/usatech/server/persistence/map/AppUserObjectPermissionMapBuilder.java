package com.usatech.server.persistence.map;

import java.util.Date;
import java.math.BigDecimal;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.TableMap;

/**
  *  This class was autogenerated by Torque on:
  *
  * [Tue Jul 13 14:25:33 EDT 2004]
  *
  */
public class AppUserObjectPermissionMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.usatech.server.persistence.map.AppUserObjectPermissionMapBuilder";


    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("security");

        dbMap.addTable("APP_USER_OBJECT_PERMISSION");
        TableMap tMap = dbMap.getTable("APP_USER_OBJECT_PERMISSION");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("APP_USER_OBJECT_PERMISSION_SEQ");

              tMap.addForeignKey(
                "APP_USER_OBJECT_PERMISSION.APP_USER_ID", new Long(0) , "APP_USER" ,
                "APP_USER_ID");
                    tMap.addForeignKey(
                "APP_USER_OBJECT_PERMISSION.APP_ID", new Long(0) , "APP" ,
                "APP_ID");
                    tMap.addForeignKey(
                "APP_USER_OBJECT_PERMISSION.APP_OBJECT_TYPE_ID", new Long(0) , "APP_OBJECT_TYPE" ,
                "APP_OBJECT_TYPE_ID");
                    tMap.addColumn("APP_USER_OBJECT_PERMISSION.OBJECT_CD", new String());
                    tMap.addColumn("APP_USER_OBJECT_PERMISSION.ALLOW_OBJECT_CREATE_YN_FLAG", new String());
                    tMap.addColumn("APP_USER_OBJECT_PERMISSION.ALLOW_OBJECT_READ_YN_FLAG", new String());
                    tMap.addColumn("APP_USER_OBJECT_PERMISSION.ALLOW_OBJECT_MODIFY_YN_FLAG", new String());
                    tMap.addColumn("APP_USER_OBJECT_PERMISSION.ALLOW_OBJECT_DELETE_YN_FLAG", new String());
          }
}
