package com.usatech.server.persistence.map;

import java.util.Date;
import java.math.BigDecimal;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.DatabaseMap;
import org.apache.torque.map.TableMap;

/**
  *  This class was autogenerated by Torque on:
  *
  * [Fri Aug 27 17:49:47 EDT 2004]
  *
  */
public class PosPtaMapBuilder implements MapBuilder
{
    /**
     * The name of this class
     */
    public static final String CLASS_NAME =
        "com.usatech.server.persistence.map.PosPtaMapBuilder";


    /**
     * The database map.
     */
    private DatabaseMap dbMap = null;

    /**
     * Tells us if this DatabaseMapBuilder is built so that we
     * don't have to re-build it every time.
     *
     * @return true if this DatabaseMapBuilder is built
     */
    public boolean isBuilt()
    {
        return (dbMap != null);
    }

    /**
     * Gets the databasemap this map builder built.
     *
     * @return the databasemap
     */
    public DatabaseMap getDatabaseMap()
    {
        return this.dbMap;
    }

    /**
     * The doBuild() method builds the DatabaseMap
     *
     * @throws TorqueException
     */
    public void doBuild() throws TorqueException
    {
        dbMap = Torque.getDatabaseMap("DB");

        dbMap.addTable("POS_PTA");
        TableMap tMap = dbMap.getTable("POS_PTA");

        tMap.setPrimaryKeyMethod(TableMap.NATIVE);

        tMap.setPrimaryKeyMethodInfo("POS_PTA_SEQ");

              tMap.addPrimaryKey("POS_PTA.POS_PTA_ID", new Long(0));
                    tMap.addForeignKey(
                "POS_PTA.POS_ID", new Long(0) , "POS" ,
                "POS_ID");
                    tMap.addForeignKey(
                "POS_PTA.PAYMENT_SUBTYPE_ID", new Long(0) , "PAYMENT_SUBTYPE" ,
                "PAYMENT_SUBTYPE_ID");
                    tMap.addColumn("POS_PTA.POS_PTA_ENCRYPT_KEY", new String());
                    tMap.addColumn("POS_PTA.POS_PTA_ACTIVITATION_TS", new Date());
                    tMap.addColumn("POS_PTA.POS_PTA_DEACTIVATION_TS", new Date());
                    tMap.addColumn("POS_PTA.CREATED_BY", new String());
                    tMap.addColumn("POS_PTA.CREATED_TS", new Date());
                    tMap.addColumn("POS_PTA.LAST_UPDATED_BY", new String());
                    tMap.addColumn("POS_PTA.LAST_UPDATED_TS", new Date());
                    tMap.addColumn("POS_PTA.PAYMENT_SUBTYPE_KEY_ID", new Long(0));
                    tMap.addColumn("POS_PTA.POS_PTA_PIN_REQ_YN_FLAG", new String());
          }
}
