package com.usatech.server.persistence;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ComboKey;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.om.StringKey;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
  
/**
 * This class was autogenerated by Torque on:
 *
 * [Fri Aug 27 17:49:47 EDT 2004]
 *
 * You should not use this class directly.  It should not even be
 * extended all references should be to HostDiagStatus
 */
public abstract class BaseHostDiagStatus extends BaseObject
{
    /** The Peer class */
    private static final HostDiagStatusPeer peer =
        new HostDiagStatusPeer();

        
    /** The value for the hostId field */
    private long hostId;
      
    /** The value for the hostDiagCd field */
    private String hostDiagCd;
      
    /** The value for the hostDiagValue field */
    private String hostDiagValue;
      
    /** The value for the hostDiagStartTs field */
    private Date hostDiagStartTs;
      
    /** The value for the hostDiagClearTs field */
    private Date hostDiagClearTs;
      
    /** The value for the createdBy field */
    private String createdBy;
      
    /** The value for the createdTs field */
    private Date createdTs;
      
    /** The value for the lastUpdatedBy field */
    private String lastUpdatedBy;
      
    /** The value for the lastUpdatedTs field */
    private Date lastUpdatedTs;
      
    /** The value for the hostDiagStatusId field */
    private long hostDiagStatusId;
      
    /** The value for the hostDiagLastReportedTs field */
    private Date hostDiagLastReportedTs;
  
    
    /**
     * Get the HostId
     *
     * @return long
     */
    public long getHostId()
    {
        return hostId;
    }

                              
    /**
     * Set the value of HostId
     *
     * @param v new value
     */
    public void setHostId(long v) throws TorqueException
    {
    
                  if (this.hostId != v)
              {
            this.hostId = v;
            setModified(true);
        }
    
                          
                if (aHost != null && !(aHost.getHostId() == v))
                {
            aHost = null;
        }
      
              }
  
    /**
     * Get the HostDiagCd
     *
     * @return String
     */
    public String getHostDiagCd()
    {
        return hostDiagCd;
    }

                        
    /**
     * Set the value of HostDiagCd
     *
     * @param v new value
     */
    public void setHostDiagCd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.hostDiagCd, v))
              {
            this.hostDiagCd = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HostDiagValue
     *
     * @return String
     */
    public String getHostDiagValue()
    {
        return hostDiagValue;
    }

                        
    /**
     * Set the value of HostDiagValue
     *
     * @param v new value
     */
    public void setHostDiagValue(String v) 
    {
    
                  if (!ObjectUtils.equals(this.hostDiagValue, v))
              {
            this.hostDiagValue = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HostDiagStartTs
     *
     * @return Date
     */
    public Date getHostDiagStartTs()
    {
        return hostDiagStartTs;
    }

                        
    /**
     * Set the value of HostDiagStartTs
     *
     * @param v new value
     */
    public void setHostDiagStartTs(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.hostDiagStartTs, v))
              {
            this.hostDiagStartTs = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HostDiagClearTs
     *
     * @return Date
     */
    public Date getHostDiagClearTs()
    {
        return hostDiagClearTs;
    }

                        
    /**
     * Set the value of HostDiagClearTs
     *
     * @param v new value
     */
    public void setHostDiagClearTs(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.hostDiagClearTs, v))
              {
            this.hostDiagClearTs = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreatedBy
     *
     * @return String
     */
    public String getCreatedBy()
    {
        return createdBy;
    }

                        
    /**
     * Set the value of CreatedBy
     *
     * @param v new value
     */
    public void setCreatedBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.createdBy, v))
              {
            this.createdBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the CreatedTs
     *
     * @return Date
     */
    public Date getCreatedTs()
    {
        return createdTs;
    }

                        
    /**
     * Set the value of CreatedTs
     *
     * @param v new value
     */
    public void setCreatedTs(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.createdTs, v))
              {
            this.createdTs = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdatedBy
     *
     * @return String
     */
    public String getLastUpdatedBy()
    {
        return lastUpdatedBy;
    }

                        
    /**
     * Set the value of LastUpdatedBy
     *
     * @param v new value
     */
    public void setLastUpdatedBy(String v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdatedBy, v))
              {
            this.lastUpdatedBy = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the LastUpdatedTs
     *
     * @return Date
     */
    public Date getLastUpdatedTs()
    {
        return lastUpdatedTs;
    }

                        
    /**
     * Set the value of LastUpdatedTs
     *
     * @param v new value
     */
    public void setLastUpdatedTs(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.lastUpdatedTs, v))
              {
            this.lastUpdatedTs = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HostDiagStatusId
     *
     * @return long
     */
    public long getHostDiagStatusId()
    {
        return hostDiagStatusId;
    }

                        
    /**
     * Set the value of HostDiagStatusId
     *
     * @param v new value
     */
    public void setHostDiagStatusId(long v) 
    {
    
                  if (this.hostDiagStatusId != v)
              {
            this.hostDiagStatusId = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the HostDiagLastReportedTs
     *
     * @return Date
     */
    public Date getHostDiagLastReportedTs()
    {
        return hostDiagLastReportedTs;
    }

                        
    /**
     * Set the value of HostDiagLastReportedTs
     *
     * @param v new value
     */
    public void setHostDiagLastReportedTs(Date v) 
    {
    
                  if (!ObjectUtils.equals(this.hostDiagLastReportedTs, v))
              {
            this.hostDiagLastReportedTs = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private Host aHost;

    /**
     * Declares an association between this object and a Host object
     *
     * @param v Host
     * @throws TorqueException
     */
    public void setHost(Host v) throws TorqueException
    {
            if (v == null)
        {
                    setHostId(0);
                  }
        else
        {
            setHostId(v.getHostId());
        }
                aHost = v;
    }

                                            
    /**
     * Get the associated Host object
     *
     * @return the associated Host object
     * @throws TorqueException
     */
    public Host getHost() throws TorqueException
    {
        if (aHost == null && (this.hostId > 0))
        {
                          aHost = HostPeer.retrieveByPK(SimpleKey.keyFor(this.hostId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               Host obj = HostPeer.retrieveByPK(this.hostId);
               obj.addHostDiagStatuss(this);
            */
        }
        return aHost;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey.  e.g.
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
           */
    public void setHostKey(ObjectKey key) throws TorqueException
    {
      
                        setHostId(((NumberKey) key).longValue());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("HostId");
              fieldNames.add("HostDiagCd");
              fieldNames.add("HostDiagValue");
              fieldNames.add("HostDiagStartTs");
              fieldNames.add("HostDiagClearTs");
              fieldNames.add("CreatedBy");
              fieldNames.add("CreatedTs");
              fieldNames.add("LastUpdatedBy");
              fieldNames.add("LastUpdatedTs");
              fieldNames.add("HostDiagStatusId");
              fieldNames.add("HostDiagLastReportedTs");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("HostId"))
        {
                return new Long(getHostId());
            }
          if (name.equals("HostDiagCd"))
        {
                return getHostDiagCd();
            }
          if (name.equals("HostDiagValue"))
        {
                return getHostDiagValue();
            }
          if (name.equals("HostDiagStartTs"))
        {
                return getHostDiagStartTs();
            }
          if (name.equals("HostDiagClearTs"))
        {
                return getHostDiagClearTs();
            }
          if (name.equals("CreatedBy"))
        {
                return getCreatedBy();
            }
          if (name.equals("CreatedTs"))
        {
                return getCreatedTs();
            }
          if (name.equals("LastUpdatedBy"))
        {
                return getLastUpdatedBy();
            }
          if (name.equals("LastUpdatedTs"))
        {
                return getLastUpdatedTs();
            }
          if (name.equals("HostDiagStatusId"))
        {
                return new Long(getHostDiagStatusId());
            }
          if (name.equals("HostDiagLastReportedTs"))
        {
                return getHostDiagLastReportedTs();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(HostDiagStatusPeer.HOST_ID))
        {
                return new Long(getHostId());
            }
          if (name.equals(HostDiagStatusPeer.HOST_DIAG_CD))
        {
                return getHostDiagCd();
            }
          if (name.equals(HostDiagStatusPeer.HOST_DIAG_VALUE))
        {
                return getHostDiagValue();
            }
          if (name.equals(HostDiagStatusPeer.HOST_DIAG_START_TS))
        {
                return getHostDiagStartTs();
            }
          if (name.equals(HostDiagStatusPeer.HOST_DIAG_CLEAR_TS))
        {
                return getHostDiagClearTs();
            }
          if (name.equals(HostDiagStatusPeer.CREATED_BY))
        {
                return getCreatedBy();
            }
          if (name.equals(HostDiagStatusPeer.CREATED_TS))
        {
                return getCreatedTs();
            }
          if (name.equals(HostDiagStatusPeer.LAST_UPDATED_BY))
        {
                return getLastUpdatedBy();
            }
          if (name.equals(HostDiagStatusPeer.LAST_UPDATED_TS))
        {
                return getLastUpdatedTs();
            }
          if (name.equals(HostDiagStatusPeer.HOST_DIAG_STATUS_ID))
        {
                return new Long(getHostDiagStatusId());
            }
          if (name.equals(HostDiagStatusPeer.HOST_DIAG_LAST_REPORTED_TS))
        {
                return getHostDiagLastReportedTs();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return new Long(getHostId());
            }
              if (pos == 1)
        {
                return getHostDiagCd();
            }
              if (pos == 2)
        {
                return getHostDiagValue();
            }
              if (pos == 3)
        {
                return getHostDiagStartTs();
            }
              if (pos == 4)
        {
                return getHostDiagClearTs();
            }
              if (pos == 5)
        {
                return getCreatedBy();
            }
              if (pos == 6)
        {
                return getCreatedTs();
            }
              if (pos == 7)
        {
                return getLastUpdatedBy();
            }
              if (pos == 8)
        {
                return getLastUpdatedTs();
            }
              if (pos == 9)
        {
                return new Long(getHostDiagStatusId());
            }
              if (pos == 10)
        {
                return getHostDiagLastReportedTs();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(HostDiagStatusPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                if (isNew())
                {
                    HostDiagStatusPeer.doInsert((HostDiagStatus) this, con);
                    setNew(false);
                }
                else
                {
                    HostDiagStatusPeer.doUpdate((HostDiagStatus) this, con);
                }
            }

                      alreadyInSave = false;
        }
      }


                    
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param  hostDiagStatusId ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        
    {
            setHostDiagStatusId(((NumberKey) key).longValue());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) 
    {
            setHostDiagStatusId(Long.parseLong(key));
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getHostDiagStatusId());
      }

 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public HostDiagStatus copy() throws TorqueException
    {
        return copyInto(new HostDiagStatus());
    }
  
    protected HostDiagStatus copyInto(HostDiagStatus copyObj) throws TorqueException
    {
          copyObj.setHostId(hostId);
          copyObj.setHostDiagCd(hostDiagCd);
          copyObj.setHostDiagValue(hostDiagValue);
          copyObj.setHostDiagStartTs(hostDiagStartTs);
          copyObj.setHostDiagClearTs(hostDiagClearTs);
          copyObj.setCreatedBy(createdBy);
          copyObj.setCreatedTs(createdTs);
          copyObj.setLastUpdatedBy(lastUpdatedBy);
          copyObj.setLastUpdatedTs(lastUpdatedTs);
          copyObj.setHostDiagStatusId(hostDiagStatusId);
          copyObj.setHostDiagLastReportedTs(hostDiagLastReportedTs);
  
                                                                          copyObj.setHostDiagStatusId(0);
                  
        
        return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public HostDiagStatusPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuffer str = new StringBuffer();
        str.append("HostDiagStatus:\n");
        str.append("HostId = ")
           .append(getHostId())
           .append("\n");
        str.append("HostDiagCd = ")
           .append(getHostDiagCd())
           .append("\n");
        str.append("HostDiagValue = ")
           .append(getHostDiagValue())
           .append("\n");
        str.append("HostDiagStartTs = ")
           .append(getHostDiagStartTs())
           .append("\n");
        str.append("HostDiagClearTs = ")
           .append(getHostDiagClearTs())
           .append("\n");
        str.append("CreatedBy = ")
           .append(getCreatedBy())
           .append("\n");
        str.append("CreatedTs = ")
           .append(getCreatedTs())
           .append("\n");
        str.append("LastUpdatedBy = ")
           .append(getLastUpdatedBy())
           .append("\n");
        str.append("LastUpdatedTs = ")
           .append(getLastUpdatedTs())
           .append("\n");
        str.append("HostDiagStatusId = ")
           .append(getHostDiagStatusId())
           .append("\n");
        str.append("HostDiagLastReportedTs = ")
           .append(getHostDiagLastReportedTs())
           .append("\n");
        return(str.toString());
    }
}
