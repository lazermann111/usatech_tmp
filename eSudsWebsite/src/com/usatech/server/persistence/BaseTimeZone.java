package com.usatech.server.persistence;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ComboKey;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.om.StringKey;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;


/**
 * This class was autogenerated by Torque on:
 *
 * [Fri Aug 27 17:49:47 EDT 2004]
 *
 * You should not use this class directly.  It should not even be
 * extended all references should be to TimeZone
 */
public abstract class BaseTimeZone extends BaseObject
{
    /** The Peer class */
    private static final TimeZonePeer peer =
        new TimeZonePeer();

        
    /** The value for the timeZoneCd field */
    private String timeZoneCd;
      
    /** The value for the timeZoneName field */
    private String timeZoneName;
      
    /** The value for the utcOffset field */
    private BigDecimal utcOffset;
      
    /** The value for the example field */
    private String example;
  
    
    /**
     * Get the TimeZoneCd
     *
     * @return String
     */
    public String getTimeZoneCd()
    {
        return timeZoneCd;
    }

                                              
    /**
     * Set the value of TimeZoneCd
     *
     * @param v new value
     */
    public void setTimeZoneCd(String v) throws TorqueException
    {
    
                  if (!ObjectUtils.equals(this.timeZoneCd, v))
              {
            this.timeZoneCd = v;
            setModified(true);
        }
    
          
                                  
        // update associated Location
        if (collLocations != null)
        {
            for (int i = 0; i < collLocations.size(); i++)
            {
                ((Location) collLocations.get(i))
                    .setLocationTimeZoneCd(v);
            }
        }
                      }
  
    /**
     * Get the TimeZoneName
     *
     * @return String
     */
    public String getTimeZoneName()
    {
        return timeZoneName;
    }

                        
    /**
     * Set the value of TimeZoneName
     *
     * @param v new value
     */
    public void setTimeZoneName(String v) 
    {
    
                  if (!ObjectUtils.equals(this.timeZoneName, v))
              {
            this.timeZoneName = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the UtcOffset
     *
     * @return BigDecimal
     */
    public BigDecimal getUtcOffset()
    {
        return utcOffset;
    }

                        
    /**
     * Set the value of UtcOffset
     *
     * @param v new value
     */
    public void setUtcOffset(BigDecimal v) 
    {
    
                  if (!ObjectUtils.equals(this.utcOffset, v))
              {
            this.utcOffset = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the Example
     *
     * @return String
     */
    public String getExample()
    {
        return example;
    }

                        
    /**
     * Set the value of Example
     *
     * @param v new value
     */
    public void setExample(String v) 
    {
    
                  if (!ObjectUtils.equals(this.example, v))
              {
            this.example = v;
            setModified(true);
        }
    
          
              }
  
         
                                
            
    /**
     * Collection to store aggregation of collLocations
     */
    protected List collLocations;

    /**
     * Temporary storage of collLocations to save a possible db hit in
     * the event objects are add to the collection, but the
     * complete collection is never requested.
     */
    protected void initLocations()
    {
        if (collLocations == null)
        {
            collLocations = new ArrayList();
        }
    }

    /**
     * Method called to associate a Location object to this object
     * through the Location foreign key attribute
     *
     * @param l Location
     * @throws TorqueException
     */
    public void addLocation(Location l) throws TorqueException
    {
        getLocations().add(l);
        l.setTimeZone((TimeZone) this);
    }

    /**
     * The criteria used to select the current contents of collLocations
     */
    private Criteria lastLocationsCriteria = null;

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getLocations(new Criteria())
     *
     * @throws TorqueException
     */
    public List getLocations() throws TorqueException
    {
        if (collLocations == null)
        {
            collLocations = getLocations(new Criteria(10));
        }
        return collLocations;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TimeZone has previously
     * been saved, it will retrieve related Locations from storage.
     * If this TimeZone is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     *
     * @throws TorqueException
     */
    public List getLocations(Criteria criteria) throws TorqueException
    {
        if (collLocations == null)
        {
            if (isNew())
            {
               collLocations = new ArrayList();
            }
            else
            {
                      criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd() );
                      collLocations = LocationPeer.doSelect(criteria);
            }
        }
        else
        {
            // criteria has no effect for a new object
            if (!isNew())
            {
                // the following code is to determine if a new query is
                // called for.  If the criteria is the same as the last
                // one, just return the collection.
                      criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                      if (!lastLocationsCriteria.equals(criteria))
                {
                    collLocations = LocationPeer.doSelect(criteria);
                }
            }
        }
        lastLocationsCriteria = criteria;

        return collLocations;
    }

    /**
     * If this collection has already been initialized, returns
     * the collection. Otherwise returns the results of
     * getLocations(new Criteria(),Connection)
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getLocations(Connection con) throws TorqueException
    {
        if (collLocations == null)
        {
            collLocations = getLocations(new Criteria(10), con);
        }
        return collLocations;
    }

    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TimeZone has previously
     * been saved, it will retrieve related Locations from storage.
     * If this TimeZone is new, it will return
     * an empty collection or the current collection, the criteria
     * is ignored on a new object.
     * This method takes in the Connection also as input so that
     * referenced objects can also be obtained using a Connection
     * that is taken as input
     */
    public List getLocations(Criteria criteria, Connection con)
            throws TorqueException
    {
        if (collLocations == null)
        {
            if (isNew())
            {
               collLocations = new ArrayList();
            }
            else
            {
                       criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                       collLocations = LocationPeer.doSelect(criteria, con);
             }
         }
         else
         {
             // criteria has no effect for a new object
             if (!isNew())
             {
                 // the following code is to determine if a new query is
                 // called for.  If the criteria is the same as the last
                 // one, just return the collection.
                       criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                       if (!lastLocationsCriteria.equals(criteria))
                 {
                     collLocations = LocationPeer.doSelect(criteria, con);
                 }
             }
         }
         lastLocationsCriteria = criteria;

         return collLocations;
     }

                                    
              
                    
                    
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TimeZone is new, it will return
     * an empty collection; or if this TimeZone has previously
     * been saved, it will retrieve related Locations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TimeZone.
     */
    protected List getLocationsJoinLocationType(Criteria criteria)
        throws TorqueException
    {
        if (collLocations == null)
        {
            if (isNew())
            {
               collLocations = new ArrayList();
            }
            else
            {
                            criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                            collLocations = LocationPeer.doSelectJoinLocationType(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            boolean newCriteria = true;
                        criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                        if (!lastLocationsCriteria.equals(criteria))
            {
                collLocations = LocationPeer.doSelectJoinLocationType(criteria);
            }
        }
        lastLocationsCriteria = criteria;

        return collLocations;
    }
                  
                    
                    
                                                      
                                                                                    
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TimeZone is new, it will return
     * an empty collection; or if this TimeZone has previously
     * been saved, it will retrieve related Locations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TimeZone.
     */
    protected List getLocationsJoinState(Criteria criteria)
        throws TorqueException
    {
        if (collLocations == null)
        {
            if (isNew())
            {
               collLocations = new ArrayList();
            }
            else
            {
                            criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                            collLocations = LocationPeer.doSelectJoinState(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            boolean newCriteria = true;
                        criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                        if (!lastLocationsCriteria.equals(criteria))
            {
                collLocations = LocationPeer.doSelectJoinState(criteria);
            }
        }
        lastLocationsCriteria = criteria;

        return collLocations;
    }
                  
                    
                    
                                
                                                              
                                                  
                    
                    
                  
                    
                              
                                
                                                              
                                        
                    
                    
          
    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TimeZone is new, it will return
     * an empty collection; or if this TimeZone has previously
     * been saved, it will retrieve related Locations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TimeZone.
     */
    protected List getLocationsJoinTimeZone(Criteria criteria)
        throws TorqueException
    {
        if (collLocations == null)
        {
            if (isNew())
            {
               collLocations = new ArrayList();
            }
            else
            {
                            criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                            collLocations = LocationPeer.doSelectJoinTimeZone(criteria);
            }
        }
        else
        {
            // the following code is to determine if a new query is
            // called for.  If the criteria is the same as the last
            // one, just return the collection.
            boolean newCriteria = true;
                        criteria.add(LocationPeer.LOCATION_TIME_ZONE_CD, getTimeZoneCd());
                        if (!lastLocationsCriteria.equals(criteria))
            {
                collLocations = LocationPeer.doSelectJoinTimeZone(criteria);
            }
        }
        lastLocationsCriteria = criteria;

        return collLocations;
    }
                            


          
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("TimeZoneCd");
              fieldNames.add("TimeZoneName");
              fieldNames.add("UtcOffset");
              fieldNames.add("Example");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("TimeZoneCd"))
        {
                return getTimeZoneCd();
            }
          if (name.equals("TimeZoneName"))
        {
                return getTimeZoneName();
            }
          if (name.equals("UtcOffset"))
        {
                return getUtcOffset();
            }
          if (name.equals("Example"))
        {
                return getExample();
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(TimeZonePeer.TIME_ZONE_CD))
        {
                return getTimeZoneCd();
            }
          if (name.equals(TimeZonePeer.TIME_ZONE_NAME))
        {
                return getTimeZoneName();
            }
          if (name.equals(TimeZonePeer.UTC_OFFSET))
        {
                return getUtcOffset();
            }
          if (name.equals(TimeZonePeer.EXAMPLE))
        {
                return getExample();
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return getTimeZoneCd();
            }
              if (pos == 1)
        {
                return getTimeZoneName();
            }
              if (pos == 2)
        {
                return getUtcOffset();
            }
              if (pos == 3)
        {
                return getExample();
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(TimeZonePeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                if (isNew())
                {
                    TimeZonePeer.doInsert((TimeZone) this, con);
                    setNew(false);
                }
                else
                {
                    TimeZonePeer.doUpdate((TimeZone) this, con);
                }
            }

                                      
                
            if (collLocations != null)
            {
                for (int i = 0; i < collLocations.size(); i++)
                {
                    ((Location) collLocations.get(i)).save(con);
                }
            }
                          alreadyInSave = false;
        }
      }


                          
      /**
     * Set the PrimaryKey using ObjectKey.
     *
     * @param  timeZoneCd ObjectKey
     */
    public void setPrimaryKey(ObjectKey key)
        throws TorqueException
    {
            setTimeZoneCd(key.toString());
        }

    /**
     * Set the PrimaryKey using a String.
     *
     * @param key
     */
    public void setPrimaryKey(String key) throws TorqueException
    {
            setTimeZoneCd(key);
        }

  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return SimpleKey.keyFor(getTimeZoneCd());
      }

 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public TimeZone copy() throws TorqueException
    {
        return copyInto(new TimeZone());
    }
  
    protected TimeZone copyInto(TimeZone copyObj) throws TorqueException
    {
          copyObj.setTimeZoneCd(timeZoneCd);
          copyObj.setTimeZoneName(timeZoneName);
          copyObj.setUtcOffset(utcOffset);
          copyObj.setExample(example);
  
                    copyObj.setTimeZoneCd((String)null);
                              
                                      
                
        List v = getLocations();
        for (int i = 0; i < v.size(); i++)
        {
            Location obj = (Location) v.get(i);
            copyObj.addLocation(obj.copy());
        }
                    
        return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public TimeZonePeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuffer str = new StringBuffer();
        str.append("TimeZone:\n");
        str.append("TimeZoneCd = ")
           .append(getTimeZoneCd())
           .append("\n");
        str.append("TimeZoneName = ")
           .append(getTimeZoneName())
           .append("\n");
        str.append("UtcOffset = ")
           .append(getUtcOffset())
           .append("\n");
        str.append("Example = ")
           .append(getExample())
           .append("\n");
        return(str.toString());
    }
}
