package com.usatech.server.persistence;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Fri Aug 27 17:49:47 EDT 2004]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class CustLocSettingParameterPeer
    extends com.usatech.server.persistence.BaseCustLocSettingParameterPeer
{
}
