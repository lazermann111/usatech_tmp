package com.usatech.server.persistence;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.ComboKey;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.om.StringKey;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Criteria;
import org.apache.torque.util.Transaction;

  
    
  
/**
 * This class was autogenerated by Torque on:
 *
 * [Fri Apr 21 13:39:02 EDT 2006]
 *
 * You should not use this class directly.  It should not even be
 * extended all references should be to HostTypeHostGroup
 */
public abstract class BaseHostTypeHostGroup extends BaseObject
{
    /** The Peer class */
    private static final HostTypeHostGroupPeer peer =
        new HostTypeHostGroupPeer();

        
    /** The value for the hostTypeId field */
    private long hostTypeId;
      
    /** The value for the hostGroupId field */
    private long hostGroupId;
  
    
    /**
     * Get the HostTypeId
     *
     * @return long
     */
    public long getHostTypeId()
    {
        return hostTypeId;
    }

                              
    /**
     * Set the value of HostTypeId
     *
     * @param v new value
     */
    public void setHostTypeId(long v) throws TorqueException
    {
    
                  if (this.hostTypeId != v)
              {
            this.hostTypeId = v;
            setModified(true);
        }
    
                          
                if (aHostType != null && !(aHostType.getHostTypeId() == v))
                {
            aHostType = null;
        }
      
              }
  
    /**
     * Get the HostGroupId
     *
     * @return long
     */
    public long getHostGroupId()
    {
        return hostGroupId;
    }

                              
    /**
     * Set the value of HostGroupId
     *
     * @param v new value
     */
    public void setHostGroupId(long v) throws TorqueException
    {
    
                  if (this.hostGroupId != v)
              {
            this.hostGroupId = v;
            setModified(true);
        }
    
                          
                if (aHostGroup != null && !(aHostGroup.getHostGroupId() == v))
                {
            aHostGroup = null;
        }
      
              }
  
      
    
                  
    
        private HostType aHostType;

    /**
     * Declares an association between this object and a HostType object
     *
     * @param v HostType
     * @throws TorqueException
     */
    public void setHostType(HostType v) throws TorqueException
    {
            if (v == null)
        {
                    setHostTypeId(0);
                  }
        else
        {
            setHostTypeId(v.getHostTypeId());
        }
                aHostType = v;
    }

                                            
    /**
     * Get the associated HostType object
     *
     * @return the associated HostType object
     * @throws TorqueException
     */
    public HostType getHostType() throws TorqueException
    {
        if (aHostType == null && (this.hostTypeId > 0))
        {
                          aHostType = HostTypePeer.retrieveByPK(SimpleKey.keyFor(this.hostTypeId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               HostType obj = HostTypePeer.retrieveByPK(this.hostTypeId);
               obj.addHostTypeHostGroups(this);
            */
        }
        return aHostType;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey.  e.g.
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
           */
    public void setHostTypeKey(ObjectKey key) throws TorqueException
    {
      
                        setHostTypeId(((NumberKey) key).longValue());
                  }
    
    
                  
    
        private HostGroup aHostGroup;

    /**
     * Declares an association between this object and a HostGroup object
     *
     * @param v HostGroup
     * @throws TorqueException
     */
    public void setHostGroup(HostGroup v) throws TorqueException
    {
            if (v == null)
        {
                    setHostGroupId(0);
                  }
        else
        {
            setHostGroupId(v.getHostGroupId());
        }
                aHostGroup = v;
    }

                                            
    /**
     * Get the associated HostGroup object
     *
     * @return the associated HostGroup object
     * @throws TorqueException
     */
    public HostGroup getHostGroup() throws TorqueException
    {
        if (aHostGroup == null && (this.hostGroupId > 0))
        {
                          aHostGroup = HostGroupPeer.retrieveByPK(SimpleKey.keyFor(this.hostGroupId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               HostGroup obj = HostGroupPeer.retrieveByPK(this.hostGroupId);
               obj.addHostTypeHostGroups(this);
            */
        }
        return aHostGroup;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey.  e.g.
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
           */
    public void setHostGroupKey(ObjectKey key) throws TorqueException
    {
      
                        setHostGroupId(((NumberKey) key).longValue());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("HostTypeId");
              fieldNames.add("HostGroupId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("HostTypeId"))
        {
                return new Long(getHostTypeId());
            }
          if (name.equals("HostGroupId"))
        {
                return new Long(getHostGroupId());
            }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(HostTypeHostGroupPeer.HOST_TYPE_ID))
        {
                return new Long(getHostTypeId());
            }
          if (name.equals(HostTypeHostGroupPeer.HOST_GROUP_TYPE_ID))
        {
                return new Long(getHostGroupId());
            }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return new Long(getHostTypeId());
            }
              if (pos == 1)
        {
                return new Long(getHostGroupId());
            }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(HostTypeHostGroupPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                if (isNew())
                {
                    HostTypeHostGroupPeer.doInsert((HostTypeHostGroup) this, con);
                    setNew(false);
                }
                else
                {
                    HostTypeHostGroupPeer.doUpdate((HostTypeHostGroup) this, con);
                }
            }

                      alreadyInSave = false;
        }
      }


    
  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return null;
      }

 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public HostTypeHostGroup copy() throws TorqueException
    {
        return copyInto(new HostTypeHostGroup());
    }
  
    protected HostTypeHostGroup copyInto(HostTypeHostGroup copyObj) throws TorqueException
    {
          copyObj.setHostTypeId(hostTypeId);
          copyObj.setHostGroupId(hostGroupId);
  
              
        
        return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public HostTypeHostGroupPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuffer str = new StringBuffer();
        str.append("HostTypeHostGroup:\n");
        str.append("HostTypeId = ")
           .append(getHostTypeId())
           .append("\n");
        str.append("HostGroupId = ")
           .append(getHostGroupId())
           .append("\n");
        return(str.toString());
    }
}
