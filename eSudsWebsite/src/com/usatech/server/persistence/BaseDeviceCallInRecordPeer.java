package com.usatech.server.persistence;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.StringKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;

// Local classes
import com.usatech.server.persistence.map.*;


/**
 * This class was autogenerated by Torque on:
 *
 * [Fri Aug 27 17:49:47 EDT 2004]
 *
 */
public abstract class BaseDeviceCallInRecordPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "DB";

     /** the table name for this class */
    public static final String TABLE_NAME = "DEVICE_CALL_IN_RECORD";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(DeviceCallInRecordMapBuilder.CLASS_NAME);
    }

      /** the column name for the DEVICE_CALL_IN_RECORD_ID field */
    public static final String DEVICE_CALL_IN_RECORD_ID;
      /** the column name for the CALL_IN_START_TS field */
    public static final String CALL_IN_START_TS;
      /** the column name for the CALL_IN_FINISH_TS field */
    public static final String CALL_IN_FINISH_TS;
      /** the column name for the CALL_IN_STATUS field */
    public static final String CALL_IN_STATUS;
      /** the column name for the NETWORK_LAYER field */
    public static final String NETWORK_LAYER;
      /** the column name for the IP_ADDRESS field */
    public static final String IP_ADDRESS;
      /** the column name for the MODEM_ID field */
    public static final String MODEM_ID;
      /** the column name for the RSSI field */
    public static final String RSSI;
      /** the column name for the CREDIT_TRANS_COUNT field */
    public static final String CREDIT_TRANS_COUNT;
      /** the column name for the CREDIT_TRANS_TOTAL field */
    public static final String CREDIT_TRANS_TOTAL;
      /** the column name for the CASH_TRANS_COUNT field */
    public static final String CASH_TRANS_COUNT;
      /** the column name for the CASH_TRANS_TOTAL field */
    public static final String CASH_TRANS_TOTAL;
      /** the column name for the PASSCARD_TRANS_COUNT field */
    public static final String PASSCARD_TRANS_COUNT;
      /** the column name for the PASSCARD_TRANS_TOTAL field */
    public static final String PASSCARD_TRANS_TOTAL;
      /** the column name for the DEVICE_INITIALIZED_FLAG field */
    public static final String DEVICE_INITIALIZED_FLAG;
      /** the column name for the DEVICE_SENT_CONFIG_FLAG field */
    public static final String DEVICE_SENT_CONFIG_FLAG;
      /** the column name for the SERVER_SENT_CONFIG_FLAG field */
    public static final String SERVER_SENT_CONFIG_FLAG;
      /** the column name for the CREATED_BY field */
    public static final String CREATED_BY;
      /** the column name for the CREATED_TS field */
    public static final String CREATED_TS;
      /** the column name for the LAST_UPDATED_BY field */
    public static final String LAST_UPDATED_BY;
      /** the column name for the LAST_UPDATED_TS field */
    public static final String LAST_UPDATED_TS;
      /** the column name for the CALL_IN_TYPE field */
    public static final String CALL_IN_TYPE;
      /** the column name for the AUTH_AMOUNT field */
    public static final String AUTH_AMOUNT;
      /** the column name for the AUTH_APPROVED_FLAG field */
    public static final String AUTH_APPROVED_FLAG;
      /** the column name for the AUTH_CARD_TYPE field */
    public static final String AUTH_CARD_TYPE;
      /** the column name for the INBOUND_MESSAGE_COUNT field */
    public static final String INBOUND_MESSAGE_COUNT;
      /** the column name for the OUTBOUND_MESSAGE_COUNT field */
    public static final String OUTBOUND_MESSAGE_COUNT;
      /** the column name for the DEX_FILE_SIZE field */
    public static final String DEX_FILE_SIZE;
      /** the column name for the DEX_RECEIVED_FLAG field */
    public static final String DEX_RECEIVED_FLAG;
      /** the column name for the OUTBOUND_BYTE_COUNT field */
    public static final String OUTBOUND_BYTE_COUNT;
      /** the column name for the LOCATION_NAME field */
    public static final String LOCATION_NAME;
      /** the column name for the CUSTOMER_NAME field */
    public static final String CUSTOMER_NAME;
      /** the column name for the SERIAL_NUMBER field */
    public static final String SERIAL_NUMBER;
      /** the column name for the INBOUND_BYTE_COUNT field */
    public static final String INBOUND_BYTE_COUNT;
      /** the column name for the SESSION_CD field */
    public static final String SESSION_CD;
  
    static
    {
          DEVICE_CALL_IN_RECORD_ID = "DEVICE_CALL_IN_RECORD.DEVICE_CALL_IN_RECORD_ID";
          CALL_IN_START_TS = "DEVICE_CALL_IN_RECORD.CALL_IN_START_TS";
          CALL_IN_FINISH_TS = "DEVICE_CALL_IN_RECORD.CALL_IN_FINISH_TS";
          CALL_IN_STATUS = "DEVICE_CALL_IN_RECORD.CALL_IN_STATUS";
          NETWORK_LAYER = "DEVICE_CALL_IN_RECORD.NETWORK_LAYER";
          IP_ADDRESS = "DEVICE_CALL_IN_RECORD.IP_ADDRESS";
          MODEM_ID = "DEVICE_CALL_IN_RECORD.MODEM_ID";
          RSSI = "DEVICE_CALL_IN_RECORD.RSSI";
          CREDIT_TRANS_COUNT = "DEVICE_CALL_IN_RECORD.CREDIT_TRANS_COUNT";
          CREDIT_TRANS_TOTAL = "DEVICE_CALL_IN_RECORD.CREDIT_TRANS_TOTAL";
          CASH_TRANS_COUNT = "DEVICE_CALL_IN_RECORD.CASH_TRANS_COUNT";
          CASH_TRANS_TOTAL = "DEVICE_CALL_IN_RECORD.CASH_TRANS_TOTAL";
          PASSCARD_TRANS_COUNT = "DEVICE_CALL_IN_RECORD.PASSCARD_TRANS_COUNT";
          PASSCARD_TRANS_TOTAL = "DEVICE_CALL_IN_RECORD.PASSCARD_TRANS_TOTAL";
          DEVICE_INITIALIZED_FLAG = "DEVICE_CALL_IN_RECORD.DEVICE_INITIALIZED_FLAG";
          DEVICE_SENT_CONFIG_FLAG = "DEVICE_CALL_IN_RECORD.DEVICE_SENT_CONFIG_FLAG";
          SERVER_SENT_CONFIG_FLAG = "DEVICE_CALL_IN_RECORD.SERVER_SENT_CONFIG_FLAG";
          CREATED_BY = "DEVICE_CALL_IN_RECORD.CREATED_BY";
          CREATED_TS = "DEVICE_CALL_IN_RECORD.CREATED_TS";
          LAST_UPDATED_BY = "DEVICE_CALL_IN_RECORD.LAST_UPDATED_BY";
          LAST_UPDATED_TS = "DEVICE_CALL_IN_RECORD.LAST_UPDATED_TS";
          CALL_IN_TYPE = "DEVICE_CALL_IN_RECORD.CALL_IN_TYPE";
          AUTH_AMOUNT = "DEVICE_CALL_IN_RECORD.AUTH_AMOUNT";
          AUTH_APPROVED_FLAG = "DEVICE_CALL_IN_RECORD.AUTH_APPROVED_FLAG";
          AUTH_CARD_TYPE = "DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE";
          INBOUND_MESSAGE_COUNT = "DEVICE_CALL_IN_RECORD.INBOUND_MESSAGE_COUNT";
          OUTBOUND_MESSAGE_COUNT = "DEVICE_CALL_IN_RECORD.OUTBOUND_MESSAGE_COUNT";
          DEX_FILE_SIZE = "DEVICE_CALL_IN_RECORD.DEX_FILE_SIZE";
          DEX_RECEIVED_FLAG = "DEVICE_CALL_IN_RECORD.DEX_RECEIVED_FLAG";
          OUTBOUND_BYTE_COUNT = "DEVICE_CALL_IN_RECORD.OUTBOUND_BYTE_COUNT";
          LOCATION_NAME = "DEVICE_CALL_IN_RECORD.LOCATION_NAME";
          CUSTOMER_NAME = "DEVICE_CALL_IN_RECORD.CUSTOMER_NAME";
          SERIAL_NUMBER = "DEVICE_CALL_IN_RECORD.SERIAL_NUMBER";
          INBOUND_BYTE_COUNT = "DEVICE_CALL_IN_RECORD.INBOUND_BYTE_COUNT";
          SESSION_CD = "DEVICE_CALL_IN_RECORD.SESSION_CD";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(DeviceCallInRecordMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(DeviceCallInRecordMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  35;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.usatech.server.persistence.DeviceCallInRecord";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseDeviceCallInRecordPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                                                                                                                                    
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(DEVICE_CALL_IN_RECORD_ID);
          criteria.addSelectColumn(CALL_IN_START_TS);
          criteria.addSelectColumn(CALL_IN_FINISH_TS);
          criteria.addSelectColumn(CALL_IN_STATUS);
          criteria.addSelectColumn(NETWORK_LAYER);
          criteria.addSelectColumn(IP_ADDRESS);
          criteria.addSelectColumn(MODEM_ID);
          criteria.addSelectColumn(RSSI);
          criteria.addSelectColumn(CREDIT_TRANS_COUNT);
          criteria.addSelectColumn(CREDIT_TRANS_TOTAL);
          criteria.addSelectColumn(CASH_TRANS_COUNT);
          criteria.addSelectColumn(CASH_TRANS_TOTAL);
          criteria.addSelectColumn(PASSCARD_TRANS_COUNT);
          criteria.addSelectColumn(PASSCARD_TRANS_TOTAL);
          criteria.addSelectColumn(DEVICE_INITIALIZED_FLAG);
          criteria.addSelectColumn(DEVICE_SENT_CONFIG_FLAG);
          criteria.addSelectColumn(SERVER_SENT_CONFIG_FLAG);
          criteria.addSelectColumn(CREATED_BY);
          criteria.addSelectColumn(CREATED_TS);
          criteria.addSelectColumn(LAST_UPDATED_BY);
          criteria.addSelectColumn(LAST_UPDATED_TS);
          criteria.addSelectColumn(CALL_IN_TYPE);
          criteria.addSelectColumn(AUTH_AMOUNT);
          criteria.addSelectColumn(AUTH_APPROVED_FLAG);
          criteria.addSelectColumn(AUTH_CARD_TYPE);
          criteria.addSelectColumn(INBOUND_MESSAGE_COUNT);
          criteria.addSelectColumn(OUTBOUND_MESSAGE_COUNT);
          criteria.addSelectColumn(DEX_FILE_SIZE);
          criteria.addSelectColumn(DEX_RECEIVED_FLAG);
          criteria.addSelectColumn(OUTBOUND_BYTE_COUNT);
          criteria.addSelectColumn(LOCATION_NAME);
          criteria.addSelectColumn(CUSTOMER_NAME);
          criteria.addSelectColumn(SERIAL_NUMBER);
          criteria.addSelectColumn(INBOUND_BYTE_COUNT);
          criteria.addSelectColumn(SESSION_CD);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static DeviceCallInRecord row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            DeviceCallInRecord obj = (DeviceCallInRecord) cls.newInstance();
            DeviceCallInRecordPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      DeviceCallInRecord obj)
        throws TorqueException
    {
        try
        {
                obj.setDeviceCallInRecordId(row.getValue(offset + 0).asLong());
                  obj.setCallInStartTs(row.getValue(offset + 1).asUtilDate());
                  obj.setCallInFinishTs(row.getValue(offset + 2).asUtilDate());
                  obj.setCallInStatus(row.getValue(offset + 3).asString());
                  obj.setNetworkLayer(row.getValue(offset + 4).asString());
                  obj.setIpAddress(row.getValue(offset + 5).asString());
                  obj.setModemId(row.getValue(offset + 6).asString());
                  obj.setRssi(row.getValue(offset + 7).asString());
                  obj.setCreditTransCount(row.getValue(offset + 8).asInt());
                  obj.setCreditTransTotal(row.getValue(offset + 9).asBigDecimal());
                  obj.setCashTransCount(row.getValue(offset + 10).asInt());
                  obj.setCashTransTotal(row.getValue(offset + 11).asBigDecimal());
                  obj.setPasscardTransCount(row.getValue(offset + 12).asInt());
                  obj.setPasscardTransTotal(row.getValue(offset + 13).asBigDecimal());
                  obj.setDeviceInitializedFlag(row.getValue(offset + 14).asString());
                  obj.setDeviceSentConfigFlag(row.getValue(offset + 15).asString());
                  obj.setServerSentConfigFlag(row.getValue(offset + 16).asString());
                  obj.setCreatedBy(row.getValue(offset + 17).asString());
                  obj.setCreatedTs(row.getValue(offset + 18).asUtilDate());
                  obj.setLastUpdatedBy(row.getValue(offset + 19).asString());
                  obj.setLastUpdatedTs(row.getValue(offset + 20).asUtilDate());
                  obj.setCallInType(row.getValue(offset + 21).asString());
                  obj.setAuthAmount(row.getValue(offset + 22).asBigDecimal());
                  obj.setAuthApprovedFlag(row.getValue(offset + 23).asString());
                  obj.setAuthCardType(row.getValue(offset + 24).asString());
                  obj.setInboundMessageCount(row.getValue(offset + 25).asInt());
                  obj.setOutboundMessageCount(row.getValue(offset + 26).asInt());
                  obj.setDexFileSize(row.getValue(offset + 27).asInt());
                  obj.setDexReceivedFlag(row.getValue(offset + 28).asString());
                  obj.setOutboundByteCount(row.getValue(offset + 29).asInt());
                  obj.setLocationName(row.getValue(offset + 30).asString());
                  obj.setCustomerName(row.getValue(offset + 31).asString());
                  obj.setSerialNumber(row.getValue(offset + 32).asString());
                  obj.setInboundByteCount(row.getValue(offset + 33).asInt());
                  obj.setSessionCd(row.getValue(offset + 34).asString());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseDeviceCallInRecordPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                                                                                                                                    
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(DeviceCallInRecordPeer.row2Object(row, 1,
                DeviceCallInRecordPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseDeviceCallInRecordPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(DEVICE_CALL_IN_RECORD_ID, criteria.remove(DEVICE_CALL_IN_RECORD_ID));
                                                                                                                                                                                                                                                                                                                                                          
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         BaseDeviceCallInRecordPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                                                                                                                                    
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(DeviceCallInRecord obj) throws TorqueException
    {
        return doSelect(buildCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(DeviceCallInRecord obj) throws TorqueException
    {
          obj.setPrimaryKey(doInsert(buildCriteria(obj)));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(DeviceCallInRecord obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(DeviceCallInRecord obj) throws TorqueException
    {
        doDelete(buildCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(DeviceCallInRecord) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(DeviceCallInRecord obj, Connection con)
        throws TorqueException
    {
          obj.setPrimaryKey(doInsert(buildCriteria(obj), con));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(DeviceCallInRecord) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(DeviceCallInRecord obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(DeviceCallInRecord) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(DeviceCallInRecord obj, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseDeviceCallInRecordPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(DEVICE_CALL_IN_RECORD_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( DeviceCallInRecord obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              if (!obj.isNew())
                criteria.add(DEVICE_CALL_IN_RECORD_ID, obj.getDeviceCallInRecordId());
                  criteria.add(CALL_IN_START_TS, obj.getCallInStartTs());
                  criteria.add(CALL_IN_FINISH_TS, obj.getCallInFinishTs());
                  criteria.add(CALL_IN_STATUS, obj.getCallInStatus());
                  criteria.add(NETWORK_LAYER, obj.getNetworkLayer());
                  criteria.add(IP_ADDRESS, obj.getIpAddress());
                  criteria.add(MODEM_ID, obj.getModemId());
                  criteria.add(RSSI, obj.getRssi());
                  criteria.add(CREDIT_TRANS_COUNT, obj.getCreditTransCount());
                  criteria.add(CREDIT_TRANS_TOTAL, obj.getCreditTransTotal());
                  criteria.add(CASH_TRANS_COUNT, obj.getCashTransCount());
                  criteria.add(CASH_TRANS_TOTAL, obj.getCashTransTotal());
                  criteria.add(PASSCARD_TRANS_COUNT, obj.getPasscardTransCount());
                  criteria.add(PASSCARD_TRANS_TOTAL, obj.getPasscardTransTotal());
                  criteria.add(DEVICE_INITIALIZED_FLAG, obj.getDeviceInitializedFlag());
                  criteria.add(DEVICE_SENT_CONFIG_FLAG, obj.getDeviceSentConfigFlag());
                  criteria.add(SERVER_SENT_CONFIG_FLAG, obj.getServerSentConfigFlag());
                  criteria.add(CREATED_BY, obj.getCreatedBy());
                  criteria.add(CREATED_TS, obj.getCreatedTs());
                  criteria.add(LAST_UPDATED_BY, obj.getLastUpdatedBy());
                  criteria.add(LAST_UPDATED_TS, obj.getLastUpdatedTs());
                  criteria.add(CALL_IN_TYPE, obj.getCallInType());
                  criteria.add(AUTH_AMOUNT, obj.getAuthAmount());
                  criteria.add(AUTH_APPROVED_FLAG, obj.getAuthApprovedFlag());
                  criteria.add(AUTH_CARD_TYPE, obj.getAuthCardType());
                  criteria.add(INBOUND_MESSAGE_COUNT, obj.getInboundMessageCount());
                  criteria.add(OUTBOUND_MESSAGE_COUNT, obj.getOutboundMessageCount());
                  criteria.add(DEX_FILE_SIZE, obj.getDexFileSize());
                  criteria.add(DEX_RECEIVED_FLAG, obj.getDexReceivedFlag());
                  criteria.add(OUTBOUND_BYTE_COUNT, obj.getOutboundByteCount());
                  criteria.add(LOCATION_NAME, obj.getLocationName());
                  criteria.add(CUSTOMER_NAME, obj.getCustomerName());
                  criteria.add(SERIAL_NUMBER, obj.getSerialNumber());
                  criteria.add(INBOUND_BYTE_COUNT, obj.getInboundByteCount());
                  criteria.add(SESSION_CD, obj.getSessionCd());
          return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static DeviceCallInRecord retrieveByPK(long pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static DeviceCallInRecord retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        DeviceCallInRecord retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static DeviceCallInRecord retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (DeviceCallInRecord)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new LinkedList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( DEVICE_CALL_IN_RECORD_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



        
  
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   }
