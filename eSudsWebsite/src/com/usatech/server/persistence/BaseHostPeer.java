package com.usatech.server.persistence;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.map.MapBuilder;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.StringKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.BasePeer;
import org.apache.torque.util.Criteria;

import com.workingdogs.village.DataSetException;
import com.workingdogs.village.QueryDataSet;
import com.workingdogs.village.Record;

// Local classes
import com.usatech.server.persistence.map.*;


  
  
/**
 * This class was autogenerated by Torque on:
 *
 * [Fri Aug 27 17:49:47 EDT 2004]
 *
 */
public abstract class BaseHostPeer
    extends BasePeer
{

    /** the default database name for this class */
    public static final String DATABASE_NAME = "DB";

     /** the table name for this class */
    public static final String TABLE_NAME = "HOST";

    /**
     * @return the map builder for this peer
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static MapBuilder getMapBuilder()
        throws TorqueException
    {
        return getMapBuilder(HostMapBuilder.CLASS_NAME);
    }

      /** the column name for the HOST_ID field */
    public static final String HOST_ID;
      /** the column name for the HOST_LAST_START_TS field */
    public static final String HOST_LAST_START_TS;
      /** the column name for the HOST_STATUS_CD field */
    public static final String HOST_STATUS_CD;
      /** the column name for the HOST_SERIAL_CD field */
    public static final String HOST_SERIAL_CD;
      /** the column name for the HOST_EST_COMPLETE_MINUT field */
    public static final String HOST_EST_COMPLETE_MINUT;
      /** the column name for the CREATED_BY field */
    public static final String CREATED_BY;
      /** the column name for the CREATED_TS field */
    public static final String CREATED_TS;
      /** the column name for the LAST_UPDATED_BY field */
    public static final String LAST_UPDATED_BY;
      /** the column name for the LAST_UPDATED_TS field */
    public static final String LAST_UPDATED_TS;
      /** the column name for the HOST_PORT_NUM field */
    public static final String HOST_PORT_NUM;
      /** the column name for the HOST_SETTING_UPDATED_YN_FLAG field */
    public static final String HOST_SETTING_UPDATED_YN_FLAG;
      /** the column name for the DEVICE_ID field */
    public static final String DEVICE_ID;
      /** the column name for the HOST_POSITION_NUM field */
    public static final String HOST_POSITION_NUM;
      /** the column name for the HOST_LABEL_CD field */
    public static final String HOST_LABEL_CD;
      /** the column name for the HOST_ACTIVE_YN_FLAG field */
    public static final String HOST_ACTIVE_YN_FLAG;
      /** the column name for the HOST_TYPE_ID field */
    public static final String HOST_TYPE_ID;
  
    static
    {
          HOST_ID = "HOST.HOST_ID";
          HOST_LAST_START_TS = "HOST.HOST_LAST_START_TS";
          HOST_STATUS_CD = "HOST.HOST_STATUS_CD";
          HOST_SERIAL_CD = "HOST.HOST_SERIAL_CD";
          HOST_EST_COMPLETE_MINUT = "HOST.HOST_EST_COMPLETE_MINUT";
          CREATED_BY = "HOST.CREATED_BY";
          CREATED_TS = "HOST.CREATED_TS";
          LAST_UPDATED_BY = "HOST.LAST_UPDATED_BY";
          LAST_UPDATED_TS = "HOST.LAST_UPDATED_TS";
          HOST_PORT_NUM = "HOST.HOST_PORT_NUM";
          HOST_SETTING_UPDATED_YN_FLAG = "HOST.HOST_SETTING_UPDATED_YN_FLAG";
          DEVICE_ID = "HOST.DEVICE_ID";
          HOST_POSITION_NUM = "HOST.HOST_POSITION_NUM";
          HOST_LABEL_CD = "HOST.HOST_LABEL_CD";
          HOST_ACTIVE_YN_FLAG = "HOST.HOST_ACTIVE_YN_FLAG";
          HOST_TYPE_ID = "HOST.HOST_TYPE_ID";
          if (Torque.isInit())
        {
            try
            {
                getMapBuilder(HostMapBuilder.CLASS_NAME);
            }
            catch (Exception e)
            {
                log.error("Could not initialize Peer", e);
            }
        }
        else
        {
            Torque.registerMapBuilder(HostMapBuilder.CLASS_NAME);
        }
    }
 
    /** number of columns for this peer */
    public static final int numColumns =  16;

    /** A class that can be returned by this peer. */
    protected static final String CLASSNAME_DEFAULT =
        "com.usatech.server.persistence.Host";

    /** A class that can be returned by this peer. */
    protected static final Class CLASS_DEFAULT = initClass(CLASSNAME_DEFAULT);

    /**
     * Class object initialization method.
     *
     * @param className name of the class to initialize
     * @return the initialized class
     */
    private static Class initClass(String className)
    {
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch (Throwable t)
        {
            log.error("A FATAL ERROR has occurred which should not "
                + "have happened under any circumstance.  Please notify "
                + "the Torque developers <torque-dev@db.apache.org> "
                + "and give as many details as possible (including the error "
                + "stack trace).", t);

            // Error objects should always be propogated.
            if (t instanceof Error)
            {
                throw (Error) t.fillInStackTrace();
            }
        }
        return c;
    }

    /**
     * Get the list of objects for a ResultSet.  Please not that your
     * resultset MUST return columns in the right order.  You can use
     * getFieldNames() in BaseObject to get the correct sequence.
     *
     * @param results the ResultSet
     * @return the list of objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List resultSet2Objects(java.sql.ResultSet results)
            throws TorqueException
    {
        try
        {
            QueryDataSet qds = null;
            List rows = null;
            try
            {
                qds = new QueryDataSet(results);
                rows = getSelectResults(qds);
            }
            finally
            {
                if (qds != null)
                {
                    qds.close();
                }
            }

            return populateObjects(rows);
        }
        catch (SQLException e)
        {
            throw new TorqueException(e);
        }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }


  
    /**
     * Method to do inserts.
     *
     * @param criteria object used to create the INSERT statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria)
        throws TorqueException
    {
        return BaseHostPeer
            .doInsert(criteria, (Connection) null);
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object used to create the INSERT statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static ObjectKey doInsert(Criteria criteria, Connection con)
        throws TorqueException
    {
                                                                                                  
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        if (con == null)
        {
            return BasePeer.doInsert(criteria);
        }
        else
        {
            return BasePeer.doInsert(criteria, con);
        }
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * @param criteria object containing the columns to add.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void addSelectColumns(Criteria criteria)
            throws TorqueException
    {
          criteria.addSelectColumn(HOST_ID);
          criteria.addSelectColumn(HOST_LAST_START_TS);
          criteria.addSelectColumn(HOST_STATUS_CD);
          criteria.addSelectColumn(HOST_SERIAL_CD);
          criteria.addSelectColumn(HOST_EST_COMPLETE_MINUT);
          criteria.addSelectColumn(CREATED_BY);
          criteria.addSelectColumn(CREATED_TS);
          criteria.addSelectColumn(LAST_UPDATED_BY);
          criteria.addSelectColumn(LAST_UPDATED_TS);
          criteria.addSelectColumn(HOST_PORT_NUM);
          criteria.addSelectColumn(HOST_SETTING_UPDATED_YN_FLAG);
          criteria.addSelectColumn(DEVICE_ID);
          criteria.addSelectColumn(HOST_POSITION_NUM);
          criteria.addSelectColumn(HOST_LABEL_CD);
          criteria.addSelectColumn(HOST_ACTIVE_YN_FLAG);
          criteria.addSelectColumn(HOST_TYPE_ID);
      }

    /**
     * Create a new object of type cls from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Host row2Object(Record row,
                                             int offset,
                                             Class cls)
        throws TorqueException
    {
        try
        {
            Host obj = (Host) cls.newInstance();
            HostPeer.populateObject(row, offset, obj);
                  obj.setModified(false);
              obj.setNew(false);

            return obj;
        }
        catch (InstantiationException e)
        {
            throw new TorqueException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Populates an object from a resultset row starting
     * from a specified offset.  This is done so that you can select
     * other rows than just those needed for this object.  You may
     * for example want to create two objects from the same row.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void populateObject(Record row,
                                      int offset,
                                      Host obj)
        throws TorqueException
    {
        try
        {
                obj.setHostId(row.getValue(offset + 0).asLong());
                  obj.setHostLastStartTs(row.getValue(offset + 1).asUtilDate());
                  obj.setHostStatusCd(row.getValue(offset + 2).asString());
                  obj.setHostSerialCd(row.getValue(offset + 3).asString());
                  obj.setHostEstCompleteMinut(row.getValue(offset + 4).asInt());
                  obj.setCreatedBy(row.getValue(offset + 5).asString());
                  obj.setCreatedTs(row.getValue(offset + 6).asUtilDate());
                  obj.setLastUpdatedBy(row.getValue(offset + 7).asString());
                  obj.setLastUpdatedTs(row.getValue(offset + 8).asUtilDate());
                  obj.setHostPortNum(row.getValue(offset + 9).asInt());
                  obj.setHostSettingUpdatedYnFlag(row.getValue(offset + 10).asString());
                  obj.setDeviceId(row.getValue(offset + 11).asLong());
                  obj.setHostPositionNum(row.getValue(offset + 12).asInt());
                  obj.setHostLabelCd(row.getValue(offset + 13).asString());
                  obj.setHostActiveYnFlag(row.getValue(offset + 14).asString());
                  obj.setHostTypeId(row.getValue(offset + 15).asLong());
              }
        catch (DataSetException e)
        {
            throw new TorqueException(e);
        }
    }

    /**
     * Method to do selects.
     *
     * @param criteria object used to create the SELECT statement.
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria) throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria));
    }

    /**
     * Method to do selects within a transaction.
     *
     * @param criteria object used to create the SELECT statement.
     * @param con the connection to use
     * @return List of selected Objects
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Criteria criteria, Connection con)
        throws TorqueException
    {
        return populateObjects(doSelectVillageRecords(criteria, con));
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method handles connections internally.  The Record objects
     * returned by this method should be considered readonly.  Do not
     * alter the data and call save(), your results may vary, but are
     * certainly likely to result in hard to track MT bugs.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria)
        throws TorqueException
    {
        return BaseHostPeer
            .doSelectVillageRecords(criteria, (Connection) null);
    }

    /**
     * Grabs the raw Village records to be formed into objects.
     * This method should be used for transactions
     *
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelectVillageRecords(Criteria criteria, Connection con)
        throws TorqueException
    {
        if (criteria.getSelectColumns().size() == 0)
        {
            addSelectColumns(criteria);
        }

                                                                                                  
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        // BasePeer returns a List of Value (Village) arrays.  The array
        // order follows the order columns were placed in the Select clause.
        if (con == null)
        {
            return BasePeer.doSelect(criteria);
        }
        else
        {
            return BasePeer.doSelect(criteria, con);
        }
    }

    /**
     * The returned List will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List populateObjects(List records)
        throws TorqueException
    {
        List results = new ArrayList(records.size());

        // populate the object(s)
        for (int i = 0; i < records.size(); i++)
        {
            Record row = (Record) records.get(i);
              results.add(HostPeer.row2Object(row, 1,
                HostPeer.getOMClass()));
          }
        return results;
    }
 

    /**
     * The class that the Peer will make instances of.
     * If the BO is abstract then you must implement this method
     * in the BO.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static Class getOMClass()
        throws TorqueException
    {
        return CLASS_DEFAULT;
    }

    /**
     * Method to do updates.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria) throws TorqueException
    {
         BaseHostPeer
            .doUpdate(criteria, (Connection) null);
    }

    /**
     * Method to do updates.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used to create the UPDATE
     *        statement.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Criteria criteria, Connection con)
        throws TorqueException
    {
        Criteria selectCriteria = new Criteria(DATABASE_NAME, 2);
                   selectCriteria.put(HOST_ID, criteria.remove(HOST_ID));
                                                                                                                                                            
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        if (con == null)
        {
            BasePeer.doUpdate(selectCriteria, criteria);
        }
        else
        {
            BasePeer.doUpdate(selectCriteria, criteria, con);
        }
    }

    /**
     * Method to do deletes.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria) throws TorqueException
     {
         BaseHostPeer
            .doDelete(criteria, (Connection) null);
     }

    /**
     * Method to do deletes.  This method is to be used during a transaction,
     * otherwise use the doDelete(Criteria) method.  It will take care of
     * the connection details internally.
     *
     * @param criteria object containing data that is used DELETE from database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
     public static void doDelete(Criteria criteria, Connection con)
        throws TorqueException
     {
                                                                                                  
        // Set the correct dbName if it has not been overridden
        // criteria.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (criteria.getDbName() == Torque.getDefaultDB())
        {
            criteria.setDbName(DATABASE_NAME);
        }
        if (con == null)
        {
            BasePeer.doDelete(criteria);
        }
        else
        {
            BasePeer.doDelete(criteria, con);
        }
     }

    /**
     * Method to do selects
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List doSelect(Host obj) throws TorqueException
    {
        return doSelect(buildCriteria(obj));
    }

    /**
     * Method to do inserts
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Host obj) throws TorqueException
    {
          obj.setPrimaryKey(doInsert(buildCriteria(obj)));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * @param obj the data object to update in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Host obj) throws TorqueException
    {
        doUpdate(buildCriteria(obj));
        obj.setModified(false);
    }

    /**
     * @param obj the data object to delete in the database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Host obj) throws TorqueException
    {
        doDelete(buildCriteria(obj));
    }

    /**
     * Method to do inserts.  This method is to be used during a transaction,
     * otherwise use the doInsert(Host) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to insert into the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doInsert(Host obj, Connection con)
        throws TorqueException
    {
          obj.setPrimaryKey(doInsert(buildCriteria(obj), con));
          obj.setNew(false);
        obj.setModified(false);
    }

    /**
     * Method to do update.  This method is to be used during a transaction,
     * otherwise use the doUpdate(Host) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to update in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doUpdate(Host obj, Connection con)
        throws TorqueException
    {
        doUpdate(buildCriteria(obj), con);
        obj.setModified(false);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(Host) method.  It will take
     * care of the connection details internally.
     *
     * @param obj the data object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(Host obj, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(obj), con);
    }

    /**
     * Method to do deletes.
     *
     * @param pk ObjectKey that is used DELETE from database.
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk) throws TorqueException
    {
        BaseHostPeer
           .doDelete(pk, (Connection) null);
    }

    /**
     * Method to delete.  This method is to be used during a transaction,
     * otherwise use the doDelete(ObjectKey) method.  It will take
     * care of the connection details internally.
     *
     * @param pk the primary key for the object to delete in the database.
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static void doDelete(ObjectKey pk, Connection con)
        throws TorqueException
    {
        doDelete(buildCriteria(pk), con);
    }

    /** Build a Criteria object from an ObjectKey */
    public static Criteria buildCriteria( ObjectKey pk )
    {
        Criteria criteria = new Criteria();
              criteria.add(HOST_ID, pk);
          return criteria;
     }

    /** Build a Criteria object from the data object for this peer */
    public static Criteria buildCriteria( Host obj )
    {
        Criteria criteria = new Criteria(DATABASE_NAME);
              if (!obj.isNew())
                criteria.add(HOST_ID, obj.getHostId());
                  criteria.add(HOST_LAST_START_TS, obj.getHostLastStartTs());
                  criteria.add(HOST_STATUS_CD, obj.getHostStatusCd());
                  criteria.add(HOST_SERIAL_CD, obj.getHostSerialCd());
                  criteria.add(HOST_EST_COMPLETE_MINUT, obj.getHostEstCompleteMinut());
                  criteria.add(CREATED_BY, obj.getCreatedBy());
                  criteria.add(CREATED_TS, obj.getCreatedTs());
                  criteria.add(LAST_UPDATED_BY, obj.getLastUpdatedBy());
                  criteria.add(LAST_UPDATED_TS, obj.getLastUpdatedTs());
                  criteria.add(HOST_PORT_NUM, obj.getHostPortNum());
                  criteria.add(HOST_SETTING_UPDATED_YN_FLAG, obj.getHostSettingUpdatedYnFlag());
                  criteria.add(DEVICE_ID, obj.getDeviceId());
                  criteria.add(HOST_POSITION_NUM, obj.getHostPositionNum());
                  criteria.add(HOST_LABEL_CD, obj.getHostLabelCd());
                  criteria.add(HOST_ACTIVE_YN_FLAG, obj.getHostActiveYnFlag());
                  criteria.add(HOST_TYPE_ID, obj.getHostTypeId());
          return criteria;
    }
 
    
        /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Host retrieveByPK(long pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        return retrieveByPK(SimpleKey.keyFor(pk));
    }
  
    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Host retrieveByPK(ObjectKey pk)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Connection db = null;
        Host retVal = null;
        try
        {
            db = Torque.getConnection(DATABASE_NAME);
            retVal = retrieveByPK(pk, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a single object by pk
     *
     * @param pk the primary key
     * @param con the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     * @throws NoRowsException Primary key was not found in database.
     * @throws TooManyRowsException Primary key was not found in database.
     */
    public static Host retrieveByPK(ObjectKey pk, Connection con)
        throws TorqueException, NoRowsException, TooManyRowsException
    {
        Criteria criteria = buildCriteria(pk);
        List v = doSelect(criteria, con);
        if (v.size() == 0)
        {
            throw new NoRowsException("Failed to select a row.");
        }
        else if (v.size() > 1)
        {
            throw new TooManyRowsException("Failed to select only one row.");
        }
        else
        {
            return (Host)v.get(0);
        }
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs(List pks)
        throws TorqueException
    {
        Connection db = null;
        List retVal = null;
        try
        {
           db = Torque.getConnection(DATABASE_NAME);
           retVal = retrieveByPKs(pks, db);
        }
        finally
        {
            Torque.closeConnection(db);
        }
        return(retVal);
    }

    /**
     * Retrieve a multiple objects by pk
     *
     * @param pks List of primary keys
     * @param dbcon the connection to use
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    public static List retrieveByPKs( List pks, Connection dbcon )
        throws TorqueException
    {
        List objs = null;
        if (pks == null || pks.size() == 0)
        {
            objs = new LinkedList();
        }
        else
        {
            Criteria criteria = new Criteria();
              criteria.addIn( HOST_ID, pks );
          objs = doSelect(criteria, dbcon);
        }
        return objs;
    }

 



            
                                              
                
                

    /**
     * selects a collection of Host objects pre-filled with their
     * HostType objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in HostPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinHostType(Criteria c)
        throws TorqueException
    {
        // Set the correct dbName if it has not been overridden
        // c.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (c.getDbName() == Torque.getDefaultDB())
        {
            c.setDbName(DATABASE_NAME);
        }

        HostPeer.addSelectColumns(c);
        int offset = numColumns + 1;
        HostTypePeer.addSelectColumns(c);


                        c.addJoin(HostPeer.HOST_TYPE_ID,
            HostTypePeer.HOST_TYPE_ID);
        

                                                                                                                                                                                                                                                                                                        
        List rows = BasePeer.doSelect(c);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = HostPeer.getOMClass();
                    Host obj1 = (Host) HostPeer
                .row2Object(row, 1, omClass);
                     omClass = HostTypePeer.getOMClass();
                    HostType obj2 = (HostType)HostTypePeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                Host temp_obj1 = (Host)results.get(j);
                HostType temp_obj2 = (HostType)temp_obj1.getHostType();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                    temp_obj2.addHost(obj1);
                    break;
                }
            }
            if (newObject)
            {
                obj2.initHosts();
                obj2.addHost(obj1);
            }
            results.add(obj1);
        }
        return results;
    }
                                                            
                
                

    /**
     * selects a collection of Host objects pre-filled with their
     * Device objects.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in HostPeer.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static List doSelectJoinDevice(Criteria c)
        throws TorqueException
    {
        // Set the correct dbName if it has not been overridden
        // c.getDbName will return the same object if not set to
        // another value so == check is okay and faster
        if (c.getDbName() == Torque.getDefaultDB())
        {
            c.setDbName(DATABASE_NAME);
        }

        HostPeer.addSelectColumns(c);
        int offset = numColumns + 1;
        DevicePeer.addSelectColumns(c);


                        c.addJoin(HostPeer.DEVICE_ID,
            DevicePeer.DEVICE_ID);
        

                                                                                                                                                                                                                                                                                                        
        List rows = BasePeer.doSelect(c);
        List results = new ArrayList();

        for (int i = 0; i < rows.size(); i++)
        {
            Record row = (Record) rows.get(i);

                            Class omClass = HostPeer.getOMClass();
                    Host obj1 = (Host) HostPeer
                .row2Object(row, 1, omClass);
                     omClass = DevicePeer.getOMClass();
                    Device obj2 = (Device)DevicePeer
                .row2Object(row, offset, omClass);

            boolean newObject = true;
            for (int j = 0; j < results.size(); j++)
            {
                Host temp_obj1 = (Host)results.get(j);
                Device temp_obj2 = (Device)temp_obj1.getDevice();
                if (temp_obj2.getPrimaryKey().equals(obj2.getPrimaryKey()))
                {
                    newObject = false;
                    temp_obj2.addHost(obj1);
                    break;
                }
            }
            if (newObject)
            {
                obj2.initHosts();
                obj2.addHost(obj1);
            }
            results.add(obj1);
        }
        return results;
    }
                    
  
    
  
      /**
     * Returns the TableMap related to this peer.  This method is not
     * needed for general use but a specific application could have a need.
     *
     * @throws TorqueException Any exceptions caught during processing will be
     *         rethrown wrapped into a TorqueException.
     */
    protected static TableMap getTableMap()
        throws TorqueException
    {
        return Torque.getDatabaseMap(DATABASE_NAME).getTable(TABLE_NAME);
    }
   }
