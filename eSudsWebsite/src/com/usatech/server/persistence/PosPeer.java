package com.usatech.server.persistence;

import java.util.List;

import org.apache.torque.TorqueException;
import org.apache.torque.util.Criteria;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Fri Feb 20 17:54:18 EST 2004]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class PosPeer
    extends com.usatech.server.persistence.BasePosPeer
{
	public static List retrieveByCustomerIdAndLocationId(long customerId, long locationId) throws TorqueException
	{
		Criteria criteria = new Criteria();
		criteria.add(CUSTOMER_ID, customerId);
		criteria.add(LOCATION_ID, locationId);
		criteria.addAscendingOrderByColumn(DEVICE_ID);
		
		return doSelect(criteria);
	}
	
	public static List retrieveByCustomerIdAndLocationId(long customerId, long locationId, boolean active) throws TorqueException
	{
		Criteria criteria = new Criteria();
		criteria.add(CUSTOMER_ID, customerId);
		criteria.add(LOCATION_ID, locationId);
		
		if (active)
		{
			criteria.add(POS_ACTIVE_YN_FLAG, "Y");
		}
		else
		{
			criteria.add(POS_ACTIVE_YN_FLAG, "N");
		}
		criteria.addAscendingOrderByColumn(DEVICE_ID);
		
		return doSelect(criteria);
	}
	
	public static List retrieveActivePos(long customerId, long locationId) throws TorqueException
	{
		Criteria criteria = new Criteria();
		criteria.add(CUSTOMER_ID, customerId);
		criteria.add(LOCATION_ID, locationId);
		criteria.add(POS_ACTIVE_YN_FLAG, "Y");
		criteria.addJoin(DEVICE_ID, DevicePeer.DEVICE_ID);
		criteria.add(DevicePeer.DEVICE_ACTIVE_YN_FLAG, "Y");
		criteria.add(DevicePeer.DEVICE_TYPE_ID, new Integer(5));
		criteria.addAscendingOrderByColumn(DEVICE_ID);
		
		return doSelect(criteria);
	}
}
