package com.usatech.server.persistence;

import java.util.List;

import org.apache.torque.TorqueException;
import org.apache.torque.util.Criteria;

/**
 * The skeleton for this class was autogenerated by Torque on:
 *
 * [Fri Feb 20 17:54:18 EST 2004]
 *
 *  You should add additional methods to this class to meet the
 *  application requirements.  This class will only be generated as
 *  long as it does not already exist in the output directory.
 */
public class ConsumerAcctPeer
    extends com.usatech.server.persistence.BaseConsumerAcctPeer
{
	public static List retrieveByConsumerAcctCdAndLocation(String cd, long locationId) throws TorqueException
	{
		Criteria criteria = new Criteria();
		criteria.add(CONSUMER_ACCT_CD, cd);
		//criteria.addJoin(CONSUMER_ID, ConsumerPeer.CONSUMER_ID);
		criteria.add(ConsumerAcctPeer.LOCATION_ID, locationId);
		
		return doSelect(criteria);
	}
	
	public static List retrieveByConsumerTypeName(String consumerType) throws TorqueException
	{
		Criteria criteria = new Criteria();
		criteria.addJoin(ConsumerAcctPeer.CONSUMER_ID, ConsumerPeer.CONSUMER_ID);
		criteria.addJoin(ConsumerPeer.CONSUMER_TYPE_ID, ConsumerTypePeer.CONSUMER_TYPE_ID);
		criteria.add(ConsumerTypePeer.CONSUMER_TYPE_DESC, consumerType);
		criteria.addAscendingOrderByColumn(ConsumerPeer.CONSUMER_LNAME);
		
		return doSelect(criteria);
	}
	
	public static List retrieveByConsumerTypeAndLocation(String consumerType, long locationId) throws TorqueException
	{
		Criteria criteria = new Criteria();
		criteria.addJoin(ConsumerAcctPeer.CONSUMER_ID, ConsumerPeer.CONSUMER_ID);
		criteria.add(ConsumerAcctPeer.LOCATION_ID, locationId);
		criteria.addJoin(ConsumerPeer.CONSUMER_TYPE_ID, ConsumerTypePeer.CONSUMER_TYPE_ID);
		criteria.add(ConsumerTypePeer.CONSUMER_TYPE_DESC, consumerType);
		criteria.addAscendingOrderByColumn(ConsumerPeer.CONSUMER_LNAME);
		
		return doSelect(criteria);
	}
}
