package com.usatech.server.persistence;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.torque.TorqueException;
import org.apache.torque.om.BaseObject;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.util.Transaction;

  
    
    
  
/**
 * This class was autogenerated by Torque on:
 *
 * [Tue Jul 13 14:25:33 EDT 2004]
 *
 * You should not use this class directly.  It should not even be
 * extended all references should be to AppUserObjectPermission
 */
public abstract class BaseAppUserObjectPermission extends BaseObject
{
    /** The Peer class */
    private static final AppUserObjectPermissionPeer peer =
        new AppUserObjectPermissionPeer();

        
    /** The value for the appUserId field */
    private long appUserId;
      
    /** The value for the appId field */
    private long appId;
      
    /** The value for the appObjectTypeId field */
    private long appObjectTypeId;
      
    /** The value for the objectCd field */
    private String objectCd;        
      
    /** The value for the allowObjectCreateYnFlag field */
    private String allowObjectCreateYnFlag;
      
    /** The value for the allowObjectReadYnFlag field */
    private String allowObjectReadYnFlag;
      
    /** The value for the allowObjectModifyYnFlag field */
    private String allowObjectModifyYnFlag;
      
    /** The value for the allowObjectDeleteYnFlag field */
    private String allowObjectDeleteYnFlag;
    
    /** The value for the objectId field */
    private long objectId;
  
    
    /**
     * Get the AppUserId
     *
     * @return long
     */
    public long getAppUserId()
    {
        return appUserId;
    }

                              
    /**
     * Set the value of AppUserId
     *
     * @param v new value
     */
    public void setAppUserId(long v) throws TorqueException
    {
    
                  if (this.appUserId != v)
              {
            this.appUserId = v;
            setModified(true);
        }
    
                          
                if (aAppUser != null && !(aAppUser.getAppUserId() == v))
                {
            aAppUser = null;
        }
      
              }
  
    /**
     * Get the AppId
     *
     * @return long
     */
    public long getAppId()
    {
        return appId;
    }

                              
    /**
     * Set the value of AppId
     *
     * @param v new value
     */
    public void setAppId(long v) throws TorqueException
    {
    
                  if (this.appId != v)
              {
            this.appId = v;
            setModified(true);
        }
    
                          
                if (aApp != null && !(aApp.getAppId() == v))
                {
            aApp = null;
        }
      
              }
  
    /**
     * Get the AppObjectTypeId
     *
     * @return long
     */
    public long getAppObjectTypeId()
    {
        return appObjectTypeId;
    }

                              
    /**
     * Set the value of AppObjectTypeId
     *
     * @param v new value
     */
    public void setAppObjectTypeId(long v) throws TorqueException
    {
    
                  if (this.appObjectTypeId != v)
              {
            this.appObjectTypeId = v;
            setModified(true);
        }
    
                          
                if (aAppObjectType != null && !(aAppObjectType.getAppObjectTypeId() == v))
                {
            aAppObjectType = null;
        }
      
              }
  
    /**
     * Get the ObjectCd
     *
     * @return String
     */
    public String getObjectCd()
    {
        return objectCd;
    }

                        
    /**
     * Set the value of ObjectCd
     *
     * @param v new value
     */
    public void setObjectCd(String v) 
    {
    
                  if (!ObjectUtils.equals(this.objectCd, v))
              {
            this.objectCd = v;
            setModified(true);
        }
    
          
              }
    
    /**
     * Get the ObjectId
     *
     * @return long
     */
    public long getObjectId()
    {
        return objectId;
    }

                        
    /**
     * Set the value of ObjectId
     *
     * @param v new value
     */
    public void setObjectId(long v) 
    {
    
                  if (!ObjectUtils.equals(this.objectId, v))
              {
            this.objectId = v;
            setModified(true);
        }
    
          
              }    
  
    /**
     * Get the AllowObjectCreateYnFlag
     *
     * @return String
     */
    public String getAllowObjectCreateYnFlag()
    {
        return allowObjectCreateYnFlag;
    }

                        
    /**
     * Set the value of AllowObjectCreateYnFlag
     *
     * @param v new value
     */
    public void setAllowObjectCreateYnFlag(String v) 
    {
    
                  if (!ObjectUtils.equals(this.allowObjectCreateYnFlag, v))
              {
            this.allowObjectCreateYnFlag = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AllowObjectReadYnFlag
     *
     * @return String
     */
    public String getAllowObjectReadYnFlag()
    {
        return allowObjectReadYnFlag;
    }

                        
    /**
     * Set the value of AllowObjectReadYnFlag
     *
     * @param v new value
     */
    public void setAllowObjectReadYnFlag(String v) 
    {
    
                  if (!ObjectUtils.equals(this.allowObjectReadYnFlag, v))
              {
            this.allowObjectReadYnFlag = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AllowObjectModifyYnFlag
     *
     * @return String
     */
    public String getAllowObjectModifyYnFlag()
    {
        return allowObjectModifyYnFlag;
    }

                        
    /**
     * Set the value of AllowObjectModifyYnFlag
     *
     * @param v new value
     */
    public void setAllowObjectModifyYnFlag(String v) 
    {
    
                  if (!ObjectUtils.equals(this.allowObjectModifyYnFlag, v))
              {
            this.allowObjectModifyYnFlag = v;
            setModified(true);
        }
    
          
              }
  
    /**
     * Get the AllowObjectDeleteYnFlag
     *
     * @return String
     */
    public String getAllowObjectDeleteYnFlag()
    {
        return allowObjectDeleteYnFlag;
    }

                        
    /**
     * Set the value of AllowObjectDeleteYnFlag
     *
     * @param v new value
     */
    public void setAllowObjectDeleteYnFlag(String v) 
    {
    
                  if (!ObjectUtils.equals(this.allowObjectDeleteYnFlag, v))
              {
            this.allowObjectDeleteYnFlag = v;
            setModified(true);
        }
    
          
              }
  
      
    
                  
    
        private AppUser aAppUser;

    /**
     * Declares an association between this object and a AppUser object
     *
     * @param v AppUser
     * @throws TorqueException
     */
    public void setAppUser(AppUser v) throws TorqueException
    {
            if (v == null)
        {
                    setAppUserId(0);
                  }
        else
        {
            setAppUserId(v.getAppUserId());
        }
                aAppUser = v;
    }

                                            
    /**
     * Get the associated AppUser object
     *
     * @return the associated AppUser object
     * @throws TorqueException
     */
    public AppUser getAppUser() throws TorqueException
    {
        if (aAppUser == null && (this.appUserId > 0))
        {
                          aAppUser = AppUserPeer.retrieveByPK(SimpleKey.keyFor(this.appUserId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               AppUser obj = AppUserPeer.retrieveByPK(this.appUserId);
               obj.addAppUserObjectPermissions(this);
            */
        }
        return aAppUser;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey.  e.g.
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
           */
    public void setAppUserKey(ObjectKey key) throws TorqueException
    {
      
                        setAppUserId(((NumberKey) key).longValue());
                  }
    
    
                  
    
        private App aApp;

    /**
     * Declares an association between this object and a App object
     *
     * @param v App
     * @throws TorqueException
     */
    public void setApp(App v) throws TorqueException
    {
            if (v == null)
        {
                    setAppId(0);
                  }
        else
        {
            setAppId(v.getAppId());
        }
                aApp = v;
    }

                                            
    /**
     * Get the associated App object
     *
     * @return the associated App object
     * @throws TorqueException
     */
    public App getApp() throws TorqueException
    {
        if (aApp == null && (this.appId > 0))
        {
                          aApp = AppPeer.retrieveByPK(SimpleKey.keyFor(this.appId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               App obj = AppPeer.retrieveByPK(this.appId);
               obj.addAppUserObjectPermissions(this);
            */
        }
        return aApp;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey.  e.g.
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
           */
    public void setAppKey(ObjectKey key) throws TorqueException
    {
      
                        setAppId(((NumberKey) key).longValue());
                  }
    
    
                  
    
        private AppObjectType aAppObjectType;

    /**
     * Declares an association between this object and a AppObjectType object
     *
     * @param v AppObjectType
     * @throws TorqueException
     */
    public void setAppObjectType(AppObjectType v) throws TorqueException
    {
            if (v == null)
        {
                    setAppObjectTypeId(0);
                  }
        else
        {
            setAppObjectTypeId(v.getAppObjectTypeId());
        }
                aAppObjectType = v;
    }

                                            
    /**
     * Get the associated AppObjectType object
     *
     * @return the associated AppObjectType object
     * @throws TorqueException
     */
    public AppObjectType getAppObjectType() throws TorqueException
    {
        if (aAppObjectType == null && (this.appObjectTypeId > 0))
        {
                          aAppObjectType = AppObjectTypePeer.retrieveByPK(SimpleKey.keyFor(this.appObjectTypeId));
              
            /* The following can be used instead of the line above to
               guarantee the related object contains a reference
               to this object, but this level of coupling
               may be undesirable in many circumstances.
               As it can lead to a db query with many results that may
               never be used.
               AppObjectType obj = AppObjectTypePeer.retrieveByPK(this.appObjectTypeId);
               obj.addAppUserObjectPermissions(this);
            */
        }
        return aAppObjectType;
    }

    /**
     * Provides convenient way to set a relationship based on a
     * ObjectKey.  e.g.
     * <code>bar.setFooKey(foo.getPrimaryKey())</code>
     *
           */
    public void setAppObjectTypeKey(ObjectKey key) throws TorqueException
    {
      
                        setAppObjectTypeId(((NumberKey) key).longValue());
                  }
       
                
    private static List fieldNames = null;

    /**
     * Generate a list of field names.
     *
     * @return a list of field names
     */
    public static synchronized List getFieldNames()
    {
        if (fieldNames == null)
        {
            fieldNames = new ArrayList();
              fieldNames.add("AppUserId");
              fieldNames.add("AppId");
              fieldNames.add("AppObjectTypeId");
              fieldNames.add("ObjectCd");
              fieldNames.add("AllowObjectCreateYnFlag");
              fieldNames.add("AllowObjectReadYnFlag");
              fieldNames.add("AllowObjectModifyYnFlag");
              fieldNames.add("AllowObjectDeleteYnFlag");
              fieldNames.add("ObjectId");
              fieldNames = Collections.unmodifiableList(fieldNames);
        }
        return fieldNames;
    }

    /**
     * Retrieves a field from the object by name passed in as a String.
     *
     * @param name field name
     * @return value
     */
    public Object getByName(String name)
    {
          if (name.equals("AppUserId"))
        {
                return new Long(getAppUserId());
            }
          if (name.equals("AppId"))
        {
                return new Long(getAppId());
            }
          if (name.equals("AppObjectTypeId"))
        {
                return new Long(getAppObjectTypeId());
            }
          if (name.equals("ObjectCd"))
        {
                return getObjectCd();
            }
          if (name.equals("AllowObjectCreateYnFlag"))
        {
                return getAllowObjectCreateYnFlag();
            }
          if (name.equals("AllowObjectReadYnFlag"))
        {
                return getAllowObjectReadYnFlag();
            }
          if (name.equals("AllowObjectModifyYnFlag"))
        {
                return getAllowObjectModifyYnFlag();
            }
          if (name.equals("AllowObjectDeleteYnFlag"))
        {
                return getAllowObjectDeleteYnFlag();
            }
          if (name.equals("ObjectId"))
          {
                  return getObjectId();
              }
          return null;
    }
    
    /**
     * Retrieves a field from the object by name passed in
     * as a String.  The String must be one of the static
     * Strings defined in this Class' Peer.
     *
     * @param name peer name
     * @return value
     */
    public Object getByPeerName(String name)
    {
          if (name.equals(AppUserObjectPermissionPeer.APP_USER_ID))
        {
                return new Long(getAppUserId());
            }
          if (name.equals(AppUserObjectPermissionPeer.APP_ID))
        {
                return new Long(getAppId());
            }
          if (name.equals(AppUserObjectPermissionPeer.APP_OBJECT_TYPE_ID))
        {
                return new Long(getAppObjectTypeId());
            }
          if (name.equals(AppUserObjectPermissionPeer.OBJECT_CD))
        {
                return getObjectCd();
            }
          if (name.equals(AppUserObjectPermissionPeer.ALLOW_OBJECT_CREATE_YN_FLAG))
        {
                return getAllowObjectCreateYnFlag();
            }
          if (name.equals(AppUserObjectPermissionPeer.ALLOW_OBJECT_READ_YN_FLAG))
        {
                return getAllowObjectReadYnFlag();
            }
          if (name.equals(AppUserObjectPermissionPeer.ALLOW_OBJECT_MODIFY_YN_FLAG))
        {
                return getAllowObjectModifyYnFlag();
            }
          if (name.equals(AppUserObjectPermissionPeer.ALLOW_OBJECT_DELETE_YN_FLAG))
        {
                return getAllowObjectDeleteYnFlag();
            }
          if (name.equals(AppUserObjectPermissionPeer.OBJECT_ID))
          {
                  return getObjectId();
              }
          return null;
    }

    /**
     * Retrieves a field from the object by Position as specified
     * in the xml schema.  Zero-based.
     *
     * @param pos position in xml schema
     * @return value
     */
    public Object getByPosition(int pos)
    {
            if (pos == 0)
        {
                return new Long(getAppUserId());
            }
              if (pos == 1)
        {
                return new Long(getAppId());
            }
              if (pos == 2)
        {
                return new Long(getAppObjectTypeId());
            }
              if (pos == 3)
        {
                return getObjectCd();
            }
              if (pos == 4)
        {
                return getAllowObjectCreateYnFlag();
            }
              if (pos == 5)
        {
                return getAllowObjectReadYnFlag();
            }
              if (pos == 6)
        {
                return getAllowObjectModifyYnFlag();
            }
              if (pos == 7)
        {
                return getAllowObjectDeleteYnFlag();
            }
              if (pos == 8)
              {
                      return getObjectId();
                  }
              return null;
    }
     
    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
     *
     * @throws Exception
     */
    public void save() throws Exception
    {
          save(AppUserObjectPermissionPeer.getMapBuilder()
                .getDatabaseMap().getName());
      }

    /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.
       * Note: this code is here because the method body is
     * auto-generated conditionally and therefore needs to be
     * in this file instead of in the super class, BaseObject.
       *
     * @param dbName
     * @throws TorqueException
     */
    public void save(String dbName) throws TorqueException
    {
        Connection con = null;
          try
        {
            con = Transaction.begin(dbName);
            save(con);
            Transaction.commit(con);
        }
        catch(TorqueException e)
        {
            Transaction.safeRollback(con);
            throw e;
        }
      }

      /** flag to prevent endless save loop, if this object is referenced
        by another object which falls in this transaction. */
    private boolean alreadyInSave = false;
      /**
     * Stores the object in the database.  If the object is new,
     * it inserts it; otherwise an update is performed.  This method
     * is meant to be used as part of a transaction, otherwise use
     * the save() method and the connection details will be handled
     * internally
     *
     * @param con
     * @throws TorqueException
     */
    public void save(Connection con) throws TorqueException
    {
          if (!alreadyInSave)
        {
            alreadyInSave = true;


  
            // If this object has been modified, then save it to the database.
            if (isModified())
            {
                if (isNew())
                {
                    AppUserObjectPermissionPeer.doInsert((AppUserObjectPermission) this, con);
                    setNew(false);
                }
                else
                {
                    AppUserObjectPermissionPeer.doUpdate((AppUserObjectPermission) this, con);
                }
            }

                      alreadyInSave = false;
        }
      }


    
  
    /**
     * returns an id that differentiates this object from others
     * of its class.
     */
    public ObjectKey getPrimaryKey()
    {
          return null;
      }

 

    /**
     * Makes a copy of this object.
     * It creates a new object filling in the simple attributes.
       * It then fills all the association collections and sets the
     * related objects to isNew=true.
       */
      public AppUserObjectPermission copy() throws TorqueException
    {
        return copyInto(new AppUserObjectPermission());
    }
  
    protected AppUserObjectPermission copyInto(AppUserObjectPermission copyObj) throws TorqueException
    {
          copyObj.setAppUserId(appUserId);
          copyObj.setAppId(appId);
          copyObj.setAppObjectTypeId(appObjectTypeId);
          copyObj.setObjectCd(objectCd);
          copyObj.setAllowObjectCreateYnFlag(allowObjectCreateYnFlag);
          copyObj.setAllowObjectReadYnFlag(allowObjectReadYnFlag);
          copyObj.setAllowObjectModifyYnFlag(allowObjectModifyYnFlag);
          copyObj.setAllowObjectDeleteYnFlag(allowObjectDeleteYnFlag);
          copyObj.setObjectId(objectId);
                                                  
        
        return copyObj;
    }

    /**
     * returns a peer instance associated with this om.  Since Peer classes
     * are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     */
    public AppUserObjectPermissionPeer getPeer()
    {
        return peer;
    }

    public String toString()
    {
        StringBuffer str = new StringBuffer();
        str.append("AppUserObjectPermission:\n");
        str.append("AppUserId = ")
           .append(getAppUserId())
           .append("\n");
        str.append("AppId = ")
           .append(getAppId())
           .append("\n");
        str.append("AppObjectTypeId = ")
           .append(getAppObjectTypeId())
           .append("\n");
        str.append("ObjectCd = ")
           .append(getObjectCd())
           .append("\n");
        str.append("AllowObjectCreateYnFlag = ")
           .append(getAllowObjectCreateYnFlag())
           .append("\n");
        str.append("AllowObjectReadYnFlag = ")
           .append(getAllowObjectReadYnFlag())
           .append("\n");
        str.append("AllowObjectModifyYnFlag = ")
           .append(getAllowObjectModifyYnFlag())
           .append("\n");
        str.append("AllowObjectDeleteYnFlag = ")
           .append(getAllowObjectDeleteYnFlag())
           .append("\n");
        str.append("ObjectId = ")
        	.append(getObjectId())
        	.append("\n");
        return(str.toString());
    }
}
