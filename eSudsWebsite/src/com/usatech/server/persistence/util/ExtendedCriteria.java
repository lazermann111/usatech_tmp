/*
 * *******************************************************************
 * 
 * File ExtendedCriteria.java
 * 
 * Created on May 12, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.server.persistence.util;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.torque.util.Criteria;

/**
 * @author melissa
 *
 */
public class ExtendedCriteria extends Criteria
{
	private static final int DEFAULT_CAPACITY = 10;
	
	/**
	 * 
	 */
	public ExtendedCriteria()
	{
		super(DEFAULT_CAPACITY);
	}
	/**
	 * @param initialCapacity
	 */
	public ExtendedCriteria(int initialCapacity)
	{
		super(initialCapacity);

	}
	/**
	 * @param dbName
	 */
	public ExtendedCriteria(String dbName)
	{
		super(dbName);
	}
	/**
	 * @param dbName
	 * @param initialCapacity
	 */
	public ExtendedCriteria(String dbName, int initialCapacity)
	{
		super(dbName, initialCapacity);
	}
	
	public ExtendedCriteria greaterThan(String columnName, int columnValue)
	{
		super.add(columnName, columnValue, Criteria.GREATER_THAN);
		return this;
	}
	
	public ExtendedCriteria greaterThan(String columnName, long columnValue)
	{
		super.add(columnName, columnValue, Criteria.GREATER_THAN);
		return this;
	}
	
	public ExtendedCriteria greaterThan(String columnName, BigDecimal columnValue)
	{
		super.add(columnName, columnValue, Criteria.GREATER_THAN);
		return this;
	}

	public ExtendedCriteria lessThan(String columnName, int columnValue)
	{
		super.add(columnName, columnValue, Criteria.LESS_THAN);
		return this;
	}
	
	public ExtendedCriteria lessThan(String columnName, long columnValue)
	{
		super.add(columnName, columnValue, Criteria.LESS_THAN);
		return this;
	}
	
	public ExtendedCriteria lessThan(String columnName, BigDecimal columnValue)
	{
		super.add(columnName, columnValue, Criteria.LESS_THAN);
		return this;
	}
	
	public ExtendedCriteria greaterThanOrEqual(String columnName, int columnValue)
	{
		super.add(columnName, columnValue, Criteria.GREATER_EQUAL);
		return this;
	}
	
	public ExtendedCriteria greaterThanOrEqual(String columnName, long columnValue)
	{
		super.add(columnName, columnValue, Criteria.GREATER_EQUAL);
		return this;
	}
	
	public ExtendedCriteria greaterThanOrEqual(String columnName, BigDecimal columnValue)
	{
		super.add(columnName, columnValue, Criteria.GREATER_EQUAL);
		return this;
	}

	public ExtendedCriteria lessThanOrEqual(String columnName, int columnValue)
	{
		super.add(columnName, columnValue, Criteria.LESS_EQUAL);
		return this;
	}

	public ExtendedCriteria lessThanOrEqual(String columnName, long columnValue)
	{
		super.add(columnName, columnValue, Criteria.LESS_EQUAL);
		return this;
	}
	
	public ExtendedCriteria lessThanOrEqual(String columnName, BigDecimal columnValue)
	{
		super.add(columnName, columnValue, Criteria.LESS_EQUAL);
		return this;
	}
	
	public ExtendedCriteria isNull(String columnName)
	{
		super.add(columnName, Criteria.ISNULL);
		return this;
	}
	
	public ExtendedCriteria isBetweenExcludeBounds(String columnName, Date minDate, Date maxDate)
	{
		super.add(columnName, minDate, Criteria.GREATER_THAN);
		Criterion criterion = this.getCriterion(columnName);
		criterion.and(this.getNewCriterion(criterion.getTable(), criterion.getColumn(), maxDate, Criteria.LESS_THAN));
		return this;
	}
	
	public ExtendedCriteria isBetweenIncludeBounds(String columnName, Date minDate, Date maxDate)
	{
		super.add(columnName, minDate, Criteria.GREATER_EQUAL);
		Criterion criterion = this.getCriterion(columnName);
		criterion.and(this.getNewCriterion(criterion.getTable(), criterion.getColumn(), maxDate, Criteria.LESS_EQUAL));
		return this;
	}
	
	public ExtendedCriteria addSelectColumns(List columns)
	{
		if (columns == null)
		{
			throw new NullPointerException("columns can not be null");
		}
		
		for (Iterator i = columns.iterator(); i.hasNext();)
		{
			Object obj = i.next();
			
			if (obj.getClass().isArray())
			{
				for (int j=0; j < Array.getLength(obj); j++)
				{
					String column = (String) Array.get(obj, j);
					this.addSelectColumn(column);
				}
			}
			else
			{
				String column = (String) obj;
				this.addSelectColumn(column);
			}
		}
		
		return this;
	}
}
