package com.usatech.security.jaas.auth;

import java.io.IOException;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextInputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.NoRowsException;
import org.apache.torque.TooManyRowsException;
import org.apache.torque.TorqueException;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;

import com.usatech.security.jaas.*;
import com.usatech.server.persistence.AppUser;
import com.usatech.server.persistence.AppUserObjectPermission;
import com.usatech.server.persistence.AppUserObjectPermissionPeer;
import com.usatech.server.persistence.AppUserPeer;

public class DatabaseLoginModule implements LoginModule {
    private static Log log = LogFactory.getLog(DatabaseLoginModule.class);

    private CallbackHandler callbackHandler;

    private Subject subject;

    private Map sharedState;

    private Map options;

    private String username;

    private String password;

    private String application;

    private long userId;

    private String name = null;

    //saved state
    private boolean success = false;

    /**
     * Constructor for DatabaseLoginModule.
     */
    public DatabaseLoginModule() {
        super();
    }

    /**
     * @see javax.security.auth.spi.LoginModule#initialize(Subject,
     *      CallbackHandler, Map, Map)
     */
    public void initialize(Subject sub, CallbackHandler handler, Map shared, Map opt) {
        subject = sub;
        callbackHandler = handler;
        sharedState = shared;
        options = opt;
    }

    /**
     * @see javax.security.auth.spi.LoginModule#login()
     */
    public boolean login() throws LoginException {
        if (callbackHandler == null) {
            throw new LoginException("No CallbackHandler available.");
        }

        Callback[] callbacks = new Callback[3];

        //params for the following constructors are not used
        callbacks[0] = new NameCallback("username");
        callbacks[1] = new PasswordCallback("password", false);
        callbacks[2] = new TextInputCallback("application");
        try {
            callbackHandler.handle(callbacks);
            username = ((NameCallback) callbacks[0]).getName();
            password = new String(((PasswordCallback) callbacks[1]).getPassword());
            application = ((TextInputCallback) callbacks[2]).getText();
        } catch (UnsupportedCallbackException uce) {
            //handle exception
            log.warn("Unsupported Callback", uce);
            throw new LoginException(uce.getCallback().toString() + " not available");
        } catch (IOException ioe) {
            //handle exception, should not happen
            log.warn("IO Exception", ioe);
            throw new LoginException(ioe.toString());
        }

        log.debug("Attempting to log '" + username + "' into " + application);
        //Access the database to authenticate the user
        Connection con = null;

        try {
            con = DataLayerMgr.getConnection("SECURITY");

            CallableStatement cs = con
                    .prepareCall("{call pkg_application_security.sp_get_user_access(?, ?, ?, ?, ?, ?, ?)}");
            cs.setString(1, username);
            cs.setString(2, password);
            cs.setString(3, application);
            cs.registerOutParameter(4, Types.BIGINT);
            cs.registerOutParameter(5, Types.CHAR);
            cs.registerOutParameter(6, Types.FLOAT);
            cs.registerOutParameter(7, Types.VARCHAR);

            cs.execute();

            long id = cs.getLong(4);
            String flag = cs.getString(5);
            float return_code = ConvertUtils.convert(Float.class, cs.getObject(6));
            String error_message = cs.getString(7);

            if (cs.getString(7) != null) {
                success = false;
                log.info("Login failed with return code " + return_code + " and error message "
                        + error_message);
            } else {
                success = true;
                userId = cs.getLong(4);
            }
        } /*catch (NamingException ne) {
            //do something with error
            log.error("While checking login", ne);
            throw new LoginException("Naming Error:" + ne.toString());

        }*/ catch (SQLException se) {
            //handle sql exception
            log.error("While checking login", se);
            throw new LoginException("Database Error:" + se.toString());
        } catch (Exception e) {
            log.error("While checking login", e);
            throw new LoginException(e.toString());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException se) {
                    // ignore
                    log.warn("Could not close connection: " + se);
                }
            }
        }

        return success;
    }

    public static void populateSubject(Subject subject, long userId, String name) throws LoginException {
    	// Assign Principals
        Set principals = subject.getPrincipals();
        // userId
        principals.add(new UserPrincipal("" + userId));
        // name
        principals.add(new NamePrincipal(name));

        // Set Credentials for this app
        Set credentials = subject.getPublicCredentials();
        List permissions = null;
        try {
            permissions = AppUserObjectPermissionPeer.retrieveByAppUserId(userId);
        } catch (TorqueException te) {
        	LoginException le = new LoginException("Error retreiving permissions: " + te.getMessage());
        	le.initCause(te);
            throw le;
        }

        for (Iterator i = permissions.iterator(); i.hasNext();) {
            AppUserObjectPermission perm = (AppUserObjectPermission) i.next();
            AppCredential credential = new AppCredential();

            // set object type
            try {
                credential.setObjectType(perm.getAppObjectType().getAppObjectCd());
            } catch (TorqueException te) {
                LoginException le = new LoginException("Error getting AppObjectType from AppUserObjectPermission: " + te.getMessage());
            	le.initCause(te);
                throw le;
            }

            // set object Id
            if (perm.getObjectCd().equals("*")) {
                credential.setObjectId(AppCredential.ALL_OBJECTS);
            } else {
                credential.setObjectId(Long.parseLong(perm.getObjectCd()));
            }

            // set actions
            String actions = new String();

            if (perm.getAllowObjectCreateYnFlag().equalsIgnoreCase("Y")) {
                actions += AppCredential.CREATE_ACTION + ", ";
            }
            if (perm.getAllowObjectReadYnFlag().equalsIgnoreCase("Y")) {
                actions += AppCredential.READ_ACTION + ", ";
            }
            if (perm.getAllowObjectModifyYnFlag().equalsIgnoreCase("Y")) {
                actions += AppCredential.MODIFY_ACTION + ", ";
            }
            if (perm.getAllowObjectDeleteYnFlag().equalsIgnoreCase("Y")) {
                actions += AppCredential.DELETE_ACTION;
            }

            credential.setActions(actions);

            try {
                credential.setApp(perm.getApp().getAppCd());
            } catch (TorqueException te) {
                LoginException le = new LoginException("Error getting App from AppUserObjectPermission: " + te.getMessage());
            	le.initCause(te);
                throw le;
            }

            credentials.add(credential);
        }
    }
    /**
     * @see javax.security.auth.spi.LoginModule#commit()
     */
    public boolean commit() throws LoginException {
        if (success) {
        	try {
                populateSubject(subject, userId, username);
            } catch (LoginException e) {
                log.error(e.getMessage(), e);
                return false;
            }
        }

        //Reset values
        username = null;
        password = null;
        application = null;
        name = null;
        userId = -1;

        return true;
    }

    /**
     * @see javax.security.auth.spi.LoginModule#abort()
     */
    public boolean abort() throws LoginException {
        username = null;
        password = null;
        userId = -1;

        return true;
    }

    /**
     * @see javax.security.auth.spi.LoginModule#logout()
     */
    public boolean logout() throws LoginException {
        return false;
    }

    private boolean loginUsingTorque() {
        AppUser user = null;
        try {
            user = AppUserPeer.retrieveByUsername(username);
        } catch (NoRowsException nre) {
            log.debug("Login Failed: user was not found.");
            return false;
        } catch (TooManyRowsException tmre) {
            log.error("Loging Failed: more than one user with username " + username);
            return false;
        } catch (TorqueException te) {
            log.error("Error accessing database: " + te.getMessage());
            return false;
        }

        if (user != null && user.isActive() && user.getAppUserPassword().equals(password)) {
            success = true;
            userId = user.getAppUserId();

            if (user.getAppUserFname() != null && user.getAppUserLname() != null) {
                name = user.getAppUserFname() + " " + user.getAppUserLname();
            }
        } else {
            success = false;
        }

        return success;
    }

}