package com.usatech.security.jaas.auth;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextInputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * @author melissa
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class FormCallbackHandler implements CallbackHandler {

	/**
	 * @see javax.security.auth.callback.CallbackHandler#handle(Callback[])
	 */
	
	private String username;
	private char[] password;
	private String application;
	
	public FormCallbackHandler(String user, String pass, String app)
	{
		username = user;
		password = pass.toCharArray();
		application = app;
	}
	
	public void handle(Callback[] callbacks)
		throws IOException, UnsupportedCallbackException
	{
		for (int i = 0; i < callbacks.length; i++)
		{
			Callback cback = callbacks[i];
			
			if (cback instanceof NameCallback)
			{
				((NameCallback) callbacks[i]).setName(username);
			}
			else if (cback instanceof PasswordCallback)
			{
				((PasswordCallback) callbacks[i]).setPassword(password);
			}
			else if (cback instanceof TextInputCallback)
			{
				((TextInputCallback) callbacks[i]).setText(application);
			}
			else
			{
				throw new UnsupportedCallbackException(callbacks[i], "Unsupported Callback class.");
			}
		}
		
	}
	
	public void clearPassword()
	{
		for (int i=0; i < password.length; i++)
		{
			password[i] = ' ';
		}
		password = null;
	}

	/**
	 * Returns the username.
	 * @return String
	 */
	public String getUsername()
	{
		return username;
	}

}
