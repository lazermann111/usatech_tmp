/*
 * *******************************************************************
 * 
 * File AppCredential.java
 * 
 * Created on Mar 30, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.security.jaas;

import java.util.StringTokenizer;

public class AppCredential
{
	static private int READ = 0x01;
	static private int MODIFY = 0x02;
	static private int CREATE = 0x04;
	static private int DELETE = 0x08;
	
	static final public String READ_ACTION = "read";
	static final public String MODIFY_ACTION = "modify";
	static final public String CREATE_ACTION = "create";
	static final public String DELETE_ACTION = "delete";
	static final public long ALL_OBJECTS = -1;
	
	private String objectType = null;
	private long objectId;
	private int mask = 0;
	private String app = null;
	
	/**
	 * 
	 */
	public AppCredential()
	{
		super();
	}
	
	public String getActions()
	{
		StringBuffer buf = new StringBuffer();

		if( (mask & READ) == READ )
		{
			buf.append(READ_ACTION);
		}
		if( (mask & MODIFY) == MODIFY )
		{
			if(buf.length() > 0)
			{
				buf.append(", ");
			}
			buf.append(MODIFY_ACTION);
		}
		if( (mask & CREATE) == CREATE )
		{
			if(buf.length() > 0)
			{
				buf.append(", ");
			}
			buf.append(CREATE_ACTION);
		}
		if( (mask & DELETE) == DELETE )
		{
			if(buf.length() > 0)
			{
				buf.append(", ");
			}
			buf.append(DELETE_ACTION);
		}

		return buf.toString();
	}
	
	public boolean implies (AppCredential credential)
	{
		// objectType must be equal
		if (!objectType.equals(credential.getObjectType()))
		{
			return false;
		}
		
		// objectId must be the same or this credential is all objects
		if (!(objectId == credential.getObjectId() || objectId == ALL_OBJECTS))
		{
			return false;
		}
		
		if (!app.equals(credential.getApp()))
		{
			return false;
		}
		
		// check mask - must have at least the test credentials actions
		if ((mask & credential.getMask()) != credential.getMask())
		{
			return false;
		}

		// we made it through all tests, return true		
		return true;
	}
	/**
	 * @return
	 */
	protected int getMask()
	{
		return mask;
	}

	/**
	 * @return
	 */
	public long getObjectId()
	{
		return objectId;
	}

	/**
	 * @return
	 */
	public String getObjectType()
	{
		return objectType;
	}

	/**
	 * @param i
	 */
	protected void setMask(int i)
	{
		mask = i;
	}

	/**
	 * @param l
	 */
	public void setObjectId(long l)
	{
		objectId = l;
	}

	/**
	 * @param string
	 */
	public void setObjectType(String string)
	{
		objectType = string;
	}
	
	public void setActions(String actions)
	{
		parseActions(actions);
	}
	
	private void parseActions(String _actions)
	{
		mask = 0;

		if(_actions != null)
		{
			StringTokenizer tokenizer = new StringTokenizer(_actions, ",\t ");
			while(tokenizer.hasMoreTokens())
			{
				String token = tokenizer.nextToken();
				if(token.equals(READ_ACTION))
				{
					mask |= READ;
				}
				else if(token.equals(MODIFY_ACTION))
				{
					mask |= MODIFY;
				}
				else if(token.equals(CREATE_ACTION))
				{
					mask |= CREATE;
				}
				else if(token.equals(DELETE_ACTION))
				{
					mask |= DELETE;
				}
				else
				{
					throw new IllegalArgumentException("Unknown action: " + token);
				}
			}
		}
		else
		{
			mask = 0;
		}
	}

	/**
	 * @return
	 */
	public String getApp()
	{
		return app;
	}

	/**
	 * @param string
	 */
	public void setApp(String string)
	{
		app = string;
	}

}
