/*
 * *******************************************************************
 * 
 * File NamePrincipal.java
 * 
 * Created on Mar 30, 2004
 *
 * Create by melissa
 * 
 * *******************************************************************
 * 
 * Updates: 
 * 
 * *******************************************************************
 */
package com.usatech.security.jaas;

import java.io.Serializable;
import java.security.Principal;

public class NamePrincipal implements Principal, Serializable
{
	private String name = null;
	/**
	 * 
	 */
	public NamePrincipal()
	{
		super();
		name = "user";
	}
	
	public NamePrincipal(String _name)
	{
		super();
		name = _name;
	}
	
	/* (non-Javadoc)
	 * @see java.security.Principal#getName()
	 */
	public String getName()
	{
		return name;
	}
}
