package com.usatech.security.jaas;

import java.security.Principal;

/**
 * @author melissa
 *
 */
public class UserPrincipal implements Principal, java.io.Serializable
{

	/**
	 * @see java.security.Principal#getName()
	 */

	private String name;
	
	public UserPrincipal()
	{
		name = "0";
	}
	
	public UserPrincipal(String _name)
	{
		name = _name;
	}
		
	public boolean equals(Object o)
	{
		if (o == null)
		{
			return false;
		}
		
		if (this == o)
		{
			return true;
		}
		
		if (o instanceof UserPrincipal)
		{
			String oname = ((UserPrincipal) o).getName();
			if (oname == null && name == null || oname != null && oname.equals(name))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;	
		}
	}

	public String getName()
	{
		return name;
	}

}
