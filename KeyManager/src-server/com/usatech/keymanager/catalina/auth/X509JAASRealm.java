package com.usatech.keymanager.catalina.auth;

import java.io.IOException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.*;

import org.apache.catalina.Context;
import org.apache.catalina.connector.*;
import org.apache.catalina.deploy.SecurityConstraint;
import org.apache.commons.logging.*;

import com.usatech.keymanager.util.CertUtil;

public class X509JAASRealm extends org.apache.catalina.realm.JAASRealm
{
	private static Log	log	= LogFactory.getLog(X509JAASRealm.class);

	public void setUserClassNames(String userClassNames)
	{
		this.userClassNames = userClassNames;
		parseClassNames(userClassNames, userClasses);
	}

	protected void parseClassNames(String classNamesString, List classNamesList)
	{
		classNamesList.clear();
		if (classNamesString == null)
			return;

		String[] classNames = classNamesString.split("[ ]*,[ ]*");
		for (int i = 0; i < classNames.length; i++)
		{
			if (classNames[i].length() == 0)
				continue;
			
			classNamesList.add(classNames[i]);
			
			try
			{
				Class principalClass = Class.forName(classNames[i]);
				if(!Principal.class.isAssignableFrom(principalClass))
				{
					log.warn("Class " + classNames[i] + " is not implementing " + "java.security.Principal!");
				}
			}
			catch (ClassNotFoundException e)
			{
				log.warn("Class " + classNames[i] + " not found!");
			}
		}
	}

	protected Principal getPrincipal(X509Certificate cert)
	{
		long st = System.currentTimeMillis();
        String subjectDN = cert.getSubjectDN().getName();

		log.debug("GET_PRINCIPAL: " + subjectDN);

        String certThumbprint;
        try
        {
            certThumbprint = CertUtil.getCertThumbprint(cert);
        }
        catch(Exception e)
        {
            log.error("Failed to generate certificate thumbprint for " + subjectDN + ": " + e.getMessage());
            return null;
        }        
        
        String certBase64;
        try
        {
            certBase64 =  com.usatech.keymanager.util.CertUtil.getCertBase64(cert);
        }
        catch(Exception e)
        {
            log.error("Failed to generate certBase64 for " + subjectDN + ": " + e.getMessage());
            return null;
        }
        
        String userInfo = subjectDN + "; " + certThumbprint + "; " + cert.getNotAfter();
		try
		{
			Principal principal = authenticate(userInfo, certBase64); //userInfo as username, certBase64 as password
			log.info("GET_PRINCIPAL_SUCCESS: " + userInfo);
			log.debug("getPrincipal: " + userInfo + ": " + (System.currentTimeMillis() - st));
			return principal;
		}
		catch (Exception e)
		{
			log.error("GET_PRINCIPAL_FAILURE: " + userInfo + ": " + e.getMessage());
			log.debug("getPrincipal failed for " + userInfo + ": " + e.getMessage(), e);
			return null;
		}
	}

	public boolean hasResourcePermission(Request request, Response response, SecurityConstraint[] constraints, Context context) throws IOException
	{
		long st = System.currentTimeMillis();

		Principal principal = request.getPrincipal();
        String userInfo = principal == null ? "NULL Principal" : principal.getName();
            
		log.debug("ACCESS_CHECK: " + request.getRemoteAddr() + " - " + userInfo);

		try
		{
			if (super.hasResourcePermission(request, response, constraints, context))
			{
				log.info("ACCESS_CHECK_OK: " + request.getRemoteAddr() + " - " + userInfo);
				log.debug("hasResourcePermission: " + (System.currentTimeMillis() - st));
				return true;
			}
			else
			{
				log.info("ACCESS_CHECK_DENIED: " + request.getRemoteAddr() + " - " + userInfo);
				return false;
			}
		}
		catch (Throwable e)
		{
			log.error("ACCESS_CHECK_FAILURE: " + request.getRemoteAddr() + " - " + userInfo + " : " + e.getMessage());
			log.debug("hasResourcePermission failure: " + e.getMessage(), e);
			return false;
		}
	}
}
