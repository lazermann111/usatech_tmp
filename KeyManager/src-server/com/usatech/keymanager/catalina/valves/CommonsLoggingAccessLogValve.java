package com.usatech.keymanager.catalina.valves;

import java.io.*;
import java.net.InetAddress;
import java.text.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.apache.catalina.*;
import org.apache.catalina.connector.*;
import org.apache.catalina.util.*;
import org.apache.catalina.valves.ValveBase;
import org.apache.catalina.valves.Constants;
import org.apache.commons.logging.*;

public class CommonsLoggingAccessLogValve extends ValveBase implements Lifecycle
{
	private static Log		log				= LogFactory.getLog("ACCESS_LOG");
	
    /**
     * The descriptive information about this implementation.
     */
    protected static final String info = "com.usatech.keymanager.catalina.valves.CommonsLoggingAccessLogValve/1.0";

	/**
	 * When formatting log lines, we often use strings like this one (" ").
	 */
	private static String					space				= " ";
    
	/**
	 * The set of log levels
	 */
	protected static final String	LEVELS[]			= { "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" };
	
	/**
	 * The set of month abbreviations for log messages.
	 */
	protected static final String	months[]			= { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	/**
	 * The lifecycle event support for this component.
	 */
	protected LifecycleSupport		lifecycle			= new LifecycleSupport(this);
	
	/**
	 * The current log level.  Default: INFO
	 */
	private int						level				= 2;

	/**
	 * If the current log pattern is the same as the common access log
	 * format pattern, then we'll set this variable to true and log in
	 * a more optimal and hard-coded way.
	 */
	private boolean					common				= false;

	/**
	 * For the combined format (common, plus useragent and referer), we do
	 * the same
	 */
	private boolean					combined			= false;

	/**
	 * The pattern used to format our access log lines.
	 */
	private String					pattern				= null;


	/**
	 * Has this component been started yet?
	 */
	private boolean					started				= false;

	/**
	 * A date formatter to format Dates into a day string in the format
	 * "dd".
	 */
	private SimpleDateFormat		dayFormatter		= null;

	/**
	 * A date formatter to format a Date into a month string in the format
	 * "MM".
	 */
	private SimpleDateFormat		monthFormatter		= null;

	/**
	 * Time taken formatter for 3 decimal places.
	 */
	private DecimalFormat			timeTakenFormatter	= null;

	/**
	 * A date formatter to format a Date into a year string in the format
	 * "yyyy".
	 */
	private SimpleDateFormat		yearFormatter		= null;

	/**
	 * A date formatter to format a Date into a time in the format
	 * "kk:mm:ss" (kk is a 24-hour representation of the hour).
	 */
	private SimpleDateFormat		timeFormatter		= null;

	/**
	 * The system timezone.
	 */
	private TimeZone				timezone			= null;

	/**
	 * The time zone offset relative to GMT in text form when daylight saving
	 * is not in operation.
	 */
	private String					timeZoneNoDST		= null;

	/**
	 * The time zone offset relative to GMT in text form when daylight saving
	 * is in operation.
	 */
	private String					timeZoneDST			= null;

	/**
	 * The system time when we last updated the Date that this valve
	 * uses for log lines.
	 */
	private Date					currentDate			= null;

	/**
	 * Resolve hosts.
	 */
	private boolean					resolveHosts		= false;

	/**
	 * Are we doing conditional logging. default false.
	 */
	private String					condition			= null;

	public CommonsLoggingAccessLogValve()
	{
		super();
		setPattern("common");
	}
	
	/**
	 * Return descriptive information about this implementation.
	 */
	public String getInfo()
	{
		return (info);
	}

	/**
	 * Return the format pattern.
	 */
	public String getPattern()
	{
		return (this.pattern);
	}

	/**
	 * Set the format pattern, first translating any recognized alias.
	 *
	 * @param pattern The new pattern
	 */
	public void setPattern(String pattern)
	{
		if (pattern == null)
			pattern = "";
		if (pattern.equals(Constants.AccessLog.COMMON_ALIAS))
			pattern = Constants.AccessLog.COMMON_PATTERN;
		if (pattern.equals(Constants.AccessLog.COMBINED_ALIAS))
			pattern = Constants.AccessLog.COMBINED_PATTERN;
		this.pattern = pattern;

		if (this.pattern.equals(Constants.AccessLog.COMMON_PATTERN))
			common = true;
		else
			common = false;

		if (this.pattern.equals(Constants.AccessLog.COMBINED_PATTERN))
			combined = true;
		else
			combined = false;
	}

	/**
	 * Set the resolve hosts flag.
	 *
	 * @param resolveHosts The new resolve hosts value
	 */
	public void setResolveHosts(boolean resolveHosts)
	{
		this.resolveHosts = resolveHosts;
	}

	/**
	 * Get the value of the resolve hosts flag.
	 */
	public boolean isResolveHosts()
	{
		return resolveHosts;
	}

	/**
	 * Return whether the attribute name to look for when
	 * performing conditional loggging. If null, every
	 * request is logged.
	 */
	public String getCondition()
	{
		return condition;
	}

	/**
	 * Set the ServletRequest.attribute to look for to perform
	 * conditional logging. Set to null to log everything.
	 *
	 * @param condition Set to null to log everything
	 */
	public void setCondition(String condition)
	{
		this.condition = condition;
	}
	
	/**
	 * Return the current Commons Logging log level
	 */
	public String getLevel()
	{
		return LEVELS[level];
	}

	/**
	 * Set the Commons Logging log level for all access log lines
	 *
	 * @param level: TRACE, DEBUG, INFO, WARN, ERROR, FATAL (default: INFO)
	 */
	public void setLevel(String level)
	{
		for(int i=0; i<LEVELS.length; i++)
		{
			if(LEVELS[i].equalsIgnoreCase(level))
			{
				this.level = i;
				break;
			}
		}
	}

	/**
	 * Log a message summarizing the specified request and response, according
	 * to the format specified by the <code>pattern</code> property.
	 *
	 * @param request Request being processed
	 * @param response Response being processed
	 *
	 * @exception IOException if an input/output error has occurred
	 * @exception ServletException if a servlet error has occurred
	 */
	public void invoke(Request request, Response response) throws IOException, ServletException
	{
		// Pass this request on to the next valve in our pipeline
		long t1 = System.currentTimeMillis();

		getNext().invoke(request, response);

		long t2 = System.currentTimeMillis();
		long time = t2 - t1;

		if (condition != null && null != request.getRequest().getAttribute(condition))
			return;

		Date date = getDate();
		StringBuilder result = new StringBuilder();

		// Check to see if we should log using the "common" access log pattern
		if (common || combined)
		{
			String value = null;

			if (isResolveHosts())
				result.append(request.getRemoteHost());
			else
				result.append(request.getRemoteAddr());

			result.append(" - ");

			value = request.getRemoteUser();
			if (value == null)
				result.append("- ");
			else
			{
				result.append(value);
				result.append(space);
			}

			result.append("[");
			result.append(dayFormatter.format(date)); // Day
			result.append('/');
			result.append(lookup(monthFormatter.format(date))); // Month
			result.append('/');
			result.append(yearFormatter.format(date)); // Year
			result.append(':');
			result.append(timeFormatter.format(date)); // Time
			result.append(space);
			result.append(getTimeZone(date)); // Time Zone
			result.append("] \"");

			result.append(request.getMethod());
			result.append(space);
			result.append(request.getRequestURI());
			if (request.getQueryString() != null)
			{
				result.append('?');
				result.append(request.getQueryString());
			}
			result.append(space);
			result.append(request.getProtocol());
			result.append("\" ");

			result.append(response.getStatus());

			result.append(space);

			int length = response.getContentCount();

			if (length <= 0)
				value = "-";
			else
				value = "" + length;
			result.append(value);

			if (combined)
			{
				result.append(space);
				result.append("\"");
				String referer = request.getHeader("referer");
				if (referer != null)
					result.append(referer);
				else
					result.append("-");
				result.append("\"");

				result.append(space);
				result.append("\"");
				String ua = request.getHeader("user-agent");
				if (ua != null)
					result.append(ua);
				else
					result.append("-");
				result.append("\"");
			}
		}
		else
		{
			// Generate a message based on the defined pattern
			boolean replace = false;
			for (int i = 0; i < pattern.length(); i++)
			{
				char ch = pattern.charAt(i);
				if (replace)
				{
					/* For code that processes {, the behavior will be ... if I
					 * do not enounter a closing } - then I ignore the {
					 */
					if ('{' == ch)
					{
						StringBuffer name = new StringBuffer();
						int j = i + 1;
						for (; j < pattern.length() && '}' != pattern.charAt(j); j++)
						{
							name.append(pattern.charAt(j));
						}
						if (j + 1 < pattern.length())
						{
							/* the +1 was to account for } which we increment now */
							j++;
							result.append(replace(name.toString(), pattern.charAt(j), request, response));
							i = j; /*Since we walked more than one character*/
						}
						else
						{
							//D'oh - end of string - pretend we never did this
							//and do processing the "old way"
							result.append(replace(ch, date, request, response, time));
						}
					}
					else
					{
						result.append(replace(ch, date, request, response, time));
					}
					replace = false;
				}
				else if (ch == '%')
				{
					replace = true;
				}
				else
				{
					result.append(ch);
				}
			}
		}
		log(result.toString(), date);

	}

	/**
	 * Log the specified message to the log file, switching files if the date
	 * has changed since the previous log call.
	 *
	 * @param message Message to be logged
	 * @param date the current Date object (so this method doesn't need to
	 *        create a new one)
	 */
	public void log(String message, Date date)
	{
		switch(level)
		{
			case 0:
				log.trace(message);
				break;
			case 1:
				log.debug(message);
				break;
			case 3:
				log.warn(message);
				break;
			case 4: 
				log.error(message);
				break;
			case 5:
				log.fatal(message);
				break;
			default:
				log.info(message);
		}
	}

	/**
	 * Return the month abbreviation for the specified month, which must
	 * be a two-digit String.
	 *
	 * @param month Month number ("01" .. "12").
	 */
	private static String lookup(String month)
	{
		int index;
		try
		{
			index = Integer.parseInt(month) - 1;
		}
		catch (Throwable t)
		{
			index = 0; // Can not happen, in theory
		}
		return (months[index]);

	}

	/**
	 * Return the replacement text for the specified pattern character.
	 *
	 * @param pattern Pattern character identifying the desired text
	 * @param date the current Date so that this method doesn't need to
	 *        create one
	 * @param request Request being processed
	 * @param response Response being processed
	 */
	private String replace(char pattern, Date date, Request request, Response response, long time)
	{
		String value = null;

		if (pattern == 'a')
		{
			value = request.getRemoteAddr();
		}
		else if (pattern == 'A')
		{
			try
			{
				value = InetAddress.getLocalHost().getHostAddress();
			}
			catch (Throwable e)
			{
				value = "127.0.0.1";
			}
		}
		else if (pattern == 'b')
		{
			int length = response.getContentCount();
			if (length <= 0)
				value = "-";
			else
				value = "" + length;
		}
		else if (pattern == 'B')
		{
			value = "" + response.getContentLength();
		}
		else if (pattern == 'h')
		{
			value = request.getRemoteHost();
		}
		else if (pattern == 'H')
		{
			value = request.getProtocol();
		}
		else if (pattern == 'l')
		{
			value = "-";
		}
		else if (pattern == 'm')
		{
			if (request != null)
				value = request.getMethod();
			else
				value = "";
		}
		else if (pattern == 'p')
		{
			value = "" + request.getServerPort();
		}
		else if (pattern == 'D')
		{
			value = "" + time;
		}
		else if (pattern == 'q')
		{
			String query = null;
			if (request != null)
				query = request.getQueryString();
			if (query != null)
				value = "?" + query;
			else
				value = "";
		}
		else if (pattern == 'r')
		{
			StringBuffer sb = new StringBuffer();
			if (request != null)
			{
				sb.append(request.getMethod());
				sb.append(space);
				sb.append(request.getRequestURI());
				if (request.getQueryString() != null)
				{
					sb.append('?');
					sb.append(request.getQueryString());
				}
				sb.append(space);
				sb.append(request.getProtocol());
			}
			else
			{
				sb.append("- - ");
				sb.append(request.getProtocol());
			}
			value = sb.toString();
		}
		else if (pattern == 'S')
		{
			if (request != null)
				if (request.getSession(false) != null)
					value = request.getSessionInternal(false).getIdInternal();
				else
					value = "-";
			else
				value = "-";
		}
		else if (pattern == 's')
		{
			if (response != null)
				value = "" + response.getStatus();
			else
				value = "-";
		}
		else if (pattern == 't')
		{
			StringBuffer temp = new StringBuffer("[");
			temp.append(dayFormatter.format(date)); // Day
			temp.append('/');
			temp.append(lookup(monthFormatter.format(date))); // Month
			temp.append('/');
			temp.append(yearFormatter.format(date)); // Year
			temp.append(':');
			temp.append(timeFormatter.format(date)); // Time
			temp.append(' ');
			temp.append(getTimeZone(date)); // Timezone
			temp.append(']');
			value = temp.toString();
		}
		else if (pattern == 'T')
		{
			value = timeTakenFormatter.format(time / 1000d);
		}
		else if (pattern == 'u')
		{
			if (request != null)
				value = request.getRemoteUser();
			if (value == null)
				value = "-";
		}
		else if (pattern == 'U')
		{
			if (request != null)
				value = request.getRequestURI();
			else
				value = "-";
		}
		else if (pattern == 'v')
		{
			value = request.getServerName();
		}
		else
		{
			value = "???" + pattern + "???";
		}

		if (value == null)
			return ("");
		else
			return (value);

	}

	/**
	 * Return the replacement text for the specified "header/parameter".
	 *
	 * @param header The header/parameter to get
	 * @param type Where to get it from i=input,c=cookie,r=ServletRequest,s=Session
	 * @param request Request being processed
	 * @param response Response being processed
	 */
	private String replace(String header, char type, Request request, Response response)
	{
		Object value = null;

		switch (type)
		{
			case 'i':
				if (null != request)
					value = request.getHeader(header);
				else
					value = "??";
				break;
			/*
			 // Someone please make me work
			 case 'o':
			 break;
			 */
			case 'c':
				Cookie[] c = request.getCookies();
				for (int i = 0; c != null && i < c.length; i++)
				{
					if (header.equals(c[i].getName()))
					{
						value = c[i].getValue();
						break;
					}
				}
				break;
			case 'r':
				if (null != request)
					value = request.getAttribute(header);
				else
					value = "??";
				break;
			case 's':
				if (null != request)
				{
					HttpSession sess = request.getSession(false);
					if (null != sess)
						value = sess.getAttribute(header);
				}
				break;
			default:
				value = "???";
		}

		/* try catch in case toString() barfs */
		try
		{
			if (value != null)
				if (value instanceof String)
					return (String) value;
				else
					return value.toString();
			else
				return "-";
		}
		catch (Throwable e)
		{
			return "-";
		}
	}

	/**
	 * This method returns a Date object that is accurate to within one
	 * second.  If a thread calls this method to get a Date and it's been
	 * less than 1 second since a new Date was created, this method
	 * simply gives out the same Date again so that the system doesn't
	 * spend time creating Date objects unnecessarily.
	 *
	 * @return Date
	 */
	private Date getDate()
	{
		if (currentDate == null)
		{
			currentDate = new Date();
		}
		else
		{
			// Only create a new Date once per second, max.
			long systime = System.currentTimeMillis();
			if ((systime - currentDate.getTime()) > 1000)
			{
				currentDate = new Date(systime);
			}
		}

		return currentDate;
	}

	private String getTimeZone(Date date)
	{
		if (timezone.inDaylightTime(date))
		{
			return timeZoneDST;
		}
		else
		{
			return timeZoneNoDST;
		}
	}

	private String calculateTimeZoneOffset(long offset)
	{
		StringBuffer tz = new StringBuffer();
		if ((offset < 0))
		{
			tz.append("-");
			offset = -offset;
		}
		else
		{
			tz.append("+");
		}

		long hourOffset = offset / (1000 * 60 * 60);
		long minuteOffset = (offset / (1000 * 60)) % 60;

		if (hourOffset < 10)
			tz.append("0");
		tz.append(hourOffset);

		if (minuteOffset < 10)
			tz.append("0");
		tz.append(minuteOffset);

		return tz.toString();
	}

	// ------------------------------------------------------ Lifecycle Methods

	/**
	 * Add a lifecycle event listener to this component.
	 *
	 * @param listener The listener to add
	 */
	public void addLifecycleListener(LifecycleListener listener)
	{
		lifecycle.addLifecycleListener(listener);
	}

	/**
	 * Get the lifecycle listeners associated with this lifecycle. If this
	 * Lifecycle has no listeners registered, a zero-length array is returned.
	 */
	public LifecycleListener[] findLifecycleListeners()
	{
		return lifecycle.findLifecycleListeners();
	}

	/**
	 * Remove a lifecycle event listener from this component.
	 *
	 * @param listener The listener to add
	 */
	public void removeLifecycleListener(LifecycleListener listener)
	{
		lifecycle.removeLifecycleListener(listener);
	}

	/**
	 * Prepare for the beginning of active use of the public methods of this
	 * component.  This method should be called after <code>configure()</code>,
	 * and before any of the public methods of the component are utilized.
	 *
	 * @exception LifecycleException if this component detects a fatal error
	 *  that prevents this component from being used
	 */
	public void start() throws LifecycleException
	{
		// Validate and update our current component state
		if (started)
			throw new LifecycleException(sm.getString("accessLogValve.alreadyStarted"));
		
		lifecycle.fireLifecycleEvent(START_EVENT, null);
		started = true;

		// Initialize the timeZone, Date formatters, and currentDate
		timezone = TimeZone.getDefault();
		timeZoneNoDST = calculateTimeZoneOffset(timezone.getRawOffset());
		Calendar calendar = Calendar.getInstance(timezone);
		int offset = calendar.get(Calendar.DST_OFFSET);
		timeZoneDST = calculateTimeZoneOffset(timezone.getRawOffset() + offset);

		dayFormatter = new SimpleDateFormat("dd");
		dayFormatter.setTimeZone(timezone);
		monthFormatter = new SimpleDateFormat("MM");
		monthFormatter.setTimeZone(timezone);
		yearFormatter = new SimpleDateFormat("yyyy");
		yearFormatter.setTimeZone(timezone);
		timeFormatter = new SimpleDateFormat("HH:mm:ss");
		timeFormatter.setTimeZone(timezone);
		currentDate = new Date();
		timeTakenFormatter = new DecimalFormat("0.000");
	}

	/**
	 * Gracefully terminate the active use of the public methods of this
	 * component.  This method should be the last one called on a given
	 * instance of this component.
	 *
	 * @exception LifecycleException if this component detects a fatal error
	 *  that needs to be reported
	 */
	public void stop() throws LifecycleException
	{
		// Validate and update our current component state
		if (!started)
			throw new LifecycleException(sm.getString("accessLogValve.notStarted"));
		
		lifecycle.fireLifecycleEvent(STOP_EVENT, null);
		started = false;
	}
}
