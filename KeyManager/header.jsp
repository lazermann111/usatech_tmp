<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%	response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server %>
<%@ page import="	java.security.*,
					java.security.cert.*, 
					javax.security.auth.x500.*, 
					javax.crypto.*,
					java.util.*, 
					java.io.*,
					java.net.*,
					org.apache.commons.logging.*,
					com.usatech.keymanager.master.*, 
					com.usatech.keymanager.util.*, 
					com.usatech.keymanager.auth.*,
					com.usatech.keymanager.ssss.*,
					com.usatech.util.*,
					com.usatech.util.mail.*,
					javax.security.auth.Subject" %>
<%@page import="com.usatech.util.Base64"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="cache-control" CONTENT="NO-CACHE">
 <link rel="stylesheet" TYPE="text/css" HREF="<%=request.getContextPath() %>/style.css">
 <title>CRYPTOGRAPHY AND KEY MANAGEMENT SERVICE</title>
</head>
<body>
<%	Log log = LogFactory.getLog("WEB_ACCESS");	

	KeyManagerUser currentUser;
	try {
		currentUser = KeyManagerUtil.getCurrentKeyManagerUser(request);
	} catch (Exception e) {
		%><div class="failure"><%=e.getMessage() %></div><%
		log.error("Error", e);
		return;
	}
	String currentCertThumbprint = currentUser.getCertThumbprint();
	String currentUserInfo = currentUser.getUserInfo();
	String currentSubjectDN = currentUser.getCertificate().getSubjectDN().getName();

	KeyManagerLoader loader = (KeyManagerLoader) getServletConfig().getServletContext().getAttribute(KeyManagerLoader.CONTEXT_KEY);
    MasterKeyLoader masterKeyLoader = loader.getMasterKeyLoader();
    
	String process = request.getParameter("process");
	List<KeyManagerRole> allRoles = KeyManagerRole.getAllRoles();
	if(allRoles == null || allRoles.size() == 0) {
		%><div class="failure">Fatal Error: Failed to lookup roles in database!</div>
        </body></html><%
		return;
	}
	allRoles.remove(KeyManagerRole.getByName(KeyManagerRole.ROLE_VALID_CERT)); %>
<center><div align="center">
<div class="headbig">
USA TECHNOLOGIES, INC<br>
CRYPTOGRAPHY AND KEY MANAGEMENT SERVICE<br></div>
<br>
YOUR CERTIFICATE<br>
<table width="750">
 <tr>
  <td><%=currentUserInfo%></td>
 </tr>
</table>
<br>