package com.usatech.keymanager.app;

import java.net.Socket;
import java.security.cert.X509Certificate;
import java.util.Map;



import com.usatech.keymanager.util.CertUtil;

public class KMPrivateKeyStrategy implements PrivateKeyStrategy {
	private String thumbprint;
	
	public KMPrivateKeyStrategy(String thumbprint) {
		this.thumbprint = thumbprint;
	}
	
	public String chooseAlias(Map<String, PrivateKeyDetails> aliases, Socket socket) {
		for (String alias: aliases.keySet()) {
			X509Certificate[] certChain = aliases.get(alias).getCertChain();
			if (certChain != null && certChain.length > 1){
				X509Certificate x509Cert = certChain[0];
				String certThumbprint;
				try {
					certThumbprint = CertUtil.getCertThumbprint(x509Cert);
					if (certThumbprint.equalsIgnoreCase(thumbprint))
						return alias;
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
		}
		return null;
	}
}
