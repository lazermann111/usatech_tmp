package com.usatech.keymanager.app;

import java.io.Serializable;

public class AppParams implements Serializable {
	private static final long serialVersionUID = 1L;

	String asymmetricCipherID;
	String encryptedBase64;
	String currentCertThumbprint;
	String error;
	
	public String getAsymmetricCipherID() {
		return asymmetricCipherID;
	}
	public void setAsymmetricCipherID(String asymmetricCipherID) {
		this.asymmetricCipherID = asymmetricCipherID;
	}
	public String getEncryptedBase64() {
		return encryptedBase64;
	}
	public void setEncryptedBase64(String encryptedBase64) {
		this.encryptedBase64 = encryptedBase64;
	}
	public String getCurrentCertThumbprint() {
		return currentCertThumbprint;
	}
	public void setCurrentCertThumbprint(String currentCertThumbprint) {
		this.currentCertThumbprint = currentCertThumbprint;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
