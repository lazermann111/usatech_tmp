package com.usatech.keymanager.app;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;
import javax.net.ssl.SSLContext;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.usatech.keymanager.applet.JComponentTable;
import com.usatech.keymanager.applet.KeyStoreEntryIterator;
import com.usatech.keymanager.util.CertUtil;


public class KeyManagerApp
{	
	private final static Logger 			log 				= Logger.getLogger(KeyManagerApp.class);	
	
	private static final String             tokenCertIssuerDN	= "CN=USA Technologies Token CA, DC=usatech, DC=com";
	private static final String 			providerName 		= "SunMSCAPI";
	private static final String 			keyStoreName 		= "Windows-My";
	
	protected static final String 			LB 					= System.getProperty("line.separator");	
	private static final SimpleDateFormat 	dateFormatter 		= new SimpleDateFormat("MM/dd/yyyy");

	private static final String[] 			columnNames 		= {"New", "Subject", "Valid from", "Valid to", "Thumbprint", "Serial Number"};
	
	private static JTable 					table;
	private static AbstractTableModel 		tableModel;
	private static ButtonGroup 				radioButtonGroup 	= new ButtonGroup();

	private static ArrayList<ArrayList<Object>>	tableRows		= new ArrayList<ArrayList<Object>>();
        
    private static String                   newCertThumbprint   = "";
    private static int                      selectedCertIndex   = 0;
	
	public static void main(String[] args) {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(Color.WHITE);
		
		final JTextArea inputArea = new JTextArea(2, 100);
		inputArea.setLineWrap(false);
		inputArea.setEditable(true);
		inputArea.setEnabled(true);
		inputArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		inputArea.setBackground(Color.WHITE);
		inputArea.setAutoscrolls(true);		

		JScrollPane inputPane = new JScrollPane(inputArea);
		JPanel inputPanel = new JPanel(new BorderLayout());
		inputPanel.add(new JLabel(" Please enter input values from Key Manager:", SwingConstants.LEFT), BorderLayout.PAGE_START);
		inputPanel.add(inputPane);
		inputPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, inputArea.getMinimumSize().height));
		mainPanel.add(inputPanel);
		
		final JTextArea statusArea = new JTextArea(1, 100);
		statusArea.setLineWrap(true);
		statusArea.setEditable(false);
		statusArea.setEnabled(true);
		statusArea.setFont(new Font("monospaced", Font.BOLD, 14));
		statusArea.setBackground(Color.WHITE);
		statusArea.setAutoscrolls(true);
		
		JScrollPane statusPane = new JScrollPane(statusArea);
		JPanel statusPanel = new JPanel(new BorderLayout());
		statusPanel.add(new JLabel(" Status", SwingConstants.LEFT), BorderLayout.PAGE_START);
		statusPanel.add(statusPane);
		statusPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, statusArea.getMinimumSize().height));
		
		final JTextArea outputArea = new JTextArea();
		outputArea.setLineWrap(true);
		outputArea.setEditable(false);
		outputArea.setEnabled(true);
		outputArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		outputArea.setBackground(Color.WHITE);
		outputArea.setAutoscrolls(true);
		
		JScrollPane outputPane = new JScrollPane(outputArea);
		JPanel outputPanel = new JPanel(new BorderLayout());
		outputPanel.add(new JLabel(" Output", SwingConstants.LEFT), BorderLayout.PAGE_START);
		outputPanel.add(outputPane);
		
		try {
			Provider provider = Security.getProvider(providerName);

			KeyStore keyStore = KeyStore.getInstance(keyStoreName, provider);
			if (keyStore == null)
				throw new IllegalComponentStateException(keyStoreName + " keystore not found: Make sure you are using Java 6 or higher!");
			keyStore.load(null, new char[0]);
			
			int rowCounter = 0;
			KeyStoreEntryIterator keyStoreIterator = new KeyStoreEntryIterator(keyStore);
			while(keyStoreIterator.hasNext())
			{
				String certAlias = keyStoreIterator.next();
				X509Certificate[] certChain = keyStoreIterator.getCertificateChain();
				PrivateKey privKey = keyStoreIterator.getPrivateKey();

				if (privKey != null && certChain != null && certChain.length > 1) {
					X509Certificate x509Cert = certChain[0];
					if (x509Cert.getIssuerDN().getName().equalsIgnoreCase(tokenCertIssuerDN)) {
    					String certDN = x509Cert.getSubjectDN().getName();
    					String certThumbprint = CertUtil.getCertThumbprint(x509Cert);
    					Date certValidFrom = x509Cert.getNotBefore();
                        Date certValidTo = x509Cert.getNotAfter();
                        if (certValidTo.before(new Date()))
                        	continue;
    					
    					ArrayList<Object> row = new ArrayList<Object>();
    
    					JRadioButton newCertRadioButton = new JRadioButton();
    					newCertRadioButton.setName(certThumbprint);
    					newCertRadioButton.setEnabled(true);
    					radioButtonGroup.add(newCertRadioButton);
    					row.add(newCertRadioButton);
                        
                        if (rowCounter == 0)
                        {
                        	newCertRadioButton.setSelected(true);
                            selectedCertIndex = rowCounter;
                            newCertThumbprint = certThumbprint;
                        }                         
    					
    					row.add(certDN);
    					row.add(dateFormatter.format(certValidFrom));
                        row.add(dateFormatter.format(certValidTo));
    					row.add(certThumbprint);
                        row.add(x509Cert.getSerialNumber().toString(16));
    					
    					tableRows.add(row);
                        rowCounter++;
                    }
				}
			}

			tableModel = new KeyManagerTableModel();
						
			table = new JComponentTable(tableModel);			
			table.setFillsViewportHeight(true);
			table.setRowSelectionAllowed(true);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setAutoCreateRowSorter(false);
			
			table.getColumnModel().getColumn(0).setMaxWidth(50);
			table.getColumnModel().getColumn(0).setResizable(false);			
			
			table.getColumnModel().getColumn(2).setMaxWidth(table.getColumnModel().getColumn(2).getPreferredWidth());
			table.getColumnModel().getColumn(3).setMaxWidth(table.getColumnModel().getColumn(3).getPreferredWidth());
			
			table.getSelectionModel().addListSelectionListener(new KeyManagerListSelectionListener());
            table.getSelectionModel().setSelectionInterval(selectedCertIndex,selectedCertIndex);
			
            BorderLayout layout = new BorderLayout();
            layout.setVgap(1);
    		JPanel certPanel = new JPanel(layout);
    		JScrollPane certPane = new JScrollPane(table);
    		JLabel label = new JLabel(" Certificates", SwingConstants.LEFT);
    		certPanel.add(label, BorderLayout.PAGE_START);    		
    		certPanel.add(certPane);
    		certPanel.setPreferredSize(new Dimension(Integer.MAX_VALUE, layout.getVgap() * 5 + label.getMinimumSize().height + table.getTableHeader().getMinimumSize().height + table.getMinimumSize().height));
    		certPanel.setMaximumSize(certPanel.getPreferredSize());
			mainPanel.add(certPanel, BorderLayout.CENTER);
		}
		catch (Exception e)
		{
			log(outputArea, e);
		}
				
		mainPanel.add(statusPanel);		
		mainPanel.add(outputPanel);
				
		final JButton decryptButton = new JButton("Decrypt");
		final JButton changeCertButton = new JButton("Change Certificate");
		final JButton clearButton = new JButton("Clear");
		JPanel buttonPanel = new JPanel(new FlowLayout());		
		buttonPanel.add(decryptButton);
		buttonPanel.add(changeCertButton);
		buttonPanel.add(clearButton);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);		
		
		decryptButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				decryptButton.setEnabled(false);
				performAction("decrypt", inputArea, statusArea, outputArea);
				decryptButton.setEnabled(true);			
			}
		});
		
		changeCertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeCertButton.setEnabled(false);
				performAction("changecert", inputArea, statusArea, outputArea);
				changeCertButton.setEnabled(true);			
			}
		});
		
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputArea.setText("");
				statusArea.setBackground(Color.WHITE);
				statusArea.setText("");
				outputArea.setText("");			
			}
		});
		
		final JFrame frame = new JFrame("USAT Key Manager App");
		frame.setSize(1000, 800);
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.setVisible(true);
	}
	
	protected static void performAction(String action, JTextArea inputArea, JTextArea statusArea, JTextArea outputArea) {
		statusArea.setBackground(Color.WHITE);
		statusArea.setText("");
		outputArea.setText("");
		
		byte[] decryptedBytes;		
		Cipher asymmetricCipher;
		byte[] encryptedBytes;		
		PrivateKey privKey = null;
		String responseString = null;
		AppParams appParams = null;
		boolean success = false;
		try
		{
			ArrayList<String> args = new ArrayList<String>();			
			BufferedReader reader = new BufferedReader(new StringReader(inputArea.getText()));					      
			String line;
			while ((line = reader.readLine()) != null)
				args.add(line);
			
			if (args.size() < 2)
		    	throw new Exception("Invalid number of arguments, expected 2, received " + args.size());
		    String url = args.get(0);
		    String thumbprint = args.get(1);
		    
		    if ("changecert".equalsIgnoreCase(action)) {
		    	if (newCertThumbprint.equalsIgnoreCase(thumbprint))
		    		throw new Exception("Your current and new certificates are the same. Please select a new certificate.");
		    }
			
			Provider provider = Security.getProvider(providerName);			
			KeyStore keyStore = KeyStore.getInstance(keyStoreName, provider);
			if (keyStore == null)
				throw new IllegalComponentStateException(keyStoreName + " keystore not found: Make sure you are using Java 6 or higher!");
            keyStore.load(null, new char[0]);
            
            HttpClient httpClient = getHttpClient(keyStore, thumbprint);
			HttpResponse response = httpClient.execute(new HttpGet(url + "/admin/appparams.jsp?action=decrypt"));
			reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			while((line = reader.readLine()) != null) 
				sb.append(line);
			responseString = sb.toString();
			Gson gson = new Gson();
			appParams = gson.fromJson(responseString, AppParams.class);
			if (appParams.getError() != null && appParams.getError().trim().length() > 0)
				throw new Exception(appParams.getError());
			
			String asymmetricCipherID = appParams.getAsymmetricCipherID();
			String encryptedBase64 = appParams.getEncryptedBase64();
			String currentCertThumbprint = appParams.getCurrentCertThumbprint();

			asymmetricCipher = Cipher.getInstance(asymmetricCipherID, provider);
			log(outputArea, "Loaded asymmetric cipher");
			
			encryptedBytes = CertUtil.decodeBase64(encryptedBase64);
			log(outputArea, "Base64 decoded " + encryptedBytes.length + " encrypted bytes");
			
			KeyStoreEntryIterator keyStoreIterator = new KeyStoreEntryIterator(keyStore);
			
			while(keyStoreIterator.hasNext())
			{
				String certAlias = keyStoreIterator.next();
				X509Certificate[] certChain = keyStoreIterator.getCertificateChain();
				PrivateKey key = keyStoreIterator.getPrivateKey();

				if (key != null && certChain != null && certChain.length > 1)
				{
					X509Certificate x509Cert = certChain[0];
					String certThumbprint = CertUtil.getCertThumbprint(x509Cert);

					if (certThumbprint.equalsIgnoreCase(currentCertThumbprint))
					{
						log(outputArea, "Found certificate with matching thumbprint " + thumbprint + " for subject: ");
						log(outputArea, x509Cert.getSubjectDN().getName());
						privKey = key;
						break;
					}
				}
			}
			
			if (privKey == null)
				throw new Exception("Certificate not found in key store");
			
			asymmetricCipher.init(Cipher.DECRYPT_MODE, privKey);
			log(outputArea, "Asymmetric cipher initialized");
									
			decryptedBytes = asymmetricCipher.doFinal(encryptedBytes);
			
			if (decryptedBytes == null || decryptedBytes.length < 1)
				throw new Exception("Empty decrypted bytes");			
			log(outputArea, "Asymmetric decrypted " + decryptedBytes.length + " bytes");
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
		    params.add(new BasicNameValuePair("s", CertUtil.encodeBase64(decryptedBytes)));
			
			if ("changecert".equalsIgnoreCase(action)) {
				httpClient = getHttpClient(keyStore, newCertThumbprint);
				params.add(new BasicNameValuePair("param", thumbprint));
			}
			
			HttpPost post = new HttpPost(url + "/admin/appresults.jsp?action=" + action);			
		    post.setEntity(new UrlEncodedFormEntity(params));
			httpClient.execute(post);
			
			if ("decrypt".equalsIgnoreCase(action))
				log(outputArea, "Please click Continue on Key Manager website to proceed");
			else if ("changecert".equalsIgnoreCase(action))
				log(outputArea, "Process complete");
			success = true;
		}
		catch (Exception e)
		{
			log(outputArea, e);
			if (appParams == null && responseString != null)
				log(outputArea, "Response: " + responseString);
		}
		finally
		{
			if (success)
			{
				statusArea.setBackground(Color.GREEN);
				statusArea.setText("SUCCESS");
			}
			else
			{
				statusArea.setBackground(Color.RED);
				statusArea.setText("FAILURE");
			}
		}
	}

	private static void log(JTextArea outputArea, Exception e)
	{
		if (e == null)
			return;
		log.error("Error", e);
		e.printStackTrace();
		String message = e.getMessage();
		if (message == null && e.getCause() != null)
			message = e.getCause().getMessage();
		log(outputArea, "Error: " + message);			
	}

	private static void log(JTextArea outputArea, String message)
	{
		if (outputArea == null)
			return;
		Document doc = outputArea.getDocument();
		if(doc == null)
			return;
		try {
			doc.insertString(doc.getLength(), message + LB, null);
		} catch(BadLocationException e) {
			// ignore
		}	
	}
	
	private static HttpClient getHttpClient(KeyStore keyStore, String thumbprint) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
		KMPrivateKeyStrategy privateKeyStrategy = new KMPrivateKeyStrategy(thumbprint);
        SSLContextBuilder sslContextBuilder = SSLContexts.custom().loadKeyMaterial(keyStore, new char[0], privateKeyStrategy);
        TrustStrategy trustStrategy = new TrustStrategy (){
            public boolean isTrusted (X509Certificate[] chain, String authType) throws CertificateException {
                //use truststore for certificate validation
            	return false;
            }
        };
        sslContextBuilder.loadTrustMaterial(trustStrategy);
        SSLContext sslContext = sslContextBuilder.build();
		HttpClient httpClient = HttpClients.custom().setSslcontext(sslContext).build();
		return httpClient;
	}
	
	private static class KeyManagerListSelectionListener implements ListSelectionListener {		
		public void valueChanged(ListSelectionEvent e)
		{
			synchronized(radioButtonGroup)
			{
				if(e.getValueIsAdjusting())
				{
					int selectedIndex = table.getSelectionModel().getAnchorSelectionIndex();
					
					int counter = 0;
					Enumeration<AbstractButton> buttonEnum = radioButtonGroup.getElements();
					
					while(buttonEnum.hasMoreElements())
					{
	                    JRadioButton button = (JRadioButton) buttonEnum.nextElement();
	                    if(counter == selectedIndex)
	                    {
	                        String certThumbprint = button.getName();
                            button.setSelected(true);
                            newCertThumbprint = certThumbprint;
                            selectedCertIndex = selectedIndex;
	                        break;
	                    }
						counter++;
					}
	                
	                table.getSelectionModel().setSelectionInterval(selectedCertIndex,selectedCertIndex);
				}
			}
		}
	}
	
	private static class KeyManagerTableModel extends AbstractTableModel
	{
		private static final long serialVersionUID = 1L;

		public int getColumnCount() 
		{
			return columnNames.length;
		}
		
		public int getRowCount()
		{
			return tableRows.size();
		}
		
		public String getColumnName(int col) 
		{
			return columnNames[col];
		}
		
		public Object getValueAt(int row, int col) 
		{
			return tableRows.get(row).get(col);
		}
		
		public boolean isCellEditable(int row, int col)
		{
			return false;
		}
	}	
}
