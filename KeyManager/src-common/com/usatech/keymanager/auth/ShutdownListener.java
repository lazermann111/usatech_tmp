package com.usatech.keymanager.auth;

import javax.servlet.*;

import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.commons.logging.*;

public class ShutdownListener implements ServletContextListener
{
	private static Log	log	= LogFactory.getLog(ShutdownListener.class);

	public void contextInitialized(ServletContextEvent event)
	{

	}

	public void contextDestroyed(ServletContextEvent event)
	{
		log.debug("Shutting down HSQLDB and Cayenne");

		DataContext context = DataContext.createDataContext();

		// shutdown HSQL
		context.performNonSelectingQuery(new SQLTemplate(KeyManagerUser.class, "SHUTDOWN"));

		// shutdown Cayenne
		context.getParentDataDomain().shutdown();

		log.info("KeyManager Destroyed!");
	}
}
