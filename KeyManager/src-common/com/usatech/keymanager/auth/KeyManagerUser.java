package com.usatech.keymanager.auth;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.Principal;
import java.util.*;

import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.*;

import com.usatech.keymanager.cayenne._KeyManagerUser;
import com.usatech.keymanager.util.CertUtil;
import com.usatech.keymanager.util.DataUtil;

public class KeyManagerUser extends _KeyManagerUser implements Principal, java.io.Serializable
{
    private X509Certificate certificate;
    
	public String toString()
	{
		List roles = super.getRoles();
		if (roles == null || roles.size() == 0)
			return "KeyManagerUser: " + getUserInfo() + ": [NO ROLES]";
		else
			return "KeyManagerUser: " + getUserInfo() + ": " + Arrays.toString(roles.toArray());
	}

	public boolean hasRole(KeyManagerRole role)
	{
		List roles = getRoles();
		if (roles != null && roles.contains(role))
			return true;
		return false;
	}

	public String getName()
	{
		return getSubjectName();
	}
    
    public X509Certificate getCertificate() throws CertificateException
    {
        if (certificate == null)
            certificate = CertUtil.getCertFromBase64(getCertBase64());
        return certificate;        
    }
    
    public void setCertificate(X509Certificate certificate)
    {
        this.certificate = certificate;
    }
    
    public String getUserInfo()
    {
        return getSubjectName() + "; " + getCertThumbprint() + "; " + getCertValidTo();
    }

	public void addToRoles(KeyManagerRole role)
	{
		super.addToRoles((KeyManagerRole) getDataContext().localObject(role.getObjectId(), role));
	}

	public void removeFromRoles(KeyManagerRole role)
	{
		super.removeFromRoles((KeyManagerRole) getDataContext().localObject(role.getObjectId(), role));
	}

	public boolean equals(Object o)
	{
		if (o == null)
			return false;

		if (this == o)
			return true;

		if (!(o instanceof KeyManagerUser))
			return false;

		KeyManagerUser that = (KeyManagerUser) o;

		if (this.hashCode() == that.hashCode())
			return true;

		return false;
	}

	public int hashCode()
	{
		return (getSubjectName() + "-" + getCertThumbprint()).hashCode();
	}

	public void save()
	{
		KeyManagerRole validCert = KeyManagerRole.getByName(KeyManagerRole.ROLE_VALID_CERT);
		getRoles().remove(validCert);
		DataUtil.commitDataChanges(getDataContext());
	}

	public void delete()
	{
		getDataContext().deleteObject(this);
		DataUtil.commitDataChanges(getDataContext());
	}

	public List<KeyManagerRole> getRoles()
	{
		return super.getRoles();
	}

	public static KeyManagerUser newInstance()
	{
		DataContext context = DataContext.getThreadDataContext();
		return (KeyManagerUser) context.newObject(KeyManagerUser.class);
	}

	public static KeyManagerUser getBySubjectName(String subjectName)
	{
		DataContext context = DataContext.getThreadDataContext();
		SelectQuery query = new SelectQuery(KeyManagerUser.class);
		query.setQualifier(ExpressionFactory.matchExp("subjectName", subjectName));
		//query.addPrefetch("roles");
		List list = context.performQuery(query);
		if (list == null || list.isEmpty())
			return null;
		return (KeyManagerUser) list.get(0);
	}

    public static KeyManagerUser getByCertThumbprint(String certThumbprint)
    {
        DataContext context = DataContext.getThreadDataContext();
        SelectQuery query = new SelectQuery(KeyManagerUser.class);
        query.setQualifier(ExpressionFactory.matchExp("certThumbprint", certThumbprint));
        //query.addPrefetch("roles");
        List list = context.performQuery(query);
        if (list == null || list.isEmpty())
            return null;
        return (KeyManagerUser) list.get(0);
    }
    
	public static List<KeyManagerUser> getAllUsers()
	{
		DataContext context = DataContext.getThreadDataContext();
		SelectQuery query = new SelectQuery(KeyManagerUser.class);
		query.addOrdering(new Ordering("subjectName", true));
		List list = context.performQuery(query);
		if (list == null || list.isEmpty())
			return null;
		return list;
	}
}
