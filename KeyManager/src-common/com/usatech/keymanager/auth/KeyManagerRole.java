package com.usatech.keymanager.auth;

import java.security.Principal;
import java.util.List;

import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.*;

import com.usatech.keymanager.cayenne._KeyManagerRole;
import com.usatech.keymanager.util.DataUtil;

public class KeyManagerRole extends _KeyManagerRole implements Principal, java.io.Serializable
{
	public static final String	ROLE_VALID_CERT		= "VALID_CERT";
	public static final String	ROLE_ENCRYPT		= "ENCRYPT";
	public static final String	ROLE_DECRYPT		= "DECRYPT";
	public static final String	ROLE_GENERATE_KEY	= "GENERATE_KEY";
	public static final String	ROLE_GET_KEY		= "GET_KEY";
	public static final String	ROLE_GET_RANDOM		= "GET_RANDOM";
	public static final String	ROLE_SECURITY_ADMIN	= "SA";
	public static final String  ROLE_DUKPT_DECRYPT  = "DUKPT_DECRYPT";

	public String toString()
	{
		return getName();
	}

	public boolean equals(Object o)
	{
		if (o == null)
			return false;

		if (this == o)
			return true;

		if (!(o instanceof KeyManagerRole))
			return false;

		KeyManagerRole that = (KeyManagerRole) o;

		if (this.getName().equals(that.getName()))
			return true;

		return false;
	}

	public int hashCode()
	{
		return getName().hashCode();
	}

	public void save()
	{
		DataUtil.commitDataChanges(getDataContext());
	}

	public static KeyManagerRole newInstance()
	{
		DataContext context = DataContext.getThreadDataContext();
		return (KeyManagerRole) context.newObject(KeyManagerRole.class);
	}

	public static KeyManagerRole getByName(String roleName)
	{
		DataContext context = DataContext.getThreadDataContext();
		SelectQuery query = new SelectQuery(KeyManagerRole.class);
		query.setName("GetRoleByName-" + roleName);
		query.setCachePolicy(QueryMetadata.SHARED_CACHE);
		query.setQualifier(ExpressionFactory.matchExp("name", roleName));
		List list = context.performQuery(query);
		if (list == null || list.isEmpty())
			return null;
		return (KeyManagerRole) list.get(0);
	}

	public static List<KeyManagerRole> getAllRoles()
	{
		DataContext context = DataContext.getThreadDataContext();
		SelectQuery query = new SelectQuery(KeyManagerRole.class);
		query.setName("GetAllRoles");
		query.setCachePolicy(QueryMetadata.SHARED_CACHE);
		query.addOrdering(new Ordering("name", true));
		List list = context.performQuery(query);
		if (list == null || list.isEmpty())
			return null;
		return list;
	}
}
