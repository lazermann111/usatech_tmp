package com.usatech.keymanager.ssss;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.usatech.util.Util;

public class SSSS
{
	protected static final String[] executableSuffixes = gatherExecutableSuffixes();
	private File splitFile;
	private File combineFile;
	private int		t;
	private int		n;
	private int		keySize;
	private int		processTimeout	= 5000;

	protected static String[] gatherExecutableSuffixes() {
		String os = System.getProperty("os.name");
		if(os == null)
			return null;
		if(os.startsWith("Window")) {
			String pathExt = new ProcessBuilder().environment().get("PATHEXT");
			if(pathExt == null)
				return new String[] { ".exe", ".bat" };
			return pathExt.split(";");
		}
		return new String[] { ".sh" };
	}

	protected static File findFile(File baseFile) throws IOException {
		if(baseFile.exists() && baseFile.canExecute())
			return baseFile.getCanonicalFile();
		if(executableSuffixes == null) {
			if(baseFile.exists())
				throw new FileNotFoundException("File '" + baseFile.getAbsolutePath() + "' is not executable");
			throw new FileNotFoundException("Could not find executable file '" + baseFile.getAbsolutePath() + "'");
		}
		for(String ext : executableSuffixes) {
			File file = new File(baseFile.getAbsolutePath() + ext);
			if(file.exists() && file.canExecute())
				return file.getCanonicalFile();
		}
		throw new FileNotFoundException("Could not find executable file '" + baseFile.getAbsolutePath() + "' with extensions " + Arrays.toString(executableSuffixes));
	}

	public SSSS(File executableDirectory, int t, int n, int keySize) throws IOException
	{
		this.splitFile = findFile(new File(executableDirectory, "ssss-split"));
		this.combineFile = findFile(new File(executableDirectory, "ssss-combine"));
		this.t = t;
		this.n = n;
		this.keySize = keySize;
	}

	public String toString()
	{
		return "SSSS[t=" + t + ", n=" + n + " keySize=" + keySize + "]";
	}


	public int getT()
	{
		return t;
	}

	public int getN()
	{
		return n;
	}

	public int getKeySize()
	{
		return keySize;
	}

	public int getProcessTimeout()
	{
		return processTimeout;
	}

	public void setProcessTimeout(int processTimeout)
	{
		this.processTimeout = processTimeout;
	}

	public synchronized List<char[]> split(char[] key) throws SSSSException
	{
		ProcessBuilder pb = new ProcessBuilder(splitFile.getAbsolutePath(), "-t", Integer.toString(getT()), "-n", Integer.toString(getN()), "-s", Integer.toString(getKeySize()), "-q");

		Process p = null;
		Writer os = null;

		try
		{
			p = pb.start();
			os = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
			AsynchReader outputReader = new AsynchReader(p.getInputStream());
			AsynchReader errorReader = new AsynchReader(p.getErrorStream());

			os.write(key);
			os.write(Util.LB);

			try
			{
				os.flush();
			}
			catch (Exception fe)
			{
			}

			int exitCode = waitForExit(p, processTimeout);

			char[] outputChars = outputReader.getChars();
			char[] errorChars = errorReader.getChars();

			if (exitCode != 0)
				throw new SSSSException("Native SSSS process returned with failed exit code: " + exitCode + ".  Process output: " + new String(errorChars));

			List<char[]> splits = new ArrayList<char[]>();

			int digits = Integer.toString(getN()).length();
			StringBuilder formatStr = new StringBuilder();
			for (int i = 0; i < digits; i++)
				formatStr.append("0");
			DecimalFormat numberFormat = new DecimalFormat(formatStr.toString());

			char[] lbChars = Util.LB.toCharArray();

			int count = 1;
			int lastPos = 0;
			for (int i = 0; i < outputChars.length; i++)
			{
				if (Util.startsWith(lbChars, outputChars, i))
				{
					char[] line = new char[(i - lastPos)];
					System.arraycopy(outputChars, lastPos, line, 0, line.length);
					lastPos = i + lbChars.length;

					char[] prefix = (numberFormat.format(count) + "-").toCharArray();

					if (Util.startsWith(prefix, line, 0))
					{
						splits.add(line);
						count++;
					}
				}
			}

			Util.overwrite(outputChars);
			Util.overwrite(errorChars);

			if (splits.size() < getN())
				throw new SSSSException("Native SSSS process produced unexpected output: " + new String(outputChars));

			return splits;
		}
		catch (Exception e)
		{
			if (e instanceof SSSSException)
				throw (SSSSException) e;
			else
				throw new SSSSException("Failed to call native SSSS process: " + e.getMessage(), e);
		}
		finally
		{
			if (p != null)
				p.destroy();

			if (os != null)
			{
				try
				{
					os.close();
				}
				catch (Exception e)
				{
				}
			}
		}
	}

	public synchronized char[] combine(List<char[]> secrets) throws SSSSException
	{
		if (secrets.size() < getT())
			throw new SSSSException("Number of suplied secrets(" + secrets.size() + ") less than t(" + getT() + ")");

		ProcessBuilder pb = new ProcessBuilder(combineFile.getAbsolutePath(), "-t", Integer.toString(getT()), "-n", Integer.toString(getN()), "-s", Integer.toString(getKeySize()), "-q", "-x");

		Process p = null;
		Writer os = null;

		try
		{
			p = pb.start();
			os = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
			AsynchReader outputReader = new AsynchReader(p.getInputStream());
			AsynchReader errorReader = new AsynchReader(p.getErrorStream());

			for (char[] secret : secrets)
			{
				os.write(secret);
				os.write(Util.LB);
				try
				{
					os.flush();
				}
				catch (Exception fe)
				{
				}
			}

			int exitCode = waitForExit(p, processTimeout);

			char[] outputChars = outputReader.getChars();
			char[] errorChars = errorReader.getChars();
			Util.overwrite(outputChars);

			if (exitCode != 0)
				throw new SSSSException("Native SSSS process returned with failed exit code: " + exitCode + ".  Process output: " + new String(errorChars));

			int e = errorChars.length;
			int s;
			int l = Util.LB.length() - 1;
			for(s = errorChars.length - 1; s >= 0; s--) {
				if(errorChars[s] == Util.LB.charAt(l)) {
					if(l == 0) {
						if(s == e - Util.LB.length()) {
							e = s;
							l = Util.LB.length();
						} else
							break;
					}
					l--;
				}
			}
			if(s < 0 && e == errorChars.length)
				return errorChars;
			char[] trimmed = new char[e - (s < 0 ? 0 : s + Util.LB.length())];
			System.arraycopy(errorChars, e - trimmed.length, trimmed, 0, trimmed.length);
			Util.overwrite(errorChars);
			return new String(fromHex(new String(trimmed))).toCharArray();
		}
		catch (Exception e)
		{
			if (e instanceof SSSSException)
				throw (SSSSException) e;
			throw new SSSSException("Failed to call native SSSS process: " + e.getMessage(), e);
		}
		finally
		{
			if (p != null)
				p.destroy();

			if (os != null)
			{
				try
				{
					os.close();
				}
				catch (Exception e)
				{
				}
			}
		}
	}

	public static int hexValue(char ch) throws IllegalArgumentException {
		switch(ch) {
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case 'A':
			case 'a':
				return 10;
			case 'B':
			case 'b':
				return 11;
			case 'C':
			case 'c':
				return 12;
			case 'D':
			case 'd':
				return 13;
			case 'E':
			case 'e':
				return 14;
			case 'F':
			case 'f':
				return 15;
			default:
				throw new IllegalArgumentException("Character '" + ch + "' (" + (int) ch + ") is not a hex character");
		}
	}

	public static byte hexValue(char ch1, char ch2) throws IllegalArgumentException {
		return (byte) (16 * hexValue(ch1) + hexValue(ch2));
	}

	public static byte[] fromHex(String hex) throws IllegalArgumentException {
		if(hex == null)
			return null;
		byte[] bytes = new byte[hex.length() / 2];
		for(int i = 0; i < bytes.length; i++)
			bytes[i] = hexValue(hex.charAt(i * 2), hex.charAt(i * 2 + 1));
		return bytes;
	}
	protected class AsynchReader extends Thread
	{
		private char[]	chars;
		private Reader	reader;
		private boolean	isFinished	= false;

		protected AsynchReader(InputStream is)
		{
			this.reader = new BufferedReader(new InputStreamReader(is));
			this.setDaemon(true);
			this.start();
		}

		protected char[] getChars()
		{
			return chars;
		}

		protected boolean isFinished()
		{
			return isFinished;
		}

		public void run()
		{
			int size = 0;
			char[] collectBuffer = new char[2048];
			char[] readBuffer = new char[2048];

			try
			{
				while (true)
				{
					int len = reader.read(readBuffer);
					if (len == -1)
						break;

					if (size + len > collectBuffer.length)
					{
						char[] temp = new char[collectBuffer.length + 2048];
						System.arraycopy(collectBuffer, 0, temp, 0, collectBuffer.length);
						Util.overwrite(collectBuffer);
						collectBuffer = temp;
					}

					System.arraycopy(readBuffer, 0, collectBuffer, size, len);
					size += len;
				}
			}
			catch (Exception e)
			{
			}

			try
			{
				reader.close();
			}
			catch (Exception e)
			{
			}

			chars = new char[size];
			System.arraycopy(collectBuffer, 0, chars, 0, size);

			Util.overwrite(collectBuffer);
			Util.overwrite(readBuffer);

			isFinished = true;
		}
	}

	private static int waitForExit(Process p, int timeout) throws SSSSException
	{
		int exitCode = -1;
		int waitedTime = 0;
		int pollTime = 100;
		while (exitCode < 0 && waitedTime < timeout)
		{
			try
			{
				return p.exitValue();
			}
			catch (java.lang.IllegalThreadStateException e)
			{
			}

			try
			{
				Thread.sleep(pollTime);
			}
			catch (Exception e)
			{
			}

			waitedTime += pollTime;
		}

		throw new SSSSException("Timeout waiting for native SSSS process to complete");
	}
}
