package com.usatech.keymanager.ssss;

public class SSSSException extends Exception
{
	public SSSSException(String msg)
	{
		super(msg);
	}

	public SSSSException(String msg, Exception cause)
	{
		super(msg, cause);
	}
}
