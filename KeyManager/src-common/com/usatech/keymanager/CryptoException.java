package com.usatech.keymanager;

public class CryptoException extends Exception implements java.io.Serializable
{
	public CryptoException()
	{
		super();
	}

	public CryptoException(String message)
	{
		super(message);
	}

	public CryptoException(String message, Exception reason)
	{
		super(message, reason);
	}

	public CryptoException(Exception reason)
	{
		super(reason);
	}

}
