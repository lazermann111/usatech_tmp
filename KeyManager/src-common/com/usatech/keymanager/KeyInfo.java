package com.usatech.keymanager;

import java.util.Date;

public class KeyInfo implements java.io.Serializable
{
	protected int		kid;
	protected Date		createdDate;
	protected String	createdBy;

	public KeyInfo()
	{

	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public String toString()
	{
		return "KeyInfo " + getKid();
	}

	public int getKid()
	{
		return kid;
	}

	public void setKid(int kid)
	{
		this.kid = kid;
	}
}
