package com.usatech.keymanager.master;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.usatech.keymanager.CryptoException;
import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.util.CertUtil;
import com.usatech.keymanager.util.FileUtil;
import com.usatech.keymanager.util.KeyManagerUtil;
import com.usatech.util.Conversions;
import com.usatech.util.Util;

public class KEKStore {
	
	private static final Log log = LogFactory.getLog(KEKStore.class);

	private final KeyManagerLoader loader;
	private String currentKEKAlias;
	private long currentKEKTime;
	private String accountKEKAlias;
	protected KeyManagerUser loadingUser;

	private final Provider provider;
	private final String kekStoreType;
	private final Cipher cipher;
	private final String algorithmName;
	private final String encodingName;
	private final String paddingName;
	private final String transformation;
	private final int keySize;
	private final int ivSize;

	private static String bdkAlgorithm;
	private static String bdkEncoding;
	private static String bdkPadding;
	private static String bdkTransformation;
	
	private final File kekStoreFile;
	private final Map<String, SecretKey> kekMap = new HashMap<String, SecretKey>();

	private static final byte[] C0Var = { (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0, 0x00, 0x00, 0x00, 0x00 };
	private static final byte[] FFVar = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0xFF };
	private static final byte[] FFDataVar = { 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0xFF, 0x00, 0x00 };

	private static final Object synchObject = new Object();

	private static final String PREFIX_AVAS = "avas";	
	private static final String PREFIX_BDK = "bdk";
	private static final String PREFIX_KEK = "kek";
	
	static {
		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null)
      Security.addProvider(new BouncyCastleProvider());
	}

	public KEKStore(KeyManagerLoader loader, char[] masterKey) throws KEKException {
		this(loader, null, masterKey);
	}

	public KEKStore(KeyManagerLoader loader, InputStream kekStoreStream, char[] password) throws KEKException {
	
		this.loader = loader;
		String kekProviderName = loader.getConfiguration().getString("kekStore.providerName");
		String kekStoreFileName = loader.getConfiguration().getString("kekStore.file");
		kekStoreType = loader.getConfiguration().getString("kekStore.type");

		algorithmName = loader.getConfiguration().getString("kekStore.algorithm");
		encodingName = loader.getConfiguration().getString("kekStore.encoding");
		paddingName = loader.getConfiguration().getString("kekStore.padding");
		keySize = loader.getConfiguration().getInt("kekStore.keySize");
		ivSize = loader.getConfiguration().getInt("kekStore.ivSizeBytes");

		transformation = (algorithmName + "/" + encodingName + "/" + paddingName);

		bdkAlgorithm = loader.getConfiguration().getString("kekStore.DUKPT.BDK.algorithm");
		bdkEncoding = loader.getConfiguration().getString("kekStore.DUKPT.BDK.encoding");
		bdkPadding = loader.getConfiguration().getString("kekStore.DUKPT.BDK.padding");
		bdkTransformation = (bdkAlgorithm + "/" + bdkEncoding + "/" + bdkPadding);
		
		provider = Security.getProvider(kekProviderName);
		if(provider == null)
			throw new KEKException("KEK Keystore provider not found: " + kekProviderName);

		try {
			cipher = Cipher.getInstance(transformation, provider);
		} catch(Exception e) {
			throw new KEKException("Error loading cipher: " + e.getMessage(), e);
		}

		kekStoreFile = loader.getFileRealPath(kekStoreFileName);
		log.debug("Loading KEK Keystore from " + kekStoreFile.getAbsolutePath());

		if(kekStoreStream == null)
			try {
				kekStoreStream = new FileInputStream(kekStoreFile);
			} catch(FileNotFoundException e) {
				throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
			}
		KeyStore kekStore;
		try {
			kekStore = KeyStore.getInstance(kekStoreType, provider);
			kekStore.load(kekStoreStream, password);
		} catch(KeyStoreException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} catch(NoSuchAlgorithmException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} catch(CertificateException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} catch(IOException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} finally {
			try {
				kekStoreStream.close();
			} catch(Exception e) {
			}
		}
		try {
			Enumeration<String> aliases = kekStore.aliases();
			while(aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				if(kekStore.isKeyEntry(alias)) {
					KeyStore.PasswordProtection serverKeyPasswordProtection = new KeyStore.PasswordProtection(password);
					KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) kekStore.getEntry(alias, serverKeyPasswordProtection);
					SecretKey key = secretKeyEntry.getSecretKey();

					kekMap.put(alias, key);
					if(alias.startsWith(PREFIX_KEK)) {
						long kekTime = Long.parseLong(alias.substring(PREFIX_KEK.length()));
						if(kekTime > currentKEKTime)
							currentKEKTime = kekTime;
					}
					log.debug("Loaded KEK: " + alias);
				}
			}
		} catch(Exception e) {
			kekMap.clear();
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		}

		if(kekMap.isEmpty()){
			log.warn("KEK Keystore is empty: you may need to generate the initial KEK!");
		}
		if(currentKEKTime == 0) {
			log.warn("KEK Keystore does not contain kek keys: you may need to generate the initial KEK!");
		} else {
			currentKEKAlias = PREFIX_KEK + currentKEKTime;
			log.debug("currentKEKAlias: " + currentKEKAlias);
			accountKEKAlias = loader.getConfiguration().getString("kekStore.accountKEKAlias", null);
			if(accountKEKAlias == null || (accountKEKAlias = accountKEKAlias.trim()).length() == 0) {
				setAccountKEKAlias(currentKEKAlias);
			} else if(!kekMap.containsKey(accountKEKAlias))
				throw new KEKException("KEK Keystore does not contain account KEK " + accountKEKAlias + "; either remove this property to start fresh or correct it");
		}
	}

	
	private synchronized List<String> getKeyAliases(String keyPrefix) {
		List<String> list = new ArrayList<String>();
		for(String alias : kekMap.keySet()) {
			if(alias.startsWith(keyPrefix))
				list.add(alias);
		}
		return list;
	}
	

	public synchronized List<String> getKEKAliases() {
		return getKeyAliases(PREFIX_KEK);
	}
	

	public synchronized List<String> getBDKAliases() {
		return getKeyAliases(PREFIX_BDK);
	}
	

	public synchronized List<String> getAVASAliases() {
		return getKeyAliases(PREFIX_AVAS);
	}
	
	
	public synchronized String getCurrentKEKAlias() {
		return currentKEKAlias;
	}

	
	public synchronized boolean genKEKIfExpired(KeyManagerUser user, char[] masterKey, long expirationMillis) throws KEKException {
		if(currentKEKTime < System.currentTimeMillis() - expirationMillis) {
			genKEK(user, masterKey);
			return true;
		}
		return false;
	}

	
	public synchronized void genKEK(KeyManagerUser user, char[] masterKey) throws KEKException {
		log.info("KEK generation triggered by " + user.getUserInfo());
		KeyGenerator keyGenerator = null;
		try {
			keyGenerator = KeyGenerator.getInstance(algorithmName, provider);
			keyGenerator.init(keySize);
		} catch(Exception e) {
			throw new KEKException("Failed to load KeyGenerator of type " + algorithmName + ": " + e.getMessage(), e);
		}

		SecretKey newKEK = keyGenerator.generateKey();
		long kekTime = System.currentTimeMillis();
		String newAlias = PREFIX_KEK + kekTime;

		log.debug("New KEK alias: " + newAlias);
		kekMap.put(newAlias, newKEK);
		saveKek(masterKey);

		try {
			loader.saveCurrentKEKAlias(newAlias);
		} catch(Exception e) {
			throw new KEKException("Failed to save new KEK alias to configuration: " + e.getMessage(), e);
		}

		currentKEKAlias = newAlias;
		currentKEKTime = kekTime;
		if(accountKEKAlias == null)
			setAccountKEKAlias(newAlias);
	}

	
	public synchronized void saveKek(char[] masterKey) throws KEKException {
		File newKekStoreFile = new File(kekStoreFile.getParentFile(), kekStoreFile.getName() + ".NEW");
		KeyStore.PasswordProtection keyPasswordProtection = new KeyStore.PasswordProtection(masterKey);
		try {
			KeyStore kekStore = KeyStore.getInstance(kekStoreType, provider);
			kekStore.load(null, masterKey);
			try {
				for(String alias : kekMap.keySet()) {
					SecretKey secretKey = kekMap.get(alias);
					KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(secretKey);
					kekStore.setEntry(alias, secretKeyEntry, keyPasswordProtection);
				}
			} catch(Exception e) {
				throw new KEKException("Failed to update KeyStore: " + e.getMessage(), e);
			}
			FileOutputStream fos = new java.io.FileOutputStream(newKekStoreFile);
			try {
				kekStore.store(fos, masterKey);
			} finally {
				fos.close();
			}
			// Now swap files
			FileUtil.swapFiles(kekStoreFile, newKekStoreFile);
			log.info("KEKStore saved to file " + kekStoreFile.getAbsolutePath());
		} catch(IOException e) {
			throw new KEKException("Failed to save KEKStore file: " + e.getMessage(), e);
		} catch(KeyStoreException e) {
			throw new KEKException("Failed to save KEKStore file: " + e.getMessage(), e);
		} catch(NoSuchAlgorithmException e) {
			throw new KEKException("Failed to save KEKStore file: " + e.getMessage(), e);
		} catch(CertificateException e) {
			throw new KEKException("Failed to save KEKStore file: " + e.getMessage(), e);
		}
	}
	
	protected void setAccountKEKAlias(String accountKEKAlias) throws KEKException {
		this.accountKEKAlias = accountKEKAlias;
		try {
			loader.saveAccountKEKAlias(accountKEKAlias);
		} catch(ConfigurationException e) {
			throw new KEKException("Failed to save accountKEKAlias in configuration: " + e.getMessage(), e);
		}
	}
	
	public synchronized byte[] wrap(byte[] bytes) throws CryptoException {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, kekMap.get(currentKEKAlias));
			byte[] ivBytes = cipher.getIV();
			int ivNumBytes = ivBytes.length;
			if(ivNumBytes != ivSize)
				throw new CryptoException("Initialization vector size " + ivNumBytes + " != " + ivSize);

			byte[] encrypted = cipher.doFinal(bytes);
			byte[] combined = new byte[encrypted.length + ivSize];
			System.arraycopy(encrypted, 0, combined, 0, encrypted.length);
			System.arraycopy(ivBytes, 0, combined, encrypted.length, ivSize);

			return combined;
		} catch(Exception e) {
			throw new CryptoException("Failed to wrap using KEK " + currentKEKAlias + ": " + e.getMessage(), e);
		}
	}

	public SecretKey getBDKSecret(long kid) throws CryptoException {
		SecretKey bdk = kekMap.get(PREFIX_BDK + kid);
		if(bdk == null)
			throw new CryptoException("BDK " + kid + " not found in KEKStore!");
		return bdk;
	}

	public SecretKey getAVASSecret(String publicKeyID) throws CryptoException {
		SecretKey avas = kekMap.get(PREFIX_AVAS + publicKeyID);
		if(avas == null)
			throw new CryptoException("AVAS Key " + publicKeyID + " not found in KEKStore!");
		return avas;
	}	
	
	public SecretKey getAccountEncryptingSecret() throws CryptoException {
		SecretKey aek = kekMap.get(accountKEKAlias);
		if(aek == null)
			throw new CryptoException("Key " + accountKEKAlias + " not found in KEKStore!");
		return aek;
	}

	public synchronized void loadSecretKey(PersistentKey persistentKey) throws CryptoException {
		String kekAlias = persistentKey.getKekAlias();
		SecretKey kek = kekMap.get(kekAlias);
		if(kek == null)
			throw new CryptoException("KEK not found for kekAlias: " + kekAlias);

		byte[] keyBytes = CertUtil.decodeBase64(persistentKey.getCipherTextBase64());
		byte[] encryptedKeyBytes = new byte[keyBytes.length - ivSize];
		byte[] ivBytes = new byte[ivSize];

		System.arraycopy(keyBytes, 0, encryptedKeyBytes, 0, encryptedKeyBytes.length);
		System.arraycopy(keyBytes, encryptedKeyBytes.length, ivBytes, 0, ivSize);
		IvParameterSpec iv = new IvParameterSpec(ivBytes);

		byte[] decryptedKeyBytes = null;

		try {
			cipher.init(Cipher.DECRYPT_MODE, kek, iv);
			decryptedKeyBytes = cipher.doFinal(encryptedKeyBytes);
		} catch(Exception e) {
			throw new CryptoException("Failed to decrypt using KEK " + kekAlias + ": " + e.getMessage(), e);
		}

		String transformation = persistentKey.getTransformation();
		String alg = transformation.substring(0, transformation.indexOf('/'));

		SecretKey secretKey = new SecretKeySpec(decryptedKeyBytes, alg);
		persistentKey.setSecretKey(secretKey);
	}

	public synchronized void changeMasterKey(KeyManagerUser user, char[] masterKey) throws KEKException {
		log.info("Master Key Change triggered by " + user.getUserInfo());
		saveKek(masterKey);
	}

	public synchronized KEKInfo genBDK(KeyManagerUser user, char[] masterKey, Collection<byte[]> bdkComponents, long keyID, boolean update) throws KEKException {
		log.info("BDK load triggered by " + user.getUserInfo());

		String kcv = null;
		SecretKey newBDK = null;
		byte[] bdk = null;
		int i = 0;

		try {
			for(byte[] bdkComponent : bdkComponents) {
				if(bdk == null) {
					bdk = new byte[24];
					System.arraycopy(bdkComponent, 0, bdk, 0, 16);
					for(i = 16; i < 24; i++)
						bdk[i] = 0x00;
				} else {
					for(i = 0; i < 16; i++)
						bdk[i] = (byte) (bdk[i] ^ bdkComponent[i]);
				}
			}

			kcv = getKCV(bdk);
			DESedeKeySpec keySpec = new DESedeKeySpec(bdk);
			newBDK = SecretKeyFactory.getInstance(bdkAlgorithm).generateSecret(keySpec);
		} catch(Exception e) {
			throw new KEKException("Failed to generate BDK: " + e.getMessage(), e);
		} finally {
			if(bdk != null) {
				Util.overwrite(bdk);
				bdk = null;
			}
		}

		String newAlias = PREFIX_BDK + keyID;
		log.debug("New BDK alias: " + newAlias);
		if(update) {
			if(!kekMap.containsKey(newAlias))
				throw new KEKException("BDK with ID " + keyID + " does not exist in the KeyStore");
		} else if(kekMap.containsKey(newAlias))
			throw new KEKException("BDK with ID " + keyID + " already exists in the KeyStore");
		kekMap.put(newAlias, newBDK);
		saveKek(masterKey);

		return new KEKInfo(keyID, kcv);
	}

	
	public synchronized KEKInfo genAVAS(KeyManagerUser user, char[] masterKey, Collection<byte[]> avasComponents, long keyID) throws KEKException {
		
		log.info("AVAS load triggered by " + user.getUserInfo());

		SecretKey avasKey = null;
		BigInteger avasKeyNumber = null;
		byte[] avasKeyBytes = null;
		int crc;
		KEKInfo kekInfo = new KEKInfo();

		try {
			//XOR the components
			for(byte[] avasComponent : avasComponents) {
				if (avasKeyNumber == null)
					avasKeyNumber = new BigInteger(avasComponent);
				else
					avasKeyNumber = avasKeyNumber.xor(new BigInteger(avasComponent));
			}
			avasKeyBytes = avasKeyNumber.toByteArray();
			//the algorithm doesn't matter and is not checked by Java, we are just storing raw bytes in SecretKey
			avasKey = new SecretKeySpec(avasKeyBytes, "RC4");

			crc = KeyManagerUtil.generateCRC(avasKeyBytes); 
			kekInfo.setCRC(crc);
			KeyManagerUtil.generateAVASKeyPair(avasKeyNumber, kekInfo);
		} catch(Exception e) {
			throw new KEKException("Failed to generate AVAS: " + e.getMessage(), e);
		} finally {
			if(avasKeyNumber != null) {
				avasKeyNumber = null;
			}
			if(avasKeyBytes != null) {
				Util.overwrite(avasKeyBytes);
				avasKeyBytes = null;
			}
		}

		String newAlias = PREFIX_AVAS + kekInfo.getPublicKeyID().toLowerCase();
		log.debug("New AVAS alias: " + newAlias);
		if(kekMap.containsKey(newAlias))
			throw new KEKException("AVAS Key with ID " + kekInfo.getPublicKeyID() + " already exists in the KeyStore");
		
		kekMap.put(newAlias, avasKey);
		saveKek(masterKey);

		log.info("Generated AVAS key, Public Key Hex: " + kekInfo.getPublicKeyHex() + ", Public Key ID: " + kekInfo.getPublicKeyID() + ", CRC: " + crc);
		return kekInfo;
	}
	
	
	private void getInitialDUKPTKey(long keyID, byte[] ksn, byte[] initialKey1, byte[] initialKey2) throws KEKException {
		String bdkAlias = PREFIX_BDK + keyID;

		SecretKey secretBDK;
		synchronized(synchObject) {
			if(!kekMap.containsKey(bdkAlias))
				throw new KEKException("BDK alias " + keyID + " not found in KEKStore!");
			secretBDK = kekMap.get(bdkAlias);
		}

		try {
			byte[] bdk = secretBDK.getEncoded();
			byte[] bdk1 = new byte[8];
			byte[] bdk2 = new byte[8];
			System.arraycopy(bdk, 0, bdk1, 0, 8);
			System.arraycopy(bdk, 8, bdk2, 0, 8);

			byte[] KSN8 = new byte[8];
			System.arraycopy(ksn, 0, KSN8, 0, 8);
			KSN8[7] &= Integer.parseInt("11100000", 2);

			byte[] bdk1C0Var = new byte[8];
			byte[] bdk2C0Var = new byte[8];

			for(int i = 0; i < 8; i++) {
				bdk1C0Var[i] = (byte) (bdk1[i] ^ C0Var[i]);
				bdk2C0Var[i] = (byte) (bdk2[i] ^ C0Var[i]);
			}

			byte[] tdesBDK1 = new byte[24];
			System.arraycopy(bdk1, 0, tdesBDK1, 0, 8);
			System.arraycopy(bdk2, 0, tdesBDK1, 8, 8);
			System.arraycopy(bdk1, 0, tdesBDK1, 16, 8);

			byte[] tdesBDK2 = new byte[24];
			System.arraycopy(bdk1C0Var, 0, tdesBDK2, 0, 8);
			System.arraycopy(bdk2C0Var, 0, tdesBDK2, 8, 8);
			System.arraycopy(bdk1C0Var, 0, tdesBDK2, 16, 8);

			Cipher cipher = Cipher.getInstance(bdkTransformation);
			DESedeKeySpec keySpec = new DESedeKeySpec(tdesBDK1);
			SecretKey secretKey = SecretKeyFactory.getInstance(bdkAlgorithm).generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			System.arraycopy(cipher.doFinal(KSN8), 0, initialKey1, 0, 8);

			keySpec = new DESedeKeySpec(tdesBDK2);
			secretKey = SecretKeyFactory.getInstance(bdkAlgorithm).generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			System.arraycopy(cipher.doFinal(KSN8), 0, initialKey2, 0, 8);
		} catch(Exception e) {
			throw new KEKException("Failed to generate initial DUKPT key: " + e.getMessage(), e);
		}
	}

	public void getDUKPTKey(byte keyVariantTypeId, long keyID, byte[] KSN, byte[] key1, byte[] key2) throws KEKException {
		getInitialDUKPTKey(keyID, KSN, key1, key2);

		try {
			byte[] R8 = new byte[8];
			byte[] R8A = new byte[8];
			byte[] R8B = new byte[8];
			System.arraycopy(KSN, 2, R8, 0, 8);
			byte[] R3Array = { 0x00, (byte) (R8[5] & Integer.parseInt("00011111", 2)), R8[6], R8[7] };
			int R3 = Conversions.byteArrayToInt(R3Array);
			R8[5] &= Integer.parseInt("11100000", 2);
			R8[6] = 0;
			R8[7] = 0;
			int SR = Integer.parseInt("100000000000000000000", 2);
			Cipher cipher = Cipher.getInstance("DES/ECB/NOPADDING");
			DESKeySpec keySpec;
			SecretKey secretKey;
			int i = 0;

			while(true) {
				if((SR & R3) == 0) {
					SR = SR >> 1;
					if(SR == 0) {
						if (keyVariantTypeId == 2) {
							// data key variant
							for(i = 0; i < 8; i++) {
								key1[i] = (byte) (key1[i] ^ FFDataVar[i]);
								key2[i] = (byte) (key2[i] ^ FFDataVar[i]);
							}
							byte[] key = new byte[24];
							System.arraycopy(key1, 0, key, 0, 8);
							System.arraycopy(key2, 0, key, 8, 8);
							System.arraycopy(key1, 0, key, 16, 8);
							DESedeKeySpec dataKeySpec = new DESedeKeySpec(key);
							Cipher dataCipher = Cipher.getInstance("DESede/ECB/NOPADDING");
							SecretKey dataSecretKey = SecretKeyFactory.getInstance("DESede").generateSecret(dataKeySpec);
							dataCipher.init(Cipher.ENCRYPT_MODE, dataSecretKey);
							byte[] encrypted1 = dataCipher.doFinal(key1);
							System.arraycopy(encrypted1, 0, key1, 0, encrypted1.length);
							byte[] encrypted2 = dataCipher.doFinal(key2);
							System.arraycopy(encrypted2, 0, key2, 0, encrypted2.length);
						} else {
							// PIN key variant
							for(i = 0; i < 8; i++) {
								key1[i] = (byte) (key1[i] ^ FFVar[i]);
								key2[i] = (byte) (key2[i] ^ FFVar[i]);
							}
						}
						return;
					}
					continue;
				}

				byte[] SRArray = Conversions.intToByteArray(SR);
				for(i = 1; i < 4; i++)
					R8[i + 4] |= SRArray[i];

				for(i = 0; i < 8; i++)
					R8A[i] = (byte) (key2[i] ^ R8[i]);

				keySpec = new DESKeySpec(key1);
				secretKey = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				System.arraycopy(cipher.doFinal(R8A), 0, R8A, 0, 8);

				for(i = 0; i < 8; i++) {
					R8A[i] ^= key2[i];
					key1[i] ^= C0Var[i];
					key2[i] ^= C0Var[i];
					R8B[i] = (byte) (key2[i] ^ R8[i]);
				}

				keySpec = new DESKeySpec(key1);
				secretKey = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				System.arraycopy(cipher.doFinal(R8B), 0, R8B, 0, 8);

				for(i = 0; i < 8; i++)
					R8B[i] ^= key2[i];
				System.arraycopy(R8B, 0, key1, 0, 8);
				System.arraycopy(R8A, 0, key2, 0, 8);
				SR = SR >> 1;
			}
		} catch(Exception e) {
			throw new KEKException("Failed to generate DUKPT key: " + e.getMessage(), e);
		}
	}

	public static String getKCV(byte[] key) throws KEKException {
		byte[] tdesKey = new byte[24];
		try {
			byte[] plainText = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			System.arraycopy(key, 0, tdesKey, 0, 16);
			System.arraycopy(key, 0, tdesKey, 16, 8);

			Cipher cipher = Cipher.getInstance(bdkTransformation);
			DESedeKeySpec keySpec = new DESedeKeySpec(tdesKey);
			SecretKey secretKey = SecretKeyFactory.getInstance(bdkAlgorithm).generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Conversions.bytesToHex(cipher.doFinal(plainText)).substring(0, 6);
		} catch(Exception e) {
			throw new KEKException("Failed to generate KCV: " + e.getMessage(), e);
		} finally {
			Util.overwrite(tdesKey);
			tdesKey = null;
		}
	}
	
	public PrivateKey getAVASPrivateKey(String publicKeyID) throws KEKException {
		String keyAlias = PREFIX_AVAS + publicKeyID.toLowerCase();

		SecretKey secretKey;
		synchronized(synchObject) {
			if(!kekMap.containsKey(keyAlias)) {
				log.info("AVAS Key alias " + keyAlias + " not found in KEKStore, forwarding encrypted data");
				return null;
			}
			secretKey = kekMap.get(keyAlias);
		}

		PrivateKey privateKey;
		try {
			privateKey = KeyManagerUtil.loadAVASPrivateKey(secretKey);
		} catch (NoSuchAlgorithmException e) {
			throw new KEKException("Failed to get AVAS Private Key: " + e.getMessage(), e);
		} catch (NoSuchProviderException e) {
			throw new KEKException("Failed to get AVAS Private Key: " + e.getMessage(), e);
		} catch (InvalidKeySpecException e) {
			throw new KEKException("Failed to get AVAS Private Key: " + e.getMessage(), e);
		}
		return privateKey;
	}
	
	public Map<String, String> testLoad(char[] masterKey) throws KEKException {
		InputStream kekStoreStream;
		try {
			kekStoreStream = new FileInputStream(kekStoreFile);
		} catch(FileNotFoundException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		}
		KeyStore kekStore;
		try {
			kekStore = KeyStore.getInstance(kekStoreType, provider);
			kekStore.load(kekStoreStream, masterKey);
		} catch(KeyStoreException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} catch(NoSuchAlgorithmException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} catch(CertificateException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} catch(IOException e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		} finally {
			try {
				kekStoreStream.close();
			} catch(Exception e) {
			}
		}
		Map<String, String> entries = new LinkedHashMap<String, String>();
		try {
			Enumeration<String> aliases = kekStore.aliases();
			while(aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				if(kekStore.isKeyEntry(alias)) {
					KeyStore.PasswordProtection serverKeyPasswordProtection = new KeyStore.PasswordProtection(masterKey);
					KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) kekStore.getEntry(alias, serverKeyPasswordProtection);
					SecretKey key = secretKeyEntry.getSecretKey();
					entries.put(alias, key.getAlgorithm() + " - " + key.getFormat());
					log.debug("Loaded KEK: " + alias);
				}
			}
		} catch(Exception e) {
			throw new KEKException("Failed to load KEK Keystore: " + e.getMessage(), e);
		}

		return entries;
	}

	public KeyManagerUser getLoadingUser() {
		return loadingUser;
	}

	public void setLoadingUser(KeyManagerUser loadingUser) {
		this.loadingUser = loadingUser;
	}
	
	public void kekStoreUpdated(){
		loader.kekStoreUpdated(kekMap);
	}
}
