package com.usatech.keymanager.master;

import java.io.File;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.ssss.SSSS;

public interface KeyManagerLoader {
	public static final String CONTEXT_KEY = (KeyManagerLoader.class.getName() + "-KEY");

	public static final int STATE_NOT_STARTED = 0;
	public static final int STATE_START_FAILED = 1;
	public static final int STATE_NOT_INSTALLED = 2;
	public static final int STATE_INSTALL_FAILED = 3;
	public static final int STATE_INSTALLED = 4;
	public static final int STATE_LOAD_FAILED = 5;
	public static final int STATE_LOADED = 6;
	public static final int STATE_INITIALIZE_FAILED = 7;
	public static final int STATE_INITIALIZED = 8;
	public static final String[] STATE_NAMES = { "NOT_STARTED", "START_FAILED", "NOT_INSTALLED", "INSTALL_FAILED", "INSTALLED", "LOAD_FAILED", "LOADED", "INITIALIZE_FAILED", "INITIALIZED" };

	public void setState(int _state);

	public int getState();

	public PropertiesConfiguration getConfiguration();

	public File getFileRealPath(String fileName);

	public SSSS getSSSS();

	public File getSecretStoreFile();

	public String getAsymmetricCipherID();

	public Cipher getAsymmetricCipher();

	public int getAVASShareCount();
	
	public int getBDKShareCount();

	public void masterKeyLoaded(KeyManagerUser user, char[] masterKey);

	public KEKStore getKEKStore();

	public KeyManagerInstaller getKeyManagerInstaller();

	public MasterKeyLoader getMasterKeyLoader() throws MasterKeyException;

	public void saveCurrentKEKAlias(String alias) throws ConfigurationException;
	
	public void saveAccountKEKAlias(String alias) throws ConfigurationException;

	public void kekStoreUpdated(Map<String, SecretKey> kekMap);

	public String getVersion();

}