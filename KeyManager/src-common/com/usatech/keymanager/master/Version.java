package com.usatech.keymanager.master;

public class Version {
	private static final String version;
	private static final String releaseDate;
	static {
		Package p = Version.class.getPackage();
		if(p != null) {
			String tmp = p.getImplementationVersion();
			if(tmp != null) {
				int pos = tmp.lastIndexOf('~');
				if(pos < 0) {		
					version = tmp;
					releaseDate = "UNKNOWN";
				} else {
					version = tmp.substring(0, pos);
					releaseDate = tmp.substring(pos + 1);
				}
			} else {
				version = "UNKNOWN";
				releaseDate = "UNKNOWN";
			}
		} else {
			version = "UNKNOWN";
			releaseDate = "UNKNOWN";
		}
	}

	public static String getVersion() {
		return version;
	}

	public static String getReleaseDate() {
		return releaseDate;
	}
}
