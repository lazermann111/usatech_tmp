package com.usatech.keymanager.master;

public class KEKInfo
{
    protected long      keyID;
    protected String    KCV;    
    protected String		publicKeyHex;
    protected String		publicKeyID;
    protected int				CRC;
    protected int				publicKeyCrc;
    
    public KEKInfo()
    {

    }
    
    public KEKInfo(long keyID, String KCV)
    {
        this.keyID = keyID;
        this.KCV = KCV;
    }
    
    public KEKInfo(String publicKeyHex, String publicKeyID, int CRC) {    		
    		this.publicKeyHex = publicKeyHex;
    		this.publicKeyID = publicKeyID;
    		this.CRC = CRC;
    }

    public String getKCV()
    {
        return KCV;
    }

    public void setKCV(String KCV)
    {
        this.KCV = KCV;
    }

    public String toString()
    {
        return "KEKInfo keyID: " + getKeyID() + ", KCV: " + getKCV();
    }

    public long getKeyID()
    {
        return keyID;
    }

    public void setKid(long keyID)
    {
        this.keyID = keyID;
    }

		public String getPublicKeyHex() {
			return publicKeyHex;
		}

		public void setPublicKeyHex(String publicKeyHex) {
			this.publicKeyHex = publicKeyHex;
		}

		public String getPublicKeyID() {
			return publicKeyID;
		}

		public void setPublicKeyID(String publicKeyID) {
			this.publicKeyID = publicKeyID;
		}

		public int getCRC() {
			return CRC;
		}

		public void setCRC(int cRC) {
			CRC = cRC;
		}

		public int getPublicKeyCrc() {
			return publicKeyCrc;
		}

		public void setPublicKeyCrc(int publicKeyCrc) {
			this.publicKeyCrc = publicKeyCrc;
		}   
}
