package com.usatech.keymanager.master;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.crypto.Cipher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.ssss.SSSS;
import com.usatech.keymanager.ssss.SSSSException;
import com.usatech.keymanager.util.FileUtil;
import com.usatech.keymanager.util.RandPass;
import com.usatech.util.Util;

public class MasterKeyLoader {
	
	private static Log log = LogFactory.getLog(MasterKeyLoader.class);
    
	public static final int ACTION_INSTALL              = 0;
  public static final int ACTION_LOAD                 = 1;
  public static final int ACTION_CHANGE_KEK           = 2;
  public static final int ACTION_CHANGE_MASTER_KEY    = 3;
  public static final int ACTION_GENERATE_BDK         = 4;
  public static final int ACTION_SAVE_KEK             = 5;
	public static final int ACTION_UPDATE_BDK           = 6;
	public static final int ACTION_GENERATE_AVAS_KEY    = 7;
	public static final int ACTION_GENERATE_AVAS_COMPONENT = 8;
	
	public static final String[] ACTION_NAMES = { "Install", "Load", "KEK Change", "Master Key Change", "Generate BDK", "Save KEK Store", "Update BDK", "Generate AVAS Key", "Generate AVAS Component"};

	private KeyManagerLoader loader;
	private String asymmetricCipherID;
  private Cipher asymmetricCipher;
	private SSSS ssss;
	private int	loadTimeoutSeconds;
	private long loadStartTS	= 0;
	private File secretStoreFile;
	private int bdkShareCount;
	private int avasShareCount;
	
	private Map<String, EncryptedSecret> encryptedSecretMap	= new HashMap<String, EncryptedSecret>();
	private LinkedHashMap<KeyManagerUser, char[]>	loaderSecrets	= new LinkedHashMap<KeyManagerUser, char[]>();
	private LinkedHashMap<KeyManagerUser, byte[]> loaderBDKComponents = new LinkedHashMap<KeyManagerUser, byte[]>();
	private long loaderBDKKeyID = 0;
	private LinkedHashMap<KeyManagerUser, byte[]> loaderAVASComponents = new LinkedHashMap<KeyManagerUser, byte[]>();
	private long loaderAVASKeyID = 0;
	
	
	MasterKeyLoader(KeyManagerLoader loader) throws MasterKeyException	{
		
    log.debug("Loading MasterKeyLoader, KeyManagerLoader state is " + KeyManagerLoader.STATE_NAMES[loader.getState()]);
    
    this.loader = loader;
    this.ssss = loader.getSSSS();
    this.secretStoreFile = loader.getSecretStoreFile();
    this.asymmetricCipherID = loader.getAsymmetricCipherID();
    this.asymmetricCipher = loader.getAsymmetricCipher();

    bdkShareCount = loader.getBDKShareCount();
    avasShareCount = loader.getAVASShareCount();
    
    loadTimeoutSeconds = loader.getConfiguration().getInt("masterKeyLoader.loadTimeoutSeconds");
    loadEncryptedSecretMap();
	
	}

	
	public String getAsymmetricCipherID(){
		return asymmetricCipherID;
	}

	
	private synchronized void clearIfExpired(){
		if (loadStartTS > 0 && getLoadRemainingSeconds() <= 0){
			log.warn("Load timeout occured: clearing secrets!");
			clearSecrets();
			loadStartTS = 0;
		}
	}
	
	
	private synchronized void clearSecrets(){
		
    for (char[] secret : loaderSecrets.values())
          Util.overwrite(secret);
    loaderSecrets.clear();
    
    for (byte[] component : loaderBDKComponents.values())
          Util.overwrite(component);
    loaderBDKComponents.clear();
    loaderBDKKeyID = 0;
    
    for (byte[] component : loaderAVASComponents.values())
      Util.overwrite(component);
    loaderAVASComponents.clear();
    loaderAVASKeyID = 0;
    
	}
  
	
	public synchronized int getLoadTimeoutSeconds(){
		return loadTimeoutSeconds;
	}

	
	public synchronized int getLoadRemainingSeconds(){
		int remaining = (int) (getLoadTimeoutSeconds() - ((System.currentTimeMillis() - loadStartTS) / 1000));
		if (remaining <= 0)
			return 0;
		return remaining;
	}

	
	private synchronized KeyManagerUser getFirstUserForLoad(){
		if (loaderSecrets.isEmpty())
			return null;

		return loaderSecrets.keySet().iterator().next();
	}

	
	public synchronized boolean canLoad(KeyManagerUser user){
		clearIfExpired();

		if (encryptedSecretMap.containsKey(user.getCertThumbprint()) && loaderSecrets.containsKey(user) && user.equals(getFirstUserForLoad()) && loaderSecrets.size() >= ssss.getT())
			return true;

		return false;
	}
	
  public synchronized boolean canGenerateBDK(KeyManagerUser user){
    if (canLoad(user) && loaderBDKComponents.containsKey(user) && loaderBDKComponents.size() >= bdkShareCount)
        return true;

    return false;
  }	

  
  public synchronized boolean canGenerateAVAS(KeyManagerUser user){
  	boolean canGenAvas = false;
  	
    if (canLoad(user) && loaderAVASComponents.containsKey(user) && loaderAVASComponents.size() >= avasShareCount)
    	canGenAvas = true;

    return canGenAvas;
  }	    
  
  
  public synchronized boolean isKeyCustodian(String certThumbprint){
    return encryptedSecretMap.containsKey(certThumbprint);        
  }

  
	public synchronized byte[] getEncryptedSecret(String certThumbprint) throws MasterKeyException {
		clearIfExpired();

		if (!encryptedSecretMap.containsKey(certThumbprint))
			throw new MasterKeyException("Your certificate is not part of the Secret Store!");

		EncryptedSecret secret = encryptedSecretMap.get(certThumbprint);
		return secret.getBytes();
	}
	
	
	public synchronized void addDecryptedSecret(KeyManagerUser user, char[] decryptedSecret) throws MasterKeyException {
		clearIfExpired();

		if (decryptedSecret == null || decryptedSecret.length == 0)
			throw new MasterKeyException("Empty secret!");

		if (!encryptedSecretMap.containsKey(user.getCertThumbprint()))
			throw new MasterKeyException("Your certificate is not part of the Secret Store!");

		if (loaderSecrets.isEmpty())
			loadStartTS = System.currentTimeMillis();

		loaderSecrets.put(user, decryptedSecret);
		log.debug(loaderSecrets.size() + " secret(s) are loaded after the addition from " + user.getUserInfo());
	}
	
	
	public synchronized void addBDKComponent(KeyManagerUser user, byte[] BDKComponent, long keyID) throws MasterKeyException {
    clearIfExpired();

    if (BDKComponent == null || BDKComponent.length == 0)
        throw new MasterKeyException("Empty BDK component!");
    
    if (BDKComponent.length != 16)
        throw new MasterKeyException("Invalid BDK component!");

    if (!encryptedSecretMap.containsKey(user.getCertThumbprint()))
        throw new MasterKeyException("Your certificate is not part of the Secret Store!");
        
		if(loaderBDKKeyID == 0) {
			if(keyID == 0)
				throw new MasterKeyException("BDK Key Id may not be 0");
			loaderBDKKeyID = keyID;
		} else if(keyID != loaderBDKKeyID)
			throw new MasterKeyException("BDK Key Id " + keyID + " does not match the one currently being loaded (" + loaderBDKKeyID + ")");
		
    loaderBDKComponents.put(user, BDKComponent);
    log.debug(loaderBDKComponents.size() + " BDK component(s) are loaded after the addition from " + user.getUserInfo());
  }	

	
	public synchronized void addAVASComponent(KeyManagerUser user, byte[] AVASComponent, long keyID) throws MasterKeyException {
    clearIfExpired();
    
    if (AVASComponent == null || AVASComponent.length == 0)
        throw new MasterKeyException("Empty AVAS component!");
      
    if (AVASComponent.length < 32)
        throw new MasterKeyException("Invalid AVAS component!");

    if (!encryptedSecretMap.containsKey(user.getCertThumbprint()))
        throw new MasterKeyException("Your certificate is not part of the Secret Store!");
      
		if(loaderAVASKeyID == 0) {
			if(keyID == 0)
				throw new MasterKeyException("AVAS Key Id may not be 0");
			loaderAVASKeyID = keyID;
		} else if(keyID != loaderAVASKeyID)
			throw new MasterKeyException("AVAS Key Id " + keyID + " does not match the one currently being loaded (" + loaderAVASKeyID + ")");
		
    loaderAVASComponents.put(user, AVASComponent);
    log.debug(loaderAVASComponents.size() + " AVAS component(s) are loaded after the addition from " + user.getUserInfo());
  }	
	
	
	public synchronized SortedMap<String, String> getKeyCustodiansInfo() {
		
    TreeMap<String, String> treeMap = new TreeMap<String, String>();
		for (String certThumbprint : encryptedSecretMap.keySet()){
	    EncryptedSecret encryptedSecret = encryptedSecretMap.get(certThumbprint);
	    treeMap.put(encryptedSecret.getSubjectDN() + "; " + certThumbprint + "; " + encryptedSecret.getCertificate().getNotAfter(), certThumbprint);
    }

		return treeMap;
	}

	
	public synchronized SortedMap<String, KeyManagerUser> getLoadersInfo() {
		clearIfExpired();

    TreeMap<String, KeyManagerUser> treeMap = new TreeMap<String, KeyManagerUser>();
		for (KeyManagerUser user : loaderSecrets.keySet())
			treeMap.put(user.getUserInfo(), user);

		return treeMap;
	}
	
	
  public synchronized SortedMap<String, KeyManagerUser> getBDKLoadersInfo(){
    clearIfExpired();

    TreeMap<String, KeyManagerUser> treeMap = new TreeMap<String, KeyManagerUser>();
    for (KeyManagerUser user : loaderBDKComponents.keySet())
        treeMap.put(user.getUserInfo(), user);

    return treeMap;
  }	

  
  public synchronized SortedMap<String, KeyManagerUser> getAVASLoadersInfo() {
    clearIfExpired();

    TreeMap<String, KeyManagerUser> treeMap = new TreeMap<String, KeyManagerUser>();
    for (KeyManagerUser user : loaderAVASComponents.keySet())
        treeMap.put(user.getUserInfo(), user);

    return treeMap;
  }
  
    
	public synchronized KEKInfo load(int action, KeyManagerUser user, ArrayList<String> certThumbprints) throws MasterKeyException, KeyManagerException	{
		
    log.info("load: " + ACTION_NAMES[action] + " triggered by: " + user.getUserInfo());
        
		clearIfExpired();

		KEKInfo kekInfo = null;
    String certThumbprint = user.getCertThumbprint();
        
		if (!encryptedSecretMap.containsKey(certThumbprint))
			throw new MasterKeyException("Your certificate is not part of the Secret Store!");

		if (loaderSecrets.size() < 1)
			return null;
		
		if (!loaderSecrets.containsKey(user))
			throw new MasterKeyException("You are not part of the load group!");

		if (!user.equals(getFirstUserForLoad()))
			throw new MasterKeyException("Only the first user in the load group may trigger the load!");

		if (loaderSecrets.size() < ssss.getT())
			throw new MasterKeyException("Insufficient secrets are available to load!");

		List<char[]> decryptedSecretList = new ArrayList<char[]>();
		for (char[] decryptedSecret : loaderSecrets.values())
			decryptedSecretList.add(decryptedSecret);

		log.debug("Attempting to combine decrypted secrets to retrieve master key...");

		char[] masterKey = null;
		try {
			masterKey = ssss.combine(decryptedSecretList);
		} catch(SSSSException e) {
			throw new MasterKeyException("Failed to combine decrypted secrets: " + e.getMessage(), e);
		}
		for (char[] c : decryptedSecretList)
			Util.overwrite(c);

		if(masterKey.length > (ssss.getKeySize() / 8))
			throw new MasterKeyException("Native SSSS process output unexpected data. Process output: " + (log.isDebugEnabled() ? new String(masterKey) : masterKey.length + " bytes"));

        loader.masterKeyLoaded(user, masterKey);
                
		long expirationMillis = loader.getConfiguration().getLong("kekStore.kekExpirationMillis", 7776000000L); /*default is 90 days */

		switch(action) {
			case ACTION_CHANGE_KEK:
				try {
					loader.getKEKStore().genKEK(user, masterKey);
				} catch(KEKException e) {
					log.error("KEK generation failed: " + e.getMessage(), e);
				}
				loader.getKEKStore().kekStoreUpdated();
				break;
			case ACTION_CHANGE_MASTER_KEY:
				ArrayList<KeyManagerUser> installers = new ArrayList<KeyManagerUser>();
				for(KeyManagerUser userForInstall : loaderSecrets.keySet())
					installers.add(userForInstall);

				if(certThumbprints != null) {
					for(String thumbprint : certThumbprints) {
						KeyManagerUser userForInstall = KeyManagerUser.getByCertThumbprint(thumbprint);
						installers.add(userForInstall);
					}
				}
				generateMasterKey(action, user, installers);
				loader.getKEKStore().kekStoreUpdated();
				break;
			case ACTION_GENERATE_BDK:
			case ACTION_UPDATE_BDK:
				if(loaderBDKComponents.size() < bdkShareCount)
					throw new KeyManagerException("Insufficient components are available to generate BDK!");

				try {
					kekInfo = loader.getKEKStore().genBDK(user, masterKey, loaderBDKComponents.values(), getLoaderBDKKeyID(), action == ACTION_UPDATE_BDK);
				} catch(KEKException e) {
					log.error("BDK generation failed: " + e.getMessage(), e);
					throw new KeyManagerException(e.getMessage(), e);
				}
				try {
					loader.getKEKStore().genKEKIfExpired(user, masterKey, expirationMillis);
				} catch(KEKException e) {
					log.error("KEK (expired) generation failed : " + e.getMessage(), e);
				}
				loader.getKEKStore().kekStoreUpdated();
				break;
			case ACTION_LOAD:
				try {
					loader.getKEKStore().genKEKIfExpired(user, masterKey, expirationMillis);
				} catch(KEKException e) {
					log.error("KEK (expired) generation failed : " + e.getMessage(), e);
				}
				loader.getKEKStore().kekStoreUpdated();
				break;
			case ACTION_SAVE_KEK:
				try {
					loader.getKEKStore().saveKek(masterKey);
				} catch(KEKException e) {
					log.error("Saving KEK Store failed : " + e.getMessage(), e);
				}
				break;
			case ACTION_GENERATE_AVAS_KEY:
				if(loaderAVASComponents.size() < avasShareCount)
					throw new KeyManagerException("Insufficient components are available to generate AVAS!");

				try {
					kekInfo = loader.getKEKStore().genAVAS(user, masterKey, loaderAVASComponents.values(), getLoaderAVASKeyID());
				} catch(KEKException e) {
					log.error("AVAS generation failed: " + e.getMessage(), e);
					throw new KeyManagerException(e.getMessage(), e);
				}
				try {
					loader.getKEKStore().genKEKIfExpired(user, masterKey, expirationMillis);
				} catch(KEKException e) {
					log.error("KEK (expired) generation failed : " + e.getMessage(), e);
				}
				loader.getKEKStore().kekStoreUpdated();
				break;
		}

    Util.overwrite(masterKey);
    masterKey = null;
    decryptedSecretList.clear();
    clearSecrets();
    loadStartTS = 0;
    return kekInfo;
	}
    
	public synchronized Map<String, String> testLoad(KeyManagerUser user) throws MasterKeyException, KeyManagerException, KEKException {
		log.info("test load triggered by: " + user.getUserInfo());

		clearIfExpired();

		String certThumbprint = user.getCertThumbprint();
		if(!encryptedSecretMap.containsKey(certThumbprint))
			throw new MasterKeyException("Your certificate is not part of the Secret Store!");

		if (loaderSecrets.size() < 1)
			return null;
		
		if(!loaderSecrets.containsKey(user))
			throw new MasterKeyException("You are not part of the load group!");

		if(!user.equals(getFirstUserForLoad()))
			throw new MasterKeyException("Only the first user in the load group may trigger the load!");

		if(loaderSecrets.size() < ssss.getT())
			throw new MasterKeyException("Insufficient secrets are available to load!");

		List<char[]> decryptedSecretList = new ArrayList<char[]>();
		for(char[] decryptedSecret : loaderSecrets.values())
			decryptedSecretList.add(decryptedSecret);

		log.debug("Attempting to combine decrypted secrets to retrieve master key...");
		char[] masterKey = null;
		try {
			masterKey = ssss.combine(decryptedSecretList);
		} catch(SSSSException e) {
			throw new MasterKeyException("Failed to combine decrypted secrets: " + e.getMessage(), e);
		}
		for(char[] c : decryptedSecretList)
			Util.overwrite(c);

		if(masterKey.length > (ssss.getKeySize() / 8))
			throw new MasterKeyException("Native SSSS process output unexpected data. Process output: " + (log.isDebugEnabled() ? new String(masterKey) : masterKey.length + " bytes"));

		Map<String, String> entries = loader.getKEKStore().testLoad(masterKey);

		Util.overwrite(masterKey);
		masterKey = null;
		decryptedSecretList.clear();
		clearSecrets();
		loadStartTS = 0;
		return entries;
	}

	
  public synchronized void generateMasterKey(int action, KeyManagerUser user, ArrayList<KeyManagerUser> installers) throws MasterKeyException {
  	
    try {
    	
      log.info("generateMasterKey: " + ACTION_NAMES[action] + " triggered by: " + user.getUserInfo());
      
      encryptedSecretMap.clear();

      int n = ssss.getN();
      
      if (installers.size() < n)
          throw new MasterKeyException("Number of installers is less than " + n);

      RandPass randPass = new RandPass(RandPass.PRINTABLE_ALPHABET);
      char[] masterKey = randPass.getPassChars(ssss.getKeySize() / 8);

      List<char[]> secrets = null;

      try {
          secrets = ssss.split(masterKey);
      } catch (SSSSException e) {
          throw new MasterKeyException("Failed to split master key: " + e.getMessage(), e);
      }

      log.debug("Encrypting secrets...");

      KeyManagerUser[] installerArray = new KeyManagerUser[n];
      installers.toArray(installerArray);

      for (int i = 0; i < n; i++) {
        KeyManagerUser installer = installerArray[i];
        String installerInfo = installer.getUserInfo();
        X509Certificate cert = installer.getCertificate();
        
        char[] secretChars = secrets.get(i);
        byte[] secretBytes = Util.convertToBytes(secretChars);
        Util.overwrite(secretChars);
        
        try {
            log.debug("Initializing asymmetric cipher for " + i);
            asymmetricCipher.init(Cipher.ENCRYPT_MODE, cert.getPublicKey());
        } catch (Exception e) {
            throw new MasterKeyException("Failed to initialize asymmetric cipher for " + installerInfo + ": " + e.getMessage(), e);
        }
        
        byte[] asymEncryptedBytes = null;
        
        try
        {
            asymEncryptedBytes = asymmetricCipher.doFinal(secretBytes);
            log.debug("Successfully asymmetric encrypted secret for " + installerInfo);
        }
        catch (Exception e)
        {
            throw new MasterKeyException("Failed to asymmetric encrypt secret for " + installerInfo + ": " + e.getMessage(), e);
        }
        Util.overwrite(secretBytes);
        encryptedSecretMap.put(installer.getCertThumbprint(), new EncryptedSecret(installer.getSubjectName(), cert, asymEncryptedBytes));
      }

      File kekStoreFile = null;
      KeyStore kekStore = null;
      
      if (action == ACTION_INSTALL) {
          if (secretStoreFile.exists())
              throw new MasterKeyException("SecretStore file already exists at: " + secretStoreFile.getAbsolutePath());

          String kekStoreFileName = loader.getConfiguration().getString("kekStore.file");
          kekStoreFile = loader.getFileRealPath(kekStoreFileName);
          if (kekStoreFile.exists())
              throw new MasterKeyException("KEK Keystore file already exists at: " + kekStoreFile.getAbsolutePath());

          String kekStoreProviderName = loader.getConfiguration().getString("kekStore.providerName");
          String kekStoreType = loader.getConfiguration().getString("kekStore.type");

          Provider kekStoreProvider = Security.getProvider(kekStoreProviderName);
          if (kekStoreProvider == null)
              throw new MasterKeyException("KEK Keystore provider not found: " + kekStoreProvider);
  
          try {
              kekStore = KeyStore.getInstance(kekStoreType, kekStoreProvider);
              kekStore.load(null, masterKey);
          }
          catch (Exception e) {
              throw new MasterKeyException("Failed create new KEK Keystore: " + e.getMessage(), e);
          }
      }
      
      // now we can save the 2 files since all exceptions cases except IO should have been tested by now
      storeEncryptedSecretMap();

      if (action == ACTION_INSTALL) {
          log.debug("Saving KEK Keystore file to: " + kekStoreFile.getAbsolutePath());
          try
          {
              FileOutputStream fos = new FileOutputStream(kekStoreFile);
              kekStore.store(fos, masterKey);
              fos.close();
              log.debug("Successfully saved KEK Keystore!");
          }
          catch (Exception e)
          {
              throw new MasterKeyException("Failed to save KEK Keystore file to: " + secretStoreFile.getAbsolutePath() + ": " + e.getMessage(), e);
          }
          
          loader.setState(KeyManagerLoader.STATE_INSTALLED);
      }
      else if (action == ACTION_CHANGE_MASTER_KEY)
          loader.getKEKStore().changeMasterKey(user, masterKey);
      
      long expirationMillis = loader.getConfiguration().getLong("kekStore.kekExpirationMillis", 7776000000L);
      try {
      	if(loader.getKEKStore()!=null)
      		loader.getKEKStore().genKEKIfExpired(user, masterKey, expirationMillis);
      }	catch(KEKException e) {
      	log.error("KEK (expired) generation failed : " + e.getMessage(), e);
      }
      
      Util.overwrite(masterKey);
      masterKey = null;

      installers.clear();
      installerArray = null;
      
      secrets.clear();
                  
      kekStore = null;
    } catch (Throwable e) {
      if (action == ACTION_INSTALL)
          loader.setState(KeyManagerLoader.STATE_INSTALL_FAILED);

      if (e instanceof MasterKeyException)
          throw (MasterKeyException) e;
      
      throw new MasterKeyException("Installation Failed: " + e.getMessage(), e);
    }
  }
    
  
  public synchronized void changeCertificate(KeyManagerUser user, String oldCertThumbprint, byte[] decryptedSecret) throws KeyManagerException {
    log.info("Key custodian certificate change triggered by: " + user.getUserInfo() + ", old certificate thumbprint: " + oldCertThumbprint);
    
    if (oldCertThumbprint == null || oldCertThumbprint.length() == 0)
        throw new MasterKeyException("Empty oldCertThumbprint!");
    
    if (decryptedSecret == null || decryptedSecret.length == 0)
        throw new MasterKeyException("Empty secret!");
            
    if (encryptedSecretMap.containsKey(user.getCertThumbprint()))
        throw new MasterKeyException("Your certificate is already part of the Secret Store!");
    
    if (!encryptedSecretMap.containsKey(oldCertThumbprint))
        throw new MasterKeyException("Your old certificate is not part of the Secret Store!");
    
    String userInfo = user.getUserInfo();

    try {
        log.debug("Initializing asymmetric cipher for " + userInfo);
        asymmetricCipher.init(Cipher.ENCRYPT_MODE, user.getCertificate().getPublicKey());
    }
    catch (Exception e) {
        throw new MasterKeyException("Failed to initialized asymmetric cipher for " + userInfo + ": " + e.getMessage(), e);
    }

    byte[] asymEncryptedBytes = null;
    
    try {
        asymEncryptedBytes = asymmetricCipher.doFinal(decryptedSecret);
        log.debug("Successfully asymmetric encrypted secret for " + userInfo);
    }
    catch (Exception e) {
        throw new MasterKeyException("Failed to asymmetric encrypt secret for " + userInfo + ": " + e.getMessage(), e);
    }

    Util.overwrite(decryptedSecret);
    X509Certificate cert = null;
    try {
        cert = user.getCertificate();
    }
    catch (CertificateException e) {
        throw new MasterKeyException("Failed to retrieve user certificate: " + e.getMessage(), e);
    }
    
    encryptedSecretMap.remove(oldCertThumbprint);
    encryptedSecretMap.put(user.getCertThumbprint(), new EncryptedSecret(user.getSubjectName(), cert, asymEncryptedBytes));
    storeEncryptedSecretMap();
  }
    
  
	public synchronized void loadEncryptedSecretMap() throws MasterKeyException {
      if (!secretStoreFile.exists())
          return;
      
      ObjectInputStream ois = null;
      try {
        ois = new ObjectInputStream(new FileInputStream(secretStoreFile));
        encryptedSecretMap = (Map<String, EncryptedSecret>) ois.readObject();
      }
      catch (Exception e) {
        throw new MasterKeyException("Failed to load SecretStore from: " + secretStoreFile.getAbsolutePath() + ": " + e.getMessage(), e);
      }
      finally {
        if (ois != null) {
          try {
            ois.close();
          }
          catch (Exception e){
          }
        }
      }
  }
    
	
	public synchronized void storeEncryptedSecretMap() throws KeyManagerException {
    log.debug("Saving SecretStore file to: " + secretStoreFile.getAbsolutePath());
		File newSecretStoreFile = new File(secretStoreFile.getParentFile(), secretStoreFile.getName() + ".NEW");
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(newSecretStoreFile));
			try {
				oos.writeObject(encryptedSecretMap);
			} finally {
				oos.close();
			}
			// Now swap files
			FileUtil.swapFiles(secretStoreFile, newSecretStoreFile);

			log.info("SecretStore saved to file " + secretStoreFile.getAbsolutePath());
		} catch(IOException e) {
			throw new KeyManagerException("Failed to save SecretStore file to: " + secretStoreFile.getAbsolutePath() + ": " + e.getMessage(), e);
    }
		log.debug("Successfully saved SecretStore!");

	}

	
	public long getLoaderBDKKeyID() {
		return loaderBDKKeyID;
	}
	
	
	public long getLoaderAVASKeyID() {
		return loaderAVASKeyID;
	}	
}
