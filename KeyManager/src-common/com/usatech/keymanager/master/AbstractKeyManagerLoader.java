package com.usatech.keymanager.master;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.Provider;
import java.security.Security;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.cayenne.access.DataContext;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.ssss.SSSS;

public abstract class AbstractKeyManagerLoader implements ServletContextListener, Runnable, KeyManagerLoader {
	
	private static final Log log = LogFactory.getLog(AbstractKeyManagerLoader.class);
	protected static final Object synchObject = new Object();

	protected PropertiesConfiguration config;
	protected ServletContext context;

	protected final ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	protected final ScheduledExecutorService maintenanceExecutor = Executors.newSingleThreadScheduledExecutor();

	protected SSSS ssss;

	protected Future<?> keyManagerLoaderFuture;
	protected final CountDownLatch masterKeyLoadedLatch = new CountDownLatch(1);

	protected final AtomicInteger state = new AtomicInteger();

	protected Provider provider;
	protected String asymmetricCipherID;
	protected Cipher asymmetricCipher;
	protected File secretStoreFile;

	protected KeyManagerInstaller installer;
	protected MasterKeyLoader masterKeyLoader;
	protected KEKStore kekStore;
	protected int bdkShareCount;
	protected File instancePropertyFile;
	protected Properties instanceProperties;
	protected int avasShareCount;

	public void contextInitialized(ServletContextEvent event) {
		synchronized(synchObject) {
			if(context != null) {
				log.debug("Already Initialized");
				return;
			}
			log.info("KeyManager Starting!");
			// store a reference to the servlet context
			context = event.getServletContext();
			keyManagerLoaderFuture = threadPool.submit(this);
			context.setAttribute(AbstractKeyManagerLoader.CONTEXT_KEY, this);
		}
	}

	public void waitUntilInitialized() throws InterruptedException, ExecutionException {
		keyManagerLoaderFuture.get();
	}

	public void waitUntilMasterKeyLoaded() throws InterruptedException {
		masterKeyLoadedLatch.await();
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#setState(int)
	 */
	public void setState(int _state) {
		state.set(_state);
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getState()
	 */
	public int getState() {
		return state.get();
	}

	public void run() {
		try {
			log.info("Cryptography and Key Management Service v" + Version.getVersion() + " (" + Version.getReleaseDate() + ") Copyright (c) 2007 USA Technologies.  All rights reserved.");

			config = initConfig();
			config.setThrowExceptionOnMissing(true);

			// load SSSS
			String ssssDir = config.getString("ssss.dir");
			int threshold = config.getInt("ssss.threshold");
			int shares = config.getInt("ssss.shares");
			int sharedSecretSize = config.getInt("ssss.keySize");

			if(threshold > shares) {
				setState(STATE_START_FAILED);
				log.fatal("Failed to load KeyManagerInstaller: Invalid parameter values: ssss.threshold > ssss.shares");
				return;
			}

			File ssssDirFile = getFileRealPath(ssssDir);
			ssss = new SSSS(ssssDirFile, threshold, shares, sharedSecretSize);

			Integer processTimeout = config.getInteger("ssss.processTimeout", null);
			if(processTimeout != null && processTimeout > 0)
				ssss.setProcessTimeout(processTimeout);

			// check if secret store is installed
			String secretStoreFileName = config.getString("secretStore.file");
			secretStoreFile = getFileRealPath(secretStoreFileName);

			// check if the KEKStore exists
			String kekStoreFileName = config.getString("kekStore.file");
			File kekStoreFile = getFileRealPath(kekStoreFileName);

			if(!secretStoreFile.exists() && kekStoreFile.exists()) {
				setState(STATE_START_FAILED);
				log.fatal("Inconsistent state detected: secretStore does not exist but kekStore does!");
				return;
			}

			if(secretStoreFile.exists() && !kekStoreFile.exists()) {
				setState(STATE_START_FAILED);
				log.fatal("Inconsistent state detected: secretStore exist but kekStore does not!");
				return;
			}

			String providerName = config.getString("secretStore.providerName");
			asymmetricCipherID = config.getString("secretStore.asymmetric.cipherID");

			provider = Security.getProvider(providerName);
			if(provider == null) {
				log.fatal("Provider not found: " + providerName);
				return;
			}

			try {
				asymmetricCipher = Cipher.getInstance(asymmetricCipherID, provider);
			} catch(Exception e) {
				log.fatal("Error loading secret key cipher " + asymmetricCipherID + ": " + e.getMessage(), e);
				return;
			}

			if(!secretStoreFile.exists()) {
				// if we're not installed then
				log.info("KeyManager is not installed. Secret Store not found at " + secretStoreFile.getAbsolutePath());
				setState(STATE_NOT_INSTALLED);
			} else {
				// else KeyManager is installed ...
				log.info("KeyManager is installed");
				setState(STATE_INSTALLED);
			}

			bdkShareCount = config.getInt("kekStore.DUKPT.BDK.shareCount");
			avasShareCount = config.getInt("kekStore.AVAS.shareCount");
			
			installer = new KeyManagerInstaller(this);
			try {
				masterKeyLoader = new MasterKeyLoader(this);
			} catch(MasterKeyException e) {
				setState(STATE_LOAD_FAILED);
				log.fatal("Failed to instantiate MasterKeyLoader: " + e.getMessage(), e);
				return;
			}

			long maintRunIntervalSeconds = config.getLong("maint.run.interval.seconds");
			long maintRunInitialDelaySeconds = config.getLong("maint.run.initial.delay.seconds");
			maintenanceExecutor.scheduleAtFixedRate(new MaintenanceThread(), maintRunInitialDelaySeconds, maintRunIntervalSeconds, TimeUnit.SECONDS);
		} catch(NoSuchElementException e) {
			setState(STATE_START_FAILED);
			log.fatal("Failed to load KeyManager configuation! Required parameter is missing: " + e.getMessage(), e);
		} catch(Throwable e) {
			setState(STATE_START_FAILED);
			log.fatal("Failed to load KeyManager configuation! Unexpected error: " + e.getMessage(), e);
		}
	}

	protected abstract PropertiesConfiguration initConfig() throws ConfigurationException;

	public void contextDestroyed(ServletContextEvent event) {
		if(maintenanceExecutor != null)
			maintenanceExecutor.shutdown();

		if(threadPool != null)
			threadPool.shutdown();
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getConfiguration()
	 */
	public PropertiesConfiguration getConfiguration() {
		return config;
	}

	public void saveCurrentKEKAlias(String alias) throws ConfigurationException {
		saveInstanceProperty("kekStore.currentKEKAlias", alias);
	}

	public void saveAccountKEKAlias(String alias) throws ConfigurationException {
		saveInstanceProperty("kekStore.accountKEKAlias", alias);
	}

	
	protected synchronized void saveInstanceProperty(String propertyName, String propertyValue) throws ConfigurationException {
		config.setProperty(propertyName, propertyValue);
		if(instanceProperties == null) {
			instancePropertyFile = getFileRealPath(getConfiguration().getString("instanceSettingsFile"));
			Properties p = new Properties();
			try {
				p.load(new FileReader(instancePropertyFile));
			} catch(FileNotFoundException e) {
				throw new ConfigurationException(e);
			} catch(IOException e) {
				throw new ConfigurationException(e);
			}
			instanceProperties = p;
		}
		String oldValue = instanceProperties.getProperty(propertyName);
		if(oldValue == null && propertyValue == null)
			return;
		if(oldValue != null && oldValue.equals(propertyValue))
			return;
		instanceProperties.setProperty(propertyName, propertyValue);
		try {
			instanceProperties.store(new FileWriter(instancePropertyFile), null);
		} catch(IOException e) {
			throw new ConfigurationException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getSSSS()
	 */
	public SSSS getSSSS() {
		return ssss;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getSecretStoreFile()
	 */
	public File getSecretStoreFile() {
		return secretStoreFile;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getAsymmetricCipherID()
	 */
	public String getAsymmetricCipherID() {
		return asymmetricCipherID;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getAsymmetricCipher()
	 */
	public Cipher getAsymmetricCipher() {
		return asymmetricCipher;
	}

	public ThreadPoolExecutor getThreadPool() {
		return threadPool;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getAVASShareCount()
	 */
	public int getAVASShareCount() {
		return avasShareCount;
	}	
	
	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getBDKShareCount()
	 */
	public int getBDKShareCount() {
		return bdkShareCount;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#masterKeyLoaded(com.usatech.keymanager.auth.KeyManagerUser, char[])
	 */
	public synchronized void masterKeyLoaded(KeyManagerUser user, char[] masterKey) {
		log.info("Master key of " + masterKey.length + " chars successfully loaded, triggered by: " + user.getUserInfo());

		try {
			if(kekStore == null) {
				log.info("Loading KEKStore...");
				kekStore = new KEKStore(this, masterKey);
				log.info("KEKStore has been loaded");
			}
		} catch(KEKException e) {
			setState(STATE_LOAD_FAILED);
			log.fatal("Failed to load KEKStore: " + e.getMessage(), e);
			return;
		}
		kekStore.setLoadingUser(user);

		setState(STATE_LOADED);

		if(handleLoaded()) {
			setState(STATE_INITIALIZED);
			masterKeyLoadedLatch.countDown();
		}
	}

	public synchronized void loadKekStore(InputStream kekStoreStream, char[] password) {
		log.info("Reloading KEKStore");

		try {
			if(getState() != STATE_LOADED) {
				waitUntilInitialized();
				log.info("Loading KEKStore...");
				kekStore = new KEKStore(this, kekStoreStream, password);
				DataContext dataContext = DataContext.createDataContext();
				DataContext.bindThreadDataContext(dataContext);
				List<KeyManagerUser> users = KeyManagerUser.getAllUsers();
				MasterKeyLoader masterKeyLoader=getMasterKeyLoader();
				Map<String, String> keyCustodians = masterKeyLoader.getKeyCustodiansInfo();
				if(keyCustodians.size()==0){
					setState(STATE_LOAD_FAILED);
					log.fatal("Failed to load KEKStore: no keyCustodian.");
					return;
				}
				KeyManagerUser loadingUser=null;
				for(KeyManagerUser user : users) {
					if (keyCustodians.containsKey(user.getUserInfo())){
						loadingUser=user;
						break;
					}
				}
				kekStore.setLoadingUser(loadingUser);
				log.info("KEKStore has been loaded");
			}
		}catch(MasterKeyException e){
			setState(STATE_LOAD_FAILED);
			log.fatal("Failed to load KEKStore: " + e.getMessage(), e);
			return;
		} catch(KEKException e) {
			setState(STATE_LOAD_FAILED);
			log.fatal("Failed to load KEKStore: " + e.getMessage(), e);
			return;
		} catch(InterruptedException e) {
			setState(STATE_LOAD_FAILED);
			log.fatal("Failed to load KEKStore: " + e.getMessage(), e);
			return;
		} catch(ExecutionException e) {
			setState(STATE_LOAD_FAILED);
			log.fatal("Failed to load KEKStore: " + e.getMessage(), e);
			return;
		}

		setState(STATE_LOADED);

		if(handleLoaded()) {
			setState(STATE_INITIALIZED);
			masterKeyLoadedLatch.countDown();
		}
	}

	protected abstract boolean handleLoaded();
	
	public void kekStoreUpdated(Map<String, SecretKey> kekMap) {
		//by default do nothing
	}


	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getKEKStore()
	 */
	public synchronized KEKStore getKEKStore() {
		return kekStore;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getKeyManagerInstaller()
	 */
	public synchronized KeyManagerInstaller getKeyManagerInstaller() {
		return installer;
	}

	/* (non-Javadoc)
	 * @see com.usatech.keymanager.master.KeyManagerLoader#getMasterKeyLoader()
	 */
	public synchronized MasterKeyLoader getMasterKeyLoader() throws MasterKeyException {
		return masterKeyLoader;
	}

	
	private class MaintenanceThread implements Runnable {
		private static final String MAINT_THREAD_NAME = "MaintenanceThread";
		private DataContext dataContext = null;

		public void run() {
			log.info(MAINT_THREAD_NAME + ": Running...");

			if(dataContext == null) {
				try {
					dataContext = DataContext.createDataContext();
					DataContext.bindThreadDataContext(dataContext);
				} catch(Exception e) {
					log.error(MAINT_THREAD_NAME + ": Error creating data context, exiting: " + e.getMessage(), e);
					return;
				}
			}

			log.info(MAINT_THREAD_NAME + ": Deleting encryption keys that have expired");
			try {
				int deleteCount = PersistentKey.deleteOldKeys();
				log.info(MAINT_THREAD_NAME + ": Deleted " + deleteCount + " old encryption keys");
			} catch(Exception e) {
				log.error(MAINT_THREAD_NAME + ": Error deleting old encryption keys: " + e.getMessage(), e);
			}

			log.info(MAINT_THREAD_NAME + ": Finished");
		}
	}
}
