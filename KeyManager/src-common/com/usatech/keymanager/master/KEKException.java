package com.usatech.keymanager.master;

public class KEKException extends Exception implements java.io.Serializable
{
	public KEKException()
	{
		super();
	}

	public KEKException(String message)
	{
		super(message);
	}

	public KEKException(String message, Throwable reason)
	{
		super(message, reason);
	}

	public KEKException(Throwable reason)
	{
		super(reason);
	}

}
