package com.usatech.keymanager.master;

import java.security.cert.X509Certificate;

public class EncryptedSecret implements java.io.Serializable
{
	private static final long serialVersionUID = -1574189658483669066L;
	protected String		     subjectDN;
	protected X509Certificate	 certificate;
	protected byte[]		     bytes;

	public EncryptedSecret(String subjectDN, X509Certificate certificate, byte[] bytes)
	{
		this.subjectDN = subjectDN;
		this.certificate = certificate;
		this.bytes = bytes;
	}

	public String getSubjectDN()
	{
		return subjectDN;
	}

	public void setSubjectDN(String subjectDN)
	{
		this.subjectDN = subjectDN;
	}

	public X509Certificate getCertificate()
	{
		return certificate;
	}

	public byte[] getBytes()
	{
		return bytes;
	}

	public void setBytes(byte[] bytes)
	{
		this.bytes = bytes;
	}
}
