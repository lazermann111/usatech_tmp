package com.usatech.keymanager.master;

public class MasterKeyException extends KeyManagerException implements java.io.Serializable
{
	public MasterKeyException()
	{
		super();
	}

	public MasterKeyException(String message)
	{
		super(message);
	}

	public MasterKeyException(String message, Throwable reason)
	{
		super(message, reason);
	}

	public MasterKeyException(Throwable reason)
	{
		super(reason);
	}

}
