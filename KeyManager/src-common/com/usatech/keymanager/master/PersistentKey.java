package com.usatech.keymanager.master;

import java.util.concurrent.atomic.AtomicInteger;

import javax.crypto.SecretKey;

import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;

import com.usatech.keymanager.KeyInfo;
import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.cayenne._PersistentKey;
import com.usatech.keymanager.util.DataUtil;

public class PersistentKey extends _PersistentKey {
	
	private SecretKey secretKey;
	protected final AtomicInteger encryptCount = new AtomicInteger();
	
	public int getEncryptCount() {
		return encryptCount.get();
	}

	public int incrementEncryptCount() {
		return encryptCount.incrementAndGet();
	}

	public SecretKey getSecretKey()
	{
		return secretKey;
	}
	
	public void setSecretKey(SecretKey secretKey)
	{
		this.secretKey = secretKey;
	}
	
	public int getKid()
	{
		return DataObjectUtils.intPKForObject(this);
	}

	public boolean equals(Object o)
	{
		if (o == null)
			return false;

		if (this == o)
			return true;

		if (!(o instanceof PersistentKey))
			return false;

		PersistentKey that = (PersistentKey) o;

		return (this.getKid() == that.getKid());
	}

	public void setCreatedByUser(KeyManagerUser createdByUser)
	{
		super.setCreatedByUser((KeyManagerUser) getDataContext().localObject(createdByUser.getObjectId(), createdByUser));
	}

	public void save()
	{
	    DataUtil.commitDataChanges(getDataContext());
	}

	public KeyInfo getInfo()
	{
		KeyInfo keyInfo = new KeyInfo();
		keyInfo.setKid(getKid());
		keyInfo.setCreatedBy(super.getCreatedByUser().getSubjectName());
		keyInfo.setCreatedDate(super.getCreatedDate());
		return keyInfo;
	}

	public String toString()
	{
		return "PersistentKey " + getKid();
	}

	public static PersistentKey newInstance()
	{
		DataContext context = DataContext.getThreadDataContext();
		return (PersistentKey) context.newObject(PersistentKey.class);
	}

	public static PersistentKey getByKID(int kid)
	{
		DataContext context = DataContext.getThreadDataContext();
		return (PersistentKey) DataObjectUtils.objectForPK(context, PersistentKey.class, kid);
	}

	public static int deleteOldKeys() {
    DataContext context = DataContext.getThreadDataContext();
    SQLTemplate update = new SQLTemplate(PersistentKey.class, "DELETE FROM KEY WHERE EXPIRATION_TS < NOW()");
    int[] updateCounts = context.performNonSelectingQuery(update);
    if (updateCounts[0] > 0)
        DataUtil.commitDataChanges(context);
    return updateCounts[0];
  }
}



