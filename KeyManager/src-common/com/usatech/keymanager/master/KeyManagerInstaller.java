package com.usatech.keymanager.master;

import java.util.*;

import org.apache.commons.logging.*;

import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.ssss.*;


public class KeyManagerInstaller {
	
	private static Log log = LogFactory.getLog(KeyManagerInstaller.class);

	private KeyManagerLoader loader;

	private int	installTimeoutSeconds;

	private SSSS ssss;

	private long installStartTS	= 0;
	
	private ArrayList<KeyManagerUser>	installers	= new ArrayList<KeyManagerUser>();

	
	KeyManagerInstaller(KeyManagerLoader loader) {
		this.loader = loader;
		this.ssss = loader.getSSSS();

		installTimeoutSeconds = loader.getConfiguration().getInt("keyManagerInstaller.installTimeoutSeconds");
	}

	private synchronized void clearIfExpired() {
		if (installStartTS > 0 && getInstallRemainingSeconds() <= 0){
			log.warn("Install timeout occured: clearing certificates!");
			installers.clear();
			installStartTS = 0;
		}
	}

	public synchronized void addInstaller(KeyManagerUser user) throws KeyManagerException {
		clearIfExpired();

		if (installers.size() > ssss.getN())
			throw new KeyManagerException(ssss.getN() + " users have already added their certificate!");

		if (installers.contains(user))
			throw new KeyManagerException("Your certificate has already been added!");

		if (installers.isEmpty())
			installStartTS = System.currentTimeMillis();

		installers.add(user);
		log.debug(installers.size() + " cert(s) are loaded after the addition from " + user.getUserInfo());
	}

	public synchronized int getInstallTimeoutSeconds() {
		return installTimeoutSeconds;
	}

	public synchronized int getInstallRemainingSeconds() {
		int remaining = (int) (getInstallTimeoutSeconds() - ((System.currentTimeMillis() - installStartTS) / 1000));
		if (remaining <= 0)
			return 0;
		return remaining;
	}

	public synchronized SortedMap<String, KeyManagerUser> getInstallersInfo()	{
		clearIfExpired();
        
    TreeMap<String, KeyManagerUser> treeMap = new TreeMap<String, KeyManagerUser>();
    for (KeyManagerUser user : installers)
        treeMap.put(user.getUserInfo(), user);

    return treeMap;
	}

	public synchronized boolean canInstall(KeyManagerUser user)	{
		clearIfExpired();

		if (installers.size() == ssss.getN() && installers.get(0).equals(user))
			return true;

		return false;
	}

	public synchronized void install(KeyManagerUser user) throws KeyManagerException {
		clearIfExpired();

		if (installers.size() != ssss.getN())
			throw new KeyManagerException("Exactly " + ssss.getN() + " users certificates are needed to install!");

		if (!installers.get(0).equals(user))
			throw new KeyManagerException("Only the first user in the install group may trigger the install!");
        
    loader.getMasterKeyLoader().generateMasterKey(MasterKeyLoader.ACTION_INSTALL, user, installers);
	}
}
