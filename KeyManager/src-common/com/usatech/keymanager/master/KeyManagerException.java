package com.usatech.keymanager.master;

public class KeyManagerException extends Exception implements java.io.Serializable
{
    public KeyManagerException()
    {
        super();
    }

    public KeyManagerException(String message)
    {
        super(message);
    }

    public KeyManagerException(String message, Throwable reason)
    {
        super(message, reason);
    }

    public KeyManagerException(Throwable reason)
    {
        super(reason);
    }

}
