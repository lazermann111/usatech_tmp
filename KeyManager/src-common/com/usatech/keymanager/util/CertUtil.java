package com.usatech.keymanager.util;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAKey;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

public class CertUtil {
	public static final String[] KEY_USAGE = { "Digital Signature", "Non-Repudiation", "Key Encipherment", "Data Encipherment", "Key Agreement", "Key Cert Sign", "CRL Sign", "Encipher Only", "Decipher Only" };

	public static String getCertThumbprint(X509Certificate cert) throws Exception {
		byte[] encoded = cert.getEncoded();
		if(encoded == null)
			throw new Exception("Failed to get encoded certificate");

		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		if(sha1 == null)
			throw new Exception("Failed to get instance of MessageDigest: SHA1");

		byte[] hashed = sha1.digest(encoded);
		if(hashed == null)
			throw new Exception("Failed to get hash of certificate using MessageDigest: SHA1");
		return HexBin.encode(hashed).toLowerCase();
		// return com.usatech.util.Conversions.bytesToHex(hashed).toLowerCase();
	}

	public static String getCertBase64(X509Certificate cert) throws Exception {
		byte[] encoded = cert.getEncoded();
		if(encoded == null)
			throw new Exception("Failed to get encoded certificate");
		return Base64.encode(encoded);
		// return com.usatech.util.Base64.encodeBytes(encoded);
	}

	public static X509Certificate getCertFromBase64(String certBase64) throws CertificateException {
		byte[] certBytes = Base64.decode(certBase64);
		// byte[] certBytes = com.usatech.util.Base64.decode(certBase64);
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		return (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(certBytes));
	}

	public static byte[] decodeBase64(String base64) {
		return Base64.decode(base64);
	}

	public static String encodeBase64(byte[] bytes) {
		return Base64.encode(bytes);
	}

	public static String getKeyUsageInfo(X509Certificate cert) {
		if(cert == null)
			return "";

		boolean firstKeyUsage = true;
		StringBuilder sb = new StringBuilder();
		boolean[] keyUsage = cert.getKeyUsage();

		if(keyUsage != null)
			for(int i = 0; i < keyUsage.length; i++) {
				if(keyUsage[i]) {
					if(firstKeyUsage)
						firstKeyUsage = false;
					else
						sb.append(", ");
					sb.append(KEY_USAGE[i]);
				}
			}

		return sb.toString();
	}

	public static String getPublicKeyInfo(X509Certificate cert) {
		if(cert == null)
			return "";

		StringBuilder sb = new StringBuilder();
		PublicKey publicKey = cert.getPublicKey();
		String algorithm = publicKey.getAlgorithm();
		sb.append(algorithm);

		if(algorithm.equalsIgnoreCase("RSA")) {
			sb.append(" (");
			sb.append(((RSAKey) publicKey).getModulus().bitLength());
			sb.append(" Bits)");
		}

		return sb.toString();
	}
}
