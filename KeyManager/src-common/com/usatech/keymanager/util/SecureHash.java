package com.usatech.keymanager.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SecureHash {
    private static final String HASH_ALGORITHM = "SHA-256";

    public static byte[] getUnsaltedHash(byte[] data) {
        if (data == null)
            return null;
        return getUnsaltedHash(data, HASH_ALGORITHM);
    }

    public static byte[] getUnsaltedHash(byte[] data, String algorithm) {
        if (data == null)
            return null;
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        digest.reset();
        return digest.digest(data);
    }
}
