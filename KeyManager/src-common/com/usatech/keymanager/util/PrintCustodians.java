package com.usatech.keymanager.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Map;

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import com.usatech.keymanager.master.EncryptedSecret;

public class PrintCustodians {
	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchAlgorithmException, CertificateException, KeyStoreException, NoSuchProviderException {
		String file;
		if(args.length < 1 || args[0] == null || (file = args[0].trim()).isEmpty()) {
			System.err.println("Requires file path as argument");
			System.exit(2);
			return;
		}
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
		try {
			MessageDigest sha1 = MessageDigest.getInstance("SHA1");	
			File truststoreFile = new File(System.getProperty("javax.net.ssl.trustStore", System.getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts"));
			String truststoreProvider = System.getProperty("javax.net.ssl.trustStoreProvider");
			String truststoreType = System.getProperty("javax.net.ssl.trustStoreType", "jks");
			String truststorePassword = System.getProperty("javax.net.ssl.trustStorePassword");
			KeyStore truststore;
			if(truststoreFile.exists()) {
				if(truststoreProvider == null || truststoreProvider.trim().isEmpty())
					truststore = KeyStore.getInstance(truststoreType);
				else
					truststore = KeyStore.getInstance(truststoreType, truststoreProvider);
				InputStream stream = new FileInputStream(truststoreFile);
				try {
					truststore.load(stream, truststorePassword != null ? truststorePassword.toCharArray() : null);
				} finally {
					stream.close();
				}
			} else
				truststore = null;
			Map<String, EncryptedSecret> map = (Map<String, EncryptedSecret>) ois.readObject();
			for(EncryptedSecret es : map.values()) {
				X509Certificate cert = es.getCertificate();
				OUTER: while(true) {
					System.out.print(cert.getSubjectDN());
					System.out.print("; ");
					byte[] hashed = sha1.digest(cert.getEncoded());
					System.out.print(HexBin.encode(hashed).toLowerCase());
					System.out.print("; ");
					System.out.print(cert.getNotAfter());
					if(cert.getIssuerDN() != null && !cert.getIssuerDN().equals(cert.getSubjectDN())) {
						System.out.print(" --> ");
						for(Enumeration<String> aliases = truststore.aliases(); aliases.hasMoreElements();) {
							String alias = aliases.nextElement();
							X509Certificate signer = (X509Certificate) truststore.getCertificate(alias);
							if(signer == null)
								continue;
							if(signer.getSubjectDN() != null && signer.getSubjectDN().equals(cert.getIssuerDN())) {
								try {
									cert.verify(signer.getPublicKey());
								} catch(GeneralSecurityException e) {
									continue;
								}
								// found it!
								cert = signer;
								continue OUTER;
							}
						}
						System.out.print(cert.getIssuerDN());
						System.out.print("; <Not Found In Truststore>; <Not Found In Truststore>");
					}
					break;
				}
				System.out.println();
			}
		} finally {
			ois.close();
		}
	}
}
