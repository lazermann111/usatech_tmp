package com.usatech.keymanager.util;

import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;

import com.usatech.keymanager.auth.KeyManagerUser;

public class DataUtil
{
    public static void commitDataChanges(DataContext dataContext)
    {
        dataContext.commitChanges();
        dataContext.performNonSelectingQuery(new SQLTemplate(KeyManagerUser.class, "CHECKPOINT;"));        
    }
}
