package com.usatech.keymanager.util;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;

import javax.crypto.SecretKey;
import javax.security.auth.Subject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.math.ec.ECMultiplier;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.WNafUtil;

import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.master.KEKInfo;
import com.usatech.util.Conversions;

public class KeyManagerUtil {
	public static final String EC_PRIME256_CURVE_NAME = "secp256r1";
  public static final String ECDH = "ECDH";
  public static final String EC_ALGORITHM = "EC";
  public static final int ECDH_KEY_LEN_BYTES = 32;
	
  public static BigInteger generateAVASKeyComponent() {
  	SecureRandom random = new SecureRandom();   	
  	X9ECParameters x9EcParams = SECNamedCurves.getByName(EC_PRIME256_CURVE_NAME);
    ECDomainParameters params = new ECDomainParameters(x9EcParams.getCurve(), x9EcParams.getG(), x9EcParams.getN(), x9EcParams.getH(), x9EcParams.getSeed());
  	
    //From Bouncy Castle ECKeyPairGenerator
  	BigInteger n = params.getN();
  	int nBitLength = n.bitLength();
    int minWeight = nBitLength >>> 2;
  	  	
		BigInteger d;
    for (;;) {
        d = new BigInteger(nBitLength, random);	
        if (d.compareTo(BigInteger.valueOf(2)) < 0  || (d.compareTo(n) >= 0))
            continue;
        if (WNafUtil.getNafWeight(d) < minWeight)
            continue;
        break;
    }
    return d;
  }
  
  public static void generateAVASKeyPair(BigInteger privateKeyNumber, KEKInfo kekInfo) {
  	ProviderConfiguration configuration = BouncyCastleProvider.CONFIGURATION;
  	X9ECParameters x9EcParams = SECNamedCurves.getByName(EC_PRIME256_CURVE_NAME);
    ECDomainParameters params = new ECDomainParameters(x9EcParams.getCurve(), x9EcParams.getG(), x9EcParams.getN(), x9EcParams.getH(), x9EcParams.getSeed());  	
  	
  	ECMultiplier multiplier = new FixedPointCombMultiplier();  	
  	ECPoint Q = multiplier.multiply(params.getG(), privateKeyNumber);
  	AsymmetricCipherKeyPair pair = new AsymmetricCipherKeyPair(new ECPublicKeyParameters(Q, params), new ECPrivateKeyParameters(privateKeyNumber, params));
  	ECPublicKeyParameters pub = (ECPublicKeyParameters)pair.getPublic();
    
    ECParameterSpec ecParamSpec = ECNamedCurveTable.getParameterSpec(EC_PRIME256_CURVE_NAME);
    BCECPublicKey pubKey = new BCECPublicKey(EC_ALGORITHM, pub, ecParamSpec, configuration);
    //Apple expects the public key to be compressed, with header in ASN.1 encoding
    pubKey.setPointFormat("COMPRESSED");
    kekInfo.setPublicKeyHex(Conversions.bytesToHex(pubKey.getEncoded()));
    
    int publicKeyCrc = generateCRC(pubKey.getEncoded());
    kekInfo.setPublicKeyCrc(publicKeyCrc);
    
    byte[] compressedPubKey = Q.getEncoded(true);
    byte[] x = new byte[ECDH_KEY_LEN_BYTES];
    System.arraycopy(compressedPubKey, 1, x, 0, ECDH_KEY_LEN_BYTES);
    byte[] xCoordinateSha256 = SecureHash.getUnsaltedHash(x);
    //Apple Wallet sends the first 4 bytes of the SHA-256 hash of the X-Coordinate of the public key for key identification
    kekInfo.setPublicKeyID(Conversions.bytesToHex(xCoordinateSha256).substring(0, 8));
  }  
    
  public static PrivateKey loadAVASPrivateKey(SecretKey secretKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
		ECParameterSpec params = ECNamedCurveTable.getParameterSpec(EC_PRIME256_CURVE_NAME);
		BigInteger privateKeyNumber = new BigInteger(secretKey.getEncoded());
		ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(privateKeyNumber, params);
		KeyFactory kf = KeyFactory.getInstance(ECDH, BouncyCastleProvider.PROVIDER_NAME);
		PrivateKey privateKey = kf.generatePrivate(privateKeySpec);
		return privateKey;
  }
  
  public static int generateCRC(byte[] bytes) {
  	CRC16 crc = new CRC16();
  	for(byte b : bytes)
  		crc.update(b);
  	return crc.value;
  }
  
  public static KeyManagerUser getCurrentKeyManagerUser(HttpServletRequest request) throws Exception {
	  X509Certificate[] certs = (X509Certificate[])request.getAttribute("javax.servlet.request.X509Certificate");
	  if(certs == null || certs.length == 0)
	  	throw new ServletException("Client certificate not detected! Ensure that eToken Software is installed. Check that you plugged in your USB eToken.");
	  X509Certificate currentCert = certs[0];
	  String currentUserInfo = request.getRemoteUser();
	  if(currentUserInfo == null) {
	  	StringBuilder sb = new StringBuilder();
	  	sb.append("Could not find valid certificate from [");
	  	for(int i = 0; i < certs.length; i++) {
	  		if(i > 0)
	  	        sb.append(", ");
	  		sb.append(CertUtil.getCertThumbprint(certs[i]));
	  	}
	  	sb.append(']');
	  	throw new ServletException(sb.toString());
	  }
	  KeyManagerUser currentUser;    
	  String currentCertThumbprint;
	  Subject subject = (Subject)request.getAttribute("javax.servlet.request.Subject");
	  Iterator<KeyManagerUser> iter;
	  if(subject != null && (iter=subject.getPrincipals(KeyManagerUser.class).iterator()).hasNext()) {
	  	currentUser = iter.next();
	  	currentCertThumbprint = currentUser.getCertThumbprint();
	  	currentUserInfo = currentUser.getUserInfo();
	  } else {
	  	String[] currentUserInfoArray = currentUserInfo.split("; ");
	  	if (currentUserInfoArray.length < 3)
	  		throw new ServletException("Unable to extract certThumbprint from provided username: " + currentUserInfo);
	  	currentCertThumbprint = currentUserInfoArray[1];
	  	currentUser = KeyManagerUser.getByCertThumbprint(currentCertThumbprint);		
	  	currentUser.setCertificate(currentCert);
	  }
	  return currentUser;
  }
}
