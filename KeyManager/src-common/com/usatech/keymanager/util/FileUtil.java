package com.usatech.keymanager.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
	public static void swapFiles(File origFile, File newFile) throws IOException {
		if(origFile.exists()) {
			String oldBaseName = origFile.getName() + "." + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
			File oldFile = new File(origFile.getParentFile(), oldBaseName);
			char extra = 'a';
			while(!oldFile.createNewFile()) {
				if(extra > 'z')
					throw new IOException("Could not find unused old file '" + origFile.getParentFile().getAbsolutePath() + '/' + oldBaseName + "'");
				oldFile = new File(origFile.getParentFile(), oldBaseName + extra++);
			}
			moveFile(origFile, oldFile);
		}
		moveFile(newFile, origFile);
	}

	protected static long moveFile(File origFile, File newFile) throws IOException {
		if(origFile.renameTo(newFile))
			return newFile.length();
		// try via copying contents
		FileInputStream in = new FileInputStream(origFile);
		try {
			FileOutputStream out = new FileOutputStream(newFile);
			try {
				byte[] b = new byte[1024];
				long tot = 0;
				for(int len = 0; (len = in.read(b)) >= 0; tot += len)
					out.write(b, 0, len);
				out.flush();
				return tot;
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
	}
}
