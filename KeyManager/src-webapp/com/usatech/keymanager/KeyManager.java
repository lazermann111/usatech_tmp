package com.usatech.keymanager;

public interface KeyManager extends java.rmi.Remote {
	
	public KeyInfo getKey(int kid) throws CryptoException;
	
	public KeyInfo generateKey() throws CryptoException;
	
	public byte[] encrypt(int kid, byte[] bytes) throws CryptoException;
	
	public byte[] decrypt(int kid, byte[] bytes) throws CryptoException;
	
	public byte[] getRandomBytes(int numBytes) throws CryptoException;

}
