package com.usatech.keymanager.util;

import java.security.Principal;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.axis.MessageContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.caucho.services.server.ServiceContext;

public class WebServiceHelper
{
	private static Log	log	= LogFactory.getLog(WebServiceHelper.class);

	public static String getServiceType()
	{
		return null;
	}

	public static Principal getRemoteUserPrincipal()
	{
		Principal principal = null;

		MessageContext axisContext = MessageContext.getCurrentContext();
		if (axisContext != null)
		{
			principal = ((HttpServletRequest) axisContext.getProperty("transport.http.servletRequest")).getUserPrincipal();
		}
		else
		{
			ServletRequest request = ServiceContext.getContextRequest();
			if (request != null)
			{
				principal = ((HttpServletRequest) request).getUserPrincipal();
			}
		}

		log.debug("Principal: " + principal);

		return principal;
	}
	
	public static String getRemoteAddress()
	{
		String address = null;

		MessageContext axisContext = MessageContext.getCurrentContext();
		if (axisContext != null)
		{
			address = ((HttpServletRequest) axisContext.getProperty("transport.http.servletRequest")).getRemoteAddr();
		}
		else
		{
			ServletRequest request = ServiceContext.getContextRequest();
			if (request != null)
			{
				address = ((HttpServletRequest) request).getRemoteAddr();
			}
		}

		return address;
	}
}
