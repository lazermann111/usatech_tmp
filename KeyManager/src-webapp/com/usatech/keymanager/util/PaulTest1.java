package com.usatech.keymanager.util;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.util.*;

public class PaulTest1
{
	public static void main(String args[]) throws Exception
	{
		byte[] bytes = "1234567890ABCDEF".getBytes();
		
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		byte[] ivBytes = new byte[16];
		
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(128);
		SecretKey secretKey = keyGen.generateKey();
		
		//AlgorithmParameters params = AlgorithmParameters.getInstance("AES");
		random.nextBytes(ivBytes);
		IvParameterSpec encryptIV = new IvParameterSpec(ivBytes);
		
		Cipher encrypt = Cipher.getInstance("AES/CBC/PKCS5PADDING", "SunJCE");
		encrypt.init(Cipher.ENCRYPT_MODE, secretKey, encryptIV);
		byte[] encrypted = encrypt.doFinal(bytes);
		
		//random.nextBytes(ivBytes);
		//IvParameterSpec decryptIV = new IvParameterSpec(ivBytes);

		Cipher decrypt = Cipher.getInstance("AES/CBC/PKCS5PADDING", "SunJCE");
		decrypt.init(Cipher.DECRYPT_MODE, secretKey, encryptIV);
		byte[] decrypted = decrypt.doFinal(encrypted);
		
		System.out.println(Arrays.equals(bytes, decrypted));
	}
}
