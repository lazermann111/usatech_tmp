package com.usatech.keymanager.util;

import javax.servlet.*;

public class ServletUtil
{
	public static String getStringParam(ServletConfig config, String name) throws ServletException
	{
		String s = config.getInitParameter(name);
		if (s == null)
			throw new ServletException("Required init parameter not found: " + name);
		return s;
	}

	public static int getIntParam(ServletConfig config, String name) throws ServletException
	{
		try
		{
			return Integer.parseInt(getStringParam(config, name));
		}
		catch (NumberFormatException e)
		{
			throw new ServletException("Invalid init parameter: " + name);
		}
	}

}
