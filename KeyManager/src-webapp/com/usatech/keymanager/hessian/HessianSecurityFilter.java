package com.usatech.keymanager.hessian;

import java.io.IOException;

import javax.security.auth.login.LoginException;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.logging.*;

import com.usatech.keymanager.auth.WebAppMethodSecurity;

public class HessianSecurityFilter implements Filter
{
	private static Log		log	= LogFactory.getLog(HessianSecurityFilter.class);

	protected FilterConfig	config;

	public void init(FilterConfig config)
	{
		this.config = config;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		if (((HttpServletRequest) request).getMethod().equals("POST"))
		{
			BufferedHessianRequestWrapper requestWrapper = new BufferedHessianRequestWrapper((HttpServletRequest) request);
			BufferedHessianRequestStream hessianRequestStream = (BufferedHessianRequestStream) requestWrapper.getInputStream();

			((HttpServletRequest) request).getSession().setAttribute(WebAppMethodSecurity.WEBAPP_METHOD_NAME, hessianRequestStream.getMethodName());

			try
			{
				WebAppMethodSecurity.authorize(requestWrapper);
			}
			catch (LoginException e)
			{
				((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN);
				return;
			}

			chain.doFilter(requestWrapper, response);
		}
		else
		{
			log.debug("Not processing " + ((HttpServletRequest) request).getMethod() + " request");
			chain.doFilter(request, response);
		}
	}

	public void destroy()
	{
		config = null;
	}
}
