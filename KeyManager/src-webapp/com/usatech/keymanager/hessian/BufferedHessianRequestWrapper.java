package com.usatech.keymanager.hessian;

import java.io.*;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class BufferedHessianRequestWrapper extends HttpServletRequestWrapper
{
	protected ServletRequest origRequest;
	protected BufferedHessianRequestStream	bufferedRequestStream;

	public BufferedHessianRequestWrapper(HttpServletRequest req)
	{
		super(req);
		this.origRequest = req;
	}

	public BufferedReader getReader() throws IOException
	{
		throw new IOException("Unsupported: getReader()");
	}

	public ServletInputStream getInputStream() throws IOException
	{
		if (bufferedRequestStream == null)
			bufferedRequestStream = new BufferedHessianRequestStream(origRequest);
		return bufferedRequestStream;
	}
}
