package com.usatech.keymanager.hessian;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;

import com.caucho.hessian.io.HessianInput;

public class BufferedHessianRequestStream extends ServletInputStream
{
	protected boolean				closed	= false;
	protected int					length	= -1;
	protected ServletRequest		request;
	protected byte[]				bufferedRequest;

	protected ByteArrayInputStream	bais;

	protected int					callVersion;
	protected Map<String, Object>	headers	= new HashMap<String, Object>();
	protected String				methodName;
	protected String				attrName;

	public BufferedHessianRequestStream(ServletRequest request) throws IOException
	{
		super();

		closed = false;
		this.request = request;

		ServletInputStream sis = request.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte[] buffer = new byte[1024];
		while (true)
		{
			int bytesRead = sis.read(buffer);
			if (bytesRead == -1)
				break;
			baos.write(buffer, 0, bytesRead);
		}
		baos.flush();

		bufferedRequest = baos.toByteArray();
		baos.close();

		bais = new ByteArrayInputStream(bufferedRequest);
		HessianInput his = new HessianInput(bais);
		callVersion = his.readCall();

		String header;
		while ((header = his.readHeader()) != null)
		{
			Object value = his.readObject();
			headers.put(header, value);
		}

		methodName = his.readMethod();

		bais.close();
		bais = new ByteArrayInputStream(bufferedRequest);

		if (methodName.indexOf('_') != -1)
			methodName = methodName.substring(0, methodName.indexOf('_'));
	}

	public int getCallVersion()
	{
		return callVersion;
	}

	public String getMethodName()
	{
		return methodName;
	}

	public Map<String, Object> getHeaders()
	{
		return headers;
	}

	public String getAttrName()
	{
		return attrName;
	}

	public void close() throws IOException
	{
		if (closed)
			throw new IOException("This input stream has already been closed");

		bais.close();
		closed = true;
	}

	public int read() throws IOException
	{
		return bais.read();
	}

	public int read(byte b[]) throws IOException
	{
		return bais.read(b);
	}

	public int read(byte b[], int off, int len)
	{
		return bais.read(b, off, len);
	}

	public long skip(long n)
	{
		return bais.skip(n);
	}

	public synchronized int available()
	{
		return bais.available();
	}

	public boolean markSupported()
	{
		return bais.markSupported();
	}

	public void mark(int readAheadLimit)
	{
		bais.mark(readAheadLimit);
	}

	public void reset()
	{
		bais.reset();
	}

	@Override
	public boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReady() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setReadListener(ReadListener readListener) {
		// TODO Auto-generated method stub
		
	}
}
