package com.usatech.keymanager;

import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.keymanager.auth.KeyManagerUser;
import com.usatech.keymanager.master.KEKStore;
import com.usatech.keymanager.master.KeyManagerLoader;
import com.usatech.keymanager.master.PersistentKey;
import com.usatech.keymanager.util.WebServiceHelper;
import com.usatech.util.Base64;
import com.usatech.util.Conversions;


public class KeyManagerService implements KeyManager {
	
	private static Log	log	= LogFactory.getLog(KeyManagerService.class);

	private static Object	synchObject	= new Object();

	private static KeyManagerLoader	loader;

	private static SecureRandom	random;
	
	private static Provider	currentProvider;
	private static KeyGenerator currentKeyGenerator;
	private static String	currentTransformation;
	private static int currentKeySize;
	private static int currentIVSize;
	
	private static String dukptAlgorithm;
	private static String dukptEncoding;
	private static String dukptPadding;
	private static String dukptTransformation;

	private static KEKStore kekStore;
    
  private static final byte PADDING = 0x00;
    
  private static byte[] NULL8 = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  private static IvParameterSpec  nullIvParamSpec = new IvParameterSpec(NULL8);
  
  
	public static void init(KeyManagerLoader _loader, KEKStore _kekStore) throws CryptoException, NoSuchElementException {

		log.debug("init");

		synchronized (synchObject){

			kekStore = _kekStore;
            
			if (loader != null)	{
				log.debug("Already initialized!");
				return;
			}

			loader = _loader;

			String randomName = loader.getConfiguration().getString("keyManagerService.randomName");
			try	{
				random = SecureRandom.getInstance(randomName);
			}
			catch (Exception e) {
				throw new CryptoException("Failed to get instance of SecureRandom " + randomName + ": " + e.getMessage(), e);
			}
			log.debug("Loaded SecureRandom: " + random.getAlgorithm() + " (" + random.getClass().getName() + ")");
			
			String algorithmName = loader.getConfiguration().getString("keyManagerService.algorithm");
			String encodingName = loader.getConfiguration().getString("keyManagerService.encoding");
			String paddingName = loader.getConfiguration().getString("keyManagerService.padding");
			
			currentKeySize = loader.getConfiguration().getInt("keyManagerService.keySize");
			currentIVSize = loader.getConfiguration().getInt("keyManagerService.ivSizeBytes");
			currentTransformation = algorithmName + "/" + encodingName + "/" + paddingName;
			
			dukptAlgorithm = loader.getConfiguration().getString("keyManagerService.DUKPT.algorithm");
			dukptEncoding = loader.getConfiguration().getString("keyManagerService.DUKPT.encoding");
			dukptPadding = loader.getConfiguration().getString("keyManagerService.DUKPT.padding");			
			dukptTransformation = dukptAlgorithm + "/" + dukptEncoding + "/" + dukptPadding;

			String providerName = loader.getConfiguration().getString("keyManagerService.providerName");
			
			currentProvider = Security.getProvider(providerName);
			if (currentProvider == null)
				throw new CryptoException("Provider not found: " + providerName);

			log.debug("Loaded security Provider: " + currentProvider.getName() + " (" + currentProvider.getClass().getName() + ")");

			try	{
				currentKeyGenerator = KeyGenerator.getInstance(algorithmName, currentProvider);
			}
			catch (Exception e)	{
				throw new CryptoException("Failed to get instance of KeyGenerator " + algorithmName + ": " + e.getMessage(), e);
			}
			
			try	{
				currentKeyGenerator.init(currentKeySize);
			}
			catch (Exception e)	{
				throw new CryptoException("Failed to get initialize KeyGenerator size " + currentKeySize + ": " + e.getMessage(), e);
			}

			log.debug("Loaded KeyGenerator: " + currentKeyGenerator.getAlgorithm() + " (" + currentKeyGenerator.getClass().getName() + ")");
		}
	}

	// a non-static method calling a static method is done because the webservice may create a new instance of this
	// class at anytime, but we want to operate as a singleton

	public byte[] getRandomBytes(int numBytes) throws CryptoException	{
		return _getRandomBytes(numBytes);
	}

	
	private static byte[] _getRandomBytes(int numBytes) throws CryptoException {
		
		KeyManagerUser user = (KeyManagerUser) WebServiceHelper.getRemoteUserPrincipal();
		log.debug("SERVICE_START: getRandomBytes("+numBytes+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());

		byte[] bytes = new byte[numBytes];

		try	{
			synchronized (random)	{
				random.nextBytes(bytes);
			}
			
			log.info("SERVICE_SUCCESS: getRandomBytes("+numBytes+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());
		}
		catch (Exception e)	{
			log.error("SERVICE_FAIL: getRandomBytes("+numBytes+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage());
			log.debug("getRandomBytes failed: " + e.getMessage(), e);
			throw new CryptoException("getRandomBytes failed due to an internal error!");
		}

		return bytes;
	}

	
	public KeyInfo getKey(int kid) throws CryptoException	{
		return _getKey(kid);
	}

	
	private static KeyInfo _getKey(int kid) throws CryptoException {
		
		KeyManagerUser user = (KeyManagerUser) WebServiceHelper.getRemoteUserPrincipal();
		log.debug("SERVICE_START: getKey("+kid+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());
		
		PersistentKey persistentKey = null;
		
		try	{
			persistentKey = PersistentKey.getByKID(kid);
		}
		catch (Exception e)	{
			log.error("SERVICE_FAIL: getKey("+kid+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage());
			log.debug("getKey failed: " + e.getMessage(), e);
			throw new CryptoException("getKey failed due to an internal error!");
		}
		
		if (persistentKey == null) {
			log.warn("SERVICE_FAIL: getKey("+kid+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + "KID " + kid + " not found");
			log.debug("KID " + kid + " not found");
			throw new CryptoException("KID " + kid + " not found");
		}

		log.info("SERVICE_SUCCESS: getKey("+kid+") - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());
		
		return persistentKey.getInfo();
	}

	
	public KeyInfo generateKey() throws CryptoException	{
		return _generateKey();
	}

	
	private static KeyInfo _generateKey() throws CryptoException {
		
		synchronized (synchObject) {
			KeyManagerUser user = (KeyManagerUser) WebServiceHelper.getRemoteUserPrincipal();
			log.debug("SERVICE_START: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());

			if (kekStore == null)
			{
				log.warn("SERVICE_FAIL: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + "KeyStore is not loaded");
				throw new CryptoException("KeyStore is not loaded");
			}

			SecretKey secretKey = null;

			try	{
				secretKey = currentKeyGenerator.generateKey();
			}
			catch (Exception e)	{
				log.error("SERVICE_FAIL: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage());
				log.debug("generateKey failed: " + e.getMessage(), e);
				throw new CryptoException("generateKey failed due to an internal error!");
			}

			if (secretKey == null) {
				log.error("SERVICE_FAIL: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + "KeyGenerator returned null key");
				log.debug("generateKey failed: KeyGenerator returned null secretKey");
				throw new CryptoException("generateKey failed due to an internal error!");
			}
			
			byte[] encryptedKey = null;
			
			try	{
				encryptedKey = kekStore.wrap(secretKey.getEncoded());
			}
			catch(CryptoException e) {
				log.error("SERVICE_FAIL: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage());
				log.debug("generateKey failed: " + e.getMessage(), e);
				throw new CryptoException("generateKey failed due to an internal error!");
			}

			String encryptedKeyBase64 = Base64.encodeBytes(encryptedKey);

			PersistentKey persistentKey = null;
			long currentTime = System.currentTimeMillis();
			try	{
				persistentKey = PersistentKey.newInstance();
				persistentKey.setTransformation(currentTransformation);
				persistentKey.setProviderName(currentProvider.getName());
				persistentKey.setKeySizeBits(currentKeySize);
				persistentKey.setIvSizeBytes(currentIVSize);
				persistentKey.setCipherTextBase64(encryptedKeyBase64);
				persistentKey.setKekAlias(kekStore.getCurrentKEKAlias());
				persistentKey.setCreatedDate(new java.util.Date(currentTime));
				persistentKey.setCreatedByUser(user);
				persistentKey.setExpirationDate(new java.util.Date(currentTime + TimeUnit.DAYS.toMillis(90)));
				persistentKey.save();
			}
			catch(Exception e) {
				log.error("SERVICE_FAIL: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage() + " : " + e.getMessage());
				log.debug("generateKey failed: " + e.getMessage(), e);
				throw new CryptoException("generateKey failed due to an internal error!");
			}
			
			log.info("SERVICE_SUCCESS: generateKey() - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());
			return persistentKey.getInfo();
		}
	}

	
	public byte[] encrypt(int kid, byte[] bytes) throws CryptoException	{
		return _encrypt(kid, bytes);
	}

	
	private static byte[] _encrypt(int kid, byte[] bytes) throws CryptoException {
		long st = System.currentTimeMillis();
		
		synchronized (synchObject) {
			KeyManagerUser user = (KeyManagerUser) WebServiceHelper.getRemoteUserPrincipal();
			log.debug("SERVICE_START: encrypt("+kid+","+bytes.length+" bytes) - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());

			try	{
				
				PersistentKey key = retrieveKey(kid);
				
				Provider provider = Security.getProvider(key.getProviderName());
				
				if (provider == null)
					throw new CryptoException("Provider not found: " + key.getProviderName());
                
        int blockSize = key.getIvSizeBytes();
        boolean useCBC = bytes.length > blockSize;
        String transformation = key.getTransformation();
        if (!useCBC)
          transformation = transformation.replace("/CBC/", "/ECB/");
				
				Cipher cipher = Cipher.getInstance(transformation, provider);
				if (cipher == null)
					throw new CryptoException("Cipher not found: " + key.getTransformation());
				
				cipher.init(Cipher.ENCRYPT_MODE, key.getSecretKey());
				
				byte[] iv = null;
        if (useCBC) {
          iv = cipher.getIV();
    		  if(iv.length != blockSize)
    				throw new CryptoException("Initialization vector size " + iv.length + " != " + blockSize);
        }
                    
				byte[] encrypted = null;
        int remainder = bytes.length % blockSize;
                
        if (remainder > 0) {
            byte[] paddedBytes = new byte[bytes.length + blockSize - remainder];
            System.arraycopy(bytes, 0, paddedBytes, 0, bytes.length);
            for (int i = bytes.length; i < paddedBytes.length; i++)
                paddedBytes[i] = PADDING;
            encrypted = cipher.doFinal(paddedBytes);
        }
        else
            encrypted = cipher.doFinal(bytes);

        byte[] combined = null;
        if (useCBC) {
        	combined = new byte[encrypted.length + iv.length];
        	System.arraycopy(encrypted, 0, combined, 0, encrypted.length);
          System.arraycopy(iv, 0, combined, encrypted.length, iv.length);
        }
				
				log.info("SERVICE_SUCCESS: encrypt(" + kid + ", " + bytes.length + " bytes) - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());
				
				log.debug("encrypt: " + (System.currentTimeMillis() - st) + " ms");
				return useCBC ? combined : encrypted;
			}
			catch (Exception e)	{
				log.error("SERVICE_FAIL: encrypt("+kid+","+bytes.length+" bytes) - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage());
				log.debug("encrypt failed: " + e.getMessage(), e);
				throw new CryptoException("encrypt failed due to an internal error!");
			}
		}
	}

	
	public byte[] decrypt(int kid, byte[] bytes) throws CryptoException	{
		return _decrypt(kid, bytes);
	}

	
	private static byte[] _decrypt(int kid, byte[] bytes) throws CryptoException {
		
		synchronized (synchObject) {
			
			KeyManagerUser user = (KeyManagerUser) WebServiceHelper.getRemoteUserPrincipal();
			log.debug("SERVICE_START: decrypt("+kid+","+bytes.length+" bytes) - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());

			try {
				
				PersistentKey key = retrieveKey(kid);
				
				Provider provider = Security.getProvider(key.getProviderName());
				if (provider == null)
					throw new CryptoException("Provider not found: " + key.getProviderName());
                
        int blockSize = key.getIvSizeBytes();
        boolean useCBC = bytes.length > blockSize;
        String transformation = key.getTransformation();
        if (!useCBC)
            transformation = transformation.replace("/CBC/", "/ECB/");
				
				Cipher cipher = Cipher.getInstance(transformation, provider);
				
				if (cipher == null)
					throw new CryptoException("Cipher not found: " + transformation);
				
          byte[] decrypted = null;
          if (useCBC) {
            byte[] encrypted = new byte[bytes.length - blockSize];
            System.arraycopy(bytes, 0, encrypted, 0, encrypted.length);                    
              
            byte[] ivBytes = new byte[blockSize];
            System.arraycopy(bytes, encrypted.length, ivBytes, 0, blockSize);				
            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            cipher.init(Cipher.DECRYPT_MODE, key.getSecretKey(), iv);
            decrypted = cipher.doFinal(encrypted);
          }
          else {
            cipher.init(Cipher.DECRYPT_MODE, key.getSecretKey());
            decrypted = cipher.doFinal(bytes);
          }
              
          boolean padded = false;
          byte[] unpadded = null;
          
          for (int i = decrypted.length - 1; i >= 0; i--) {
            if (decrypted[i] == PADDING)
              padded = true;
            else {
              unpadded = new byte[i + 1];
              System.arraycopy(decrypted, 0, unpadded, 0, unpadded.length);
              break;
            }
          }
								
				log.info("SERVICE_SUCCESS: decrypt("+kid+","+bytes.length+" bytes) - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo());
				
				return padded ? unpadded : decrypted;
			}
			catch (Exception e)	{
				log.error("SERVICE_FAIL: decrypt("+kid+","+bytes.length+" bytes) - " + WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo() + " : " + e.getMessage());
				log.debug("decrypt failed: " + e.getMessage(), e);
				throw new CryptoException("decrypt failed due to an internal error!");
			}
		}
	}

	
	private static synchronized PersistentKey retrieveKey(int kid) throws Exception	{
		try {
			PersistentKey persistentKey = PersistentKey.getByKID(kid);
			if (persistentKey == null)
				throw new CryptoException("Key not found for kid: " + kid);
			
			if (kekStore == null)	{
				log.debug("retrieveKey failed: KeyStore is not loaded"); 
				throw new CryptoException("KeyStore is not loaded");
			}
			
			kekStore.loadSecretKey(persistentKey);
			
			return persistentKey;
		}
		catch (Exception e)	{
			log.debug("retrieveKey failed: " + e.getMessage(), e);
			throw e;
		}
	}
	

  public byte[] DUKPTDecrypt(String algorithm, long keyID, byte[] KSN, byte[] bytes) throws CryptoException {
    //algorithm is 2TDES initially, reserved for future use
    return _DUKPTDecrypt(algorithm, keyID, KSN, bytes);
  }

  
  
  private static byte[] _DUKPTDecrypt(String algorithm, long keyID, byte[] ksn, byte[] bytes) throws CryptoException {
  	
    long st = System.currentTimeMillis();
    String ksnHex = Conversions.bytesToHex(ksn);
    KeyManagerUser user = (KeyManagerUser) WebServiceHelper.getRemoteUserPrincipal();
    String clientInfo = WebServiceHelper.getRemoteAddress() + " - " + user.getUserInfo();
    
    if (log.isDebugEnabled())
      log.debug("SERVICE_START: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + keyID + ", ksn: " + ksnHex + ", " + bytes.length + " bytes) - " + clientInfo);
    
    if (kekStore == null) {
      log.warn("SERVICE_FAIL: DUKPTDecrypt() - " + clientInfo + " : " + "KeyStore is not loaded");
      throw new CryptoException("KeyStore is not loaded");
    }
    
    try {
      byte[] key1 = new byte[8];
      byte[] key2 = new byte[8];
      kekStore.getDUKPTKey((byte)1, keyID, ksn, key1, key2);
      
      byte[] tdesKey = new byte[24];
      System.arraycopy(key1, 0, tdesKey, 0, 8);
      System.arraycopy(key2, 0, tdesKey, 8, 8);
      System.arraycopy(key1, 0, tdesKey, 16, 8);
      
      DESedeKeySpec keySpec = new DESedeKeySpec(tdesKey);
      SecretKey secretKey = SecretKeyFactory.getInstance(dukptAlgorithm).generateSecret(keySpec);
      Cipher cipher = Cipher.getInstance(dukptTransformation);
      cipher.init(Cipher.DECRYPT_MODE, secretKey, nullIvParamSpec);
      byte[] decrypted = cipher.doFinal(bytes);
                  
      log.info("SERVICE_SUCCESS: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + keyID + ", ksn: " + ksnHex + ", " + bytes.length + " bytes) - " + clientInfo + " : " + (System.currentTimeMillis() - st) + " ms");
      return decrypted;
    }
    catch (Exception e) {
      log.error("SERVICE_FAIL: DUKPTDecrypt(algorithm: " + algorithm + ", keyID: " + keyID + ", ksn: " + ksnHex + ", " + bytes.length + " bytes) - " + clientInfo + " : " + (System.currentTimeMillis() - st) + " ms" + " : " + e.getMessage());
      throw new CryptoException("Failed to DUKPT decrypt data: " + e.getMessage(), e);
    }
  }
}
