package com.usatech.keymanager.master;

import java.io.File;
import java.util.NoSuchElementException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.keymanager.CryptoException;
import com.usatech.keymanager.KeyManagerService;

public class WebAppKeyManagerLoader extends AbstractKeyManagerLoader {
	private static final Log log = LogFactory.getLog(AbstractKeyManagerLoader.class);
	public static final String SETTINGS_FILE_NAME = "WEB-INF/keymanager.properties";

	@Override
	protected PropertiesConfiguration initConfig() throws ConfigurationException {
		// load the config properties file
		String configFilePath = context.getRealPath(SETTINGS_FILE_NAME);
		try {
			return new PropertiesConfiguration(configFilePath);
		} catch(ConfigurationException e) {
			setState(STATE_START_FAILED);
			log.fatal("Failed to load KeyManager configuation: " + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	protected boolean handleLoaded() {
		try {
			log.info("Loading KeyManagerService...");
			KeyManagerService.init(this, kekStore);
			log.info("KeyManagerService has been loaded");
			return true;
		} catch(CryptoException e) {
			setState(STATE_INITIALIZE_FAILED);
			log.fatal("Failed to load KeyManagerService: " + e.getMessage(), e);
			return false;
		} catch(NoSuchElementException e) {
			setState(STATE_INITIALIZE_FAILED);
			log.fatal("Failed to load KeyManagerService configuation! Required parameter is missing: " + e.getMessage(), e);
			return false;
		}
	}

	public File getFileRealPath(String fileName) {
		return new File(context.getRealPath(fileName));
	}

	public String getVersion() {
		return Version.getVersion() + " - " + Version.getReleaseDate();
	}
}
