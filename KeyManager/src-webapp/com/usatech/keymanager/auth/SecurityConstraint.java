package com.usatech.keymanager.auth;

public class SecurityConstraint
{
	private String			methodName;
	private AuthConstraint	authConstraint	= null;

	public SecurityConstraint()
	{

	}

	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}

	public String getMethodName()
	{
		return methodName;
	}

	public void setAuthConstraint(AuthConstraint authConstraint)
	{
		this.authConstraint = authConstraint;
	}

	public AuthConstraint getAuthConstraint()
	{
		return authConstraint;
	}
}
