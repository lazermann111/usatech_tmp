package com.usatech.keymanager.auth;

import java.net.URL;
import java.security.Principal;
import java.util.Collection;

import javax.security.auth.login.LoginException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.*;

public class WebAppMethodSecurity extends GenericServlet
{
	public static final String					CONTEXT_ATTRIBUTE_NAME	= "webAppMethodSecurity";
	public static final String					DEFAULT_CONFIG_FILE		= "/WEB-INF/webapp-method-security-config.xml";
	public static final String					WEBAPP_METHOD_NAME		= WebAppMethodSecurity.class.getName() + "." + "WEBAPP_METHOD_NAME";

	private static Log							log						= LogFactory.getLog(WebAppMethodSecurity.class);
	private static Object						synchObject				= new Object();

	protected static WebAppMethodSecurityConfig	securityConfig;

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

		log.debug("init");

		synchronized (synchObject)
		{
			if (securityConfig != null)
			{
				log.warn("Already initialized!");
				return;
			}

			String configFileName = config.getInitParameter("configFileName");
			if (configFileName == null)
				configFileName = DEFAULT_CONFIG_FILE;

			try
			{
				URL configURL = config.getServletContext().getResource(configFileName);
				log.debug("Loading config from " + configURL);

				securityConfig = new WebAppMethodSecurityConfig();
				securityConfig.loadConfig(configURL);
			}
			catch (Exception e)
			{
				throw new ServletException("Error initializing WebAppMethodSecurity: " + e.getMessage(), e);
			}

			config.getServletContext().setAttribute(CONTEXT_ATTRIBUTE_NAME, this);
			log.info("Initialized Successfully");
		}
	}

	public void service(ServletRequest req, ServletResponse res) throws ServletException, java.io.IOException
	{
		log.warn("Received service call from " + req.getRemoteAddr());
		throw new ServletException("NO SERVICE");
	}

	public void destroy()
	{
		log.debug("destroy");
		super.destroy();
	}

	public synchronized static void authorize(HttpServletRequest request) throws ServletException, LoginException
	{
		if (!securityConfig.getSecurityConstraints().isEmpty())
		{
			Principal principal = request.getUserPrincipal();
			log.debug("Checking roles for " + principal);

			String webAppMethodName = (String) request.getSession().getAttribute(WEBAPP_METHOD_NAME);
			if (webAppMethodName == null)
			{
				log.info("authorize failed: WEBAPP_METHOD_NAME not defined!");
				throw new LoginException("authorize failed: WEBAPP_METHOD_NAME not defined!");
			}

			Collection<SecurityConstraint> constraints = securityConfig.getSecurityConstraints();
			for (SecurityConstraint constraint : constraints)
			{
				if (constraint.getMethodName().equalsIgnoreCase(webAppMethodName))
				{
					boolean hasRole = false;

					Collection<String> roles = constraint.getAuthConstraint().getRoles();
					for (String role : roles)
					{
						if (request.isUserInRole(role))
							hasRole = true;
						else
							log.debug("Principal " + principal.getName() + " does NOT have role " + role);
					}

					if (!hasRole)
					{
						log.info("authorize failed: user does not have role " + webAppMethodName);
						throw new LoginException("authorize failed: user does not have role " + webAppMethodName);
					}
				}
			}
		}

		log.debug("Successfully passed webappmethod security constraints");
		return;
	}
}
