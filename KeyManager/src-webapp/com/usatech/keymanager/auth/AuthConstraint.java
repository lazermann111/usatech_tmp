package com.usatech.keymanager.auth;

import java.util.*;

public class AuthConstraint
{
	private Set<String>	roles;

	public AuthConstraint()
	{
		roles = new HashSet<String>();
	}

	public void addRole(String role)
	{
		roles.add(role);
	}

	public Collection<String> getRoles()
	{
		return Collections.unmodifiableCollection(roles);
	}
}
