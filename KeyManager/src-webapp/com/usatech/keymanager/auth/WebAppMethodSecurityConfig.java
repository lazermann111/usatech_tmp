package com.usatech.keymanager.auth;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import org.apache.commons.digester.Digester;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.*;

public class WebAppMethodSecurityConfig
{
	private List<SecurityConstraint>	securityConstraints	= null;

	public WebAppMethodSecurityConfig()
	{

	}

	public Collection<SecurityConstraint> getSecurityConstraints()
	{
		return Collections.unmodifiableCollection(securityConstraints);
	}

	public void addSecurityConstraint(SecurityConstraint constraint)
	{
		securityConstraints.add(constraint);
	}

	public void loadConfig(URL configURL) throws IOException, SAXException
	{
		securityConstraints = new ArrayList<SecurityConstraint>();

		Digester digester = new Digester();

		registerLocalDTDs(digester);

		digester.push(this);
		digester.setUseContextClassLoader(true);
		digester.setValidating(true);

		digester.addObjectCreate("webapp-method-security-config/security-constraint", "com.usatech.keymanager.auth.SecurityConstraint");
		digester.addSetNext("webapp-method-security-config/security-constraint", "addSecurityConstraint", "com.usatech.keymanager.auth.SecurityConstraint");

		digester.addObjectCreate("webapp-method-security-config/security-constraint/auth-constraint", "com.usatech.keymanager.auth.AuthConstraint");
		digester.addSetNext("webapp-method-security-config/security-constraint/auth-constraint", "setAuthConstraint", "com.usatech.keymanager.auth.AuthConstraint");
		digester.addCallMethod("webapp-method-security-config/security-constraint/auth-constraint/role-name", "addRole", 0);

		digester.addCallMethod("webapp-method-security-config/security-constraint/method-name", "setMethodName", 0);

		digester.setLogger(LogFactory.getLog(Digester.class));

		InputSource input = new InputSource(configURL.openStream());
		digester.parse(input);
	}

	protected void registerLocalDTDs(Digester digester)
	{
		URL dtd1_0 = this.getClass().getResource("/com/usatech/keymanager/auth/webapp-method-security-config_1_0.dtd");
		if (dtd1_0 != null)
			digester.register("-//usatech.com//DTD Web Application Method Security Configuration 1.0//EN", dtd1_0.toString());
	}
}
