package com.usatech.keymanager.auth;

import java.util.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.security.auth.Subject;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;
import javax.security.auth.spi.LoginModule;

import org.apache.cayenne.access.DataContext;
import org.apache.commons.logging.*;

import com.usatech.keymanager.util.CertUtil;

public class KeyManagerLoginModule implements LoginModule
{
	private static Log		log				= LogFactory.getLog(KeyManagerLoginModule.class);

	private Subject			subject;
	private CallbackHandler	callbackHandler;

	// the authentication status
	private boolean			loginSucceeded	= false;
	private boolean			commitSucceeded	= false;
   
	private KeyManagerUser	user;

	public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options)
	{
		this.subject = subject;
		this.callbackHandler = callbackHandler;        

		/*if(subject != null)
			log.debug("Initialized with subject (" + subject.getClass().getName() + "): " + subject);
		else
			log.debug("Initialized with null subject");*/

		DataContext.bindThreadDataContext(DataContext.createDataContext());
	}

	public boolean login() throws LoginException
	{
		if (callbackHandler == null)
			throw new LoginException("Error: No CallbackHandler(s) available!");

		NameCallback nameCallback = new NameCallback("certThumbprint");
		PasswordCallback passwordCallback = new PasswordCallback("certBase64", false);

		Callback[] callbacks = new Callback[2];
		callbacks[0] = nameCallback;
		callbacks[1] = passwordCallback;

        String userInfo, certThumbprint, certBase64, subjectDN;
        X509Certificate cert = null;
		try
		{
			callbackHandler.handle(callbacks);
			userInfo = nameCallback.getName();
            String[] userInfoArray = userInfo.split("; ");
            if (userInfoArray.length < 3)
                throw new LoginException("Error: Unable to extract certThumbprint from provided username: " + userInfo + "!");
            certThumbprint = userInfoArray[1];            
            certBase64 = new String(passwordCallback.getPassword());
                        
            try
            {
                cert = CertUtil.getCertFromBase64(certBase64);
            }
            catch (CertificateException ce)
            {
                throw new LoginException("Failed to generate certificate from certBase64: " + ce.getMessage());
            }
            
            subjectDN = cert.getSubjectDN().getName();
			passwordCallback.clearPassword();
		}
		catch (java.io.IOException ioe)
		{
			throw new LoginException(ioe.toString());
		}
		catch (UnsupportedCallbackException uce)
		{
			throw new LoginException("Error: " + uce.getCallback().toString() + " not available!");
		}
		
		// simply getting this far means the cert was valid

		user = KeyManagerUser.getByCertThumbprint(certThumbprint);
		if (user == null)
		{
			log.info("Storing new KeyManagerUser: " + userInfo);
			
			// new user
			user = KeyManagerUser.newInstance();
			user.setSubjectName(subjectDN);
			user.setCertThumbprint(certThumbprint);
            user.setCertValidFrom(cert.getNotBefore().toString());
            user.setCertValidTo(cert.getNotAfter().toString());
            user.setCertSerialNumber(cert.getSerialNumber().toString(16));
            user.setCertBase64(certBase64);
			user.save();
		}

		log.debug("Adding default role ROLE_VALID_CERT");
		user.addToRoles(KeyManagerRole.getByName(KeyManagerRole.ROLE_VALID_CERT));

		loginSucceeded = true;
		return true;
	}

	public boolean commit() throws LoginException
	{
		if (loginSucceeded == false)
			return false;

		List<KeyManagerRole> roles = user.getRoles();
		log.debug("user has " + roles.size() + " roles...");
		
		for (KeyManagerRole role : roles)
		{
			log.debug("role="+role);
			if (!subject.getPrincipals().contains(role))
			{
				subject.getPrincipals().add(role);
				log.debug("added role");
			}
		}

		if (!subject.getPrincipals().contains(user))
		{
			log.debug("added user");
			subject.getPrincipals().add(user);
		}

		// in any case, clean out state
		user = null;

		commitSucceeded = true;

		return true;
	}

	public boolean abort() throws LoginException
	{
		if (loginSucceeded == false)
		{
			return false;
		}
		else if (loginSucceeded == true && commitSucceeded == false)
		{
			// login succeeded but overall authentication failed
			loginSucceeded = false;
			user = null;
		}
		else
		{
			// overall authentication succeeded and commit succeeded,
			// but someone else's commit failed
			logout();
		}
		return true;
	}

	public boolean logout() throws LoginException
	{
		subject.getPrincipals().remove(user);
		loginSucceeded = false;
		user = null;
		return true;
	}
}
