package com.usatech.keymanager.auth.axis;

import javax.xml.ws.handler.MessageContext;

import org.apache.axis.AxisFault;
import org.apache.axis.components.logger.LogFactory;
import org.apache.axis.description.OperationDesc;
import org.apache.axis.handlers.BasicHandler;
import org.apache.axis.message.RPCElement;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.commons.logging.Log;
import org.xml.sax.SAXException;

public abstract class SOAPMethodSecurityHandler extends BasicHandler
{
	protected static Log	log	= LogFactory.getLog(SOAPMethodSecurityHandler.class.getName());

	public void invoke(MessageContext messageContext) throws AxisFault
	{
//		HttpServletRequest request = (HttpServletRequest) messageContext.getProperty("transport.http.servletRequest");
//		if (request == null)
//			throw new AxisFault("Server.Unauthenticated", "Request not found in message context!", null, null);
//
//		KeyManagerUser user = (KeyManagerUser) request.getUserPrincipal();
//		if (user == null)
//			throw new AxisFault("Server.Unauthenticated", "User principal not initialized!", null, null);
//
//		// this is probably unnecessary
//		/*messageContext.setUsername(user.getCertThumbprint());
//		messageContext.setPassword(user.getCertBase64());
//		messageContext.setProperty(MessageContext.AUTHUSER, new SimpleAuthenticatedUser(user.getName()));*/
//
//		try
//		{
//			Message message = messageContext.getRequestMessage();
//			SOAPEnvelope envelope = message.getSOAPEnvelope();
//			RPCElement body = getBody(envelope, messageContext);
//			OperationDesc operation = getOperationDesc(messageContext, body);
//
//			request.getSession().setAttribute(WebAppMethodSecurity.WEBAPP_METHOD_NAME, operation.getName());
//			WebAppMethodSecurity.authorize(request);
//		}
//		catch (LoginException e)
//		{
//			throw new AxisFault("Server.Unauthenticated", e.getMessage(), null, null);
//		}
//		catch (Exception e)
//		{
//			throw AxisFault.makeFault(e);
//		}
	}

	public void onFault(MessageContext msgContext)
	{
		log.warn("Fault: " + msgContext);
	}

	/* The following was mostly copied directly from org.apache.axis.providers.java.RPCProvider */
	protected RPCElement getBody(SOAPEnvelope reqEnv, MessageContext msgContext) throws Exception
	{
		return null; //todo
//		SOAPService service = msgContext.getService();
//		ServiceDesc serviceDesc = service.getServiceDescription();
//		OperationDesc operation = msgContext.getOperation();
//		Vector bodies = reqEnv.getBodyElements();
//		RPCElement body = null; // Find the first "root" body element, which is the RPC call.
//		for (int bNum = 0; body == null && bNum < bodies.size(); bNum++)
//		{
//			// If this is a regular old SOAPBodyElement, and it's a root,
//			// we're probably a non-wrapped doc/lit service.  In this case,
//			// we deserialize the element, and create an RPCElement "wrapper"
//			// around it which points to the correct method.
//			// FIXME : There should be a cleaner way to do this...
//			if (!(bodies.get(bNum) instanceof RPCElement))
//			{
//				SOAPBodyElement bodyEl = (SOAPBodyElement) bodies.get(bNum);
//				// igors: better check if bodyEl.getID() != null
//				// to make sure this loop does not step on SOAP-ENC objects
//				// that follow the parameters! FIXME?
//				if (bodyEl.isRoot() && operation != null && bodyEl.getID() == null)
//				{
//					ParameterDesc param = operation.getParameter(bNum);
//					// at least do not step on non-existent parameters!
//					if (param != null)
//					{
//						Object val = bodyEl.getValueAsType(param.getTypeQName());
//						body = new RPCElement("", operation.getName(), new Object[] { val });
//					}
//				}
//			}
//			else
//			{
//				body = (RPCElement) bodies.get(bNum);
//			}
//		} // special case code for a document style operation with no
//		// arguments (which is a strange thing to have, but whatever)
//		if (body == null)
//		{
//			// throw an error if this isn't a document style service
//			if (!(serviceDesc.getStyle().equals(Style.DOCUMENT)))
//			{
//				throw new Exception(Messages.getMessage("noBody00"));
//			}
//
//			// look for a method in the service that has no arguments,
//			// use the first one we find.
//			ArrayList ops = serviceDesc.getOperations();
//			for (Iterator iterator = ops.iterator(); iterator.hasNext();)
//			{
//				OperationDesc desc = (OperationDesc) iterator.next();
//				if (desc.getNumInParams() == 0)
//				{
//					// found one with no parameters, use it
//					msgContext.setOperation(desc);
//					// create an empty element
//					body = new RPCElement(desc.getName());
//					// stop looking
//					break;
//				}
//			}
//
//			// If we still didn't find anything, report no body error.
//			if (body == null)
//			{
//				throw new Exception(Messages.getMessage("noBody00"));
//			}
//		}
//		return body;
	}

	/* The following was mostly copied directly from org.apache.axis.providers.java.RPCProvider */
	protected OperationDesc getOperationDesc(MessageContext msgContext, RPCElement body) throws SAXException, AxisFault
	{
		return null; //todo
//		SOAPService service = msgContext.getService();
//		ServiceDesc serviceDesc = service.getServiceDescription();
//		String methodName = body.getMethodName();
//
//		// FIXME (there should be a cleaner way to do this)
//		OperationDesc operation = msgContext.getOperation();
//		if (operation == null)
//		{
//			QName qname = new QName(body.getNamespaceURI(), body.getName());
//			operation = serviceDesc.getOperationByElementQName(qname);
//
//			if (operation == null)
//			{
//				SOAPConstants soapConstants = msgContext == null ? SOAPConstants.SOAP11_CONSTANTS : msgContext.getSOAPConstants();
//				if (soapConstants == SOAPConstants.SOAP12_CONSTANTS)
//				{
//					AxisFault fault = new AxisFault(Constants.FAULT_SOAP12_SENDER, Messages.getMessage("noSuchOperation", methodName), null, null);
//					fault.addFaultSubCode(Constants.FAULT_SUBCODE_PROC_NOT_PRESENT);
//					throw new SAXException(fault);
//				}
//				else
//				{
//					throw new AxisFault(Constants.FAULT_CLIENT, Messages.getMessage("noSuchOperation", methodName), null, null);
//				}
//			}
//		}
//		return operation;
	}
}
