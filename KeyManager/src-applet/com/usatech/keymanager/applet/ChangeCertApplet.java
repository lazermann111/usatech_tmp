package com.usatech.keymanager.applet;

import java.awt.*;
import java.io.*;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.*;
import java.text.*;
import javax.swing.event.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.EtchedBorder;

import com.usatech.keymanager.util.CertUtil;

public class ChangeCertApplet extends JApplet implements ListSelectionListener
{
	private static final String	LB								= System.getProperty("line.separator");
	private static final SimpleDateFormat 	dateFormatter 		= new SimpleDateFormat("MM/dd/yyyy");
	
	private static final String 			providerName 		= "SunMSCAPI";
	private static final String 			keyStoreName 		= "Windows-My";

	private static final String[] 			columnNames 		= {"Old", "Current", "Subject", "Valid from", "Valid to", "Thumbprint", "Serial Number"};
	
	private JTable 							table;
	private AbstractTableModel 				tableModel;
	private ButtonGroup 					radioButtonGroup 	= new ButtonGroup();

	private ArrayList<ArrayList<Object>>	tableRows			= new ArrayList<ArrayList<Object>>();
        
    private String                          currentCertThumbprint;
    private String                          currentSubjectDN;
    private String                          tokenCertIssuerDN;
    private String                          oldCertThumbprint   = "";
    private int                             selectedCertIndex   = 0;

	public void init()
	{
		try
		{
			currentCertThumbprint = getStringParam("currentCertThumbprint");
            currentSubjectDN = getStringParam("currentSubjectDN");
            tokenCertIssuerDN = getStringParam("tokenCertIssuerDN");

			Provider provider = Security.getProvider(providerName);

			KeyStore keyStore = KeyStore.getInstance(keyStoreName, provider);
			if (keyStore == null)
				throw new IllegalComponentStateException(keyStoreName + " keystore not found: Make sure you are using Java 6 or higher!");
			keyStore.load(null, new char[0]);
			
			KeyStoreEntryIterator keyStoreIterator = new KeyStoreEntryIterator(keyStore);
			
            boolean firstCert = true;
            boolean foundOldDN = false;
            int rowCounter = 0;
			while(keyStoreIterator.hasNext())
			{
				String certAlias = keyStoreIterator.next();
				X509Certificate[] certChain = keyStoreIterator.getCertificateChain();
				PrivateKey privKey = keyStoreIterator.getPrivateKey();

				if (privKey != null && certChain != null && certChain.length > 1)
				{
					X509Certificate x509Cert = certChain[0];
					if (x509Cert.getIssuerDN().getName().equalsIgnoreCase(tokenCertIssuerDN))
                    {
    					String certDN = x509Cert.getSubjectDN().getName();
    					String certThumbprint = CertUtil.getCertThumbprint(x509Cert);
    					Date certValidFrom = x509Cert.getNotBefore();
                        Date certValidTo = x509Cert.getNotAfter();
    					
    					ArrayList<Object> row = new ArrayList<Object>();
    
    					JRadioButton oldCertRadioButton = new JRadioButton();
                        oldCertRadioButton.setName(certThumbprint);
    					radioButtonGroup.add(oldCertRadioButton);
    					row.add(oldCertRadioButton);
                        
                        JRadioButton currentCertRadioButton = new JRadioButton();
                        row.add(currentCertRadioButton);
                        currentCertRadioButton.setEnabled(false);
    					
    					if (certThumbprint.equals(currentCertThumbprint))	
    					{
    						currentCertRadioButton.setSelected(true);
                            oldCertRadioButton.setEnabled(false);
    					}
    					else
                        {
                            oldCertRadioButton.setEnabled(true);                        
                            boolean setSelection = false;
                            
                            if (firstCert)
                            {
                                firstCert = false;
                                setSelection = true;
                            }
                            
                            if (!foundOldDN && certDN.equalsIgnoreCase(currentSubjectDN))
                            {
                                foundOldDN = true;
                                setSelection = true;
                            }
                            
                            if (setSelection)
                            {
                                oldCertRadioButton.setSelected(true);
                                selectedCertIndex = rowCounter;
                                oldCertThumbprint = certThumbprint;
                            }
                        }
    					
    					row.add(certDN);
    					row.add(dateFormatter.format(certValidFrom));
                        row.add(dateFormatter.format(certValidTo));
    					row.add(certThumbprint);
                        row.add(x509Cert.getSerialNumber().toString(16));
    					
    					tableRows.add(row);
                        rowCounter++;
                    }
				}
			}
			
			BorderLayout layout = new BorderLayout();
			layout.setHgap(2);
			layout.setVgap(2);
	
			super.setLayout(layout);
			super.setBackground(Color.WHITE);

			tableModel = new MyTableModel();
			
			table = new JComponentTable(tableModel);
			JScrollPane scrollPane = new JScrollPane(table);
			table.setFillsViewportHeight(true);
			table.setRowSelectionAllowed(true);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			//table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			table.setAutoCreateRowSorter(false);
			
			table.getColumnModel().getColumn(0).setMaxWidth(50);
			table.getColumnModel().getColumn(0).setResizable(false);
			
			table.getColumnModel().getColumn(1).setMaxWidth(50);
			table.getColumnModel().getColumn(1).setResizable(false);
			
			table.getSelectionModel().addListSelectionListener(this);
            table.getSelectionModel().setSelectionInterval(selectedCertIndex,selectedCertIndex);
			
			super.add(scrollPane, BorderLayout.CENTER);
		}
		catch(Throwable e)
		{
			StringBuffer logBuffer = new StringBuffer();
			
			logBuffer.append("Fatal Error: " + e.getMessage());
			logBuffer.append(LB);
			logBuffer.append(getStackTraceAsString(e));
			logBuffer.append(LB);
			
			JTextArea textArea = new JTextArea();
			textArea.setLineWrap(false);
			textArea.setEditable(false);
			textArea.setEnabled(true);
			textArea.setFont(new Font("monospaced", Font.PLAIN, 12));
			textArea.setBackground(Color.WHITE);
			textArea.setAutoscrolls(true);
			textArea.setText(logBuffer.toString());
			
			JScrollPane jsp = new JScrollPane(textArea);
			jsp.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
			
			super.add(jsp, BorderLayout.CENTER);
		}
	}
    
    public String getOldCertThumbprint()
    {
        return oldCertThumbprint;
    }
	
	public void valueChanged(ListSelectionEvent e)
	{
		synchronized(radioButtonGroup)
		{
			if(e.getValueIsAdjusting())
			{
				int selectedIndex = table.getSelectionModel().getAnchorSelectionIndex();
				
				int counter = 0;
				Enumeration<AbstractButton> buttonEnum = radioButtonGroup.getElements();
				
				while(buttonEnum.hasMoreElements())
				{
                    JRadioButton button = (JRadioButton) buttonEnum.nextElement();
                    if(counter == selectedIndex)
                    {
                        String certThumbprint = button.getName();
                        if (!certThumbprint.equalsIgnoreCase(currentCertThumbprint))
                        {
                            button.setSelected(true);
                            oldCertThumbprint = certThumbprint;
                            selectedCertIndex = selectedIndex;
                        }
                        break;
                    }
					counter++;
				}
                
                table.getSelectionModel().setSelectionInterval(selectedCertIndex,selectedCertIndex);
			}
		}
	}
	
	private String getStringParam(String name) throws IllegalComponentStateException
	{
		String s = super.getParameter(name);
		if (s == null)
			throw new IllegalComponentStateException("Required applet parameter not found: " + name);
		return s;
	}
	
	private static String getStackTraceAsString(Throwable e)
	{
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(bytes, true);
		e.printStackTrace(writer);
		String str = bytes.toString();

		try
		{
			writer.close();
			bytes.close();
		}
		catch (IOException ex)
		{
		}

		return str;
	}
	
	private class MyTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{
			return columnNames.length;
		}
		
		public int getRowCount()
		{
			return tableRows.size();
		}
		
		public String getColumnName(int col) 
		{
			return columnNames[col];
		}
		
		public Object getValueAt(int row, int col) 
		{
			return tableRows.get(row).get(col);
		}
		
		public boolean isCellEditable(int row, int col)
		{
			return false;
		}
	}
}
