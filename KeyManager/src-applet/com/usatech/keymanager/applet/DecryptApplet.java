package com.usatech.keymanager.applet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.IllegalComponentStateException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.X509Certificate;

import javax.crypto.Cipher;
import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EtchedBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import com.usatech.keymanager.util.CertUtil;

public class DecryptApplet extends JApplet
{
	private static final String	LB	= System.getProperty("line.separator");

	private byte[]				decryptedBytes;
	private Throwable			e;

	private StringBuilder		logBuffer	= new StringBuilder();
	private JTextArea			textArea;
	private JTextPane			statusPane;
	
	private Cipher 				asymmetricCipher;
	private byte[] 				encryptedBytes;
	
	private PrivateKey 			privKey;

	public void init()
	{
		BorderLayout layout = new BorderLayout();
		layout.setHgap(2);
		layout.setVgap(2);

		super.setLayout(layout);
		super.setBackground(Color.WHITE);

		textArea = new JTextArea();
		textArea.setLineWrap(false);
		textArea.setEditable(false);
		textArea.setEnabled(true);
		textArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		textArea.setBackground(Color.WHITE);
		textArea.setAutoscrolls(true);

		JScrollPane jsp = new JScrollPane(textArea);
		jsp.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED), BorderFactory.createEmptyBorder(2, 2, 2, 2)));

		statusPane = new JTextPane();
		statusPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
		statusPane.setFont(new Font("monospaced", Font.BOLD, 14));
		statusPane.setEditable(false);
		
		super.add(statusPane, BorderLayout.NORTH);
		super.add(jsp, BorderLayout.CENTER);

		try
		{
			String asymmetricCipherID = getStringParam("asymmetricCipherID");
			String encryptedBase64 = getStringParam("encryptedBase64");
			String currentCertThumbprint = getStringParam("certThumbprint");
			
			String providerName = "SunMSCAPI";
			Provider provider = Security.getProvider(providerName);

			asymmetricCipher = Cipher.getInstance(asymmetricCipherID, provider);
			log("Loaded asymmetric cipher");
			
			String keyStoreName = "Windows-My";
			KeyStore keyStore = KeyStore.getInstance(keyStoreName, provider);
			if (keyStore == null)
				throw new IllegalComponentStateException(keyStoreName + " keystore not found: Make sure you are using Java 6 or higher!");
            keyStore.load(null, new char[0]);
			
			encryptedBytes = CertUtil.decodeBase64(encryptedBase64);
			log("Base64 decoded " + encryptedBytes.length + " encrypted bytes");
			
			KeyStoreEntryIterator keyStoreIterator = new KeyStoreEntryIterator(keyStore);
			
			while(keyStoreIterator.hasNext())
			{
				String certAlias = keyStoreIterator.next();
				X509Certificate[] certChain = keyStoreIterator.getCertificateChain();
				PrivateKey privKey = keyStoreIterator.getPrivateKey();

				if (privKey != null && certChain != null && certChain.length > 1)
				{
					X509Certificate x509Cert = certChain[0];
					String certThumbprint = CertUtil.getCertThumbprint(x509Cert);

					if (certThumbprint.equalsIgnoreCase(currentCertThumbprint))
					{
						log("Found certificate with matching thumbprint for subject: " + x509Cert.getSubjectDN().getName());
						this.privKey = privKey;
						break;
					}
				}
			}
			
			asymmetricCipher.init(Cipher.DECRYPT_MODE, privKey);
			log("Asymmetric cipher initialized");
			
			decryptedBytes = asymmetricCipher.doFinal(encryptedBytes);
			log("Asymmetric decrypted " + decryptedBytes.length + " bytes");
		}
		catch (Throwable e)
		{
			log("Fatal Error: " + e.getMessage(), e);
			e.printStackTrace();
			this.e = e;
		}
		finally
		{
			textArea.setText(logBuffer.toString());

			if (e == null)
			{
				statusPane.setBackground(Color.GREEN);
				statusPane.setText("DECRYPTION SUCCESSFUL!");
			}
			else
			{
				statusPane.setBackground(Color.RED);
				statusPane.setText("DECRYPTION FAILURE!");
			}
		}
	}
	
	public Throwable getException()
	{
		return e;
	}

	public boolean decryptSuccess()
	{
		return (decryptedBytes != null);
	}

	public String getDecryptedBase64()
	{
		if (decryptedBytes == null)
			return null;
		return CertUtil.encodeBase64(decryptedBytes);
		// return Base64.encodeBytes(decryptedBytes);
	}

	private void log(String s)
	{
		log(s, null);
	}

	private void log(String s, Throwable e)
	{
		logBuffer.append(s);
		logBuffer.append(LB);

		if(textArea != null) {
			Document doc = textArea.getDocument();
			try {
				doc.insertString(doc.getLength(), s + LB, null);
			} catch(BadLocationException e1) {
				// ignore
			}
		}
		if (e != null)
		{
			//logBuffer.append(getStackTraceAsString(e));
			//logBuffer.append(LB);
			e.printStackTrace();
		}
	}

	private String getStringParam(String name) throws IllegalComponentStateException
	{
		String s = super.getParameter(name);
		if (s == null)
			throw new IllegalComponentStateException("Required applet parameter not found: " + name);
		return s;
	}
	
	private static String getStackTraceAsString(Throwable e)
	{
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		PrintWriter writer = new PrintWriter(bytes, true);
		e.printStackTrace(writer);
		String str = bytes.toString();

		try
		{
			writer.close();
			bytes.close();
		}
		catch (IOException ex)
		{
		}

		return str;
	}
}
