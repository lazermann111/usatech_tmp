package com.usatech.keymanager.applet;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.KeyStore;
import java.security.KeyStoreSpi;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

public class KeyStoreEntryIterator implements Iterator<String> {
	private final KeyStoreSpi keyStoreSpi;
	private final Collection<?> entries;
	private final Iterator<?> iterator;
	private Object entry;

	public KeyStoreEntryIterator(KeyStore keyStore) throws Exception {
		keyStoreSpi = (KeyStoreSpi) getField(keyStore.getClass(), keyStore, "keyStoreSpi");
		entries = ((Map<?, ?>) getField(keyStoreSpi.getClass().getSuperclass(), keyStoreSpi, "entries")).values();
		iterator = entries.iterator();
	}

	public boolean hasNext() {
		return iterator.hasNext();
	}

	public String next() {
		try {
			entry = iterator.next();
			return (String) executeMethod(entry, "getAlias");
		} catch(Exception e) {
			throw new NoSuchElementException("executeMethod getAlias failed: " + e.getMessage());
		}
	}

	public X509Certificate[] getCertificateChain() throws Exception {
		if(entry == null)
			throw new NoSuchElementException();

		return (X509Certificate[]) executeMethod(entry, "getCertificateChain");
	}

	public PrivateKey getPrivateKey() throws Exception {
		if(entry == null)
			throw new NoSuchElementException();

		return (PrivateKey) executeMethod(entry, "getPrivateKey");
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

	private static Object getField(Class<?> cls, Object instance, String fieldName) throws Exception {
		Field[] fields = cls.getDeclaredFields();
		for(Field field : fields) {
			if(field.getName().equals(fieldName)) {
				field.setAccessible(true);
				return field.get(instance);
			}
		}

		throw new Exception("Field not found: " + fieldName);
	}

	private static Object executeMethod(Object instance, String methodName) throws Exception {
		Method[] methods = instance.getClass().getDeclaredMethods();
		for(Method method : methods) {
			if(method.getName().equals(methodName)) {
				method.setAccessible(true);
				return method.invoke(instance, new Object[] {});
			}
		}

		throw new Exception("Method not found: " + methodName);
	}
}
