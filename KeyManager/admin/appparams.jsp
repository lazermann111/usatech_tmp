<%@ page import="
com.google.gson.Gson,
com.usatech.keymanager.app.AppParams, 
com.usatech.keymanager.auth.KeyManagerUser,
com.usatech.keymanager.master.KeyManagerLoader,
com.usatech.keymanager.master.MasterKeyLoader,
com.usatech.keymanager.util.KeyManagerUtil,
com.usatech.util.Base64
"%>
<%
AppParams appParams = new AppParams();
Gson gson = new Gson();
try {
	String action = request.getParameter("action");
	if ("decrypt".equalsIgnoreCase(action)) {
		KeyManagerUser currentUser = KeyManagerUtil.getCurrentKeyManagerUser(request);
		String currentCertThumbprint = currentUser.getCertThumbprint();
		String currentUserInfo = currentUser.getUserInfo();
		KeyManagerLoader loader = (KeyManagerLoader) getServletConfig().getServletContext().getAttribute(KeyManagerLoader.CONTEXT_KEY);
		MasterKeyLoader masterKeyLoader = loader.getMasterKeyLoader();
		String encryptedBase64 = Base64.encodeBytes(masterKeyLoader.getEncryptedSecret(currentCertThumbprint), 0x8);
		String asymmetricCipherID = masterKeyLoader.getAsymmetricCipherID();
				
		appParams.setAsymmetricCipherID(asymmetricCipherID);
		appParams.setCurrentCertThumbprint(currentCertThumbprint);
		appParams.setEncryptedBase64(encryptedBase64);
	}
} catch (Exception e) {
	String message = e.getMessage();
	if (message == null && e.getCause() != null)
		message = e.getCause().getMessage();
	appParams.setError(message);
}
out.write(gson.toJson(appParams));
%>