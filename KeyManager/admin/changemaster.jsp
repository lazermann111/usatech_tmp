<%@ include file="../header.jsp" %>

<%
	SSSS ssss = loader.getSSSS();
	List<KeyManagerUser> users = KeyManagerUser.getAllUsers();
	SortedMap<String, KeyManagerUser> loaders = masterKeyLoader.getLoadersInfo();
  
  	if (process != null)
  	{
		if (process.equals("Decrypt My Secret For Change")) {
			response.sendRedirect("decrypt.jsp?return=" + request.getRequestURI());
			return;
		}
		else if(process.equals("Continue")) {
			response.sendRedirect(request.getRequestURI());
			return;
		}
		else if(process.equals("Complete Master Key Change")) {
			ArrayList<String> certThumbprints = new ArrayList<String>();
			Enumeration params = request.getParameterNames();
			while(params.hasMoreElements()) {
				String paramName = (String) params.nextElement();
				if (paramName.startsWith("cb_installer_"))
					certThumbprints.add(paramName.substring(13));
			}
			
			if (loaders.size() + certThumbprints.size() != ssss.getN()) {
%>		
<table width="750">
 <tr>
  <th class="failure">Please select exactly <%=ssss.getN() - loaders.size()%> additional key custodian(s)!</th>
 </tr>
</table>
<br>
<%
			}
			else
			{
				masterKeyLoader.load(MasterKeyLoader.ACTION_CHANGE_MASTER_KEY, currentUser, certThumbprints);
				response.sendRedirect("index.jsp?msg=" + response.encodeURL("Master Key has been changed"));
				return;
			}
		}
	}
	
	boolean canLoad = masterKeyLoader.canLoad(currentUser);	
%>

<form method="POST">

<%	if(loader.getState() < KeyManagerLoader.STATE_INSTALLED) { %>
<table width="750">
 <tr>
  <th class="failure">Invalid state for Master Change: <%=KeyManagerLoader.STATE_NAMES[loader.getState()]%></th>
 </tr>
</table><br>
<br>
<%	} else { %>
MASTER KEY CHANGE
<table width="750">
<%
		if(loaders.size() == 0) { 
%>
 <tr>
  <th>
   Change has not been initiated.<br>
   Upon initiation, at least <%=ssss.getT()%> SA's are required to submit their secret within <%=masterKeyLoader.getLoadTimeoutSeconds()%> seconds.
  </th>
 </tr>
<%		} else { %>
 <tr>
  <th colspan="3">
   <%=loaders.size() %> of at least <%=ssss.getT()%> required secrets are loaded with <%=masterKeyLoader.getLoadRemainingSeconds()%> seconds remaining.
   <%=canLoad && loaders.size() < ssss.getN() ? "<br>Please select " + (ssss.getN() - loaders.size()) + " additional key custodian(s)." : ""%>
  <%if (canLoad) {%>
  <br>
  <%}%>
  </th>
 </tr>
<%			for(KeyManagerUser user : users) {
				if (!user.hasRole(KeyManagerRole.getByName(KeyManagerRole.ROLE_SECURITY_ADMIN)))
					continue;
				boolean isLoader = loaders.containsValue(user);
%>
 <tr>
  <td><%=isLoader ? "X" : canLoad && loaders.size() < ssss.getN() ? "<input type='checkbox' name='cb_installer_" + user.getCertThumbprint() + "'>" : ""%></td>
  <td><%=user.getUserInfo()%></td>
  <%=isLoader ? "<th class=\"success\" nowrap>Loaded</th>" : "<th nowrap>Not Loaded</th>" %>
 </tr>
<%			} %>
<%		} %>
</table>
<br>
<%		if(!loaders.containsKey(currentUserInfo)) { %>
<input type="submit" name="process" value="Decrypt My Secret For Change">
<%		} else { %>
<input type=button value="Refresh" onClick="javascript:window.location=window.location;"> 
<%		} %>

<%		if(canLoad) { %>
<input type="submit" name="process" value="Complete Master Key Change" onclick="this.style.display = 'none'; document.getElementById('loading').style.display = 'inline';">
<span id="loading" style="display:none;">Loading...</span>
<%		} %>
<%	} %>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>

<%@ include file="../footer.jsp" %>
