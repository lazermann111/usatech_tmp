<%@ include file="../header.jsp" %>

<%	KeyManagerInstaller installer = loader.getKeyManagerInstaller();
	
	if (process != null) {
		if (process.equals("Continue")) {
			installer.addInstaller(currentUser);
			response.sendRedirect(request.getRequestURI());
			return;
		}
		else if (process.equals("Complete Install")) {
			installer.install(currentUser);
			response.sendRedirect(request.getRequestURI());
			return;
		}
	}
%>

<form method="POST">

<%	if(loader.getState() == KeyManagerLoader.STATE_INSTALLED) { %>
<table width="750">
 <tr>
  <th>Installation Complete!</th>
 </tr>
</table><br>
<br>
<%	} else if(installer == null) { %>
<table width="750">
 <tr>
  <th class="failure">Installer Not Found!</th>
 </tr>
</table><br>
<br>
<%	} else { %>
KEYMANAGER INSTALLATION
<table width="750">
<%		SSSS ssss = loader.getSSSS();
		SortedMap<String, KeyManagerUser> installers = installer.getInstallersInfo();
		if(installers.size() == 0) { %>
 <tr>
  <th>
   Installation has not been initiated.<br>
   Upon initiation, <%=ssss.getN()%> SA's are required to complete the install within <%=installer.getInstallTimeoutSeconds()%> seconds.
  </th>
 </tr>
<%		} else { %>
 <tr>
  <th>
   <%=installers.size()%> of <%=ssss.getN()%> required certificate(s) are loaded with <%=installer.getInstallRemainingSeconds()%> seconds remaining.
  </th>
 </tr>
<%			for(String installerInfo : installers.keySet()) { %>
 <tr>
  <td><%=installerInfo%></td>
 </tr>
<%			} %>
<%		} %>
</table>
<br>
<%		if(!installers.containsKey(currentUserInfo)) { %>
<table width="750">
 <tr>
  <td class="td_center">
   <input type="submit" name="process" value="Continue">
  </td>
 </tr>
</table>
<br>
<%		} else { %>
<input type=button value="Refresh" onClick="javascript:window.location=window.location;">
<%		} %>

<%		if(installer.canInstall(currentUser)) { %>
<input type="submit" name="process" value="Complete Install" onclick="this.style.display = 'none'; document.getElementById('loading').style.display = 'inline';">
<span id="loading" style="display:none;">Loading...</span>
<%		} %>
<%	} %>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>

<%@ include file="../footer.jsp" %>
