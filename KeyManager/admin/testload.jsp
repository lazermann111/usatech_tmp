<%@ include file="../header.jsp" %>

<%
	if(process != null) {
		if (process.equals("Decrypt My Secret For Loading")) {
			response.sendRedirect("decrypt.jsp?return=" + request.getRequestURI());
			return;
		}
		else if (process.equals("Continue")) {
			response.sendRedirect(request.getRequestURI());
			return;
		}
		else if(process.equals("Test Load")) {
			Map<String,String> entries;
			try {
			    entries = masterKeyLoader.testLoad(currentUser);
			} catch(Exception e) {
				log.error("Could not load KEK Store", e);
				%><table width="750">
 <tr>
  <th class="failure">Could not load KEK Store from file because "<%=e.getMessage() %>". It is recommended that you Save the KEK Store. Also see the log for more details.</th>
 </tr>
</table>

<%@ include file="../footer.jsp" %><%
                 return;
			}
			%><b>Found <%=entries.size() %> KEK Store Entries:</b>
			<table width="750">
			 <tr>
			  <th>Alias</th><th>Type</th></tr><%
			  for(Map.Entry<String,String> entry : entries.entrySet()) {
				  %><tr><td><%=entry.getKey()%></td><td><%=entry.getValue()%></td></tr><%
			  }
			  %>			 
			</table>
			<br/>
			<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
			<%@ include file="../footer.jsp" %><%
			 return;
		}
	}
%>

<form method="POST">

<%	if(loader.getState() != KeyManagerLoader.STATE_INITIALIZED) { %>
<table width="750">
 <tr>
  <th class="failure">Invalid state for testing load of KEK Store: <%=KeyManagerLoader.STATE_NAMES[loader.getState()]%></th>
 </tr>
</table><br>
<br>
<%	} else { %>
TEST LOAD
<table width="750">
<%		SSSS ssss = loader.getSSSS();
		SortedMap<String, String> keyCustodians = masterKeyLoader.getKeyCustodiansInfo();
		SortedMap<String, KeyManagerUser> loaders = masterKeyLoader.getLoadersInfo();
		if(loaders.size() == 0) { %>
 <tr>
  <th>
   Testing of Load has not been initiated.<br>
   Upon initiation, at least <%=ssss.getT()%> SA's are required to submit their secret within <%=masterKeyLoader.getLoadTimeoutSeconds()%> seconds.
  </th>
 </tr>
<%		} else { %>
 <tr>
  <th colspan="2">
   <%=loaders.size() %> of at least <%=ssss.getT()%> required secrets are loaded with <%=masterKeyLoader.getLoadRemainingSeconds()%> seconds remaining.
  </th>
 </tr>
<%			for(String keyCustodianInfo : keyCustodians.keySet()) { %>
 <tr>
  <td><%=keyCustodianInfo%></td>
  <%=loaders.containsKey(keyCustodianInfo) ? "<th class=\"success\" nowrap>Loaded</th>" : "<th nowrap>Not Loaded</th>" %>
 </tr>
<%			} %>
<%		} %>
</table>
<br>
<%		if(!loaders.containsKey(currentUserInfo)) { %>
<input type="submit" name="process" value="Decrypt My Secret For Loading">
<%		} else { %>
<input type=button value="Refresh" onClick="javascript:window.location=window.location;">
<%		} %>

<%		if(masterKeyLoader.canLoad(currentUser)) { %>
<input type="submit" name="process" value="Test Load" onclick="this.style.display = 'none'; document.getElementById('loading').style.display = 'inline';">
<span id="loading" style="display:none;">Loading...</span>
<%		} %>
<%	} %>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>

<%@ include file="../footer.jsp" %>
