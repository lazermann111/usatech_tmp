<%@ include file="../header.jsp" %>

<%	String certThumbprint = request.getParameter("certThumbprint");
	if(certThumbprint == null || certThumbprint.length() <= 0) {
		log.warn("EDIT_CLIENT: Required parameter not found: certThumbprint " + request.getRemoteAddr() + " - " + currentUserInfo);
		out.println("Required parameter not found: certThumbprint");
		return;
	}
	
	KeyManagerUser user = KeyManagerUser.getByCertThumbprint(certThumbprint);
	if(user == null) {
		log.warn("EDIT_CLIENT: Fatal Error: Failed to lookup user in database for certThumbprint " + certThumbprint + "! " + request.getRemoteAddr() + " - " + currentUserInfo);
		out.println("Fatal Error: Failed to lookup user in database!");
		return;
	}
	
	String userSubjectDN = user.getSubjectName();
	String userCertThumbprint = user.getCertThumbprint();
	String userInfo = user.getUserInfo();
	X509Certificate userCert = user.getCertificate();
	
	String[] roleParams = request.getParameterValues("user_role");
	if (process != null) {
		if (process.equals("Update Roles")) {
			List<String> newRoles;
			if(roleParams == null || roleParams.length == 0) 
				newRoles = new ArrayList<String>();
			else
				newRoles = Arrays.asList(roleParams);
			List<KeyManagerRole> assignedRoles = user.getRoles();
			for(String newRoleName : newRoles) {
				KeyManagerRole newRole = KeyManagerRole.getByName(newRoleName);
				if(newRole == null) {
					log.warn("EDIT_CLIENT("+userInfo+"): Fatal Error: Role " + newRoleName + " not found " + request.getRemoteAddr() + " - " + currentUserInfo);
					out.println("Fatal Error: Role " + newRoleName + " not found");
					return;
				}
	
				if(assignedRoles != null && !assignedRoles.contains(newRole)) {
					log.info("EDIT_CLIENT("+userInfo+"): addToRoles("+newRole.getName()+") " + request.getRemoteAddr() + " - " + currentUserInfo);
					user.addToRoles(newRole);
				}
			}
			
			List<KeyManagerRole> rolesToRemove = new ArrayList<KeyManagerRole>();
			
			for(KeyManagerRole oldRole : assignedRoles) {
				if(oldRole.getName().equals(KeyManagerRole.ROLE_VALID_CERT))
					continue;
				if(!newRoles.contains(oldRole.getName()))
					rolesToRemove.add(oldRole);
			}
			
			for(KeyManagerRole roleToRemove : rolesToRemove) {
				log.info("EDIT_CLIENT("+userInfo+"): removeFromRoles("+roleToRemove.getName()+") " + request.getRemoteAddr() + " - " + currentUserInfo);
				user.removeFromRoles(roleToRemove);
			}
			
			user.save();
			
			if(userCertThumbprint.equals(currentCertThumbprint)) {
				request.getSession().invalidate();
				response.sendRedirect(request.getRequestURI() + "?process=view&certThumbprint=" + userCertThumbprint);
				return;
			}
			
		}		
		else if(process.equals("Delete Client")) {
			if(userCertThumbprint.equals(currentCertThumbprint)) {
				log.warn("EDIT_CLIENT("+userInfo+"): You may not delete yourself!" + request.getRemoteAddr() + " - " + currentUserInfo);
				out.println("You may not delete yourself!");
				return;
			}
					
			log.info("EDIT_CLIENT("+userInfo+"): delete " + request.getRemoteAddr() + " - " + currentUserInfo);
			user.delete();
		}
	}
%>
<form method="POST" action="edit_client.jsp">
CLIENT CERTIFICATE<br>
<table width="750">
 <tr>
  <td>SUBJECT</td>
  <td><%=userSubjectDN%></td>
 </tr>
 <tr>
  <td>THUMBPRINT</td>
  <td><%=userCertThumbprint%></td>
 </tr>
 <tr>
  <td>VALID FROM</td>
  <td><%=user.getCertValidFrom()%></td>
 </tr>
 <tr>
  <td>VALID TO</td>
  <td><%=user.getCertValidTo()%></td>
 </tr>
 <tr>
  <td>SERIAL #</td>
  <td><%=user.getCertSerialNumber()%></td>
 </tr>
 <tr>
 	<td>PUBLIC KEY</td>
 	<td><%=CertUtil.getPublicKeyInfo(userCert)%></td>
 </tr>
 <tr>
 	<td>KEY USAGE</td>
 	<td><%=CertUtil.getKeyUsageInfo(userCert)%></td>
 </tr>
</table>
<br>
ASSIGNED ROLES<br>
<table width="750">
<%	
  boolean userKeyEncipherment = false;
  if (userCert != null && userCert.getKeyUsage() != null && userCert.getKeyUsage().length > 2)
  	userKeyEncipherment = userCert.getKeyUsage()[2];
  
  for(KeyManagerRole role : allRoles) { 
	if (!userKeyEncipherment && role.getName().equals("SA"))
		continue;
%>
 <tr>
  <td class="td_center"><input type="checkbox" name="user_role" value="<%=role.getName() %>" <%=(user.getRoles().contains(role) ? "checked" : "") %>>
  <td><%=role.getName() %></td>
  <td><%=role.getDesc() %></td>
 </tr>
<%	} %>
</table>
<br>
<input type="submit" name="process" value="Update Roles" class="button"> 
<%if (!masterKeyLoader.isKeyCustodian(userCertThumbprint) && !userCertThumbprint.equalsIgnoreCase(currentCertThumbprint)) {%>
<input type="submit" name="process" value="Delete Client" class="button">
<%}%>
<input type="hidden" name="certThumbprint" value="<%=userCertThumbprint%>"><br>
<br>
<input type=button value="Client Administration" onClick="javascript:window.location='list_clients.jsp';"> 
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 

</form>

<%@ include file="../footer.jsp" %>
