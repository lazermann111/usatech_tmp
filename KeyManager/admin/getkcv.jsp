<%@ include file="../header.jsp" %>
<%
String bdkKeyIdText = request.getParameter("bdkKeyId");
if(bdkKeyIdText != null && (bdkKeyIdText=bdkKeyIdText.trim()).length() > 0) {
	long bdkKeyId = Long.parseLong(bdkKeyIdText);
	String kcv = KEKStore.getKCV(loader.getKEKStore().getBDKSecret(bdkKeyId).getEncoded());
    %><table width="250"><tr><td>KCV of BDK <%=bdkKeyId %>: <b><%=kcv %></b></td></tr></table><%
 }
 %>
 <br/>
<form method="POST">
BDK Key ID: <select name="bdkKeyId"><option value="">-- Please select --</option>
<% for(String alias : loader.getKEKStore().getBDKAliases()) {
	if(alias.matches("bdk\\d+")) {
		long bdkId = Long.parseLong(alias.substring(3));
	    %><option value="<%=bdkId%>"><%=bdkId%></option><%
	}
}%>
</select>
<input type="submit" value="View KCV"/>
<br/><br/>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>
<%@ include file="../footer.jsp" %>
