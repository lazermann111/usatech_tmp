<%@ page import="
com.usatech.keymanager.auth.KeyManagerUser,
com.usatech.keymanager.master.KeyManagerLoader,
com.usatech.keymanager.master.MasterKeyLoader,
com.usatech.keymanager.util.KeyManagerUtil,
com.usatech.util.Base64,
com.usatech.util.Util
"%>
<%
KeyManagerUser currentUser = KeyManagerUtil.getCurrentKeyManagerUser(request);
String action = request.getParameter("action");
if (action == null || action.length() < 1)
	return;
KeyManagerLoader loader = (KeyManagerLoader) getServletConfig().getServletContext().getAttribute(KeyManagerLoader.CONTEXT_KEY);
MasterKeyLoader masterKeyLoader = loader.getMasterKeyLoader();
String decryptedSecretBase64 = request.getParameter("s");
byte[] decryptedSecretBytes = Base64.decode(decryptedSecretBase64);
char[] decryptedSecretChars = Util.convertToChars(decryptedSecretBytes);
if ("decrypt".equals(action)) {
	masterKeyLoader.addDecryptedSecret(currentUser, decryptedSecretChars);
} else if ("changecert".equals(action)) {
	String oldCertThumbprint = request.getParameter("param");
	masterKeyLoader.changeCertificate(currentUser, oldCertThumbprint, decryptedSecretBytes);
}
Util.overwrite(decryptedSecretBytes);
%>