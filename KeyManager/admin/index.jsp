<%@ include file="../header.jsp" %>

<%	
	KEKStore kekStore = loader.getKEKStore();
	boolean isKeyCustodian = masterKeyLoader.isKeyCustodian(currentCertThumbprint);
%>

<form method="POST">

<table width="750">
<%	if(loader == null) { %>
 <tr>
  <th class="failure">SERIOUS ERROR: KEYMANAGER NOT FOUND</th>
 </tr>
<%	} else { %>
 <tr>
  <th>Version <%=loader.getVersion()%></th>
 </tr>  
<%	String msg = request.getParameter("msg");
	if (msg != null) { %>
	<tr><th class="success"><%=msg%></th></tr>
<%	} %>
</table>
&nbsp;
<br>
KEY MANAGER SERVICE STATUS
<table width="750">
<%		
		for(int i=0; i<KeyManagerLoader.STATE_NAMES.length; i++) {
			String stateName = KeyManagerLoader.STATE_NAMES[i];
			String stateClass = "inactive_status";
			if (loader.getState() == i)
			{
				if (stateName.startsWith("NOT_") || stateName.endsWith("_FAILED"))
					stateClass = "failure";
				else if (stateName.equals("INITIALIZED"))
					stateClass = "active_initialized_status";
				else
					stateClass = "active_status";
			}
%>
 <tr>
  <td class="<%=stateClass%>"><%=stateName%></td>
 </tr>
<%		} %>
<%	} %>
</table>
<br>
<% 	if(loader.getState() == KeyManagerLoader.STATE_INITIALIZED && kekStore.getKEKAliases().isEmpty()) { %>
<table width="750">
 <tr>
  <th class="failure">WARNING: KEK Keystore is Empty!</th>
 </tr>
</table>
<br>
<%	} %>
<input type=button value="Client Administration" onClick="javascript:window.location='list_clients.jsp';">  
<%	int loaderState = loader.getState();
	if(isKeyCustodian && loaderState >= KeyManagerLoader.STATE_INSTALLED) { %>
		<input type=button value="Change Master Key" onClick="javascript:window.location='changemaster.jsp';">
<%	}
	if(loaderState == KeyManagerLoader.STATE_NOT_INSTALLED || loaderState == KeyManagerLoader.STATE_INSTALL_FAILED) { %>
		<input type=button value="Start Install" onClick="javascript:window.location='install.jsp';">  
<%	} else if(isKeyCustodian) {
    	if(loaderState == KeyManagerLoader.STATE_INSTALLED || loaderState == KeyManagerLoader.STATE_LOAD_FAILED) { %>
			<input type=button value="Start Load" onClick="javascript:window.location='load.jsp';">  
<%		} else if(loaderState == KeyManagerLoader.STATE_INITIALIZED) {
    		if(kekStore.getKEKAliases().isEmpty()) { %>
				<input type=button value="Generate KEK" onClick="javascript:window.location='genkek.jsp';">  
<%			} else { %>
				<input type=button value="Change KEK" onClick="javascript:window.location='genkek.jsp';">
				<input type=button value="Generate BDK" onClick="javascript:window.location='genbdk.jsp';">
		        <input type=button value="Update BDK" onClick="javascript:window.location='genbdk.jsp?update=true';">
		        <br><br>
				<input type=button value="Generate AVAS Component" onClick="javascript:window.location='genavas.jsp?component=true';">
				<input type=button value="Generate AVAS Key" onClick="javascript:window.location='genavas.jsp';">				
                <input type=button value="Save KEK Store" onClick="javascript:window.location='savekekstore.jsp';">
                <input type=button value="Test Load" onClick="javascript:window.location='testload.jsp';">
                <input type=button value="View KCV" onClick="javascript:window.location='getkcv.jsp';">
<%			} %>
<%		}
	} %>
</form>

<%@ include file="../footer.jsp" %>
