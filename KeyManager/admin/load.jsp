<%@ include file="../header.jsp" %>

<%
	if (process != null) {
		if (process.equals("Decrypt My Secret For Load")) {
			response.sendRedirect("decrypt.jsp?return=" + request.getRequestURI());
			return;
		}
		else if (process.equals("Continue")) {
			response.sendRedirect(request.getRequestURI());
			return;
		}
		else if (process.equals("Complete Load")) {
			masterKeyLoader.load(MasterKeyLoader.ACTION_LOAD, currentUser, null);
			response.sendRedirect(request.getRequestURI());
			return;
		}
	}
%>

<form method="POST">

<%	if(loader.getState() == KeyManagerLoader.STATE_LOADED || loader.getState() == KeyManagerLoader.STATE_INITIALIZED) { %>
<table width="750">
 <tr>
  <th>Load Complete!</th>
 </tr>
</table><br>
<br>
<%	} else if(masterKeyLoader == null) { %>
<table width="750">
 <tr>
  <th class="failure">Loader Not Found!</th>
 </tr>
</table><br>
<br>
<%	} else { %>
KEYMANAGER LOADER
<table width="750">
<%		SSSS ssss = loader.getSSSS();
		SortedMap<String, String> keyCustodians = masterKeyLoader.getKeyCustodiansInfo();
		SortedMap<String, KeyManagerUser> loaders = masterKeyLoader.getLoadersInfo();
		if(loaders.size() == 0) { %>
 <tr>
  <th>
   Load has not been initiated.<br>
   Upon initiation, at least <%=ssss.getT()%> SA's are required to submit their secret within <%=masterKeyLoader.getLoadTimeoutSeconds()%> seconds.
  </th>
 </tr>
<%		} else { %>
 <tr>
  <th colspan="2">
   <%=loaders.size() %> of at least <%=ssss.getT()%> required secrets are loaded with <%=masterKeyLoader.getLoadRemainingSeconds()%> seconds remaining.
  </th>
 </tr>
<%			for(String keyCustodianInfo : keyCustodians.keySet()) { %>
 <tr>
  <td><%=keyCustodianInfo%></td>
  <%=loaders.containsKey(keyCustodianInfo) ? "<th class=\"success\" nowrap>Loaded</th>" : "<th nowrap>Not Loaded</th>" %>
 </tr>
<%			} %>
<%		} %>
</table>
<br>
<%		if(!loaders.containsKey(currentUserInfo)) { %>
<input type="submit" name="process" value="Decrypt My Secret For Load">
<%		} else { %>
<input type=button value="Refresh" onClick="javascript:window.location=window.location;">
<%		} %>

<%		if(masterKeyLoader.canLoad(currentUser)) { %>
<input type="submit" name="process" value="Complete Load" onclick="this.style.display = 'none'; document.getElementById('loading').style.display = 'inline';">
<span id="loading" style="display:none;">Loading...</span>
<%		} %>
<%	} %>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>

<%@ include file="../footer.jsp" %>
