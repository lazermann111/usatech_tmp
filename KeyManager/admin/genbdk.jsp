<%@ include file="../header.jsp" %>

<%
boolean update = "true".equalsIgnoreCase(request.getParameter("update"));
if(process != null) {
		if (process.equals("Decrypt My Secret For Generation")) {
			response.sendRedirect("decrypt.jsp?return=" + request.getRequestURI() + (update ? "%3Fupdate=true" : ""));
			return;
		}
		else if (process.equals("Continue")) {
			if (request.getParameter("bdkComponent") != null) {
				String tmp = request.getParameter("bdkKeyId");
				if(tmp == null || (tmp=tmp.trim()).length() == 0) {
					%><table width="750">
				 <tr>
				  <th class="failure">Required parameter not found: bdkKeyId</th>
				 </tr>
				</table><%
			        return;
				}
				long keyID;
				try {
					keyID = Long.parseLong(tmp);
				} catch(NumberFormatException e) {
					%><table width="750">
	                 <tr>
	                  <th class="failure">Could not parse parameter "bdkKeyId" with value "<![CDATA[<%=tmp.replace("]]>", "]]]><![CDATA[]>") %>]]>" to a number</th>
	                 </tr>
	                </table><%
	                    return;
				}
				masterKeyLoader.addBDKComponent(currentUser, Conversions.hexToByteArray(request.getParameter("bdkComponent")), keyID);
			}
			response.sendRedirect(request.getRequestURI() + (update ? "?update=true" : ""));
			return;
		}
		else if(process.equals("Generate BDK") || process.equals("Update BDK")) {
			KEKInfo keyInfo = masterKeyLoader.load(update ? MasterKeyLoader.ACTION_UPDATE_BDK : MasterKeyLoader.ACTION_GENERATE_BDK, currentUser, null);
			if(keyInfo == null)
				response.sendRedirect("index.jsp?msg=" + response.encodeURL("BDK FAILED to be " + (update ? "updated" : "generated") + "; check logs for details"));
			else
				response.sendRedirect("index.jsp?msg=" + response.encodeURL("BDK has been " + (update ? "updated" : "generated") + ", ID: " + keyInfo.getKeyID() + ", KCV: " + keyInfo.getKCV()));
			return;
		}
	}
%>

<form method="POST">

<%	if(loader.getState() != KeyManagerLoader.STATE_INITIALIZED) { %>
<table width="750">
 <tr>
  <th class="failure">Invalid state for BDK Generation: <%=KeyManagerLoader.STATE_NAMES[loader.getState()]%></th>
 </tr>
</table><br>
<br>
<%	} else { %>
BDK <%=(update ? "UPDATE" : "GENERATION")%>
<table width="750">
<%		SSSS ssss = loader.getSSSS();
		SortedMap<String, String> keyCustodians = masterKeyLoader.getKeyCustodiansInfo();
		SortedMap<String, KeyManagerUser> loaders = masterKeyLoader.getLoadersInfo();
		SortedMap<String, KeyManagerUser> BDKLoaders = masterKeyLoader.getBDKLoadersInfo();
		int loadTimeout = masterKeyLoader.getLoadTimeoutSeconds();
		int loadRemainingSeconds = masterKeyLoader.getLoadRemainingSeconds();
		int bdkShareCount = loader.getBDKShareCount();
		if(loaders.size() == 0) { %>
 <tr>
  <th>
   Generation has not been initiated.<br>
   Upon initiation, at least <%=ssss.getT()%> SA's are required to submit their secret within <%=loadTimeout%> seconds.<br>
   <%=bdkShareCount%> SA's are required to submit their key component within <%=loadTimeout%> seconds.
  </th>
 </tr>
<%		} else { %>
 <tr>
  <th colspan="2">
   <%=loaders.size()%> of at least <%=ssss.getT()%> required secrets are loaded with <%=loadRemainingSeconds%> seconds remaining.<br>
   <%=BDKLoaders.size()%> of <%=bdkShareCount%> required BDK components are loaded with <%=loadRemainingSeconds%> seconds remaining.
  </th>
 </tr>
<%			for(String keyCustodianInfo : keyCustodians.keySet()) { %>
 <tr>
  <td><%=keyCustodianInfo%></td>
  <%=loaders.containsKey(keyCustodianInfo) ? "<th class=\"success\" nowrap>Loaded</th>" : "<th nowrap>Not Loaded</th>" %>
 </tr>
<%			} %>
<%		} %>
</table>
<br>
<%		if(!loaders.containsKey(currentUserInfo)) { %>
<input type="submit" name="process" value="Decrypt My Secret For Generation">
<%		} else { %>
<input type=button value="Refresh" onClick="javascript:window.location=window.location;">
<%		} %>

<%		if(masterKeyLoader.canGenerateBDK(currentUser)) { %>
<input type="submit" name="process" value="<%=(update ? "Update" : "Generate")%> BDK" onclick="this.style.display = 'none'; document.getElementById('loading').style.display = 'inline';">
<span id="loading" style="display:none;">Loading...</span>
<%		} %>
<%	} %>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>

<%@ include file="../footer.jsp" %>
