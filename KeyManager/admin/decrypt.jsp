<%@ include file="../header.jsp" %> 
<%
	String returnPage = request.getParameter("return");
	String param = request.getParameter("param");
	int bdkComponentCount =  masterKeyLoader.getBDKLoadersInfo().size();
	boolean getBDKComponent = returnPage.indexOf("genbdk.jsp") > -1 && bdkComponentCount < loader.getBDKShareCount();
	String bdkComponent = ""; 
	String kcv = "";
	String encryptedBase64, asymmetricCipherID;
	String certThumbprint = currentCertThumbprint; 
	int avasComponentCount =  masterKeyLoader.getAVASLoadersInfo().size();
	boolean getAVASComponent = returnPage.indexOf("genavas.jsp") > -1 && avasComponentCount < loader.getAVASShareCount();
	String avasComponent = "";
	String crc = "";
	
	String action = request.getParameter("action");
	String url = request.getRequestURL().toString();
	String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath();
	
	if (process != null)
	{
	    if (process.equals("changeCert"))
			certThumbprint = param;
	    else if (getBDKComponent && process.equals("genKCV"))
	    {
	        bdkComponent = request.getParameter("bdkComponent");
			if (bdkComponent != null && bdkComponent.length() == 32)
			    kcv = KEKStore.getKCV(Conversions.hexToByteArray(bdkComponent));
			else
			    kcv = "Error";
	    }
	    else if (getAVASComponent && process.equals("genCRC"))
	    {
	        avasComponent = request.getParameter("avasComponent");
			if (avasComponent != null && avasComponent.length() >= 64)
			    crc = String.valueOf(KeyManagerUtil.generateCRC(Conversions.hexToByteArray(avasComponent)));
			else
			    crc = "Error";
	    }
	}
	
	encryptedBase64 = Base64.encodeBytes(masterKeyLoader.getEncryptedSecret(certThumbprint), 0x8);
	asymmetricCipherID = masterKeyLoader.getAsymmetricCipherID();
	
	if(masterKeyLoader == null) { %>
<table width="750">
 <tr>
  <th class="failure">MasterKeyLoader Not Found!</th>
 </tr>
</table>
<%		return;
	} else if(returnPage == null) { %>
<table width="750">
 <tr>
  <th class="failure">Required parameter not found: returnPage</th>
 </tr>
</table>
<%		return;
	} %>
<script type="text/javascript">
  function processInputValues() {
    <%if (getBDKComponent) {%>
    	if (document.kmForm.bdkComponent.value.length < 32)
    	{
    		alert('Invalid BDK component!');
    		return 0;
    	}
    	var kcv;
    	if(document.getElementById('kcv').innerText)
    		kcv = document.getElementById('kcv').innerText;
    	else if(document.getElementById('kcv').textContent)
    		kcv = document.getElementById('kcv').textContent;
    	if (!kcv || kcv.length < 6)
    	{
    		alert('Please generate KCV first!');
    		return 0;
    	}
    	if (!confirm ('Is the KCV (Key Check Value) correct?'))
    		return 0;
    <%}%>
    <%if (getAVASComponent) {%>
	  	if (document.kmForm.avasComponent.value.length < 64)
	  	{
	  		alert('Invalid AVAS component!');
	  		return 0;
	  	}
	  	var crc;
	  	if(document.getElementById('crc').innerText)
	  		crc = document.getElementById('crc').innerText;
	  	else if(document.getElementById('crc').textContent)
	  		crc = document.getElementById('crc').textContent;
	  	if (!crc || crc.length < 1)
	  	{
	  		alert('Please generate CRC first!');
	  		return 0;
	  	}
	  	if (crc == '0')
	  	{
	  		alert('Please enter a valid AVAS component!');
	  		return 0;
	  	}
	  	if (!confirm ('Is the CRC correct?'))
	  		return 0;
  <%}%>
    document.kmForm.submit();
  }
<%if (getBDKComponent) {%>
  function genKCV()
  {
    if (document.kmForm.bdkComponent.value.length < 32)
    {
    	alert('Invalid BDK component!');
    	return 0;
    }  
  	document.kmForm.action = 'decrypt.jsp';
  	document.kmForm.process.value = 'genKCV';
  	document.kmForm.submit();
  }
<%}%>  
<%if (getAVASComponent) {%>
function genCRC()
{
  if (document.kmForm.avasComponent.value.length < 64)
  {
  	alert('Invalid AVAS component!');
  	return 0;
  }  
	document.kmForm.action = 'decrypt.jsp';
	document.kmForm.process.value = 'genCRC';
	document.kmForm.submit();
}
<%}%>
</script>

KEY MANAGER APP
<br> 
<table width="750">
	<tr>
		<td>
		Please copy and paste the values below and click "Decrypt" in Key Manager App.<br>
		If you do not have it already, please download and unzip <a href="KeyManagerApp.zip">Key Manager App</a>. 
		</td>
	</tr>
	<tr>
		<td>
		<textarea rows="2" cols="50" readonly style="width: 99%; font-weight:bold;">
<%=baseURL%>
<%=currentCertThumbprint%></textarea>
		</td>
	</tr>
</table>
<br>
<form name="kmForm" action="<%=returnPage%>" method="post">
<%if (getBDKComponent) {
    if(bdkComponentCount > 0) {
    %>BDK Key ID: <input readonly="readonly" type="text" name="bdkKeyId" size="8" maxlength="32" value="<%=masterKeyLoader.getLoaderBDKKeyID()%>"/> <%
    } else { String keyId = request.getParameter("bdkKeyId"); %>Enter BDK Key ID: <input type="text" name="bdkKeyId" size="8" maxlength="32" value="<%=(keyId == null ? "" : keyId)%>"/> <%
    } %> Enter BDK Component: <input type="text" name="bdkComponent" size="32" maxlength="32" value="<%=bdkComponent%>"/> Component KCV: <span id="kcv"><%=kcv%></span><br><br>
<input type="button" value="Generate KCV" onClick="javascript:genKCV()">
<%}%>
<%if (getAVASComponent) {
    if(avasComponentCount > 0) {
    %>AVAS Key ID: <input readonly="readonly" type="text" name="avasKeyId" size="8" maxlength="32" value="<%=masterKeyLoader.getLoaderAVASKeyID()%>"/><br/><br/> <%
    } else { String keyId = request.getParameter("avasKeyId"); %>Enter AVAS Key ID: <input type="text" name="avasKeyId" size="8" maxlength="32" value="<%=(keyId == null ? "" : keyId)%>"/><br/><br/> <%
    } %> Enter AVAS Component: <input type="text" name="avasComponent" size="70" maxlength="66" value="<%=avasComponent%>"/><br/><br/>Component CRC: <span id="crc"><%=crc%></span><br/><br/>
<input type="button" value="Generate CRC" onClick="javascript:genCRC()">
<%}%>
<input type="button" value="Continue" onClick="javascript:processInputValues()">
<input type="hidden" name="param" value="<%=param%>">
<input type="hidden" name="return" value="<%=returnPage%>">
<input type="hidden" name="process" value="Continue">
</form>
<%@ include file="../footer.jsp" %>
