<%@page import="java.math.BigInteger"%>
<%@ include file="../header.jsp" %>

<%
if(process != null) {
	if (process.equals("Decrypt My Secret For Generation")) {
		response.sendRedirect("decrypt.jsp?return=" + request.getRequestURI());
		return;
	}
	else if (process.equals("Continue")) {
		if (request.getParameter("avasComponent") != null) {
			String tmp = request.getParameter("avasKeyId");
			if(tmp == null || (tmp=tmp.trim()).length() == 0) {
				%><table width="750">
			 <tr>
			  <th class="failure">Required parameter not found: avasKeyId</th>
			 </tr>
			</table><%
		        return;
			}
			long keyID;
			try {
				keyID = Long.parseLong(tmp);
			} catch(NumberFormatException e) {
				%><table width="750">
                 <tr>
                  <th class="failure">Could not parse parameter "avasKeyId" with value "<![CDATA[<%=tmp.replace("]]>", "]]]><![CDATA[]>") %>]]>" to a number</th>
                 </tr>
                </table><%
                    return;
			}

	    
			masterKeyLoader.addAVASComponent(currentUser, Conversions.hexToByteArray(request.getParameter("avasComponent")), keyID);
		}
		response.sendRedirect(request.getRequestURI());
	}
	else if(process.equals("Generate AVAS Key")) {
		KEKInfo keyInfo = masterKeyLoader.load(MasterKeyLoader.ACTION_GENERATE_AVAS_KEY, currentUser, null);
		if(keyInfo == null)
			response.sendRedirect("index.jsp?msg=" + response.encodeURL("AVAS Key FAILED to be generated; check logs for details"));
		else
			response.sendRedirect("index.jsp?msg=" + response.encodeURL("AVAS Key has been generated, Public Key Hex: " + keyInfo.getPublicKeyHex() + ", Public Key ID: " + keyInfo.getPublicKeyID() + ", Public Key CRC: " + keyInfo.getPublicKeyCrc() + ", CRC: " + keyInfo.getCRC()));
		return;
	}
}
%>

<form method="POST">

<%
boolean component = "true".equalsIgnoreCase(request.getParameter("component"));
if (component) {
	BigInteger keyComponent = KeyManagerUtil.generateAVASKeyComponent();
	byte[] keyComponentBytes = keyComponent.toByteArray();
	int crc = KeyManagerUtil.generateCRC(keyComponentBytes);	
%>
<table width="750">
	<tr>
		<td>
		<b>Your AVAS Key Component:</b> <%=Conversions.bytesToHex(keyComponentBytes)%><br/><b>AVAS Key Component CRC:</b>&nbsp;&nbsp;<%=crc%>
		<br/>Please use these values to Generate AVAS Key 
		</td>
	</tr>
</table>
<br/>
<input type=button value="Generate AVAS Key" onClick="javascript:window.location='genavas.jsp';">
<%} else {%>

<%	if(loader.getState() != KeyManagerLoader.STATE_INITIALIZED) { %>
<table width="750">
 <tr>
  <th class="failure">Invalid state for AVAS Generation: <%=KeyManagerLoader.STATE_NAMES[loader.getState()]%></th>
 </tr>
</table><br>
<br>
<%	} else { %>
AVAS KEY GENERATION
<table width="750">
<%		SSSS ssss = loader.getSSSS();
		SortedMap<String, String> keyCustodians = masterKeyLoader.getKeyCustodiansInfo();
		SortedMap<String, KeyManagerUser> loaders = masterKeyLoader.getLoadersInfo();
		SortedMap<String, KeyManagerUser> AVASLoaders = masterKeyLoader.getAVASLoadersInfo();
		int loadTimeout = masterKeyLoader.getLoadTimeoutSeconds();
		int loadRemainingSeconds = masterKeyLoader.getLoadRemainingSeconds();
		int avasShareCount = loader.getAVASShareCount();
		if(loaders.size() == 0) { %>
 <tr>
  <th>
   Generation has not been initiated.<br>
   Upon initiation, at least <%=ssss.getT()%> SA's are required to submit their secret within <%=loadTimeout%> seconds.<br>
   <%=avasShareCount%> SA's are required to submit their key component within <%=loadTimeout%> seconds.
  </th>
 </tr>
<%		} else { %>
 <tr>
  <th colspan="2">
   <%=loaders.size()%> of at least <%=ssss.getT()%> required secrets are loaded with <%=loadRemainingSeconds%> seconds remaining.<br>
   <%=AVASLoaders.size()%> of <%=avasShareCount%> required AVAS components are loaded with <%=loadRemainingSeconds%> seconds remaining.
  </th>
 </tr>
<%			for(String keyCustodianInfo : keyCustodians.keySet()) { %>
 <tr>
  <td><%=keyCustodianInfo%></td>
  <%=loaders.containsKey(keyCustodianInfo) ? "<th class=\"success\" nowrap>Loaded</th>" : "<th nowrap>Not Loaded</th>" %>
 </tr>
<%			} %>
<%		} %>
</table>
<br>
<%		if(!loaders.containsKey(currentUserInfo)) { %>
<input type="submit" name="process" value="Decrypt My Secret For Generation">
<%		} else { %>
<input type=button value="Refresh" onClick="javascript:window.location=window.location;">
<%		} %>

<%		if(masterKeyLoader.canGenerateAVAS(currentUser)) { %>
<input type="submit" name="process" value="Generate AVAS Key" onclick="this.style.display = 'none'; document.getElementById('loading').style.display = 'inline';">
<span id="loading" style="display:none;">Loading...</span>
<%		} %>
<%	} }%>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';"> 
</form>

<%@ include file="../footer.jsp" %>
