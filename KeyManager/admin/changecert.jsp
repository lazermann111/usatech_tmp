<%@ include file="../header.jsp" %>

<%
String url = request.getRequestURL().toString();
String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath();

if(loader.getState() < KeyManagerLoader.STATE_INSTALLED) { %>
<table width="750">
 <tr>
  <th class="failure">Invalid state for certificate change: <%=KeyManagerLoader.STATE_NAMES[loader.getState()]%></th>
 </tr>
 <tr>
  <th align="center"><a href="list_clients.jsp">Client Administration</a></th>
 </tr>
</table>
<% 
	return;
}
%>

KEY MANAGER APP
<br> 
<table width="750">
	<tr>
		<td>
		Please copy and paste the values below and click "Change Certificate" in Key Manager App.<br>
		If you do not have it already, please download and unzip <a href="KeyManagerApp.zip">Key Manager App</a>. 
		</td>
	</tr>
	<tr>
		<td>
		<textarea rows="2" cols="50" readonly style="width: 99%; font-weight:bold;">
<%=baseURL%>
<%=currentCertThumbprint%></textarea>
		</td>
	</tr>
</table>
<br>
<input type=button value="Client Administration" onClick="javascript:window.location='list_clients.jsp';">

<%@ include file="../footer.jsp" %>
