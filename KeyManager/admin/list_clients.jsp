<%@ include file="../header.jsp" %>

<% SortedMap<String, String> keyCustodians = masterKeyLoader.getKeyCustodiansInfo(); %>

<form method="POST">

KEY CUSTODIANS<br>
<table width="750">
<%	if (keyCustodians.size() == 0) { %>
 <tr><td class="td_center">None</td></tr>	
<% 	}
	else
	{		
		for(String keyCustodianInfo : keyCustodians.keySet()) { %>
 <tr>
  <td>
   <a href="edit_client.jsp?process=view&certThumbprint=<%=keyCustodians.get(keyCustodianInfo)%>">
   	<%=keyCustodianInfo%>
   </a>
  </td>
 </tr>
<%		} 
	} %>
</table>
<br>

OTHER CLIENTS<br>
<table width="750">
<%	List<KeyManagerUser> users = KeyManagerUser.getAllUsers();
	if (users.size() == keyCustodians.size()) { %>
 <tr><td class="td_center">None</td></tr>	
<%
	}
	else
	{	
		for(KeyManagerUser user : users) {
			if (keyCustodians.containsKey(user.getUserInfo()))
				continue;
			List<KeyManagerRole> assignedRoles = user.getRoles(); %>
 <tr>
  <td>
   <a href="edit_client.jsp?process=view&certThumbprint=<%=user.getCertThumbprint()%>">
   <%=user.getUserInfo()%>
   </a>
  </td>
 </tr>
<%		}
	} %>
</table>
<br>
<input type=button value="Security Administration" onClick="javascript:window.location='index.jsp';">
<input type=button value="Certificate Changes" onClick="javascript:window.location='changecert.jsp';">
</form>

<%@ include file="../footer.jsp" %>
