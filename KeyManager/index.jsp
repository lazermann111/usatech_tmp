<%@ include file="header.jsp" %>
<%	String msg = request.getParameter("msg");
	String[] roleParams = request.getParameterValues("role");
	
	if(process != null && process.equals("Request Access") && roleParams != null && roleParams.length > 0) {
		
		String rolesStr = Arrays.toString(roleParams);
		
		SMTPConnection mailer = new SMTPConnection(loader.getConfiguration().getString("smtp.server"));
		StringBuilder sb = new StringBuilder();
		sb.append(request.getRequestURL());
		sb.append(Util.LB);
		sb.append(Util.LB);
		sb.append(currentUserInfo);
		sb.append(Util.LB);
		sb.append(Util.LB);
		sb.append("Role(s): ");		
		sb.append(rolesStr);
		mailer.send(loader.getConfiguration().getString("smtp.from"), loader.getConfiguration().getString("smtp.to"), null, "Key Manager Role Request", sb.toString(), null, SMTPConnection.TEXT_FORMAT);
		
		response.sendRedirect(request.getRequestURI()+"?msg=" + URLEncoder.encode("REQUEST SENT TO SECURITY ADMIN"));
		return;
	}
%>
<form method="POST">
YOUR ROLES<br>
<table width="750">
<%	List<KeyManagerRole> assignedRoles = currentUser.getRoles();
	boolean missingSome = false;
	for(KeyManagerRole role : allRoles) {
		if(role.getName().equals(KeyManagerRole.ROLE_VALID_CERT)) continue; %>
 <tr>
  <td class="td_center">
<%		if(!assignedRoles.contains(role)) { 
			missingSome = true; 
%> 
   <input type="checkbox" name="role" value="<%=role.getName() %>"> 
<%		} else { %>
   X
<%		} %>
  </td>
  <td><%=role.getName() %></td>
  <td><%=role.getDesc() %></td>
 </tr>
<%	} %>
</table>
<br>
<%	if(missingSome) { %>
<input type="submit" name="process" value="Request Access" class="button"> 
<%	} %>
<% if(request.isUserInRole(KeyManagerRole.ROLE_SECURITY_ADMIN)) { %>
<input type=button value="Security Administration" onClick="javascript:window.location='admin';"> 
<% } %>
<br>&nbsp;
<%	if(msg != null && msg.length() > 0) { %><br><%=msg%><br>&nbsp;<% } %>
<table>
 <tr>
  <td>
   <pre>

                            NOTICE TO USERS                                
                                                                           
  This computer system is the private property of USA Technologies, Inc.   
                 *** It is for authorized use only. ***                    
      Users (authorized or unauthorized) have no explicit or implicit      
                        expectation of privacy.                            
                                                                           
    Any or all uses of this system and all files on this system may be     
    intercepted, monitored, recorded, copied, audited, inspected, and      
   disclosed to your employer, to authorized site, government, and law     
   enforcement personnel, as well as authorized officials of government    
                    agencies, both domestic and foreign.                   
                                                                           
 By using this system, the user consents to such interception, monitoring, 
     recording, copying, auditing, inspection, and disclosure at the       
 discretion of such personnel or officials.  Unauthorized or improper use  
      of this system may result in civil and criminal penalties and        
administrative or disciplinary action, as appropriate. By continuing to use
   this system you indicate your awareness of and consent to these terms   
   and conditions of use. LOG OFF IMMEDIATELY if you do not agree to the   
                   conditions stated in this warning.</pre></td>
 </tr>
</table>
</form>
<%@ include file="footer.jsp" %>
