After Key Manager installation file paths need to be modified in these files:
- tomcat/common/classes/log4j.properties
- tomcat/webapps/KeyManager/WEB-INF/KeyManagerNode.driver.xml

DBEncrypt cron job:
# 01/07/2008 - dkouznetsov - DBEncrypt
0,5,10,15,20,25,30,35,40,45,50,55 * * * * /usr/bin/sh /opt/keymgr/dbencrypt/encrypt_db

Notes:
- SA certificate should have Purpose: Signature and Encryption (Key Usage: Digital Signature and Key Encipherment)
- Firefox crashing on java applets:
	- rename C:\Program Files\Mozilla Firefox\plugins\npoji600dll to npoji600dll.bak
	- copy C:\java\jdk1.6.0_01\jre\bin\npoji610.dll to C:\Program Files\Mozilla Firefox\plugins

KeyManager release 1.3.0 changes:

- installed and configured new USA Technologies Token CA (subordinate to USA Technologies Root CA) for smart card and Key Manager service account certificates
- functionality to change and renew key custodian certificates
- ability to change master key and re-split it between N key custodians (only T out of N key custodians are required for master key changes)
- extended user certificate properties on website: subject, thumbprint, valid from, valid to, serial #, key algorithm and size, key usage
- key custodians in Client Administration
- prohibiting deletion of key custodian user accounts and restricting SA role to certificates with Key Encipherment key usage
- restricted access to Key Manager install, load, KEK generation, master key changes to key custodians
- changed Key Manager user identifiers from keyIdentifierBase64 (public key hash in base64) to thumbprint (certificate SHA1 hash in hex)
- storing certificate for each user in base64
- changed secret store map key from certificate subjectDN to thumbprint to uniquely identify key custodians and added storing of certificates in secret store
- replaced JSP exceptions with user-friendly server and javascript error messages
- signed applet.jar with USAT Code Signing certificate
- internal code and user interface cleanup and optimizations

KeyManagerClient release 1.1.0 changes:
- changed KeyManager encryption padding from PKCS5PADDING to NOPADDING and added null padding to encrypt/decrypt methods
- added configurable list of hashed columns to only hash columns that need to be hashed
- added min and max primary key constraints to keep encrypting data via a scheduled job
- added waitBeforeExitTimeMS configurable parameter to decrease the load on the target database
- updated buzzsql.jar to the latest version (1.3.8)
