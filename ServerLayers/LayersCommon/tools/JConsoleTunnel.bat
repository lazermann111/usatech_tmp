REM start an ssh socks proxy connection to remote server with the command: ssh -fN -D 55555 user@server
REM 55555 above can be any unused port on your workstation, user is your username, server is the name of the server you are connecting to
REM after launching JConsole, log on to it like you normally would, except that you use localhost:<jmx port> instead of <server>:<jmx port>
REM this can be added for SSL debugging: -J-Djavax.net.debug=ssl:handshake:verbose:keymanager:trustmanager
jconsole.exe -J-Djavax.net.ssl.trustStore=truststore.ts -J-Djavax.net.ssl.trustStorePassword=usatech -J-Djava.util.logging.config.file=logging.properties -J-DsocksProxyHost=localhost -J-DsocksProxyPort=55555 -J-DsocksNonProxyHosts=
