@echo Creating Virtual Disk N:
@echo off
sc config imdisk start= auto
net start imdisk
imdisk -a -t vm -s 1024m -m n:
format n: /q /Y
call mkdir N:\logs
label n: RAMDISK
