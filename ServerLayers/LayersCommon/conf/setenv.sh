#!/bin/sh
echo "SET ENV OPTIONS..."
JAVA_OPTS="-Dsun.net.inetaddr.ttl=86400 -Xmx512m -XX:MaxPermSize=128M -Djava.util.logging.config.file=/opt/USAT/conf/logging.properties"
JAVA_HOME=/usr/jdk/latest
export JAVA_OPTS
export JAVA_HOME
