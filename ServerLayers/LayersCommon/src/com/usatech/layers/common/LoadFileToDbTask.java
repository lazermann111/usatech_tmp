package com.usatech.layers.common;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DeflaterInputStream;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.StringUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class LoadFileToDbTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected static final char[] DIRECTORY_SEPARATORS = "/\\".toCharArray();

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ResourceFolder rf = getResourceFolder();
		if(rf == null)
			throw new ServiceException("ResourceFolder property is not set on " + this);
		String[] resourceKeys;
		Long[] modificationTimes;
		String[] filePaths;
		String host;
		int fileType;
		boolean compress;
		try {
			resourceKeys = ConvertUtils.convertToStringArrayNoParse(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_RESOURCE, Object.class, true));
			modificationTimes = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, Long[].class, false);
			filePaths = ConvertUtils.convertToStringArrayNoParse(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_FILE_PATH, Object.class, false));
			host = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_HOST, String.class, false);
			fileType = taskInfo.getStep().getAttributeDefault(CommonAttrEnum.ATTR_FILE_TYPE, Integer.class, -1);
			compress = taskInfo.getStep().getAttributeDefault(CommonAttrEnum.ATTR_COMPRESS, Boolean.class, Boolean.FALSE);
		} catch(AttributeConversionException | ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not get convert attributes", e, WorkRetryType.NO_RETRY);
		}
		boolean okay = false;
		try {
			Connection conn = DataLayerMgr.getConnection("OPER");
			try {
				long[] fileIds = new long[resourceKeys.length];
				for(int i = 0; i < resourceKeys.length; i++) {
					String filePath = (filePaths == null || i >= filePaths.length ? null : filePaths[i]);
					Long modificationTime = (modificationTimes == null || i >= modificationTimes.length ? null : modificationTimes[i]);
					Resource resource;
					try {
						resource = rf.getResource(resourceKeys[i], ResourceMode.READ);
					} catch(IOException e) {
						if(!rf.isAvailable(ResourceMode.READ)) {
							throw new RetrySpecifiedServiceException("Could not get resource at '" + resourceKeys[i] + "' for uploading file", e, WorkRetryType.NONBLOCKING_RETRY);
						}
						throw new ServiceException("Could not get resource at '" + resourceKeys[i] + "' for uploading file", e);
					}
					try {
						if(StringUtils.isBlank(filePath))
							filePath = resource.getName();
						int pos = StringUtils.lastIndexOf(filePath, DIRECTORY_SEPARATORS);
						String fileDir;
						String fileName;
						if(pos < 0) {
							fileDir = null;
							fileName = filePath;
						} else {
							fileDir = filePath.substring(0, pos);
							fileName = filePath.substring(pos + 1);
						}
						String fileSource = IOUtils.getFullPath(host, fileDir);
						InputStream in = resource.getInputStream();
						if(compress)
							in = new DeflaterInputStream(in);
						try {
							fileIds[i] = uploadFile(conn, fileName, fileSource, fileType, modificationTime, in);
						} finally {
							in.close();
						}
						log.info("Uploaded " + fileType + " '" + filePath + "' to database");
					} finally {
						resource.release();
					}
				}
				taskInfo.getStep().setResultAttribute(CommonAttrEnum.ATTR_FILE_ID, fileIds.length == 0 ? fileIds[0] : fileIds);
				okay = true;
			} finally {
				DbUtils.commitOrRollback(conn, okay, true);
			}
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException | IOException | ConvertException e) {
			throw new ServiceException("Could not upload file", e);
		}
		return 0;
	}

	/**
	 * @throws ServiceException
	 * @throws IOException
	 * @throws ConvertException
	 */
	protected long uploadFile(Connection conn, String fileName, String fileSource, int fileType, long modificationTime, InputStream content) throws ServiceException, IOException, SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<>();
		params.put("fileContent", content);
		params.put("modificationTime", modificationTime);
		params.put("fileName", fileName);
		params.put("fileSource", fileSource);
		params.put("fileType", fileType);
		DataLayerMgr.executeCall(conn, "UPLOAD_FILE", params);
		return ConvertUtils.getLong(params.get("fileId"));
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
