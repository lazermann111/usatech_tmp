/**
 *
 */
package com.usatech.layers.common;

import simple.text.StringUtils;

import com.usatech.layers.common.constants.CRCType;

/**
 * @author Brian S. Krug
 *
 */
public class CRC {
	protected final CRCType crcType;
	protected final byte[] crcValue;

	public CRC(CRCType crcType, byte[] crcValue) {
		super();
		this.crcType = crcType;
		this.crcValue = crcValue;
	}
	public CRCType getCrcType() {
		return crcType;
	}
	public byte[] getCrcValue() {
		return crcValue;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return crcType.toString() + " [0x" + StringUtils.toHex(crcValue) + "]";
	}
}
