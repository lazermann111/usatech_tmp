package com.usatech.layers.common;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;
import simple.io.logging.AbstractPrefixedLog;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageDirection;

public abstract class AbstractAppLayerMessage {
	private static final Log log = Log.getLog();
	protected final MessageChainTaskInfo taskInfo;
	protected final String deviceName;
	protected final byte protocol;
	protected final String globalSessionCode;
	protected final DeviceType deviceType;
	protected final long startTime;
	protected final long serverTime;
	protected final long sessionStartTime;
	protected final MessageData data;
	protected final Log messageLog;
	protected final Map<String, Object> resultAttributes;
	
	public AbstractAppLayerMessage(final MessageChainTaskInfo taskInfo) throws ServiceException, AttributeConversionException {
		this.taskInfo = taskInfo;
		MessageChainStep step = taskInfo.getStep();
		deviceName = step.getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
		protocol = step.getAttributeDefault(MessageAttrEnum.ATTR_PROTOCOL, Byte.class, (byte) -1);
		globalSessionCode = step.getAttribute(MessageAttrEnum.ATTR_GLOBAL_SESSION_CODE, String.class, true);
		deviceType = step.getAttribute(DeviceInfoProperty.DEVICE_TYPE, DeviceType.class, true);
		startTime = System.currentTimeMillis();
		serverTime = step.getAttributeDefault(MessageAttrEnum.ATTR_SERVER_TIME, Long.class, startTime);
		sessionStartTime = step.getAttributeDefault(MessageAttrEnum.ATTR_SESSION_START_TIME, Long.class, startTime);
		try {
			final ByteBuffer readBuffer = ByteBuffer.wrap(step.getAttribute(MessageAttrEnum.ATTR_DATA, byte[].class, true));
			data = MessageDataFactory.readMessage(readBuffer, MessageDirection.CLIENT_TO_SERVER, deviceType);
		} catch(BufferUnderflowException e) {
			throw new RetrySpecifiedServiceException("Invalid message", e, WorkRetryType.NO_RETRY);
		}
		final StringBuilder sb = new StringBuilder();
		sb.append("'").append(taskInfo.getMessageChainGuid()).append("': [").append(deviceName).append(" @ ").append(globalSessionCode).append(" #").append(data.getMessageNumber() & 0xFF).append("] ");
		sb.append(data.getMessageType().getDescription()).append(": ");
		final String prefix = sb.toString();
		messageLog = new AbstractPrefixedLog(log) {
			protected Object prefixMessage(Object message) {
				return prefix + message;
			}
		};
		resultAttributes = taskInfo.getStep().getResultAttributes();
		if(messageLog.isInfoEnabled()) {
			sb.setLength(0);
			sb.append("Started message [received ").append(startTime - serverTime).append(" ms ago] ").append(data);
			messageLog.info(sb.toString());
		}
	}

	public Log getLog() {
		return messageLog;
	}

	public Object getSessionAttribute(String name) {
		String key = ProcessingConstants.SESSION_ATTRIBUTE_PREFIX + name;
		if(resultAttributes.containsKey(key))
			return resultAttributes.get(key);
		return taskInfo.getStep().getAttributes().get(key);
	}

	public long getServerTime() {
		return serverTime;
	}

	public byte getProtocolId() {
		return protocol;
	}

	public String getGlobalSessionCode() {
		return globalSessionCode;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public MessageData getData() {
		return data;
	}

	public long getSessionStartTime() {
		return sessionStartTime;
	}
}