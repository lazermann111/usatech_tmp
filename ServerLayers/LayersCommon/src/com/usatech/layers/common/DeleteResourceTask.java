package com.usatech.layers.common;

import java.io.IOException;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class DeleteResourceTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ResourceFolder rf = getResourceFolder();
		if(rf == null)
			throw new ServiceException("ResourceFolder property is not set on " + this);
		Object resourceKeys;
		try {
			resourceKeys = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_RESOURCE, Object.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Resource key not provided", e, WorkRetryType.NO_RETRY);
		}
		if(ConvertUtils.isCollectionType(resourceKeys.getClass())) {
			String[] resourceKeyArray;
			try {
				resourceKeyArray = ConvertUtils.convert(String[].class, resourceKeys);
			} catch(ConvertException e) {
				throw new RetrySpecifiedServiceException("Could not convert resource key attribute", e, WorkRetryType.NO_RETRY);
			}
			for(String resourceKey : resourceKeyArray)
				deleteResource(rf, resourceKey);
		} else {
			String resourceKey;
			try {
				resourceKey = ConvertUtils.convert(String.class, resourceKeys);
			} catch(ConvertException e) {
				throw new RetrySpecifiedServiceException("Could not convert resource key attribute", e, WorkRetryType.NO_RETRY);
			}
			deleteResource(rf, resourceKey);
		}
		return 0;
	}
	
	protected void deleteResource(ResourceFolder rf, String resourceKey) throws ServiceException {
		Resource resource;
		try {
			resource = rf.getResource(resourceKey, ResourceMode.DELETE);
		} catch(IOException e) {
			if(!rf.isAvailable(ResourceMode.DELETE)) {
				throw new RetrySpecifiedServiceException("Could not get resource at '" + resourceKey + "' for deleting", e, WorkRetryType.NONBLOCKING_RETRY);
			}
			throw new ServiceException("Could not get resource at '" + resourceKey + "' for deleting", e);
		}
		try {
			if(!resource.delete())
				log.warn("Could not delete resource file '" + resourceKey + "'");
			else
				log.info("Deleted resource file '" + resourceKey + "'");
		} finally {
			resource.release();
		}
	}

	public static void requestResourceDeletion(Resource resource, Publisher<ByteInput> publisher) throws ServiceException {
		requestResourceDeletion(resource.getKey(), publisher);
	}

	public static void requestResourceDeletion(String resourceKey, Publisher<ByteInput> publisher) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.file.resource.delete");
		step.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resourceKey);
		MessageChainService.publish(mc, publisher);
	}
	
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
