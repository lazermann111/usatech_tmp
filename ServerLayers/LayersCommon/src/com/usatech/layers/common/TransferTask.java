package com.usatech.layers.common;

import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.lang.Decision;
import simple.text.StringUtils;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class TransferTask implements MessageChainTask {
	protected static final int PREFIX_LENGTH = "referencedStep.".length();
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {		
		final MessageChainStep step = taskInfo.getStep();
		MessageChainStep[] mcss = step.getNextSteps(0);
		if(mcss != null && mcss.length > 0 && mcss[0] != null) {
			final MessageChainStep nextStep = mcss[0];
			final Map<String, Object> attributes = step.getAttributes();
			step.copyAttributes(nextStep, new Decision<String>() {
				public boolean decide(String argument) {
					if(argument.startsWith("referencedStep.")) {
						int stepIndex = ConvertUtils.getIntSafely(attributes.get(argument), -1);
						if(stepIndex > 0) {
							String attribute = ConvertUtils.getStringSafely(attributes.get("referencedAttribute." + argument.substring(PREFIX_LENGTH)), null);
							if(!StringUtils.isBlank(attribute)) {
								MessageChainStep refStep = step.getMessageChain().getStep(stepIndex);
								if(refStep != null)
									refStep.getResultAttributes().put(attribute, attributes.get("transferValue." + argument.substring(PREFIX_LENGTH)));
							}
						}
					} else if(argument.startsWith("referencedAttribute.") || argument.startsWith("transferValue."))
						return false;
					return true;
				}
			});
		}
		return 0;
	}

}
