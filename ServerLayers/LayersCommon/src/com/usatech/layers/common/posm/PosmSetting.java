package com.usatech.layers.common.posm;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;


/**
 * This enum defines posm settings (in PSS.POSM_SETTING table)
 *
 * @author bkrug
 *
 */
public enum PosmSetting {
	SETTLE_PROCESSING_ENABLED("SETTLE_PROCESSING_ENABLED", "Y/N flag on whether settlement processing is enabled"),
	TRAN_PROCESSING_ENABLED("TRAN_PROCESSING_ENABLED", "Y/N flag on whether transaction processing is enabled"),
    ;
    private String value;
    private String description;
    private static final EnumStringValueLookup<PosmSetting> lookup = new EnumStringValueLookup<PosmSetting>(PosmSetting.class);

    /**
     * Internal constructor for the enum type.
     *
     * @param value lower case name of the command
     * @param arguments String[] of arguments
     * @param description description of the command
     */
    private PosmSetting(String value, String description) {
        this.value = value;
        this.description = description;
    }

	/**
	 * Get the command's name.
	 *
	 * @return the lower case form of the command name
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Get the description of the command.
	 *
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get the corresponding AdminCommand from value.
	 *
	 * @param value
	 *            the command's id
	 * @return an AdminCommand corresponding to the id
	 * @throws InvalidValueException
	 */
	public static PosmSetting getByValue(String value) throws InvalidValueException {
		return lookup.getByValue(value);
	}
}
