package com.usatech.layers.common.posm;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;


/**
 * This enum defines types of administrative commands.
 *
 * @author Evan
 *
 */
public enum AdminCommandType {
	SETTLE_ALL(1, "Settle all batches now."),
	RETRY_ALL(2, "Retry all transactions/settlements now."),
	RETRY_SETTLE(3, "Retry a specific settlement now."),
    RETRY_TRAN(4, "Retry a specific transaction now."),
    FORCE_TRAN(5, "Force a settlement of a specific Transaction."),
    FORCE_SETTLE(6, "Force a settlement of a specific batch."),
    ERROR_TRAN(7, "Mark a specific transaction as error complete."),
    SETTLE_BATCH(8, "Settle a specific batch now."),
	;
    private int value;
    private String description;
    private static final EnumIntValueLookup<AdminCommandType> lookup = new EnumIntValueLookup<AdminCommandType>(AdminCommandType.class);

    /**
     * Internal constructor for the enum type.
     *
     * @param value lower case name of the command
     * @param arguments String[] of arguments
     * @param description description of the command
     */
    private AdminCommandType(int value, String description) {
        this.value = value;
        this.description = description;
    }

	/**
	 * Get the command's name.
	 *
	 * @return the lower case form of the command name
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Get the description of the command.
	 *
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get the corresponding AdminCommand from value.
	 *
	 * @param value
	 *            the command's id
	 * @return an AdminCommand corresponding to the id
	 * @throws InvalidIntValueException
	 */
	public static AdminCommandType getByValue(int value) throws InvalidIntValueException {
		return lookup.getByValue(value);
	}
}
