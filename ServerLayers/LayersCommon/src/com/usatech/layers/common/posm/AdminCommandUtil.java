/**
 *
 */
package com.usatech.layers.common.posm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;

/**
 * @author Brian S. Krug
 *
 */
public class AdminCommandUtil {
	private static Log log = Log.getLog();
	protected static String posmCheckQueue = "usat.posm.terminal.check";
	protected static final BasicQoS posmCheckQueueQos = new BasicQoS(300000, 5, false, null);
	protected static Publisher<ByteInput> publisher;

	public static void disableSettlementProcessing() throws ServiceException {
		updatePosmSetting(PosmSetting.SETTLE_PROCESSING_ENABLED, 'N');
	}
	public static void enableSettlementProcessing() throws ServiceException {
		updatePosmSetting(PosmSetting.SETTLE_PROCESSING_ENABLED, 'Y');
	}
	public static void disableTransactionProcessing() throws ServiceException {
		updatePosmSetting(PosmSetting.TRAN_PROCESSING_ENABLED, 'N');
	}
	public static void enableTransactionProcessing() throws ServiceException {
		updatePosmSetting(PosmSetting.TRAN_PROCESSING_ENABLED, 'Y');
	}
	protected static void updatePosmSetting(PosmSetting posmSetting, Object value) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("posmSettingName", posmSetting);
		params.put("posmSettingValue", value);
		try {
			int rows = DataLayerMgr.executeUpdate("UPDATE_POSM_SETTING", params, true);
			if(rows != 1)
				throw new ServiceException("Unexpected number of rows updated in PSS.POSM_SETTING: " + rows);
		} catch(SQLException e) {
			throw new ServiceException("Could not update posm setting", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update posm setting", e);
		}
	}
	public static void forceSettlement(long terminalBatchId, String forceReason, String requestedBy, int priority) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("terminalBatchId", terminalBatchId);
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		try {
			DataLayerMgr.selectInto("GET_TERMINAL_FROM_BATCH", params);
			paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
			paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
		} catch(SQLException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		boolean okay = true;
		try {
			params.put("adminCmdTypeId", AdminCommandType.FORCE_SETTLE);
			params.put("requestedBy", requestedBy);
			params.put("priority", priority);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
			params.put("paramName", "TERMINAL_BATCH_ID");
			params.put("paramValue", terminalBatchId);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			params.put("paramName", "FORCE_REASON");
			params.put("paramValue", forceReason);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
	}
	public static void forceTransaction(long tranId, String forceReason, String requestedBy, int priority) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		try {
			DataLayerMgr.selectInto("GET_TERMINAL_FROM_TRAN", params);
			paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
			paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
		} catch(SQLException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		boolean okay = true;
		try {
			params.put("adminCmdTypeId", AdminCommandType.FORCE_TRAN);
			params.put("requestedBy", requestedBy);
			params.put("priority", priority);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
			params.put("paramName", "TRAN_ID");
			params.put("paramValue", tranId);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			params.put("paramName", "FORCE_REASON");
			params.put("paramValue", forceReason);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
	}

	public static void errorTransaction(long tranId, String errorReason, String requestedBy, int priority) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		try {
			DataLayerMgr.selectInto("GET_TERMINAL_FROM_TRAN", params);
			paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
			paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
		} catch(SQLException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		boolean okay = true;
		try {
			params.put("adminCmdTypeId", AdminCommandType.ERROR_TRAN);
			params.put("requestedBy", requestedBy);
			params.put("priority", priority);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
			params.put("paramName", "TRAN_ID");
			params.put("paramValue", tranId);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			params.put("paramName", "ERROR_REASON");
			params.put("paramValue", errorReason);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
	}
	public static void retrySettlement(long terminalBatchId, String requestedBy, int priority) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("terminalBatchId", terminalBatchId);
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		try {
			DataLayerMgr.selectInto("GET_TERMINAL_FROM_BATCH", params);
			paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
			paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
		} catch(SQLException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		boolean okay = true;
		try {
			params.put("adminCmdTypeId", AdminCommandType.RETRY_SETTLE);
			params.put("requestedBy", requestedBy);
			params.put("priority", priority);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
			params.put("paramName", "TERMINAL_BATCH_ID");
			params.put("paramValue", terminalBatchId);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
	}
	public static void retryTransaction(long tranId, String requestedBy, int priority) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		try {
			DataLayerMgr.selectInto("GET_TERMINAL_FROM_TRAN", params);
			paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
			paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
		} catch(SQLException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		boolean okay = true;
		try {
			params.put("adminCmdTypeId", AdminCommandType.RETRY_TRAN);
			params.put("requestedBy", requestedBy);
			params.put("priority", priority);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
			params.put("paramName", "TRAN_ID");
			params.put("paramValue", tranId);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
	}

	public static void settleBatch(long terminalBatchId, String requestedBy, int priority) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("terminalBatchId", terminalBatchId);
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		char terminalBatchClosed;
		try {
			DataLayerMgr.selectInto("GET_TERMINAL_FROM_BATCH", params);
			paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
			paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
			terminalBatchClosed = ConvertUtils.convertRequired(Character.class, params.get("terminalBatchClosed"));
		} catch(NotEnoughRowsException e) {
			throw new ServiceException("Terminal Batch " + terminalBatchId + " does not exist", e);
		} catch(SQLException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not get terminal batch details", e);
		}
		if(terminalBatchClosed != 'N')
			throw new ServiceException("Terminal Batch " + terminalBatchId + " is already closed");
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		boolean okay = true;
		try {
			params.put("adminCmdTypeId", AdminCommandType.SETTLE_BATCH);
			params.put("requestedBy", requestedBy);
			params.put("priority", priority);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
			params.put("paramName", "TERMINAL_BATCH_ID");
			params.put("paramValue", terminalBatchId);
			DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD_PARAM", params);
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
	}
	public static int settleAll(String requestedBy, int priority) throws ServiceException {
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_OPEN_TERMINAL_BATCHES", null);
		} catch(SQLException e) {
			throw new ServiceException("Could not get open terminal batches", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get open terminal batches", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		int cnt = 0;
		boolean okay = true;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			while(results.next()) {
				long paymentSubtypeKeyId = ConvertUtils.getLong(results.getValue("paymentSubtypeKeyId"));
				String paymentSubtypeClass = ConvertUtils.getString(results.getValue("paymentSubtypeClass"), true);
				params.put("paymentSubtypeKeyId", paymentSubtypeKeyId);
				params.put("paymentSubtypeClass", paymentSubtypeClass);
				params.put("adminCmdTypeId", AdminCommandType.SETTLE_ALL);
				params.put("requestedBy", requestedBy);
				params.put("priority", priority);
				DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
				checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
				cnt++;
			}
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(ConvertException e) {
			okay = false;
			throw new ServiceException("Could not convert terminal info", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		return cnt;
	}

	public static int checkAll() throws ServiceException {
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_PENDING_TERMINALS", null);
		} catch(SQLException e) {
			throw new ServiceException("Could not get pending terminals", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get pending terminals", e);
		}
		int cnt = 0;
		try {
			while(results.next()) {
				long paymentSubtypeKeyId = ConvertUtils.getLong(results.getValue("paymentSubtypeKeyId"));
				String paymentSubtypeClass = ConvertUtils.getString(results.getValue("paymentSubtypeClass"), true);
				checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
				cnt++;
			}
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert terminal info", e);
		} 
		return cnt;
	}

	public static int retryAll(String requestedBy, int priority) throws ServiceException {
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_RETRYABLE_TERMINAL_BATCHES", null);
		} catch(SQLException e) {
			throw new ServiceException("Could not get open terminal batches", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get open terminal batches", e);
		}
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall("ADD_ADMIN_CMD");
		} catch(SQLException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get a connection to insert the admin command", e);
		}
		int cnt = 0;
		boolean okay = true;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			while(results.next()) {
				long paymentSubtypeKeyId = ConvertUtils.getLong(results.getValue("paymentSubtypeKeyId"));
				String paymentSubtypeClass = ConvertUtils.getString(results.getValue("paymentSubtypeClass"), true);
				params.put("paymentSubtypeKeyId", paymentSubtypeKeyId);
				params.put("paymentSubtypeClass", paymentSubtypeClass);
				params.put("adminCmdTypeId", AdminCommandType.RETRY_ALL);
				params.put("requestedBy", requestedBy);
				params.put("priority", priority);
				DataLayerMgr.executeCall(conn, "ADD_ADMIN_CMD", params);
				checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
				cnt++;
			}
			conn.commit();
		} catch(SQLException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(DataLayerException e) {
			okay = false;
			throw new ServiceException("Could not insert admin command", e);
		} catch(ConvertException e) {
			okay = false;
			throw new ServiceException("Could not convert terminal info", e);
		} finally {
			if(!okay)
				try { conn.rollback(); } catch(SQLException e) { log.info("Could not rollback connection: " + e.getMessage()); }
			try { conn.close(); } catch(SQLException e) { log.info("Could not close connection: " + e.getMessage()); }
		}
		return cnt;
	}
	public static void checkTerminal(long paymentSubtypeKeyId, String paymentSubtypeClass) throws ServiceException {
		if (publisher == null) {
			log.info("Publisher is not configured, not checking terminal");
			return;
		}
		MessageChain messageChain = new MessageChainV11();
        MessageChainStep checkStep = messageChain.addStep(getPosmCheckQueue());
        checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
        checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubtypeClass);
        MessageChainService.publish(messageChain, publisher, null, getPosmCheckQueueQos());
	}
	public static String getPosmCheckQueue() {
		return posmCheckQueue;
	}
	public static void setPosmCheckQueue(String posmCheckQueue) {
		AdminCommandUtil.posmCheckQueue = posmCheckQueue;
	}
	
	public static BasicQoS getPosmCheckQueueQos() {
		return posmCheckQueueQos;
	}
	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	public static void setPublisher(Publisher<ByteInput> publisher) {
		AdminCommandUtil.publisher = publisher;
	}
}
