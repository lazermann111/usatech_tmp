package com.usatech.layers.common.process;

import java.rmi.RemoteException;

import javax.management.MXBean;

@MXBean(true)
public interface ProcessRequestTaskMXBean {
	public int processRequest(long processId) throws RemoteException;
}