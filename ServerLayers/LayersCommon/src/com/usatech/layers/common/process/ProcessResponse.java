package com.usatech.layers.common.process;

import simple.app.ServiceException;

public interface ProcessResponse {
	public void appendResponseText(String text) throws ServiceException;

	public String getResponseSummary();

	public void setResponseSummary(String summary);

	public String getEmailSubject();

	public void setEmailSubject(String emailSubject);

	public String getEmailFromName();

	public void setEmailFromName(String emailFromName);

	public String getEmailFromAddress();

	public void setEmailFromAddress(String emailFromAddress);

	public String getEmailHeader();

	public void setEmailHeader(String emailHeader);

	public String getEmailFooter();

	public void setEmailFooter(String emailFooter);

	public String getEmailMiddle();

	public void setEmailMiddle(String emailMiddle);

	public boolean isEmailSummaryFirst();

	public void setEmailSummaryFirst(boolean emailSummaryFirst);
}
