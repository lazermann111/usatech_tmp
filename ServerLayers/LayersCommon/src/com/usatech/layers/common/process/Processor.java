package com.usatech.layers.common.process;

import java.io.InputStream;

import simple.app.ServiceException;
import simple.servlet.RecordRequestFilter.CallInputs;

public interface Processor {
	public void processRequest(long requestId, long profileId, long userId, CallInputs ci, InputStream content, final ProcessResponse response, long lastProcessedLine, long updatedCount) throws ServiceException;
}
