package com.usatech.layers.common.process;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.sql.rowset.serial.SerialBlob;

import simple.app.DatabasePrerequisite;
import simple.app.DialectResolver;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.text.StringUtils;
import simple.text.StringUtils.EncodedHandler;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class ProcessRequestTask implements MessageChainTask, ProcessRequestTaskMXBean {
	private static final Log log = Log.getLog();
	protected final ConcurrentMap<ProcessType, Processor> processors = new ConcurrentHashMap<>();
	protected String appCd;

	public ProcessRequestTask() {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		try {
			mbs.registerMBean(this, new ObjectName("AppJobs:Name=ProcessRequest"));
		} catch(InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException | MalformedObjectNameException e) {
			log.error("Could not register MXBean", e);
		}
	}
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long processId;
		try {
			processId = taskInfo.getStep().getAttributeDefault(CommonAttrEnum.ATTR_PROCESS_ID, Long.class, 0L);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		return process(processId);
	}

	/* (non-Javadoc)
	 * @see com.usatech.report.ProcessRequestTaskMXBean#processRequest(long)
	 */
	public int processRequest(long processId) throws RemoteException {
		try {
			return process(processId);
		} catch(ServiceException e) {
			throw new RemoteException(e.getMessage(), e.getCause() != null ? e.getCause() : e);
		}
	}

	protected int process(long processId) throws ServiceException {
		try {
			Map<String, Object> params = new HashMap<>();
			Processor p;
			ProcessResponse pr;
			long userId;
			ProcessType processType;
			String processTypeDesc;
			String notifyEmail;
			Blob processParams;
			Blob processContent;
			long processRequestId;
			long lastProcessedLine;
			long updatedCount;
			long retryCount;
			long retryProcessRequestMax;
			long profileId;
			params.put("processorCd", appCd);
			params.put("processId", processId);
			while(true) {
				DataLayerMgr.selectInto("GET_RETRY_PROCESS_REQUEST_MAX", params);
				retryProcessRequestMax = ConvertUtils.getLongSafely(params.get("retryProcessRequestMax"), 5);
				DataLayerMgr.executeCall("NEXT_PROCESS_REQUEST", params, true);
				boolean okay = false;
				try {
					String processStatus = ConvertUtils.getString(params.get("processStatus"), false);
					if(StringUtils.isBlank(processStatus)) {
						log.info("No pending process requests for processor " + appCd + " were found; exitting");
						okay = true;
						return 0;
					}
					processRequestId = ConvertUtils.getLong(params.get("processRequestId"));
					profileId = ConvertUtils.getLong(params.get("profileId"));
					retryCount = ConvertUtils.getLong(params.get("retryCount"));
					processType = ConvertUtils.convertRequired(ProcessType.class, params.get("processType"));
					processTypeDesc = processType.getDescription();
					switch(processStatus) {
						case "I":
							log.warn("Another thread is processing " + processTypeDesc + " + request " + processRequestId + " for user " + profileId + "; exitting");
							okay = true;
							return 1;
						case "P":
							log.info("Starting processing of " + processTypeDesc + " request " + processRequestId + " for user " + profileId);
							break;
						case "R":
							log.info("Starting processing of " + processTypeDesc + " request " + processRequestId + " (Retry) for user " + profileId);
							retryCount++;
							break;
						default:
							throw new ServiceException("Invalid process status '" + processStatus + "'");
					}
					userId = ConvertUtils.getLong(params.get("userId"));
					notifyEmail = ConvertUtils.getString(params.get("notifyEmail"), false);
					if (DialectResolver.isOracle()) {
						processParams = ConvertUtils.convertRequired(Blob.class, params.get("processParams"));
						processContent = ConvertUtils.convertRequired(Blob.class, params.get("processContent"));
					} else {
						byte [] processParamsBytes = ConvertUtils.convertRequired(byte[].class, params.get("processParams"));
						processParams = new SerialBlob(processParamsBytes);
						byte [] processContentBytes = ConvertUtils.convertRequired(byte[].class, params.get("processContent"));
						processContent = new SerialBlob(processContentBytes);
					}
						
					lastProcessedLine = ConvertUtils.getLong(params.get("lastProcessedLine"));
					updatedCount = ConvertUtils.getLong(params.get("updatedCount"));

					try {
						p = getProcessor(processType);
					} catch(InstantiationException | IllegalAccessException | ClassNotFoundException e) {
						throw new RetrySpecifiedServiceException("Processor for " + processTypeDesc + " could not be created", e, WorkRetryType.NO_RETRY);
					}
					pr = new SavingProcessResponse(processRequestId, "UPDATE_PROCESS_RESPONSE");
					okay = true;
				} finally {
					if(!okay) {
						// mark as pending
						DataLayerMgr.executeUpdate("RESET_PROCESS_REQUEST", params, true);
					}
				}

				HashMap<String, Object> contentParams = new HashMap<String, Object>();
				contentParams.put("processRequestId", processRequestId);
				CallInputs ci = null;
				PROCESS_REQUEST: while(true) {
					try {
						if (ci == null) {
							ci = new CallInputs();
							Reader reader = new InputStreamReader(processParams.getBinaryStream());
							try {
							readEncoded(ci, reader);
							} catch(IOException e) {
								throw new ServiceException(e);
							}
						}
						p.processRequest(processRequestId, profileId, userId, ci, processContent.getBinaryStream(), pr, lastProcessedLine, updatedCount);
						break;
					} catch(ServiceException | RuntimeException | Error e) {
						if (e.getMessage().indexOf("Closed Connection") > -1) {
							log.warn("Closed Connection while processing " + processTypeDesc + " request " + processRequestId + ", opening new connection and proceeding", e);
							DataLayerMgr.executeCall("GET_PROCESS_CONTENT", contentParams, true);
							processContent = ConvertUtils.convertRequired(Blob.class, contentParams.get("processContent"));
							lastProcessedLine = ConvertUtils.getLong(contentParams.get("lastProcessedLine"));
							updatedCount = ConvertUtils.getLong(contentParams.get("updatedCount"));
							continue PROCESS_REQUEST;
						}

						// mark as retry
						if(retryCount == retryProcessRequestMax){
							DataLayerMgr.executeUpdate("SET_PROCESS_REQUEST_CD", new Object[]{"E", processRequestId, profileId}, true);
							return 0;
						}else{
							DataLayerMgr.executeUpdate("SET_PROCESS_REQUEST_CD", new Object[]{"R", processRequestId, profileId}, true);
							throw e;
						}
					}
				}
				Connection conn = DataLayerMgr.getConnection("REPORT");
				okay = false;
				try {
					params.put("processResponse", pr);
					int n = DataLayerMgr.executeUpdate(conn, "COMPLETE_PROCESS_REQUEST", params);
					switch(n) {
						case 1:
							log.info("Processing complete for " + processTypeDesc + " request " + processRequestId + " for user " + profileId);
							break;
						case 0:
							throw new ServiceException("Zero rows updated as complete for " + processTypeDesc + " request " + processRequestId + " for user " + profileId);
						default:
							throw new ServiceException("More than one row updated as complete for " + processTypeDesc + " request " + processRequestId + " for user " + profileId);
					}
					if(!StringUtils.isBlank(notifyEmail))
						DataLayerMgr.executeCall(conn, "SEND_PROCESS_REQUEST_EMAIL", params);
					okay = true;
					conn.commit();
				} finally {
					if(!okay)
						DbUtils.rollbackSafely(conn);
					conn.close();
				}
			}
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException | ConvertException |BeanException e) {
			throw new ServiceException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Processor getProcessor(ProcessType processType) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Processor processor = processors.get(processType);
		if(processor == null) {
			Class<? extends Processor> processorClass = (Class<? extends Processor>) ReflectionUtils.loadContextClass(processType.getProcessorClass());
			processor = processorClass.newInstance();
			Processor oldProcessor = processors.putIfAbsent(processType, processor);
			if(oldProcessor != null)
				processor = oldProcessor;
		}
		return processor;
	}

	public Processor getProcessor(int processTypeId) throws InstantiationException, IllegalAccessException, InvalidIntValueException, ClassNotFoundException {
		return getProcessor(ProcessType.getByValue(processTypeId));
	}
	
	protected static void readEncoded(final CallInputs ci, Reader reader) throws IOException {
		final Map<String, String> headers = new LinkedHashMap<>();
		final Map<String, Object> params = new LinkedHashMap<>();
		StringUtils.readEncoded(reader, new EncodedHandler<IOException>() {
			public void handle(String prefix, String name, String value) throws IOException {
				if(prefix == null) {
					switch(name) {
						case "requestURL":
							ci.setRequestURL(value);
							break;
						case "remoteAddr":
							ci.setRemoteAddr(value);
							break;
						case "remotePort":
							ci.setRemotePort(ConvertUtils.getIntSafely(value, 0));
							break;
						case "sessionId":
							ci.setSessionId(value);
							break;
					}
				} else {
					switch(prefix) {
						case "HEADER":
							headers.put(name, value);
							break;
						case "PARAM":
							params.put(name, value);
							break;
					}
				}
			}
		});
		ci.setHeaders(headers);
		ci.setRealParameters(params, 20);
	}	

	public String getAppCd() {
		return appCd;
	}

	public void setAppCd(String appCd) {
		this.appCd = appCd;
	}
}
