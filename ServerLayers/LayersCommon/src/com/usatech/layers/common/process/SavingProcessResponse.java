package com.usatech.layers.common.process;

import java.sql.SQLException;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;

public class SavingProcessResponse implements ProcessResponse {
	protected final long processRequestId;
	protected final String updateCallId;
	protected String appendResponseText;
	protected String responseSummary;
	protected String emailSubject;
	protected String emailFromName;
	protected String emailFromAddress;
	protected String emailHeader;
	protected String emailMiddle;
	protected String emailFooter;
	protected boolean emailSummaryFirst;

	protected class UpdateBean {
		public long getProcessRequestId() {
			return processRequestId;
		}

		public String getAppendResponseText() {
			return appendResponseText;
		}
	}

	protected final UpdateBean updateBean = new UpdateBean();

	public SavingProcessResponse(long processRequestId, String updateCallId) {
		this.processRequestId = processRequestId;
		this.updateCallId = updateCallId;
	}

	public void appendResponseText(String text) throws ServiceException {
		this.appendResponseText = text;
		try {
			DataLayerMgr.executeCall(updateCallId, updateBean, true);
		} catch(SQLException | DataLayerException e) {
			throw new ServiceException(e);
		}
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailFromName() {
		return emailFromName;
	}

	public void setEmailFromName(String emailFromName) {
		this.emailFromName = emailFromName;
	}

	public String getEmailFromAddress() {
		return emailFromAddress;
	}

	public void setEmailFromAddress(String emailFromAddress) {
		this.emailFromAddress = emailFromAddress;
	}

	public String getEmailHeader() {
		return emailHeader;
	}

	public void setEmailHeader(String emailHeader) {
		this.emailHeader = emailHeader;
	}

	public String getEmailFooter() {
		return emailFooter;
	}

	public void setEmailFooter(String emailFooter) {
		this.emailFooter = emailFooter;
	}

	public long getProcessRequestId() {
		return processRequestId;
	}

	public String getEmailMiddle() {
		return emailMiddle;
	}

	public void setEmailMiddle(String emailMiddle) {
		this.emailMiddle = emailMiddle;
	}

	public boolean isEmailSummaryFirst() {
		return emailSummaryFirst;
	}

	public void setEmailSummaryFirst(boolean emailSummaryFirst) {
		this.emailSummaryFirst = emailSummaryFirst;
	}

	public String getResponseSummary() {
		return responseSummary;
	}

	public void setResponseSummary(String responseSummary) {
		this.responseSummary = responseSummary;
	}
}
