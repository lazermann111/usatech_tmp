package com.usatech.layers.common.process;

import java.sql.SQLException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledJob;
import com.usatech.layers.common.constants.CommonAttrEnum;
/**
 * REPORT.PROCESS_TYPE table
 * 1	Mass Device Update from Spreadsheet	REPORT_GENERATOR
	 2	Mass EFT Adjustments from CSV File	POSM_LAYER
   3	Cellular Device Support	POSM_LAYER
 * @author yhe
 *
 */
@DisallowConcurrentExecution
public class ProcessRequestRetry extends QuartzCronScheduledJob{
	private static final Log log = Log.getLog();
	protected static String processorCd;
	protected static Publisher<ByteInput> publisher;	
	

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}


	public static void setPublisher(Publisher<ByteInput> publisher) {
		ProcessRequestRetry.publisher = publisher;
	}


	public static String getProcessorCd() {
		return processorCd;
	}


	public static void setProcessorCd(String processorCd) {
		ProcessRequestRetry.processorCd = processorCd;
	}


	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		Results reportResults=null;
		try{
			reportResults = DataLayerMgr.executeQuery("GET_RETRY_PROCESS_REQUEST_USERS", new Object[]{processorCd});
			int count=0;
			while(reportResults.next()) {
				MessageChain mc = new MessageChainV11();
				MessageChainStep step = null;
				if(processorCd.equals("REPORT_GENERATOR")){
						step=mc.addStep("usat.process.request");
				}else{
					 step=mc.addStep("usat.process.request.opr");
				}
				step.setAttribute(CommonAttrEnum.ATTR_PROCESS_ID, reportResults.get("processRequestId"));
				step.setAttribute(CommonAttrEnum.ATTR_USER_ID, reportResults.get("profileId"));
				MessageChainService.publish(mc, publisher);
				log.info("Publish retry process request for processRequestId: " + reportResults.get("processRequestId") + ", profileId: " + reportResults.get("profileId"));
				count++;
			}
			if(count==0){
				log.info("No retry process request found");
			}
		}
		catch(DataLayerException|SQLException|ServiceException e){
			log.error("Failed to publish retry process request",e);
		}finally{
			if(reportResults!=null){
				reportResults.close();
			}
		}
		
	}

	
}
