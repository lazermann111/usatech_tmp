package com.usatech.layers.common.process;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum ProcessType {
	MASS_DEVICE_UPDATE_EXCEL(1, "Mass Device Update", "REPORT_GENERATOR", "com.usatech.report.MassDeviceUpdate"),
	MASS_EFT_ADJUSTMENTS_CSV(2, "Mass EFT Adjustments", "POSM_LAYER", "com.usatech.posm.process.MassEftAdjustment"), 
	CELLULAR_DEVICE_SUPPORT_CSV(3, "Cellular Device Support", "POSM_LAYER", "com.usatech.posm.process.MassCellularNumbersUpdate")
	;
	protected final static EnumIntValueLookup<ProcessType> lookup = new EnumIntValueLookup<ProcessType>(ProcessType.class);

	public static ProcessType getByValue(int value) throws InvalidIntValueException {
		return lookup.getByValue(value);
	}

	private final int value;
	private final String description;
	private final String processorCD;
	private final String processorClass;

	private ProcessType(int value, String description, String processorCD, String processorClass) {
		this.value = value;
		this.description = description;
		this.processorCD = processorCD.toUpperCase();
		this.processorClass = processorClass;
	}

	public String getDescription() {
		return description;
	}

	public int getValue() {
		return value;
	}

	public String getProcessorCD() {
		return processorCD;
	}

	public String getProcessorClass() {
		return processorClass;
	}

}
