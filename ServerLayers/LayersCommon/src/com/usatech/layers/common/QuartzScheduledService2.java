/** 
 * USA Technologies, Inc. 2011
 * QuartzScheduledService.java by phorsfield, Aug 26, 2011 4:45:53 PM
 */
package com.usatech.layers.common;

import static org.quartz.TriggerBuilder.newTrigger;

import java.beans.IntrospectionException;
import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.StandardMBean;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.MapConfiguration;
import org.apache.commons.configuration.SubsetConfiguration;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.JobPersistenceException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;
import org.quartz.impl.JobExecutionContextImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.calendar.BaseCalendar;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

import simple.app.BaseWithConfig;
import simple.app.DialectResolver;
import simple.app.Service;
import simple.app.ServiceException;
import simple.app.ServiceStatusListener;
import simple.app.WorkQueue;
import simple.bean.ConvertException;
import simple.io.Log;
import simple.lang.ParallelInitializer;
import simple.text.StringUtils;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.RunOnGetFuture;

/**
 * <p>
 * The scheduled service for performing tasks periodically using Quartz.
 * </p>
 * 
 * <p>
 * Note: Quartz jobs are stored in a database
 * </p>
 * 
 * Quartz will restore all jobs from the database
 * 
 * Therefore we only create new jobs for the database if they do not already exist.</p>
 * 
 * There will really only be one job type in the database we can deal with: <br />
 * 
 * <ul>
 * <li>recurring jobs</li>
 * </ul>
 * 
 * Others will have ran and completed.</p>
 * 
 * Quartz Properties Example <code>
simple.app.Service.DailyeSudsDiagnostic.class=com.usatech.layers.common.QuartzScheduledService
simple.app.Service.DailyeSudsDiagnostic.jobs.class=com.usatech.layers.common.QuartzScheduledJobMapping
simple.app.Service.DailyeSudsDiagnostic.initCycleTimeMillis=60000
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).class=com.usatech.posm.schedule.ESudsDiagnosticReport
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).cron= * * * * 0
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).group = group1
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).forceReset = true
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).jobDataMap(smtp.server)=localhost
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).jobDataMap(smtp.port)=26
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).jobDataMap(from.address)=esuds@usatech.com
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.scheduler.instanceName) = QuartzTimeScheduler
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.scheduler.instanceId) = ${USAT.instanceId}
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.threadPool.class) = org.quartz.simpl.SimpleThreadPool
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.threadPool.threadCount) = 5
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.threadPool.threadPriority) = 5
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.misfireThreshold) = 60000
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.class)=org.quartz.impl.jdbcjobstore.JobStoreTX
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.driverDelegateClass)=org.quartz.impl.jdbcjobstore.oracle.OracleDelegate
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.useProperties)=false
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.dataSource)=myDS
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.tablePrefix)=QUARTZ.QRTZ_
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.isClustered)=true
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.dataSource.myDS.jndiURL)=java:/simple/jdbc/OPER

 * </code>
 * 
 * @author bkrug
 * 
 */
public class QuartzScheduledService2 implements Service, JobFactory, QuartzScheduledServiceMXBean {
	private static final Log log = Log.getLog();
	
	public static final ObjectName QUARTZ_OBJECT_NAME;
	static {
		ObjectName on;
		try {
			on = new ObjectName("QuartzScheduledService:name=quartzJMX");
		} catch(MalformedObjectNameException e) {
			log.error(e);
			on = null;
		} catch(NullPointerException e) {
			log.error(e);
			on = null;
		}
		QUARTZ_OBJECT_NAME = on;
	}
	
	protected final String serviceName;
	protected final Configuration config;
	protected final Map<String,Object> namedReferences;
	protected SchedulerFactory factory;
	protected final Properties schedulerProps = new Properties();
	protected final Map<String, JobInfo> jobs = new HashMap<>();
	protected final Map<String, ManuallyRunnableSettings> manuallyRunnable = new HashMap<>();
	protected boolean removeJobs = true;
	protected static final String MANUALLY_RUN_PREFIX = "job:";

	public class ManuallyRunnableSettings {
		protected final Map<String, String> parameters = new LinkedHashMap<>();
		protected final String jobName;
		
		public ManuallyRunnableSettings(String jobName) {
			this.jobName = jobName;
		}

		public String getParameter(String parameterName) {
			return parameters.get(parameterName);
		}
		
		public void setParameter(String parameterName, String parameterLabel) {
			if(!StringUtils.isBlank(parameterLabel)) {
				boolean wasEmpty = parameters.isEmpty();
				parameters.put(parameterName, parameterLabel);
				if(wasEmpty)
					manuallyRunnable.put(jobName, this);
			} else {
				parameters.remove(parameterName);
				if(parameters.isEmpty())
					manuallyRunnable.remove(jobName);
			}
		}
	}
	public class JobInfo {
		protected final String jobId;
		protected String group;
		protected String cron;
		protected Class<? extends Job> jobClass;
		protected boolean forceReset;
		protected int misfireInstruction;

		public JobInfo(String jobId) {
			this.jobId = jobId;
		}

		public String getGroup() {
			return group;
		}

		public void setGroup(String group) {
			this.group = group;
		}

		public String getCron() {
			return cron;
		}

		public void setCron(String cron) {
			this.cron = cron;
		}

		public Class<? extends Job> getJobClass() {
			return jobClass;
		}

		public void setJobClass(Class<? extends Job> jobClass) {
			this.jobClass = jobClass;
		}

		public boolean isForceReset() {
			return forceReset;
		}

		public void setForceReset(boolean forceReset) {
			this.forceReset = forceReset;
		}

		public int getMisfireInstruction() {
			return misfireInstruction;
		}

		public void setMisfireInstruction(int misfireInstruction) {
			this.misfireInstruction = misfireInstruction;
		}

		public String getJobId() {
			return jobId;
		}
	}
	// -- initializer protected variables
	protected Scheduler scheduler;

	// -- synchronized variables
	protected AtomicInteger threadPoolSize = new AtomicInteger(0); 
	
	/**
	 * @return the number of milliseconds between attempts to start Quartz
	 */
	public long getInitCycleTimeMillis() {
		return this.initializer.getInitCycleTimeMillis();
	}

	/**
	 * @param initCycleTimeMillis the number of milliseconds between attempts to start Quartz
	 */
	public void setInitCycleTimeMillis(long initCycleTimeMillis) {
		this.initializer.setInitCycleTimeMillis(initCycleTimeMillis);
	}
	
	/**
	 * @return the number of milliseconds to allow shutdown of Quartz to proceed
	 */
	public long getResetBlockMillis() {
		return this.initializer.getResetBlockMillis();
	}

	/**
	 * @param resetBlockMillis the number of milliseconds to allow shutdown of Quartz to proceed
	 */
	public void setResetBlockMillis(long resetBlockMillis) {
		this.initializer.setResetBlockMillis(resetBlockMillis);
	}	
	/**
	 * @return the schedulerProps
	 */
	public Properties getSchedulerProps() {
		return schedulerProps;
	}

	protected final ParallelInitializer<SchedulerException> initializer = new ParallelInitializer<SchedulerException>() {
		@Override
		protected void doParallelInitialize() throws SchedulerException {
			factory = new StdSchedulerFactory(schedulerProps);
			scheduler = factory.getScheduler();
			scheduler.setJobFactory(QuartzScheduledService2.this);
			@SuppressWarnings("unchecked")
			Set<JobKey> preexistingJobs = scheduler.getJobKeys(GroupMatcher.groupStartsWith(""));
			registerJMX();
			
			for(JobInfo job : jobs.values()) {
				if(job.getJobClass() == null) {
					log.warn("jobId={0} is invalid: no class property", job.getJobId());
					continue;
				}
				
				JobKey jobKey = new JobKey(job.getJobId(), job.getGroup());
				if (!DialectResolver.isOracle() || !jobKey.toString().startsWith("postgres")) {
					preexistingJobs.remove(jobKey);
				} else {
					log.info("jobId={0} was omitted: Only for PostgreSQL", job.getJobId());
					continue;
				}
				
				Trigger trigger = null;
				TriggerKey triggerKey = null;
				
				boolean createTrigger = false;
				boolean createJob = true;

				if(scheduler.checkExists(jobKey)) {
					// job exists, so now check trigger to see if it is scheduled
					triggerKey = new TriggerKey(job.getJobId() + "_trigger", job.getGroup());
					trigger = scheduler.getTrigger(triggerKey);

					if(trigger != null) {
						// trigger exists
						if(job.isForceReset()) { // we are explicitly asked to remove it
							createTrigger = true;
						} else if(trigger instanceof CronTrigger) { // or, check if trigger of the matching type
							CronTrigger ct = (CronTrigger) trigger;
							if(!ct.getCronExpression().equalsIgnoreCase(job.getCron())) { // and, check that the trigger is the same Cron string
								// schedule does not match
								createTrigger = true;
							}
							// cron type and schedule match - leave alone
						} else {// or not a cron trigger, so reschedule
							createTrigger = true;
						}

						// trigger exists but needs to change, so unschedule
						if(createTrigger) {
							scheduler.unscheduleJob(triggerKey);
						}
					} else {
						// trigger does not exist, do not unschedule
						createTrigger = true;
					}

					// job exists, check if we are asked to recreate the job through properties
					if(!job.isForceReset()) {
						// and check job Data Map to see if properties have been updated
						try {
							JobDetail currentJob = scheduler.getJobDetail(jobKey);
							if(currentJob.getJobClass().equals(job.getJobClass()) && currentJob.isDurable() && !currentJob.requestsRecovery()) {
								// job is the same, leave it alone.
								createJob = false;
							}
						} catch(JobPersistenceException e) {
							if(e.getCause() instanceof ClassNotFoundException) {
								// assume that class has changed
								createJob = true;
							} else
								throw e;
						}

					}
				} else {
					// job does not exist
					createTrigger = true;
				}
				if(createJob) {
					JobDetail jobDetail = JobBuilder.newJob(job.getJobClass()).withIdentity(job.getJobId(), job.getGroup()).storeDurably().requestRecovery(false).build();
					scheduler.addJob(jobDetail, true);
				}
				if(createTrigger && !StringUtils.isBlank(job.getCron())) {
					scheduler.scheduleJob(createTrigger(job));
				}
			}
			if(isRemoveJobs() && preexistingJobs.size() > 0)
				for(JobKey jobKey : preexistingJobs)
					scheduler.deleteJob(jobKey);

			scheduler.start();
			threadPoolSize.compareAndSet(0, scheduler.getMetaData().getThreadPoolSize());
			threadPoolSize.compareAndSet(1, scheduler.getMetaData().getThreadPoolSize());
			log.info("Quartz Scheduler started with " + jobs.size() + " job" + (jobs.size() == 1 ? "." : "s."));
			
		}
		
		@Override
		protected void doParallelReset(){
			try {
				int tps = threadPoolSize.get();
				scheduler.shutdown(true);
				scheduler = null;
				threadPoolSize.compareAndSet(tps, 0);				
			} catch(SchedulerException e){
				log.warn("Failed to shutdown scheduler.", e);
			}			
		}
	};
	
	public QuartzScheduledService2(String serviceName, Configuration config, Map<String, Object> namedReferences) throws SecurityException, IllegalArgumentException, IntrospectionException, ParseException, InstantiationException, ConvertException, ServiceException {
		this.serviceName = serviceName;
		this.config = config;
		this.namedReferences = namedReferences;
		BaseWithConfig.configureProperties(this, config, namedReferences, true);
	}
	
	/**
	 * Does not support this feature.
	 * @return false
	 */
	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		return false;
	}

	/**
	 * Get the number of working threads, there is only one thread for the ScheduledService,
	 * so 1 is always returned.
	 * @return number of threads
	 * @see simple.app.Service#getNumThreads()
	 */
	public int getNumThreads() {
		try {
			if(initializer.hasInitialized()) {
				return threadPoolSize.get();
			}
		} catch (InterruptedException e) {
			log.debug("Exception querying the number of threads in scheduler", e);
		} 
		return 0;
	}

	/**
	 *  Return the service name.
	 * 
	 * @see simple.app.Service#getServiceName()
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Pause the working thread of the scheduled service.
	 * @return number of threads paused
	 * @see simple.app.Service#pauseThreads()
	 */
	public int pauseThreads() throws ServiceException {
		log.info("Pause is not supported - we have no access to Quartz worker threads. Use 'quartz' beans in JMX to pause jobs.");
		return 0;
	}

	/**
	 * Does not support this feature.
	 * @return false
	 */
	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return false;
	}

	/**
	 * Attempts to start the scheduler if it has not already been started.
	 * @param timeout
	 * @return 0
	 */
	public int restartThreads(long timeout) throws ServiceException {
		try {
			initializer.reset();
			initializer.initialize();
			initializer.waitForInitialize(initializer.getInitCycleTimeMillis(), TimeUnit.MILLISECONDS);
			return threadPoolSize.get();
		} catch (InterruptedException e) {
			throw new ServiceException(e);
		} catch (SchedulerException e) {
			throw new ServiceException(e);
		}
	}

	/** Start the working thread of this ScheduledService.
	 * @param numThreads this parameter will be ignored.
	 * @return number of threads started.
	 * @see simple.app.Service#startThreads(int)
	 */
	public int startThreads(int numThreads) throws ServiceException {
		try {
			if(initializer.hasInitialized())
				return 0;
			if(initializer.initialize()) {
				threadPoolSize.compareAndSet(0, 1);
				return 1;
			}
			return 0;
		} catch (InterruptedException e) {
			throw new ServiceException(e);
		} catch (SchedulerException e) {
			throw new ServiceException(e);
		}		 
	}

	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		return new RunOnGetFuture<Integer>(new Callable<Integer>() {
	    	   public Integer call() {
				int ltp = threadPoolSize.get();
				if(initializer.reset()) {
					return ltp;
				}
				return 0;
			}
		});
	}
	
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		return stopAllThreads(force);
	}

	/**
	 * Stop the working thread.
	 * @param force this parameter will be ignored.
	 * @param timeout this parameter will be ignored.
	 * @return number of threads stopped.
	 * @see simple.app.Service#stopAllThreads(boolean, long)
	 */
	public int stopAllThreads(boolean force, long timeout) {
		try {
			return stopAllThreads(force).get(timeout, TimeUnit.MILLISECONDS);
		} catch (ServiceException e) {
			log.error("Error in stopAllThreads(force,timeout):",e);
			return 0;
		} catch (InterruptedException e) {
			log.error("Interrupted waiting for all threads in stopAllThreads(force,timeout):",e);
			return 0;
		} catch (ExecutionException e) {
			log.error("Error in stopAllThreads(force,timeout):",e);
			return 0;
		} catch (TimeoutException e) {
			log.error("Timeout while stopping all threads in stopAllThreads(force,timeout):",e);
			return 0;
		}
	}

	/**
	 * Stop specific number of threads, but 
	 * for the ScheduledService, all threads are stopped.
	 * 
	 * @param numThreads this parameter will be ignored.
	 * @param force this parameter will be ignored.
	 * @param timeout this parameter will be ignored.
	 * @return number of threads stopped.
	 * @see simple.app.Service#stopThreads(int, boolean, long)
	 */
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		int tps = threadPoolSize.get();
		if(initializer.reset()) {
			// reset returns true if and when reset() is called
			// if so, then it was responsible for triggering the
			// shutdown, which may have completed by now, so
			// we return the # threads active before reset.
			return tps;
		}
		return 0;
	}

	/**
	 * Feature unsupported;
	 * @return number of threads unpaused.
	 * 
	 * @see simple.app.Service#unpauseThreads()
	 */
	public int unpauseThreads() throws ServiceException {
		log.info("Unpause is not supported - we have no access to Quartz worker threads. Use 'quartz' beans in JMX to unpause jobs.");
		return 0;
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		return WorkQueue.TRUE_FUTURE;
	}

	/**
	 * @see simple.app.Service#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		return new ResultFuture<Boolean>() {
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				initializer.setResetBlockMillis(unit == null ? timeout : unit.toMillis(timeout));
				return initializer.reset();
			}
		};
	}

	@Override
	public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
		JobDetail jobDetail = bundle.getJobDetail();
		Class<? extends Job> jobClass = jobDetail.getJobClass();
		try {
			if(log.isDebugEnabled())
				log.debug("Producing instance of Job '" + jobDetail.getKey() + "', class=" + jobClass.getName());
			return BaseWithConfig.configure(jobClass, config.subset("job(" + jobDetail.getKey().getName() + ")"), namedReferences, true);
		} catch(Exception e) {
			SchedulerException se = new SchedulerException("Problem instantiating class '" + jobDetail.getJobClass().getName() + "'", e);
			throw se;
		}
	}	
	
	protected CronTrigger createTrigger(JobInfo job) throws SchedulerException {
		CronScheduleBuilder builder;
		try {
			builder = CronScheduleBuilder.cronSchedule(job.getCron());
		} catch(ParseException e) {
			throw new SchedulerException(e);
		}
		switch(job.getMisfireInstruction()) {
			case CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING:
				builder = builder.withMisfireHandlingInstructionDoNothing();
				break;
			case CronTrigger.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY:
				builder = builder.withMisfireHandlingInstructionIgnoreMisfires();
				break;
			default:
				builder = builder.withMisfireHandlingInstructionFireAndProceed();
				break;
		}
		return newTrigger().withIdentity(job.getJobId() + "_trigger", job.getGroup()).withSchedule(builder).forJob(job.getJobId(), job.getGroup()).build();
	}

	public JobInfo getJob(String jobId) {
		JobInfo jobInfo = jobs.get(jobId);
		if(jobInfo == null) {
			jobInfo = new JobInfo(jobId);
			jobs.put(jobId, jobInfo);
		}
		return jobInfo;
	}
	// ----
	public void registerJMX() {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		Object mbean;
		if(manuallyRunnable.isEmpty())
			mbean = this;
		else
			try {
				mbean = new StandardMBean(this, QuartzScheduledServiceMXBean.class) {
					@Override
					protected void cacheMBeanInfo(MBeanInfo info) {
						// Customize by adding additional ops for manuallyRunnable
						MBeanOperationInfo[] old = info.getOperations();
						MBeanOperationInfo[] operations = new MBeanOperationInfo[old.length + manuallyRunnable.size()];
						System.arraycopy(old, 0, operations, 0, old.length);
						int i = old.length;
						for(Map.Entry<String, ManuallyRunnableSettings> entry : manuallyRunnable.entrySet()) {
							MBeanParameterInfo[] signature;
							if(entry.getValue() == null)
								signature = new MBeanParameterInfo[0];
							else {
								signature = new MBeanParameterInfo[entry.getValue().parameters.size()];
								int k = 0;
								for(Map.Entry<String, String> parameter : entry.getValue().parameters.entrySet())
									signature[k++] = new MBeanParameterInfo(parameter.getValue(), "java.lang.String", "Value for " + parameter.getValue());
							}
							operations[i++] = new MBeanOperationInfo(MANUALLY_RUN_PREFIX + entry.getKey(), "Run job " + entry.getKey() + " with certain parameters", signature, "void", MBeanOperationInfo.ACTION);
						}
						super.cacheMBeanInfo(new MBeanInfo(info.getClassName(), info.getDescription(), info.getAttributes(), info.getConstructors(), operations, info.getNotifications(), info.getDescriptor()));
					}

					@Override
					public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
						if(actionName.startsWith(MANUALLY_RUN_PREFIX)) {
							String jobName = actionName.substring(MANUALLY_RUN_PREFIX.length());
							String jobGroup = config.getString("job(" + jobName + ").group");
							ManuallyRunnableSettings mrs = manuallyRunnable.get(jobName);
							if(mrs == null)
								throw new MBeanException(null, "This job '" + jobName + "' is not manually runnable");
							if(mrs.parameters.size() != params.length)
								throw new MBeanException(null, "This job '" + jobName + "' has " + mrs.parameters.size() + " manually configurable parameters but " + params.length + " were used");
							SubsetConfiguration subset = (SubsetConfiguration)config.subset("job(" + jobName + ")");
							String prefix = subset.getPrefix();
							Configuration root = config;
							while(root instanceof SubsetConfiguration && ((SubsetConfiguration) root).getParent() != null)
								root = ((SubsetConfiguration) root).getParent();

							Map<String, Object> manualProps = new HashMap<>();
							int i = 0;
							for(String paramName : mrs.parameters.keySet())
								manualProps.put(StringUtils.isBlank(prefix) ? paramName : prefix + '.' + paramName, params[i++]);
							CompositeConfiguration cc = new CompositeConfiguration();
							cc.addConfiguration(new MapConfiguration(manualProps));
							cc.addConfiguration(root);
							try {
								JobDetail jobDetail = scheduler.getJobDetail(new JobKey(jobName, jobGroup));
								Class<? extends Job> jobClass = jobDetail.getJobClass();
								if(log.isDebugEnabled())
									log.debug("Producing instance of Job '" + jobName + "', class=" + jobClass.getName());
								Job job = BaseWithConfig.configure(jobClass, cc.subset(prefix), namedReferences, true);
								Date date = new Date();
								SimpleTriggerImpl trigger = new SimpleTriggerImpl();
								job.execute(new JobExecutionContextImpl(scheduler, new TriggerFiredBundle(jobDetail, trigger, new BaseCalendar(), false, date, date, null, null), job));
							} catch(ServiceException | SchedulerException e) {
								throw new MBeanException(e);
							}
							return null;
						}
						return super.invoke(actionName, params, signature);
					}
				};
			} catch(NotCompliantMBeanException e) {
				log.warn("Failed to create customized MBean for " + this, e);
				mbean = this;
			}
		try {
			mbs.registerMBean(mbean, QUARTZ_OBJECT_NAME);
		} catch(Exception e) {
			log.warn("Failed to register QuartzScheduledServiceMXBeanImpl.", e);
		}
	}

	@Override
	public void triggerJobNow(String jobId) throws SchedulerException {
		JobInfo job = jobs.get(jobId);
		scheduler.triggerJob(new JobKey(jobId, job.getGroup()));
		log.info("Triggered now jobId=" + jobId);
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	@Override
	public Set<String> getJobIds() {
		return jobs.keySet();
	}

	@Override
	public void pauseJob(String jobId) throws SchedulerException {
		JobInfo job = jobs.get(jobId);
		if (job == null) {
			log.warn("Could not pause jobId={0}: not found", jobId);
			return;
		}
		scheduler.pauseJob(new JobKey(jobId, job.getGroup()));
		log.info("Paused now jobId=" + jobId);
	}

	@Override
	public void resumeJob(String jobId) throws SchedulerException {
		JobInfo job = jobs.get(jobId);
		if (job == null) {
			log.warn("Could not resume jobId={0}: not found", jobId);
			return;
		}
		scheduler.resumeJob(new JobKey(jobId, job.getGroup()));
		log.info("Resumed now jobId=" + jobId);
	}

	@Override
	public Set<String> getPausedJobs() throws SchedulerException {
		Set<String> pausedJobs = new HashSet<String>();
		for(Map.Entry<String, JobInfo> jobEntry : jobs.entrySet()) {
			String jobId = jobEntry.getKey();
			JobInfo job = jobEntry.getValue();
			if(scheduler.getTriggerState(new TriggerKey(jobId + "_trigger", job.getGroup())) == TriggerState.PAUSED) {
				pausedJobs.add(jobId);
			}
		}
		return pausedJobs;
	}

	public boolean isRemoveJobs() {
		return removeJobs;
	}

	public void setRemoveJobs(boolean removeJobs) {
		this.removeJobs = removeJobs;
	}
	
	public ManuallyRunnableSettings getManuallyRunnable(String jobName) {
		ManuallyRunnableSettings mrs = manuallyRunnable.get(jobName);
		if(mrs == null)
			mrs = new ManuallyRunnableSettings(jobName);
		return mrs;
	}
	
}
