package com.usatech.layers.common;

import simple.text.StringUtils;

public class BCDInt {
	protected final int value;
	protected final String hex;

	public BCDInt(int value, String hex) {
		super();
		this.value = value;
		this.hex = StringUtils.pad(hex, '0', 8, StringUtils.Justification.RIGHT);
	}
	public int getValue() {
		return value;
	}
	public String getHex() {
		return hex;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return value + " [0x" + hex + "]";
	}
}
