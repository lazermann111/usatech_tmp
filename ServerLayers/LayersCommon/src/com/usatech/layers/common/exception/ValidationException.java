package com.usatech.layers.common.exception;

public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String messageId;
	private String defaultMessage;
	
	public ValidationException(String messageId, String defaultMessage){
		super(defaultMessage);
		this.messageId = messageId;
		this.defaultMessage = defaultMessage;
	}

	public String getMessageId() {
		return messageId;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

}
