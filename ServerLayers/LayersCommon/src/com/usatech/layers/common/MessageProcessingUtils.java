package com.usatech.layers.common;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.IOUtils;
import simple.io.NullOutputStream;
import simple.lang.InvalidIntValueException;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;

import com.usatech.ec2.EC2Response;
import com.usatech.layers.common.constants.AuthResponseActionCode;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.CRCOutputStream;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_0106;
import com.usatech.layers.common.messagedata.MessageData_0106.ErrorData;
import com.usatech.layers.common.messagedata.MessageData_0301;
import com.usatech.layers.common.messagedata.MessageData_030B;
import com.usatech.layers.common.messagedata.MessageData_030C;
import com.usatech.layers.common.messagedata.MessageData_030D;
import com.usatech.layers.common.messagedata.MessageData_0310;
import com.usatech.layers.common.messagedata.MessageData_60;
import com.usatech.layers.common.messagedata.MessageData_76;
import com.usatech.layers.common.messagedata.MessageData_7C;
import com.usatech.layers.common.messagedata.MessageData_83;
import com.usatech.layers.common.messagedata.MessageData_84;
import com.usatech.layers.common.messagedata.MessageData_85;
import com.usatech.layers.common.messagedata.MessageData_87;
import com.usatech.layers.common.messagedata.MessageData_88;
import com.usatech.layers.common.messagedata.MessageData_8F;
import com.usatech.layers.common.messagedata.MessageData_90;
import com.usatech.layers.common.messagedata.MessageData_9A46;
import com.usatech.layers.common.messagedata.MessageData_A1;
import com.usatech.layers.common.messagedata.MessageData_A4;
import com.usatech.layers.common.messagedata.MessageData_AB;
import com.usatech.layers.common.messagedata.MessageData_AF;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationAuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationV2AuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C3.CallInCommand;
import com.usatech.layers.common.messagedata.MessageData_C3.ProcessPropertyListCommand;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_C8;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.WS2TokenizeMessageData;
import com.usatech.layers.common.messagedata.WSAuthorizeMessageData;
import com.usatech.layers.common.messagedata.WSRequestMessageData;

public class MessageProcessingUtils {

	/** Adds an outgoing file transfer to the message and then constructs the reply
	 * @param message
	 * @param fileGroupNum
	 * @param fileName
	 * @param fileTypeId
	 * @param fileCreateTime
	 * @param fileContent
	 * @throws ServiceException
	 * @throws SQLException
	 */
	public static void writeFileTransferStart(Message message, long fileTransferId, long pendingCommandId, int fileGroupNum, String fileName, int fileTypeId, int filePacketSize, Calendar fileCreateTime, Blob fileContent, MessageType responseMessageType, MessageData extraCommand) throws ServiceException, SQLException {
		long fileSize = fileContent.length();
		message.addFileTransfer(false, fileTransferId, pendingCommandId, fileGroupNum, fileName, fileTypeId, filePacketSize, fileSize);
		if(responseMessageType == MessageType.FILE_XFER_START_4_1) {
			CRCType crcType = CRCType.CRC16Server;
			NullOutputStream nullOut = new NullOutputStream();
			CRCOutputStream out = new CRCOutputStream(nullOut, crcType);
			try {
				IOUtils.copy(fileContent.getBinaryStream(), out);
				out.close();
			} catch (IOException e) {
				throw new ServiceException("Error calculating CRC", e);
			}
			writeFileTransferStart(message, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileSize, crcType, out.getCRCValue(), responseMessageType, extraCommand);
		} else {
			// legacy
			writeFileTransferStart(message, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileSize, null, null, responseMessageType, extraCommand);
		}
	}

	public static void writeFileTransferStart(Message message, int fileGroupNum, String fileName, int fileTypeId, int filePacketSize, Calendar fileCreateTime, long fileSize, CRCType crcType, byte[] crc, MessageType responseMessageType, MessageData extraCommand) throws ServiceException{
		int packets;
		try {
			packets = BigDecimal.valueOf(fileSize).divide(BigDecimal.valueOf(filePacketSize), 0, BigDecimal.ROUND_CEILING).shortValueExact();
	
			if(responseMessageType==MessageType.FILE_XFER_START_4_1){
			MessageData_C8 reply =new MessageData_C8();
			reply.setCreationTime(fileCreateTime);
			reply.setEventId(0);//masterId
			reply.setFilesRemaining(0);// files remaining
			reply.setTotalPackets(packets);
			reply.setTotalBytes(fileSize);
			reply.setFileType(FileType.getByValue(fileTypeId));
			reply.setCrc(new CRC(crcType, crc));
			reply.setFileName(fileName);
			message.sendReply(reply);
			}else if(responseMessageType== MessageType.FILE_XFER_START_2_0){
				MessageData_7C reply = new MessageData_7C();
				reply.setTotalPackets(packets);
				reply.setTotalBytes(fileSize);
				reply.setGroup((byte)fileGroupNum);
				reply.setFileType(FileType.getByValue(fileTypeId));
				reply.setFileName(fileName);
				if (extraCommand == null)
					message.sendReply(reply);
				else
					message.sendReply(extraCommand, reply);
			}else if(responseMessageType== MessageType.FILE_XFER_START_3_0){
				MessageData_A4 reply = new MessageData_A4();
				reply.setTotalPackets(packets);
				reply.setTotalBytes(fileSize);
				reply.setGroup((byte)fileGroupNum);
				reply.setFileType(FileType.getByValue(fileTypeId));
				reply.setFileName(fileName);
				if (extraCommand == null)
					message.sendReply(reply);
				else
					message.sendReply(extraCommand, reply);
			}
		} catch(ArithmeticException e) {
			throw new ServiceException("Could not calculate number of packets", e);
		} catch(InvalidIntValueException e){
			throw new ServiceException("Could not get FileType by fileTypeId:"+fileTypeId, e);
		}
	}

	public static void writeFileTransferStart(Message message, int fileGroupNum, String fileName, int fileTypeId, int filePacketSize, Calendar fileCreateTime, long fileSize, CRCType crcType, byte[] crc, MessageType responseMessageType) throws ServiceException{
		writeFileTransferStart(message, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileSize, crcType, crc, responseMessageType, null);
	}

	/** Constructs a short file transfer message
	 * @param message
	 * @param fileTransferId
	 * @param fileName
	 * @param fileTypeId
	 * @param fileCreateTime
	 * @param fileContent
	 * @throws ServiceException
	 * @throws SQLException
	 * @throws IOException
	 * @throws BufferOverflowException
	 */
	public static void writeShortFileTransfer(Message message, long fileTransferId, String fileName, int fileTypeId, Calendar fileCreateTime, Blob fileContent) throws ServiceException, SQLException, IOException{
		long totalSize = fileContent.length();
		if(totalSize > 0xFFFF)
			throw new ServiceException("File '" + fileName + "' is too big for short file transfer");
	
		MessageData_C7 reply=new MessageData_C7();
		reply.setCreationTime(fileCreateTime);
		reply.setEventId(0); //masterId
		reply.setFilesRemaining(0); // files remaining
		try{
			reply.setFileType(FileType.getByValue(fileTypeId));
		}catch(InvalidIntValueException e){
			throw new ServiceException("Could not get FileType by fileTypeId:"+fileTypeId, e);
		}
		reply.setFileName(fileName);
		reply.setContent(fileContent.getBytes(1, (int)totalSize));
		message.sendReply(reply);
	
	}

	public static void writeGenericResponseV4_1(Message message,GenericResponseResultCode resultCode, String text, GenericResponseServerActionCode actionCode) throws ServiceException{
		MessageData_CB reply=new MessageData_CB();
		reply.setResultCode(resultCode);
		reply.setResponseMessage(text);
		reply.setServerActionCode(actionCode);
		message.sendReply(reply);
	}

	public static void writeGenericResponseWithPropertyListV4_1(Message message, GenericResponseResultCode resultCode, String text, Map<Integer, String> propertyValues) throws ServiceException{
		MessageData_CB reply=new MessageData_CB();
		reply.setResultCode(resultCode);
		reply.setResponseMessage(text);
		reply.setServerActionCode(GenericResponseServerActionCode.PROCESS_PROP_LIST);
		((PropertyValueListAction) reply.getServerActionCodeData()).setPropertyValues(propertyValues);
		message.sendReply(reply);
	}

	public static void writeMessageFromBytes(Message message, MessageType messageType, byte[] commandBytes) throws ServiceException{
		message.sendReply(MessageResponseUtils.getMessageFromBytes(message, messageType, commandBytes));
	}

	public static void writeMessageFromBytes(Message message, MessageType messageType, byte[] commandBytes, MessageData extraCommand) throws ServiceException{
		message.sendReply(extraCommand, MessageResponseUtils.getMessageFromBytes(message, messageType, commandBytes));
	}

	public static void writeAppLayerHealthCheckResponse(Message message, int checked, int success, List<String> errors) throws ServiceException{
		MessageData_0106 reply=new MessageData_0106();
		int messageSize=5;
		reply.setChecked(checked);
		reply.setSuccess(success);
		for(String text: errors){
			int textLength=text.length();
			if(messageSize>=1024){
				break;
			}else if(messageSize+textLength>=1024){
				ErrorData error=reply.addError();
				error.setText(text.substring(0, 1024-messageSize-1));
				break;
			}else{
				ErrorData error=reply.addError();
				error.setText(text);
			}
			messageSize+=textLength;
		}
		message.sendReply(reply);
	}

	public static void writeLegacyInitResponse(Message message, byte[] encryptionKey, String evNumber) throws ServiceException{
		MessageData_8F reply=new MessageData_8F();
		reply.setEncryptionKey(encryptionKey);
		reply.setDeviceSerialNum(evNumber.getBytes(reply.getCharset()));
		message.sendReply(reply);
	}

	public static void writeLegacy83ServerToClientRequest(Message message, byte requestNumber)throws ServiceException{
		MessageData_83 reply=new MessageData_83();
		reply.setRequestNumber(requestNumber);
		message.sendReply(reply);
	}

	public static void writeLegacy87GxPeek(Message message, byte memoryCode, long startingAddress, long numberOfBytesToRead)throws ServiceException{
		MessageData_87 reply=new MessageData_87();
		reply.setMemoryCode(memoryCode);
		reply.setStartingAddress(startingAddress);
		reply.setNumberOfBytesToRead(numberOfBytesToRead);
		message.sendReply(reply);
	}

	public static void writeLegacy88GxPoke(Message message, byte memoryCode, long startingAddress, byte[] dataToPoke)throws ServiceException{
		MessageData_88 reply=new MessageData_88();
		reply.setMemoryCode(memoryCode);
		reply.setStartingAddress(startingAddress);
		reply.setDataToPoke(dataToPoke);
		message.sendReply(reply);
	}

	public static void writeLegacy76DeviceReactivate(Message message)throws ServiceException{
		MessageData_76 reply=new MessageData_76();
		message.sendReply(reply);
	}

	public static void writeLegacy90TerminalUpdateStatus(Message message, byte terminalUpdateStatus)throws ServiceException{
		MessageData_90 reply=new MessageData_90();
		reply.setTerminalUpdateStatus(terminalUpdateStatus);
		message.sendReply(reply);
	}

	public static void writeLegacy84UnixTime(Message message, long deviceLocalTime)throws ServiceException{
		MessageData_84 reply = new MessageData_84();
		reply.setUnixTime(deviceLocalTime);
		message.sendReply(reply);
	}

	public static void writeLegacy85BCDTime(Message message, long deviceLocalTime)throws ServiceException{
		MessageData_85 reply = new MessageData_85();
		reply.setBcdTime(deviceLocalTime);
		message.sendReply(reply);
	}

	public static void writeLegacy2FGenericAck(Message message) throws ServiceException{
		message.sendReply(MessageResponseUtils.createLegacy2FGenericAck(message));
	}

	public static void writeLegacy9A46RoomLayoutAck(Message message) throws ServiceException{
		MessageData_9A46 reply = new MessageData_9A46();
		message.sendReply(reply);
	}

	public static void populateReplyTemplates(MessageData replyData, Map<String, Object> attributes, Translator translator, DeviceType deviceType) throws ServiceException, ParseException {
		Map<Integer, Map<String, Object>> maps = null;
		for(Map.Entry<String, Object> entry : attributes.entrySet()) {
			if(StringUtils.isBlank(entry.getKey()) || !entry.getKey().startsWith("replyTemplate") || entry.getKey().length() < 15)
				continue;
			switch(entry.getKey().charAt(13)) {
				case '.':
					// apply immediately
					String name = entry.getKey().substring(14);
					Object value = entry.getValue();
					switch(name.charAt(0)) {
						case '@':
							break;
						case '~':
							name = name.substring(1);
							String key = ConvertUtils.getStringSafely(entry.getValue());
							if(StringUtils.isBlank(key))
								continue;
							Object[] params = ConvertUtils.convertSafely(Object[].class, attributes.get(entry.getKey().substring(0, 14) + '@' + name), null);
							if(translator == null)
								translator = TranslatorFactory.getDefaultFactory().getTranslator(null, null);
							value = MessageDataUtils.formatForDisplay(translator.translate(key, null, params), deviceType);
						default:
							try {
								ReflectionUtils.setProperty(replyData, name, value, true);
							} catch(IntrospectionException e) {
								throw new ServiceException("Could not set properties on MessageData " + replyData, e);
							} catch(IllegalAccessException e) {
								throw new ServiceException("Could not set properties on MessageData " + replyData, e);
							} catch(InvocationTargetException e) {
								throw new ServiceException("Could not set properties on MessageData " + replyData, e);
							} catch(InstantiationException e) {
								throw new ServiceException("Could not set properties on MessageData " + replyData, e);
							} catch(ConvertException e) {
								throw new ServiceException("Could not set properties on MessageData " + replyData, e);
							}
					}
					break;
				case '/':
					int pos = entry.getKey().indexOf('.', 13);
					if(pos < 0 || pos >= entry.getKey().length() - 1)
						continue;
					int index = ConvertUtils.getIntSafely(entry.getKey().substring(14, pos), -1);
					if(index < 0)
						continue;
					name = entry.getKey().substring(pos + 1);
					value = entry.getValue();
					switch(name.charAt(0)) {
						case '@':
							break;
						case '~':
							name = name.substring(1);
							String key = ConvertUtils.getStringSafely(entry.getValue());
							if(StringUtils.isBlank(key))
								continue;
							Object[] params = ConvertUtils.convertSafely(Object[].class, attributes.get(entry.getKey().substring(0, pos + 1) + '@' + name), null);
							if(translator == null)
								translator = TranslatorFactory.getDefaultFactory().getTranslator(null, null);
							value = MessageDataUtils.formatForDisplay(translator.translate(key, null, params), deviceType);
						default:
							if(maps == null)
								maps = new TreeMap<Integer, Map<String, Object>>();
							Map<String, Object> map = maps.get(index);
							if(map == null) {
								map = new HashMap<String, Object>();
								maps.put(index, map);
							}
							map.put(name, value);
					}
					break;
			}
		}
		if(maps != null) {
			for(Map<String, Object> map : maps.values()) {
				for(Map.Entry<String, Object> entry : map.entrySet()) {
					try {
						ReflectionUtils.setProperty(replyData, entry.getKey(), entry.getValue(), true);
					} catch(IntrospectionException e) {
						throw new ServiceException("Could not set properties on MessageData " + replyData, e);
					} catch(IllegalAccessException e) {
						throw new ServiceException("Could not set properties on MessageData " + replyData, e);
					} catch(InvocationTargetException e) {
						throw new ServiceException("Could not set properties on MessageData " + replyData, e);
					} catch(InstantiationException e) {
						throw new ServiceException("Could not set properties on MessageData " + replyData, e);
					} catch(ConvertException e) {
						throw new ServiceException("Could not set properties on MessageData " + replyData, e);
					}
				}
			}
		}
	}

	public static MessageData prepareAuthResponse(DeviceInfo deviceInfo, AuthorizeMessageData dataAuth, AuthResultCode authResponse, Long globalAccountId, String cardType, BigDecimal approvedAmount, boolean invalidTranId, String responseMessage, boolean freeTransaction, boolean noConvenienceFee, byte[] appSessionKey) {
		String formattedResponseMessage = MessageDataUtils.formatForDisplay(responseMessage, deviceInfo);
		switch(dataAuth.getMessageType()) {
			case AUTH_REQUEST_4_1: // v4.1 Auth
			case CHARGE_REQUEST_4_1: // v4.1 Charge
				MessageData_C3 replyC3 = new MessageData_C3();
				Integer plv = deviceInfo.getPropertyListVersion();
				if(invalidTranId) {
					replyC3.setAuthResponseType(AuthResponseType.CONTROL);
					if(plv == null || plv <= 11) {
						replyC3.setCommandCode(AuthResponseActionCode.PERFORM_CALL_IN);
						((CallInCommand) replyC3.getCommandCodeData()).setDelaySeconds(0);
					} else {
						replyC3.setCommandCode(AuthResponseActionCode.PROCESS_PROP_LIST);
						((ProcessPropertyListCommand) replyC3.getCommandCodeData()).setPropertyValues(Collections.singletonMap(DeviceProperty.MASTER_ID.getValue(), String.valueOf(deviceInfo.getMasterId() + 1)));
					}
				} else {
					if(plv == null || plv <= 11) {
						replyC3.setAuthResponseType(AuthResponseType.AUTHORIZATION);
						AuthorizationAuthResponse aar = (AuthorizationAuthResponse) replyC3.getAuthResponseTypeData();
						if(approvedAmount != null)
							aar.setResultAmount(approvedAmount);
						// aar.setBalanceAmount(balanceAmount);
						aar.setResponseCode(authResponse.getAuthResponseCodeC3());
						aar.setResultMessage(formattedResponseMessage);
					} else {
						replyC3.setAuthResponseType(AuthResponseType.AUTHORIZATION_V2);
						AuthorizationV2AuthResponse aar2 = (AuthorizationV2AuthResponse) replyC3.getAuthResponseTypeData();
						if(approvedAmount != null)
							aar2.setResultAmount(approvedAmount);
						// aar.setBalanceAmount(balanceAmount);
						aar2.setResponseCode(authResponse.getAuthResponseCodeC3());
						aar2.setResultMessage(formattedResponseMessage);
						aar2.setFreeTransaction(freeTransaction);
						aar2.setNoConvenienceFee(noConvenienceFee);
						if(appSessionKey != null)
							aar2.setAppSessionKey(appSessionKey);
					if (globalAccountId != null && (!StringUtils.isBlank(dataAuth.getCardReaderType().getCipherName())
							|| dataAuth.getEntryType().isCardIdRequiredInReply())) {
						aar2.setAuthorizationCode(aar2.getAuthorizationCode() == null ? "|" + globalAccountId
								: aar2.getAuthorizationCode() + "|" + globalAccountId);
					}
					}
					replyC3.setCommandCode(deviceInfo.getActionCode().getAuthResponseActionCode());
				}
				return replyC3;
			case AUTH_REQUEST_3_2: // v3.2 Auth
				MessageData_AF replyAF = new MessageData_AF();
				replyAF.setApprovedAmount(approvedAmount == null ? null : approvedAmount.longValue());
				replyAF.setResponseCode(authResponse.getAuthResponseCodeAF());
				replyAF.setResponseMessage(formattedResponseMessage);
				replyAF.setTransactionId(dataAuth.getTransactionId());
				replyAF.setFreeTransaction(freeTransaction);
				replyAF.setNoConvenienceFee(noConvenienceFee);
				return replyAF;
			case AUTH_REQUEST_3_1: // v3.1 Auth
			case AUTH_REQUEST_3_0: // v3.0 Auth
				MessageData_A1 replyA1 = new MessageData_A1();
				replyA1.setResponseCode(authResponse.getAuthResponseCodeA1());
				replyA1.setTransactionId(dataAuth.getTransactionId());
				replyA1.setApprovedAmount(approvedAmount == null ? null : approvedAmount.longValue());
				return replyA1;
			case PERMISSION_REQUEST_3_1: // v3.1 Permission Request
				MessageData_AB replyAB = new MessageData_AB();
				replyAB.setResponseCode(authResponse.getPermissionResponseCodeAB());
				replyAB.setTransactionId(dataAuth.getTransactionId());
				return replyAB;
			case AUTH_REQUEST_2_0: // v2.0 Auth
				MessageData_60 reply60 = new MessageData_60();
				reply60.setResponseCode(authResponse.getAuthResponseCode60());
				reply60.setTransactionId(dataAuth.getTransactionId());
				return reply60;
			case WS_AUTH_V_3_REQUEST:
			case WS_AUTH_V_3_1_REQUEST:
			case WS_CHARGE_V_3_REQUEST:
			case WS_CHARGE_V_3_1_REQUEST:
				MessageData_0301 reply0301 = (MessageData_0301) ((WSAuthorizeMessageData) dataAuth).createResponse();
				if(approvedAmount != null)
					reply0301.setApprovedAmount(approvedAmount.longValue());
				reply0301.setAuthResultCd(authResponse);
				reply0301.setReturnCode(authResponse.getAuthResponseCodeEC1());
				reply0301.setReturnMessage(responseMessage);
				return reply0301;
			case WS2_AUTH_ENCRYPTED_REQUEST:
			case WS2_AUTH_PLAIN_REQUEST:
			case WS2_CHARGE_ENCRYPTED_REQUEST:
			case WS2_CHARGE_PLAIN_REQUEST:
				MessageData_030B reply030B = (MessageData_030B) ((WSRequestMessageData) dataAuth).createResponse();
				if(approvedAmount != null)
					reply030B.setApprovedAmount(approvedAmount.longValue());
				// reply030B.setBalanceAmount(balanceAmount);
				reply030B.setAuthResultCd(authResponse);
				if(globalAccountId != null)
					reply030B.setCardId(globalAccountId);
				reply030B.setCardType(cardType);
				// reply030B.setConsumerId(consumerId);
				reply030B.setReturnCode(authResponse.getAuthResponseCodeEC2());
				reply030B.setReturnMessage(formattedResponseMessage);
				if(invalidTranId) {
					reply030B.setNewTranId(deviceInfo.getMasterId() + 1);
					reply030B.setActionCode(EC2Response.ACT_UPDATE_TRAN_ID);
				}
				reply030B.setFreeTransaction(freeTransaction);
				reply030B.setNoConvenienceFee(noConvenienceFee);
				return reply030B;
			case WS2_REPLENISH_CASH_REQUEST:
			case WS2_REPLENISH_ENCRYPTED_REQUEST:
			case WS2_REPLENISH_PLAIN_REQUEST:
				MessageData_030C reply030C = (MessageData_030C) ((WSRequestMessageData) dataAuth).createResponse();
				if(approvedAmount != null)
					reply030C.setApprovedAmount(approvedAmount.longValue());
				// reply030C.setBalanceAmount(balanceAmount);
				reply030C.setAuthResultCd(authResponse);
				if(globalAccountId != null)
					reply030C.setCardId(globalAccountId);
				reply030C.setCardType(cardType);
				// reply030C.setConsumerId(consumerId);
				// reply030C.setReplenishBalanceAmount(replenishBalanceAmount);
				reply030C.setReturnCode(authResponse.getAuthResponseCodeEC2());
				reply030C.setReturnMessage(formattedResponseMessage);
				if(invalidTranId) {
					reply030C.setNewTranId(deviceInfo.getMasterId() + 1);
					reply030C.setActionCode(EC2Response.ACT_UPDATE_TRAN_ID);
				}
				return reply030C;
			case WS2_GET_CARD_INFO_PLAIN_REQUEST:
			case WS2_GET_CARD_INFO_ENCRYPTED_REQUEST:
				MessageData_030D reply030D = (MessageData_030D) ((WSRequestMessageData) dataAuth).createResponse();
				if(approvedAmount != null)
					reply030D.setBalanceAmount(approvedAmount.longValue());
				if(globalAccountId != null)
					reply030D.setCardId(globalAccountId);
				reply030D.setCardType(cardType);
				// reply030D.setConsumerId(consumerId);
				reply030D.setReturnCode(authResponse.getCardInfoResponseCodeEC2());
				reply030D.setReturnMessage(formattedResponseMessage);
				return reply030D;
			case WS2_TOKENIZE_PLAIN_REQUEST:
			case WS2_TOKENIZE_ENCRYPTED_REQUEST:
				MessageData_0310 reply0310 = ((WS2TokenizeMessageData) dataAuth).createResponse();
				if(globalAccountId != null)
					reply0310.setCardId(globalAccountId);
				reply0310.setCardType(cardType);
				// reply0310.setConsumerId(consumerId);
				reply0310.setReturnCode(authResponse.getAuthResponseCodeEC2());
				reply0310.setReturnMessage(formattedResponseMessage);
				return reply0310;
			default:
				throw new IllegalStateException("This should never happen");
		}
	}

}
