package com.usatech.layers.common.fees;

public enum RateType {
	PERCENT("P", "Percent<br/>of<br/>Sale", "", "% +"),
	FLAT("F", "Flat<br/>Rate", "$", ""),
	MIN("M", "Minimum<br/>Total<br/>Amount", "($", ")");

	private final String id;
	private final String desc;
	private final String prefix;
	private final String suffix;

	RateType(String id, String desc, String prefix, String suffix) {
		this.id = id;
		this.desc = desc;
		this.prefix = prefix;
		this.suffix = suffix;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public String toString() {
		return getId();
	}

	public boolean isFlat() {
		return this == FLAT;
	}

	public static RateType getById(String id) {
		for(RateType rateType: RateType.values()) {
			if(rateType.getId().equalsIgnoreCase(id))
				return rateType;
		}

		throw new IllegalArgumentException("Invalid RateType " + id);
	}
}
