package com.usatech.layers.common.fees;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.MapInputForm;

public class TerminalFees extends Fees {
	private static final long serialVersionUID = 2L;

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog();

	private final long terminalId;
	private long commissionBankId; // customerBankid
	private int serviceFeeFrequencyId;
	private boolean dirty;

	private TerminalFees(long terminalId) {
		super();
		this.terminalId = terminalId;
		setShowZerosAsBlank(false);
	}

	public long getTerminalId() {
		return terminalId;
	}


	public long getCommissionBankId() {
		return commissionBankId;
	}

	public void setCommissionBankId(long commissionBankId) {
		this.commissionBankId = commissionBankId;
	}
	
	public int getServiceFeeFrequencyId() {
		return serviceFeeFrequencyId;
	}
	
	public void setServiceFeeFrequencyId(int serviceFeeFrequencyId) {
		this.serviceFeeFrequencyId = serviceFeeFrequencyId;
	}

	@Override
	public boolean isDirty() {
		return dirty || super.isDirty();
	}

	@Override
	protected void setClean() {
		dirty = false;
		super.setClean();
	}

	@Override
	public boolean hasCommissions() {
		//return commissionBankId > 0;
		for(Fee fee : getFees()) {
			if (fee.getExchangeType() == ExchangeType.BUY_RATE || fee.getExchangeType() == ExchangeType.SELL_RATE)
				return true;
		}
		
		return false;
	}

	@Override
	public void writeHtmlTable(PageContext pageContext, boolean isReseller) throws IOException {
		JspWriter out = pageContext.getOut();

		out.println("<table class=\"fields\" style=\"width:100%;\">");
		out.println("  <tr class=\"tableHeader\">");
		out.println("    <th style=\"border-right:1px solid white;\"></th>");
		for(ExchangeType exchangeType: ExchangeType.values()) {
			if(exchangeType.isStandardRate())
				continue;
			out.println("    <th colspan=\"3\" style=\"border-right:1px solid white; border-bottom:1px solid white; text-align:center;\">" + exchangeType.getDesc() + "</th>");
		}
		out.println("  </tr>");
		out.println("  <tr class=\"tableHeader\">");
		out.println("    <th class=\"feeCell\" style=\"border-right:1px solid white;\">Fee Type</th>");
		for(ExchangeType exchangeType: ExchangeType.values()) {
			if(exchangeType.isStandardRate())
				continue;
			for(RateType rateType: RateType.values()) {
				out.println("    <th class=\"feeCell\" style=\"" + (rateType == RateType.MIN ? "border-right: 1px solid white;" : "") + "\">" + rateType.getDesc() + "</th>");
			}
		}
		out.println("  </tr>");
		for(ProcessFeeType feeType: getDistinctProcessFeeTypes()) {
			if(isReseller && !feeType.isResellerVisible())
				continue;
			out.println("  <tr class=\"tableDataShade\">");
			out.println("    <td style=\"white-space:nowrap;\">" + feeType.getName() + "<span class=\"required\">*</span></td>");
			for(ExchangeType exchangeType: ExchangeType.values()) {
				if(exchangeType.isStandardRate())
					continue;
				for(RateType rateType: RateType.values()) {
					Fee fee = this.getFee(EntryType.PROCESS_FEE, exchangeType, feeType, rateType);
					if(fee != null) {
						out.println("    <td class=\"feeCell\">" + fee.getRateType().getPrefix() + "<input type=\"text\" id=\"" + fee.getId() + "\" name=\"" + fee.getId() + "\" size=\"3\" value=\"" + fee.getFormattedValue() + "\"" + (fee.isInput() ? " onchange=\"sellRateChanged(this)\"" : " disabled=\"disabled\"") + " data-validators=\"validate-numeric " + (fee.isRequired() ? "required\"" : "\"") + " minvalue=\"0\"/>" + fee.getRateType().getSuffix() + "</td>");
					} else {
						out.println("    <td class=\"feeCell\"></td>");
					}
				}
			}
			out.println("  </tr>");
		}
		for(ServiceFeeType feeType: getDistinctServiceFeeTypes()) {
			if(isReseller && !feeType.isResellerVisible())
				continue;
			Frequency feeFrequency = getFee(EntryType.SERVICE_FEE, ExchangeType.BUY_RATE, feeType, RateType.FLAT).getFrequency();
			out.println("  <tr class=\"tableDataShade\">");
			out.println("    <td style=\"white-space:nowrap;\">" + feeType.getName() + "<br/>(" + feeFrequency + ")<span class=\"required\">*</span></td>");
			for(ExchangeType exchangeType: ExchangeType.values()) {
				if(exchangeType.isStandardRate())
					continue;
				out.println("    <td></td>");
				Fee fee = this.getFee(EntryType.SERVICE_FEE, exchangeType, feeType, RateType.FLAT);
				if(fee != null) {
					out.println("    <td class=\"feeCell\" colspan=\"2\">" + fee.getRateType().getPrefix() + "<input type=\"text\" id=\"" + fee.getId() + "\" name=\"" + fee.getId() + "\" size=\"5\" value=\"" + fee.getFormattedValue() + "\"" + (fee.isInput() ? " onchange=\"sellRateChanged(this)\"" : " disabled=\"disabled\"") + " data-validators=\"validate-numeric " + (fee.isRequired() ? "required\"" : "\"") + " minvalue=\"0\" />" + fee.getRateType().getSuffix() + "</td>");
				} else {
					out.println("    <td class=\"feeCell\" colspan=\"2\"></td>");
				}
			}
			out.println("  </tr>");
		}
		out.println("</table>");
	}

	public void updateFromForm(InputForm form, boolean updateBuyRates) throws ServletException, ConvertException {
		long commissionBankId = form.getLong("COMMISSION_BANK_ID", false, -1);
		if(commissionBankId > 0)
			setCommissionBankId(commissionBankId);

		super.updateFromForm(form, updateBuyRates);
	}

	public static TerminalFees loadByTerminalId(long terminalId) throws DataLayerException, SQLException {
		Connection connection = DataLayerMgr.getConnectionForCall("GET_TERMINAL_FEES_COMMON", true);
		try {
			return loadByTerminalId(connection, terminalId);
		} finally {
			DbUtils.closeSafely(connection);
		}
	}

	public static TerminalFees loadByTerminalId(Connection connection, long terminalId) throws DataLayerException, SQLException {
		String queryName = "GET_TERMINAL_FEES_COMMON";

		Map<String, Object> terminalInfoParams = new HashMap<String, Object>();
		terminalInfoParams.put("TERMINAL_ID", terminalId);

		// TODO: load terminal fees metadata

		TerminalFees fees = null;

		try {
			fees = new TerminalFees(terminalId);

			loadFees(connection, fees);
		} catch(Exception e) {
			throw new SQLException(format("{0} returned invalid results for terminal {1}", queryName, terminalId), e);
		}

		return fees;
	}

	private static void loadFees(Connection connection, TerminalFees fees) throws DataLayerException, SQLException, ConvertException {
		String queryName = "GET_TERMINAL_FEES_COMMON";

		Map<String, Object> terminalFeesParams = new HashMap<String, Object>();
		terminalFeesParams.put("TERMINAL_ID", fees.getTerminalId());

		Results terminalFeesResults = DataLayerMgr.executeQuery(connection, queryName, terminalFeesParams);
		if(!terminalFeesResults.next())
			throw new SQLException(format("{0} returned no results for terminal {1}", queryName, fees.getTerminalId()));

		fees.loadFromResults(terminalFeesResults, true);
	}

	public void saveToDatabase() throws DataLayerException, SQLException {
		Connection connection = DataLayerMgr.getConnectionForCall("SET_TERMINAL_FEES_COMMON", true);
		try {
			saveToDatabase(connection);
		} finally {
			DbUtils.closeSafely(connection);
		}
	}

	public void saveToDatabase(Connection connection) throws DataLayerException, SQLException {
		InputForm form = new MapInputForm(new HashMap<String, Object>());
		form.setAttribute("TERMINAL_ID", getTerminalId());
		if (commissionBankId > 0)
			form.setAttribute("COMMISSION_BANK_ID", commissionBankId);
		if (serviceFeeFrequencyId > 0)
			form.setAttribute("SF_FREQUENCY_ID", serviceFeeFrequencyId);

		saveToForm(form, true);

		DataLayerMgr.executeCall(connection, "SET_TERMINAL_FEES_COMMON", form);
		setClean();
	}

}
