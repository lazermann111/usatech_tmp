package com.usatech.layers.common.fees;

public enum InitiationType {
	TRIGGERED("T", "Fee starts after a trigger event (transaction or file)"),
	IMMEDIATE("I", "Fee starts immediately"),
	GRACE("G", "Fee starts after a trigger event (transaction or file) or at the end of the grace period whichever is first"),
	PER_TRANSACTION("P", "Fee applies to all CC transactions in the date range"),
	UPON_ACTIVATION("A", "Fee is a one time upon activation fee"),
	GRACE_WITH_INACTIVITY_REDUCTION("R", "Fee starts after a trigger event (transaction or file) or at the end of the grace period whichever is first and also charges a reduced rate when user requests and unit is inactive for full month"),
	QUICK_START_IMMEDIATE("Q", "Quick Start Immediate - fee starts immediately in current month"),
	;
	private final String id;
	private final String desc;

	InitiationType(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public String toString() {
		return getId();
	}

	public static InitiationType getById(String id) {
		for(InitiationType interval: InitiationType.values()) {
			if(interval.getId().equalsIgnoreCase(id))
				return interval;
		}

		throw new IllegalArgumentException("Invalid InitiationType " + id);
	}
}
