package com.usatech.layers.common.fees;

import java.io.Serializable;
import java.util.*;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class Frequency implements Serializable {
	private static final long serialVersionUID = 1L;

	private static Log log = Log.getLog();

	private static Map<Integer, Frequency> map = new LinkedHashMap<Integer, Frequency>();

	private static synchronized void load() {
		if(map.isEmpty()) {
			try {
				Results results = DataLayerMgr.executeQuery("GET_FREQUENCIES_COMMON", null);
				while(results.next()) {
					Frequency frequency = new Frequency();
					results.fillBean(frequency);
					map.put(frequency.getId(), frequency);
				}
			} catch(Exception e) {
				log.error("Failed to load Frequencies", e);
			}
		}
	}

	public static synchronized Frequency getById(int id) {
		load();

		Frequency frequency = map.get(id);
		if(frequency != null)
			return frequency;

		throw new IllegalArgumentException("Invalid Frequency " + id);
	}

	public static Collection<Frequency> values() {
		return Collections.unmodifiableCollection(map.values());
	}

	private int id;
	private String name;
	private Interval interval;
	private int months;
	private String dateField;
	private double amount;
	private long offsetMs;
	private double days;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}

	public String getDateField() {
		return dateField;
	}

	public void setDateField(String dateField) {
		this.dateField = dateField;
	}

	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public long getOffsetMs() {
		return offsetMs;
	}

	public void setOffsetMs(long offsetMs) {
		this.offsetMs = offsetMs;
	}

	public double getDays() {
		return days;
	}

	public void setDays(double days) {
		this.days = days;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		result = prime * result + ((dateField == null) ? 0 : dateField.hashCode());
		temp = Double.doubleToLongBits(days);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		result = prime * result + id;
		result = prime * result + ((interval == null) ? 0 : interval.hashCode());
		result = prime * result + months;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int)(offsetMs ^ (offsetMs >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Frequency other = (Frequency)obj;
		if(Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		if(dateField == null) {
			if(other.dateField != null)
				return false;
		} else if(!dateField.equals(other.dateField))
			return false;
		if(Double.doubleToLongBits(days) != Double.doubleToLongBits(other.days))
			return false;
		if(id != other.id)
			return false;
		if(interval != other.interval)
			return false;
		if(months != other.months)
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(offsetMs != other.offsetMs)
			return false;
		return true;
	}

	@Override
	public String toString() {
		//return String.format("Frequency[id=%s, name=%s, interval=%s, dateField=%s, months=%s, amount=%s, offsetMs=%s, days=%s]", id, name, interval, dateField, months, amount, offsetMs, days);
		return getName();
	}

}
