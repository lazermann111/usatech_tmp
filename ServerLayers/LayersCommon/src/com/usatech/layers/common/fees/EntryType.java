package com.usatech.layers.common.fees;

public enum EntryType {
	PROCESS_FEE("PF", "Process Fee"),
	SERVICE_FEE("SF", "Service Fee"),
	CREDIT_CARD("CC", "Credit Card"),
	ROUNDING("SB", "Rounding"),
	REFUND("RF", "Refund"),
	CHARGEBACK("CB", "Chargeback"),
	ADJUSTMENT("AD", "Adjustment"),
	PROCESS_FEE_COMMISSION("PC", "Process Fee Commission"),
	SERVICE_FEE_COMMISSION("SC", "Service Fee Commission");

	private final String id;
	private final String desc;

	EntryType(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public boolean isProcessFee() {
		return this == PROCESS_FEE || this == EntryType.PROCESS_FEE_COMMISSION;
	}

	public boolean isServiceFee() {
		return this == SERVICE_FEE || this == EntryType.SERVICE_FEE_COMMISSION;
	}

	public boolean isCommission() {
		return this == PROCESS_FEE_COMMISSION || this == EntryType.SERVICE_FEE_COMMISSION;
	}

	public String toString() {
		return getId();
	}

	public static EntryType getById(String id) {
		for(EntryType feeType: EntryType.values()) {
			if(feeType.getId().equalsIgnoreCase(id))
				return feeType;
		}

		throw new IllegalArgumentException("Invalid EntryType " + id);
	}
}
