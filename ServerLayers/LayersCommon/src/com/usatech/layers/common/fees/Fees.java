package com.usatech.layers.common.fees;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.text.StringUtils;

public abstract class Fees implements Serializable {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog();

	private final Map<FeeId, Fee> fees = new ConcurrentHashMap<>();

	private boolean showZerosAsBlank = false;

	protected Fees() {
		//		for(TransType TransType : TransType.values()) {
		//			for(ExchangeType exchangeType : ExchangeType.values()) {
		//				for(RateType rateType : RateType.values()) {
		//					addFee(EntryType.PROCESS_FEE, transType, null, rateType, exchangeType);
		//				}
		//			}
		//		}
		//		
		//		for(ExchangeType exchangeType : ExchangeType.values()) {
		//			addFee(EntryType.SERVICE_FEE, null, Frequency.MONTHLY, RateType.FLAT, exchangeType);
		//		}
	}

	public Fee getFee(FeeId id) {
		Fee fee = fees.get(id);
		if(fee == null) {
			fee = new Fee(id);
			fees.put(id, fee);
		}
		return fee;
	}

	public Fee getFee(EntryType entryType, ExchangeType exchangeType, FeeType feeType, RateType rateType) {
		return getFee(new FeeId(entryType, exchangeType, feeType, rateType));
	}

	//	public void setFee(Fee fee)
	//	{
	//		fees.put(fee.getId(), fee);
	//	}
	//	
	//	public void setFee(EntryType entryType, ExchangeType exchangeType, FeeType feeType, RateType rateType, double value)
	//	{
	//		FeeId id = new FeeId(entryType, exchangeType, feeType, rateType);
	//		
	//		Fee fee = getFee(id);
	//		if (fee == null) {
	//			fee = new Fee(id);
	//			setFee(fee);
	//		}
	//		
	//		fee.setValue(value);
	//	}
	//
	//	public Fee getFee(EntryType feeType, ProcessFeeType transType, Frequency frequency, RateType rateType, ExchangeType exchangeType)
	//	{
	//		return fees.get(createId(feeType, exchangeType, transType, frequency, rateType));
	//	}
	//
	//	public Fee getProcessFee(ExchangeType exchangeType, ProcessFeeType transType, RateType rateType)
	//	{
	//		return fees.get(createId(EntryType.PROCESS_FEE, exchangeType, transType, rateType));
	//	}
	//
	//	public Fee getServiceFee(ExchangeType exchangeType, Frequency frequency, RateType rateType)
	//	{
	//		return fees.get(createId(EntryType.SERVICE_FEE, exchangeType, frequency, rateType));
	//	}

	public Collection<Fee> values() {
		return Collections.unmodifiableCollection(fees.values());
	}
	
	public Collection<Fee> getFees() {
		return values();
	}
	
	public Collection<FeeType> getDistinctFeeTypes() {
		return fees.values().stream().map(f -> f.getFeeType()).collect(Collectors.toSet());
	}

	public Collection<Fee> getProcessFees() {
		return fees.values().stream().filter(f -> f.isProcessFee()).collect(Collectors.toList());
	}

	public Collection<Fee> getServiceFees() {
		return fees.values().stream().filter(f -> f.isServiceFee()).collect(Collectors.toList());
	}
	
	public Collection<ProcessFeeType> getDistinctProcessFeeTypes() {
		return fees.values().stream().filter(f -> f.isProcessFee()).map(f -> (ProcessFeeType) f.getFeeType()).collect(Collectors.toSet());
	}

	public Collection<ServiceFeeType> getDistinctServiceFeeTypes() {
		return fees.values().stream().filter(f -> f.isServiceFee()).map(f -> (ServiceFeeType) f.getFeeType()).collect(Collectors.toSet());
	}

	public boolean isDirty() {
		for(Fee fee: values()) {
			if(fee.isDirty())
				return true;
		}

		return false;
	}

	protected void setClean() {
		for(Fee fee: values()) {
			fee.setClean();
		}
	}

	public boolean isShowZerosAsBlank() {
		return showZerosAsBlank;
	}

	public void setShowZerosAsBlank(boolean showZerosAsBlank) {
		this.showZerosAsBlank = showZerosAsBlank;
	}

	public abstract boolean hasCommissions();

	public void loadFromResults(Results results, boolean loadBuyRates) throws DataLayerException, SQLException, ConvertException {
		do // assume caller has already called results.next to validate results
		{
			String entryType = results.getFormattedValue("entryType");
			String exchangeType = results.getFormattedValue("exchangeType");
			String feeType = results.getFormattedValue("feeType");

			for(RateType rateType: RateType.values()) {
				FeeId id = FeeId.createId(entryType, exchangeType, feeType, rateType.getId());
				if(id.isBuyRate() && !loadBuyRates)
					continue;

				if(id.isServiceFee() && !rateType.isFlat()) // only flat rates for service fees
					continue;

				double value = results.getValue(rateType.getId(), Double.class);

				Fee fee = getFee(id);
				fee.setValue(value);

				if(fee.isServiceFee()) {
					fee.setFrequency(Frequency.getById(results.getValue("frequency", Integer.class)));
				}

			}
		} while(results.next());

		updateCommissions();
		setClean();
	}

	public void updateFromForm(InputForm form, boolean updateBuyRates) throws ServletException, ConvertException {
		try {
			Map<String, Object> params = form.getParameters();
			if(params == null || params.isEmpty())
				return;

			for(Map.Entry<String, Object> entry: params.entrySet()) {
				String key = entry.getKey();
				if(!key.startsWith("FEE_"))
					continue;

				FeeId id = FeeId.createId(entry.getKey());
				if(id.isBuyRate() & !updateBuyRates)
					continue;

				double value = ConvertUtils.getDouble(entry.getValue(), 0);
				getFee(id).setValue(value);
			}
		} catch(Exception e) {
			throw new ServletException(e);
		}

		updateCommissions();
	}

	public void saveToForm(InputForm form, boolean saveBuyRates) {
		for(Fee fee: values()) {
			if((fee.isBuyRate() && !saveBuyRates) || fee.isCommission())
				continue;

			form.setAttribute(fee.getKey(), fee.getValue());
		}

		updateCommissions();
	}

	public void updateCommissions() {
		if(!hasCommissions())
			return;

		for(Fee fee: values()) {
			if(!fee.isBuyRate())
				continue;

			Fee sellRateFee = getFee(fee.getEntryType(), ExchangeType.SELL_RATE, fee.getFeeType(), fee.getRateType());
			Fee commission = getFee(fee.getEntryType(), ExchangeType.COMMISSION, fee.getFeeType(), fee.getRateType());
			commission.setValue(sellRateFee.getValue() - fee.getValue());
		}

	}

	public abstract void writeHtmlTable(PageContext pageContext, boolean isReseller) throws IOException;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fees == null) ? 0 : fees.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Fees other = (Fees)obj;
		if(fees == null) {
			if(other.fees != null)
				return false;
		} else if(!fees.equals(other.fees))
			return false;
		return true;
	}

	public String toString() {
		return StringUtils.join(fees.values(), ",");
	}

	public class Fee implements Serializable {
		private static final long serialVersionUID = 1L;

		private final FeeId id;
		protected Frequency frequency;
		protected double value;
		protected boolean dirty = false;

		protected Fee(FeeId id) {
			this.id = id;
		}

		public FeeId getId() {
			return id;
		}

		public FeeType getFeeType() {
			return id.getFeeType();
		}

		public EntryType getEntryType() {
			return id.getEntryType();
		}

		public ExchangeType getExchangeType() {
			return id.getExchangeType();
		}

		public RateType getRateType() {
			return id.getRateType();
		}

		public String getKey() {
			return id.getKey();
		}

		public Frequency getFrequency() {
			return frequency;
		}

		public void setFrequency(Frequency frequency) {
			if(this.frequency != frequency) {
				this.frequency = frequency;
				dirty = true;
			}
		}

		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			if(this.value != value) {
				this.value = value;
				dirty = true;
			}
		}

		public boolean isProcessFee() {
			return getEntryType().isProcessFee();
		}

		public boolean isServiceFee() {
			return getEntryType().isServiceFee();
		}

		public boolean isBuyRate() {
			return getExchangeType().isBuyRate();
		}

		public boolean isSellRate() {
			return getExchangeType().isSellRate();
		}

		public boolean isCommission() {
			return getExchangeType().isCommission();
		}

		public String getFormattedValue() {
			if(isSellRate() && value == 0 && showZerosAsBlank)
				return "";
			else if(isCommission() && getFee(getEntryType(), ExchangeType.SELL_RATE, getFeeType(), getRateType()).getValue() == 0 && showZerosAsBlank)
				return "";
			else return ConvertUtils.formatObject(value, "NUMBER:#,##0.00###");
		}

		public boolean isInput() {
			return isSellRate();
		}

		public boolean isRequired() {
			return isSellRate() && getRateType() != RateType.MIN;
		}

		public boolean isDirty() {
			return dirty;
		}

		protected void setClean() {
			this.dirty = false;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (dirty ? 1231 : 1237);
			result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			long temp;
			temp = Double.doubleToLongBits(value);
			result = prime * result + (int)(temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			Fee other = (Fee)obj;
			if(dirty != other.dirty)
				return false;
			if(frequency == null) {
				if(other.frequency != null)
					return false;
			} else if(!frequency.equals(other.frequency))
				return false;
			if(id == null) {
				if(other.id != null)
					return false;
			} else if(!id.equals(other.id))
				return false;
			if(Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			//return String.format("Fee[id=%s, frequency=%s, value=%s, dirty=%s]", id, frequency, value, dirty);
			return String.format("%s=%.2f", id, value);
		}
	}

}
