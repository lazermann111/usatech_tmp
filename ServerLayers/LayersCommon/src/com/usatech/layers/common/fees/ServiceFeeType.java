package com.usatech.layers.common.fees;

import java.util.*;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class ServiceFeeType extends FeeType {
	private static final long serialVersionUID = 1L;

	public static final int STANDARD_SERVICE_FEE_ID = 1;

	private static Log log = Log.getLog();

	private static Map<Integer, ServiceFeeType> map = new LinkedHashMap<Integer, ServiceFeeType>();

	private static synchronized void load() {
		if(map.isEmpty()) {
			try {
				Results results = DataLayerMgr.executeQuery("GET_SERVICE_FEE_TYPES_COMMON", null);
				while(results.next()) {
					ServiceFeeType serviceFeeType = new ServiceFeeType();
					results.fillBean(serviceFeeType);
					map.put(serviceFeeType.getId(), serviceFeeType);
				}
			} catch(Exception e) {
				log.error("Failed to load TranTypes", e);
			}
		}
	}

	public static synchronized ServiceFeeType getById(int id) {
		load();

		ServiceFeeType serviceFeeType = map.get(id);
		if(serviceFeeType != null)
			return serviceFeeType;

		throw new IllegalArgumentException("Invalid ServiceFeeType " + id);
	}

	public static Collection<ServiceFeeType> values() {
		return Collections.unmodifiableCollection(map.values());
	}

	private String description;
	private int defaultFrequencyId;
	private InitiationType initiationType;
	private Indicator gracePeriodIndicator;
	private Bounds feeBounds;
	private Bounds graceDaysBounds;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDefaultFrequencyId() {
		return defaultFrequencyId;
	}

	public void setDefaultFrequencyId(int defaultFrequencyId) {
		this.defaultFrequencyId = defaultFrequencyId;
	}

	public Frequency getDefaultFrequency() {
		return Frequency.getById(getDefaultFrequencyId());
	}

	public InitiationType getInitiationType() {
		return initiationType;
	}

	public void setInitiationType(InitiationType initiationType) {
		this.initiationType = initiationType;
	}

	public Indicator getGracePeriodIndicator() {
		return gracePeriodIndicator;
	}

	public void setGracePeriodIndicator(Indicator gracePeriodIndicator) {
		this.gracePeriodIndicator = gracePeriodIndicator;
	}

	public Bounds getFeeBounds() {
		return feeBounds;
	}

	public void setFeeBounds(Bounds feeBounds) {
		this.feeBounds = feeBounds;
	}

	public Bounds getGraceDaysBounds() {
		return graceDaysBounds;
	}

	public void setGraceDaysBounds(Bounds graceDaysBounds) {
		this.graceDaysBounds = graceDaysBounds;
	}

	public boolean isResellerVisible() {
		return getId() == STANDARD_SERVICE_FEE_ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + defaultFrequencyId;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((feeBounds == null) ? 0 : feeBounds.hashCode());
		result = prime * result + ((graceDaysBounds == null) ? 0 : graceDaysBounds.hashCode());
		result = prime * result + ((gracePeriodIndicator == null) ? 0 : gracePeriodIndicator.hashCode());
		result = prime * result + ((initiationType == null) ? 0 : initiationType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!super.equals(obj))
			return false;
		if(getClass() != obj.getClass())
			return false;
		ServiceFeeType other = (ServiceFeeType)obj;
		if(defaultFrequencyId != other.defaultFrequencyId)
			return false;
		if(description == null) {
			if(other.description != null)
				return false;
		} else if(!description.equals(other.description))
			return false;
		if(feeBounds == null) {
			if(other.feeBounds != null)
				return false;
		} else if(!feeBounds.equals(other.feeBounds))
			return false;
		if(graceDaysBounds == null) {
			if(other.graceDaysBounds != null)
				return false;
		} else if(!graceDaysBounds.equals(other.graceDaysBounds))
			return false;
		if(gracePeriodIndicator != other.gracePeriodIndicator)
			return false;
		if(initiationType != other.initiationType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		//return String.format("ServiceFeeType[id=%s, name=%s, description=%s, defaultFrequencyId=%s, initiationType=%s, displayOrder=%s, gracePeriodIndicator=%s, feeBounds=%s, graceDaysBounds=%s]", getId(), getName(), description, defaultFrequencyId, initiationType, displayOrder, gracePeriodIndicator, feeBounds, graceDaysBounds);
		return String.valueOf(getId());
	}

}
