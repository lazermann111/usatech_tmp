package com.usatech.layers.common.fees;

import java.util.*;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class ProcessFeeType extends FeeType {
	private static final long serialVersionUID = 1L;

	public static final int CREDIT_CARD_PRESENT_ID = 16;
	public static final int CREDIT_MANUAL_ENTRY_ID = 13;

	private static Log log = Log.getLog();

	private static Map<Integer, ProcessFeeType> map = new LinkedHashMap<Integer, ProcessFeeType>();

	private static synchronized void load() {
		if(map.isEmpty()) {
			try {
				Results results = DataLayerMgr.executeQuery("GET_PROCESS_FEE_TRANS_TYPES_COMMON", null);
				while(results.next()) {
					ProcessFeeType processFeeType = new ProcessFeeType();
					results.fillBean(processFeeType);
					map.put(processFeeType.getId(), processFeeType);
				}
			} catch(Exception e) {
				log.error("Failed to load TranTypes", e);
			}
		}
	}

	public static synchronized ProcessFeeType getById(int id) {
		load();

		ProcessFeeType processFeeType = map.get(id);
		if(processFeeType != null)
			return processFeeType;

		throw new IllegalArgumentException("Invalid ProcessFeeType " + id);
	}

	public static Collection<ProcessFeeType> values() {
		return Collections.unmodifiableCollection(map.values());
	}

	private Bounds percentBounds;
	private Bounds amountBounds;
	private Bounds minBounds;
	private Indicator payableIndicator;
	private Indicator customerDebitIndicator;
	private EntryType entryType;
	private Indicator fillBatchIndicator;
	private Indicator cashIndicator;
	private char transTypeCode;
	private String transItemType;
	private Indicator refundIndicator;
	private Indicator refundableIndicator;
	private Indicator chargebackEnabledIndicator;
	private Indicator creditIndicator;
	private int operatorTransType;
	private Indicator cashlessDeviceTranIndicator;
	private Indicator replenishIndicator;
	private Indicator prepaidIndicator;

	private ProcessFeeType() {

	}

	public Bounds getPercentBounds() {
		return percentBounds;
	}

	public void setPercentBounds(Bounds percentBounds) {
		this.percentBounds = percentBounds;
	}

	public Bounds getAmountBounds() {
		return amountBounds;
	}

	public void setAmountBounds(Bounds amountBounds) {
		this.amountBounds = amountBounds;
	}

	public Bounds getMinBounds() {
		return minBounds;
	}

	public void setMinBounds(Bounds minBounds) {
		this.minBounds = minBounds;
	}

	public Indicator getPayableIndicator() {
		return payableIndicator;
	}

	public void setPayableIndicator(Indicator payableIndicator) {
		this.payableIndicator = payableIndicator;
	}

	public Indicator getCustomerDebitIndicator() {
		return customerDebitIndicator;
	}

	public void setCustomerDebitIndicator(Indicator customerDebitIndicator) {
		this.customerDebitIndicator = customerDebitIndicator;
	}

	public EntryType getEntryType() {
		return entryType;
	}

	public void setEntryType(EntryType entryType) {
		this.entryType = entryType;
	}

	public Indicator getFillBatchIndicator() {
		return fillBatchIndicator;
	}

	public void setFillBatchIndicator(Indicator fillBatchIndicator) {
		this.fillBatchIndicator = fillBatchIndicator;
	}

	public Indicator getCashIndicator() {
		return cashIndicator;
	}

	public void setCashIndicator(Indicator cashIndicator) {
		this.cashIndicator = cashIndicator;
	}

	public char getTransTypeCode() {
		return transTypeCode;
	}

	public void setTransTypeCode(char transTypeCode) {
		this.transTypeCode = transTypeCode;
	}

	public String getTransItemType() {
		return transItemType;
	}

	public void setTransItemType(String transItemType) {
		this.transItemType = transItemType;
	}

	public Indicator getRefundIndicator() {
		return refundIndicator;
	}

	public void setRefundIndicator(Indicator refundIndicator) {
		this.refundIndicator = refundIndicator;
	}

	public Indicator getRefundableIndicator() {
		return refundableIndicator;
	}

	public void setRefundableIndicator(Indicator refundableIndicator) {
		this.refundableIndicator = refundableIndicator;
	}

	public Indicator getChargebackEnabledIndicator() {
		return chargebackEnabledIndicator;
	}

	public void setChargebackEnabledIndicator(Indicator chargebackEnabledIndicator) {
		this.chargebackEnabledIndicator = chargebackEnabledIndicator;
	}

	public Indicator getCreditIndicator() {
		return creditIndicator;
	}

	public void setCreditIndicator(Indicator creditIndicator) {
		this.creditIndicator = creditIndicator;
	}

	public int getOperatorTransType() {
		return operatorTransType;
	}

	public void setOperatorTransType(int operatorTransType) {
		this.operatorTransType = operatorTransType;
	}

	public Indicator getCashlessDeviceTranIndicator() {
		return cashlessDeviceTranIndicator;
	}

	public void setCashlessDeviceTranIndicator(Indicator cashlessDeviceTranIndicator) {
		this.cashlessDeviceTranIndicator = cashlessDeviceTranIndicator;
	}

	public Indicator getReplenishIndicator() {
		return replenishIndicator;
	}

	public void setReplenishIndicator(Indicator replenishIndicator) {
		this.replenishIndicator = replenishIndicator;
	}

	public Indicator getPrepaidIndicator() {
		return prepaidIndicator;
	}

	public void setPrepaidIndicator(Indicator prepaidIndicator) {
		this.prepaidIndicator = prepaidIndicator;
	}

	public boolean isResellerVisible() {
		return(getId() == CREDIT_CARD_PRESENT_ID || getId() == CREDIT_MANUAL_ENTRY_ID);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amountBounds == null) ? 0 : amountBounds.hashCode());
		result = prime * result + ((cashIndicator == null) ? 0 : cashIndicator.hashCode());
		result = prime * result + ((cashlessDeviceTranIndicator == null) ? 0 : cashlessDeviceTranIndicator.hashCode());
		result = prime * result + ((chargebackEnabledIndicator == null) ? 0 : chargebackEnabledIndicator.hashCode());
		result = prime * result + ((creditIndicator == null) ? 0 : creditIndicator.hashCode());
		result = prime * result + ((customerDebitIndicator == null) ? 0 : customerDebitIndicator.hashCode());
		result = prime * result + ((entryType == null) ? 0 : entryType.hashCode());
		result = prime * result + ((fillBatchIndicator == null) ? 0 : fillBatchIndicator.hashCode());
		result = prime * result + ((minBounds == null) ? 0 : minBounds.hashCode());
		result = prime * result + operatorTransType;
		result = prime * result + ((payableIndicator == null) ? 0 : payableIndicator.hashCode());
		result = prime * result + ((percentBounds == null) ? 0 : percentBounds.hashCode());
		result = prime * result + ((prepaidIndicator == null) ? 0 : prepaidIndicator.hashCode());
		result = prime * result + ((refundIndicator == null) ? 0 : refundIndicator.hashCode());
		result = prime * result + ((refundableIndicator == null) ? 0 : refundableIndicator.hashCode());
		result = prime * result + ((replenishIndicator == null) ? 0 : replenishIndicator.hashCode());
		result = prime * result + ((transItemType == null) ? 0 : transItemType.hashCode());
		result = prime * result + transTypeCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!super.equals(obj))
			return false;
		if(getClass() != obj.getClass())
			return false;
		ProcessFeeType other = (ProcessFeeType)obj;
		if(amountBounds == null) {
			if(other.amountBounds != null)
				return false;
		} else if(!amountBounds.equals(other.amountBounds))
			return false;
		if(cashIndicator != other.cashIndicator)
			return false;
		if(cashlessDeviceTranIndicator != other.cashlessDeviceTranIndicator)
			return false;
		if(chargebackEnabledIndicator != other.chargebackEnabledIndicator)
			return false;
		if(creditIndicator != other.creditIndicator)
			return false;
		if(customerDebitIndicator != other.customerDebitIndicator)
			return false;
		if(entryType != other.entryType)
			return false;
		if(fillBatchIndicator != other.fillBatchIndicator)
			return false;
		if(minBounds == null) {
			if(other.minBounds != null)
				return false;
		} else if(!minBounds.equals(other.minBounds))
			return false;
		if(operatorTransType != other.operatorTransType)
			return false;
		if(payableIndicator != other.payableIndicator)
			return false;
		if(percentBounds == null) {
			if(other.percentBounds != null)
				return false;
		} else if(!percentBounds.equals(other.percentBounds))
			return false;
		if(prepaidIndicator != other.prepaidIndicator)
			return false;
		if(refundIndicator != other.refundIndicator)
			return false;
		if(refundableIndicator != other.refundableIndicator)
			return false;
		if(replenishIndicator != other.replenishIndicator)
			return false;
		if(transItemType == null) {
			if(other.transItemType != null)
				return false;
		} else if(!transItemType.equals(other.transItemType))
			return false;
		if(transTypeCode != other.transTypeCode)
			return false;
		return true;
	}

	@Override
	public String toString() {
		//return String.format("ProcessFeeType[id=%s, name=%s, percentBounds=%s, amountBounds=%s, minBounds=%s, payableIndicator=%s, customerDebitIndicator=%s, entryType=%s, fillBatchIndicator=%s, cashIndicator=%s, transTypeCode=%s, transItemType=%s, refundIndicator=%s, refundableIndicator=%s, chargebackEnabledIndicator=%s, creditIndicator=%s, operatorTransType=%s, cashlessDeviceTranIndicator=%s, replenishIndicator=%s, prepaidIndicator=%s]", getId(), getName(), percentBounds, amountBounds, minBounds, payableIndicator, customerDebitIndicator, entryType, fillBatchIndicator, cashIndicator, transTypeCode, transItemType, refundIndicator, refundableIndicator, chargebackEnabledIndicator, creditIndicator, operatorTransType, cashlessDeviceTranIndicator, replenishIndicator, prepaidIndicator);
		return String.valueOf(getId());
	}
}
