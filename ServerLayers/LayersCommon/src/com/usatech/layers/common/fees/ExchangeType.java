package com.usatech.layers.common.fees;

public enum ExchangeType {
	STANDARD_RATE("SD", "Standard Rate"),
	BUY_RATE("BR", "Buy Rate"),
	SELL_RATE("SR", "Sell Rate"),
	COMMISSION("CM", "Commission");

	private final String id;
	private final String desc;

	ExchangeType(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public String toString() {
		return getId();
	}

	public boolean isBuyRate() {
		return this == BUY_RATE;
	}

	public boolean isSellRate() {
		return this == SELL_RATE;
	}

	public boolean isCommission() {
		return this == COMMISSION;
	}

	public boolean isStandardRate() {
		return this == STANDARD_RATE;
	}

	public static ExchangeType getById(String id) {
		for(ExchangeType exchangeType: ExchangeType.values()) {
			if(exchangeType.getId().equalsIgnoreCase(id))
				return exchangeType;
		}

		throw new IllegalArgumentException("Invalid ExchangeType " + id);
	}
}
