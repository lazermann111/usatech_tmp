package com.usatech.layers.common.fees;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.MapInputForm;

public class LicenseFees extends Fees {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static final Log log = Log.getLog();

	private long licenseId;
	private String title;
	private String description;
	private LicenseType licenseType;
	private long parentLicenseId;

	private LicenseFees(long licenseId) {
		this.licenseId = licenseId;
		setShowZerosAsBlank(true);
	}

	public long getLicenseId() {
		return licenseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LicenseType getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(LicenseType licenseType) {
		this.licenseType = licenseType;
	}

	public long getParentLicenseId() {
		return parentLicenseId;
	}

	public void setParentLicenseId(long parentLicenseId) {
		this.parentLicenseId = parentLicenseId;
	}

	@Override
	public boolean hasCommissions() {
		return licenseType != null && licenseType != LicenseType.STANDARD;
	}

	@Override
	public void writeHtmlTable(PageContext pageContext, boolean isReseller) throws IOException {
		JspWriter out = pageContext.getOut();

		out.println("<table class=\"fields\" style=\"width:100%;\">");
		out.println("  <tr class=\"tableHeader\">");
		out.println("    <th style=\"border-right:1px solid white;\"></th>");
		for(ExchangeType exchangeType: ExchangeType.values()) {
			if(exchangeType.isStandardRate())
				continue;
			out.println("    <th colspan=\"3\" style=\"border-right:1px solid white; border-bottom:1px solid white; text-align:center;\">" + exchangeType.getDesc() + "</th>");
		}
		out.println("  </tr>");
		out.println("  <tr class=\"tableHeader\">");
		out.println("    <th class=\"feeCell\" style=\"border-right:1px solid white;\">Fee Type</th>");
		for(ExchangeType exchangeType: ExchangeType.values()) {
			if(exchangeType.isStandardRate())
				continue;
			for(RateType rateType: RateType.values()) {
				out.println("    <th class=\"feeCell\" style=\"" + (rateType == RateType.MIN ? "border-right: 1px solid white;" : "") + "\">" + rateType.getDesc() + "</th>");
			}
		}
		out.println("  </tr>");
		for(ProcessFeeType feeType: getDistinctProcessFeeTypes()) {
			if(isReseller && !feeType.isResellerVisible())
				continue;
			out.println("  <tr class=\"tableDataShade\">");
			out.println("    <td style=\"white-space:nowrap;\">" + feeType.getName() + "<span class=\"required\">*</span></td>");
			for(ExchangeType exchangeType: ExchangeType.values()) {
				if(exchangeType.isStandardRate())
					continue;
				for(RateType rateType: RateType.values()) {
					Fee fee = this.getFee(EntryType.PROCESS_FEE, exchangeType, feeType, rateType);
					if(fee != null) {
						out.println("    <td class=\"feeCell\">" + fee.getRateType().getPrefix() + "<input type=\"text\" id=\"" + fee.getId() + "\" name=\"" + fee.getId() + "\" size=\"3\" value=\"" + fee.getFormattedValue() + "\"" + (fee.isInput() ? " onchange=\"sellRateChanged(this)\"" : " disabled=\"disabled\"") + " data-validators=\"validate-numeric " + (fee.isRequired() ? "required\"" : "\"") + " minvalue=\"0\"/>" + fee.getRateType().getSuffix() + "</td>");
					} else {
						out.println("    <td class=\"feeCell\"></td>");
					}
				}
			}
			out.println("  </tr>");
		}
		for(ServiceFeeType feeType: getDistinctServiceFeeTypes()) {
			if(isReseller && !feeType.isResellerVisible())
				continue;
			Frequency feeFrequency = getFee(EntryType.SERVICE_FEE, ExchangeType.BUY_RATE, feeType, RateType.FLAT).getFrequency();
			out.println("  <tr class=\"tableDataShade\">");
			out.println("    <td style=\"white-space:nowrap;\">" + feeType.getName() + "<br/>(" + feeFrequency + ")<span class=\"required\">*</span></td>");
			for(ExchangeType exchangeType: ExchangeType.values()) {
				if(exchangeType.isStandardRate())
					continue;
				out.println("    <td></td>");
				Fee fee = this.getFee(EntryType.SERVICE_FEE, exchangeType, feeType, RateType.FLAT);
				if(fee != null) {
					out.println("    <td class=\"feeCell\" colspan=\"2\">" + fee.getRateType().getPrefix() + "<input type=\"text\" id=\"" + fee.getId() + "\" name=\"" + fee.getId() + "\" size=\"5\" value=\"" + fee.getFormattedValue() + "\"" + (fee.isInput() ? " onchange=\"sellRateChanged(this)\"" : " disabled=\"disabled\"") + " data-validators=\"validate-numeric " + (fee.isRequired() ? "required\"" : "\"") + " minvalue=\"0\" />" + fee.getRateType().getSuffix() + "</td>");
				} else {
					out.println("    <td class=\"feeCell\" colspan=\"2\"></td>");
				}
			}
			out.println("  </tr>");
		}
		out.println("</table>");
	}

	public static LicenseFees loadByCustomerId(long customerId) throws DataLayerException, SQLException {
		Connection connection = DataLayerMgr.getConnectionForCall("GET_LICENSE_INFO_BY_CUSTOMER_ID_COMMON", true);
		try {
			return loadByCustomerId(connection, customerId);
		} finally {
			DbUtils.closeSafely(connection);
		}
	}

	public static LicenseFees loadByCustomerId(Connection connection, long customerId) throws DataLayerException, SQLException {
		String queryName = "GET_LICENSE_INFO_BY_CUSTOMER_ID_COMMON";

		Map<String, Object> licenseInfoParams = new HashMap<String, Object>();
		licenseInfoParams.put("CUSTOMER_ID", customerId);

		Results licenseInfoResults = DataLayerMgr.executeQuery(connection, queryName, licenseInfoParams);
		if(!licenseInfoResults.next())
			throw new SQLException(format("{0} returned no results for customer {1}", queryName, customerId));

		LicenseFees fees = null;

		try {
			fees = new LicenseFees(licenseInfoResults.getValue("LICENSE_ID", Long.class));
			fees.setTitle(licenseInfoResults.getFormattedValue("TITLE"));
			fees.setDescription(ConvertUtils.getString(licenseInfoResults.getValue("DESCRIPTION"), null));
			fees.setLicenseType(LicenseType.getById(licenseInfoResults.getFormattedValue("LICENSE_TYPE")));
			fees.setParentLicenseId(ConvertUtils.getLong(licenseInfoResults.getValue("PARENT_LICENSE_ID"), 0));

			loadFees(connection, fees);
		} catch(Exception e) {
			throw new SQLException(format("{0} returned invalid results for customer {1}", queryName, customerId), e);
		}

		return fees;
	}

	public static LicenseFees loadByLicenseId(long customerId) throws DataLayerException, SQLException {
		Connection connection = DataLayerMgr.getConnectionForCall("GET_LICENSE_INFO_BY_LICENSE_ID_COMMON", true);
		try {
			return loadByLicenseId(connection, customerId);
		} finally {
			DbUtils.closeSafely(connection);
		}
	}

	public static LicenseFees loadByLicenseId(Connection connection, long licenseId) throws DataLayerException, SQLException {
		String queryName = "GET_LICENSE_INFO_BY_LICENSE_ID_COMMON";

		Map<String, Object> licenseInfoParams = new HashMap<String, Object>();
		licenseInfoParams.put("LICENSE_ID", licenseId);

		Results licenseInfoResults = DataLayerMgr.executeQuery(connection, queryName, licenseInfoParams);
		if(!licenseInfoResults.next())
			throw new SQLException(format("{0} returned no results for license {1}", queryName, licenseId));

		LicenseFees fees = null;

		try {
			fees = new LicenseFees(licenseId);
			fees.setTitle(licenseInfoResults.getFormattedValue("TITLE"));
			fees.setDescription(ConvertUtils.getString(licenseInfoResults.getValue("DESCRIPTION"), null));
			fees.setLicenseType(LicenseType.getById(licenseInfoResults.getFormattedValue("LICENSE_TYPE")));
			fees.setParentLicenseId(ConvertUtils.getLong(licenseInfoResults.getValue("PARENT_LICENSE_ID"), 0));

			loadFees(connection, fees);
		} catch(Exception e) {
			throw new SQLException(format("{0} returned invalid results for license {1}", queryName, licenseId), e);
		}

		return fees;
	}

	private static void loadFees(Connection connection, LicenseFees fees) throws DataLayerException, SQLException, ConvertException {
		String queryName = "GET_LICENSE_FEES_COMMON";

		Map<String, Object> licenseFeesParams = new HashMap<String, Object>();
		licenseFeesParams.put("LICENSE_ID", fees.getLicenseId());

		Results licenseFeesResults = DataLayerMgr.executeQuery(connection, queryName, licenseFeesParams);
		if(!licenseFeesResults.next())
			throw new SQLException(format("{0} returned no results for license {1}", queryName, fees.getLicenseId()));

		fees.loadFromResults(licenseFeesResults, true);
	}

	public long createChildLicense(String partnerName, String customerName) throws DataLayerException, SQLException {
		Connection connection = DataLayerMgr.getConnectionForCall("CREATE_CHILD_LICENSE_COMMON", true);
		try {
			return createChildLicense(connection, partnerName, customerName);
		} finally {
			DbUtils.closeSafely(connection);
		}
	}

	public long createChildLicense(Connection connection, String partnerName, String customerName) throws DataLayerException, SQLException {
		String parentName = partnerName.length() > 15 ? partnerName.substring(0, 15) : partnerName;
		String childName = customerName.length() > 15 ? customerName.substring(0, 15) : customerName;
		String title = format("Generated for {0} by {1}", childName, parentName);
		String description = format("Reseller generated merchant license created on {0} by {1} for {2}", DateFormat.getDateTimeInstance().format(new Date()), partnerName, customerName);

		InputForm form = new MapInputForm(new HashMap<String, Object>());
		form.setAttribute("DEALER_NAME", title);
		form.setAttribute("LICENSE_TITLE", title);
		form.setAttribute("DESCRIPTION", description);
		form.setAttribute("TYPE", "C"); // C = child/commissioned license
		form.setAttribute("PARENT_LICENSE_ID", licenseId);

		saveToForm(form, false);

		// out params from stored proc is new dealerId, licenseId 
		Object[] createLicenseResults;
		try {
			createLicenseResults = DataLayerMgr.executeCall(connection, "CREATE_CHILD_LICENSE_COMMON", form);
		} catch(Exception e) {
			throw new SQLException("CREATE_CHILD_LICENSE_COMMON returned unexpected results", "", 20200);
		}

		if(createLicenseResults == null || createLicenseResults.length != 3) {
			throw new SQLException("CREATE_CHILD_LICENSE_COMMON returned unexpected results");
		}

		setClean();

		try {
			return ConvertUtils.getLong(createLicenseResults[2]);
		} catch(Exception e) {
			throw new SQLException(format("CREATE_CHILD_LICENSE_COMMON returned unexpected results: {0}", createLicenseResults[2]));
		}
	}

	public void saveToDatabase() throws DataLayerException, SQLException {
		Connection connection = DataLayerMgr.getConnectionForCall("LICENSE_PF_UPDATE_COMMON", true);
		try {
			saveToDatabase(connection);
		} finally {
			DbUtils.closeSafely(connection);
		}
	}

	public void saveToDatabase(Connection connection) throws DataLayerException, SQLException {
		// for now, only allow updating of the sell rate on a commissioned license
		// when we start using this for DMS, we'll have to allow updating of both buy and sell rates on a reseller license
		if(!hasCommissions()) {
			throw new UnsupportedOperationException("Saving commissioned license sell rates only is supported.");
		}

		ProcessFeeType cardPresent = ProcessFeeType.getById(ProcessFeeType.CREDIT_CARD_PRESENT_ID);
		ProcessFeeType manualEntry = ProcessFeeType.getById(ProcessFeeType.CREDIT_MANUAL_ENTRY_ID);
		ServiceFeeType standardMonthly = ServiceFeeType.getById(ServiceFeeType.STANDARD_SERVICE_FEE_ID);

		saveProcessFee(connection, getLicenseId(), cardPresent, getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.PERCENT), getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.FLAT), getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.MIN));

		saveProcessFee(connection, getLicenseId(), manualEntry, getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, manualEntry, RateType.PERCENT), getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, manualEntry, RateType.FLAT), getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, manualEntry, RateType.MIN));

		saveServiceFee(connection, getLicenseId(), getFee(EntryType.SERVICE_FEE, ExchangeType.SELL_RATE, standardMonthly, RateType.FLAT));

		setClean();
	}

	private static void saveProcessFee(Connection connection, long licenseId, ProcessFeeType feeType, Fee percentFee, Fee flatFee, Fee minFee) throws DataLayerException, SQLException {
		InputForm form = new MapInputForm(new HashMap<String, Object>());
		form.setAttribute("LICENSE_ID", licenseId);
		form.setAttribute("TRANS_TYPE_ID", feeType.getId());
		form.setAttribute(RateType.PERCENT.getId(), percentFee.getValue());
		form.setAttribute(RateType.FLAT.getId(), flatFee.getValue());
		form.setAttribute(RateType.MIN.getId(), minFee.getValue());

		DataLayerMgr.executeCall(connection, "LICENSE_PF_UPDATE_COMMON", form);
	}

	private static void saveServiceFee(Connection connection, long licenseId, Fee serviceFee) throws DataLayerException, SQLException {
		InputForm form = new MapInputForm(new HashMap<String, Object>());
		form.setAttribute("LICENSE_ID", licenseId);
		form.setAttribute("FEE_ID", serviceFee.getFeeType().getId());
		form.setAttribute(serviceFee.getRateType().getId(), serviceFee.getValue()); // this has to be FLAT
		form.setAttribute("FREQUENCY_ID", serviceFee.getFrequency().getId());

		DataLayerMgr.executeCall(connection, "LICENSE_SF_UPDATE_COMMON", form);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int)(licenseId ^ (licenseId >>> 32));
		result = prime * result + ((licenseType == null) ? 0 : licenseType.hashCode());
		result = prime * result + (int)(parentLicenseId ^ (parentLicenseId >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!super.equals(obj))
			return false;
		if(getClass() != obj.getClass())
			return false;
		LicenseFees other = (LicenseFees)obj;
		if(description == null) {
			if(other.description != null)
				return false;
		} else if(!description.equals(other.description))
			return false;
		if(licenseId != other.licenseId)
			return false;
		if(licenseType != other.licenseType)
			return false;
		if(parentLicenseId != other.parentLicenseId)
			return false;
		if(title == null) {
			if(other.title != null)
				return false;
		} else if(!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("LicenseFees[licenseId=%s, title=%s, description=%s, licenseType=%s, parentLicenseId=%s, fees=[%s]]", licenseId, title, description, licenseType, parentLicenseId, super.toString());
	}

}
