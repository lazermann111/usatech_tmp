package com.usatech.layers.common.fees;

import java.io.Serializable;

public abstract class FeeType implements Serializable {
	private static final long serialVersionUID = 1L;

	public static FeeType getById(EntryType entryType, int id) {
		if(entryType.isProcessFee())
			return ProcessFeeType.getById(id);
		else if(entryType.isServiceFee())
			return ServiceFeeType.getById(id);
		else throw new IllegalArgumentException(String.format("EntryType {0} for ID {1} is not a FeeType", entryType, id));
	}

	private int id;
	private String name;
	private int displayOrder;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public abstract boolean isResellerVisible();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + displayOrder;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		FeeType other = (FeeType)obj;
		if(displayOrder != other.displayOrder)
			return false;
		if(id != other.id)
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		return true;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
}
