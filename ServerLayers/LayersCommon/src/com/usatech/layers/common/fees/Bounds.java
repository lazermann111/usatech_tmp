package com.usatech.layers.common.fees;

import java.io.Serializable;

public class Bounds implements Serializable {
	private static final long serialVersionUID = 1L;

	private double lower;
	private double upper;

	public Bounds() {
		this(0, Double.MAX_VALUE);
	}

	public Bounds(double upper) {
		this(0, upper);
	}

	public Bounds(double lower, double upper) {
		this.lower = lower;
		this.upper = upper;
	}

	public double getLower() {
		return lower;
	}

	public void setLower(double lower) {
		this.lower = lower;
	}

	public double getUpper() {
		return upper;
	}

	public void setUpper(double upper) {
		this.upper = upper;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(lower);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(upper);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Bounds other = (Bounds)obj;
		if(Double.doubleToLongBits(lower) != Double.doubleToLongBits(other.lower))
			return false;
		if(Double.doubleToLongBits(upper) != Double.doubleToLongBits(other.upper))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("(%.2f to %.2f)", lower, upper);
	}
}
