package com.usatech.layers.common.fees;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class FeeId implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final Pattern ID_PATTERN = Pattern.compile("FEE_([A-Z]{2})_([A-Z]{2})_([0-9]{1,3})_([PFM]{1})");

	private final EntryType entryType;
	private final ExchangeType exchangeType;
	private final FeeType feeType;
	private final RateType rateType;
	private final String key;

	public FeeId(EntryType entryType, ExchangeType exchangeType, FeeType feeType, RateType rateType) {
		this.entryType = entryType;
		this.exchangeType = exchangeType;
		this.feeType = feeType;
		this.rateType = rateType;

		key = FeeId.createKey(entryType, exchangeType, feeType, rateType);
	}

	public FeeType getFeeType() {
		return feeType;
	}

	public EntryType getEntryType() {
		return entryType;
	}

	public ExchangeType getExchangeType() {
		return exchangeType;
	}

	public RateType getRateType() {
		return rateType;
	}

	public String getKey() {
		return key;
	}

	public boolean isProcessFee() {
		return getEntryType().isProcessFee();
	}

	public boolean isServiceFee() {
		return getEntryType().isServiceFee();
	}

	public boolean isBuyRate() {
		return getExchangeType().isBuyRate();
	}

	public boolean isSellRate() {
		return getExchangeType().isSellRate();
	}

	public boolean isCommission() {
		return getExchangeType().isCommission();
	}

	public static String createKey(EntryType entryType, ExchangeType exchangeType, FeeType feeType, RateType rateType) {
		// ORDER:
		// 1. EntryType
		// 2. ExchangeType
		// 3. FeeType
		// 4. Rate Type

		// Example: PF_SR_16_P

		return String.format("FEE_%s_%s_%s_%s", entryType.getId(), exchangeType.getId(), feeType.getId(), rateType.getId());
	}

	public static FeeId createId(String key) throws ConvertException {
		Matcher matcher = ID_PATTERN.matcher(key);
		if(!matcher.matches()) {
			throw new IllegalArgumentException("Invalid FeeId key: " + key);
		}

		return createId(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4));
	}

	public static FeeId createId(String entryTypeStr, String exchangeTypeStr, String feeTypeStr, String rateTypeStr) throws ConvertException {
		return createId(entryTypeStr, exchangeTypeStr, ConvertUtils.getInt(feeTypeStr), rateTypeStr);
	}

	public static FeeId createId(String entryTypeStr, String exchangeTypeStr, int feeTypeInt, String rateTypeStr) throws ConvertException {
		EntryType entryType = EntryType.getById(entryTypeStr);
		ExchangeType exchangeType = ExchangeType.getById(exchangeTypeStr);
		FeeType feeType = FeeType.getById(entryType, feeTypeInt);
		RateType rateType = RateType.getById(rateTypeStr);

		return new FeeId(entryType, exchangeType, feeType, rateType);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entryType == null) ? 0 : entryType.hashCode());
		result = prime * result + ((exchangeType == null) ? 0 : exchangeType.hashCode());
		result = prime * result + ((feeType == null) ? 0 : feeType.hashCode());
		result = prime * result + ((rateType == null) ? 0 : rateType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		FeeId other = (FeeId)obj;
		if(entryType != other.entryType)
			return false;
		if(exchangeType != other.exchangeType)
			return false;
		if(feeType == null) {
			if(other.feeType != null)
				return false;
		} else if(!feeType.equals(other.feeType))
			return false;
		if(rateType != other.rateType)
			return false;
		return true;
	}

	public String toString() {
		return key;
	}

}
