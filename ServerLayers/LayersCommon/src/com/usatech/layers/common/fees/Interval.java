package com.usatech.layers.common.fees;

public enum Interval {
	HOURLY("HH", "Hourly"),
	DAILY("DD", "Daily"),
	WEEKLY("DY", "Weekly"),
	MONTHLY("MM", "Monthly"),
	YEARLY("YY", "Yearly");

	private final String id;
	private final String desc;

	Interval(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public String toString() {
		return getId();
	}

	public static Interval getById(String id) {
		for(Interval interval: Interval.values()) {
			if(interval.getId().equalsIgnoreCase(id))
				return interval;
		}

		throw new IllegalArgumentException("Invalid Interval " + id);
	}
}
