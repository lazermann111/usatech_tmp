package com.usatech.layers.common.fees;

public enum LicenseType {
	STANDARD("S", "Standard"),
	RESELLER("R", "Reseller"),
	COMMISSIONED("C", "Commission Added");

	private final String id;
	private final String desc;

	LicenseType(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public String toString() {
		return getId();
	}

	public static LicenseType getById(String id) {
		for(LicenseType licenseType: LicenseType.values()) {
			if(licenseType.getId().equalsIgnoreCase(id))
				return licenseType;
		}

		throw new IllegalArgumentException("Invalid LicenseType " + id);
	}
}
