package com.usatech.layers.common;

public class EdgeInitMessageProcessor extends CommonProcessing {
    protected DeviceInfoManager deviceInfoManager;
    protected long encryptionKeyMinAgeMin;

	public DeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(DeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}

	public long getEncryptionKeyMinAgeMin() {
		return encryptionKeyMinAgeMin;
	}

	public void setEncryptionKeyMinAgeMin(long encryptionKeyMinAgeMin) {
		this.encryptionKeyMinAgeMin = encryptionKeyMinAgeMin;
	}
}
