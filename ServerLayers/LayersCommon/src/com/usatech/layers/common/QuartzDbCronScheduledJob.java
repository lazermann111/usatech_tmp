package com.usatech.layers.common;

import java.sql.SQLException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
/**
 * Pass in jobDataMap(dbCallId) to exsute a db call cron.
 * @author yhe
 *
 */
@DisallowConcurrentExecution
public class QuartzDbCronScheduledJob extends QuartzCronScheduledJob {
	
	private static final Log log = Log.getLog();

	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		String dbCallId=null;
		try {
			JobDataMap quartzJobMap = context.getMergedJobDataMap();
			dbCallId=quartzJobMap.getString("dbCallId");
			if (dbCallId == null)  
				dbCallId = jobDataMap.getProperty("dbCallid");
			DataLayerMgr.executeUpdate(dbCallId, null, true);
		} catch(SQLException e) {
			log.error("Could not execute db dbCallId="+dbCallId, e);
		} catch(DataLayerException e) {
			log.error("Could not execute db dbCallId="+dbCallId, e);
		}

	}

}
