package com.usatech.layers.common;


import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

/**
 * This class creates and publishes a message chain
 * 
 * @author bkrug
 * 
 */
@DisallowConcurrentExecution
public class KickoffJob extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected static final Pattern REFERENCE_ATTRIBUTE_PATTERN = Pattern.compile("(\\d+):(.+)");

	public static class StepConfig {
		protected String queueKey;
		protected final Map<String, Object> attributes = new HashMap<>();
		protected final Map<String, String> referenceAttributes = new HashMap<>();
		protected final Map<Integer, int[]> nextSteps = new HashMap<>();

		public String getQueueKey() {
			return queueKey;
		}

		public void setQueueKey(String queueKey) {
			this.queueKey = queueKey;
		}

		public Map<String, Object> getAttribute() {
			return attributes;
		}

		public Map<String, String> getReferenceAttribute() {
			return referenceAttributes;
		}

		public int[] getNextStep(int resultCode) {
			return nextSteps.get(resultCode);
		}

		public void setNextStep(int resultCode, int[] stepIndexes) {
			nextSteps.put(resultCode, stepIndexes);
		}
	}

	protected final SortedMap<Integer, StepConfig> stepsConfig = new TreeMap<>();

	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		if(stepsConfig.isEmpty()) {
			log.error("No Steps have been configured for job " + context.getJobDetail().getKey());
			return;
		}
		MessageChain mc = new MessageChainV11();
		Map<Integer, MessageChainStep> steps = new HashMap<>();
		for(Map.Entry<Integer, StepConfig> entry : stepsConfig.entrySet()) {
			MessageChainStep step = steps.get(entry.getKey());
			if(step == null) {
				step = mc.addStep(null);
				steps.put(entry.getKey(), step);
			}
			step.setQueueKey(entry.getValue().getQueueKey());
			step.getAttributes().putAll(entry.getValue().attributes);
			for(Map.Entry<String, String> raEntry : entry.getValue().referenceAttributes.entrySet()) {
				Matcher raMatcher = REFERENCE_ATTRIBUTE_PATTERN.matcher(raEntry.getValue());
				if(!raMatcher.matches()) {
					log.warn("ReferenceAttribute '" + raEntry.getValue() + "' is not formatted correctly. Use <referenced step index>:<resultAttributeName>; skipping");
					continue;
				}
				int referencedIndex;
				try {
					referencedIndex = ConvertUtils.getInt(raMatcher.group(1));
				} catch(ConvertException e) {
					log.warn("Could not extract referenced step index from '" + raEntry.getValue() + "'; skipping", e);
					continue;
				}
				MessageChainStep referencedStep = steps.get(referencedIndex);
				if(referencedStep == null) {
					referencedStep = mc.addStep(null);
					steps.put(referencedIndex, referencedStep);
				}

				step.addReferenceAttribute(raEntry.getKey(), referencedStep, raMatcher.group(2));
			}
			for(Map.Entry<Integer, int[]> nextEntry : entry.getValue().nextSteps.entrySet()) {
				if(nextEntry.getValue() != null && nextEntry.getValue().length > 0) {
					MessageChainStep[] nextSteps = new MessageChainStep[nextEntry.getValue().length];
					for(int i = 0; i < nextSteps.length; i++) {
						MessageChainStep nextStep = steps.get(nextEntry.getValue()[i]);
						if(nextStep == null) {
							nextStep = mc.addStep(null);
							steps.put(nextEntry.getValue()[i], nextStep);
						}
						nextSteps[i] = nextStep;
					}
					step.setNextSteps(nextEntry.getKey(), nextSteps);
				}

			}
		}
		try {
			MessageChainService.publish(mc, publisher);
		} catch(ServiceException e) {
			throw new JobExecutionException("Failed to kickoff message chain", e);
		} 
	}

	public StepConfig getStepConfig(int index) {
		return stepsConfig.get(index);
	}

	public void setStepConfig(int index, StepConfig config) {
		stepsConfig.put(index, config);
	}
}
