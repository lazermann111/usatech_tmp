package com.usatech.layers.common;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.util.CompositeMap;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.task.SQLExceptionHandler;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;

public class InsertReplenishTask extends AuthInsertTask {
	private static final Log log = Log.getLog();

	@Override
	public int process(MessageChainTaskInfo taskInfo) {
		final MessageChainStep step = taskInfo.getStep();
		AuthResultCode result;
		String responseCd;
		String responseDesc;
		DeniedReason deniedReason = null;
		String id = getCallId();
		if(id == null || id.trim().length() == 0) {
			result = AuthResultCode.FAILED;
			responseCd = DeniedReason.APP_LAYER_PROCESSING_ERROR.toString();
			responseDesc = "CallId property is not set on " + getClass().getSimpleName();
			log.error(responseDesc);
		} else {
			// get attributes from message data
			Connection conn;
			try {
				conn = DataLayerMgr.getConnectionForCall(id);
				try {
					result = process(conn, step.getAttributes(), step.getResultAttributes(), taskInfo.isRedelivered(), taskInfo.getPublisher());
					responseCd = step.getAttributeSafely(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class, null);
					responseDesc = step.getAttributeSafely(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class, null);

					try {
						conn.commit();
					} catch(SQLException e) {
						ProcessingUtils.rollbackDbConnection(log, conn);
						result = AuthResultCode.FAILED;
						deniedReason = DeniedReason.APP_LAYER_PROCESSING_ERROR;
						responseCd = deniedReason.toString();
						responseDesc = "Could not commit changes for " + getClass().getSimpleName();
						log.error(responseDesc, e);
					}
				} catch(ServiceException e) {
					ProcessingUtils.rollbackDbConnection(log, conn);
					result = AuthResultCode.FAILED;
					deniedReason = DeniedReason.APP_LAYER_PROCESSING_ERROR;
					responseCd = deniedReason.toString();
					responseDesc = e.getMessage() + " for " + getClass().getSimpleName();
					log.error(responseDesc, e);
				} finally {
					ProcessingUtils.closeDbConnection(log, conn);
				}
			} catch(SQLException e) {
				result = AuthResultCode.FAILED;
				deniedReason = DeniedReason.APP_LAYER_PROCESSING_ERROR;
				responseCd = deniedReason.toString();
				responseDesc = "Could not get database connection for " + getClass().getSimpleName();
				log.error(responseDesc, e);
			} catch(DataLayerException e) {
				result = AuthResultCode.FAILED;
				deniedReason = DeniedReason.APP_LAYER_PROCESSING_ERROR;
				responseCd = deniedReason.toString();
				responseDesc = "Could not get database connection for " + getClass().getSimpleName();
				log.error(responseDesc, e);
			}
		}
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, result.getValue());
		if(deniedReason != null)
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, deniedReason);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, responseCd);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, responseDesc);
		switch(result) {
			case AVS_MISMATCH: // allow AVS MIS-MATCH
			case APPROVED:
			case PARTIAL:
				break;
			default:
				String clientTextKey = "client.message.auth.replenish.denied-" + (deniedReason == null ? "UNKNOWN" : deniedReason.toString());
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, clientTextKey);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, new Object[0]);
		}

		return determineResultInt(result, step);
	}

	@Override
	protected void authInserted(Connection conn, AuthResultCode result, Map<String, Object> attributes, Map<String, Object> resultAttributes) throws ServiceException, DataLayerException {
		switch(result) {
			case AVS_MISMATCH: // allow AVS MIS-MATCH
			case APPROVED:
			case PARTIAL:
				CompositeMap<String, Object> merged;
				if(attributes instanceof CompositeMap) {
					merged = (CompositeMap<String, Object>) attributes;
					merged.merge(0, resultAttributes);
				} else {
					merged = new CompositeMap<String, Object>();
					merged.merge(resultAttributes);
					merged.merge(attributes);
				}

				try {
					DataLayerMgr.executeCall(conn, "INSERT_REPLENISHMENT", merged, resultAttributes);
				} catch(SQLException e) {
					SQLExceptionHandler handler = getSqlExceptionHandler();
					if(handler != null)
						handler.handleSQLException("INSERT_REPLENISHMENT", attributes, e);
					else
						throw new ServiceException(e);
				}
				int minorCurrencyFactor = ConvertUtils.getIntSafely(attributes.get("minorCurrencyFactor"), 0);
				BigDecimal saleAmount = ConvertUtils.convertSafely(BigDecimal.class, resultAttributes.get("saleAmount"), null);
				if(saleAmount != null)
					resultAttributes.put("saleAmount", saleAmount.multiply(BigDecimal.valueOf(minorCurrencyFactor)).longValue());
				BigDecimal replenishBalanceAmount = ConvertUtils.convertSafely(BigDecimal.class, resultAttributes.get("replenishBalanceAmount"), null);
				if(replenishBalanceAmount != null)
					resultAttributes.put("replenishBalanceAmount", replenishBalanceAmount.multiply(BigDecimal.valueOf(minorCurrencyFactor)).longValue());

				resultAttributes.put("sessionUpdateNeeded", 'Y');
				resultAttributes.put("tranImportNeeded", 'Y');
				resultAttributes.put("saleGlobalSessionCode", attributes.get("globalSessionCode"));
				resultAttributes.put("saleSessionStartTime", attributes.get("authTime"));
				resultAttributes.put("tranLineItemCount", 1);
				break;
			case CVV_MISMATCH:
			case CVV_AND_AVS_MISMATCH:
				if(attributes instanceof CompositeMap) {
					merged = (CompositeMap<String, Object>) attributes;
					merged.merge(0, resultAttributes);
				} else {
					merged = new CompositeMap<String, Object>();
					merged.merge(resultAttributes);
					merged.merge(attributes);
				}

				try {
					DataLayerMgr.executeCall(conn, "CANCEL_REPLENISHMENT", merged, resultAttributes);
				} catch(SQLException e) {
					SQLExceptionHandler handler = getSqlExceptionHandler();
					if(handler != null)
						handler.handleSQLException("CANCEL_REPLENISHMENT", attributes, e);
					else
						throw new ServiceException(e);
				}
				resultAttributes.put("sessionUpdateNeeded", 'Y');
				resultAttributes.put("tranImportNeeded", 'N');
				resultAttributes.put("saleGlobalSessionCode", attributes.get("globalSessionCode"));
				resultAttributes.put("saleSessionStartTime", attributes.get("authTime"));
				resultAttributes.put("tranLineItemCount", 0);
				break;
		}
	}
}
