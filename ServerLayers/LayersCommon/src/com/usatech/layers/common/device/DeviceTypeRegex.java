package com.usatech.layers.common.device;

import java.util.regex.Pattern;

public class DeviceTypeRegex {
	private byte deviceTypeId;
	private String deviceTypeDesc;
	private String deviceTypeSerialCdRegex;
	private Pattern deviceTypeSerialCdPattern;
	
	public byte getDeviceTypeId() {
		return deviceTypeId;
	}
	public void setDeviceTypeId(byte deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	public String getDeviceTypeDesc() {
		return deviceTypeDesc;
	}
	public void setDeviceTypeDesc(String deviceTypeDesc) {
		this.deviceTypeDesc = deviceTypeDesc;
	}
	public String getDeviceTypeSerialCdRegex() {
		return deviceTypeSerialCdRegex;
	}
	public void setDeviceTypeSerialCdRegex(String deviceTypeSerialCdRegex) {
		this.deviceTypeSerialCdRegex = deviceTypeSerialCdRegex;
		deviceTypeSerialCdPattern = Pattern.compile(deviceTypeSerialCdRegex);
	}
	public Pattern getDeviceTypeSerialCdPattern() {
		return deviceTypeSerialCdPattern;
	}
	public void setDeviceTypeSerialCdPattern(Pattern deviceTypeSerialCdPattern) {
		this.deviceTypeSerialCdPattern = deviceTypeSerialCdPattern;
	}
}
