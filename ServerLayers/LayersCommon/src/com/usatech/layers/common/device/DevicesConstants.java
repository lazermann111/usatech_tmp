/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.layers.common.device;

import java.util.Set;
import java.util.TreeSet;

import com.usatech.layers.common.constants.DeviceType;

public class DevicesConstants
{
    /** Don't let anyone instantiate this class. */
    private DevicesConstants()
    {}
    
    public static int GX_MAP_CALL_SERIAL_NUMBER = 0;
    public static int GX_MAP_ENCRYPTION_KEY = 144;
    public static int GX_MAP_DEVICE_NAME = 162;
    public static int GX_MAP_CALL_IN_TIME = 172;
    public static int GX_MAP_CALL_HOME_NOW_FLAG = 210;
    public static int GX_MAP_VENDOR_INTERFACE_TYPE = 241;
    public static int GX_MAP_CONVENIENCE_FEE = 362;
    public static int GX_MAP_IP_ADDRESS = 284;
    public static int GX_MAP_PORT_NUMBER = 136;
    public static int GX_MAP_CURRENCY_TRAN_COUNTER = 320;
    public static int GX_MAP_CURRENCY_MONEY_COUNTER = 324;
    public static int GX_MAP_CASHLESS_TRAN_COUNTER = 328;
    public static int GX_MAP_CASHLESS_MONEY_COUNTER = 332;
    public static int GX_MAP_PASSCARD_TRAN_COUNTER = 336;
    public static int GX_MAP_PASSCARD_MONEY_COUNTER = 340;
    public static int GX_MAP_SESSION_COUNTER = 352;
    public static int GX_MAP_TRAN_COUNTER = 344;
    public static int GX_MAP_BYTE_COUNTER = 348;
    
    public static int MEI_MAP_CALL_IN_TIME = 17;
    public static int MEI_MAP_SIM_NUMBER = 28;
    public static int MEI_MAP_GSM_MODEM_PHONE_NUMBER = 46;
    
    public static int MEI_CONFIG_SIZE = 57;
    
    public static final Set<Integer> GX_MAP_PROTECTED_FIELDS = new TreeSet<Integer>();
    public static final Set<Integer> GX_MAP_IMPORT_PROTECTED_FIELDS = new TreeSet<Integer>();
    public static final Set<Integer> GX_MAP_COIN_PULSE_FIELDS = new TreeSet<Integer>();
    
    public static final Set<Integer> EDGE_COIN_PULSE_FIELDS = new TreeSet<Integer>();
    
    static {
    	GX_MAP_PROTECTED_FIELDS.add(GX_MAP_CALL_SERIAL_NUMBER);
    	GX_MAP_PROTECTED_FIELDS.add(GX_MAP_ENCRYPTION_KEY);
    	GX_MAP_PROTECTED_FIELDS.add(GX_MAP_DEVICE_NAME);
    	
    	GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_CALL_SERIAL_NUMBER);
    	GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_ENCRYPTION_KEY);
    	GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_DEVICE_NAME);
    	GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_IP_ADDRESS);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_PORT_NUMBER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_CURRENCY_TRAN_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_CURRENCY_MONEY_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_CASHLESS_TRAN_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_CASHLESS_MONEY_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_PASSCARD_TRAN_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_PASSCARD_MONEY_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_SESSION_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_TRAN_COUNTER);
        GX_MAP_IMPORT_PROTECTED_FIELDS.add(GX_MAP_BYTE_COUNTER);
    	
    	GX_MAP_COIN_PULSE_FIELDS.add(184);
    	GX_MAP_COIN_PULSE_FIELDS.add(240);
    	GX_MAP_COIN_PULSE_FIELDS.add(241);
    	GX_MAP_COIN_PULSE_FIELDS.add(242);
    	GX_MAP_COIN_PULSE_FIELDS.add(243);
    	GX_MAP_COIN_PULSE_FIELDS.add(258);
    	GX_MAP_COIN_PULSE_FIELDS.add(260);
    	GX_MAP_COIN_PULSE_FIELDS.add(266);
    	GX_MAP_COIN_PULSE_FIELDS.add(272);
    	GX_MAP_COIN_PULSE_FIELDS.add(274);
    	GX_MAP_COIN_PULSE_FIELDS.add(276);
    	GX_MAP_COIN_PULSE_FIELDS.add(278);
    	GX_MAP_COIN_PULSE_FIELDS.add(280);
    	GX_MAP_COIN_PULSE_FIELDS.add(282);
    	GX_MAP_COIN_PULSE_FIELDS.add(390);
    	GX_MAP_COIN_PULSE_FIELDS.add(402);
    	GX_MAP_COIN_PULSE_FIELDS.add(414);
    	GX_MAP_COIN_PULSE_FIELDS.add(426);
    	GX_MAP_COIN_PULSE_FIELDS.add(438);
    	GX_MAP_COIN_PULSE_FIELDS.add(450);
    	GX_MAP_COIN_PULSE_FIELDS.add(462);
    	GX_MAP_COIN_PULSE_FIELDS.add(474);
    	
    	EDGE_COIN_PULSE_FIELDS.add(1500);
    	EDGE_COIN_PULSE_FIELDS.add(1501);
    	EDGE_COIN_PULSE_FIELDS.add(1502);
    	EDGE_COIN_PULSE_FIELDS.add(1503);
    	EDGE_COIN_PULSE_FIELDS.add(1504);
    	EDGE_COIN_PULSE_FIELDS.add(1505);
    	EDGE_COIN_PULSE_FIELDS.add(1506);
    	EDGE_COIN_PULSE_FIELDS.add(1507);
    	EDGE_COIN_PULSE_FIELDS.add(1508);
    	EDGE_COIN_PULSE_FIELDS.add(1509);
    	EDGE_COIN_PULSE_FIELDS.add(1510);
    	EDGE_COIN_PULSE_FIELDS.add(1511);
    	EDGE_COIN_PULSE_FIELDS.add(1512);
    	EDGE_COIN_PULSE_FIELDS.add(1513);
    	EDGE_COIN_PULSE_FIELDS.add(1514);
    	EDGE_COIN_PULSE_FIELDS.add(1515);
    	EDGE_COIN_PULSE_FIELDS.add(1516);
    	EDGE_COIN_PULSE_FIELDS.add(1517);
    	EDGE_COIN_PULSE_FIELDS.add(1518);
    	EDGE_COIN_PULSE_FIELDS.add(1519);
    	EDGE_COIN_PULSE_FIELDS.add(1520);
    	EDGE_COIN_PULSE_FIELDS.add(1521);
    	EDGE_COIN_PULSE_FIELDS.add(1522);
    	EDGE_COIN_PULSE_FIELDS.add(1523);
    	EDGE_COIN_PULSE_FIELDS.add(1524);
    	EDGE_COIN_PULSE_FIELDS.add(1525);
    	EDGE_COIN_PULSE_FIELDS.add(1526);
    }    
    
    public static final String[] letter = new String[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    public static final String[] number = new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    public static final Integer[] device_type_id_for_firmware = new Integer[] {Integer.valueOf(DeviceType.G4.getValue()), Integer.valueOf(DeviceType.GX.getValue())};
    public static final String[] counter_names_for_edge = new String[] {
    	"CASHLESS_TRANSACTION","CASHLESS_MONEY","CURRENCY_TRANSACTION","CURRENCY_MONEY",
    	"I_CASHLESS_TRANSACTION","I_CASHLESS_MONEY","I_CURRENCY_TRANSACTION","I_CURRENCY_MONEY",
    	"P_CASHLESS_TRANSACTION", "P_CASHLESS_MONEY", "P_CURRENCY_TRANSACTION", "P_CURRENCY_MONEY" };
    public static final String[] counter_names_for_other = new String[] {
    	"CASHLESS_TRANSACTION", "CURRENCY_TRANSACTION","PASSCARD_TRANSACTION", "TOTAL_SESSIONS",
    	"CASHLESS_MONEY", "CURRENCY_MONEY",  "PASSCARD_MONEY", "TOTAL_BYTES"};
    
    public static final boolean PARAM_USE_DB_PROCESS = true;
    public static final int PAYMENT_CONFIG_POS_PTA_CUSTOM_YEAR_LIMIT = 1;
    
    public static final String NULL = "NULL";
    public static final String CHANGE_HISTORY__OBJECT_TYPE_CD = "change_history__object_type_cd";
    public static final String CHANGE_HISTORY__OBJECT_CD = "change_history__object_cd";
    public static final String PARAM_BEZEL_APP_VER = "bezel_app_ver";
    public static final String PARAM_BEZEL_APP_VER_PREFIX = "bezel_app_ver_prefix";
    public static final String PARAM_BEZEL_MFGR = "bezel_mfgr";
    public static final String PARAM_BEZEL_MFGR_PREFIX = "bezel_mfgr_prefix";
    public static final String STORED_DEVICE = "com.usatech.dms.device";
    public static final String PARAM_SEARCH_PARAM = "search_param";
    public static final String PARAM_SEARCH_TYPE = "search_type";
    public static final String PARAM_EV_NUMBER = "ev_number";
    public static final String PARAM_SERIAL_NUMBER = "serial_number";
    public static final String PARAM_COMM_METHOD = "comm_method";
    public static final String PARAM_CONSUMER_ID = "consumer_id";
    public static final String PARAM_CONSUMER_ACCT_ID = "consumer_acct_id";
    public static final String PARAM_CREDENTIAL_ID = "credential_id";
    public static final String PARAM_DIAG_APP_VERSION = "diag_app_version";
    public static final String PARAM_DIAG_APP_VERSION_PREFIX = "diag_app_version_prefix";
    public static final String PARAM_FIRMWARE_VERSION = "firmware_version";
    public static final String PARAM_FIRMWARE_VERSION_PREFIX = "firmware_version_prefix";
    public static final String PARAM_PTEST_VERSION = "ptest_version";
    public static final String PARAM_PTEST_VERSION_PREFIX = "ptest_version_prefix";
    public static final String PARAM_HOST_SERIAL_NUMBER = "host_serial_number";
    public static final String PARAM_PROPERTY_LIST_VERSION = "plv";
    public static final String PARAM_CUSTOMER_ID = "customer_id";
    public static final String PARAM_CORP_CUSTOMER_NAME = "corp_customer_name";
    public static final String PARAM_CUSTOMER_NAME = "customer_name";
    public static final String PARAM_LOCATION_ID = "location_id";
    public static final String PARAM_LOCATION_NAME = "location_name";
    public static final String PARAM_ENABLED = "enabled";
    public static final String PARAM_DEVICE_ID = "device_id";
    public static final String PARAM_DEVICE_TYPE_ID = "device_type_id";
    public static final String PARAM_COUNTER_COUNT = "counter_count";
    public static final String PARAM_USER_OP = "userOP";
    public static final String PARAM_DEVICE_NAME = "device_name";
    public static final String PARAM_EVENT_FROM_DATE = "event_from_date";
    public static final String PARAM_EVENT_FROM_TIME = "event_from_time";
    public static final String PARAM_EVENT_TO_DATE = "event_to_date";
    public static final String PARAM_EVENT_TO_TIME = "event_to_time";
    public static final String PARAM_FILE_ID = "file_id";
    public static final String PARAM_TRAN_FROM_DATE = "transaction_from_date";
    public static final String PARAM_TRAN_FROM_TIME = "transaction_from_time";
    public static final String PARAM_TRAN_TO_DATE = "transaction_to_date";
    public static final String PARAM_TRAN_TO_TIME = "transaction_to_time";
    public static final String PARAM_PTA_DUPLICATE_SETTINGS = "pos_pta_duplicate_settings";
    public static final String PARAM_POS_ID = "pos_id";
    public static final String PARAM_PAYMENT_SUBTYPE_ID = "payment_subtype_id";
    public static final String PARAM_POS_OLD_DATA = "pos_old_data";
    public static final String PARAM_MERCHANT_ID = "merchant_id";
    public static final String PARAM_TERMINAL_ID = "terminal_id";
    public static final String PARAM_TERMINAL_NUMBER = "terminal_number";
    public static final String PARAM_TERMINAL_BATCH_ID = "terminal_batch_id";
    public static final String PARAM_AUTHORITY_ID = "authority_id";
    public static final String PARAM_POS_PTA_ID = "pos_pta_id";
    public static final String PARAM_TEMPLATE_ID = "template_id";
    public static final String PARAM_MODE_CD = "mode_cd";
    public static final String PARAM_ORDER_CD = "order_cd";
    public static final String PARAM_POS_PTA_TMPL_ID = "pos_pta_tmpl_id";
    public static final String PARAM_POS_PTA_TMPL_ENTRY_ID = "pos_pta_tmpl_entry_id";
    public static final String PARAM_PAYMENT_ACTION_TYPE_CD = "payment_action_type_cd";
    public static final String PARAM_PAYMENT_ENTRY_METHOD_CD = "payment_entry_method_cd";
    public static final String PARAM_ACTION = "action";
    public static final String PARAM_START_SERIAL_NUMBER = "start_serial_number";
    
    public static final String DEFAULT_CONFIG_FILE_EDGE_NAME_ROOT = "DEFAULT-CFG-13";

    public static final String COLUMN_POS_PTA_ENCRYPT_KEY = "pos_pta_encrypt_key";
    public static final String COLUMN_POS_PTA_ENCRYPT_KEY2 = "pos_pta_encrypt_key2";
    public static final String COLUMN_PAYMENT_SUBTYPE_KEY_ID = "payment_subtype_key_id";
    public static final String COLUMN_PTA_DEVICE_SERIAL_CD = "pos_pta_device_serial_cd";
    public static final String COLUMN_PTA_PIN_REQ_YN_FLAG = "pos_pta_pin_req_yn_flag";
    public static final String COLUMN_POS_PTA_REGEX = "pos_pta_regex";
    public static final String COLUMN_PTA_REGEX_BREF = "pos_pta_regex_bref";
    public static final String COLUMN_AUTHORITY_PAYMENT_MASK_ID = "authority_payment_mask_id";
    public static final String COLUMN_POS_PTA_PASSTHRU_ALLOW_YN_FLAG = "pos_pta_passthru_allow_yn_flag";
    public static final String COLUMN_POS_PTA_DISABLE_DEBIT_DENIAL = "pos_pta_disable_debit_denial";
    public static final String COLUMN_NO_CONVENIENCE_FEE = "no_convenience_fee";
    public static final String COLUMN_POS_PTA_PREF_AUTH_AMT = "pos_pta_pref_auth_amt";
    public static final String COLUMN_POS_PTA_PREF_AUTH_AMT_MAX = "pos_pta_pref_auth_amt_max";
    public static final String COLUMN_CURRENCY_CD = "currency_cd";

    public static final String USER_OP_LIST_ALL = "list_all";
    public static final String USER_OP_LIST_COUNTERS = "list_counters";
    public static final String USER_OP_LIST_EVENTS = "list_events";
    public static final String USER_OP_LIST_SETTINGS = "list_settings";
    public static final String USER_OP_LIST_TRAN = "list_tran";

    public static final int GX_MAP_AUTH_MODE = 387;
    public static final int GX_MAP_MDB_INVENTORY_FORMAT = 389;
    
    public static final String CONFIG_TYPE_KIOSK = "kiosk";
    public static final String CONFIG_TYPE_T2 = "t2";
    public static final String CONFIG_TYPE_EDGE = "edge";
}