/** 
 * USA Technologies, Inc. 2011
 * DeviceConfigurationUtils.java by phorsfield, Sep 2, 2011 4:00:41 PM
 */
package com.usatech.layers.common.device;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.StringHelper;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

/**
 * <p>Utilities to parse flexibly parse device configuration data.
 * Leverages the DMS UI to configure this data.</p>
 * 
 * <p>This class was sourced from some DMS functionality that 
 * needed to be moved into a place where it can be utilitized 
 * by the App Layer.</p>
 *  
 * @author phorsfield
 *
 */
public class DeviceConfigurationUtils {
	private static final simple.io.Log log = simple.io.Log.getLog();

	/**For files that are of "map" type (fixed width file with specific length and location for each parameter value), parse the file,
	 * and load the value onto a ConfigTemplateSetting object.
	 * 
	 * @param defaultConfData
	 * @param fileData
	 * @param deviceTypeId
	 * @param fileName
	 * @return
	 */
	public static LinkedHashMap<String, String> parseMapFileData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, String fileData, boolean returnRawHex)
	{
	   LinkedHashMap<String, String> deviceSettings = new LinkedHashMap<String, String>();
	   for (Map.Entry<String, ConfigTemplateSetting> entry : defaultConfData.entrySet()) {
		   ConfigTemplateSetting data = entry.getValue();
		   if (!data.isServerOnly())
			   deviceSettings.put(data.getKey(), parseFixedWidthField(fileData, data, returnRawHex));
	   }
	   return deviceSettings;
	}

    /**
     * Parse a field from a fixed width map type file.
     * @param fileData
     * @param data
     * @return
     */
	public static String parseFixedWidthField(String fileData, ConfigTemplateSetting data, boolean returnRawHex) {
		if (data.getFieldOffset() < 0)
			return null;
		int startAddr = data.getFieldOffset();
		int size = data.getFieldSize();
		int endAddr = startAddr + size;
		String locationData;
		if (fileData != null && fileData.length() >= endAddr)
			locationData = fileData.substring(startAddr, endAddr);
		else {
			if (returnRawHex)
				return "";
			locationData = "";
		}
		if (returnRawHex)
			return StringHelper.encodeHexString(locationData);
		else
			return parseFixedWidthLocationData(locationData, data, false);
	}
	
	public static String parseFixedWidthLocationData(String locationData, ConfigTemplateSetting data, boolean isHex) {
		if (data.getFieldOffset() < 0)
			return null;
		int startAddr = data.getFieldOffset();
		String padChar = data.getPadChar();
		String align = data.getAlign();
		String editor = data.getEditor();
				
		if("H".equalsIgnoreCase(data.getDataMode())) {
			if (isHex) {
				if (locationData != null && !locationData.equals(locationData.toUpperCase()))
					locationData = locationData.toUpperCase();
			} else
				locationData = StringHelper.encodeHexString(locationData);
		    if (padChar != null) {
		    	if (" ".equalsIgnoreCase(padChar))
		    		padChar = "0";
		    	else
		    		padChar = padChar.toUpperCase();
		    }
		} else if (isHex)
			locationData = StringHelper.decodeHexString(locationData);			
		
		switch(startAddr) {
			case DevicesConstants.GX_MAP_AUTH_MODE:
				locationData = "4E";
				break;
			case DevicesConstants.GX_MAP_MDB_INVENTORY_FORMAT:
				locationData = "59";
				break;
		}
		
		if (editor != null && editor.startsWith("TEXT")) {
			/*
			 * Strip out any pad characters.
			 */
			Matcher matcher;
			if (align != null && padChar != null && !" ".equalsIgnoreCase(padChar)) {
			    if ("R".equalsIgnoreCase(align) || "C".equalsIgnoreCase(align)) {
			    	matcher = Pattern.compile(new StringBuilder("^(").append(padChar).append(")+").toString()).matcher(locationData);
			    	if (matcher.find())
			    		locationData = matcher.replaceAll("");
			    }
			    
			    if ("L".equalsIgnoreCase(align) || "C".equalsIgnoreCase(align)) {
			    	matcher = Pattern.compile(new StringBuilder("(").append(padChar).append(")+$").toString()).matcher(locationData);
			    	if (matcher.find())
			    		locationData = matcher.replaceAll("");
			    }
			}
		}

		data.setLocationData(locationData);
		return locationData;
	}	
    
    public static LinkedHashMap<String, ConfigTemplateSetting> getDefaultConfFileDataByDeviceTypeId(int deviceTypeId, Integer propertyListVersion, String orderBy, String customerEditable, Long deviceId, Connection conn) throws ServiceException
    {
    	LinkedHashMap<String, ConfigTemplateSetting> defaultConfData =  new LinkedHashMap<String, ConfigTemplateSetting>(); 
    	ConfigTemplateSetting data = null;
    	if (deviceTypeId < 0)
    		throw new ServiceException("Invalid deviceTypeId: " + deviceTypeId);
    		    	
        Results rs = null;
        try {
        	HashMap<String, Object> params = new HashMap<String, Object>();
        	params.put("device_type_id", deviceTypeId);
        	params.put("plv", propertyListVersion);
        	params.put("orderBy", orderBy);
        	params.put("customer_editable", customerEditable);
        	params.put("device_id", deviceId);
        	if (conn == null)
        		rs = DataLayerMgr.executeQuery("GET_DEFAULT_DEVICE_CONFIG_BY_DEVICE_TYPE_ID", params, false);
        	else
        		rs = DataLayerMgr.executeQuery(conn, "GET_DEFAULT_DEVICE_CONFIG_BY_DEVICE_TYPE_ID", params);
            while(rs.next()){
            	data = new ConfigTemplateSetting();
            	rs.fillBean(data);
            	if (data.getConfigTemplateSettingValue() == null)
            		data.setConfigTemplateSettingValue("");
            	if(deviceTypeId == DeviceType.G4.getValue() || deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.MEI.getValue()) {
            		String value = DeviceConfigurationUtils.parseFixedWidthLocationData(data.getConfigTemplateSettingValue(), data, true);
            		if (StringHelper.isDataPrintable(value))
            			data.setConfigTemplateSettingValue(value);
            	}
            	defaultConfData.put(data.getKey(), data);
            }
        }catch(Exception e){
        	throw new ServiceException("Error getting default config file data, deviceTypeId: " + deviceTypeId + ", propertyListVersion: " + propertyListVersion, e);
        } finally {
        	if (rs != null)
        		rs.close();
        }
        return defaultConfData;
    }
    
    public static LinkedHashMap<String, ConfigTemplateSetting> getDefaultConfFileDataByDeviceTypeId(int deviceTypeId, Integer propertyListVersion, String orderBy, Connection conn) throws ServiceException {
    	return getDefaultConfFileDataByDeviceTypeId(deviceTypeId, propertyListVersion, orderBy, null, null, conn);
    }
    
    public static LinkedHashMap<String, ConfigTemplateSetting> getDefaultConfFileDataByDeviceTypeId(int deviceTypeId, Integer propertyListVersion, String orderBy) throws ServiceException {
    	return getDefaultConfFileDataByDeviceTypeId(deviceTypeId, propertyListVersion, orderBy, null, null, null);
    }
    
    
    public static LinkedHashMap<String, ConfigTemplateSetting> getDefaultConfFileDataByDeviceTypeId(int deviceTypeId, Integer propertyListVersion) throws ServiceException {
    	return getDefaultConfFileDataByDeviceTypeId(deviceTypeId, propertyListVersion, null, null, null, null);
    }    

	/**
	 * Insert a new Device Setting Parameter.
	 * @param params
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public static void insertDeviceSetting(Map<String, Object> params, Connection conn) throws SQLException, DataLayerException {
		if(conn == null)
			DataLayerMgr.executeCall("INSERT_NEW_DEVICE_SETTING", params, true);
		else
			DataLayerMgr.executeCall(conn, "INSERT_NEW_DEVICE_SETTING", params);
	}

	public static void upsertDeviceSetting(long deviceId, String deviceSettingParameterCd, String deviceSettingValue, Integer fileOrder, Connection conn, String userName) throws ServiceException {
		try
		{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("deviceId", deviceId);
			params.put("deviceSettingParameterCd", deviceSettingParameterCd);
			params.put("deviceSettingValue", deviceSettingValue);			
			params.put("fileOrder", fileOrder);
			params.put("changedBy", userName);
			
			if(conn == null)
				DataLayerMgr.executeCall("UPSERT_DEVICE_SETTING", params, true);
			else
				DataLayerMgr.executeCall(conn, "UPSERT_DEVICE_SETTING", params);
		}
		catch (Exception e)
		{
		    throw new ServiceException("Error upserting device setting for deviceId: " + deviceId + ", deviceSettingParameterCd: " + deviceSettingParameterCd + ", deviceSettingValue: " + deviceSettingValue, e);
		}
	}
	
	public static void upsertDeviceSetting(long deviceId, String deviceSettingParameterCd, String deviceSettingValue, Integer fileOrder, Connection conn) throws ServiceException {
		upsertDeviceSetting(deviceId, deviceSettingParameterCd, deviceSettingValue, fileOrder, conn, null);
	}
	
	public static void upsertDeviceSetting(long deviceId, String deviceSettingParameterCd, String deviceSettingValue, Connection conn, String userName) throws ServiceException {
		upsertDeviceSetting(deviceId, deviceSettingParameterCd, deviceSettingValue, null, conn, userName);
	}
	
	public static void upsertDeviceSetting(long deviceId, String deviceSettingParameterCd, String deviceSettingValue, Connection conn) throws ServiceException {
		upsertDeviceSetting(deviceId, deviceSettingParameterCd, deviceSettingValue, null, conn, null);
	}
	
	public static void upsertConfigTemplateSetting(long configTemplateId, String deviceSettingParameterCd, String configTemplateSettingValue, Connection conn) throws ServiceException {
		try
		{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("CONFIG_TEMPLATE_ID", configTemplateId);
			params.put("DEVICE_SETTING_PARAMETER_CD", deviceSettingParameterCd);
			params.put("CONFIG_TEMPLATE_SETTING_VALUE", configTemplateSettingValue);
			
			if(conn == null)
				DataLayerMgr.executeCall("UPSERT_CONFIG_TEMPLATE_SETTING", params, true);
			else
				DataLayerMgr.executeCall(conn, "UPSERT_CONFIG_TEMPLATE_SETTING", params);
		}
		catch (Exception e)
		{
		    throw new ServiceException("Error upserting config template setting for configTemplateId: " + configTemplateId + ", deviceSettingParameterCd: " + deviceSettingParameterCd + ", configTemplateSettingValue: " + configTemplateSettingValue, e);
		}
	}	

	public static boolean saveDeviceSettingData(long deviceId, Map<String, String> deviceSettingData, boolean deleteOldDeviceSettings, boolean deleteOldServerSettings, Connection conn, String userName) throws ConvertException, DataLayerException, SQLException, ServiceException {
		boolean change = false;
		boolean success = false;
		boolean newConnection = false;
		Set<String> processedKeys = new HashSet<String>();
		try {
			if (conn == null) {
	    		conn = DataLayerMgr.getConnection("OPER");
	    		newConnection = true;
	    	}
			Results deviceSettings = DataLayerMgr.executeQuery(conn, "GET_CONFIGURABLE_DEVICE_SETTINGS", new Object[] {deviceId});
			int fileOrder = 0;
			while (deviceSettings.next()) {
				fileOrder++;
				String key = deviceSettings.getValue("device_setting_parameter_cd", String.class);
				if (!deviceSettingData.containsKey(key)) {					
					boolean serverOnly = deviceSettings.getValue("is_server_only", boolean.class);
					if (deleteOldDeviceSettings && !serverOnly || deleteOldServerSettings && serverOnly) {
						DataLayerMgr.executeCall(conn, "DELETE_DEVICE_SETTING", new Object[] {deviceId, key});
						change = true;
					}
				} else {
					String value = deviceSettings.getValue("device_setting_value", String.class);
					if (value == null)
						value = "";
					String newValue = deviceSettingData.get(key);
					if (newValue == null)
						newValue = "";
					if (!value.equals(newValue)) {
						change = true;
						upsertDeviceSetting(deviceId, key, newValue, fileOrder, conn, userName);
					}
					processedKeys.add(key);
				}
			}
			
			Set<String> keys = deviceSettingData.keySet();
			Iterator<String> i = keys.iterator();
			while(i.hasNext()){
				fileOrder++;
				String key = i.next();
				if (!processedKeys.contains(key)) {
					change = true;
					upsertDeviceSetting(deviceId, key, deviceSettingData.get(key), fileOrder, conn, userName);
					processedKeys.add(key);
				}
			}
			success = true;
		} finally {
			if (newConnection) {
	        	if (success)
	        		conn.commit();
	        	else
	    			ProcessingUtils.rollbackDbConnection(log, conn);    		
	    		ProcessingUtils.closeDbConnection(log, conn);
	        }
		}
		return change;
	}
	
	public static boolean saveDeviceSettingData(long deviceId, Map<String, String> deviceSettingData, boolean deleteOldDeviceSettings, boolean deleteOldServerSettings, Connection conn) throws ConvertException, DataLayerException, SQLException, ServiceException {
		return saveDeviceSettingData(deviceId, deviceSettingData, deleteOldDeviceSettings, deleteOldServerSettings, conn, null);
	}
	
	public static boolean saveDeviceSettingData(long deviceId, Map<String, String> deviceSettingData, boolean deleteOldServerSettings, Connection conn) throws ConvertException, DataLayerException, SQLException, ServiceException {
		return saveDeviceSettingData(deviceId, deviceSettingData, true, deleteOldServerSettings, conn);
	}
	
	public static boolean saveConfigTemplateSettingData(long configTemplateId, Map<String, String> templateSettingData, boolean deleteOldServerSettings, Connection conn) throws ConvertException, DataLayerException, SQLException, ServiceException {
		boolean change = false;
		Set<String> processedKeys = new HashSet<String>();
		Results templateSettings = DataLayerMgr.executeQuery(conn, "GET_CONFIGURABLE_CONFIG_TEMPLATE_SETTINGS", new Object[] {configTemplateId});
		while (templateSettings.next()) {
			String key = templateSettings.getValue("device_setting_parameter_cd", String.class);
			if (!templateSettingData.containsKey(key)) {
				change = true;
				if (deleteOldServerSettings || !templateSettings.getValue("is_server_only", boolean.class))
					DataLayerMgr.executeCall(conn, "DELETE_CONFIG_TEMPLATE_SETTING", new Object[] {configTemplateId, key});
			} else {
				String value = templateSettings.getValue("config_template_setting_value", String.class);
				if (value == null)
					value = "";
				String newValue = templateSettingData.get(key);
				if (newValue == null)
					newValue = "";
				if (!value.equals(newValue)) {
					change = true;
					upsertConfigTemplateSetting(configTemplateId, key, newValue, conn);
				}
				processedKeys.add(key);
			}
		}
		
		Set<String> keys = templateSettingData.keySet();
		Iterator<String> i = keys.iterator();
		while(i.hasNext()){
			String key = i.next();
			if (!processedKeys.contains(key)) {
				change = true;
				upsertConfigTemplateSetting(configTemplateId, key, templateSettingData.get(key), conn);
				processedKeys.add(key);
			}
		}
		return change;
	}	
}
