package com.usatech.layers.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import simple.text.StringUtils;

public class Cryption {
	private static SecureRandom random;
    private static String randomError;
	static {
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
		} catch(Exception e) {
			randomError = e.getMessage();
		}
	}
	public static final Provider DEFAULT_PROVIDER = Security.getProvider("SunJCE");
	public static final String DEFAULT_CIPHER_NAME = "AES/CBC/NOPADDING";
    
	protected String cipherName = DEFAULT_CIPHER_NAME;
	protected Provider provider = DEFAULT_PROVIDER;
	protected int keyLength = 16;
	protected int blockSize = 16;
	
	public byte[] generateKey() throws GeneralSecurityException {
		return generateKey(getKeyLength());
	}
	
	public CipherOutputStream createEncryptingOutputStream(byte[] key, OutputStream out) throws GeneralSecurityException {    
		return createEncryptingOutputStream(getProvider(), getCipherName(), key, getBlockSize(), out);
	}
	
	public CipherInputStream createDecryptingInputStream(byte[] key, InputStream in) throws GeneralSecurityException {
		return createDecryptingInputStream(getProvider(), getCipherName(), key, getBlockSize(), in);
	}
	
	public static byte[] generateKey(int length) throws GeneralSecurityException {
		byte[] key = new byte[length];
		if(random == null)
			throw new GeneralSecurityException("SecureRandom could not be initialized: " + randomError);
		random.nextBytes(key);
		return key;
	}
	
	public static CipherOutputStream createEncryptingOutputStream(String providerName, String cipherName, byte[] key, int blockSize, OutputStream out) throws GeneralSecurityException {
    	Provider provider = Security.getProvider(providerName);
        if (provider == null)
            throw new GeneralSecurityException("Provider not found: " + providerName);
        return createEncryptingOutputStream(provider, cipherName, key, blockSize, out);
	}
	public static CipherOutputStream createEncryptingOutputStream(Provider provider, String cipherName, byte[] key, int blockSize, OutputStream out) throws GeneralSecurityException {    
        Cipher cipher = Cipher.getInstance(cipherName, provider);
        if (cipher == null)
            throw new GeneralSecurityException("Cipher not found: " + provider.getName());
        String[] cipherParts = StringUtils.split(cipherName, '/');
		String algorithm;
		String padding = null;
		switch(cipherParts.length) {
			case 3:
				padding = cipherParts[2];
				//fall-through
			case 1:
			case 2:
				algorithm = cipherParts[0];
				break;
			default:
				throw new GeneralSecurityException("Invalid cipherName '" + cipherName + "': must be of the form 'algorithm/encoding/padding'");
		}
		final boolean needsNullPadding = (padding == null || padding.length() == 0 || "NOPADDING".equalsIgnoreCase(padding));
		SecretKeySpec keySpec = new SecretKeySpec(key, algorithm);
		final byte[] nullBytes = new byte[blockSize];
        IvParameterSpec ivSpec = new IvParameterSpec(nullBytes);        
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        return new CipherOutputStream(out, cipher) {
        	@Override
        	public void close() throws IOException {
        		// write padding if necessary
        		if(needsNullPadding) {
        			write(nullBytes, 0, nullBytes.length - 1);
        		}
				flush(); // DO NOT ignore exceptions as FilterOutputStream does
				out.close();
        	}
        };
    }

    public static CipherInputStream createDecryptingInputStream(String providerName, String cipherName, byte[] key, int blockSize, InputStream in) throws GeneralSecurityException {
    	Provider provider = Security.getProvider(providerName);
        if (provider == null)
            throw new GeneralSecurityException("Provider not found: " + providerName);
        return createDecryptingInputStream(provider, cipherName, key, blockSize, in);
    }
    public static CipherInputStream createDecryptingInputStream(Provider provider, String cipherName, byte[] key, int blockSize, InputStream in) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(cipherName, provider);
        if (cipher == null)
            throw new GeneralSecurityException("Cipher not found: " + provider.getName());
        String algorithm = StringUtils.substringBefore(cipherName, "/");
        SecretKeySpec keySpec = new SecretKeySpec(key, algorithm);
        IvParameterSpec ivSpec = new IvParameterSpec(new byte[blockSize]);        
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
        return new CipherInputStream(in, cipher);
    }

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	public void setProvider(String providerName) throws GeneralSecurityException {
		Provider p = Security.getProvider(providerName);
		if (p == null)
            throw new GeneralSecurityException("Provider not found: " + providerName);
        this.provider = p;
	}

	public int getKeyLength() {
		return keyLength;
	}

	public void setKeyLength(int keyLength) {
		this.keyLength = keyLength;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public String getCipherName() {
		return cipherName;
	}

	public void setCipherName(String cipherName) {
		this.cipherName = cipherName;
	}
}
