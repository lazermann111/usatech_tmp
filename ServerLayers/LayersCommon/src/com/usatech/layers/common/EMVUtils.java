package com.usatech.layers.common;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

public class EMVUtils {

	private static final Log log = Log.getLog();

	public enum DownloadRequestStatus {
		INITIAL("I"),
		SUBSEQUENT("S"),
		DOWNLOAD_SUCCESSFUL("Y"),
		DOWNLOAD_FAILED("N");
		private String value;
		DownloadRequestStatus(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}
	public enum ResponseStatusInd {
		MORE_TO_FOLLOW("M"),
		FINAL_BUFFER("F");
		private String value;
		ResponseStatusInd(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}
	

	public static void processEMVParameterDownloadRequiredFlag(long paymentSubtypeKeyId) {
		Map<String, Object>params = new HashMap<String, Object>();
		params.put("terminalId", paymentSubtypeKeyId);
		try {
			DataLayerMgr.executeUpdate("INSERT_EMV_PARAMETER_DOWNLOAD_REQD", params, true);
		} catch (SQLException | DataLayerException e) {
			log.error("Could not insert emv parameter dowload request for terminal " + paymentSubtypeKeyId, e);
		}
	}
	
	public static String initialRetrievalReferenceNumber() {
		return "00000000";
	}
	
	public static String determineDownloadRequestStatus(String responseStatusInd) {
		String parameterDownloadRequestStatus = null;
		if (responseStatusInd == null) {
			parameterDownloadRequestStatus = DownloadRequestStatus.INITIAL.getValue();
		} else if (responseStatusInd.equals(ResponseStatusInd.MORE_TO_FOLLOW.getValue())) {
			parameterDownloadRequestStatus = DownloadRequestStatus.SUBSEQUENT.getValue();
		} else if (responseStatusInd.equals(ResponseStatusInd.FINAL_BUFFER.getValue())) {
			parameterDownloadRequestStatus = DownloadRequestStatus.DOWNLOAD_SUCCESSFUL.getValue();
		}
		return parameterDownloadRequestStatus;
	}

	public static String emvResourceName() {
		String dateStr = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS").format(new Date());
		return "emv-param-dl-" + dateStr;
	}

}
