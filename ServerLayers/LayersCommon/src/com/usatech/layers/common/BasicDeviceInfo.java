/**
 *
 */
package com.usatech.layers.common;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.util.CollectionUtils;
import simple.util.IncludingMap;
import simple.util.IntegerRangeSet;
import simple.util.OptimizedIntegerRangeSet;
import simple.util.UnmodifiableIntegerRangeSet;

import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;

/**
 * @author Brian S. Krug
 *
 */
public abstract class BasicDeviceInfo implements DeviceInfo {
	protected class ByteArrayComparable implements Comparable<ByteArrayComparable> {
		protected final byte[] value;

		public ByteArrayComparable(byte[] value) {
			this.value = value;
		}

		/**
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(ByteArrayComparable bc) {
			byte[] o = (bc == null ? null : bc.getValue());
			if(value == null) {
				if(o == null)
					return 0;
				return -1;
			} else if(o == null) {
				return 1;
			} else {
				for(int i = 0; i < value.length && i < o.length; i++) {
					int c = value[i] - o[i];
					if(c != 0)
						return c;
				}
				return value.length - o.length;
			}
		}

		public byte[] getValue() {
			return value;
		}
	}
	protected static class StampedComparable<T extends Comparable<T>> {
		protected T value;
		protected long timestamp;
		public StampedComparable(T value, long timestamp) {
			this.value = value;
			this.timestamp = timestamp;
		}
		protected int compareTo(T value) {
			return compare(this.value, value);
		}
		public Comparable<T> getValue() {
			return value;
		}
		public boolean updateIfDifferent(T value, long timestamp) {
			if(timestamp < this.timestamp)
				return false;
			this.timestamp = timestamp;
			if(compareTo(value) == 0)
				return false;
			this.value = value;
			return true;
		}
		public boolean updateIfGreater(T value, T limit, long timestamp) {
			if(timestamp < this.timestamp)
				return false;
			if(compareTo(value) >= 0)
				return false;
			if(limit != null && value.compareTo(limit) > 0)
				return false;
			this.timestamp = timestamp;
			this.value = value;
			return true;
		}
	}
	private static final Log log = Log.getLog();
	protected static final char FS = 0x1C;
	protected static <T extends Comparable<T>> int compare(T value1, T value2) {
		if(value1 == null) {
			if(value2 == null)
				return 0;
			return -1;
		} else if(value2 == null) {
			return 1;
		} else {
			return value1.compareTo(value2);
		}
	}
	protected final String deviceName;
	protected String deviceInfoHash = null;
	public static final Set<String> attributeKeys;
	static {
		Set<String> set = new HashSet<String>();
		for(DeviceInfoProperty dip : DeviceInfoProperty.values()) {
			set.add(dip.getAttributeKey());
		}
		attributeKeys = Collections.unmodifiableSet(set);
	}
	protected final Map<DeviceInfoProperty,Comparable<?>> updatedProperties = new HashMap<DeviceInfoProperty,Comparable<?>>();
	protected final Set<DeviceInfoProperty> updateTimestamp = new HashSet<DeviceInfoProperty>();
	protected final Map<DeviceInfoProperty,StampedComparable<?>> committedProperties = new HashMap<DeviceInfoProperty,StampedComparable<?>>();

	public BasicDeviceInfo(String deviceName) {
		this.deviceName = deviceName;
	}
	protected void acceptAndProcessChanges(long timestamp, boolean internalOnly, Connection targetConn) throws ServiceException {
		Map<String,Long> timestamps = CollectionUtils.singleValueMap(attributeKeys, timestamp);
		Map<String, Object> changes = acceptChanges(timestamps);
		if(!changes.isEmpty()) {
			processChanges(changes, CollectionUtils.singleValueMap(changes.keySet(), timestamp), internalOnly, targetConn);
		}
	}
	
	protected void acceptAndProcessChanges(Map<String,Long> timestamps, boolean internalOnly, Connection targetConn) throws ServiceException {
		Map<String, Object> changes = acceptChanges(timestamps);
		if(!changes.isEmpty()) {
			processChanges(changes, new IncludingMap<String, Long>(timestamps, changes.keySet()), internalOnly, targetConn);
		}
	}
	/**
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Map<String, Object> acceptChanges(Map<String,Long> timestamps) {
		Map<String,Object> changes = new HashMap<String, Object>();
		for(Map.Entry<DeviceInfoProperty, Comparable<?>> entry : updatedProperties.entrySet()) {
			if(entry.getValue() == null)
				continue;
			StampedComparable sc = committedProperties.get(entry.getKey());
			Long timestamp = timestamps.get(entry.getKey().getAttributeKey());
			if(timestamp == null)
				timestamp = 0L;
			if(sc == null) {
				committedProperties.put(entry.getKey(), new StampedComparable(entry.getValue(), timestamp));
				changes.put(entry.getKey().getAttributeKey(), unwrap(entry.getValue()));
			} else if(entry.getKey() == DeviceInfoProperty.MASTER_ID && isMasterIdIncrementOnly() ? sc.updateIfGreater(entry.getValue(), System.currentTimeMillis() / 1000 + ProcessingUtils.MAX_ID_ADVANCE, timestamp) : sc.updateIfDifferent(entry.getValue(), timestamp)) {
				changes.put(entry.getKey().getAttributeKey(), unwrap(entry.getValue()));
			}
			updateTimestamp.remove(entry.getKey());
		}
		updatedProperties.clear();
		for(DeviceInfoProperty dip : updateTimestamp) {
			StampedComparable sc = committedProperties.get(dip);
			Long timestamp = timestamps.get(dip.getAttributeKey());
			if(timestamp == null)
				timestamp = 0L;
			if(sc != null && sc.timestamp < timestamp) {
				sc.timestamp = timestamp;
				if(dip != DeviceInfoProperty.MASTER_ID)
					changes.put(dip.getAttributeKey(), unwrap(sc.getValue())); // so that we don't mess up device setting
			}
		}
		updateTimestamp.clear();
		if(!changes.isEmpty())
			setDeviceInfoHash(null);
		return changes;
	}
	/**
	 * @throws ServiceException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Map<String, Object> acceptChanges(Map<String,Object> values, Map<String,Long> timestamp) throws ServiceException {
		updatedProperties.clear(); // wipe out any pending changes
		for(DeviceInfoProperty dip : DeviceInfoProperty.values()) {
			Object value = values.get(dip.getAttributeKey());
			Long ts;
			if(value != null || ((ts = timestamp.get(dip.getAttributeKey())) != null && ts > 0)) {
				Object cvalue = ConvertUtils.convertSafely(dip.getAttributeType(), value, null);
				if(cvalue instanceof byte[] || (cvalue == null && byte[].class.isAssignableFrom(dip.getAttributeType()))) {
					internalUpdateIfDifferent(dip, new ByteArrayComparable((byte[])cvalue));
				} else if(cvalue instanceof Comparable || (cvalue == null && Comparable.class.isAssignableFrom(dip.getAttributeType()))) {
					internalUpdateIfDifferent(dip, (Comparable)cvalue);
				} else					
					log.warn("Converted " + cvalue + " to " + cvalue);
			}
		}
		if(!updatedProperties.isEmpty()) {
			return acceptChanges(timestamp);
		}
		return Collections.emptyMap();
	}
	public void clearChanges() {
		lock();
		try {
			updatedProperties.clear();
			updateTimestamp.clear();
		} finally {
			unlock();
		}
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#commitChanges()
	 */
	@Override
	public void commitChanges(long timestamp) throws ServiceException {
		commitChanges(timestamp, false);
	}

	@Override
	public void commitChanges(long timestamp, boolean internalOnly) throws ServiceException {
		lock();
		try {
			if(!updatedProperties.isEmpty() || !updateTimestamp.isEmpty()) {
				acceptAndProcessChanges(timestamp, internalOnly, null);
			}
		} finally {
			unlock();
		}
	}
	protected <T extends Comparable<T>> T get(DeviceInfoProperty propertyName) {
		lock();
		try {
			return internalGet(propertyName);
		} finally {
			unlock();
		}
	}

	/**
	 * @see com.usatech.layers.common.DeviceInfo#getActionCode()
	 */
	@Override
	public ServerActionCode getActionCode() {
		return get(DeviceInfoProperty.ACTION_CODE);
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getActivationStatus()
	 */
	@Override
	public ActivationStatus getActivationStatus() {
		return get(DeviceInfoProperty.ACTIVATION_STATUS);
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getCredentialTimestamp()
	 */
	@Override
	public long getCredentialTimestamp() {
		Long value = get(DeviceInfoProperty.CREDENTIAL_TIMESTAMP);
		return value;
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getDeviceCharset()
	 */
	@Override
	public Charset getDeviceCharset() {
		return get(DeviceInfoProperty.DEVICE_CHARSET);
	}

	@Override
	public String getDeviceInfoHash() {
		if (deviceInfoHash == null) {
			StringBuilder sb = new StringBuilder();
			if (getDeviceSerialCd() != null)
				sb.append(getDeviceSerialCd());
			sb.append(FS);
			if (getDeviceType() != null)
				sb.append(getDeviceType().getValue());
			sb.append(FS);
			sb.append(isInitOnly());
			sb.append(FS);
			if (getDeviceCharset() != null)
				sb.append(getDeviceCharset());
			sb.append(FS);
			if (getTimeZoneGuid() != null)
				sb.append(getTimeZoneGuid());
			sb.append(FS);
			if (getActionCode() != null)
				sb.append(getActionCode().getValue());
			sb.append(FS);
			byte[] tmp;
			if((tmp=getEncryptionKey()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			if((tmp=getPreviousEncryptionKey()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			sb.append(getRejectUntil());
			sb.append(FS);
			if (getUsername() != null)
				sb.append(getUsername());			
			sb.append(FS);			
			if((tmp=getPasswordHash()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			if((tmp=getPasswordSalt()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			sb.append(getCredentialTimestamp());
			sb.append(FS);
			if (getPreviousUsername() != null)
				sb.append(getPreviousUsername());
			sb.append(FS);			
			if((tmp=getPreviousPasswordHash()) != null)
				sb.append(new String(tmp));
			sb.append(FS);			
			if((tmp=getPreviousPasswordSalt()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			if (getNewUsername() != null)
				sb.append(getNewUsername());			
			sb.append(FS);
			if((tmp=getNewPasswordHash()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			if((tmp=getNewPasswordSalt()) != null)
				sb.append(new String(tmp));
			sb.append(FS);
			sb.append(getMaxAuthAmount());
			try {
				setDeviceInfoHash(MessageResponseUtils.calculateSHA1(sb.toString()));
			} catch(NoSuchAlgorithmException e) {
				log.error("", e);
			}
		}
		return deviceInfoHash;	
	}

	/**
	 * @see com.usatech.layers.common.DeviceInfo#getDeviceName()
	 */
	@Override
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getDeviceSerialCd()
	 */
	@Override
	public String getDeviceSerialCd() {
		return get(DeviceInfoProperty.DEVICE_SERIAL_CD);
	}

	/**
	 * @see com.usatech.layers.common.DeviceInfo#getDeviceType()
	 */
	@Override
	public DeviceType getDeviceType() {
		return get(DeviceInfoProperty.DEVICE_TYPE);
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getEncryptionKey()
	 */
	@Override
	public byte[] getEncryptionKey() {
		ByteArrayComparable bac = get(DeviceInfoProperty.ENCRYPTION_KEY);
		return bac == null ? null : bac.getValue();
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getLocale()
	 */
	@Override
	public String getLocale() {
		return get(DeviceInfoProperty.LOCALE);
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getMasterId()
	 */
	@Override
	public long getMasterId() {
		Long value = get(DeviceInfoProperty.MASTER_ID);
		return value;
	}

	/**
	 * @see com.usatech.layers.common.DeviceInfo#getMaxAuthAmount()
	 */
	@Override
	public long getMaxAuthAmount() {
		Long value = get(DeviceInfoProperty.MAX_AUTH_AMOUNT);
		return value;
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getNewPasswordHash()
	 */
	@Override
	public byte[] getNewPasswordHash() {
		ByteArrayComparable bac = get(DeviceInfoProperty.NEW_PASSWORD_HASH);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getNewPasswordSalt()
	 */
	@Override
	public byte[] getNewPasswordSalt() {
		ByteArrayComparable bac = get(DeviceInfoProperty.NEW_PASSWORD_SALT);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getNewUsername()
	 */
	@Override
	public String getNewUsername() {
		return get(DeviceInfoProperty.NEW_USERNAME);
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPasswordHash()
	 */
	@Override
	public byte[] getPasswordHash() {
		ByteArrayComparable bac = get(DeviceInfoProperty.PASSWORD_HASH);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPasswordSalt()
	 */
	@Override
	public byte[] getPasswordSalt() {
		ByteArrayComparable bac = get(DeviceInfoProperty.PASSWORD_SALT);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPreviousEncryptionKey()
	 */
	@Override
	public byte[] getPreviousEncryptionKey() {
		ByteArrayComparable bac = get(DeviceInfoProperty.PREVIOUS_ENCRYPTION_KEY);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPreviousPasswordHash()
	 */
	@Override
	public byte[] getPreviousPasswordHash() {
		ByteArrayComparable bac = get(DeviceInfoProperty.PREVIOUS_PASSWORD_HASH);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPreviousPasswordSalt()
	 */
	@Override
	public byte[] getPreviousPasswordSalt() {
		ByteArrayComparable bac = get(DeviceInfoProperty.PREVIOUS_PASSWORD_SALT);
		return bac == null ? null : bac.getValue();
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPreviousUsername()
	 */
	@Override
	public String getPreviousUsername() {
		return get(DeviceInfoProperty.PREVIOUS_USERNAME);
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPropertyIndexesToRequest()
	 */
	@Override
	public IntegerRangeSet getPropertyIndexesToRequest() {
		return get(DeviceInfoProperty.PROPERTY_INDEXES_TO_REQUEST);
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getPropertyListVersion()
	 */
	@Override
	public Integer getPropertyListVersion() {
		Integer value = get(DeviceInfoProperty.PROPERTY_LIST_VERSION);
		return value;
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getRejectUntil()
	 */
	@Override
	public long getRejectUntil() {
		Long value = get(DeviceInfoProperty.REJECT_UNTIL);
		return value;
	}

	public long getTimestamp(DeviceInfoProperty property) {
		StampedComparable<?> sc = committedProperties.get(property);
		return sc == null ? 0 : sc.timestamp;
	}

	/**
	 * @see com.usatech.layers.common.DeviceInfo#getTimeZoneGuid()
	 */
	@Override
	public String getTimeZoneGuid() {
		return get(DeviceInfoProperty.TIME_ZONE_GUID);
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#getUsername()
	 */
	@Override
	public String getUsername() {
		return get(DeviceInfoProperty.USERNAME);
	}

	@SuppressWarnings("unchecked")
	protected <T extends Comparable<T>> T internalGet(DeviceInfoProperty propertyName) {
		if(updatedProperties.containsKey(propertyName)) {
			return (T)updatedProperties.get(propertyName);
		} else if(committedProperties.containsKey(propertyName)) {
			StampedComparable<?> sc = committedProperties.get(propertyName);
			return (T)(sc == null ? null : sc.getValue());
		} else {
			return (T)propertyName.getDefaultValue();
		}
	}
	
	protected <T extends Comparable<T>> UpdateIfResult internalUpdateIfDifferent(DeviceInfoProperty propertyName, T value) {
		T oldValue = internalGet(propertyName);
		if(compare(oldValue, value) == 0) {
			if(propertyName.isSameValueAccepted())
				updateTimestamp.add(propertyName);
			return UpdateIfResult.NO_CHANGE;
		}
		updatedProperties.put(propertyName, value);
		return UpdateIfResult.UPDATED;
	}
	
	protected <T extends Comparable<T>> UpdateIfResult internalUpdateIfGreater(DeviceInfoProperty propertyName, T value, T limit) {
		T oldValue = internalGet(propertyName);
		int c = compare(oldValue, value);
		if(c > 0)
			return UpdateIfResult.TOO_LOW;
		if(c == 0)
			return UpdateIfResult.NO_CHANGE;
		if(limit != null && value.compareTo(limit) > 0)
			return UpdateIfResult.TOO_HIGH;
		updatedProperties.put(propertyName, value);
		return UpdateIfResult.UPDATED;
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#isInitOnly()
	 */
	@Override
	public boolean isInitOnly() {
		Boolean value = get(DeviceInfoProperty.INIT_ONLY);
		return value == null ? false : value.booleanValue();
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#isMasterIdIncrementOnly()
	 */
	@Override
	public boolean isMasterIdIncrementOnly() {
		Boolean value = get(DeviceInfoProperty.MASTER_ID_INCREMENT_ONLY);
		return value == null ? true : value.booleanValue();
	}
	
	protected abstract void lock() ;
	
	/**
	 * @throws ServiceException
	 */
	protected void processChanges(Map<String,Object> changes, Map<String,Long> timestamp, boolean internalOnly, Connection targetConn) throws ServiceException {
	}
	
	public void putAll(Map<String,Object> values, Map<String,Long> timestamps) {
		for(DeviceInfoProperty dip : DeviceInfoProperty.values()) {
			StampedComparable<?> sc = committedProperties.get(dip);
			if(sc != null) {
				values.put(dip.getAttributeKey(), unwrap(sc.getValue()));
				if(sc.timestamp != 0)
					timestamps.put(dip.getAttributeKey(), sc.timestamp);
			} else {
				values.put(dip.getAttributeKey(), dip.getDefaultValue());
			}
		}
	}
	
	public void putAllWithTimestamps(Map<String,Object> values) {
		for(DeviceInfoProperty dip : DeviceInfoProperty.values())
			putWithTimestamp(dip, values);
	}

	public void putWithTimestamp(DeviceInfoProperty dip, Map<String, Object> values) {
		StampedComparable<?> sc = committedProperties.get(dip);
		if(sc != null) {
			values.put(dip.getAttributeKey(), unwrap(sc.getValue()));
			if(sc.timestamp != 0)
				values.put(dip.getAttributeKey() + ".timestamp", sc.timestamp);
		} else {
			values.put(dip.getAttributeKey(), dip.getDefaultValue());
		}
	}
	
	@Override
	public int putPendingWithTimestamp(Map<String, Object> values, long timestamp) {
		for(Map.Entry<DeviceInfoProperty, Comparable<?>> entry : updatedProperties.entrySet()) {
			values.put(entry.getKey().getAttributeKey(), unwrap(entry.getValue()));
			values.put(entry.getKey().getAttributeKey() + ".timestamp", timestamp);
		}
		return updatedProperties.size();
	}

	@Override
	public void refresh() throws ServiceException {
		//do nothing
	}

	@Override
	public boolean refreshSafely() {
		try {
			refresh();
			return true;
		} catch(ServiceException e) {
			StringBuilder sb = new StringBuilder();
			sb.append("Could not refresh device info because");
			for(Throwable t = e; t != null; t = t.getCause())
				sb.append(": ").append(t.getMessage());
			log.info(sb.toString());
			return false;
		}
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setActionCode(com.usatech.layers.common.constants.ServerActionCode)
	 */
	@Override
	public void setActionCode(ServerActionCode actionCode) {
		updateIfDifferent(DeviceInfoProperty.ACTION_CODE, actionCode);
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setActivationStatus(com.usatech.layers.common.constants.ActivationStatus)
	 */
	@Override
	public void setActivationStatus(ActivationStatus activationStatus) {
		updateIfDifferent(DeviceInfoProperty.ACTIVATION_STATUS, activationStatus);
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setCredentialTimestamp(long)
	 */
	@Override
	public void setCredentialTimestamp(long credentialTimestamp) {
		updateIfDifferent(DeviceInfoProperty.CREDENTIAL_TIMESTAMP, credentialTimestamp);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setDeviceCharset(java.nio.charset.Charset)
	 */
	@Override
	public void setDeviceCharset(Charset deviceCharset) {
		updateIfDifferent(DeviceInfoProperty.DEVICE_CHARSET, deviceCharset);
	}
	
	protected void setDeviceInfoHash(String deviceInfoHash) {
		lock();
		try {
			this.deviceInfoHash = deviceInfoHash;
		} finally {
			unlock();
		}		
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setDeviceSerialCd(java.lang.String)
	 */
	@Override
	public void setDeviceSerialCd(String deviceSerialCd) {
		updateIfDifferent(DeviceInfoProperty.DEVICE_SERIAL_CD, deviceSerialCd);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setDeviceType(DeviceType)
	 */
	@Override
	public void setDeviceType(DeviceType deviceType) {
		updateIfDifferent(DeviceInfoProperty.DEVICE_TYPE, deviceType);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setEncryptionKey(byte[])
	 */
	@Override
	public void setEncryptionKey(byte[] encryptionKey) {
		updateIfDifferent(DeviceInfoProperty.ENCRYPTION_KEY, new ByteArrayComparable(encryptionKey));
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setInitOnly(boolean)
	 */
	@Override
	public void setInitOnly(boolean initOnly) {
		updateIfDifferent(DeviceInfoProperty.INIT_ONLY, initOnly);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setLocale(java.lang.String)
	 */
	@Override
	public void setLocale(String locale) {
		updateIfDifferent(DeviceInfoProperty.LOCALE, locale);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setMasterIdIncrementOnly(boolean)
	 */
	@Override
	public void setMasterIdIncrementOnly(boolean masterIdIncrementOnly) {
		updateIfDifferent(DeviceInfoProperty.MASTER_ID_INCREMENT_ONLY, masterIdIncrementOnly);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setMaxAuthAmount(long)
	 */
	@Override
	public void setMaxAuthAmount(long maxAuthAmount) {
		updateIfDifferent(DeviceInfoProperty.MAX_AUTH_AMOUNT, maxAuthAmount);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setNewPasswordHash(byte[])
	 */
	@Override
	public void setNewPasswordHash(byte[] newPasswordHash) {
		updateIfDifferent(DeviceInfoProperty.NEW_PASSWORD_HASH, new ByteArrayComparable(newPasswordHash));
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setNewPasswordSalt(byte[])
	 */
	@Override
	public void setNewPasswordSalt(byte[] newPasswordSalt) {
		updateIfDifferent(DeviceInfoProperty.NEW_PASSWORD_SALT, new ByteArrayComparable(newPasswordSalt));
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setNewUsername(java.lang.String)
	 */
	@Override
	public void setNewUsername(String newUsername) {
		updateIfDifferent(DeviceInfoProperty.NEW_USERNAME, newUsername);
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPasswordHash(byte[])
	 */
	@Override
	public void setPasswordHash(byte[] passwordHash) {
		updateIfDifferent(DeviceInfoProperty.PASSWORD_HASH, new ByteArrayComparable(passwordHash));
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPasswordSalt(byte[])
	 */
	@Override
	public void setPasswordSalt(byte[] passwordSalt) {
		updateIfDifferent(DeviceInfoProperty.PASSWORD_SALT, new ByteArrayComparable(passwordSalt));
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPreviousEncryptionKey(byte[])
	 */
	@Override
	public void setPreviousEncryptionKey(byte[] previousEncryptionKey) {
		updateIfDifferent(DeviceInfoProperty.PREVIOUS_ENCRYPTION_KEY, new ByteArrayComparable(previousEncryptionKey));
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPreviousPasswordHash(byte[])
	 */
	@Override
	public void setPreviousPasswordHash(byte[] previousPasswordHash) {
		updateIfDifferent(DeviceInfoProperty.PREVIOUS_PASSWORD_HASH, new ByteArrayComparable(previousPasswordHash));
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPreviousPasswordSalt(byte[])
	 */
	@Override
	public void setPreviousPasswordSalt(byte[] previousPasswordSalt) {
		updateIfDifferent(DeviceInfoProperty.PREVIOUS_PASSWORD_SALT, new ByteArrayComparable(previousPasswordSalt));
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPreviousUsername(java.lang.String)
	 */
	@Override
	public void setPreviousUsername(String previousUsername) {
		updateIfDifferent(DeviceInfoProperty.PREVIOUS_USERNAME, previousUsername);
	}

	/**
	 * @see com.usatech.layers.common.DeviceInfo#setPropertyIndexesToRequest(java.lang.String)
	 */
	@Override
	public void setPropertyIndexesToRequest(String propertyIndexesToRequest) {
		IntegerRangeSet irs = new OptimizedIntegerRangeSet();
		irs.addRanges(propertyIndexesToRequest);
		irs = new UnmodifiableIntegerRangeSet(irs);
		internalUpdateIfDifferent(DeviceInfoProperty.PROPERTY_INDEXES_TO_REQUEST, irs);
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setPropertyListVersion(Integer)
	 */
	@Override
	public void setPropertyListVersion(Integer propertyListVersion) {
		updateIfDifferent(DeviceInfoProperty.PROPERTY_LIST_VERSION, propertyListVersion);
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setTimeZoneGuid(java.lang.String)
	 */
	@Override
	public void setTimeZoneGuid(String timeZoneGuid) {
		updateIfDifferent(DeviceInfoProperty.TIME_ZONE_GUID, timeZoneGuid);
	}

	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#setUsername(java.lang.String)
	 */
	@Override
	public void setUsername(String username) {
		updateIfDifferent(DeviceInfoProperty.USERNAME, username);
	}

	protected abstract void unlock() ;

	protected Object unwrap(Comparable<?> value) {
		if(value instanceof ByteArrayComparable)
			return ((ByteArrayComparable)value).getValue();
		return value;
	}
	
	protected <T extends Comparable<T>> UpdateIfResult updateIfDifferent(DeviceInfoProperty propertyName, T value) {
		lock();
		try {
			return internalUpdateIfDifferent(propertyName, value);
		} finally {
			unlock();
		}
	}

	protected <T extends Comparable<T>> UpdateIfResult updateIfGreater(DeviceInfoProperty propertyName, T value, T limit) {
		lock();
		try {
			return internalUpdateIfGreater(propertyName, value, limit);
		} finally {
			unlock();
		}
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfo#updateInternal(Map<String,Object>, Map<String,Long>, Connection)
	 */
	@Override
	public void updateInternal(Map<String,Object> values, Map<String,Long> timestamps, Connection targetConn) throws ServiceException {
		lock();
		try {
			Map<String, Object> changes = acceptChanges(values, timestamps);
			if(!changes.isEmpty()) {
				processChanges(changes, new IncludingMap<String, Long>(timestamps, changes.keySet()), true, targetConn);
			}
		} finally {
			unlock();
		}
	}
	/**
	 * @see com.usatech.layers.common.DeviceInfo#updateMasterId(long)
	 */
	@Override
	public UpdateIfResult updateMasterId(long masterId) {
		if(isMasterIdIncrementOnly())
			return updateIfGreater(DeviceInfoProperty.MASTER_ID, masterId, System.currentTimeMillis() / 1000 + ProcessingUtils.MAX_ID_ADVANCE);
		return updateIfDifferent(DeviceInfoProperty.MASTER_ID, masterId);
	}
	
	/**
	 * @see com.usatech.layers.common.BasicDeviceInfo#updateRejectUntil(long)
	 */
	@Override
	public UpdateIfResult updateRejectUntil(long rejectUntil) {
		return updateIfGreater(DeviceInfoProperty.REJECT_UNTIL, rejectUntil, System.currentTimeMillis() + ProcessingUtils.MAX_ID_ADVANCE * 1000);
	}
}
