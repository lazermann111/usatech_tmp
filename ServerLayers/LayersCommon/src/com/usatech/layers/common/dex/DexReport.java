
package com.usatech.layers.common.dex;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
 

public class DexReport 
{
	
	public static String getDexReport(DexRead read)
	{
		StringBuilder sb  = new StringBuilder();
		
		File file = new File("c:\\temp\\dex_report_begin.txt");
		int ch;    
		StringBuffer content = new StringBuffer("");    
		FileInputStream fin = null;     
		try    
		{
			/*       * Create new FileInputStream object. Constructor of FileInputStream throws       * FileNotFoundException if the agrument File does not exist.       */       
			fin = new FileInputStream(file);       
			while( (ch = fin.read()) != -1)        
				content.append((char)ch);             
			fin.close();      
		}    
		catch(Exception e)    
		{     
		
		}
		
		NumberFormat formatter = new DecimalFormat("#0.00");

		sb.append(content);
		  
		sb.append( "<TABLE class=\"reportCriteria\" cellSpacing=0 cellPadding=2 border=0>");
		sb.append( "<TR><TH>Machine:</TH><TD>" + read.machineNumber + "</TD></TR>");
		sb.append( "<TR><TH>Machine Type:</TH><TD>" + read.machineType  + "</TD></TR>");
		//sb.append( "<TR><TH>Serial Number:</TH><TD>" + read.serialNumber  + "</TD></TR>");
		sb.append( "<TR><TH>Read Date:</TH><TD>" + read.readDate.toString() + "</TD></TR>");
		sb.append( "</TABLE><HR>");

		sb.append( "<P class=\"regionName\">Sales:</P><TABLE class=\"reportTable\" cellpadding=2 cellspacing=2 border=1 width=\"100%\"><TR class=\"tableHeader\"><TH>Category</TH><TH>Vends</TH><TH>Sales</TH></TR>" );

		sb.append( "<TR class=\"tableDataShade\"><TD>Cash</TD><TD>" + Integer.toString(read.cashVends) + "</TD><TD>$" + formatter.format(read.cashSales) + "</TD></TR>");
		sb.append( "<TR class=\"tableDataShade\"><TD>Credit</TD><TD>" + Integer.toString(read.creditVends) + "</TD><TD>$" + formatter.format(read.creditSales) + "</TD></TR>");
		sb.append( "<TR class=\"tableTotals\"><TD>Total</TD><TD>" + Integer.toString(read.totalVends) + "</TD><TD>$" + formatter.format(read.totalSales) + "</TD></TR>");
        sb.append( "</TABLE></P>" );
        
		sb.append( "<P class=\"regionName\">Columns:</P><TABLE class=\"reportTable\" cellpadding=2 cellspacing=2 border=1 width=\"100%\"><TR class=\"tableHeader\"><TH>Column</TH><TH>Price</TH><TH>Vends</TH><TH>Sales</TH></TR>" );

		Hashtable c = (Hashtable)read.columns;
		Vector v = new Vector(c.keySet());
	    Collections.sort(v);
	    Iterator it = v.iterator();
	    while (it.hasNext()) 
	    {
	    	String name = (String)it.next();
        	DexDataColumn column = read.getColumn(name);
        	
    		sb.append( "<TR class=\"tableDataShade\"><TD>" + column.columnName + "</TD><TD>" + formatter.format(column.columnPrice) + "</TD><TD>" + Integer.toString(column.columnVends) + "</TD><TD>" + formatter.format(column.columnSales) + "</TD></TR>");
        }
        
        sb.append( "</TABLE></P>" );

        sb.append( "<P class=\"regionName\">Alerts:</P><TABLE class=\"reportTable\" cellpadding=2 cellspacing=2 border=1 width=\"100%\"><TR class=\"tableHeader\"><TH>Alert</TH><TH>Date/Time</TH><TH>Value</TH></TR>" );

        for ( DexAlert alert : read.alerts )
        {
    		sb.append( "<TR class=\"tableDataShade\"><TD>" + alert.alertType + "</TD><TD>" + alert.alertDate.toString() + "</TD><TD>" + alert.alertValue + "</TD></TR>");
        }
        sb.append( "</TABLE></P>" );
        
        sb.append( "</TD></TR></TABLE></BODY></HTML>");
        
		return sb.toString();
	}
}
