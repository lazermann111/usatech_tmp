
package com.usatech.layers.common.dex;

public class DexConstants 
{
	public static String start = "ST*";
	public static String newStart = "\nST*";
	public static char recordDelimeter = '*';
	public static String primaryDateLineRecord = "ID5";
	public static String secondaryDateLineRecord = "EA3";
}
