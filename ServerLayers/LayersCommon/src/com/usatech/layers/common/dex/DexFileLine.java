package com.usatech.layers.common.dex;

/**
 * This is used by the inbound file import task
 * as part of the full line alert parsing
 */
public class DexFileLine {
	
	private int lineNumber;
	private String lineContents;
	
	public DexFileLine(int lineNumber, String lineContents){
		this.lineNumber = lineNumber;
		this.lineContents = lineContents;
	}

	public int getLineNumber(){
		return this.lineNumber;
	}
	
	public String getLineContents(){
		return this.lineContents;
	}
}
