
package com.usatech.layers.common.dex;

import java.util.*;

public class DexAlert 
{
	public DexDataRecord alertRecord;
	
	public java.util.Date alertDate;
	
	public String alertType;
	
	public String alertValue;
	
	public boolean isRecent()
	{
		java.util.Date now = new java.util.Date();
		
		long diff = now.getTime() - alertDate.getTime();  
	
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		if ( diffDays < 7 )
		{
			return true;
		}
		
		return false;
		
	}
}
