package com.usatech.layers.common.dex;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import simple.app.DatabasePrerequisite;
import simple.app.DialectResolver;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.ByteInput;
import simple.io.EnhancedBufferedReader;
import simple.io.Log;
import simple.io.resource.ByteArrayResource;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.EnumCharValueLookup;
import simple.lang.Initializer;
import simple.lang.InvalidValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.EnhancedMessageFormat;
import simple.text.ThreadSafeDateFormat;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AlertSourceCode;
import com.usatech.layers.common.constants.AlertingAttrEnum;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.FileType;

public class InboundFileImportTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final String DEX_HEADER_SEPARATOR = "-";
	// protected static final String DEX_LINE_SEPARATOR = "\r\n";
	protected static final DateFormat DEX_DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MMddyy HHmmss"));
	protected static final DateFormat DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MMddyy"));
	protected static final DateFormat TIME_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("HHmmss"));
	protected static final String PRODUCT_PREFIX = "PA";
	protected static final String PRODUCT_LINE_PREFIX = "PA1";
	protected static List<DexCode> dexCodes = new ArrayList<DexCode>();
	
	// protected static Map<Integer, DexCodeContext> contextIds = new HashMap<Integer, DexCodeContext>();

	protected static enum AlertStatus {
		ACTIVE('A'), INACTIVE('I'), DELETED('D');
		private final char value;
		private static final EnumCharValueLookup<AlertStatus> lookup = new EnumCharValueLookup<AlertStatus>(AlertStatus.class);

		private AlertStatus(char value) {
			this.value = value;
		}

		public char getValue() {
			return value;
		}

		public static AlertStatus getByValue(char value) throws InvalidValueException {
			return lookup.getByValue(value);
		}

		public static AlertStatus getByValue(int value) throws InvalidValueException {
			return getByValue((char) value);
		}
	};

	protected ResourceFolder resourceFolder;
	protected String requestQueuePrefix = "usat.report.request.";
	protected Publisher<ByteInput> reportingPublisher;

	public static class DexParseResult {
		protected final Long terminalId;
		protected final int alertCount;
		protected final Long dexFileId;
		protected final Date dexDate;

		protected DexParseResult(Long terminalId, int alertCount, Long dexFileId, Date dexDate) {
			super();
			this.terminalId = terminalId;
			this.alertCount = alertCount;
			this.dexFileId = dexFileId;
			this.dexDate = dexDate;
		}
		public Long getTerminalId() {
			return terminalId;
		}
		public int getAlertCount() {
			return alertCount;
		}
		public Long getDexFileId() {
			return dexFileId;
		}
		public Date getDexDate() {
			return dexDate;
		}
	}
	protected static final Initializer<ServiceException> initializer = new Initializer<ServiceException>() {
		@Override
		protected void doInitialize() throws ServiceException {
			try {
				loadDEXCodes();
			} catch(SQLException e) {
				throw new ServiceException("Could not load DEX codes from database", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not load DEX codes from database", e);
			} catch(ConvertException e) {
				throw new ServiceException("Could not load DEX codes from database", e);
			}
		}

		@Override
		protected void doReset() {
			dexCodes.clear();
			// contextIds.clear();
		}
	};
	
	protected static void loadDEXCodes() throws SQLException, DataLayerException, ConvertException {
		Results results = null;
		try {
			results = DataLayerMgr.executeQuery("GET_DEX_CODE_ALERTS", null);
			while(results.next())
				dexCodes.add(new DexCodeAlert(String.valueOf(results.get("dexCode")), ConvertUtils.getInt(results.get("alertId")), String.valueOf(results.get("message")), ConvertUtils.convertSafely(AlertStatus.class, results.get("alertStatus"), null)));
			results.close();
			/*
			results = DataLayerMgr.executeQuery("GET_DEX_CODE_CONTEXTS", null);
			while(results.next())
				dexCodes.add(new DexCodeContext(String.valueOf(results.get("dexCode")), ConvertUtils.getInt(results.get("contextId"))));
			results.close();
			*/
		} finally {
			if (results != null)
				results.close();
		}
		Collections.sort(dexCodes);
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long importStartTs = System.currentTimeMillis();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		
		StringBuilder sb = new StringBuilder();
		sb.append('\'').append(taskInfo.getMessageChainGuid()).append("': ");
		Log log = ProcessingUtils.getPrefixedLog(InboundFileImportTask.log, sb.toString());
		log.debug("STARTED Inbound File Import with " + attributes);
		FileType fileType;
		try{
			fileType= FileType.getByValue(ConvertUtils.getInt(attributes.get("fileType")));
		}catch(Exception e) {
			throw new ServiceException("Error getting fileType.", e);
		}
		if(fileType.equals(FileType.CUSTOM_FILE_UPLOAD)){
			try{
				DataLayerMgr.selectInto("GET_TERMINAL_ID_FOR_FORWARD_FILE", attributes);
			}catch(SQLException e) {
				throw new ServiceException("Could not get terminalId for " + attributes.get("deviceSerialCd"), e);
			}catch(DataLayerException e) {
				throw new ServiceException("Could not get terminalId for " + attributes.get("deviceSerialCd"), e);
			} catch(BeanException e) {
				throw new RetrySpecifiedServiceException("Could not get terminalId for " + attributes.get("deviceSerialCd") , e, WorkRetryType.NO_RETRY);
			}
			MessageChain mc = new MessageChainV11();
			MessageChainStep requestStep = mc.addStep(getRequestQueuePrefix() + 8 /*ReportScheduleType.CUSTOM_FILE_UPLOAD.getValue()*/);
			for(Map.Entry<String, Object> entry : attributes.entrySet())
				requestStep.addLiteralAttribute(entry.getKey(), entry.getValue());
			MessageChainService.publish(mc, getReportingPublisher());
			return 0;
		}else{
			Resource resource;
			boolean delete;
			byte[] resourceContent = ConvertUtils.convertSafely(byte[].class, attributes.get("resourceContent"), null);
			if(resourceContent != null && resourceContent.length > 0) {
				String fileName = ConvertUtils.getStringSafely(attributes.get("fileName"));
				resource = new ByteArrayResource(resourceContent, "in memory", fileName);
				delete = false;
			} else {
				String resourceKey = ConvertUtils.getStringSafely(attributes.get("resourceKey"));
				if (resourceKey == null || resourceKey.trim().length() == 0) {
					log.error("Received file import message with empty resourceKey");
					return 1;
				}
				ResourceFolder rf = getResourceFolder();
				if(rf == null)
					throw new ServiceException("ResourceFolder property is not set on " + this);
				
				try {
					resource = rf.getResource(resourceKey, ResourceMode.READ);
				} catch(IOException e) {
					if(!rf.isAvailable(ResourceMode.READ)) {
						throw new RetrySpecifiedServiceException("Could not get resource at '" + resourceKey + "' for processing file import", e, WorkRetryType.NONBLOCKING_RETRY);
					}
					throw new ServiceException("Could not get resource at '" + resourceKey + "' for processing file import", e);
				}
				delete = true;
			}
			
			try {
				int result=processFileImport(taskInfo, attributes, resource, log, importStartTs);
				if (result < 100 && delete)
					DeleteResourceTask.requestResourceDeletion(resource, taskInfo.getPublisher());
				log.debug("RESULT = " + result);
				return result;
			} finally {
				resource.release();
				log.debug("ENDED Inbound File Import");
			}
		}
	}
	
	protected int processFileImport(MessageChainTaskInfo taskInfo, Map<String, Object> attributes, Resource resource, final Log log, long importStartTs) throws ServiceException {
		String resourceKey = ConvertUtils.getStringSafely(attributes.get("resourceKey"));
		String fileName = ConvertUtils.getStringSafely(attributes.get("fileName"));
		long fileTransferId;
		FileType fileType;
		String deviceSerialCd;
		Calendar fileTransferTs;
		String timeZoneGuid;
		try {
			fileTransferId = ConvertUtils.getLong(attributes.get("fileTransferId"));
			fileType = FileType.getByValue(ConvertUtils.getInt(attributes.get("fileType")));
			deviceSerialCd = ConvertUtils.getString(attributes.get("deviceSerialCd"), true);
			fileTransferTs = ConvertUtils.convert(Calendar.class, attributes.get("fileTransferTs"));
			timeZoneGuid = ConvertUtils.getString(attributes.get("timeZoneGuid"), false);
		} catch(Exception e) {
			throw new ServiceException("Error parsing data in file import message, fileName: " + fileName + ", resourceKey: " + resourceKey, e);
		}
		
		StringBuilder sb = new StringBuilder("Importing file into REPORT database, fileName: ").append(fileName).append(", fileType: ").append(fileType)
			.append(", fileTransferId: " ).append(fileTransferId).append(", resourceKey: ").append(resourceKey);
		log.info(sb);
		
		InputStream is;
		try {
			is = resource.getInputStream();
		} catch(IOException e) {
			throw new ServiceException("Could not read from resource at '" + resource.getKey() + "' for processing file transfer", e);
		}
		
		// String deviceName = ConvertUtils.getStringSafely(attributes.get("deviceName"));
		Charset deviceCharset = taskInfo.getStep().getAttributeSafely(DeviceInfoProperty.DEVICE_CHARSET, Charset.class, ProcessingConstants.US_ASCII_CHARSET);

		switch(fileType) {
			case DEX_FILE:
			case DEX_FILE_FOR_FILL_TO_FILL:
				DexParseResult dexParseResult = parseDexFile(fileTransferId, fileName, new EnhancedBufferedReader(new InputStreamReader(is, deviceCharset)), deviceSerialCd, fileTransferTs, true, timeZoneGuid);
				if(dexParseResult != null) {
					if(dexParseResult.getTerminalId() != null) {
						// Publish to RequestAlertBatchTask
						if(dexParseResult.getAlertCount() > 0) {
							MessageChain mc = new MessageChainV11();
							MessageChainStep requestStep = mc.addStep(getRequestQueuePrefix() + 6 /*ReportScheduleType.EVENT.getValue()*/);
							requestStep.setAttribute(AlertingAttrEnum.AL_SERIAL_NUM, deviceSerialCd);
							requestStep.setAttribute(AlertingAttrEnum.AL_TERMINAL, dexParseResult.getTerminalId());
							requestStep.setAttribute(AlertingAttrEnum.AL_EVENT_ID, fileTransferId);
							requestStep.setAttribute(AlertingAttrEnum.AL_EVENT_SOURCE_TYPE, AlertSourceCode.DEX);
							requestStep.setAttribute(AlertingAttrEnum.AL_NOTIFICATION_TS, fileTransferTs);
							MessageChainService.publish(mc, getReportingPublisher());
						}
						// Publish to RequestDexBatchTask
						if(dexParseResult.getDexFileId() != null) {
							MessageChain mcDex = new MessageChainV11();
							MessageChainStep requestDexStep = mcDex.addStep(getRequestQueuePrefix() + 2 /*ReportScheduleType.DEX.getValue()*/);
							for(Map.Entry<String, Object> entry : attributes.entrySet())
								requestDexStep.addLiteralAttribute(entry.getKey(), entry.getValue());
							requestDexStep.addLongAttribute("dexId", dexParseResult.getDexFileId());
							requestDexStep.addLongAttribute("terminalId", dexParseResult.getTerminalId());
							MessageChainService.publish(mcDex, getReportingPublisher());
						}
					}
				}
				break;
			default:
				// TODO: Publish to RequestFileTransferBatchTask
				throw new ServiceException("Import of this fileType is not supported, fileTransferId: " + fileTransferId + ", fileName: " + fileName + ", fileType: " + fileType + ", resourceKey: " + resourceKey);
		}
		return 0;
	}

	/**
	 * @param fileTransferId
	 * @param fileName
	 * @param contentReader
	 * @param deviceSerialCd
	 * @param fileTransferTs
	 * @return <code>terminalId</code> if there are any alerts created as part of this, otherwise return <code>null</code>
	 * @throws IOException
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 * @throws ServiceException
	 * @throws BeanException
	 */
	public static DexParseResult parseDexFile(long fileTransferId, String fileName, EnhancedBufferedReader contentReader, String deviceSerialCd, Calendar fileTransferTs, boolean sendOn, String timeZoneGuid) throws ServiceException {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new ServiceException("Interrupted while initializing", e);
		}
		try {
			String headerLine = contentReader.readLine();
			if(headerLine == null) {
				log.warn("DEX file " + fileName + ", fileTransferId: " + fileTransferId + " is empty; Not parsing");
				return null;
			}
			if((headerLine = headerLine.trim()).isEmpty()) {
				log.warn("DEX file " + fileName + ", fileTransferId: " + fileTransferId + " has a blank header line; not parsing");
				return null;
			}
			headerLine=headerLine.replaceAll("_#", "");
			String[] headerInfo = headerLine.split(DEX_HEADER_SEPARATOR);
			if(headerInfo.length < 4) {
				log.warn("Invalid 'DEX-' header in DEX file " + fileName + ", fileTransferId: " + fileTransferId + ", not importing");
				return null;
			}
			if(!"DEX".equals(headerInfo[0])) {
				log.warn("No 'DEX-' header in DEX file " + fileName + ", fileTransferId: " + fileTransferId + ", not importing");
				return null;
			}

			Date dexDate;
			try {
				dexDate = DEX_DATE_FORMAT.parse(headerInfo[3] + " " + headerInfo[2]);
			} catch(ParseException pe) {
				//@Todo R35 found a case when device upload dex file failed, resulting incomplete dex file 07302013 EV176876-DEX-SCHEDULED--SC1F-EC52
				log.warn("Could not parse the date in 'DEX-' header at position " + pe.getErrorOffset() + ", skipping file.");
				return null;
			}
			
			if (deviceSerialCd.startsWith("K3VS")) {
				//Interactive devices send timestamps in UTC, we are converting them to device local time
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
				calendar.setTime(dexDate);
				long dexLocalTsMs = ConvertUtils.getLocalTime(calendar.getTimeInMillis(), timeZoneGuid);
				calendar.setTimeInMillis(dexLocalTsMs);
				dexDate = calendar.getTime();
				headerInfo[3] = DATE_FORMAT.format(dexDate);
				headerInfo[2] = TIME_FORMAT.format(dexDate);
			}
			
			StringBuilder sb = new StringBuilder(headerLine.length());
			sb.append(headerInfo[0]).append(DEX_HEADER_SEPARATOR).append(deviceSerialCd); // Use the deviceSerialCd and not what the device sent
			for(int i = 2; i < 5; i++) {
				sb.append(DEX_HEADER_SEPARATOR);
				if(headerInfo.length > i && headerInfo[i] != null)
					sb.append(headerInfo[i]);
			}
			sb.append(".log");

			String dexFileName = sb.toString();
			sb.setLength(0);
			
			int dexType = 0;
			if(headerInfo.length > 4 && headerInfo[4] != null) {
				if(headerInfo[4].equalsIgnoreCase("SCHEDULED"))
					dexType = 1;
				else if(headerInfo[4].equalsIgnoreCase("INTERVAL"))
					dexType = 2;
				else if(headerInfo[4].equalsIgnoreCase("FILL"))
					dexType = 3;
				else if(headerInfo[4].equalsIgnoreCase("ALARM"))
					dexType = 4;
				else
					log.warn("Unknown DEX type: '" + headerInfo[4] + "', fileTransferId: " + fileTransferId);
			}

			final Map<String, Object> params = new HashMap<String, Object>();
			boolean startSentinel = false;
			int cnt = 1;
			int alertCnt = 0;
			int exceptionCnt = 0;
			Long dexFileId=null;
			Long terminalId = null;
			Clob dexContentClob = null;
			Writer dexContent = null;
			boolean okay = false;
			boolean removeNonAscii = false;
			boolean useFullLineAlertFilter = false;
			Set<String> alertDetails = new LinkedHashSet<>();
			String line;
			String eol;
			Connection conn = DataLayerMgr.getConnection("REPORT");
			List<DexFileLine> previousFileLines = new ArrayList<DexFileLine>(); 
			String productLine = "";
			try {
				while((line = contentReader.readLine()) != null) {
					cnt++;
					if(!startSentinel) {
						int pos = line.indexOf("DXS*");
						if(pos >= 0) {
							startSentinel = true;
							params.put("fileName", dexFileName);
							params.put("deviceSerialCd", deviceSerialCd);
							params.put("dexDate", dexDate);
							params.put("dexType", dexType);
							params.put("fileTransferId", fileTransferId);
							params.put("uploadDate", fileTransferTs);
							params.put("sent", sendOn ? "N" : "I"); // N = No, I = Ignore
							// if file is already imported, we get the # of alerts and if > 0 then we return terminalId, else return null
							DataLayerMgr.executeCall(conn, "INSERT_DEX_FILE", params);
							dexFileId = ConvertUtils.convert(Long.class, params.get("dexFileId"));
							if(dexFileId == null) {
								log.info("DEX file " + fileName + " already imported but with a different file transfer id (not " + fileTransferId + "); not parsing again and not sending alerts");
								okay = true;
								return null;
							}
							DataLayerMgr.selectInto(conn, "GET_TERMINAL_ID_FOR_DEX_FILE", params);
							terminalId = ConvertUtils.convert(Long.class, params.get("terminalId"));
							removeNonAscii = ConvertUtils.getBooleanSafely(params.get("removeNonAscii"), false);
							//the device level flag for alert parsing
							boolean deviceUseFullLineAlertFilter = ConvertUtils.getBooleanSafely(params.get("dexAlertFullLineFilterInd"), false);
							byte [] dexContentBytea = null;
							if (DialectResolver.isOracle()) {
							    dexContentClob = ConvertUtils.convert(Clob.class, params.get("fileContent"));
							} else {
								dexContentBytea = ConvertUtils.convert(byte[].class, params.get("fileContent"));
								dexContent = new StringWriter();
							}
	                         
							if((dexContentClob == null && DialectResolver.isOracle()) || (dexContentBytea == null  && !DialectResolver.isOracle()))  {
								DataLayerMgr.selectInto(conn, "GET_ALERT_COUNT_FOR_DEX_FILE", params);
								alertCnt = ConvertUtils.getInt(params.get("alertCount"));
								log.info("DEX file " + fileName + " already imported but with file transfer id " + fileTransferId + "; not parsing again but sending " + alertCnt + " alerts");
								okay = true;
								return new DexParseResult(terminalId, alertCnt, dexFileId, dexDate);
							}
							if (DialectResolver.isOracle())
								dexContent = dexContentClob.setCharacterStream(1L);
							writeDexContent(dexContent, line, pos, line.length() - pos, removeNonAscii);
							eol = contentReader.readEOL();
							if(eol != null)
								dexContent.write(eol);
							//Get the company preference for alert parsing
							DataLayerMgr.selectInto(conn, "GET_PARSING_METHOD_FOR_DEX_FILE", params);
							boolean cmpUseFullLineAlertFilter = ConvertUtils.getBooleanSafely(params.get("customerFullLineFilterInd"), false);
							//if company preference or device level alert parsing flag set to true 
							if(cmpUseFullLineAlertFilter || deviceUseFullLineAlertFilter){
								Results results = null;
								try {
									results = DataLayerMgr.executeQuery("GET_PREVIOUS_DEX_FILE_CONTENTS", params);
									while(results.next()){
										previousFileLines.add(new DexFileLine(ConvertUtils.getInt(results.get("lineNumber")), String.valueOf(results.get("lineContents"))));
									}
								} finally {
									if (results != null)
										results.close();
								}
								//If there was a previous dex file with lines then set to true
								//if not then there isn't anything for the full line filter to
								//use for comparison so set to false
								useFullLineAlertFilter = previousFileLines.size() > 0;
							}
						} else if(log.isInfoEnabled())
							log.info("Skipping line " + cnt + " of DEX file " + fileName + ", fileTransferId: " + fileTransferId + " because it doesn't have the start sentinel");
					} else if(line.startsWith("DXE*")) {
						writeDexContent(dexContent, line, removeNonAscii);
						eol = contentReader.readEOL();
						if(eol != null)
							dexContent.write(eol);
						dexContent.flush();
						if (!DialectResolver.isOracle()) {
							params.clear();
							params.put("fileContent", dexContent);
							params.put("dexFileId", dexFileId);
							DataLayerMgr.executeCall(conn, "UPDATE_DEX_FILE_CONTENT", params);
						}
						dexContent.close();
						params.clear();
						params.put("alertCount", alertCnt);
						params.put("exceptionCount", exceptionCnt);
						params.put("dexFileId", dexFileId);
						StringBuilder alertSummary = new StringBuilder();
						for(String details : alertDetails) {
							if(alertSummary.length() > 0)
								alertSummary.append("; ");
							if(alertSummary.length() + details.length() > 3997) {
								alertSummary.append(details, 0, 3997 - alertSummary.length()).append("...");
								break;
							}
							alertSummary.append(details);
						}
						params.put("alertSummary", alertSummary.toString());

						DataLayerMgr.executeCall(conn, "UPDATE_DEX_ALERT_COUNT", params);
						okay = true;
						conn.commit();
						return new DexParseResult(terminalId, alertCnt + exceptionCnt, dexFileId, dexDate);
					} else {
						if (line != null && line.startsWith(PRODUCT_LINE_PREFIX))
							productLine = line;
						if (line == null || !line.startsWith(PRODUCT_PREFIX))
							productLine = "";
						DexCodeAlert dca = getDexAlertCode(line);
						if (dca != null && dca.alertId == 10 && !isProductPresent(productLine))
							// no Product Sold Out alert if productId is not found
							dca = null;
						if(dca != null) {
							String details;
							params.put("dexCode", dca.code);
							if(dca.alertId==34){//this is dex exception generated by ePort
								params.put("component", 0); // 0 is base host
								if(dca.code.equals("EA1*EK2M*")){
									if(line.indexOf("ePort DEX")>0){
										details=line.substring(line.indexOf("ePort DEX"));
									}else{
										details=dca.msg;
									}		
								}else{
									details = line;
								}
							}else{
								params.put("component", 1); // 1 is vmc
								if (dca.alertId == 10) 
									// get product from productLine
									details = formatMessage(fileTransferId, dca.msg, productLine);
								else
									details = formatMessage(fileTransferId, dca.msg, line);
							}
							params.put("details", details);
							params.put("alertId", dca.alertId);
							params.put("lineContent", line);
							boolean sendAlert = true;
							
							if(useFullLineAlertFilter){
								//Go looking for same line in previous dex file
								for(DexFileLine previousFileLine : previousFileLines){
									if(line.trim().equalsIgnoreCase(previousFileLine.getLineContents().trim())){
										//TODO add logic checking line number for relative position
										//some dex files contain multiple entries with same content
										sendAlert = false;
										break;
									}
								}
							}
							
							if(sendAlert){
								//Do what has always been done to send the alert
								params.put("alertStatus", dca.alertStatus);
								DataLayerMgr.executeCall(conn, "INSERT_DEX_FILE_LINE", params);
								
								if(dca.alertStatus == AlertStatus.ACTIVE) {
									if(!ConvertUtils.getBooleanSafely(params.get("ignore"), false)) {
										if(dca.alertId == 34) {
											exceptionCnt++;
										} else {
											alertCnt++;
										}
									}
									alertDetails.add(details);
								}
							} else {
								//writing to the terminal alert table happens in the DEX_DATA pkg
								//and is controlled by the alertStatus value. By setting it 
								//to Inactive the row is written into the dex file line table but
								//not the terminal alert table
								params.put("alertStatus", AlertStatus.INACTIVE);
								DataLayerMgr.executeCall(conn, "INSERT_DEX_FILE_LINE", params);								
							}
						}
						writeDexContent(dexContent, line, removeNonAscii);
						eol = contentReader.readEOL();
						if(eol != null)
							dexContent.write(eol);
					}
				}
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				if(dexContentClob != null)
					try {
						dexContentClob.free();
					} catch(SQLException e) {
					}
				DbUtils.closeSafely(conn);
			}
		} catch(IOException e) {
			throw new ServiceException("Could not read file content of DEX file " + fileName + ", fileTransferId: " + fileTransferId, e);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Could not import DEX file " + fileName + ", fileTransferId: " + fileTransferId, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not import DEX file " + fileName + ", fileTransferId: " + fileTransferId, e);
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not import DEX file " + fileName + ", fileTransferId: " + fileTransferId, e, WorkRetryType.NO_RETRY);
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException("Could not import DEX file " + fileName + ", fileTransferId: " + fileTransferId, e, WorkRetryType.NO_RETRY);
		}
		log.warn("No DXE* marker found in DEX file " + fileName + ", fileTransferId: " + fileTransferId + ", not importing");
		return null;
	}
	
	public static DexParseResult parseDexFile(long fileTransferId, String fileName, EnhancedBufferedReader contentReader, String deviceSerialCd, Calendar fileTransferTs, boolean sendOn) throws ServiceException {
		return parseDexFile(fileTransferId, fileName, contentReader, deviceSerialCd, fileTransferTs, sendOn, null);
	}

	protected static void writeDexContent(Writer dexContent, String line, boolean removeNonAscii) throws IOException {
		writeDexContent(dexContent, line, 0, line.length(), removeNonAscii);
	}

	protected static void writeDexContent(Writer dexContent, String line, int offset, int length, boolean removeNonAscii) throws IOException {
		if(removeNonAscii)
			for(int i = offset; i < length; i++) {
				char ch = line.charAt(i);
				if(ch >= ' ' && ch <= '~')
					dexContent.write(ch);
			}
		else
			dexContent.write(line, offset, length);

	}

	protected static DexCodeAlert getDexAlertCode(String line) {
		DexCode dc = getDexCode(line);
		if(dc instanceof DexCodeAlert)
			return (DexCodeAlert) dc;
		return null;
	}

	protected static DexCode getDexCode(String line) {
		int index = Collections.binarySearch(dexCodes, line);
		if(index == -1)
			return null; // not found in list
		if(index < 0)
			index = -index - 2;
		DexCode dc = dexCodes.get(index);
		if(line.startsWith(dc.code))
			return dc;
		int p = 0;
		for(; p < Math.min(dc.code.length(), line.length()); p++)
			if(dc.code.charAt(p) != line.charAt(p))
				break;
		if(p >= line.length())
			return null;
		return getDexCode(line.substring(0, p));
	}

	protected static String formatMessage(long fileTransferId, String alertMessage, String fileLine) {
		StringBuilder sb = new StringBuilder(alertMessage);
		String fileLineInfo[] = fileLine.split("\\*");
		try {
			for(int i = sb.toString().indexOf("{~"); i > -1;  i = sb.toString().indexOf("{~", i)) {
				i += 2;
				int i1 = sb.toString().indexOf("~}", i);
				if(i1 < 0) throw new ParseException("Could not fully format alert message: Missing '~}' in '" + sb.toString(), i);
				int i0 = sb.toString().indexOf("-", i);
				if(i0 < 0) throw new ParseException("Could not fully format alert message: Missing '-' in '" + sb.toString(), i);
				/*int contextId;
				try {
					contextId = Integer.parseInt(sb.substring(i,i0));
				} catch(NumberFormatException nfe) {
					throw new ParseException("Could not fully format alert message: Couldn't parse context id in '" + sb.toString(), i);
				}
				DexCodeContext dcc = (DexCodeContext) contextIds.get(new Integer(contextId));*/
				String inner = sb.substring(i0+1, i1);
				try {
					inner = EnhancedMessageFormat.format(inner, fileLineInfo);
				} catch(Exception e) {
					ParseException pe = new ParseException("Could not fully format alert message: Couldn't format inner message, '" + inner + "'", i0 + 1);
					pe.initCause(e);
					throw pe;
				}
				sb.replace(i-2,i1+2,inner);
				i = i + inner.length();
			}
			
			try {
				return EnhancedMessageFormat.format(sb.toString(), fileLineInfo);
			} catch(Exception e) {
				ParseException pe = new ParseException("Could not format alert message: '" + sb.toString() + "'", 0);
				pe.initCause(e);
				throw pe;
			}
		} catch (ParseException e) {
			log.error("Error formatting DEX alert message '" + alertMessage + "' for line '" + fileLine + "'; using raw", e);
			return "UNPARSEABLE: " + fileLine;
		}
	}
	
	private static boolean isProductPresent(String productLine) {
		if (productLine == null || productLine.isEmpty())
			return false;
		String[] tokens = productLine.split("\\*");
		if (tokens.length < 2 || tokens[1].trim().length() == 0)
			return false;
		return true;
	}

	protected static class DexCode implements Comparable<Object> {
		public DexCode(String code) {
			this.code = code;
		}

		public final String code;
		//protected String info[];
		public int compareTo(Object o) {
			String s = (o instanceof DexCode ? ((DexCode)o).code : o.toString());
			return code.compareTo(s);
		}
	}

	protected static class DexCodeAlert extends DexCode {
		public DexCodeAlert(String code, int alertId, String message, AlertStatus alertStatus) {
			super(code);
			this.alertId = alertId;
			this.msg = message;
			this.alertStatus = alertStatus;
		}

		public final int alertId;
		public final String msg;
		public final AlertStatus alertStatus;
	}

	/*
	protected static class DexCodeContext extends DexCode {
		public DexCodeContext(String code, int contextId) {
			super(code);
			this.contextId = contextId;
			contextIds.put(new Integer(contextId),this);
		}

		public final int contextId;
	}	
	*/
	
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String getRequestQueuePrefix() {
		return requestQueuePrefix;
	}

	public void setRequestQueuePrefix(String requestQueuePrefix) {
		this.requestQueuePrefix = requestQueuePrefix;
	}

	public Publisher<ByteInput> getReportingPublisher() {
		return reportingPublisher;
	}

	public void setReportingPublisher(Publisher<ByteInput> reportingPublisher) {
		this.reportingPublisher = reportingPublisher;
	}
}
