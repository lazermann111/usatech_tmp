
package com.usatech.layers.common.dex;

import java.util.*;

public class DexRead 
{
	public List<DexDataRecord> records;
	public List<DexAlert> alerts;
	
	public Dictionary<String, DexDataColumn> columns;
	
	public String contents;
	public String device;
	public String machineNumber;
	public String machineType;
	
	public double totalSales = 0.0;
	public int totalVends = 0;
	
	public double cashSales = 0.0;
	public int cashVends = 0;
	
	public double creditSales = 0.0;
	public int creditVends = 0;
		
	public double creditSalesAlt = 0.0;
	public int creditVendsAlt = 0;
		
	public java.util.Date readDate;
	
	public DexField[] colFields;
	
	public boolean isErrorRead = false;
	public String errorData;
	
	public void initialize()
	{
		List<DexDataRecord> recs = getAllRecordsOfType(DexConstants.primaryDateLineRecord);
		
		if ( recs == null || recs.size() == 0 )
		{
			recs = getAllRecordsOfType(DexConstants.secondaryDateLineRecord);
			for (DexDataRecord record : recs )
			{
				String field = record.getString(8);
				
				if ( field.length() > 0 )
				{
					readDate = record.getDateTime(2);
					
					java.util.Date dtTime = record.getDateTime(3);
					
					readDate.setHours(dtTime.getHours());
					readDate.setMinutes(dtTime.getMinutes());
				}
			}
		}
		else
		{
			for (DexDataRecord record : recs )
			{
				readDate = record.getDateTime(1);
				
				java.util.Date dtTime = record.getDateTime(2);
				
				readDate.setHours(dtTime.getHours());
				readDate.setMinutes(dtTime.getMinutes());
			}
		}
		
        DexField priceField = getField(DexParser.DexFieldColumnPrice);
        DexField vendField = getField(DexParser.DexFieldColumnVends);
        DexField salesField = getField(DexParser.DexFieldColumnSales);
        
        DexField testVField = getField(DexParser.DexFieldTestVends);
        DexField testSField = getField(DexParser.DexFieldTestSales);
        
        DexField freeVField = getField(DexParser.DexFieldFreeVends);
        DexField freeSField = getField(DexParser.DexFieldFreeSales);

        colFields = new DexField[] { priceField, vendField, salesField, testVField, testSField, freeVField, freeSField };

	}
	
	public void setErrorRead(String data)
	{
		this.isErrorRead = true;
		this.errorData = data;
	}
	
	public void parse()
	{
		DexMachineConfiguration map = DexParser.defaultConfiguration;
		DexDataRecord rec = getRecord(map.machineIdRecordName);
		
		if ( rec != null )
		{
            machineType = rec.getString(map.machineIdFieldIndex);
		}

		loadAll();
	}
	
	public void loadAll()
	{
		loadMainRecords();

		loadColumns();
	
		loadAlerts();		
	}
	
	private DexField getField(int type)
	{
        //DexField field = DexParser.defaultConfiguration.getField(type);
        DexField field = DexParser.getMatchingMachineConfiguration(machineType).getField(type);

		return field;
	}

	private boolean doesFieldExist(int type)
	{
        //DexField field = DexParser.defaultConfiguration.getField(type);
        DexField field = DexParser.getMatchingMachineConfiguration(machineType).getField(type);

        if ( field == null )
        {
        	return false;
        }
		return true;
	}

	@SuppressWarnings("unused")
	private boolean doesRecordExist(int type)
	{
        //DexField field = DexParser.defaultConfiguration.getField(type);
        DexField field = DexParser.getMatchingMachineConfiguration(machineType).getField(type);

        if ( field == null )
        {
        	return false;
        }

        DexDataRecord record = getRecord(field.recordName);

		if (record == null)
		{
            return false;
		}
		return true;
	}

	public String loadStringField(int fieldId, String defaultValue)
	{
		DexField field;
		DexDataRecord record;
		String val = "";

		field = getField(fieldId);
		if(field != null)
		{
			record = getRecord(field.recordName);

			if(record != null)
			{
                val = record.getString(field.recordFieldIndex);
			}
			else
			{
                val = defaultValue;
			}
		}
	
		return val;
	}
	
	public double loadDoubleField(int fieldId, double defaultValue)
	{
		DexField field;
		DexDataRecord record;
		double val = 0.0;

		field = getField(fieldId);
		if(field != null)
		{
			record = getRecord(field.recordName);

			if(record != null)
			{
                val = record.getDouble(field.recordFieldIndex) * field.dexFieldFactor;
			}
			else
			{
                val = defaultValue;
			}
		}
	
		return val;
	}
	
	public int loadIntegerField(int fieldId, int defaultValue)
	{
		DexField field;
		DexDataRecord record;
		int val = 0;

		field = getField(fieldId);
		if(field != null)
		{
			record = getRecord(field.recordName);

			if(record != null)
			{
                val = record.getInteger(field.recordFieldIndex);
			}
			else
			{
                val = defaultValue;
			}
		}
	
		return val;
	}
	
	public double loadDoubleField(int fieldId, double defaultValue,
			int altFieldId)
	{
		DexField field;
		DexDataRecord record;
		double val = 0.0;

		field = getField(fieldId);
		if(field != null)
		{
			record = getRecord(field.recordName);

			if(record != null)
			{
                val = record.getDouble(field.recordFieldIndex) * field.dexFieldFactor;
			}
			else
			{
                val = defaultValue;
			}
		}
	
		return val;
	}
	
	public int loadIntegerField(int fieldId, int defaultValue,
			int altFieldId)
	{
		DexField field;
		DexDataRecord record;
		int val = 0;

		field = getField(fieldId);
		if(field != null)
		{
			record = getRecord(field.recordName);

			if(record != null)
			{
                val = record.getInteger(field.recordFieldIndex);
			}
			else
			{
                val = defaultValue;
			}
		}
			
	
		return val;
	}
	
	public boolean hasAltCredit()
	{
		if ( this.creditSales == 0 && this.creditSalesAlt > 0 )
		{
			return true;
		}
		return false;
	}
	
	public void loadMainRecords()
	{
		this.machineNumber = loadStringField( DexParser.DexFieldMachineNumber, "" );

		this.totalSales = loadDoubleField(DexParser.DexFieldTotalSales, 0);
		this.totalVends = loadIntegerField(DexParser.DexFieldTotalVends, 0);

		this.creditSales = loadDoubleField(DexParser.DexFieldCreditSales, 0);
		this.creditVends = loadIntegerField(DexParser.DexFieldCreditVends, 0);
		
		this.creditSalesAlt = loadDoubleField(DexParser.DexFieldCreditSalesAlt, 0);
		this.creditVendsAlt = loadIntegerField(DexParser.DexFieldCreditVendsAlt, 0);
		
		//this.cashSales = loadDoubleField( DexParser.DexFieldCashSales, totalSales - creditSales );
		//this.cashVends = loadIntegerField( DexParser.DexFieldCashVends, totalVends - creditVends );
		this.cashSales = loadDoubleField( DexParser.DexFieldCashSales, 0 );
		this.cashVends = loadIntegerField( DexParser.DexFieldCashVends, 0 );

		boolean calculatedTotal = false;
		boolean calculatedCash = false;
		
		double testCreditSales = creditSales;
		if ( creditSales == 0 && creditSalesAlt > 0 )
		{
			testCreditSales = creditSalesAlt;
		}
		int testCreditVends = creditVends;
		if ( creditVends == 0 && creditVendsAlt > 0 )
		{
			testCreditVends = creditVendsAlt;
		}

		if ( doesRecordExist(DexParser.DexFieldTotalSales) == false )
		{
            totalSales = cashSales + testCreditSales;
            calculatedTotal = true;
		}
		if ( doesRecordExist(DexParser.DexFieldTotalVends) == false )
		{
            totalVends = cashVends + testCreditVends;
            calculatedTotal = true;
		}
		
		if ( calculatedTotal )
		{
			return;
		}

		if ( doesRecordExist(DexParser.DexFieldCashSales) == false )
		{
			if ( doesFieldExist(DexParser.DexFieldCreditSales) == false )
			{
				cashSales = totalSales;
			}
			else
			{
				cashSales = totalSales - testCreditSales;
			}
            calculatedCash = true;
		}
		if ( doesRecordExist(DexParser.DexFieldCashVends) == false )
		{
			if ( doesFieldExist(DexParser.DexFieldCreditVends) == false )
			{
				cashVends = totalVends;
			}
			else
			{
				cashVends = totalVends - testCreditVends;
			}
            calculatedCash = true;
		}
		
		if ( calculatedCash == true ) 
		{
			return;
		}
		
		boolean calculatedCredit = false;
		
		if ( ( doesRecordExist(DexParser.DexFieldCreditSales) == false ) 
				&& ( cashSales != 0  ) )
		{
			creditSales = totalSales - cashSales;
			calculatedCredit = true;
		}
		if ( ( doesRecordExist(DexParser.DexFieldCreditVends) == false )
				&& ( cashVends != 0 ) ) 
		{
			creditVends = totalVends - cashVends;
			calculatedCredit = true;
		}
		
		if ( calculatedCredit == true ) return;
		
		if ( totalVends > 0 && cashVends == 0 )
		{
			cashVends = totalVends - testCreditVends;
		}
		if ( totalSales > 0 && cashSales == 0 )
		{
			cashSales = totalSales - testCreditSales;
		}
	}
	
	public void loadAlerts()
	{
		alerts = new ArrayList<DexAlert>();
		
		List<DexDataRecord> recordsEA1 = getAllRecordsOfType("EA1");

		for(DexDataRecord recordEA1 : recordsEA1)
		{
			DexAlert alertEA1 = new DexAlert();
			
			alertEA1.alertRecord = recordEA1;
			
			alertEA1.alertType = recordEA1.getString(1);
			alertEA1.alertDate = recordEA1.getDateTime(2);
			
			java.util.Date dtTime = recordEA1.getDateTime(3);
			
			alertEA1.alertDate.setHours(dtTime.getHours());
			alertEA1.alertDate.setMinutes(dtTime.getMinutes());
			
			alertEA1.alertValue = recordEA1.getString(4);
			
			alerts.add( alertEA1 );
		}	
	
		List<DexDataRecord> recordsEA2 = getAllRecordsOfType("EA2");

		for(DexDataRecord recordEA2 : recordsEA2)
		{
			DexAlert alertEA2 = new DexAlert();
			
			alertEA2.alertRecord = recordEA2;
			
			alertEA2.alertType = recordEA2.getString(1);
			alertEA2.alertDate = recordEA2.getDateTime(2);
			
			java.util.Date dtTime = recordEA2.getDateTime(3);
			
			alertEA2.alertDate.setHours(dtTime.getHours());
			alertEA2.alertDate.setMinutes(dtTime.getMinutes());
			
			alertEA2.alertValue = recordEA2.getString(4);
			
			alerts.add(alertEA2);
		}
		
		List<DexDataRecord> recordsMA5 = getAllRecordsOfType("MA5");

		for(DexDataRecord recordMA5 : recordsMA5)
		{
			DexAlert alertMA5 = new DexAlert();
			
			alertMA5.alertRecord = recordMA5;
			
            String data = recordMA5.getString(2);

            if (data.isEmpty() == false)
            {
                data = data.trim();
                int pos = data.length() - 11;

                if (pos > 0)
                {
                    String datePart = data.substring(pos);

                    alertMA5.alertValue = data.substring(0, pos);

                    alertMA5.alertValue = alertMA5.alertValue.trim();

                    try
                    {
                        int month = Integer.parseInt(datePart.substring(0, 2));
                        int day = Integer.parseInt(datePart.substring(3, 5));
                        int hour = Integer.parseInt(datePart.substring(6, 8));
                        int minute = Integer.parseInt(datePart.substring(9, 11));

                        java.util.Date now = new java.util.Date();
                        
                        alertMA5.alertDate = new java.util.Date(now.getYear(), 
                        		month, day, hour, minute, 0);
                    }
                    catch( Exception ex)
                    {
                      alertMA5.alertDate = new java.util.Date();

                    }

                    alertMA5.alertType = recordMA5.getString(1);
                    
                    alerts.add(alertMA5);
                }
            }


		}	

	}
	
	public void loadColumns()
	{
        columns = new Hashtable<String, DexDataColumn>();

		List<DexDataRecord> records = getAllColumnRecords();

		for(DexDataRecord record : records)
		{
			DexDataColumn column = new DexDataColumn();
			DexDataRecord rec = record;

			column.columnName = record.getString(1);

			while( true )
			{
                for (DexField field : colFields)
				{
					if(field != null)
					{
						if(rec.getType().equals( field.recordName))
						{
							if ( field.dexFieldType == DexParser.DexFieldColumnPrice )
							{
                                column.columnPrice = rec.getDouble(field.recordFieldIndex) * field.dexFieldFactor;
							}
							
							if ( field.dexFieldType == DexParser.DexFieldColumnVends )
							{
                                column.columnVends = rec.getInteger(field.recordFieldIndex);
							}
							
							if ( field.dexFieldType == DexParser.DexFieldColumnSales )
							{
                                column.columnSales = rec.getDouble(field.recordFieldIndex) * field.dexFieldFactor;
							}
							
							if ( field.dexFieldType == DexParser.DexFieldFreeVends )
							{
                                column.freeVends = rec.getInteger(field.recordFieldIndex);
							}
							
							if ( field.dexFieldType == DexParser.DexFieldFreeSales )
							{
                                column.freeSales = field.dexFieldFactor * rec.getDouble(field.recordFieldIndex);
							}
							
							if ( field.dexFieldType == DexParser.DexFieldTestVends )
							{
                                column.testVends = rec.getInteger(field.recordFieldIndex);
							}
							
							if ( field.dexFieldType == DexParser.DexFieldTestSales )
							{
                                column.testSales = rec.getDouble(field.recordFieldIndex) * field.dexFieldFactor;
							}
							
						}
						
					}

				}

				rec = rec.nextDexRecord;

				if ( rec == null )
				{
					break;
				}
				if ( rec.getType().equals("PA1") )
				{
					break;
				}
			} 

            columns.put(column.columnName, column);
		}	
	}
	
	public DexDataColumn getColumn(String name)
	{
		return columns.get(name);
	}
	
	public boolean containsVends()
	{
		for ( int i = 0; i < columns.size(); i++)
		{
			DexDataColumn col = columns.get(i);
			
			if ( col.columnVends > 0  )			
			{
				return true;
			}
	
		}
		
		return false;
	}
	
	public DexDataRecord getRecord( String type )
	{
		for ( DexDataRecord rec : records )
		{
			if ( rec.getType().equals( type) )
			{
				return rec;
			}
		}
		
		return null;
	}
	
	public DexDataRecord findRecord( String type, String data )
	{
		List<DexDataRecord> recs = getAllRecordsOfType(type);
		
		for (int i = 0; i < recs.size(); i++ )
		{
			DexDataRecord rec = recs.get(i);
			
			for(int j = 1; j < rec.values.length; j++)
			{
				String field = rec.getString(j);

				data = data.toLowerCase();
				field = field.toLowerCase();
				
				if(field.indexOf(data) != -1)
				{
					return rec;
				}
			}
			
		}
		
		return null;
	}
	
	public List<DexDataRecord> getAllRecordsOfType( String type )
	{
		List<DexDataRecord> temprec = new ArrayList<DexDataRecord>();
		
		for ( int i = 0; i < records.size(); i++ )
		{
			DexDataRecord rec = records.get(i);
			if ( rec.getType().equals(type) )
			{
				temprec.add(rec);
			}
		}
		
		return temprec;
	}
	
	public List<DexDataRecord> getAllColumnRecords()
	{	
		return getAllRecordsOfType( "PA1");
		
	}
}
