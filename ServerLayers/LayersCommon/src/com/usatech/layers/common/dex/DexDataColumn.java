
package com.usatech.layers.common.dex;

public class DexDataColumn 
{
    public String columnName = "";
    public double columnPrice = 0.0;
    public int columnVends = 0;
    public double columnSales = 0.0;
    
    public static DexDataColumn subtractColumns( DexDataColumn now, DexDataColumn before)
    {
    	DexDataColumn col = new DexDataColumn();
    	
    	col.columnName = now.columnName;
    	col.columnPrice = now.columnPrice;
    	
    	col.columnSales = Math.max( 0, now.columnSales - before.columnSales);
    	col.columnVends = Math.max( 0, now.columnVends - before.columnVends);
    	
    	col.freeSales = Math.max( 0, now.freeSales - before.freeSales);
    	col.freeVends = Math.max( 0, now.freeVends - before.freeVends);

    	col.testSales = Math.max( 0, now.testSales - before.testSales);
    	col.testVends = Math.max( 0, now.testVends - before.testVends);

    	return col;
    }

    public int freeVends = 0;
    public double freeSales = 0.0;
    public int testVends = 0;
    public double testSales = 0.0;
}
