
package com.usatech.layers.common.dex;

import java.text.SimpleDateFormat;
import java.util.*;

public class DexDataRecord 
{
	public String[] values;
	
	public String dexLine;
	
	public String type;
	
	public int index;
	
	public DexDataRecord nextDexRecord;
	
	public DexDataRecord(String line)
	{
		dexLine = line;
		
		values = line.split( DexParser.DexRecordDelimiter );

		if ( values == null || values.length == 0 )
		{
			type = "";
		}
		else
		{
			type = values[0].trim();
		}
}
	
	public String getType()
	{
		return type;
	}
	public String getString( int index )
	{
		if ( index >= values.length )
		{
			return "";
		}
		
		return values[index].trim();
	}
	
	public int getInteger( int index )
	{
		if ( index >= values.length )
		{
			return 0;
		}
		
		try
		{
			return Integer.parseInt( values[index].trim() );
		}
		catch( Exception ex)
		{
		}
		
		return 0;
		
	}
	
	public double getDouble( int index )
	{
		if ( index >= values.length )
		{
			return 0.0;
		}
		
		try
		{
			return Double.parseDouble( values[index].trim() );
		}
		catch( Exception ex)
		{
		}
		
		return 0.0;
		
	}
	
	public java.util.Date getDateTime( int index )
	{
		SimpleDateFormat format =
            new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat format2 =
            new SimpleDateFormat("HH:mm:ss");

		//java.util.Date dt = format.parse(xml.substring(0,10));
		
		java.util.Date dt = new java.util.Date(java.util.Date.parse("1/1/1900"));
		
		if ( index >= values.length )
		{
			return dt;
		}
		
		String val = values[index].trim();
		
		int year = 1900;
		int day = 1;
		int month = 1;
		int hour = 0;
		int minute = 0;

		java.util.Date dtNow = new java.util.Date();
		String sdt = "";
		
		try
		{
			if(val.length() == 4)
			{
				sdt = "1/1/1900 " + val.substring(0, 2) + ":" + val.substring(2, 4);
				
				hour = Integer.parseInt(val.substring(0, 2),10);
				minute = Integer.parseInt(val.substring(2, 4),10);
			}
			else if(val.length() == 6)
			{
				sdt = val.substring(2, 4) + "/" + val.substring(4, 6) +"/" +
					val.substring(0, 2) + " 00:00";
				
				year = 2000 + Integer.parseInt(val.substring(0, 2),10);
				month = Integer.parseInt(val.substring(2, 4),10);
				day = Integer.parseInt(val.substring(4, 6),10);
			}

			dt = new java.util.Date(java.util.Date.parse(sdt));
			
			
			//dt.setYear( year );
			//dt.setMonth(month);
			//dt.setDate(day);
			//dt.setHours(hour);
			//dt.setMinutes(minute);
			
		}
		catch( Exception ex)
		{
		}

		return dt;
		
	}
	
	
}
