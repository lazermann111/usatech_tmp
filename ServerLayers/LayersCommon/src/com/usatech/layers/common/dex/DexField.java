
package com.usatech.layers.common.dex;

import java.util.*;

public class DexField 
{
	public String recordName;
	
	public int recordFieldIndex;
	
	public String dataTypeName;
	
	public int dexFieldType;
	
	public double dexFieldFactor;
}
