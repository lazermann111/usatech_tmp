
package com.usatech.layers.common.dex;

import java.util.*;

public class  DexMachineConfiguration
{
    public DexField getField(int type)
    {
        for (DexField field : dexFields)
        {            
            if (field.dexFieldType == type)
                return field;
        }

        return null;
    }

	public String name;
	
	public String machineIdRecordName;
	
	public int machineIdFieldIndex;
	
	public String machineIdFieldValue;
	
	public boolean defaultConfiguration;
	
	public List<DexField> dexFields;
	
}
