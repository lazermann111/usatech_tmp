
package com.usatech.layers.common.dex;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

public class DexParser 
{
	public static final String DexStart = "ST*";
	public static final String DexNewStart = "\nST*";
	public static final String DexRecordDelimiter = "\\*";
	
	public static Dictionary<String, DexMachineConfiguration> machineConfigurations;
	public static DexMachineConfiguration defaultConfiguration;
	
	public static final int DexFieldColumnName = 1000;
	public static final int DexFieldColumnPrice = 1001;
	public static final int DexFieldColumnVends = 1002;
	public static final int DexFieldColumnSales = 1003;
	
	public static final int DexFieldMachineSerialNumber = 2000;
	public static final int DexFieldMachineNumber = 2001;

	public static final int DexFieldCashIn = 3000;
	public static final int DexFieldCashOut = 3001;
	public static final int DexFieldCashVends = 3002;
	public static final int DexFieldCashSales = 3003;

/*
	public static final int DexFieldBoxCash = 4000;
	public static final int DexFieldTubeCash = 4001;
	public static final int DexFieldBillsToStacker = 4002;
*/	
	public static final int DexFieldCreditVends = 5000;
	public static final int DexFieldCreditSales = 5001;
	public static final int DexFieldFreeVends = 5002;
	public static final int DexFieldFreeSales = 5003;
	public static final int DexFieldTestVends = 5004;
	public static final int DexFieldTestSales = 5005;
	public static final int DexFieldTotalVends = 5006;
	public static final int DexFieldTotalSales = 5007;

	// HACK for CRANE
	public static final int DexFieldCreditVendsAlt = 5008;
	public static final int DexFieldCreditSalesAlt = 5009;

	public static List<String> separateDexReads(String dex)
	{
		List<String> list = new ArrayList<String>();
		int spos = 0;
		int epos = 0;
				
		while (true)
		{
			spos = dex.indexOf( DexStart, epos);
			
			if ( spos != -1 )
			{
				epos = dex.indexOf( DexNewStart, spos + 1);
				
				if ( epos > 0 )
				{
					list.add( dex.substring( spos, epos - spos ));
				}
				else
				{
					list.add( dex.substring(spos));
					break;
				}
			}
			else
			{
				break;
			}
		}
		
		return list;
	}
	
	public static DexField newField( String recordName, int recordFieldIndex,
			String dataTypeName, int dexFieldType, double factor)
	{
		DexField fm = new DexField();
		
		fm.recordName = recordName;
		fm.recordFieldIndex = recordFieldIndex;
		fm.dataTypeName = dataTypeName;
		fm.dexFieldType = dexFieldType;
		fm.dexFieldFactor = factor;
		
		return fm;
	}
	
	public static void loadMachineConfiguration()
	{
		// Load it
		DexMachineConfiguration config = new DexMachineConfiguration();
		config.name = "Default";
		config.machineIdRecordName = "ID1";
		config.machineIdFieldIndex = 2;
		config.defaultConfiguration = true;
		
		config.dexFields = new ArrayList<DexField>();
		config.dexFields.add( DexParser.newField("PA1", 1, "", DexParser.DexFieldColumnName, 0.01));
		config.dexFields.add( DexParser.newField("PA1", 2, "", DexParser.DexFieldColumnPrice, 0.01));
		config.dexFields.add( DexParser.newField("PA2", 3, "", DexParser.DexFieldColumnVends, 1.00));
		config.dexFields.add( DexParser.newField("PA2", 4, "", DexParser.DexFieldColumnSales, 0.01));
		config.dexFields.add( DexParser.newField("CA3", 5, "", DexParser.DexFieldCashIn, 0.01));
		config.dexFields.add( DexParser.newField("CA4", 3, "", DexParser.DexFieldCashOut, 0.01));
		//config.dexFields.add( DexParser.newField("CA3", 6, "", DexParser.DexFieldBoxCash, 0.01));
		//config.dexFields.add( DexParser.newField("CA3", 7, "", DexParser.DexFieldTubeCash, 0.01));
		//config.dexFields.add( DexParser.newField("CA3", 8, "", DexParser.DexFieldBillsToStacker, 1.00));
		config.dexFields.add( DexParser.newField("CA2", 2, "", DexParser.DexFieldCashVends, 1.00));
		config.dexFields.add( DexParser.newField("DA2", 4, "", DexParser.DexFieldCreditVends, 1.00));
		config.dexFields.add( DexParser.newField("VA1", 2, "", DexParser.DexFieldTotalVends, 1.00));
		config.dexFields.add( DexParser.newField("CA2", 1, "", DexParser.DexFieldCashSales, 0.01));
		config.dexFields.add( DexParser.newField("DA2", 3, "", DexParser.DexFieldCreditSales, 0.01));
		config.dexFields.add( DexParser.newField("VA1", 1, "", DexParser.DexFieldTotalSales, 0.01));
		config.dexFields.add( DexParser.newField("ID1", 1, "", DexParser.DexFieldMachineSerialNumber, 1.00));
		config.dexFields.add( DexParser.newField("ID1", 1, "", DexParser.DexFieldMachineNumber, 1.00));
		
		// LCB
		DexMachineConfiguration configLCB= new DexMachineConfiguration();
		configLCB.name = "LCB";
		configLCB.machineIdRecordName = "ID1";
		configLCB.machineIdFieldIndex = 2;
		configLCB.defaultConfiguration = true;
		
		configLCB.dexFields = new ArrayList<DexField>();
		configLCB.dexFields.add( DexParser.newField("PA1", 1, "", DexParser.DexFieldColumnName, 0.01));
		configLCB.dexFields.add( DexParser.newField("PA1", 2, "", DexParser.DexFieldColumnPrice, 0.01));
		configLCB.dexFields.add( DexParser.newField("PA2", 3, "", DexParser.DexFieldColumnVends, 1.00));
		configLCB.dexFields.add( DexParser.newField("PA2", 4, "", DexParser.DexFieldColumnSales, 0.01));
		configLCB.dexFields.add( DexParser.newField("CA3", 5, "", DexParser.DexFieldCashIn, 0.01));
		configLCB.dexFields.add( DexParser.newField("CA4", 3, "", DexParser.DexFieldCashOut, 0.01));
		//config.dexFields.add( DexParser.newField("CA3", 6, "", DexParser.DexFieldBoxCash, 0.01));
		//config.dexFields.add( DexParser.newField("CA3", 7, "", DexParser.DexFieldTubeCash, 0.01));
		//config.dexFields.add( DexParser.newField("CA3", 8, "", DexParser.DexFieldBillsToStacker, 1.00));
		configLCB.dexFields.add( DexParser.newField("CA2", 2, "", DexParser.DexFieldCashVends, 1.00));
		configLCB.dexFields.add( DexParser.newField("DA2", 2, "", DexParser.DexFieldCreditVends, 1.00));
		configLCB.dexFields.add( DexParser.newField("VA1", 2, "", DexParser.DexFieldTotalVends, 1.00));
		configLCB.dexFields.add( DexParser.newField("CA2", 1, "", DexParser.DexFieldCashSales, 0.01));
		configLCB.dexFields.add( DexParser.newField("DA2", 1, "", DexParser.DexFieldCreditSales, 0.01));
		configLCB.dexFields.add( DexParser.newField("VA1", 1, "", DexParser.DexFieldTotalSales, 0.01));
		configLCB.dexFields.add( DexParser.newField("ID1", 1, "", DexParser.DexFieldMachineSerialNumber, 1.00));
		configLCB.dexFields.add( DexParser.newField("ID1", 1, "", DexParser.DexFieldMachineNumber, 1.00));
		configLCB.dexFields.add( DexParser.newField("DA2", 4, "", DexParser.DexFieldCreditVendsAlt, 1.00));
		configLCB.dexFields.add( DexParser.newField("DA2", 3, "", DexParser.DexFieldCreditSalesAlt, 0.01));
		
		machineConfigurations = new Hashtable<String, DexMachineConfiguration>();
		
		machineConfigurations.put( "Default", configLCB );
		machineConfigurations.put( "LCB", configLCB );
		machineConfigurations.put( "VEC", configLCB );
		machineConfigurations.put( "ROYAL", configLCB );
		machineConfigurations.put( "RVV", configLCB );
		machineConfigurations.put( "DN", configLCB );
		machineConfigurations.put( "CRANE", configLCB );
		machineConfigurations.put( "STXXX", configLCB );
		machineConfigurations.put( "SLXXX", configLCB );
		machineConfigurations.put( "KO_GIII", configLCB );
		machineConfigurations.put( "AMS", configLCB );
		machineConfigurations.put( "ETI", configLCB );
		machineConfigurations.put( "F820", configLCB );
		machineConfigurations.put( "UNI", configLCB );
		machineConfigurations.put( "501E", configLCB );
		machineConfigurations.put( "181", configLCB );
		machineConfigurations.put( "VEI", configLCB );
		
		Enumeration e = machineConfigurations.elements();
		
		while ( e.hasMoreElements())
		{
			DexMachineConfiguration def = (DexMachineConfiguration)e.nextElement();
			if (def.defaultConfiguration)
			{
				defaultConfiguration = def;
				return;
			}
		}
	}
	
	public static DexMachineConfiguration getMachineConfiguration(String machineType)
	{
		if ( machineConfigurations == null ) return defaultConfiguration;
		
		return machineConfigurations.get( machineType );
	}
		
	public static DexMachineConfiguration getMatchingMachineConfiguration( String match ) 
	{
		if ( machineConfigurations == null ) return defaultConfiguration;
		if ( match == null || match.isEmpty() ) return defaultConfiguration;

		Enumeration e = machineConfigurations.elements();
		Enumeration ekey = machineConfigurations.keys();
		
		while ( e.hasMoreElements())
		{
			String key = (String)ekey.nextElement();
			DexMachineConfiguration def = (DexMachineConfiguration)e.nextElement();
			if (match.startsWith(key))
			{
				return def;
			}
		}
		
		return defaultConfiguration;
	}
	
	public static List<DexRead> parseDexFile( String path )
	{
		File file = new File(path);
		String sfile = file.getName();
		
		String[] parts = sfile.split( "-" );
		
		String machineNumber = "";
		String fileDate = "";
		
		if ( parts.length >= 2 )
		{
			machineNumber = parts[1];
		}
		if ( parts.length >= 4 )
		{
			fileDate = parts[3] + " " + parts[2];

		}
		
		int ch;    
		StringBuffer content = new StringBuffer("");    
		FileInputStream fin = null;     
		try    
		{
			fin = new FileInputStream(file);       
			while( (ch = fin.read()) != -1)        
				content.append((char)ch);             
			fin.close();      
		}    
		catch(Exception e)    
		{     
		
		}
		
		return DexParser.parse(content.toString(), machineNumber, fileDate);
	}

	public static List<DexRead> parse( String contents, String machineNumber,
			String fileDate)
	{
		// Check the DEX file for DEX errors
		if ( contents.contains("Please check") || contents.contains("Error="))
		{
			List<DexRead> errorReadList = new ArrayList<DexRead>();
			DexRead errorRead = new DexRead();
			errorRead.machineNumber = machineNumber;
			try {
				SimpleDateFormat format =
		            new SimpleDateFormat("MMddyy HHmmss");
				errorRead.readDate = format.parse( fileDate );
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			errorRead.errorData = contents;
			errorRead.isErrorRead = true;
			
			errorReadList.add( errorRead );
			return errorReadList;
		}
		
		List<String> reads = separateDexReads(contents);
		
		List<DexRead> readList = new ArrayList<DexRead>();
		
		for ( int i = 0; i < reads.size(); i++ )
		{
			String read = reads.get(i);
			
			DexRead dexRead = new DexRead();
			
			dexRead.records = new ArrayList<DexDataRecord>();
			
			BufferedReader reader = new BufferedReader(new StringReader(read));
			String full = "";
			
			try 
			{
				String dexLine = "";				
				DexDataRecord prev = null;
				
				while ( (dexLine = reader.readLine()) != null )
				{					
					if ( dexLine.length() > 0 )
					{
						full += dexLine + "\n";
						
						if ( dexLine.startsWith("VA1"))
						{
							// Some machines, especially CRANE, blur the VA1 line and the 
							// first EA1 line, we need to check and separate
							if ( dexLine.indexOf("EA1") == -1 )
							{
								DexDataRecord rec = new DexDataRecord(dexLine);
								
								if(prev != null)
								{
									prev.nextDexRecord = rec;
								}
								
								dexRead.records.add(rec);
								
								prev = rec;
							}
							else
							{
								// This is really two lines, separate them
								int lineSplit = dexLine.indexOf("EA1");
								String line1 = dexLine.substring(0, lineSplit);
								String line2 = dexLine.substring(lineSplit);
								
								line1 = line1 + "\n";
								line2 = line2 + "\n";
								
								DexDataRecord rec1 = new DexDataRecord(line1);
								DexDataRecord rec2 = new DexDataRecord(line2);
								
								rec1.nextDexRecord = rec2;
								
								if(prev != null)
								{
									prev.nextDexRecord = rec1;
								}
								
								dexRead.records.add(rec1);
								dexRead.records.add(rec2);
								
								prev = rec2;
							}
						}
						else
						{
							DexDataRecord rec = new DexDataRecord(dexLine);
							
							if(prev != null)
							{
								prev.nextDexRecord = rec;
							}
							
							dexRead.records.add(rec);
							
							prev = rec;
						}
					}
				}
			} 
			catch(IOException e) 
			{
				e.printStackTrace();
			} 
			
			dexRead.contents = full;
			
			dexRead.initialize();
			
			dexRead.parse();
			
			dexRead.machineNumber = machineNumber;
			
			java.util.Date dtFromFile = null;
			
			try {
				if ( fileDate != null && fileDate.length() > 0 )
				{
					SimpleDateFormat format =
			            new SimpleDateFormat("MMddyy HHmmss");
					dtFromFile = format.parse( fileDate );
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// If the file year does not match the DEX year, take the file date
			if ( dexRead.readDate == null || 
				( ( dtFromFile != null ) && 
				   ( dtFromFile.getYear() != dexRead.readDate.getYear()) ) )
			{
				dexRead.readDate = dtFromFile;
			}

			readList.add(dexRead);
		}
		
		return readList;
	}
	
}
