package com.usatech.layers.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.sql.rowset.serial.SerialBlob;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.app.task.BridgeTask;
import com.usatech.layers.common.ProcessingUtils.PropertyValueHandler;
import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.AlertSourceCode;
import com.usatech.layers.common.constants.AlertingAttrEnum;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.CredentialResult;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventDetailType;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.FillEventAuthType;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.GxBezelType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ResultCode;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.ServerActionCode;
import com.usatech.layers.common.constants.SessionControlAction;
import com.usatech.layers.common.constants.TranBatchType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.constants.UpdateStatusType;
import com.usatech.layers.common.messagedata.Component;
import com.usatech.layers.common.messagedata.InitMessageData;
import com.usatech.layers.common.messagedata.LineItem;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CA.PropertyListRequest;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.Sale;
import com.usatech.layers.common.messagedata.WSMessageData;
import com.usatech.layers.common.messagedata.WSResponseMessageData;

import simple.app.DatabasePrerequisite;
import simple.app.DialectResolver;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call.BatchUpdate;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.db.ParameterException;
import simple.io.BufferStream;
import simple.io.ByteArrayUtils;
import simple.io.ByteInput;
import simple.io.HeapBufferStream;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.lang.Holder;
import simple.lang.InvalidByteArrayException;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidIntValueException;
import simple.lang.InvalidValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.security.SecurityUtils;
import simple.sql.BlobableBufferStream;
import simple.text.StringUtils;
import simple.translator.TranslatorFactory;
import simple.util.IntegerRangeSet;
import simple.util.TimeUtils;

public class AppLayerUtils {
	private static final Log log = Log.getLog();
	private static SecureRandom random;
    private static String randomError;
	private static MessageDigest hash;
	private static String hashError;
	protected static String emailQueueHistQueueKey = "usat.email.sent";
	protected static int validDeviceTimeDays = 365;
	protected static Pattern cdmaModemExpression = Pattern.compile("^(CC864|CE910)-DUAL");
	protected static String requestQueuePrefix = "usat.report.request.";
	protected static Publisher<ByteInput> reportingPublisher;
	protected static Pattern meiIgnorePositionPattern = Pattern.compile("0+");
	protected static final String DEVICE_INFORMATION_PARAMETER_CD = "Initialization Device Information";
	protected static final String TERMINAL_INFORMATION_PARAMETER_CD = "Initialization Terminal Information";
	protected static final Map<String,String> deviceInformationToSetting = new HashMap<String, String>();
	protected static final Map<String,String> bezelInformationToSetting = new HashMap<String, String>();
	protected static final Map<String, String> componentInformationToSetting = new HashMap<String, String>();
	protected static final Set<Integer> replenishItemIds = new HashSet<Integer>(Arrays.asList(new Integer[] { 550, 551, 552, 553 }));
	static {
		deviceInformationToSetting.put("FW", "Firmware Version");
		deviceInformationToSetting.put("BL", "Bootloader Rev");
		deviceInformationToSetting.put("DIAG", "Diagnostic App Rev");
		deviceInformationToSetting.put("PTEST", "PTest Rev");
		deviceInformationToSetting.put("CY", "Default Currency");
		bezelInformationToSetting.put("AV", "Application Version");
		bezelInformationToSetting.put("BV", "Boot Version");
		bezelInformationToSetting.put("HV", "Hardware Version");
		componentInformationToSetting.put("AV", "Application Version");
		componentInformationToSetting.put("HV", "Hardware Version");
		componentInformationToSetting.put("BV", "Boot Version");
		componentInformationToSetting.put("DV", "Diagnostic Version");
		componentInformationToSetting.put("TV", "Test Version");
		componentInformationToSetting.put("CGMR", "Firmware Version");
		componentInformationToSetting.put("MD", "Mode");//Jira G9D-186

		try {
			random = SecureRandom.getInstance(ProcessingConstants.RANDOM_TYPE);
		} catch(Exception e) {
			randomError = e.getMessage();
		}

		try {
			hash = MessageDigest.getInstance(ProcessingConstants.HASH_TYPE);
		} catch(Exception e) {
			hashError = e.getMessage();
		}
	}

	public static interface AppLayerMessage extends Message, DeviceInfoUpdateListener {
		public void sendReplyTemplate(MessageData replyData) throws ServiceException ;
	    public int complete() throws ServiceException;
	}

	public static AppLayerOnlineMessage constructMessage(final MessageChainTaskInfo taskInfo, final ByteBuffer replyBuffer, final DeviceInfoManager deviceInfoManager, final TranslatorFactory translatorFactory,
			final String translatorContext, final ResourceFolder resourceFolder, final String outboundFileTransferDirectory) throws ServiceException {
		try {
			return new AppLayerOnlineMessage(taskInfo, deviceInfoManager, replyBuffer, translatorFactory, translatorContext, resourceFolder, outboundFileTransferDirectory);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Message attributes are faulty", e, WorkRetryType.NO_RETRY);
		}
	}

	public static OfflineMessage constructOfflineMessage(final MessageChainTaskInfo taskInfo) throws ServiceException {
		try {
			return new AppLayerOfflineMessage(taskInfo);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Message attributes are faulty", e, WorkRetryType.NO_RETRY);
		}
	}
	/*
	public static void broadcastDeviceInfo(String infoUpdateQueueKey, String deviceName, int deviceType, boolean initOnly, byte[] encryptionKey, byte[] previousEncryptionKey, Long messageId, Long masterId, Long rejectUntil, String deviceCharset, String locale, ActivationStatus activationStatus, String timeZoneGuid, boolean override, Publisher<ByteInput> publisher) throws ServiceException {
    	MessageChain mc = new MessageChainV11();
    	MessageChainStep broadcast = mc.addStep(infoUpdateQueueKey);
		broadcast.addStringAttribute("deviceName", deviceName);
		if(encryptionKey != null)
			broadcast.addByteArrayAttribute("encryptionKey", encryptionKey);
		if(previousEncryptionKey != null)
			broadcast.addByteArrayAttribute("previousEncryptionKey", previousEncryptionKey);
		if(messageId != null)
			broadcast.addLongAttribute("messageId", messageId);
		if(rejectUntil != null)
			broadcast.addLongAttribute("rejectUntil", rejectUntil);
		if(deviceCharset != null)
			broadcast.addStringAttribute("deviceCharset", deviceCharset);
		if(locale != null)
			broadcast.addStringAttribute("locale", locale);
		if(override)
			broadcast.addBooleanAttribute("override", override);
		broadcast.addBooleanAttribute("initOnly", initOnly);
		broadcast.addIntAttribute("deviceType", deviceType);
		if(masterId != null)
			broadcast.addLongAttribute("masterId", masterId);
		if(activationStatus != null)
			broadcast.addByteAttribute("activationStatus", activationStatus.getValue());
		if(timeZoneGuid != null)
			broadcast.addStringAttribute("timeZoneGuid", timeZoneGuid);

		MessageChainService.publish(mc, publisher, true);
    }
*/
	public static int upsertDeviceSetting(Connection conn, Map<String, Object> params, long deviceId, String deviceSettingParameterCd, String deviceSettingValue) throws SQLException, DataLayerException, ConvertException {
		params.clear();
		params.put("deviceId", deviceId);
		params.put("deviceSettingParameterCd", deviceSettingParameterCd);
		params.put("deviceSettingValue", deviceSettingValue);
		Object[] ret = DataLayerMgr.executeCall(conn, "UPSERT_DEVICE_SETTING", params);
		return ConvertUtils.convert(Integer.class, ret[1]);
	}

	public static void upsertHostSetting(Connection conn, Map<String, Object> params, long hostId, String hostSettingParameter, String hostSettingValue) throws SQLException, DataLayerException {
		params.clear();
		params.put("hostId", hostId);
		params.put("hostSettingParameter", hostSettingParameter);
		params.put("hostSettingValue", hostSettingValue);
		DataLayerMgr.executeCall(conn, "UPSERT_HOST_SETTING", params);
	}

	public static long upsertHost(Connection conn, Map<String, Object> params, long deviceId, int hostPort, int hostPosition, int hostType, String hostSerialCd, String hostManufacturer, String hostModel, String labelCd, long localTime, String componentInformation) throws SQLException, ConvertException, DataLayerException {
		params.clear();
		params.put("deviceId", deviceId);
		params.put("hostPortNum", hostPort);
	    params.put("hostPositionNum", hostPosition);
	    params.put("hostTypeId", hostType);
	    params.put("hostSerialCd", hostSerialCd);
	    params.put("hostLabelCd", labelCd);
	    params.put("hostEquipmentMfgr", hostManufacturer);
	    params.put("hostEquipmentModel", hostModel);
		params.put("localTime", localTime);
		params.put("componentInfo", componentInformation);
	    DataLayerMgr.executeCall(conn, "UPSERT_HOST", params);
	    long hostId = ConvertUtils.getLong(params.get("hostId"));
	    return hostId;
	}
	
	public static long getOrCreateEvent(Log log, Connection conn, Map<String, Object> params, String globalEventCdPrefix, String globalSessionCd, String deviceName, int componentNumber,
			int eventTypeId, int eventTypeHostTypeNumber, EventState eventStateId, String eventDeviceTranCd, BooleanType eventEndFlag, BooleanType eventUploadCompleteFlag, long eventStartTsMs,
			long eventEndTsMs) throws Exception {
		// eventStartTsMs and eventEndTsMs are in device local time zone
		long currentUtcTsMs = System.currentTimeMillis();
		long currentTsMs = ConvertUtils.getLocalTime(currentUtcTsMs, TimeZone.getDefault());
		
		if(currentUtcTsMs - validDeviceTimeDays*24*60*60*1000L > eventStartTsMs + 12*60*60*1000L || currentUtcTsMs < eventStartTsMs - 14*60*60*1000L){
			log.warn("Received invalid event start timestamp: " + eventStartTsMs + ", using server local time: " + currentTsMs);
			eventStartTsMs = currentTsMs;
		}
		
		if(currentUtcTsMs - validDeviceTimeDays*24*60*60*1000L > eventEndTsMs + 12*60*60*1000L || currentUtcTsMs < eventEndTsMs - 14*60*60*1000L){
			log.warn("Received invalid event end timestamp: " + eventEndTsMs + ", using server local time: " + currentTsMs);
			eventEndTsMs = currentTsMs;
		}
		
		if(eventEndTsMs < eventStartTsMs)
			eventEndTsMs = eventStartTsMs;
		
		params.clear();
		params.put("globalSessionCd", globalSessionCd);
		params.put("deviceName", deviceName);
		params.put("globalEventCdPrefix", globalEventCdPrefix);
		params.put("componentNumber", componentNumber);
		params.put("eventTypeId", eventTypeId);
		params.put("eventTypeHostTypeNum", eventTypeHostTypeNumber);
		params.put("eventStateId", eventStateId);
		params.put("eventDeviceTranCd", eventDeviceTranCd);
		params.put("eventEndFlag", eventEndFlag.getValue());
		params.put("eventUploadCompleteFlag", eventUploadCompleteFlag.getValue());
		params.put("eventStartTsMs", eventStartTsMs);
		params.put("eventEndTsMs", eventEndTsMs);

		Object[] ret = DataLayerMgr.executeCall(conn, "GET_OR_CREATE_EVENT", params);
		ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
		String errorMessage = ConvertUtils.convert(String.class, ret[2]);

		if(resultCd == ResultCode.SUCCESS) {
			long eventId = ConvertUtils.convert(Long.class, ret[3]);
			BooleanType exists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[4]));
			log.info((exists == BooleanType.TRUE ? "Existing" : "New") + " eventId: " + eventId + " for eventDeviceTranCd: " + eventDeviceTranCd);
			return eventId;
		}
		throw new ServiceException(errorMessage);
	}

	public static long getOrCreateEventDetail(Log log, Connection conn, Map<String, Object> params, long eventId, EventDetailType eventDetailType, String eventDetailValue, long eventDetailValueTsMs)
			throws Exception {
		params.clear();
		params.put("eventId", eventId);
		params.put("eventDetailTypeId", eventDetailType.getValue());
		params.put("eventDetailValue", eventDetailValue);
		params.put("eventDetailValueTsMs", eventDetailValueTsMs > -1 ? eventDetailValueTsMs : null);

		Object[] ret = DataLayerMgr.executeCall(conn, "GET_OR_CREATE_EVENT_DETAIL", params);
		ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
		String errorMessage = ConvertUtils.convert(String.class, ret[2]);

		if(resultCd == ResultCode.SUCCESS) {
			long eventDetailId = ConvertUtils.convert(Long.class, ret[3]);
			BooleanType exists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[4]));
			log.info((exists == BooleanType.TRUE ? "Existing" : "New") + " eventDetailId: " + eventDetailId + " for eventId: " + eventId + ", eventDetailType: " + eventDetailType.getValue());
			return eventDetailId;
		}
		throw new ServiceException(errorMessage);
	}

	public static boolean getOrCreateSettlementEvent(Log log, Connection conn, Map<String, Object> params, long eventId, long deviceBatchId, long deviceNewBatchId, long deviceEventUtcTsMs,
			int deviceEventUtcOffsetMin) throws Exception {
		params.clear();
		params.put("eventId", eventId);
		params.put("deviceBatchId", deviceBatchId);
		params.put("deviceNewBatchId", deviceNewBatchId);
		params.put("deviceEventUtcTsMs", deviceEventUtcTsMs);
		params.put("deviceEventUtcOffsetMin", deviceEventUtcOffsetMin);

		Object[] ret = DataLayerMgr.executeCall(conn, "GET_OR_CREATE_SETTLEMENT_EVENT", params);
		ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
		String errorMessage = ConvertUtils.convert(String.class, ret[2]);

		if(resultCd == ResultCode.SUCCESS) {
			BooleanType exists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[3]));
			log.info((exists == BooleanType.TRUE ? "Existing" : "New") + " settlement for eventId: " + eventId);
			return exists == BooleanType.TRUE;
		}
		throw new ServiceException(errorMessage);
	}

	public static long getOrCreateHostCounter(Log log, Connection conn, Map<String, Object> params, long eventId, int componentNumber, String hostCounterParameter, String hostCounterValue) throws Exception {
		params.clear();
		params.put("componentNumber", componentNumber);
		params.put("eventId", eventId);
		params.put("hostCounterParameter", hostCounterParameter);
		params.put("hostCounterValue", hostCounterValue);

		Object[] ret = DataLayerMgr.executeCall(conn, "GET_OR_CREATE_HOST_COUNTER", params);
		ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
		String errorMessage = ConvertUtils.convert(String.class, ret[2]);

		if(resultCd == ResultCode.SUCCESS) {
			long hostCounterId = ConvertUtils.convert(Long.class, ret[3]);
			BooleanType exists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[4]));
			log.info((exists == BooleanType.TRUE ? "Existing" : "New") + " hostCounterId: " + hostCounterId + " for eventId: " + eventId + ", hostCounterParameter: " + hostCounterParameter);
			return hostCounterId;
		}
		throw new ServiceException(errorMessage);
	}

	public static Boolean getDexCreatedDuringFill(Log log, Connection conn, Map<String, Object> params, long eventId, FillEventAuthType authType) throws ConvertException, SQLException, DataLayerException {
		params.clear();
		params.put("eventId", eventId);
		DataLayerMgr.executeCall(conn, "GET_DEX_CREATED_DURING_" + authType + "_FILL", params);
		return ConvertUtils.convert(Boolean.class, params.get("dexCreated"));
	}

	public static Boolean getDEXNeededForAuth(Log log, Connection conn, Map<String, Object> params, long authId) throws ConvertException, SQLException, DataLayerException {
		params.clear();
		params.put("authId", authId);
		DataLayerMgr.executeCall(conn, "GET_DEX_NEEDED_AUTH", params);
		return ConvertUtils.convert(Boolean.class, params.get("createDEX"));
	}

	public static byte[] getLineItemBytes(List<? extends LineItem> lineItems){
		ByteBuffer lineItemBuffer=ByteBuffer.allocate(2048);
		for(LineItem lineItem: lineItems){
				lineItem.writeData(lineItemBuffer, true);
		}
		lineItemBuffer.flip();
		byte[] data=new byte[lineItemBuffer.remaining()];
		lineItemBuffer.get(data);
		return data;
	}

	protected static boolean ignoreTimeZone(TimeZone timeZone, OfflineMessage message) {
		// Ignore the specified timezone offset for Sales and use the configured device's timezone when offset = 0 and device is a K3 device with VSRevolution AppType
		MessageData data;
		String serialNumber;
		return timeZone.getRawOffset() == 0 && !timeZone.useDaylightTime() && message.getDeviceType() == DeviceType.KIOSK 
				&& (data=message.getData()) instanceof WSMessageData 
				&& (serialNumber = ((WSMessageData) data).getSerialNumber()) != null
				&& serialNumber.startsWith("K3VS");
	}
	
	public static long processSale(Sale sale, OfflineMessage message, Publisher<ByteInput> publisher, boolean voidAllowed) throws ServiceException {
		boolean saleHasTimeZone = true;
		TimeZone timeZone = sale.getSaleTimeZone();
		if(timeZone == null) {
			saleHasTimeZone = false;
			timeZone = TimeZone.getTimeZone(message.getTimeZoneGuid());
		} else if(ignoreTimeZone(timeZone, message))
			timeZone = TimeZone.getTimeZone(message.getTimeZoneGuid());
		long saleTimeUTC, saleTimeLocal;
		if(sale.getSaleStartTime() == null) {
			saleTimeUTC = message.getServerTime(); //server time is UTC
			saleTimeLocal = ConvertUtils.getLocalTime(saleTimeUTC, timeZone);
		}
		else if(!saleHasTimeZone) { // saleStartTime is LOCAL time in this case
			saleTimeLocal = sale.getSaleStartTime();
			saleTimeUTC = ConvertUtils.getMillisUTC(saleTimeLocal, timeZone);
			}
		else { // saleStartTime is UTC time in this case
			saleTimeUTC = sale.getSaleStartTime();
			saleTimeLocal = ConvertUtils.getLocalTime(saleTimeUTC, timeZone);
		}
		long messageStartTimeMs=message.getServerTime();
		if(messageStartTimeMs - validDeviceTimeDays*24*60*60*1000L > saleTimeUTC || messageStartTimeMs < saleTimeUTC){
			log.warn("Received invalid sale timestamp: "+saleTimeUTC+", using server message start time: "+messageStartTimeMs);
			saleTimeUTC=messageStartTimeMs;
			saleTimeLocal=ConvertUtils.getLocalTime(saleTimeUTC, timeZone);
		}		
		int saleTimeOffsetMin = (int)(saleTimeLocal - saleTimeUTC) / (60*1000);
		List<? extends LineItem> lineItems = sale.getLineItems();
		char tranBatchTypeCd = (sale.getSaleType() == SaleType.INTENDED ? TranBatchType.INTENDED : TranBatchType.ACTUAL).getValue();
		SaleResult saleResult = sale.getSaleResult();
		TranDeviceResultType transactionResult = sale.getTransactionResult();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("globalSessionCode", message.getGlobalSessionCode());
		params.put("deviceName", message.getDeviceName());
		params.put("globalEventCdPrefix", EventCodePrefix.APP_LAYER.getValue());
		params.put("deviceBatchId", sale.getBatchId());
		params.put("saleUtcTsMs", saleTimeUTC);
		params.put("saleUtcOffsetMin", saleTimeOffsetMin);
		params.put("tranDeviceResultTypeCd", transactionResult.getValue());
		params.put("receiptResultCd", sale.getReceiptResult().getValue());
		params.put("tranBatchTypeCd", tranBatchTypeCd);
		params.put("hashTypeCd", ProcessingConstants.HASH_TYPE);
		params.put("saleTax", sale.getSaleTax());

		int lineItemOffsetSec = 0;
		Map<String, Object> tliParams = new HashMap<String, Object>();
		BigDecimal partialCashAmount = null;
		Log log = message.getLog();
		Connection conn = AppLayerUtils.getConnection(true);
		try {
			BatchUpdate batchUpdate = DataLayerMgr.createBatchUpdate("CREATE_TRAN_LINE_ITEM", false, 0);
			if(sale.getSaleType() == SaleType.ACTUAL) {
				int partialCashCnt = 0;
				for(LineItem lineItem : lineItems) {
					if(lineItem.getItem() == ProcessingConstants.PARTIAL_CASH_ITEM_TYPE && lineItem.getPrice() != null) {
						try {
							BigDecimal cashAmount = ConvertUtils.convert(BigDecimal.class, lineItem.getPrice()).multiply(BigDecimal.valueOf(lineItem.getQuantity()));
							if(partialCashAmount == null)
								partialCashAmount = cashAmount;
							else
								partialCashAmount = partialCashAmount.add(cashAmount);
							params.put("saleTypeCd", SaleType.CASH.getValue());
							params.put("deviceTranCd", sale.getDeviceTranCd() + ':' + ++partialCashCnt);
							params.put("saleResultId", SaleResult.SUCCESS.getValue());
							params.put("saleAmount", cashAmount);
							params.put("voidAllowed", 'N');
							params.put("tranLineItemHash", getDataHashHex(getLineItemBytes(Collections.singletonList(lineItem))));
							Object[] ret = DataLayerMgr.executeCall(conn, "CREATE_SALE", params);
							ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, params.get("resultCd")));
							String errorMessage = ConvertUtils.convert(String.class, params.get("errorMessage"));
							if(errorMessage != null)
								log.warn(errorMessage);
							switch(resultCd) {
								case SUCCESS:
									long tranId = ConvertUtils.getLong(params.get("tranId"));
									tliParams.put("tranId", tranId);
									tliParams.put("tranBatchTypeCd", tranBatchTypeCd);
									tliParams.put("saleAmount", cashAmount);
									addLineItemToBatch(lineItem, tliParams, saleTimeUTC, lineItemOffsetSec, saleTimeOffsetMin, batchUpdate);
									batchUpdate.executeBatch(conn);
									lineItemOffsetSec += lineItem.getDuration();
									params.put("saleDurationSec", lineItemOffsetSec);
									ret = DataLayerMgr.executeCall(conn, "FINALIZE_SALE", params);
									resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
									errorMessage = ConvertUtils.convert(String.class, ret[2]);
									if(errorMessage != null)
										log.warn(errorMessage);
									InteractionUtils.publishTranImport(tranId, publisher, log);
									break;
								case ILLEGAL_STATE:
									throw new RetrySpecifiedServiceException(errorMessage, WorkRetryType.NONBLOCKING_RETRY);
								case DUPLICATE:
									break;
								default:
									throw new ServiceException(errorMessage);
							}

						} catch(ConvertException e) {
							throw new RetrySpecifiedServiceException(e, WorkRetryType.NONBLOCKING_RETRY);
						}
					} else if(replenishItemIds.contains(lineItem.getItem())) {
						saleResult = SaleResult.CANCELLED_MACHINE_FAILURE;
						transactionResult = TranDeviceResultType.CANCELLED;
						break;
					}
				}
			}
			Number saleAmount;
			switch(saleResult) {
				case SUCCESS:
					saleAmount = sale.getSaleAmount();
					if(partialCashAmount != null)
						try {
							saleAmount = ConvertUtils.convert(BigDecimal.class, saleAmount).subtract(partialCashAmount);
						} catch(ConvertException e) {
							throw new RetrySpecifiedServiceException(e, WorkRetryType.NONBLOCKING_RETRY);
						}
					break;
				default:
					saleAmount = BigDecimal.ZERO;
			}

			params.put("saleTypeCd", sale.getSaleType().getValue());
			params.put("deviceTranCd", sale.getDeviceTranCd());
			params.put("saleResultId", saleResult.getValue());
			params.put("saleAmount", saleAmount);
			params.put("voidAllowed", voidAllowed ? 'Y' : 'N');
			params.put("tranLineItemHash", getDataHashHex(getLineItemBytes(lineItems)));

			Object[] ret = DataLayerMgr.executeCall(conn, "CREATE_SALE", params);
			ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, params.get("resultCd")));
			String errorMessage = ConvertUtils.convert(String.class, params.get("errorMessage"));
			if (errorMessage != null)
				log.warn(errorMessage);

			long tranId = 0;
			boolean checkTerminal;
			boolean tranImportNeeded;
			char sessionUpdateNeeded = 'N';
			switch(resultCd) {
				case SUCCESS:
					tranId = ConvertUtils.getLong(params.get("tranId"));
					char tranStateCd = ConvertUtils.convert(Character.class, params.get("tranStateCd"));
					log.info("Created sale for tranId: " + tranId);
					tliParams.put("tranId", tranId);
					tliParams.put("tranBatchTypeCd", tranBatchTypeCd);
					tliParams.put("saleAmount", saleAmount);
					for(LineItem lineItem : lineItems) {
						if(lineItem.getItem() == ProcessingConstants.PARTIAL_CASH_ITEM_TYPE)
							continue;
						lineItemOffsetSec += lineItem.getDuration();
						if(message.getDeviceType() == DeviceType.MEI && lineItem.getPrice() != null && lineItem.getPrice().intValue() == 0 && lineItem.getItem() == 200 && (lineItem.getPosition() == null || meiIgnorePositionPattern.matcher(lineItem.getPosition()).matches())) {
							/*See Bugzilla #497*/
						} else {
							addLineItemToBatch(lineItem, tliParams, saleTimeUTC, lineItemOffsetSec, saleTimeOffsetMin, batchUpdate);
						}
					}
					if(sale.getSaleTax() != null && sale.getSaleTax().intValue() > 0) {
						tliParams.put("hostPortNum", 0);
						tliParams.put("tliTypeId", 611);
						tliParams.put("tliQuantity", 1);
						tliParams.put("tliAmount", sale.getSaleTax());
						tliParams.put("tliTax", BigDecimal.ZERO);
						tliParams.put("tliDesc", "Sale Tax");
						tliParams.put("tliUtcTsMs", saleTimeUTC + (lineItemOffsetSec * 1000));
						tliParams.put("tliUtcOffsetMin", saleTimeOffsetMin);
						tliParams.put("tliPositionCd", null);
						tliParams.put("tliSaleResultId", SaleResult.SUCCESS.getValue());
						tliParams.put("hostPositionNum", 0);
						batchUpdate.addBatch(tliParams);
					}
					if(batchUpdate.getPendingCount() > 0)
						batchUpdate.executeBatch(conn);

					params.put("saleDurationSec", lineItemOffsetSec);

					ret = DataLayerMgr.executeCall(conn, "FINALIZE_SALE", params);
					resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
					errorMessage = ConvertUtils.convert(String.class, ret[2]);
					if(errorMessage != null)
						log.warn(errorMessage);
					switch(tranStateCd) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
							checkTerminal = false;
							tranImportNeeded = false;
							break;
						case 'F':
						case 'G':
						case 'H':
						case 'L':
						case 'M':
						case 'Z':
						case 'K':
						case 'U':
							checkTerminal = false;
							tranImportNeeded = false;
							break;
						case '9':
							checkTerminal = true;
							tranImportNeeded = false;
							break;
						case '8':
							checkTerminal = true;
							tranImportNeeded = true;
							break;
						default: // A, B, C, D, E, I, J, N, P, Q, R, S, T, V, W
							checkTerminal = false;
							tranImportNeeded = true;
					}
					try {
						sessionUpdateNeeded = ConvertUtils.convertRequired(Character.class, params.get("sessionUpdateNeeded"));
					} catch(ConvertException e) {
						throw new ServiceException("Could not convert sessionUpdateNeeded to a char", e);
					}

					if(resultCd == ResultCode.SUCCESS)
						log.info("Finalized sale for tranId: " + tranId);
					else
						throw new ServiceException(errorMessage);
					if(sale.getSaleType() != SaleType.INTENDED && message.getDeviceType() == DeviceType.ESUDS) {
						LegacyUtils.processESudsSaleCommon(message.getLog(), publisher, tranId, conn);
					}
					break;
				case ILLEGAL_STATE:
					throw new RetrySpecifiedServiceException(errorMessage, WorkRetryType.NONBLOCKING_RETRY);
				case DUPLICATE:
					tranId = ConvertUtils.getLong(params.get("tranId"));
					checkTerminal = false;
					tranImportNeeded = true; // make sure it was imported
					break;
				case SALE_VOIDED:
					tranId = ConvertUtils.getLong(params.get("tranId"));
					checkTerminal = false;
					tranImportNeeded = true; // make sure it was imported
					break;
				default:
					throw new ServiceException(errorMessage);
			}
			conn.commit();
			if(tranImportNeeded && tranId > 0) {
				InteractionUtils.publishTranImport(tranId, publisher, log);
				InteractionUtils.checkForReplenishBonuses(tranId, conn, publisher, log);
			}
			if(checkTerminal)
				InteractionUtils.publishCheckTerminal(tranId, conn, publisher, log);
			if (sessionUpdateNeeded == 'Y') {
				params.put("saleGlobalSessionCode", params.get("globalSessionCode"));
				params.put("saleSessionStartTime", message.getSessionStartTime());
				InteractionUtils.publishSessionControl(SessionControlAction.ADD_TRAN_TO_DEVICE_SESSION.toString(), params, publisher, log);
			}
			InteractionUtils.doRiskChecks(publisher, message.getDeviceName(), log);
			return tranId;
        } catch(SQLException e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
            throw new RetrySpecifiedServiceException(e, WorkRetryType.NONBLOCKING_RETRY);
		} catch(InvalidByteValueException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
		    throw new RetrySpecifiedServiceException(e, WorkRetryType.NONBLOCKING_RETRY);
		} catch(ConvertException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
		    throw new RetrySpecifiedServiceException(e, WorkRetryType.NONBLOCKING_RETRY);
		} catch(Exception e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
		    throw new RetrySpecifiedServiceException(e, WorkRetryType.NONBLOCKING_RETRY);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
        }
	}

	protected static void addLineItemToBatch(LineItem lineItem, Map<String, Object> tliParams, long saleTimeUTC, int lineItemOffsetSec, int saleTimeOffsetMin, BatchUpdate batchUpdate) throws ParameterException {
		addLineItemToBatch(lineItem, lineItem.getItem(), lineItem.getPrice(), tliParams, saleTimeUTC, lineItemOffsetSec, saleTimeOffsetMin, batchUpdate);
	}

	protected static void addLineItemToBatch(LineItem lineItem, int itemType, Number price, Map<String, Object> tliParams, long saleTimeUTC, int lineItemOffsetSec, int saleTimeOffsetMin, BatchUpdate batchUpdate) throws ParameterException {
		tliParams.put("hostPortNum", lineItem.getComponentNumber());
		tliParams.put("tliTypeId", itemType);
		tliParams.put("tliQuantity", lineItem.getQuantity());
		tliParams.put("tliAmount", price);
		tliParams.put("tliTax", BigDecimal.ZERO);
		tliParams.put("tliDesc", lineItem.getDescription());
		tliParams.put("tliUtcTsMs", saleTimeUTC + (lineItemOffsetSec * 1000));
		tliParams.put("tliUtcOffsetMin", saleTimeOffsetMin);
		tliParams.put("tliPositionCd", lineItem.getPosition());
		tliParams.put("tliSaleResultId", (replenishItemIds.contains(lineItem.getItem()) ? SaleResult.CANCELLED_MACHINE_FAILURE : lineItem.getSaleResult()).getValue());
		tliParams.put("hostPositionNum", lineItem.getComponentPosition());
		batchUpdate.addBatch(tliParams);
	}
	public static TimeInfo calculateTimeInfo(long localTime, String deviceName) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		params.put("localTime", localTime);
		Results results = DataLayerMgr.executeQuery("GET_TIMEZONE_FOR_DEVICE", params);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(localTime);
		while(results.next()) {
			long effectiveTs = results.getValue("effectiveTs", Long.class);
			String timeZoneGuid = results.getValue("timeZoneGuid", String.class);
			if(timeZoneGuid != null && (timeZoneGuid = timeZoneGuid.trim()).length() > 0) {
				TimeZone tz = TimeZone.getTimeZone(timeZoneGuid);
				cal.setTimeZone(tz);
				long time = ConvertUtils.getMillisUTC(cal);
				if(time >= effectiveTs)
					return new TimeInfo(time, tz.getOffset(time), tz);
			}
		}
		log.info("Could not determine TimeZone for device " + deviceName + "; using server time zone");
		TimeZone tz = cal.getTimeZone();
		long time = ConvertUtils.getMillisUTC(cal);
		return new TimeInfo(time, tz.getOffset(time), tz);
	}

	public static class TimeInfo {
		protected final long timeUTC;
		protected final long offset;
		protected final TimeZone timeZone;

		public TimeInfo(long timeUTC, long offset, TimeZone timeZone) {
			this.timeUTC = timeUTC;
			this.offset = offset;
			this.timeZone = timeZone;
		}

		public long getTimeUTC() {
			return timeUTC;
		}

		public long getOffset() {
			return offset;
		}

		public TimeZone getTimeZone() {
			return timeZone;
		}
	}

    public static byte[] generateEncryptionKey(Log log, int keyLength) throws ServiceException {
		if(random == null)
			throw new ServiceException("Failed to get instance of SecureRandom " + ProcessingConstants.RANDOM_TYPE + ": " + randomError);

		byte[] key = new byte[keyLength];
		random.nextBytes(key);
		log.info("Generated new encryption key");
		return key;
	}

    public static byte[] generateEncryptionKey(Log log) throws ServiceException {
    	return generateEncryptionKey(log, ProcessingConstants.EPORT_ENCRYPTION_KEY_LENGTH);
    }

	public static String getDataHashHex(byte[] data) throws ServiceException
    {
    	if (hash == null)
            throw new ServiceException("Failed to get instance of MessageDigest " + ProcessingConstants.HASH_TYPE + ": " + hashError);

    	byte[] hashData;
    	synchronized (hash)
    	{
    		hashData = hash.digest(data);
    	}
    	return StringUtils.toHex(hashData);
    }

	public static ServerActionCode determineActionCode(Message message) {
		try {
			return determineActionCode(message.getDeviceInfo(), message.getLog());
		} catch(ServiceException e) {
			log.warn("Could not get Device Info", e);
			return ServerActionCode.NO_ACTION; // We could send DISCONNECT, but let's be as nice as possible for now
		}
	}
	public static ServerActionCode determineActionCode(DeviceInfo deviceInfo, Log log) {
		switch(deviceInfo.getActivationStatus()) {
			case FACTORY:
			case NOT_ACTIVATED:
				return ServerActionCode.NO_ACTION;
			case READY_TO_ACTIVATE:
				if(log.isInfoEnabled())
					log.info("Device is ready to be activated; Requesting activation from device");
				return ServerActionCode.ACTIVATE;
			case ACTIVATED:
				//look for pending updates
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("deviceName", deviceInfo.getDeviceName());
				int count;
				try {
					DataLayerMgr.selectInto("GET_COMMAND_PENDING_COUNT", params);
					count = ConvertUtils.getInt(params.get("count"));
				} catch(ConvertException e) {
					log.warn("Could not convert pending command count", e);
					return ServerActionCode.NO_ACTION;
				} catch(SQLException e) {
					log.warn("Could not get pending command count", e);
					return ServerActionCode.NO_ACTION;
				} catch(DataLayerException e) {
					log.warn("Could not get pending command count", e);
					return ServerActionCode.NO_ACTION;
				} catch(BeanException e) {
					log.warn("Could not get pending command count", e);
					return ServerActionCode.NO_ACTION;
				}
				if(count > 0) {
					if(log.isInfoEnabled())
						log.info("Found " + count + " pending commands for this device; Requesting USR from device");
					return ServerActionCode.PERFORM_CALL_IN;
				}
				if(log.isInfoEnabled())
					log.info("Found no pending commands for this device; Sending No Action Response");
				return ServerActionCode.NO_ACTION;
			default:
				log.warn("Invalid activationStatus " + deviceInfo.getActivationStatus() + " for device " + deviceInfo.getDeviceName());
				return ServerActionCode.NO_ACTION;
		}
	}

	public static void processUpdateStatusRequest(Message message, int maxExecuteOrder) throws BufferOverflowException, ServiceException{
		String text;
		Long commandId;
		DeviceInfo deviceInfo;
		try {
			deviceInfo = message.getDeviceInfo();
		} catch(ServiceException e) {
			log.warn("Could not get Device Info", e);
			text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
			return;
		}
		boolean allPropertiesReceived = ConvertUtils.getBooleanSafely(message.getSessionAttribute("all-properties-received"), false);
		if(!allPropertiesReceived) {
			Set<Integer> propertyIndexes = getPropertyIndexesToRequest(deviceInfo);
			if(propertyIndexes == null || propertyIndexes.isEmpty() || (Integer) propertyIndexes.toArray()[0] == 99999) {
				message.setSessionAttribute("all-properties-received", true);
				if(message.getLog().isInfoEnabled())
					message.getLog().info("No properties are being requested");
			} else {
				int[] propertyIndexesReceived = ConvertUtils.convertSafely(int[].class, message.getSessionAttribute("property-indexes-received"), null);
				if(propertyIndexesReceived != null && propertyIndexesReceived.length > 0) {
					propertyIndexes = new HashSet<Integer>(propertyIndexes);
					for(int index : propertyIndexesReceived)
						propertyIndexes.remove(index);
				}
				if(propertyIndexes.isEmpty()) {
					message.setSessionAttribute("all-properties-received", true);
					if(message.getLog().isInfoEnabled())
						message.getLog().info("All requested properties were received");
				} else {
					MessageData_CA replyCA = new MessageData_CA();
					replyCA.setRequestType(GenericRequestType.PROPERTY_LIST_REQUEST);
					((PropertyListRequest)replyCA.getRequestTypeData()).setPropertyIndexes(propertyIndexes);
					message.sendReply(replyCA);
					return;
				}
			}
		}

		Map<String,Object> params = new HashMap<String, Object>();
		long updateTime = 0;
		Connection conn = null;
		try {
			switch(deviceInfo.getActivationStatus()) {
				case ACTIVATED:
					try {
						conn = DataLayerMgr.getConnectionForCall("UPDATE_PENDING_COMMAND_NEXT");
						try {
							// Mark complete any pending "Send USR" commands
							params.put("deviceName", deviceInfo.getDeviceName());
							params.put("actionCode", GenericResponseServerActionCode.SEND_UPDATE_STATUS_REQUEST);
							DataLayerMgr.executeCall(conn, "UPDATE_CB_PENDING_COMMANDS_SUCCESS", params);
							conn.commit();
							params.put("actionCode", GenericResponseServerActionCode.SEND_UPDATE_STATUS_REQUEST_IMMEDIATE);
							DataLayerMgr.executeCall(conn, "UPDATE_CB_PENDING_COMMANDS_SUCCESS", params);
							conn.commit();
						} catch(SQLException e) {
							log.warn("Could not mark CB - USR commands as success", e);
						} catch(DataLayerException e) {
							log.warn("Could not mark CB - USR commands as success", e);
						}
						params.clear();
						params.put("deviceName", message.getDeviceName());
						Object triggerClearCommandId = message.getSessionAttribute("triggerClearCommandId");
						if(triggerClearCommandId != null)
							params.put("priorityCommandId", triggerClearCommandId);
						params.put("maxExecuteOrder", maxExecuteOrder);
						params.put("sessionAttributes", ConvertUtils.getStringSafely(message.getSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR), ""));
						params.put("globalSessionCode", message.getGlobalSessionCode());
						params.put("updateStatus", UpdateStatusType.NORMAL.getValue());
						params.put("v4Messages", 'Y');
						DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_NEXT", params);
						message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR,  ConvertUtils.getStringSafely(params.get("sessionAttributes"), ""));
						conn.commit();
						commandId = ConvertUtils.convertSafely(Long.class, params.get("commandId"), null);
						updateTime = ConvertUtils.getLongSafely(params.get("updateTime"), 0);
					} catch(SQLException e) {
						ProcessingUtils.rollbackDbConnection(log, conn);
						log.warn("Could not get pending command from database", e);
						commandId = null;
					} catch(DataLayerException e) {
						ProcessingUtils.rollbackDbConnection(log, conn);
						log.warn("Could not get pending command from database", e);
						commandId = null;
					}
					break;
				default: //TODO: should we allow some pending commands?
					commandId = null;
				break;
			}
			if(commandId != null) {
				try {
					Blob fileContent;
					byte[] fileBytes;
					String commandDataType = ConvertUtils.getString(params.get("dataType"), true);
					MessageType messageType = MessageType.getByValue(ByteArrayUtils.fromHex(commandDataType), 0);
					byte[] commandBytes = ConvertUtils.convert(byte[].class, params.get("commandBytes"));
					switch(messageType) {
						case FILE_XFER_START_4_1: // File Transfer Start
							long fileTransferId = ConvertUtils.getInt(params.get("fileTransferId"));
							String fileName = ConvertUtils.getString(params.get("fileName"), true);
							int fileTypeId = ConvertUtils.getInt(params.get("fileTypeId"));
							if (DialectResolver.isOracle())
						    	fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
						    else {
						    	fileBytes = ConvertUtils.convert(byte[].class, params.get("fileContent"));
						    	fileContent = new SerialBlob(fileBytes);
						    }
							Calendar fileCreateTime = ConvertUtils.convert(Calendar.class, params.get("fileCreateTime"));
							int filePacketSize = ConvertUtils.getInt(params.get("filePacketSize"), CommonProcessing.defaultFilePacketSize);
							int fileGroupNum = 0; /*b/c half-duplex is enforced, only one file transfer occurs at a time and there is not file Group Num*/
							// for 7C and A4 we must use fileGroupNum from params or convert the fileTransferId to an unsigned-byte
							//Use short file transfer if possible
							if(fileTypeId == FileType.PROPERTY_LIST.getValue())
								try {
									fileContent = adjustPropertyListToCurrentValues(deviceInfo, message.getData().getMessageId(), fileContent, conn);
								} catch(ParseException e) {
									log.warn("Property Value List could not be parsed; Canceling command " + commandId, e);
									DataLayerMgr.executeCall(conn, "UPDATE_PENDING_COMMAND_CANCEL", Collections.singletonMap("pendingCommandId", (Object) commandId));
									conn.commit();
									processUpdateStatusRequest(message, maxExecuteOrder);
									return;
								}

							if(fileContent.length() <= filePacketSize) {
								MessageProcessingUtils.writeShortFileTransfer(message, fileTransferId, fileName, fileTypeId, fileCreateTime, fileContent);
								if(message.getLog().isInfoEnabled())
									message.getLog().info("Sending short file transfer for command " + commandId);
							} else {
								MessageProcessingUtils.writeFileTransferStart(message, fileTransferId, commandId, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileContent, MessageType.FILE_XFER_START_4_1, null);
								if(message.getLog().isInfoEnabled())
									message.getLog().info("Sending file transfer start for command " + commandId);
							}
							break;
						case SHORT_FILE_XFER_4_1: // Short File Transfer
							fileTransferId = ConvertUtils.getInt(params.get("fileTransferId"));
							fileName = ConvertUtils.getString(params.get("fileName"), true);
							fileTypeId = ConvertUtils.getInt(params.get("fileTypeId"));
						    if (DialectResolver.isOracle())
							  fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
						    else {
							  fileBytes = ConvertUtils.convert(byte[].class, params.get("fileContent"));
							  fileContent = new SerialBlob(fileBytes);
						    }
							fileCreateTime = ConvertUtils.convert(Calendar.class, params.get("fileCreateTime"));
							if(fileTypeId == FileType.PROPERTY_LIST.getValue())
								try {
									fileContent = adjustPropertyListToCurrentValues(deviceInfo, message.getData().getMessageId(), fileContent, conn);
								} catch(ParseException e) {
									log.warn("Property Value List could not be parsed; Canceling command " + commandId, e);
									DataLayerMgr.executeCall(conn, "UPDATE_PENDING_COMMAND_CANCEL", Collections.singletonMap("pendingCommandId", (Object) commandId));
									conn.commit();
									processUpdateStatusRequest(message, maxExecuteOrder);
									return;
								}
							if(fileContent.length() <= CommonProcessing.defaultFilePacketSize) {
								MessageProcessingUtils.writeShortFileTransfer(message, fileTransferId, fileName, fileTypeId, fileCreateTime, fileContent);
								if(message.getLog().isInfoEnabled())
									message.getLog().info("Sending short file transfer for command " + commandId);
							} else {
								MessageProcessingUtils.writeFileTransferStart(message, fileTransferId, commandId, 0, fileName, fileTypeId, CommonProcessing.defaultFilePacketSize, fileCreateTime, fileContent, MessageType.FILE_XFER_START_4_1, null);
								if(message.getLog().isInfoEnabled())
									message.getLog().info("Sending file transfer start for command " + commandId);
							}
							break;
						case GENERIC_RESPONSE_4_1:
							MessageData_CB dataCB = (MessageData_CB) MessageResponseUtils.getMessageFromBytes(message, messageType, commandBytes);
							if(dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
								adjustPropertyListToCurrentValues(deviceInfo, message.getData().getMessageId(), ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues(), conn);
							}
							message.sendReply(dataCB);
							if(message.getLog().isInfoEnabled())
								message.getLog().info("Sending " + messageType + " for command " + commandId);
							break;
						default:
							//send message
							MessageProcessingUtils.writeMessageFromBytes(message, messageType, commandBytes);
							if(message.getLog().isInfoEnabled())
								message.getLog().info("Sending " + messageType + " for command " + commandId);
					}
					message.setNextCommandId(commandId);
					deviceInfo.setActionCode(ServerActionCode.PERFORM_CALL_IN); // In case device doesn't acknowledge this
					try {
						deviceInfo.commitChanges(updateTime);
					} catch(ServiceException e) {
						log.warn("Could not commit Device Info changes for device '" + deviceInfo.getDeviceName() + "'", e);
					}
				} catch(ConvertException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(IllegalArgumentException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(InvalidByteValueException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(InvalidByteArrayException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(SQLException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(IOException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(ServiceException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				} catch(DataLayerException e) {
					log.warn("Error while trying to process generic request - update status request", e);
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				}
			} else {
				deviceInfo.setActionCode(ServerActionCode.NO_ACTION);
				try {
					deviceInfo.commitChanges(updateTime);
				} catch(ServiceException e) {
					log.warn("Could not commit Device Info changes for device '" + deviceInfo.getDeviceName() + "'", e);
				}
				text = message.getTranslator().translate("client.message.no-updates-pending", "No Updates Pending");
				MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
				if(message.getLog().isInfoEnabled())
					message.getLog().info("No pending commands found; Sending generic response with no action");
			}
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	protected static void adjustPropertyListToCurrentValues(DeviceInfo deviceInfo, Long messageId, Map<Integer, String> propertyValueList, Connection conn) throws SQLException, DataLayerException, ConvertException {
		populatePropertyValueList(deviceInfo, messageId, propertyValueList.keySet(), propertyValueList, true, conn);
	}

	protected static Blob adjustPropertyListToCurrentValues(DeviceInfo deviceInfo, Long messageId, Blob fileContent, Connection conn) throws IOException, ParseException, SQLException, DataLayerException, ConvertException {
		final Set<Integer> propertyList = new HashSet<Integer>();
		ProcessingUtils.parsePropertyList(fileContent.getBinaryStream(), deviceInfo.getDeviceCharset(), new PropertyValueHandler<RuntimeException>() {
			public void handleProperty(int propertyIndex, String propertyValue) throws RuntimeException {
				propertyList.add(propertyIndex);
			}
		});
		// get current values
		Map<Integer, String> propertyValueList = new LinkedHashMap<Integer, String>();
		populatePropertyValueList(deviceInfo, messageId, propertyList, propertyValueList, true, conn);

		// rewrite blob
		BlobableBufferStream bufferStream = new BlobableBufferStream(true);
		Writer writer = new OutputStreamWriter(bufferStream.getOutputStream(), deviceInfo.getDeviceCharset());
		MessageDataUtils.appendEfficientPropertyValueList(writer, propertyValueList, false);
		writer.flush();
		writer.close();
		return bufferStream.getBlob();
	}

	/**
	 * @param deviceInfo
	 * @return
	 */
	protected static IntegerRangeSet getPropertyIndexesToRequest(DeviceInfo deviceInfo) {
		return deviceInfo.getPropertyIndexesToRequest();
	}

	public static long getDeviceId(String deviceName, long time) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		params.put("effectiveTime", time);
		try {
			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
		} catch(NotEnoughRowsException e) {
			throw new ServiceException("Could not find device " + deviceName, e);
		} catch(SQLException e) {
			throw new ServiceException("Could not get device id", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get device id", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get device id", e);
		}
		try {
			return ConvertUtils.getLong(params.get("deviceId"));
		} catch(ConvertException e) {
			throw new ServiceException("Could not get device id", e);
		}
	}
	public static Object addPendingFileTransfer (int fileTypeId, String fileName, InputStream in, long contentLength, String deviceName, int filePacketSize, int executeOrder, Connection conn) throws SQLException, DataLayerException {
		int fileGroupNum = 0; /*b/c half-duplex is enforced, only one file transfer occurs at a time and there is not file Group Num*/

		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		params.put("fileName", fileName);
		params.put("fileTypeId", fileTypeId);
		params.put("dataType", MessageType.FILE_XFER_START_4_1.getHex());
		params.put("fileGroupNum", fileGroupNum);
		params.put("filePacketSize", filePacketSize);
		params.put("executeOrder", executeOrder);
		DataLayerMgr.executeCall(conn, "ADD_PENDING_FILE_TRANSFER", params);
		params.put("fileContent", in);
		DataLayerMgr.executeCall(conn, "UPDATE_FILE_TRANSFER_DATA", params);
		return params.get("commandId");
	}
	public static Map<Integer,String> constructUSRPropertyList(Message message, Set<Integer> readyToActivateProperties) {
		Map<Integer,String> props = new HashMap<Integer, String>();
		DeviceInfo deviceInfo;
		try {
			deviceInfo = message.getDeviceInfo();
		} catch(ServiceException e) {
			log.warn("Could not get Device Info", e);
			return props;
		}
		long currentTime = System.currentTimeMillis();
		props.put(DeviceProperty.UTC_TIME.getValue(), String.valueOf(currentTime / 1000));
		props.put(DeviceProperty.MASTER_ID.getValue(), String.valueOf(Math.max(currentTime / 1000, deviceInfo.getMasterId())));
		String timeZoneGuid = deviceInfo.getTimeZoneGuid();
		if(timeZoneGuid != null  && (timeZoneGuid=timeZoneGuid.trim()).length() > 0) {
			props.put(DeviceProperty.UTC_OFFSET.getValue(), String.valueOf(TimeZone.getTimeZone(timeZoneGuid).getOffset(currentTime) / 1000 / 60 / 15));
		} else {
			log.info("Could not get time zone for device '" + message.getDeviceName() + "'");
		}
		ActivationStatus activationStatus = deviceInfo.getActivationStatus();
		switch(activationStatus) {
			case READY_TO_ACTIVATE:
				//Add any non-default readyToActivateProperties (typically MDB or DEX settings)
				if(readyToActivateProperties != null && !readyToActivateProperties.isEmpty()) {
					try {
						populatePropertyValueList(deviceInfo, message.getData().getMessageId(), readyToActivateProperties, props, false);
					} catch(SQLException e) {
						log.warn("Could not get ready-to-activate properties", e);
					} catch(DataLayerException e) {
						log.warn("Could not get ready-to-activate properties", e);
					} catch(ConvertException e) {
						log.warn("Could not get ready-to-activate properties", e);
					}
				}
			default:
				props.put(DeviceProperty.ACTIVATION_STATUS.getValue(), String.valueOf(activationStatus.getValue()));
				break;
		}
		return props;
	}

	public static BufferStream constructPropertyValueStream(DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, boolean includeDefaults) throws SQLException, DataLayerException, ConvertException, IOException {
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnectionForCall("GET_PROPERTY_LIST_VALUES");
			return constructPropertyValueStream(deviceInfo, messageId, propertyList, includeDefaults, conn);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
	public static HeapBufferStream constructPropertyValueStream(DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, boolean includeDefaults, Connection conn) throws SQLException, DataLayerException, ConvertException, IOException {
		Map<Integer,String> propertyValues = new LinkedHashMap<Integer, String>();
		populatePropertyValueList(deviceInfo, messageId, propertyList, propertyValues, includeDefaults, conn);
		return constructPropertyValueStream(deviceInfo, propertyValues);
	}

	public static HeapBufferStream constructPropertyValueStream(String deviceName, Charset deviceCharset, Long messageId, Collection<Integer> propertyList, boolean includeDefaults, Connection conn) throws SQLException, DataLayerException, ConvertException, IOException {
		Map<Integer, String> propertyValues = new LinkedHashMap<Integer, String>();
		populatePropertyValueList(deviceName, null, messageId, propertyList, propertyValues, includeDefaults, conn);
		return constructPropertyValueStream(deviceName, deviceCharset, propertyValues);
	}

	public static Map<Integer, String> constructPropertyValue(DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, boolean includeDefaults) throws SQLException, DataLayerException, ConvertException {
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnectionForCall("GET_PROPERTY_LIST_VALUES");
			Map<Integer,String> propertyValues = new LinkedHashMap<Integer, String>();
			populatePropertyValueList(deviceInfo, messageId, propertyList, propertyValues, includeDefaults, conn);
			return propertyValues;
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	public static Map<Integer, String> constructPropertyValue(DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, boolean includeDefaults, Connection conn) throws SQLException, DataLayerException, ConvertException {
		Map<Integer,String> propertyValues = new LinkedHashMap<Integer, String>();
		populatePropertyValueList(deviceInfo, messageId, propertyList, propertyValues, includeDefaults, conn);
		return propertyValues;
	}

	public static HeapBufferStream constructPropertyValueStream(DeviceInfo deviceInfo, Map<Integer, String> propertyValues) throws IOException {
		return constructPropertyValueStream(deviceInfo.getDeviceName(), deviceInfo.getDeviceCharset(), propertyValues);
	}

	public static HeapBufferStream constructPropertyValueStream(String deviceName, Charset deviceCharset, Map<Integer, String> propertyValues) throws IOException {
		log.info(deviceName + ": Constructed property list for the device: " + MessageDataUtils.buildEfficientPropertyValueList(propertyValues, true).replace("\n", "\\n"));
		//TODO: we may need to set a limit on the number of properties for which we use this "in-memory" method of generating the propertyValueString
		HeapBufferStream bufferStream = new HeapBufferStream(1024, 64, false);
		Writer writer = new OutputStreamWriter(bufferStream.getOutputStream(), deviceCharset);
		MessageDataUtils.appendEfficientPropertyValueList(writer, propertyValues, false);
		writer.flush();
		return bufferStream;
	}

	public static void populatePropertyValueList(DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, Map<Integer,String> propertyValueList, boolean includeDefaults) throws SQLException, DataLayerException, ConvertException {
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnectionForCall("GET_PROPERTY_LIST_VALUES");
			populatePropertyValueList(deviceInfo, messageId, propertyList, propertyValueList, includeDefaults, conn);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
	public static void populatePropertyValueList(DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, Map<Integer,String> propertyValueList, boolean includeDefaults, Connection conn) throws SQLException, DataLayerException, ConvertException {
		populatePropertyValueList(deviceInfo.getDeviceName(), deviceInfo, messageId, propertyList, propertyValueList, includeDefaults, conn);
	}

	protected static void populatePropertyValueList(String deviceName, DeviceInfo deviceInfo, Long messageId, Collection<Integer> propertyList, Map<Integer, String> propertyValueList, boolean includeDefaults, Connection conn) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		if(propertyList != null) {
			// Handle dynamic properties (and those not allowed)
			propertyList = new HashSet<>(propertyList); // create copy to avoid ConcurrentModificationException
			long currentTime = 0;
			for(Iterator<Integer> iter = propertyList.iterator(); iter.hasNext(); ) {
				Integer i = iter.next();
				if(i != null) {
					DeviceProperty dp;
					try {
						dp = DeviceProperty.getByValue(i);
					} catch(InvalidIntValueException e) {
						continue;
					}
					switch(dp.getDevicePropertyType()) {
						case READ_ONLY:
							// do not send - client read only
							iter.remove();
							break;
						case DYNAMIC:
							iter.remove();
							if(deviceInfo != null) // if deviceInfo is null, this request is offline and makes no sense to include dynamic properties
								switch(dp) {
									case MASTER_ID:
										if(currentTime == 0)
											currentTime = System.currentTimeMillis();
										propertyValueList.put(i, String.valueOf(Math.max(currentTime / 1000, deviceInfo.getMasterId())));
										break;
									case MESSAGE_ID:
										if(currentTime == 0)
											currentTime = System.currentTimeMillis();
										propertyValueList.put(i, String.valueOf(messageId == null || messageId < currentTime ? currentTime : messageId));
										break;
									case UTC_TIME:
										if(currentTime == 0)
											currentTime = System.currentTimeMillis();
										propertyValueList.put(i, String.valueOf(currentTime / 1000));
										break;
									case UTC_OFFSET:
										if(currentTime == 0)
											currentTime = System.currentTimeMillis();
										String timeZoneGuid = deviceInfo.getTimeZoneGuid();
										if(timeZoneGuid != null && (timeZoneGuid = timeZoneGuid.trim()).length() > 0) {
											propertyValueList.put(i, String.valueOf(TimeZone.getTimeZone(timeZoneGuid).getOffset(currentTime) / 1000 / 60 / 15));
										} else {
											log.info("Could not get time zone for device '" + deviceInfo.getDeviceName() + "'");
										}
										break;
									case ACTIVATION_STATUS:
										propertyValueList.put(i, String.valueOf(deviceInfo.getActivationStatus().getValue()));
										break;
									default:
										log.warn("Dynamic Property " + dp + " not handled", new Throwable());
								}
							break;
					}
				}
			}
		}
		if(includeDefaults)
			params.put("includeDefaults", "Y");
		params.put("propertyList", propertyList);
		params.put("deviceName", deviceName);
		Results results = DataLayerMgr.executeQuery(conn, "GET_PROPERTY_LIST_VALUES", params);
		propertyValueList.clear();
		try {
			while (results.next()) {
				Integer propertyIndex = results.getValue("propertyIndex", Integer.class);
				String propertyValue = results.getValue("propertyValue", String.class);
				String isEqualToDefault = results.getValue("isEqualToDefault", String.class);
				propertyValue = transformPropertyValue(deviceName, propertyIndex, propertyValue, deviceInfo);
				if (isEqualToDefault.equals("Y")) {
					propertyValueList.put(propertyIndex, propertyValue);
				} else {
					if (propertyValue == null) {
						propertyValueList.put(propertyIndex, "");
					} else {
						propertyValueList.put(propertyIndex, propertyValue);
					}
				}
			}
		} finally {
			results.close();
		}
	}
	
	// Some properties may not be backwards compatible and need manual transformation to work for devices with older property revs 
	public static String transformPropertyValue(String deviceName, int propertyIndex, String propertyValue, DeviceInfo deviceInfo) {
		if (propertyValue == null)
			return null;
		
		String newValue = propertyValue;
		if (deviceInfo.getPropertyListVersion() < 20 && propertyValue != null && propertyValue.startsWith("E^")) {
			// schedule type E (daily time list) is not supported in property list versions less than 20
			// convert the daily time list to interval type
			try {
				String[] timeList = propertyValue.substring(2).split("\\|");
				String firstHHMM = timeList[0];
				if (timeList.length == 1) { // only 1 time per day, just use the daily schedule type
					newValue = "D^" + firstHHMM;
				} else {
					// approximate an interval schedule format 
					int firstUtcOffsetTimeSeconds = TimeUtils.hhmmToUtcOffsetSeconds(firstHHMM, deviceInfo.getTimeZoneGuid());
					String secondHHMM = timeList[1];
					int secondUtcOffsetTimeSeconds = TimeUtils.hhmmToUtcOffsetSeconds(secondHHMM, deviceInfo.getTimeZoneGuid());
					int intervalSeconds = Math.abs(firstUtcOffsetTimeSeconds - secondUtcOffsetTimeSeconds);
					// team requests that in addition to transform we also store what is sent
					newValue = String.format("I^%s^%s", firstUtcOffsetTimeSeconds, intervalSeconds);
				}
			} catch (Exception e) {
				log.warn("Failed to convert property {0} daily time list ''{1}'' to interval time for device {2}", propertyIndex, propertyValue, deviceInfo.getDeviceName());
				newValue = "D^0300";
			}
		}
		
		if (!newValue.equals(propertyValue)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("deviceName", deviceName);
			params.put("name", Integer.toString(propertyIndex));
			params.put("value", newValue);
			try {
				DataLayerMgr.executeCall("UPDATE_DEVICE_SETTING_BY_DEVICE_NAME", params, true);
			} catch (Exception e) {
				log.warn("Failed to update transformed device setting for deviceName={0}, name={1}, newValue={2}, oldValue={3}", deviceName, propertyIndex, newValue, propertyValue);
			}
		}
		
		return newValue;
	}

	public static Connection getConnection(String dataSourceName, boolean retry) throws RetrySpecifiedServiceException, ServiceException{
		Connection conn=null;
		try {
            conn = DataLayerMgr.getConnection(dataSourceName);
        } catch(Exception e) {
        	if(retry){
        		throw new RetrySpecifiedServiceException("Could not get connection for data source " + dataSourceName, e, WorkRetryType.BLOCKING_RETRY);
        	}
			throw new ServiceException("Could not get connection for data source " + dataSourceName, e);
        }
        return conn;
	}
	
	public static Connection getConnection(boolean retry) throws RetrySpecifiedServiceException, ServiceException{
		return getConnection("OPER", retry);		
	}
	
	public static void loadHostESuds(Log log, int hostNumber, int hostType, Component component, Connection conn, Map<Integer, String> deviceTypeHostTypes, Map<String, Object> params, Map<String, Map<String, Object>> hosts, Charset deviceCharset, int hostPositionNum) throws Exception
    {
		loadHost(log, hostNumber, null, hostType, component, conn, false, deviceTypeHostTypes, params, hosts, deviceCharset, true, hostPositionNum);
    }
	
	public static void loadHost(Log log, int hostNumber, String hostCurrency, int hostType, Component component, Connection conn, boolean baseHost, Map<Integer, String> deviceTypeHostTypes, Map<String, Object> params, Map<String, Map<String, Object>> hosts, Charset deviceCharset) throws Exception
    {
		loadHost(log, hostNumber, hostCurrency, hostType, component, conn, baseHost, deviceTypeHostTypes, params, hosts, deviceCharset, false, -1);
    }
	
	public static void loadHost(Log log, int hostNumber, String hostCurrency, int hostType, Component component, Connection conn, boolean baseHost, Map<Integer, String> deviceTypeHostTypes, Map<String, Object> params, Map<String, Map<String, Object>> hosts, Charset deviceCharset, boolean useHostPositionNum, int hostPositionNum) throws Exception
    {
        HashMap<String, Object> host = new HashMap<String, Object>();

        if (baseHost == true)
        {
            host.put("hostCurrency", hostCurrency);
        }
        String hostManufacturer = component.getManufacturer();
        String hostModel = component.getModel();
        String hostSerialNumber = component.getSerialNumber();
        String hostRevision = component.getRevision();
        String hostLabel = component.getLabel();

        log.info("Host hostNumber: " + hostNumber +
                (baseHost == true ? ", hostCurrency: " + hostCurrency : "") +
        		", hostType: " + hostType +
                ", hostManufacturer: " + hostManufacturer +
                ", hostModel: " + hostModel +
                ", hostSerialNumber: " + hostSerialNumber +
                ", hostRevision: " + hostRevision +
                ", hostLabel: " + hostLabel);

        if (deviceTypeHostTypes.containsKey(hostType) == false)
            throw new ServiceException("Invalid hostType: " + hostType);

        if (baseHost && !deviceTypeHostTypes.get(hostType).equals("B"))
        	throw new ServiceException("Invalid base host hostType: " + hostType);

        if (!baseHost && deviceTypeHostTypes.get(hostType).equals("B"))
        	throw new ServiceException("Invalid non-base host hostType: " + hostType);

        if (hostManufacturer.length() == 0)
            hostManufacturer = "Unknown";
        if (hostModel.length() == 0)
            hostModel = "Unknown";

        params.clear();
        params.put("hostEquipmentMfgr", hostManufacturer);
        params.put("hostEquipmentModel", hostModel);
        Object[] ret = DataLayerMgr.executeCall(conn, "GET_OR_CREATE_HOST_EQUIPMENT", params);
        long hostEquipmentId = ConvertUtils.convert(Long.class, ret[1]);
        BooleanType hostEquipmentExists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[2]));
        log.info((hostEquipmentExists == BooleanType.TRUE ? "Existing" : "New") + " hostEquipmentId: " + hostEquipmentId);

        host.put("hostType", hostType);
        host.put("hostSerialNumber", hostSerialNumber);
        host.put("hostRevision", hostRevision);
        host.put("hostLabel", hostLabel);
        host.put("hostEquipmentId", hostEquipmentId);
        if(useHostPositionNum){
        	//for eSuds
        	host.put("hostPortNum", hostNumber);
        	host.put("hostPositionNum", hostPositionNum);
        	hosts.put(getHostMapKey(hostNumber, hostPositionNum), host);
        }else{
        	//Others
        	hosts.put(String.valueOf(hostNumber), host);
        }
    }

	public static Map<Integer, String> getDeviceTypeHostTypes(int deviceTypeId) throws DataLayerException, SQLException, ConvertException{
		Map<Integer, String> deviceTypeHostTypes = new HashMap<Integer, String>();
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("deviceTypeId", deviceTypeId);
        Results results = DataLayerMgr.executeQuery("GET_DEVICE_TYPE_HOST_TYPES", params);
        while(results.next()) {
            deviceTypeHostTypes.put(results.getValue("hostTypeId", Integer.class), results.getValue("deviceTypeHostTypeCd", String.class));
        }
        return deviceTypeHostTypes;
	}

	public static String getHostMapKey(int hostPortNum, int hostPositionNum){
    	return hostPortNum+"-"+hostPositionNum;
    }

	public static void sendEmail(Publisher<ByteInput> bridgePublisher, String queueKey, String emailTo, String emailFrom, String subject, String body) throws ServiceException {
		//put it to a queue and SendEmailTask will send it out
		MessageChain mc = new MessageChainV11();
		MessageChainStep step;
		Publisher<ByteInput> publisher = getReportingPublisher();
		if(publisher != null)
			step = mc.addStep(queueKey);
		else if(bridgePublisher == null)
			throw new ServiceException("Both ReportingPublisher and BridgePublisher are null");
		else {
			publisher = bridgePublisher;
			step = mc.addStep("usat.bridge.to.msr");
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, queueKey);
		}
		step.addStringAttribute("emailTo", emailTo);
		step.addStringAttribute("emailFrom", emailFrom);
		step.addStringAttribute("subject", subject);
		step.addStringAttribute("body", body);
		MessageChainService.publish(mc, publisher);
	}

	public static void sendEmail(Publisher<ByteInput> bridgePublisher, Map<String, String> params) throws ServiceException {
		//put it to a queue and SendEmailTask will send it out
		MessageChain mc = new MessageChainV11();
		String queueKey = params.get("queueKey");
		MessageChainStep step;
		Publisher<ByteInput> publisher = getReportingPublisher();
		if(publisher != null)
			step = mc.addStep(queueKey);
		else if(bridgePublisher == null)
			throw new ServiceException("Both ReportingPublisher and BridgePublisher are null");
		else {
			publisher = bridgePublisher;
			step = mc.addStep("usat.bridge.to.msr");
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, queueKey);
		}
		step.addStringAttribute("emailTo", params.get("emailTo"));
		step.addStringAttribute("emailFrom", params.get("emailFrom"));
		step.addStringAttribute("subject", params.get("subject"));
		step.addStringAttribute("body", params.get("body"));
		if(!StringUtils.isBlank(emailQueueHistQueueKey)) {
			MessageChainStep dbStep = mc.addStep(emailQueueHistQueueKey);
			dbStep.addStringAttribute("emailTo", params.get("emailTo"));
			dbStep.addStringAttribute("emailFrom", params.get("emailFrom"));
			dbStep.addStringAttribute("subject", params.get("subject"));
			dbStep.addStringAttribute("body", params.get("body"));
			dbStep.addLongAttribute("scheduledTime", System.currentTimeMillis());
			step.setNextSteps(0, dbStep);
		}
		MessageChainService.publish(mc, publisher);
	}

	public static String getEmailQueueHistQueueKey() {
		return emailQueueHistQueueKey;
	}

	public static void setEmailQueueHistQueueKey(String emailQueueHistQueueKey) {
		AppLayerUtils.emailQueueHistQueueKey = emailQueueHistQueueKey;
	}
	
	public static int getValidDeviceTimeDays() {
		return validDeviceTimeDays;
	}

	public static void setValidDeviceTimeDays(int validDeviceTimeDays) {
		AppLayerUtils.validDeviceTimeDays = validDeviceTimeDays;
	}

	public static String getCdmaModemExpression() {
		return cdmaModemExpression.pattern();
	}

	public static void setCdmaModemExpression(String cdmaModemExpression) {
		AppLayerUtils.cdmaModemExpression = Pattern.compile(cdmaModemExpression);
	}
	public static boolean isCdmaModem(String cgmm) {
		return cdmaModemExpression.matcher(cgmm).find();
	}

	public static int clearHostSetting(Connection conn, Map<String, Object> params, long hostId) throws SQLException, DataLayerException {	
		params.clear();
		params.put("hostId", hostId);
		return  DataLayerMgr.executeUpdate(conn, "CLEAR_HOST_SETTING", params);
	}

	public static DeviceInfo processOfflineInit(InitMessageData data, Log log, long encryptionKeyMinAgeMin, boolean deviceReactivationOnReinit) throws SQLException, DataLayerException, ConvertException, ServiceException, InvalidByteValueException, BeanException, MessagingException {
		return processInit(null, data, log, encryptionKeyMinAgeMin, deviceReactivationOnReinit, false, null, null, null, true, null);
	}

	public static DeviceInfo processOfflineInit(InitMessageData data, Log log, long encryptionKeyMinAgeMin, boolean deviceReactivationOnReinit, Holder<InitInfo> initInfo) throws SQLException, DataLayerException, ConvertException, ServiceException, InvalidByteValueException, BeanException, MessagingException {
		return processInit(null, data, log, encryptionKeyMinAgeMin, deviceReactivationOnReinit, false, null, null, null, true, initInfo);
	}

	protected static DeviceInfo processInit(Message message, InitMessageData data, Log log, long encryptionKeyMinAgeMin, boolean deviceReactivationOnReinit, boolean initEmailEnabled, String emailQueueKey, String emailTo, String emailFrom, boolean preregistering, Holder<InitInfo> initInfo) throws SQLException, DataLayerException, ConvertException, ServiceException, InvalidByteValueException,
			BeanException,
			MessagingException {
		DeviceType deviceType = data.getDeviceType();
		String deviceSerialCd = data.getDeviceSerialNum();
		String deviceInformation = data.getDeviceInfo();
		String terminalInformation = data.getTerminalInfo();
		switch(deviceType) {
			case MEI:
				if(!deviceSerialCd.startsWith("M1"))
					deviceSerialCd = "M1" + deviceSerialCd;
				break;
			case KIOSK:
				deviceSerialCd = deviceSerialCd.toUpperCase();
				break;
			default:
				break;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceTypeId", deviceType.getValue());
		params.put("deviceSerialCd", deviceSerialCd);
		params.put("preregistering", preregistering ? 'Y' : 'N');
		String deviceName;
		String timeZoneGuid;
		boolean masterIdIncrementOnly;
		byte[] newEncryptionKey = null;
		long updateTime;
		boolean okay = false;
		Connection conn = DataLayerMgr.getConnection("OPER");
		try {
			DataLayerMgr.executeCall(conn, "INITIALIZE_DEVICE", params);
			ResultCode resultCd = ResultCode.getByValue(ConvertUtils.getByte(params.get("resultCd")));
			String errorMessage = ConvertUtils.getString(params.get("errorMessage"), false);
			if(resultCd != ResultCode.SUCCESS)
				throw new ServiceException("Could not initialize device " + deviceSerialCd + " because " + errorMessage);
			long deviceId = ConvertUtils.getInt(params.get("deviceId"));
			deviceName = ConvertUtils.getString(params.get("deviceName"), null);
			String deviceTypeDesc = ConvertUtils.getString(params.get("deviceTypeDesc"), null);
			int deviceSubTypeId = ConvertUtils.getInt(params.get("deviceSubTypeId"), 0);
			Boolean prevActive = ConvertUtils.convert(Boolean.class, params.get("prevDeviceActiveFlag"));
			long keyGenerationTime = ConvertUtils.getLong(params.get("keyGenerationTime"), 0);
			boolean legacySafeKey = ConvertUtils.getBoolean(params.get("legacySafeKeyFlag"), false);
			masterIdIncrementOnly = ConvertUtils.getBoolean(params.get("masterIdIncrementOnly"));
			timeZoneGuid = ConvertUtils.getStringSafely(params.get("timeZoneGuid"));
			int newHostCount = ConvertUtils.getInt(params.get("newHostCount"), 0);
			updateTime = ConvertUtils.getLongSafely(params.get("updateTime"), 0);

			log.info("Initialized " + (prevActive == null ? " new " : prevActive ? " already active " : " deactive ") + deviceTypeDesc + " device " + deviceSerialCd + " (deviceName: " + deviceName + "; deviceId: " + deviceId + "; newHostCount: " + newHostCount + ")");
	
			if((!deviceSerialCd.startsWith("K3")) && (keyGenerationTime == 0 || (message != null && rotateEncryptionKey(deviceType, deviceSubTypeId, keyGenerationTime, encryptionKeyMinAgeMin, data.getMessageType(), legacySafeKey)))) {
				switch(deviceType) {
					case GX:
						if(data.getMessageType() != MessageType.INITIALIZATION_3_1) {
							newEncryptionKey = LegacyUtils.generateLegacyEncryptionKey();
							break;
						}
						// fall-through
					case KIOSK:
					case ESUDS:
					case EDGE:
						newEncryptionKey = generateEncryptionKey(log);
						break;
					default:
						newEncryptionKey = LegacyUtils.generateLegacyEncryptionKey();
				}
				params.clear();
				params.put("encryptionKey", newEncryptionKey);
				params.put("deviceId", deviceId);
				DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_ENCRYPTION_KEY", params);
				log.info("Generated a new encryption key for " + deviceName);
			}
	
			// Add deviceInformation and terminalInformation to DEVICE_SETTING
			params.clear();
			params.put("deviceId", deviceId);
			params.put("name", DEVICE_INFORMATION_PARAMETER_CD);
			params.put("value", deviceInformation);
			DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);
			params.put("name", TERMINAL_INFORMATION_PARAMETER_CD);
			params.put("value", terminalInformation);
			DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);
	
			if(prevActive != null && !prevActive.booleanValue()) {
				if (deviceReactivationOnReinit) {
					params.clear();
					params.put("deviceId", deviceId);
					DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_ACTIVE_YN_FLAG", params);
				} else
					throw new ServiceException("Device " + deviceSerialCd + " is disabled");
			}
	
			if(data.getMessageType() == MessageType.INITIALIZATION_4_1) {
				MessageData_C0 dataC0 = (MessageData_C0) data;
				AppLayerUtils.upsertDeviceSetting(conn, params, deviceId, "Protocol Revision", Long.toString(dataC0.getProtocolComplianceRevision()));
				AppLayerUtils.upsertDeviceSetting(conn, params, deviceId, "Firmware Version", dataC0.getDeviceFirmwareVersion());
				parseDeviceInfo(conn, deviceId, deviceType, deviceName, ConvertUtils.getLocalTime(data.getMessageStartTime(), timeZoneGuid), deviceInformation);
			}
			boolean needFirmwareVersion = true;
			int oldPropertyListVersion = -1;
			switch(data.getDeviceType()) {
				case GX:
					if(data.getMessageType() == MessageType.INITIALIZATION_3_1) {
						if(parseDeviceInfo(conn, deviceId, deviceType, deviceName, ConvertUtils.getLocalTime(data.getMessageStartTime(), timeZoneGuid), deviceInformation))
							needFirmwareVersion = false;
					}
					// fall-through
				case G4:
				case MEI:
				case TRANSACT:
				case EDGE:
					params.clear();
					params.put("deviceName", deviceName);
					params.put("deviceTypeId", deviceType);
					params.put("propertyListVersion", data.getPropertyListVersion() < 0 ? null : data.getPropertyListVersion());
					DataLayerMgr.executeCall(conn, "INITIALIZE_CONFIG_FILE", params);
					oldPropertyListVersion = ConvertUtils.getIntSafely(params.get("oldPropertyListVersion"), -1);

					// for G4 and GX config poke
					if(deviceType == DeviceType.G4 || deviceType == DeviceType.GX) {
						LegacyUtils.configPoke(params, conn, deviceName, deviceType.getValue());
						// device firmware version query
						if(needFirmwareVersion)
							LegacyUtils.createPeekRequest(params, conn, deviceName, LegacyUtils.CMD_FIRMWARE_VERSION, -97);
						// device gprs settings query
						// processed by InboundFileTransferTask in isCheckGPRSModem() if block
						LegacyUtils.createPeekRequest(params, conn, deviceName, LegacyUtils.CMD_WAVECOM_MODEM_INFO, -96);
						String pendingCmdState = ConvertUtils.getStringSafely(params.get("sessionAttributes"), "");
						if(pendingCmdState.length() == 0)
							pendingCmdState = "CG";
						else {
							if(pendingCmdState.indexOf('C') < 0)
								pendingCmdState += "C";
							if(pendingCmdState.indexOf('G') < 0)
								pendingCmdState += "G";
						}
						if(message != null)
							message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR, pendingCmdState);
					}
					/**
					 * This block does not seem necessary because MEI after 8E
					 * will send the deviceName-CFG file to the server Rerix
					 * just delete 87 then when device do 92 server will say no
					 * pending command:
					 * else if(deviceType == DeviceType.MEI) {
					 * //create MEI peek command Peek for it's full config
					 * createPeekRequest(params, conn, deviceName, LegacyUtils.CMD_UPLOAD_CONFIG, 1); }
					 */
					break;
				case KIOSK:
					LegacyUtils.createKioskInitConfigCommand(params, conn, deviceName, deviceId);
					// call INITIALIZE_CONFIG_FILE takes care of updating Property List Version
					if(data.getPropertyListVersion() >= 0) {
						AppLayerUtils.upsertDeviceSetting(conn, params, deviceId, "Property List Version", Integer.toString(data.getPropertyListVersion()));
						log.info("Updated property list version to " + data.getPropertyListVersion());
					}

					// send init email
					if(initEmailEnabled && message != null) {
						// Further device specific processing
						if(data.getMessageType() != MessageType.INITIALIZATION_3_0) {
							if(deviceInformation != null && deviceInformation.length() > 0) {
								if(deviceInformation.length() > 2 && deviceInformation.startsWith("A\n"))
									deviceInformation = deviceInformation.substring(2);
								else
									log.warn("Received unsupported Device Info format!");
							}

							if(terminalInformation != null && terminalInformation.length() > 0) {
								if(terminalInformation.length() > 2 && terminalInformation.startsWith("A\n"))
									terminalInformation = terminalInformation.substring(2);
								else
									log.warn("Received unsupported Terminal Info format!");
							}
						}

						LegacyUtils.sendKioskInitEmail(message.getPublisher(), conn, emailQueueKey, emailTo, emailFrom, data.getMessageType().getTitle(), deviceId, deviceName, deviceSerialCd, deviceInformation, terminalInformation);
					}
					break;
				case ESUDS:
					// eSuds client version request 83
					LegacyUtils.createServerToClientRequest(params, conn, deviceName, "0B", 15);
					// Device Reactivate 76
					params.clear();
					params.put("deviceName", deviceName);
					params.put("dataType", MessageType.DEVICE_REACTIVATE.getHex());// value
					params.put("command", null);
					params.put("executeOrder", 1);
					DataLayerMgr.executeUpdate(conn, "ADD_PENDING_COMMAND", params);
					// call INITIALIZE_CONFIG_FILE takes care of updating Property List Version
					if(data.getPropertyListVersion() >= 0) {
						AppLayerUtils.upsertDeviceSetting(conn, params, deviceId, "Property List Version", Integer.toString(data.getPropertyListVersion()));
						log.info("Updated property list version to " + data.getPropertyListVersion());
					}
					break;
				default:
					// call INITIALIZE_CONFIG_FILE takes care of updating Property List Version
					if(data.getPropertyListVersion() >= 0) {
						AppLayerUtils.upsertDeviceSetting(conn, params, deviceId, "Property List Version", Integer.toString(data.getPropertyListVersion()));
						log.info("Updated property list version to " + data.getPropertyListVersion());
					}
			}
			conn.commit();
			okay = true;
			if(initInfo != null)
				initInfo.setValue(new InitInfo(deviceId, deviceName, prevActive == null, oldPropertyListVersion));
		} finally {
			try {
				if(!okay)
					conn.rollback();
			} finally {
				ProcessingUtils.closeDbConnection(log, conn);
			}
		}
		if(message != null) {
			DeviceInfo deviceInfoObj = message.getNewDeviceInfo(deviceName);
			okay = false;
			try {
				deviceInfoObj.setDeviceSerialCd(deviceSerialCd);
				deviceInfoObj.setDeviceType(deviceType);
				deviceInfoObj.setInitOnly(false);
				deviceInfoObj.setPropertyListVersion(data.getPropertyListVersion() < 0 ? null : data.getPropertyListVersion());
				deviceInfoObj.setMasterIdIncrementOnly(masterIdIncrementOnly);
				if(data.getMessageType() != MessageType.INITIALIZATION_4_1)
					deviceInfoObj.setActivationStatus(ActivationStatus.ACTIVATED);

				if(newEncryptionKey != null) {
					byte[] prevEncryptionKey = message.getDeviceInfo().getEncryptionKey();
					if(prevEncryptionKey != null) {
						deviceInfoObj.setPreviousEncryptionKey(prevEncryptionKey);
					}
					deviceInfoObj.setEncryptionKey(newEncryptionKey);
				}

				// Get TimeZone
				if(timeZoneGuid != null && (timeZoneGuid = timeZoneGuid.trim()).length() > 0)
					deviceInfoObj.setTimeZoneGuid(timeZoneGuid);
				deviceInfoObj.commitChanges(updateTime);
				log.info("Updated device info for initialization of " + deviceName);
				okay = true;
				return deviceInfoObj;
			} finally {
				if(!okay)
					deviceInfoObj.clearChanges();
			}
		}
		return null;
	}

	protected static final Pattern COMPONENT_KEY_PATTERN = Pattern.compile("C_(\\d+)");

	public static boolean parseDeviceInfo(Connection conn, long deviceId, DeviceType deviceType, String deviceName, long localTime, String deviceInformation) throws SQLException, DataLayerException, ConvertException, ServiceException {
		boolean validDeviceInfo = false;
		if(deviceInformation != null && deviceInformation.length() > 0) {
			if(deviceInformation.length() > 2 && deviceInformation.startsWith("A\n")) {
				Map<String, String> deviceInformationMap = ProcessingUtils.parseEndpointInformation(deviceInformation.substring(2));
				try {
					InitializationReason reason = ConvertUtils.convert(InitializationReason.class, deviceInformationMap.get("REASON"));
					if(reason != null)
						log.info("Device is initializing with reason code " + reason);
				} catch(ConvertException e) {
					log.warn("Invalid reason code '" + deviceInformationMap.get("REASON") + "' received from the device");
				}
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("deviceId", deviceId);
				for(Map.Entry<String, String> entry : deviceInformationToSetting.entrySet()) {
					if(deviceInformationMap.containsKey(entry.getKey())) {
						String value = deviceInformationMap.get(entry.getKey());
						params.put("name", entry.getValue());
						params.put("value", value);
						DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);
						log.info("Updated device setting '" + entry.getValue() + "' to '" + value + "'");
						validDeviceInfo = true;
					}
				}
				Set<Integer> providedComponentNumbers = new HashSet<>();
				// create host for bezel
				String bezelInformation = deviceInformationMap.get("BEZEL");
				if(bezelInformation != null && bezelInformation.length() > 0) {
					parseBezelInfo(bezelInformation, deviceId, params, localTime, conn);
					providedComponentNumbers.add(1);
					providedComponentNumbers.add(ProcessingConstants.BEZEL_HOST_PORT_NUM);
					providedComponentNumbers.add(ProcessingConstants.MODEM_HOST_PORT_NUM);
				}
				// parse modem info
				String modemInformation = deviceInformationMap.get("MODEM");
				if(modemInformation != null && modemInformation.length() > 0) {
					LegacyUtils.parseModemInfo(modemInformation, deviceId, deviceType, deviceName, localTime, conn);
					providedComponentNumbers.add(1);
					providedComponentNumbers.add(ProcessingConstants.MODEM_HOST_PORT_NUM);
					providedComponentNumbers.add(ProcessingConstants.BEZEL_HOST_PORT_NUM);
				}
				// parse new-style components
				for(Map.Entry<String, String> entry : deviceInformationMap.entrySet()) {
					Matcher matcher = COMPONENT_KEY_PATTERN.matcher(entry.getKey());
					if(matcher.matches()) {
						int n = ConvertUtils.getInt(matcher.group(1));
						Map<String, String> componentInformationMap = ProcessingUtils.parseEndpointSubInformation(entry.getValue());
						Map<String, String> componentSettings = null;
						int hostType = 0;
						String manufacturer = null;
						String model = null;
						String serialNum = null;
						String label = null;
						for(Map.Entry<String, String> compEntry : componentInformationMap.entrySet()) {
							String compValue = compEntry.getValue().trim();
							if(compValue.isEmpty())
								continue;
							String compKey = compEntry.getKey().trim();
							switch(compKey) {
								case "TP": // case "TYPE":
									hostType = ConvertUtils.getIntSafely(compValue, 0);
									if(hostType == 0) {
										log.error("Invalid Host Type (TP) '" + compValue + "' in component info for component " + n + " of device " + deviceName);
										continue;
									}
									break;
								case "MI":
								case "CGMI":
									manufacturer = compValue;
									break;
								case "MN":
								case "CGMM":
									model = compValue;
									break;
								case "SN":
								case "MEID":
								case "CCID":
									serialNum = compValue;
									break;
								case "LB":
								case "CGSN":
									label = compValue;
									break;
								default:
									String setting = componentInformationToSetting.get(compKey);
									if(setting == null)
										setting = compKey;
									if(componentSettings == null)
										componentSettings = new HashMap<>();
									componentSettings.put(setting, compValue);
							}
						}
						if(hostType == 0) {
							log.error("Missing Host Type (TP) in component info for component " + n + " of device " + deviceName);
							continue;
						}
						if(manufacturer == null) {
							log.error("Missing Manufacturer (MI) in component info for component " + n + " of device " + deviceName);
							continue;
						}
						if(model == null) {
							log.error("Missing Model (MN) in component info for component " + n + " of device " + deviceName);
							continue;
						}
						boolean okay = false;
						try {
							long hostId;
							try {
								hostId = AppLayerUtils.upsertHost(conn, params, deviceId, n, 0, hostType, serialNum, manufacturer, model, label, localTime, entry.getValue());
							} catch(SQLIntegrityConstraintViolationException e) {
								throw new RetrySpecifiedServiceException("Invalid Host Type", e, WorkRetryType.NO_RETRY);
							}
							AppLayerUtils.clearHostSetting(conn, params, hostId);
							if(componentSettings != null)
								for(Map.Entry<String, String> settingEntry : componentSettings.entrySet())
									AppLayerUtils.upsertHostSetting(conn, params, hostId, settingEntry.getKey(), settingEntry.getValue());
							params.clear();
							params.put("hostId", hostId);
							params.put("componentInfo", entry.getValue());
							params.put("localTime", localTime);
							DataLayerMgr.executeCall(conn, "UPDATE_GPRS_FOR_DEVICE", params);
							okay = true;
							log.info("Updated Component " + n + " on device " + deviceName + " from '" + entry.getValue() + "'");
						} finally {
							if(okay)
								conn.commit();
							else
								conn.rollback();
						}
						providedComponentNumbers.add(n);
					}
				}
				providedComponentNumbers.add(0); // never remove base host
				params.clear();
				params.put("deviceId", deviceId);
				params.put("newHosts", providedComponentNumbers);
				DataLayerMgr.executeCall(conn, "REMOVE_OLD_HOSTS", params);
				conn.commit();
			} else
				log.warn("Received unsupported Device Info format!");
		}
		return validDeviceInfo;
	}

	public static void parseBezelInfo(String bezelInformation, long deviceId, Map<String, Object> params, long localTime, Connection conn) throws SQLException, ConvertException, DataLayerException {
		Map<String, String> bezelInformationMap = ProcessingUtils.parseEndpointSubInformation(bezelInformation);
		long hostId = AppLayerUtils.upsertHost(conn, params, deviceId, ProcessingConstants.BEZEL_HOST_PORT_NUM, 0, ProcessingConstants.BEZEL_HOST_TYPE_ID, bezelInformationMap.get("SN"), getManufacturer(bezelInformationMap.get("BT")), ConvertUtils.getStringSafely(bezelInformationMap.get("MN"), "Unknown"), null, localTime, bezelInformation);
		AppLayerUtils.clearHostSetting(conn, params, hostId);
		for(Map.Entry<String, String> entry : bezelInformationToSetting.entrySet()) {
			String value = bezelInformationMap.get(entry.getKey());
			if(value != null && value.trim().length() > 0)
				AppLayerUtils.upsertHostSetting(conn, params, hostId, entry.getValue(), value);
		}
		log.info("Updated bezel information for host: " + hostId);
	}

	protected static boolean rotateEncryptionKey(DeviceType deviceType, int deviceSubTypeId, long lastGenerationTime, long encryptionKeyMinAgeMin, MessageType initMessageType, boolean existingIsLegacySafe) {
		switch(deviceType) {
			case GX:
				if(initMessageType == MessageType.INITIALIZATION_3_0 && !existingIsLegacySafe)
					return true;
				break;
			case G4:
			case ESUDS:
			case MEI:
			case EDGE:
			case KIOSK:
				break;
			default:
				return false;
		}
		return lastGenerationTime == 0 || System.currentTimeMillis() > lastGenerationTime + encryptionKeyMinAgeMin * 60 * 1000;
	}

	protected static String getManufacturer(String bezelTypeString) {
		if(bezelTypeString == null || bezelTypeString.length() == 0)
			return "<Not Provided>";
		try {
			return GxBezelType.getByValue(bezelTypeString.charAt(0)).getManufacturer();
		} catch(InvalidValueException e) {
			return "<Invalid Type:" + bezelTypeString + ">";
		}
	}
	
	public static MessageResponse processLegacyInit(Message message, InitMessageData data, Log log, long encryptionKeyMinAgeMin,
			boolean deviceReactivationOnReinit, boolean initEmailEnabled, String emailQueueKey, String emailTo, String emailFrom) throws RetrySpecifiedServiceException {
		try {
			DeviceInfo deviceInfoObj = processInit(message, data, log, encryptionKeyMinAgeMin, deviceReactivationOnReinit, initEmailEnabled, emailQueueKey, emailTo, emailFrom, false, null);
			if(message != null && deviceInfoObj != null)
				MessageProcessingUtils.writeLegacyInitResponse(message, deviceInfoObj.getEncryptionKey(), deviceInfoObj.getDeviceName());
			log.info("Processed initialization for " + data.getDeviceSerialNum());
		} catch(RetrySpecifiedServiceException e) {
			if(e.getRetryType() == WorkRetryType.BLOCKING_RETRY)
				throw e;
			log.warn("Could not process message", e);
			return MessageResponse.CLOSE_SESSION;
		} catch(ServiceException e) {
			log.warn("Could not process message", e);
			return MessageResponse.CLOSE_SESSION;
		} catch(Exception e) {
			log.warn("Could not process message", e);
			return MessageResponse.CLOSE_SESSION;
		}

		return MessageResponse.CONTINUE_SESSION;
	}

	public static class InitInfo {
		public final long deviceId;
		public final String deviceName;
		public final boolean isNew;
		public final int oldPropertyListVersion;

		public InitInfo(long deviceId, String deviceName, boolean isNew, int oldPropertyListVersion) {
			super();
			this.deviceId = deviceId;
			this.deviceName = deviceName;
			this.isNew = isNew;
			this.oldPropertyListVersion = oldPropertyListVersion;
		}
	}
		
    public static MessageResponse processInit(Message message, MessageData_C0 data, Log log, long encryptionKeyMinAgeMin, int defaultFilePacketSize) throws ServiceException {
    	try {
			final Holder<InitInfo> initInfoHolder = new Holder<InitInfo>();
			final DeviceInfo deviceInfoObj = processInit(message, data, log, encryptionKeyMinAgeMin, false, false, null, null, null, false, initInfoHolder);
			final InitInfo initInfo = initInfoHolder.getValue();
			final Map<Integer, String> propertyValues = new LinkedHashMap<Integer, String>();
			final Map<String, Object> params = new HashMap<String, Object>();
			boolean okay = false;
			final Connection conn = DataLayerMgr.getConnection("OPER");
			try {
				switch(data.getDeviceType()) {
					case G4:
					case GX:
					case MEI:
					case TRANSACT:
					case EDGE:
						switch(data.getReasonCode()) {
							case COMMUNICATION_FAILURE:
								if(message != null) {
									if (data.getPropertyListVersion() < 19) // TODO: need a better way to manage these per PLV
										AppLayerUtils.populatePropertyValueList(deviceInfoObj, message.getData().getMessageId(), ProcessingConstants.COMMUNICATION_PROPERTY_INDEXES_PRE_19, propertyValues, true, conn);
									else if (data.getPropertyListVersion() == 19)
										AppLayerUtils.populatePropertyValueList(deviceInfoObj, message.getData().getMessageId(), ProcessingConstants.COMMUNICATION_PROPERTY_INDEXES_19, propertyValues, true, conn);
									else if (data.getPropertyListVersion() == 20)
										AppLayerUtils.populatePropertyValueList(deviceInfoObj, message.getData().getMessageId(), ProcessingConstants.COMMUNICATION_PROPERTY_INDEXES_20, propertyValues, true, conn);
									else if (data.getPropertyListVersion() >= 21)
										AppLayerUtils.populatePropertyValueList(deviceInfoObj, message.getData().getMessageId(), ProcessingConstants.COMMUNICATION_PROPERTY_INDEXES_21, propertyValues, true, conn);
								}
							case APPLICATION_UPGRADE:
								if(initInfo.oldPropertyListVersion != data.getPropertyListVersion() && message != null) {
									/*
									Results results = DataLayerMgr.executeQuery(conn, "GET_UPGRADE_PROPERTY_LIST_VALUES", params);
									try {
										while(results.next()) {
											propertyValues.put(results.getValue("propertyIndex", Integer.class), results.getValue("propertyValue", String.class));
										}
									} finally {
										results.close();
									}*/
									// Just send all the properties to avoid issues
									AppLayerUtils.populatePropertyValueList(deviceInfoObj, data.getMessageId(), null, propertyValues, true, conn);
									cancelPendingPropertyValueListUpdates(deviceInfoObj, conn);
								}
								break;
							case NEW_DEVICE:
								if(message != null) {
									AppLayerUtils.populatePropertyValueList(deviceInfoObj, data.getMessageId(), null, propertyValues, false, conn);
									cancelPendingPropertyValueListUpdates(deviceInfoObj, conn);
								}
								try {
									// Mark complete any pending "Reinitialize" commands
									params.clear();
									params.put("deviceName", initInfo.deviceName);
									params.put("actionCode", GenericResponseServerActionCode.REINITIALIZE);
									DataLayerMgr.executeCall(conn, "UPDATE_CB_PENDING_COMMANDS_SUCCESS", params);
								} catch(SQLException e) {
									log.info("Could not mark CB - Initialize command as complete", e);
								} catch(DataLayerException e) {
									log.info("Could not mark CB - Initialize command as complete", e);
								}
								break;
							case DATA_CORRUPTION:
							case UNDEFINED:
								if(message != null) {
									AppLayerUtils.populatePropertyValueList(deviceInfoObj, data.getMessageId(), null, propertyValues, true, conn);
									cancelPendingPropertyValueListUpdates(deviceInfoObj, conn);
								}
								break;
						}
						break;
					default:
						break;
				}

				Map<Integer, String> propertyMap = new HashMap<Integer, String>();
				if(message != null) {
		            if(deviceInfoObj.getEncryptionKey() != null) {
		            	propertyMap.put(DeviceProperty.ENCRYPTION_KEY.getValue(), StringUtils.toHex(deviceInfoObj.getEncryptionKey()));
		            }
					propertyMap.put(DeviceProperty.DEVICE_GUID.getValue(), deviceInfoObj.getDeviceName());
		
					long currentTime = System.currentTimeMillis();
					long currentSeconds = currentTime / 1000;
					propertyMap.put(DeviceProperty.UTC_TIME.getValue(), String.valueOf(currentSeconds));
					propertyMap.put(DeviceProperty.UTC_OFFSET.getValue(), String.valueOf(TimeZone.getTimeZone(deviceInfoObj.getTimeZoneGuid()).getOffset(currentTime) / 1000 / 60 / 15));
					propertyMap.put(DeviceProperty.MASTER_ID.getValue(), String.valueOf(Math.max(currentSeconds, deviceInfoObj.getMasterId())));
					propertyMap.put(DeviceProperty.MESSAGE_ID.getValue(), String.valueOf(Math.max(data.getMessageId(), currentSeconds)));
					propertyMap.put(DeviceProperty.ACTIVATION_STATUS.getValue(), String.valueOf(deviceInfoObj.getActivationStatus().getValue()));
					message.setSessionAttribute("time-update-sent", true);

					if(initInfo.isNew) {
						params.clear();
						params.put("deviceId", initInfo.deviceId);
						params.put("deviceTypeId", deviceInfoObj.getDeviceType().getValue());
						params.put("deviceSerialCd", deviceInfoObj.getDeviceSerialCd());
						params.put("deviceTimeZoneGuid", deviceInfoObj.getTimeZoneGuid());
						DataLayerMgr.executeCall(conn, "NORMALIZE_CALL_IN_TIME", params);
						if (ConvertUtils.getInt(params.get("resultCd")) == 1) {
							String activatedCallInSchedule = ConvertUtils.getString(params.get("activatedCallInSchedule"), true);
							if (!activatedCallInSchedule.equals(0x00))
								propertyMap.put(DeviceProperty.ACTIVATED_CALL_IN_SCHEDULE.getValue(), activatedCallInSchedule);
							String nonActivatedCallInSchedule = ConvertUtils.getString(params.get("nonActivatedCallInSchedule"), true);
							if (!nonActivatedCallInSchedule.equals(0x00))
								propertyMap.put(DeviceProperty.NON_ACTIVATED_CALL_IN_SCHEDULE.getValue(), nonActivatedCallInSchedule);
							String settlementSchedule = ConvertUtils.getString(params.get("settlementSchedule"), true);
							if (!settlementSchedule.equals(0x00))
								propertyMap.put(DeviceProperty.SETTLEMENT_SCHEDULE.getValue(), settlementSchedule);
							String dexSchedule = ConvertUtils.getString(params.get("dexSchedule"), true);
							if (!dexSchedule.equals(0x00))
								propertyMap.put(DeviceProperty.DEX_SCHEDULE.getValue(), dexSchedule);
						}
					}
		
					// Add a pending command of properties
					if(!propertyValues.isEmpty()) {
						HeapBufferStream bufferStream = AppLayerUtils.constructPropertyValueStream(deviceInfoObj, propertyValues);
						addPendingPropertyListUpdate(message, deviceInfoObj, bufferStream, conn, defaultFilePacketSize);
		            }
		
					deviceInfoObj.commitChanges(message.getServerTime());
	            }
				conn.commit();
				okay = true;
				if (message != null)
					MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.OKAY, null, propertyMap);
				log.info("Successfully initialized device deviceSerialCd: " + data.getDeviceSerialNum());
			} finally {
				try {
					if(!okay)
						conn.rollback();
				} finally {
					ProcessingUtils.closeDbConnection(log, conn);
				}
            }
		} catch(SQLException e) {
			if(DatabasePrerequisite.determineRetryType(e) == WorkRetryType.BLOCKING_RETRY)
				throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
			log.error("Failed to initialize device deviceSerialCd: " + data.getDeviceSerialNum(), e);
			if(message != null)
				MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.DENIED, null, GenericResponseServerActionCode.NO_ACTION); // XXX: What actionCode should be used?
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(Exception e) {
			log.error("Failed to initialize device deviceSerialCd: " + data.getDeviceSerialNum(), e);
            if (message != null)
            	MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.DENIED, null, GenericResponseServerActionCode.NO_ACTION); //XXX: What actionCode should be used?
        }

		return MessageResponse.CONTINUE_SESSION;
    }

	protected static boolean cancelPendingPropertyValueListUpdates(DeviceInfo deviceInfoObj, Connection conn) {
		try {
			int rows = DataLayerMgr.executeUpdate(conn, "CANCEL_PENDING_PROP_LIST_UPDATES", deviceInfoObj);
			log.info("Canceled " + rows + " pending property list updates");
			return true;
		} catch(SQLException e) {
			log.info("Could not cancel pending property list updates", e);
			return false;
		} catch(DataLayerException e) {
			log.info("Could not cancel pending property list updates", e);
			return false;
		}
	}

	protected static void addPendingPropertyListUpdate(Message message, DeviceInfo deviceInfoObj, HeapBufferStream bufferStream, Connection conn,
 int defaultFilePacketSize) throws SQLException, DataLayerException {
    	//Add update property list command to pending command list
    	int filePacketSize = defaultFilePacketSize;
		String text = message.getTranslator().translate("client.message.updating-properties", "Updating properties...");
    	if(bufferStream.size() > defaultFilePacketSize) {
			Calendar fileCreateTime = Calendar.getInstance();
			String fileName = deviceInfoObj.getDeviceName() + "-CFG-partial-" + ConvertUtils.formatObject(fileCreateTime.getTime(), "DATE:yyyy-MM-dd_HH-mm-ss.SSS");
			AppLayerUtils.addPendingFileTransfer(19, fileName, bufferStream.getInputStream(), bufferStream.size(), deviceInfoObj.getDeviceName(), filePacketSize, 1, conn);
		} else if(bufferStream.size() > 0) {
			ByteBuffer writeBuffer = ByteBuffer.allocate(bufferStream.size() + 6 + text.length()); // XXX: if charset uses more than 1 byte per char, we will need to change this calculation
        	writeBuffer.put(GenericResponseResultCode.OKAY.getValue());
            ProcessingUtils.writeShortString(writeBuffer, text, deviceInfoObj.getDeviceCharset());
            writeBuffer.put(GenericResponseServerActionCode.PROCESS_PROP_LIST.getValue());
            ProcessingUtils.writeShortInt(writeBuffer, bufferStream.size());
            if(bufferStream.putInto(writeBuffer, 0) != bufferStream.size())
            	throw new BufferOverflowException();
            writeBuffer.flip();

            Map<String,Object> params = new HashMap<String,Object>();
            params.put("deviceName", deviceInfoObj.getDeviceName());
			params.put("dataType", MessageType.GENERIC_RESPONSE_4_1.getHex());
			params.put("command", writeBuffer);
			params.put("executeOrder", 1);
			try {
				DataLayerMgr.executeUpdate(conn, "ADD_PENDING_COMMAND", params);
			} catch(SQLException e) {
				message.getLog().warn("Could not register a pending command to have device '" + deviceInfoObj.getDeviceName() + "' send an update status request", e);
			} catch(DataLayerException e) {
				message.getLog().warn("Could not register a pending command to have device '" + deviceInfoObj.getDeviceName() + "' send an update status request", e);
			}
		}
    	if(deviceInfoObj.getActivationStatus() == ActivationStatus.ACTIVATED) {
    		deviceInfoObj.setActionCode(ServerActionCode.PERFORM_CALL_IN);
    	}
	}

	public static Long getOrCreateAlertForEvent(Log log, Connection conn, Map<String, Object> params, long eventId, long eventUtcTime) throws SQLException, DataLayerException, ConvertException, ServiceException {
		params.clear();
		params.put("eventId", eventId);
		DataLayerMgr.executeCall(conn, "ADD_EVENT_ALERT", params);
		Long terminalAlertId = ConvertUtils.convert(Long.class, params.get("terminalAlertId"));
		boolean ignore = ConvertUtils.getBoolean(params.get("ignore"), false);
		if(!ignore && terminalAlertId != null) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep requestStep = mc.addStep(getRequestQueuePrefix() + 6 /*ReportScheduleType.EVENT.getValue()*/);
			requestStep.setAttribute(AlertingAttrEnum.AL_SERIAL_NUM, params.get("deviceSerialCd"));
			requestStep.setAttribute(AlertingAttrEnum.AL_TERMINAL, params.get("terminalId"));
			requestStep.setAttribute(AlertingAttrEnum.AL_EVENT_ID, eventId);
			requestStep.setAttribute(AlertingAttrEnum.AL_EVENT_SOURCE_TYPE, AlertSourceCode.DEVICE);
			requestStep.setAttribute(AlertingAttrEnum.AL_NOTIFICATION_TS, eventUtcTime);
			MessageChainService.publish(mc, getReportingPublisher());
		}
		return terminalAlertId;
	}

	public static boolean checkDevicePassword(DeviceInfo deviceInfo, CredentialResult credentialResult, String username, String password, long credentialMaxAgeMs, long newCredentialMaxAgeMs, Connection conn, WSResponseMessageData reply) throws SQLException, DataLayerException, ServiceException, GeneralSecurityException, ConvertException, UnsupportedEncodingException {
		if(!StringUtils.isBlank(password) || credentialResult == CredentialResult.TMP_PASSWORD_MATCHED) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("deviceName", deviceInfo.getDeviceName());
			Results result = DataLayerMgr.executeQuery(conn, "GET_DEVICE_CREDENTIAL_INFO", params);
			if(result.next()) {
				long credentialUtcTsMs = result.getValue("credentialUtcTsMs", long.class);
				byte[] newPasswordHash = result.getValue("newPasswordHash", byte[].class);
				byte[] newPasswordSalt = result.getValue("newPasswordSalt", byte[].class);
				byte[] currentPasswordHash = result.getValue("currentPasswordHash", byte[].class);
				byte[] currentPasswordSalt = result.getValue("currentPasswordSalt", byte[].class);
				long lastUpdateTime = result.getValue("lastUpdateTime", long.class);
				long currentUtcTsMs = System.currentTimeMillis();
				if(credentialResult == null) {
					if(currentPasswordHash != null && hashMatches(password, currentPasswordHash, currentPasswordSalt))
						credentialResult = CredentialResult.CURRENT_PASSWORD_MATCHED;
					else if(newPasswordHash != null && hashMatches(password, newPasswordHash, newPasswordSalt))
						credentialResult = CredentialResult.NEW_PASSWORD_MATCHED;
					else
						credentialResult = CredentialResult.TMP_PASSWORD_MATCHED;
				} else {
					switch(credentialResult) {
						case NO_MATCH:
						case INTERNAL:
							return false;
						case CURRENT_PASSWORD_MATCHED:
							if(currentPasswordHash == null || !hashMatches(password, currentPasswordHash, currentPasswordSalt))
								return false; // something changed; don't do anything to be safe
							break;
						case NEW_PASSWORD_MATCHED:
							if(newPasswordHash == null || !hashMatches(password, newPasswordHash, newPasswordSalt))
								return false; // something changed; don't do anything to be safe
							break;
					}
				}
				boolean sendNewPassword;
				switch(credentialResult) {
					case TMP_PASSWORD_MATCHED:
						sendNewPassword = true;
						log.info("Create new password because temporary passcode was used to authenticate");
						break;
					case CURRENT_PASSWORD_MATCHED:
						sendNewPassword = (currentUtcTsMs > credentialUtcTsMs + credentialMaxAgeMs);
						log.info("Create new password because credentials are expired (current credential time = " + credentialUtcTsMs + ")");
						break;
					case NEW_PASSWORD_MATCHED:
					default:
						return false; // new password was used - don't do anything (update of current password to new password is done in SessionControl)
				}
				if(sendNewPassword) {
					// create new password
					String generatedPassword = SecurityUtils.getRandomPassword(20);
					byte[] generatedSalt = SecureHash.getSalt();
					byte[] generatedHash = SecureHash.getHash(generatedPassword, generatedSalt);
					params.put("newUsername", username);
					params.put("newPasswordHash", generatedHash);
					params.put("newPasswordSalt", generatedSalt);
					params.put("lastUpdateTime", lastUpdateTime);
					DataLayerMgr.executeUpdate(conn, "CHANGE_DEVICE_PASSWORD", params);
					if('Y' == ConvertUtils.getChar(params.get("updated"), 'N')) {
						reply.setNewPassword(generatedPassword);
						deviceInfo.setNewUsername(username);
						deviceInfo.setNewPasswordHash(generatedHash);
						deviceInfo.setNewPasswordSalt(generatedSalt);
						deviceInfo.commitChanges(currentUtcTsMs);
						log.info("Created and sent new password of " + generatedPassword.length() + " characters (and 8-bit hash code of " + (generatedPassword.hashCode() & 0xFF) + ") to device " + deviceInfo.getDeviceName());
						log.info("Password Hash=" + StringUtils.toHex(generatedHash));
						return true;
					}
					log.info("Did not change password because expected time did not match last updated");
				}
			}
		}
		return false;
	}

	protected static boolean hashMatches(String password, byte[] hash, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] check = SecureHash.getHash(password, salt);
		return Arrays.equals(check, hash);
	}
	public static String getRequestQueuePrefix() {
		return requestQueuePrefix;
	}

	public static void setRequestQueuePrefix(String requestQueuePrefix) {
		AppLayerUtils.requestQueuePrefix = requestQueuePrefix;
	}

	public static Publisher<ByteInput> getReportingPublisher() {
		return reportingPublisher;
	}

	public static void setReportingPublisher(Publisher<ByteInput> reportingPublisher) {
		AppLayerUtils.reportingPublisher = reportingPublisher;
	}

	public static Map<String, String> getComponentInformationToSetting() {
		return componentInformationToSetting;
	}
}
