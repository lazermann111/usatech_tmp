/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.layers.common.model;

import java.io.Serializable;

/**
 * Model class represents the Location.
 */

public class Location implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String county;
	private String country;
    private String postalCode;
    private Long parentId;
    private String countryCode;
    private Integer typeId;
    private String typeDesc;
	private String stateCode;
    private String timeZoneCode;
    private String activeFlag;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the address1
     */
    public String getAddress1()
    {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2()
    {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country)
    {
        this.country = country;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode()
    {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode()
    {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    /**
     * @return the stateCode
     */
    public String getStateCode()
    {
        return stateCode;
    }

    /**
     * @param stateCode the stateCode to set
     */
    public void setStateCode(String stateCode)
    {
        this.stateCode = stateCode;
    }

    /**
     * @return the typeId
     */
    public Integer getTypeId()
    {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(Integer typeId)
    {
        this.typeId = typeId;
    }

    /**
     * @return the activeFlag
     */
    public String getActiveFlag()
    {
        return activeFlag;
    }

    /**
     * @param activeFlag the activeFlag to set
     */
    public void setActiveFlag(String activeFlag)
    {
        this.activeFlag = activeFlag;
    }

    /**
     * @return the parentId
     */
    public Long getParentId()
    {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    /**
     * @return the timeZoneCode
     */
    public String getTimeZoneCode()
    {
        return timeZoneCode;
    }

    /**
     * @param timeZoneCode the timeZoneCode to set
     */
    public void setTimeZoneCode(String timeZoneCode)
    {
        this.timeZoneCode = timeZoneCode;
    }
    
    /**
     * 
     * @return
     */
    public String getTypeDesc() {
		return typeDesc;
	}
    
    /**
     * 
     * @param typeDesc
     */
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCounty() {
		return county;
	}
	
	/**
	 * 
	 * @param county
	 */
	public void setCounty(String county) {
		this.county = county;
	}
}
