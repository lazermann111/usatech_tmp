/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.layers.common.model;

import java.io.Serializable;
import java.util.List;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.PosEnvironment;

/**
 * Model class represents the Device.
 */
public class Device implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private long id = -1;
    private int typeId = -1;
    private String deviceName = "";
    private String serialNumber = "";
    private String configFileName = "";
    private String typeDesc = "";
    private String commMethodCd = "";
	private String customerName = "";
    private String locationName = "";
    private String createdTs = "";
    private String lastUpdatedTs = "";
    private String lastActivityTs = "";
    private float lastActivityDays = -1;
    private String lastActivityColor = "green";
    private String commMethodName = "";
    private long credentialId;
    private String lastPasswordGeneratedTs = "";
    private String lastPasswordUpdatedTs = "";
    private int subTypeId = -1;
    private String subTypeDesc = "";
    private String rejectUntilTs = "";
    private String rejectUntilColor = "green";
    private int deviceRecordCount = 0;
	private PosEnvironment posEnvironment = PosEnvironment.UNSPECIFIED;
	private String entryCapability = "";
	private int deviceUtcOffsetMin;
		private String lastClientIPAddress = "";
		private String lastSaleTs = "";

    private boolean enabled = true;
    private long posId = -1;

    private int protocolRevision = 0;
    private int propertyListVersion = 0;

    private Location location = null;
    private Customer customer = null;
    private List<PaymentConfig> paymentConfigList = null;
    
    private String restrictFirmwareUpdate;
    private String firmwareRestrictionReason;
    private String modemStatusDesc;
    private String modemStatusTs;
    private String restrictSettingsUpdate;
    private String settingsRestrictionReason;
    
    /**
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * @return the typeId
     */
    public int getTypeId()
    {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(int typeId)
    {
        this.typeId = typeId;
    }

    /**
     * @return the deviceName
     */
    public String getDeviceName()
    {
        return deviceName;
    }

    /**
     * @param deviceName the deviceName to set
     */
    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
        if (deviceName != null)
        	configFileName = deviceName.concat("-CFG");
    }

    /**
     * @return the serialNumber
     */
    public String getSerialNumber()
    {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber)
    {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the typeDesc
     */
    public String getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * @param typeDesc the typeDesc to set
     */
    public void setTypeDesc(String typeDesc)
    {
        this.typeDesc = typeDesc;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName()
    {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    /**
     * @return the locationName
     */
    public String getLocationName()
    {
        return locationName;
    }

    /**
     * @param locationName the locationName to set
     */
    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    /**
     * @return the lastActivityTs
     */
    public String getLastActivityTs()
    {
        return lastActivityTs;
    }

    /**
     * @param lastActivityTs the lastActivityTs to set
     */
    public void setLastActivityTs(String lastActivityTs)
    {
        this.lastActivityTs = lastActivityTs;
    }

    /**
     * @return the configFileName
     */
    public String getConfigFileName()
    {
        return configFileName;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled()
    {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    /**
     * @return the lastUpdatedTs
     */
    public String getLastUpdatedTs()
    {
        return lastUpdatedTs;
    }

    /**
     * @param lastUpdatedTs the lastUpdatedTs to set
     */
    public void setLastUpdatedTs(String lastUpdatedTs)
    {
        this.lastUpdatedTs = lastUpdatedTs;
    }

    /**
     * @return the createdTs
     */
    public String getCreatedTs()
    {
        return createdTs;
    }

    /**
     * @param createdTs the createdTs to set
     */
    public void setCreatedTs(String createdTs)
    {
        this.createdTs = createdTs;
    }

    /**
     * @return <code>true</code> if this is a G4 device
     */
    public boolean isG4()
    {
        return DeviceType.G4.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a G5 device
     */
    public boolean isGX()
    {
        return DeviceType.GX.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a BRICK device
     */
    public boolean isNG()
    {
        return DeviceType.NG.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a LEGACY_KIOSK device
     */
    public boolean isLegacyKIOSK()
    {
        return DeviceType.LEGACY_KIOSK.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a ESUDS device
     */
    public boolean isESUDS()
    {
        return DeviceType.ESUDS.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a MEI_TELEMETER device
     */
    public boolean isMEI()
    {
        return DeviceType.MEI.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a EZ80_EPORT_DEVELOPMENT device
     */
    public boolean isEZ80Dev()
    {
        return DeviceType.EZ80_DEVELOPMENT.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a EZ80_EPORT_PRODUCTION device
     */
    public boolean isEZ80Prod()
    {
        return DeviceType.EZ80.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a LEGACY_G4 device
     */
    public boolean isLegacyG4()
    {
        return DeviceType.LEGACY_G4.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a TRANSACT device
     */
    public boolean isTRANSACT()
    {
        return DeviceType.TRANSACT.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a KIOSK device
     */
    public boolean isKIOSK()
    {
        return DeviceType.KIOSK.getValue() == typeId;
    }
    
    public boolean isKIOSKWebService()
    {
        return DeviceType.KIOSK.getValue() == typeId && subTypeId == 3;
    }

    /**
     * @return <code>true</code> if this is a T2 device
     */
    public boolean isT2()
    {
        return DeviceType.T2.getValue() == typeId;
    }

    /**
     * @return <code>true</code> if this is a EDGE device
     */
    public boolean isEDGE()
    {
        return DeviceType.EDGE.getValue() == typeId;
    }

    /**
     * @return the showDisabled
     */
    public boolean isShowDisabled()
    {
        return !enabled;
    }

    /**
     * @return the protocolRevision
     */
    public int getProtocolRevision()
    {
        return protocolRevision;
    }

    /**
     * @param protocolRevision the protocolRevision to set
     */
    public void setProtocolRevision(int protocolRevision)
    {
        this.protocolRevision = protocolRevision;
    }

    /**
     * @return the propertyListVersion
     */
    public int getPropertyListVersion()
    {
        return propertyListVersion;
    }

    /**
     * @param propertyListVersion the propertyListVersion to set
     */
    public void setPropertyListVersion(int propertyListVersion)
    {
        this.propertyListVersion = propertyListVersion;
    }

    /**
     * @return the posId
     */
    public long getPosId()
    {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(long posId)
    {
        this.posId = posId;
    }

    /**
     * @return the location
     */
    public Location getLocation()
    {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location)
    {
        this.location = location;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer()
    {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }

    /**
     * @return the paymentConfigList
     */
    public List<PaymentConfig> getPaymentConfigList()
    {
        return paymentConfigList;
    }

    /**
     * @param paymentConfigList the paymentConfigList to set
     */
    public void setPaymentConfigList(List<PaymentConfig> paymentConfigList)
    {
        this.paymentConfigList = paymentConfigList;
    }
    
    public double getLastActivityDays() {
		return lastActivityDays;
	}

	public void setLastActivityDays(float lastActivityDays) {
		this.lastActivityDays = lastActivityDays;
		if (lastActivityDays <= 2)
			lastActivityColor = "green";
		else if (lastActivityDays <= 5)
			lastActivityColor = "orange";
		else
			lastActivityColor = "red";
	}
	
	public String getLastActivityColor() {
		return lastActivityColor;
	}
	
    public String getCommMethodCd() {
		return commMethodCd;
	}

	public void setCommMethodCd(String commMethodCd) {
		this.commMethodCd = commMethodCd;
	}
	
	public boolean hasCounters() {
		return isEDGE() || isGX() || isG4() || isMEI() || isKIOSK();
	}
	
	public boolean hasEvents() {
		return isEDGE() || isGX() || isG4() || isMEI() || isKIOSK();
	}

	public String getCommMethodName() {
		return commMethodName;
	}

	public void setCommMethodName(String commMethodName) {
		this.commMethodName = commMethodName;
	}

	public long getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(long credentialId) {
		this.credentialId = credentialId;
	}

	public String getLastPasswordGeneratedTs() {
		return lastPasswordGeneratedTs;
	}

	public void setLastPasswordGeneratedTs(String lastPasswordGeneratedTs) {
		this.lastPasswordGeneratedTs = lastPasswordGeneratedTs;
	}

	public String getLastPasswordUpdatedTs() {
		return lastPasswordUpdatedTs;
	}

	public void setLastPasswordUpdatedTs(String lastPasswordUpdatedTs) {
		this.lastPasswordUpdatedTs = lastPasswordUpdatedTs;
	}

	public int getSubTypeId() {
		return subTypeId;
	}

	public void setSubTypeId(int subTypeId) {
		this.subTypeId = subTypeId;
	}
	
	public String getSubTypeDesc() {
		return subTypeDesc;
	}

	public void setSubTypeDesc(String subTypeDesc) {
		this.subTypeDesc = subTypeDesc;
	}

	public String getRejectUntilTs() {
		return rejectUntilTs;
	}

	public void setRejectUntilTs(String rejectUntilTs) {
		this.rejectUntilTs = rejectUntilTs;
	}

	public String getRejectUntilColor() {
		return rejectUntilColor;
	}

	public void setRejectUntilColor(String rejectUntilColor) {
		this.rejectUntilColor = rejectUntilColor;
	}

	public int getDeviceRecordCount() {
		return deviceRecordCount;
	}

	public void setDeviceRecordCount(int deviceRecordCount) {
		this.deviceRecordCount = deviceRecordCount;
	}

	public PosEnvironment getPosEnvironment() {
		return posEnvironment;
	}

	public void setPosEnvironment(PosEnvironment posEnvironment) {
		this.posEnvironment = posEnvironment;
	}

	public String getEntryCapability() {
		return entryCapability;
	}

	public void setEntryCapability(String entryCapability) {
		this.entryCapability = entryCapability;
	}
	
	public int getDeviceUtcOffsetMin() {
		return deviceUtcOffsetMin;
	}

	public void setDeviceUtcOffsetMin(int deviceUtcOffsetMin) {
		this.deviceUtcOffsetMin = deviceUtcOffsetMin;
	}

	public String getLastClientIPAddress() {
		return lastClientIPAddress;
	}

	public void setLastClientIPAddress(String lastClientIPAddress) {
		this.lastClientIPAddress = lastClientIPAddress;
	}

	public String getLastSaleTs() {
		return lastSaleTs;
	}

	public void setLastSaleTs(String lastSaleTs) {
		this.lastSaleTs = lastSaleTs;
	}

	public String getRestrictFirmwareUpdate() {
		return restrictFirmwareUpdate;
	}

	public void setRestrictFirmwareUpdate(String restrictFirmwareUpdate) {
		this.restrictFirmwareUpdate = restrictFirmwareUpdate;
	}

	public String getFirmwareRestrictionReason() {
		return firmwareRestrictionReason;
	}

	public void setFirmwareRestrictionReason(String firmwareRestrictionReason) {
		this.firmwareRestrictionReason = firmwareRestrictionReason;
	}

	public String getRestrictSettingsUpdate() {
		return restrictSettingsUpdate;
	}

	public void setRestrictSettingsUpdate(String restrictSettingsUpdate) {
		this.restrictSettingsUpdate = restrictSettingsUpdate;
	}

	public String getSettingsRestrictionReason() {
		return settingsRestrictionReason;
	}

	public void setSettingsRestrictionReason(String settingsRestrictionReason) {
		this.settingsRestrictionReason = settingsRestrictionReason;
	}
	
	public String getModemStatusDesc() {
		return modemStatusDesc;
	}

	public void setModemStatusDesc(String modemStatusDesc) {
		this.modemStatusDesc = modemStatusDesc;
	}

	public String getModemStatusTs() {
		return modemStatusTs;
	}

	public void setModemStatusTs(String modemStatusTs) {
		this.modemStatusTs = modemStatusTs;
	}
	
}
