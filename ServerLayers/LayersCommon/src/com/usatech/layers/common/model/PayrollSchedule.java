/**

 * Copyright 2016 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.layers.common.model;

import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.bean.MapDynaBean;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.text.StringUtils;

public class PayrollSchedule {
	
	public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	
	public enum PayrollScheduleType
	{
		NONE("", 0),
		WEEKLY_ON_DAY_OF_WEEK("Weekly", 1),
		BIWEEKLY_ON_DAY_OF_WEEK("Biweekly", 1),
		SEMIMONTHLY_ON_DAYS_OF_MONTH("Semi-Monthly",  2),
		SEMIMONTHLY_ON_PRIOR_WEEKDAY("Semi-Monthly on closest prior weekday", 2),
		SEMIMONTHLY_ON_NEXT_WEEKDAY("Semi-Monthly on closest next weekday", 2),
		MONTHLY_ON_DAY_OF_MONTH("Monthly", 1),
		MONTHLY_ON_PRIOR_WEEKDAY("Monthly on closest prior weekday", 1),
		MONTHLY_ON_NEXT_WEEKDAY("Monthly on closest next weekday", 1);
		
		private String desc;
		private int days;
		
		private PayrollScheduleType(String desc, int days) {
			this.desc = desc;
			this.days = days;
		}
		
		public String getDesc() {
			return desc;
		}
		
		public int getDays() {
			return days;
		}
		
		public boolean isMultiDate() {
			return days > 1;
		}
	}
	
	public static class PayPeriod {
		
		protected Date startDate;
		protected Date endDate;
		
		protected PayPeriod(Date startDate, Date endDate) {
			this.startDate = startDate;
			this.endDate = endDate;
		}
		
		public Date getStartDate() {
			return startDate;
		}
		
		public Date getEndDate() {
			return endDate;
		}
		
		public String toString() {
			return startDate + " - " + endDate;
		}
	}

	public static PayrollSchedule create(PayrollScheduleType payrollScheduleType, LocalDate... samplePayDates) {
		Objects.requireNonNull(payrollScheduleType, "PayrollScheduleType");
		if (payrollScheduleType.getDays() > 0 && (payrollScheduleType == null || samplePayDates == null || payrollScheduleType.getDays() > samplePayDates.length))
			throw new NullPointerException("Sample pay dates are required");

		return PayrollSchedule.create(payrollScheduleType, Arrays.stream(samplePayDates).collect(Collectors.toList()));
	}

	public static PayrollSchedule create(PayrollScheduleType payrollScheduleType, List<LocalDate> samplePayDates) {
		Objects.requireNonNull(payrollScheduleType, "PayrollScheduleType");
		if (payrollScheduleType.getDays() > 0 && (payrollScheduleType == null || samplePayDates == null || payrollScheduleType.getDays() > samplePayDates.size()))
			throw new NullPointerException("Sample pay dates are required");

		PayrollScheduler scheduler = null;
		switch(payrollScheduleType) {
			case WEEKLY_ON_DAY_OF_WEEK:
				scheduler = new WeeklyPayScheduler(samplePayDates.get(0));
				break;
			case BIWEEKLY_ON_DAY_OF_WEEK:
				scheduler = new BiWeeklyPayScheduler(samplePayDates.get(0));
				break;
			case MONTHLY_ON_DAY_OF_MONTH:
			case SEMIMONTHLY_ON_DAYS_OF_MONTH:
				scheduler = new DaysOfMonthPayScheduler(samplePayDates);
				break;
			case MONTHLY_ON_PRIOR_WEEKDAY:
			case SEMIMONTHLY_ON_PRIOR_WEEKDAY:
				scheduler = new PriorWeekdayDaysOfMonthPayScheduler(samplePayDates);
				break;
			case MONTHLY_ON_NEXT_WEEKDAY:
			case SEMIMONTHLY_ON_NEXT_WEEKDAY:
				scheduler = new NextWeekdayDaysOfMonthPayScheduler(samplePayDates);
				break;
			case NONE:
				scheduler = new NullPayScheduler();
				break;
			default:
				throw new IllegalArgumentException("Unhandled PayrollScheduleType: " + payrollScheduleType);
		}
		
		return new PayrollSchedule(payrollScheduleType, scheduler);
	}
	
	public static PayrollSchedule loadFrom(String payrollScheduleType, String payrollScheduleData) throws ConvertException {
		if (StringUtils.isBlank(payrollScheduleType))
			return null;
		
		Map<String, Object> map = new HashMap<>();
		map.put("payrollScheduleType", payrollScheduleType);
		map.put("payrollScheduleData", payrollScheduleData);
		
		return PayrollSchedule.loadFrom(new MapDynaBean(map));
	}
	
	public static PayrollSchedule loadFrom(DynaBean bean) throws ConvertException {
		if (bean == null)
			return null;
		String payrollScheduleTypeStr = ConvertUtils.getStringSafely(bean.get("payrollScheduleType"));
		if (StringUtils.isBlank(payrollScheduleTypeStr))
			return null;
		
		PayrollScheduleType payrollScheduleType = PayrollScheduleType.valueOf(payrollScheduleTypeStr);
		if (payrollScheduleType == PayrollScheduleType.NONE)
			return PayrollSchedule.create(payrollScheduleType);
		
		List<LocalDate> payDates = new ArrayList<>();
		
		String payDay1Str = ConvertUtils.getStringSafely(bean.get("payDay1"));
		if (!StringUtils.isBlank(payDay1Str)) {
			payDates.add(LocalDate.parse(payDay1Str, DATE_FORMAT));
		}
			
		String payDay2Str = ConvertUtils.getStringSafely(bean.get("payDay2"));
		if (payrollScheduleType.isMultiDate() && !StringUtils.isBlank(payDay2Str)) {
			payDates.add(LocalDate.parse(payDay2Str, DATE_FORMAT));
		}
		
		if (payDates.isEmpty()) {
			String payrollScheduleDataStr = ConvertUtils.getStringSafely(bean.get("payrollScheduleData"));
			if (payrollScheduleDataStr == null)
				return null;
			payDates = Arrays.stream(StringUtils.split(payrollScheduleDataStr, ',')).map(s -> LocalDate.parse(s, DATE_FORMAT)).collect(Collectors.toList());
		}
		
		return PayrollSchedule.create(payrollScheduleType, payDates);
	}

	public static PayrollSchedule loadFrom(Results results, InputForm form, boolean loadDummy) throws ConvertException {
		PayrollSchedule payrollSchedule = PayrollSchedule.loadFrom(form);
		if (payrollSchedule == null)
			payrollSchedule = PayrollSchedule.loadFrom(results);
		if (payrollSchedule == null && loadDummy)
			payrollSchedule = new PayrollSchedule(PayrollScheduleType.NONE, new NullPayScheduler());
		return payrollSchedule;
	}
	
	// TODO: add a method that takes a connection as a param
	public static PayrollSchedule loadByCustomerId(long customerId) throws SQLException, DataLayerException, ConvertException {
		Results results = DataLayerMgr.executeQuery("GET_CUSTOMER_PAY_PERIOD", new Object[] {customerId});
		if (!results.next())
			return null;
		return loadFrom(results);
	}

	// TODO: add a method that takes a connection as a param
	public static PayrollSchedule loadByUserId(long userId) throws SQLException, DataLayerException, ConvertException {
		Results results = DataLayerMgr.executeQuery("GET_CUSTOMER_PAY_PERIOD_BY_USER_ID", new Object[] {userId});
		if (!results.next())
			return null;
		return loadFrom(results);
	}

	protected PayrollScheduleType payrollScheduleType;
	protected PayrollScheduler scheduler;
	protected LocalDate currentDate;
	
	public PayrollSchedule(PayrollScheduleType payrollScheduleType, PayrollScheduler scheduler) {
		this.payrollScheduleType = payrollScheduleType;
		this.scheduler = scheduler;
	}
	
	public PayPeriod calcCurrentPayPeriod(LocalDate fromDate) {
		if (fromDate == null)
			fromDate = LocalDate.now();
		
		LocalDate prevLocalDate = calcPrevPayrollDate(fromDate);
		Date startDate = Date.from(prevLocalDate.plus(Period.ofDays(1)).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
		
		LocalDate nextLocalDate = scheduler.calcNextPayrollDate(fromDate);
		Date endDate = Date.from(nextLocalDate.plus(Period.ofDays(1)).atStartOfDay().minusSeconds(1).atZone(ZoneId.systemDefault()).toInstant());
		
		return new PayPeriod(startDate, endDate);
	}

	public PayPeriod getCurrentPayPeriod() {
		return calcCurrentPayPeriod(currentDate);
	}
	
	public PayPeriod getPrevPayPeriod() {
		LocalDate fromDate = currentDate;
		if (fromDate == null)
			fromDate = LocalDate.now();
		fromDate = calcPrevPayrollDate(fromDate).minus(Period.ofDays(1));
		return calcCurrentPayPeriod(fromDate);
	}

	public PayPeriod getNextPayPeriod() {
		LocalDate fromDate = currentDate;
		if (fromDate == null)
			fromDate = LocalDate.now();
		fromDate = scheduler.calcNextPayrollDate(fromDate).plus(Period.ofDays(1));
		return calcCurrentPayPeriod(fromDate);
	}

	public void setCurrentDate(LocalDate currentDate) {
		this.currentDate = currentDate;
	}
	
	public LocalDate getNextPayrollDate() {
		currentDate = scheduler.calcNextPayrollDate(currentDate);
		return currentDate;
	}

	public LocalDate getPrevPayrollDate() {
		currentDate = calcPrevPayrollDate(currentDate);
		return currentDate;
	}
	
	// TODO: improve this, this is not best way to do it
	// Basically I'm rolling the clock back 32 days, then asking for the next payroll
	// date until I get back to the next date, then the previous was is the one I'm looking for
	protected LocalDate calcPrevPayrollDate(LocalDate fromDate) {
		if (fromDate == null)
			fromDate = LocalDate.now();
		
		LocalDate targetDate = scheduler.calcNextPayrollDate(fromDate.minus(Period.ofDays(1)));
		LocalDate prevDate = targetDate.minus(Period.ofDays(32));
		LocalDate lastDate = prevDate;
		while(prevDate.isBefore(targetDate)) {
			lastDate = prevDate;
			prevDate = scheduler.calcNextPayrollDate(lastDate);
		}
		return lastDate;
	}
	
	public LocalDate getCurrentPayrollDate() {
		return currentDate;
	}
	
	public PayrollScheduleType getPayrollScheduleType() {
		return payrollScheduleType;
	}
	
	public String getPayrollScheduleData() {
		return scheduler.getStringRep();
	}
	
	public String getPayrollScheduleData(int index) {
		if (payrollScheduleType.isMultiDate()) {
			List<LocalDate> payDates = ((DaysOfMonthPayScheduler)scheduler).getStartDates();
			return payDates.get(index-1).format(DATE_FORMAT);
		} else if (index > 1) {
			return null;
		} else {
			return scheduler.getStringRep();
		}
	}
	
	public boolean isNone() {
		return payrollScheduleType == null || payrollScheduleType == PayrollScheduleType.NONE;
	}

	public void saveTo(InputForm form) {
		form.setAttribute("payrollScheduleType", payrollScheduleType.toString());
		form.setAttribute("payrollScheduleData", scheduler.getStringRep());

		switch(payrollScheduleType) {
			case WEEKLY_ON_DAY_OF_WEEK:
			case BIWEEKLY_ON_DAY_OF_WEEK:
			case MONTHLY_ON_DAY_OF_MONTH:
			case MONTHLY_ON_PRIOR_WEEKDAY:
			case MONTHLY_ON_NEXT_WEEKDAY:
				form.setAttribute("payDay1", scheduler.getStringRep());
				break;
			case SEMIMONTHLY_ON_DAYS_OF_MONTH:
			case SEMIMONTHLY_ON_PRIOR_WEEKDAY:
			case SEMIMONTHLY_ON_NEXT_WEEKDAY:
				List<LocalDate> payDates = ((DaysOfMonthPayScheduler)scheduler).getStartDates();
				for(int i=0; i<payDates.size(); i++) {
					form.setAttribute("payDay" + (i+1), payDates.get(i).format(DATE_FORMAT));
				}
				break;
			default:
				throw new IllegalArgumentException("Unhandled PayrollScheduleType: " + payrollScheduleType);
		}
	}
	
	
	public interface PayrollScheduler {
		public LocalDate calcNextPayrollDate(LocalDate fromDate);
		public String getStringRep();
	}

	public static class WeeklyPayScheduler implements PayrollScheduler {
		
		protected LocalDate startDate;
		
		protected WeeklyPayScheduler(LocalDate startDate) {
			this.startDate = startDate;
		}
		
		public String getStringRep() {
			return startDate.format(DATE_FORMAT);
		}

		@Override
		public LocalDate calcNextPayrollDate(LocalDate fromDate) {
			if (fromDate == null) {
				fromDate = LocalDate.now();
			}
			
			DayOfWeek dow = startDate.getDayOfWeek();
			
			if (fromDate.getDayOfWeek().getValue() < dow.getValue()) {
				fromDate = fromDate.minus(Period.ofWeeks(1)).with(dow);
			} else if (fromDate.getDayOfWeek().getValue() != dow.getValue()) {
				fromDate = fromDate.with(dow);
			}
			
			return fromDate.plus(Period.ofWeeks(1));
		}
		
	}
	
	public static class BiWeeklyPayScheduler implements PayrollScheduler {
		
		protected LocalDate startDate;
		
		protected BiWeeklyPayScheduler(LocalDate startDate) {
			this.startDate = startDate;
		}
		
		public String getStringRep() {
			return startDate.format(DATE_FORMAT);
		}

		private LocalDate rollForwardUntilAfter(LocalDate currentDate, LocalDate targetDate) {
			while(currentDate.compareTo(targetDate) < 0)
				currentDate = currentDate.plus(Period.ofWeeks(2));
			return currentDate;
		}
		
		private LocalDate rollBackwardUntilBefore(LocalDate currentDate, LocalDate targetDate) {
			while(currentDate.compareTo(targetDate) > 0)
				currentDate = currentDate.minus(Period.ofWeeks(2));
			return currentDate;
		}

		@Override
		public LocalDate calcNextPayrollDate(LocalDate fromDate) {
			if (fromDate == null) {
				fromDate = LocalDate.now();
			}
			
			LocalDate nextDate = startDate;
			nextDate = rollBackwardUntilBefore(nextDate, fromDate);
			nextDate = rollForwardUntilAfter(nextDate, fromDate);
			
			return nextDate;
		}
	}
	
	public static class DaysOfMonthPayScheduler implements PayrollScheduler {
		
		protected List<LocalDate> startDates;
		protected int[] daysOfMonth;
		
		protected DaysOfMonthPayScheduler(List<LocalDate> startDates) {
			this.startDates = startDates;
			daysOfMonth = startDates.stream().mapToInt(d -> d.getDayOfMonth()).sorted().toArray();
		}
		
		public int[] getDaysOfMonth() {
			return daysOfMonth;
		}
		
		public List<LocalDate> getStartDates() {
			return startDates;
		}
		
		public String getStringRep() {
			return startDates.stream().map(d -> d.format(DATE_FORMAT)).collect(Collectors.joining(","));
		}

		@Override
		public LocalDate calcNextPayrollDate(LocalDate fromDate) {
			if (fromDate == null)
				fromDate = LocalDate.now();
			
			int currentDayOfMonth = fromDate.getDayOfMonth();

			int nextDOM = -1;
			if (currentDayOfMonth < fromDate.lengthOfMonth()) { 
				for(int i=0; i<daysOfMonth.length; i++) {
					if (daysOfMonth[i] > currentDayOfMonth) {
						nextDOM = daysOfMonth[i];
						break;
					}
				}
			}
			
			if (nextDOM == -1) {
				fromDate = fromDate.plusMonths(1);
				nextDOM = daysOfMonth[0];
			}
			
			if (nextDOM > fromDate.lengthOfMonth()) {
				nextDOM =  fromDate.lengthOfMonth();
			}
			
			return fromDate.withDayOfMonth(nextDOM);
		}
		
	}
	
	public static class PriorWeekdayDaysOfMonthPayScheduler extends DaysOfMonthPayScheduler {
		
		protected PriorWeekdayDaysOfMonthPayScheduler(List<LocalDate> startDates) {
			super(startDates);
		}

		@Override
		public LocalDate calcNextPayrollDate(final LocalDate fromDate) {
			LocalDate nextDate = super.calcNextPayrollDate(fromDate);
			LocalDate origNextDate = nextDate;
			if (nextDate.getDayOfWeek().getValue() > 5)
				nextDate = nextDate.with(DayOfWeek.FRIDAY);
			if (nextDate.equals(fromDate)) {
				nextDate = super.calcNextPayrollDate(origNextDate);
				if (nextDate.getDayOfWeek().getValue() > 5)
					nextDate = nextDate.with(DayOfWeek.FRIDAY);
			}
			return nextDate;
		}
		
	}
	
	public static class NextWeekdayDaysOfMonthPayScheduler extends DaysOfMonthPayScheduler {
		
		protected NextWeekdayDaysOfMonthPayScheduler(List<LocalDate> startDates) {
			super(startDates);
		}

		@Override
		public LocalDate calcNextPayrollDate(LocalDate fromDate) {
			if (fromDate.getDayOfWeek().getValue() > 5 && Arrays.binarySearch(daysOfMonth, fromDate.getDayOfMonth()) >= 0) {
				fromDate = fromDate.minus(Period.ofDays(1));
			}
			LocalDate nextDate = super.calcNextPayrollDate(fromDate);
			LocalDate origNextDate = nextDate;
			if (nextDate.getDayOfWeek().getValue() > 5) {
				nextDate = nextDate.plusWeeks(1).with(DayOfWeek.MONDAY);
			}
			if (nextDate.equals(fromDate)) {
				nextDate = super.calcNextPayrollDate(origNextDate);
				if (nextDate.getDayOfWeek().getValue() > 5)
					nextDate = nextDate.plusWeeks(1).with(DayOfWeek.MONDAY);
			}
			return nextDate;
		}
		
	}
	
	public static class NullPayScheduler implements PayrollScheduler {

		@Override
		public LocalDate calcNextPayrollDate(LocalDate fromDate) {
			return null;
		}

		@Override
		public String getStringRep() {
			return "";
		}
		
	}

	@Override
	public String toString() {
		return String.format("PayrollSchedule[payrollScheduleType=%s, currentDate=%s]", payrollScheduleType, currentDate);
	}

}
