/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.layers.common.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Model class represents the CustomerBankTerminal.
 */
public class CustomerBankTerminal implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long customerBankId;
    private Date startDate;
    private Date endDate;
    private String bankAccountNumber;
    private String status;
    private String description;

    /**
     * @return the customerBankId
     */
    public Long getCustomerBankId()
    {
        return customerBankId;
    }

    /**
     * @param customerBankId the customerBankId to set
     */
    public void setCustomerBankId(Long customerBankId)
    {
        this.customerBankId = customerBankId;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * @return the bankAccountNumber
     */
    public String getBankAccountNumber()
    {
        return bankAccountNumber;
    }

    /**
     * @param bankAccountNumber the bankAccountNumber to set
     */
    public void setBankAccountNumber(String bankAccountNumber)
    {
        this.bankAccountNumber = bankAccountNumber;
    }

    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    public String getStatusLabel()
    {
        if (status != null)
        {
            if (status.equalsIgnoreCase("A"))
            {
                return "Active";
            }
            if (status.equalsIgnoreCase("P"))
            {
                return "Pending";
            }
            if (status.equalsIgnoreCase("D"))
            {
                return "Deactivated";
            }
            return status;
        }
        return "";
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

}
