/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.layers.common.model;

import java.math.BigDecimal;
import java.util.List;

import simple.util.NameValuePair;

/**
 * Model class represents a line of CSV configuration data, or a line of default config template setting detail.
 */
public class ConfigTemplateSetting
{
    private Long fileTransferId;
	private int counter;
    private String name; // device_setting_parameter_cd
    private String label; // label for client, derived from alt_name
    private int fieldOffset;
    private int fieldSize;
    private String align;
    private String categoryName;
    private String padChar;
    private String editor;
    private String editorType;
    private String clientSend;
    private String dataMode;
    private String display;
    private String description;
    private String choices;
    private String configTemplateSettingValue;
    private String allowChange;
    private int propertyListVersion;
    private String dataModeAux; // field used to signify that a param description indicates that this field is a specific data type, i.e. numeric only
    private int displayOrder;
    private int categoryId;
    private String categoryDisplay;
    private int categoryDisplayOrder;
    private String subtypeCategory;
    private int deviceTypeId;
    private boolean required;
    private boolean supported;
    private boolean serverOnly;
    private String customerEditable;
    private String storedInPennies;
    private String unsupportedDefaultValue;
    private BigDecimal minValue;
    private BigDecimal maxValue;

    private int fieldLength;
    private String eeromLocation;
    private String locationData;
    private String lineDesc;
    //Field to denote if this field should be a hidden field on the client.
    private String hideField;
    private String settingType;
    private String key;
    private String regex;
    
	private List<NameValuePair> options;
    private String number;
    private String pairStr;
    
    /**
     * Get Field to denote if this field should be a hidden field on the client.
     * @return
     */
    public String getHideField() {
		return hideField;
	}
    
    /**
     * Get Field to denote if this field should be a hidden field on the client.
     * @param hideField
     */
	public void setHideField(String hideField) {
		this.hideField = hideField;
	}

    
    /**
	 * @return the lineDesc
	 */
	public String getLineDesc() {
		return lineDesc;
	}

	/**
	 * @param lineDesc the lineDesc to set
	 */
	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}
    
    /**
	 * @return the fileTransferId
	 */
	public Long getFileTransferId() {
		return fileTransferId;
	}

	/**
	 * @param fileTransferId the fileTransferId to set
	 */
	public void setFileTransferId(Long fileTransferId) {
		this.fileTransferId = fileTransferId;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the fieldOffset
	 */
	public int getFieldOffset() {
		return fieldOffset;
	}

	/**
	 * @param fieldOffset the fieldOffset to set
	 */
	public void setFieldOffset(int fieldOffset) {
		this.fieldOffset = fieldOffset;
	}

	/**
	 * @return the fieldSize
	 */
	public int getFieldSize() {
		return fieldSize;
	}

	/**
	 * @param fieldSize the fieldSize to set
	 */
	public void setFieldSize(int fieldSize) {
		this.fieldSize = fieldSize;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the padChar
	 */
	public String getPadChar() {
		return padChar;
	}

	/**
	 * @param padChar the padChar to set
	 */
	public void setPadChar(String padChar) {
		this.padChar = padChar;
	}

	/**
	 * @return the configTemplateSettingValue
	 */
	public String getConfigTemplateSettingValue() {
		return configTemplateSettingValue;
	}

	/**
	 * @param configTemplateSettingValue the configTemplateSettingValue to set
	 */
	public void setConfigTemplateSettingValue(String configTemplateSettingValue) {
		this.configTemplateSettingValue = configTemplateSettingValue;
	}

	/**
	 * @return the propertyListVersion
	 */
	public int getPropertyListVersion() {
		return propertyListVersion;
	}

	/**
	 * @param propertyListVersion the propertyListVersion to set
	 */
	public void setPropertyListVersion(int propertyListVersion) {
		this.propertyListVersion = propertyListVersion;
	}

	/**
	 * @return the dataModeAux
	 */
	public String getDataModeAux() {
		return dataModeAux;
	}

	/**
	 * @param dataModeAux the dataModeAux to set
	 */
	public void setDataModeAux(String dataModeAux) {
		this.dataModeAux = dataModeAux;
	}

	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryDisplay() {
		return categoryDisplay;
	}

	public void setCategoryDisplay(String categoryDisplay) {
		this.categoryDisplay = categoryDisplay;
	}

	/**
	 * @return the categoryDisplayOrder
	 */
	public int getCategoryDisplayOrder() {
		return categoryDisplayOrder;
	}

	/**
	 * @param categoryDisplayOrder the categoryDisplayOrder to set
	 */
	public void setCategoryDisplayOrder(int categoryDisplayOrder) {
		this.categoryDisplayOrder = categoryDisplayOrder;
	}

	public String getSubtypeCategory() {
		return subtypeCategory;
	}

	public void setSubtypeCategory(String subtypeCategory) {
		this.subtypeCategory = subtypeCategory;
	}

	/**
	 * @return the deviceTypeId
	 */
	public int getDeviceTypeId() {
		return deviceTypeId;
	}

	public void setDeviceTypeId(int deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	
    public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isSupported() {
		return supported;
	}

	public void setSupported(boolean supported) {
		this.supported = supported;
	}

	public boolean isServerOnly() {
		return serverOnly;
	}

	public void setServerOnly(boolean serverOnly) {
		this.serverOnly = serverOnly;
	}

	/**
	 * @return the editor
	 */
	public String getEditor() {
		return editor;
	}

	/**
	 * @param editor the editor to set
	 */
	public void setEditor(String editor) {
		this.editor = editor;
		if (editor != null) {
			int pos = editor.indexOf(":");
			if (pos > -1)
				this.editorType = editor.substring(0, pos);
		}
	}

	public String getEditorType() {
		return editorType;
	}

	public void setEditorType(String editorType) {
		this.editorType = editorType;
	}

	/**
	 * @return the clientSend
	 */
	public String getClientSend() {
		return clientSend;
	}

	/**
	 * @param clientSend the clientSend to set
	 */
	public void setClientSend(String clientSend) {
		this.clientSend = clientSend;
	}

	/**
     * @return the counter
     */
    public int getCounter()
    {
        return counter;
    }

    /**
     * @param counter the counter to set
     */
    public void setCounter(int counter)
    {
        this.counter = counter;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the align
     */
    public String getAlign()
    {
        return align == null ? "" : align;
    }

    /**
     * @param align the align to set
     */
    public void setAlign(String align)
    {
        this.align = align;
    }

    /**
     * @return the dataMode
     */
    public String getDataMode()
    {
        return dataMode;
    }

    /**
     * @param dataMode the dataMode to set
     */
    public void setDataMode(String dataMode)
    {
        this.dataMode = dataMode;
    }

    /**
     * @return the display
     */
    public String getDisplay()
    {
        return display;
    }

    /**
     * @param display the display to set
     */
    public void setDisplay(String display)
    {
        this.display = display;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the choices
     */
    public String getChoices()
    {
        return choices;
    }

    /**
     * @param choices the choices to set
     */
    public void setChoices(String choices)
    {
        this.choices = choices;
    }

    /**
     * @return the allowChange
     */
    public String getAllowChange()
    {
        return allowChange;
    }

    /**
     * @param allowChange the allowChange to set
     */
    public void setAllowChange(String allowChange)
    {
        this.allowChange = allowChange;
    }

    /**
     * @return the fieldLength
     */
    public int getFieldLength()
    {
        return fieldLength;
    }

    /**
     * @param fieldLength the fieldLength to set
     */
    public void setFieldLength(int fieldLength)
    {
        this.fieldLength = fieldLength;
    }

    /**
     * @return the eeromLocation
     */
    public String getEeromLocation()
    {
        return eeromLocation;
    }

    /**
     * @param eeromLocation the eeromLocation to set
     */
    public void setEeromLocation(String eeromLocation)
    {
        this.eeromLocation = eeromLocation;
    }

    /**
     * @return the locationData
     */
    public String getLocationData()
    {
        return locationData == null ? "" : locationData;
    }

    /**
     * @param locationData the locationData to set
     */
    public void setLocationData(String locationData)
    {
        this.locationData = locationData;
    }

    /**
     * @return the options
     */
    public List<NameValuePair> getOptions()
    {
        return options;
    }

    /**
     * @param options the options to set
     */
    public void setOptions(List<NameValuePair> options)
    {
        this.options = options;
    }

    /**
     * @return the number
     */
    public String getNumber()
    {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number)
    {
        this.number = number;
    }

    /**
     * @return the pairStr
     */
    public String getPairStr()
    {
        return pairStr;
    }

    /**
     * @param pairStr the pairStr to set
     */
    public void setPairStr(String pairStr)
    {
        this.pairStr = pairStr;
    }

	public String getSettingType() {
		return settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getCustomerEditable() {
		return customerEditable;
	}

	public void setCustomerEditable(String customerEditable) {
		this.customerEditable = customerEditable;
	}

	public String getStoredInPennies() {
		return storedInPennies;
	}

	public void setStoredInPennies(String storedInPennies) {
		this.storedInPennies = storedInPennies;
	}

	public String getUnsupportedDefaultValue() {
		return unsupportedDefaultValue;
	}

	public void setUnsupportedDefaultValue(String unsupportedDefaultValue) {
		this.unsupportedDefaultValue = unsupportedDefaultValue;
	}

	public BigDecimal getMinValue() {
		return minValue;
	}

	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}

	public BigDecimal getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}	
}
