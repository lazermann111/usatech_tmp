package com.usatech.layers.common.sprout;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class SproutPurchaseDetail {
	protected String account_id;
	protected String created;
	protected Date createdDate;
	protected String external_reference_id;
	protected List<SproutPurchaseItem> items;
	protected String refund_url;
	protected String reversal_url;
	protected BigDecimal transaction_amount;
	protected int transaction_id;
	protected int transaction_points;
	protected String url;
	protected String device_serial_number ;
	protected String vampire_pos_id ;
	
	public SproutPurchaseDetail() {
		super();
	}
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
		if(created!=null){
			try{
				createdDate=SproutUtils.sproutDateFormat.parse(created);
			}catch(ParseException e){}
		}
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public List<SproutPurchaseItem> getItems() {
		return items;
	}
	public void setItems(List<SproutPurchaseItem> items) {
		this.items = items;
	}
	public String getRefund_url() {
		return refund_url;
	}
	public void setRefund_url(String refund_url) {
		this.refund_url = refund_url;
	}
	public String getReversal_url() {
		return reversal_url;
	}
	public void setReversal_url(String reversal_url) {
		this.reversal_url = reversal_url;
	}
	public BigDecimal getTransaction_amount() {
		return transaction_amount;
	}
	public void setTransaction_amount(BigDecimal transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public int getTransaction_points() {
		return transaction_points;
	}
	public void setTransaction_points(int transaction_points) {
		this.transaction_points = transaction_points;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDevice_serial_number() {
		return device_serial_number;
	}
	public void setDevice_serial_number(String device_serial_number) {
		this.device_serial_number = device_serial_number;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
	
	@Override
	public String toString() {
		return "SproutPurchaseDetail [account_id=" + account_id + ", created="
				+ created + ", createdDate=" + createdDate
				+ ", external_reference_id=" + external_reference_id
				+ ", items=" + items + ", refund_url=" + refund_url
				+ ", reversal_url=" + reversal_url + ", transaction_amount="
				+ transaction_amount + ", transaction_id=" + transaction_id
				+ ", transaction_points=" + transaction_points + ", url=" + url
				+ ", device_serial_number=" + device_serial_number
				+ ", vampire_pos_id=" + vampire_pos_id + "]";
	}
}
