package com.usatech.layers.common.sprout;

import java.math.BigDecimal;

public class SproutLoad {
	protected String account_id;
	protected BigDecimal amount;
	protected String external_reference_id;
	protected String merchant_id;
	protected String vampire_pos_id ;
	protected String load_type ;
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public String getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
	public String getLoad_type() {
		return load_type;
	}
	public void setLoad_type(String load_type) {
		this.load_type = load_type;
	}
	@Override
	public String toString() {
		return "SproutLoad [account_id=" + account_id + ", amount=" + amount
				+ ", external_reference_id=" + external_reference_id
				+ ", merchant_id=" + merchant_id + ", vampire_pos_id="
				+ vampire_pos_id + ", load_type=" + load_type + "]";
	}
	
}
