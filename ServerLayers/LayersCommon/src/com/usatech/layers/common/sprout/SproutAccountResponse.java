package com.usatech.layers.common.sprout;

import java.math.BigDecimal;
import java.util.List;

public class SproutAccountResponse {
	protected String account_id;
	protected boolean active;
	protected BigDecimal available_balance;
	protected BigDecimal current_balance;
	protected int points_balance;
	protected String program_url;
	protected String url;
	protected List<String> aliases;
	protected Integer vendor_id;
	
	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public BigDecimal getAvailable_balance() {
		return available_balance;
	}

	public void setAvailable_balance(BigDecimal available_balance) {
		this.available_balance = available_balance;
	}

	public BigDecimal getCurrent_balance() {
		return current_balance;
	}

	public void setCurrent_balance(BigDecimal current_balance) {
		this.current_balance = current_balance;
	}

	public int getPoints_balance() {
		return points_balance;
	}

	public void setPoints_balance(int points_balance) {
		this.points_balance = points_balance;
	}

	public String getProgram_url() {
		return program_url;
	}

	public void setProgram_url(String program_url) {
		this.program_url = program_url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}

	public Integer getVendor_id() {
		return vendor_id;
	}

	public void setVendor_id(Integer vendor_id) {
		this.vendor_id = vendor_id;
	}

	@Override
	public String toString() {
		return "SproutAccountResponse [account_id=" + account_id + ", active="
				+ active + ", available_balance=" + available_balance
				+ ", current_balance=" + current_balance + ", points_balance="
				+ points_balance + ", program_url=" + program_url + ", url="
				+ url + ", aliases=" + aliases + ", vendor_id=" + vendor_id
				+ "]";
	}
	
}
