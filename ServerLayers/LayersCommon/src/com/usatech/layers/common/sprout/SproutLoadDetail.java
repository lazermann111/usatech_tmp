package com.usatech.layers.common.sprout;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

public class SproutLoadDetail {
	protected String created;
	protected Date createdDate;
	protected String external_reference_id;
	protected String load_type ;
	protected String merchant_id;
	protected BigDecimal transaction_amount;
	protected int transaction_id;
	protected String url;
	protected String vampire_pos_id ;
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
		if(created!=null){
			try{
				createdDate=SproutUtils.sproutDateFormat.parse(created);
			}catch(ParseException e){}
		}
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public String getLoad_type() {
		return load_type;
	}
	public void setLoad_type(String load_type) {
		this.load_type = load_type;
	}
	public String getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}
	public BigDecimal getTransaction_amount() {
		return transaction_amount;
	}
	public void setTransaction_amount(BigDecimal transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
	@Override
	public String toString() {
		return "SproutLoadDetail [created=" + created + ", createdDate="
				+ createdDate + ", external_reference_id="
				+ external_reference_id + ", load_type=" + load_type
				+ ", merchant_id=" + merchant_id + ", transaction_amount="
				+ transaction_amount + ", transaction_id=" + transaction_id
				+ ", url=" + url + ", vampire_pos_id=" + vampire_pos_id + "]";
	}
	
}
