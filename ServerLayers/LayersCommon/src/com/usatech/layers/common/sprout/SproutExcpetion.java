package com.usatech.layers.common.sprout;

public class SproutExcpetion extends Exception {

	private static final long serialVersionUID = 1L;
	protected int httpStatus;
	protected int code;
	protected String message;
	public SproutExcpetion() {
		super();
	}
	public SproutExcpetion(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	public SproutExcpetion(String message, Throwable cause) {
		super(message, cause);
	}
	
	public SproutExcpetion(int httpStatus) {
		super();
		this.httpStatus = httpStatus;
	}
	public SproutExcpetion(int httpStatus, int code, String message) {
		super();
		this.httpStatus = httpStatus;
		this.code = code;
		this.message = message;
	}
	public SproutExcpetion(String message) {
		super(message);
	}
	public SproutExcpetion(Throwable cause) {
		super(cause);
	}
	public int getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
