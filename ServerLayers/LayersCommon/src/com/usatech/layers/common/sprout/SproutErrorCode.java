package com.usatech.layers.common.sprout;

public class SproutErrorCode {
	protected int code;
	protected String message;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "SproutErrorCode [code=" + code + ", message=" + message + "]";
	}
	
}
