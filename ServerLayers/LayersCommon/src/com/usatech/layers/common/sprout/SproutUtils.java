package com.usatech.layers.common.sprout;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import com.google.gson.Gson;

import simple.io.Log;

public class SproutUtils {
	private static final Log log = Log.getLog();
	
	protected static final String CONTENT_TYPE_JSON="application/json;version=1.0";
	protected static final SimpleDateFormat sproutDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSXXX");
	public static final SimpleDateFormat sproutMonthFormat=new SimpleDateFormat("yyyy-MM");
	protected static String sproutAuthorizationToken = "";
	protected static String sproutUrl;
	protected static HostnameVerifier hostNameVerifier;
	
	protected final PoolingHttpClientConnectionManager httpConnectionManager;
	protected final HttpClient httpClient;
	protected int connectionTimeout;
	protected int socketTimeout;
	
	public SproutUtils() {
		this(null);
	}
	

	public SproutUtils(String[] sslProtocols) {
		super();
		if(hostNameVerifier==null){ hostNameVerifier=SSLConnectionSocketFactory.getDefaultHostnameVerifier();}
		this.httpConnectionManager = new PoolingHttpClientConnectionManager(RegistryBuilder.<ConnectionSocketFactory> create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", new SSLConnectionSocketFactory((javax.net.ssl.SSLSocketFactory) javax.net.ssl.SSLSocketFactory.getDefault(), sslProtocols, null, hostNameVerifier) {
			@Override
			public Socket createLayeredSocket(Socket socket, String target, int port, HttpContext context) throws IOException {
				Socket s = super.createLayeredSocket(socket, target, port, context);
				socketCreated(s);
				return s;
			}
		}).build());

		this.httpClient = HttpClients.custom().setConnectionManager(httpConnectionManager).build();
	}

	protected void socketCreated(Socket socket) {
		if(socket instanceof SSLSocket && log.isInfoEnabled()) {
			SSLSession ss = ((SSLSocket) socket).getSession();
			log.info(new StringBuilder().append("Created SSL Socket to ").append(socket.getInetAddress()).append(':').append(socket.getPort()).append(" using ").append(ss.getProtocol()).append(" with ").append(ss.getCipherSuite()));
		}
	}

	protected String readContent(HttpEntity httpEntity, int maxSize) throws IOException {
		if(httpEntity == null)
			return "";
		int len;
		if(httpEntity.getContentLength() > maxSize || httpEntity.getContentLength() < 0)
			len = maxSize;
		else
			len = (int) httpEntity.getContentLength();
		byte[] content = new byte[len];
		InputStream in;
		int off=0;
		try {
			in = httpEntity.getContent();
		} catch(UnsupportedOperationException e) {
			return "";
		}
		try {
			int b;
			while(off<len&&(b = in.read(content,off,len-off))>-1){
				off+=b;
            }
			
		} finally {
			in.close();
		}
		Charset charset;
		try {
			final ContentType contentType = ContentType.get(httpEntity);
			if(contentType != null)
				charset = contentType.getCharset();
			else
				charset = HTTP.DEF_CONTENT_CHARSET;
		} catch(final UnsupportedCharsetException ex) {
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		if(charset==null){
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		return new String(content, 0, off, charset);
	}

	protected void updateRequestSettings(HttpRequestBase request) {
		RequestConfig.Builder builder;
		if(request.getConfig() == null)
			builder = RequestConfig.custom();
		else
			builder = RequestConfig.copy(request.getConfig());
		builder.setConnectTimeout(getConnectionTimeout());
		builder.setSocketTimeout(getSocketTimeout());
		request.setConfig(builder.build());
	}

	public int getMaxTotalConnections() {
		return httpConnectionManager.getMaxTotal();
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		httpConnectionManager.setMaxTotal(maxTotalConnections);
	}

	public int getMaxHostConnections() {
		return httpConnectionManager.getDefaultMaxPerRoute();
	}

	public void setMaxHostConnections(int maxHostConnections) {
		httpConnectionManager.setDefaultMaxPerRoute(maxHostConnections);
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public static String getSproutAuthorizationToken() {
		return sproutAuthorizationToken;
	}

	public static void setSproutAuthorizationToken(String sproutAuthorizationToken) {
		SproutUtils.sproutAuthorizationToken = sproutAuthorizationToken;
	}
	
	public static String getSproutUrl() {
		return sproutUrl;
	}

	public static void setSproutUrl(String sproutUrl) {
		SproutUtils.sproutUrl = sproutUrl;
	}

	public static HostnameVerifier getHostNameVerifier() {
		return hostNameVerifier;
	}

	public static void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		SproutUtils.hostNameVerifier = hostNameVerifier;
	}
	
	protected HttpResponse executeHttpMethod(HttpRequestBase httpMethod) throws HttpException, IOException {		
		httpMethod.addHeader("Authorization", new StringBuilder("Token ").append(sproutAuthorizationToken).toString());
		httpMethod.addHeader("Accept", CONTENT_TYPE_JSON);		
		updateRequestSettings(httpMethod);
		return httpClient.execute(httpMethod);
	}
	
	public SproutAccountResponse getAccount(String cardId) throws SproutExcpetion{
		HttpGet httpMethod = new HttpGet(sproutUrl+"/account/"+cardId);
		try{
			HttpResponse response=executeHttpMethod(httpMethod);
			Gson gson = new Gson();
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 200: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
					SproutAccountResponse account = gson.fromJson(responseStr, SproutAccountResponse.class);
					return account;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	if (errorCode == null)
			        		throw new SproutExcpetion(statusCode);
			        	else
			        		throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutAccountResponse createAccout(String cardId,String programCode, int vendorId, String email) throws SproutExcpetion{
		HttpPost httpMethod = new HttpPost(sproutUrl+"/account/");
		try{
			SproutAccount newAccount=new SproutAccount();
			newAccount.setAlias(cardId);
			newAccount.setProgram_code(programCode);
			//newAccount.setVendor_id(vendorId);
			newAccount.setEmail(email);
			Gson gson = new Gson();
			StringEntity postEntity = new StringEntity(gson.toJson(newAccount), HTTP.DEF_CONTENT_CHARSET);
			postEntity.setContentType(CONTENT_TYPE_JSON);
			httpMethod.setEntity(postEntity);
			HttpResponse response=executeHttpMethod(httpMethod);
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 201: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
					SproutAccountResponse account = gson.fromJson(responseStr, SproutAccountResponse.class);
					return account;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutAccountTransactions getAccountTransactions(String accountId, String month) throws SproutExcpetion{
		HttpGet httpMethod = new HttpGet(sproutUrl+"/account/"+accountId+"/transactions?month="+month);
		try{
			HttpResponse response=executeHttpMethod(httpMethod);
			Gson gson = new Gson();
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 200: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
					SproutAccountTransactions accountTrans = gson.fromJson(responseStr, SproutAccountTransactions.class);
					return accountTrans;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public List<SproutTransaction> getAccountTransactionsByDate(String accountId, Date beginDate, Date endDate) throws SproutExcpetion{
		
		return null;
	}
	
	public SproutPurchaseResponse purchase(String account_info,String external_reference_id, String vampire_pos_id, List<SproutPurchaseItem> items) throws SproutExcpetion, HttpException, IOException{
		HttpPost httpMethod = new HttpPost(sproutUrl+"/purchase/");
		try{
			SproutPurchase purchase=new SproutPurchase();
			purchase.setAccount_info(account_info);
			purchase.setExternal_reference_id(external_reference_id);
			purchase.setVampire_pos_id(vampire_pos_id);
			purchase.setItems(items);
			Gson gson = new Gson();
			StringEntity postEntity = new StringEntity(gson.toJson(purchase), HTTP.DEF_CONTENT_CHARSET);
			postEntity.setContentType(CONTENT_TYPE_JSON);
			httpMethod.setEntity(postEntity);
			HttpResponse response=executeHttpMethod(httpMethod);
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 201: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        SproutPurchaseResponse purchaseResp = gson.fromJson(responseStr, SproutPurchaseResponse.class);
					return purchaseResp;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw e;
		}
	}
	
	public SproutPurchaseDetail getPurchaseDetailsByUrl(String url) throws SproutExcpetion{
		HttpGet httpMethod = new HttpGet(sproutUrl+url);
		try{
			HttpResponse response=executeHttpMethod(httpMethod);
			Gson gson = new Gson();
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 200: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        SproutPurchaseDetail purchaseDetails = gson.fromJson(responseStr, SproutPurchaseDetail.class);
					return purchaseDetails;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutLoadOrRefundResponse load(String account_id, BigDecimal amount, String external_reference_id, String merchant_id,String vampire_pos_id,String load_type ) throws SproutExcpetion{
		HttpPost httpMethod = new HttpPost(sproutUrl+"/load/");
		try{
			SproutLoad load=new SproutLoad();
			load.setAccount_id(account_id);
			load.setAmount(amount);
			load.setExternal_reference_id(external_reference_id);
			load.setMerchant_id(merchant_id);
			load.setVampire_pos_id(vampire_pos_id);
			load.setLoad_type(load_type);
			Gson gson = new Gson();
			StringEntity postEntity = new StringEntity(gson.toJson(load), HTTP.DEF_CONTENT_CHARSET);
			postEntity.setContentType(CONTENT_TYPE_JSON);
			httpMethod.setEntity(postEntity);
			HttpResponse response=executeHttpMethod(httpMethod);
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 201: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        SproutLoadOrRefundResponse resp = gson.fromJson(responseStr, SproutLoadOrRefundResponse.class);
					return resp;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutLoadDetail getLoadDetailByUrl(String url) throws SproutExcpetion{
		HttpGet httpMethod = new HttpGet(sproutUrl+url);
		try{
			HttpResponse response=executeHttpMethod(httpMethod);
			Gson gson = new Gson();
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 200: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        SproutLoadDetail loadDetail = gson.fromJson(responseStr, SproutLoadDetail.class);
					return loadDetail;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutReversalResponse reversal(String external_reference_id, String original_external_reference_id, String original_transaction_type,String vampire_pos_id) throws SproutExcpetion{
		HttpPost httpMethod = new HttpPost(sproutUrl+"/reversal/");
		try{
			SproutReversal reversal=new SproutReversal();
			reversal.setExternal_reference_id(external_reference_id);
			reversal.setOriginal_external_reference_id(original_external_reference_id);
			reversal.setVampire_pos_id(vampire_pos_id);
			reversal.setOriginal_transaction_type(original_transaction_type);
			Gson gson = new Gson();
			StringEntity postEntity = new StringEntity(gson.toJson(reversal), HTTP.DEF_CONTENT_CHARSET);
			postEntity.setContentType(CONTENT_TYPE_JSON);
			httpMethod.setEntity(postEntity);
			HttpResponse response=executeHttpMethod(httpMethod);
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 201: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
					SproutReversalResponse reversalResp = gson.fromJson(responseStr, SproutReversalResponse.class);
					return reversalResp;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutReversalDetail getReversalDetailByUrl(String url) throws SproutExcpetion{
		HttpGet httpMethod = new HttpGet(sproutUrl+url);
		try{
			HttpResponse response=executeHttpMethod(httpMethod);
			Gson gson = new Gson();
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 200: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        SproutReversalDetail reversalDetail = gson.fromJson(responseStr, SproutReversalDetail.class);
					return reversalDetail;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	
	public SproutLoadOrRefundResponse refund(String external_reference_id, int purchase_transaction_id,String reason ) throws SproutExcpetion{
		HttpPost httpMethod = new HttpPost(sproutUrl+"/refund/");
		try{
			SproutRefund refund=new SproutRefund();
			refund.setExternal_reference_id(external_reference_id);
			refund.setPurchase_transaction_id(purchase_transaction_id);
			refund.setReason(reason);
			Gson gson = new Gson();
			StringEntity postEntity = new StringEntity(gson.toJson(refund), HTTP.DEF_CONTENT_CHARSET);
			postEntity.setContentType(CONTENT_TYPE_JSON);
			httpMethod.setEntity(postEntity);
			HttpResponse response=executeHttpMethod(httpMethod);
			final int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseStr;
			switch (statusCode) {
				case 201: //OK
					if (entity == null) {
			            throw new SproutExcpetion(statusCode, 0, "Http response contains no content");
			        }
			        responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        SproutLoadOrRefundResponse resp = gson.fromJson(responseStr, SproutLoadOrRefundResponse.class);
					return resp;
				case 400: //bad request
				case 401: //unauthorized		
					throw new SproutExcpetion(statusCode);
				case 404: //not found
					throw new SproutExcpetion(statusCode);
				default:
					if (entity == null) {
			            throw new SproutExcpetion(statusCode);
			        }else{
			        	responseStr= readContent(response.getEntity(), Integer.MAX_VALUE);
			        	SproutErrorCode errorCode = gson.fromJson(responseStr, SproutErrorCode.class);
			        	throw new SproutExcpetion(statusCode, errorCode.getCode(), errorCode.getMessage());
			        }
			}
		}catch(IOException|HttpException e){
			log.info(e.getMessage(), e);
			throw new SproutExcpetion(e.getMessage(), e);
		}
	}
	

}
