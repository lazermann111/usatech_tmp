package com.usatech.layers.common.sprout;

import java.util.List;

public class SproutPurchase {
	protected String account_info;
	protected String external_reference_id;
	protected String vampire_pos_id ;
	protected List<SproutPurchaseItem> items;

	public String getAccount_info() {
		return account_info;
	}
	public void setAccount_info(String account_info) {
		this.account_info = account_info;
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
	public List<SproutPurchaseItem> getItems() {
		return items;
	}
	public void setItems(List<SproutPurchaseItem> items) {
		this.items = items;
	}
	@Override
	public String toString() {
		return "SproutPurchase [account_info=" + account_info
				+ ", external_reference_id=" + external_reference_id
				+ ", vampire_pos_id=" + vampire_pos_id + ", items=" + items
				+ "]";
	}
	
}
