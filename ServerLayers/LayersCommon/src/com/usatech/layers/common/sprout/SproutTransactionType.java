package com.usatech.layers.common.sprout;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

public enum SproutTransactionType {
	POINTS_REDEMPTION("Points Redemption"),
	REVERSAL("Reversal"),
	PURCHASE("Purchase"),
	REFUND("Refund"),
	INSTANT_PROMOTION_REWARD("Instant Promotion Reward"),
	RECURRING_LOAD("Recurring Load"),
	PROMOTION_REWARD("Promotion Reward"),
	OTHER_TRANSACTION("Other Transaction"),
	LOAD("Load")
	;

	private final String value;
	protected final static EnumStringValueLookup<SproutTransactionType> lookup = new EnumStringValueLookup<SproutTransactionType>(SproutTransactionType.class);

	private SproutTransactionType(String value) {
        this.value = value;
	}
    public String getValue() {
        return value;
    }
    public static SproutTransactionType getByValue(String value) throws InvalidValueException {
		return lookup.getByValue(value);
    }
}
