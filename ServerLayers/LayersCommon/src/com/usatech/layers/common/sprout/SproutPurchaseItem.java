package com.usatech.layers.common.sprout;

import java.math.BigDecimal;

public class SproutPurchaseItem {
	protected String barcode;
	protected String coil_name;
	protected BigDecimal deposit;
	protected String description;
	protected BigDecimal price;
	protected BigDecimal tax;
	protected String item_url;
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getCoil_name() {
		return coil_name;
	}
	public void setCoil_name(String coil_name) {
		this.coil_name = coil_name;
	}
	public BigDecimal getDeposit() {
		return deposit;
	}
	public void setDeposit(BigDecimal deposit) {
		this.deposit = deposit;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public String getItem_url() {
		return item_url;
	}
	public void setItem_url(String item_url) {
		this.item_url = item_url;
	}
	@Override
	public String toString() {
		return "SproutPurchaseItem [barcode=" + barcode + ", coil_name="
				+ coil_name + ", deposit=" + deposit + ", description="
				+ description + ", price=" + price + ", tax=" + tax
				+ ", item_url=" + item_url + "]";
	}
	
}
