package com.usatech.layers.common.sprout;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

public class SproutTransaction {
	protected String comment;
	protected String created;
	protected Date createdDate;
	protected String external_reference_id;
	protected int item_count;
	protected String load_type;
	protected int new_points_balance;
	protected BigDecimal new_total_balance;
	protected BigDecimal transaction_amount;
	protected int transaction_id;
	protected int transaction_points;
	protected String transaction_type;
	protected String url;
	public SproutTransaction() {
		super();
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
		if(created!=null){
			try{
				createdDate=SproutUtils.sproutDateFormat.parse(created);
			}catch(ParseException e){}
		}
	}
	
	public Date getCreatedDate() {
		if(createdDate!=null){
			return createdDate;
		}else{
			if(created!=null){
				try{
					createdDate=SproutUtils.sproutDateFormat.parse(created);
				}catch(ParseException e){}
			}
			return createdDate;
		}
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public int getItem_count() {
		return item_count;
	}
	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}
	public String getLoad_type() {
		return load_type;
	}
	public void setLoad_type(String load_type) {
		this.load_type = load_type;
	}
	public int getNew_points_balance() {
		return new_points_balance;
	}
	public void setNew_points_balance(int new_points_balance) {
		this.new_points_balance = new_points_balance;
	}
	public BigDecimal getNew_total_balance() {
		return new_total_balance;
	}
	public void setNew_total_balance(BigDecimal new_total_balance) {
		this.new_total_balance = new_total_balance;
	}
	public BigDecimal getTransaction_amount() {
		return transaction_amount;
	}
	public void setTransaction_amount(BigDecimal transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public int getTransaction_points() {
		return transaction_points;
	}
	public void setTransaction_points(int transaction_points) {
		this.transaction_points = transaction_points;
	}
	public String getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "SproutTransaction [comment=" + comment + ", created=" + created
				+ ", external_reference_id=" + external_reference_id
				+ ", item_count=" + item_count + ", load_type=" + load_type
				+ ", new_points_balance=" + new_points_balance
				+ ", new_total_balance=" + new_total_balance
				+ ", transaction_amount=" + transaction_amount
				+ ", transaction_id=" + transaction_id
				+ ", transaction_points=" + transaction_points
				+ ", transaction_type=" + transaction_type + ", url=" + url
				+ "]";
	}
	
}
