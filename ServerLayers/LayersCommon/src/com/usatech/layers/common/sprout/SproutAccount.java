package com.usatech.layers.common.sprout;

public class SproutAccount {
	protected String alias;
	protected boolean eula=true;
	protected String program_code;
	protected String recurring_load_code;
	protected Integer vendor_id;
	protected String email;
	
	public SproutAccount() {
		super();
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public boolean isEula() {
		return eula;
	}
	public void setEula(boolean eula) {
		this.eula = eula;
	}
	public String getProgram_code() {
		return program_code;
	}
	public void setProgram_code(String program_code) {
		this.program_code = program_code;
	}
	public String getRecurring_load_code() {
		return recurring_load_code;
	}
	public void setRecurring_load_code(String recurring_load_code) {
		this.recurring_load_code = recurring_load_code;
	}
	
	public Integer getVendor_id() {
		return vendor_id;
	}
	public void setVendor_id(Integer vendor_id) {
		this.vendor_id = vendor_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
