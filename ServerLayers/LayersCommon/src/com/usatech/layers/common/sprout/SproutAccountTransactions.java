package com.usatech.layers.common.sprout;

import java.util.List;

public class SproutAccountTransactions {
	protected String account_id;
	protected String account_url;
	protected String next_url;
	protected String prev_url;
	protected List<SproutTransaction> transactions;
	protected String url;
	public SproutAccountTransactions() {
		super();
	}
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getAccount_url() {
		return account_url;
	}
	public void setAccount_url(String account_url) {
		this.account_url = account_url;
	}
	public String getNext_url() {
		return next_url;
	}
	public void setNext_url(String next_url) {
		this.next_url = next_url;
	}
	public String getPrev_url() {
		return prev_url;
	}
	public void setPrev_url(String prev_url) {
		this.prev_url = prev_url;
	}
	
	public List<SproutTransaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(List<SproutTransaction> transactions) {
		this.transactions = transactions;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "SproutAccountTransactions [account_id=" + account_id
				+ ", account_url=" + account_url + ", next_url=" + next_url
				+ ", prev_url=" + prev_url + ", transactions=" + transactions
				+ ", url=" + url + "]";
	}
	
}
