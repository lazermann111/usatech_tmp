package com.usatech.layers.common.sprout;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

public class SproutReversalDetail {
	protected String created;
	protected Date createdDate;
	protected String external_reference_id;
	protected int points_change ;
	protected String original_transaction_url;
	protected BigDecimal transaction_amount;
	protected int transaction_id;
	protected String url;
	
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
		if(created!=null){
			try{
				createdDate=SproutUtils.sproutDateFormat.parse(created);
			}catch(ParseException e){}
		}
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public BigDecimal getTransaction_amount() {
		return transaction_amount;
	}
	public void setTransaction_amount(BigDecimal transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "SproutReversalDetail [created=" + created + ", createdDate="
				+ createdDate + ", external_reference_id="
				+ external_reference_id + ", points_change=" + points_change
				+ ", original_transaction_url=" + original_transaction_url
				+ ", transaction_amount=" + transaction_amount
				+ ", transaction_id=" + transaction_id + ", url=" + url + "]";
	}
	
}
