package com.usatech.layers.common.sprout;

public class SproutRefund {
	protected String external_reference_id;
	protected int purchase_transaction_id;
	protected String reason;
	public SproutRefund() {
		super();
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public int getPurchase_transaction_id() {
		return purchase_transaction_id;
	}
	public void setPurchase_transaction_id(int purchase_transaction_id) {
		this.purchase_transaction_id = purchase_transaction_id;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "SproutRefund [external_reference_id=" + external_reference_id
				+ ", purchase_transaction_id=" + purchase_transaction_id
				+ ", reason=" + reason + "]";
	}
}
