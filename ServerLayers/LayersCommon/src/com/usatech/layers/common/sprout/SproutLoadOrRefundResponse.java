package com.usatech.layers.common.sprout;

import java.math.BigDecimal;

public class SproutLoadOrRefundResponse {
	protected String account_id;
	protected String account_url;
	protected BigDecimal net_transaction_amount;
	protected int net_transaction_points;
	protected BigDecimal new_available_balance;
	protected int new_points_balance;
	protected int transaction_id;
	protected String url;
	
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getAccount_url() {
		return account_url;
	}
	public void setAccount_url(String account_url) {
		this.account_url = account_url;
	}
	public BigDecimal getNet_transaction_amount() {
		return net_transaction_amount;
	}
	public void setNet_transaction_amount(BigDecimal net_transaction_amount) {
		this.net_transaction_amount = net_transaction_amount;
	}
	public int getNet_transaction_points() {
		return net_transaction_points;
	}
	public void setNet_transaction_points(int net_transaction_points) {
		this.net_transaction_points = net_transaction_points;
	}
	public BigDecimal getNew_available_balance() {
		return new_available_balance;
	}
	public void setNew_available_balance(BigDecimal new_available_balance) {
		this.new_available_balance = new_available_balance;
	}
	public int getNew_points_balance() {
		return new_points_balance;
	}
	public void setNew_points_balance(int new_points_balance) {
		this.new_points_balance = new_points_balance;
	}
	public int getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "SproutLoadOrRefundResponse [account_id=" + account_id
				+ ", account_url=" + account_url + ", net_transaction_amount="
				+ net_transaction_amount + ", net_transaction_points="
				+ net_transaction_points + ", new_available_balance="
				+ new_available_balance + ", new_points_balance="
				+ new_points_balance + ", transaction_id=" + transaction_id
				+ ", url=" + url + "]";
	}
	
}
