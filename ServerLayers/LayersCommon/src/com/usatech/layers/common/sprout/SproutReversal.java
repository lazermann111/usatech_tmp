package com.usatech.layers.common.sprout;

public class SproutReversal {
	protected String external_reference_id;
	protected String original_external_reference_id;
	protected int original_transaction_id;
	protected String original_transaction_type;
	protected String device_serial_number;
	protected String vampire_pos_id;
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public String getOriginal_external_reference_id() {
		return original_external_reference_id;
	}
	public void setOriginal_external_reference_id(
			String original_external_reference_id) {
		this.original_external_reference_id = original_external_reference_id;
	}
	public int getOriginal_transaction_id() {
		return original_transaction_id;
	}
	public void setOriginal_transaction_id(int original_transaction_id) {
		this.original_transaction_id = original_transaction_id;
	}
	public String getOriginal_transaction_type() {
		return original_transaction_type;
	}
	public void setOriginal_transaction_type(String original_transaction_type) {
		this.original_transaction_type = original_transaction_type;
	}
	public String getDevice_serial_number() {
		return device_serial_number;
	}
	public void setDevice_serial_number(String device_serial_number) {
		this.device_serial_number = device_serial_number;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
	@Override
	public String toString() {
		return "SproutReversal [external_reference_id=" + external_reference_id
				+ ", original_external_reference_id="
				+ original_external_reference_id + ", original_transaction_id="
				+ original_transaction_id + ", original_transaction_type="
				+ original_transaction_type + ", device_serial_number="
				+ device_serial_number + ", vampire_pos_id=" + vampire_pos_id
				+ "]";
	}
	
}
