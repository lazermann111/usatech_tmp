/**
 *
 */
package com.usatech.layers.common;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.translator.Translator;

import com.usatech.layers.common.messagedata.MessageData;

/**
 * @author Brian S. Krug
 *
 */
public interface Message {
    public String getGlobalSessionCode() ;
    public byte getProtocolId() ;
    public String getDeviceName() ;
    public MessageData getData() ;
    public void sendReply(MessageData... replyData) throws ServiceException ;
	public int getReplyCount();
    public Publisher<ByteInput> getPublisher() ;
    public Log getLog() ;

	public Translator getTranslator();
    public Object getSessionAttribute(String name) ;
    public void setResultAttribute(String name, Object value) ;
    public void setSessionAttribute(String name, Object value) ;
    public Long getLastCommandId() ;
    public void setNextCommandId(long commandId) ;
    public Resource addFileTransfer(boolean useResource, long fileTransferId, long pendingCommandId, int fileGroup, String fileName, int fileTypeId, int filePacketSize, long fileSize) throws ServiceException ;
    public long getServerTime() ;
    public DeviceInfo getDeviceInfo() throws ServiceException ;
	public DeviceInfo getNewDeviceInfo(String deviceName) throws ServiceException ;
}
