package com.usatech.layers.common;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import com.trilead.ssh2.SFTPException;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.SFTPv3DirectoryEntry;
import com.trilead.ssh2.SFTPv3FileAttributes;
import com.trilead.ssh2.SFTPv3FileHandle;
import com.trilead.ssh2.sftp.ErrorCodes;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.RegexUtils;
import simple.text.StringUtils;

/**
 * The task pulls a file from the server via SecureFTP
 * 
 * @author bkrug
 * 
 */
public class DownloadSftpTask extends AbstractSftpTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;

	@Override
	protected int processSftp(MessageChainTaskInfo taskInfo, SFTPv3Client sftp) throws IOException, ServiceException {
		String filePath;
		long currentModificationTime;
		boolean wildcard;
		MessageChainStep step = taskInfo.getStep();
		try {
			filePath = step.getAttribute(CommonAttrEnum.ATTR_FILE_PATH, String.class, true);
			currentModificationTime = step.getAttributeDefault(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, Long.class, 0L);
			wildcard = step.getAttributeDefault(CommonAttrEnum.ATTR_WILDCARD, Boolean.class, Boolean.FALSE);
		} catch(AttributeConversionException e) {
			log.error("Could not convert attributes", e);
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, StringUtils.exceptionToString(e));
			return 2;
		}
		if(wildcard) {
			List<DownloadInfo> dis = new ArrayList<>();
			String[] fileParts = StringUtils.splitQuotesAnywhere(RegexUtils.fromQuotesToBackslash(filePath).replaceAll("(?<!\\\\)\\\\/", "/"), '/', '\'', true);
			downloadFiles(sftp, fileParts, 0, ".", currentModificationTime, dis);
			if(dis.isEmpty())
				return 1;
			String[] resourceKeys = new String[dis.size()];
			long[] modificationTimes = new long[dis.size()];
			String[] filePaths = new String[dis.size()];
			int i = 0;
			for(DownloadInfo di : dis) {
				filePaths[i] = di.filePath;
				resourceKeys[i] = di.resourceKey;
				modificationTimes[i] = di.modificationTime;
				i++;
			}
			step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, modificationTimes);
			step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE, resourceKeys);
			step.setResultAttribute(CommonAttrEnum.ATTR_FILE_PATH, filePaths);
		} else {
			DownloadInfo di = downloadFile(sftp, filePath, currentModificationTime);
			if(di == null)
				return 1;
			step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, di.modificationTime);
			step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE, di.resourceKey);
			step.setResultAttribute(CommonAttrEnum.ATTR_FILE_PATH, di.filePath);
		}
		return 0;
	}

	protected void downloadFiles(SFTPv3Client sftp, String[] fileParts, int offset, String dirPath, long currentModificationTime, List<DownloadInfo> dis) throws IOException, ServiceException {
		String s = fileParts[offset];
		if(!s.isEmpty() && s.charAt(0) == '^')
			s = s.substring(1);
		if(!s.isEmpty() && s.charAt(s.length() - 1) == '$')
			s = s.substring(0, s.length() - 1);
		if(s.isEmpty()) {
			if(offset + 1 < fileParts.length)
				downloadFiles(sftp, fileParts, offset + 1, dirPath, currentModificationTime, dis);
			return;
		}
		if(RegexUtils.containsWildcards(s)) {
			Pattern pattern = Pattern.compile(s);
			Vector<?> files = sftp.ls(dirPath);
			for(Object f : files) {
				SFTPv3DirectoryEntry entry = (SFTPv3DirectoryEntry) f;
				if(pattern.matcher(entry.filename).matches()) {
					if(offset + 1 == fileParts.length) {
						// download the file
						DownloadInfo di = downloadFile(sftp, dirPath + '/' + entry.filename, currentModificationTime);
						if(di != null)
							dis.add(di);
					} else {
						downloadFiles(sftp, fileParts, offset + 1, dirPath + '/' + entry.filename, currentModificationTime, dis);
					}
				}
			}
		} else {
			s = RegexUtils.unescapeBackslash(s);
			if(offset + 1 == fileParts.length) {
				// download the file
				DownloadInfo di = downloadFile(sftp, dirPath + '/' + s, currentModificationTime);
				if(di != null)
					dis.add(di);
			} else {
				downloadFiles(sftp, fileParts, offset + 1, dirPath + '/' + s, currentModificationTime, dis);
			}
		}
	}

	protected class DownloadInfo {
		public final String resourceKey;
		public final long length;
		public final long modificationTime;
		public final String filePath;

		public DownloadInfo(String filePath, String resourceKey, long length, long modificationTime) {
			this.filePath = filePath;
			this.resourceKey = resourceKey;
			this.length = length;
			this.modificationTime = modificationTime;
		}
	}

	protected DownloadInfo downloadFile(SFTPv3Client sftp, String filePath, long currentModificationTime) throws IOException, ServiceException {
		long newModificationTime;
		SFTPv3FileAttributes atts;
		try {
			atts = sftp.stat(filePath);
		} catch(SFTPException e) {
			if(e.getServerErrorCode() == ErrorCodes.SSH_FX_NO_SUCH_FILE) {
				log.info("File '" + filePath + "' does not exist on remote system");
				return null;
			}
			throw e;
		}
		if(atts.mtime != null)
			newModificationTime = 1000L * atts.mtime.longValue();
		else
			newModificationTime = 0L;
		log.info("Found file '" + filePath + "' with modified time of " + newModificationTime + " and size of " + atts.size);
		if(currentModificationTime != 0 && currentModificationTime == newModificationTime) {
			log.info("File '" + filePath + "' has not changed. No refresh.");
			return null;
		} else if(currentModificationTime > newModificationTime) {
			log.info("File '" + filePath + "' modified time is less than previously read. No refresh.");
			return null;
		}
		Resource resource = resourceFolder.getResource(filePath, ResourceMode.CREATE);
		long fileOffset = 0L;
		boolean okay = false;
		try {
			SFTPv3FileHandle handle = sftp.openFileRO(filePath);
			try {
				OutputStream out = resource.getOutputStream();
				try {
					byte[] buffer = new byte[bufferSize];
					int r;
					while((r = sftp.read(handle, fileOffset, buffer, 0, buffer.length)) >= 0) {
						out.write(buffer, 0, r);
						fileOffset += r;
					}
					log.info("Read " + fileOffset + " bytes from file '" + filePath + "'");
					out.flush();
					okay = true;
				} finally {
					out.close();
				}
			} finally {
				sftp.closeFile(handle);
			}
		} catch(SFTPException e) {
			if(e.getServerErrorCode() == ErrorCodes.SSH_FX_NO_SUCH_FILE) {
				log.info("File '" + filePath + "' does not exist on remote system");
				return null;
			}
		} catch (IOException e) {
			if (filePath.endsWith(".dfr"))
				throw new RetrySpecifiedServiceException("Error downloading DFR file '" + filePath + "', not retrying via Queue Layer", e, WorkRetryType.NO_RETRY);
			else
				throw e;
		} finally {
			if(!okay)
				resource.delete();
			resource.release();
		}
		return new DownloadInfo(filePath, resource.getKey(), fileOffset, newModificationTime);
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
