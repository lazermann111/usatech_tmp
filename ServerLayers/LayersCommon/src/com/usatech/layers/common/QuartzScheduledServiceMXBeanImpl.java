package com.usatech.layers.common;

import java.lang.management.ManagementFactory;
import java.util.HashSet;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;

import simple.io.Log;

import com.usatech.layers.common.QuartzCronScheduledJob;
import com.usatech.layers.common.QuartzScheduledJobMapping;
import com.usatech.layers.common.QuartzScheduledService;
/**
 * This is a convenient class that can lookup scheduled job and trigger/pause/cancel job
 * @author yhe
 *
 */
public class QuartzScheduledServiceMXBeanImpl implements QuartzScheduledServiceMXBean {
	private static final Log log = Log.getLog();
	protected QuartzScheduledJobMapping jobMap;
	protected Scheduler scheduler;
	
	public QuartzScheduledServiceMXBeanImpl(QuartzScheduledJobMapping jobMap,
			Scheduler scheduler) {
		super();
		this.jobMap = jobMap;
		this.scheduler = scheduler;
		registerJMX();
	}
	
	public void registerJMX(){
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		try{
			mbs.registerMBean(this, new ObjectName("QuartzScheduledService:name=quartzJMX"));
		}catch(Exception e){
			log.warn("Failed to register QuartzScheduledServiceMXBeanImpl.", e );
		}
	}

	public QuartzScheduledServiceMXBeanImpl() {
		super();
		registerJMX();
	}
	

	@Override
	public void triggerJobNow(String jobId) throws SchedulerException {
		QuartzCronScheduledJob job=jobMap.getJob(jobId);
		scheduler.triggerJob(new JobKey(jobId, job.getGroup()), QuartzScheduledService.buildJobDataMap(job.getJobDataMap()));
		log.info("Triggered now jobId="+jobId);
	}

	public QuartzScheduledJobMapping getJobMap() {
		return jobMap;
	}

	public void setJobMap(QuartzScheduledJobMapping jobMap) {
		this.jobMap = jobMap;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	@Override
	public Set<String> getJobIds() {
		return jobMap.getJobMap().keySet();
	}

	@Override
	public void pauseJob(String jobId) throws SchedulerException {
		QuartzCronScheduledJob job=jobMap.getJob(jobId);
		scheduler.pauseJob(new JobKey(jobId, job.getGroup()));
		log.info("Paused now jobId="+jobId);
	}

	@Override
	public void resumeJob(String jobId) throws SchedulerException {
		QuartzCronScheduledJob job=jobMap.getJob(jobId);
		scheduler.resumeJob(new JobKey(jobId, job.getGroup()));
		log.info("Resumed now jobId="+jobId);
	}

	@Override
	public Set<String> getPausedJobs() throws SchedulerException{
		Set<String> pausedJobs=new HashSet<String>();
		for(String jobId: jobMap.getJobMap().keySet()){
			QuartzCronScheduledJob job=jobMap.getJob(jobId);
			if(scheduler.getTriggerState(new TriggerKey(jobId+"_trigger", job.getGroup()))==TriggerState.PAUSED){
				pausedJobs.add(jobId);
			}
		}
		return pausedJobs;
	}

}
