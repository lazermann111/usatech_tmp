/**
 *
 */
package com.usatech.layers.common;

import com.usatech.layers.common.DeviceInfo;

/**
 * @author Brian S. Krug
 *
 */
public interface AppLayerDeviceInfo extends DeviceInfo {
	public void addDeviceInfoListener(DeviceInfoUpdateListener listener) ;
	public void removeDeviceInfoListener(DeviceInfoUpdateListener listener) ;
}
