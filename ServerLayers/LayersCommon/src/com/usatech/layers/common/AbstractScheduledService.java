package com.usatech.layers.common;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.Service;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.app.ServiceStatusListener;
import simple.app.WorkQueue;
import simple.io.Log;
import simple.util.MapBackedSet;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.TrivialFuture;

/**
 * The scheduled service for performing tasks periodically.
 * 
 * @author bkrug
 * 
 */
public abstract class AbstractScheduledService implements Service {
	private static final Log log = Log.getLog();
	protected final String serviceName;
	protected final Set<ServiceStatusListener> serviceStatusListeners = new MapBackedSet<ServiceStatusListener>(new ConcurrentHashMap<ServiceStatusListener, Object>());
	protected final Thread thread;
	protected final AtomicLong iterationCount = new AtomicLong();
	protected ServiceStatus serviceStatus = ServiceStatus.INITIALIZING;
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	
	protected void fireServiceStatusChanged(ServiceStatus serviceStatus) {
		if(serviceStatusListeners.isEmpty())
			return;
		ServiceStatusInfo info = new ServiceStatusInfo();
		info.setQueueLatency(null);
		info.setService(this);
		info.setThread(thread);
		info.setIterationCount(iterationCount.get());
		info.setServiceStatus(serviceStatus);
		for(ServiceStatusListener l : serviceStatusListeners) {
			l.serviceStatusChanged(info);
		}
	}
	/**
	 *  Construct a ScheduledService with a service name.
	 * @param serviceName
	 */
	public AbstractScheduledService(String serviceName) {
		this.serviceName = serviceName;
		this.thread = new Thread(serviceName + " Scheduled Thread") {
			@Override
			public void run() {
				lock.lock();
				try {
					while(true) {
						switch(serviceStatus) {
							case STARTING:
								serviceStatus = ServiceStatus.STARTED;
								fireServiceStatusChanged(ServiceStatus.STARTED);
							case STARTED:
								iterationCount.incrementAndGet();
								fireServiceStatusChanged(ServiceStatus.PROCESSING_MSG);
								lock.unlock();
								try {
									try {
										process();
									} finally {
										lock.lock();
									}
									fireServiceStatusChanged(ServiceStatus.ACKNOWLEDGING_MSG);
								} catch(ServiceException e) {
									fireServiceStatusChanged(ServiceStatus.NACKING_MSG);
									log.warn("Could not process task", e);
								} catch(RuntimeException e) {
									fireServiceStatusChanged(ServiceStatus.NACKING_MSG);
									log.warn("Could not process task", e);
								} catch(Error e) {
									fireServiceStatusChanged(ServiceStatus.NACKING_MSG);
									log.warn("Could not process task", e);
								}
								switch(serviceStatus) {
									case STARTED:
										fireServiceStatusChanged(ServiceStatus.AWAITING_NEXT);
										long d = getDelay();
										if(d > 0)
											try {
												signal.await(d, TimeUnit.MILLISECONDS);
											} catch(InterruptedException e) {
												// ignore
											}
										break;
									case STOPPING:
										break;
									default:
										fireServiceStatusChanged(serviceStatus);
								}
								break;
							case STOPPING:
								serviceStatus = ServiceStatus.STOPPED;
							case INITIALIZING:
							case STOPPED:
							case PAUSED:
								fireServiceStatusChanged(serviceStatus);
								signal.awaitUninterruptibly();
								break;
						}
					}
				} finally {
					serviceStatus = ServiceStatus.DONE;
					fireServiceStatusChanged(ServiceStatus.DONE);
					lock.unlock();
				}
			}
		};
	}
	
	protected abstract void process() throws ServiceException;

	protected abstract long getDelay();

	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		boolean changed = serviceStatusListeners.add(listener);
		if(changed) {
			fireServiceStatusChanged(serviceStatus);
		}
		return changed;
	}

	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return serviceStatusListeners.remove(listener);
	}

	/**
	 * Get the number of working threads, there is only one thread for the ScheduledService,
	 * so 1 is always returned.
	 * @return number of threads
	 * @see simple.app.Service#getNumThreads()
	 */
	@Override
	public int getNumThreads() {
		return 1;
	}

	/**
	 *  Return the service'name.
	 * 
	 * @see simple.app.Service#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Pause the working thread of the scheduled service.
	 * @return number of threads paused
	 * @see simple.app.Service#pauseThreads()
	 */
	@Override
	public int pauseThreads() throws ServiceException {
		lock.lock();
		try {
			switch(serviceStatus) {
				case STARTED:
					serviceStatus = ServiceStatus.PAUSED;
					signal.signalAll();
					return 1;
				case INITIALIZING:
				case STARTING:
				case STOPPING:
				case STOPPED:
					serviceStatus = ServiceStatus.PAUSED;
					fireServiceStatusChanged(ServiceStatus.PAUSED);
					return 1;
				default:
					return 0;
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Does not support this feature.
	 * @param timeout
	 * @return 0
	 */
	@Override
	public int restartThreads(long timeout) throws ServiceException {
		return 0;
	}

	/** Start the working thread of this ScheduledService.
	 * @param numThreads this parameter will be ignored.
	 * @return number of threads started.
	 * @see simple.app.Service#startThreads(int)
	 */
	@Override
	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads < 1)
			return 0;
		lock.lock();
		try {
			switch(serviceStatus) {
				case INITIALIZING:
					serviceStatus = ServiceStatus.STARTING;
					fireServiceStatusChanged(ServiceStatus.STARTING);
					thread.start();
					return 1;
				case STOPPED:
				case STOPPING:
					serviceStatus = ServiceStatus.STARTING;
					fireServiceStatusChanged(ServiceStatus.STARTING);
					signal.signalAll();
					return 1;
				default:
					return 0;
			}
		} finally {
			lock.unlock();
		}
	}

	@Override
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(stopAllThreads(force, 0L));
	}
	@Override
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(stopThreads(numThreads, force, 0L));
	}
	/**
	 * Stop the working thread.
	 * @param force this parameter will be ignored.
	 * @param timeout this parameter will be ignored.
	 * @return number of threads stopped.
	 * @see simple.app.Service#stopAllThreads(boolean, long)
	 */
	@Override
	public int stopAllThreads(boolean force, long timeout)
			throws ServiceException {
		lock.lock();
		try {
			switch(serviceStatus) {
				case INITIALIZING:
				case STARTING:
				case STARTED:
				case PAUSED:
					serviceStatus = ServiceStatus.STOPPED;
					fireServiceStatusChanged(ServiceStatus.STOPPED);
					return 1;
				default:
					return 0;
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Stop specific number of threads, there is only one working thread 
	 * for the ScheduledService, so it will always be stopped.
	 * 
	 * @param numThreads this parameter will be ignored.
	 * @param force this parameter will be ignored.
	 * @param timeout this parameter will be ignored.
	 * @return number of threads stopped.
	 * @see simple.app.Service#stopThreads(int, boolean, long)
	 */
	@Override
	public int stopThreads(int numThreads, boolean force, long timeout)
			throws ServiceException {
		if(numThreads < 1)
			return 0;
		return stopAllThreads(force, timeout);
	}

	/**
	 * Unpause the working thread.
	 * @return number of threads unpaused.
	 * 
	 * @see simple.app.Service#unpauseThreads()
	 */
	@Override
	public int unpauseThreads() throws ServiceException {
		lock.lock();
		try {
			switch(serviceStatus) {
				case INITIALIZING:
					serviceStatus = ServiceStatus.STARTING;
					fireServiceStatusChanged(ServiceStatus.STARTING);
					thread.start();
					return 1;
				case PAUSED:
					serviceStatus = ServiceStatus.STARTED;
					fireServiceStatusChanged(ServiceStatus.STARTED);
					signal.signalAll();
					return 1;
				default:
					return 0;
			}
		} finally {
			lock.unlock();
		}
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		return WorkQueue.TRUE_FUTURE;
	}

	/**
	 * @see simple.app.Service#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		return new ResultFuture<Boolean>() {
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				try {
					return stopAllThreads(false, unit.toMillis(timeout)) > 0;
				} catch(ServiceException e) {
					throw new ExecutionException(e);
				}
			}
		};
	}
}
