package com.usatech.layers.common;

import java.util.Set;

import javax.management.MXBean;

import org.quartz.SchedulerException;

@MXBean(true)
public interface QuartzScheduledServiceMXBean {
	
	public abstract void triggerJobNow(String jobId) throws SchedulerException;
	
	public abstract Set<String> getJobIds();
	
	public abstract void pauseJob(String jobId) throws SchedulerException;
	
	public abstract void resumeJob(String jobId) throws SchedulerException;
	
	public abstract Set<String >getPausedJobs()throws SchedulerException;
	

}
