/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.layers.common.util;

import java.util.HashSet;
import java.util.Set;


/**
 * This class provides the constants and methods used for pagination and sort.
 */
public class PaginationUtil
{
    private static final simple.io.Log log = simple.io.Log.getLog();

    /* param names will be used in <jsp:param> to include the pagination.jsp */
    public static final String PARAM_REQUEST_URL = "_param_request_url";
    public static final String PARAM_STORED_NAMES = "_param_stored_names";
    public static final String PARAM_PAGINATION_ID = "_param_pagination_id";

    public static final int DEFAULT_PAGE_SIZE = 15;
    public static final String DEFAULT_PAGINATION_ID = "__paging";

    private static final String TOTAL_FIELD = "_total";
    private static final String INDEX_FIELD = "_current_page";
    private static final String SIZE_FIELD = "_page_size";
    private static final String SORT_FIELD = "_sort_index";

    private static final String HTML_SORTING_DESC = "<img src=\"/images/descending.gif\" border=\"0\" title=\"descending\" />";
    private static final String HTML_SORTING_ASC = "<img src=\"/images/ascending.gif\" border=\"0\" title=\"ascending\" />";

    /** Don't let anyone instantiate this class. */
    private PaginationUtil()
    {}

    /**
     * Retrieve the name of the hidden field which transferring the "total count"
     * 
     * @param paginationId
     * @return
     */
    public static String getTotalField(String paginationId)
    {
        return StringHelper.isBlank(paginationId) ? DEFAULT_PAGINATION_ID + TOTAL_FIELD : paginationId.trim().concat(TOTAL_FIELD);
    }

    /**
     * Retrieve the name of the hidden field which transferring the "page size"
     * 
     * @param paginationId
     * @return
     */
    public static String getSizeField(String paginationId)
    {
        return StringHelper.isBlank(paginationId) ? DEFAULT_PAGINATION_ID + SIZE_FIELD : paginationId.trim().concat(SIZE_FIELD);
    }

    /**
     * Retrieve the name of the hidden field which transferring the "page index"
     * 
     * @param paginationId
     * @return
     */
    public static String getIndexField(String paginationId)
    {
        return StringHelper.isBlank(paginationId) ? DEFAULT_PAGINATION_ID + INDEX_FIELD : paginationId.trim().concat(INDEX_FIELD);
    }

    /**
     * Retrieve the name of the hidden field which transferring the "sort index"
     * 
     * @param paginationId
     * @return
     */
    public static String getSortField(String paginationId)
    {
        return StringHelper.isBlank(paginationId) ? DEFAULT_PAGINATION_ID + SORT_FIELD : paginationId.trim().concat(SORT_FIELD);
    }

    /**
     * Construct the "ORDER BY field1, field2..." string to use in the query sql.
     * 
     * @param fields the sort column names
     * @param sortIndex the sort indexs, the index starts with 1
     * @return
     */
	public static String constructOrderBy(String[] fields, String sortIndex) {
		return constructOrderBy(fields, sortIndex, null);
	}

	public static String constructOrderBy(String[] fields, String sortIndex, Set<Integer> requiredIndexes)
    {
        String result = "";
        if (!StringHelper.isBlank(sortIndex) && fields != null)
        {
            String[] indexs = decodeStoredNames(sortIndex);
			Set<Integer> usedIndexes = new HashSet<>();
            StringBuilder buffer = new StringBuilder();
            for (String index : indexs)
            {
                // ignore blank string
                if (StringHelper.isBlank(index))
                {
                    continue;
                }
                try
                {
                    int tempi = Integer.parseInt(index.trim());
                    boolean desc = (tempi < 0);
                    tempi = Math.abs(tempi);
                    if (tempi < 1 || tempi > (fields.length + 1))
                    {
                        log.warn(">>> sort index [" + index + "] in \"" + sortIndex + "\" is out of range.");
                        continue;
                    }
					if(!usedIndexes.add(tempi))
						continue;
                    // add a comma to connect the order bys
                    if (buffer.length() > 0)
                    {
                        buffer.append(',');
                    }
                    buffer.append(fields[tempi - 1]);
                    if (desc)
                    {
                        buffer.append(" DESC");
                    }
                }
                catch (NumberFormatException e)
                {
                    log.warn(">>> unknown sort index [" + index + "] found in \"" + sortIndex + "\".");
                    continue;
                }
            }
			if(requiredIndexes != null)
				for(Integer ri : requiredIndexes) {
					int tempi = ri.intValue();
					boolean desc = (tempi < 0);
					if(!usedIndexes.add(tempi))
						tempi = Math.abs(tempi);
					if(tempi < 1 || tempi > (fields.length + 1)) {
						log.warn(">>> sort index [" + ri + "] in \"" + sortIndex + "\" is out of range.");
						continue;
					}
					// add a comma to connect the order bys
					if(buffer.length() > 0) {
						buffer.append(',');
					}
					buffer.append(fields[tempi - 1]);
					if(desc) {
						buffer.append(" DESC");
					}
				}

            if (buffer.length() > 0)
            {
                result = " ORDER BY " + buffer.toString();
            }
        }
        return result;
    }

    /**
     * Gets the HTML string for showing the "sorting icon".
     * 
     * @param content the sorting content for this field
     * @param sortIndex the current sort index
     * @return
     */
    public static String getSortingIconHtml(String content, String sortIndex)
    {
        String result = "";
        if (!StringHelper.isBlank(content) && !StringHelper.isBlank(sortIndex) && isCurrentSorting(content, sortIndex))
        {
            result = sortIndex.startsWith("-") ? HTML_SORTING_DESC : HTML_SORTING_ASC;
        }
        return result;
    }

    private static boolean isCurrentSorting(String content, String sortIndex)
    {
        String temp1 = content.startsWith("-") ? content.substring(1) : content;
        String temp2 = sortIndex.startsWith("-") ? sortIndex.substring(1) : sortIndex;
        return temp1.equals(temp2);
    }

    /**
     * Gets the javascript string for the sorting link action.
     * 
     * @param paginationId
     * @param content the sorting content for this field
     * @param sortIndex the current sort index
     * @return
     */
    public static String getSortingScript(String paginationId, String content, String sortIndex)
    {
        String result = "#";
        paginationId = paginationId == null ? DEFAULT_PAGINATION_ID : paginationId;
        if (!StringHelper.isBlank(content))
        {
            String absoulteContent = content.startsWith("-") ? content.substring(1) : content;
            String temp = "javascript:sort_" + paginationId + "('";
            if (!StringHelper.isBlank(sortIndex) && isCurrentSorting(content, sortIndex))
            {
                result = sortIndex.startsWith("-") ? temp + absoulteContent + "')" : temp + '-' + absoulteContent + "')";
            }
            else
            {
                result = temp + content + "');";
            }
        }
        return result;
    }

    /**
     * Gets the start record No.
     * 
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public static int getStartNum(int pageSize, int pageIndex)
    {
        return (pageIndex - 1) * pageSize + 1;
    }
    
    /**
     * Gets the start record No.
     * 
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public static int getStartNum(Integer pageSize, Integer pageIndex)
    {
        //return (pageIndex - 1) * pageSize + 1;
    	return getStartNum(pageSize.intValue(), pageIndex.intValue());
    }

    /**
     * Gets the end record No.
     * 
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public static int getEndNum(int pageSize, int pageIndex)
    {
        return pageIndex * pageSize;
    }
    
    /**
     * Gets the end record No.
     * 
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public static int getEndNum(Integer pageSize, Integer pageIndex)
    {
        return getEndNum(pageSize.intValue(), pageIndex.intValue());
    }

    /**
     * Calculate the page count.
     * 
     * @param pageSize
     * @param total
     * @return
     */
    public static int getPageCount(int pageSize, int total)
    {
        if (total <= 0)
        {
            return 0;
        }
        if (total % pageSize > 0)
        {
            return (total / pageSize) + 1;
        }
        else
        {
            return total / pageSize;
        }
    }

    /**
     * Check to see if the link should be shown or not.
     * 
     * @param No the link No.
     * @param pageCount page count
     * @return
     */
    public static boolean showPageLink(int No, int pageCount)
    {
        if (No < 1 || No > 15 || No > pageCount)
        {
            return false;
        }
        return true;
    }

    /**
     * Calculate the "page number" of the showing link.
     * 
     * @param No the link No.
     * @param pageIndex current page index
     * @param pageCount page count
     * @return
     */
    public static int getLinkNum(int No, int pageIndex, int pageCount)
    {
        if (pageIndex <= 8 || pageCount <= 15)
        {
            return No;
        }
        else if (pageIndex > (pageCount - 7))
        {
            return ((pageCount - 15) + No);
        }
        else
        {
            return pageIndex + (No - 8);
        }
    }

    /**
     * Connect the names with the comma character.
     * 
     * @param names
     * @return
     */
    public static String encodeStoredNames(String[] names)
    {
        if (names == null)  return null;
        StringBuilder buffer = new StringBuilder();
        for (String name : names)
        {
            if (buffer.length() > 0)
            {
                buffer.append(',');
            }
            buffer.append(name);
        }
        return buffer.toString();
    }

    /**
     * Get the names of the stored parameters which connect with comma character.
     * 
     * @param source the formatted string includes the names, like "name1,name2,name3"
     * @return
     */
    public static String[] decodeStoredNames(String source)
    {
        if (source == null) return null;
        String[] result = null;
        if (!StringHelper.isBlank(source))
        {
            result = source.split(",");
        }
        return result;
    }

}
