package com.usatech.layers.common.util;

import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetUtils;
import simple.security.crypt.EncryptionInfoMapping;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class KeyMgrAccountUpdater {
	protected static Publisher<ByteInput> defaultPublisher;
	protected static ResourceFolder defaultResourceFolder;
	protected static String defaultMassUpdateAccountQueueKey = "usat.keymanager.account.update.enmasse";
	protected static final Cryption defaultKeyMgrCryption = new Cryption();
	protected static EncryptionInfoMapping defaultEncryptionInfoMapping;
	protected static final String[] header = new String[] { AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH.getValue(), AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), AuthorityAttrEnum.ATTR_TRACK_CRC.getValue(), AuthorityAttrEnum.ATTR_INSTANCE.getValue() };

	protected final Object[] data = new Object[header.length];
	protected final Resource resource;
	protected final byte[] encryptionKey;
	protected final ByteOutput output;
	protected final Publisher<ByteInput> publisher;
	protected final MessageChain messageChain;
	protected final EncryptionInfoMapping encryptionInfoMapping;
	protected final OutputStream out;
	protected int count = 0;

	public KeyMgrAccountUpdater() throws IOException, GeneralSecurityException, IllegalStateException {
		this(getDefaultPublisher(), getDefaultResourceFolder(), getDefaultMassUpdateAccountQueueKey(), getKeymgrCryption(), getDefaultEncryptionInfoMapping());
	}

	public KeyMgrAccountUpdater(Publisher<ByteInput> publisher, ResourceFolder resourceFolder, String massUpdateAccountQueueKey, Cryption keyMgrCryption, EncryptionInfoMapping encryptionInfoMapping) throws IOException, GeneralSecurityException, IllegalStateException {
		if(resourceFolder == null)
			throw new IllegalStateException("ResourceFolder is not set");
		if(publisher == null)
			throw new IllegalStateException("Publisher is not set");
		this.publisher = publisher;
		this.resource = resourceFolder.getResource("KeyMgrAccountUpdate_" + System.currentTimeMillis(), ResourceMode.CREATE);
		this.encryptionInfoMapping = encryptionInfoMapping;
		boolean okay = false;
		try {
			this.encryptionKey = keyMgrCryption.generateKey();
			messageChain = new MessageChainV11();
			MessageChainStep massUpdateStep = messageChain.addStep(massUpdateAccountQueueKey, true);
			massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resource.getKey());
			massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
			massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
			massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());
			MessageChainStep deleteFileStep = messageChain.addStep("usat.file.resource.delete");
			deleteFileStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
			deleteFileStep.setJoined(true);
			deleteFileStep.addWaitFor(massUpdateStep);
			massUpdateStep.setNextSteps(0, deleteFileStep);
			this.out = keyMgrCryption.createEncryptingOutputStream(encryptionKey, resource.getOutputStream());
			this.output = new OutputStreamByteOutput(out);
			DatasetUtils.writeHeader(output, header);
			okay = true;
		} finally {
			if(!okay) {
				abort();
			}
		}
	}

	public void addCard(byte[] accountCdHash, long globalAccountId, Integer trackCrc, short instance) throws IOException {
		data[0] = accountCdHash;
		data[1] = globalAccountId;
		data[2] = trackCrc;
		data[3] = instance;
		DatasetUtils.writeRow(output, data);
		count++;
	}

	public int send() throws IOException, ServiceException {
		if(count > 0) {
			DatasetUtils.writeFooter(output);
			output.flush();
			out.close();
			MessageChainService.publish(messageChain, publisher, encryptionInfoMapping);
		}
		resource.release();
		return count;
	}

	public void abort() {
		resource.delete();
		resource.release();
	}

	public static Publisher<ByteInput> getDefaultPublisher() {
		return defaultPublisher;
	}

	public static void setDefaultPublisher(Publisher<ByteInput> publisher) {
		KeyMgrAccountUpdater.defaultPublisher = publisher;
	}

	public static ResourceFolder getDefaultResourceFolder() {
		return defaultResourceFolder;
	}

	public static void setDefaultResourceFolder(ResourceFolder resourceFolder) {
		KeyMgrAccountUpdater.defaultResourceFolder = resourceFolder;
	}

	public static String getDefaultMassUpdateAccountQueueKey() {
		return defaultMassUpdateAccountQueueKey;
	}

	public static void setDefaultMassUpdateAccountQueueKey(String massUpdateAccountQueueKey) {
		KeyMgrAccountUpdater.defaultMassUpdateAccountQueueKey = massUpdateAccountQueueKey;
	}

	public static Cryption getKeymgrCryption() {
		return defaultKeyMgrCryption;
	}

	public static EncryptionInfoMapping getDefaultEncryptionInfoMapping() {
		return defaultEncryptionInfoMapping;
	}

	public static void setDefaultEncryptionInfoMapping(EncryptionInfoMapping defaultEncryptionInfoMapping) {
		KeyMgrAccountUpdater.defaultEncryptionInfoMapping = defaultEncryptionInfoMapping;
	}
}
