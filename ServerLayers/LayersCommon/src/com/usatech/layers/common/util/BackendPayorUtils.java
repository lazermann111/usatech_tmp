package com.usatech.layers.common.util;

import java.beans.IntrospectionException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.TranDeviceResultType;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Interceptor;
import simple.servlet.JspRequestDispatcherFactory;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;

public class BackendPayorUtils {
	private static final Log log = Log.getLog();

	protected static String chargeAttributes = "requireCVVMatch=false\nrequireAVSMatch=false\n";
	protected static String initialAttributes = "requireCVVMatch=false\nrequireAVSMatch=false\n";
	protected static final JspRequestDispatcherFactory jrdf = new JspRequestDispatcherFactory();
	protected static String jspPageDirectory = "/usalive-shared/";
	protected static final Set<String> SENSITIVE_PROPERTIES = new HashSet<String>();
	protected static final DateFormat INVOICE_DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy"));

	static {
		SENSITIVE_PROPERTIES.add("tokenHex");
	}

	public static EC2AuthResponse chargePayor(long payorId, String deviceSerialCd, long deviceTranCd, long cardId, byte[] token, String userName, String doingBusinessAs, BigDecimal chargeAmount, String chargeReference, Date invoiceDate, String chargeDesc) {
		StringBuilder cardData = new StringBuilder().append(cardId).append("|");
		StringUtils.appendHex(cardData, token);
		long amountInPennies = chargeAmount.movePointRight(2).longValue();
		if(!StringUtils.isBlank(chargeDesc))
			chargeDesc = chargeDesc.replace('|', '/');
		else if(!StringUtils.isBlank(doingBusinessAs))
			chargeDesc = "Online Charge By " + doingBusinessAs.replace('|', '/');
		else
			chargeDesc = "Online Charge";
		StringBuilder tranDetails = new StringBuilder("A0|205|").append(amountInPennies).append("|1|").append(chargeDesc);
		if(!StringUtils.isBlank(chargeReference))
			tranDetails.append("|403||1|").append(chargeReference.replace('|', ' '));
		if(invoiceDate != null)
			tranDetails.append("|409||1|").append(INVOICE_DATE_FORMAT.format(invoiceDate));
		tranDetails.append("|207||1|By ");
		if(!StringUtils.isBlank(userName))
			tranDetails.append(userName.replace('|', ' '));
		else
			tranDetails.append("UNKNOWN");
		log.info(new StringBuilder("Charging payor ").append(payorId).append(" for ").append(chargeAmount).append(" on card ").append(cardId));
		EC2AuthResponse response = EC2ClientUtils.getEportConnect().chargePlain(null, null, deviceSerialCd, deviceTranCd, amountInPennies, cardData.toString(), String.valueOf(EntryType.TOKEN.getValue()), String.valueOf(TranDeviceResultType.SUCCESS.getValue()), tranDetails.toString(), chargeAttributes);
		try {
			log.info("Received EC2 response: " + StringUtils.toStringProperties(response, SENSITIVE_PROPERTIES));
		} catch(IntrospectionException e) {
			log.warn("Could not read properties from EC2 response", e);
		}
		return response;
	}

	public static String getInitialAttributes() {
		return initialAttributes;
	}

	public static void setInitialAttributes(String initialAttributes) {
		BackendPayorUtils.initialAttributes = initialAttributes;
	}

	public static String getChargeAttributes() {
		return chargeAttributes;
	}

	public static void setChargeAttributes(String chargeAttributes) {
		BackendPayorUtils.chargeAttributes = chargeAttributes;
	}

	public static void sendEmail(String fromEmail, String fromName, String subject, String toEmail, String toName, String jspPage, Locale locale, Map<String, Object> attributes) throws ServletException, SQLException, DataLayerException {
		String body = Interceptor.intercept(jrdf, jrdf, jspPageDirectory + jspPage, locale, null, attributes);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fromEmail", fromEmail);
		params.put("fromName", fromName);
		params.put("subject", subject);
		params.put("toEmail", toEmail);
		params.put("toName", toName);
		params.put("body", body);
		DataLayerMgr.executeCall("SEND_EMAIL", params, true);
	}

	public static void sendReceipt(String payorName, String payorEmail, String baseUrl, long deviceTranCd, String deviceSerialCd, String maskedCardNumber, String cardType, long chargeTime, BigDecimal chargeAmount, String chargeDesc, Date invoiceDate, String chargeReference, String doingBusinessAs, String address, String city, String stateCd, String postalCd, String countryCd, String phone,
			String email, Locale locale) throws ServletException, SQLException, DataLayerException, ConvertException {
		Map<String, Object> attributes = new HashMap<String, Object>();
		// get logo id for customer
		attributes.put("chargeReference", chargeReference);
		attributes.put("chargeDesc", chargeDesc);
		attributes.put("chargeAmount", chargeAmount);
		attributes.put("chargeTime", chargeTime);
		attributes.put("invoiceDate", invoiceDate);
		attributes.put("cardType", cardType);
		attributes.put("cardNumber", maskedCardNumber);
		attributes.put("deviceSerialCd", deviceSerialCd);
		attributes.put("deviceTranCd", deviceTranCd);
		attributes.put("company", doingBusinessAs);
		attributes.put("address", address);
		attributes.put("city", city);
		attributes.put("stateCd", stateCd);
		attributes.put("postalCd", postalCd);
		attributes.put("countryCd", countryCd);
		attributes.put("phone", phone);
		attributes.put("email", email);
		sendReceipt(payorName, payorEmail, baseUrl, attributes, locale);
	}

	protected static void sendReceipt(String payorName, String payorEmail, String baseUrl, Map<String, Object> attributes, Locale locale) throws ServletException, SQLException, DataLayerException, ConvertException {
		// get logo id for customer
		// attributes.put("logoUrl", baseUrl + "/images/usa_tech_logo_trimmed.gif"); // "/repository/logo." + ext + "?id=" + id);
		// attributes.put("linkUrl", baseUrl);
		String company = ConvertUtils.getString(attributes.get("company"), false);
		String fromEmail = (StringUtils.isBlank(company) ? "usalive" : company.replaceAll("\\W+", "_").toLowerCase()) + "@usatech.com";
		BackendPayorUtils.sendEmail(fromEmail, StringUtils.isBlank(company) ? "USALive" : company, "Online Charge " + (StringUtils.isBlank(company) ? "from USALive" : "by " + company), payorEmail, payorName, "backoffice_receipt.jsp", locale, attributes);
	}

	public static String getJspPageDirectory() {
		return jspPageDirectory;
	}

	public static void setJspPageDirectory(String jspPageDirectory) {
		BackendPayorUtils.jspPageDirectory = jspPageDirectory;
	}
}
