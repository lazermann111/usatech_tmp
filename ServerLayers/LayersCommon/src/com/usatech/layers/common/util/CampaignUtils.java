package com.usatech.layers.common.util;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.OrdinalFormat;
import simple.text.StringUtils;

public class CampaignUtils {
	protected static final int MIN_DAY_OF_MONTH=1;
	protected static final int MAX_DAY_OF_MONTH=31;
	protected static final String DELIMITER=",";
	protected static final String DURATION_SYMBOL="-";
	protected static final Pattern MONTH_ALLOWED = Pattern.compile("^([1-9]|[12][0-9]|3[01]|(([1-9]|[12][0-9]|3[0])[-]([1-9]|[12][0-9]|3[01]))){1}([,]([1-9]|[12][0-9]|3[01]|(([1-9]|[12][0-9]|3[0])[-]([1-9]|[12][0-9]|3[01]))))*$");
	protected static final Pattern WEEK_ALLOWED = Pattern.compile("^([1-7]){1}([,][1-7])*$");
	protected static final Pattern HOUR_ALLOWED = Pattern.compile("^(([01]?[0-9]|2[0-3]):[0-5][0-9][-]([01]?[0-9]|2[0-3]):[0-5][0-9]){1}([,](([01]?[0-9]|2[0-3]):[0-5][0-9][-]([01]?[0-9]|2[0-3]):[0-5][0-9]))*$");
	protected static final Pattern DATES_ALLOWED= Pattern.compile("^([0-9]{2}/[0-9]{2}/[0-9]{4}){1}([,]([0-9]{2}/[0-9]{2}/[0-9]{4}))*$");
	public static final SimpleDateFormat HOUR_MIN_FORMAT = new SimpleDateFormat("HH:mm");
	public static final SimpleDateFormat READ_HOUR_MIN_FORMAT = new SimpleDateFormat("h:mm a");
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	protected static final SimpleDateFormat APPLY_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	/**
	 * Allowed is combination of single digit or duration notation like 1-2, delimited by comma.
	 * eg. 1,2,10-20
	 * @param month
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static boolean validateCampaignRecurScheduleMonth(String month) throws IllegalArgumentException{
		if(month==null||month.equals("")){
			throw new IllegalArgumentException("Day of Month is empty.");
		}else{
			ArrayList<Integer> days=new ArrayList<Integer>();
			if(MONTH_ALLOWED.matcher(month).matches()){
				try{
					String[] strArray=month.split(DELIMITER);
					for(String str:strArray){
						if(str.contains(DURATION_SYMBOL)){
							String[] strArray2=str.split(DURATION_SYMBOL);
							if(strArray2.length!=2){
								throw new IllegalArgumentException("Invalid month expression. Invalid duration.");
							}else{
								int dayStart=ConvertUtils.getInt(strArray2[0]);
								int dayEnd=ConvertUtils.getInt(strArray2[1]);
								if(dayStart>=dayEnd){
									throw new IllegalArgumentException("Invalid month expression. Invalid duration.");
								}else{
									if(dayStart<MIN_DAY_OF_MONTH||dayEnd>MAX_DAY_OF_MONTH){
										throw new IllegalArgumentException("Invalid month expression. Only number 1 to 31 is allowed.");
									}else{
										for(int i=dayStart;i<=dayEnd;i++){
											if(days.contains(i)){
												throw new IllegalArgumentException("Invalid month expression. There is overlap in the expression.");
											}else{
												days.add(i);
											}
										}
									}
								}
							}
						}else{
							int aDay=ConvertUtils.getInt(str);
							if(aDay<MIN_DAY_OF_MONTH||aDay>MAX_DAY_OF_MONTH){
								throw new IllegalArgumentException("Invalid month expression. Only number 1 to 31 is allowed.");
							}else{
								if(days.contains(aDay)){
									throw new IllegalArgumentException("Invalid month expression. There is overlap in it.");
								}else{
									days.add(aDay);
								}
							}
						}
					}
				}catch(ConvertException e){
					throw new IllegalArgumentException("Invalid month expression.");
				}
				
			}else{
				throw new IllegalArgumentException("Invalid month expression. Only valid day of month - and , are allowed.");
			}
			return true;
		}
	}
	/**
	 *  Allowed is single digit, delimited by comma.
	 *  1,2,3,4
	 * @param week
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static boolean validateCampaignRecurScheduleWeek(String week) throws IllegalArgumentException{
		if(week==null||week.equals("")){
			throw new IllegalArgumentException("Day of Week is empty.");
		}else{
			if(WEEK_ALLOWED.matcher(week).matches()){
				return true;
			}else{
				throw new IllegalArgumentException("Invalid week expression.");
			}
		}
	}
	
	public static boolean validateCampaignRecurScheduleDay(String days) throws IllegalArgumentException{
		if(days==null||days.equals("")){
			throw new IllegalArgumentException("Specific Days is empty.");
		}else{
			if(DATES_ALLOWED.matcher(days).matches()){
				try{
					String[] strArray=days.split(DELIMITER);
					ArrayList<Date> dateList=new ArrayList<Date>();
					Date aDate=null;
					for(String str: strArray){
						aDate=DATE_FORMAT.parse(str);
						if(dateList.contains(aDate)){
							throw new IllegalArgumentException("Invalid specific days expression.It contains duplidate dates.");
						}else{
							dateList.add(aDate);
						}
					}
					return true;
				}catch(ParseException e){
					throw new IllegalArgumentException("Invalid specific days expression.It contains invalid dates.");
				}
			}else{
				throw new IllegalArgumentException("Invalid specific days expression.");
			}
		}
	}
	
	public static class TimeDuration implements Comparable<TimeDuration>{
		protected Date startTime;
		protected Date endTime;
		protected TimeDuration(Date startTime, Date endTime) {
			super();
			if(startTime.after(endTime)){
				throw new IllegalArgumentException("Recurring schedule hour time duration start time is after end time.");
			}
			this.startTime = startTime;
			this.endTime = endTime;
		}
		
		public Date getStartTime() {
			return startTime;
		}

		public Date getEndTime() {
			return endTime;
		}

		public int compareTo(TimeDuration o){
			int i=startTime.compareTo(o.getStartTime());
			if(i==0){
				return endTime.compareTo(o.getEndTime());
			}else{
				return i;
			}
		}
	}
	/**
	 * Allowed is duration notation in 24hr format like 02:30-04:30, delimited by comma
	 * 09:00-11:00,14:00-16:00
	 * @param hour
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static boolean validateCampaignRecurScheduleHour(String hour) throws IllegalArgumentException{
		if(hour==null||hour.equals("")){
			return true;
		}else{
			try{
			if(HOUR_ALLOWED.matcher(hour).matches()){
				ArrayList<TimeDuration> timeList=new ArrayList<TimeDuration>();
				String[] strArray=hour.split(DELIMITER);
				for(String str:strArray){
					String[] strArray2=str.split(DURATION_SYMBOL);
					timeList.add(new TimeDuration(HOUR_MIN_FORMAT.parse(strArray2[0]),HOUR_MIN_FORMAT.parse(strArray2[1])));
				}
				Collections.sort(timeList);
				for(int i=1;i<timeList.size();i++){
					TimeDuration previous=timeList.get(i-1);
					TimeDuration current=timeList.get(i);
					if(previous.startTime.compareTo(current.startTime)==0){
						throw new IllegalArgumentException("Invalid hour expression. There is overlap duration.");
					}else{
						if(current.startTime.before(previous.endTime)&&current.startTime.after(previous.startTime)){
							throw new IllegalArgumentException("Invalid hour expression. There is overlap of duration.");
						}
					}
				}
				return true;
			}else{
				throw new IllegalArgumentException("Invalid hour expression.");
			}
			}catch(ParseException e){
				throw new IllegalArgumentException("Invalid hour expression.");
			}
		}
	}
	
	public static Date validateApplyCampaignTime(String applyingTimestamp) throws IllegalArgumentException{
		if(applyingTimestamp==null||applyingTimestamp.equals("")){
			throw new IllegalArgumentException("Appling timestamp is empty.");
		}else{
			try{
				return APPLY_DATE_FORMAT.parse(applyingTimestamp);
			}catch(ParseException e){
				throw new IllegalArgumentException("Invalid applying timestamp.");
			}
			
		}
	}

	protected static final Pattern RECUR_SCHEDULE_PATTERN = Pattern.compile("(?:D|(?:[WMS]:([^|]+)))(?:\\|(.*)?)?", Pattern.CASE_INSENSITIVE);
	protected static final Pattern NUMBER_OR_RANGE_PATTERN = Pattern.compile("\\s*(\\d+)\\s*(?:-\\s*(\\d+)\\s*)?");
	public static String readRecurSchedule(String recurSchedule) {
		if(recurSchedule == null || (recurSchedule = recurSchedule.trim()).equals(""))
			return "every day";
		Matcher matcher = RECUR_SCHEDULE_PATTERN.matcher(recurSchedule);
		if(!matcher.matches())
			throw new IllegalArgumentException("Invalid recurSchedule. Does not match pattern.");
		StringBuilder sb = new StringBuilder();
		switch(recurSchedule.charAt(0)) {
			case 'D':
			case 'd':
				sb.append("every day");
				break;
			case 'M':
			case 'm':
				sb.append("on the ");
				String[] daysArray = matcher.group(1).split(DELIMITER);
				for(int i = 0; i < daysArray.length; i++) {
					Matcher subMatcher = NUMBER_OR_RANGE_PATTERN.matcher(daysArray[i]);
					if(!subMatcher.matches())
						throw new IllegalArgumentException("Invalid recurSchedule. Day(s) of Month '" + daysArray[i] + "' is not valid.");
					int n = ConvertUtils.getIntSafely(subMatcher.group(1), 0);
					if(n <= 0 || n > 31)
						throw new IllegalArgumentException("Invalid recurSchedule. Day of Month '" + subMatcher.group(1) + "' is not valid.");
					if(i > 0) {
						if(i == daysArray.length - 1)
							sb.append(" and ");
						else
							sb.append(", ");
					}
					sb.append(n).append(OrdinalFormat.getSuffix(n));
					if(!StringUtils.isBlank(subMatcher.group(2))) {
						n = ConvertUtils.getIntSafely(subMatcher.group(2), 0);
						if(n <= 0 || n > 31)
							throw new IllegalArgumentException("Invalid recurSchedule. Day of Month '" + subMatcher.group(2) + "' is not valid.");
						sb.append(" - ").append(n).append(OrdinalFormat.getSuffix(n));
					}
				}
				sb.append(" of each month");
				break;
			case 'W':
			case 'w':
				sb.append("each ");
				daysArray = matcher.group(1).split(DELIMITER);
				DateFormatSymbols dfs = new DateFormatSymbols();
				String[] weekdayLookup = dfs.getWeekdays();
				for(int i = 0; i < daysArray.length; i++) {
					int n = ConvertUtils.getIntSafely(daysArray[i], 0);
					if(n <= 0 || n > 7)
						throw new IllegalArgumentException("Invalid recurSchedule. Day of Week '" + daysArray[i] + "' is not valid.");
					if(i > 0) {
						if(i == daysArray.length - 1)
							sb.append(" and ");
						else
							sb.append(", ");
					}
					sb.append(weekdayLookup[n]);
				}
				break;
			case 'S':
			case 's':
				sb.append("on ");
				String[] datesArray = matcher.group(1).split(DELIMITER);
				for(int i = 0; i < datesArray.length; i++) {
					if(i > 0) {
						if(i == datesArray.length - 1)
							sb.append(" and ");
						else
							sb.append(", ");
					}
					sb.append(datesArray[i]);
				}
				break;
		}
		String hours = matcher.group(2);
		if(!StringUtils.isBlank(hours)) {
			String[] hourArray = hours.split(DELIMITER);
			for(int i = 0; i < hourArray.length; i++) {
				String[] hourStartEnd = hourArray[i].split(DURATION_SYMBOL);
				String hourStart = hourStartEnd[0];
				String hourEnd = hourStartEnd[1];
				try {
					sb.append(" between ").append(CampaignUtils.READ_HOUR_MIN_FORMAT.format(CampaignUtils.HOUR_MIN_FORMAT.parse(hourStart)));
					sb.append(" and ").append(CampaignUtils.READ_HOUR_MIN_FORMAT.format(CampaignUtils.HOUR_MIN_FORMAT.parse(hourEnd)));
				} catch(ParseException e) {
					throw new IllegalArgumentException("Could not parse hours '" + hourArray[i] + "'", e);
				}
				if(i != hourArray.length - 1) {
					sb.append(" and");
				}
			}
		}
		return sb.toString();
	}
	
}
