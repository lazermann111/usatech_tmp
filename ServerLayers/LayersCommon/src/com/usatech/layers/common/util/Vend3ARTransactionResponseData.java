package com.usatech.layers.common.util;

public class Vend3ARTransactionResponseData {
	private byte [] trackTwoData;
	private byte [] tlvData;
	
	public Vend3ARTransactionResponseData(byte[] trackTwoData, byte[] tlvData) {
		super();
		this.trackTwoData = trackTwoData;
		this.tlvData = tlvData;
	}
	
	public byte[] getTrackTwoData() {
		return trackTwoData;
	}
	public byte[] getTlvData() {
		return tlvData;
	}
}
