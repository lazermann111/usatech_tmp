package com.usatech.layers.common.util;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.usatech.app.GenericAttribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.app.task.BridgeTask;
import com.usatech.layers.common.model.ConfigTemplateSetting;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.Holder;
import simple.mail.MXRecordMailHost;
import simple.mail.MXRecordMailHost.AddressVerification;
import simple.servlet.RecordRequestFilter;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.TimeUtils;

public class WebHelper {
	protected static String appRequestRecordQueueKey = "usat.app.request.record";
	protected static Publisher<ByteInput> defaultPublisher;
	protected static Set<String> truncatedParameterNames;
	protected static AddressVerification emailVerification = AddressVerification.MX_CONNECT;
	protected static String craneDevicePrefix="K3CMS";
	public static final String MILITARY_TIME_REGEX = "^(?:(?:[01][0-9]|2[0-3])(?:[0-5][0-9]))?$";
	public static final Pattern MILITARY_TIME_PATTERN = Pattern.compile(MILITARY_TIME_REGEX);
	protected static String environmentSuffix = "";
	protected static String environmentLetter;
	protected static Map<String, String> environmentMap = new HashMap<String, String>();
	static {		
		environmentMap.put("P", "(P) Production");
		environmentMap.put("E", "(E) Certification");
		environmentMap.put("C", "(C) Certification"); //TODO: Not sure about it, we have C, but what is C?
		environmentMap.put("I", "(I) Integration");
		environmentMap.put("D", "(D) Development");
		environmentMap.put("L", "(L) Local");
	}
	
	public static boolean isProductionVersion() {
		return StringHelper.isBlank(environmentSuffix);
	}
	
	protected static void appendOnChangeAtDomReady(StringBuilder input, ConfigTemplateSetting template, String onchange) {
		String deviceTypeId = template.getDeviceTypeId() == 0 ? "" : (String.valueOf(template.getDeviceTypeId()) + "_");
		input.append("<script type=\"text/javascript\">window.addEvent(\"domready\",function() {\n(function() {\n").append(onchange).append("\n}).bind($(\"cfg_").append(template.getCounter()).append("_cat_").append(deviceTypeId).append(template.getCategoryId()).append("\"))();\n});</script>\n");
	}
	public static String generateTextInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk) {
		return generateTextInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, null, "");
	}

	public static String generateTextInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange) {
		return generateTextInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, onchange, "");
	}

	public static String generateTextInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange, String keyPrefix) {
		StringBuilder input = new StringBuilder("<input type=\"text\"");
		input.append(" name=\"cfg_field_").append(keyPrefix).append(StringUtils.prepareCDATA(template.getKey())).append("\"");
		input.append(" id=\"cfg_").append(template.getCounter()).append("_cat_").append(keyPrefix).append(template.getCategoryId()).append("\"");
		input.append(" label=\"").append(StringUtils.prepareCDATA(template.getLabel())).append("\"");
		input.append(" value=\"").append(StringUtils.prepareCDATA(value)).append("\"");
		String regex = template.getRegex();
		if (!StringHelper.isBlank(regex))
			input.append(" valid=\"").append(StringUtils.prepareCDATA(regex)).append("\"");
		String[] option = editorDetails.split(" to ");		
		if (template.isRequired())
			input.append(" usatRequired=\"true\"");
		int to = ConvertUtils.getIntSafely(option[1], 200);
		input.append(" maxlength=\"").append(to).append("\"");
		int size = to + 5;
		if (size > 65) 
			size = 65;
		input.append(" size=\"").append(size).append("\"");
		if (template.getMinValue() != null)
			input.append(" minValue=\"").append(template.getMinValue()).append("\"");
		if (template.getMaxValue() != null)
			input.append(" maxValue=\"").append(template.getMaxValue()).append("\"");
		if(bulk) {
			input.append(" onchange=\"document.getElementById(this.id.replace('cfg_', 'cb_')).checked = true;");
			if(!StringUtils.isBlank(onchange))
				input.append(' ').append(StringUtils.prepareCDATA(onchange));
			input.append('"');
		} else if(!StringUtils.isBlank(onchange)) {
			input.append(" onchange=\"").append(StringUtils.prepareCDATA(onchange)).append('"');
		}
		if (!supported)
			input.append(" disabled=\"disabled\"");
		input.append(" />\n");
		if(!StringUtils.isBlank(onchange))
			appendOnChangeAtDomReady(input, template, onchange);
		return input.toString();
	}
	
	public static String generateSelectInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk) {
		return generateSelectInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, null, null, "");
	}

	public static String generateSelectInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange, Holder<Boolean> optionalHolder) {
		return generateSelectInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, null, optionalHolder, "");
	}

	public static String generateSelectInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange, Holder<Boolean> optionalHolder, String keyPrefix) {
		StringBuilder input = new StringBuilder("<select");
		input.append(" name=\"cfg_field_").append(keyPrefix).append(StringUtils.prepareCDATA(template.getKey())).append("\"");
		input.append(" id=\"cfg_").append(template.getCounter()).append("_cat_").append(keyPrefix).append(template.getCategoryId()).append("\"");
		input.append(" label=\"").append(StringUtils.prepareCDATA(template.getLabel())).append("\"");
		if(bulk) {
			input.append(" onchange=\"document.getElementById(this.id.replace('cfg_', 'cb_')).checked = true;");
			if(!StringUtils.isBlank(onchange))
				input.append(' ').append(StringUtils.prepareCDATA(onchange));
			input.append('"');
		} else if(!StringUtils.isBlank(onchange)) {
			input.append(" onchange=\"").append(StringUtils.prepareCDATA(onchange)).append('"');
		}
		if(!supported)
			input.append(" disabled=\"disabled\"");
		int pos = input.length();
		input.append(">");		
		if(StringUtils.isBlank(value))
			value = defaultValue;
		String[] options = editorDetails.split(";", -1);

		boolean needValue = !StringUtils.isBlank(value);
		boolean optional = false;
		boolean onlyIfSelected = false;
		String[] option;
		for(String item : options) {
			if(StringUtils.isBlank(item)) {
				onlyIfSelected = true;
				continue;
			}
			option = item.split("=", -1);
			if (option.length < 2)
				continue;
			if(onlyIfSelected) {
				if(!option[0].equals(value))
					continue;
			} else if(StringUtils.isBlank(option[0]))
				optional = true;
			input.append("<option value=\"").append(StringUtils.prepareCDATA(option[0])).append("\"");
			if(option[0].equals(value)) {
				input.append(" selected=\"selected\"");
				needValue = false;
			}
			input.append(">").append(StringUtils.prepareHTMLBlank(option[1])).append("</option>\n");
			if(onlyIfSelected)
				break;
		}
		if(needValue)
			input.append("<option value=\"").append(StringUtils.prepareCDATA(value)).append("\" disabled=\"true\" selected=\"selected\">").append(StringUtils.prepareHTMLBlank(value)).append(" [Unsupported]</option>\n");
		if(!optional)
			input.insert(pos, " usatRequired=\"true\"");

		input.append("</select>\n");
		if(!StringUtils.isBlank(onchange))
			appendOnChangeAtDomReady(input, template, onchange);
		if(optionalHolder != null)
			optionalHolder.setValue(optional);
		return input.toString();
	}
	
	public static String generateBitmapInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk) {
		return generateBitmapInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, null, "");
	}

	public static String generateBitmapInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange) {
		return generateBitmapInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, onchange, "");
	}

	public static String generateBitmapInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange, String keyPrefix) {
		StringBuilder input = new StringBuilder();		
		String[] options = editorDetails.split(";", -1);
		int intValue = StringHelper.isInteger(value) ? Integer.valueOf(value) : 0;		
		String[] option;
		for (int i=0; i<options.length; i++) {
			String item = options[i];
			option = item.split("=", -1);
			input.append("<input type=\"checkbox\"");
			input.append(" name=\"cfg_field_").append(keyPrefix).append(StringUtils.prepareCDATA(template.getKey())).append("\"");
			input.append(" id=\"cfg_").append(template.getCounter()).append("_cat_").append(keyPrefix).append(template.getCategoryId()).append("_").append(StringUtils.prepareCDATA(option[0])).append("\"");
			input.append(" value=\"").append(StringUtils.prepareCDATA(option[0])).append("\"");
			if(bulk) {
				input.append(" onchange=\"document.getElementById('cb_").append(template.getCounter()).append("_cat_").append(template.getCategoryId()).append("').checked = true;");
				if(!StringUtils.isBlank(onchange))
					input.append(' ').append(StringUtils.prepareCDATA(onchange));
				input.append('"');
			} else if(!StringUtils.isBlank(onchange)) {
				input.append(" onchange=\"").append(StringUtils.prepareCDATA(onchange)).append('"');
			}
			int intOptionValue = Integer.valueOf(option[0]);
			if ((intOptionValue & intValue) == intOptionValue)
				input.append(" checked=\"checked\"");
			if (!supported)
				input.append(" disabled=\"disabled\"");
			input.append("> ").append(StringUtils.prepareHTMLBlank(option[1])).append("<br/>\n");
		}
		if(!StringUtils.isBlank(onchange))
			appendOnChangeAtDomReady(input, template, onchange);
		return input.toString();
	}
	
	public static String generateScheduleInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange, Holder<Boolean> optionalHolder, String keyPrefix) {
		return generateScheduleInput(template, editorDetails, value, deviceTypeId, 0, supported, defaultValue, bulk, onchange, optionalHolder, keyPrefix);
	}
	
	public static String generateScheduleInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, int plv, boolean supported, String defaultValue, boolean bulk, String onchange, Holder<Boolean> optionalHolder, String keyPrefix) {
		StringBuilder input = new StringBuilder();
		
		String idSuffix = keyPrefix + StringUtils.prepareCDATA(template.getKey());
		String cfgSuffix = template.getCounter() + "_cat_" + keyPrefix + template.getCategoryId();
		
		input.append("<div style=\"display:inline-block;vertical-align:top;\">\n");
		input.append("<select");
		input.append(" name=\"cfg_field_").append(idSuffix).append("\"");
		input.append(" id=\"cfg_").append(cfgSuffix).append("\"");
		input.append(" label=\"").append(StringUtils.prepareCDATA(template.getLabel())).append("\"");
		input.append(" style=\"vertical-align:top;\"");
		input.append(" onchange=\"change_schedule(this);");
		if(bulk)
			input.append(" document.getElementById(this.id.replace('cfg_', 'cb_')).checked = true;");
		if(!StringUtils.isBlank(onchange))
			input.append(' ').append(StringUtils.prepareCDATA(onchange));
		input.append('"');
		if(!supported)
			input.append(" disabled=\"disabled\"");
		int pos = input.length();
		input.append(">");
		String[] options = editorDetails.split(";", -1);
		String[] valueOptions = value.split("\\^", -1);
		String scheduleType = valueOptions[0];

		boolean needValue = !StringUtils.isBlank(value);
		boolean optional = false;
		boolean onlyIfSelected = false;
		boolean usesDailyTimeList = false;
		boolean supportsDailyTimeList = (deviceTypeId == 13 && plv > 0 && plv < 20) ? false : true; // G9 less than PLV 20 should not show daily time list
		String[] option;
		for(String item : options) {
			if(StringUtils.isBlank(item)) {
				onlyIfSelected = true;
				continue;
			}
			option = item.split("=", -1);
			if (option.length < 2)
				continue;
			if ("E".equals(option[0]) && !supportsDailyTimeList)
				continue;
			if ("E".equals(option[0]))
				usesDailyTimeList=true;
			if(onlyIfSelected) {
				if(!option[0].equals(value))
					continue;
			} else if(StringUtils.isBlank(option[0])) {
				optional = true;
				if (template.isRequired())
					continue;
			}
			input.append("<option value=\"").append(StringUtils.prepareCDATA(option[0])).append("\"");
			
			if(option[0].equals(scheduleType)) {
				input.append(" selected=\"selected\"");
				needValue = false;
			}
			if(!supported)
				input.append(" disabled=\"disabled\"");	
			
			input.append(">").append(StringUtils.prepareHTMLBlank(option[1])).append("</option>");
			
			if(onlyIfSelected)
				break;
		}
		
		if(needValue)
			input.append("<option value=\"").append(StringUtils.prepareCDATA(value)).append("\" disabled=\"true\" selected=\"selected\">").append(StringUtils.prepareHTMLBlank(value)).append(" [Unsupported]</option>\n");
		if(!optional)
			input.insert(pos, " usatRequired=\"true\"");

		input.append("</select>&nbsp;");
		
		String paramOnChange; 
		if (bulk)
			paramOnChange = new StringBuilder("\"document.getElementById('cb_").append(cfgSuffix).append("').checked = true;\"").toString();
		else
			paramOnChange = "";
			
		input.append("<span id=\"span_time_").append(idSuffix).append("\"");
		if (StringHelper.isBlank(scheduleType) || (scheduleType.equals("E") && plv >= 20))
			input.append(" style=\"display:none\"");
		input.append(">Start Time: <input type=\"text\" maxlength=\"4\" class=\"time\" value=\"");
		if (valueOptions.length > 1)
			input.append(StringUtils.pad(valueOptions[1], '0', 4, Justification.LEFT));
		else
			input.append("0300");
		input.append("\" name=\"sch_time_").append(idSuffix).append("\"");
		if (bulk)
			input.append(" onchange=").append(paramOnChange);
		input.append(" label=\"Time\" valid=\"").append(MILITARY_TIME_REGEX).append("\" usatRequired=\"true\" /></span>");
	
		input.append("&nbsp;<span id=\"span_W_").append(idSuffix).append("\"");
		if (!"W".equalsIgnoreCase(scheduleType))
			input.append(" style=\"display:none\"");
		input.append(">");
		List<String> days = null;
		if (valueOptions.length > 2)
			days = Arrays.asList(valueOptions[2].split("\\|", -1));
		for (int i = 0; i < 7; i++) {
			input.append("<input type=\"checkbox\" name=\"sch_day_").append(idSuffix).append("\"");
			if (bulk)
				input.append(" onclick=").append(paramOnChange);
			if (days != null && days.contains(String.valueOf(i)))
					input.append(" checked=\"checked\"");
			input.append(" value=\"").append(i).append("\" />");
			if (i == 0)
				input.append("Sun");
			else if (i == 1)
				input.append("Mon");
			else if (i == 2)
				input.append("Tue");
			else if (i == 3)
				input.append("Wed");
			else if (i == 4)
				input.append("Thu");
			else if (i == 5)
				input.append("Fri");
			else if (i == 6)
				input.append("Sat");
			input.append("&nbsp;");
		}
		input.append("</span>");
			
		input.append("<span id=\"span_I_").append(idSuffix).append("\"");
		if (!"I".equalsIgnoreCase(scheduleType))
			input.append(" style=\"display:none\"");
		input.append(">");
		input.append("&nbsp;Every <input type=\"text\" maxlength=\"4\" class=\"time\" value=\"");
		if ("I".equalsIgnoreCase(scheduleType) && valueOptions.length > 2)
			input.append(valueOptions[2]);
		else
			input.append("720");
		input.append("\" name=\"sch_min_").append(idSuffix).append("\"");
		if (bulk)
			input.append(" onchange=").append(paramOnChange);
		int minMinutes = isProductionVersion() ? 180 : 5;
		input.append(" label=\"Interval Minutes\" valid=\"^[0-9]+\" usatRequired=\"true\" minValue=\"").append(minMinutes).append("\" maxValue=\"1439\" /> Minutes</span>\n");
		
		if (usesDailyTimeList) {
			input.append("<span id=\"span_E_").append(idSuffix).append("\"");
			if (!"E".equalsIgnoreCase(scheduleType))
				input.append(" style=\"display:none\"");
			else
				input.append(" style=\"display:inline-block\"");
			input.append(">");
			input.append(WebHelper.generateTimelistInput(template, editorDetails, value, deviceTypeId, supported, defaultValue, bulk, onchange, optionalHolder, keyPrefix, false));
			input.append("</span>\n");
		}
		
		input.append("</div>\n");
		
		if(!StringUtils.isBlank(onchange))
			appendOnChangeAtDomReady(input, template, onchange);
		if(optionalHolder != null)
			optionalHolder.setValue(optional);
		return input.toString();
	}
	
	// I'm sorry, this is awful
	public static String generateTimelistInput(final ConfigTemplateSetting template, String editorDetails, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, String onchange, Holder<Boolean> optionalHolder, String keyPrefix, boolean writeScheduleType) {
		StringBuilder input = new StringBuilder();
		
		String idSuffix = keyPrefix + StringUtils.prepareCDATA(template.getKey());
		String cfgSuffix = template.getCounter() + "_cat_" + keyPrefix + template.getCategoryId();
		
		if (writeScheduleType) {
			input.append("<input type=\"hidden\" name=\"cfg_field_").append(idSuffix).append("\"");
			input.append(" id=\"cfg_").append(cfgSuffix).append("\"");
			input.append(" value=\"E\"/>");
		}
		input.append("<table>");
		int i=0;
		String[] valueOptions = null;
		String[] timeList = null;
		if(!StringUtils.isBlank(value) && value.contains("^")) {
			valueOptions = value.split("\\^");
			timeList = valueOptions[1].split("\\|");
		} else {
			timeList = new String[] {"0300"};
		}
		
		for(;i<timeList.length;i++){
			String elementId = "cfg_" + cfgSuffix + "_" + i;
			input.append("<tr id='tr_").append(elementId).append("'><td><input");
			input.append(" name=\"sch_timelist_").append(idSuffix).append("\"");
			input.append(" id=\"").append(elementId).append("\"");
			input.append(" label=\"").append(StringUtils.prepareCDATA(template.getLabel())).append("\" valid=\"").append(MILITARY_TIME_REGEX).append("\"");
			input.append(" class=\"time\"");
			input.append(" usatRequired=\"true\"");
			input.append(" value=\"").append(StringUtils.prepareCDATA(timeList[i])).append("\"/>");
			if (i > 0)
				input.append("<input type=\"button\" onclick=\"timeCount_").append(idSuffix).append("=timeCount_").append(idSuffix).append("-1;document.getElementById('add_time_button_").append(idSuffix).append("').disabled=(timeCount_").append(idSuffix).append(">=6);document.getElementById('tr_").append(elementId).append("').parentNode.removeChild(document.getElementById('tr_").append(elementId).append("'));\" value=\"Delete\" />");
			input.append("</td></tr>");
		}
		
		input.append("<tr id='addDexTime_").append(idSuffix).append("'><td><input id=\"add_time_button_").append(idSuffix).append("\" type=\"button\" onclick=\"addNewTime_").append(idSuffix).append("();\" value=\"Add Time\" /></td></tr>");
		input.append("</table>\n");
		
//		StringBuilder newTime=new StringBuilder();
//		newTime.append("<input");
//		newTime.append(" name=\"sch_timelist_").append(keyPrefix).append(StringUtils.prepareCDATA(template.getKey())).append("\"");
//		newTime.append(" label=\"").append(StringUtils.prepareCDATA(template.getLabel())).append("\"");
//		newTime.append(" valid=\"").append(MILITARY_TIME_REGEX).append("\"");
//		newTime.append(" value=\"\"/>");
//		newTime.append(" <input type=\"button\" onclick=\"document.getElementById(newTrId).remove();\" value=\"Delete\" />");
		
		input.append("<script type=\"text/javascript\">")
			.append("var newTimeCount_").append(idSuffix).append("=").append(i).append(";\n")
			.append("var timeCount_").append(idSuffix).append("=newTimeCount_").append(idSuffix).append(";\n")
			.append("function addNewTime_").append(idSuffix).append("(){\n")
			.append("	var newTrId='tr_"+cfgSuffix+"_'+newTimeCount_").append(idSuffix).append(";\n")
			.append("	var trEl=new Element('tr',{'id':newTrId});\n")
			.append("	var tdEl=new Element('td');\n")
			.append("	var newInput1 = new Element('input', {")
			.append("		'name': \"sch_timelist_"+keyPrefix+StringUtils.prepareCDATA(template.getKey())+"\",\n")
			.append("		'label': \""+StringUtils.prepareCDATA(template.getLabel())+"\",\n")
			.append("		'valid': \"").append(MILITARY_TIME_REGEX).append("\",\n")
			.append("		'class': \"time").append("\",\n")
			.append("		'usatRequired': \"true\",\n")
			.append("		'value': \"\"\n")
			.append("	});\n")
			.append("	var newInput2 = new Element('input', {\n")
			.append("		'type': \"button\",\n")
			.append("		'value': \"Delete\",\n")
			.append("		'onclick': \"timeCount_").append(idSuffix).append("=timeCount_").append(idSuffix).append("-1;document.getElementById('add_time_button_").append(idSuffix).append("').disabled=(timeCount_").append(idSuffix).append(">=6);document.getElementById('\"+newTrId+\"').parentNode.removeChild(document.getElementById('\"+newTrId+\"'));\"\n")
			.append("	});\n")
			.append("	tdEl.adopt(newInput1);\n")
			.append("	tdEl.adopt(newInput2);\n")
			.append("	trEl.adopt(tdEl);\n")
			.append("	trEl.inject($('addDexTime_").append(idSuffix).append("'),'before');\n")
			.append("	newTimeCount_").append(idSuffix).append("=newTimeCount_").append(idSuffix).append("+1;\n")
			.append("	timeCount_").append(idSuffix).append("=timeCount_").append(idSuffix).append("+1;\n")
			.append("	document.getElementById('add_time_button_").append(idSuffix).append("').disabled=(timeCount_").append(idSuffix).append(">=6);\n")
			.append("}\n")
			.append("document.getElementById('add_time_button_").append(idSuffix).append("').disabled=(timeCount_").append(idSuffix).append(">=6);</script>\n");
		
		return input.toString();
	}
	
	public static String buildSchedule(HttpServletRequest request, Map<String, Object> paramMap, ConfigTemplateSetting defaultSetting, String key, String scheduleType, int deviceUtcOffsetMin) throws ServletException {
		if (StringHelper.isBlank(scheduleType))
			return "";
		StringBuilder sb = new StringBuilder(scheduleType).append("^");
		String time;
		if (paramMap != null)
			time = ConvertUtils.getStringSafely(paramMap.get(new StringBuilder("sch_time_").append(key).toString()), "0300");
		else
			time = ConvertUtils.getStringSafely(request.getParameter(new StringBuilder("sch_time_").append(key).toString()), "0300");
		if (!MILITARY_TIME_PATTERN.matcher(time).matches())
			throw new ServletException("Invalid " + (defaultSetting == null ? key : defaultSetting.getName()) + " time");	
		if (scheduleType.equalsIgnoreCase("D")) {
			sb.append(time);
		} else if (scheduleType.equalsIgnoreCase("I")) {
			int utcOffsetSec = TimeUtils.hhmmToUtcOffsetSeconds(time, deviceUtcOffsetMin);
			sb.append(utcOffsetSec).append("^");
			String intervalMin;
			if (paramMap != null)
				intervalMin = ConvertUtils.getStringSafely(paramMap.get(new StringBuilder("sch_min_").append(key).toString()), "720");
			else
				intervalMin = ConvertUtils.getStringSafely(request.getParameter(new StringBuilder("sch_min_").append(key).toString()), "720");
			int minMinutes = isProductionVersion() ? 60 : 5;
			if (!StringHelper.isInteger(intervalMin) || Integer.valueOf(intervalMin) < minMinutes || Integer.valueOf(intervalMin) > 1439)
				throw new ServletException("Invalid " + (defaultSetting == null ? key : defaultSetting.getName()) + " Interval Minutes");
			int intervalSec = TimeUtils.intervalMinutesToSeconds(intervalMin);
			sb.append(intervalSec);
		} else if (scheduleType.equalsIgnoreCase("W")) {
			sb.append(time).append("^");
			String[] days;
			if (paramMap != null)
				days = (String[]) paramMap.get(new StringBuilder("sch_day_").append(key).toString());
			else
				days = request.getParameterValues(new StringBuilder("sch_day_").append(key).toString());
			if (days != null && days.length > 0) {
				for (int i = 0; i < days.length; i++) {
					if (days[i].indexOf("|") > -1) {
						sb.append(days[i]);
						break;
					}
					if (i > 0)
						sb.append("|");
					sb.append(days[i]);
				}
			} else
				sb.append("0");
		} else if (scheduleType.equalsIgnoreCase("E")) {
			String[] times;
			if (paramMap != null)
				times = (String[]) paramMap.get(new StringBuilder("sch_timelist_").append(key).toString());
			else
				times = request.getParameterValues(new StringBuilder("sch_timelist_").append(key).toString());
			if (times != null && times.length > 0) {
				for(String t : times) {
					if (!MILITARY_TIME_PATTERN.matcher(t).matches()) {
						throw new ServletException("Invalid " + (defaultSetting == null ? key : defaultSetting.getName()) + " time: " + t);
					}
				}
				times = Arrays.stream(times).distinct().sorted().toArray(s -> new String[s]);
				for(int i=0; i<times.length; i++) {
					if (i > 0)
						sb.append("|");
					sb.append(times[i]);
				}
			} else {
				sb.append(time); // fallback default
			}
		}
		return sb.toString();
	}
	
	public static String buildSchedule(HttpServletRequest request, ConfigTemplateSetting defaultSetting, String key, String scheduleType, int deviceUtcOffsetMin) throws Exception {
		return buildSchedule(request, null, defaultSetting, key, scheduleType, deviceUtcOffsetMin);
	}
	
	public static String buildIntervalScheduleValue(String value, int deviceUtcOffsetMin) {
		String[] valueParts = value.split("\\^", -1);
		if (valueParts.length == 3 && StringHelper.isLong(valueParts[1]) && StringHelper.isLong(valueParts[2])) {
			String hhmm = TimeUtils.utcTimeOffsetSecondsToHHMM(valueParts[1], deviceUtcOffsetMin);
			int intervalMinutes = TimeUtils.intervalSecondsToMinutes(valueParts[2]);
			return new StringBuilder("I^").append(hhmm).append("^").append(intervalMinutes).toString();
		} else
			return value;
	}

	protected static Pattern EDITOR_REGEX = Pattern.compile("([^:]+)(?::((?:[^\\\\]*(?:\\\\\\\\)*)*))?(?:\\\\(.*))?");

	public static String generateInput(final ConfigTemplateSetting template, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk) {
		return generateInput(template, value, deviceTypeId, supported, defaultValue, bulk, null, "");
	}
	
	public static String generateInput(final ConfigTemplateSetting template, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, Holder<Boolean> optionalHolder) {
		return generateInput(template, value, deviceTypeId, supported, defaultValue, bulk, optionalHolder, "");
	}
	
	public static String generateInput(final ConfigTemplateSetting template, String value, int deviceTypeId, boolean supported, String defaultValue, boolean bulk, Holder<Boolean> optionalHolder, String keyPrefix) {
		return generateInput(template, value, deviceTypeId, 0, supported, defaultValue, bulk, optionalHolder, keyPrefix);
	}
	
	public static String generateInput(final ConfigTemplateSetting template, String value, int deviceTypeId, int plv, boolean supported, String defaultValue, boolean bulk, Holder<Boolean> optionalHolder, String keyPrefix) {
		Matcher matcher = EDITOR_REGEX.matcher(template.getEditor());
		if(!matcher.matches())
			return "";
		String inputType = matcher.group(1);
		String options = matcher.group(2);
		String onchange = matcher.group(3);
		if (inputType.equalsIgnoreCase("TEXT"))
			return generateTextInput(template, options, value, deviceTypeId, supported, defaultValue, bulk, onchange, keyPrefix);
		else if (inputType.equalsIgnoreCase("SELECT"))
			return generateSelectInput(template, options, value, deviceTypeId, supported, defaultValue, bulk, onchange, optionalHolder, keyPrefix);
		else if (inputType.equalsIgnoreCase("BITMAP"))
			return generateBitmapInput(template, options, value, deviceTypeId, supported, defaultValue, bulk, onchange, keyPrefix);
		else if (inputType.equalsIgnoreCase("SCHEDULE")){
			if(!StringUtils.isBlank(options) && (options.startsWith("E=") && plv >= 20)) {
				return generateTimelistInput(template, options, value, deviceTypeId, supported, defaultValue, bulk, onchange, optionalHolder, keyPrefix, true);
			} else {
				return generateScheduleInput(template, options, value, deviceTypeId, plv, supported, defaultValue, bulk, onchange, optionalHolder, keyPrefix);
			}
		}
		else
			return "";
	}
	
	public static String generateDefaultValueString(final ConfigTemplateSetting template, String defaultValue, String description) {
		String defaultString = defaultValue;
		Matcher matcher = EDITOR_REGEX.matcher(template.getEditor());
		if(matcher.matches()) {
			String inputType = matcher.group(1);
			if (inputType.equalsIgnoreCase("SELECT")) {
				String[] options = matcher.group(2).split(";", -1);
				String[] option;
				for(String item : options) {
					option = item.split("=", -1);
					if (option.length < 2)
						continue;
					if(option[0].equals(defaultValue))
						defaultString = option[1];
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		if(!StringUtils.isBlank(description)) {
			if (description.contains("<br"))
				sb.append("<br />");
			else {
				if (!description.endsWith("."))
					sb.append(".");
				sb.append(" ");
			}
		}
		sb.append("Default: ").append(defaultString);
		return sb.toString();
	}
	
	private static void cleanParameters(Map<String, String> parameters) {
		if (truncatedParameterNames == null || truncatedParameterNames.size() < 1 || parameters == null || parameters.size() < 1)
			return;
		for (String key: truncatedParameterNames) {
			if (parameters.get(key) != null)
				parameters.put(key, new StringBuilder("<").append(parameters.get(key).length()).append(" bytes>").toString());
		}
	}
	
	public static void publishAppRequestRecord(CallInputs ci, Map<String, String> attributes, Log log) throws ServiceException {
		if (StringHelper.isBlank(getAppRequestRecordQueueKey())) {
			log.error("appRequestRecordQueueKey is empty");
			return;
		}
		if (StringHelper.isBlank(ci.getAppCd())) {
			log.error("appCd is empty");
			return;
		}
		
		MessageChain mc = new MessageChainV11();
		
		MessageChainStep step;
		if ("USALive".equalsIgnoreCase(ci.getAppCd()))
			step = mc.addStep(getAppRequestRecordQueueKey());
		else {
			step = mc.addStep("usat.bridge.to.msr");
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, getAppRequestRecordQueueKey());
		}
		
		step.setAttribute(new GenericAttribute("appCd"), ci.getAppCd());
		step.setAttribute(new GenericAttribute("currentTime"), ci.getCurrentTime());
		step.setAttribute(new GenericAttribute("duration"), ci.getDuration());
		if (ci.getExceptionText() != null)
			step.setAttribute(new GenericAttribute("exceptionText"), ci.getExceptionText());
					
		if (ci.getActionName() != null)
			step.setAttribute(new GenericAttribute("actionName"), ci.getActionName());
		
		if (ci.getObjectTypeCd() != null)
			step.setAttribute(new GenericAttribute("objectTypeCd"), ci.getObjectTypeCd());
		if (ci.getObjectCd() != null)
			step.setAttribute(new GenericAttribute("objectCd"), ci.getObjectCd());
		
		step.setAttribute(new GenericAttribute("requestURL"), ci.getRequestURL());
		step.setAttribute(new GenericAttribute("remoteAddr"), ci.getRemoteAddr());
		step.setAttribute(new GenericAttribute("remotePort"), ci.getRemotePort());
		step.setAttribute(new GenericAttribute("sessionId"), ci.getSessionId());
		
		if (ci.getHeaders() != null && ci.getHeaders().size() > 0) {
			step.setAttribute(new GenericAttribute("headers"), ci.getHeaders().toString());
			if (ci.getHeaders().get("referer") != null)
				step.setAttribute(new GenericAttribute("referer"), ci.getHeaders().get("referer"));
			if (ci.getHeaders().get("user-agent") != null)
				step.setAttribute(new GenericAttribute("userAgent"), ci.getHeaders().get("user-agent"));
		}
		
		if(ci.getUser() != null)
			step.setAttribute(new GenericAttribute("userName"), ci.getUser().getUserName());
		
		if (ci.getExplicitParameters() != null && ci.getExplicitParameters().size() > 0)
			step.setAttribute(new GenericAttribute("explicitParameters"), ci.getExplicitParameters().toString());
		
		if (ci.getParameters() != null && ci.getParameters().size() > 0) {
			HashMap<String, String> parameters = new HashMap<String, String>(ci.getParameters());
			cleanParameters(parameters);
			step.setAttribute(new GenericAttribute("parameters"), parameters.toString());
			if (ci.getParameters().get("requestHandler") != null)
				step.setAttribute(new GenericAttribute("requestHandler"), ci.getParameters().get("requestHandler"));
			if (ci.getParameters().get("folioId") != null)
				step.setAttribute(new GenericAttribute("folioId"), ci.getParameters().get("folioId"));
			if (ci.getParameters().get("reportId") != null)
				step.setAttribute(new GenericAttribute("reportId"), ci.getParameters().get("reportId"));
		}
		
		if (attributes != null && attributes.size() > 0) {
			for (String key: attributes.keySet())
				step.setAttribute(new GenericAttribute(key), attributes.get(key));
		}
		
		MessageChainService.publish(mc, defaultPublisher);
	}
	
	public static void publishAppRequestRecord(String appCd, HttpServletRequest request, RecordRequestFilter rrf, RecordRequestFilter.CallInputs ci, long startTsMs, String actionName, String objectTypeCd, String objectCd, Map<String, Object> explicitParameters, Map<String, String> attributes, Log log) throws ServiceException {
		ci.setDuration(System.currentTimeMillis() - startTsMs);
		rrf.updateWithRequest(ci, request);
		ci.setAppCd(appCd);
		ci.setActionName(actionName);
		ci.setObjectTypeCd(objectTypeCd);
		ci.setObjectCd(objectCd);
		if (explicitParameters != null && explicitParameters.size() > 0)
			ci.setExplicitParameters(explicitParameters);
		publishAppRequestRecord(ci, attributes, log);
	}
	
	public static void publishAppRequestRecord(String appCd, HttpServletRequest request, RecordRequestFilter rrf, RecordRequestFilter.CallInputs ci, long startTsMs, String actionName, String objectTypeCd, String objectCd, Log log) throws ServiceException {
		publishAppRequestRecord(appCd, request, rrf, ci, startTsMs, actionName, objectTypeCd, objectCd, null, null, log);
	}

	public static String getAppRequestRecordQueueKey() {
		return appRequestRecordQueueKey;
	}

	public static void setAppRequestRecordQueueKey(String appRequestRecordQueueKey) {
		WebHelper.appRequestRecordQueueKey = appRequestRecordQueueKey;
	}

	public static Publisher<ByteInput> getDefaultPublisher() {
		return defaultPublisher;
	}

	public static void setDefaultPublisher(Publisher<ByteInput> defaultPublisher) {
		WebHelper.defaultPublisher = defaultPublisher;
	}

	public static Set<String> getTruncatedParameterNames() {
		return truncatedParameterNames;
	}

	public static void setTruncatedParameterNames(Set<String> truncatedParameterNames) {
		WebHelper.truncatedParameterNames = truncatedParameterNames;
	}
	
	public static void checkEmail(String email) throws MessagingException {
		InternetAddress[] ias = InternetAddress.parse(email);
		switch(ias.length) {
			case 0:
				throw new AddressException("Blank ");
			case 1:
				MXRecordMailHost.verifyAddress(ias[0], getEmailVerification());
				return;
			default:
				throw new AddressException("Too many addresses");
		}
	}
	
	public static AddressVerification getEmailVerification() {
		return emailVerification;
	}

	public static void setEmailVerification(AddressVerification emailVerification) {
		WebHelper.emailVerification = emailVerification;
	}
	
	public static boolean isDailyExtendedSchedule(ConfigTemplateSetting configTemplateSetting){
		return configTemplateSetting!=null&&configTemplateSetting.getEditor()!=null&&configTemplateSetting.getEditor().equalsIgnoreCase("SCHEDULE:E=Daily");
	}
	
	public static String buildScheduleDailyExtended(String paramValue){
		if(!StringUtils.isBlank(paramValue)){
			String[] paramValueList=paramValue.split(",");
			StringBuilder paramValueSb=new StringBuilder("E^");
			for(String paramValueListValue:paramValueList){
				paramValueSb.append(paramValueListValue).append("|");
			}
			paramValue=paramValueSb.substring(0,paramValueSb.length()-1);
		}
		return paramValue;
	}
	
	public static boolean isCraneDevice(String deviceSerialCd){
		if(!StringUtils.isBlank(deviceSerialCd)&&deviceSerialCd.startsWith(craneDevicePrefix)){
			return true;
		}else{
			return false;
		}
	}
	
	public static String generateHourOptions(String selectName, String selectId, int selectedHour, boolean hourUseText){
		StringBuilder sb=new StringBuilder();
		sb.append("<select ");
		if(selectName!=null){
			sb.append("name='").append(selectName).append("'");
		}
		if(selectId!=null){
			sb.append(" id='").append(selectId).append("' ");
		}
		sb.append(">").append("\n");
		String hourText;
		for(int hour=0;hour<24;hour++){
			if(hour==0){
				hourText="12 am";
			}else if(hour==12){
				hourText="12 pm";
			}else if(hour<12){
				hourText=hour+" am";
			}else{
				hourText=(hour-12)+" pm";
			}
			if(selectedHour==hour){
				sb.append("<option selected='selected' value='").append(hour).append("'>").append(hourUseText?hourText:hour).append("</option>").append("\n");
			}else{
				sb.append("<option value='").append(hour).append("'>").append(hourUseText?hourText:hour).append("</option>").append("\n");
			}
		}
		sb.append("</select>").append("\n");
		return sb.toString();
	}
	
	public static String generateDayOrMinOptions(String selectName, String selectId, int num, int selectedDay){
		StringBuilder sb=new StringBuilder();
		sb.append("<select ");
		if(selectName!=null){
			sb.append("name='").append(selectName).append("'");
		}
		if(selectId!=null){
			sb.append(" id='").append(selectId).append("' ");
		}
		sb.append(">").append("\n");
		String dayText;
		for(int day=1;day<num+1;day++){
			if(day<10){
				dayText="0"+day;
			}else{
				dayText=String.valueOf(day);
			}
			if(selectedDay==day){
				sb.append("<option selected='selected' value='").append(day).append("'>").append(dayText).append("</option>").append("\n");
			}else{
				sb.append("<option value='").append(day).append("'>").append(dayText).append("</option>").append("\n");
			}
		}
		sb.append("</select>").append("\n");
		return sb.toString();
	}
	
	public static String generateMonthOptions(String selectName, String selectId, int month, String[] monthNames){
		StringBuilder sb=new StringBuilder();
		sb.append("<select ");
		if(selectName!=null){
			sb.append("name='").append(selectName).append("'");
		}
		if(selectId!=null){
			sb.append(" id='").append(selectId).append("' ");
		}
		sb.append(">").append("");
		for(int i=0;i<monthNames.length-1;i++){	
			if(month==i){
				sb.append("<option selected='selected' value='").append(i).append("'>").append(monthNames[i]).append("</option>").append("");
			}else{
				sb.append("<option value='").append(i).append("'>").append(monthNames[i]).append("</option>").append("");
			}
		}
		sb.append("</select>").append("\n");
		return sb.toString();
	}
	
	public static String convertDateToLocalStr(Date date) {
		SimpleDateFormat formater = new SimpleDateFormat();
		return formater.format(date);
	}

	public static Date getDate(int year, int month, int day, int hour,int min,int sec){
		Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, year);
		date.set(Calendar.MONTH, month);
		date.set(Calendar.DAY_OF_MONTH, day);
		date.set(Calendar.HOUR_OF_DAY, hour);
		date.set(Calendar.MINUTE, min);
		date.set(Calendar.SECOND, sec);
		return date.getTime();
	}

	public static Date getDate(int year, int month, int day, int hour,int min,int sec, String timeZoneGuid){
		Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, year);
		date.set(Calendar.MONTH, month);
		date.set(Calendar.DAY_OF_MONTH, day);
		date.set(Calendar.HOUR_OF_DAY, hour);
		date.set(Calendar.MINUTE, min);
		date.set(Calendar.SECOND, sec);
		date.setTimeZone(TimeZone.getTimeZone(timeZoneGuid));
		return date.getTime();
	}
	public static String getEnvironmentSuffix() {
		return environmentSuffix;
	}
	public static void setEnvironmentSuffix(String environmentSuffix) {
		WebHelper.environmentSuffix = environmentSuffix;
		if (StringHelper.isBlank(environmentSuffix))
			environmentLetter = "P";
		else if (environmentSuffix.startsWith("-") && environmentSuffix.length() > 1)
			environmentLetter = environmentSuffix.substring(1, 2).toUpperCase();
	}

	public static String getEnvironmentLetter() {
		return environmentLetter;
	}
	
	public static String getEnvironment(String envLetter) {
		String rv =  environmentMap.get(envLetter);
		return rv == null ? envLetter : rv;
	}
	
	public static String getCurrentEnvironment() {
		return getEnvironment(environmentLetter);
	}
	
}
