package com.usatech.layers.common.util;

public class VendXEnhancedEncryptedMSRData {
	private byte [] ksn;
	private byte [] track2;
	private int track2Length;
	
	public VendXEnhancedEncryptedMSRData(byte[] ksn, byte[] track2, int track2Length) {
		super();
		this.ksn = ksn;
		this.track2 = track2;
		this.track2Length = track2Length;
	}
	public byte[] getKsn() {
		return ksn;
	}
	public byte[] getTrack2() {
		return track2;
	}
	public int getTrack2Length() {
		return track2Length;
	}

}
