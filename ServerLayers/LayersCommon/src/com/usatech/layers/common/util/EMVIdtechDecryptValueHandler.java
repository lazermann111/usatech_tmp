package com.usatech.layers.common.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;

import com.usatech.layers.common.constants.TLVTag;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.io.TLVParser;
import simple.io.TLVParser.ValueHandler;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;

public class EMVIdtechDecryptValueHandler implements ValueHandler {
	protected static final TLVParser TLV_PARSER = new TLVParser();
	protected static final Log log = Log.getLog();
	
	private ByteBuffer byteBuffer;
	private byte [] trackTwoData;
	private Map<String, byte[]> values;
	private Pattern acctPattern;
	private Charset charset;
	private String keyPrefix;
	
	private Cipher cipher;
	
	public EMVIdtechDecryptValueHandler(ByteBuffer byteBuffer, Cipher cipher, Map<String, byte[]> values, Pattern acctPattern, Charset charset, String keyPrefix) {
		this.byteBuffer = byteBuffer;
		this.cipher = cipher;
		this.values = values;
		this.acctPattern = acctPattern;
		this.charset = charset;
		this.keyPrefix = keyPrefix;
	}
	
	public EMVIdtechDecryptValueHandler(ByteBuffer byteBuffer, Cipher cipher, Map<String, byte[]> values, Pattern acctPattern, Charset charset) {
		this.byteBuffer = byteBuffer;
		this.cipher = cipher;
		this.values = values;
		this.acctPattern = acctPattern;
		this.charset = charset;
		this.keyPrefix = "";
	}
	
	public EMVIdtechDecryptValueHandler(ByteBuffer byteBuffer, Cipher cipher, Map<String, byte[]> values) {
		this.byteBuffer = byteBuffer;
		this.cipher = cipher;
		this.values = values;
	}
	
	@Override
	public void handle(byte[] key, byte[] value, boolean mask, boolean encryption) throws IOException {
		boolean encryptedValue = Arrays.equals(key, TLVTag.IDTECH_MSR_EQUIVALENT_DATA.getValue());
		if (cipher != null && (encryption || encryptedValue)) {
			try {
				value = cipher.doFinal(value);
				if (!encryptedValue) {
					// the tlv tag, length, and value are in the decrypted result.  The tlv length is being used to distinguish data from padding
					final Map<byte[], byte[]> map = new HashMap<>();
					TLVParser.handleTag(value, 0, value.length, new ValueHandler() {
						@Override
						public void handle(byte[] key, byte[] value, boolean mask, boolean encryption) throws IOException {
							map.put(key, value);
						}
					});
					Object [] entryList = map.entrySet().toArray();
					if (entryList != null && entryList.length > 0) {
						@SuppressWarnings("unchecked")
						Map.Entry<byte [], byte []>entry = (Entry<byte[], byte[]>) entryList[0];
						value = entry.getValue();
						TLVParser.formatTagValue(byteBuffer, key, value);
					}
				}
			} catch (GeneralSecurityException e) {
				throw new IOException("Decryption issue: ", e);
			}
		} else {
			TLVParser.formatTagValue(byteBuffer, key, value);
		}
		
		// Good touch point for diagnostics while parsing tags - jms - 1/7/16
		/*try {
			String newValue = StringUtils.toHex(value);
			String keyString = StringUtils.toHex(key);
			String tlvTagName = TLVTag.getByValue(key).toString();
			log.error("DEBUG: key - " + keyString + "(" + tlvTagName + "), encryption: " + encryption + ", value - " + newValue);
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		if (trackTwoData == null && acctPattern != null && (
				Arrays.equals(key, TLVTag.IDTECH_MSR_EQUIVALENT_DATA.getValue())
				|| Arrays.equals(key, TLVTag.TRACK2_EQUIVALENT_DATA.getValue())
				|| Arrays.equals(key, TLVTag.IDTECH_VEND_TRACK2_MAGSTRIPE.getValue())
				|| Arrays.equals(key, TLVTag.PAYPASS_TRACK2_DATA.getValue()))) {
				extractCardData(value);
		}
		
		if (StringUtils.isBlank(keyPrefix))
			values.put(StringUtils.toHex(key), value);
		else
			values.put(new StringBuilder(keyPrefix).append(StringUtils.toHex(key)).toString(), value);
		
		TLVTag tag;
		try {
			tag = TLVTag.getByValue(key);
		} catch (InvalidValueException e) {
			log.warn("Unknown TLVTag: " + StringUtils.toHex(key));
			return;
		}

		if (tag.isContainer()) {
			EMVIdtechDecryptValueHandler valueHandler = processRecursively(value, tag.getKeyPrefix());
			if (trackTwoData == null && acctPattern != null) 
				trackTwoData = valueHandler.getTrackTwoData();
		}
	}
	
	protected EMVIdtechDecryptValueHandler processRecursively(byte[] value, String keyPrefix) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(value.length);
		EMVIdtechDecryptValueHandler valueHandler = new EMVIdtechDecryptValueHandler(buffer, cipher, values, acctPattern, charset, keyPrefix);
		TLV_PARSER.parse(value, 0, value.length, valueHandler);
		return valueHandler;
	}
	
	protected EMVIdtechDecryptValueHandler processRecursively(byte[] value) throws IOException {
		return processRecursively(value, "");
	}

	public byte [] getTrackTwoData() {
		return trackTwoData;
	}

	protected void extractCardData(byte[] value) {
		if (value == null)
			return;
		byte[] data;
		String acctData;
		if (ByteArrayUtils.isBCDSeparator(value))
			data = ByteArrayUtils.fromBCD(value);
		else
			data = value;
		if (charset == null)
			acctData = new String(data);
		else
			acctData = charset.decode(ByteBuffer.wrap(data)).toString();
		Matcher matcher = TLVParser.TRACK2_PATTERN.matcher(acctData);
		if (acctPattern.matcher(acctData).matches() && matcher.matches()) {
			byte[] trackLengths = values.get(TLVTag.IDTECH_MSR_EQUIVALENT_DATA_LENGTHS.getHexValue());
			if (trackLengths != null && trackLengths.length > 1 && trackLengths[1] > 0x00) {
				trackTwoData = new byte[trackLengths[1]];
				System.arraycopy(data, trackLengths[0], trackTwoData, 0, trackLengths[1]);
			} else {
				if (charset == null)
					trackTwoData = matcher.group(1).getBytes();
				else
					trackTwoData = matcher.group(1).getBytes(charset);
			}
		}
	}
}
