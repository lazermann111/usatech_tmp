package com.usatech.layers.common.util;

import simple.text.StringUtils;

public class VendXEMVData {

	private byte [] ksn;
	private byte [] trackTwoData;
    private byte [] tlvData;
    
	protected static int extractEncryptedTrack(int maskedTrackLength, int padding) {
		int trackLength = 0;
		if (maskedTrackLength > 0) {
			int remainder = maskedTrackLength % padding;
			trackLength = maskedTrackLength + (padding - remainder);
		}
		return trackLength;
	}
	
	public static String encodeChipCardData(byte [] tlvData) {
		StringBuilder sb = new StringBuilder();
		String chipCardDataLength = Integer.toString(tlvData.length * 2);
		sb.append(chipCardDataLength);
		sb.append(encode(tlvData));
		return sb.toString();
	}

	public static String encode(byte [] value) {
		String result = StringUtils.toHex(value);
		return result;
	}

	public static int bit55DataSize(byte [] chipCardTlvData) {
		return chipCardTlvData.length * 2;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("KSN: ");
		sb.append(StringUtils.toHex(getKsn()));
		sb.append(", trackTwoData: ");
		sb.append(StringUtils.toHex(getTrackTwoData()));
		sb.append(", TLV Data: ");
		sb.append(StringUtils.toHex(getTlvData()));
		return sb.toString();
	}

	public byte[] getTrackTwoData() {
		return trackTwoData;
	}
	public void setTrackTwoData(byte[] trackTwoData) {
		this.trackTwoData = trackTwoData;
	}

	public byte[] getTlvData() {
		return tlvData;
	}
	public void setTlvData(byte[] tlvData) {
		this.tlvData = tlvData;
	}


	public byte[] getKsn() {
		return ksn;
	}

	public void setKsn(byte[] ksn) {
		this.ksn = ksn;
	}
}
