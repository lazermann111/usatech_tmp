/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.layers.common.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import simple.io.Base64;
import simple.io.ByteArrayUtils;

/**
 * 
 */
public final class StringHelper
{
	public static final Pattern CONFIG_FILE_NAME_PATTERN = Pattern.compile("^(?:EV|TD)[0-9]{6}-CFG$");
	public static final Pattern HEX_PATTERN = Pattern.compile("^[0-9A-Fa-f]+$");
	public static final Pattern PRINTABLE_ASCII_EXPRESSION = Pattern.compile("^[\\x09\\x0A\\x0D\\x20-\\x7E\\xA1-\\xFF]*$");
	
    /** Don't let anyone instantiate this class. */
    private StringHelper()
    {}
    
    /**
     * Check to see if the given string is <code>null</code>, empty string("") or only contains
     * whitespace(s).
     * 
     * @param str
     * @return
     */
    public static boolean isBlank(String str)
    {
        return str == null || str.trim().length() == 0;
    }
    
    public static boolean isHex(String s) {
    	if (isBlank(s))
    		return false;
    	return HEX_PATTERN.matcher(s).matches();
    }

    /**
     * Encode the source ASCII string to the hex string.
     * 
     * @param sourceText
     * @return
     */
    public static final String encodeHexString(String sourceText)
    {
    	return simple.text.StringUtils.toHex(sourceText);
    }

    /**
     * Decode the hex string to ASCII string.
     * 
     * @param hexText
     * @return
     */
    public static final String decodeHexString(String hexText)
    {
    	if (hexText.contains(" "))
    		hexText = hexText.replaceAll(" ", "0");
    	try {
    		return new String(ByteArrayUtils.fromHex(hexText));
    	} catch (Exception e) {
    		return "";
    	}
    }
    
    public static String padE4(String ssn){    	
    	if(!isBlank(ssn)) {
    		ssn = ssn.trim();    			
	    	if (ssn.length() <= 6) {
	    		StringBuilder newSSN = new StringBuilder("E4");
	    		newSSN.append("0000000".substring(0, 6-ssn.length())).append(ssn);
	    		return newSSN.toString();
	    	}
    	}
    	return ssn;
    }
    
    public static String rightPad(String source, int finalLength, char padChar) {
    	StringBuilder sb = new StringBuilder(source);
    	while (sb.length() < finalLength)
    		sb.append(padChar);
    	return sb.toString();
    }
    
	 /**
	  * Get the number of occurrences a word or phrase appears in a string.
	  * @param source
	  * @param word
	  * @return
	  */
    public static int countMatchesInString(String source, String word){
    	//trim the string, front and back
    	//if the first indexof == 0 (at the begining) or (indexof(-1) == "\n" or "\r\n"))
    	return StringUtils.countMatches(source, word);
    }
    
    /**
	  * Get the number of occurrences a word or phrase appears in a string.
	  * @param source
	  * @param word
	  * @return
	  */
   public static int countParamNameMatchesInString(String str, String param){
	   
	   int totalParamMatches = 0;
	   if((!StringHelper.isBlank(str)) && (!StringHelper.isBlank(param))){
		   String s = str.trim();
	       int initialMatch = StringUtils.countMatches(s, param);
	       
	       int beginIndex = 0;
	       int lf = 0;
	       int sp1 = 0;
	       if(initialMatch>1){
		       	beginIndex = s.indexOf(param);
		       	lf = StringUtils.countMatches(s, "\n" + param);
		       	
		       	sp1 = StringUtils.countMatches(s, "\n " + param);
		       	
		       	
		       	if(beginIndex==0)
		       		totalParamMatches++;
		       	
		       	try{
			       	if(lf>0){
			       		int si = 0;
			       		for(int i = 1;i<=lf;i++){
			       			si = StringUtils.ordinalIndexOf(s, param, i);
			       			int l = (param).length();
			       			String sub = StringUtils.substring(s, (si-1), si+l);
			       			if(sub.equalsIgnoreCase("\n"+param)){
			       				totalParamMatches++;
			       			}
			       		}
			       		
			       	}
		       	}catch(Exception e){
		       		//no-op
		       	}
		       	
		       	try{
			       	if(sp1>0){
			       		int si = 0;
			       		for(int i = 1;i<=sp1;i++){
			       			si = StringUtils.ordinalIndexOf(s, " " + param, i);
			       			int l = (" " + param).length();
			       			String sub = StringUtils.substring(s, (si-1), si+l);
			       			if(sub.equalsIgnoreCase("\n "+param)){
			       				totalParamMatches++;
			       			}
			       		}
			       		
			       	}
		       	}catch(Exception e){
		       		//no-op
		       	}
		       	
	       	
	       }else{
	    	   totalParamMatches = initialMatch;
	       }
	   }
       return totalParamMatches;
   }
   
   public static String encodeRequestMap(Map<String, String[]> requestParamMap) {
	   HashMap<String, String[]> paramMap = new HashMap<String, String[]>(requestParamMap);
	   String encoded = Base64.encodeObject(paramMap);
	   if (encoded != null)
		   return encoded.replaceAll("\\s", "");
	   else
		   return "";
   }
   
   @SuppressWarnings("unchecked")
   public static HashMap<String, Object> decodeRequestMap(final String encodedMap) {
	   if (StringHelper.isBlank(encodedMap))
		   return new HashMap<String, Object>();
	   Object decoded = Base64.decodeToObject(encodedMap);
	   if (decoded != null)
		   return (HashMap<String, Object>) decoded;
	   else
		   return new HashMap<String, Object>();
   }
   
	public static boolean validateRegexWithMessage(Map<String, String> errorMap, String value, String name, String regexPattern, String errorMessage) {
		Pattern p = Pattern.compile(regexPattern);
		// Create a matcher with an input string
		Matcher m = p.matcher(value);
		if(!m.find()) {
			errorMap.put(errorMessage, "value: \"" + value + "\"");
			return false;
		}
		return true;
	}
	
	public static boolean isNumeric(String str)   
	{   
	    try {
	    	Double.parseDouble(str);   
	    } catch(Exception e) {   
	    	return false;
	    }   
	    return true;   
	}
	
	public static boolean isInteger(String str)   
	{   
	    try {
	    	Integer.parseInt(str);   
	    } catch(Exception e) {   
	    	return false;
	    }   
	    return true;   
	}
	
	public static boolean isLong(String str)   
	{   
	    try {
	    	Long.parseLong(str);   
	    } catch(Exception e) {   
	    	return false;
	    }   
	    return true;
	}
	
    public static boolean isMilitaryTime(String time) {
		if (StringHelper.isBlank(time))
			return false;
		if (time.length() != 4)
			return false;
		int timeNumber = Integer.valueOf(time);
		if (timeNumber < 0 || timeNumber > 2359)
			return false;
		return true;
	}
    
    public static boolean isDataPrintable(final String data) {
		return PRINTABLE_ASCII_EXPRESSION.matcher(data).matches();
	}
    
    public static boolean equalConfigValues(String value1, String value2, boolean compareNumericValues) {
    	if (value1 == null)
    		return value2 == null;
    	if (compareNumericValues && isNumeric(value1) && isNumeric(value2))
    		return new BigDecimal(value1).compareTo(new BigDecimal(value2)) == 0;
    	return value1.equals(value2);
    }
}
