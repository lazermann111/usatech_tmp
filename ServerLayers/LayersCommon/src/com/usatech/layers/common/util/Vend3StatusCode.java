package com.usatech.layers.common.util;

public class Vend3StatusCode {
	public static final byte OK = 0x00;
	public static final byte TIMEOUT = 0x08;
	public static final byte CRC_ERROR_IN_FRAME = 0x04;
	public static final byte REQUEST_ONLINE_AUTH = 0x23;
	public static final byte REQUEST_ONLINE_AUTH_CONTACT_EMV_ONLY = 0x30;
	public static final byte REQUEST_ONLINE_PIN = 0x31;
	public static final byte REQUEST_SIGNATURE = 0x32;
	public static final byte NO_ADVICE_REVERSAL_REQUIRED_DECLINED = 0x36;
	public static final byte FAILED = 0x0a;
}
