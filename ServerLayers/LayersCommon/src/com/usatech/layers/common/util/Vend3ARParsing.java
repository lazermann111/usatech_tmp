package com.usatech.layers.common.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import simple.io.Log;
import simple.io.TLVParser;
import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.TLVTag;

public class Vend3ARParsing {
	private static final Log log = Log.getLog();

	private static int TRACK1_LENGTH_LENGTH = 1;
	private static int TRACK2_LENGTH_LENGTH = 1;
	private static int TRACK3_LENGTH_LENGTH = 1;
	private static int STATUS_CODE_OFFSET = 11;
	private static int DATA_START_OFFSET = 14;
	public static int CRC_LENGTH = 2;
	private static int ONLINE_PIN_LENGTH_LENGTH = 1;
	
	protected static final TLVParser TLV_PARSER = new TLVParser();

	public static Vend3ARTransactionResponseData parseAnyARResponseData(byte []entireResponse) throws IOException {
		Vend3ARTransactionResponseData result = null;
		if (entireResponse != null && entireResponse.length > 0) {
			byte statusCode = entireResponse[STATUS_CODE_OFFSET];
			if (statusCode == Vend3StatusCode.OK || statusCode == Vend3StatusCode.REQUEST_ONLINE_AUTH) {
				result = parseLeading3TrackResponseData(entireResponse, DATA_START_OFFSET);
			} else if (statusCode == Vend3StatusCode.NO_ADVICE_REVERSAL_REQUIRED_DECLINED || statusCode == Vend3StatusCode.REQUEST_ONLINE_PIN) {
				result = parseTLVOnlyResponseData(entireResponse, DATA_START_OFFSET);
			} else if (statusCode == Vend3StatusCode.REQUEST_ONLINE_AUTH_CONTACT_EMV_ONLY) {
				result = parsePINAndTLVResponseData(entireResponse, DATA_START_OFFSET);
			}
		}
		return result;
	}
	
	/*
	 * See 730-1002-09 AR 2 1 5 IDG.pdf pages 79 (Table 21) and 88 (Table 25).
	 */
	public static Vend3ARTransactionResponseData parseLeading3TrackResponseData(byte [] responseData, int start) throws IOException {
		byte [] trackTwoData = null;
		byte [] tlvData = null;
		Vend3ARTransactionResponseData data = null;
		if (responseData != null && responseData.length > 0) {
			int trackOneLength = 0;
			int trackTwoLength = 0;
			int trackThreeLength = 0;
			
			trackOneLength = responseData[0 + start] & 0xff;
			int trackTwoLengthOffset = TRACK1_LENGTH_LENGTH + trackOneLength;
			trackTwoLength = responseData[trackTwoLengthOffset + start] & 0xff;
			int trackTwoDataOffset = TRACK1_LENGTH_LENGTH + trackOneLength + TRACK2_LENGTH_LENGTH;
			trackTwoData = new byte [trackTwoLength];
			System.arraycopy(responseData, trackTwoDataOffset + start, trackTwoData, 0, trackTwoLength);
			
			int trackThreeLengthOffset = trackTwoDataOffset + trackTwoLength;
			trackThreeLength = responseData[trackThreeLengthOffset + start] & 0xff;
			
			int tlvOffset = trackThreeLengthOffset + TRACK3_LENGTH_LENGTH + trackThreeLength;
			int tlvLength = responseData.length - tlvOffset - start - CRC_LENGTH;			
			
			tlvData = new byte[tlvLength];
			System.arraycopy(responseData, tlvOffset + start, tlvData, 0, tlvLength);
			
//			byte [] trackTwoEquivData = null;
//			Map<byte [], byte []>map = TLV_PARSER.parse(tlvData, 0, tlvData.length);
//			for (Map.Entry<byte [], byte []> entry: map.entrySet()) {
//				if (isTrack2EquivalentData(entry)) {
//					trackTwoEquivData = entry.getValue();
//				} else if (Arrays.equals(entry.getKey(), TLVTag.MASTERCARD_DATA_RECORD.getValue())) {
//					Map<byte [], byte []>mcDataRecordMap = TLV_PARSER.parse(entry.getValue(), 0, entry.getValue().length);
//					for (Map.Entry<byte [], byte[]>mcEntry: mcDataRecordMap.entrySet()) {
//						if (isTrack2EquivalentData(mcEntry)) {
//							trackTwoEquivData = mcEntry.getValue();
//						}
//					}
//				}
//			}
//			data = new Vend3ARTransactionResponseData(trackTwoData, trackTwoEquivData, tlvData);
			if (trackTwoLength < 1) {
				trackTwoData = parseTrackTwoDataFromTlv(tlvData, 0);
			}
			data = new Vend3ARTransactionResponseData(trackTwoData, tlvData);
		}
		return data;
	}

	/*
	 * See 730-1002-09 AR 2 1 5 IDG.pdf pages 90 and 91 section 7.1.2.7.
	 */
	public static Vend3ARTransactionResponseData parseTLVOnlyResponseData(byte [] responseData, int start) throws IOException {
		
		int tlvLength = responseData.length - start - CRC_LENGTH;
		byte [] tlvData = new byte [tlvLength]; 
		System.arraycopy(responseData, start, tlvData, 0, tlvLength);
		
		byte [] trackTwoData = parseTrackTwoDataFromTlv(tlvData, 0);		
		
		Vend3ARTransactionResponseData data = new Vend3ARTransactionResponseData(trackTwoData, tlvData);
		return data;
	}	

	/*
	 * See 730-1002-09 AR 2 1 5 IDG.pdf page 91 section 7.1.2.6.
	 */
	public static Vend3ARTransactionResponseData parsePINAndTLVResponseData(byte [] responseData, int start) throws IOException {
		byte onlinePinLength = responseData[start];
		int tlvOffset = onlinePinLength + start + ONLINE_PIN_LENGTH_LENGTH;
		
		int tlvLength = responseData.length - tlvOffset - CRC_LENGTH;	
		byte [] tlvData = new byte [tlvLength];
		System.arraycopy(responseData, tlvOffset, tlvData, 0, tlvLength);
		
		byte [] trackTwoData = parseTrackTwoDataFromTlv(tlvData, 0);

		Vend3ARTransactionResponseData data = new Vend3ARTransactionResponseData(trackTwoData, tlvData);
		return data;
	}	
	
	public static byte [] dataOnlyFromEntireResponse(byte [] entireResponse) {
		int CRC_LENGTH = 2;
		int dataLength = entireResponse.length - DATA_START_OFFSET - CRC_LENGTH;
		byte [] result = new byte [dataLength];
		System.arraycopy(entireResponse, DATA_START_OFFSET, result, 0, dataLength);
		return result;
	}
	
	private static byte [] parseTrackTwoDataFromTlv(byte []tlvData, int offset) throws IOException {
		byte [] trackTwoEquivData = null;
		Map<byte [], byte []>map = TLV_PARSER.parse(tlvData, offset, tlvData.length - offset);
		for (Map.Entry<byte [], byte []> entry: map.entrySet()) {
			if (isTrack2EquivalentData(entry)) {
				trackTwoEquivData = entry.getValue();
			} else if (Arrays.equals(entry.getKey(), TLVTag.MASTERCARD_DATA_RECORD.getValue())) {
				Map<byte [], byte []>mcDataRecordMap = TLV_PARSER.parse(entry.getValue(), 0, entry.getValue().length);
				for (Map.Entry<byte [], byte[]>mcEntry: mcDataRecordMap.entrySet()) {
					if (isTrack2EquivalentData(mcEntry)) {
						trackTwoEquivData = mcEntry.getValue();
					}
				}
			}
		}
		return trackTwoEquivData;
	}
	
	private static boolean isTrack2EquivalentData(Map.Entry<byte [], byte[]>entry) {
		boolean result = Arrays.equals(entry.getKey(), TLVTag.TRACK2_EQUIVALENT_DATA.getValue());
		return result;
	}
	
	public static byte [] parseTrackDataSource(byte [] tlvData) throws IOException {
		
		Map<byte [], byte []>map = TLV_PARSER.parse(tlvData, 0, tlvData.length);
		byte [] trackDataSource = null; 
		if (map != null) {
			trackDataSource = map.get(TLVTag.TRACK_DATA_SOURCE.getValue());
			if (trackDataSource == null || trackDataSource.length == 0) {
				byte [] vivotechGroupTag = map.get(TLVTag.VIVOTECH_GROUP_TAG.getValue());
				if (vivotechGroupTag != null) {
					Map<byte [], byte[]>vtagData = TLV_PARSER.parse(vivotechGroupTag, 0, vivotechGroupTag.length);
					trackDataSource = vtagData.get(TLVTag.TRACK_DATA_SOURCE.getValue());
					if (trackDataSource == null || trackDataSource.length == 0) {
						String msg = "TLV Parse in VIVOTECH_GROUP_TAG did not find " + TLVTag.TRACK_DATA_SOURCE;
						log.info(msg);
						throw new IOException(msg);
					}
				} else {
					String msg = "TLV Parse did not find vivotech group tag";
					log.info(msg);
					throw new IOException(msg);
				}
			}
		}
		return trackDataSource;
	}
	
}
