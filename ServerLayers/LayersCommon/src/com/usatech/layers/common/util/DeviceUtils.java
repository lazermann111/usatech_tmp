package com.usatech.layers.common.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.PosEnvironment;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.model.Device;

public class DeviceUtils {
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	public static final Pattern EDGE_COMM_SETTING_EXPRESSION = Pattern.compile("^(?:0|1|2|3|20|21|22|23)$");
	public static final Pattern EDGE_SCHEDULE_EXPRESSION = Pattern.compile("^(?:85|86|87|1101)$");
	public static final Pattern TWO_DOT_EXPRESSION = Pattern.compile("\\..+\\.");
	
	public static final int DEFAULT_PACKET_SIZE = 1024;
	public static final int GX_PACKET_SIZE = 248;
	public static final int EPORT_CONNECT_PACKET_SIZE = 102400;
	public static final int DEFAULT_EXECUTE_ORDER = 0;
	
	public static final String EPORT_CMD_GENERIC_REQUEST_V4_1 = "CA";
	public static final String EPORT_CMD_GENERIC_RESPONSE_V4_1 = "CB";
	public static final String EPORT_CMD_FILE_TRANSFER_START_V1 = "7C";
	public static final String EPORT_CMD_FILE_TRANSFER_START_V1_1 = "A4";
	public static final String EPORT_CMD_FILE_TRANSFER_START_V4_1 = "C8";
	public static final String EPORT_CMD_FILE_TRANSFER_V4_1 = "C9";
	public static final String EPORT_CMD_PEEK_V1 = "87";
	public static final String EPORT_CMD_POKE_V1 = "88";
	public static final String EPORT_CMD_SHORT_FILE_TRANSFER_V4_1 = "C7";
	public static final String EPORT_CMD_FILE_TRANSFER_REQUEST_9BH = "9B";
	
	private static void parseDeviceSettings(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, LinkedHashMap<String, String> deviceSettingData, Results results, int deviceTypeId, Connection conn) throws Exception {
		while (results.next()) {
			String paramName = results.getFormattedValue("device_setting_parameter_cd");
			String paramValue = results.getFormattedValue("device_setting_value");
					 	
			if(deviceTypeId == DeviceType.G4.getValue() || deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.MEI.getValue()){
				if(defaultConfData == null || defaultConfData.size() <= 0)
					defaultConfData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, -1, null, conn);
				ConfigTemplateSetting cts = defaultConfData.get(paramName);
				if (cts == null)
					continue;
				deviceSettingData.put(paramName, DeviceConfigurationUtils.parseFixedWidthLocationData(paramValue, cts, true));					
			}else
				deviceSettingData.put(paramName, paramValue);
		}
	}	
	
	public static LinkedHashMap<String, String> getDeviceSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceTypeId, boolean includeServerOnlySettings, boolean parseDeviceSettings, Connection conn) throws ServletException
	{
		LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String>();
		Results results;
		try{
			if (conn == null)
				results = DataLayerMgr.executeQuery("GET_DEVICE_CONFIGURATION_SETTINGS", new Object[] {deviceId}, false);
			else
				results = DataLayerMgr.executeQuery(conn, "GET_DEVICE_CONFIGURATION_SETTINGS", new Object[] {deviceId});
			
			if (parseDeviceSettings)
				parseDeviceSettings(defaultConfData, deviceSettingData, results, deviceTypeId, conn);
			else {
				while (results.next())
					deviceSettingData.put(results.getFormattedValue("device_setting_parameter_cd"), results.getFormattedValue("device_setting_value"));
			}
		}catch(Exception e){
			throw new ServletException("Error getting device setting data for deviceId " + deviceId, e);
		}
		if (includeServerOnlySettings) {
			try {
				if (conn == null)
					results = DataLayerMgr.executeQuery("GET_DEVICE_SERVER_ONLY_SETTINGS", new Object[] {deviceId}, false);
				else
					results = DataLayerMgr.executeQuery(conn, "GET_DEVICE_SERVER_ONLY_SETTINGS", new Object[] {deviceId});
				while (results.next())
					deviceSettingData.put(results.getFormattedValue("device_setting_parameter_cd"), results.getFormattedValue("device_setting_value"));
			} catch (Exception e) {
				throw new ServletException("Error getting server-only device settings for deviceId " + deviceId, e);
			}
		}
		return deviceSettingData;
	}
	
	public static LinkedHashMap<String, String> getDeviceSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceTypeId, boolean includeServerOnlySettings, Connection conn) throws ServletException
	{
		return getDeviceSettingData(defaultConfData, deviceId, deviceTypeId, includeServerOnlySettings, true, conn);
	}
	
	public static LinkedHashMap<String, String> getAllDeviceSettingData(long deviceId, Connection conn) throws ServletException
	{
		LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String>();
		Results results;
		try {
			if (conn == null)
				results = DataLayerMgr.executeQuery("GET_ALL_DEVICE_SETTINGS", new Object[] {deviceId}, false);
			else
				results = DataLayerMgr.executeQuery(conn, "GET_ALL_DEVICE_SETTINGS", new Object[] {deviceId});
			while (results.next())
				deviceSettingData.put(results.getFormattedValue("device_setting_parameter_cd"), results.getFormattedValue("device_setting_value"));
		} catch (Exception e) {
			throw new ServletException("Error getting all device settings for deviceId " + deviceId, e);
		}
		return deviceSettingData;
	}
	
	public static LinkedHashMap<String, String> getBackupDeviceSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceTypeId, boolean includeServerOnlySettings, Connection conn) throws ServletException
	{
		LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String>();
		Results results;
		try{
			if (conn == null)
				results = DataLayerMgr.executeQuery("GET_BACKUP_DEVICE_CONFIGURATION_SETTINGS", new Object[] {deviceId}, false);
			else
				results = DataLayerMgr.executeQuery(conn, "GET_BACKUP_DEVICE_CONFIGURATION_SETTINGS", new Object[] {deviceId});
			
			parseDeviceSettings(defaultConfData, deviceSettingData, results, deviceTypeId, conn);
		}catch(Exception e){
			throw new ServletException("Error getting device setting data for deviceId " + deviceId, e);
		}
		if (includeServerOnlySettings) {
			try {
				if (conn == null)
					results = DataLayerMgr.executeQuery("GET_BACKUP_DEVICE_SERVER_ONLY_SETTINGS", new Object[] {deviceId}, false);
				else
					results = DataLayerMgr.executeQuery(conn, "GET_BACKUP_DEVICE_SERVER_ONLY_SETTINGS", new Object[] {deviceId});
				while (results.next())
					deviceSettingData.put(results.getFormattedValue("device_setting_parameter_cd"), results.getFormattedValue("device_setting_value"));
			} catch (Exception e) {
				throw new ServletException("Error getting server-only device settings for deviceId " + deviceId, e);
			}
		}
		return deviceSettingData;
	}
	
	public static LinkedHashMap<String, String> getConfigTemplateSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long templateId, int deviceTypeId, boolean includeServerOnlySettings, boolean parseDeviceSettings, Connection conn) throws ServletException
	{
		LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String>();
		Results results;
		try{		
			if (conn == null)
				results = DataLayerMgr.executeQuery("GET_CONFIG_TEMPLATE_SETTINGS", new Object[] {templateId}, false);
			else
				results = DataLayerMgr.executeQuery(conn, "GET_CONFIG_TEMPLATE_SETTINGS", new Object[] {templateId});
			
			if (parseDeviceSettings)
				parseDeviceSettings(defaultConfData, deviceSettingData, results, deviceTypeId, conn);
			else {
				while (results.next())
					deviceSettingData.put(results.getFormattedValue("device_setting_parameter_cd"), results.getFormattedValue("device_setting_value"));
			}
		}catch(Exception e){
			throw new ServletException("Error getting device setting data for templateId " + templateId, e);
		}
		if (includeServerOnlySettings) {
			try {
				if (conn == null)
					results = DataLayerMgr.executeQuery("GET_CONFIG_TEMPLATE_SERVER_ONLY_SETTINGS", new Object[] {templateId}, false);
				else
					results = DataLayerMgr.executeQuery(conn, "GET_CONFIG_TEMPLATE_SERVER_ONLY_SETTINGS", new Object[] {templateId});
				while (results.next())
					deviceSettingData.put(results.getFormattedValue("device_setting_parameter_cd"), results.getFormattedValue("device_setting_value"));
			} catch (Exception e) {
				throw new ServletException("Error getting server-only device settings for templateId " + templateId, e);
			}
		}
		return deviceSettingData;
	}
	
	public static LinkedHashMap<String, String> getConfigTemplateSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long templateId, int deviceTypeId, boolean includeServerOnlySettings, Connection conn) throws ServletException
	{
		return getConfigTemplateSettingData(defaultConfData, templateId, deviceTypeId, includeServerOnlySettings, true, conn);
	}
	
	public static int createPendingCommand(Connection conn, String machineId, String dataType, String command, String executeCd, int order, boolean cancelExisting, long fileTransferId, boolean checkExistingFile, long deviceId, int packetSize, int deviceTypeId) throws SQLException, DataLayerException
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("machineId", machineId);
		params.put("dataType", dataType);
		params.put("command", command);
		params.put("executeCd", executeCd);
		params.put("executeOrder", order);
		params.put("cancelExisting", cancelExisting ? 1 : 0);
		params.put("fileTransferId", fileTransferId);
		params.put("checkExistingFile", checkExistingFile ? 1 : 0);
		params.put("deviceId", deviceId);
		params.put("packetSize", packetSize);
		params.put("deviceTypeId", deviceTypeId);
		if(conn == null)
			DataLayerMgr.executeUpdate("CREATE_PENDING_COMMAND", params, true);
		else
			DataLayerMgr.executeUpdate(conn, "CREATE_PENDING_COMMAND", params);
		return ConvertUtils.convertSafely(int.class, params.get("commandCount"), 0);
	}
	
	/**
	 * Create a pending command for the device by the given evNumber, passing in Connection for transactional integrity.
	 * 
	 * @param evNumber
	 * @param dataType
	 * @param command
	 * @param executeCd
	 * @param order
	 * @param Connection
	 * @throws DataLayerException
	 * @throws SQLException
	 */
	public static void createPendingCommand(Connection conn, String evNumber, String dataType, String command, String executeCd, int order, boolean cancelExisting) throws SQLException, DataLayerException
	{
		createPendingCommand(conn, evNumber, dataType, command, executeCd, order, cancelExisting, 0, false, 0, 0, -1);
	}
	
	/**
	 * Create the poke command by the given parameters, this is used to process the "Save" for
	 * "edit configuration".
	 * 
	 * @param evNumber
	 * @param addr
	 * @param length
	 * @param execOrderCounter
	 * @return
	 * @throws ServletException
	 */
	public static void createPoke(Connection conn, String evNumber, int addr, int length, int execOrder) throws ServletException
	{
		String poke = new StringBuilder(StringHelper.encodeHexString("B")).append(StringUtils.toHex(addr)).append(StringUtils.toHex(length)).toString();

		// don't put actual poke data in the pending table, just the poke info. We'll construct
		// the poke on the fly when the command is sent - this is done in case there are changes
		// made to the
		// config file between the time we create the pokes when they are sent

		try
		{
			createPendingCommand(conn, evNumber, "88", poke, "P", execOrder, false);
		}
		catch (Exception e)
		{
			throw new ServletException("Failed to create poke: " + evNumber + " 88 " + poke + " P " + execOrder, e);
		}
	}
	
	private static String padCenter(String str, int len, String pad)
	{
		int counter = 1;
		StringBuilder out = new StringBuilder(str);
		while (out.length() < len) {
			if ((counter % 2) == 0)
				out.append(pad);
			else
				out.insert(0, pad);
			counter++;
		}
		return out.toString();
	}
	
	/**
	 * Pad the value with the given parameters, this is used to process the "Save" for
	 * "edit configuration".
	 * 
	 * @param data
	 * @param padWith
	 * @param size
	 * @param align
	 * @param dataMode
	 * @return a string array, where array[0] is the message, and array[1] is the padded data
	 */
	public static String[] pad(String data, String padWith, int size, String align, String dataMode)
	{
		String[] result = new String[2];
		StringBuilder buffer = new StringBuilder();
		String out;
		int padSize = size;
		if ("H".equals(dataMode))
		{
			padSize = size * 2;
		}
		String padPosition = "";
		if ("L".equals(align))
		{
			padPosition = "R";
		}
		else if ("R".equals(align))
		{
			padPosition = "L";
		}
		else if ("C".equals(align))
		{
			padPosition = "C";
		}
		buffer.append("Padding on ").append(padPosition).append(" with \"").append(padWith).append("\" to ").append(padSize);

		StringBuilder padBuffer = new StringBuilder();
		if (("L".equals(padPosition) || "R".equals(padPosition)) && data.length() < padSize)
		{
			for (int padCounter = 0; padCounter < (padSize - data.length()); padCounter += padWith.length())
			{
				padBuffer.append(padWith);
			}
		}

		if ("L".equals(padPosition))
		{
			out = new StringBuilder(padBuffer.toString()).append(data).toString();
		}
		else if ("R".equals(padPosition))
		{
			out = new StringBuilder(data).append(padBuffer.toString()).toString();
		}
		else if ("C".equals(padPosition))
		{
			out = padCenter(data, padSize, padWith);
		}
		else
			out = "";

		result[0] = buffer.toString();
		result[1] = out;
		return result;
	}
	
	public static boolean isWeekdayList(String s) {
		if (StringHelper.isBlank(s))
			return false;
		String[] days = s.split("\\|", -1);
		int previousDay = -1;
		for (int i=0; i<days.length; i++) {
			if (!StringHelper.isInteger(days[i]))
				return false;
			int day = Integer.valueOf(days[i]);
			if (day <= previousDay || day > 6)
				return false;
			previousDay = day;
		}
		return true;
	}
	
	public static boolean isMonthList(String s) {
		if (StringHelper.isBlank(s))
			return false;
		if (s.equals("A"))
			return true;
		String[] months = s.split("\\|", -1);
		int previousMonth = 0;
		for (int i=0; i<months.length; i++) {
			if (!StringHelper.isInteger(months[i]))
				return false;
			int month = Integer.valueOf(months[i]);
			if (month <= previousMonth || month > 12)
				return false;
			previousMonth = month;
		}
		return true;
	}
	
	public static boolean isTimeList(String s) {
		if (StringHelper.isBlank(s))
			return false;
		String[] times = s.split("\\|");
		for(String t : times) {
			if (!StringHelper.isMilitaryTime(t)) {
				return false;
			}
		}
		return true;
	}

	public static boolean validateEdgeSchedule (String key, String value, Map<String, String> errorMap, String label) {
		if (StringHelper.isBlank(value))
			return true;
		if ("1101".equalsIgnoreCase(key) && "0".equalsIgnoreCase(value.trim()))
			return true;
		boolean valid = true;
		String[] parts = value.split("\\^", -1);
		if ("D".equals(parts[0])) {
			if (parts.length != 2 || !StringHelper.isMilitaryTime(parts[1]))
				valid = false;
		} else if ("W".equals(parts[0])) {
			if (parts.length != 3 || !StringHelper.isMilitaryTime(parts[1]))
				valid = false;
			else if (!isWeekdayList(parts[2]))
				valid = false;
		} else if ("M".equals(parts[0])) {
			if (parts.length != 4 || !StringHelper.isMilitaryTime(parts[1]) || !StringHelper.isInteger(parts[2]))
				valid = false;
			else {
				int dayOfMonth = Integer.valueOf(parts[2]);
				if (dayOfMonth < 1 || dayOfMonth > 31)
					valid = false;
				else if (!isMonthList(parts[3]))
					valid = false;
			}
		} else if ("I".equals(parts[0])) {
			if (parts.length != 3 || !StringHelper.isLong(parts[1]) || !StringHelper.isInteger(parts[2]))
				valid = false;
			else {
				long utcTimeOffset = Long.valueOf(parts[1]);
				int intervalSeconds = Integer.valueOf(parts[2]);
				if (utcTimeOffset < 0 || intervalSeconds < 60)
					valid = false;
			}
		} else if ("E".equals(parts[0])) {
			if (parts.length != 2) {
				valid = false;
			} else {
				valid = isTimeList(parts[1]);
			}
		} else {
			valid = false;
		}
		if (!valid)
			errorMap.put("Value validation failed for field: " + (StringHelper.isBlank(label) ? key : label), "value: \"" + value + "\"");
		return valid;
	}
	
	public static boolean validateEdgeSchedule (String key, String value, Map<String, String> errorMap) {
		return validateEdgeSchedule(key, value, errorMap, null);
	}
	
	public static long getNextFileTransferSequenceNum(Connection conn) throws ServletException{
		Results result;
		try{
			if (conn == null)
				result = DataLayerMgr.executeQuery("GET_NEXT_FILE_TRANSFER_SEQ_ID", null, false);
			else
				result = DataLayerMgr.executeQuery(conn, "GET_NEXT_FILE_TRANSFER_SEQ_ID", null);
			if(result.next())
				return result.getValue("seq_file_transfer_id", long.class);
			else
				throw new ServletException("Error getting next file transfer id");
		}catch(Exception e){
			throw new ServletException("Error getting next file transfer id", e);
		}
	}
	
	public static long getFileTransferId(String fileName, int fileTransferTypeCd, Connection conn) throws SQLException, DataLayerException, ConvertException {
		long fileTransferId = -1;
		
		Results result;
		if(conn == null)
			result = DataLayerMgr.executeQuery("GET_FILE_TRANSFER_ID_BY_FILE_NAME_FILE_TRANSFER_TYPE_CD", new Object[] {fileName, fileTransferTypeCd}, false);
		else
			result = DataLayerMgr.executeQuery(conn, "GET_FILE_TRANSFER_ID_BY_FILE_NAME_FILE_TRANSFER_TYPE_CD", new Object[] {fileName, fileTransferTypeCd});
		if(result.next())
			fileTransferId = result.getValue("fileTransferId", long.class);
		
		return fileTransferId;
	}
	
	public static void insertFileTransferWithId(long fileTransferId, String fileName, int fileTransferTypeCd, Connection conn) throws ServletException{
		Map<String, Object> params = null;
		params = new HashMap<String, Object>();
		params.put("file_transfer_id", fileTransferId);
		params.put("file_transfer_name", fileName);
		params.put("file_transfer_type_cd", fileTransferTypeCd);
	
		try{
			if(conn == null)
				DataLayerMgr.executeCall("INSERT_FILE_TRANSFER_WITH_ID", params, true);
			else
				DataLayerMgr.executeCall(conn,"INSERT_FILE_TRANSFER_WITH_ID", params);
			
		}catch(Exception e){
			throw new ServletException("Unable to insert File Transfer for " + fileName, e);
		}
	}
	
	public static void updateFileData(long fileId, String fileName, String fileContent, Connection conn) throws DataLayerException, SQLException
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileContent", fileContent);
		boolean newConnection = false;
		boolean success = false;
		if(conn == null) {
			conn = DataLayerMgr.getConnection("OPER");
			newConnection = true;
		}

		try {
			Object[] results;
			int fileType;
			if (fileId > 0) {
				params.put("fileId", fileId);
				results = DataLayerMgr.executeCall(conn, "UPDATE_FILE_DATA_BY_ID", params);
				fileType = ConvertUtils.getIntSafely(results[1], -1);
				if (fileType < 0)
					throw new DataLayerException("Error updating file content for fileId: " + fileId);
				log.info("Updated file content for fileId: " + fileId);
			} else {
				params.put("fileName", fileName);
				results = DataLayerMgr.executeCall(conn, "UPDATE_FILE_DATA_BY_NAME", params);
				fileType = ConvertUtils.getIntSafely(results[1], -1);
				if (fileType < 0)
					throw new DataLayerException("Error updating file content for fileName: " + fileName);
				log.info("Updated file content for fileName: " + fileName);
			}
						
			if (newConnection)
				conn.commit();
			success = true;
		} finally {
			if (newConnection) {
				if (!success)
					ProcessingUtils.rollbackDbConnection(log, conn);
				ProcessingUtils.closeDbConnection(log, conn);
			}
		}
	}
	
	public static long saveFile(String fileName, String fileContent, long fileTransferId, int fileTransferTypeCd, Connection conn) throws ServletException
	{
		if (StringHelper.isBlank(fileName))
			throw new ServletException("File name not specified");
		
		if (StringHelper.isBlank(fileContent))
			throw new ServletException("No file content specified");
				
		try{
			if (fileTransferId < 1) {
				fileTransferId = getFileTransferId(fileName, fileTransferTypeCd, conn);
				if (fileTransferId < 1) {
					fileTransferId = getNextFileTransferSequenceNum(conn);
					insertFileTransferWithId(fileTransferId, fileName, fileTransferTypeCd, conn);
				}
			} else
				insertFileTransferWithId(fileTransferId, fileName, fileTransferTypeCd, conn);
			updateFileData(fileTransferId, fileName, fileContent, conn);
			
		}catch (Exception e){
			throw new ServletException(e);
		}
		return fileTransferId;
	}
	
	public static long getFileSize(Connection conn, long fileTransferId) throws ServletException{
		Results result;
		try{
			if (conn == null)
				result = DataLayerMgr.executeQuery("GET_FILE_SIZE", new Object[]{fileTransferId}, false);
			else
				result = DataLayerMgr.executeQuery(conn, "GET_FILE_SIZE", new Object[]{fileTransferId});
			if(result.next())
				return result.getValue("file_size", long.class);
			else
				throw new ServletException("Error getting file size for fileTransferId: " + fileTransferId);
		}catch(Exception e){
			throw new ServletException("Error getting file size for fileTransferId: " + fileTransferId, e);
		}
	}
	
	public static String getFileSizeFormatted(Connection conn, long fileTransferId) throws ServletException{
		Results result;
		try{
			if (conn == null)
				result = DataLayerMgr.executeQuery("GET_FILE_SIZE_FORMATTED", new Object[]{fileTransferId}, false);
			else
				result = DataLayerMgr.executeQuery(conn, "GET_FILE_SIZE_FORMATTED", new Object[]{fileTransferId});
			if(result.next())
				return result.getValue("file_size_formatted", String.class);
			else
				throw new ServletException("Error getting file size for fileTransferId: " + fileTransferId);
		}catch(Exception e){
			throw new ServletException("Error getting file size for fileTransferId: " + fileTransferId, e);
		}
	}
	
	public static int insertDeviceFileTransfer(String deviceName, long deviceFileTransferId, long deviceId, long fileTransferId, int packetSize, boolean removeRequests, String command, Connection conn) throws ServletException{
		Map<String, Object> params = null;
		params = new HashMap<String, Object>();
		params.put("file_transfer_id", fileTransferId);
		params.put("remove_requests", removeRequests ? 1 : 0);
		params.put("device_name", deviceName);
		params.put("command", command);
		params.put("device_file_transfer_id", deviceFileTransferId);
		params.put("device_id", deviceId);
		params.put("direction", "O");
		params.put("status", "0");
		params.put("packet_size", packetSize);
		Object[] res = null;
		
		int success = 0;
		try{
			if(conn == null)
				res = DataLayerMgr.executeCall("INSERT_DEVICE_FILE_TRANSFER", params, true);
			else
				res = DataLayerMgr.executeCall(conn, "INSERT_DEVICE_FILE_TRANSFER", params);
			
			success = ((Integer)res[0]).intValue();
		}catch(Exception e){
			throw new ServletException("Unable to insert Device File Transfer for " + deviceName, e);
		}
		return success;
	}
	
	public static int sendCommand(int deviceTypeId, long deviceId, String deviceName, long fileTransferId, long fileSize, int packetSize, int executeOrder, Connection conn, String command, boolean checkExistingFile)throws ServletException{
		if (DeviceType.EDGE.getValue() != deviceTypeId && DeviceType.GX.getValue() != deviceTypeId && DeviceType.KIOSK.getValue() != deviceTypeId 
				&& DeviceType.ESUDS.getValue() != deviceTypeId)
			return 0;
		
		try {
			return createPendingCommand(conn, deviceName, null, command, "P", executeOrder, false, fileTransferId, checkExistingFile, deviceId, packetSize, deviceTypeId);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
	public static long getNextMasterIdBySerial(String deviceSerialCd, Connection conn) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceSerialCd", deviceSerialCd);
		if (conn == null)
			DataLayerMgr.executeCall("NEXT_MASTER_ID_BY_SERIAL", params, true);
		else
			DataLayerMgr.executeCall(conn, "NEXT_MASTER_ID_BY_SERIAL", params);
		return ConvertUtils.getLong(params.get("masterId"));
	}
	
	public static int getMaxPropertyListVersion(Connection conn) throws SQLException, DataLayerException, ConvertException {
		Results results;
		if (conn == null)
			results = DataLayerMgr.executeQuery("GET_MAX_PROPERTY_LIST_VERSION", null, false);
		else
			results = DataLayerMgr.executeQuery(conn, "GET_MAX_PROPERTY_LIST_VERSION", null);
		if (results.next())
			return results.getValue("propertyListVersion", int.class);
		else
			return 0;
	}
	
	public static Device generateDevice(long deviceId) throws ServletException
	{
		Device device = new Device();
		device.setId(deviceId);
		Results result = null;
		try {
			result = DataLayerMgr.executeQuery("GET_DEVICE_PROFILE_INFO", new Long[] {device.getId()}, false);
			if (!result.next())
				return device;
			device.setDeviceName(result.getValue("device_name", String.class));
			device.setTypeId(result.getValue("device_type_id", int.class));
			device.setTypeDesc(result.getFormattedValue("device_type_desc"));
			device.setSerialNumber(result.getFormattedValue("device_serial_cd"));
			device.setCommMethodCd(ConvertUtils.getStringSafely(result.get("comm_method_cd"), "E"));
			device.setCreatedTs(result.getFormattedValue("created_ts"));
			device.setLastUpdatedTs(result.getFormattedValue("last_updated_ts"));
			device.setLastActivityTs(result.getFormattedValue("last_activity_ts"));
			device.setLastActivityDays(ConvertUtils.getFloat(result.get("last_activity_days"), -1));
			device.setEnabled(result.getValue("device_active_yn_flag", Boolean.class));
			device.setPosId(result.getValue("pos_id", Long.class));
			device.setCustomerName(result.getFormattedValue("customer_name"));
			device.setLocationName(result.getFormattedValue("location_name"));
			device.setPropertyListVersion(ConvertUtils.getInt(result.get("property_list_version"), -1));
			device.setProtocolRevision(ConvertUtils.getInt(result.get("protocol_revision"), -1));
			device.setCommMethodName(result.getFormattedValue("comm_method_name"));
			device.setCredentialId(ConvertUtils.getLong(result.getValue("credential_id"), -1));
			device.setLastPasswordGeneratedTs(result.getFormattedValue("last_password_generated_ts"));
			device.setLastPasswordUpdatedTs(result.getFormattedValue("last_password_updated_ts"));
			device.setSubTypeId(result.getValue("device_sub_type_id", int.class));
			device.setSubTypeDesc(result.getFormattedValue("device_sub_type_desc"));
			device.setRejectUntilTs(result.getFormattedValue("reject_until_ts"));
			device.setRejectUntilColor(result.getFormattedValue("reject_until_color"));
			device.setDeviceRecordCount(result.getValue("device_record_count", int.class));
			device.setPosEnvironment(result.getValue("pos_environment_cd", PosEnvironment.class));
			device.setEntryCapability(result.getValue("entry_capability_cds", String.class));
			device.setDeviceUtcOffsetMin(result.getValue("device_utc_offset_min", int.class));
			device.setLastClientIPAddress(result.getFormattedValue("last_client_ip_address"));
			device.setLastSaleTs(result.getFormattedValue("last_sale_ts"));
			device.setRestrictFirmwareUpdate(result.getFormattedValue("restrict_firmware_update"));
			device.setFirmwareRestrictionReason(result.getFormattedValue("firmware_restriction_reason"));
			device.setModemStatusDesc(result.getFormattedValue("modem_status_desc"));
			device.setModemStatusTs(result.getFormattedValue("modem_status_ts"));		
			device.setRestrictSettingsUpdate(result.getFormattedValue("restrict_settings_update"));
			device.setSettingsRestrictionReason(result.getFormattedValue("settings_restriction_reason"));
		} catch (Exception e) {
			throw new ServletException("Error loading device profile info", e);
		} finally {
			if (result != null)
				result.close();
		}
		return device;
	}
}
