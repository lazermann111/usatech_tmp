package com.usatech.layers.common.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.TreeSet;

import org.quartz.CronExpression;

import simple.text.StringUtils;

public class ExtendedCronExpression extends CronExpression {
	private static final long serialVersionUID = -8136783337213463080L;

	public ExtendedCronExpression(String cronExpression) throws ParseException {
		super(cronExpression);
	}

	public int[] getArray(int type) {
		TreeSet<Integer> set = getSet(type);
		if(set == null || set.contains(NO_SPEC))
			return null;
		int sz = set.size();
		if(set.contains(ALL_SPEC))
			sz--;
		int[] array = new int[sz];
		Iterator<Integer> iter = set.iterator();
		for(int i = 0; i < sz; i++)
			array[i] = iter.next();
		return array;
	}

	public int[] getHours() {
		return getArray(HOUR);
	}

	public int[] getDaysOfMonth() {
		return getArray(DAY_OF_MONTH);
	}

	public int[] getMonths() {
		return getArray(MONTH);
	}

	public int[] getDaysOfWeek() {
		return getArray(DAY_OF_WEEK);
	}

	public int getWeekOfMonth() {
		return lastdayOfWeek ? -1 : nthdayOfWeek;
	}

	public static String buildExpression(int hour, int[] daysOfMonth, int monthInterval, int[] daysOfWeek, int weekOfMonth) {
		int[] months;
		switch(monthInterval) {
			case 1:
				months = null;
				break;
			case 2:
				months = new int[] { 1, 3, 5, 7, 9, 11 };
				break;
			case 3:
				months = new int[] { 1, 4, 7, 10 };
				break;
			case 4:
				months = new int[] { 1, 5, 9 };
				break;
			case 6:
				months = new int[] { 1, 7 };
				break;
			default:
				months = null;
		}
		return buildExpression(hour, daysOfMonth, months, daysOfWeek, weekOfMonth);
	}

	public static String buildExpression(int hour, int[] daysOfMonth, int[] months, int[] daysOfWeek, int weekOfMonth) {
		StringBuilder sb = new StringBuilder();
		sb.append("0 0 ").append(hour).append(' ');
		if(daysOfMonth == null || daysOfMonth.length == 0) {
			if(daysOfWeek == null || daysOfWeek.length == 0)
				sb.append("* ");
			else
				sb.append("? ");
		} else {
			boolean first = true;
			for(int d : daysOfMonth) {
				if(first)
					first = false;
				else
					sb.append(',');
				if(d == -1)
					sb.append('L');
				else
					sb.append(d);
			}
			sb.append(' ');
		}
		if(months == null || months.length == 0)
			sb.append("* ");
		else {
			boolean first = true;
			for(int m : months) {
				if(first)
					first = false;
				else
					sb.append(',');
				sb.append(m);
			}
			sb.append(' ');
		}
		if(daysOfWeek == null || daysOfWeek.length == 0) {
			sb.append("? ");
		} else {
			boolean first = true;
			for(int w : daysOfWeek) {
				if(first)
					first = false;
				else
					sb.append(',');
				sb.append(w);
				if(daysOfWeek.length == 1) {
					if(weekOfMonth > 0)
						sb.append('#').append(weekOfMonth);
					else if(weekOfMonth == -1)
						sb.append("L");
				}
			}
			sb.append(' ');
		}
		sb.append('*');
		return sb.toString();
	}

	public static List<Date> getNextDates(String cronExpression, String timeZoneGuid, Date startTs, Date endTs, int num) {
		if(endTs != null && !startTs.before(endTs))
			return Collections.emptyList();
		CronExpression ce;
		try {
			ce = new CronExpression(cronExpression);
		} catch(ParseException e) {
			return null;
		}
		if(!StringUtils.isBlank(timeZoneGuid))
			ce.setTimeZone(TimeZone.getTimeZone(timeZoneGuid));
		List<Date> dates = new ArrayList<>(num > 0 ? num : 16);
		int n = (num > 0 ? num : Integer.MAX_VALUE);
		while(true) {
			Date nextTs = ce.getTimeAfter(startTs);
			if(endTs != null && !nextTs.before(endTs))
				break;
			dates.add(nextTs);
			if(--n <= 0)
				break;
			startTs = nextTs;
		}
		return dates;
	}
}

