package com.usatech.layers.common.util;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.TLVTag;

import simple.io.TLVParser;
import simple.text.StringUtils;

public class VendXParsing {
	
	public static int BIT_55_MAX_LENGTH = 513;

	public static int ATTRIBUTION_LENGTH = 1;
	public static int CLEARING_RECORD_LENGTH = 1;
	public static int CRC_LENGTH = 2;
	
	public static int KSN_LENGTH = 10;
	public static int KSN_TLV_TAG_LENGTH = 4;

    private static TLVParser TLV_PARSER = new TLVParser();

	/**
	 * See NEO v1.0 IDG_REV 67.pdf page ??? NON-Encrypted data section for starting point 
	 * @param responseData
	 */
	public static VendXNonEncryptedData parseIdtechNonEncryptedData(byte [] responseData) throws IOException {
		int trackOneLength = 0;
		int trackTwoLength = 0;
		int TRACK1_LENGTH_LENGTH = 1;
		int TRACK2_LENGTH_LENGTH = 1;
		int CLEARING_RECORD_LENGTH = 1;
		VendXNonEncryptedData result = null;
		if (responseData != null) {
			if (responseData.length > 0) {
				trackOneLength = responseData[0] & 0xff;
			}
			int track2LengthOffset = trackOneLength + TRACK1_LENGTH_LENGTH;
			if (responseData.length > track2LengthOffset) {
				trackTwoLength = responseData[track2LengthOffset] & 0xff;
			}
			int clearingRecordStart = track2LengthOffset + TRACK2_LENGTH_LENGTH + trackTwoLength;
			int tlvStartOffset = clearingRecordStart + CLEARING_RECORD_LENGTH;
			
			byte [] trackTwoMagStripeData = new byte [trackTwoLength];
			int trackTwoOffset = TRACK1_LENGTH_LENGTH + trackOneLength + TRACK2_LENGTH_LENGTH;
			if ((trackTwoOffset + trackTwoLength) <= responseData.length) {
				if (trackTwoLength > 0) {
					System.arraycopy(responseData, trackTwoOffset, trackTwoMagStripeData, 0, trackTwoLength);
				}
			} else {
				throw new IOException("Invalid track 2 offset or length. Will exceed length of emvData  Track 2 offset " + trackTwoOffset + ". Track 2 length " + trackTwoLength + " emvData.length " + responseData.length);
			}

			int tlvLength = responseData.length - tlvStartOffset;
			byte [] tlvData = new byte [tlvLength];
			if (tlvLength > 0 && (tlvStartOffset + tlvLength) <= responseData.length) {
		    	System.arraycopy(responseData, tlvStartOffset, tlvData, 0, tlvLength);
			} else {
//				throw new IOException("Invalid chip card tlv data offset or length. Will exceed length of emvData  tlv data offset " + tlvStartOffset + ". tlv length " + tlvLength + " emvData.length " + emvData.length);
			}
			result = new VendXNonEncryptedData(trackTwoMagStripeData, tlvData);
		}
		return result;
	}
	
//	/**
//	 * See NEO v1.0 IDG_REV 67.pdf page 145 Encrypted EMV section for starting point.  This parses out the KSN only 
//	 * @param responseData
//	 */
//	public static VendXEMVData parseIdtechEncryptedEMVKSN(byte [] responseData) throws IOException {
//		VendXEMVData emvData = new VendXEMVData();
//		emvData.setRawResponse(responseData);
//		if (responseData != null && responseData.length > 0) {
//			EMVValueHandler valueHandler = emvData.new EMVValueHandler(emvData);
//			TLVParser.handleTag(responseData, ATTRIBUTION_LENGTH, responseData.length, valueHandler);
//		}
//		return emvData;
//	}
	
	/**
	 * See NEO v1.0 IDG_REV 73.pdf page 142, 311 Enhanced Encrypted MSR section and Appendix A.11:Enhanced Encrypted MSR Data Output Format
	 * and 80000403-001 Enhanced Encrypted MSR Data Output Format.pdf page 6 for parsing details
	 * @param responseData
	 */
	public static VendXEnhancedEncryptedMSRData parseIdtechEnhancedEncryptedMSR(byte [] responseData) throws IOException {
		byte STX = 0x02;
		byte ETX = 0x03;
		int STX_OFFSET = 0;
		int CARD_ENCODE_TYPE_OFFSET = 3;
		int TRACK_1_LENGTH_OFFSET = 5;
		int TRACK_2_LENGTH_OFFSET = 6;
		int TRACK_3_LENGTH_OFFSET = 7;
		int CLEAR_MASK_DATA_STATUS_OFFSET = 8;
		int ENCRYPTED_HASH_DATA_STATUS_OFFSET = 9;
		int TRACK1_MASKED_DATA_OFFSET = 10;
		int ETX_LENGTH = 1;
		int SESSION_ID_LENGTH = 8;
		int TRACK1_HASHED_LENGTH = 20;
		int TRACK2_HASHED_LENGTH = 20;
		int TRACK3_HASHED_LENGTH = 20;
		int READER_SERIAL_NUMBER_LENGTH = 10;
		int KSN_LENGTH = 10;
		
		int CHECKSUM_LENGTH = 1;
		int CHECK_LRC_LENGTH = 1;
		
		int etxOffset = responseData.length - ETX_LENGTH;

		// check lrc and checksum
		int checkLrcOffset = etxOffset - CHECKSUM_LENGTH - CHECK_LRC_LENGTH;
		byte lrcValue = 0;
		byte checksumValue = 0;
		for (int ii = CARD_ENCODE_TYPE_OFFSET; ii < checkLrcOffset; ii++) {
			byte b = responseData[ii];
			lrcValue = (byte)(lrcValue ^ b);
			checksumValue = (byte)(checksumValue + b);
		}
		byte lrc = responseData[checkLrcOffset];
		if (lrc != lrcValue) {
			throw new IOException("Invalid lrc value. lrc: " + lrc + ", calculated lrc: " + lrcValue);
		}
		byte checksum = responseData[checkLrcOffset + CHECK_LRC_LENGTH];
		if (checksum != checksumValue) {
			throw new IOException("Invalid checksum value. checksum: " + checksum + ", calculated checksum: " + checksumValue);
		}
		
		// check stx and etx exist
		byte stx = responseData[STX_OFFSET];
		byte etx = responseData[etxOffset];
		if (stx != STX || etx != ETX) {
			throw new IOException("Invalid Enchanced Encrypted MSR.  Missing STX or ETX");
		}
		
		int track1Length = responseData[TRACK_1_LENGTH_OFFSET];
		int track2Length = responseData[TRACK_2_LENGTH_OFFSET];
		int track3Length = responseData[TRACK_3_LENGTH_OFFSET];
//		System.out.println("track1Length: " + track1Length + ", track2Length: " + track2Length + ", track3Length: " + track3Length);
		
		byte clearMaskDataSentStatus = responseData[CLEAR_MASK_DATA_STATUS_OFFSET];
		// See 80000403-001 Enhanced Encrypted MSR Data Output Format.pdf foe details on the bits being checked
		boolean aesEncrypted = (clearMaskDataSentStatus & 0x10) > 0;	// if bit 4 is set, then AES encryption, else TDES
		boolean readerSerialNumberPresent = (clearMaskDataSentStatus & 0x80) > 0; 
		
		int padding = (aesEncrypted) ? 16 : 8;	// if AES encrypted then the padding is 16, else TDES padding is 8
		byte encryptedHashDataStatus = responseData[ENCRYPTED_HASH_DATA_STATUS_OFFSET];
		// See 80000403-001 Enhanced Encrypted MSR Data Output Format.pdf foe details on the bits being checked
		boolean track1HashPresent = (encryptedHashDataStatus & 0x08) > 0;
		boolean track2HashPresent = (encryptedHashDataStatus & 0x10) > 0;
		boolean track3HashPresent = (encryptedHashDataStatus & 0x20) > 0;
		boolean sessionIdPresent = (encryptedHashDataStatus & 0x40) > 0;
		boolean ksnPresent = (encryptedHashDataStatus & 0x80) > 0;
		
		int track2MaskedDataOffset = TRACK1_MASKED_DATA_OFFSET + track1Length;
		int track3MaskedDataOffset = track2MaskedDataOffset + track2Length;
		
		int track1EncryptedOffset = track3MaskedDataOffset + track3Length;
		int track1EncryptedLength = VendXEMVData.extractEncryptedTrack(track1Length, padding);
		int track2EncryptedLength = VendXEMVData.extractEncryptedTrack(track2Length, padding);
		int track2EncryptedOffset = track1EncryptedOffset + track1EncryptedLength;
		int track3EncryptedLength = VendXEMVData.extractEncryptedTrack(track3Length, padding);
		byte [] track2EncryptedData = new byte[track2EncryptedLength]; 
		System.arraycopy(responseData, track2EncryptedOffset, track2EncryptedData, 0, track2EncryptedLength);
		
		int track3EncryptedOffset = track2EncryptedOffset + track2EncryptedLength;
		int ksnOffset = track3EncryptedOffset + track3EncryptedLength;
		if (sessionIdPresent) ksnOffset += SESSION_ID_LENGTH;
		if (track1HashPresent) ksnOffset += TRACK1_HASHED_LENGTH;
		if (track2HashPresent) ksnOffset += TRACK2_HASHED_LENGTH;
		if (track3HashPresent) ksnOffset += TRACK3_HASHED_LENGTH;
		if (readerSerialNumberPresent) { 
			byte [] readerSerialNumber = new byte [READER_SERIAL_NUMBER_LENGTH];
			int readerSerialNumberOffset = ksnOffset;
			System.arraycopy(responseData, readerSerialNumberOffset, readerSerialNumber, 0, READER_SERIAL_NUMBER_LENGTH);
//			String serialNumberString = StringUtils.toHex(readerSerialNumber);
//			System.out.println("serial number: " + serialNumberString);
			ksnOffset += READER_SERIAL_NUMBER_LENGTH;
		}
		
		byte [] ksn = null;
		if (ksnPresent) {
			ksn = new byte [KSN_LENGTH];
			System.arraycopy(responseData, ksnOffset, ksn, 0, KSN_LENGTH);
		}
		VendXEnhancedEncryptedMSRData result = new VendXEnhancedEncryptedMSRData(ksn, track2EncryptedData, track2Length);
		return result;
	}

	public static String panSequenceNumber(Map<String, byte[]> tlvMap) throws IOException {
		byte [] panSeqBytes = tlvMap.get(TLVTag.PAN_SEQUENCE_NUMBER.getHexValue());
		if (panSeqBytes != null)
			return StringUtils.toHex(panSeqBytes);

		// go looking for it
		byte [] masterCardDataRecord = tlvMap.get(TLVTag.MASTERCARD_DATA_RECORD.getHexValue());
		if (masterCardDataRecord != null) {
			Map<byte[], byte[]> masterCardDataTlv = TLV_PARSER.parse(masterCardDataRecord, 0, masterCardDataRecord.length);
			if (masterCardDataTlv != null)
				return StringUtils.toHex(masterCardDataTlv.get(TLVTag.PAN_SEQUENCE_NUMBER.getValue()));
		}

		return null;
	}
	
	private static void interrogateForChaseTags(StringBuilder buffer, Map<String, byte[]> tlvMap, AuthorityAction action) {
		List<TLVTag> tags = null;
		switch (action) {
			case AUTHORIZATION:
				tags = TLVTag.CHASE_AUTHORIZATION_EMV_TAGS;
				break;
			case SALE:
				tags = TLVTag.CHASE_SETTLEMENT_EMV_TAGS;
				break;
			case REVERSAL:
				tags = TLVTag.CHASE_REVERSAL_EMV_TAGS;
				break;
		}
		if (tags == null)
			return;
		for (TLVTag tag: tags) {
			byte [] value = tlvMap.get(tag.getHexValue());
			if (value != null)
				TLVParser.formatTagValueToHex(buffer, tag.getValue(), value);
    }
	}
	
	public static String extractChaseChipCardData(Map<String, byte[]> tlvMap, AuthorityAction action) throws IOException {
		StringBuilder buffer = new StringBuilder(BIT_55_MAX_LENGTH);
		buffer.append("000");
		interrogateForChaseTags(buffer, tlvMap, action);
		String lengthStr = Integer.toString(buffer.length() - 3);
		int start = Math.max(3 - lengthStr.length(), 0);
		buffer.replace(start, 3, lengthStr);
		return buffer.toString();
	}
}

