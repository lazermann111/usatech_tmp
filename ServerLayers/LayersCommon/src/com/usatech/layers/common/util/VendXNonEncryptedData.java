package com.usatech.layers.common.util;

public class VendXNonEncryptedData {

	private byte [] trackTwoMagStripeData;
	private byte [] tlvData;
	
	public VendXNonEncryptedData(byte [] trackTwoMagStripeData, byte [] tlvData) {
		this.trackTwoMagStripeData = trackTwoMagStripeData;
		this.tlvData = tlvData;
	}

	public byte[] getTrackTwoMagStripeData() {
		return trackTwoMagStripeData;
	}

	public byte[] getTlvData() {
		return tlvData;
	}
}
