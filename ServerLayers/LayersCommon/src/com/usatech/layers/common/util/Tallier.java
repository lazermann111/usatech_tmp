package com.usatech.layers.common.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import simple.util.CollectionUtils;

public class Tallier<C extends Comparable<? super C>> {
	protected final Map<C, Integer> tallies = new HashMap<C, Integer>();
	protected final Class<C> componentType;
	protected final Comparator<Map.Entry<C, Integer>> instanceComparator = new Comparator<Map.Entry<C, Integer>>() {
		@Override
		public int compare(Entry<C, Integer> o1, Entry<C, Integer> o2) {
			int i = o1.getValue().compareTo(o2.getValue());
			if(i != 0)
				return -i;
			return o1.getKey().compareTo(o2.getKey());
		}
	};

	public Tallier(Class<C> componentType) {
		this.componentType = componentType;
	}

	public void vote(C instance) {
		Integer current = tallies.get(instance);
		tallies.put(instance, current == null ? 1 : current + 1);
	}

	public void vote(C... instances) {
		for(C instance : instances)
			vote(instance);
	}

	public void vote(Collection<C> instances) {
		for(C instance : instances)
			vote(instance);
	}

	public C[] getWinners(int maxlength) {
		Map.Entry<C, Integer>[] entries = tallies.entrySet().toArray(new Map.Entry[tallies.size()]);
		Arrays.sort(entries, instanceComparator);
		int len = Math.min(maxlength, entries.length);
		C[] instances = CollectionUtils.createArray(componentType, len);
		for(int i = 0; i < len; i++)
			instances[i] = entries[i].getKey();
		return instances;
	}

	public void exclude(C instance) {
		tallies.remove(instance);
	}

	public void exclude(C... instances) {
		for(C instance : instances)
			exclude(instance);
	}

	public int getInstanceCount() {
		return tallies.size();
	}
}
