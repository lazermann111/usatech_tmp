/** 
 * USA Technologies, Inc. 2011
 * QuartzScheduledJobMapping.java by phorsfield, Aug 26, 2011 4:45:53 PM
 */
package com.usatech.layers.common;
 
import java.util.HashMap;
import java.util.Map;
import simple.util.CollectionUtils;

/**
 * Map jobs to a scheduler
 * @author phorsfield
 *
 */
public class QuartzScheduledJobMapping {

	protected final Map<String, QuartzCronScheduledJob> jobMap = new HashMap<String, QuartzCronScheduledJob>();
	public QuartzCronScheduledJob getJob(String jobId) {
		return jobMap.get(jobId);
	}

	public void setJob(String jobId, QuartzCronScheduledJob scheduledJob) {
		jobMap.put(jobId, scheduledJob);
	}
	public QuartzCronScheduledJob findJob(String jobId) {
		String key = CollectionUtils.bestPrefixMatch(jobMap.keySet(), jobId);
		return jobMap.get(key);
	}

	public Map<String, QuartzCronScheduledJob> getJobMap() {		
		return jobMap;
	}
	
}
