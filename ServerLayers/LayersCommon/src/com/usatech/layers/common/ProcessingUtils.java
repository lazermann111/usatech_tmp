package com.usatech.layers.common;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.ByteBufferInputStream;
import simple.io.EncodingAppendable;
import simple.io.Log;
import simple.io.logging.AbstractPrefixedLog;
import simple.lang.InterruptGuard;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidIntValueException;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.text.ThreadSafeDateFormat;
import simple.util.OptimizedIntegerRangeSet;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.CRCTypeLegacy;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ESudsLogType;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.VariableByteLengthNumber;


public class ProcessingUtils {
	protected static final Pattern propertyIndexPattern = Pattern.compile("\\s*(\\d+)(?:\\s*-\\s*(\\d+))?\\s*(?:\\||$)");
	protected static final Pattern propertyValuePattern = Pattern.compile("((?:\\d+(?:\\-\\d+)?)(?:\\|\\d+(?:\\-\\d+)?)*)(?:\\!|(?:\\=([^\\r\\n]*)))");
	protected static final Pattern endpointInformationPattern = Pattern.compile("([^=]*)\\=([^\\n]*)\\n");
	protected static final Pattern endpointSubInformationPattern = Pattern.compile("([^:]*)\\:([^\\r]*)\\r");
	protected static final Pattern onlineQueuePattern = Pattern.compile("^(usat\\.keymanager\\.decrypt\\.magtek|usat\\.keymanager\\.device\\.getkey|usat\\.inbound\\.message\\.auth|usat\\.inbound\\.message\\.neartime)$");
	public static final long MAX_ID_ADVANCE = 365*24*60*60;
	protected static final DateFormat simpleBcdDateFormat = new SimpleDateFormat("HHmmssMMddyyyy");
	protected static final DateFormat simpleEsudsDateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss.SSS yyyy");
	public static final DateFormat bcdDateFormat = new ThreadSafeDateFormat(simpleBcdDateFormat);
	public static final DateFormat esudsDateFormat = new ThreadSafeDateFormat(simpleEsudsDateFormat);
	public static final PropertyValueHandler<RuntimeException> DO_NOTHING_PROPERTY_VALUE_HANDLER = new PropertyValueHandler<RuntimeException>() {
		public void handleProperty(int propertyIndex, String propertyValue) {
		}
	};
	
	static {
		simpleBcdDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		simpleEsudsDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	public static interface PropertyIndexHandler<E extends Exception> {
		/** Handles an index property parsed from a property index list.
		 * @param propertyIndex
		 */
		public void handlePropertyIndex(int propertyIndex) throws E;
	}
	public static interface PropertyValueHandler<E extends Exception> {
		/** Handles a property parsed from a property list. If the property is set to the default,
		 *  propertyValue will be <code>null</code>.
		 * @param propertyIndex
		 * @param propertyValue
		 */
		public void handleProperty(int propertyIndex, String propertyValue) throws E;
	}

	public static <E extends Exception> int parsePropertyIndexList(String propertyIndexList, PropertyIndexHandler<E> handler) throws ParseException, E {
		int propertyCount = 0;
		Matcher matcher = propertyIndexPattern.matcher(propertyIndexList);
		while(matcher.lookingAt()) {
			int s = Integer.parseInt(matcher.group(1));
			String g2 = matcher.group(2);
			if(g2 != null && g2.length() > 0) {
				int e = Integer.parseInt(g2);
				if(s > e)
					throw new ParseException("Start value (" + s + ") of property index range is greater than end value (" + e + ") at character " + (1+matcher.start(1)), matcher.start(1));
				for(;s <= e; s++) {
					handler.handlePropertyIndex(s);
					propertyCount++;
				}
			} else {
				handler.handlePropertyIndex(s);
				propertyCount++;
			}
			matcher.region(matcher.end(), matcher.regionEnd());
		}
		if(!matcher.hitEnd())
			throw new ParseException("Property index list not formatted correctly at character " + (1+matcher.regionStart()), matcher.regionStart());
		return propertyCount;
	}

	public static Set<Integer> parsePropertyIndexList(String propertyIndexList) throws ParseException {
		OptimizedIntegerRangeSet set = new OptimizedIntegerRangeSet();
		String[] pils = StringUtils.split(propertyIndexList, MessageDataUtils.PROPERTY_VALUE_SEPARATOR);
		if(pils != null)
			for(String pil : pils)
				try {
					set.addRanges(pil);
				} catch(NumberFormatException e) {
					throw new ParseException("Invalid property index list; could not parse '" + pil + " to a number or range of numbers", 1);
				} catch(IllegalArgumentException e) {
					throw new ParseException("Invalid property index list; could not parse '" + pil + " to a number or range of numbers", 1);
				}
		return set;
	}

	public static <E extends Exception> int parsePropertyList(InputStream in, Charset charset, PropertyValueHandler<E> handler) throws IOException, ParseException, E {
		return parsePropertyList(new BufferedReader(new InputStreamReader(in, charset)), handler);
	}

	public static <E extends Exception> int parsePropertyList(Reader reader, Charset charset, PropertyValueHandler<E> handler) throws IOException, ParseException, E {
		return parsePropertyList(reader instanceof BufferedReader ? (BufferedReader) reader : new BufferedReader(reader), handler);
	}
	public static <E extends Exception> int parsePropertyList(String propertyValueList, final PropertyValueHandler<E> handler) throws IOException, ParseException, E {
		return parsePropertyList(new BufferedReader(new StringReader(propertyValueList)), handler);
	}
	public static <E extends Exception> int parsePropertyList(BufferedReader reader, final PropertyValueHandler<E> handler) throws IOException, ParseException, E {
		class PassOnPropertyIndexHandler implements PropertyIndexHandler<E> {
			protected String propertyValue;
			public void handlePropertyIndex(int propertyIndex) throws E {
				handler.handleProperty(propertyIndex, propertyValue);
			}
		}
		PassOnPropertyIndexHandler indexHandler = new PassOnPropertyIndexHandler();
		int pos = 1;
		int propertyCount = 0;
		String line;
		for(int n = 1; (line=reader.readLine()) != null; n++) {
			if(line.trim().length() > 0) {
				Matcher matcher = propertyValuePattern.matcher(line);
				if(!matcher.matches())
					throw new ParseException("Invalid property list at line " + n, pos);
				String propertyIndexList = matcher.group(1);
				indexHandler.propertyValue = matcher.group(2);
				propertyCount += parsePropertyIndexList(propertyIndexList, indexHandler);
			}
			pos += line.length() + 1; //Assume one newline char
		}
		return propertyCount;
	}

	public static Map<String,String> parseEndpointInformation(String information) {
		final Map<String,String> map = new LinkedHashMap<String, String>();
		parseEndpointInformation(information, map);
		return map;
	}
	public static void parseEndpointInformation(String information, Map<String,String> map) {
		Matcher matcher = endpointInformationPattern.matcher(information);
    	int index = 0;
    	while(matcher.lookingAt()) {
    		map.put(matcher.group(1), matcher.group(2));
    		index = matcher.end();
    		matcher.region(index, information.length());
    	}
	}
	
	public static Map<String,String> parseEndpointSubInformation(String subInformation) {
		final Map<String,String> map = new LinkedHashMap<String, String>();
		parseEndpointSubInformation(subInformation, map);
		return map;
	}
	public static void parseEndpointSubInformation(String subInformation, Map<String,String> map) {
		Matcher matcher = endpointSubInformationPattern.matcher(subInformation);
    	int index = 0;
    	while(matcher.lookingAt()) {
    		map.put(matcher.group(1), matcher.group(2));
    		index = matcher.end();
    		matcher.region(index, subInformation.length());
    	}
	}
	public static byte readPrev6BitInt(ByteBuffer buffer) throws BufferUnderflowException {
		return (byte)(buffer.get(buffer.position()-1) & 0x3F);
	}

	public static void writePrev6BitInt(ByteBuffer buffer, byte i) {
		if(i < 0x00 || i > 0x3F)
			throw new IllegalArgumentException("6-bit Int must be between 0 and 63, not " + i);
		byte b = buffer.get(buffer.position()-1);
		b = (byte)(b | i);
		buffer.put(buffer.position()-1, b);
	}

	public static byte read2BitInt(ByteBuffer buffer) throws BufferUnderflowException {
		return (byte)((buffer.get() >> 6) & 0x03);
	}

	public static void write2BitInt(ByteBuffer buffer, byte i) {
		if(i < 0x00 || i > 0x03)
			throw new IllegalArgumentException("2-bit Int must be between 0 and 3, not " + i);
		buffer.put((byte)(i << 6));
	}

	public static byte readByte(ByteBuffer buffer) throws BufferUnderflowException {
		return buffer.get();
	}

	public static void writeByte(ByteBuffer buffer, byte b) {
		buffer.put(b);
	}

	public static int readByteInt(ByteBuffer buffer) throws BufferUnderflowException {
		return buffer.get() & 0xFF;
	}

	public static void writeByteInt(ByteBuffer buffer, byte i) {
		buffer.put(i);
	}

	public static void writeByteInt(ByteBuffer buffer, int i) throws IllegalArgumentException, BufferOverflowException {
		if(i < 0x00 || i > 0xFF)
			throw new IllegalArgumentException("Byte Int must be between 0 and 255, not " + i);
		buffer.put((byte)i);
	}

	public static int readShortInt(ByteBuffer buffer) throws BufferUnderflowException {
		return buffer.getShort() & 0xFFFF;
	}

	public static void writeShortInt(ByteBuffer buffer, int i) throws IllegalArgumentException, BufferOverflowException {
		if(i < 0x0000 || i > 0xFFFF)
			throw new IllegalArgumentException("Short Int must be between 0 and 65535, not " + i);
		buffer.putShort((short)i);
	}

	public static long readLongInt(ByteBuffer buffer) throws BufferUnderflowException {
		return buffer.getInt() & 0xFFFFFFFFL;
	}

	public static void writeLongInt(ByteBuffer buffer, long i) throws IllegalArgumentException, BufferOverflowException {
		if(i < 0x00000000 || i > 0xFFFFFFFFL)
			throw new IllegalArgumentException("Long Int must be between 0 and 4294967295, not " + i);
		buffer.putInt((int)i);
	}

	public static long readUncheckedLong(ByteBuffer buffer) throws BufferUnderflowException {
		return buffer.getLong();
	}

	public static void writeUncheckedLong(ByteBuffer buffer, long l) throws IllegalArgumentException, BufferOverflowException {
		buffer.putLong(l);
	}

	public static Long readLongIntRemaining(ByteBuffer buffer) throws BufferUnderflowException {
		return buffer.remaining() < 4 ? null : buffer.getInt() & 0xFFFFFFFFL;
	}

	public static void writeLongIntRemaining(ByteBuffer buffer, Long i) throws IllegalArgumentException, BufferOverflowException {
		if(i != null) {
			if(i < 0x00000000 || i > 0xFFFFFFFFL)
				throw new IllegalArgumentException("Long Int must be between 0 and 4294967295, not " + i);
			buffer.putInt(i.intValue());
		}
	}

	public static VariableByteLengthNumber readVarIntRemaining(ByteBuffer buffer) throws BufferUnderflowException {
		int byteLength = buffer.remaining();
		if(byteLength > 4)
			byteLength = 4;
		switch(byteLength) {
			case 1:
				return new VariableByteLengthNumber(ProcessingUtils.readByteAsBCD(buffer), byteLength);
			case 2:
				return new VariableByteLengthNumber(ProcessingUtils.readShortInt(buffer), byteLength);
			case 3:
				return new VariableByteLengthNumber(ProcessingUtils.read3ByteInt(buffer), byteLength);
			case 4:
				return new VariableByteLengthNumber(ProcessingUtils.readLongInt(buffer), byteLength);
			default:
				throw new BufferUnderflowException();
		}
	}

	public static void writeVarIntRemaining(ByteBuffer buffer, VariableByteLengthNumber i) throws IllegalArgumentException, BufferOverflowException {
		if(i != null) {
			switch(i.getByteLength()) {
				case 1:
					ProcessingUtils.writeByteAsBCD(buffer, i.byteValue());
					break;
				case 2:
					ProcessingUtils.writeShortInt(buffer, i.intValue());
					break;
				case 3:
					ProcessingUtils.write3ByteInt(buffer, i.intValue());
					break;
				case 4:
					ProcessingUtils.writeLongInt(buffer, i.longValue());
					break;
				default:
					throw new IllegalArgumentException("ByteLength " + i.getByteLength() + " is not supported");
			}
		}
	}

	protected static BigInteger readStringInt(ByteBuffer buffer, boolean returnNull) throws BufferUnderflowException, NumberFormatException {
		int length = readByteInt(buffer);
		if(length==0){
			return returnNull == true ? null : new BigInteger("0");
		}
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return new BigInteger(new String(bytes, ProcessingConstants.US_ASCII_CHARSET));
	}

	public static BigInteger readStringInt(ByteBuffer buffer) throws BufferUnderflowException, NumberFormatException {
		return readStringInt(buffer, false);
	}

	public static BigInteger readStringIntNull(ByteBuffer buffer) throws BufferUnderflowException, NumberFormatException {
		return readStringInt(buffer, true);
	}

	public static void writeStringInt(ByteBuffer buffer, BigInteger i) throws IllegalArgumentException, BufferOverflowException {
		if(i == null) {
			writeShortString(buffer, null, ProcessingConstants.US_ASCII_CHARSET);
		} else {
			writeShortString(buffer, i.toString(), ProcessingConstants.US_ASCII_CHARSET);
		}
	}

	protected static BigDecimal readStringAmount(ByteBuffer buffer, boolean returnNull) throws BufferUnderflowException, NumberFormatException {
		int length = readByteInt(buffer);
		if(length == 0){
			return returnNull == true ? null : new BigDecimal("0");
		}
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return new BigDecimal(new String(bytes, ProcessingConstants.US_ASCII_CHARSET));
	}

	public static BigDecimal readStringAmount(ByteBuffer buffer) throws BufferUnderflowException, NumberFormatException {
		return readStringAmount(buffer, false);
	}

	public static BigDecimal readStringAmountNull(ByteBuffer buffer) throws BufferUnderflowException, NumberFormatException {
		return readStringAmount(buffer, true);
	}

	public static void writeStringAmount(ByteBuffer buffer, BigDecimal i) throws IllegalArgumentException, BufferOverflowException {
		if(i == null) {
			writeShortString(buffer, null,ProcessingConstants.US_ASCII_CHARSET);
		} else {
			writeShortString(buffer, i.toString(), ProcessingConstants.US_ASCII_CHARSET);
		}
	}

	public static String readString(ByteBuffer buffer, int length, Charset charset) throws BufferUnderflowException {
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return new String(bytes, charset);
	}

	public static byte[] readBytes(ByteBuffer buffer, int length) throws BufferUnderflowException {
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return bytes;
	}
	public static void writeString(ByteBuffer buffer, String string, Charset charset) throws BufferUnderflowException {
		if(string!=null){
			buffer.put(string.getBytes(charset));
		}
	}
	public static void writeString(ByteBuffer buffer, StringBuilder stringBuilder, Charset charset) throws BufferUnderflowException {
		if(stringBuilder != null) {
			charset.newEncoder().encode(CharBuffer.wrap(stringBuilder), buffer, true);
		}
	}
	public static String readShortString(ByteBuffer buffer, Charset charset) throws BufferUnderflowException {
		int length = readByteInt(buffer);
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return new String(bytes, charset);
	}

	public static byte[] readShortBytes(ByteBuffer buffer) throws BufferUnderflowException {
		int length = readByteInt(buffer);
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return bytes;
	}

	public static void writeShortBytes(ByteBuffer buffer, byte[] bytes) throws IllegalArgumentException, BufferOverflowException {
		writeShortBytes(buffer, bytes, 0, bytes == null ? 0 : bytes.length);
	}
	public static void writeShortBytes(ByteBuffer buffer, byte[] bytes, int offset, int length) throws IllegalArgumentException, BufferOverflowException {
		if(bytes == null || bytes.length == 0) {
			writeByteInt(buffer, 0);
		} else {
			int len = Math.min(255, bytes.length);
		    writeByteInt(buffer,len);
			buffer.put(bytes, offset, len);
		}
	}

	public static void writeShortString(ByteBuffer buffer, String string, Charset charset) throws IllegalArgumentException, BufferOverflowException {
		if(string == null) {
			writeByteInt(buffer, 0);
		} else {
			byte[] bytes = string.getBytes(charset);
			int len = Math.min(255, bytes.length);
		    writeByteInt(buffer, len);
		    buffer.put(bytes, 0, len);
		}
	}

	public static String readLongString(ByteBuffer buffer, Charset charset) throws BufferUnderflowException {
		int length = readShortInt(buffer);
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return new String(bytes, charset);
	}

	public static void writeLongString(ByteBuffer buffer, String string, Charset charset) throws IllegalArgumentException, BufferOverflowException {
		if(string == null) {
			writeShortInt(buffer, 0);
		} else {
			byte[] bytes = string.getBytes(charset);
			int len = Math.min(65535, bytes.length);
		    writeShortInt(buffer, len);
		    buffer.put(bytes, 0, len);
		}
	}

	public static byte[] readLongBytes(ByteBuffer buffer) throws BufferUnderflowException {
		int length = readShortInt(buffer);
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return bytes;
	}
	public static void writeLongBytes(ByteBuffer buffer, byte[] bytes) throws IllegalArgumentException, BufferOverflowException {
		writeLongBytes(buffer, bytes, 0, bytes == null ? 0 : bytes.length);
	}
	public static void writeLongBytes(ByteBuffer buffer, byte[] bytes, int offset, int length) throws IllegalArgumentException, BufferOverflowException {
		if(bytes == null || bytes.length == 0) {
			writeShortInt(buffer, 0);
		} else {
			int len = Math.min(65535, bytes.length);
		    writeShortInt(buffer,len);
			buffer.put(bytes, offset, len);
		}
	}

	public static void writeLongBytes(ByteBuffer toBuffer, ByteBuffer fromBuffer) throws IllegalArgumentException, BufferOverflowException {
		if(fromBuffer == null || fromBuffer.remaining() == 0) {
			writeShortInt(toBuffer, 0);
		} else {
			int len = Math.min(65535, fromBuffer.remaining());
			writeShortInt(toBuffer, len);
			int pos = fromBuffer.position();
			int limit = fromBuffer.limit();
			if(fromBuffer.remaining() != len)
				fromBuffer.limit(pos + limit);
			toBuffer.put(fromBuffer);
			fromBuffer.position(pos);
			fromBuffer.limit(limit);
		}
	}
	public static byte[] readBytesRemaining(ByteBuffer buffer) throws BufferUnderflowException {
		return readBytes(buffer, buffer.remaining());
	}

	public static void writeBytesRemaining(ByteBuffer buffer, byte[] bytes) {
		if(bytes != null)
			buffer.put(bytes);
	}
	public static Calendar readTimestamp(ByteBuffer buffer) throws BufferUnderflowException {
		long time = readLongInt(buffer) * 1000;
		int offsetMillis = buffer.get() * 15 * 60 * 1000;
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTimeInMillis(time);
		calendar.setTimeZone(ConvertUtils.createTimeZone(offsetMillis));
		return calendar;
	}

	public static byte[] readTimestampRaw(ByteBuffer buffer) throws BufferUnderflowException {
		byte[] bytes = new byte[5];
		buffer.get(bytes);
		return bytes;
	}

	public static void writeTimestamp(ByteBuffer buffer, Calendar timestamp) throws IllegalArgumentException, BufferOverflowException {
		writeLongInt(buffer, timestamp.getTimeInMillis() / 1000);
		buffer.put((byte)(timestamp.getTimeZone().getOffset(timestamp.getTimeInMillis()) / (15*60*1000)));
	}

	public static byte[] timestampToBytes(Calendar timestamp) {
		if(timestamp == null)
			return null;
		byte[] bytes = new byte[5];
		ByteArrayUtils.writeUnsignedLong(bytes, 0, timestamp.getTimeInMillis() / 1000);
		bytes[4] = (byte)(timestamp.getTimeZone().getOffset(timestamp.getTimeInMillis()) / (15*60*1000));
		return bytes;
	}
	public static Calendar bytesToTimestamp(byte[] bytes) {
		if(bytes == null)
			return null;
		return readTimestamp(ByteBuffer.wrap(bytes));
	}
	public static CRC readCRC(ByteBuffer buffer) throws BufferUnderflowException, InvalidByteValueException {
		CRCType crcType = CRCType.getByValue(buffer.get());
		byte[] crcValue = ProcessingUtils.readBytes(buffer,crcType.getLength());
		return new CRC(crcType, crcValue);
	}

	public static void writeCRC(ByteBuffer buffer, CRC crc) throws IllegalArgumentException, BufferOverflowException {
		if(crc != null) {
			buffer.put(crc.getCrcType().getValue());
			buffer.put(crc.getCrcValue(), 0, crc.getCrcType().getLength());
		}
	}

	public static Currency readCurrency(ByteBuffer buffer, Charset charset) throws BufferUnderflowException {
		return Currency.getInstance(readString(buffer, 3, charset));
	}

	public static void writeCurrency(ByteBuffer buffer, Currency currency, Charset charset) throws IllegalArgumentException, BufferOverflowException {
		if(currency == null)
			throw new IllegalArgumentException("Currency cannot be null");
		byte[] bytes = currency.getCurrencyCode().getBytes(charset);
		if(bytes.length != 3)
			throw new IllegalArgumentException("Invalid currency code '" + currency.getCurrencyCode() + "'. Not three bytes");
		buffer.put(bytes);
	}
	public static Map<Integer,String> readPropertyValueList(ByteBuffer buffer, Charset charset) throws ParseException, BufferUnderflowException {
		int length = readShortInt(buffer);
		ByteBuffer slice = buffer.slice();
		slice.limit(length);
		buffer.position(buffer.position() + length);
		InputStream in = new ByteBufferInputStream(slice);
		final Map<Integer,String> propertyValueList = new LinkedHashMap<Integer,String>();
		try {
			parsePropertyList(in, charset, new PropertyValueHandler<RuntimeException>() {
				@Override
				public void handleProperty(int propertyIndex, String propertyValue) {
					propertyValueList.put(propertyIndex, propertyValue);
				}
			});
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
		return propertyValueList;
	}

	public static void writePropertyValueList(ByteBuffer buffer, Map<Integer,String> propertyValueList, Charset charset) throws IllegalArgumentException, BufferOverflowException {
		writePropertyValueList(buffer, propertyValueList, charset, false);
	}

	public static void writePropertyValueList(ByteBuffer buffer, Map<Integer,String> propertyValueList, Charset charset, boolean maskSensitiveData) throws IllegalArgumentException, BufferOverflowException {
		if(propertyValueList == null || propertyValueList.isEmpty()) {
			writeShortInt(buffer, 0);
		} else {
			int pos = buffer.position();
			buffer.position(pos + 2);
			EncodingAppendable appendTo = new EncodingAppendable(charset, buffer);
			try {
				MessageDataUtils.appendEfficientPropertyValueList(appendTo, propertyValueList, maskSensitiveData);
			} catch(IOException e) {
				throw new UndeclaredThrowableException(e);
			}
			buffer.putShort(pos, (short) (buffer.position() - pos - 2));
		}
	}

	public static Set<Integer> readPropertyIndexList(ByteBuffer buffer, Charset charset) throws BufferUnderflowException, ParseException {
		return parsePropertyIndexList(readLongString(buffer, charset));
	}

	public static void writePropertyIndexList(ByteBuffer buffer, Set<Integer> propertyIndexList, Charset charset) throws IllegalArgumentException, BufferOverflowException {
		if(propertyIndexList == null || propertyIndexList.isEmpty()) {
			writeShortInt(buffer, 0);
		} else {
			int pos = buffer.position();
			buffer.position(pos + 2);
			EncodingAppendable appendTo = new EncodingAppendable(charset, buffer);
			try {
				MessageDataUtils.appendPropertyIndexList(appendTo, propertyIndexList);
			} catch(IOException e) {
				throw new UndeclaredThrowableException(e);
			}
			appendTo.append(MessageDataUtils.PROPERTY_VALUE_SEPARATOR); //<LF>
			buffer.putShort(pos, (short)(buffer.position() - pos - 2));
		}
	}

	public static Date readDate(ByteBuffer buffer) throws BufferUnderflowException {
		long l = buffer.getLong();
		if(l == Long.MAX_VALUE)
			return null;
		return new Date(l);
	}

	public static void writeDate(ByteBuffer buffer, Date d) throws BufferOverflowException {
		if(d == null)
			buffer.putLong(Long.MAX_VALUE);
		else
			buffer.putLong(d.getTime());
	}

	public static Boolean readBoolean(ByteBuffer buffer) throws BufferUnderflowException {
		switch(buffer.get()) {
			case -1:
				return null;
			case 0:
				return false;
			default:
				return true;
		}
	}

	public static void writBoolean(ByteBuffer buffer, Boolean b) throws BufferOverflowException {
		if(b == null)
			buffer.put((byte) -1);
		else if(b.booleanValue())
			buffer.put((byte) 1);
		else
			buffer.put((byte) 0);
	}

	//for legacy messages
	public static int read3ByteInt(ByteBuffer buffer) throws BufferUnderflowException {
		return ((buffer.get() & 0xFF) << 16)
				| ((buffer.get() & 0xFF) << 8)
				| ((buffer.get() & 0xFF) << 0);
	}

	public static void write3ByteInt(ByteBuffer buffer, int i) throws IllegalArgumentException, BufferOverflowException {
		if(i < 0x00000000 || i > 0xFFFFFF)
			throw new IllegalArgumentException("3Byte Int must be between 0 and 16777215, not " + i);
		buffer.put((byte)(i >> 16));
		buffer.put((byte)(i >> 8));
		buffer.put((byte)(i >> 0));
	}

	public static void writeBytes(ByteBuffer buffer, InputStream in, int length) throws BufferOverflowException, IOException {
		byte[] bytes = new byte[Math.min(length, 512)];
		int read = 0;
        int count = 0;
        while((read = in.read(bytes)) > -1) {
            count += read;
            if(count >= length) {
            	 buffer.put(bytes, 0, read - (count - length));
            	 return;
            }
			buffer.put(bytes, 0, read);
        }
        if(count < length)
        	throw new EOFException("Not enough bytes could be read from inputstream");
    }

	public static void logAttributes(Log log, String prefix, Map<String, Object> attributes, String[] attributeKeys, String[] additionalAttributeKeys)
	{
		StringBuilder sb = new StringBuilder(prefix);
		for (int i=0; i<attributeKeys.length; i++)
		{
			sb.append(attributeKeys[i]);
			sb.append("=");
			sb.append(attributes.get(attributeKeys[i]));
			if (i < attributeKeys.length - 1)
				sb.append("; ");
		}
		if (additionalAttributeKeys != null)
		{
			sb.append("; ");
			for (int i=0; i<additionalAttributeKeys.length; i++)
			{
				sb.append(additionalAttributeKeys[i]);
				sb.append("=");
				sb.append(attributes.get(additionalAttributeKeys[i]));
				if (i < additionalAttributeKeys.length - 1)
					sb.append("; ");
			}
		}
		log.info(sb.toString());
	}

	public static void logAttributes(Log log, String prefix, Map<String, Object> attributes, int index, String[] attributeKeys, String[] additionalAttributeKeys) {
		StringBuilder sb = new StringBuilder(prefix);
		for(int i = 0; i < attributeKeys.length; i++) {
			String key = index < 1 ? attributeKeys[i] : attributeKeys[i] + '.' + index;
			sb.append(key);
			sb.append("=");
			sb.append(attributes.get(key));
			if(i < attributeKeys.length - 1)
				sb.append("; ");
		}
		if(additionalAttributeKeys != null) {
			sb.append("; ");
			for(int i = 0; i < additionalAttributeKeys.length; i++) {
				String key = index < 1 ? additionalAttributeKeys[i] : additionalAttributeKeys[i] + '.' + index;
				sb.append(key);
				sb.append("=");
				sb.append(attributes.get(key));
				if(i < additionalAttributeKeys.length - 1)
					sb.append("; ");
			}
		}
		log.info(sb.toString());
	}

	public static void rollbackDbConnection(Log log, Connection conn) {
		try {
			if(conn != null)
				conn.rollback();
		} catch(SQLException e) {
			log.warn("Could not rollback", e);
		}
	}

	public static void closeDbConnection(Log log, Connection conn) {
		try {
			if(conn != null)
				conn.close();
		} catch(SQLException e) {
			log.warn("Could not close connection", e);
		}
	}
	
	public static void closeDbResultSet(Log log, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				log.warn("Could not close result set", e);
			}
		}
	}
	
	public static void closeDbStatement(Log log, Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				log.warn("Could not close statement", e);
			}
		}
	}

	public static int generateRandomNumber(int lBound, int uBound) {
		// lBound <= random < uBound
		return (int) (lBound + Math.random() * (uBound - lBound));
	}

	public static String generateRandomDailySchedule(int hourLBound, int hourUBound) {
		int callInHour = ProcessingUtils.generateRandomNumber(hourLBound, hourUBound);
		int callInMinute = ProcessingUtils.generateRandomNumber(0, 60);
		return "D^" + String.format("%02d", callInHour) + String.format("%02d", callInMinute);
	}

	public static String generateRandomWeeklySchedule(int hourLBound, int hourUBound, int day) {
		int callInHour = ProcessingUtils.generateRandomNumber(hourLBound, hourUBound);
		int callInMinute = ProcessingUtils.generateRandomNumber(0, 60);
		return "W^" + String.format("%02d", callInHour) + String.format("%02d", callInMinute) + '^' + day;
	}

	public static String generateRandomIntervalSchedule(int intervalSec) {
		int utcOffset = ProcessingUtils.generateRandomNumber(0, 86400);
		return "I^" + utcOffset + '^' + intervalSec;
	}

	/* NOTE: Removed because Gx devices format BCD incorrectly, they may send hex characters in BCD values.
	 * The writeBCDInt should be used instead to accommodate Gx BCD bugs...	
	public static void writeBCDInt(ByteBuffer buffer, int i) throws IllegalArgumentException, BufferOverflowException {
		if(i < 0 || i > 99999999)
			throw new IllegalArgumentException("BCD number must be between 0 and 99999999");
		String number = StringUtils.pad(String.valueOf(i), '0', 8, StringUtils.Justification.RIGHT);
		byte[] bcdArray = ByteArrayUtils.fromHex(number);
		buffer.put(bcdArray);
	}*/
	
	public static void writeBCDInt(ByteBuffer buffer, BCDInt bcdInt) throws IllegalArgumentException, BufferOverflowException {
		if (bcdInt != null)
			buffer.put(ByteArrayUtils.fromHex(bcdInt.getHex()));
	}	

	/* NOTE: Removed because Gx devices format BCD incorrectly, they may send hex characters in BCD values.
	 * The readBCDInt function below that was directly ported from ReRix should be used instead to accommodate Gx BCD bugs... 
	public static int readBCDInt(ByteBuffer buffer) throws BufferUnderflowException {
		byte[] bytes = new byte[4];
		buffer.get(bytes);
		return Integer.parseInt(StringUtils.toHex(bytes));
	}*/
	
	public static BCDInt readBCDInt(ByteBuffer buffer) throws BufferUnderflowException {
		byte[] bytes = new byte[4];
		buffer.get(bytes);
		int num = 0, lsn, msn, digit;
		for (int i=0; i<4; i++) {
			digit = bytes[i] & 0xFF;
			lsn = digit % 16;
			msn = digit / 16;
			num = (num * 10 + msn) * 10 + lsn; 
		}
		return new BCDInt(num, StringUtils.toHex(bytes));
	}	

	public static int readByteAsBCD(ByteBuffer buffer) throws BufferUnderflowException {
		int digit = buffer.get() & 0xFF;
		return (digit / 16) * 10 + (digit % 16);
	}

	public static void writeByteAsBCD(ByteBuffer buffer, int i) throws IllegalArgumentException, BufferOverflowException {
		if(i < 0 || i > 99)
			throw new IllegalArgumentException("Byte BCD must be between 0 and 99, not " + i);
		buffer.put((byte) (i / 10 * 16 + (i % 10)));
	}
	public static String decimalToHex(long d, int width) {
		return StringUtils.pad(StringUtils.toHex(d), '0', width, Justification.RIGHT);
    }

	public static String decimalToHex(Number d, int width) {
		return decimalToHex(d.longValue(), width);
    }
	public static void writeByteTimestamp(ByteBuffer buffer, long time){
		Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		calendar.setTimeInMillis(time);
		buffer.put((byte)calendar.get(Calendar.HOUR_OF_DAY));
		buffer.put((byte)calendar.get(Calendar.MINUTE));
		buffer.put((byte)calendar.get(Calendar.SECOND));
		buffer.put((byte)(calendar.get(Calendar.MONTH)+1));
		buffer.put((byte)calendar.get(Calendar.DAY_OF_MONTH));
		buffer.put((byte)(calendar.get(Calendar.YEAR)/100));
		int year2=(calendar.get(Calendar.YEAR)-calendar.get(Calendar.YEAR)/100*100);
		buffer.put((byte)year2);
	}

	public static long readByteTimestamp(ByteBuffer buffer) throws BufferUnderflowException {
		int hour=buffer.get() & 0xFF;
		int min=buffer.get() & 0xFF;
		int seconds=buffer.get() & 0xFF;
		int month=buffer.get() & 0xFF;
		int dayOfMonth=buffer.get() & 0xFF;
		int year1=buffer.get()& 0xFF;
		int year2=buffer.get()& 0xFF;
		Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(year1*100+year2, month-1, dayOfMonth, hour, min, seconds);
		return calendar.getTimeInMillis();
	}

	public static void writeBCDTimestamp(ByteBuffer buffer, long time){
		Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		calendar.setTimeInMillis(time);
		String dateStr = bcdDateFormat.format(calendar.getTime());
		buffer.put(ByteArrayUtils.fromHex(dateStr));
	}
	public static long readBCDTimestamp(ByteBuffer buffer) throws BufferUnderflowException, ParseException{
		byte[] bcdTime=new byte[7];
		buffer.get(bcdTime);
		Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		calendar.setTime(bcdDateFormat.parse(StringUtils.toHex(bcdTime)));
		return calendar.getTimeInMillis();
	}

	public static void writeEncryptionKey(ByteBuffer buffer, byte[] encryptionKey) throws IllegalArgumentException{
		if(encryptionKey.length!=16){
			throw new IllegalArgumentException("Invalid encryptionKey length:"+encryptionKey.length+". Legacy encryption key is 16 bytes.");
		}
		buffer.put(encryptionKey);
	}

	public static byte[] readEncryptionKey(ByteBuffer buffer) throws BufferUnderflowException {
		byte[] bytes = new byte[16];
		buffer.get(bytes);
		return bytes;
	}
	
	public static void writeFileTransferEncryptionKey(ByteBuffer buffer, byte[] encryptionKey) throws IllegalArgumentException{
		writeByteInt(buffer, encryptionKey.length);
		buffer.put(encryptionKey);
	}

	public static byte[] readFileTransferEncryptionKey(ByteBuffer buffer) throws BufferUnderflowException {
		int length = readByteInt(buffer);
		byte[] bytes = new byte[length];
		buffer.get(bytes);
		return bytes;
	}

	public static String[] readStringArray(ByteBuffer buffer) throws BufferUnderflowException {
		String versionString = new String(readBytesRemaining(buffer), ProcessingConstants.US_ASCII_CHARSET);
		return StringUtils.split(versionString, "\n");
	}

	public static void writeStringArray(ByteBuffer buffer, String[] versionString){
		buffer.put(StringUtils.join(versionString, "\n").getBytes(ProcessingConstants.US_ASCII_CHARSET));
	}

	public static int readPrev4BitInt(ByteBuffer buffer) throws BufferUnderflowException {
		return (buffer.get(buffer.position()-1) & 0x0F);
	}

	public static void writePrev4BitInt(ByteBuffer buffer, int i) {
		if(i < 0x00 || i > 0x0F)
			throw new IllegalArgumentException("4-bit Int must be between 0 and 15, not " + i);
		byte b = buffer.get(buffer.position()-1);
		b = (byte)(b | i);
		buffer.put(buffer.position()-1, b);
	}

	public static int read4BitInt(ByteBuffer buffer) throws BufferUnderflowException {
		return ((buffer.get() >> 4) & 0x0F);
	}

	public static void write4BitInt(ByteBuffer buffer, int i) {
		if(i < 0x00 || i > 0x0F)
			throw new IllegalArgumentException("4-bit Int must be between 0 and 15, not " + i);
		buffer.put((byte)(i << 4));
	}
	/*
	public static byte readPrev4BitInt(ByteBuffer buffer) throws BufferUnderflowException {
		return (byte)(buffer.get(buffer.position()-1)>>4 & 0x0f);
	}

	public static void writePrev4BitInt(ByteBuffer buffer, int anInt) {
		byte i=(byte)anInt;
		if(i < 0x00 || i > 0x0f)
			throw new IllegalArgumentException("4-bit Int must be between 0 and 15, not " + i);
		byte b = buffer.get(buffer.position()-1);
		b = (byte)(b | i<<4);
		buffer.put(buffer.position()-1, i);
	}

	public static byte read4BitInt(ByteBuffer buffer) throws BufferUnderflowException {
		return (byte)(buffer.get()& 0x0f);
	}

	public static void write4BitInt(ByteBuffer buffer, int anInt) {
		byte i= (byte)anInt;
		if(i < 0x00 || i > 0x0f)
			throw new IllegalArgumentException("4-bit Int must be between 0 and 15, not " + i);
		buffer.put(i);
	}
*/
	public static void write10ByteString(ByteBuffer buffer, String string, Charset charset) throws BufferUnderflowException {
		buffer.put(StringUtils.pad(string, ' ', 10, Justification.LEFT).getBytes(charset));
	}

	public static String read10ByteString(ByteBuffer buffer, Charset charset)throws BufferUnderflowException {
		return readString(buffer, 10, charset);
	}

	public static void write3ByteString(ByteBuffer buffer, String string, Charset charset) throws BufferUnderflowException {
		buffer.put(StringUtils.pad(string, ' ', 3, Justification.LEFT).getBytes(charset));
	}

	public static String read3ByteString(ByteBuffer buffer, Charset charset)throws BufferUnderflowException {
		return readString(buffer, 3, charset);
	}
	
	public static void writeIP(ByteBuffer buffer, byte[] ipAddress) throws IllegalArgumentException{
		if(ipAddress.length!=4){
			throw new IllegalArgumentException("Invalid ipAddress length:"+ipAddress.length+" ipAddress is 4 bytes.");
		}
		buffer.put(ipAddress);
	}

	public static byte[] readIP(ByteBuffer buffer) throws BufferUnderflowException {
		byte[] bytes = new byte[4];
		buffer.get(bytes);
		return bytes;
	}
	
	public static CRCLegacy readCRCLegacy(ByteBuffer buffer) throws BufferUnderflowException, InvalidByteValueException {
		CRCTypeLegacy crcType = CRCTypeLegacy.getByValue(buffer.get());
		byte[] crcValue = ProcessingUtils.readBytes(buffer,crcType.getLength());
		return new CRCLegacy(crcType, crcValue);
	}

	public static void writeCRCLegacy(ByteBuffer buffer, CRCLegacy crc) throws IllegalArgumentException, BufferOverflowException {
		if(crc != null) {
			buffer.put(crc.getCrcType().getValue());
			buffer.put(crc.getCrcValue(), 0, crc.getCrcType().getLength());
		}
	}

	public static void checkMessageExpiration(MessageChainTaskInfo taskInfo) throws RetrySpecifiedServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = step.getAttributes();
		long currentTime = System.currentTimeMillis();
		long messageStartTime = ConvertUtils.convertSafely(Long.class, attributes.get("serverTime"), currentTime);
		long sessionTimeout = ConvertUtils.convertSafely(Long.class, attributes.get("sessionTimeout"), 20000L);
		if (messageStartTime + sessionTimeout < currentTime)
			throw new RetrySpecifiedServiceException("Message has expired, messageStartTime: " + messageStartTime + ", sessionTimeout: " + sessionTimeout + ", currentTime: " + currentTime, WorkRetryType.NO_RETRY);
	}	
	
	public static boolean isNetlayerWaiting(MessageChainTaskInfo taskInfo) {
		return isNetlayerWaiting(taskInfo.getStep());
	}
	protected static boolean isNetlayerWaiting(MessageChainStep step) {	
		MessageChainStep[] nextSteps = step.getNextSteps(0);
		if(nextSteps == null || nextSteps.length == 0)
			return false;
		for(MessageChainStep nextStep : nextSteps) {
			if(nextStep.getQueueKey().startsWith("netlayer-"))
				return true;
		}
		for(MessageChainStep nextStep : nextSteps) {
			if(isNetlayerWaiting(nextStep))
				return true;
		}
		return false;
	}	
	
	public static DeviceInfo getDeviceInfoSafely(DeviceInfoManager deviceInfoManager, InterruptGuard guard, Map<String, Object> attributes, Log log) throws ServiceException {
		String deviceName = ConvertUtils.getStringSafely(attributes.get("deviceName"));
		DeviceInfo deviceInfo = deviceInfoManager.getDeviceInfo(deviceName, OnMissingPolicy.RETURN_NULL, false, guard, false); //NOTE: Device may be deactivated
		if(deviceInfo == null) {
			DeviceType deviceType;
			try {
				deviceType = ConvertUtils.convert(DeviceType.class, attributes.get("deviceType"));
			} catch(ConvertException e) {
				log.info("Could not convert deviceType '" + attributes.get("deviceType") + " to DeviceType object - using DEFAULT");
				deviceType = null;
			}
			deviceInfo = new NoUpdateDeviceInfo(deviceName, null, null, null, 0L, 0L, deviceType, null, false, null, null, null, null, null, null, null, null, 0L, null, null, null, null, null, null, 0L, null, false);
		}
		return deviceInfo;
	}
	
	public static AbstractPrefixedLog getPrefixedLog(final Log log, final String prefix) {
		return new AbstractPrefixedLog(log) {
			/**
			 * @see simple.io.logging.AbstractPrefixedLog#prefixMessage(java.lang.Object)
			 */
			@Override
			protected Object prefixMessage(Object message) {
				return prefix + message;
			}
		};
	}
	
	public static void parseESudsGenericLog(Map<String,Object> params, byte[] genericLog) throws ServiceException{
		try{
			short diag_code=ByteArrayUtils.readShort(genericLog, 0);
			String objectName;
			try{
				objectName=ESudsLogType.getByValue(diag_code).toString();
			}catch(InvalidIntValueException e){
				objectName="UNKNOWN_CODE_"+diag_code;
			}
			long time = ByteArrayUtils.readLong(genericLog, 2);
			Calendar returnCalendar = Calendar.getInstance();
			returnCalendar.clear();
			returnCalendar.setTimeInMillis(time);
			String genericData = ProcessingUtils.esudsDateFormat.format(returnCalendar.getTime());
			params.put("genericData", genericData);
	        params.put("objectName", objectName);
		}catch(IOException e){
			throw new ServiceException("Failed to parse eSuds generic data log.", e);
		}
	}
	
	public static byte[] parseTLV(byte[] data, byte tagPrefix, byte tag) {
		if (data == null)
			return null;
		byte[] value = null;
		int dataLength = data.length;
		int length;
		for (int i=0; i<dataLength; i++) {
			if (data[i] == tagPrefix) {
				if (dataLength < i + 3)
					break;
				length = data[i + 2] & 0xFF;
				if (dataLength < i + 3 + length)
					break;
				if (data[i + 1] == tag) {
					value = new byte[length];
					System.arraycopy(data, i + 3, value, 0, length);
					break;
				}
				i += 3 + length;
			}
		}
		return value;
	}
	
	public static void sendEmail(String fromEmail, String fromName, String toEmail, String toName, String subject, String body, Connection conn) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fromEmail", fromEmail);
		params.put("fromName", fromName);
		params.put("toEmail", toEmail);
		params.put("toName", toName);
		params.put("subject", subject);
		params.put("body", body);
		if (conn == null)
			DataLayerMgr.executeUpdate("SEND_EMAIL", params, true);
		else
			DataLayerMgr.executeUpdate(conn, "SEND_EMAIL", params);
	}
}
