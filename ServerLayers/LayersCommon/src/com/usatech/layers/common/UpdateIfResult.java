package com.usatech.layers.common;

public enum UpdateIfResult {
	NO_CHANGE, UPDATED, TOO_HIGH, TOO_LOW;
}
