package com.usatech.layers.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import simple.app.ServiceException;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.StringUtils;
import com.trilead.ssh2.SFTPException;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.SFTPv3FileAttributes;
import com.trilead.ssh2.SFTPv3FileHandle;
import com.trilead.ssh2.sftp.ErrorCodes;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * The task pulls a file from the server via SecureFTP
 * 
 * @author bkrug
 * 
 */
public class UploadSftpTask extends AbstractSftpTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;

	@Override
	protected int processSftp(MessageChainTaskInfo taskInfo, SFTPv3Client sftp) throws IOException, ServiceException {
		String filePath;
		String resourceKey;
		long modificationTime;
		String renameFilePath;
		MessageChainStep step = taskInfo.getStep();
		try {
			filePath = step.getAttribute(CommonAttrEnum.ATTR_FILE_PATH, String.class, true);
			resourceKey = step.getAttribute(CommonAttrEnum.ATTR_RESOURCE, String.class, true);
			modificationTime = step.getAttributeDefault(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, Long.class, 0L);
			String fileRenameMatch = step.getAttribute(CommonAttrEnum.ATTR_FILE_RENAME_MATCH, String.class, false);
			String fileRenameReplace = step.getAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, String.class, false);
			if(StringUtils.isBlank(fileRenameReplace))
				renameFilePath = null; // No renaming
			else if(StringUtils.isBlank(fileRenameMatch))
				renameFilePath = fileRenameReplace; // Literal renaming: rename to "fileRenameReplace" value
			else
				renameFilePath = Pattern.compile(fileRenameMatch).matcher(filePath).replaceAll(fileRenameReplace); // Use regex replace to determine new name of file
		} catch(AttributeConversionException e) {
			log.error("Could not convert attributes", e);
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, StringUtils.exceptionToString(e));
			return 2;
		}
		boolean success = uploadFile(sftp, filePath, resourceKey, modificationTime, renameFilePath);
		if(!success)
			return 1;
		return 0;
	}

	protected boolean uploadFile(SFTPv3Client sftp, String filePath, String resourceKey, long modificationTime, String renameFilePath) throws IOException {
		if(modificationTime > 0L) {
			try {
				SFTPv3FileAttributes atts;
				if(!StringUtils.isBlank(renameFilePath))
					atts = sftp.stat(renameFilePath);
				else
					atts = sftp.stat(filePath);
				if(atts.mtime != null && atts.mtime > 0L && atts.mtime * 1000L >= modificationTime) {
					log.info("File at '" + filePath + "' is newer than upload. not changed.");
					return false;
				}
				log.info("Replacing file '" + filePath + "' with modified time of " + atts.mtime + " and size of " + atts.size);
			} catch(SFTPException e) {
				switch(e.getServerErrorCode()) {
					case ErrorCodes.SSH_FX_NO_SUCH_FILE:
					case ErrorCodes.SSH_FX_NO_SUCH_PATH:
						// Ignore
						break;
					default:
						throw e;
				}
			}
		} else
			log.info("Uploading resource to '" + filePath + "'");
		Resource resource = resourceFolder.getResource(resourceKey, ResourceMode.READ);
		long fileOffset = 0L;
		try {
			SFTPv3FileHandle handle = sftp.createFileTruncate(filePath);
			try {
				InputStream in = resource.getInputStream();
				try {
					byte[] buffer = new byte[bufferSize];
					int r;
					while((r = in.read(buffer)) >= 0) {
						sftp.write(handle, fileOffset, buffer, 0, r);
						fileOffset += r;
					}
					log.info("Wrote " + fileOffset + " bytes to file '" + filePath + "'");
				} finally {
					in.close();
				}
				if(!StringUtils.isBlank(renameFilePath)) {
					try {
						sftp.rm(renameFilePath);
					} catch(SFTPException e) {
						switch(e.getServerErrorCode()) {
							case ErrorCodes.SSH_FX_NO_SUCH_FILE:
							case ErrorCodes.SSH_FX_NO_SUCH_PATH:
								// Ignore
								break;
							default:
								log.warn("Could not delete file '" + renameFilePath + "' that is being replaced", e);
						}
					}
					sftp.mv(filePath, renameFilePath);
					log.info("Renamed file '" + filePath + "' to '" + renameFilePath + "'");
				}
			} finally {
				sftp.closeFile(handle);
			}
		} finally {
			resource.release();
		}
		return true;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
