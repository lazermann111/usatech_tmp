/**
 *
 */
package com.usatech.layers.common;

import java.io.IOException;

/**
 * @author Brian S. Krug
 *
 */
public class CountingAppendable implements Appendable {
	protected int size = 0;
	/**
	 * @see java.lang.Appendable#append(java.lang.CharSequence)
	 */
	@Override
	public Appendable append(CharSequence csq) throws IOException {
		size += csq.length();
		return this;
	}

	/**
	 * @see java.lang.Appendable#append(char)
	 */
	@Override
	public Appendable append(char c) throws IOException {
		size++;
		return this;
	}

	/**
	 * @see java.lang.Appendable#append(java.lang.CharSequence, int, int)
	 */
	@Override
	public Appendable append(CharSequence csq, int start, int end) throws IOException {
		size += (end - start);
		return this;
	}

	public int getSize() {
		return size;
	}

	public void reset() {
		size = 0;
	}
}
