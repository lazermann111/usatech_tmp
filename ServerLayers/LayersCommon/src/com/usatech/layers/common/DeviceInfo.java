/**
 *
 */
package com.usatech.layers.common;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.util.Map;

import simple.app.ServiceException;
import simple.util.IntegerRangeSet;

import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;

/**
 * @author Brian S. Krug
 *
 */
public interface DeviceInfo {

	/** Rolls back any changes made since last clear or commit
	 * @throws ServiceException
	 */
	public void clearChanges() ;

	/**
	 * Commits any pending changes for this device info. Implementations may commit changes before this is called,
	 * but must commit any pending changes when this is called.
	 * 
	 * @param timestamp
	 *            The time of the changes (for conflict resolution)
	 * @throws ServiceException
	 */
	public void commitChanges(long timestamp) throws ServiceException;

	public void commitChanges(long timestamp, boolean internalOnly) throws ServiceException;

	/**
	 * Returns the committed timestamp of the particular property or 0 if property does not exist
	 * 
	 * @param property
	 * @return
	 */
	public long getTimestamp(DeviceInfoProperty property);

	/** Compares the provided rejectUntil with the currently stored value and updates it if the provided is greater than
	 * the currently stored value.
	 * @param rejectUntil
	 * @return <code>true</code>, if and only if the provided rejectUntil is greater than the currently stored value
	 */
	public UpdateIfResult updateRejectUntil(long rejectUntil);

	/** Compares the provided masterId with the currently stored value and updates it if the provided is greater than
	 * the currently stored value.
	 * @param masterId
	 * @return <code>true</code>, if and only if the provided masterId is greater than the currently stored value
	 */
	public UpdateIfResult updateMasterId(long masterId);

	public long getMasterId();

	public long getRejectUntil();

	public byte[] getEncryptionKey();

	public byte[] getPreviousEncryptionKey();

	public DeviceType getDeviceType();

	public String getLocale();

	public boolean isInitOnly();

	public Charset getDeviceCharset();
	
	public void setDeviceSerialCd(String deviceSerialCd);

	public void setEncryptionKey(byte[] encryptionKey);

	public void setPreviousEncryptionKey(byte[] previousEncryptionKey);

	public void setLocale(String locale);

	public void setDeviceCharset(Charset deviceCharset);

	public String getTimeZoneGuid();

	public void setTimeZoneGuid(String timeZoneGuid);

	public ActivationStatus getActivationStatus();

	public void setActivationStatus(ActivationStatus activationStatus);

	public ServerActionCode getActionCode();

	public void setActionCode(ServerActionCode actionCode);
	
	public void setUsername(String username);
	
	public void setPasswordHash(byte[] passwordHash);
	
	public void setPasswordSalt(byte[] passwordSalt);
	
	public void setCredentialTimestamp(long credentialTimestamp);
	
	public void setPreviousUsername(String previousUsername);
	
	public void setPreviousPasswordHash(byte[] previousPasswordHash);
	
	public void setPreviousPasswordSalt(byte[] previousPasswordSalt);
	
	public void setNewUsername(String newUsername);
	
	public void setNewPasswordHash(byte[] newPasswordHash);
	
	public void setNewPasswordSalt(byte[] newPasswordSalt);
	
	public void setMaxAuthAmount(long maxAuthAmount);

	public String getDeviceName();
	
	public String getDeviceSerialCd();
	
	public String getUsername();
	
	public byte[] getPasswordHash();
	
	public byte[] getPasswordSalt();
	
	public long getCredentialTimestamp();
	
	public String getPreviousUsername();
	
	public byte[] getPreviousPasswordHash();
	
	public byte[] getPreviousPasswordSalt();
	
	public String getNewUsername();
	
	public byte[] getNewPasswordHash();
	
	public byte[] getNewPasswordSalt();
	
	public long getMaxAuthAmount();

	public void setDeviceType(DeviceType deviceType);

	public void setInitOnly(boolean initOnly);

	public IntegerRangeSet getPropertyIndexesToRequest() ;

	public void setPropertyIndexesToRequest(String propertyIndexesToRequest) ;

	public boolean isMasterIdIncrementOnly();

	public void setMasterIdIncrementOnly(boolean masterIdIncrementOnly);

	public Integer getPropertyListVersion();

	public void setPropertyListVersion(Integer propertyListVersion);

	public void updateInternal(Map<String,Object> values, Map<String,Long> timestamp, Connection targetConn) throws ServiceException ;

	public void putAll(Map<String,Object> values, Map<String,Long> timestamps) ;

	public void putAllWithTimestamps(Map<String,Object> values) ;
	
	public int putPendingWithTimestamp(Map<String, Object> values, long timestamp);

	public void putWithTimestamp(DeviceInfoProperty dip, Map<String, Object> values);

	public void refresh() throws ServiceException ;
	
	public boolean refreshSafely() ;
	
	public String getDeviceInfoHash();

}