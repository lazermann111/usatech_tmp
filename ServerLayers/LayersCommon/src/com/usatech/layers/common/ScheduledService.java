package com.usatech.layers.common;

import java.util.concurrent.Callable;

import simple.app.SelfProcessor;
import simple.app.ServiceException;

/**
 * The scheduled service for performing tasks periodically.
 * 
 * @author Evan, bkrug
 * 
 */
public class ScheduledService extends AbstractScheduledService {
	protected SelfProcessor task;
	protected long configuredPeriod;
	

	/**
	 *  Construct a ScheduledService with a service name.
	 * @param serviceName
	 */
	public ScheduledService(String serviceName) {
		super(serviceName);
	}
	
	@Override
	protected void process() throws ServiceException {
		task.process();
	}

	@Override
	protected long getDelay() {
		return configuredPeriod;
	}

	public SelfProcessor getTask() {
		return task;
	}

	public void setTask(SelfProcessor task) {
		this.task = task;
	}

	public void setTask(final Callable<?> task) {
		this.task = new SelfProcessor() {
			@Override
			public void process() throws ServiceException {
				try {
					task.call();
				} catch(ServiceException e) {
					throw e;
				} catch(Exception e) {
					throw new ServiceException(e);
				}
			}
		};
	}

	public long getConfiguredPeriod() {
		return configuredPeriod;
	}

	public void setConfiguredPeriod(long configuredPeriod) {
		this.configuredPeriod = configuredPeriod;
	}
}
