package com.usatech.layers.common;

import java.io.IOException;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import com.trilead.ssh2.SFTPException;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.sftp.ErrorCodes;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * The task pulls renames files on a server via SecureFTP
 * 
 * @author bkrug
 * 
 */
public class RenameSftpTask extends AbstractSftpTask {
	private static final Log log = Log.getLog();

	@Override
	protected int processSftp(MessageChainTaskInfo taskInfo, SFTPv3Client sftp) throws IOException {
		String[] filePaths;
		Pattern fileRenameMatch;
		String fileRenameReplace;
		MessageChainStep step = taskInfo.getStep();
		try {
			filePaths = ConvertUtils.convertToStringArrayNoParse(step.getAttribute(CommonAttrEnum.ATTR_FILE_PATH, Object.class, true));
			String tmp = step.getAttribute(CommonAttrEnum.ATTR_FILE_RENAME_MATCH, String.class, false);
			if(StringUtils.isBlank(tmp))
				fileRenameMatch = null;
			else
				fileRenameMatch = Pattern.compile(tmp);
			fileRenameReplace = step.getAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, String.class, true);
		} catch(AttributeConversionException | ConvertException e) {
			log.error("Could not convert attributes", e);
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, StringUtils.exceptionToString(e));
			return 2;
		}
		int success = 0;
		for(String filePath : filePaths) {
			try {
				sftp.stat(filePath);
			} catch(IOException e) {
				log.warn("File '" + filePath + "' is not accessible: " + e.getMessage());
				continue;
			}
			String newPath;
			if(fileRenameMatch == null)
				newPath = fileRenameReplace;
			else
				newPath = fileRenameMatch.matcher(filePath).replaceAll(fileRenameReplace);
			try {
				sftp.rm(newPath);
			} catch(SFTPException e) {
				switch(e.getServerErrorCode()) {
					case ErrorCodes.SSH_FX_NO_SUCH_FILE:
					case ErrorCodes.SSH_FX_NO_SUCH_PATH:
						// Ignore
						break;
					default:
						log.warn("Could not delete file '" + newPath + "' that is being replaced", e);
				}
			}
			sftp.mv(filePath, newPath);
			log.info("Renamed '" + filePath + "' to '" + newPath + "'");
			success++;
		}
		return success == filePaths.length ? 0 : success == 0 ? 3 : 1;
	}
}
