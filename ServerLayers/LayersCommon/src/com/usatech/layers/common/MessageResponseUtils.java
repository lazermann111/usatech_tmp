package com.usatech.layers.common;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.IOUtils;
import simple.io.Log;

import com.usatech.layers.common.ProcessingUtils.PropertyValueHandler;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.DeviceTypeSpecific;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_2F;
import com.usatech.layers.common.messagedata.MessageDirection;

public class MessageResponseUtils {
	private static final Log log = Log.getLog();

    protected static Charset getDeviceCharset(Message message) {
    	try {
			return message.getDeviceInfo().getDeviceCharset();
		} catch(ServiceException e) {
			log.warn("Could not get Device Info", e);
			return ProcessingConstants.US_ASCII_CHARSET;
		}
    }
    
    

    /*public static String endPropertyValueList(StringBuilder appendTo) {
    	return appendTo.substring(0, appendTo.length() - 1);
    }*/

    public static void markPropertyIndexesReceived(InputStream propertyValueList, Message message) throws IOException, ServiceException {
    	// check list of properties and update session attributes for proper CA processing
		boolean allPropertiesReceived = ConvertUtils.getBooleanSafely(message.getSessionAttribute("all-properties-received"), false);
		if(!allPropertiesReceived) {
			final Set<Integer> propertyIndexesReceived = new HashSet<Integer>();
			try {
				ProcessingUtils.parsePropertyList(propertyValueList, message.getDeviceInfo().getDeviceCharset(), new PropertyValueHandler<RuntimeException>() {
					public void handleProperty(int propertyIndex, String propertyValue) {
						propertyIndexesReceived.add(propertyIndex);
					}
				});
			} catch(ParseException e) {
				log.warn("Could not parse property value list", e);
			}
			if(!propertyIndexesReceived.isEmpty()) {
				int[] oldPropertyIndexesReceived = ConvertUtils.convertSafely(int[].class, message.getSessionAttribute("property-indexes-received"), null);
				if(oldPropertyIndexesReceived != null && oldPropertyIndexesReceived.length > 0) {
					for(int index : oldPropertyIndexesReceived)
						propertyIndexesReceived.add(index);
				}
				message.setSessionAttribute("property-indexes-received", ConvertUtils.getStringSafely(propertyIndexesReceived));
			}
		}
    }
	protected static final ReentrantLock digestLock = new ReentrantLock();
	protected static MessageDigest digest;

	public static String calculateSHA1(String s) throws NoSuchAlgorithmException {
    	if(s == null) return null;
    	byte[] bytes = s.getBytes();
    	digestLock.lock();
    	try {
    		if(digest == null)
        		digest = MessageDigest.getInstance("SHA1");
    		bytes = digest.digest(bytes);
		} finally {
			digestLock.unlock();
		}
		return Base64.encodeBytes(bytes, true);
    }

	public static byte[] maskEncryptionKey(byte[] encryptionKey) {
		return encryptionKey == null ? null : new byte[encryptionKey.length];
	}

	/**
	 * @param tracks
	 * @return
	 */
	public static String maskTrackData(String trackData, boolean maskAfterPlusSign) {
		if(trackData == null || trackData.trim().length() == 0)
			return "";
		// replace all alphabetics with '@' and all digits except the first six and last four before next punctuation with *
		char[] masked = new char[trackData.length()];
		int p = 0;
		boolean hitCardEnd = false;
	    for(int i = 0; i < trackData.length(); i++) {
	    	char ch = trackData.charAt(i);
	    	if(Character.isDigit(ch)) {
	    		if(++p > 6)
	    			masked[i] = '*';
	    		else
	    			masked[i] = ch;
	    	} else if(Character.isLetter(ch)) {
	    		if(++p > 2)
	    			masked[i] = '@';
	    		else
	    			masked[i] = ch;
			} else if(ch == '+' && maskAfterPlusSign) { // mask all digits and letters hereafter
				if(p > 0 && !hitCardEnd) {
					for(int k = 0; k < Math.min(p - 2, 4); k++) {
						masked[i - k - 1] = trackData.charAt(i - k - 1);
					}
				}
				p = trackData.length();
				hitCardEnd = true;
				masked[i] = ch;
	    	} else {
	    		if(p > 0 && !hitCardEnd) {
	    			hitCardEnd = true;
	    			for(int k = 0; k < Math.min(p - 2, 4); k++) {
	    				masked[i-k-1] = trackData.charAt(i-k-1);
	    			}
	    		}
	    		masked[i] = ch;
	    	}
	    }
	    if(p > 0 && !hitCardEnd) {
			for(int k = 0; k < Math.min(p - 2, 4); k++) {
				masked[masked.length-k-1] = trackData.charAt(masked.length-k-1);
			}
		}
		
		return new String(masked);
	}
	
	public static String maskTrackData(String trackData) {
		return maskTrackData(trackData, true);
	}

	public static String maskFully(String data) {
		if(data == null || data.trim().length() == 0)
			return "";
		//each char with *
		char[] masked = new char[data.length()];
		Arrays.fill(masked, '*');
		return new String(masked);
	}
	
	public static byte[] maskFully(byte[] data) {
		if(data == null || data.length == 0)
			return IOUtils.EMPTY_BYTES;
		byte[] masked = new byte[data.length];
		Arrays.fill(masked, (byte)'*');
		return masked;
	}

	protected static final Pattern cardNumberPattern = Pattern.compile("([0-9]{13,})");
	protected static final Pattern CONSECUTIVE_DIGITS_PATTERN = Pattern.compile("([0-9]+)");
    public static String getCardNumber(String card) {
		if(card == null || (card=card.trim()).length() == 0)
			return "";
		// find the largest run of numbers
		String largest = "";
		Matcher matcher = CONSECUTIVE_DIGITS_PATTERN.matcher(card);
		while(matcher.find()) {
			String candidate = matcher.group(1);
			if(candidate.length() >= 13)
				return candidate;
			if(candidate.length() > largest.length())
				largest = candidate;
		}
		return largest.length() >= 4 ? largest : card;
	}	
    
	protected static final Pattern VALID_CARD_NUMBER_PATTERN = Pattern.compile("\\d{1,19}");

	public static boolean isValidCardNumber(String card) {
		return VALID_CARD_NUMBER_PATTERN.matcher(MessageResponseUtils.getCardNumber(card)).matches();
	}
    protected static final Charset reversibleCharset = Charset.forName("ISO_8859_1");
    public static byte[] maskAnyCardNumbers(byte[] data, boolean maskAfterPlusSign) {
    	CharBuffer charBuffer = reversibleCharset.decode(ByteBuffer.wrap(data));
    	maskAnyCardNumbers(charBuffer, maskAfterPlusSign);
    	ByteBuffer resultBuffer = reversibleCharset.encode(charBuffer);
    	if(resultBuffer.hasArray() && resultBuffer.arrayOffset() == 0 && resultBuffer.position() == 0 && resultBuffer.limit() == resultBuffer.capacity())
    		return resultBuffer.array();
		byte[] result = new byte[resultBuffer.remaining()];
		resultBuffer.get(result);
		return result;
    }
    
    public static byte[] maskAnyCardNumbers(byte[] data) {
    	return maskAnyCardNumbers(data, true);
    }
    
    public static String maskAnyCardNumbers(String data) {
    	CharBuffer buffer = CharBuffer.wrap(data.toCharArray());
    	maskAnyCardNumbers(buffer);
    	return buffer.toString();
    }
    public static void maskAnyCardNumbers(CharBuffer data, boolean maskAfterPlusSign) {
    	if(data == null || data.remaining() == 0)
    		return;
    	if (maskAfterPlusSign) {
				int pos = 0;
				for(; pos < data.remaining(); pos++)
					if(data.get(pos + data.position()) == '+')
						break;
				if(pos < data.remaining()) {
					maskAnyCardNumbers(data.subSequence(0, pos));
					for(int i = pos + 1; i < data.remaining(); i++) {
						char ch = data.get(i + data.position());
						if(Character.isDigit(ch)) 
							data.put(i + data.position(), '*');
						else if(Character.isLetter(ch))
				    		data.put(i + data.position(), '@');
					}
					return;
				}
    	}
		if(data.remaining() < 13)
			return;
		Matcher maskPanMatcher = cardNumberPattern.matcher(data);
    	while(maskPanMatcher.find()) {
    		for(int i = maskPanMatcher.start() + 6; i < maskPanMatcher.end() - 4; i++)
				data.put(i, '*');
			if(data.limit() > maskPanMatcher.end() && (data.get(maskPanMatcher.end()) == '=' || data.get(maskPanMatcher.end()) == '|')) {
				for(int i = maskPanMatcher.end() + 1; i < data.limit(); i++) {
					char ch = data.get(i);
					if(Character.isDigit(ch))
						data.put(i, '*');
					else if(Character.isLetter(ch))
						data.put(i, '@');
					else if(ch != '?' && ch != '#' && ch != '|')
						break;
				}
			}
    	}
	}
    
    public static void maskAnyCardNumbers(CharBuffer data) {
    	maskAnyCardNumbers(data, true);
    }
    
    public static String maskCardNumber(String card, boolean maskAfterPlusSign) {
		if(card == null || (card=card.trim()).length() == 0)
			return "";
		if (maskAfterPlusSign) {
			int pos = card.indexOf('+');
			if(pos == 0) {
				char[] masked = new char[card.length() - 1];
				for(int i = 0; i < masked.length; i++) {
					char ch = card.charAt(i + 1);
					if(Character.isDigit(ch))
						masked[i] = '*';
					else if(Character.isLetter(ch))
						masked[i] = '@';
					else
						masked[i] = ch;
				}
				return new String(masked);
			} else if(pos > 0) {
				return maskCardNumber(card.substring(0, pos));
			}
		}
		if(card.length() < 13)
			return card;
		Matcher maskPanMatcher = cardNumberPattern.matcher(card);
    	if(maskPanMatcher.find()) {
    		String unmasked = maskPanMatcher.group(1);
    		char[] masked = unmasked.toCharArray();
    		Arrays.fill(masked, 6, unmasked.length() - 4, '*');
    		return new String(masked);    		
		}
		return card;
	}
    
    public static String maskCardNumber(String card) {
    	return maskCardNumber(card, true);
    }
    
    public static String maskCard(String card) {
    	return card == null ? "0 bytes, pan: null" : card.length() + " bytes, pan: " + maskCardNumber(card);
    }

	protected static final Pattern trackCleanerPattern = Pattern.compile("(^[%;])|((\\?[\\x01-\\xFF]?)?(\\x00[\\x00-\\xFF]*)?$)");
	public static String cleanTrack(String track) {
		Pattern p = trackCleanerPattern;
		if(p != null)
			track = p.matcher(track).replaceAll("");
		return track;
	}
	
	public static String cleanNewlines(String message) {
		if(message == null)
			return "";
		return message.replace("\r", "\\r").replace("\n", "\\n");
	}

	public static MessageData getMessageFromBytes(Message message, MessageType messageType, byte[] commandBytes) throws ServiceException{
		MessageData reply=MessageDataFactory.createMessageData(messageType);
		reply.setDirection(MessageDirection.SERVER_TO_CLIENT);
		if(commandBytes!=null){
			ByteBuffer bb = ByteBuffer.allocate(2048);
			if(messageType.isMessageIdExpected()){
				ProcessingUtils.writeLongInt(bb, message.getData().getMessageId());
			}
			bb.put(commandBytes);
			bb.flip();
			try{
				if(reply instanceof DeviceTypeSpecific){
					((DeviceTypeSpecific)reply).setDeviceType(message.getDeviceInfo().getDeviceType());
				}
				reply.readData(bb);
			}catch(ParseException e){
				throw new ServiceException("Could not parse commandBytes:"+commandBytes, e);
			}
		}
		return reply;
	}
	
	public static MessageData createLegacy2FGenericAck(Message message){
		MessageData_2F reply = new MessageData_2F();
		reply.setAckedMessageNumber(message.getData().getMessageNumber());
		return reply;
	}
}
