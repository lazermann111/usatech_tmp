package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum ReceiptResult {
    PRINTED('P'),
    ERROR('E'),
    NOT_REQUESTED('N'),
    DISABLED('D'),
    UNAVAILABLE('U'),
    ;

    private final char value;
    private static final EnumCharValueLookup<ReceiptResult> lookup = new EnumCharValueLookup<ReceiptResult>(ReceiptResult.class);

    private ReceiptResult(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
    public static ReceiptResult getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
    public static ReceiptResult getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
}
