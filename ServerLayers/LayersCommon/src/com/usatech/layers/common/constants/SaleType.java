package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum SaleType {
    ACTUAL('A'),
    CASH('C'),
    INTENDED('I'),
    ;

    private final char value;
    private static final EnumCharValueLookup<SaleType> lookup = new EnumCharValueLookup<SaleType>(SaleType.class);
    private SaleType(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
    public static SaleType getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
    public static SaleType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
}
