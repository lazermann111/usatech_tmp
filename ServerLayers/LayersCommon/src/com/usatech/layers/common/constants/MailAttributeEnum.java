package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;

public enum MailAttributeEnum implements Attribute {
	TO("emailTo", "To Email"), 
	FROM("emailFrom", "From Email"), 
	SUBJECT("subject", "Message Subject"), 
	BODY("body", "Message Body"), 
    BODY_CONTENT_TYPE("bodyContentType", "Message Body content type"),
	CC("emailCc", "Comma separated RFC822 addresses for carbon copy"),
	BCC("emailBcc", "Comma separated RFC822 addresses for blind carbon copy"),
	FAILED("failedEmails", "Comma separated RFC822 addresses that failed previous attempts");

	private final String value;
	private final String description;

	protected final static EnumStringValueLookup<MailAttributeEnum> lookup = new EnumStringValueLookup<MailAttributeEnum>(
			MailAttributeEnum.class);

	public static MailAttributeEnum getByValue(String value)
			throws InvalidValueException {
		return lookup.getByValue(value);
	}

	/**
	 * Internal constructor.
	 * 
	 * @param value
	 */
	private MailAttributeEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 * 
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}