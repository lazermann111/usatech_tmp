package com.usatech.layers.common.constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.util.VendXEnhancedEncryptedMSRData;
import com.usatech.layers.common.util.VendXParsing;

import simple.io.ByteArrayUtils;
import simple.io.TLVParser;
import simple.lang.EnumValueLookup;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;

public enum TLVTag {	
	NOT_KNOWN_02("02"),
	APP_PAN("5A"),
    PAN_SEQUENCE_NUMBER( "5F34"),
    APPLICATION_LABEL( "50"),
    ISSUER_AUTHENTICATION_DATA("91"),
    CVM_RESULTS( "9F34"),
    TRACK1_DISCRETIONARY_DATA("9F1F"),
    DATA_AUTHENTICATION_CODE( "9F45"),
    ICC_DYNAMIC_NUMBER( "9F4C"),
    TRACK1_EQUIV_DATA_MCHIP_CARD( "56"),
    TRANSACTION_STATUS_INFORMATION( "9B"),
    CARDHOLDER_NAME( "5F20"),
    APPLICATION_USAGE_CONTROL( "9F07"),
    ISSUER_ACTION_CODE_DEFAULT( "9F0D"),
    ISSUER_ACTION_CODE_DENIAL( "9F0E"),
    ISSUER_ACTION_CODE_ONLINE( "9F0F"),
    ISSUER_SCRIPT_RESULTS("9F5B"),
    AUTH_CODE( "E300"),
    ISSUER_AUTH_CODE( "9F74"),
    APPLICATION_IDENTIFIER( "9F06"),
    APPLICATION_VERSION_NUMBER("9F08"),
    AVAILABLE_OFFLINE_SPENDING_AMOUNT_BALANCE( "9F5D"),
    APPLICATION_EXPIRATION_DATE("5F24"),
    APPLICATION_EFFECTIVE_DATE( "5F25"),
    PAYPASS_TRACK2_DATA("9F6B"),
    CARD_TRANSACTION_QUALIFIERS( "9F6C"),
    PAYPASS_MAGSTRIPE_APPLICATION_VERSION_NUMBER("9F6D"),
    // PAYPASS_THIRD_PARTY_DATA( "9F6E"),		// 9F6E is used by MasterCard as Third Party Data
    										// and Visa (qVSDC) as Form Factor Indicator.
    										// Check the AID to determine interpretation - 10/20/2015 - jms
	FORM_FACTOR_INDICATOR("9F6E"),
    MASTERCARD_DATA_RECORD("FF8105", true),
    MASTERCARD_DISCRETIONARY_DATA("FF8106", true),
    MASTERCARD_OUTCOME_PARAMETER_SET("DF8129"),
    APPLICATION_PREFERRED_NAME( "9F12"),
    ISSUER_CODE_TABLE_INDEX( "9F11"),
    MERCHANT_CATEGORY_CODE("9F15"),
    MERCHANT_IDENTIFIER("9F16"),
    TERMINAL_IDENTIFICATION("9F1C"),
    LANGUAGE_PREFERENCE( "5F2D"),
    IDTECH_MSR_TLV("DFEE23"),
    VIVOTECH_GROUP_TAG("FFEE01", true),
    IDTECH_TLV_ERROR_CODE("FFEE1F"),
    IDTECH_VEND_KSN("FFEE12"),
    IDTECH_VEND_TRACK1_MAGSTRIPE("FFEE13"),
    IDTECH_VEND_TRACK2_MAGSTRIPE("FFEE14"),
    IDTECH_MSR_EQUIVALENT_DATA("DFEF4D"),
    IDTECH_MSR_EQUIVALENT_DATA_LENGTHS("DFEF4C"),
    CLEARING_RECORD("E1", true),
    IDTECH_VEND3_ISSUER_SCRIPT_RESULT("DF21"),
    TRACK_DATA_SOURCE( "DF30"),
    DD_CARD_TRACK1( "DF31"),
    DD_CARD_TRACK2( "DF32"),
    DD_CARD_TRACK1_PAYPASS("DF812A"),
    DD_CARD_TRACK2_PAYPASS("DF812B"),
    MASTERCARD_ERROR_INDICATION("DF8115"),
    TERMINAL_ENTRY_CAPABILITY( "DF5B"),
    TERMINAL_VERIFICATION_RESULTS_GEN_AC("DF76"),
    ENCRYPT_INFORMATION("DFEE26"),
    APPLICATION_INTERCHANGE_PROFILE( "82"),
    TERMINAL_VERIFICATION_RESULTS( "95"),
    TRANSACTION_DATE( "9A"),
    TRANSACTION_TYPE( "9C"),
    ISSUER_COUNTRY_CODE("5F28"),
    TRANSACTION_CURRENCY_CODE( "5F2A"),
    ACCOUNT_TYPE("5F57"),
    TRANSACTION_AMOUNT_AUTHORIZED( "9F02"),
    TRANSACTION_AMOUNT_OTHER( "9F03"),
    TERMINAL_APPLICATION_VERSION_NUMBER("9F09"),
    ISSUER_APPLICATION_DATA( "9F10"),
    TERMINAL_COUNTRY_CODE( "9F1A"),
    POS_SERIAL_NUMBER("9F1E"),
    APPLICATION_CRYPTOGRAM( "9F26"),
    CRYPTOGRAM_INFORMATION_DATA( "9F27"),
    APPLICATION_TRANSACTION_COUNTER( "9F36"),
    UNPREDICTABLE_NUMBER( "9F37"),
    TRANSACTION_SEQUENCE_COUNTER("9F41"),
    UNKNOWN_1("9F42"),
    VISA_TTQ_VISA_ONLY( "9F66"),
    CUSTOMER_EXCLUSIVE_DATA1( "9F7C"),
    CUSTOMER_EXCLUSIVE_DATA2( "9F7E"),
    TRANSACTION_CVM( "DF52"),
    TRACK2_EQUIVALENT_DATA("57"),
    DEDICATED_FILE_NAME("84"),
    AUTHORIZATION_RESPONSE_CODE("8A"),
    CARDHOLDER_VERIFICATION_METHOD("8E"),
    ACQUIRER_IDENTIFIER("9F01"),
    TLV_TRANSACTION_TIME("9F21"),
    TERMINAL_CAPABILITIES("9F33"),
    POS_ENTRY_MODE("9F39"),
    TERMINAL_TYPE("9F35"),
    IDTECH_MOBILE_WALLET_INDICATOR("DFEF7B"),
    
    TRANSACTION_CATEGORY_CODE("9F53"),
    
    ENABLE_TRANSACTION_LOGGING("DF11"),	// Idtech's when sending a command
    
    //Apple VAS tags
    AVAS_CONTAINER("FFEE06", true, "FFFFFFFFFF"),
    AVAS_MERCHANT_ID(ProcessingConstants.AVAS_TAG_PREFIX + "9F25"),
    AVAS_VAS_DATA(ProcessingConstants.AVAS_TAG_PREFIX + "9F27"),
    
    OTI_USER_INTERFACE_REQUEST_DATA("DF8144"),
    OTI_ERROR_INDICATION("DF814B"),
    OTI_TRACK2("DF22"),		// OR Idtech's Force Transaction Online when sending a command
    OTI_TRACK1("DF23"),
    OTI_DISCRETIONARY_TRACK1_DATA("DF43"),
    OTI_DISCRETIONARY_TRACK2_DATA("DF44"),
    OTI_PCD_TRANSACTION_RESULT("DF69"),
    OTI_DATA_CHANNEL("DF70"),
    OTI_RESULT_DATA_TEMPLATE("FC"),

    OTI_DATA_CRYPTOGRAM("DF8167"),
    OTI_KEY_VARIANT("DF8169"),
    OTI_DESCRIPTOR("DF816F"),
    OTI_KEY_SERIAL_NUMBER("DF816A"),
    OTI_AID("4F"),
    OTI_PARTIAL_PAN("DF8168"),
    OTI_SUCCESS_TEMPLATE("FF01"),
    OTI_CONTAINER2("E9"),
    OTI_DECRYPTED_CONTAINER("EA"),
    
    RESERVED( "FF78")
    ;
	
	public static final List<TLVTag> CHASE_AUTHORIZATION_EMV_TAGS = new ArrayList<TLVTag>();
	public static final List<TLVTag> CHASE_REVERSAL_EMV_TAGS = new ArrayList<TLVTag>();
	public static final List<TLVTag> CHASE_SETTLEMENT_EMV_TAGS = new ArrayList<TLVTag>();
	public static final List<TLVTag> TNS_EMV_TAGS = new ArrayList<TLVTag>();
	public static final Set<String> INTERAC_AIDS = new HashSet<String>();
	static {
		CHASE_AUTHORIZATION_EMV_TAGS.add(APPLICATION_INTERCHANGE_PROFILE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TERMINAL_VERIFICATION_RESULTS);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_DATE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_TYPE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_CURRENCY_CODE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_AMOUNT_AUTHORIZED);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_AMOUNT_OTHER);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TERMINAL_APPLICATION_VERSION_NUMBER);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TERMINAL_COUNTRY_CODE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(POS_SERIAL_NUMBER);
		CHASE_AUTHORIZATION_EMV_TAGS.add(APPLICATION_CRYPTOGRAM);
		CHASE_AUTHORIZATION_EMV_TAGS.add(CRYPTOGRAM_INFORMATION_DATA);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TERMINAL_CAPABILITIES);
		CHASE_AUTHORIZATION_EMV_TAGS.add(CVM_RESULTS);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TERMINAL_TYPE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(APPLICATION_TRANSACTION_COUNTER);
		CHASE_AUTHORIZATION_EMV_TAGS.add(UNPREDICTABLE_NUMBER);
		CHASE_AUTHORIZATION_EMV_TAGS.add(POS_ENTRY_MODE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_SEQUENCE_COUNTER);
		CHASE_AUTHORIZATION_EMV_TAGS.add(TRANSACTION_CATEGORY_CODE);
		CHASE_AUTHORIZATION_EMV_TAGS.add(DEDICATED_FILE_NAME);
		CHASE_AUTHORIZATION_EMV_TAGS.add(ISSUER_APPLICATION_DATA);
		CHASE_AUTHORIZATION_EMV_TAGS.add(FORM_FACTOR_INDICATOR);

		CHASE_REVERSAL_EMV_TAGS.add(APPLICATION_INTERCHANGE_PROFILE);
		CHASE_REVERSAL_EMV_TAGS.add(TERMINAL_VERIFICATION_RESULTS);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_DATE);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_TYPE);
		CHASE_REVERSAL_EMV_TAGS.add(APPLICATION_EXPIRATION_DATE);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_CURRENCY_CODE);
		CHASE_REVERSAL_EMV_TAGS.add(PAN_SEQUENCE_NUMBER);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_AMOUNT_AUTHORIZED);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_AMOUNT_OTHER);
		CHASE_REVERSAL_EMV_TAGS.add(TERMINAL_APPLICATION_VERSION_NUMBER);
		CHASE_REVERSAL_EMV_TAGS.add(TERMINAL_COUNTRY_CODE);
		CHASE_REVERSAL_EMV_TAGS.add(POS_SERIAL_NUMBER);
		CHASE_REVERSAL_EMV_TAGS.add(APPLICATION_CRYPTOGRAM);
		CHASE_REVERSAL_EMV_TAGS.add(CRYPTOGRAM_INFORMATION_DATA);
		CHASE_REVERSAL_EMV_TAGS.add(TERMINAL_CAPABILITIES);
		CHASE_REVERSAL_EMV_TAGS.add(CVM_RESULTS);
		CHASE_REVERSAL_EMV_TAGS.add(TERMINAL_TYPE);
		CHASE_REVERSAL_EMV_TAGS.add(APPLICATION_TRANSACTION_COUNTER);
		CHASE_REVERSAL_EMV_TAGS.add(UNPREDICTABLE_NUMBER);
		CHASE_REVERSAL_EMV_TAGS.add(POS_ENTRY_MODE);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_SEQUENCE_COUNTER);
		CHASE_REVERSAL_EMV_TAGS.add(TRANSACTION_CATEGORY_CODE);
		CHASE_REVERSAL_EMV_TAGS.add(DEDICATED_FILE_NAME);
		CHASE_REVERSAL_EMV_TAGS.add(ISSUER_APPLICATION_DATA);
		CHASE_REVERSAL_EMV_TAGS.add(FORM_FACTOR_INDICATOR);

		CHASE_SETTLEMENT_EMV_TAGS.add(APPLICATION_INTERCHANGE_PROFILE);
		CHASE_SETTLEMENT_EMV_TAGS.add(AUTHORIZATION_RESPONSE_CODE);
		CHASE_SETTLEMENT_EMV_TAGS.add(ISSUER_AUTHENTICATION_DATA);
		CHASE_SETTLEMENT_EMV_TAGS.add(TERMINAL_VERIFICATION_RESULTS);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_DATE);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_TYPE);
		CHASE_SETTLEMENT_EMV_TAGS.add(APPLICATION_EXPIRATION_DATE);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_CURRENCY_CODE);
		CHASE_SETTLEMENT_EMV_TAGS.add(PAN_SEQUENCE_NUMBER);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_AMOUNT_AUTHORIZED);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_AMOUNT_OTHER);
		CHASE_SETTLEMENT_EMV_TAGS.add(TERMINAL_APPLICATION_VERSION_NUMBER);
		CHASE_SETTLEMENT_EMV_TAGS.add(TERMINAL_COUNTRY_CODE);
		CHASE_SETTLEMENT_EMV_TAGS.add(POS_SERIAL_NUMBER);
		CHASE_SETTLEMENT_EMV_TAGS.add(APPLICATION_CRYPTOGRAM);
		CHASE_SETTLEMENT_EMV_TAGS.add(CRYPTOGRAM_INFORMATION_DATA);
		CHASE_SETTLEMENT_EMV_TAGS.add(TERMINAL_CAPABILITIES);
		CHASE_SETTLEMENT_EMV_TAGS.add(CVM_RESULTS);
		CHASE_SETTLEMENT_EMV_TAGS.add(TERMINAL_TYPE);
		CHASE_SETTLEMENT_EMV_TAGS.add(APPLICATION_TRANSACTION_COUNTER);
		CHASE_SETTLEMENT_EMV_TAGS.add(UNPREDICTABLE_NUMBER);
		CHASE_SETTLEMENT_EMV_TAGS.add(POS_ENTRY_MODE);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_SEQUENCE_COUNTER);
		CHASE_SETTLEMENT_EMV_TAGS.add(TRANSACTION_CATEGORY_CODE);
		CHASE_SETTLEMENT_EMV_TAGS.add(DEDICATED_FILE_NAME);
		CHASE_SETTLEMENT_EMV_TAGS.add(ISSUER_APPLICATION_DATA);
		CHASE_SETTLEMENT_EMV_TAGS.add(ISSUER_SCRIPT_RESULTS);
		CHASE_SETTLEMENT_EMV_TAGS.add(FORM_FACTOR_INDICATOR);

		TNS_EMV_TAGS.add(TRANSACTION_AMOUNT_AUTHORIZED);
		TNS_EMV_TAGS.add(TRANSACTION_AMOUNT_OTHER);
		TNS_EMV_TAGS.add(APPLICATION_CRYPTOGRAM);
		TNS_EMV_TAGS.add(APPLICATION_INTERCHANGE_PROFILE);
		TNS_EMV_TAGS.add(APPLICATION_TRANSACTION_COUNTER);
		TNS_EMV_TAGS.add(CRYPTOGRAM_INFORMATION_DATA);
		TNS_EMV_TAGS.add(CVM_RESULTS);
		TNS_EMV_TAGS.add(ISSUER_APPLICATION_DATA);
		TNS_EMV_TAGS.add(PAN_SEQUENCE_NUMBER);
		TNS_EMV_TAGS.add(TERMINAL_CAPABILITIES);
		TNS_EMV_TAGS.add(TERMINAL_COUNTRY_CODE);
		TNS_EMV_TAGS.add(TERMINAL_TYPE);
		TNS_EMV_TAGS.add(TERMINAL_VERIFICATION_RESULTS);
		TNS_EMV_TAGS.add(TRANSACTION_CURRENCY_CODE);
		TNS_EMV_TAGS.add(TRANSACTION_DATE);
		TNS_EMV_TAGS.add(TRANSACTION_TYPE);
		TNS_EMV_TAGS.add(UNPREDICTABLE_NUMBER);

		INTERAC_AIDS.add("A00000000310100001");
		INTERAC_AIDS.add("A000000003101001");
		INTERAC_AIDS.add("A000000003101002");
		INTERAC_AIDS.add("A00000000410100002");
		INTERAC_AIDS.add("A000000025010203");
		INTERAC_AIDS.add("A0000002");
		INTERAC_AIDS.add("A0000002771010");
		INTERAC_AIDS.add("A0000002771010000001");
		INTERAC_AIDS.add("A0000002771010000002");
		INTERAC_AIDS.add("A000000277101001");
		INTERAC_AIDS.add("A000000277101002");
		INTERAC_AIDS.add("A0000012771010");
		INTERAC_AIDS.add("A111111111");
		INTERAC_AIDS.add("B0000002771010");
	}

	protected static final TLVParser TLV_PARSER = new TLVParser();
	
	private static final EnumValueLookup<TLVTag, byte[]> lookup = new EnumValueLookup<>(TLVTag.class);
	private final byte[] value;
	private final String hexValue;
	private final boolean container;
	private final String keyPrefix;
	
	private TLVTag(String hexValue) {		
		this.value = ByteArrayUtils.fromHex(hexValue);
		this.hexValue = hexValue;
		this.container = false;
		this.keyPrefix = "";
	}
	
	private TLVTag(String hexValue, boolean container) {		
		this.value = ByteArrayUtils.fromHex(hexValue);
		this.hexValue = hexValue;
		this.container = container;
		this.keyPrefix = "";
	}
	
	private TLVTag(String hexValue, boolean container, String keyPrefix) {		
		this.value = ByteArrayUtils.fromHex(hexValue);
		this.hexValue = hexValue;
		this.container = container;
		this.keyPrefix = keyPrefix;
	}

	private TLVTag(byte[] value) {
		this.value = value;
		this.hexValue = StringUtils.toHex(value);
		this.container = false;
		this.keyPrefix = "";
	}

	public byte[] getValue() {
		return value;
	}
	
	public String getHexValue() {
		return hexValue;
	}	

	public boolean isContainer() {
		return container;
	}

	public String getKeyPrefix() {
		return keyPrefix;
	}

	public static TLVTag getByValue(byte[] value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

	public static int parseTlvLength(byte [] data, int lengthStart) {
		int lengthOffset = lengthStart;
		int tlvLen = 0;
		if ((data[lengthOffset] & 0xFF) < 0x80) {
			tlvLen = data[lengthOffset++] & 0xFF;
		} else {
			int lengthBytes = (data[lengthOffset++] & 0x0F);
			tlvLen = 0;
			for (int ii = 0; ii < lengthBytes; ii++) {
				tlvLen = tlvLen * 256 + (data[lengthOffset++] & 0xFF);
			}
		}
		return tlvLen;
	}

	/*
	 * This is per the OTI spec but may be more general 
	 * @param length
	 * @return
	 */
	public static int lengthOfLength(int length) {
		int result = 1;
		if (length > 127) result++; 
		if (length > 255) result++;
		if (length > 32767) result++;	// this one is not in the spec
		return result;
	}
	
	public static void dumpTlvParseMap(final Map<byte [], byte []>parseMap) {
		dumpTlvParseMap(parseMap, "");
	}
	
	
	// 		boolean encryption = ((source[offset] & 0xC0) == 0xC0);
	public static void dumpTlvParseMap(final Map<byte [], byte []>parseMap, String prefix) {
    	for (Map.Entry<byte [], byte []>entry: parseMap.entrySet()) {
    		byte [] key = entry.getKey();
    		if (key != null && key.length > 0 && key[0] != 0) {
    	    	try {
		    		TLVTag tag = TLVTag.getByValue(key);
		    		System.out.println(prefix + tag);
		    		System.out.println(prefix + "Key " + StringUtils.toHex(entry.getKey()) + " Value: " + StringUtils.toHex(entry.getValue()) + "\n");
		    		if (tag.equals(VIVOTECH_GROUP_TAG) || tag.equals(MASTERCARD_DATA_RECORD) || tag.equals(MASTERCARD_DISCRETIONARY_DATA) 
		    				|| tag.equals(OTI_RESULT_DATA_TEMPLATE) || tag.equals(OTI_CONTAINER2)) {
		    			try {
							Map<byte [], byte []>groupTagMap = TLV_PARSER.parse(entry.getValue(), 0, entry.getValue().length);
							dumpTlvParseMap(groupTagMap, prefix + "\t");
						} catch (IOException e) {
							System.err.println(tag.toString() + " parse failed." + e);
							e.printStackTrace();
						}
		    		}
		    		if (tag.equals(IDTECH_MSR_TLV)) {
		    			VendXEnhancedEncryptedMSRData msrData = VendXParsing.parseIdtechEnhancedEncryptedMSR(entry.getValue());
		    			StringBuffer sb = new StringBuffer("\t ksn: " + StringUtils.toHex(msrData.getKsn()));
		    			sb.append(", track2: ");
		    			sb.append(StringUtils.toHex(msrData.getTrack2()));
		    			sb.append(", track2 length: ");
		    			sb.append(msrData.getTrack2Length());
		    			sb.append("\n");
		    			System.out.println(sb.toString());
		    		}
    	    	} catch (InvalidValueException e) {
    	    		System.out.println("Value: " + StringUtils.toHex(key) + " not found in TLVTag.  Continuing to dump tags.");
//    	    		String tagString = StringUtils.toHex(key);
//		    		System.out.println(prefix + tagString);
		    		System.out.println(prefix + "Key " + StringUtils.toHex(entry.getKey()) + " Value: " + StringUtils.toHex(entry.getValue()) + "\n");
    	    	} catch (IOException e) {
    	    		System.err.println("Parse failed for value: " + StringUtils.toHex(key) + ".  Continuing to dump tags.");
    	    		e.printStackTrace();
				}
    		} else {
    			System.out.println("Skipping key that was either null or zero bytes in length or array of [0]");
    		}
    	}
		
	}
	

}
