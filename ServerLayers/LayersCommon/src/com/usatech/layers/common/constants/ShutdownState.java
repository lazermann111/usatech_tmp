package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;



public enum ShutdownState {
	CAN_REACTIVATE((byte)1),
    SHUTDOWN_COMPLETE((byte)0);

    private final byte value;
    private ShutdownState(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<ShutdownState> lookup = new EnumByteValueLookup<ShutdownState>(ShutdownState.class);
    public static ShutdownState getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
