package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum AFActionCode {
	NO_ACTION((byte)0),
	PERFORM_SESSION_CALL_IN((byte)1),
	REBOOT((byte)2),
	REINITIALIZE((byte)3);
	private final byte value;
	private AFActionCode(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<AFActionCode> lookup = new EnumByteValueLookup<AFActionCode>(AFActionCode.class);
    public static AFActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
