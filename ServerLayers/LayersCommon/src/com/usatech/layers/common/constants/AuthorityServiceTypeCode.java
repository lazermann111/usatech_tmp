package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

/**
 * This class defined all the available account type Codes . Reference : /POSM/USAT/POS/Const.pm #
 * PKG_AUTHORITY_SERVICE_TYPE_GLOBALS
 *
 * @author sjillidimudi
 *
 */

public enum AuthorityServiceTypeCode {
    BALANCE_BASED_ACCOUNT(1, ""),
    PRIVILEGES_ACCOUNT(2, ""),
    PSEUDO_PRIVILEGES_ACCOUNT(3, ""),
    HIDDEN_BALANCE_ACCOUNT(4, "");

    private final int value;
    private final String description;

    private static final EnumIntValueLookup<AuthorityServiceTypeCode> lookup = new EnumIntValueLookup<AuthorityServiceTypeCode>(AuthorityServiceTypeCode.class);

	/**
	 * Internal Constructor
	 *
	 * @param value
	 * @param description
	 */
	private AuthorityServiceTypeCode(int value, String description) {
		this.description = description;
		this.value = value;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return int
	 */
	public int getValue() {
		return value;
	}

	/**
	 * defined description for the key.
	 *
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * This Methods returns the TransactionStateCode representation
	 *
	 * @param value
	 * @return TransactionStateCode representation of the input Value
	 * @throws InvalidDataException
	 */

	public static AuthorityServiceTypeCode getByValue(int value) throws InvalidIntValueException {
		return lookup.getByValue(value);
	}

}
