package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * ABh permission type action codes.
 * @author yhe
 *
 */
public enum ABPermissionActionCode {
	SHOW_ALL_SERVICE_FIELDS_NOT_CLEAR_COUNTERS((byte)0),
	SHOW_ALL_SERVICE_FIELDS_CLEAR_COUNTERS((byte)1),
	SHOW_COUNTERS_ONLY_NOT_CLEAR_COUNTERS((byte)2),
	SHOW_COUNTERS_ONLY_CLEAR_COUNTERS((byte)3),
	SHOW_CASHLESS_COUNTERS_NOT_CLEAR_COUNTERS((byte)4),
	SHOW_CASHLESS_COUNTERS_REDISPLAY((byte)5);
	private final byte value;
	private ABPermissionActionCode(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<ABPermissionActionCode> lookup = new EnumByteValueLookup<ABPermissionActionCode>(ABPermissionActionCode.class);
    public static ABPermissionActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
    public static ABPermissionActionCode getByValueSafely(byte value) {
    	return lookup.getByValueSafely(value);
    }
}
