package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * 71h transaction result code
 * @author yhe
 *
 */
public enum BatchTransactionResult71 {
	FAIL((byte)0),
	PASS((byte)1),
	ERROR((byte)2);
	private final byte value;
	private BatchTransactionResult71(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<BatchTransactionResult71> lookup = new EnumByteValueLookup<BatchTransactionResult71>(BatchTransactionResult71.class);
    public static BatchTransactionResult71 getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
