package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum ExternalTransferProtocol {
	FTP((byte)'F'),
    FTP_PASSIVE((byte)'P'),
    HTTP((byte)'H'),
    HTTPS((byte)'S');

    private final byte value;
    private ExternalTransferProtocol(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<ExternalTransferProtocol> lookup = new EnumByteValueLookup<ExternalTransferProtocol>(ExternalTransferProtocol.class);
    public static ExternalTransferProtocol getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
