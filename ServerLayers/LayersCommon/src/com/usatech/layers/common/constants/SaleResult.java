package com.usatech.layers.common.constants;

import simple.lang.InvalidByteValueException;

public enum SaleResult {
    SUCCESS((byte)0),
    CANCELLED_BY_USER((byte)1),
    CANCELLED_BY_AUTH_TIMEOUT((byte)2),
    CANCELLED_BY_AUTH_FAILURE((byte)3),
    CANCELLED_MACHINE_FAILURE((byte)4),
    CANCELLED_DEVICE_FAILURE((byte)5),
    CREDIT_POST_FAILURE((byte)6),
    CREDIT_POST_TIMEOUT((byte)7),
    VEND_SESSION_START_FAILURE((byte)8),
    VMC_REQUEST_RESET((byte)9),
    VEND_REQUEST_TIMEOUT((byte)10),
    VEND_TERMINATION_TIMEOUT((byte)11);
    private final byte value;
    private SaleResult(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    public static SaleResult getByValue(byte value) throws InvalidByteValueException {
        try {
            return values()[value];
        } catch(ArrayIndexOutOfBoundsException e) {
            throw new InvalidByteValueException(value);
        }
    }
	public TranDeviceResultType getTransactionResult() {
		switch(this) {
			case SUCCESS: return TranDeviceResultType.SUCCESS_NO_PRINTER;
			case CANCELLED_BY_USER: return TranDeviceResultType.CANCELLED;
			case CANCELLED_BY_AUTH_TIMEOUT: return TranDeviceResultType.AUTH_FAILURE;
			case CANCELLED_BY_AUTH_FAILURE: return TranDeviceResultType.AUTH_FAILURE;
			case CANCELLED_MACHINE_FAILURE: return TranDeviceResultType.FAILURE;
			case CANCELLED_DEVICE_FAILURE: return TranDeviceResultType.FAILURE;
			case CREDIT_POST_FAILURE: return TranDeviceResultType.CREDIT_POST_FAILURE;
			case CREDIT_POST_TIMEOUT: return TranDeviceResultType.CREDIT_POST_TIMEOUT;
			case VEND_REQUEST_TIMEOUT: return TranDeviceResultType.VEND_REQUEST_TIMEOUT;
			case VEND_SESSION_START_FAILURE: return TranDeviceResultType.VEND_SESSION_START_FAILURE;
			case VEND_TERMINATION_TIMEOUT: return TranDeviceResultType.VEND_TERMINATION_TIMEOUT;
			case VMC_REQUEST_RESET: return TranDeviceResultType.VMC_REQUEST_RESET;
			default: throw new IllegalStateException("Enum " + this + " doesn't match any");
		}
	}
}
