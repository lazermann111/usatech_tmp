/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum GenericResponseResultCode {
	OKAY((byte)0), NETLAYER_ERROR((byte)1), APPLAYER_ERROR((byte)2), NOT_SUPPORTED((byte)3), SUSPENDED((byte)4), DENIED((byte)5);

	private final byte value;
	private GenericResponseResultCode(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}
    protected final static EnumByteValueLookup<GenericResponseResultCode> lookup = new EnumByteValueLookup<GenericResponseResultCode>(GenericResponseResultCode.class);
    public static GenericResponseResultCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
