package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;




public enum ServerActionCode {
	NO_ACTION((byte)0, GenericResponseServerActionCode.NO_ACTION, AuthResponseActionCode.NO_ACTION, AFActionCode.NO_ACTION),
	PERFORM_CALL_IN((byte)1, GenericResponseServerActionCode.SEND_UPDATE_STATUS_REQUEST_IMMEDIATE, AuthResponseActionCode.PERFORM_CALL_IN, AFActionCode.PERFORM_SESSION_CALL_IN),
	ACTIVATE((byte)2, GenericResponseServerActionCode.NO_ACTION, AuthResponseActionCode.PERFORM_CALL_IN, AFActionCode.NO_ACTION),
	;

	private final GenericResponseServerActionCode genericResponseServerActionCode;
	private final AuthResponseActionCode authResponseActionCode;
	private final AFActionCode afActionCode;
	private final byte value;
	private ServerActionCode(byte value, GenericResponseServerActionCode genericResponseServerActionCode, AuthResponseActionCode authResponseActionCode, AFActionCode afActionCode) {
		this.genericResponseServerActionCode = genericResponseServerActionCode;
		this.authResponseActionCode = authResponseActionCode;
		this.afActionCode = afActionCode;
		this.value = value;
	}
	public byte getValue() {
		return value;
	}
    public GenericResponseServerActionCode getGenericResponseServerActionCode() {
		return genericResponseServerActionCode;
	}
	public AuthResponseActionCode getAuthResponseActionCode() {
		return authResponseActionCode;
	}
	public AFActionCode getAFActionCode() {
		return afActionCode;
	}
	protected final static EnumByteValueLookup<ServerActionCode> lookup = new EnumByteValueLookup<ServerActionCode>(ServerActionCode.class);
    public static ServerActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
