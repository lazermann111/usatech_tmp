package com.usatech.layers.common.constants;

import com.usatech.app.Attribute;
import com.usatech.layers.common.MessageResponseUtils;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum PaymentMaskBRef {
	PRIMARY_ACCOUNT_NUMBER((byte) 1, "pan", 1) {
		@Override
		public String maskData(String value) {
			return MessageResponseUtils.maskCardNumber(value);
		}
	},// Primary Account Number
	CARD_HOLDER((byte) 2, "cardHolder", 5) {
		@Override
		public String maskData(String value) {
			return null;
		}
	},// Name (Card Holder)
	EXPIRATION_DATE((byte) 3, "expirationDate", 2) {
		@Override
		public String maskData(String value) {
			return null;
		}
	}, // Expiration Date (YYMM or YYYYMM)
	ADDITIONAL_DATA((byte) 4, "additionalData", 0), // Additional Data (service code, etc.)
	DISCRETIONARY_DATA((byte) 5, "discretionaryData", 0), // Discretionary Data (PVKI, PVV, Offset, CVV, CVC, etc.). NOTE: We've changed the meaning of this to only represent the manually entered CVV2 value or security code and not something that is part of Track 2 data.
	SECURITY_DATA((byte) 6, "securityData", 0), // Use and Security Data (Track 3)
	CUSTOM_DATA_1((byte) 7, "customData1", 0), // Custom Data 1
	CUSTOM_DATA_2((byte) 8, "customData2", 0), // Custom Data 2
	CUSTOM_DATA_3((byte) 9, "customData3", 0), // Custom Data 3
	ISSUE_NUM((byte) 10, "issueNum", 0), // Issue Code (Issue Num, Lost Card Code, etc.)
	COUNTRY_CODE((byte) 11, "countryCode", 0), // Country Code (numeric)
	LRC((byte) 12, "lrc", 0), // Check Data: Longitude Redundancy
	MOD_10((byte) 13, "mod10", 0), // Check Data: mod 10 (Luhn)
	MOD_97((byte) 14, "mod97", 0), // Check Data: mod 97
	GROUP_CODE((byte) 15, "groupCode", 0), // Group Code
	VERIFICATION((byte) 16, "verificationData", 0), // Verification Value
	PROCESS_CODE((byte) 17, "processCode", 0), // Service Card Process Code
	ZIP_CODE((byte) 18, "avsZipCode", 3), // Zip Code
	ADDRESS((byte) 19, "avsAddress", 4), // Address
	PARTIAL_PRIMARY_ACCOUNT_NUMBER((byte) 20, "partialPan", 0) {
		@Override
		public String maskData(String value) {
			return MessageResponseUtils.maskCardNumber(value);
		}
	},// Partial Primary Account Number
	COUPON_CODE((byte) 21, "coupon", 6) {
		@Override
		public String maskData(String value) {
			return MessageResponseUtils.maskFully(value);
		}
	},// Coupon Code
	;
	private final byte value;
	private final String attributeName;
	private final int storeIndex;
	private static int maxStoreIndex = -1;
	public final Attribute attribute = new Attribute() {
		@Override
		public String getValue() {
			return attributeName;
		}
	};
	public final Attribute decryptionAttribute = new Attribute() {
		@Override
		public String getValue() {
			return CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + '.' + getStoreIndex();
		}
	};
	private PaymentMaskBRef(byte value, String attributeName, int storeIndex) {
		this.value = value;
		this.attributeName = attributeName;
		this.storeIndex = storeIndex;
	}
	public byte getValue() {
		return value;
	}
	public String maskData(String value) {
		return MessageResponseUtils.maskFully(value);
	}
    protected final static EnumByteValueLookup<PaymentMaskBRef> lookup = new EnumByteValueLookup<PaymentMaskBRef>(PaymentMaskBRef.class);
    public static PaymentMaskBRef getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }

	public int getStoreIndex() {
		return storeIndex;
	}

	public static int getMaxStoreIndex() {
		if(maxStoreIndex == -1) {
			maxStoreIndex = 0;
			for(PaymentMaskBRef br : values())
				if(br.storeIndex > 0 && br.storeIndex > maxStoreIndex)
					maxStoreIndex = br.storeIndex;
		}
		return maxStoreIndex;
	}
}
