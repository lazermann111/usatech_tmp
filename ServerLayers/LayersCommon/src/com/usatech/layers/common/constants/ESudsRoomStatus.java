package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum ESudsRoomStatus {
	EQUIP_STATUS_NO_STATUS_AVAILABLE(0),
	EQUIP_STATUS_IDLE_AVAILABLE(1),
	EQUIP_STATUS_IN_CYCLE_FIRST(2),
	EQUIP_STATUS_OUT_OF_SERVICE(3),
	EQUIP_STATUS_NO_WASHER_OR_DRYER_ON_PORT(4),
	EQUIP_STATUS_IDLE_NOT_AVAILABLE(5),
	EQUIP_STATUS_IN_MANUAL_SERVICE_MODE(6),
	EQUIP_STATUS_IN_CYCLE_SECOND(7),
	EQUIP_STATUS_TRANSACTION_IN_PROGRESS(8);
	private final int value;
	private ESudsRoomStatus(int value) {
        this.value = value;
	}
	public int getValue() {
		return value;
	}

    protected final static EnumIntValueLookup<ESudsRoomStatus> lookup = new EnumIntValueLookup<ESudsRoomStatus>(ESudsRoomStatus.class);
    public static ESudsRoomStatus getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
