package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

public enum C3AdditionalDataType {
    NO_ADDITIONAL_DATA((byte)0, "HEX"),
    BEHAVIOR_BITMAP((byte)1, "HEX");

    private final byte value;
    private final String displayFormat;
    private C3AdditionalDataType(byte value, String displayFormat) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
        this.displayFormat = displayFormat;
    }
    public byte getValue() {
        return value;
    }
    public String getDisplayFormat() {
    	return displayFormat;
    }
    public String getReadableAdditionalData(String additionalData) {
    	if (additionalData == null)
    		return null;
    	if ("HEX".equalsIgnoreCase(displayFormat))
    		return StringUtils.toHex(additionalData.getBytes());
    	else
    		return additionalData;
    }
    protected final static EnumByteValueLookup<C3AdditionalDataType> lookup = new EnumByteValueLookup<C3AdditionalDataType>(C3AdditionalDataType.class);
    public static C3AdditionalDataType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
