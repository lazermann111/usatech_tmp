package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

public enum AEAdditionalDataType {
    NO_ADDITIONAL_DATA((byte)0, "HEX"),
    ISIS_SMARTTAP((byte)1, "HEX");

    private final byte value;
    private final String displayFormat;
    private AEAdditionalDataType(byte value, String displayFormat) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
        this.displayFormat = displayFormat;
    }
    public byte getValue() {
        return value;
    }
    public String getDisplayFormat() {
    	return displayFormat;
    }
    public String getReadableAdditionalData(String additionalData) {
    	if (additionalData == null)
    		return null;
    	if ("HEX".equalsIgnoreCase(displayFormat))
    		return StringUtils.toHex(additionalData.getBytes());
    	else
    		return additionalData;
    }
    protected final static EnumByteValueLookup<AEAdditionalDataType> lookup = new EnumByteValueLookup<AEAdditionalDataType>(AEAdditionalDataType.class);
    public static AEAdditionalDataType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
