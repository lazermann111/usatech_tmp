package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum Language {
		ENGLISH(1),
		FRENCH(2),
		FRENCH_AND_ENGLISH(3),
		FRENCH_OR_ENGLISH(4),
    ;

    private final int value;
    
    private Language(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }

		protected final static EnumIntValueLookup<Language> lookup = new EnumIntValueLookup<Language>(Language.class);
    public static Language getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
