package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * ABh permission type response codes. The spec is wrong, 0 is Decline
 * @author yhe
 *
 */
public enum PermissionResponseCodeAB {
	DECLINE((byte)0),
	SUCCESS((byte)1),
	CONDITIONAL_SUCCESS((byte)2),
	DECLINE_PERMANENT((byte)3),
	FAILURE((byte)4);
	private final byte value;
	private PermissionResponseCodeAB(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<PermissionResponseCodeAB> lookup = new EnumByteValueLookup<PermissionResponseCodeAB>(PermissionResponseCodeAB.class);
    public static PermissionResponseCodeAB getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
