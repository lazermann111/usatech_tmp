/**
 *
 */
package com.usatech.layers.common.constants;

import java.nio.charset.Charset;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;
import simple.util.IntegerRangeSet;

import com.usatech.app.Attribute;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;

public enum DeviceInfoProperty implements Attribute {
	DEVICE_NAME(String.class, null, false),
	DEVICE_SERIAL_CD(String.class, null, false),
	STORE_DEVICE_NAME(Boolean.class, false, false),
	ENCRYPTION_KEY(byte[].class, null, false),
	PREVIOUS_ENCRYPTION_KEY(byte[].class, null, false),
	REJECT_UNTIL(Long.class, 0L, true),
	DEVICE_TYPE(DeviceType.class, DeviceType.DEFAULT, false),
	LOCALE(String.class, null, false),
	INIT_ONLY(Boolean.class, true, false),
	DEVICE_CHARSET(Charset.class, ProcessingConstants.US_ASCII_CHARSET, false),
	TIME_ZONE_GUID(String.class, null, false),
	ACTIVATION_STATUS(ActivationStatus.class, ActivationStatus.FACTORY, true),
	ACTION_CODE(ServerActionCode.class, ServerActionCode.NO_ACTION, true),
	MASTER_ID(Long.class, 0L, true),
	PROPERTY_INDEXES_TO_REQUEST(IntegerRangeSet.class, null, true),
	USERNAME(String.class, null, false),
	PASSWORD_HASH(byte[].class, null, false),
	PASSWORD_SALT(byte[].class, null, false),
	CREDENTIAL_TIMESTAMP(Long.class, 0L, false),
	PREVIOUS_USERNAME(String.class, null, false),
	PREVIOUS_PASSWORD_HASH(byte[].class, null, false),
	PREVIOUS_PASSWORD_SALT(byte[].class, null, false),
	NEW_USERNAME(String.class, null, false),
	NEW_PASSWORD_HASH(byte[].class, null, false),
	NEW_PASSWORD_SALT(byte[].class, null, false),
	MAX_AUTH_AMOUNT(Long.class, 0L, false),
	PROPERTY_LIST_VERSION(Integer.class, null, false),
	MASTER_ID_INCREMENT_ONLY(Boolean.class, Boolean.TRUE, false),
	;
	private final String attributeKey;
	private final Class<?> attributeType;
	private final Object defaultValue;
	private final boolean sameValueAccepted;
	private static String convertToName(String name) {
		StringBuilder sb = new StringBuilder(name.toLowerCase());
		int pos = 0;
		while((pos=sb.indexOf("_", pos)) >= 0) {
			if(pos < sb.length() - 1)
				sb.replace(pos, pos+2, sb.substring(pos+1, pos+2).toUpperCase());
		}
        return sb.toString();
	}
	private <T> DeviceInfoProperty(Class<T> attributeType, T defaultValue, boolean sameValueAccepted) {
		this.attributeKey = convertToName(name());
		this.attributeType = attributeType;
		this.defaultValue = defaultValue;
		this.sameValueAccepted = sameValueAccepted;
	}
	private <T> DeviceInfoProperty(String attributeKey, Class<T> attributeType, T defaultValue, boolean sameValueAccepted) {
		this.attributeKey = attributeKey;
		this.attributeType = attributeType;
		this.defaultValue = defaultValue;
		this.sameValueAccepted = sameValueAccepted;
	}
	public String getAttributeKey() {
		return attributeKey;
	}
	public Class<?> getAttributeType() {
		return attributeType;
	}
	public Object getDefaultValue() {
		return defaultValue;
	}
	public boolean isSameValueAccepted() {
		return sameValueAccepted;
	}

	protected final static EnumStringValueLookup<DeviceInfoProperty> lookup = new EnumStringValueLookup<DeviceInfoProperty>(DeviceInfoProperty.class);

	public static DeviceInfoProperty getByValue(String value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

	public static DeviceInfoProperty getByValueSafely(String value) {
		return lookup.getByValueSafely(value);
	}

	@Override
	public String getValue() {
		return getAttributeKey();
	}

	public Object getProperty(DeviceInfo deviceInfo) {
		switch(this) {
			case ACTION_CODE:
				return deviceInfo.getActionCode();
			case ACTIVATION_STATUS:
				return deviceInfo.getActivationStatus();
			case CREDENTIAL_TIMESTAMP:
				return deviceInfo.getCredentialTimestamp();
			case DEVICE_CHARSET:
				return deviceInfo.getDeviceCharset();
			case DEVICE_NAME:
				return deviceInfo.getDeviceName();
			case DEVICE_SERIAL_CD:
				return deviceInfo.getDeviceSerialCd();
			case DEVICE_TYPE:
				return deviceInfo.getDeviceType();
			case ENCRYPTION_KEY:
				return deviceInfo.getEncryptionKey();
			case INIT_ONLY:
				return deviceInfo.isInitOnly();
			case LOCALE:
				return deviceInfo.getLocale();
			case MASTER_ID:
				return deviceInfo.getMasterId();
			case MASTER_ID_INCREMENT_ONLY:
				return deviceInfo.isMasterIdIncrementOnly();
			case MAX_AUTH_AMOUNT:
				return deviceInfo.getMaxAuthAmount();
			case NEW_PASSWORD_HASH:
				return deviceInfo.getNewPasswordHash();
			case NEW_PASSWORD_SALT:
				return deviceInfo.getNewPasswordSalt();
			case NEW_USERNAME:
				return deviceInfo.getNewUsername();
			case PASSWORD_HASH:
				return deviceInfo.getPasswordHash();
			case PASSWORD_SALT:
				return deviceInfo.getPasswordSalt();
			case PREVIOUS_ENCRYPTION_KEY:
				return deviceInfo.getPreviousEncryptionKey();
			case PREVIOUS_PASSWORD_HASH:
				return deviceInfo.getPreviousPasswordHash();
			case PREVIOUS_PASSWORD_SALT:
				return deviceInfo.getPreviousPasswordSalt();
			case PREVIOUS_USERNAME:
				return deviceInfo.getPreviousUsername();
			case PROPERTY_INDEXES_TO_REQUEST:
				return deviceInfo.getPropertyIndexesToRequest();
			case PROPERTY_LIST_VERSION:
				return deviceInfo.getPropertyListVersion();
			case REJECT_UNTIL:
				return deviceInfo.getRejectUntil();
			case STORE_DEVICE_NAME:
				return null;
			case TIME_ZONE_GUID:
				return deviceInfo.getTimeZoneGuid();
			case USERNAME:
				return deviceInfo.getUsername();
			default:
				return null;
		}
	}
}