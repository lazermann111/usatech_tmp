package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidValueException;

public enum PaymentType {
    CASH('M'),
    CREDIT('C'),
    SPECIAL('S')
    ;

    private final char value;
    private static final Map<Character, PaymentType> paymentTypes = new HashMap<Character, PaymentType>();
    static {
        for(PaymentType item : values()) {
        	paymentTypes.put(item.getValue(), item);
        }
    }
    private PaymentType(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }

	public static PaymentType getByValue(char value) throws InvalidValueException {
        if (paymentTypes.containsKey(value))
            return paymentTypes.get(value);
        else
            throw new InvalidValueException(value);
    }

    public static PaymentType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
}
