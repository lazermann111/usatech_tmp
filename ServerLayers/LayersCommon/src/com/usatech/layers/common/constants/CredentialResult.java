package com.usatech.layers.common.constants;

public enum CredentialResult {
	CURRENT_PASSWORD_MATCHED, NEW_PASSWORD_MATCHED, TMP_PASSWORD_MATCHED, NO_MATCH, INTERNAL, INVALID_USERNAME
}