package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum TranDeviceResultType {
    AUTH_FAILURE('U'),
    CANCELLED('C'),
    CREDIT_POST_FAILURE('P'),
    CREDIT_POST_TIMEOUT('E'),
    FAILURE('F'),
    INCOMPLETE('I'),
    INVALID('\u0000'),
    SUCCESS('S'),
    SUCCESS_NO_PRINTER('Q'),
    SUCCESS_NO_RECEIPT('N'),
    SUCCESS_RECEIPT_ERROR('R'),
    TIMEOUT('T'),
    UNKNOWN(' '),
    VEND_REQUEST_TIMEOUT('B'),
    VEND_SESSION_START_FAILURE('V'),
    VEND_TERMINATION_TIMEOUT('D'),
    VMC_REQUEST_RESET('A')
    ;

    private final char value;
    private static final EnumCharValueLookup<TranDeviceResultType> lookup = new EnumCharValueLookup<TranDeviceResultType>(TranDeviceResultType.class);
    private TranDeviceResultType(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
    public static TranDeviceResultType getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
    public static TranDeviceResultType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }

	public SaleResult getSaleResult() {
		switch(this) {
			case AUTH_FAILURE: return SaleResult.CANCELLED_BY_AUTH_FAILURE;
			case CANCELLED: return SaleResult.CANCELLED_BY_USER;
			case CREDIT_POST_FAILURE: return SaleResult.CREDIT_POST_FAILURE;
			case CREDIT_POST_TIMEOUT: return SaleResult.CREDIT_POST_TIMEOUT;
			case FAILURE: return SaleResult.CANCELLED_MACHINE_FAILURE;
			case INCOMPLETE: return SaleResult.CANCELLED_MACHINE_FAILURE;
			case INVALID: return SaleResult.CANCELLED_MACHINE_FAILURE;
			case SUCCESS: return SaleResult.SUCCESS;
			case SUCCESS_NO_PRINTER: return SaleResult.SUCCESS;
			case SUCCESS_NO_RECEIPT: return SaleResult.SUCCESS;
			case SUCCESS_RECEIPT_ERROR: return SaleResult.SUCCESS;
			case TIMEOUT: return SaleResult.CANCELLED_BY_USER;
			case UNKNOWN: return SaleResult.CANCELLED_MACHINE_FAILURE;
			case VEND_REQUEST_TIMEOUT: return SaleResult.VEND_REQUEST_TIMEOUT;
			case VEND_SESSION_START_FAILURE: return SaleResult.VEND_SESSION_START_FAILURE;
			case VEND_TERMINATION_TIMEOUT: return SaleResult.VEND_TERMINATION_TIMEOUT;
			case VMC_REQUEST_RESET: return SaleResult.VMC_REQUEST_RESET;
			default: throw new IllegalStateException("Enum " + this + " doesn't match any");
		}
	}
}
