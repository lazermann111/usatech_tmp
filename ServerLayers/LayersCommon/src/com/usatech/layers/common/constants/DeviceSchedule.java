package com.usatech.layers.common.constants;

public enum DeviceSchedule {
	SESSION, DEX, SETTLEMENT, INACTIVE_SESSION
}
