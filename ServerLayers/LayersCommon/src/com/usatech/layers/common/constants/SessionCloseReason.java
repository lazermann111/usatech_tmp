package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum SessionCloseReason {
	AUTHENTICATION_FAILURE('A', "Failure: Authentication Error", "#CC3300"),
	DECRYPTION_FAILURE('D', "Failure: Decryption Error", "#CC3300"),	
	IN_PROGRESS('I', "In-Progress", "#996600"),
	MESSAGE_BASED('M', "Success: Server Closed Session", "#009933"),
	INACTIVE_DEVICE('N', "Failure: Inactive Device", "#CC3300"),
	CLIENT_REQUESTED('S', "Success: Client Closed Session", "#009933"),
	TIMEOUT('T', "Failure: Timeout", "#CC3300"),		
	ERROR('U', "Failure: Error Occurred", "#CC3300"),
	SHUTDOWN('W', "Failure: NetLayer Shutdown", "#CC3300"),
	UNKNOWN('X', "Unknown", "#000000");
	
    private final char value;
    private final String name;
    private final String color;
    private static final EnumCharValueLookup<SessionCloseReason> lookup = new EnumCharValueLookup<SessionCloseReason>(SessionCloseReason.class);
    private SessionCloseReason(char value, String name, String color) {
        this.value = value;
        this.name = name;
        this.color = color;
    }

    public char getValue() {
        return value;
    }

    public String getName() {
		return name;
	}
    
    public String getColor() {
		return color;
	}

    public static SessionCloseReason getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
}
