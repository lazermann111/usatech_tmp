package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum EventType {
    BATCH(1, 66),
    FILL(2, 70),
    SCHEDULED(3, 83),
    ERROR(4, 0),
    FORCED(5, 0),
    INITIAL(6, 0),
    MACHINE_ALERT(7, 0), // Machine Alert (such as column jam or compressor failure or power outage)
    DEVICE_ALERT(8, 0), // Device Alert (such as re-boot, or other debug notifications)
    COIN_CHANGER_ALERT_BAD_TUBE_SENSOR(9, 0),
    COIN_CHANGER_ALERT_CHANGER_UNPLUGGED(10, 0),
    COIN_CHANGER_ALERT_TUBE_JAM(11, 0),
    COIN_CHANGER_ALERT_CORRUPT_ROM(12, 0),
    COIN_CHANGER_ALERT_COIN_ROUTING_ERROR(13, 0),
    COIN_CHANGER_ALERT_COIN_JAM(14, 0),
    COIN_CHANGER_ALERT_CREDIT_STOLEN(15, 0),
    BILL_ACCEPTOR_ALERT_BAD_MOTOR(16, 0),
    BILL_ACCEPTOR_ALERT_BAD_SENSOR(17, 0),
    BILL_ACCEPTOR_ALERT_CORRUPT_ROM(18, 0),
    BILL_ACCEPTOR_ALERT_JAMMED(19, 0),
    BILL_ACCEPTOR_ALERT_CREDIT_STOLEN(20, 0),
    BILL_ACCEPTOR_ALERT_STACKER_OUT_OF_POSITION(21, 0),
    BILL_ACCEPTOR_ALERT_DISABLED(22, 0),
    COMPONENT_DETAILS(23, 0),
    DEX_EXCEPTION(24,0),
    INTERNAL_MACHINE_ALERT(25, 0), // Machine Alert (Internal Only)
    INTERNAL_DEVICE_ALERT(26, 0), // Device Alert (Internal Only)
    LOCAL_FILL_BUT_STLMNT(27, 0), //not a server event 
    DEVICE_DOOR_SWITCH_CHANGED(28, 0),
    ;

    private final int value;
    private final int hostNumber;
    private EventType(int value, int hostNumber) {
        this.value = value;
        this.hostNumber = hostNumber;
    }
    public int getValue() {
        return value;
    }
    public int getHostNumber() {
        return hostNumber;
    }
    protected final static EnumIntValueLookup<EventType> lookup = new EnumIntValueLookup<EventType>(EventType.class);
    public static EventType getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}