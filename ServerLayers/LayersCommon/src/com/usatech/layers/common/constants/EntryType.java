package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum EntryType {
	BAR_CODE('B', false, null),
    CONTACTLESS('C', false, null),
    CASH('M', false, null),
    DYNAMIC('D', false, null),
    INTERAC_FLASH('F', false, null),
    ISIS('I', false, null),
    MANUAL('N', false, null),
    SWIPE('S', false, null),
    UNSPECIFIED('U', false, null),
    PHONE('P', false, null),
    CARD_ID('A', false, null),
    CARD_ID_CREDENTIALS('T', false, null),
    BLUETOOTH('L', false, null),
    TOKEN('K', false, null),
    EMV_CONTACT('E', false, EntryType.SWIPE),
    EMV_CONTACTLESS('Q', false, EntryType.CONTACTLESS),
    GOOGLE_PAY('G', false, EntryType.CONTACTLESS),
	APPLE_PAY('Y', false, EntryType.CONTACTLESS),
	VAS('V', true, EntryType.SWIPE),
	APPLE_PAY_CASH('H', false, EntryType.CONTACTLESS)
    ;

    private final char value;
    private boolean cardIdRequiredInReply;
    private final EntryType fallbackEntryType;
	protected final static EnumCharValueLookup<EntryType> lookup = new EnumCharValueLookup<EntryType>(EntryType.class);

	private EntryType(char value, boolean cardIdRequiredInReply, EntryType fallbackEntryType) {
        this.value = value;
        this.cardIdRequiredInReply = cardIdRequiredInReply;
        this.fallbackEntryType = fallbackEntryType;
	}
    public char getValue() {
        return value;
    }
    
    public boolean isCardIdRequiredInReply() {
    	return cardIdRequiredInReply;
    }
   
		public EntryType getFallbackEntryType() {
			return fallbackEntryType;
		}
		public static EntryType getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
    }
    public static EntryType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
}
