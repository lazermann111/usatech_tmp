/**
 *
 */
package com.usatech.layers.common.constants;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import simple.io.Log;
import simple.lang.InvalidByteArrayException;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *96, A3
 *
 */
public enum MessageType {
	//General Message Type
	PING(new byte[0], "General Ping", false, false, false, false, 0, true),
	NETLAYER_HEALTH_CHECK(new byte[]{0, 5}, "Network Layer Health Check", false, false, false, false, 0, true),
	APPLAYER_HEALTH_CHECK(new byte[]{0, 6}, "App Layer Health Check", false, false, false, false, 0, true),
	APPLAYER_HEALTH_CHECK_RESPONSE(new byte[]{1, 6}, "App Layer Health Check Response", false, false, false, false, 0, true),
	QUEUE_TIMING_CHECK(new byte[]{0, 7}, "Queue Timing Check", false, false, false, false, 0, true),
	QUEUE_TIMING_CHECK_RESPONSE(new byte[]{1, 7}, "Queue Timing Check Response", false, false, false, false, 0, true),
	APPLAYER_ALERT_MESSAGE(new byte[]{0, 8}, "App Layer Alert Block", false, false, false, false, 0, true),

	//V 4.1 Message Types
	INITIALIZATION_4_1((byte)0xC0, "Initialization 4.1", true, true),
    COMPONENT_ID_4_1((byte)0xC1, "Component Identification 4.1", false, true),
    AUTH_REQUEST_4_1((byte)0xC2, "Authorization Request 4.1", false, true, true, false, 12),
    AUTH_RESPONSE_4_1((byte)0xC3, "Authorization Response 4.1", false, true, false, true, 0),
    SALE_4_1((byte)0xC4, "Sale 4.1", false, true),
    SETTLEMENT_4_1((byte)0xC5, "Settlement 4.1", false, true),
    GENERIC_EVENT_4_1((byte)0xC6, "Generic Event 4.1", false, true),
    SHORT_FILE_XFER_4_1((byte)0xC7, "Short File Transfer 4.1", false, true, false, false, 19),
    FILE_XFER_START_4_1((byte)0xC8, "File Transfer Start 4.1", false, true),
    FILE_XFER_4_1((byte)0xC9, "File Transfer 4.1", false, true, false, false, 10),
    GENERIC_REQUEST_4_1((byte)0xCA, "Generic Request 4.1", false, true),
    GENERIC_RESPONSE_4_1((byte)0xCB, "Generic Response 4.1", false, true, false, false, 8),
    CHARGE_REQUEST_4_1((byte) 0xCC, "Charge Request 4.1", false, true, true, false, 13),

    // V 3.2 Message Types
    AUTH_REQUEST_3_2((byte)0xAE, "Authorization Request 3.2", false, false, true, false, 15),
    AUTH_RESPONSE_3_2((byte)0xAF, "Authorization Response 3.2", false, false, false, true, 0),
    
    // V 3.1 Message Types
    PERMISSION_REQUEST_3_1((byte)0xAA, "Permission Request 3.1", false, false, true, false, 8),
    AUTH_REQUEST_3_1((byte)0xAC, "Authorization Request 3.1", false, false, true, false, 25),
    INITIALIZATION_3_1((byte)0xAD, "Initialization 3.1", true, false),

    // V 3.0 Message Types
    INITIALIZATION_3_0((byte)0x8E, "Initialization 3.0", true, false),
    SET_ID_NUMBER_AND_KEY((byte)0x8F, "Set ID Number and Key", false, false, false, false, 2),
    AUTH_REQUEST_3_0((byte)0xA0, "Authorization Request 3.0", false, false, true, false, 12),
    AUTH_RESPONSE_3_0((byte)0xA1, "Authorization Response 3.0", false, false, false, true, 0),
    FILE_XFER_START_3_0((byte)0xA4, "File Transfer Start 3.0", false, false),
    FILE_XFER_START_ACK_3_0((byte)0xA5, "File Transfer Start ACK 3.0", false, false),
    FILE_XFER_3_0((byte)0xA6, "File Transfer 3.0", false, false, false, false, 5),
    FILE_XFER_ACK_3_0((byte)0xA7, "File Transfer ACK 3.0", false, false),

    // V 2.0 Message Types
    INITIALIZATION_2_0((byte)0x49, "Initialization 2.0", true, false),
    AUTH_REQUEST_2_0((byte)0x5E, "Authorization Request 2.0", false, false, true, false, 10),
    AUTH_RESPONSE_2_0((byte)0x60, "Authorization Response 2.0", false, false, false, true, 0),
    FILE_XFER_START_2_0((byte)0x7C, "File Transfer Start 2.0", false, false),
    FILE_XFER_START_ACK_2_0((byte)0x7D, "File Transfer Start ACK 2.0", false, false),
    FILE_XFER_2_0((byte)0x7E, "File Transfer 2.0", false, false, false, false, 4),
    FILE_XFER_ACK_2_0((byte)0x7F, "File Transfer ACK 2.0", false, false),
    DEVICE_REACTIVATE((byte)0x76, "Device Reactivate", false, false),
    KILL_FILE_TRANSFER((byte)0x80, "Kill File Transfer", false, false),
    KILL_FILE_TRANSFER_ACK((byte)0x81, "Kill File Transfer ACK", false, false),
    CLIENT_TO_SERVER_REQUEST((byte)0x82, "Client to Server Request", false, false),
    SERVER_TO_CLIENT_REQUEST((byte)0x83, "Server to Client Request", false, false),
    UNIX_TIME_2_0((byte)0x84, "Unix Time 2.0", false, false),
    BCD_TIME((byte)0x85, "BCD Time", false, false),
    GX_COUNTERS((byte)0x86, "Gx Counters", false, false),
    GX_PEEK((byte)0x87, "Gx Peek", false, false),
    GX_POKE((byte)0x88, "Gx Poke", false, false),
    NETWORK_AUTH_BATCH_2_0((byte)0x2A, "Network Authorization Batch 2.0", false, false),
    LOCAL_AUTH_BATCH_2_0((byte)0x2B, "Local Authorization Batch 2.0", false, false, false, false, 17),
    LOCAL_AUTH_BATCH_BCD_2_0((byte)0x93, "Local Authorization Batch 2.0 BCD Time Format", false, false, false, false, 20),
    CASH_SALE_DETAIL_2_0((byte)0x94, "Cash Sale Detail 2.0", false, false),
    CASH_SALE_DETAIL_ACK_2_0((byte)0x95, "Cash Sale Detail ACK 2.0", false, false),
    CASH_SALE_DETAIL_3_0((byte)0x96, "Cash Sale Detail 3.0", false, false),
    CLIENT_VERSION((byte)0x99, "Client Version", false, false),
    GENERIC_DATA_LOG((byte)0x2E, "Generic Data Log", false, false),
    GENERIC_ACK((byte)0x2F, "Generic ACK", false, false),
    NETWORK_AUTH_BATCH_2_1((byte)0xA2, "Network Authorization Batch 2.1", false, false),
    LOCAL_AUTH_BATCH_2_2((byte)0xA3, "Local Authorization Batch 2.2", false, false, false, false, 16),
    BATCH_ACK_2_0((byte)0x71, "Batch ACK V2.0", false, false),
    TERMINAL_UPDATE_STATUS_REQUEST((byte)0x92, "Terminal Update Status Request", false, false),
    TERMINAL_UPDATE_STATUS((byte)0x90, "Terminal Update Status", false, false),
    I_AM_ALIVE_ALERT((byte)0x5C, "I Am Alive Alert", false, false),
    DEVICE_CONTROL_ACK((byte)0x75, "Device Control ACK", false, false),
    GENERIC_NAK((byte)0x91, "Generic NAK", false, false),
    COUNTERS_2_0((byte)0xA8, "Counters 2.0", false, false, false, false, 44),
    PERMISSION_RESPONSE_1_0((byte)0xAB, "Permission Response 1.0", false, false, false, true, 0),
    SONY_INFO_LOG_2_0((byte)0xB9, "Sony Info Log 2.0", false, false),
    FILE_TRANSFER_REQUEST((byte)0x9B, "File Transfer Request", false, false),
    SERVER_FILE_TRANSFER_REQUEST((byte)0xA9, "External Server to Client File Transfer Request", false, false),
    FORCE_DEVICE_SHUTDOWN((byte)0x73, "Force the device to shutdown", false, false),
    //Esuds
    ROOM_STATUS(new byte[]{(byte)0x9A, (byte)0x47}, "Room Status", false, false),
    ROOM_STATUS_WITH_STARTING_PORT_NUMBER(new byte[]{(byte)0x9A, (byte)0x41}, "Room Status With Starting Port Number", false, false),
    WASHER_DRYER_LABELS(new byte[]{(byte)0x9A, (byte)0x6A}, "Washer/Dryer Labels", false, false),
    GET_WASHER_DRYER_DIAGNOSTICS(new byte[]{(byte)0x9A, (byte)0x61}, "Get Washer/Dryer Diagnostics", false, false),
    WASHER_DRYER_DIAGNOSTICS(new byte[]{(byte)0x9A, (byte)0x62}, "Washer/Dryer Diagnostics", false, false),
    ROOM_MODEL_INFO_LAYOUT_2_0(new byte[]{(byte)0x9A, (byte)0x63}, "Room Model Info/Layout 2.0", false, false),
    ROOM_MODEL_INFO_LAYOUT_ACK(new byte[]{(byte)0x9A, (byte)0x46}, "Room Model Info/Layout ACK", false, false),
    ACTUAL_PURCHASE_NETWORK_AUTH_BATCH(new byte[]{(byte)0x9A, (byte)0x5E}, "Actual Purchase Network Auth Batch", false, false),
    ESUDS_LOCAL_AUTH_BATCH(new byte[]{(byte)0x9A, (byte)0x5F}, "eSuds Local Auth Batch", false, false, false, false, 12, false),
    INTENDED_PURCHASES_NETWORK_AUTH_BATCH(new byte[]{(byte)0x9A, (byte)0x60}, "Intended Purchases Network Auth Batch", false, false),
    
	// Web Service
    WS_AUTH_V_3_REQUEST(new byte[]{2, 0}, "Web Service - Auth V 3 Request", false, false, true, false, 0, false),
    WS_AUTH_V_3_1_REQUEST(new byte[]{2, 1}, "Web Service - Auth V 3 1 Request", false, false, true, false, 0, false),
	WS_CHARGE_V_3_REQUEST(new byte[]{2, 2}, "Web Service - Charge V 3 Request", false, false, true, false, 0, false),
	WS_CHARGE_V_3_1_REQUEST(new byte[]{2, 3}, "Web Service - Charge V 3 1 Request", false, false, true, false, 0, false),
	WS_BATCH_V_3_REQUEST(new byte[]{2, 4}, "Web Service - Batch V 3 Request", false, false),
	WS_CASH_V_3_REQUEST(new byte[]{2, 5}, "Web Service - Cash V 3 Request", false, false),
	WS_PROCESS_UPDATES_REQUEST(new byte[]{2, 6}, "Web Service - Process Updates Request", false, false),
	WS_UPLOAD_FILE_REQUEST(new byte[]{2, 7}, "Web Service - Upload File Request", false, false),

	WS_RESPONSE(new byte[]{3, 0}, "Web Service - Response", false, false),
	WS_AUTH_RESPONSE(new byte[]{3, 1}, "Web Service - Auth Response", false, false, false, true, 0, false),
	WS_PROCESS_UPDATES_RESPONSE(new byte[]{3, 2}, "Web Service - Process Updates Response", false, false),
	
	// Web Service 2
	WS2_AUTH_PLAIN_REQUEST(new byte[]{2, 10}, "Web Service - Auth Plain Request", false, false, true, false, 0, false),
	WS2_AUTH_ENCRYPTED_REQUEST(new byte[]{2, 11}, "Web Service - Auth Encrypted Request", false, false, true, false, 0, false),
	
	WS2_CHARGE_PLAIN_REQUEST(new byte[]{2, 14}, "Web Service - Charge Plain Request", false, false, true, false, 0, false),
	WS2_CHARGE_ENCRYPTED_REQUEST(new byte[]{2, 15}, "Web Service - Charge Encrypted Request", false, false, true, false, 0, false),
	
	WS2_CASH_REQUEST(new byte[]{2, 18}, "Web Service - Cash Request", false, false),
	WS2_BATCH_REQUEST(new byte[]{2, 19}, "Web Service - Batch Request", false, false),

	WS2_GET_CARD_ID_PLAIN_REQUEST(new byte[]{2, 28}, "Web Service - Get Card Id Plain Request", false, false),
	WS2_GET_CARD_ID_ENCRYPTED_REQUEST(new byte[]{2, 29}, "Web Service - Get Card Id Encrypted Request", false, false),
	WS2_GET_CARD_INFO_PLAIN_REQUEST(new byte[]{2, 20}, "Web Service - Get Card Info Plain Request", false, false),
	WS2_GET_CARD_INFO_ENCRYPTED_REQUEST(new byte[]{2, 21}, "Web Service - Get Card Info Encrypted Request", false, false),
	WS2_REPLENISH_PLAIN_REQUEST(new byte[]{2, 22}, "Web Service - Replenish Plain Request", false, false, true, false, 0, false),
	WS2_REPLENISH_ENCRYPTED_REQUEST(new byte[]{2, 23}, "Web Service - Replenish Encrypted Request", false, false, true, false, 0, false),
	WS2_REPLENISH_CASH_REQUEST(new byte[]{2, 24}, "Web Service - Replenish Cash Request", false, false, true, false, 0, false),
	
	WS2_TOKENIZE_PLAIN_REQUEST(new byte[]{2, 30}, "Web Service - Tokenize Plain Request", false, false, true, false, 0, false),
    WS2_TOKENIZE_ENCRYPTED_REQUEST(new byte[]{2, 31}, "Web Service - Tokenize Encrypted Request", false, false, true, false, 0, false),
    WS2_UNTOKENIZE_REQUEST(new byte[]{2, 32}, "Web Service - Untokenize Request", false, false),
    WS2_RETOKENIZE_REQUEST(new byte[]{2, 33}, "Web Service - Retokenize Request", false, false),
    
	WS2_PROCESS_UPDATES_REQUEST(new byte[]{2, 25}, "Web Service - Process Updates Request", false, false),
	WS2_UPLOAD_DEVICE_INFO_REQUEST(new byte[]{2, 26}, "Web Service - Upload Device Info Request", false, false),
	WS2_UPLOAD_FILE_REQUEST(new byte[]{2, 27}, "Web Service - Upload File Request", false, false),
	
	WS2_RESPONSE(new byte[]{3, 10}, "Web Service - Response", false, false),
	WS2_AUTH_RESPONSE(new byte[]{3, 11}, "Web Service - Auth Response", false, false, false, true, 0, false),
	WS2_REPLENISH_RESPONSE(new byte[]{3, 12}, "Web Service - Replenish Response", false, false, false, true, 0, false),
	WS2_CARD_ID(new byte[]{3, 15}, "Web Service - Card Id", false, false),
	WS2_CARD_INFO(new byte[]{3, 13}, "Web Service - Card Info", false, false),
	WS2_PROCESS_UPDATES_RESPONSE(new byte[]{3, 14}, "Web Service - Process Updates Response", false, false),
	WS2_TOKEN_RESPONSE(new byte[] { 3, 16 }, "Web Service - Token Response", false, false, false, true, 0, false),

	EBEACON_AUTHORIZATION_REQUEST((byte) 0xF1, "eBeacon Authorization Request 1.0", false, false, true, false, 12),

    UNKNOWN("Unknown", false, false, "??"),
    ;

	private final byte majorValue;
	private final byte[] value;
	private final String hex;
	private final String title;
	private final String description;
	private final boolean initMessage;
	private final boolean authRequest;
	private final boolean authResponse;
	private final boolean messageIdExpected;
	private final int clearTextBytes;
	private final boolean healthCheck;
	private static final Map<String,MessageType> messageTypes = new HashMap<String, MessageType>();
	private static final MessageType[] messageTypeArray = new MessageType[256];
	private static final Map<Byte,Byte> messageTypeLengths = new HashMap<Byte, Byte>();
	private static final Log log = Log.getLog();
	static {
		for(MessageType mt : values()) {
			MessageType dup;
			if(mt.getValue() != null && mt.getValue().length == 1) {
				dup = messageTypeArray[mt.getMajorValue() & 0xFF];
				messageTypeArray[mt.getMajorValue() & 0xFF] = mt;
			} else {
				dup = messageTypes.put(mt.getHex(), mt);
			}
			if(dup != null)
				log.error("Duplicate value for MessageTypes " + dup + " and " + mt);
			else if(mt.getValue() != null && mt.getValue().length > 0) {
				byte len = (byte)mt.getValue().length;
				Byte oldLen = messageTypeLengths.get(mt.getMajorValue());
				if(oldLen == null)
					messageTypeLengths.put(mt.getMajorValue(), len);
				else if(len != oldLen)
					log.error("Inconsistent message type length for " + mt);
			}
		}
	}
	//This is only for UNKNOWN 
	private MessageType(String title, boolean initMessage, boolean messageIdExpected, String hex) {
		this.value = null;
		this.majorValue = (byte) 0;
		this.hex = hex;
		this.title = title;
		this.description = title + (hex.length() > 0 ? " - " + hex + "h" : "");
		this.initMessage = initMessage;
		this.messageIdExpected = messageIdExpected;
		this.authRequest = false;
		this.authResponse = false;
		this.clearTextBytes = 0;
		this.healthCheck = false;
	}
	//This is for most others
	private MessageType(byte[] value, String title, boolean initMessage, boolean messageIdExpected) {
		this(value, title, initMessage, messageIdExpected, false, false, 0, false);
	}
	private MessageType(byte[] value, String title, boolean initMessage, boolean messageIdExpected, boolean authRequest, boolean authResponse, int clearTextBytes, boolean healthCheck) {
		this.value = value;
		this.majorValue = (value != null && value.length > 0 ? value[0] : (byte) 0);
		this.hex = StringUtils.toHex(value);
		this.title = title;
		this.description = title + (hex.length() > 0 ? " - " + hex + "h" : "");
		this.initMessage = initMessage;
		this.messageIdExpected = messageIdExpected;
		this.authRequest = authRequest;
		this.authResponse = authResponse;
		this.clearTextBytes = clearTextBytes;
		this.healthCheck = healthCheck;
	}
	private MessageType(byte majorValue, String description, boolean initMessage, boolean messageIdExpected) {
		this(new byte[] {majorValue}, description, initMessage, messageIdExpected);
	}
	private MessageType(byte majorValue, String description, boolean initMessage, boolean messageIdExpected, boolean authMessage, boolean authResponse, int clearTextBytes) {
		this(new byte[] {majorValue}, description, initMessage, messageIdExpected, authMessage, authResponse, clearTextBytes, false);
	}

	public byte[] getValue() {
		return value;
	}
	public byte getMajorValue() {
		return majorValue;
	}
	public String getHex() {
		return hex;
	}
	public String getDescription() {
		return description;
	}
	public static MessageType getByValue(byte value) throws InvalidByteValueException {
		try {
			MessageType mt = messageTypeArray[value & 0xFF];
			if(mt == null)
				throw new InvalidByteValueException(value);
			return mt;
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new InvalidByteValueException(value);
		}
	}

	/*
	public static MessageType getByValueSafely(byte value) {
		try {
			return MessageType.getByValue(value);
		} catch(InvalidByteValueException e) {
			log.warn("Unrecognized message type " + StringUtils.toHex(value) + "h", e);
			return null;
		}
	}
	public static MessageType getByValue(byte[] value) throws InvalidByteValueException, InvalidByteArrayException {
		if(value != null && value.length == 1)
			return getByValue(value[0]);
		else {
			MessageType mt = messageTypes.get(StringUtils.toHex(value));
			if(mt == null)
				throw new InvalidByteArrayException(value);
			else
				return mt;
		}
	}
	public static MessageType getByValue(byte[] value, int offset, int length) throws InvalidByteValueException, InvalidByteArrayException {
		if(value != null && length == 1)
			return getByValue(value[offset]);
		else {
			MessageType mt = messageTypes.get(StringUtils.toHex(value, offset, length));
			if(mt == null)
				throw new InvalidByteArrayException(value);
			else
				return mt;
		}
	}*/
	public static MessageType getByValue(byte[] data, int offset) throws InvalidByteValueException, InvalidByteArrayException {
		if(data == null || data.length <= offset)
			return MessageType.PING;
		byte majorValue = data[offset];
		int len = MessageType.getMessageTypeLength(majorValue);
		if(len == 1)
			return MessageType.getByValue(majorValue);
		else {
			MessageType mt = messageTypes.get(StringUtils.toHex(data, offset, len));
			if(mt == null) {
				byte[] bytes = new byte[len];
				System.arraycopy(data, 0, bytes, 0, len);
				throw new InvalidByteArrayException(bytes);
			} else
				return mt;
		}
	}
	/** Figures out the Message Type based on the bytes in the ByteBuffer. Starts reading from the current
	 *  position in the ByteBuffer and reads to the end of the message type.
	 * @param data The ByteBuffer to read
	 * @return
	 * @throws InvalidByteValueException
	 * @throws InvalidByteArrayException
	 */
	public static MessageType getByValue(ByteBuffer data) throws InvalidByteValueException, InvalidByteArrayException, BufferUnderflowException {
		if(!data.hasRemaining()) {
			return MessageType.PING;
		} else {
			byte majorValue = data.get();
			int len = MessageType.getMessageTypeLength(majorValue);
			if(len == 1)
				return MessageType.getByValue(majorValue);
			else {
				byte[] bytes = new byte[len];
				bytes[0] = majorValue;
				data.get(bytes, 1, len-1);
				MessageType mt = messageTypes.get(StringUtils.toHex(bytes));
				if(mt == null) {
					throw new InvalidByteArrayException(bytes);
				} else
					return mt;
			}
		}
	}/*
	public static MessageType getByValueSafely(byte[] value) {
		try {
			return MessageType.getByValue(value);
		} catch(InvalidByteValueException e) {
			log.warn("Unrecognized message type " + StringUtils.toHex(value) + "h", e);
			return null;
		} catch(InvalidByteArrayException e) {
			log.warn("Unrecognized message type " + StringUtils.toHex(value) + "h", e);
			return null;
		}
	}*/
	public static String getMessageDescriptionSafely(byte value) {
		try {
			return MessageType.getByValue(value).getDescription();
		} catch(InvalidByteValueException e) {
			log.warn("Unrecognized message type " + StringUtils.toHex(value) + "h", e);
			return "UNKNOWN (" + StringUtils.toHex(value) + "h";
		}
	}

	public static String getMessageDescriptionSafely(byte[] value) {
		try {
			return MessageType.getByValue(value,0).getDescription();
		} catch(InvalidByteArrayException e) {
			log.warn("Unrecognized message type " + StringUtils.toHex(value) + "h", e);
			return "UNKNOWN (" + StringUtils.toHex(value) + "h";
		}catch(InvalidByteValueException e) {
			log.warn("Unrecognized message type " + StringUtils.toHex(value) + "h", e);
			return "UNKNOWN (" + StringUtils.toHex(value) + "h";
		}
	}

	protected static byte getMessageTypeLength(byte dataType) {
		Byte length = messageTypeLengths.get(dataType);
		return length == null ? 1 : length;
	}

	public boolean isInitMessage() {
		return initMessage;
	}
	public boolean isMessageIdExpected() {
		return messageIdExpected;
	}
	public String getTitle() {
		return title;
	}
	public int getClearTextBytes() {
		return clearTextBytes;
	}
	public boolean isAuthRequest() {
		return authRequest;
	}
	public boolean isAuthResponse() {
		return authResponse;
	}
	public boolean isHealthCheck() {
		return healthCheck;
	}
}
