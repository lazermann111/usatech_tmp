package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

public enum DataSyncType {
	ARAMARK_AUTHORITY("ARAMARK_AUTHORITY"),
	ARAMARK_PAYMENT_TYPE("ARAMARK_PAYMENT_TYPE"),
	AUTHORITY("AUTHORITY"),
	AUTHORITY_PAYMENT_MASK("AUTHORITY_PAYMENT_MASK"),
	AUTHORITY_SERVICE("AUTHORITY_SERVICE"),
	BLACKBRD_AUTHORITY("BLACKBRD_AUTHORITY"),
	BLACKBRD_TENDER("BLACKBRD_TENDER"),
	CONSUMER_ACCT("CONSUMER_ACCT"),
	CREDENTIAL("CREDENTIAL"),
	CURRENCY("CURRENCY"),
	DEVICE("DEVICE"),
	DEVICE_INFO("DEVICE_INFO"),
	DEVICE_PTAS("DEVICE_PTAS"),	
	INTERNAL_AUTHORITY("INTERNAL_AUTHORITY"),
	INTERNAL_PAYMENT_TYPE("INTERNAL_PAYMENT_TYPE"),
	LOCATION("LOCATION"),
	MASTER_ID("MASTER_ID"),
	MERCHANT("MERCHANT"),
	PAYMENT_SUBTYPE("PAYMENT_SUBTYPE"),
	PAYMENT_SUBTYPE_DETAIL("PAYMENT_SUBTYPE_DETAIL"),
	TERMINAL("TERMINAL"),
	DEVICE_DETAIL("DEVICE_DETAIL"),
	CUSTOMER_DETAIL("CUSTOMER_DETAIL"),
	BLACKLIST("BLACKLIST"),	
	;
	private final String value;
	private DataSyncType(String value) {
        this.value = value;
	}
	public String getValue() {
		return value;
	}

    protected final static EnumStringValueLookup<DataSyncType> lookup = new EnumStringValueLookup<DataSyncType>(DataSyncType.class);
    public static DataSyncType getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }
}
