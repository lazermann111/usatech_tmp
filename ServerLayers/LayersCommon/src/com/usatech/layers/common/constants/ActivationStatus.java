package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;


public enum ActivationStatus {
    FACTORY((byte)0),
    NOT_ACTIVATED((byte)1),
    READY_TO_ACTIVATE((byte)2),
    ACTIVATED((byte)3);

    private final byte value;
    private ActivationStatus(byte value) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<ActivationStatus> lookup = new EnumByteValueLookup<ActivationStatus>(ActivationStatus.class);
    public static ActivationStatus getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
