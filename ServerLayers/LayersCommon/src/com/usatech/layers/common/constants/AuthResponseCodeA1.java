package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * A1h Transaction Result response codes.
 * @author yhe
 *
 */
public enum AuthResponseCodeA1 {
	DENIED((byte)0),
	APPROVED((byte)1),
	CONDITIONALLY_APPROVED((byte)2),
	FAILED((byte)3),
	INVALID_PIN((byte)4);
	private final byte value;
	private AuthResponseCodeA1(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<AuthResponseCodeA1> lookup = new EnumByteValueLookup<AuthResponseCodeA1>(AuthResponseCodeA1.class);
    public static AuthResponseCodeA1 getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
