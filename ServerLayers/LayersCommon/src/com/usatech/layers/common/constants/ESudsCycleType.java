package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum ESudsCycleType {
	TLI_REGULAR_WASH_CYCLE(1, true),
	TLI_REGULAR_COLD_WASH_CYCLE(2, false),
	TLI_REGULAR_WARM_WASH_CYCLE(3, false),
	TLI_REGULAR_HOT_WASH_CYCLE(4, false),
	TLI_SPECIAL_WASH_CYCLE(5, false),
	TLI_SPECIAL_COLD_WASH_CYCLE(6, false),
	TLI_SPECIAL_WARM_WASH_CYCLE(7, false),
	TLI_SPECIAL_HOT_WASH_CYCLE(8, false),
	TLI_SUPER_WASH_CYCLE(9, false),
	TLI_REGULAR_DRY_CYCLE(40, true),
	TLI_SPECIAL_DRY_CYCLE(41, true),
	TLI_TOP_OFF_DRY		(42, true);
	private final int value;
	private boolean regularWashCycle;
	private ESudsCycleType(int value, boolean regularWashCycle) {
        this.value = value;
        this.regularWashCycle=regularWashCycle;
	}
	public int getValue() {
		return value;
	}

    protected final static EnumIntValueLookup<ESudsCycleType> lookup = new EnumIntValueLookup<ESudsCycleType>(ESudsCycleType.class);
    public static ESudsCycleType getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
    public static ESudsCycleType getByValueSafely(int value) {
    	return lookup.getByValueSafely(value);
    }
	public boolean isRegularWashCycle() {
		return regularWashCycle;
	}
}
