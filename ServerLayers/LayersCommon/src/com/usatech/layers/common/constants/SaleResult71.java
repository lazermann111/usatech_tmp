package com.usatech.layers.common.constants;

import simple.lang.InvalidByteValueException;

public enum SaleResult71 {
    FAIL((byte)0),
    PASS((byte)1),
    ERROR((byte)2),
    ;

    private final byte value;
    private SaleResult71(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    public static SaleResult71 getByValue(byte value) throws InvalidByteValueException {
        try {
            return values()[value];
        } catch(ArrayIndexOutOfBoundsException e) {
            throw new InvalidByteValueException(value);
        }
    }
}
