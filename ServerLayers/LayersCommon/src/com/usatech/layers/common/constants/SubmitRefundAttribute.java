/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;

/**
 * @author Brian S. Krug
 *
 */
public enum SubmitRefundAttribute implements Attribute {
	ATTR_REFUND_GLOBAL_TRANS_CD("refundGlobalTransCd", "The new PSS.TRAN.TRAN_GLOBAL_TRANS_CD of the refund"),
	ATTR_ORIG_GLOBAL_TRANS_CD("origGlobalTransCd", "The PSS.TRAN.TRAN_GLOBAL_TRANS_CD of the original transaction against which this refund is applied"),
	ATTR_REFUND_TIME("refundTime", "The server local time (in milliseconds past epoch) that the refund was issued (REPORT.TRANS.CLOSE_DATE)"),
	ATTR_ISSUER("issuer", "The user name of the person who created the refund"),
	ATTR_REASON("reason", "The reason for the refund"),
	ATTR_GENERAL_REFUND_AMOUNT("refundAmount", "The amount (in major currency) of the general refund line item [type 500]"),
	ATTR_ADDITIONAL_REFUND_AMOUNT("additionalRefundAmount", "The amount (in major currency) of the additional refund line item [type 501]"),
	ATTR_ORIG_IMPORT_TIME("origImportTime", "The server local time (in milliseconds past epoch) that the original transaction was loaded into the reporting system"),
	;
	private final String value;
    private final String description;

    protected final static EnumStringValueLookup<SubmitRefundAttribute> lookup = new EnumStringValueLookup<SubmitRefundAttribute>(SubmitRefundAttribute.class);
    public static SubmitRefundAttribute getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private SubmitRefundAttribute(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}
