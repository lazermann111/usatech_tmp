package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidValueException;

public enum ESudsTopOrBottomType {
    TOP((byte)'T', 1, "Top"),
    BOTTOM((byte)'B', 0, "Bottom"),
    UNKNOWN((byte)'U', 0, "");

    private final byte value;
    private final int position;
    private final String description;
    private static final Map<Byte, ESudsTopOrBottomType> topOrBottomTypes = new HashMap<Byte, ESudsTopOrBottomType>();
    static {
        for(ESudsTopOrBottomType item : values()) {
        	topOrBottomTypes.put(item.getValue(), item);
        }
    }
    private ESudsTopOrBottomType(byte value, int position, String description) {
        this.value = value;
        this.position= position;
        this.description=description;
    }

    public byte getValue() {
        return value;
    }
    public static ESudsTopOrBottomType getByValue(byte value) throws InvalidValueException {
        if (topOrBottomTypes.containsKey(value))
            return topOrBottomTypes.get(value);
        else
            throw new InvalidValueException(value);
    }
    public static ESudsTopOrBottomType getByValue(int value) throws InvalidValueException {
        return getByValue((byte)value);
    }

	public int getPosition() {
		return position;
	}

	public String getDescription() {
		return description;
	}
}
