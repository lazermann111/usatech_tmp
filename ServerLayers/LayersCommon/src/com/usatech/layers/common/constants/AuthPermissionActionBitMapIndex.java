/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;


public enum AuthPermissionActionBitMapIndex {
	CAPTURE_DEX((byte)1, "Capture DEX"),
	CALL_IN((byte)2, "Call In to Server"),
	SHOW_APP_CASH_ITEMS((byte)3, "Show A-Counter: Cash Vended Items"),
	SHOW_APP_CASH_AMOUNT((byte)4, "Show A-Counter: Cash Vended Amount (Pennies)"),
	SHOW_APP_CASHLESS_ITEMS((byte)5, "Show A-Counter: Cashless Vended Items"),
	SHOW_APP_CASHLESS_AMOUNT((byte)6, "Show A-Counter: Cashless Vended Amount (Pennies)"),
	SHOW_INTV_CASH_ITEMS((byte)7, "Show I-Counter: Cash Vended Items"),
	SHOW_INTV_CASH_AMOUNT((byte)8, "Show I-Counter: Cash Vended Amount (Pennies)"),
	SHOW_INTV_CASHLESS_ITEMS((byte)9, "Show I-Counter: Cashless Vended Items"),
	SHOW_INTV_CASHLESS_AMOUNT((byte)10, "Show I-Counter: Cashless Vended Amount (Pennies)"),
	SHOW_PERM_CASH_ITEMS((byte)11, "Show P-Counter: Cash Vended Items"),
	SHOW_PERM_CASH_AMOUNT((byte)12, "Show P-Counter: Cash Vended Amount (Pennies)"),
	SHOW_PERM_CASHLESS_ITEMS((byte)13, "Show P-Counter: Cashless Vended Items"),
	SHOW_PERM_CASHLESS_AMOUNT((byte)14, "Show P-Counter: Cashless Vended Amount (Pennies)");

	protected final byte bitIndex;
	protected final String description;
	private AuthPermissionActionBitMapIndex(byte bitIndex, String description) {
		if(bitIndex != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.bitIndex = bitIndex;
        this.description = description;
	}

	public byte getValue() {
		return bitIndex;
	}
	public byte getBitIndex() {
		return bitIndex;
	}
	public String getDescription() {
		return description;
	}

    protected final static EnumByteValueLookup<AuthPermissionActionBitMapIndex> lookup = new EnumByteValueLookup<AuthPermissionActionBitMapIndex>(AuthPermissionActionBitMapIndex.class);
    public static AuthPermissionActionBitMapIndex getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}