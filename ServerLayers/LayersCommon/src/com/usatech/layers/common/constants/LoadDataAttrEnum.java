package com.usatech.layers.common.constants;

import com.usatech.app.Attribute;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;



/**
 * This enum defines the attribute names that are used in the construction of the message chain.
 *
 * @author bkrug
 *
 */

public enum LoadDataAttrEnum implements Attribute {
    ATTR_EVENT_ID("eventId", "From DEVICE.EVENT.EVENT_ID"),
    ATTR_DEVICE_SERIAL_CD("deviceSerialCd", "From DEVICE.DEVICE.DEVICE_SERIAL_CD"),
    ATTR_EVENT_TIME("eventTime", "Milliseconds in local time"),
    ATTR_DEVICE_TYPE("deviceType", "From DEVICE.DEVICE_TYPE.DEVICE_TYPE_ID"),
    ATTR_COUNTER_CASH_AMOUNT("counterCashAmount", "The amount of cash"),
    ATTR_COUNTER_CASHLESS_AMOUNT("counterCashlessAmount", "The amount of cashless"),
    ATTR_COUNTER_PASSCARD_AMOUNT("counterPasscardAmount", "The amount of pass cards"),
    ATTR_COUNTER_CASH_ITEMS("counterCashItems", "The count of items of cash"),
    ATTR_COUNTER_CASHLESS_ITEMS("counterCashlessItems", "The count of items of cashless"),
    ATTR_COUNTER_PASSCARD_ITEMS("counterPasscardItems", "The count of items of pass cards"),
    ATTR_COUNTERS_DISPLAYED("countersDisplayed", "Were counters shown to service tech?"),
    ATTR_COUNTERS_RESET_CODE("countersResetCode", "Were counters reset after this event ('Y' - yes; 'N' - no; 'C' - Cashless only; 'A' - all)?"),
    ATTR_COUNTERS_REPORTED("countersReported", "Were counters sent to the server?"),
    ATTR_MINOR_CURRENCY_FACTOR("minorCurrencyFactor", "The number to divide the amount by to get it in the unit of the major currency (100 for US since there are 100 cents in 1 dollar)"),
    ATTR_CREATE_DEX("createDEX", "Should a DEX file be created as a marker for this fill event?"),
    
	ATTR_TRAN_COUNT("tranCount", "Number of transactions in settlement batch"),
	ATTR_TRAN_GLOBAL_TRANS_CD("tranGlobalTransCd", "From PSS.TRAN.TRAN_GLOBAL_TRANS_CD"),
	ATTR_SOURCE_SYSTEM_CD("sourceSystemCd", "For REPORT.TRANS.SOURCE_SYSTEM_CD"),
	ATTR_SETTLE_STATE_ID("settleStateId", "For CORP.LEDGER.SETTLE_STATE_ID"),
	ATTR_SETTLE_DATE("settleDate", "Time transaction was finalized"),
	ATTR_SETTLE_UPDATE_NEEDED("settleUpdateNeeded", "Y if the settlement state needs to be updated, N otherwise"),
	ATTR_TRANS_TYPE_ID("transTypeId", "For REPORT.TRANS.TRANS_TYPE_ID"),
	
	ATTR_FILE_TRANSFER_ID("fileTransferId", "From DEVICE.FILE_TRANSFER.FILE_TRANSFER_ID"),
	ATTR_FILE_TRANSFER_TS("fileTransferTs", "From DEVICE.DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS"),
	ATTR_DEVICE_ID("deviceId", "From DEVICE.DEVICE.DEVICE_ID"),
	ATTR_DEVICE_CHARSET("deviceCharset", "The charset of the device"),
    
	;
    private final String value;
    private final String description;

    protected final static EnumStringValueLookup<LoadDataAttrEnum> lookup = new EnumStringValueLookup<LoadDataAttrEnum>(LoadDataAttrEnum.class);
    public static LoadDataAttrEnum getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private LoadDataAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return The unique key of this attribute
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}
