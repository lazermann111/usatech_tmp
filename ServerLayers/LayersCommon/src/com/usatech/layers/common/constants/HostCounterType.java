package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidStringValueException;

public enum HostCounterType {
    CURRENCY_TRANSACTION("CURRENCY_TRANSACTION"),
    CURRENCY_MONEY("CURRENCY_MONEY"),
    CASHLESS_TRANSACTION("CASHLESS_TRANSACTION"),
    CASHLESS_MONEY("CASHLESS_MONEY"),
    PASSCARD_TRANSACTION("PASSCARD_TRANSACTION"),
    PASSCARD_MONEY("PASSCARD_MONEY"),
    TOTAL_BYTES("TOTAL_BYTES"),
    TOTAL_SESSIONS("TOTAL_SESSIONS"),
    I_CURRENCY_TRANSACTION("I_CURRENCY_TRANSACTION"),
    I_CURRENCY_MONEY("I_CURRENCY_MONEY"),
    I_CASHLESS_TRANSACTION("I_CASHLESS_TRANSACTION"),
    I_CASHLESS_MONEY("I_CASHLESS_MONEY"),
    P_CURRENCY_TRANSACTION("P_CURRENCY_TRANSACTION"),
    P_CURRENCY_MONEY("P_CURRENCY_MONEY"),
    P_CASHLESS_TRANSACTION("P_CASHLESS_TRANSACTION"),
    P_CASHLESS_MONEY("P_CASHLESS_MONEY"),    
    ;

    private final String value;
    private static final Map<String, HostCounterType> hostCounterTypes = new HashMap<String, HostCounterType>();
    static {
        for(HostCounterType hct : values()) {
            hostCounterTypes.put(hct.getValue(), hct);
        }
    }
    private HostCounterType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static HostCounterType getByValue(String value) throws InvalidStringValueException {
        if (hostCounterTypes.containsKey(value))
            return hostCounterTypes.get(value);
        else
            throw new InvalidStringValueException(value);
    }
}
