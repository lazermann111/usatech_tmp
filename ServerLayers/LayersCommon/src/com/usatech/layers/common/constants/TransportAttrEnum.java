package com.usatech.layers.common.constants;

import com.usatech.app.Attribute;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

public enum TransportAttrEnum implements Attribute {
	ATTR_TRANSPORT_MAX_RETRY_ALLOWED("transportMaxRetryAllowed", "Maximum number of retry of transport allowed."),
	ATTR_TRANSPORT_RESULT_ID("transportResultId", "The id of the transport test result."),
	ATTR_SENT_SUCCESS("sentSuccess", "Flag of true or false to indicate transport result."),
	ATTR_SENT_DETAILS("sentDetails", "Detailed info for the transport result."),
	ATTR_TRANSPORT_ID("transport.transportId", "The Transport Id"),
	ATTR_TRANSPORT_TYPE_ID("transport.transportTypeId", "The Transport Type Id"),
	ATTR_TRANSPORT_REASON("transport.transportReason", ""),
	ATTR_FILE_NAME("file.fileName", "From REPORT.STORED_FILE.STORED_FILE_NAME"),
	ATTR_FILE_KEY("file.fileKey", "From REPORT.STORED_FILE.STORED_FILE_KEY"),
	ATTR_FILE_CONTENT_TYPE("file.fileContentType", "From REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE"),
	ATTR_FILE_ID("transport.fileId", "From REPORT.STORED_FILE.STORED_FILE_ID"),
	ATTR_FILE_PASSCODE("transport.filePasscode", "From REPORT.STORED_FILE.STORED_FILE_PASSCODE"),
	ATTR_SENT_DATE("sentDate", "Timestamp of transport attempt"),
	ATTR_ERROR_TEXT("error.errorText", ""),
	ATTR_ERROR_TIME("error.errorTs", ""),
	ATTR_ERROR_TYPE("error.errorTypeId", ""),
	ATTR_ERROR_RETRY_PROPS("error.retryProps", ""),
	;
	private final String value;
    private final String description;

    protected final static EnumStringValueLookup<TransportAttrEnum> lookup = new EnumStringValueLookup<TransportAttrEnum>(TransportAttrEnum.class);
    public static TransportAttrEnum getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	private TransportAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}
