package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;



public enum ShutdownOption {
	SEND_TRAN_BEFORE_SHUTDOWN((byte)1),
    SHUTDOWN_NOW((byte)0);

    private final byte value;
    private ShutdownOption(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<ShutdownOption> lookup = new EnumByteValueLookup<ShutdownOption>(ShutdownOption.class);
    public static ShutdownOption getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
