package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum CRCTypeLegacy {
	CRC16((byte)0, 2),
	CRC32((byte)1, 4);
	protected final byte value;
	protected final int length;
	
	private CRCTypeLegacy(byte value, int length) {
        this.value = value;
		this.length = length;
	}

	public byte getValue() {
		return value;
	}
	public int getLength() {
		return length;
	}
	
    protected final static EnumByteValueLookup<CRCTypeLegacy> lookup = new EnumByteValueLookup<CRCTypeLegacy>(CRCTypeLegacy.class);
    public static CRCTypeLegacy getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
