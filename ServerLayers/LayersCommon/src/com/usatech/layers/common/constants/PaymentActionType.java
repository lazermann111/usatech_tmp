package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidValueException;

public enum PaymentActionType {
    PURCHASE('C'),
    REPLENISHMENT('R'),
    BALANCE_CHECK('B'),
    TOKENIZE('T'),
    ;

    private final char value;
    private static final Map<Character, PaymentActionType> paymentActionTypes = new HashMap<Character, PaymentActionType>();
    static {
        for(PaymentActionType item : values()) {
        	paymentActionTypes.put(item.getValue(), item);
        }
    }
    private PaymentActionType(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }

	public static PaymentActionType getByValue(char value) throws InvalidValueException {
        if (paymentActionTypes.containsKey(value))
            return paymentActionTypes.get(value);
		throw new InvalidValueException(value);
    }

    public static PaymentActionType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
}
