package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum InitializationReason {
    NEW_DEVICE((byte)0),
    COMMUNICATION_FAILURE((byte)1),
    APPLICATION_UPGRADE((byte)2),
    DATA_CORRUPTION((byte)3),
    SERVER_REQUEST((byte)4),
    DEVICE_INFO_SYNC((byte)5),
    UNDEFINED((byte)15)
    ;
	private static final EnumByteValueLookup<InitializationReason> lookup = new EnumByteValueLookup<InitializationReason>(InitializationReason.class);

    private final byte value;

    private InitializationReason(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    public static InitializationReason getByValue(byte value) throws InvalidByteValueException {
		return lookup.getByValue(value);
    }
}
