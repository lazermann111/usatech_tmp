/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum GenericRequestType {
	UPDATE_STATUS_REQUEST((byte)0), FILE_TRANSFER_REQUEST((byte)1), PROPERTY_LIST_REQUEST((byte)2), ACTIVATION_REQUEST((byte)3);

	private final byte value;
	private GenericRequestType(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}
    protected final static EnumByteValueLookup<GenericRequestType> lookup = new EnumByteValueLookup<GenericRequestType>(GenericRequestType.class);
    public static GenericRequestType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
