package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum DEXReason {
	SCHEDULED(1, 0),
	INTERVAL(2, 0), //None of these exist in production as of 10/26/2009; assume it is unused
	FILL(3, 1),
	ALARM(4, 5),
	UNKNOWN(0, 99)
	;
	private final int value;
    private final int vdiDexReason;
    private DEXReason(int value, int vdiDexReason) {
        this.value = value;
        this.vdiDexReason = vdiDexReason;
    }
    public int getValue() {
        return value;
    }
    public int getVdiDexReason() {
		return vdiDexReason;
	}

    protected final static EnumIntValueLookup<DEXReason> lookup = new EnumIntValueLookup<DEXReason>(DEXReason.class);

    public static DEXReason getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
	
	public static DEXReason fromVdiDexReason(int vdiDexReason) {
		for(DEXReason d : DEXReason.values()) {
			if(d.getVdiDexReason() == vdiDexReason)
				return d;
		}
		return DEXReason.UNKNOWN;
	}
}
