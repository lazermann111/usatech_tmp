package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum CardReaderType {
		GENERIC((byte) 0, null, KeyVariantType.UNSUPPORTED, false, false),
    MAGTEK_MAGNESAFE((byte)1, "DESede/CBC/NOPADDING", KeyVariantType.PIN, false, false),
    IDTECH_SECUREMAG((byte)2, "DESede/CBC/NOPADDING", KeyVariantType.PIN, false, false),
    IDTECH_SECURED((byte)3, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    ACS_ACR31((byte)4, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    UIC_BEZEL((byte)5, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    OTI_BEZEL((byte)6, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    BLUETOOTH((byte)7, "RSA/ECB/OAEPPADDING,AES/CBC/NOPADDING", KeyVariantType.UNSUPPORTED, false, false),
    INGENICO_MOBILE((byte)8, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR((byte)9, "AES/CBC/NOPADDING", KeyVariantType.DATA, true, false),
    IDTECH_VENDX_ENCRYPTED_EMV((byte)10, "AES/CBC/NOPADDING", KeyVariantType.DATA, true, false),
    IDTECH_VENDX_NON_ENCRYPTED((byte)11, "", KeyVariantType.DATA, true, false),
    IDTECH_VEND3AR((byte)12, "", KeyVariantType.DATA, true, false),
    OTI_EMV((byte)13, "DESede/CBC/NOPADDING", KeyVariantType.PIN, true, false),    
    IDTECH_VENDX_PARSED_AES((byte)14, "AES/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    IDTECH_VENDX_PARSED_3DES((byte)15, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    OTI_BEZEL_PARSED_BCD((byte)16, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    INGENICO_BEZEL((byte)17, "DESede/CBC/NOPADDING", KeyVariantType.DATA, false, false),
    IDTECH_VENDX_UNPARSED((byte)18, "DYNAMIC/CBC/NOPADDING", KeyVariantType.DATA, true, true),
    ;

    private final byte value;
    private final String cipherName;
    private final KeyVariantType keyVariantType; 
    private final boolean rawCardReader;
    private final boolean emvCertified;
    
    private CardReaderType(byte value, String cipherName, KeyVariantType keyVariantType, boolean rawCardReader, boolean emvCertified) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
        this.cipherName = cipherName;
        this.keyVariantType = keyVariantType;
        this.rawCardReader = rawCardReader;
        this.emvCertified = emvCertified;
    }
    
    public byte getValue() {
        return value;
    }
    
		public String getCipherName() {
			return cipherName;
		}
	
    public KeyVariantType getKeyVariantType() {
        return keyVariantType;
    }
    
    public boolean isRawCardReader() {
			return rawCardReader;
		}

		public boolean isEmvCertified() {
			return emvCertified;
		}

		protected final static EnumByteValueLookup<CardReaderType> lookup = new EnumByteValueLookup<CardReaderType>(CardReaderType.class);
    public static CardReaderType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
