package com.usatech.layers.common.constants;

public enum DeniedReason {
	UNPARSEABLE_CARD_DATA, // NetLayer could not parse card data
	INSUFFICIENT_FUNDS, // Processor found account but it does not have enough money on it
	NO_ACCOUNT, // Processor could not find an account
	NOT_REGISTERED, // Processor found account but it is not fully activated
	NOT_ACTIVE, // Processor found account but it is expired or closed
	UNSUPPORTED_CURRENCY, // Auth Layer does not support the currency requested
	CONFIGURATION_ISSUE, // When payment config is not correct
	KEYMGRS_NOT_LOADED, // When Netlayer cannot connect to any keymgr either because they are down or not loaded
	DUKPT_DECRYPTION_FAILURE, // KeyMgr could not decrypt card data
	INTERNAL_TIMEOUT, // Interprocess communication timed out
	INVALID_VERIFICATION_DATA, // Verification data does not match according to the Processor
	MISSING_INPUT, // Some required data was not provided in Auth Layer
	INVALID_INPUT, // Some data is not formatted correctly Auth Layer
	INVALID_RESPONSE, // Processor returned a response that we don't recognize
	PROCESSOR_ERROR, // Processor returned a response that means an error occurred on it
	PROCESSOR_RESPONSE, // Processor returned a response that means not accepted
	PROCESSOR_TIMEOUT, // Processor timed out
	CVV_MISMATCH, // The security code did not match according to the Processor
	AVS_MISMATCH, // The address did not match according to the Processor
	CVV_AND_AVS_MISMATCH, // Neither the security code nor the address matched according to the Processor
	AUTH_LAYER_PROCESSING_ERROR, // Auth Layer got an error while converting, parsing, or processing attributes
	APP_LAYER_PROCESSING_ERROR, // App Layer got an error while converting, parsing, or processing attributes
	DATA_FROM_CLIENT, // Client sent data that is invalid for some reason,
	NO_GEOLOCATION,
	DISALLOWED_GEOLOCATION,
	STALE_GEOLOCATION,
	DEVICE_TEMP_BLACKLIST
	;
}