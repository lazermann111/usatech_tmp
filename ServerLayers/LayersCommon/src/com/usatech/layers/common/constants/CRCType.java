/**
 *
 */
package com.usatech.layers.common.constants;

import java.io.IOException;
import java.io.InputStream;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum CRCType {
	CRC16Server((byte)0, 2, "CRC16- Server Style") {
		@Override
		public Calculator newCalculator() {
			return new Calculator16_ccitt(0xFFFF) {
				public byte[] getValue() {
					return new byte[] { (byte)((crc & 0xFF00) >> 8), (byte)(crc & 0x00FF) };
				}
			};
		}
	},
	CRC16FMM((byte)1, 2, "CRC16- G6 FMM Style") {
		@Override
		public Calculator newCalculator() {
			return new Calculator16_ccitt(0x0000) {
				public byte[] getValue() {
					return new byte[] { (byte)(crc & 0x00FF), (byte)((crc & 0xFF00) >> 8) };
				}
			};
		}
	},
	CRC16SUM((byte) 2, 2, "CRC16- Simple Sum") {
		@Override
		public Calculator newCalculator() {
			return new Calculator16_ccitt(0x0000) {
				public byte[] getValue() {
					return new byte[] { (byte) ((crc & 0xFF00) >> 8), (byte) (crc & 0x00FF) };
				}

				public void update(byte b) {
					crc = (crc + (0xFF & b));
				}
			};
		}
	},
	/*
	CRC16SUM((byte) 2, 2, "CRC16- Simple Sum") {
		protected boolean odd = true;
		@Override
		public Calculator newCalculator() {
			return new Calculator16_ccitt(0x0000) {
				public byte[] getValue() {
					return new byte[] { (byte) ((crc & 0xFF00) >> 8), (byte) (crc & 0x00FF) };
				}

				public void update(byte b) {
					if(odd)
						crc = (crc + 256 * (0xFF & b));
					else
						crc = (crc + (0xFF & b));
					odd = !odd;
				}
			};
		}
	}, */

	;
	protected final byte value;
	protected final int length;
	protected final String description;
	public static interface Calculator {
		public void update(byte b) ;
		public void update(byte[] bytes) ;
		public void update(byte[] bytes, int offset, int length) ;
		public byte[] getValue() ;
		public void reset() ;
	}
	protected abstract static class Calculator16_ccitt implements Calculator {
		protected final int initial;
		protected int crc;
		public Calculator16_ccitt(int initial) {
			this.initial = initial;
			this.crc = initial;
		}
		public void update(byte[] bytes) {
			if(bytes != null)
				for(byte b : bytes)
					update(b);
		}
		public void reset() {
			crc = initial;
		}
		public void update(byte[] bytes, int offset, int length) {
			if(bytes != null)
				for(int i = 0; i < length; i++)
					update(bytes[i+offset]);
		}
		//taken from com.usatech.util.crypto.crc.CRC16
		public void update(byte b) {
			crc  = ((crc & 0xff00) >>> 8) | ((crc & 0xff) << 8);
			crc ^= b & 0xff;
			crc ^= (crc & 0xff) >>> 4;
			crc ^= crc << 12;
			crc ^= (crc & 0xff) << 5;
		}
	}
	private CRCType(byte value, int length, String description) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
		this.length = length;
		this.description = description;
	}

	public void check(byte[] expected, InputStream in) throws InvalidCRCException, IOException  {
		byte[] calculated = calculate(in);
		for(int i = 0; i < length; i++)
			if(expected[i] != calculated[i])
			throw new InvalidCRCException(this, expected, calculated);
	}

	public byte[] calculate(InputStream in) throws IOException {
		Calculator calc = newCalculator();
		int b;
		while((b=in.read()) >= 0) {
			calc.update((byte)b);
		}
		return calc.getValue();
	}

	public abstract Calculator newCalculator() ;

	public byte getValue() {
		return value;
	}
	public int getLength() {
		return length;
	}
	public String getDescription() {
		return description;
	}
    protected final static EnumByteValueLookup<CRCType> lookup = new EnumByteValueLookup<CRCType>(CRCType.class);
    public static CRCType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
