package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * 60h Authorization Response v2.0
 * @author yhe
 *
 */
public enum AuthResponseCode60 {
	FAIL((byte)0),
	PASS((byte)1);
	private final byte value;
	private AuthResponseCode60(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<AuthResponseCode60> lookup = new EnumByteValueLookup<AuthResponseCode60>(AuthResponseCode60.class);
    public static AuthResponseCode60 getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
