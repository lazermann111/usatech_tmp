package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum AuthResponseActionCode {
	NO_ACTION((byte)0),
	PERFORM_CALL_IN((byte)1),
	REBOOT((byte)2),
	PROCESS_PROP_LIST((byte)3),
	;

	private final byte value;
	private AuthResponseActionCode(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}
    protected final static EnumByteValueLookup<AuthResponseActionCode> lookup = new EnumByteValueLookup<AuthResponseActionCode>(AuthResponseActionCode.class);
    public static AuthResponseActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
