/**
 *
 */
package com.usatech.layers.common.constants;

/**
 * @author Brian S. Krug
 *
 */
public enum DevicePropertyType {
	STATIC, DYNAMIC, READ_ONLY
}
