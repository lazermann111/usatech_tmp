package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidValueException;

public enum TranBatchType {
    ACTUAL('A'),
    INTENDED('I'),
    ;

    private final char value;
    private static final Map<Character, TranBatchType> TranBatchTypes = new HashMap<Character, TranBatchType>();
    static {
        for(TranBatchType item : values()) {
            TranBatchTypes.put(item.getValue(), item);
        }
    }
    private TranBatchType(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
    public static TranBatchType getByValue(char value) throws InvalidValueException {
        if (TranBatchTypes.containsKey(value))
            return TranBatchTypes.get(value);
        else
            throw new InvalidValueException(value);
    }
}
