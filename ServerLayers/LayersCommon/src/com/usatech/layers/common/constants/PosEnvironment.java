package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum PosEnvironment {
	UNATTENDED('U', "Unattended"),
    ATTENDED('A', "Attended"),
    MOTO('M', "Mail Order / Telephone Order"),
    ECOMMERCE('E', "Ecommerce"),
    MOBILE('C', "Mobile"),
    UNSPECIFIED('?', "Unspecified"),
    ;

    private final char value;
    private final String description;
	protected final static EnumCharValueLookup<PosEnvironment> lookup = new EnumCharValueLookup<PosEnvironment>(PosEnvironment.class);

	private PosEnvironment(char value, String description) {
        this.value = value;
        this.description = description;
	}
    public char getValue() {
        return value;
    }
    public static PosEnvironment getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
    }
    public static PosEnvironment getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
	public String getDescription() {
		return description;
	}
}
