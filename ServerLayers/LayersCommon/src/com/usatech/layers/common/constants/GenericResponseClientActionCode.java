/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum GenericResponseClientActionCode {
	NO_ACTION((byte)0), UPDATE_STATUS_REQUEST((byte)1), STOP_FILE_TRANSFER((byte)2);

	private final byte value;
	private GenericResponseClientActionCode(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<GenericResponseClientActionCode> lookup = new EnumByteValueLookup<GenericResponseClientActionCode>(GenericResponseClientActionCode.class);
    public static GenericResponseClientActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
