package com.usatech.layers.common.constants;

public enum AdditionalInfoType {
	ISIS_SMARTTAP((byte)1, "ISIS SmartTap"),
	EMV_CONTACT((byte)2, "EMV Contact"),
	EMV_CONTACTLESS((byte)3, "EMV Contactless");
	private final byte value;
	private final String description;
	
	AdditionalInfoType(byte value, String description) {
		this.value = value;
		this.description = description;
	}

	public byte getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
	
}