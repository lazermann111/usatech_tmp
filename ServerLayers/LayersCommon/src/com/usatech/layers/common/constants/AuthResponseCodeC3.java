package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * Auth authorization type response codes.
 * @author yhe
 *
 */
public enum AuthResponseCodeC3 {
	// Note: AuthResultCode references AuthResponseCodeC3 during initialization, and AuthResponseCodeC3 was 
	// referencing AuthResultCode during initialization. This was a circular enum initialization which 
	// creates NULL values in whichever enum is initialized 2nd. I am changing this to use enum polymorphism 
	// to override the getAuthResultCode to return the appropriate AuthResultCode to work around this issue.
	// For more information see http://stackoverflow.com/questions/1506410/java-enums-two-enum-types-each-containing-references-to-each-other
	SUCCESS((byte)0) {
		public AuthResultCode getAuthResultCode() {
			return AuthResultCode.APPROVED;
		}
	},
	CONDITIONAL_SUCCESS((byte)1) {
		public AuthResultCode getAuthResultCode() {
			return AuthResultCode.PARTIAL;
		}
	},
	DECLINE((byte)2) {
		public AuthResultCode getAuthResultCode() {
			return AuthResultCode.DECLINED;
		}
	},
	DECLINE_PERMANENT((byte)3) {
		public AuthResultCode getAuthResultCode() {
			return AuthResultCode.DECLINED_PERMANENT;
		}
	},
	FAILURE((byte)4) {
		public AuthResultCode getAuthResultCode() {
			return AuthResultCode.FAILED;
		}
	},
	DECLINE_RESTRICTED((byte)5) {
		public AuthResultCode getAuthResultCode() {
			return AuthResultCode.DECLINED_PAYMENT_METHOD;
		}
	};
	
	private final byte value;

	private AuthResponseCodeC3(byte value) {
		if (value != this.ordinal())
			throw new IllegalArgumentException("Value must equal ordinal");
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public abstract AuthResultCode getAuthResultCode();

	protected final static EnumByteValueLookup<AuthResponseCodeC3> lookup = new EnumByteValueLookup<AuthResponseCodeC3>(AuthResponseCodeC3.class);

	public static AuthResponseCodeC3 getByValue(byte value) throws InvalidByteValueException {
		return lookup.getByValue(value);
	}
}
