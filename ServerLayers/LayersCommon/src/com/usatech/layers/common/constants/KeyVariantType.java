package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum KeyVariantType {
    UNSUPPORTED((byte)0),
	PIN((byte)1),
    DATA((byte)2);

    private final byte value;
    private KeyVariantType(byte value) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<KeyVariantType> lookup = new EnumByteValueLookup<KeyVariantType>(KeyVariantType.class);
    public static KeyVariantType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
