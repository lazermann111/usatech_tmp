package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;



/**
 * This enum defines the attribute names that are used in the construction of the message chain.
 *
 * @author bkrug
 *
 */

public enum ConsumerCommAttrEnum implements Attribute {
	ATTR_CONSUMER_ACCT_ID("consumerAcctId"),
	ATTR_COMMUNICATION_TYPE("communicationType"),
	ATTR_CASH_BACK_AMOUNT("cashBackAmount"),
	ATTR_PURCHASE_AMOUNT("purchaseAmount"),
	ATTR_CAMPAIGN_ID("campaignId"),
	ATTR_REPLENISH_ID("replenishId"),
	ATTR_DENIED_COUNT("deniedCount"),
	ATTR_REPLENISH_CARD_MASKED("replenishCardMasked"),
	ATTR_REPLENISH_TRAN_CD("replenishDeviceTranCd"),
	ATTR_REPLENISH_CASH_IND("replenishCashInd"),
	ATTR_REPORT_TRAN_ID("reportTranId"),
	ATTR_REPLENISH_AMOUNT("replenishAmount"),
	ATTR_CONSUMER_ACCT_BALANCE("consumerAcctBalance"),
	ATTR_BELOW_BALANCE_THRESHOLD("belowBalanceThreshold"),
	ATTR_REPLENISH_PRIORITY("replenishPriority"),
	ATTR_EXPIRATION_DATE("expirationDate"),
	ATTR_PASS_URL("passUrl"),
	ATTR_CARD_ID("cardId"),
	ATTR_MASKED_CARD("maskedCard"),
	ATTR_PASS_SERIAL_NUMBER("passSerialNumber"),
	ATTR_PASS_TEMPLATE_ID("passTemplateId"),
	ATTR_PASS_COMMUNICATION_EMAIL("passCommunicationEmail"),
	ATTR_PASS_IS_DEMO("isDemo"),
	ATTR_CONSUMER_PASS_ID("consumerPassId"),
	ATTR_MORE_PASS_QUEUENAME("morePassQueueName"),
	ATTR_CARD_TOKEN("cardToken"),
	ATTR_PASS_PUBLIC_KEY("passPublicKey"),
	ATTR_CALL_ID("callId"),
	ATTR_CONSUMER_PASS_IDENTIFIER("consumerPassIdentifier")
    ;
    private final String value;
    
    protected final static EnumStringValueLookup<ConsumerCommAttrEnum> lookup = new EnumStringValueLookup<ConsumerCommAttrEnum>(ConsumerCommAttrEnum.class);
    public static ConsumerCommAttrEnum getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private ConsumerCommAttrEnum(String value) {
		this.value = value;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return a char
	 */
	public String getValue() {
		return value;
	}
}
