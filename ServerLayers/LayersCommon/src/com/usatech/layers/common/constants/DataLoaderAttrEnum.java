package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;

public enum DataLoaderAttrEnum implements Attribute {
	ATTR_ACTION("action"),
	ATTR_TABLE("table"), 
	ATTR_SOURCE("source"),
	ATTR_METADATA("metadata"), 
	ATTR_SOURCE_VALUES("sourceValues"),

	;
	private final String value;

	protected final static EnumStringValueLookup<DataLoaderAttrEnum> lookup = new EnumStringValueLookup<DataLoaderAttrEnum>(DataLoaderAttrEnum.class);

	public static DataLoaderAttrEnum getByValue(String value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

	/**
	 * Internal constructor.
	 * 
	 * @param value
	 */
	private DataLoaderAttrEnum(String value) {
		this.value = value;
	}

	/**
	 * Get the intrinsic value of the type.
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}
}
