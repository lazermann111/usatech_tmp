package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

public enum ESudsManufacturer {
	UNKNOWN("Unknown"),
	MAYTAG("Maytag"),
	SPEEDQUEEN("Speed Queen");
	private final String value;
	private ESudsManufacturer(String value) {
        this.value = value;
	}
	public String getValue() {
		return value;
	}

    protected final static EnumStringValueLookup<ESudsManufacturer> lookup = new EnumStringValueLookup<ESudsManufacturer>(ESudsManufacturer.class);
    public static ESudsManufacturer getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }
}
