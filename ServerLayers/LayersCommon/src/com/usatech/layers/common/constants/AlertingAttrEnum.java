/*
 * (C) USA Technologies 2011
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;

/**
 * Alert message attributes used both in the app layer and in report generator
 * 
 * @author phorsfield
 */
public enum AlertingAttrEnum implements Attribute {
	AL_TERMINAL("terminalId", "Long; Terminal ID associated with this set of alerts"),
	AL_SERIAL_NUM("serialNum", "String; Serial Number associated with the device"),
	AL_NOTIFICATION_TS("notificationTs","Date; Time at which notification of event was received; must match database call"),/*
	AL_NOTIFY_USER_ID("request.userId", "Long; Set when alert is duplicated by Report Generator."), 
	AL_NOTIFY_USER_REPORT_ID("request.userReportId", "Long; Report Register entry. Set when alert is duplicated by Report Generator."), 
	AL_NOTIFY_REPORT_ID("request.reportId", "Long; Report Register entry. Set when alert is duplicated by Report Generator."), 
	AL_NOTIFY_TITLE("request.title", "String; Report Register 'title' entry. Set when alert is duplicated by Report Generator."),
	AL_NOTIFY_FREQ_NAME("request.frequencyName", "String; Report Register 'frequency' entry. Set when alert is duplicated by Report Generator."), 
	AL_REPORT_DEVICE("report.specificDevice", "String; Set by Report Generator so that the report title can include the device name."),
	*/
	AL_EVENT_ID("eventId", "DEVICE.EVENT.EVENT_ID of the alert or DEVICE.FILE_TRANSFER.FILE_TRANSFER_ID of the DEX file"),
 AL_EVENT_SOURCE_TYPE("eventSourceType", "The AlertSourceCode of the alert"),
	; 
	
	private final String value;
	private final String description;

	protected final static EnumStringValueLookup<AlertingAttrEnum> lookup = new EnumStringValueLookup<AlertingAttrEnum>(
			AlertingAttrEnum.class);

	public static AlertingAttrEnum getByValue(String value)
			throws InvalidValueException {
		return lookup.getByValue(value);
	}

	/**
	 * Internal constructor.
	 * 
	 * @param value
	 */
	private AlertingAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 * 
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
	
}

/*	AL_COUNT("alerts.count", "Number of alerts in this message"),
AL_SOURCE("alert.##.source", "Enum String;The defined source name"),
AL_SOURCE_ID("alert.##.sourceId", "Long;The primary key identifier of the specific source"),
AL_DEVICE_NAME("alert.##.deviceName", "String;The device name (or, user friendly name of the originator within the source namespace)"),
AL_EVENT_TYPE("alert.##.eventType", "Int;Alert/Event Type Id"),
AL_COMPONENT("alert.##.component", "Int;A component identifier from the originator"),
AL_SESSION("alert.##.session", "String;A communications session identifier"),
AL_TIMESTAMP("alert.##.timeStamp", "Long UTC Timestamp millis;The time at which the event was sent"),
AL_DESCRIPTION("alert.##.description", "String; Description of the alert"),
*/