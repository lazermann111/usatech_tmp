package com.usatech.layers.common.constants;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum CardType {
    CREDIT_SWIPE('C', EntryType.SWIPE, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Credit"),
    CREDIT_CONTACTLESS('R', EntryType.CONTACTLESS, PaymentType.CREDIT, PaymentActionType.PURCHASE, "RFID Credit"),
    CREDIT_ISIS('J', EntryType.ISIS, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Softcard Credit"),
    CREDIT_MANUAL_ENTRY('N', EntryType.MANUAL, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Manual Entry Credit"),
    CREDIT_BLUETOOTH('B', EntryType.BLUETOOTH, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Bluetooth Credit"),
    DYNAMIC('D', EntryType.DYNAMIC, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Dynamic"),
    INTERAC_FLASH('F', EntryType.INTERAC_FLASH, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Interac Flash"),
    SPECIAL_SWIPE('S', EntryType.SWIPE, PaymentType.SPECIAL, PaymentActionType.PURCHASE, "Special"),
    SPECIAL_CONTACTLESS('P', EntryType.CONTACTLESS, PaymentType.SPECIAL, PaymentActionType.PURCHASE, "RFID Special"),
    SPECIAL_ISIS('K', EntryType.ISIS, PaymentType.SPECIAL, PaymentActionType.PURCHASE, "Softcard Special"),
    SPECIAL_MANUAL_ENTRY('T', EntryType.MANUAL, PaymentType.SPECIAL, PaymentActionType.PURCHASE, "Manual Entry Special"),
    SPECIAL_BLUETOOTH('A', EntryType.BLUETOOTH, PaymentType.SPECIAL, PaymentActionType.PURCHASE, "Bluetooth Special"),
    EMV_CREDIT_CONTACT('L', EntryType.EMV_CONTACT, PaymentType.CREDIT, PaymentActionType.PURCHASE, "EMV Contact"),
    EMV_CREDIT_CONTACTLESS('U', EntryType.EMV_CONTACTLESS, PaymentType.CREDIT, PaymentActionType.PURCHASE, "EMV Contactless"),
    INVENTORY_CARD('I', EntryType.SWIPE, null, PaymentActionType.PURCHASE, "Inventory"),
    ERROR('E', EntryType.UNSPECIFIED, null, PaymentActionType.PURCHASE, "Error"),
    CASH('M', EntryType.CASH, PaymentType.CASH, PaymentActionType.PURCHASE, "Cash"),
    REPLENISH_CREDIT_ISIS('V', EntryType.ISIS, PaymentType.CREDIT, PaymentActionType.REPLENISHMENT, "Replenish Softcard Credit"),
    REPLENISH_CREDIT_SWIPE('W', EntryType.SWIPE, PaymentType.CREDIT, PaymentActionType.REPLENISHMENT, "Replenish Swiped Credit"),
    REPLENISH_CREDIT_CONTACTLESS('X', EntryType.CONTACTLESS, PaymentType.CREDIT, PaymentActionType.REPLENISHMENT, "Replenish RFID Credit"),
    REPLENISH_CREDIT_MANUAL_ENTRY('Y', EntryType.MANUAL, PaymentType.CREDIT, PaymentActionType.REPLENISHMENT, "Replenish Manual Entry Credit"),
    REPLENISH_CASH('Z', EntryType.CASH, PaymentType.CASH, PaymentActionType.REPLENISHMENT, "Replenish Cash"),
    TOKEN('Q', EntryType.TOKEN, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Tokenized Account"),
    VAS('O', EntryType.VAS, PaymentType.CREDIT, PaymentActionType.PURCHASE, "Apple VAS"),
    ;

    private final char value;
    private final EntryType entryType;
	private final PaymentType paymentType;
    private final PaymentActionType paymentActionType;
    private final String cardTypeName;
	protected final static EnumCharValueLookup<CardType> lookup = new EnumCharValueLookup<CardType>(CardType.class);

	protected static class XRefItem extends AbstractList<CardType> implements RandomAccess {
		private CardType[] a = new CardType[3];
		private int length = 0;

		protected void internalAdd(CardType ct) {
			if(length >= a.length) {
				CardType[] tmp = new CardType[length + 3];
				System.arraycopy(a, 0, tmp, 0, a.length);
				a = tmp;
			}
			a[length++] = ct;
		}

		@Override
		public int size() {
			return length;
		}

		@Override
		public Object[] toArray() {
			return Arrays.copyOf(this.a, length, Object[].class);
		}

		@Override
		@SuppressWarnings("unchecked")
		public <T> T[] toArray(T[] a) {
			int size = size();
			if(a.length < size)
				return Arrays.copyOf(this.a, size, (Class<? extends T[]>) a.getClass());
			System.arraycopy(this.a, 0, a, 0, size);
			if(a.length > size)
				a[size] = null;
			return a;
		}

		@Override
		public CardType get(int index) {
			if(index >= length)
				throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + length);
			return a[index];
		}

		@Override
		public int indexOf(Object o) {
			CardType[] a = this.a;
			if(o == null) {
				for(int i = 0; i < length; i++)
					if(a[i] == null)
						return i;
			} else {
				for(int i = 0; i < length; i++)
					if(o.equals(a[i]))
						return i;
			}
			return -1;
		}

		@Override
		public boolean contains(Object o) {
			return indexOf(o) != -1;
		}

		@Override
		public Spliterator<CardType> spliterator() {
			return Spliterators.spliterator(a, Spliterator.ORDERED);
		}

		@Override
		public void forEach(Consumer<? super CardType> action) {
			Objects.requireNonNull(action);
			for(int i = 0; i < length; i++) {
				action.accept(a[i]);
			}
		}

		@Override
		public void sort(Comparator<? super CardType> c) {
			Arrays.sort(a, 0, length, c);
		}
	}

	protected final static XRefItem[][] cardTypeXref;
	static {
		EntryType[] entryTypes = EntryType.values();
		PaymentActionType[] paymentActionTypes = PaymentActionType.values();
		cardTypeXref = new XRefItem[entryTypes.length][];
		for(EntryType entryType : entryTypes)
			cardTypeXref[entryType.ordinal()] = new XRefItem[paymentActionTypes.length];
		for(CardType cardType : CardType.values()) {
			XRefItem list = cardTypeXref[cardType.getEntryType().ordinal()][cardType.getPaymentActionType().ordinal()];
			if(list == null) {
				list = new XRefItem();
				cardTypeXref[cardType.getEntryType().ordinal()][cardType.getPaymentActionType().ordinal()] = list;
			}
			list.internalAdd(cardType);
		}
	}

	private CardType(char value, EntryType entryType, PaymentType paymentType, PaymentActionType paymentActionType, String cardTypeName) {
        this.value = value;
        this.entryType = entryType;
        this.paymentType = paymentType;
        this.paymentActionType = paymentActionType;
        this.cardTypeName = cardTypeName;
	}

    public char getValue() {
        return value;
    }

    public EntryType getEntryType() {
		return entryType;
	}
    
    public PaymentType getPaymentType() {
		return paymentType;
	}
    
    public PaymentActionType getPaymentActionType() {
		return paymentActionType;
	}

	public String getCardTypeName() {
			return cardTypeName;
		}

	public static CardType getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
    }

    public static CardType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
    
    public static String getCardTypeNameByValue(char value) {
				try {
					CardType cardType = CardType.getByValue(value);
					return cardType.getCardTypeName();
				} catch (InvalidValueException e) {
					return "Unknown";
				}
    }

	public static List<CardType> findAll(EntryType entryType, PaymentActionType paymentActionType) {
		List<CardType> cardTypes = cardTypeXref[entryType.ordinal()][paymentActionType.ordinal()];
		if (entryType.getFallbackEntryType() == null)
			return cardTypes;
		List<CardType> fallbackCardTypes = cardTypeXref[entryType.getFallbackEntryType().ordinal()][paymentActionType.ordinal()];
		if (fallbackCardTypes == null)
			return cardTypes;
		if (cardTypes == null)
			return fallbackCardTypes;
		List<CardType> allCardTypes = new ArrayList<CardType>(cardTypes);
		for (CardType cardType : fallbackCardTypes) {
			if (!allCardTypes.contains(cardType))
				allCardTypes.add(cardType);
		}
		return allCardTypes;
	}
}
