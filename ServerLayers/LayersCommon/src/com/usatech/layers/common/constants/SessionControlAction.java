/**
 *
 */
package com.usatech.layers.common.constants;

/**
 * @author Brian S. Krug
 *
 */
public enum SessionControlAction {
	START, END, ADD_TRAN_TO_DEVICE_SESSION
}
