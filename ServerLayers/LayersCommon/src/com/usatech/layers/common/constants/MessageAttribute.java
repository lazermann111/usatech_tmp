package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidStringValueException;

import com.usatech.app.Attribute;

public enum MessageAttribute implements Attribute {
    ACTION_TYPE("actionType"),
    AUTH_TIME("authTime"),
	AMOUNT("amount"),
    AUTHORITY_NAME("authorityName"),
	CURRENCY_CD("currencyCd"),
	DISCRETIONARY_DATA("discretionaryData"),
	ENTRY_TYPE("entryType"),
	EXPIRATION_DATE("expirationDate"),
	ISSUE_NUM("issueNum"),	
	MERCHANT_CD("merchantCd"),
	MINOR_CURRENCY_FACTOR("minorCurrencyFactor"),
	MOD_10("mod10"),
	MOD_97("mod97"),
	ONLINE("online"),
	PAN("pan"),
	POS_PTA_ID("posPtaId"),
	REMOTE_SERVER_ADDR("remoteServerAddr"),
	REMOTE_SERVER_ADDR_ALT("remoteServerAddrAlt"),
	REMOTE_SERVER_PORT("remoteServerPort"),
    RESPONSE_TIMEOUT("responseTimeout"),
    SERVICE_TYPE("serviceType"),
    TERMINAL_CD("terminalCd"),
    TERMINAL_ENCRYPT_KEY("terminalEncryptKey"),
    TERMINAL_ENCRYPT_KEY_2("terminalEncryptKey2"),
	TIME_ZONE_GUID("timeZoneGuid"),
	TRACE_NUMBER("traceNumber"),
	TRACK_DATA("trackData"),
	VALIDATION_DATA("validationData"),
	VERIFICATION("verification"),
    ;

    private final String value;
    private static final Map<String, MessageAttribute> attributes = new HashMap<String, MessageAttribute>();
    static {
        for(MessageAttribute item : values()) {
        	attributes.put(item.getValue(), item);
        }
    }
    private MessageAttribute(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static MessageAttribute getByValue(String value) throws InvalidStringValueException {
        if (attributes.containsKey(value))
            return attributes.get(value);
        else
            throw new InvalidStringValueException(value);
    }
}
