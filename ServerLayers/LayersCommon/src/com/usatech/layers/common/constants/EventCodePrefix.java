package com.usatech.layers.common.constants;

import java.util.HashMap;
import java.util.Map;

import simple.lang.InvalidStringValueException;

public enum EventCodePrefix {
    APP_LAYER("A"),
    LEGACY("X"),
    EC("E"),
    VDI("V"),
    UNKNOWN("U"),
    ;

    private final String value;
    private static final Map<String, EventCodePrefix> eventCodePrefixes = new HashMap<String, EventCodePrefix>();
    static {
        for(EventCodePrefix item : values()) {
            eventCodePrefixes.put(item.getValue(), item);
        }
    }
    private EventCodePrefix(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static EventCodePrefix getByValue(String value) throws InvalidStringValueException {
        if (eventCodePrefixes.containsKey(value))
            return eventCodePrefixes.get(value);
        else
            throw new InvalidStringValueException(value);
    }
}
