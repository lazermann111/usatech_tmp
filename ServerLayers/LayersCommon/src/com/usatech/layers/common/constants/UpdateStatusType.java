package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum UpdateStatusType {
    NORMAL(0),
    RETRY(1);

    private final int value;
    private UpdateStatusType(int value) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
    }
    public int getValue() {
        return value;
    }
    protected final static EnumIntValueLookup<UpdateStatusType> lookup = new EnumIntValueLookup<UpdateStatusType>(UpdateStatusType.class);
    public static UpdateStatusType getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
