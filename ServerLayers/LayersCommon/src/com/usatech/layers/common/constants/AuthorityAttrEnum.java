package com.usatech.layers.common.constants;

import com.usatech.app.Attribute;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;



/**
 * This enum defines the attribute names that are used in the construction of the message chain.
 *
 * @author sjillidimudi, bkrug
 *
 */

public enum AuthorityAttrEnum implements Attribute {
    ATTR_ACTION_SUB_TYPE("actionSubType", "Transaction Sub Type. One of: AUTHORIZATION, ADJUSTMENT, SALE, REFUND"),
    ATTR_ACTION_TYPE("actionType", "Transaction Type. One of: SETTLEMENT, SETTLEMENT_RETRY, REVERSAL ADJUSTMENT SALE, REFUND, AUTHORIZATION, TEST, VOID, UPLOAD, TERMINAL_ADD, TERMINAL_UPDATE, TERMINAL_DELETE"),
    ATTR_AMOUNT("amount", "Amount in minor currency (cents for US)"),
    ATTR_AUTH_ACCOUNT_DATA("authAccountData", "From PSS.AUTH.AUTH_PARSED_ACCT_DATA"), // Used by Aramark and Blackboard
    ATTR_AUTH_AUTHORITY_MISC_DATA("authAuthorityMiscData", "For ISO8583 this is either xml that contains: indicator, identifier, validationCode, responseCode, entryMode or a set of name/value pairs of additional transaction processing fields"),
    ATTR_AUTH_AUTHORITY_REF_CD("authAuthorityRefCd", "Retrieval Reference Number"),
    ATTR_AUTH_AUTHORITY_TRAN_CD("authAuthorityTranCd", "Approval Code"),
    ATTR_AUTH_AUTHORITY_TS("authAuthorityTs", "Time that the authority gives on Authorization"),
    ATTR_AUTHORITY_NAME("authorityName", ""),
    ATTR_CONSUMER_ACCT_ID("consumerAcctId", "From PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID"),
    ATTR_CONSUMER_ACCT_TYPE_ID("consumerAcctTypeId", "From PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID"),
    ATTR_CURRENCY_CD("currencyCd", "Currency Code of the transaction"),
    ATTR_DOING_BUSINESS_AS("doingBusinessAs", "Doing Business As"),
    ATTR_ENTRY_TYPE("entryType", " Point of Sale Entry Mode, Required where defined " + "M = Manual Entry via Keyboard/Keypad " + "S = Swiped - Magnetic Stripe " + "R = Contactless Swipe (RFID)"),
    ATTR_ACCT_ENTRY_METHOD_CD("acctEntryMethodCd", "From PSS.ACCT_ENTRY_METHOD.ACCT_ENTRY_METHOD_CD"),
    ATTR_MERCHANT_CD("merchantCd", "Acquirer Id"),
    ATTR_MERCHANT_NAME("merchantName", "Merchant's Name"),
    ATTR_MINOR_CURRENCY_FACTOR("minorCurrencyFactor", "The number to divide the amount by to get it in the unit of the major currency (100 for US since there are 100 cents in 1 dollar)"),
    ATTR_ONLINE("online", ""),
    ATTR_ORIGINAL_AMOUNT("originalAmount", "Amount in minor currency (cents for US)"),
    ATTR_ORIGINAL_TRACE_NUMBER("originalTraceNumber", ""),
    ATTR_PAN_OVERRIDE("panOverride", ""),
    ATTR_POS_PTA_ID("posPtaId", "From PSS.POS_PTA.POS_PTA_ID"),
    ATTR_REMOTE_SERVER_ADDRESS("remoteServerAddr", "The IP or hostname of the server the gateway will hit (for some gateways)"),
    ATTR_REMOTE_SERVER_ALTERNATIVE("remoteServerAddrAlt", "The Alternate IP or hostname of the server the gateway will hit (for some gateways)"),
    ATTR_REMOTE_SERVER_PORT("remoteServerPort", "The post on the server the gateway will hit (for some gateways)"),
    ATTR_RESPONSE_TIME_OUT("responseTimeout", ""),
    ATTR_SERVICE_TYPE("serviceType", "From AUTHORITY.AUTHORITY_SERVICE_TYPE. AUTHORITY_SERVICE_TYPE_ID"),
    ATTR_TERMINAL_BATCH_NUM("terminalBatchNum", "From PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM"),
    ATTR_DEVICE_SERIAL_CD("deviceSerialCd", "Device Serial Code (From DEVICE.DEVICE.DEVICE_SERIAL_CD)"),
    ATTR_TERMINAL_CD("terminalCd", "Terminal Id (From PSS.TERMINAL.TERMINAL_CD)"),
    ATTR_TERMINAL_ENCRYPTION_KEY("terminalEncryptKey", ""),
    ATTR_TERMINAL_ENCRYPTION_KEY2("terminalEncryptKey2", ""),
    ATTR_TIMEZONE_GUID("timeZoneGuid", "Time Zone Code (from Java's TimeZone.getTimeZone()) of the Device"),
    ATTR_TRACE_NUMBER("traceNumber", "Either from PSS.AUTH.TRACE_NUMVER or, PSS.AUTH.AUTH_ID. If trace_num exists use it if not user auth_id."),
    ATTR_VALIDATION_DATA("validationData", "Format is: key1=value1|key2=value2| Possible keys include: address, zip, cvv2, pin"),
    ATTR_AUTHORITY_RESPONSE_CD("authorityRespCd", ""),
    ATTR_AUTHORITY_RESPONSE_DESC("authorityRespDesc", ""),
    ATTR_AUTH_RESULT_CD("authResultCd", ""),
    ATTR_CARD_TYPE("cardType", "Upper case and trimmed version of the Authority Assn Name"),
    ATTR_VOID_ALLOWED("voidAllowed", "Whether or not void of sale is allowed"),
    ATTR_PARTIAL_AUTH_ALLOWED("allowPartialAuth", "Whether or not a partial authorization is allowed"),
    ATTR_BALANCE_CHECK_ONLY("balanceCheckOnly", "Whether or not a balance check is requested"),
    ATTR_ADD_AUTH_HOLD_DAYS("addAuthHoldDays", "Number of days an auth hold lasts (leave null or 0 to not add an auth hold)"),
    ATTR_ALLOW_CVV_MISMATCH("allowCVVMismatch", "Whether to allow CVV mismatch auth result code"),
    ATTR_ALLOW_AVS_MISMATCH("allowAVSMismatch", "Whether to allow AVS mismatch auth result code"),
    ATTR_AUTH_HOLD_USED("authHoldUsed", "Whether an auth hold was used"),
    ATTR_DENIED_REASON("deniedReason", "The DeniedReason (enum) for why the auth was declined or failed"),
    ATTR_MERCHANT_CATEGORY_CODE("mcc", "The merchant category code for the device"),
    ATTR_ADDRESS("address", "Address where the sale occurred"),
    ATTR_CITY("city", "City where the sale occurred"),
    ATTR_STATE_CD("stateCd", "State abbreviation where the sale occurred"),
    ATTR_POSTAL("postal", "Postal or zip where the sale occurred"),
    ATTR_COUNTRY_CD("countryCd", "Country code where the sale occurred"),
    ATTR_LOCATION_NAME("locationName", "Device location name"),
    ATTR_LONGITUDE("longitude", "Longitude where the sale occurred"), 
    ATTR_LATITUDE("latitude", "Latitude where the sale occurred"), 
    ATTR_GEOCODE_COUNTRY("geocodeCountryCd", "Geocode Country code where the sale occurred"),
    ATTR_GEOCODE_TIME("geocodeTime", "Time that the longitude and latitude was read"),
    ATTR_AUTH_ADJUSTED("authAdjusted", "Whether or not a partial authorization reversal was performed"),
    ATTR_PREPAID_FLAG("prepaidFlag", "Whether the card was detected to be a prepaid card"),
    
    // New Codes
    ATTR_CREDIT_SALE_COUNT("creditSaleCount", "No of Sales"),
    ATTR_CREDIT_SALE_AMOUNT("creditSaleAmount", "Total Sale Amount"),
    ATTR_CREDIT_REFUND_COUNT("creditRefundCount", "No of Refunds"),
    ATTR_CREDIT_REFUND_AMOUNT("creditRefundAmount", "Total Refund Amount"),

    // New Codes added to handle Sale Transaction processing
    ATTR_POS_ENVIRONMENT("posEnvironment", "Point of Sale Environment: See , Required where defined #A = Attended (Retail) #M = Mail/Telephone Order"),
    ATTR_PIN_CAPABILITY("pinCapability", "Whether the POS is capable of pin entry"),
    ATTR_ENTRY_CAPABILITY("entryCapability", "What entry types the POS is capable of"),
    ATTR_INVOICE_NUMBER("invoiceNumber", "An invoice number"),
    
    ATTR_TRACK_DATA("trackData", "Magstripe Data from Swipe or RFID, Required where defined, Can be either Track1 or Track2 data"),
    ATTR_TRACK_DATA_OVERRIDE("trackDataOverride", "Overridden Magstripe Data from Swipe or RFID, Required where defined, Can be either Track1 or Track2 data"),
    // For Sale processing
    ATTR_AUTH_TIME("authTime", "Auth Time"),
    // NOTE: The following attribute is terminalCd for those authorities that use it (AquaFill) and is not needed:
    // ATTR_ACCOUNT("account", "This takes the value of pp.pos_pta_device_serial_cd"),
    
    ATTR_ADDITIONAL_INFO_TYPE("additionalInfoType", "Type of Additional Info"),
    ATTR_ADDITIONAL_INFO("additionalInfo", "Additional Info"),
    ATTR_CONSUMER_ACCT_CD_HASH("consumerAcctCdHash", "Unsalted PAN hash"),
    ATTR_CONSUMER_IDENTIFIER("consumerIdentifier", "Consumer identifier such as Softcard Consumer ID"),
    ATTR_NO_CONVENIENCE_FEE("noConvenienceFee", "No Two-Tier Pricing for this transaction"),

    ATTR_TRAN_COUNT("tran.count", "The number of transactions included in a terminal capture settlement"),
    
    ATTR_ITEM_COUNT("itemCount", "Count of line items"),

	ATTR_CUSTOMER_SERVICE_PHONE("csPhone", "The telephone number to call for C/S concerns"),
	ATTR_CUSTOMER_SERVICE_EMAIL("csEmail", "The email for C/S concerns"),
	ATTR_CUSTOMER_ID("customerId", "From CORP.CUSTOMER.CUSTOMER_ID"),
	ATTR_SALE_UPLOAD_TIME("saleUpdateTs", "Time that the device uploaded the sale"),

    // For Black Board
    ATTR_PIN("pin", ""),

    // For Internal
    ATTR_TRAN_ID("tranId", "From PSS.TRAN.TRAN_ID"),
    ATTR_AUTH_ID("authId", "From PSS.AUTH.AUTH_ID"),
    ATTR_PARENT_TRAN_ID("parentTranId", "From PSS.TRAN.PARENT_TRAN_ID"),
    
    // output
    ATTR_START_TIME("startTime", "Time that task started processing the message"),
    ATTR_END_TIME("endTime", "Time that task finished processing the message"),

    ATTR_AUTH_AMOUNT("authAmount", "The amount requested by the device"),
    ATTR_AMT_APPROVED("approvedAmount", "The amount for which the Authority is approved"),
    ATTR_SETTLED("settled","This attribute holds the settlement status of the transaction. "),
    ATTR_BALANCE_AMOUNT("balanceAmount","The balance amount that is left in the card. Applicable only for few cards."),
    ATTR_REPLENISH_BALANCE_AMOUNT("replenishBalanceAmount", "The balance amount that is left after replenishing the account."),
    ATTR_PAYMENT_TYPE("paymentType", "The card type."),
    ATTR_CONSUMER_ID("consumerId", "The PSS.CONSUMER.CONSUMER_ID"),
    ATTR_FREE_TRANSACTION("freeTransaction", "Whether the transaction should be free"),
    ATTR_ON_HOLD_AMOUNT("onHoldAmount", "The amount on hold"),
    ATTR_TRAN_IMPORT_NEEDED("tranImportNeeded", "If transaction needs to be imported into Reporting"),
    
    //For settlement update
    ATTR_PAYMENT_SUBTYPE_KEY_ID("paymentSubtypeKeyId", "From PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID (and also PSS.TERMINAL.TERMINAL_ID for generic authorities)"),
    ATTR_PAYMENT_SUBTYPE_CLASS("paymentSubtypeClass", "From PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS"),
    ATTR_TERMINAL_BATCH_IDS("terminalBatchIds", "Array of PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID"),
    ATTR_TRAN_IDS("tranIds", "Array of PSS.TRAN.TRAN_ID"),
    ATTR_TERMINAL_BATCH_ID("terminalBatchId", "From PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID"),
    ATTR_SETTLEMENT_BATCH_ID("settlementBatchId", "From PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID"),
    ATTR_GLOBAL_TOKEN_CD("globalTokenCd", "From PSS.TERMINAL.GLOBAL_TOKEN_CD"),
    ATTR_NEW_GLOBAL_TOKEN_CD("newGlobalTokenCd", "From PSS.TERMINAL.GLOBAL_TOKEN_CD"),
    ATTR_LAST_ATTEMPT("lastAttempt", "Is this the last retry?"),
    ATTR_PRIOR_ATTEMPTS("priorAttempts", "Number of prior attempts"),
    ATTR_TERMINAL_CAPTURE_FLAG("terminalCaptureFlag", "From PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG"),
    ATTR_FORCE_REASON("forceReason", "Explanation of why batch or tran is forced"),
    ATTR_UNIQUE_SECONDS("uniqueSeconds", "A unique number that is close to the seconds past epoch"),

    //For sale update
    ATTR_SALE_PHASE("salePhaseCd", "Is the sale/refund intended, actual, or reversal (con.usatech.posm.constants.SalePhase"),
    ATTR_AUTH_TYPE_CD("authTypeCd", "From PSS.AUTH.AUTH_TYPE_CD"),
    ATTR_TRAN_TYPE_CD("tranTypeCd", "Either '+' for sales or '-' for refunds"),
    ATTR_SALE_AUTH_ID("saleAuthId", "From PSS.AUTH.AUTH_ID of the sale record"),
    ATTR_REFUND_ID("refundId", "From PSS.REFUND.REFUND_ID"),
    ATTR_TERMINAL_ID("terminalId", "From PSS.TERMINAL.TERMINAL_ID"),
    ATTR_AUTHORITY_START_TIME("authorityStartTime", "Time that authority started processing message"),
    ATTR_AUTHORITY_END_TIME("authorityEndTime", "Time that authority finished processing message"),
    ATTR_CLIENT_TEXT("clientText","Text to send to the client"),
    ATTR_CLIENT_TEXT_KEY("~clientText","Key of the translation of the text to send to the client"),
    ATTR_CLIENT_TEXT_PARAMS("@clientText","The parameters for the translation of the text to send to the client"),
    ATTR_OVERRIDE_CLIENT_TEXT_KEY("~overrideClientText","Key of the translation of the text to send to the client"),
    ATTR_OVERRIDE_CLIENT_TEXT_PARAMS("@overrideClientText","The parameters for the translation of the text to send to the client"),
    
    // For terminal capture resource
    ATTR_CAPTURE_DETAILS_RESOURCE("captureDetailsResource", "The resource key of the sale details"),
    ATTR_CAPTURE_DETAILS_ENCRYPTION_KEY("captureDetailsKey", "The encryption key for the resource with sale details"),
    ATTR_CAPTURE_DETAILS_CIPHER("captureDetailsCipher", "The cipher (transformation) name for decrypting the sale details"),
    ATTR_CAPTURE_DETAILS_BLOCK_SIZE("captureDetailsBlockSize", "The cipher block size for decrypting the sale details"),
    
    ATTR_FEEDBACK_TYPE("feedbackType", "If the feedback is batch level or transaction level"),
    ATTR_FEEDBACK_RESOURCE("feedbackResource", "The resource key of the settlement feedback data"),
    ATTR_FEEDBACK_PROCESS_QUEUE("feedbackProcessQueue", "The queue key of the settlement feedback processor"),
    ATTR_IN_MAJOR_CURRENCY("majorCurrencyUsed", "If the amounts are in major currency units instead of minor"),

	// For KMS
	ATTR_CARD_KEY("cardKey", "The KeyManager Card Key"),
	ATTR_ENCRYPT_KEY_LIST("encryptKey", "The KeyManager Encrypt Key List"),
	ATTR_STORE_EXPIRATION_TIME("storeExpirationTime", "Time that the stored card expires in KeyManager"),
	ATTR_EXCLUDE_INSTANCES("excludeInstances", "Instances to exclude from multi-instance queue list"),
	
	// for authorize
	ATTR_ACCOUNT_DATA("accountData", "The decrypted track data"),
	ATTR_ACCOUNT_DATA_1("accountData1", "The decrypted track 1 data"),
	ATTR_ACCOUNT_DATA_2("accountData2", "The decrypted track 2 data"),
	ATTR_ACCOUNT_DATA_3("accountData3", "The decrypted track 3 data"),
	
	ATTR_GLOBAL_ACCOUNT_ID("globalAccountId", "The global account id of the account data"),
	ATTR_AUTH_REPLY_CARD_ID("authReplyCardId", "The auth reply card id of the account data"),
	ATTR_TRACK_CRC_MATCH("trackCrcMatch", "Whether the stored CRC matches that of the account data"),
	ATTR_ACCOUNT_CD_HASH("accountCdHash", "The hash of the card number"),
	ATTR_TRACK_CRC("trackCrc", "The calculated CRC of the track data"),
	ATTR_INSTANCE("instance", "The ordinal of the instance"),
	ATTR_AUTH_TOKEN("authToken", "The token to authorize"),
	ATTR_DEVICE_TRAN_CD("deviceTranCd", "The device tran cd (transaction id)"),
    ATTR_FORCE_TOKENIZATION("forceTokenize", "Force tokenization process"),
	
	ATTR_OLD_GLOBAL_ACCOUNT_ID("oldGlobalAccountId", "The former global account id of the account data"),
	ATTR_OLD_INSTANCE("oldInstance", "The former instance of the account data"),

	// for loyalty
	ATTR_CONSUMER_ACCT_TOKEN("consumerAcctToken", "From pss.consumer_acct.consumer_acct_token"),
	ATTR_CONSUMER_EMAIL("consumerEmail", "From pss.consumer_acct.consumer_email_addr1"),
	ATTR_SPROUT_VENDOR_ID("sproutVendorId", "From corp.sprout_vendor"),
	
	//for settlement
	ATTR_JOB_ID("jobId", "The Quartz Job Id"),
	ATTR_NEW_IND_DB_MOD_TIME("newIndDbModTime", "New time of file modification"),
	ATTR_FILE_KEY("file.fileKey", "The resource key of the file"),
	
	// for submerchants/MATCH
	ATTR_SUBMERCHANTS_UPLOAD_FULL("submerchantsUploadType", "true for FULL, false for changes-only"),
	ATTR_SUBMERCHANTS_UPLOAD_TS("submerchantsUploadTs", "Timestamp submerchants file was generated"),
	ATTR_SUBMERCHANTS_UPLOAD_FILENAME("submerchantsUploadFilename", "Filename of the submerchants file that was generated"),
	ATTR_SUBMERCHANTS_UPLOAD_RECORD_COUNT("submerchantsUploadRecordCount", "Number of submerchants records in the file that was generated"),
	
	    // emv parameter download
    ATTR_SYSTEM_TRACE_AUDIT_NUM("systemTraceAuditNum", "From PSS.EMV_PARAM_DOWNLOAD_REQD.SYSTEM_TRACE_AUDIT_NUMBER"),
	ATTR_EMV_PARM_DNDL_ID("emvParmDnldId", "emv parameter download id"),
	ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP("emvParameterDwnlReqdTimestamp", "Timestamp when the EMV parameter download required bit was set on auth or sale"),

	//vas related
    ATTR_OLD_VAS_ENCRYPTED_DATA("vasOldEncryptedData", "Encrypted data from VAS transaction"),
	ATTR_VAS_ENCRYPTED_DATA("vasEncryptedData", "Encrypted data from VAS transaction"),
    ATTR_VAS_PASS_TYPE_ID("passTypeId", "passTypeId which is non sha-256 of the merchant id for Apple VAS"),
	ATTR_VAS_PASS_MERCHANT_ID("passMerchantId", "passMerchantId is a sha-256 of the passTypeId for Apple VAS"),
	
	//USAT-449
  ATTR_OFFLINE("offline", " "),
    ;
    private final String value;
    private final String description;

    protected final static EnumStringValueLookup<AuthorityAttrEnum> lookup = new EnumStringValueLookup<AuthorityAttrEnum>(AuthorityAttrEnum.class);
    public static AuthorityAttrEnum getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private AuthorityAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}
