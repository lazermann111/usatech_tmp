package com.usatech.layers.common.constants;

import java.util.Set;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum EntryCapability {
	KEYED('M', "Keyed"),
    SWIPE('S', "Swipe"),
    PROXIMITY('P', "Proximity"),
    EMV_CONTACT('C', "EMV Contact"),
    EMV_PROXIMITY('E', "EMV Proximity"),
    MICR('I', "MICR"),
    SIGNATURE('X', "Electronic Signature Capture"),
    BLUETOOTH('B', "BlueTooth"),
    ;

    private final char value;
    private final String description;
	protected final static EnumCharValueLookup<EntryCapability> lookup = new EnumCharValueLookup<EntryCapability>(EntryCapability.class);

	private EntryCapability(char value, String description) {
        this.value = value;
        this.description = description;
	}
    public char getValue() {
        return value;
    }
    public static EntryCapability getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
    }
    public static EntryCapability getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
	public String getDescription() {
		return description;
	}
	
	public static boolean isCapable(String capabilityList, EntryCapability... capabilityTests) {
		if(capabilityTests == null || capabilityTests.length == 0)
			return true;
		if(capabilityList == null || capabilityList.length() == 0)
			return false;
		for(EntryCapability c : capabilityTests)
			if(capabilityList.indexOf(c.getValue()) < 0)
				return false;
		return true;
	}
	
	public static boolean isCapable(Set<EntryCapability> entryCapabilities, EntryType entryType) {
		switch(entryType) {
			case MANUAL:
				return entryCapabilities.contains(EntryCapability.KEYED);
			case SWIPE:
				return entryCapabilities.contains(EntryCapability.SWIPE);
			case CONTACTLESS:
			case ISIS:
				return entryCapabilities.contains(EntryCapability.PROXIMITY);
			case EMV_CONTACTLESS:
				return entryCapabilities.contains(EntryCapability.EMV_PROXIMITY);
			case EMV_CONTACT:
				return entryCapabilities.contains(EntryCapability.EMV_CONTACT);
			case BLUETOOTH:
				return entryCapabilities.contains(EntryCapability.BLUETOOTH);
			case CARD_ID:
			case CARD_ID_CREDENTIALS:
			case TOKEN:
				return true;
			case BAR_CODE:
			case CASH:
			case PHONE:
			case UNSPECIFIED:
			default:
				if (entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
					return entryCapabilities.contains(EntryCapability.PROXIMITY);
				return false;
		}
	}
}
