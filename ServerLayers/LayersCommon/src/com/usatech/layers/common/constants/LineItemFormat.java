package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum LineItemFormat {
    FORMAT_0((byte)0);

    private final byte value;
    private LineItemFormat(byte value) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<LineItemFormat> lookup = new EnumByteValueLookup<LineItemFormat>(LineItemFormat.class);
    public static LineItemFormat getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
