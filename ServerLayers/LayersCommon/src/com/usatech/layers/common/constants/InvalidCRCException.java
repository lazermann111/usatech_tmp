/**
 *
 */
package com.usatech.layers.common.constants;

import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class InvalidCRCException extends Exception {
	private static final long serialVersionUID = 6833753406353213650L;

	public InvalidCRCException(CRCType type, byte[] expected, byte[] calculated) {
		super("Invalid " + type.getDescription() + " : expected " + StringUtils.toHex(expected) + " but calculated value was " + StringUtils.toHex(calculated));
	}
}
