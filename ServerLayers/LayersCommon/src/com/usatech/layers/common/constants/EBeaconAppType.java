package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum EBeaconAppType {
    MORE_MOBILE_APP((byte)0),
    BYNDL_MOBILE_APP((byte)1),
    ;
	private static final EnumByteValueLookup<EBeaconAppType> lookup = new EnumByteValueLookup<EBeaconAppType>(EBeaconAppType.class);

    private final byte value;

    private EBeaconAppType(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    public static EBeaconAppType getByValue(byte value) throws InvalidByteValueException {
		return lookup.getByValue(value);
    }
}
