package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

public enum AFAdditionalDataType {
    NO_ADDITIONAL_DATA((byte)0, null),
    BEHAVIOR_BITMAP((byte)1, "HEX");

    private final byte value;
    private final String displayFormat;
    private AFAdditionalDataType(byte value, String displayFormat) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
        this.displayFormat = displayFormat;
    }
    public byte getValue() {
        return value;
    }
    public String getDisplayFormat() {
    	return displayFormat;
    }
    public String getReadableAdditionalData(String additionalData) {
    	if (additionalData == null)
    		return null;
    	if ("HEX".equalsIgnoreCase(displayFormat))
    		return StringUtils.toHex(additionalData.getBytes());
    	else
    		return additionalData;
    }
    protected final static EnumByteValueLookup<AFAdditionalDataType> lookup = new EnumByteValueLookup<AFAdditionalDataType>(AFAdditionalDataType.class);
    public static AFAdditionalDataType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
