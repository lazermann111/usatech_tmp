/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum FileType {
	DEX_FILE(0, ""),
	CONFIGURATION_FILE(1, ""),
	GX_PEEK_RESPONSE(2, ""),
	DEX_FILE_FOR_FILL_TO_FILL(3, "DEX File for Fill to Fill Reconciliation (this is sent when the fill button is pressed)"),
	ESUDS_GENERIC_FILE(4, ""),
	APPLICATION_SOFTWARE_UPGRADE(5, "Application Software Upgrade (Note: Application must be restarted after upgrade)"),
	G4_CONFIGURATION_DATA_TEMPLATE(6, ""),
	EXECUTABLE_FILE(7, ""),
	EXECUTABLE_FILE_THAT_REQUIRES_APP_RESTART(8, ""),
	LOG_FILE(9, ""),
	GENERIC_DATA_FILE_FROM_EZ80_EPORT(10, ""),
	DEX_FILE_FROM_EZ80_EPORT(11, ""),
	AUTO_EMAIL(12, "Auto Email (Note; Email addresses will apprear on the first line of the file transfer and will be separated with a comma [,])"),
	BOOTLOADER(13, ""),
	MULTIPLEXOR_APPLICATION(14, ""),
	KIOSK_CONFIGURATION_TEMPLATE(15, ""),
	KIOSK_GENERIC_FILE(16, ""),
	MEMORY_MAP_CSV(17, ""),
	GENERIC_BINARY_DATA(18, ""),
	PROPERTY_LIST(19, ""),
	PROPERTY_LIST_REQUEST(20, ""),
	MESSAGE_SET(21, ""),
	EDGE_DEFAULT_CONFIGURATION_TEMPLATE(22, ""),
	EDGE_CUSTOM_CONFIGURATION_TEMPLATE(23, ""),
	DEX_FILE_FOR_DEVICE_DOWNLOAD_AUTOMATIC(24, ""),
	DEX_FILE_FOR_DEVICE_DOWNLOAD_MANUAL(25, ""),
	CUSTOM_FILE_UPLOAD(26, "The file is sent by the device and we will forward onto the customer"),
	CARD_READER_APPLICATION_FIRMWARE(27, "Card reader firmware upgrade file"),
	ISIS_SMARTTAP_CONFIGURATION(28, "Binary file with Softcard SmartTap configuration settings"),
	GENERIC_DEVICE_INFORMATION(29, "Information file uploaded by devices with info type in the first line: DEVICE, MODEM, etc. and name/value pairs with one name/value pair per line"),
	CUSTOMER_WEBSITE_FILE(30, "Customer Website File"),
	COMPOSITE_UPGRADE(31, "Composite Upgrade"),
	NACHA_ACH_CCD_FILE(32, "NACHA ACH CCD File"),
	EMV_PARAMETERS(33, "EMV parameter file sent to device."),
	MODEM_FOTA_COMMAND(34, "Modem FOTA Command"),
	BEZEL_PROFILE(35, "Bezel Profile"),
	CARD_READER_EMV_PARAMETER_FILE(36, "Card Reader EMV Parameter File"),
	INVALID_FILE(101, ""),

	/*
	SELECT name || '(' || value ||', "' || description || '"),'
	 FROM (
	select ft.file_transfer_type_Cd value,
	  REPLACE(REPLACE( REPLACE(REPLACE(UPPER(ft.file_transfer_type_name), ' ', '_'), '/', '_'), '(', ''), ')', '') name,
	  ft.file_transfer_type_desc description
	 from device.file_transfer_type ft)
	 order by value;
	 */
	;

	private final int value;
    private final String description;
    private FileType(int value, String description) {
        this.value = value;
        this.description = description;
    }
    public int getValue() {
        return value;
    }
    public String getDescription() {
        return description;
    }
	protected final static EnumIntValueLookup<FileType> lookup = new EnumIntValueLookup<FileType>(FileType.class);
    public static FileType getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
