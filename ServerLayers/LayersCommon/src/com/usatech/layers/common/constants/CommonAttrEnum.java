package com.usatech.layers.common.constants;

import com.usatech.app.Attribute;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;



/**
 * This enum defines the attribute names that are used in the construction of the message chain.
 *
 * @author bkrug
 *
 */

public enum CommonAttrEnum implements Attribute {
	// Common interim attributes
	ATTR_CARD_READER_TYPE_ID("cardReaderTypeId", "The Id of Card Reader Type"),
	ATTR_KEY_ID("keyId", "The Key Id for decryption"),
    ATTR_KEY_SERIAL_NUM("keySerialNum", "The Key Serial Number for decryption"),
    ATTR_ENCRYPTED_DATA("encryptedData", "The encrypted data in bytes. This also serves as the prefix for a set of attributes 'encrypteData.0...n' to be decrypted"),
    ATTR_ENCRYPTED_DATA_COUNT("encryptedDataCount", "The number of pieces of data that should be decrypted"),
    ATTR_LOOKUP_DATA_COUNT("lookupDataCount", "The number of pieces of data that should be looked up"),
    ATTR_ENCRYPTED_VAS_DATA_COUNT("encryptedVasDataCount", "The number of pieces of VAS data that should be decrypted"),
    ATTR_DECRYPTED_DATA("decryptedData", "The decrypted data in bytes. This also serves as the prefix for a set of attributes 'decrypteData.0...n' that were decrypted"),
    ATTR_PASS_IDENTIFIER("passIdentifier", "Apple VAS pass identifier"),
    ATTR_DECRYPTED_DATA_OVERRIDE("decryptedDataOverride", "The overridden decrypted data in bytes. This also serves as the prefix for a set of attributes 'decrypteData.0...n' that were decrypted"),
    ATTR_DECRYPTED_ENCODING("decryptedDataEncoding", "The charset encoding to use to turn the resultant bytes into a String. If not provided, then the raw bytes are returned"),
    ATTR_CIPHER_NAME("cipherName", "The cryptographic transformation's name to use (in the form 'algorithm/encoding/padding'"),
    ATTR_VALIDATION_REGEX("validationRegex", "A regular expression against which to validate the decrypted data"),
    ATTR_ENTRY_COUNT("entryCount", "The number 'rows' of encrypted data"),
    ATTR_LANGUAGE("language", "Device language"),
    
    // For mass decrypt resource
    ATTR_CRYPTO_RESOURCE("cryptoResource", "The resource key of the encrypted or decrypted data"),
    ATTR_CRYPTO_ENCRYPTION_KEY("cryptoKey", "The encryption key for the resource with the encrypted or decrypted data"),
    ATTR_CRYPTO_CIPHER("cryptoCipher", "The cipher (transformation) name for decrypting the encrypted or decrypted data"),
    ATTR_CRYPTO_BLOCK_SIZE("cryptoBlockSize", "The cipher block size for decrypting the encrypted or decrypted data"),   
    
    ATTR_PROCESS_ID("processId", "The process identifier (typically hostname@pid)"),
    
    ATTR_REPLY("reply", "The reply bytes for the device message"),
    ATTR_REPLY_TEMPLATE("replyTemplate", "The reply template bytes for the device message"),
    ATTR_REPLY_TEMPLATE_PARTS("replyTemplate/n", "The number of replyTemplate parts"),
    ATTR_END_SESSION("endSession", "Whether to end the current session after the response or not"),
    ATTR_SESSION_ID("sessionId", "The netlayer session id"),
    ATTR_REPLY_TIME("replyTime", "The time in ms that the reply was sent to the device"),
    ATTR_SENT_TO_DEVICE("sentToDevice", "true if message was possibly sent to the device"),
    ATTR_CREDENTIAL_RESULT("credentialResult", "The CredentialResult from authorization"),
    
    ATTR_RESOURCE("resourceKey", "The resource key"),
    ATTR_RESOURCE_MODIFIED_TIME("resourceModifiedTime", "The time the resource was modified"),
    ATTR_RESOURCE_LENGTH("resourceLength", "The number of bytes in the resource"),
    ATTR_SENSITIVE("sensitive", "Whether the data needs encryption"),
    ATTR_URLS("urls", "The urls"),
    ATTR_HOST("host", "The host name or IP"),
    ATTR_PORT("port", "The port"),
    ATTR_FILE_PATH("filePath", "The file path"),
    ATTR_FILE_NAME("fileName", "Name of the file"),
	ATTR_USERNAME("username", "The username"),
    ATTR_PASSWORD("password", "The password"),
    ATTR_ERROR("error", "The error message"),    
    ATTR_FILE_RENAME_MATCH("fileRenameMatch", "The regular expression to match the file name for renaming it"),
    ATTR_FILE_RENAME_REPLACE("fileRenameReplace", "The replace string for file rename"),
    ATTR_FILE_TYPE("fileType", "The file type"),
    ATTR_FILE_ID("fileId", "The file id"),
    ATTR_WILDCARD("wildcard", "Whether to use wildcard matching to find files"),
    ATTR_COMPRESS("compress", "Whether to compress the files"),
    ATTR_PROCESS_TIME("processTime", "Timestamp of processing"),
    
    ATTR_METHODS("methods", "The http methods"),
    ATTR_VALIDATION_LENGTH("validationLength", "The number of bytes to read to validate the result"),
    
    ATTR_EFT_ID("eftId", "The EFT ID"),
    ATTR_SKIP_ON_SAME_INSTANCE("skipSameInstance", "Whether to skip an update if instance is the same as the processing app"),
    
	ATTR_TRANSFERS("transfers", "A map of attribute transfers"),
	ATTR_REFERENCED_STEP("referencedStep", "The step whose result attributes will be updated"),
	ATTR_REFERENCED_RESULT_ATTRIBUTE("referencedAttribute", "The result attribute that will be updated"),
	ATTR_TRANSFER_VALUE_ATTRIBUTE("transferValue", "The attribute that holds the value to use in updating"),
	
	ATTR_APP_SESSION_KEY("appSessionKey", "The session key for the app"),

	ATTR_CREATE("create", "Whether to create when missing"),
	ATTR_USER_ID("userId", "The user id"),
	ATTR_ITEM_ID("itemId", "The item id"),
	ATTR_SKIP_EMPTY("skipEmpty", "Whether to skip empty files"),
    ATTR_OBJECT("object", "Object"),
  ATTR_INTERNAL_MESSAGE("internalMessage", "Indicates an internal message as opposed to a message received via an external API such as Quick Connect"),

    ATTR_ERROR_CODE("errorCode", "Error code"),
    ATTR_ERROR_MESSAGE("errorMessage", "Error message"),
	
    ;
    private final String value;
    private final String description;

    protected final static EnumStringValueLookup<CommonAttrEnum> lookup = new EnumStringValueLookup<CommonAttrEnum>(CommonAttrEnum.class);
    public static CommonAttrEnum getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private CommonAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}
