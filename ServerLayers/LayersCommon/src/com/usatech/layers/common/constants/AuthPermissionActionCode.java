package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * Auth permission type action codes.
 * @author yhe
 *
 */
public enum AuthPermissionActionCode {
	DO_NOTHING((byte)0),
	SHOW_ALL_PERMANENT_COUNTERS((byte)1),
	SHOW_ALL_INTERNAL_COUNTERS((byte)2),
	SHOW_PERMANENT_CASHLESS_COUNTERS((byte)3),
	SHOW_INTERNAL_CASHLESS_COUNTERS((byte)4),
	DRIVER_CARD((byte)5),
	//ACTIVATE_CLIENT((byte)6),
	//DEACTIVATE_CLIENT((byte)7),
	FORCED_CARD((byte)8),
	AUDIT_CARD((byte)9),
	SHOW_PREVIOUS((byte)10),
	READ_DEX_WITH_DIAG_CODES((byte)12),
	SHOW_RSSI((byte)13),
	SHOW_SERVICE_MENU((byte)14),
	;
	private final byte value;
	private AuthPermissionActionCode(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<AuthPermissionActionCode> lookup = new EnumByteValueLookup<AuthPermissionActionCode>(AuthPermissionActionCode.class);
    public static AuthPermissionActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
    public static AuthPermissionActionCode getByValueSafely(byte value) {
    	return lookup.getByValueSafely(value);
    }
}
