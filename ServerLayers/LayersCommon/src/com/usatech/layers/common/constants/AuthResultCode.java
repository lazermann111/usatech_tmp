package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.ec.ECResponse;

public enum AuthResultCode {
	APPROVED('Y', "Approved", AuthResponseCodeC3.SUCCESS, AuthResponseCodeAF.APPROVED, AuthResponseCodeA1.APPROVED, AuthResponseCode60.PASS, ECResponse.RES_APPROVED, ECResponse.RES_APPROVED, ECResponse.RES_OK, PermissionResponseCodeC3.SUCCESS, PermissionResponseCodeAB.SUCCESS),
	DECLINED('N', "Declined", AuthResponseCodeC3.DECLINE, AuthResponseCodeAF.DENIED, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_DECLINED, ECResponse.RES_FAILED, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.DECLINE),
	PARTIAL('P', "Partially Approved", AuthResponseCodeC3.CONDITIONAL_SUCCESS, AuthResponseCodeAF.CONDITIONALLY_APPROVED, AuthResponseCodeA1.CONDITIONALLY_APPROVED, AuthResponseCode60.FAIL, ECResponse.RES_PARTIALLY_APPROVED, ECResponse.RES_PARTIALLY_APPROVED, ECResponse.RES_OK, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.CONDITIONAL_SUCCESS),
	FAILED('F', "Failed", AuthResponseCodeC3.FAILURE, AuthResponseCodeAF.FAILED, AuthResponseCodeA1.FAILED, AuthResponseCode60.FAIL, ECResponse.RES_FAILED, ECResponse.RES_FAILED, ECResponse.RES_FAILED, PermissionResponseCodeC3.FAILURE, PermissionResponseCodeAB.FAILURE),
	DECLINED_PERMANENT('O', "Declined - Permanent", AuthResponseCodeC3.DECLINE_PERMANENT, AuthResponseCodeAF.DENIED, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_DECLINED, ECResponse.RES_FAILED, PermissionResponseCodeC3.DECLINE_PERMANENT, PermissionResponseCodeAB.DECLINE_PERMANENT),
	DECLINED_PAYMENT_METHOD('R', "Declined - Restricted", AuthResponseCodeC3.DECLINE_RESTRICTED, AuthResponseCodeAF.DENIED_PAYMENT_METHOD, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_RESTRICTED_PAYMENT_METHOD, ECResponse.RES_FAILED, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.DECLINE),
	DECLINED_DEBIT('D', "Declined - Debit", AuthResponseCodeC3.DECLINE_RESTRICTED, AuthResponseCodeAF.DENIED_PAYMENT_METHOD, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_RESTRICTED_DEBIT_CARD_TYPE, ECResponse.RES_FAILED, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.DECLINE),
	CVV_MISMATCH('C', "CVV Mis-match", AuthResponseCodeC3.DECLINE, AuthResponseCodeAF.DENIED, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_CVV_MISMATCH, ECResponse.RES_CVV_MISMATCH, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.DECLINE),
	AVS_MISMATCH('V', "AVS Mis-match", AuthResponseCodeC3.DECLINE, AuthResponseCodeAF.DENIED, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_AVS_MISMATCH, ECResponse.RES_AVS_MISMATCH, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.DECLINE),
	CVV_AND_AVS_MISMATCH('M', "CVV and AVS Mis-match", AuthResponseCodeC3.DECLINE, AuthResponseCodeAF.DENIED, AuthResponseCodeA1.DENIED, AuthResponseCode60.FAIL, ECResponse.RES_DECLINED, ECResponse.RES_CVV_AND_AVS_MISMATCH, ECResponse.RES_CVV_AND_AVS_MISMATCH, PermissionResponseCodeC3.DECLINE, PermissionResponseCodeAB.DECLINE);

	private final char value;
	private final String description;
	private final AuthResponseCodeC3 authResponseCodeC3;
	private final AuthResponseCodeAF authResponseCodeAF;
	private final AuthResponseCodeA1 authResponseCodeA1;
	private final AuthResponseCode60 authResponseCode60;
	private final int authResponseCodeEC1;
	private final int authResponseCodeEC2;
	private final int cardInfoResponseCodeEC2;
	private final PermissionResponseCodeC3 permissionResponseCodeC3;
	private final PermissionResponseCodeAB permissionResponseCodeAB;
	private static final EnumCharValueLookup<AuthResultCode> lookup = new EnumCharValueLookup<AuthResultCode>(AuthResultCode.class);

	private AuthResultCode(char value, String description, AuthResponseCodeC3 authResponseCodeC3, AuthResponseCodeAF authResponseCodeAF, AuthResponseCodeA1 authResponseCodeA1, AuthResponseCode60 authResponseCode60, int authResponseCodeEC1, int authResponseCodeEC2, int cardInfoResponseCodeEC2, PermissionResponseCodeC3 permissionResponseCodeC3, PermissionResponseCodeAB permissionResponseCodeAB) {
		this.value = value;
		this.description = description;
		this.authResponseCodeC3 = authResponseCodeC3;
		this.authResponseCodeAF = authResponseCodeAF;
		this.authResponseCodeA1 = authResponseCodeA1;
		this.authResponseCode60 = authResponseCode60;
		this.authResponseCodeEC1 = authResponseCodeEC1;
		this.authResponseCodeEC2 = authResponseCodeEC2;
		this.cardInfoResponseCodeEC2 = cardInfoResponseCodeEC2;
		this.permissionResponseCodeC3 = permissionResponseCodeC3;
		this.permissionResponseCodeAB = permissionResponseCodeAB;
	}

	public char getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	public static AuthResultCode getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

	public AuthResponseCodeC3 getAuthResponseCodeC3() {
		return authResponseCodeC3;
	}

	public AuthResponseCodeAF getAuthResponseCodeAF() {
		return authResponseCodeAF;
	}

	public AuthResponseCodeA1 getAuthResponseCodeA1() {
		return authResponseCodeA1;
	}

	public AuthResponseCode60 getAuthResponseCode60() {
		return authResponseCode60;
	}

	public int getAuthResponseCodeEC1() {
		return authResponseCodeEC1;
	}

	public int getAuthResponseCodeEC2() {
		return authResponseCodeEC2;
	}

	public int getCardInfoResponseCodeEC2() {
		return cardInfoResponseCodeEC2;
	}

	public PermissionResponseCodeC3 getPermissionResponseCodeC3() {
		return permissionResponseCodeC3;
	}

	public PermissionResponseCodeAB getPermissionResponseCodeAB() {
		return permissionResponseCodeAB;
	}
}
