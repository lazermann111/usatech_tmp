package com.usatech.layers.common.constants;

import java.util.BitSet;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum CounterReasonCode {
	BATCH_EVENT((byte) 'B', 1, 2, null, null, null, false, false), 
	FILL_CARD_SWIPE((byte) 'F', 2, 2, EntryType.SWIPE, null, null, true, false),
	REDISPLAY_CARD_SWIPE((byte) 'R', 98, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.NO, true, false),
	FILL_MANUAL_LOCAL_NO_RESET((byte)128, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.NO, false, false),
	FILL_MANUAL_LOCAL_CASH_RESET((byte)129, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, false),
	FILL_MANUAL_LOCAL_CASHLESS_RESET((byte)130, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, false),
	FILL_MANUAL_LOCAL_YES_RESET((byte)131, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.YES, false, false),
	FILL_MANUAL_LOCAL_NO_RESET_DISPLAYED((byte)132, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.NO, true, false),
	FILL_MANUAL_LOCAL_CASH_RESET_DISPLAYED((byte)133, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, false),
	FILL_MANUAL_LOCAL_CASHLESS_RESET_DISPLAYED((byte)134, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, false),
	FILL_MANUAL_LOCAL_YES_RESET_DISPLAYED((byte)135, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.YES, true, false),
	FILL_MANUAL_LOCAL_NO_RESET_DEX((byte)136, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.NO, false, true),
	FILL_MANUAL_LOCAL_CASH_RESET_DEX((byte)137, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, true),
	FILL_MANUAL_LOCAL_CASHLESS_RESET_DEX((byte)138, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, true),
	FILL_MANUAL_LOCAL_YES_RESET_DEX((byte)139, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.YES, false, true),
	FILL_MANUAL_LOCAL_NO_RESET_DISPLAYED_DEX((byte)140, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.NO, true, true),
	FILL_MANUAL_LOCAL_CASH_RESET_DISPLAYED_DEX((byte)141, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, true),
	FILL_MANUAL_LOCAL_CASHLESS_RESET_DISPLAYED_DEX((byte)142, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, true),
	FILL_MANUAL_LOCAL_YES_RESET_DISPLAYED_DEX((byte)143, 2, 2, EntryType.MANUAL, FillEventAuthType.LOCAL, CountersResetCode.YES, true, true),
	FILL_MANUAL_NETWORK_NO_RESET((byte)144, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.NO, false, false),
	FILL_MANUAL_NETWORK_CASH_RESET((byte)145, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, false),
	FILL_MANUAL_NETWORK_CASHLESS_RESET((byte)146, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, false),
	FILL_MANUAL_NETWORK_YES_RESET((byte)147, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.YES, false, false),
	FILL_MANUAL_NETWORK_NO_RESET_DISPLAYED((byte)148, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.NO, true, false),
	FILL_MANUAL_NETWORK_CASH_RESET_DISPLAYED((byte)149, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, false),
	FILL_MANUAL_NETWORK_CASHLESS_RESET_DISPLAYED((byte)150, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, false),
	FILL_MANUAL_NETWORK_YES_RESET_DISPLAYED((byte)151, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.YES, true, false),
	FILL_MANUAL_NETWORK_NO_RESET_DEX((byte)152, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.NO, false, true),
	FILL_MANUAL_NETWORK_CASH_RESET_DEX((byte)153, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, true),
	FILL_MANUAL_NETWORK_CASHLESS_RESET_DEX((byte)154, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, true),
	FILL_MANUAL_NETWORK_YES_RESET_DEX((byte)155, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.YES, false, true),
	FILL_MANUAL_NETWORK_NO_RESET_DISPLAYED_DEX((byte)156, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.NO, true, true),
	FILL_MANUAL_NETWORK_CASH_RESET_DISPLAYED_DEX((byte)157, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, true),
	FILL_MANUAL_NETWORK_CASHLESS_RESET_DISPLAYED_DEX((byte)158, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, true),
	FILL_MANUAL_NETWORK_YES_RESET_DISPLAYED_DEX((byte)159, 2, 2, EntryType.MANUAL, FillEventAuthType.NETWORK, CountersResetCode.YES, true, true),
	FILL_SWIPE_LOCAL_NO_RESET((byte)160, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.NO, false, false),
	FILL_SWIPE_LOCAL_CASH_RESET((byte)161, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, false),
	FILL_SWIPE_LOCAL_CASHLESS_RESET((byte)162, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, false),
	FILL_SWIPE_LOCAL_YES_RESET((byte)163, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.YES, false, false),
	FILL_SWIPE_LOCAL_NO_RESET_DISPLAYED((byte)164, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.NO, true, false),
	FILL_SWIPE_LOCAL_CASH_RESET_DISPLAYED((byte)165, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, false),
	FILL_SWIPE_LOCAL_CASHLESS_RESET_DISPLAYED((byte)166, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, false),
	FILL_SWIPE_LOCAL_YES_RESET_DISPLAYED((byte)167, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.YES, true, false),
	FILL_SWIPE_LOCAL_NO_RESET_DEX((byte)168, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.NO, false, true),
	FILL_SWIPE_LOCAL_CASH_RESET_DEX((byte)169, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, true),
	FILL_SWIPE_LOCAL_CASHLESS_RESET_DEX((byte)170, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, true),
	FILL_SWIPE_LOCAL_YES_RESET_DEX((byte)171, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.YES, false, true),
	FILL_SWIPE_LOCAL_NO_RESET_DISPLAYED_DEX((byte)172, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.NO, true, true),
	FILL_SWIPE_LOCAL_CASH_RESET_DISPLAYED_DEX((byte)173, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, true),
	FILL_SWIPE_LOCAL_CASHLESS_RESET_DISPLAYED_DEX((byte)174, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, true),
	FILL_SWIPE_LOCAL_YES_RESET_DISPLAYED_DEX((byte)175, 2, 2, EntryType.SWIPE, FillEventAuthType.LOCAL, CountersResetCode.YES, true, true),
	FILL_SWIPE_NETWORK_NO_RESET((byte)176, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.NO, false, false),
	FILL_SWIPE_NETWORK_CASH_RESET((byte)177, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, false),
	FILL_SWIPE_NETWORK_CASHLESS_RESET((byte)178, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, false),
	FILL_SWIPE_NETWORK_YES_RESET((byte)179, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.YES, false, false),
	FILL_SWIPE_NETWORK_NO_RESET_DISPLAYED((byte)180, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.NO, true, false),
	FILL_SWIPE_NETWORK_CASH_RESET_DISPLAYED((byte)181, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, false),
	FILL_SWIPE_NETWORK_CASHLESS_RESET_DISPLAYED((byte)182, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, false),
	FILL_SWIPE_NETWORK_YES_RESET_DISPLAYED((byte)183, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.YES, true, false),
	FILL_SWIPE_NETWORK_NO_RESET_DEX((byte)184, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.NO, false, true),
	FILL_SWIPE_NETWORK_CASH_RESET_DEX((byte)185, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, true),
	FILL_SWIPE_NETWORK_CASHLESS_RESET_DEX((byte)186, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, true),
	FILL_SWIPE_NETWORK_YES_RESET_DEX((byte)187, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.YES, false, true),
	FILL_SWIPE_NETWORK_NO_RESET_DISPLAYED_DEX((byte)188, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.NO, true, true),
	FILL_SWIPE_NETWORK_CASH_RESET_DISPLAYED_DEX((byte)189, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, true),
	FILL_SWIPE_NETWORK_CASHLESS_RESET_DISPLAYED_DEX((byte)190, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, true),
	FILL_SWIPE_NETWORK_YES_RESET_DISPLAYED_DEX((byte)191, 2, 2, EntryType.SWIPE, FillEventAuthType.NETWORK, CountersResetCode.YES, true, true),
	FILL_CONTACTLESS_LOCAL_NO_RESET((byte)192, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.NO, false, false),
	FILL_CONTACTLESS_LOCAL_CASH_RESET((byte)193, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, false),
	FILL_CONTACTLESS_LOCAL_CASHLESS_RESET((byte)194, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, false),
	FILL_CONTACTLESS_LOCAL_YES_RESET((byte)195, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.YES, false, false),
	FILL_CONTACTLESS_LOCAL_NO_RESET_DISPLAYED((byte)196, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.NO, true, false),
	FILL_CONTACTLESS_LOCAL_CASH_RESET_DISPLAYED((byte)197, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, false),
	FILL_CONTACTLESS_LOCAL_CASHLESS_RESET_DISPLAYED((byte)198, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, false),
	FILL_CONTACTLESS_LOCAL_YES_RESET_DISPLAYED((byte)199, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.YES, true, false),
	FILL_CONTACTLESS_LOCAL_NO_RESET_DEX((byte)200, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.NO, false, true),
	FILL_CONTACTLESS_LOCAL_CASH_RESET_DEX((byte)201, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, true),
	FILL_CONTACTLESS_LOCAL_CASHLESS_RESET_DEX((byte)202, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, true),
	FILL_CONTACTLESS_LOCAL_YES_RESET_DEX((byte)203, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.YES, false, true),
	FILL_CONTACTLESS_LOCAL_NO_RESET_DISPLAYED_DEX((byte)204, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.NO, true, true),
	FILL_CONTACTLESS_LOCAL_CASH_RESET_DISPLAYED_DEX((byte)205, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, true),
	FILL_CONTACTLESS_LOCAL_CASHLESS_RESET_DISPLAYED_DEX((byte)206, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, true),
	FILL_CONTACTLESS_LOCAL_YES_RESET_DISPLAYED_DEX((byte)207, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.LOCAL, CountersResetCode.YES, true, true),
	FILL_CONTACTLESS_NETWORK_NO_RESET((byte)208, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.NO, false, false),
	FILL_CONTACTLESS_NETWORK_CASH_RESET((byte)209, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, false),
	FILL_CONTACTLESS_NETWORK_CASHLESS_RESET((byte)210, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, false),
	FILL_CONTACTLESS_NETWORK_YES_RESET((byte)211, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.YES, false, false),
	FILL_CONTACTLESS_NETWORK_NO_RESET_DISPLAYED((byte)212, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.NO, true, false),
	FILL_CONTACTLESS_NETWORK_CASH_RESET_DISPLAYED((byte)213, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, false),
	FILL_CONTACTLESS_NETWORK_CASHLESS_RESET_DISPLAYED((byte)214, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, false),
	FILL_CONTACTLESS_NETWORK_YES_RESET_DISPLAYED((byte)215, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.YES, true, false),
	FILL_CONTACTLESS_NETWORK_NO_RESET_DEX((byte)216, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.NO, false, true),
	FILL_CONTACTLESS_NETWORK_CASH_RESET_DEX((byte)217, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, true),
	FILL_CONTACTLESS_NETWORK_CASHLESS_RESET_DEX((byte)218, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, true),
	FILL_CONTACTLESS_NETWORK_YES_RESET_DEX((byte)219, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.YES, false, true),
	FILL_CONTACTLESS_NETWORK_NO_RESET_DISPLAYED_DEX((byte)220, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.NO, true, true),
	FILL_CONTACTLESS_NETWORK_CASH_RESET_DISPLAYED_DEX((byte)221, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, true),
	FILL_CONTACTLESS_NETWORK_CASHLESS_RESET_DISPLAYED_DEX((byte)222, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, true),
	FILL_CONTACTLESS_NETWORK_YES_RESET_DISPLAYED_DEX((byte)223, 2, 2, EntryType.CONTACTLESS, FillEventAuthType.NETWORK, CountersResetCode.YES, true, true),
	FILL_BAR_CODE_LOCAL_NO_RESET((byte)224, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.NO, false, false),
	FILL_BAR_CODE_LOCAL_CASH_RESET((byte)225, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, false),
	FILL_BAR_CODE_LOCAL_CASHLESS_RESET((byte)226, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, false),
	FILL_BAR_CODE_LOCAL_YES_RESET((byte)227, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.YES, false, false),
	FILL_BAR_CODE_LOCAL_NO_RESET_DISPLAYED((byte)228, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.NO, true, false),
	FILL_BAR_CODE_LOCAL_CASH_RESET_DISPLAYED((byte)229, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, false),
	FILL_BAR_CODE_LOCAL_CASHLESS_RESET_DISPLAYED((byte)230, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, false),
	FILL_BAR_CODE_LOCAL_YES_RESET_DISPLAYED((byte)231, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.YES, true, false),
	FILL_BAR_CODE_LOCAL_NO_RESET_DEX((byte)232, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.NO, false, true),
	FILL_BAR_CODE_LOCAL_CASH_RESET_DEX((byte)233, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASH, false, true),
	FILL_BAR_CODE_LOCAL_CASHLESS_RESET_DEX((byte)234, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, false, true),
	FILL_BAR_CODE_LOCAL_YES_RESET_DEX((byte)235, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.YES, false, true),
	FILL_BAR_CODE_LOCAL_NO_RESET_DISPLAYED_DEX((byte)236, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.NO, true, true),
	FILL_BAR_CODE_LOCAL_CASH_RESET_DISPLAYED_DEX((byte)237, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASH, true, true),
	FILL_BAR_CODE_LOCAL_CASHLESS_RESET_DISPLAYED_DEX((byte)238, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.CASHLESS, true, true),
	FILL_BAR_CODE_LOCAL_YES_RESET_DISPLAYED_DEX((byte)239, 2, 2, EntryType.BAR_CODE, FillEventAuthType.LOCAL, CountersResetCode.YES, true, true),
	FILL_BAR_CODE_NETWORK_NO_RESET((byte)240, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.NO, false, false),
	FILL_BAR_CODE_NETWORK_CASH_RESET((byte)241, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, false),
	FILL_BAR_CODE_NETWORK_CASHLESS_RESET((byte)242, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, false),
	FILL_BAR_CODE_NETWORK_YES_RESET((byte)243, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.YES, false, false),
	FILL_BAR_CODE_NETWORK_NO_RESET_DISPLAYED((byte)244, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.NO, true, false),
	FILL_BAR_CODE_NETWORK_CASH_RESET_DISPLAYED((byte)245, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, false),
	FILL_BAR_CODE_NETWORK_CASHLESS_RESET_DISPLAYED((byte)246, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, false),
	FILL_BAR_CODE_NETWORK_YES_RESET_DISPLAYED((byte)247, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.YES, true, false),
	FILL_BAR_CODE_NETWORK_NO_RESET_DEX((byte)248, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.NO, false, true),
	FILL_BAR_CODE_NETWORK_CASH_RESET_DEX((byte)249, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASH, false, true),
	FILL_BAR_CODE_NETWORK_CASHLESS_RESET_DEX((byte)250, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, false, true),
	FILL_BAR_CODE_NETWORK_YES_RESET_DEX((byte)251, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.YES, false, true),
	FILL_BAR_CODE_NETWORK_NO_RESET_DISPLAYED_DEX((byte)252, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.NO, true, true),
	FILL_BAR_CODE_NETWORK_CASH_RESET_DISPLAYED_DEX((byte)253, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASH, true, true),
	FILL_BAR_CODE_NETWORK_CASHLESS_RESET_DISPLAYED_DEX((byte)254, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.CASHLESS, true, true),
	FILL_BAR_CODE_NETWORK_YES_RESET_DISPLAYED_DEX((byte)255, 2, 2, EntryType.BAR_CODE, FillEventAuthType.NETWORK, CountersResetCode.YES, true, true),
    ;

    private final byte value;
    private final int eventTypeId;
    private final int eventVersion;
    private final EntryType entryType;
    private final FillEventAuthType authType;
    private final CountersResetCode countersResetCode;
    private final boolean countersDisplayed;
	private final boolean dexCreated;

    protected final static EnumByteValueLookup<CounterReasonCode> lookup = new EnumByteValueLookup<CounterReasonCode>(CounterReasonCode.class);
    public static CounterReasonCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }

	private CounterReasonCode(byte value, int eventTypeId, int eventVersion, EntryType entryType, FillEventAuthType authType, CountersResetCode countersResetCode, boolean countersDisplayed, boolean dexCreated) {
        this.value = value;
        this.eventTypeId = eventTypeId;
        this.eventVersion = eventVersion;
        this.entryType = entryType;
        this.authType = authType;
        this.countersResetCode = countersResetCode;
        this.countersDisplayed = countersDisplayed;
		this.dexCreated = dexCreated;
    }
    public byte getValue() {
        return value;
    }
    public int getEventTypeId() {
		return eventTypeId;
	}
	public int getEventVersion() {
		return eventVersion;
	}
	public EntryType getEntryType() {
		return entryType;
	}
	public FillEventAuthType getAuthType() {
		return authType;
	}
	public CountersResetCode getCountersResetCode() {
		return countersResetCode;
	}
	public boolean isCountersDisplayed() {
		return countersDisplayed;
	}

	public boolean isDexCreated() {
		return dexCreated;
	}
	
	public static void main(String[] args) {
		BitSet bits = new BitSet(8);
		for(int n = 128; n < 256; n++) {
			for(int i = 0; i < 8; i++) {
				int chk = 1 << i;
				bits.set(i, (n & chk) == chk);
			}
			produceEnum(bits);
		}
	}

	private static void produceEnum(BitSet bits) {
		EntryType entryType = new EntryType[] {
 EntryType.MANUAL, EntryType.SWIPE, EntryType.CONTACTLESS, EntryType.BAR_CODE }[(bits.get(6) ? 2 : 0) + (bits.get(5) ? 1 : 0)];
		FillEventAuthType authType = bits.get(4) ? FillEventAuthType.NETWORK : FillEventAuthType.LOCAL;
		boolean dexCreated = bits.get(3);
		boolean countersDisplayed = bits.get(2);
		CountersResetCode countersResetCode = new CountersResetCode[] {
				CountersResetCode.NO, CountersResetCode.CASH, CountersResetCode.CASHLESS, CountersResetCode.YES
		}[(bits.get(1) ? 2 : 0) + (bits.get(0) ? 1 : 0)];
		System.out.print("FILL_");
		System.out.print(entryType);
		System.out.print("_");
		System.out.print(authType);
		System.out.print("_");
		System.out.print(countersResetCode);
		System.out.print("_RESET");
		if(countersDisplayed)
			System.out.print("_DISPLAYED");
		if(dexCreated)
			System.out.print("_DEX");
		System.out.print("((byte)");
		int n = 0;
		for(int i = 0; i < 8; i++) {
			if(bits.get(i))
				n += (1 << i);
		}
		System.out.print(n);
		System.out.print(", 2, 2, EntryType.");
		System.out.print(entryType);
		System.out.print(", FillEventAuthType.");
		System.out.print(authType);
		System.out.print(", CountersResetCode.");
		System.out.print(countersResetCode);
		System.out.print(", ");
		System.out.print(countersDisplayed);
		System.out.print(", ");
		System.out.print(dexCreated);
		System.out.print("),");
		System.out.println();		
	}

}
