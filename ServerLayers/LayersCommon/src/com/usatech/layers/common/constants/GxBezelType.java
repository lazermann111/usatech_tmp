package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum GxBezelType {
	DEFAULT('D', "USAT"),
	BVCR('B', "CoinCo"),
	UIC_RFID('U', "UIC"),
	OTI_RFID('O', "OTI"),
	OTI_ISIS('S', "OTI Softcard"),
	VIVOTECH_RFID('V', "VivoTech"),
	VIVOTECH_ISIS('I', "VivoTech Softcard"),
	UNSUPPORTED('-', "<Unsupported>"),
	UNKNOWN('?', "<Unknown>"),
	MEI_GEN_1('M', "MEI Generation 1"), 
	MEI_3_IN_1('3', "MEI 3-in-1"),
	MEI_4_IN_1('4', "MEI 4-in-1"),
	CRANE('C', "Crane"),
	SATURN_6500_EMC('Q', "Saturn 6500 EMC"),
	SATURN_6500_EM('R', "Saturn 6500 EM"),
	VEND3('E', "IDTech Vend3"),
	VENDI('F', "IDTech Vendi"),
	;

    private final char value;
    private final String manufacturer;
    private static final EnumCharValueLookup<GxBezelType> lookup = new EnumCharValueLookup<GxBezelType>(GxBezelType.class);
    private GxBezelType(char value, String manufacturer) {
        this.value = value;
        this.manufacturer = manufacturer;
    }

    public char getValue() {
        return value;
    }

    public String getManufacturer() {
		return manufacturer;
	}

    public static GxBezelType getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
}
