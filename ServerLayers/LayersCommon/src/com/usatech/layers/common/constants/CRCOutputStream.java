/**
 *
 */
package com.usatech.layers.common.constants;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Brian S. Krug
 *
 */
public class CRCOutputStream extends OutputStream {
	protected final OutputStream delegate;
	protected final CRCType.Calculator crcCalc;

	public CRCOutputStream(OutputStream delegate, CRCType crcType) {
		super();
		this.delegate = delegate;
		this.crcCalc = crcType.newCalculator();
	}

	public byte[] getCRCValue() {
		return crcCalc.getValue();
	}

	/**
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public void write(int b) throws IOException {
		delegate.write(b);
		crcCalc.update((byte)b);
	}
	/**
	 * @see java.io.OutputStream#write(byte[])
	 */
	@Override
	public void write(byte[] b) throws IOException {
		delegate.write(b);
		crcCalc.update(b);
	}
	/**
	 * @see java.io.OutputStream#write(byte[], int, int)
	 */
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		delegate.write(b, off, len);
		crcCalc.update(b, off, len);
	}
	/**
	 * @see java.io.OutputStream#flush()
	 */
	@Override
	public void flush() throws IOException {
		delegate.flush();
	}
	/**
	 * @see java.io.OutputStream#close()
	 */
	@Override
	public void close() throws IOException {
		delegate.close();
	}
}
