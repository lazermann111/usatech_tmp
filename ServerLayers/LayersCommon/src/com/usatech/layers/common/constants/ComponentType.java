/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum ComponentType {
	VIRTUAL(1, "Virtual", false),
	GEN2_STACK_DRYER_WASHER(9, "Gen2 Stack Dryer/Washer", false),
	GEN2_DRYER(47, "Gen2 Dryer", false),
	GEN2_STACK_DRYER_DRYER(48, "Gen2 Stack Dryer/Dryer", false),
	GEN2_WASHER(49, "Gen2 Washer", false),
	GEN1_STACK_DRYER_DRYER_TOP(88, "Gen1 Stack Dryer/Dryer (Top)", false),
	GEN1_STACK_DRYER_DRYER_BOTTOM(89, "Gen1 Stack Dryer/Dryer (Bottom)", false),
	GEN1_WASHER(90, "Gen1 Washer", false),
	GEN1_DRYER(91, "Gen1 Dryer", false),
	GEN1_STACK_DRYER_DRYER_TOP_ALT(92, "Gen1 Stack Dryer/Dryer (Top)", false),
	GEN1_STACK_DRYER_DRYER_BOTTOM_ALT(93, "Gen1 Stack Dryer/Dryer (Bottom)", false),
	VENDING_MACHINE(200, "Vending Machine", false),
	EPORT_EDGE_G9(201, "ePort Edge/G9", true),
	GPRS_MODEM(202, "GPRS Modem", false),
	CARD_READER(203, "Card Reader", false),
	CDMA_MODEM(204, "CDMA Modem", false),
	NETWORK_INTERFACE_CONTROLLER(205, "Network Interface Controller", false),
	CDMA_VERIZON_MODEM(206, "CDMA Verizon Modem", false),
	GPRS_ATT_MODEM(207, "GPRS AT&T Modem", false),
	GPRS_ESEYE_MODEM(208, "GPRS Eseye Modem", false),
	GPRS_ROGERS_MODEM(209, "GPRS Rogers Modem", false),
	HSPA_ATT_MODEM(210, "HSPA AT&T Modem", false),
	HSPA_ESEYE_MODEM(211, "HSPA Eseye Modem", false),
	HSPA_ROGERS_MODEM(212, "HSPA Rogers Modem", false),
	HSPA5_ATT_MODEM(213, "HSPA5 AT&T Modem", false),
	HSPA5_ESEYE_MODEM(214, "HSPA5 Eseye Modem", false),
	VLTE_VERIZON_MODEM(215, "VLTE Verizon Modem", false),
	CDMA_USC_MODEM(216, "CDMA US-Cellular Modem", false),
	EPORT_G4(303, "ePort G4", true),
	EPORT_G5(304, "ePort G5", true),
	EPORT_NG_BRICK(305, "ePort NG Brick", true),
	PICTURESTATION_SOFTWARE(306, "PictureStation Software", true),
	RDP(307, "RDP", true),
	EZAPN_BOARD(308, "eZ-APN Board", true),
	USALIVE_TRANSACT(309, "USALive Transact", true),
	KIOSK_SOFTWARE(310, "Kiosk Software", true),
	PRINTER(311, "Printer", false),
	COPIER(312, "Copier", false),
	FAX(313, "Fax", false),
	SCANNER(314, "Scanner", false),
	LAPTOP(315, "Laptop", false),
	MFP(316, "MFP", false),
	EPORT_CONNECT_WEB_SERVICE_CLIENT(320, "ePort Connect Web Service Client", true),
	BEZEL(400, "Bezel", false),
	/*
	SELECT name || '(' || value ||', "' || description || '", ' || base || '),'
	 FROM (
	select ht.host_type_id value,
	  REPLACE(REPLACE( REPLACE(REPLACE(UPPER(ht.host_type_desc), ' ', '_'), '/', '_'), '(', ''), ')', '') name,
	  ht.host_type_desc description,
	  DECODE(gt.HOST_GROUP_TYPE_CD, NULL, 'false', 'true') base
	 from device.host_type ht
	left outer join (device.host_type_host_group_type htgt
	join device.host_group_type gt on htgt.host_group_type_id = gt.host_group_type_id
	and gt.Host_group_type_cd = 'BASE_HOST')
	 on ht.HOST_TYPE_ID =  htgt.HOST_TYPE_ID)
	 order by value;
	 */
	;

	private final int value;
    private final String description;
    private final boolean baseComponent;
    private ComponentType(int value, String description, boolean baseComponent) {
        this.value = value;
        this.description = description;
        this.baseComponent = baseComponent;
    }
    public int getValue() {
        return value;
    }
    public String getDescription() {
        return description;
    }

	public boolean isBaseComponent() {
		return baseComponent;
	}
	protected final static EnumIntValueLookup<ComponentType> lookup = new EnumIntValueLookup<ComponentType>(ComponentType.class);
    public static ComponentType getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
