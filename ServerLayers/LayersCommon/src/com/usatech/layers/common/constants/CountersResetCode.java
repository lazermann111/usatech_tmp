package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum CountersResetCode {
    YES('Y', "Yes"),
    NO('N', "No"),
    CASHLESS('C', "Cashless only"),
    ALL('A', "All"),
	UNKNOWN('?', "Unknown"),
	CASH('M', "Cash only"),
    ;

    private final char value;
    private final String description;
    private static final EnumCharValueLookup<CountersResetCode> lookup = new EnumCharValueLookup<CountersResetCode>(CountersResetCode.class);
    private CountersResetCode(char value, String description) {
        this.value = value;
        this.description = description;
    }

    public char getValue() {
        return value;
    }

    public String getDescription() {
		return description;
	}

    public static CountersResetCode getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
}
