package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum SettlementFormat {
    FORMAT_0((byte)0),
    FORMAT_1((byte)1),
    FORMAT_2((byte)2);

    private final byte value;
    private SettlementFormat(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<SettlementFormat> lookup = new EnumByteValueLookup<SettlementFormat>(SettlementFormat.class);
    public static SettlementFormat getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
