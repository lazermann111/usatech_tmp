package com.usatech.layers.common.constants;

public enum DataLoaderAction {
	UPSERT, METADATA, REPOPULATE, PARTIAL
}
