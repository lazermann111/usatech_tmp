package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum CurrencyCd {
	USD("USD"),
	CAD("CAD");
	
    private final String value;
	protected final static EnumCharValueLookup<CurrencyCd> lookup = new EnumCharValueLookup<CurrencyCd>(CurrencyCd.class);

	private CurrencyCd(String value) {
        this.value = value;
	}
    public String getValue() {
        return value;
    }
    
    public static CurrencyCd getByValue(String value) throws InvalidValueException {
        return getByValue(value);
    }
	
}
