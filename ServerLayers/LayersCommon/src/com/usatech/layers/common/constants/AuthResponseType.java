package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * Auth C3 response type.
 * @author yhe
 *
 */
public enum AuthResponseType {
	CONTROL((byte)0),
	AUTHORIZATION((byte)1),
	PERMISSION((byte)2),
	AUTHORIZATION_V2((byte)3);
	private final byte value;
	private AuthResponseType(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<AuthResponseType> lookup = new EnumByteValueLookup<AuthResponseType>(AuthResponseType.class);
    public static AuthResponseType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
