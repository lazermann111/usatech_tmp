/**
 *
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum GenericResponseServerActionCode {
	NO_ACTION((byte)0),
	CAPTURE_MDB_DUMP_INIT_SESSION((byte)1),
	CAPTURE_MDB_DUMP_VEND_SESSION((byte)2),
	REBOOT((byte)5),
	REINITIALIZE((byte)6),
	PERFORM_SETTLEMENT((byte)7),
	UPLOAD_PROPERTY_VALUE_LIST((byte)8),
	STOP_FILE_TRANSFER((byte)9),
	DISCONNECT((byte)10),
	SEND_UPDATE_STATUS_REQUEST_IMMEDIATE((byte)11),
	SEND_UPDATE_STATUS_REQUEST((byte)12),
	SEND_COMP_IDENTITY((byte)13),
	PROCESS_PROP_LIST((byte)14),
	TRACE_EVENTS((byte)15),
	;

	private final byte value;
	private GenericResponseServerActionCode(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}
    protected final static EnumByteValueLookup<GenericResponseServerActionCode> lookup = new EnumByteValueLookup<GenericResponseServerActionCode>(GenericResponseServerActionCode.class);
    public static GenericResponseServerActionCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
