package com.usatech.layers.common.constants;

import java.util.Collections;
import java.util.Set;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum DeviceProperty {
	SERIAL_NUM(50, "Device Serial Number", DevicePropertyType.READ_ONLY, 0),
	DEVICE_GUID(51, "Device GUID", DevicePropertyType.READ_ONLY, 0),
	ENCRYPTION_KEY(52, "Encryption Key", DevicePropertyType.READ_ONLY, 0),
	MASTER_ID(60, "Master ID", DevicePropertyType.DYNAMIC, 0),
	MESSAGE_ID(61, "Message ID", DevicePropertyType.DYNAMIC, 0),
	TRANSACTION_ID(62, "Transaction ID", DevicePropertyType.READ_ONLY, 0),
	BATCH_ID(63, "Batch ID", DevicePropertyType.READ_ONLY, 0),
	EVENT_ID(64, "Event ID", DevicePropertyType.READ_ONLY, 0),
	ACTIVATION_STATUS(70, "Activation Status", DevicePropertyType.DYNAMIC, 0),
	UTC_TIME(80, "UTC Time", DevicePropertyType.DYNAMIC, 0),
	UTC_OFFSET(81, "UTC Offset", DevicePropertyType.DYNAMIC, 0),
	SETTLEMENT_SCHEDULE(85, "Settlement Schedule", DevicePropertyType.STATIC, 0),
	NON_ACTIVATED_CALL_IN_SCHEDULE(86, "Non-Activated Call-in Schedule", DevicePropertyType.STATIC, 0),
	ACTIVATED_CALL_IN_SCHEDULE(87, "Activated Call-in Schedule", DevicePropertyType.STATIC, 0),
	PCOUNTER_CASH_ITEMS(100, "Pcounter - Cash Vended Items", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_CASH_AMOUNT(101, "Pcounter - Cash Vended Amount", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_CASHLESS_ITEMS(102, "Pcounter - Cashless Vended Items", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_CASHLESS_AMOUNT(103, "Pcounter - Cashless Vended Amount", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_BYTES_SENT(104, "Pcounter - Bytes Transmitted", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_BYTES_RECEIVED(105, "Pcounter - Bytes Received", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_SESSION_COUNT(106, "Pcounter - # of Communication Sessions", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_FAILED_COUNT(107, "Pcounter - # of Failed Connection Attempts", DevicePropertyType.READ_ONLY, 0),
	PCOUNTER_INCOMPLETE_COUNT(108, "Pcounter - # of Incomplete Sessions", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_CASH_ITEMS(200, "Icounter - Cash Vended Items", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_CASH_AMOUNT(201, "Icounter - Cash Vended Amount", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_CASHLESS_ITEMS(202, "Icounter - Cashless Vended Items", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_CASHLESS_AMOUNT(203, "Icounter - Cashless Vended Amount", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_BYTES_SENT(204, "Icounter - Bytes Transmitted", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_BYTES_RECEIVED(205, "Icounter - Bytes Received", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_SESSION_COUNT(206, "Icounter - # of Communication Sessions", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_FAILED_COUNT(207, "Icounter - # of Failed Connection Attempts", DevicePropertyType.READ_ONLY, 0),
	ICOUNTER_INCOMPLETE_COUNT(208, "Icounter - # of Incomplete Sessions", DevicePropertyType.READ_ONLY, 0),
	COMSTATS_RSSI(300, "ComStats - RSSI", DevicePropertyType.READ_ONLY, 0),
	COMSTAT_BER(301, "ComStats - BER", DevicePropertyType.READ_ONLY, 0),
	COMSTAT_RFSTS(302, "ComStats - RFSTS ", DevicePropertyType.READ_ONLY, 20),
	COMSTAT_MISC_PDP_ATTEMPTS(310, "ComStats - Misc PDP Context Attempt Counter", DevicePropertyType.READ_ONLY, 20),
	COMSTAT_MISC_PDP_ERRORS(311, "ComStats - Misc PDP Context Error Counter", DevicePropertyType.READ_ONLY, 20),
	COMSTAT_MISC_TCP_ATTEMPTS(312, "ComStats - Misc TCP Session Attempt Counter", DevicePropertyType.READ_ONLY, 20),
	COMSTAT_MISC_TCP_ERRORS(313, "ComStats - Misc TCP Session Error Counter", DevicePropertyType.READ_ONLY, 20),
	COMSTAT_AUTH_TCP_ATTEMPTS(314, "ComStats - Auth TCP Session Attempt Counter", DevicePropertyType.READ_ONLY, 20),
	COMSTAT_AUTH_TCP_ERRORS(315, "ComStats - Auth TCP Session Error Counter", DevicePropertyType.READ_ONLY, 20),
	DEX_SCHEDULE(1101, "DEX Schedule", DevicePropertyType.STATIC, 0),
	CONVENIENCE_FEE(1202, "Two-Tier Pricing", DevicePropertyType.STATIC, 0),
	VMC_INTERFACE_TYPE(1500, "VMC Interface Type", DevicePropertyType.STATIC, 0),;

	private final int value;
	private final String description;
	private final DevicePropertyType devicePropertyType;
	private final int minVersion;

	private DeviceProperty(int value, String description, DevicePropertyType devicePropertyType, int minVersion) {
		this.value = value;
		this.description = description;
		this.devicePropertyType = devicePropertyType;
		this.minVersion = minVersion;
	}

	public int getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	public DevicePropertyType getDevicePropertyType() {
		return devicePropertyType;
	}
	
	public int getMinVersion() {
		return minVersion;
	}

	protected final static EnumIntValueLookup<DeviceProperty> lookup = new EnumIntValueLookup<DeviceProperty>(DeviceProperty.class);

	public static DeviceProperty getByValue(int value) throws InvalidIntValueException {
		return lookup.getByValue(value);
	}

	public static DeviceProperty getByValueSafely(int value) {
		return lookup.getByValueSafely(value);
	}

	protected final static Set<DeviceProperty> sensitiveProperties = Collections.singleton(ENCRYPTION_KEY);

	public static Set<DeviceProperty> getSensitiveProperties() {
		return sensitiveProperties;
	}
}
