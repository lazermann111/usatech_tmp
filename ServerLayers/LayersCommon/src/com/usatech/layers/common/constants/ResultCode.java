package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum ResultCode {
    FAILURE((byte)0),
    SUCCESS((byte)1),
    TRAN_NOT_FOUND((byte)2),
    ILLEGAL_STATE((byte)3),
    INVALID_PARAMETER((byte)4),
    DUPLICATE((byte)5),
    PAYMENT_NOT_ACCEPTED((byte)6),
    HOST_NOT_FOUND((byte)7),
    ITEM_TYPE_NOT_FOUND((byte)8),
    SALE_VOIDED((byte)9),
    ;

    private final byte value;
    private ResultCode(byte value) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<ResultCode> lookup = new EnumByteValueLookup<ResultCode>(ResultCode.class);
    public static ResultCode getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}