package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum EventDetailType {
    EVENT_TYPE_VERSION((byte)1),
    HOST_EVENT_TIMESTAMP((byte)2),
    EVENT_DATA((byte)3),
    AUTHORIZATION_TYPE((byte)4),
	ENTRY_METHOD((byte) 5),
    ;

    private final byte value;
    private EventDetailType(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }

    protected final static EnumByteValueLookup<EventDetailType> lookup = new EnumByteValueLookup<EventDetailType>(EventDetailType.class);
    public static EventDetailType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
