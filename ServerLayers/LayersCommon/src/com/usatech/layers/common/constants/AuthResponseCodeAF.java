package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum AuthResponseCodeAF {
	DENIED((byte)0),
	APPROVED((byte)1),
	CONDITIONALLY_APPROVED((byte)2),
	FAILED((byte)3),
	DENIED_PAYMENT_METHOD((byte)5);
	private final byte value;
	private AuthResponseCodeAF(byte value) {
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<AuthResponseCodeAF> lookup = new EnumByteValueLookup<AuthResponseCodeAF>(AuthResponseCodeAF.class);
    public static AuthResponseCodeAF getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
