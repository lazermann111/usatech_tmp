package com.usatech.layers.common.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum WasherDryerType {
    NO_WASHER_DRYER_ON_PORT('N', -1, ESudsManufacturer.UNKNOWN, false, false, new int[]{-1,-1}),
    GEN2_WASHER('W', 7721, ESudsManufacturer.MAYTAG, false, false, new int[]{1,1}),
    GEN2_DRYER('D', 7722, ESudsManufacturer.MAYTAG, false, false, new int[]{40,40}),
    GEN2_STACK_DRYER_DRYER('S', 48, ESudsManufacturer.MAYTAG, true, false, new int[]{40,40}),
    GEN2_STACK_DRYER_WASHER('U', 9, ESudsManufacturer.MAYTAG, true, false, new int[]{1,40}),
    GEN1_STACKED_DRYER_DRYER_TOP('G', 6719, ESudsManufacturer.MAYTAG, true, true, new int[]{40,40}),
    GEN1_STACKED_DRYER_DRYER_BOTTOM('H', 6720, ESudsManufacturer.MAYTAG, true, true, new int[]{40,40}),
    GEN1_WASHER('A', 6717, ESudsManufacturer.SPEEDQUEEN, false, false, new int[]{1,1}),
    GEN1_DRYER('R', 6718, ESudsManufacturer.SPEEDQUEEN, false, false, new int[]{40,40}),
    GEN1_STACKED_DRYER_DRYER_TOP_ALT('I', 7723, ESudsManufacturer.SPEEDQUEEN, true, true, new int[]{40,40}),
    GEN1_STACKED_DRYER_DRYER_BOTTOM_ALT('J', 7724, ESudsManufacturer.SPEEDQUEEN, true, true, new int[]{40,40})
    ;

    private final char value;
    private final int hostType;
    private final ESudsManufacturer manufacturer;
    private final int[] cycleTypeId;
    private boolean stacked;
    private boolean stackedGen1;
    private static final EnumCharValueLookup<WasherDryerType> lookup = new EnumCharValueLookup<WasherDryerType>(WasherDryerType.class);

    private WasherDryerType(char value, int hostType, ESudsManufacturer manufacturer, boolean stacked, boolean stackedGen1, int[] cycleTypeId) {
        this.value = value;
        this.hostType=hostType;
        this.manufacturer=manufacturer;
        this.stacked=stacked;
        this.stackedGen1=stackedGen1;
        this.cycleTypeId=cycleTypeId;
    }

    public char getValue() {
        return value;
    }
    public static WasherDryerType getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
    public static WasherDryerType getByValue(int value) throws InvalidValueException {
        return getByValue((char)value);
    }
	public int getHostType() {
		return hostType;
	}

	public ESudsManufacturer getManufacturer() {
		return manufacturer;
	}
	
	public int getCycleTypeIdByPosition(int position) throws IllegalArgumentException{
		if(position!=0&&position!=1){
			throw new IllegalArgumentException("Position can only be 0 or 1.");
		}
		return cycleTypeId[position];
	}

	public boolean isStacked() {
		return stacked;
	}

	public boolean isStackedGen1() {
		return stackedGen1;
	}
}
