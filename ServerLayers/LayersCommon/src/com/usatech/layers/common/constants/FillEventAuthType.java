package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;



/**
 * This enum defines the attribute names that are used in the construction of the message chain.
 *
 * @author bkrug
 *
 */

public enum FillEventAuthType {
    LOCAL("local"),
    NETWORK("network"),
    ;
    private final String value;
    
    protected final static EnumStringValueLookup<FillEventAuthType> lookup = new EnumStringValueLookup<FillEventAuthType>(FillEventAuthType.class);
    public static FillEventAuthType getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private FillEventAuthType(String value) {
		this.value = value;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return The unique key of this attribute
	 */
	public String getValue() {
		return value;
	}
}
