package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum EventState {
    INCOMPLETE((byte)1),
    COMPLETE_FINAL((byte)2),
    COMPLETE_ERROR((byte)3),
    INCOMPLETE_ERROR((byte)4);

    private final byte value;
    private EventState(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<EventState> lookup = new EnumByteValueLookup<EventState>(EventState.class);
    public static EventState getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
