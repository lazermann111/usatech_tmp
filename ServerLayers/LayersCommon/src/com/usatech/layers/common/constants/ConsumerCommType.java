package com.usatech.layers.common.constants;

public enum ConsumerCommType {
	CASH_BACK_ON_PURCHASE,
	REPLENISH_BONUS,
    REPLENISH_DISABLED,
    PREPAID_PURCHASE,
    REPLENISH_DECLINED,
    REPLENISH_SUCCESS,
    BELOW_BALANCE,
    CARD_EXPIRATION,
    ADD_PASS
}
