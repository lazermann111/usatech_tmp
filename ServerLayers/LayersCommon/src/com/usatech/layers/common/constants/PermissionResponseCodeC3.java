package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

/**
 * Auth permission type response codes.
 * @author yhe
 *
 */
public enum PermissionResponseCodeC3 {
	SUCCESS((byte)0),
	RESERVED_NOT_USED((byte)1),
	DECLINE((byte)2),
	DECLINE_PERMANENT((byte)3),
	FAILURE((byte)4);
	private final byte value;
	private PermissionResponseCodeC3(byte value) {
		if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
	}
	public byte getValue() {
		return value;
	}

    protected final static EnumByteValueLookup<PermissionResponseCodeC3> lookup = new EnumByteValueLookup<PermissionResponseCodeC3>(PermissionResponseCodeC3.class);
    public static PermissionResponseCodeC3 getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
