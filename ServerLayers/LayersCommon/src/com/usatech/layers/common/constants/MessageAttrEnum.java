package com.usatech.layers.common.constants;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;



/**
 * This enum defines the attribute names that are used in the construction of the message chain.
 * 
 * @author bkrug
 * 
 */

public enum MessageAttrEnum implements Attribute {
	ATTR_DEVICE_NAME("deviceName"),
	ATTR_PROTOCOL("protocol"),
	ATTR_DATA("data"),
	ATTR_GLOBAL_SESSION_CODE("globalSessionCode"),
	ATTR_LAST_COMMAND_ID("lastCommandId"),
	ATTR_SERVER_TIME("serverTime"),	
    ATTR_DEVICE_INFO_HASH("deviceInfoHash"),
    ATTR_SESSION_START_TIME("sessionStartTime"),
    ATTR_RESPONSE_MESSAGE("responseMessage"),
    ATTR_RESPONSE_CODE("responseCode"),
    
    ;
    private final String value;
    private final String description;

    protected final static EnumStringValueLookup<MessageAttrEnum> lookup = new EnumStringValueLookup<MessageAttrEnum>(MessageAttrEnum.class);
    public static MessageAttrEnum getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }
    private MessageAttrEnum(String value) {
		this.value = value;
		this.description = null;
	}
	/**
	 * Internal constructor.
	 *
	 * @param value
	 */
	private MessageAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 *
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}
