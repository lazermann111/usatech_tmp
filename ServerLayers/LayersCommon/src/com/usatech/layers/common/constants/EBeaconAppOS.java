package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum EBeaconAppOS {
    ANDROID((byte)0),
    IOS((byte)1),
    ;
	private static final EnumByteValueLookup<EBeaconAppOS> lookup = new EnumByteValueLookup<EBeaconAppOS>(EBeaconAppOS.class);

    private final byte value;

    private EBeaconAppOS(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    public static EBeaconAppOS getByValue(byte value) throws InvalidByteValueException {
		return lookup.getByValue(value);
    }
}
