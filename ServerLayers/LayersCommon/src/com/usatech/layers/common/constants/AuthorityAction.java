/**
 *
 */
package com.usatech.layers.common.constants;

/**
 * @author Brian S. Krug
 *
 */
public enum AuthorityAction {
	SETTLEMENT,
	SETTLEMENT_RETRY,
	ADJUSTMENT,// (SALE, REFUND)
	SALE,
	REFUND,
	AUTHORIZATION,
	REVERSAL,
	EMV_PARAMETER_DOWNLOAD,
	TERMINAL_ADD,
	TERMINAL_UPDATE,
	TERMINAL_DELETE
}
