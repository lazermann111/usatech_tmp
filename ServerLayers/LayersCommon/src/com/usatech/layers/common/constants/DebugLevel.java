package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum DebugLevel {
	OFF((byte)0), DEBUG((byte)1);

	private final byte value;
	private DebugLevel(byte value) {
		this.value = value;
	}
	public byte getValue() {
		return value;
	}
    protected final static EnumByteValueLookup<DebugLevel> lookup = new EnumByteValueLookup<DebugLevel>(DebugLevel.class);
    public static DebugLevel getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
