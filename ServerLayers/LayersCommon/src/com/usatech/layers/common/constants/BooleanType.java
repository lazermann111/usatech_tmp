package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

public enum BooleanType {
    FALSE((byte)0), TRUE((byte)1);

    private final byte value;
    private BooleanType(byte value) {
    	if(value != this.ordinal())
    		throw new IllegalArgumentException("Value must equal ordinal");
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<BooleanType> lookup = new EnumByteValueLookup<BooleanType>(BooleanType.class);
    public static BooleanType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
