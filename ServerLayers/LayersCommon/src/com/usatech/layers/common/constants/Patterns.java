package com.usatech.layers.common.constants;

import java.util.regex.Pattern;

public class Patterns {
	public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("^(?:\\s*\\w+[\\w.-]*@\\w+[\\w.-]*?\\.[A-Za-z]{2,3}\\s*(?:[,;]|$))+$");
}
