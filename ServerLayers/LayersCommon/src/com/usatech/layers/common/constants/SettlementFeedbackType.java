package com.usatech.layers.common.constants;

public enum SettlementFeedbackType {
	BATCH, TRAN
}
