/*
 * (C) USA Technologies 2011
 */
package com.usatech.layers.common.constants;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

/**
 * Source subsystem sending an alerts,
 * either extracted from DEX files by 
 * InboundFileImportTask, or directly
 * from a device by MessageProcessor_C6 
 * 
 */
public enum AlertSourceCode {
    DEX(1, "DEX Alerts"),
	DEVICE(2, "Device Events"),
    MDB(3, "MDB Alerts"),
    ;

    private final int value;
    private final String description;
    private static final EnumIntValueLookup<AlertSourceCode> lookup = new EnumIntValueLookup<AlertSourceCode>(AlertSourceCode.class);
    private AlertSourceCode(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
		return description;
	}

    public static AlertSourceCode getByValue(int value) throws InvalidIntValueException {
        return lookup.getByValue(value);
    }
}
