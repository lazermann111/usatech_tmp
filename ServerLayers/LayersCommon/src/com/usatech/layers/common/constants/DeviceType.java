package com.usatech.layers.common.constants;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;



public enum DeviceType {
	DEFAULT((byte)-1),
    G4((byte)0),
    GX((byte)1),
    NG((byte)3),
    LEGACY_KIOSK((byte)4),
    ESUDS((byte)5),
    MEI((byte)6),
    EZ80_DEVELOPMENT((byte)7),
    EZ80((byte)8),
    LEGACY_G4((byte)9),
    TRANSACT((byte)10),
    KIOSK((byte)11),
    T2((byte)12),
    EDGE((byte)13),
    VIRTUAL((byte)14),
    ;

    private final byte value;
    private DeviceType(byte value) {
        this.value = value;
    }
    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<DeviceType> lookup = new EnumByteValueLookup<DeviceType>(DeviceType.class);
    public static DeviceType getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
}
