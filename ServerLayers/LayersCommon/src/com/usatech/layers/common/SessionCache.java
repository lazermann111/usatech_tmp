package com.usatech.layers.common;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentFutureCache;

public class SessionCache {
	protected static class NetLayerInfo {
		protected final String netLayerName;
		protected final String netLayerHost;
		protected final int netLayerPort;
		protected final String netLayerDesc;
		public NetLayerInfo(String netLayerName, String netLayerHost, int netLayerPort) {
			super();
			this.netLayerName = netLayerName;
			this.netLayerHost = netLayerHost;
			this.netLayerPort = netLayerPort;
			this.netLayerDesc = netLayerName + " at " + netLayerHost + ":" + netLayerPort;
		}
		public String getNetLayerName() {
			return netLayerName;
		}
		public String getNetLayerHost() {
			return netLayerHost;
		}
		public int getNetLayerPort() {
			return netLayerPort;
		}
		public String getNetLayerDesc() {
			return netLayerDesc;
		}
		/**
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if(obj == null)
				return false;
			return netLayerDesc.equals(obj.toString());
		}
		/**
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return netLayerDesc.hashCode();
		}
		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return netLayerDesc;
		}
	}
	protected static final Cache<NetLayerInfo, Number, ExecutionException> netLayerIdCache = new LockSegmentFutureCache<NetLayerInfo, Number>(100, 128, 0.75f, 8) {
		@Override
		protected Number createValue(NetLayerInfo key, Object... additionalInfo) throws Exception {
			Object[] ret = DataLayerMgr.executeCall("GET_OR_CREATE_NET_LAYER_ID", key, true);
			return ConvertUtils.convert(Number.class, ret[1]);
		}
	};

	public static Number getOrCreateNetLayerId(Map<String, Object> attributes) throws ServiceException {
		String netlayerName = ConvertUtils.getStringSafely(attributes.get("netlayerName"));
		String netlayerHost = ConvertUtils.getStringSafely(attributes.get("netlayerHost"));
		int netlayerPort = ConvertUtils.getIntSafely(attributes.get("netlayerPort"), -1);
		if(netlayerName == null || netlayerHost == null) {
			// For R23: netLayerDesc=NetworkLayer at usanet23:14108
			String netlayerDesc = ConvertUtils.getStringSafely(attributes.get("netLayerDesc"));
			if(netlayerDesc != null) {
				Matcher matcher = Pattern.compile("\\s*(\\S+)\\s+at\\s+([^:]+):(\\d+)\\s*").matcher(netlayerDesc);
				if(matcher.matches()) {
					netlayerName = matcher.group(1);
					netlayerHost = matcher.group(2);
					netlayerPort = ConvertUtils.getIntSafely(matcher.group(3), -1);
				}
			}
		}
		try {
			return netLayerIdCache.getOrCreate(new NetLayerInfo(netlayerName, netlayerHost, netlayerPort));
		} catch(ExecutionException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException)e.getCause();
			else if(e.getCause() instanceof SQLException)
				throw DatabasePrerequisite.createServiceException("Could not get netlayer id for '" + netlayerName + " at " + netlayerHost + ":" + netlayerPort + "'", (SQLException)e.getCause());
			else
				throw new ServiceException(e);
		}
	}
}
