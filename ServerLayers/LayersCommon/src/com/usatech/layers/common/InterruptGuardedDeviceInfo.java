package com.usatech.layers.common;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.util.Map;

import simple.app.ServiceException;
import simple.lang.InterruptGuard;
import simple.util.IntegerRangeSet;

import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;

public class InterruptGuardedDeviceInfo implements DeviceInfo {
	protected final DeviceInfo delegate;
	protected final InterruptGuard guard;
	public InterruptGuardedDeviceInfo(DeviceInfo delegate, InterruptGuard guard) {
		this.delegate = delegate;
		this.guard = guard;
	}

	public void clearChanges() {
		delegate.clearChanges();
	}

	public void commitChanges(long timestamp) throws ServiceException {
		boolean original = guard.setInterruptible(false);
		try {
			delegate.commitChanges(timestamp);
		} finally {
			guard.setInterruptible(original);
		}
	}

	public void commitChanges(long timestamp, boolean internalOnly) throws ServiceException {
		boolean original = guard.setInterruptible(false);
		try {
			delegate.commitChanges(timestamp, internalOnly);
		} finally {
			guard.setInterruptible(original);
		}
	}

	public ServerActionCode getActionCode() {
		return delegate.getActionCode();
	}

	public ActivationStatus getActivationStatus() {
		return delegate.getActivationStatus();
	}

	public Charset getDeviceCharset() {
		return delegate.getDeviceCharset();
	}

	public String getDeviceInfoHash() {
		return delegate.getDeviceInfoHash();
	}

	public String getDeviceName() {
		return delegate.getDeviceName();
	}
	
	public String getDeviceSerialCd() {
		return delegate.getDeviceSerialCd();
	}
	
	public String getUsername() {
		return delegate.getUsername();
	}
	
	public byte[] getPasswordHash() {
		return delegate.getPasswordHash();
	}
	
	public byte[] getPasswordSalt() {
		return delegate.getPasswordSalt();
	}
	
	public long getCredentialTimestamp() {
		return delegate.getCredentialTimestamp();
	}
	
	public String getPreviousUsername() {
		return delegate.getPreviousUsername();
	}
	
	public byte[] getPreviousPasswordHash() {
		return delegate.getPreviousPasswordHash();
	}
	
	public byte[] getPreviousPasswordSalt() {
		return delegate.getPreviousPasswordSalt();
	}
	
	public String getNewUsername() {
		return delegate.getNewUsername();
	}
	
	public byte[] getNewPasswordHash() {
		return delegate.getNewPasswordHash();
	}
	
	public byte[] getNewPasswordSalt() {
		return delegate.getNewPasswordSalt();
	}
	
	public long getMaxAuthAmount() {
		return delegate.getMaxAuthAmount();
	}

	public DeviceType getDeviceType() {
		return delegate.getDeviceType();
	}

	public byte[] getEncryptionKey() {
		return delegate.getEncryptionKey();
	}

	public String getLocale() {
		return delegate.getLocale();
	}

	public long getMasterId() {
		return delegate.getMasterId();
	}

	public byte[] getPreviousEncryptionKey() {
		return delegate.getPreviousEncryptionKey();
	}

	public IntegerRangeSet getPropertyIndexesToRequest() {
		return delegate.getPropertyIndexesToRequest();
	}

	public long getRejectUntil() {
		return delegate.getRejectUntil();
	}

	public String getTimeZoneGuid() {
		return delegate.getTimeZoneGuid();
	}

	public boolean isInitOnly() {
		return delegate.isInitOnly();
	}

	public void putAll(Map<String, Object> values, Map<String, Long> timestamps) {
		delegate.putAll(values, timestamps);
	}

	public void putAllWithTimestamps(Map<String, Object> values) {
		delegate.putAllWithTimestamps(values);
	}

	public void putWithTimestamp(DeviceInfoProperty dip, Map<String, Object> values) {
		delegate.putWithTimestamp(dip, values);
	}

	@Override
	public int putPendingWithTimestamp(Map<String, Object> values, long timestamp) {
		return delegate.putPendingWithTimestamp(values, timestamp);
	}

	public void refresh() throws ServiceException {
		boolean original = guard.setInterruptible(false);
		try {
			delegate.refresh();
		} finally {
			guard.setInterruptible(original);
		}
	}
	
	public void setDeviceSerialCd(String deviceSerialCd) {
		delegate.setDeviceSerialCd(deviceSerialCd);
	}

	public void setActionCode(ServerActionCode actionCode) {
		delegate.setActionCode(actionCode);
	}

	public void setActivationStatus(ActivationStatus activationStatus) {
		delegate.setActivationStatus(activationStatus);
	}

	public void setDeviceCharset(Charset deviceCharset) {
		delegate.setDeviceCharset(deviceCharset);
	}

	public void setDeviceType(DeviceType deviceType) {
		delegate.setDeviceType(deviceType);
	}

	public void setEncryptionKey(byte[] encryptionKey) {
		delegate.setEncryptionKey(encryptionKey);
	}

	public void setInitOnly(boolean initOnly) {
		delegate.setInitOnly(initOnly);
	}

	public void setLocale(String locale) {
		delegate.setLocale(locale);
	}

	public void setPreviousEncryptionKey(byte[] previousEncryptionKey) {
		delegate.setPreviousEncryptionKey(previousEncryptionKey);
	}

	public void setPropertyIndexesToRequest(String propertyIndexesToRequest) {
		delegate.setPropertyIndexesToRequest(propertyIndexesToRequest);
	}

	public void setTimeZoneGuid(String timeZoneGuid) {
		delegate.setTimeZoneGuid(timeZoneGuid);
	}
	
	public void setUsername(String username) {
		delegate.setUsername(username);
	}
	
	public void setPasswordHash(byte[] passwordHash) {
		delegate.setPasswordHash(passwordHash);
	}
	
	public void setPasswordSalt(byte[] passwordSalt) {
		delegate.setPasswordSalt(passwordSalt);
	}
	
	public void setCredentialTimestamp(long credentialTimestamp) {
		delegate.setCredentialTimestamp(credentialTimestamp);
	}
	
	public void setPreviousUsername(String previousUsername) {
		delegate.setPreviousUsername(previousUsername);
	}
	
	public void setPreviousPasswordHash(byte[] previousPasswordHash) {
		delegate.setPreviousPasswordHash(previousPasswordHash);
	}
	
	public void setPreviousPasswordSalt(byte[] previousPasswordSalt) {
		delegate.setPreviousPasswordSalt(previousPasswordSalt);
	}
	
	public void setNewUsername(String newUsername) {
		delegate.setNewUsername(newUsername);
	}
	
	public void setNewPasswordHash(byte[] newPasswordHash) {
		delegate.setNewPasswordHash(newPasswordHash);
	}
	
	public void setNewPasswordSalt(byte[] newPasswordSalt) {
		delegate.setNewPasswordSalt(newPasswordSalt);
	}
	
	public void setMaxAuthAmount(long maxAuthAmount) {
		delegate.setMaxAuthAmount(maxAuthAmount);
	}

	public void updateInternal(Map<String, Object> values, Map<String, Long> timestamp, Connection targetConn) throws ServiceException {
		boolean original = guard.setInterruptible(false);
		try {
			delegate.updateInternal(values, timestamp, targetConn);
		} finally {
			guard.setInterruptible(original);
		}
	}

	public UpdateIfResult updateMasterId(long masterId) {
		return delegate.updateMasterId(masterId);
	}

	public UpdateIfResult updateRejectUntil(long rejectUntil) {
		return delegate.updateRejectUntil(rejectUntil);
	}

	public boolean refreshSafely() {
		return delegate.refreshSafely();
	}

	public boolean isMasterIdIncrementOnly() {
		return delegate.isMasterIdIncrementOnly();
	}

	public void setMasterIdIncrementOnly(boolean masterIdIncrementOnly) {
		delegate.setMasterIdIncrementOnly(masterIdIncrementOnly);
	}

	public Integer getPropertyListVersion() {
		return delegate.getPropertyListVersion();
	}

	public void setPropertyListVersion(Integer propertyListVersion) {
		delegate.setPropertyListVersion(propertyListVersion);
	}

	public long getTimestamp(DeviceInfoProperty property) {
		return delegate.getTimestamp(property);
	}
}
