/** 
 * USA Technologies, Inc. 2011
 * QuartzScheduledService.java by phorsfield, Aug 26, 2011 4:45:53 PM
 */
package com.usatech.layers.common;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import simple.app.Publisher;
import simple.app.Service;
import simple.app.ServiceException;
import simple.app.ServiceStatusListener;
import simple.app.WorkQueue;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.ParallelInitializer;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.RunOnGetFuture;

/**
 * <p>The scheduled service for performing tasks periodically using Quartz.</p>
 * 
 * <p>Note: Quartz jobs are stored in a database</p>
 * 
 * Quartz will restore all jobs from the database
 * 
 * Therefore we only create new jobs for the database if they do not already exist.</p>
 * 
 * There will really only be one job type in the database we can deal with: <br />
 * 
 * <ul>
 * <li>recurring jobs</li>
 * </ul>
 * 
 * Others will have ran and completed.</p>
 * 
 * Quartz Properties Example 
 * <code>
simple.app.Service.DailyeSudsDiagnostic.class=com.usatech.layers.common.QuartzScheduledService
simple.app.Service.DailyeSudsDiagnostic.jobs.class=com.usatech.layers.common.QuartzScheduledJobMapping
simple.app.Service.DailyeSudsDiagnostic.initCycleTimeMillis=60000
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).class=com.usatech.posm.schedule.ESudsDiagnosticReport
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).cron= * * * * 0
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).group = group1
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).forceReset = true
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).jobDataMap(smtp.server)=localhost
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).jobDataMap(smtp.port)=26
simple.app.Service.DailyeSudsDiagnostic.jobs.job(eSudsDiagnostic).jobDataMap(from.address)=esuds@usatech.com
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.scheduler.instanceName) = QuartzTimeScheduler
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.scheduler.instanceId) = ${USAT.instanceId}
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.threadPool.class) = org.quartz.simpl.SimpleThreadPool
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.threadPool.threadCount) = 5
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.threadPool.threadPriority) = 5
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.misfireThreshold) = 60000
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.class)=org.quartz.impl.jdbcjobstore.JobStoreTX
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.driverDelegateClass)=org.quartz.impl.jdbcjobstore.oracle.OracleDelegate
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.useProperties)=false
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.dataSource)=myDS
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.tablePrefix)=QUARTZ.QRTZ_
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.jobStore.isClustered)=true
simple.app.Service.DailyeSudsDiagnostic.schedulerProps(org.quartz.dataSource.myDS.jndiURL)=java:/simple/jdbc/OPER

 * </code>
 * 
 * @author phorsfield
 *
 */
public class QuartzScheduledService implements Service {
	
	private static final Log log = Log.getLog();
	
	protected final String serviceName;
	protected SchedulerFactory factory;
	protected final Properties schedulerProps = new Properties();
	protected QuartzScheduledJobMapping jobs;

	// -- initializer protected variables
	protected Scheduler scheduler;

	// -- synchronized variables
	protected AtomicInteger threadPoolSize = new AtomicInteger(0); 
	
	protected Publisher<ByteInput> publisher;
	
	protected QuartzScheduledServiceMXBeanImpl quartzJMX;

	/**
	 * @return the number of milliseconds between attempts to start Quartz
	 */
	public long getInitCycleTimeMillis() {
		return this.initializer.getInitCycleTimeMillis();
	}

	/**
	 * @param initCycleTimeMillis the number of milliseconds between attempts to start Quartz
	 */
	public void setInitCycleTimeMillis(long initCycleTimeMillis) {
		this.initializer.setInitCycleTimeMillis(initCycleTimeMillis);
	}
	
	/**
	 * @return the number of milliseconds to allow shutdown of Quartz to proceed
	 */
	public long getResetBlockMillis() {
		return this.initializer.getResetBlockMillis();
	}

	/**
	 * @param resetBlockMillis the number of milliseconds to allow shutdown of Quartz to proceed
	 */
	public void setResetBlockMillis(long resetBlockMillis) {
		this.initializer.setResetBlockMillis(resetBlockMillis);
	}	
	/**
	 * @return the schedulerProps
	 */
	public Properties getSchedulerProps() {
		return schedulerProps;
	}

	/**
	 * @return the jobs
	 */
	public QuartzScheduledJobMapping getJobs() {
		return jobs;
	}

	/**
	 * @param jobs the jobs to set
	 */
	public void setJobs(QuartzScheduledJobMapping jobs) {
		this.jobs = jobs;		
	}
	
	protected final ParallelInitializer<SchedulerException> initializer = new ParallelInitializer<SchedulerException>() {
		@Override
		protected void doParallelInitialize() throws SchedulerException {
			Properties propMap = new Properties();
			propMap.putAll(schedulerProps);
			factory = new StdSchedulerFactory(propMap);
			scheduler = factory.getScheduler();
			@SuppressWarnings("unchecked")
			Set<JobKey> scheduledJobs = scheduler.getJobKeys(GroupMatcher.groupStartsWith(""));
			if(quartzJMX==null){
				quartzJMX=new QuartzScheduledServiceMXBeanImpl(jobs, scheduler);
			}else{
				quartzJMX.setJobMap(jobs);
				quartzJMX.setScheduler(scheduler);
			}
			
			if(publisher != null) {
				scheduler.setJobFactory(new QuartzPublisherJobFactory(publisher, quartzJMX));
			}
			
			Set<JobKey> configuredJobs = new HashSet<JobKey>();
			for (Map.Entry<String, QuartzCronScheduledJob> jobEntry : jobs.getJobMap().entrySet()) {
				configuredJobs.add(new JobKey(jobEntry.getKey(), jobEntry.getValue().getGroup()));
			}
			for (JobKey jobScheduled : scheduledJobs) {
				if (!configuredJobs.contains(jobScheduled))
					scheduler.deleteJob(jobScheduled);
			}

			for(Map.Entry<String, QuartzCronScheduledJob> jobEntry : jobs.getJobMap().entrySet()) {
				QuartzCronScheduledJob job = jobEntry.getValue();
				String jobName = jobEntry.getKey();
				String jobGroup = job.getGroup();
				JobKey jobKey = new JobKey(jobName, jobGroup);
				Trigger trigger = null;
				TriggerKey triggerKey = null;

				boolean createTrigger = false;
				boolean createJob = true;

				if(scheduler.checkExists(jobKey)) {
					// job exists, so now check trigger to see if it is scheduled
					triggerKey = new TriggerKey(jobName + "_trigger", jobGroup);
					trigger = scheduler.getTrigger(triggerKey);

					if(trigger != null) {
						// trigger exists
						if(job.isForceReset()) { // we are explicitly asked to remove it
							createTrigger = true;
						} else if(trigger instanceof CronTrigger) { // or, check if trigger of the matching type
							CronTrigger ct = (CronTrigger) trigger;
							if(!ct.getCronExpression().equalsIgnoreCase(job.getCron())) { // and, check that the trigger is the same Cron string
								// schedule does not match
								createTrigger = true;
							}
							// cron type and schedule match - leave alone
						} else {// or not a cron trigger, so reschedule
							createTrigger = true;
						}

						// trigger exists but needs to change, so unschedule
						if(createTrigger) {
							scheduler.unscheduleJob(triggerKey);
						}
					} else {
						// trigger does not exist, do not unschedule
						createTrigger = true;
					}

					// job exists, check if we are asked to recreate the job through properties
					if(job.isForceReset()) {
						scheduler.deleteJob(jobKey);
						createTrigger = true; // trigger is deleted when job is
					} else {
						// and check job Data Map to see if properties have been updated
						JobDetail currentJob = scheduler.getJobDetail(jobKey);
						if(currentJob.getJobDataMap().equals(buildJobDataMap(job.getJobDataMap()))) {
							// job is the same, leave it alone.
							createJob = false;
						} else {
							// properties have changed.
							scheduler.deleteJob(jobKey);
							createTrigger = true; // trigger is deleted when job is
						}
					}
				} else {
					// job does not exist
					createTrigger = true;
				}

				if(createTrigger) {
					try {
						job.initializeQuartz(jobName);
					} catch(ParseException e) {
						throw new SchedulerException("Error parsing CRON text for job named '" + jobName + "'", e);
					}
				}

				if(createTrigger && createJob) {
					JobDetail jobDetail = JobBuilder.newJob(job.getClass()).withIdentity(jobName, job.getGroup()).storeDurably().requestRecovery(false).usingJobData(buildJobDataMap(job.getJobDataMap())).build();
					scheduler.scheduleJob(jobDetail, job.getCronTrigger());
				} else if(createTrigger) {
					scheduler.scheduleJob(job.getCronTrigger());
				}
			}

			scheduler.start();
			threadPoolSize.compareAndSet(0, scheduler.getMetaData().getThreadPoolSize());
			threadPoolSize.compareAndSet(1, scheduler.getMetaData().getThreadPoolSize());
			log.info("Quartz Scheduler started with " + jobs.getJobMap().size() + " job" + (jobs.getJobMap().size() == 1 ? "." : "s."));
			
		}
		
		@Override
		protected void doParallelReset(){
			try {
				int tps = threadPoolSize.get();
				scheduler.shutdown(true);
				scheduler = null;
				threadPoolSize.compareAndSet(tps, 0);				
			} catch(SchedulerException e){
				log.warn("Failed to shutdown scheduler.", e);
			}			
		}
	};
	
	/**
	 *  Construct a QuartzScheduledService with a service name.
	 * @param serviceName
	 */
	public QuartzScheduledService(String serviceName) {
		this.serviceName = serviceName;
	}	
	
	/**
	 * Does not support this feature.
	 * @return false
	 */
	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		return false;
	}

	/**
	 * Get the number of working threads, there is only one thread for the ScheduledService,
	 * so 1 is always returned.
	 * @return number of threads
	 * @see simple.app.Service#getNumThreads()
	 */
	public int getNumThreads() {
		try {
			if(initializer.hasInitialized()) {
				return threadPoolSize.get();
			}
		} catch (InterruptedException e) {
			log.debug("Exception querying the number of threads in scheduler", e);
		} 
		return 0;
	}

	/**
	 *  Return the service name.
	 * 
	 * @see simple.app.Service#getServiceName()
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Pause the working thread of the scheduled service.
	 * @return number of threads paused
	 * @see simple.app.Service#pauseThreads()
	 */
	public int pauseThreads() throws ServiceException {
		log.info("Pause is not supported - we have no access to Quartz worker threads. Use 'quartz' beans in JMX to pause jobs.");
		return 0;
	}

	/**
	 * Does not support this feature.
	 * @return false
	 */
	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return false;
	}

	/**
	 * Attempts to start the scheduler if it has not already been started.
	 * @param timeout
	 * @return 0
	 */
	public int restartThreads(long timeout) throws ServiceException {
		try {
			initializer.reset();
			initializer.initialize();
			initializer.waitForInitialize(initializer.getInitCycleTimeMillis(), TimeUnit.MILLISECONDS);
			return threadPoolSize.get();
		} catch (InterruptedException e) {
			throw new ServiceException(e);
		} catch (SchedulerException e) {
			throw new ServiceException(e);
		}
	}

	/** Start the working thread of this ScheduledService.
	 * @param numThreads this parameter will be ignored.
	 * @return number of threads started.
	 * @see simple.app.Service#startThreads(int)
	 */
	public int startThreads(int numThreads) throws ServiceException {
		try {
			if(initializer.hasInitialized())
				return 0;
			if(initializer.initialize()) {
				threadPoolSize.compareAndSet(0, 1);
				return 1;
			}
			return 0;
		} catch (InterruptedException e) {
			throw new ServiceException(e);
		} catch (SchedulerException e) {
			throw new ServiceException(e);
		}		 
	}

	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		return new RunOnGetFuture<Integer>(new Callable<Integer>() {
	    	   public Integer call() {
				int ltp = threadPoolSize.get();
				if(initializer.reset()) {
					return ltp;
				}
				return 0;
			}
		});
	}
	
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		return stopAllThreads(force);
	}

	/**
	 * Stop the working thread.
	 * @param force this parameter will be ignored.
	 * @param timeout this parameter will be ignored.
	 * @return number of threads stopped.
	 * @see simple.app.Service#stopAllThreads(boolean, long)
	 */
	public int stopAllThreads(boolean force, long timeout) {
		try {
			return stopAllThreads(force).get(timeout, TimeUnit.MILLISECONDS);
		} catch (ServiceException e) {
			log.error("Error in stopAllThreads(force,timeout):",e);
			return 0;
		} catch (InterruptedException e) {
			log.error("Interrupted waiting for all threads in stopAllThreads(force,timeout):",e);
			return 0;
		} catch (ExecutionException e) {
			log.error("Error in stopAllThreads(force,timeout):",e);
			return 0;
		} catch (TimeoutException e) {
			log.error("Timeout while stopping all threads in stopAllThreads(force,timeout):",e);
			return 0;
		}
	}

	/**
	 * Stop specific number of threads, but 
	 * for the ScheduledService, all threads are stopped.
	 * 
	 * @param numThreads this parameter will be ignored.
	 * @param force this parameter will be ignored.
	 * @param timeout this parameter will be ignored.
	 * @return number of threads stopped.
	 * @see simple.app.Service#stopThreads(int, boolean, long)
	 */
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		int tps = threadPoolSize.get();
		if(initializer.reset()) {
			// reset returns true if and when reset() is called
			// if so, then it was responsible for triggering the
			// shutdown, which may have completed by now, so
			// we return the # threads active before reset.
			return tps;
		}
		return 0;
	}

	/**
	 * Feature unsupported;
	 * @return number of threads unpaused.
	 * 
	 * @see simple.app.Service#unpauseThreads()
	 */
	public int unpauseThreads() throws ServiceException {
		log.info("Unpause is not supported - we have no access to Quartz worker threads. Use 'quartz' beans in JMX to unpause jobs.");
		return 0;
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		return WorkQueue.TRUE_FUTURE;
	}

	/**
	 * @see simple.app.Service#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		return new ResultFuture<Boolean>() {
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				initializer.setResetBlockMillis(unit == null ? timeout : unit.toMillis(timeout));
				return initializer.reset();
			}
		};
	}

	public static JobDataMap buildJobDataMap(Properties jobDataMap) {
		JobDataMap jdm = new JobDataMap();
		for(Object key : jobDataMap.keySet()) {
			jdm.put(key, jobDataMap.get(key));
		}
		return jdm;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public QuartzScheduledServiceMXBeanImpl getQuartzJMX() {
		return quartzJMX;
	}

	public void setQuartzJMX(QuartzScheduledServiceMXBeanImpl quartzJMX) {
		this.quartzJMX = quartzJMX;
	}	
	
	
}
