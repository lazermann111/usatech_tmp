package com.usatech.layers.common;

import simple.app.Publisher;
import simple.io.ByteInput;

public abstract class QuartzCronScheduledWithPublisherJob extends QuartzCronScheduledJob{

	protected Publisher<ByteInput> publisher;
	
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
}
