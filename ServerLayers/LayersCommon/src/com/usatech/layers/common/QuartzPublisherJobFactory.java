package com.usatech.layers.common;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

import simple.app.Publisher;
import simple.io.ByteInput;
import simple.io.Log;
/**
 * Based on SimpleJobFactory which is the default. This one does the setPublisher if the method exists on the job.
 * @author yhe
 *
 */
public class QuartzPublisherJobFactory implements JobFactory {
	private static final Log log = Log.getLog();
	
	protected Publisher<ByteInput> publisher;
	protected QuartzScheduledServiceMXBean quartzJMX;
	

	public QuartzPublisherJobFactory(Publisher<ByteInput> publisher, QuartzScheduledServiceMXBean quartzJMX) {
		super();
		this.publisher = publisher;
		this.quartzJMX=quartzJMX;
	}

	public Job newJob(TriggerFiredBundle bundle, Scheduler Scheduler)
			throws SchedulerException {

		JobDetail jobDetail = bundle.getJobDetail();
		Class<? extends Job> jobClass = jobDetail.getJobClass();
		try {
			if (log.isDebugEnabled()) {
				log.debug("Producing instance of Job '" + jobDetail.getKey()
						+ "', class=" + jobClass.getName());
			}

			Job job=jobClass.newInstance();
			
			try{
				jobClass.getMethod("setPublisher", Publisher.class).invoke(job, publisher);
				jobClass.getMethod("setQuartxJMX", QuartzScheduledServiceMXBean.class).invoke(job, quartzJMX);
			}catch(Exception e){ //ignore if the method does not exist etc
			}
			
			return job;
		} catch (Exception e) {
			SchedulerException se = new SchedulerException(
					"Problem instantiating class '"
							+ jobDetail.getJobClass().getName() + "'", e);
			throw se;
		}
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	
	
}
