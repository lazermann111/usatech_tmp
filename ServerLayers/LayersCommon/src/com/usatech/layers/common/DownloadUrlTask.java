package com.usatech.layers.common;

import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.StringUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class DownloadUrlTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected final Cryption resultCryption = new Cryption();

	protected final static class LocalCookieHandler extends CookieHandler {
		protected final ThreadLocal<CookieManager> locals = new ThreadLocal<>();

		public void trackCookies() {
			CookieManager cm = locals.get();
			if(cm == null)
				locals.set(new CookieManager());
			else if(cm.getCookieStore() != null)
				cm.getCookieStore().removeAll();
		}

		@Override
		public Map<String, List<String>> get(URI uri, Map<String, List<String>> requestHeaders) throws IOException {
			CookieManager cm = locals.get();
			if(cm != null)
				return cm.get(uri, requestHeaders);
			return Collections.emptyMap();
		}

		@Override
		public void put(URI uri, Map<String, List<String>> responseHeaders) throws IOException {
			CookieManager cm = locals.get();
			if(cm != null)
				cm.put(uri, responseHeaders);
		}

		public void clearCookies() {
			CookieManager cm = locals.get();
			if(cm != null && cm.getCookieStore() != null)
				cm.getCookieStore().removeAll();
		}
	}

	protected static final LocalCookieHandler COOKIE_HANDLER = new LocalCookieHandler();
	static {
		CookieHandler.setDefault(COOKIE_HANDLER);
	}
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		URL[] urls;
		String[] methods;
		String successRegex;
		final int validationLength;
		try {
			urls = step.getAttribute(CommonAttrEnum.ATTR_URLS, URL[].class, true);
			methods = step.getAttribute(CommonAttrEnum.ATTR_METHODS, String[].class, false);
			successRegex = step.getAttribute(CommonAttrEnum.ATTR_VALIDATION_REGEX, String.class, false);
			int tmp = step.getAttributeDefault(CommonAttrEnum.ATTR_VALIDATION_LENGTH, Integer.class, 512);
			if(tmp > 0)
				validationLength = tmp;
			else
				validationLength = Integer.MAX_VALUE;
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Invalid or missing urls", e, WorkRetryType.NO_RETRY);
		}
		if(urls.length == 0)
			throw new RetrySpecifiedServiceException("Missing urls", WorkRetryType.NO_RETRY);
		boolean sensitive;
		long prevModified;
		try {
			sensitive = step.getAttributeDefault(CommonAttrEnum.ATTR_SENSITIVE, Boolean.class, true);
			prevModified = step.getAttributeDefault(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, Long.class, 0L);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not convert sensitive attribute", e, WorkRetryType.NO_RETRY);
		}
		if(getResourceFolder() == null) {
			log.error("ResourceFolder is not set on " + this);
			return 2;
		}
		byte[] encryptionKey;
		if(sensitive)
			try {
				encryptionKey = getResultCryption().generateKey();
			} catch(GeneralSecurityException e) {
				throw new ServiceException(e);
			}
		else
			encryptionKey = null;
		boolean okay = false;
		URL lastUrl = urls[urls.length - 1];
		Resource resource;
		try {
			resource = getResourceFolder().getResource(lastUrl.getPath(), ResourceMode.CREATE);
		} catch(IOException e) {
			throw new RetrySpecifiedServiceException("Could not write resource", e, WorkRetryType.BLOCKING_RETRY);
		}
		try {
			OutputStream out;
			if(encryptionKey != null)
				out = getResultCryption().createEncryptingOutputStream(encryptionKey, resource.getOutputStream());
			else
				out = resource.getOutputStream();
			COOKIE_HANDLER.trackCookies();
			URLConnection uconn = null;
			long time = System.currentTimeMillis();
			for(int i = 0; i < urls.length; i++) {
				try {
					uconn = urls[i].openConnection();
					if(uconn instanceof HttpURLConnection) {
						HttpURLConnection huc = (HttpURLConnection) uconn;
						if(methods != null && methods.length > i && !StringUtils.isBlank(methods[i]))
							huc.setRequestMethod(methods[i].trim().toUpperCase());
					}
					uconn.getInputStream(); // really connect
				} catch(IOException e) {
					throw new RetrySpecifiedServiceException("Could not connect to '" + lastUrl.toString() + "'", e, WorkRetryType.NONBLOCKING_RETRY);
				}
			}
			long lastModified = uconn.getLastModified();
			if(lastModified == 0)
				lastModified = time;
			int supposedlength = uconn.getContentLength();
			if(prevModified > 0 && prevModified >= lastModified) {
				log.info("Url '" + lastUrl.toString() + "' of " + supposedlength + " bytes has not been modified since the last time it was downloaded (" + new Date(prevModified) + "). Last modified at " + new Date(lastModified));
				return 1;
			}
			long length;
			log.info("Downloading '" + lastUrl.toString() + "' of " + supposedlength + " bytes last modified at " + new Date(lastModified) + "...");
			InputStream in;
			try {
				in = uconn.getInputStream();
				if(!StringUtils.isBlank(successRegex)) {
					// figure out how much to read to check regex
					final Pattern successPattern = Pattern.compile(successRegex);
					in = new FilterInputStream(in) {
						protected final ByteArrayOutputStream buffer = new ByteArrayOutputStream(512);
						protected boolean success = false;
						@Override
						public int read() throws IOException {
							int ch = super.read();
							if(ch >= 0 && !success) {
								buffer.write(ch);
								check();
							}
							return ch;
						}

						@Override
						public int read(byte[] b, int off, int len) throws IOException {
							int count = super.read(b, off, len);
							if(count > 0 && !success) {
								buffer.write(b, off, count);
								check();
							}
							return count;
						}

						@Override
						public void close() throws IOException {
							super.close();
							if(!success)
								validate();
						}

						protected void check() throws IOException {
							if(buffer.size() >= validationLength)
								validate();
						}
						protected void validate() throws IOException {
							String check = buffer.toString();
							if(successPattern.matcher(check).matches())
								success = true;
							else {
								StringBuilder msg = new StringBuilder((check.length() > 512 ? 515 : check.length()) + 50);
								msg.append("Downloaded data does not match regex: '");
								if(check.length() > 512)
									msg.append(check, 0, 512).append("...");
								else
									msg.append(check);
								msg.append('\'');
								throw new IOException(msg.toString());
							}
						}
					};
				}
				try {
					length = IOUtils.copy(in, out);
				} finally {
					in.close();
				}
			} catch(IOException e) {
				throw new RetrySpecifiedServiceException("Could not read from '" + lastUrl.toString() + "'", e, WorkRetryType.NONBLOCKING_RETRY);
			}
			out.flush();
			log.info("Downloaded " + length + " bytes from '" + lastUrl.toString() + "'");
			if(encryptionKey != null) {
				step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resource.getKey());
				step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
				step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, getResultCryption().getCipherName());
				step.setResultAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, getResultCryption().getBlockSize());
			} else {
				step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
			}
			step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, lastModified);
			step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE_LENGTH, length);
			okay = true;
		} catch(IOException e) {
			throw new RetrySpecifiedServiceException("Could not write resource", e, WorkRetryType.NONBLOCKING_RETRY);
		} catch(GeneralSecurityException e) {
			throw new RetrySpecifiedServiceException("Could encrypt contents", e, WorkRetryType.NONBLOCKING_RETRY);
		} finally {
			if(!okay)
				resource.delete();
			resource.release();
			COOKIE_HANDLER.clearCookies();
		}
		return 0;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public Cryption getResultCryption() {
		return resultCryption;
	}
}
