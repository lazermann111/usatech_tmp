package com.usatech.layers.common;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.messagedata.MessageData;

public class AppLayerOnlineMessage extends AbstractAppLayerMessage implements Message, DeviceInfoUpdateListener {
	protected final DeviceInfoManager deviceInfoManager;
	protected DeviceInfo newDeviceInfo;
	protected final DeviceInfo deviceInfo;
	protected boolean deviceInfoUpdated;
	protected int replyCount = 0;
	protected final List<String> replyLog = new ArrayList<String>(2);
	protected final ResourceFolder resourceFolder;
	protected final String outboundFileTransferDirectory;
	protected final ByteBuffer replyBuffer;
	protected final Long lastCommandId;
	protected final TranslatorFactory translatorFactory;
	protected final String translatorContext;
	protected Translator translator;

	public AppLayerOnlineMessage(MessageChainTaskInfo taskInfo, final DeviceInfoManager deviceInfoManager, ByteBuffer replyBuffer, TranslatorFactory translatorFactory, String translatorContext, final ResourceFolder resourceFolder, final String outboundFileTransferDirectory) throws ServiceException, AttributeConversionException {
		super(taskInfo);
		this.translatorFactory = translatorFactory;
		this.translatorContext = translatorContext;
		this.deviceInfoManager = deviceInfoManager;
		this.resourceFolder = resourceFolder;
		this.outboundFileTransferDirectory = outboundFileTransferDirectory;
		MessageChainStep step = taskInfo.getStep();
		this.lastCommandId = step.getAttribute(MessageAttrEnum.ATTR_LAST_COMMAND_ID, Long.class, false);
		this.replyBuffer = replyBuffer;
		if(replyBuffer != null) {
			replyBuffer.clear();
			replyBuffer.flip();
		}
		this.deviceInfo = deviceInfoManager.getDeviceInfo(deviceName, OnMissingPolicy.RETURN_NULL, false, taskInfo, false);
		if(this.deviceInfo instanceof AppLayerDeviceInfo) {
			((AppLayerDeviceInfo) this.deviceInfo).addDeviceInfoListener(this);
		}
		Long netlayerMasterId;
		if(deviceInfo != null && (netlayerMasterId = step.getAttribute(DeviceInfoProperty.MASTER_ID, Long.class, false)) != null)
			deviceInfo.updateMasterId(netlayerMasterId);
		String netLayerDeviceInfoHash = taskInfo.getStep().getAttribute(MessageAttrEnum.ATTR_DEVICE_INFO_HASH, String.class, false);
		if(netLayerDeviceInfoHash != null && deviceInfo != null && !netLayerDeviceInfoHash.equals(deviceInfo.getDeviceInfoHash()))
			this.propertiesUpdated(null, null);
	}

	public void propertiesUpdated(Map<String, Object> updatedProperties, Map<String, Long> timestamps) {
		deviceInfoUpdated = true;
	}

	@Override
	public Translator getTranslator() {
		if(translator == null) {
			if(translatorFactory == null)
				translator = DefaultTranslatorFactory.getTranslatorInstance();
			else {
				Locale locale = ConvertUtils.convertSafely(Locale.class, deviceInfo.getLocale(), Locale.getDefault());
				try {
					translator = translatorFactory.getTranslator(locale, translatorContext);
				} catch(ServiceException e) {
					getLog().warn("Could not get translations", e);
					translator = DefaultTranslatorFactory.getTranslatorInstance();
				}
			}
		}
		return translator;
	}

	@Override
	public Long getLastCommandId() {
		return lastCommandId;
	}

	/**
	 * @see com.usatech.layers.common.Message#getNewDeviceInfo(java.lang.String)
	 */
	@Override
	public DeviceInfo getNewDeviceInfo(String deviceName) throws ServiceException {
		if(newDeviceInfo != null) {
			if(!deviceName.equals(newDeviceInfo.getDeviceName()))
				throw new ServiceException("Message.getNewDeviceInfo() was called twice in a task");
		} else {
			newDeviceInfo = deviceInfoManager.getDeviceInfo(deviceName, OnMissingPolicy.CREATE_NEW, false, taskInfo, false);
			if(newDeviceInfo instanceof AppLayerDeviceInfo) {
				((AppLayerDeviceInfo) newDeviceInfo).addDeviceInfoListener(this);
			}
		}
		return newDeviceInfo;
	}

	public int complete() {
		int updates = 0;
		if(newDeviceInfo != null) {
			resultAttributes.put("newDeviceName", newDeviceInfo.getDeviceName());
			newDeviceInfo.putAllWithTimestamps(resultAttributes);
		} else if(deviceInfoUpdated)
			deviceInfo.putAllWithTimestamps(resultAttributes);
		else
			updates = deviceInfo.putPendingWithTimestamp(resultAttributes, serverTime);

		if(messageLog.isInfoEnabled()) {
			long endTime = System.currentTimeMillis();
			StringBuilder sb = new StringBuilder();
			sb.append("Finished message [in ").append(endTime - startTime).append(" ms; received ").append(endTime - serverTime).append(" ms ago] ").append(data);
			switch(replyLog.size()) {
				case 0:
					sb.append(" with NO reply");
					break;
				case 1:
					sb.append(" with ").append(replyLog.get(0));
					break;
				default:
					sb.append(" with ").append(replyLog);
					break;
			}
			messageLog.info(sb.toString());
		}
		return updates > 0 || deviceInfoUpdated ? 5 : 0;
	}

	/**
	 * @see com.usatech.layers.common.Message#sendReply(com.usatech.layers.common.messagedata.MessageData)
	 */
	@Override
	public void sendReply(MessageData... replyData) throws ServiceException {
		int cnt = 0;
		if(replyData != null && replyData.length > 0) {
			replyBuffer.compact();
			for(MessageData rd : replyData) {
				if(rd != null) {
					try {
						rd.writeData(replyBuffer, false);
					} catch(BufferOverflowException e) {
						throw new ServiceException("Reply Buffer is too small", e);
					} catch(IllegalStateException e) {
						throw new ServiceException("Reply Data is not properly prepared", e);
					}
					if(getLog().isInfoEnabled()) {
						String rdStr = rd.toString();
						getLog().info("Sending reply " + rdStr);
						replyLog.add("reply " + rdStr);
					}
					cnt++;
				}
			}
			replyBuffer.flip();
			resultAttributes.put("reply", replyBuffer);
			replyCount += cnt;
		}
		if(cnt == 0) {
			if(getDeviceType() == DeviceType.EDGE)
				throw new ServiceException("Edge devices are half-duplex and cannot handle no reply");
			if(getLog().isInfoEnabled())
				getLog().info("Sending no reply");
		}
	}

	public void sendReplyTemplate(MessageData replyData) throws ServiceException {
		replyBuffer.clear();
		try {
			replyData.writeData(replyBuffer, false);
		} catch(BufferOverflowException e) {
			throw new ServiceException("Reply Buffer is too small", e);
		} catch(IllegalStateException e) {
			throw new ServiceException("Reply Data is not properly prepared", e);
		}
		replyBuffer.flip();
		resultAttributes.put("replyTemplate", replyBuffer);
		if(getLog().isInfoEnabled()) {
			String rdStr = replyData.toString();
			getLog().info("Sending reply template " + rdStr);
			replyLog.add("reply template " + rdStr);
		}
	}

	public int getReplyCount() {
		return replyCount;
	}

	@Override
	public Publisher<ByteInput> getPublisher() {
		return taskInfo.getPublisher();
	}
	
	@Override
	public void setResultAttribute(String name, Object value) {
		resultAttributes.put(name, value);
	}

	@Override
	public void setSessionAttribute(String name, Object value) {
		String key = ProcessingConstants.SESSION_ATTRIBUTE_PREFIX + name;
		resultAttributes.put(key, value);
	}

	@Override
	public void setNextCommandId(long commandId) {
		resultAttributes.put("commandId", commandId);
	}

	@Override
	public Resource addFileTransfer(boolean useResource, long fileTransferId, long pendingCommandId, int fileGroup, String fileName, int fileTypeId, int filePacketSize, long fileSize) throws ServiceException {
		if(useResource && resourceFolder == null)
			throw new ServiceException("ResourceFolder is not provided");
		if(fileName == null || (fileName = fileName.trim()).length() == 0)
			fileName = "UNKNOWN-" + fileTransferId;
		Resource resource = null;
		if (useResource) {
			try {
				resource = resourceFolder.getResource(outboundFileTransferDirectory + "/type_" + fileTypeId + "/" + fileName, ResourceMode.CREATE);
			} catch(IOException e) {
				throw new ServiceException("Could not create resource", e);
			}
			resultAttributes.put("fileTransferKey", resource.getKey());
		}
		resultAttributes.put("fileTransferId", fileTransferId);
		resultAttributes.put("pendingCommandId", pendingCommandId);
		resultAttributes.put("fileGroup", fileGroup);
		resultAttributes.put("filePacketSize", filePacketSize);
		resultAttributes.put("fileType", fileTypeId);
		resultAttributes.put("fileSize", fileSize);
		resultAttributes.put("fileName", fileName);
		return resource;
	}

	@Override
	public DeviceInfo getDeviceInfo() throws ServiceException {
		return deviceInfo;
	}
}
