package com.usatech.layers.common;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.util.Map;

import simple.app.ServiceException;
import simple.util.IntegerRangeSet;

import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;

public class NoUpdateDeviceInfo implements DeviceInfo {
	protected final String deviceName;
	protected final String deviceSerialCd;	
	protected final byte[] encryptionKey;
	protected final byte[] previousEncryptionKey;
	protected final long masterId;
	protected final long rejectUntil;
	protected final DeviceType deviceType;
	protected final String locale;
	protected boolean initOnly;
	protected final Charset deviceCharset;
	protected final ActivationStatus activationStatus;
	protected final String timeZoneGuid;
	protected final ServerActionCode actionCode;
	protected final IntegerRangeSet propertyIndexesToRequest;
	protected final String username;
	protected final byte[] passwordHash;
	protected final byte[] passwordSalt;
	protected final long credentialTimestamp;
	protected final String previousUsername;
	protected final byte[] previousPasswordHash;
	protected final byte[] previousPasswordSalt;
	protected final String newUsername;
	protected final byte[] newPasswordHash;
	protected final byte[] newPasswordSalt;
	protected final long maxAuthAmount;
	protected final Integer propertyListVersion;
	protected final boolean masterIdIncrementOnly;
	
	public NoUpdateDeviceInfo(String deviceName, String deviceSerialCd, byte[] encryptionKey, byte[] previousEncryptionKey, long masterId, long rejectUntil, DeviceType deviceType, String locale, boolean initOnly,
			Charset deviceCharset, ActivationStatus activationStatus, String timeZoneGuid, ServerActionCode actionCode, IntegerRangeSet propertyIndexesToRequest, String username, byte[] passwordHash, byte[] passwordSalt, long credentialTimestamp,
			String previousUsername, byte[] previousPasswordHash, byte[] previousPasswordSalt, String newUsername, byte[] newPasswordHash, byte[] newPasswordSalt, long maxAuthAmount, Integer propertyListVersion, boolean masterIdIncrementOnly) {
		super();
		this.deviceName = deviceName;
		this.deviceSerialCd = deviceSerialCd;
		this.encryptionKey = encryptionKey;
		this.previousEncryptionKey = previousEncryptionKey;
		this.masterId = masterId;
		this.rejectUntil = rejectUntil;
		this.deviceType = (deviceType == null ? DeviceType.DEFAULT : deviceType);
		this.locale = locale;
		this.initOnly = initOnly;
		this.deviceCharset = (deviceCharset == null ? ProcessingConstants.US_ASCII_CHARSET : deviceCharset);
		this.activationStatus = (activationStatus == null ? ActivationStatus.FACTORY : activationStatus);
		this.timeZoneGuid = timeZoneGuid;
		this.actionCode = (actionCode == null ? ServerActionCode.NO_ACTION : actionCode);
		this.propertyIndexesToRequest = propertyIndexesToRequest;
		this.username = username;
		this.passwordHash = passwordHash;
		this.passwordSalt = passwordSalt;
		this.credentialTimestamp = credentialTimestamp;
		this.previousUsername = previousUsername;
		this.previousPasswordHash = previousPasswordHash;
		this.previousPasswordSalt = previousPasswordSalt;
		this.newUsername = newUsername;
		this.newPasswordHash = newPasswordHash;
		this.newPasswordSalt = newPasswordSalt;
		this.maxAuthAmount = maxAuthAmount;
		this.propertyListVersion = propertyListVersion;
		this.masterIdIncrementOnly = masterIdIncrementOnly;
	}

	public void clearChanges() {
	}

	public void commitChanges(long timestamp) throws ServiceException {
	}

	public void commitChanges(long timestamp, boolean internalOnly) throws ServiceException {
	}
	public String getDeviceName() {
		return deviceName;
	}

	public String getDeviceSerialCd() {
		return deviceSerialCd;
	}	

	public byte[] getEncryptionKey() {
		return encryptionKey;
	}

	public byte[] getPreviousEncryptionKey() {
		return previousEncryptionKey;
	}

	public long getMasterId() {
		return masterId;
	}

	public long getRejectUntil() {
		return rejectUntil;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public String getLocale() {
		return locale;
	}

	public boolean isInitOnly() {
		return initOnly;
	}

	public Charset getDeviceCharset() {
		return deviceCharset;
	}

	public ActivationStatus getActivationStatus() {
		return activationStatus;
	}

	public String getTimeZoneGuid() {
		return timeZoneGuid;
	}

	public ServerActionCode getActionCode() {
		return actionCode;
	}

	public IntegerRangeSet getPropertyIndexesToRequest() {
		return propertyIndexesToRequest;
	}
	
	public String getUsername() {
		return username;
	}
	
	public byte[] getPasswordHash() {
		return passwordHash;
	}
	
	public byte[] getPasswordSalt() {
		return passwordSalt;
	}
	
	public long getCredentialTimestamp() {
		return credentialTimestamp;
	}
	
	public String getPreviousUsername() {
		return previousUsername;
	}
	
	public byte[] getPreviousPasswordHash() {
		return previousPasswordHash;
	}
	
	public byte[] getPreviousPasswordSalt() {
		return previousPasswordSalt;
	}
	
	public String getNewUsername() {
		return newUsername;
	}
	
	public byte[] getNewPasswordHash() {
		return newPasswordHash;
	}
	
	public byte[] getNewPasswordSalt() {
		return newPasswordSalt;
	}
	
	public long getMaxAuthAmount() {
		return maxAuthAmount;
	}

	@Override
	public void putAll(Map<String, Object> values, Map<String, Long> timestamps) {
		putAllWithTimestamps(values);
	}

	@Override
	public void putAllWithTimestamps(Map<String, Object> values) {
		values.put(DeviceInfoProperty.ENCRYPTION_KEY.getAttributeKey(), getEncryptionKey());
		values.put(DeviceInfoProperty.PREVIOUS_ENCRYPTION_KEY.getAttributeKey(), getPreviousEncryptionKey()); 
		values.put(DeviceInfoProperty.REJECT_UNTIL.getAttributeKey(), getRejectUntil());
		values.put(DeviceInfoProperty.DEVICE_TYPE.getAttributeKey(), getDeviceType());
		values.put(DeviceInfoProperty.LOCALE.getAttributeKey(), getLocale());
		values.put(DeviceInfoProperty.INIT_ONLY.getAttributeKey(), isInitOnly());
		values.put(DeviceInfoProperty.DEVICE_CHARSET.getAttributeKey(), getDeviceCharset());
		values.put(DeviceInfoProperty.TIME_ZONE_GUID.getAttributeKey(), getTimeZoneGuid());
		values.put(DeviceInfoProperty.ACTIVATION_STATUS.getAttributeKey(), getActivationStatus());
		values.put(DeviceInfoProperty.ACTION_CODE.getAttributeKey(), getActionCode());
		values.put(DeviceInfoProperty.MASTER_ID.getAttributeKey(), getMasterId());
		values.put(DeviceInfoProperty.PROPERTY_INDEXES_TO_REQUEST.getAttributeKey(), getPropertyIndexesToRequest());
		values.put(DeviceInfoProperty.USERNAME.getAttributeKey(), getUsername());
		values.put(DeviceInfoProperty.PASSWORD_HASH.getAttributeKey(), getPasswordHash());
		values.put(DeviceInfoProperty.PASSWORD_SALT.getAttributeKey(), getPasswordSalt());
		values.put(DeviceInfoProperty.PREVIOUS_USERNAME.getAttributeKey(), getPreviousUsername());
		values.put(DeviceInfoProperty.PREVIOUS_PASSWORD_HASH.getAttributeKey(), getPreviousPasswordHash());
		values.put(DeviceInfoProperty.PREVIOUS_PASSWORD_SALT.getAttributeKey(), getPreviousPasswordSalt());
		values.put(DeviceInfoProperty.MAX_AUTH_AMOUNT.getAttributeKey(), getMaxAuthAmount());
	}

	public void putWithTimestamp(DeviceInfoProperty dip, Map<String, Object> values) {
		values.put(dip.getAttributeKey(), dip.getProperty(this));
	}

	@Override
	public int putPendingWithTimestamp(Map<String, Object> values, long timestamp) {
		// Can't change so nothing is pending
		return 0;
	}

	@Override
	public void refresh() throws ServiceException {
	}

	@Override
	public boolean refreshSafely() {
		return true;
	}
	
	@Override
	public void setDeviceSerialCd(String deviceSerialCd) {
	}
	
	@Override
	public void setActionCode(ServerActionCode actionCode) {
	}

	@Override
	public void setActivationStatus(ActivationStatus activationStatus) {
	}

	@Override
	public void setDeviceCharset(Charset deviceCharset) {
	}

	@Override
	public void setDeviceType(DeviceType deviceType) {
	}

	@Override
	public void setEncryptionKey(byte[] encryptionKey) {
	}

	@Override
	public void setInitOnly(boolean initOnly) {
	}

	@Override
	public void setLocale(String locale) {
	}

	@Override
	public void setPreviousEncryptionKey(byte[] previousEncryptionKey) {
	}

	@Override
	public void setPropertyIndexesToRequest(String propertyIndexesToRequest) {
	}

	@Override
	public void setTimeZoneGuid(String timeZoneGuid) {
	}
	
	@Override
	public void setUsername(String username) {
	}
	
	@Override
	public void setPasswordHash(byte[] passwordHash) {
	}
	
	@Override
	public void setPasswordSalt(byte[] passwordSalt) {
	}
	
	@Override
	public void setCredentialTimestamp(long credentialTimestamp) {
	}
	
	@Override
	public void setPreviousUsername(String previousUsername) {
	}
	
	@Override
	public void setPreviousPasswordHash(byte[] previousPasswordHash) {
	}
	
	@Override
	public void setPreviousPasswordSalt(byte[] previousPasswordSalt) {
	}
	
	@Override
	public void setNewUsername(String newUsername) {
	}
	
	@Override
	public void setNewPasswordHash(byte[] newPasswordHash) {
	}
	
	@Override
	public void setNewPasswordSalt(byte[] newPasswordSalt) {
	}
	
	@Override
	public void setMaxAuthAmount(long maxAmount) {
	}

	@Override
	public void updateInternal(Map<String, Object> values, Map<String, Long> timestamp, Connection targetConn) throws ServiceException {
	}

	@Override
	public UpdateIfResult updateMasterId(long masterId) {
		return UpdateIfResult.NO_CHANGE;
	}

	@Override
	public UpdateIfResult updateRejectUntil(long rejectUntil) {
		return UpdateIfResult.NO_CHANGE;
	}

	@Override
	public String getDeviceInfoHash() {
		return null;
	}

	public Integer getPropertyListVersion() {
		return propertyListVersion;
	}

	public boolean isMasterIdIncrementOnly() {
		return masterIdIncrementOnly;
	}

	@Override
	public void setMasterIdIncrementOnly(boolean masterIdIncrementOnly) {
	}

	@Override
	public void setPropertyListVersion(Integer propertyListVersion) {
	}

	public long getTimestamp(DeviceInfoProperty property) {
		return 0;
	}
}
