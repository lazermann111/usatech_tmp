package com.usatech.layers.common;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import simple.app.ServiceException;
import simple.io.Log;
import simple.monitor.sftp.KnownHostsVerifier;
import simple.text.StringUtils;

import com.trilead.ssh2.Connection;
import com.trilead.ssh2.InteractiveCallback;
import com.trilead.ssh2.SFTPv3Client;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * The task uses SecureFTP to manipulate files on a host
 * 
 * @author bkrug
 * 
 */
public abstract class AbstractSftpTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected KnownHostsVerifier hostKeyVerification;
	protected int connectTimeout = 15000;
	// setting kexTimeout to 10000 caused "java.net.SocketTimeoutException: The kexTimeout (10000 ms) expired." in the middle of file transfer
	protected int kexTimeout = 0;
	protected int bufferSize = 8 * 1024;
	protected int maxRetryAllowed = 10;
	protected boolean useCompression = false;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		return processInternal(taskInfo);
	}

	protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		String host;
		int port;
		String username;
		final String password;
		MessageChainStep step = taskInfo.getStep();
		try {
			host = step.getAttribute(CommonAttrEnum.ATTR_HOST, String.class, true);
			port = step.getAttribute(CommonAttrEnum.ATTR_PORT, Integer.class, true);
			username = step.getAttribute(CommonAttrEnum.ATTR_USERNAME, String.class, true);
			password = step.getAttribute(CommonAttrEnum.ATTR_PASSWORD, String.class, false);
		} catch(AttributeConversionException e) {
			log.error("Could not convert attributes", e);
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, StringUtils.exceptionToString(e));
			return 2;
		}
		try {
			Connection connection = new Connection(host, port);
			try {
				// Connect to the host
				long connectionStart = System.currentTimeMillis();
				connection.setCompression(useCompression);
				connection.connect(hostKeyVerification, connectTimeout, kexTimeout);
				log.info("Connection to " + host + ":" + port + " took " + (System.currentTimeMillis() - connectionStart) + " ms");
				// authenticate
				String[] methods = connection.getRemainingAuthMethods(username);
				boolean authenticated = false;
				if(methods != null)
					for(String method : methods) {
						switch(method) {
							case "keyboard-interactive":
								if(!StringUtils.isBlank(password))
									authenticated = connection.authenticateWithKeyboardInteractive(username, new InteractiveCallback() {
										@Override
										public String[] replyToChallenge(String name, String instruction, int numPrompts, String[] prompt, boolean[] echo) throws Exception {
											switch(numPrompts) {
												case 1:
													return new String[] { password };
												case 0:
													return new String[0];
												default:
													return new String[numPrompts];
											}
										}
									});
								break;
							case "password":
								if(!StringUtils.isBlank(password))
									authenticated = connection.authenticateWithPassword(username, password);
								break;
							case "publickey":
								File privateKeyFile = new File(System.getProperty("user.home", "."), ".ssh/id_rsa");
								long len;
								if(privateKeyFile.canRead() && (len = privateKeyFile.length()) > 0 && len <= 1024 * 1024) {
									char[] privateKey = new char[(int) len];
									int tot = 0;
									FileReader reader = new FileReader(privateKeyFile);
									try {
										for(int r = 0; r >= 0 && tot < privateKey.length; tot += r)
											r = reader.read(privateKey, tot, privateKey.length - tot);
									} finally {
										reader.close();
									}
									authenticated = connection.authenticateWithPublicKey(username, privateKey, password);
								} else {
									log.warn("Could not read private key from '" + privateKeyFile.getAbsolutePath() + "' because " + (privateKeyFile.canRead() ? " file size is " + privateKeyFile.length() : " file cannot be read"));
								}
								break;
						}
						if(authenticated)
							break;
					}
				if(!authenticated)
					authenticated = connection.authenticateWithNone(username);
				if(!authenticated)
					throw new ServiceException("Could not log into " + host + ':' + port + " using username '" + username + "' and password of " + (password == null ? 0 : password.length()) + " characters");
				// The connection is authenticated we can now do some real work!
				SFTPv3Client sftp = new SFTPv3Client(connection);
				try {
					return processSftp(taskInfo, sftp);
				} finally {
					sftp.close();
				}
			} catch(IOException e) {
				throw new ServiceException(e);
			}
		} catch(ServiceException e) {
			if(maxRetryAllowed > 0 && taskInfo.getRetryCount() >= maxRetryAllowed) {
				log.info("Reach maxRetryAllowed=" + maxRetryAllowed, e);
				step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, StringUtils.exceptionToString(e));
				return 2;
			}
			throw e;
		}
	}

	protected abstract int processSftp(MessageChainTaskInfo taskInfo, SFTPv3Client sftp) throws IOException, ServiceException;

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getKexTimeout() {
		return kexTimeout;
	}

	public void setKexTimeout(int kexTimeout) {
		this.kexTimeout = kexTimeout;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}
	
	public boolean isUseCompression() {
		return useCompression;
	}

	public void setUseCompression(boolean useCompression) {
		this.useCompression = useCompression;
	}

	public int getMaxRetryAllowed() {
		return maxRetryAllowed;
	}

	public void setMaxRetryAllowed(int maxRetryAllowed) {
		this.maxRetryAllowed = maxRetryAllowed;
	}

	public String getKnownHostsFile() {
		return hostKeyVerification == null ? null : hostKeyVerification.getKnownHostsFile().getPath();
	}

	public void setKnownHostsFile(String knownHostsFile) throws IOException {
		hostKeyVerification = new KnownHostsVerifier(new File(knownHostsFile));
	}

}
