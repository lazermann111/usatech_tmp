package com.usatech.layers.common;

import simple.text.StringUtils;

import com.usatech.layers.common.constants.CRCTypeLegacy;

public class CRCLegacy {
	protected final CRCTypeLegacy crcType;
	protected final byte[] crcValue;

	public CRCLegacy(CRCTypeLegacy crcType, byte[] crcValue) {
		super();
		this.crcType = crcType;
		this.crcValue = crcValue;
	}
	public CRCTypeLegacy getCrcType() {
		return crcType;
	}
	public byte[] getCrcValue() {
		return crcValue;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return crcType.toString() + " [0x" + StringUtils.toHex(crcValue) + "]";
	}
}
