package com.usatech.layers.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import simple.io.Base64;
import simple.io.IOUtils;
import simple.io.Log;
import simple.text.StringUtils;

public class HttpRequester {
	private static final Log log = Log.getLog();
	
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String JSON_VERSION = ";version=1.0";
	public static final String CONTENT_TYPE_ANY = "*/*";
	protected HostnameVerifier hostNameVerifier;
	protected final PoolingHttpClientConnectionManager httpConnectionManager;
	protected HttpClient httpClient;
	protected int connectionTimeout;
	protected int socketTimeout;
	protected boolean includeJsonVersion = true;
	protected int bufferSize = 2048;
	protected Gson gson = new Gson();
	
	public static interface Authenticator {
		public void addAuthentication(HttpRequestBase method) throws HttpException;
	}

	public static class BasicAuthenticator implements Authenticator {
		protected final String headerValue;

		public BasicAuthenticator(String username, String password) {
			this.headerValue = new StringBuilder("Basic ").append(Base64.encodeString(username + ":" + password, true)).toString();
		}

		@Override
		public void addAuthentication(HttpRequestBase method) throws HttpException {
			method.addHeader("Authorization", headerValue);
		}
	}

	public static class TokenAuthenticator implements Authenticator {
		protected final String headerValue;

		public TokenAuthenticator(String token) {
			this.headerValue = new StringBuilder("Token ").append(Base64.encodeString(token, true)).toString();
		}

		@Override
		public void addAuthentication(HttpRequestBase method) throws HttpException {
			method.addHeader("Authorization", headerValue);
		}
	}
	
	public static class HttpResponseData {
		private final int httpStatusCode;
		private Map<String, String> headers = new HashMap<>();
		private String contentType;
		private String rawResponse;
		private Map<String, Object> jsonResponse;
		private HttpException exception;
		
		public HttpResponseData(int httpStatusCode) {
			this.httpStatusCode = httpStatusCode;
		}
		
		public int getHttpStatusCode() {
			return httpStatusCode;
		}
		
		public Map<String, String> getHeaders() {
			return headers;
		}
		
		public void addHeader(String name, String value) {
			headers.put(name, value);
		}
		
		public String getContentType() {
			return contentType;
		}
		
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		
		public Map<String, Object> getJsonResponse() {
			return jsonResponse;
		}
		
		public void setJsonResponse(Map<String, Object> jsonResponse) {
			this.jsonResponse = jsonResponse;
		}
		
		public String getRawResponse() {
			return rawResponse;
		}
		
		public void setRawResponse(String rawResponse) {
			this.rawResponse = rawResponse;
		}
		
		public HttpException getException() {
			return exception;
		}
		
		public void setException(HttpException exception) {
			this.exception = exception;
		}
		
		public boolean isHttpSuccess() {
			return (httpStatusCode >= 200 && httpStatusCode < 300);
		}
		
		public boolean hasException() {
			return exception != null;
		}
		
		public boolean hasResponse() {
			return rawResponse != null;
		}
		
		public boolean hasJsonResponse() {
			return jsonResponse != null;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(httpStatusCode).append(":").append(contentType);
			sb.append(" headers=");
			sb.append(headers);
			sb.append(" content=");
			// don't log raw data as it can contain sensitive information
//			if (jsonResponse != null)
//				sb.append(jsonResponse);
//			else if (rawResponse != null)
//				sb.append(rawResponse);
			if (jsonResponse != null) {
				sb.append(rawResponse.length()).append(" bytes of json");
			} else if (rawResponse != null) {
				sb.append(rawResponse.length()).append(" bytes of data");
			}
			if (exception != null) {
				if (jsonResponse != null || rawResponse != null)
					sb.append(" ");
				sb.append(exception);
			}
			return sb.toString();
		}
	}

	public HttpRequester() {
		this(null);
	}
	
	public HttpRequester(int bufferSize) {
		this(null);
		this.bufferSize = bufferSize;
	}

	public HttpRequester(String[] sslProtocols) {
		super();
		this.hostNameVerifier = SSLConnectionSocketFactory.getDefaultHostnameVerifier();
		HostnameVerifier delegatingHostnameVerifier = new HostnameVerifier() {
			@Override
			public boolean verify(String paramString, SSLSession paramSSLSession) {
				HostnameVerifier hv = hostNameVerifier;
				return hv == null ? true : hv.verify(paramString, paramSSLSession);
			}
		};
		this.httpConnectionManager = new PoolingHttpClientConnectionManager(RegistryBuilder.<ConnectionSocketFactory> create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", new SSLConnectionSocketFactory((javax.net.ssl.SSLSocketFactory) javax.net.ssl.SSLSocketFactory.getDefault(), sslProtocols, null, delegatingHostnameVerifier) {
			@Override
			public Socket createLayeredSocket(Socket socket, String target, int port, HttpContext context) throws IOException {
				Socket s = super.createLayeredSocket(socket, target, port, context);
				socketCreated(s);
				return s;
			}
		}).build());

		this.httpClient = HttpClients.custom().setConnectionManager(httpConnectionManager).build();
	}

	protected void socketCreated(Socket socket) {
		if(socket instanceof SSLSocket && log.isInfoEnabled()) {
			SSLSession ss = ((SSLSocket) socket).getSession();
			log.info(new StringBuilder().append("Created SSL Socket to ").append(socket.getInetAddress()).append(':').append(socket.getPort()).append(" using ").append(ss.getProtocol()).append(" with ").append(ss.getCipherSuite()));
		}
	}
	
	public HttpClient getHttpClient() {
		return httpClient;
	}
	
	// this is primarily to make unit testing possible
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	protected String readContent(HttpEntity httpEntity, int maxSize) throws IOException {
		if(httpEntity == null)
			return "";
		int len;
		if(httpEntity.getContentLength() > maxSize || httpEntity.getContentLength() < 0)
			len = maxSize;
		else
			len = (int) httpEntity.getContentLength();
		byte[] content = new byte[len];
		InputStream in;
		int off=0;
		try {
			in = httpEntity.getContent();
		} catch(UnsupportedOperationException e) {
			return "";
		}
		try {
			int b;
			while(off<len&&(b = in.read(content,off,len-off))>-1){
				off+=b;
            }
			
		} finally {
			in.close();
		}
		Charset charset;
		try {
			final ContentType contentType = ContentType.get(httpEntity);
			if(contentType != null)
				charset = contentType.getCharset();
			else
				charset = Consts.UTF_8;
		} catch(final UnsupportedCharsetException ex) {
			charset = Consts.UTF_8;
		}
		if(charset==null){
			charset = Consts.UTF_8;
		}
		return new String(content, 0, off, charset);
	}

	protected Reader createContentReader(HttpEntity httpEntity) throws IOException {
		if(httpEntity == null)
			return new StringReader("");
		Charset charset;
		try {
			final ContentType contentType = ContentType.get(httpEntity);
			if(contentType != null)
				charset = contentType.getCharset();
			else
				charset = Consts.UTF_8;
		} catch(final UnsupportedCharsetException ex) {
			charset = Consts.UTF_8;
		}
		if(charset == null)
			charset = Consts.UTF_8;
		return new InputStreamReader(httpEntity.getContent(), charset);
	}

	protected void updateRequestSettings(HttpRequestBase request) {
		RequestConfig.Builder builder;
		if(request.getConfig() == null)
			builder = RequestConfig.custom();
		else
			builder = RequestConfig.copy(request.getConfig());
		builder.setConnectTimeout(getConnectionTimeout());
		builder.setSocketTimeout(getSocketTimeout());
		request.setConfig(builder.build());
	}

	public int getMaxTotalConnections() {
		return httpConnectionManager.getMaxTotal();
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		httpConnectionManager.setMaxTotal(maxTotalConnections);
	}

	public int getMaxHostConnections() {
		return httpConnectionManager.getDefaultMaxPerRoute();
	}

	public void setMaxHostConnections(int maxHostConnections) {
		httpConnectionManager.setDefaultMaxPerRoute(maxHostConnections);
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public HostnameVerifier getHostNameVerifier() {
		return hostNameVerifier;
	}

	public void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		this.hostNameVerifier = hostNameVerifier;
	}
	
	public boolean isIncludeJsonVersion() {
		return includeJsonVersion;
	}

	public void setIncludeJsonVersion(boolean includeJsonVersion) {
		this.includeJsonVersion = includeJsonVersion;
	}
	
	public int getBufferSize() {
		return bufferSize;
	}
	
	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public <S, T, E> T jsonPost(String url, S payload, Class<T> successClass, Class<E> errorClass, Authenticator authenticator) throws HttpException, IOException {
		HttpPost method = new HttpPost(url);
		if(payload != null) {
			StringEntity postEntity = new StringEntity(gson.toJson(payload), Consts.UTF_8);
			postEntity.setContentType(CONTENT_TYPE_JSON + (includeJsonVersion ? JSON_VERSION : ""));
			method.setEntity(postEntity);
		}
		if(authenticator != null)
			authenticator.addAuthentication(method);
		method.addHeader("Accept", CONTENT_TYPE_JSON + (includeJsonVersion ? JSON_VERSION : ""));
		method.addHeader("Accept", CONTENT_TYPE_ANY + ";q=0.1");
		updateRequestSettings(method);
		HttpResponse response = httpClient.execute(method);
		return jsonHandleResponse(response, successClass, errorClass, bufferSize);
	}
	
	public <P> HttpResponseData jsonPost(String url, P payload, Authenticator authenticator) throws HttpException, IOException {
		HttpPost method = new HttpPost(url);
		if(payload != null) {
			StringEntity postEntity = new StringEntity(gson.toJson(payload), Consts.UTF_8);
			postEntity.setContentType(CONTENT_TYPE_JSON + (includeJsonVersion ? JSON_VERSION : ""));
			method.setEntity(postEntity);
		}
		if(authenticator != null)
			authenticator.addAuthentication(method);
		method.addHeader("Accept", CONTENT_TYPE_ANY + ";q=0.1");
		updateRequestSettings(method);
		HttpResponse response = httpClient.execute(method);
		return jsonHandleResponse(response, bufferSize);

	}

	public <T, E> T jsonGet(String url, Class<T> successClass, Class<E> errorClass, Authenticator authenticator) throws HttpException, IOException {
		HttpGet method = new HttpGet(url);
		if(authenticator != null)
			authenticator.addAuthentication(method);
		method.addHeader("Accept", CONTENT_TYPE_JSON + (includeJsonVersion ? JSON_VERSION : ""));
		method.addHeader("Accept", CONTENT_TYPE_ANY + ";q=0.1");
		updateRequestSettings(method);
		HttpResponse response = httpClient.execute(method);
		return jsonHandleResponse(response, successClass, errorClass, bufferSize);
	}
	
	public HttpResponseData jsonGet(String url, Authenticator authenticator) throws HttpException, IOException {
		HttpGet method = new HttpGet(url);
		if(authenticator != null)
			authenticator.addAuthentication(method);
		method.addHeader("Accept", CONTENT_TYPE_JSON + (includeJsonVersion ? JSON_VERSION : ""));
		method.addHeader("Accept", CONTENT_TYPE_ANY + ";q=0.1");
		updateRequestSettings(method);
		HttpResponse response = httpClient.execute(method);
		return jsonHandleResponse(response, bufferSize);
	}

	protected <T, E> T jsonHandleResponse(HttpResponse response, Class<T> successClass, Class<E> errorClass, int bufferLimit) throws HttpException, IOException {
		final int statusCode = response.getStatusLine().getStatusCode();
		switch(statusCode) {
			case 200: // OK
				HttpEntity entity = response.getEntity();
				if(entity == null)
					throw new HttpException("Http response " + statusCode + " contains no content");
				try (BufferedReader reader = new BufferedReader(createContentReader(entity), bufferLimit)) {
					reader.mark(bufferLimit);
					try {
						return gson.fromJson(reader, successClass);
					} catch(JsonIOException | JsonSyntaxException e) {
						reader.reset();
						String details = IOUtils.readFully(reader, bufferLimit);
						throw new HttpException("Could not parse response as Json for content='" + details + "'", e);
					}
				}
			case 400: // bad request
			case 401: // unauthorized
				throw new HttpException("Not Authorized (" + statusCode + ")");
			case 404: // not found
				throw new HttpException("Not Found (" + statusCode + ")");
			default:
				entity = response.getEntity();
				if(entity == null)
					throw new HttpException("Invalid Http response " + statusCode + " (no content)");
				String details;
				if(errorClass == null)
					details = readContent(entity, bufferLimit);
				else {
					try {
						E error = gson.fromJson(createContentReader(entity), errorClass);
						details = (error == null ? "<BLANK>" : error.toString());
					} catch(JsonIOException | JsonSyntaxException e) {
						details = "<UNREADABLE>";
					}
				}
				throw new HttpException("Invalid Http response " + statusCode + "; content='" + details + "'");
		}
	}
	
	protected HttpResponseData jsonHandleResponse(HttpResponse response, int bufferLimit) throws IOException {
		final int statusCode = response.getStatusLine().getStatusCode();
		
		HttpResponseData responseData = new HttpResponseData(statusCode);
		Header[] headers = response.getAllHeaders();
		if (headers != null && headers.length > 0) {
			Arrays.stream(headers).forEach(h -> responseData.addHeader(h.getName(), h.getValue()));
		}
		
		HttpEntity entity = response.getEntity();
		if(entity == null)
			return responseData; // empty response body, this could be normal for a 204 response
		
		ContentType contentType = null;
		try {
			contentType = ContentType.get(entity);
		} catch (Exception e) {
			responseData.setException(new HttpException("Failed to determine Content-Type", e));
			return responseData;
		}
		
		responseData.setContentType(contentType.toString());
		
		String rawResponse = null;
		try (BufferedReader reader = new BufferedReader(createContentReader(entity), bufferLimit)) {
			rawResponse = IOUtils.readFully(reader, bufferLimit);
		} catch(IOException e) {
			responseData.setException(new HttpException("Failed to read response content", e));
			return responseData;
		}
		
		responseData.setRawResponse(rawResponse);
		
		if (contentType.getMimeType().toLowerCase().contains("json")) {
			try {
				responseData.setJsonResponse(gson.fromJson(rawResponse, Map.class));
			} catch (JsonIOException | JsonSyntaxException e) {
				responseData.setException(new HttpException("Failed to convert response to JSON: rawResponse=" + rawResponse, e));
				return responseData;
			}
		}
		
		return responseData;
	}

	public Reader get(String url, String contentType, Authenticator authenticator) throws HttpException, IOException {
		HttpGet method = new HttpGet(url);
		if(authenticator != null)
			authenticator.addAuthentication(method);
		if(!StringUtils.isBlank(contentType))
			method.addHeader("Accept", contentType);
		updateRequestSettings(method);
		HttpResponse response = httpClient.execute(method);
		return handleResponse(response);
	}

	protected Reader handleResponse(HttpResponse response) throws HttpException, IOException {
		final int statusCode = response.getStatusLine().getStatusCode();
		switch(statusCode) {
			case 200: // OK
				HttpEntity entity = response.getEntity();
				if(entity == null)
					throw new HttpException("Http response " + statusCode + " contains no content");
				return createContentReader(entity);
			case 400: // bad request
			case 401: // unauthorized
				throw new HttpException("Not Authorized (" + statusCode + ")");
			case 404: // not found
				throw new HttpException("Not Found (" + statusCode + ")");
			default:
				entity = response.getEntity();
				if(entity == null)
					throw new HttpException("Invalid Http response " + statusCode + " (no content)");
				String details = readContent(entity, bufferSize);
				throw new HttpException("Invalid Http response " + statusCode + "; content='" + details + "'");
		}
	}
}
