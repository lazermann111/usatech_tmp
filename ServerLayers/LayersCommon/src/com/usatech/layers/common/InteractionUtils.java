/**
 *
 */
package com.usatech.layers.common;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.usatech.app.GenericAttribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.app.task.BridgeTask;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommType;
import com.usatech.layers.common.constants.MessageAttrEnum;

import simple.app.BasicQoS;
import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.lang.sun.misc.CRC16;
import simple.text.StringUtils;
import simple.util.LRUMap;

/**
 * @author Brian S. Krug
 *
 */
public class InteractionUtils {
	private static String checkTerminalQueueKey;
	private static final BasicQoS checkTerminalQueueQos = new BasicQoS(300000, 5, false, null);
	private static String tranImportQueueKey = "usat.import.tran";
	private static String tranReportQueueKey = "usat.import.tran.report";
	private static String sessionControlQueueKey = "usat.inbound.session.control";
	private static String rdwLoadQueueKey = "usat.rdw.load.single";
	protected static String consumerCommQueueKey = "usat.consumer.communicate";
	protected static String defaultCustomerServicePhone = "888-561-4748";
	protected static String defaultCustomerServiceEmail = "support@usatech.com";
	protected static String defaultTerminalAddress = "100 Deerfield Lane, Suite 300";
	protected static String defaultTerminalCity = "Malvern";
	protected static String defaultTerminalStateCd = "PA";
	protected static String defaultTerminalPostal = "19355";
	protected static String defaultTerminalCountryCd = "US";
	protected static LRUMap<String, Long> lastRiskCheckCache = new LRUMap<>(500);
	protected static double riskCheckDays = 1;
	protected static boolean riskCheckEnabled = true;
	
	public static String getCheckTerminalQueueKey() {
		return checkTerminalQueueKey;
	}

	public static void setCheckTerminalQueueKey(String checkTerminalQueueKey) {
		InteractionUtils.checkTerminalQueueKey = checkTerminalQueueKey;
	}
	
	public static String getTranImportQueueKey() {
		return tranImportQueueKey;
	}

	public static void setTranImportQueueKey(String tranImportQueueKey) {
		InteractionUtils.tranImportQueueKey = tranImportQueueKey;
	}
	
	public static String getSessionControlQueueKey() {
		return sessionControlQueueKey;
	}

	public static void setSessionControlQueueKey(String sessionControlQueueKey) {
		InteractionUtils.sessionControlQueueKey = sessionControlQueueKey;
	}

	public static String getRdwLoadQueueKey() {
		return rdwLoadQueueKey;
	}

	public static void setRdwLoadQueueKey(String rdwLoadQueueKey) {
		InteractionUtils.rdwLoadQueueKey = rdwLoadQueueKey;
	}

	public static void publishCheckTerminal(long tranId, Connection conn, Publisher<ByteInput> publisher, Log log) throws ServiceException, SQLException, DataLayerException, BeanException {
		String queue = getCheckTerminalQueueKey();
		if(queue != null && (queue=queue.trim()).length() > 0) {
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("tranId", tranId);
	        DataLayerMgr.selectInto(conn, "GET_TERMINAL_ID_FOR_TRAN", params);
	        MessageChain mc = new MessageChainV11();
	        MessageChainStep step = mc.addStep(checkTerminalQueueKey);
	        for(Map.Entry<String,Object> entry : params.entrySet()) {
	        	step.addLiteralAttribute(entry.getKey(), entry.getValue());
	        }
	        MessageChainService.publish(mc, publisher, null, getCheckTerminalQueueQos());
		} else {
			log.info("Would check terminal for tranId=" + tranId + " but checkTerminalQueueKey is null");
		}
	}
	
	public static void publishTranImport(long tranId, Publisher<ByteInput> publisher, Log log) throws ServiceException {
		if (tranImportQueueKey == null || tranImportQueueKey.length() == 0) {
			log.info("Would import transaction tranId: " + tranId + " but tranImportQueueKey is null");
			return;
		}
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(tranImportQueueKey);
		step.addLongAttribute("tranId", tranId);
		MessageChainService.publish(mc, publisher);
	}

	public static void publishTranReport(boolean isReload, long tranId, int transTypeId, Publisher<ByteInput> publisher, Log log, Long reportTranId) throws ServiceException {
		if (tranReportQueueKey == null || tranReportQueueKey.length() == 0) {
			log.info("Would report transaction reportTranId: " + reportTranId + " but tranReportQueueKey is null");
			return;
		}
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(tranReportQueueKey);
		step.addIntAttribute("transTypeId", transTypeId);
		step.addLongAttribute("reportTranId", reportTranId);
		step.addBooleanAttribute("isReload", isReload);

		MessageChainService.publish(mc, publisher);
	}
	
	public static void publishSessionControl(String action, Map<String,Object> params, Publisher<ByteInput> publisher, Log log) throws ServiceException {
		if (sessionControlQueueKey == null || sessionControlQueueKey.length() == 0) {
			log.error("sessionControlQueueKey is empty");
			return;
		}
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(sessionControlQueueKey);
		// removed to fix USAT-373
		//step.setTemporary(true);
		step.addStringAttribute("action", action);
		for (String key: params.keySet())
			step.setAttribute(new GenericAttribute(key), params.get(key));
		MessageChainService.publish(mc, publisher);
	}
	

	public static BasicQoS getCheckTerminalQueueQos() {
		return checkTerminalQueueQos;
	}
	
	public static boolean lockProcess(String processCd, String processTokenCd, Map<String,Object> attributes) throws ConvertException, DataLayerException, SQLException {
		attributes.put("processCd", processCd);
    	attributes.put("processTokenCd", processTokenCd);
    	DataLayerMgr.executeCall("LOCK_PROCESS", attributes, true);
    	return "Y".equals(ConvertUtils.getString(attributes.get("successFlag"), true));
	}
	
	public static void unlockProcess(String processCd, String processTokenCd, Map<String,Object> attributes) throws DataLayerException, SQLException {
		attributes.put("processCd", processCd);
    	attributes.put("processTokenCd", processTokenCd);
    	DataLayerMgr.executeCall("UNLOCK_PROCESS", attributes, true);
	}

	public static boolean lockProcess(Connection conn, String processCd, String processTokenCd, Map<String, Object> attributes) throws ConvertException, DataLayerException, SQLException {
		attributes.put("processCd", processCd);
		attributes.put("processTokenCd", processTokenCd);
		try {
			DataLayerMgr.executeCall(conn, "LOCK_PROCESS", attributes);
		} catch(SQLException e) {
			conn.rollback();
			throw e;
		}
		conn.commit();
		return "Y".equals(ConvertUtils.getString(attributes.get("successFlag"), true));
	}

	public static void unlockProcess(Connection conn, String processCd, String processTokenCd, Map<String, Object> attributes) throws DataLayerException, SQLException {
		attributes.put("processCd", processCd);
		attributes.put("processTokenCd", processTokenCd);
		try {
			DataLayerMgr.executeCall(conn, "UNLOCK_PROCESS", attributes);
		} catch(SQLException e) {
			conn.rollback();
			throw e;
		}
		conn.commit();

	}

	public static String getTranReportQueueKey() {
		return tranReportQueueKey;
	}

	public static void setTranReportQueueKey(String tranReportQueueKey) {
		InteractionUtils.tranReportQueueKey = tranReportQueueKey;
	}	

	public static void parseCardIdList(String cardIdList, Map<String, Long> parsedMap) throws ParseException {
		int pos = 0;
		while(pos < cardIdList.length()) {
			int next = cardIdList.indexOf(':', pos);
			if(next < 0)
				throw new ParseException("Missing a ':'", cardIdList.length());
			String instance = cardIdList.substring(pos, next);
			pos = next + 1;
			next = cardIdList.indexOf(',', pos);
			if(next < 0)
				next = cardIdList.length();
			long cardId;
			try {
				cardId = Long.parseLong(cardIdList.substring(pos, next));
			} catch(NumberFormatException e) {
				throw new ParseException("Could not convert '" + cardIdList.substring(pos, next) + "' to a number at position " + pos, pos);
			}
			parsedMap.put(instance, cardId);
			pos = next + 1;
		}
	}

	public static String buildCardIdList(Map<String, Long> parsedMap) {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, Long> entry : parsedMap.entrySet()) {
			if(StringUtils.isBlank(entry.getKey()))
				throw new IllegalArgumentException("Instance is null");
			if(entry.getKey().indexOf(':') >= 0)
				throw new IllegalArgumentException("Instance contains illegal character ':'");
			if(entry.getValue() == null)
				throw new IllegalArgumentException("CardId is null");
			if(sb.length() > 0)
				sb.append(',');
			sb.append(entry.getKey()).append(':').append(entry.getValue());
		}

		return sb.toString();
	}

	public static BigDecimal toMajorCurrency(BigInteger amount, int minorCurrencyFactor) {
		int fractionDigits = (int) Math.ceil(Math.log10(minorCurrencyFactor));
		// NOTE: The following gives the wrong scale in some cases! So use the older variety.
		// new BigDecimal(amount).divide(BigDecimal.valueOf(minorCurrencyFactor), new MathContext(fractionDigits, RoundingMode.UP));
		return new BigDecimal(amount).divide(BigDecimal.valueOf(minorCurrencyFactor), fractionDigits, RoundingMode.UP);
	}

	public static BigDecimal toMinorCurrency(BigDecimal amount, int minorCurrencyFactor) {
		return amount.multiply(BigDecimal.valueOf(minorCurrencyFactor)).stripTrailingZeros();
	}

	public static void checkForReplenishBonuses(long parentTranId, Connection conn, Publisher<ByteInput> publisher, Log log) throws ServiceException {
		Results results;
		try {
			results = DataLayerMgr.executeQuery(conn, "GET_REPLENISH_BONUSES", new Object[] { parentTranId });
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		while(results.next()) {
			try {
				InteractionUtils.publishTranImport(results.getValue("tranId", Long.class), publisher, log);
				InteractionUtils.handleReplenishBonusEarned(publisher, results.getValue("consumerAcctId", Long.class), results.getValue("campaignId", Long.class), results.getValue("replenishBonusAmount", BigDecimal.class), results.getValue("usedReplenishBonusBalance", BigDecimal.class), log);
			} catch(ConvertException e) {
				throw new ServiceException(e);
			}
		}
	}

	public static void handleReplenishBonusEarned(Publisher<ByteInput> publisher, long consumerAcctId, long campaignId, BigDecimal replenishBonus, BigDecimal replenishments, Log log) throws ServiceException {
		log.info("Prepaid account " + consumerAcctId + " received replenish bonus. Enqueuing notification");
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.bridge.to.msr");
		step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, getConsumerCommQueueKey());
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.REPLENISH_BONUS);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CASH_BACK_AMOUNT, replenishBonus);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_PURCHASE_AMOUNT, replenishments);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CAMPAIGN_ID, campaignId);
		MessageChainService.publish(mc, publisher);
	}
	
	public static void checkForBelowBalance(long consumerAcctId, Connection conn, Publisher<ByteInput> publisher, Log log) throws ServiceException {
		Results results;
		try {
			results = DataLayerMgr.executeQuery(conn, "GET_BELOW_BALANCE_REQUEST", new Object[] { consumerAcctId });
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		if(results.next()) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep("usat.bridge.to.msr");
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, getConsumerCommQueueKey());
			step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
			step.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.BELOW_BALANCE);
			step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_BALANCE, results.get("consumerAcctBalance"));
			step.setAttribute(ConsumerCommAttrEnum.ATTR_BELOW_BALANCE_THRESHOLD, results.get("belowBalanceThreshold"));
			MessageChainService.publish(mc, publisher);
		}
	}

	public static void checkForCashBack(long parentTranId, Connection conn, Publisher<ByteInput> publisher, Log log) throws ServiceException {
		Results results;
		try {
			results = DataLayerMgr.executeQuery(conn, "GET_CASH_BACKS", new Object[] { parentTranId });
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		while(results.next()) {
			try {
				InteractionUtils.publishTranImport(results.getValue("tranId", Long.class), publisher, log);
				InteractionUtils.handleCashBackEarned(publisher, results.getValue("consumerAcctId", Long.class), results.getValue("campaignId", Long.class), results.getValue("cashBackAmount", BigDecimal.class), results.getValue("usedCashBackBalance", BigDecimal.class), log);
			} catch(ConvertException e) {
				throw new ServiceException(e);
			}
		}
	}

	public static void handleCashBackEarned(Publisher<ByteInput> publisher, long consumerAcctId, long campaignId, BigDecimal cashBack, BigDecimal purchases, Log log) throws ServiceException {
		log.info("Prepaid account " + consumerAcctId + " received cash back. Enqueuing notification");
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.bridge.to.msr");
		step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, getConsumerCommQueueKey());
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.CASH_BACK_ON_PURCHASE);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CASH_BACK_AMOUNT, cashBack);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_PURCHASE_AMOUNT, purchases);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CAMPAIGN_ID, campaignId);
		MessageChainService.publish(mc, publisher);
	}

	public static void handleReplenishDisabled(Publisher<ByteInput> publisher, long consumerAcctId, long replenishId, String replenishCardMasked, int deniedCount, Log log) throws ServiceException {
		log.info("Replenishemnt of prepaid account " + consumerAcctId + " using card " + replenishCardMasked + " was denied " + deniedCount + " times and is being disabled. Enqueuing notification");
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.bridge.to.msr");
		step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, getConsumerCommQueueKey());
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.REPLENISH_DISABLED);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_ID, replenishId);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_CARD_MASKED, replenishCardMasked);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_DENIED_COUNT, deniedCount);
		MessageChainService.publish(mc, publisher);
	}
	
	public static void handleReplenishDeclined(Publisher<ByteInput> publisher, long consumerAcctId, String replenishCardMasked, BigDecimal replenishAmount, Log log) throws ServiceException {
		log.info("Replenishemnt of prepaid account " + consumerAcctId + " using card " + replenishCardMasked + " was declined. Enqueuing notification");
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.bridge.to.msr");
		step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, getConsumerCommQueueKey());
		step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.REPLENISH_DECLINED);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_CARD_MASKED, replenishCardMasked);
		step.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_AMOUNT, replenishAmount);
		MessageChainService.publish(mc, publisher);
	}

	public static void doRiskChecks(Publisher<ByteInput> publisher, String deviceName, Log log) throws ServiceException {
		if (!riskCheckEnabled)
			return;
		
		try {
			if(!checkAndUpdateLastRiskCheck(deviceName, riskCheckDays)) {
				log.debug("Device {0} does not require risk checking", deviceName);
				return;
			}
		} catch(SQLException | DataLayerException e) {
			log.error("Failed to check and update last risk check for device {0}", deviceName, e);
			return;
		}

		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.posm.risk.check");
		// removed to fix USAT-373
		//step.setTemporary(true);
		step.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceName);
		MessageChainService.publish(mc, publisher);
	}

	private static boolean checkAndUpdateLastRiskCheck(String deviceName, double days) throws SQLException, DataLayerException {
		// cache last risk check to prevent some repeated database lookups for recent devices
		if(lastRiskCheckCache != null) {
			long now = System.currentTimeMillis();
			synchronized(lastRiskCheckCache) {
				Long lastCheck = lastRiskCheckCache.get(deviceName);
				if(lastCheck != null) {
					long et = (now - lastCheck);
					if(et < (86400000 * days)) {
						return false;
					}
				}
				lastRiskCheckCache.put(deviceName, now);
			}
		}

		// if call updates the row then we should do risk checking
		return DataLayerMgr.executeUpdate("CHECK_UPDATE_EPORT_LAST_RISK_CHECK", new Object[] {days,deviceName}, true) > 0;
	}

	public static String getConsumerCommQueueKey() {
		return consumerCommQueueKey;
	}

	public static void setConsumerCommQueueKey(String consumerCommQueueKey) {
		InteractionUtils.consumerCommQueueKey = consumerCommQueueKey;
	}

	protected static Pattern EXP_DATE_PATTERN = Pattern.compile("^(\\d{2,4})(\\d{2})$");

	/**
	 * Convert YYMM string into milliseconds past epoch
	 * 
	 * @param expDate
	 * @return
	 */
	public static long expDateToMillis(String expDate) {
		if(!StringUtils.isBlank(expDate)) {
			Matcher expDateMatcher = EXP_DATE_PATTERN.matcher(expDate);
			if(expDateMatcher.matches()) {
				Calendar cal = Calendar.getInstance();
				int currYear = cal.get(Calendar.YEAR);
				int year = Integer.parseInt(expDateMatcher.group(1));
				switch(expDateMatcher.group(1).length()) {
					case 2:
						year = Integer.parseInt(expDateMatcher.group(1)) + currYear - (currYear % 100);
						if(year < currYear - 10)
							year += 100;
						break;
					case 3:
						year = Integer.parseInt(expDateMatcher.group(1)) + currYear - (currYear % 1000);
						if(year < currYear - 100)
							year += 1000;
						break;
				}
				int month = Integer.parseInt(expDateMatcher.group(2));
				cal.clear();
				cal.set(year, month - 1, 1);
				cal.add(Calendar.MONTH, 1);
				return cal.getTimeInMillis();
			}
		}
		return 0L;
	}

	public static int getCRC16(String s) {
		CRC16 crc = new CRC16();
		for(byte b : s.getBytes())
			crc.update(b);
		return crc.value;
	}
	
  public static int generateCRC(byte[] bytes) {
  	CRC16 crc = new CRC16();
  	for(byte b : bytes)
  		crc.update(b);
  	return crc.value;
  }	

	public static String getDefaultCustomerServicePhone() {
		return defaultCustomerServicePhone;
	}

	public static void setDefaultCustomerServicePhone(String defaultCustomerServicePhone) {
		InteractionUtils.defaultCustomerServicePhone = defaultCustomerServicePhone;
	}

	public static String getDefaultCustomerServiceEmail() {
		return defaultCustomerServiceEmail;
	}

	public static void setDefaultCustomerServiceEmail(String defaultCustomerServiceEmail) {
		InteractionUtils.defaultCustomerServiceEmail = defaultCustomerServiceEmail;
	}

	public static String getDefaultTerminalAddress() {
		return defaultTerminalAddress;
	}

	public static void setDefaultTerminalAddress(String defaultTerminalAddress) {
		InteractionUtils.defaultTerminalAddress = defaultTerminalAddress;
	}

	public static String getDefaultTerminalCity() {
		return defaultTerminalCity;
	}

	public static void setDefaultTerminalCity(String defaultTerminalCity) {
		InteractionUtils.defaultTerminalCity = defaultTerminalCity;
	}

	public static String getDefaultTerminalStateCd() {
		return defaultTerminalStateCd;
	}

	public static void setDefaultTerminalStateCd(String defaultTerminalStateCd) {
		InteractionUtils.defaultTerminalStateCd = defaultTerminalStateCd;
	}

	public static String getDefaultTerminalPostal() {
		return defaultTerminalPostal;
	}

	public static void setDefaultTerminalPostal(String defaultTerminalPostal) {
		InteractionUtils.defaultTerminalPostal = defaultTerminalPostal;
	}

	public static String getDefaultTerminalCountryCd() {
		return defaultTerminalCountryCd;
	}

	public static void setDefaultTerminalCountryCd(String defaultTerminalCountryCd) {
		InteractionUtils.defaultTerminalCountryCd = defaultTerminalCountryCd;
	}
	
	public static LRUMap<String, Long> getLastRiskCheckCache() {
		return lastRiskCheckCache;
	}

	public static double getRiskCheckDays() {
		return riskCheckDays;
	}

	public static void setRiskCheckDays(double riskCheckDays) {
		InteractionUtils.riskCheckDays = riskCheckDays;
	}

	public static boolean isRiskCheckEnabled() {
		return riskCheckEnabled;
	}

	public static void setRiskCheckEnabled(boolean riskCheckEnabled) {
		InteractionUtils.riskCheckEnabled = riskCheckEnabled;
	}

}
