/**
 *
 */
package com.usatech.layers.common;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.security.SecurityUtils;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class QueueKeyGenerator {
	protected final static int BASE_PORT = 10240;
	protected final String hostname;
	protected int port;
	protected String prefix;
	protected String suffix;
	protected String portSystemProperty = "jmx.remote.x.rmiRegistryPort";
	protected ServerSocket serverSocket;

	public QueueKeyGenerator() {
		hostname = StringUtils.substringBefore(getCanonicalHostName(), ".");
	}

	public static String getCanonicalHostName() {
		try {
			return Inet4Address.getLocalHost().getCanonicalHostName();
		} catch(UnknownHostException e) {
			return "localhost";
		}
	}
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getPortSystemProperty() {
		return portSystemProperty;
	}

	public void setPortSystemProperty(String portSystemProperty) {
		String old = this.portSystemProperty;
		this.portSystemProperty = portSystemProperty;
		if(!ConvertUtils.areEqual(old, portSystemProperty))
			port = -1;
	}
	protected int findPort() {
		if(port <= 0) {
			port = ConvertUtils.getIntSafely(SecurityUtils.getProperties().getProperty(getPortSystemProperty()), -1);
			if(port <= 0) {
				for(int i = 0; i < 100; i++) {
					try {
						serverSocket = new ServerSocket(BASE_PORT + i);
						port = serverSocket.getLocalPort();
						break;
					} catch(IOException e) {
						// try next one
					}
				}
				if(port <= 0) {
					port = ConvertUtils.getIntSafely(SystemUtils.getSystemInfo().get("processId"), -1);
					if(port <= 0) {
						try {
							serverSocket = new ServerSocket(0);
							port = serverSocket.getLocalPort();
						} catch(IOException e) {
							port = (int) (System.currentTimeMillis() & 0x7FFFFFFF);
						}
					}
				}
			}
		}
		return port;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(prefix != null) sb.append(prefix);
		sb.append(hostname);
		sb.append('-');
		sb.append(findPort());
		if(suffix != null) sb.append(suffix);
		return sb.toString();
	}
	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		ServerSocket ss = this.serverSocket;
		if(ss != null) {
			try {
				ss.close();
			} catch(Throwable e) {
				//ignore
			}
			this.serverSocket = null;
		}
	}
}
