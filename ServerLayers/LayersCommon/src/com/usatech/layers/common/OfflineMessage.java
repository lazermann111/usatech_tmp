/**
 *
 */
package com.usatech.layers.common;

import java.nio.charset.Charset;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData;

/**
 * @author Brian S. Krug
 *
 */
public interface OfflineMessage {
    public String getGlobalSessionCode() ;
    public byte getProtocolId() ;
    public String getDeviceName() ;

	public String getTimeZoneGuid() throws ServiceException;
	public DeviceType getDeviceType();
	public Charset getDeviceCharset();
    public MessageData getData() ;
    public Log getLog() ;
    public long getServerTime() ;

	public long getSessionStartTime();
}
