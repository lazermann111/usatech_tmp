package com.usatech.layers.common;

import java.sql.Blob;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.sql.rowset.serial.SerialBlob;

import simple.app.DialectResolver;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.UpdateStatusType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_90;

public class LegacyUpdateStatusProcessor {
	protected static final Map<Integer,Boolean> sendTerminalStatusUpdates = new HashMap<Integer,Boolean>();
	public static MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message, boolean ackOnError) throws ServiceException {
		Log log = message.getLog();
		DeviceInfo deviceInfo = message.getDeviceInfo();
		MessageChainStep step = taskInfo.getStep();
		Map<String,Object> attributes = step.getAttributes();
		Map<String,Object> resultAttributes = step.getResultAttributes();
		Connection conn = null;
		Long commandId;
		try {
			conn = AppLayerUtils.getConnection(false);
			Map<String,Object> params = new HashMap<String, Object>();
			deviceInfo.refreshSafely();
			params.put("deviceName", deviceInfo.getDeviceName());
			params.put("maxExecuteOrder", ProcessingConstants.MAX_COMMAND_EXECUTE_ORDER);
			params.put("sessionAttributes", ConvertUtils.getStringSafely(message.getSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR), ""));
			params.put("globalSessionCode", message.getGlobalSessionCode());
			params.put("updateStatus", UpdateStatusType.NORMAL.getValue());
			params.put("v4Messages", 'N');
			DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_NEXT", params);
			if(log.isInfoEnabled())
				log.info("Got results from UPDATE_PENDING_COMMAND_NEXT: " + params);
			message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR,  ConvertUtils.getStringSafely(params.get("sessionAttributes"), ""));
			
			commandId = ConvertUtils.convertSafely(Long.class, params.get("commandId"), null);
			
			DeviceType deviceType = deviceInfo.getDeviceType();
			if(commandId==null) {
				if(deviceInfo.getDeviceType() == DeviceType.ESUDS){
					switch(message.getData().getMessageType()) {
						case I_AM_ALIVE_ALERT:
						case ROOM_STATUS:
						case ROOM_STATUS_WITH_STARTING_PORT_NUMBER:
						case WASHER_DRYER_LABELS:
						case WASHER_DRYER_DIAGNOSTICS:
							MessageProcessingUtils.writeLegacy2FGenericAck(message);
							break;
					}
				} else{
					MessageProcessingUtils.writeLegacy90TerminalUpdateStatus(message, (byte)0);
				}
			}else {
				//eSuds can send 4 5C in a row for test network connection.
				String commandDataType = ConvertUtils.getString(params.get("dataType"), true);
				MessageType messageType = MessageType.getByValue(ByteArrayUtils.fromHex(commandDataType), 0);
				if(commandId==ConvertUtils.getLong(message.getSessionAttribute(LegacyUtils.LAST_PENDING_COMMAND_SENT), -1)){
					if(deviceInfo.getDeviceType() == DeviceType.ESUDS){
						switch(message.getData().getMessageType()) {
							case I_AM_ALIVE_ALERT:
							case ROOM_STATUS:
							case ROOM_STATUS_WITH_STARTING_PORT_NUMBER:
							case WASHER_DRYER_LABELS:
							case WASHER_DRYER_DIAGNOSTICS:
								MessageProcessingUtils.writeLegacy2FGenericAck(message);
								break;
						}
					} else{
						//it is possible that device send 92 in a row before ack, eg, server asks for an nonexist file, device will not do anything but send 92 again or the file is identical to the server
						if(messageType==MessageType.FILE_TRANSFER_REQUEST){
							params.clear();
							params.put("pendingCommandId", commandId);
							params.put("globalSessionCode", message.getGlobalSessionCode());
							DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_SUCCESS", params);
							MessageProcessingUtils.writeLegacy90TerminalUpdateStatus(message, (byte)0);
						}
					}
				}else{
					byte[] commandBytes = ConvertUtils.convert(byte[].class, params.get("commandBytes"));
					boolean isAckMessageNumber=false;//whether it will be 2Fh ack
					switch(messageType) {
						case FILE_XFER_START_2_0:
						case FILE_XFER_START_3_0:
							//expect final 7F/A7
							String fileName = ConvertUtils.getString(params.get("fileName"), true);
							int fileTypeId = ConvertUtils.getInt(params.get("fileTypeId"));
							Blob fileContent = null;
							if (!DialectResolver.isOracle()) {
								byte[] filebytes = ConvertUtils.convert(byte[].class, params.get("fileContent"));
								fileContent = new SerialBlob(fileContent);
							} else {
								fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
							}
							Calendar fileCreateTime = ConvertUtils.convert(Calendar.class, params.get("fileCreateTime"));
							int filePacketSize = ConvertUtils.getInt(params.get("filePacketSize"), CommonProcessing.defaultFilePacketSize);
							int fileGroupNum = message.getData().getMessageNumber();//use messageNumber for group number
							long fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
							MessageData_90 updateStatus = null;
							if(isSendTerminalStatusUpdate(message)) {
								updateStatus = new MessageData_90();
								updateStatus.setTerminalUpdateStatus((byte)1);
							}
							MessageProcessingUtils.writeFileTransferStart(message, fileTransferId, commandId, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileContent, messageType, updateStatus);
							break;
						case SERVER_TO_CLIENT_REQUEST:
							int requestNumber = commandBytes[0];
							//p7 spec
							switch(requestNumber){
								case 0:
								case 1:
								case 2:
								case 5:
								case 13:
								case 14:
								case 15:
								case 16:
								case 12://roomlayout or labels will send 2F for this request
									isAckMessageNumber=true;
									break;
								case 11:
									if(deviceType == DeviceType.ESUDS){
										isAckMessageNumber=true;
									}
									break;
							}
							updateSendReply(deviceType, message, messageType, commandBytes);
							break;
						case GX_PEEK://87 expect 7C
						case DEVICE_REACTIVATE://76 expect 75
						case FILE_TRANSFER_REQUEST:
						case FORCE_DEVICE_SHUTDOWN:
							updateSendReply(deviceType, message, messageType, commandBytes);
							break;
						default:
							//send message
							updateSendReply(deviceType, message, messageType, commandBytes);
							isAckMessageNumber = true;
					}
					if(isAckMessageNumber) {
						//when the response gets back use getSessionAttribute to get commandId and update table
						message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_PREFIX+message.getData().getMessageNumber(), commandId);
					} else {
						//use last command id
						message.setNextCommandId(commandId);
					}
					message.setSessionAttribute(LegacyUtils.LAST_PENDING_COMMAND_SENT, commandId);
					if(message.getLog().isInfoEnabled()) {
						message.getLog().info("Sending " + messageType + " for command " + commandId);
					}
				}
			}
			conn.commit();
		} catch(ServiceException e) {
			message.getLog().warn("Could not process message", e);
			ProcessingUtils.rollbackDbConnection(log, conn);
			if(ackOnError) {
				if(message.getReplyCount() == 0)
					switch(message.getData().getMessageType()) {
						case TERMINAL_UPDATE_STATUS_REQUEST:
							MessageProcessingUtils.writeLegacy90TerminalUpdateStatus(message, (byte) 0);
							break;
						default:
							MessageProcessingUtils.writeLegacy2FGenericAck(message);
					}
			} else
				return MessageResponse.CLOSE_SESSION;
        } catch(Exception e) {
        	message.getLog().warn("Could not process message", e);
			ProcessingUtils.rollbackDbConnection(log, conn);
			if(ackOnError) {
				if(message.getReplyCount() == 0)
					switch(message.getData().getMessageType()) {
						case TERMINAL_UPDATE_STATUS_REQUEST:
							MessageProcessingUtils.writeLegacy90TerminalUpdateStatus(message, (byte) 0);
							break;
						default:
							MessageProcessingUtils.writeLegacy2FGenericAck(message);
					}
			} else
				return MessageResponse.CLOSE_SESSION;
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
        return MessageResponse.CONTINUE_SESSION;
	}
	
	public static void updateSendReply(DeviceType deviceType, Message message, MessageType messageType, byte[] commandBytes) throws ServiceException{
		if(deviceType == DeviceType.ESUDS){
			switch(message.getData().getMessageType()) {
				case I_AM_ALIVE_ALERT:
					MessageData_90 reply90=new MessageData_90();
					reply90.setTerminalUpdateStatus((byte)1);
					MessageProcessingUtils.writeMessageFromBytes(message, messageType, commandBytes, reply90);
					break;
				case ROOM_STATUS:
				case ROOM_STATUS_WITH_STARTING_PORT_NUMBER:
				case WASHER_DRYER_LABELS:
				case WASHER_DRYER_DIAGNOSTICS:
					MessageData reply2F = MessageResponseUtils.createLegacy2FGenericAck(message);
					MessageProcessingUtils.writeMessageFromBytes(message, messageType, commandBytes, reply2F);
					break;
				default:
					MessageProcessingUtils.writeMessageFromBytes(message, messageType, commandBytes);
			}
		} else{
			if(isSendTerminalStatusUpdate(message)){
				MessageData_90 reply90=new MessageData_90();
				reply90.setTerminalUpdateStatus((byte)1);
				MessageProcessingUtils.writeMessageFromBytes(message, messageType, commandBytes, reply90);
			}else{
				MessageProcessingUtils.writeMessageFromBytes(message, messageType, commandBytes);
			}
		}
	}
	
	public static boolean isESudsNeed2F(MessageType incomingMessageType){
		boolean send2F;
		switch(incomingMessageType){
			case I_AM_ALIVE_ALERT:
			case ROOM_STATUS:
			case ROOM_STATUS_WITH_STARTING_PORT_NUMBER:
			case WASHER_DRYER_LABELS:
			case WASHER_DRYER_DIAGNOSTICS:
				send2F=true;
				break;
			default:
				send2F=false;
		}
		return send2F;
	}
	public static boolean isSendTerminalStatusUpdate(Message message) throws ServiceException{
		int deviceType = message.getDeviceInfo().getDeviceType().getValue();
		return sendTerminalStatusUpdates.get(deviceType) == null || sendTerminalStatusUpdates.get(deviceType);
	}
	public static void setSendTerminalStatusUpdate(int deviceType, boolean flag) {
		sendTerminalStatusUpdates.put(deviceType, flag);
	}
}
