/**
 *
 */
package com.usatech.layers.common;




/**
 * @author Brian S. Krug
 *
 */
public class CommonProcessing {
	protected static int defaultFilePacketSize = 1024;

	public int getDefaultFilePacketSize() {
		return defaultFilePacketSize;
	}

	public void setDefaultFilePacketSize(int defaultFilePacketSize) {
		CommonProcessing.defaultFilePacketSize = defaultFilePacketSize;
	}
}
