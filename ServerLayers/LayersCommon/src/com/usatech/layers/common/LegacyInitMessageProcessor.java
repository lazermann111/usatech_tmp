package com.usatech.layers.common;

public class LegacyInitMessageProcessor {
	protected long encryptionKeyMinAgeMin;
	protected boolean deviceReactivationOnReinit;
	protected boolean initEmailEnabled=true;
	protected String initEmailTo="KioskAlerts@usatech.com";
	protected String initEmailFrom="AppLayer@usatech.com";
	protected String emailQueueKey="usat.email";
	public long getEncryptionKeyMinAgeMin() {
		return encryptionKeyMinAgeMin;
	}
	public void setEncryptionKeyMinAgeMin(long encryptionKeyMinAgeMin) {
		this.encryptionKeyMinAgeMin = encryptionKeyMinAgeMin;
	}
	public boolean isDeviceReactivationOnReinit() {
		return deviceReactivationOnReinit;
	}
	public void setDeviceReactivationOnReinit(boolean deviceReactivationOnReinit) {
		this.deviceReactivationOnReinit = deviceReactivationOnReinit;
	}
	public boolean isInitEmailEnabled() {
		return initEmailEnabled;
	}
	public void setInitEmailEnabled(boolean initEmailEnabled) {
		this.initEmailEnabled = initEmailEnabled;
	}
	public String getInitEmailTo() {
		return initEmailTo;
	}
	public void setInitEmailTo(String initEmailTo) {
		this.initEmailTo = initEmailTo;
	}
	public String getInitEmailFrom() {
		return initEmailFrom;
	}
	public void setInitEmailFrom(String initEmailFrom) {
		this.initEmailFrom = initEmailFrom;
	}
	public String getEmailQueueKey() {
		return emailQueueKey;
	}
	public void setEmailQueueKey(String emailQueueKey) {
		this.emailQueueKey = emailQueueKey;
	}
}
