package com.usatech.layers.common;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.NamingException;

import org.apache.commons.configuration.Configuration;

import simple.app.BasicCommandArgument;
import simple.app.Command;
import simple.app.CommandArgument;
import simple.app.CommandLineInterfaceWithConfig;
import simple.app.AbstractCommandManager.AbstractCommand;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ConsoleInteraction;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.text.FixedWidthPrinter;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.CaseInsensitiveComparator;
import simple.util.CompositeSet;

import com.usatech.app.jms.ExtendedSession;
import com.usatech.app.jms.JMSBase;

public class QueueMaintenance extends CommandLineInterfaceWithConfig<QueueMaintenance.MaintenanceContext> {
	//private static final Log log = Log.getLog();
	protected static final Pattern virtualTopicPattern = Pattern.compile("Consumer\\.(.+)\\.VirtualTopic\\.(.+)");
	protected static final CaseInsensitiveComparator<String> sorter = new CaseInsensitiveComparator<String>();
	protected static final String retryQueuePrefix = "_RETRY.";
	protected static final String deadLetterQueueName = "_DLQ";
	protected static class QueueInfo {
		protected final String name;
		protected final AtomicLong consumerCount = new AtomicLong();
		protected final AtomicLong messageCount = new AtomicLong();
		protected QueueInfo(String name) {
			this.name = name;
		}
	}
	protected static class RetryQueueCounts {
		protected final Integer retryCount;
		protected final AtomicLong messageCount = new AtomicLong();
		protected final AtomicLong messageEnqueueMin = new AtomicLong();
		protected final AtomicLong messageEnqueueMax = new AtomicLong();
		protected RetryQueueCounts(Integer retryCount) {
			this.retryCount = retryCount;
		}
	}

	protected static class RetryQueueSummary {
		protected final String originalName;
		protected final Map<Integer,RetryQueueCounts> counts = new TreeMap<Integer, RetryQueueCounts>();
		protected RetryQueueSummary(String name) {
			this.originalName = name;
		}
	}
	protected static class MaintenanceContext {
		protected Configuration config;
		protected Interaction interaction;
		protected List<JMXConnector> connectors;
		/**
		 * @param out
		 */
		public boolean listVirtualTopics() {
			Map<String,Set<String>> virtualTopics;
			PrintWriter out = interaction.getWriter();
			try {
				virtualTopics = getVirtualTopics();
			} catch(Exception e) {
				out.println(e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
			out.println();
			out.println("Virtual Topic Name\t\t\tSubscribers");
			out.println("------------------\t\t\t-------------------------");
			String[] names = virtualTopics.keySet().toArray(new String[virtualTopics.size()]);
			Arrays.sort(names, sorter);
			for(String name : names) {
				out.print(name);
				out.print("\t\t\t");
				out.println(virtualTopics.get(name));
			}
			out.println("-------------");
			out.flush();
			return true;
		}
		/**
		 * @param out
		 */
		public boolean listQueues() {
			Map<String,QueueInfo> queues;
			PrintWriter out = interaction.getWriter();
			try {
				queues = getQueues();
			} catch(Exception e) {
				out.println(e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
			out.println();
			FixedWidthPrinter fwp = new FixedWidthPrinter(out);
			fwp.setColumnSeparator("  ");
			fwp.addColumn("Queue Name", 60, Justification.LEFT);
			fwp.addColumn("Consumer Count", 14, Justification.RIGHT);
			fwp.addColumn("Message Count", 13, Justification.RIGHT);
			for(QueueInfo info : queues.values()) {
				fwp.writeRow(info.name, info.consumerCount.get(), info.messageCount.get());
			}
			fwp.writeFooter();
			return true;
		}

		/**
		 * @param out
		 */
		public boolean summarizeRetryQueues() {
			Map<String,RetryQueueSummary> queues;
			PrintWriter out = interaction.getWriter();

			try {
				queues = getRetryQueueCounts_jms();
			} catch(Exception e) {
				out.println(e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
			out.println();
			FixedWidthPrinter fwp = new FixedWidthPrinter(out);
			fwp.setColumnSeparator("  ");
			fwp.addColumn("Original Queue Name", 60, Justification.LEFT);
			fwp.addColumn("Retry Count", 11, Justification.RIGHT);
			fwp.addColumn("Message Count", 13, Justification.RIGHT);
			fwp.addColumn("First Enqueue Date", 20, Justification.LEFT);
			fwp.addColumn("Last Enqueue Date", 20, Justification.LEFT);
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			for(RetryQueueSummary sum : queues.values()) {
				for(RetryQueueCounts cnts : sum.counts.values())
					fwp.writeRow(sum.originalName, cnts.retryCount == null || cnts.retryCount < 0 ? "DEAD" : cnts.retryCount, cnts.messageCount.get(), df.format(new Date(cnts.messageEnqueueMin.get())), df.format(new Date(cnts.messageEnqueueMax.get())));
			}
			fwp.writeFooter();
			return true;
		}
		/**
		 * @return
		 * @throws IOException
		 * @throws ReflectionException
		 * @throws MBeanException
		 * @throws NullPointerException
		 * @throws AttributeNotFoundException
		 * @throws InstanceNotFoundException
		 * @throws MalformedObjectNameException
		 */
		protected Map<String, Set<String>> getVirtualTopics() throws CancellationException, IllegalArgumentException, MalformedObjectNameException, InstanceNotFoundException, AttributeNotFoundException, NullPointerException, MBeanException, ReflectionException, IOException {
			Map<String,Set<String>> virtualTopics = new HashMap<String,Set<String>>();
			for(JMXConnector connector : getConnectors()) {
		    	MBeanServerConnection conn = connector.getMBeanServerConnection();
			    Set<ObjectInstance> brokerMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Broker"), null);
			    if(brokerMBeans.isEmpty())
			    	throw new InstanceNotFoundException("Could not find broker mbean");
			    ObjectInstance oi = brokerMBeans.iterator().next();
			    ObjectName[] subscribers = (ObjectName[])conn.getAttribute(oi.getObjectName(), "Queues");
			    for(ObjectName on : subscribers) {
	    			String dn = on.getKeyProperty("Destination");
	    			Matcher matcher = virtualTopicPattern.matcher(dn);
	    			if(matcher.matches()) {
	    				String subscriberName = matcher.group(1);
	    				String topicName = matcher.group(2);
	    				Set<String> subs = virtualTopics.get(topicName);
	    				if(subs == null) {
	    					subs = new HashSet<String>();
	    					virtualTopics.put(topicName, subs);
	    				}
	    				if(subs.add(subscriberName))
	    					interaction.getWriter().println("Found subscriber '" + subscriberName + "' for virtual topic '" + topicName + "'");
	    			}
	    		}
		    }
			return virtualTopics;
		}
		/**
		 * @return
		 * @throws IOException
		 * @throws ReflectionException
		 * @throws MBeanException
		 * @throws NullPointerException
		 * @throws AttributeNotFoundException
		 * @throws InstanceNotFoundException
		 * @throws MalformedObjectNameException
		 */
		protected Map<String, QueueInfo> getQueues() throws CancellationException, IllegalArgumentException, MalformedObjectNameException, InstanceNotFoundException, AttributeNotFoundException, NullPointerException, MBeanException, ReflectionException, IOException {
			Map<String,QueueInfo> queues = new TreeMap<String,QueueInfo>();
			for(JMXConnector connector : getConnectors()) {
		    	MBeanServerConnection conn = connector.getMBeanServerConnection();
			    Set<ObjectInstance> queueMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Queue,Destination=*"), null);
			    if(queueMBeans.isEmpty())
			    	interaction.getWriter().println("No queues found on " + connector.getConnectionId());
			    else
			    	for(ObjectInstance oi : queueMBeans) {
			    		String queueName = oi.getObjectName().getKeyProperty("Destination");
			    		Long consumerCount = (Long)conn.getAttribute(oi.getObjectName(), "ConsumerCount");
			    		Long messageCount = (Long)conn.getAttribute(oi.getObjectName(), "QueueSize");
			    		QueueInfo info = queues.get(queueName);
			    		if(info == null) {
			    			info = new QueueInfo(queueName);
			    			queues.put(queueName, info);
			    		}
			    		if(consumerCount != null)
			    			info.consumerCount.addAndGet(consumerCount);
			    		if(messageCount != null)
			    			info.messageCount.addAndGet(messageCount);
				    }

		    }
			return queues;
		}
		protected Map<String, RetryQueueSummary> getRetryQueueCounts_jmx() throws CancellationException, IllegalArgumentException, MalformedObjectNameException, InstanceNotFoundException, AttributeNotFoundException, NullPointerException, MBeanException, ReflectionException, IOException, ConvertException {
			Map<String,RetryQueueSummary> queues = new TreeMap<String,RetryQueueSummary>();
			for(JMXConnector connector : getConnectors()) {
		    	MBeanServerConnection conn = connector.getMBeanServerConnection();
			    Set<ObjectInstance> retryQueueMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Queue,Destination=" + retryQueuePrefix + "*"), null);
			    Set<ObjectInstance> deadLetterQueueMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Queue,Destination=" + deadLetterQueueName), null);
			    CompositeSet<ObjectInstance> queueMBeans = new CompositeSet<ObjectInstance>();
			    queueMBeans.merge(retryQueueMBeans);
			    queueMBeans.merge(deadLetterQueueMBeans);
			    if(queueMBeans.isEmpty())
			    	interaction.getWriter().println("No queues found on " + connector.getConnectionId());
			    else
			    	for(ObjectInstance oi : queueMBeans) {
			    		String queueName = oi.getObjectName().getKeyProperty("Destination");
			    		int retryCount;
			    		if(queueName.equalsIgnoreCase(deadLetterQueueName))
			    			retryCount = -1;
			    		else
			    			retryCount = ConvertUtils.getInt(queueName.substring(retryQueuePrefix.length()));
			    		/*
			    		CompositeData[] messages = (CompositeData[])conn.invoke(oi.getObjectName(), "browse", null, null);
			    		for(CompositeData message : messages) {
			    		*/
			    		TabularData messages = (TabularData)conn.invoke(oi.getObjectName(), "browseAsTable", null, null);
			    		for(Object key : messages.keySet()) {
			    			CompositeData message = messages.get(((List<?>)key).toArray());
			    			// */
			    			String origQueueName = (String)message.get("JMSReplyTo");
			    			long enqueueTime = ((Date)message.get("JMSTimestamp")).getTime();
			    			//String messageId = (String)message.get("JMSMessageID");
			    			addMessageInfoToCounts(queues, origQueueName, retryCount, enqueueTime);
			    		}
				    }

		    }
			return queues;
		}

		protected void addMessageInfoToCounts(Map<String, RetryQueueSummary> queues, String origQueueName, int retryCount, long enqueueTime) {
			RetryQueueSummary sum = queues.get(origQueueName);
    		if(sum == null) {
    			sum = new RetryQueueSummary(origQueueName);
    			queues.put(origQueueName, sum);
    		}
    		RetryQueueCounts cnts = sum.counts.get(retryCount);
    		if(cnts == null) {
    			cnts = new RetryQueueCounts(retryCount);
    			sum.counts.put(retryCount, cnts);
    		}
    		cnts.messageCount.incrementAndGet();

    		if(cnts.messageEnqueueMax.get() == 0 || cnts.messageEnqueueMax.get() < enqueueTime) {
    			cnts.messageEnqueueMax.set(enqueueTime);
    		}
    		if(cnts.messageEnqueueMin.get() == 0 || cnts.messageEnqueueMin.get() > enqueueTime) {
    			cnts.messageEnqueueMin.set(enqueueTime);
    		}
		}
		protected void updateRetryQueueCounts_jms(Map<String, RetryQueueSummary> queues, ExtendedSession session, int retryCount) throws JMSException {
			String queueName;
			if(retryCount < 0)
				queueName = deadLetterQueueName;
			else
				queueName = retryQueuePrefix + retryCount;
			QueueBrowser browser = session.createBrowser((Queue)JMSBase.getDestination(queueName, false, session));
			try {
				for(Enumeration<?> messages = browser.getEnumeration(); messages.hasMoreElements(); ) {
					Message message = (Message) messages.nextElement();
					addMessageInfoToCounts(queues, message.getJMSReplyTo().toString(), retryCount, message.getJMSTimestamp());
				}
			} finally {
				browser.close();
			}
		}

		protected Map<String, RetryQueueSummary> getRetryQueueCounts_jms() throws JMSException, NamingException, InterruptedException, TimeoutException {
			Map<String,RetryQueueSummary> queues = new TreeMap<String,RetryQueueSummary>();
			ExtendedSession session = JMSBase.createSession(true);
			try {
				updateRetryQueueCounts_jms(queues, session, -1);
				for(int i = 1; i < 11; i++)
					updateRetryQueueCounts_jms(queues, session, i);
			} finally {
				session.close();
			}
			return queues;
		}
		protected boolean retryMessages_jms(Date after, String queueName, String origQueueNameMatch, int maxBatchSize) throws JMSException, TimeoutException, NamingException, InterruptedException {
			if(!queueName.equals(deadLetterQueueName) && !queueName.startsWith(retryQueuePrefix)) {
				interaction.getWriter().println("Queue '" + queueName + "' is neither a dead-letter queue nor a retry queue. Messages in it cannot be retried");
				return false;
			}
			RetryQueueCounts cnts = new RetryQueueCounts(null);
			ExtendedSession session = JMSBase.createSession(true);
			try {
				StringBuilder selector = new StringBuilder();
				if(origQueueNameMatch != null && origQueueNameMatch.length() > 0)
					selector.append("JMSReplyTo LIKE '" + origQueueNameMatch.replace("'", "''") + "'");
				if(after != null) {
					if(selector.length() > 0)
						selector.append(" AND ");
					selector.append("JMSTimestamp >= " + after.getTime());
				}

				MessageConsumer consumer = session.createConsumer(JMSBase.getDestination(queueName, false, session), selector.toString());
				interaction.getWriter().println("Finding messages in '" + queueName + "' with selector '" + selector.toString() + "'");
				
				MessageProducer producer = session.createProducer(null);
				int batchCnt = 0;
				Message message;
				while((message=consumer.receive(1000)) != null) {
					boolean okay = JMSBase.resurrectMessage(message, producer);
					if(!okay) {
						interaction.getWriter().println("Could not move message '" + message.getJMSMessageID() + "' from " + queueName + " to " + message.getJMSReplyTo() + "; rolling back retry of " + batchCnt + " messages");
						session.rollback();
						batchCnt = 0;
					} else {
						interaction.getWriter().println("Moved message '" + message.getJMSMessageID() + "' from " + queueName + " to " + message.getJMSReplyTo());
	    				long enqueueTime = message.getJMSTimestamp();
						cnts.messageCount.incrementAndGet();
	    				if(cnts.messageEnqueueMax.get() == 0 || cnts.messageEnqueueMax.get() < enqueueTime) {
			    			cnts.messageEnqueueMax.set(enqueueTime);
			    		}
			    		if(cnts.messageEnqueueMin.get() == 0 || cnts.messageEnqueueMin.get() > enqueueTime) {
			    			cnts.messageEnqueueMin.set(enqueueTime);
			    		}
			    		if(++batchCnt >= maxBatchSize)
			    			session.commit();
					}
				}
				if(batchCnt > 0)
					session.commit();
			} finally {
				session.close();
			}

			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			interaction.getWriter().println("Moved " + cnts.messageCount.get() + " messages enqueued between " +
					df.format(new Date(cnts.messageEnqueueMin.get())) + " and " +
					df.format(new Date(cnts.messageEnqueueMax.get())) + " from '" + queueName + "' to their original queue");
			return true;
		}
		protected boolean retryMessages_jmx(Date after, String queueName, String origQueueNameMatch) throws CancellationException, IllegalArgumentException, MalformedObjectNameException, InstanceNotFoundException, AttributeNotFoundException, NullPointerException, MBeanException, ReflectionException, IOException, ConvertException {
			if(!queueName.equals(deadLetterQueueName) && !queueName.startsWith(retryQueuePrefix)) {
				interaction.getWriter().println("Queue '" + queueName + "' is neither a dead-letter queue nor a retry queue. Messages in it cannot be retried");
				return false;
			}
			RetryQueueCounts cnts = new RetryQueueCounts(null);
			for(JMXConnector connector : getConnectors()) {
		    	MBeanServerConnection conn = connector.getMBeanServerConnection();
			    Set<ObjectInstance> queueMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Queue,Destination=" + queueName), null);
			    if(queueMBeans.isEmpty())
			    	interaction.getWriter().println("Queue '" + queueName + "' does not exist on " + connector.getConnectionId());
			    else
			    	for(ObjectInstance oi : queueMBeans) {
			    		TabularData messages = (TabularData)conn.invoke(oi.getObjectName(), "browseAsTable", null, null);
			    		for(Iterator<?> iter = messages.keySet().iterator(); iter.hasNext(); ) {
			    			Object key = iter.next();
			    			CompositeData message = messages.get(((List<?>)key).toArray());
			    			// */
			    			String origQueueName = (String)message.get("JMSReplyTo");
			    			if(origQueueNameMatch == null || origQueueNameMatch.length() == 0 || origQueueName.matches(origQueueNameMatch)) {
				    			long enqueueTime = ((Date)message.get("JMSTimestamp")).getTime();
				    			if(after == null || enqueueTime >= after.getTime()) {
					    			String messageId = (String)message.get("JMSMessageID");
					    			Boolean okay = (Boolean)conn.invoke(oi.getObjectName(), "moveMessageTo", new Object[] {messageId, origQueueName}, new String[] {String.class.getName(), String.class.getName()});
					    			if(okay) {
					    				interaction.getWriter().println("Moved message '" + messageId + "' from " + queueName + " to " + origQueueName);
					    				cnts.messageCount.incrementAndGet();
					    				if(cnts.messageEnqueueMax.get() == 0 || cnts.messageEnqueueMax.get() < enqueueTime) {
							    			cnts.messageEnqueueMax.set(enqueueTime);
							    		}
							    		if(cnts.messageEnqueueMin.get() == 0 || cnts.messageEnqueueMin.get() > enqueueTime) {
							    			cnts.messageEnqueueMin.set(enqueueTime);
							    		}
							    		iter.remove();
					    			} else
					    				interaction.getWriter().println("Could not move message '" + messageId + "' from " + queueName + " to " + origQueueName);
				    			}
			    			}
			    		}
				    }
		    }
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			interaction.getWriter().println("Moved " + cnts.messageCount.get() + " messages enqueued between " +
					df.format(new Date(cnts.messageEnqueueMin.get())) + " and " +
					df.format(new Date(cnts.messageEnqueueMax.get())) + " from '" + queueName + "' to their original queue");
			return true;
		}
		/**
		 * @param out
		 * @return
		 * @throws IOException
		 */
		protected List<JMXConnector> getConnectors() throws CancellationException, IllegalArgumentException, IOException {
			if(connectors == null) {
				String[] urls = config.getStringArray("USAT.jmx.activemq.maintenance.urls");
				if(urls == null || urls.length == 0) {
					throw new IllegalArgumentException("The 'USAT.jmx.activemq.maintenance.urls' property is not specified in the properties file");
				} else if(urls.length == 1) {
					urls = StringUtils.split(urls[0], StringUtils.STANDARD_DELIMS, false);
				}
				String line = interaction.readLine("Enter the user name to login to the ActiveMQ JMX Servers [" + Arrays.toString(urls) + "]:");
				if(line == null) {
					throw new CancellationException("Exitting...");
				}
				line = line.trim();
				Map<String,Object> environment = new HashMap<String,Object>();
			    if(line.length() > 0) {
					String username = line;
					char[] pwd = interaction.readPassword("Enter the password to login to the ActiveMQ JMX Servers for " + username + ":");
					if(pwd == null) {
						throw new CancellationException("Exitting...");
					}
					String password = new String(pwd);
					environment.put(JMXConnector.CREDENTIALS,  new String[] {username, password });
				}

				interaction.getWriter().println("Connecting to ActiveMQ Servers...");
			    List<JMXConnector> connectors = new ArrayList<JMXConnector>(urls.length);
			    for(String url : urls) {
			    	if(url != null && (url=url.trim()).length() > 0) {
			    		JMXServiceURL address = new JMXServiceURL(url);
			    		JMXConnector connector = JMXConnectorFactory.connect(address, environment);
			    	    connectors.add(connector);
			    	    interaction.getWriter().println("Successfully connected to '" + url + "'");
			    	}
			    }
			    this.connectors = connectors;
			}
			return connectors;
		}
		/**
		 * @param string
		 * @param out
		 * @return
		 */
		public boolean retryDeadMessages(Date after, String origQueueName) {
			try {
				retryMessages_jms(after, deadLetterQueueName, origQueueName, 100);
			} catch(Exception e) {
				interaction.getWriter().println(e.getMessage());
				e.printStackTrace(System.err);
				//return false;
			}
			return true;
		}
		/**
		 * @param string
		 * @param out
		 * @return
		 */
		public boolean retryMessages(int retryNumber, Date after, String origQueueName) {
			try {
				retryMessages_jms(after, retryQueuePrefix + retryNumber, origQueueName, 100);
			} catch(Exception e) {
				interaction.getWriter().println(e.getMessage());
				e.printStackTrace(System.err);
				//return false;
			}
			return true;
		}
		/**
		 * @param string
		 * @param out
		 * @return
		 */
		public boolean purgeVirtualTopic(String virtualTopic, String subscriber) {
			try {
				purgeQueue("Consumer." + subscriber + ".VirtualTopic." + virtualTopic);
			} catch(Exception e) {
				interaction.getWriter().println(e.getMessage());
				e.printStackTrace(System.err);
				//return false;
			}
			return true;
		}
		/**
		 * @param string
		 * @param out
		 * @return
		 */
		public boolean deleteVirtualTopic(String virtualTopic, String subscriber) {
			try {
				deleteQueue("Consumer." + subscriber + ".VirtualTopic." + virtualTopic);
			} catch(Exception e) {
				interaction.getWriter().println(e.getMessage());
				e.printStackTrace(System.err);
				//return false;
			}
			return true;
		}
		public boolean purgeQueueSafely(String queueName) {
			try {
				purgeQueue(queueName);
			} catch(Exception e) {
				interaction.getWriter().println(e.getMessage());
				e.printStackTrace(System.err);
				//return false;
			}
			return true;
		}
		public boolean deleteQueueSafely(String queueName) {
			try {
				deleteQueue(queueName);
			} catch(Exception e) {
				interaction.getWriter().println(e.getMessage());
				e.printStackTrace(System.err);
				//return false;
			}
			return true;
		}
		protected void purgeQueue(String queueName) throws CancellationException, MalformedObjectNameException, InstanceNotFoundException, IllegalArgumentException, NullPointerException, MBeanException, ReflectionException, IOException {
			for(JMXConnector connector : getConnectors()) {
		    	MBeanServerConnection conn = connector.getMBeanServerConnection();
			    Set<ObjectInstance> queueMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Queue,Destination=" + queueName), null);
			    if(queueMBeans.isEmpty())
			    	interaction.getWriter().println("Queue '" + queueName + "' does not exist on " + connector.getConnectionId());
			    else
			    	for(ObjectInstance oi : queueMBeans) {
					    conn.invoke(oi.getObjectName(), "purge", null, null);
					    interaction.getWriter().println("Purged '" + queueName + "' on " + connector.getConnectionId());
				    }
		    }
		}
		protected void deleteQueue(String queueName) throws CancellationException, MalformedObjectNameException, InstanceNotFoundException, IllegalArgumentException, NullPointerException, MBeanException, ReflectionException, IOException {
			for(JMXConnector connector : getConnectors()) {
		    	MBeanServerConnection conn = connector.getMBeanServerConnection();
			    Set<ObjectInstance> brokerMBeans = conn.queryMBeans(new ObjectName("org.apache.activemq:BrokerName=*,Type=Broker"), null);
			    if(brokerMBeans.isEmpty())
			    	throw new InstanceNotFoundException("Could not find broker mbean");
			    for(ObjectInstance oi : brokerMBeans) {
				    conn.invoke(oi.getObjectName(), "removeQueue", new Object[] {queueName}, new String[] {"java.lang.String"});
				    interaction.getWriter().println("Deleted '" + queueName + "' on " + connector.getConnectionId());
			    }
		    }
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new QueueMaintenance().run(args);
	}

	@Override
	protected MaintenanceContext createContext(Configuration config, BufferedReader in, PrintStream out) {
		MaintenanceContext mc = new MaintenanceContext();
		Console console;
		if(out == System.out && (console = System.console()) != null) {
			mc.interaction = new ConsoleInteraction(console);
		} else {
			mc.interaction = new InOutInteraction(new PrintWriter(out, true), in);
		}
		mc.config = config;
		return mc;
	}

	/**
	 * @see simple.app.CommandLineInterface#getCommands(java.util.Properties)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Command<MaintenanceContext>[] getCommands(Configuration config) {
		return (Command<MaintenanceContext>[]) new Command<?>[] {
				new AbstractCommand<MaintenanceContext>("quit", "Exits the program", null) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						out.println("Exiting...");
						out.flush();
						return false;
					}
				},
				new AbstractCommand<MaintenanceContext>("listVirtualTopics", "Lists all virtual topics present on the set of JMS Brokers", null) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.listVirtualTopics();
					}
				},
				new AbstractCommand<MaintenanceContext>("purgeVirtualTopic", "Purges the specified subscriber on the virtual topic on each of the JMS Brokers",
					new CommandArgument[] {
						new BasicCommandArgument("virtualTopic", String.class, "The virtual topic to purge", false),
						new BasicCommandArgument("subscriber", String.class, "The subscriber to purge", false)}) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.purgeVirtualTopic((String)arguments[0], (String)arguments[1]);
					}
				},
				new AbstractCommand<MaintenanceContext>("deleteVirtualTopic", "Deletes the specified subscriber on the virtual topic on each of the JMS Brokers",
					new CommandArgument[] {
						new BasicCommandArgument("virtualTopic", String.class, "The virtual topic to delete", false),
						new BasicCommandArgument("subscriber", String.class, "The subscriber to delete", false)}) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.deleteVirtualTopic((String)arguments[0], (String)arguments[1]);
					}
				},
				new AbstractCommand<MaintenanceContext>("listQueues", "Lists all queues present on the set of JMS Brokers",
					null) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.listQueues();
					}
				},
				new AbstractCommand<MaintenanceContext>("purgeQueue", "Purges the specified queue on each of the JMS Brokers",
					new CommandArgument[] {
						new BasicCommandArgument("queueName", String.class, "The queue to purge", false)}) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.purgeQueueSafely((String)arguments[0]);
					}
				},
				new AbstractCommand<MaintenanceContext>("deleteQueue", "Deletes the specified queue on each of the JMS Brokers",
					new CommandArgument[] {
						new BasicCommandArgument("queueName", String.class, "The queue to delete", false)}) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.deleteQueueSafely((String)arguments[0]);
					}
				},
				new AbstractCommand<MaintenanceContext>("listRetryQueueCounts", "Lists a summary of all the messages in the retry and dead-letter queues on the set of JMS Brokers",
					null) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.summarizeRetryQueues();
					}
				},
				new AbstractCommand<MaintenanceContext>("retryDeadMessages", "Moves messages in the dead-letter queue back into their original queue on each of the JMS Brokers",
					new CommandArgument[] {
						new BasicCommandArgument("after", Date.class, "Messages enqueued on or after this date are moved; those enqueued before this date are ignored. Enter nothing to move all messages", true),
						new BasicCommandArgument("origQueueName", String.class, "The queue name to which messages are moved if they originally came from that queue. Enter \"%\" to move all messages", false)}) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.retryDeadMessages((Date)arguments[0], (String)arguments[1]);
					}
				},
				new AbstractCommand<MaintenanceContext>("retryMessages", "Moves messages in a retry queue (of a specific number) back into their original queue on each of the JMS Brokers",
					new CommandArgument[] {
						new BasicCommandArgument("retryNumber", Integer.class, "The retry number of the queue from which to move messages [1-9]", false),
						new BasicCommandArgument("after", Date.class, "Messages enqueued on or after this date are moved; those enqueued before this date are ignored. Enter nothing to move all messages", true),
						new BasicCommandArgument("origQueueName", String.class, "The queue name to which messages are moved if they originally came from that queue. Enter \"%\" to move all messages", true)}) {
					public boolean executeCommand(MaintenanceContext context, PrintWriter out, Object[] arguments) {
						return context.retryMessages((Integer)arguments[0], (Date)arguments[1], (String)arguments[2]);
					}
				},
		};
	}

}
