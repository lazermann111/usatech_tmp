/**
 *
 */
package com.usatech.layers.common;

import java.util.Map;

/**
 * @author Brian S. Krug
 *
 */
public interface DeviceInfoUpdateListener {
	public void propertiesUpdated(Map<String,Object> updatedProperties, Map<String,Long> timestamps) ;
}
