/**
 *
 */
package com.usatech.layers.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.util.CompositeMap;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.task.ExecuteDbCallTask;
import com.usatech.app.task.SQLExceptionHandler;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.SessionControlAction;

/**
 * @author Brian S. Krug
 *
 */
public class AuthInsertTask extends ExecuteDbCallTask {
	private static final Log log = Log.getLog();
	protected static final String[] sensitiveDataAttributes = new String[] {
		"pan", "cardHolder","expirationDate", "pin"
	};

	/**
	 * @see com.usatech.app.task.ExecuteDbCallTask#process(java.lang.String, java.util.Map, java.util.Map, simple.app.Publisher)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		String id = getCallId();
		if(id == null || id.trim().length() == 0)
			throw new ServiceException("CallId property is not set on " + this);
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnectionForCall(id);
		} catch(SQLException e) {
			throw new RetrySpecifiedServiceException("Could not get connection for AuthInsertTask", e, WorkRetryType.BLOCKING_RETRY);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Could not get connection for AuthInsertTask", e, WorkRetryType.BLOCKING_RETRY);
		}
		final MessageChainStep step = taskInfo.getStep();
		try {
			AuthResultCode result = process(conn, step.getAttributes(), step.getResultAttributes(), taskInfo.isRedelivered(), taskInfo.getPublisher());
			try {
				conn.commit();
			} catch(SQLException e) {
				ProcessingUtils.rollbackDbConnection(log, conn);
				throw new RetrySpecifiedServiceException("Could not commit changes for AuthInsertTask", e, WorkRetryType.NONBLOCKING_RETRY);
			}
			return determineResultInt(result, step);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	public int determineResultInt(AuthResultCode result, MessageChainStep step) {
		switch(result) {
			case APPROVED:
			case PARTIAL:
			case AVS_MISMATCH:
			case CVV_MISMATCH:
			case CVV_AND_AVS_MISMATCH:
				return 0;
			default:
				if(step.getResultCodes().contains(1))
					return 1;
				return 0;
		}

	}
	
	public AuthResultCode process(Connection conn, Map<String, Object> attributes, Map<String, Object> resultAttributes, boolean redelivered, Publisher<ByteInput> publisher) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		if(redelivered) {
			try {
				DataLayerMgr.selectInto(conn, "FIND_AUTH", attributes, resultAttributes);
				Long tranId = ConvertUtils.convert(Long.class, resultAttributes.get("tranId"));
				if(tranId != null) {
					try {
						possiblyCheckTerminal(tranId, ConvertUtils.convertRequired(Character.class, resultAttributes.get("tranStateCd")), conn, publisher);
					} catch(ConvertException e) {
						log.warn("Could not convert tranStateCd to a char - not sending checkTerminal to POSM", e);
					}
					return ConvertUtils.convert(AuthResultCode.class, resultAttributes.get("authResultCd"));
				}
			} catch(NotEnoughRowsException e) {
				log.debug("Message is redelivered but auth is not already saved in database. Processing normally...");
			} catch(SQLException e) {
				SQLExceptionHandler handler = getSqlExceptionHandler();
				if(handler != null)
					handler.handleSQLException("FIND_AUTH", params, e);
				else
					throw new RetrySpecifiedServiceException("While trying to find auth", e, WorkRetryType.NONBLOCKING_RETRY);
			} catch(DataLayerException e) {
				throw new RetrySpecifiedServiceException("While trying to find auth", e, WorkRetryType.NONBLOCKING_RETRY);
			} catch(BeanException e) {
				throw new RetrySpecifiedServiceException("While trying to find auth", e, WorkRetryType.NONBLOCKING_RETRY);
			} catch(ConvertException e) {
				throw new ServiceException("Could not convert tranId to a Long", e);
			}
		}
		
		try {
			long requestStartTimestamp = ConvertUtils.getLong(attributes.get(AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP.getValue()), 0);
			if (requestStartTimestamp > 0) {
				Long paymentSubtypeKeyId = ConvertUtils.getLong(attributes.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue()), 0);
				EMVUtils.processEMVParameterDownloadRequiredFlag(paymentSubtypeKeyId);
			}
		} catch (ConvertException e) {
			
		}
		
		long authTime;
		try {
			authTime = ConvertUtils.getLong(attributes.get("authTime"));
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not convert authTime to a long", e, WorkRetryType.NO_RETRY);
		}
		String timeZoneGuid;
		try {
			timeZoneGuid = ConvertUtils.getString(attributes.get("timeZoneGuid"), true);
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not get timezone for the device", e, WorkRetryType.NO_RETRY);
		}
		Long consumerAcctId = ConvertUtils.convertSafely(Long.class, attributes.get("consumerAcctId"), null);
		char paymentType;
		try {
			paymentType = ConvertUtils.convertRequired(Character.class, attributes.get("paymentType"));
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not convert payment type to a char", e, WorkRetryType.NO_RETRY);
		}
		if(consumerAcctId == null && attributes.get(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue()) != null) {
			String lookupCACallId;
			if(attributes.containsKey(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue()))
				lookupCACallId = "GET_CONSUMER_ACCT_FOR_TRAN";
			else
				lookupCACallId = "GET_CONSUMER_ACCT_FOR_TRAN_OLD";
			try {
				DataLayerMgr.executeCall(conn, lookupCACallId, attributes, params);
				conn.commit();
			} catch(SQLException e) {
				SQLExceptionHandler handler = getSqlExceptionHandler();
				if(handler != null)
					handler.handleSQLException(lookupCACallId, attributes, e);
				else
					throw new RetrySpecifiedServiceException("Could not get consumerAcctId for the auth", e, WorkRetryType.NONBLOCKING_RETRY); //TODO: maybe blocking retry would be better for some of these
			} catch(DataLayerException e) {
				throw new RetrySpecifiedServiceException("Could not get consumerAcctId for the auth", e, WorkRetryType.NONBLOCKING_RETRY);
			}
			consumerAcctId = ConvertUtils.convertSafely(Long.class, params.get("consumerAcctId"), null);
		}
		params.put("tranStartTime", ConvertUtils.getLocalTime(authTime, timeZoneGuid));
		AuthResultCode result;
		try {
			result = ConvertUtils.convertRequired(AuthResultCode.class, attributes.get("authResultCd"));
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not get authResultCode for the authorization", e, WorkRetryType.NO_RETRY);
		}
		//if auth was approved or if authHoldUsed = true, then do not wipe out sensitive data as we need it for settlement
		switch(result) {
			case APPROVED:
			case PARTIAL:
				params.put("passThru", false);
				break;
			case FAILED:
				if(ConvertUtils.getBooleanSafely(attributes.get("authHoldUsed"), false))
					break;
				//otherwise, fall-through					
			default:
				for(String s : sensitiveDataAttributes)
					params.put(s, null);
		}
		CompositeMap<String, Object> merged;
		if(attributes instanceof CompositeMap) {
			merged = (CompositeMap<String, Object>) attributes;
			merged.merge(0, params);
		} else {
			merged = new CompositeMap<String, Object>();
			merged.merge(params);
			merged.merge(attributes);
		}
		
		if (merged.get("consumerAcctId") != null && (merged.get("prepaidFlag") != null || merged.get("creditDebitInd") != null || merged.get("countryCode") != null)) {
			// update consumer_acct_base with additional data collected during the auth
			try {
				DataLayerMgr.executeUpdate(conn, "UPDATE_CONSUMER_ACCT_DATA", merged);
			} catch (Exception e) {
				log.warn("Update consumer_acct_base {0} failed: prepaidFlag={1}, creditDebitInd={2}, countryCode={3}", merged.get("consumerAcctId"), merged.get("prepaidFlag"), merged.get("creditDebitInd"), merged.get("countryCode"), e);
			}
		}
		
		String id = getCallId();
		if(id == null || id.trim().length() == 0)
			throw new ServiceException("CallId property is not set on " + this);
		
		if(log.isInfoEnabled())
			log.info("Inserting auth on consumer acct " + consumerAcctId + " for pos pta " + attributes.get("posPtaId") + " with " + merged);
		try {
			DataLayerMgr.executeCall(conn, id, merged, resultAttributes);
			authInserted(conn, result, merged, resultAttributes);
			conn.commit();
		} catch(SQLException e) {
			SQLExceptionHandler handler = getSqlExceptionHandler();
			if(handler != null)
				handler.handleSQLException(id, merged, e);
			else
				throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		
		long tranId;
		char tranStateCd;
		char tranImportNeeded;
		char sessionUpdateNeeded;
		try {
			tranId = ConvertUtils.getLong(resultAttributes.get("tranId"));
			tranStateCd = ConvertUtils.convertRequired(Character.class, resultAttributes.get("tranStateCd"));
			tranImportNeeded = ConvertUtils.convertRequired(Character.class, resultAttributes.get("tranImportNeeded"));
			sessionUpdateNeeded = ConvertUtils.convertRequired(Character.class, resultAttributes.get("sessionUpdateNeeded"));
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert tranId to a long, tranStateCd to a char, tranImportNeeded to a char or sessionUpdateNeeded to a char from attributes: " + resultAttributes, e);
		}
		if(tranId > 0) {
			if(tranImportNeeded == 'Y') {
				InteractionUtils.publishTranImport(tranId, publisher, log);
				InteractionUtils.checkForReplenishBonuses(tranId, conn, publisher, log);
			} else if((result == AuthResultCode.APPROVED || result == AuthResultCode.PARTIAL) && attributes.get("authActionId") != null) {
				try {
					long authId = ConvertUtils.getLong(resultAttributes.get("authId"));
					Boolean createDEX = AppLayerUtils.getDEXNeededForAuth(log, conn, params, authId);
					if(createDEX != null && createDEX.booleanValue()) {
						DataLayerMgr.executeCall(conn, "CREATE_DEX_FOR_FILL", params);
						conn.commit();
					}
				} catch(ConvertException | SQLException | DataLayerException e) {
					throw new ServiceException("Could not create dex for fill authorization", e);
				}
			}
		}
		if (sessionUpdateNeeded == 'Y') {
			Map<String, Object> sessionParams = new HashMap<String, Object>();
			sessionParams.put("deviceName", merged.get("deviceName"));
			sessionParams.put("minorCurrencyFactor", merged.get("minorCurrencyFactor"));
			sessionParams.put("saleGlobalSessionCode", resultAttributes.get("saleGlobalSessionCode"));
			sessionParams.put("saleSessionStartTime", resultAttributes.get("saleSessionStartTime"));
			sessionParams.put("clientPaymentTypeCd", resultAttributes.get("clientPaymentTypeCd"));
			sessionParams.put("saleAmount", resultAttributes.get("saleAmount"));
			sessionParams.put("tranLineItemCount", resultAttributes.get("tranLineItemCount"));
			InteractionUtils.publishSessionControl(SessionControlAction.ADD_TRAN_TO_DEVICE_SESSION.toString(), sessionParams, publisher, log);
		}
		
		possiblyCheckTerminal(tranId, tranStateCd, conn, publisher);
		return result;
	}

	/**
	 * @param conn
	 * @param result
	 * @param attributes
	 * @param resultAttributes
	 * @throws ServiceException
	 * @throws DataLayerException
	 * @throws SQLException
	 */
	protected void authInserted(Connection conn, AuthResultCode result, Map<String, Object> attributes, Map<String, Object> resultAttributes) throws ServiceException, SQLException, DataLayerException {
		// Do nothing - hook for subclasses
	}

	/**
	 * @param posPtaId
	 * @param convert
	 * @throws BeanException
	 * @throws DataLayerException
	 * @throws SQLException
	 * @throws ServiceException
	 */
	protected void possiblyCheckTerminal(long tranId, char tranStateCd, Connection conn, Publisher<ByteInput> publisher) {
		switch(tranStateCd) {
			case '8': case '9': case 'W': //post message to check queue
				try {
					InteractionUtils.publishCheckTerminal(tranId, conn, publisher, log);
				} catch(ServiceException e) {
					log.warn("Could not post message to check terminal queue", e);
				} catch(SQLException e) {
					log.warn("Could not post message to check terminal queue", e);
				} catch(DataLayerException e) {
					log.warn("Could not post message to check terminal queue", e);
				} catch(BeanException e) {
					log.warn("Could not post message to check terminal queue", e);
				}
		}
	}
}
