/**
 *
 */
package com.usatech.layers.common;

/**
 * @author Brian S. Krug
 *
 */
public enum MessageResponse {
	CONTINUE_SESSION, CLOSE_SESSION
}
