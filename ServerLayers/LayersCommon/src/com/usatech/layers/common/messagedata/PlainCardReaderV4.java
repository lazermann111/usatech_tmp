package com.usatech.layers.common.messagedata;

public interface PlainCardReaderV4 extends CardReader {

	public String getAccountData1();

	public String getAccountData2();

	public String getAccountData3();
}