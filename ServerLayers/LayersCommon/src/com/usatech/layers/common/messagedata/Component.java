/**
 *
 */
package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.ComponentType;

/**
 * @author Brian S. Krug
 *
 */
public interface Component {
	public ComponentType getComponentType() ;

	public void setComponentType(ComponentType componentType) ;

	public int getComponentNumber() ;

	public String getManufacturer();

	public void setManufacturer(String manufacturer);

	public String getModel();

	public void setModel(String model);

	public String getSerialNumber();

	public void setSerialNumber(String serialNumber);

	public String getRevision();

	public void setRevision(String revision);

	public String getLabel();

	public void setLabel(String label);
}