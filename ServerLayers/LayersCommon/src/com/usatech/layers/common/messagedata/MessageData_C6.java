package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.Calendar;
import simple.lang.InvalidIntValueException;

/**
 *  Message Data for GENERIC_EVENT_4_1 - "Generic Event 4.1 - C6h"
 */
public class MessageData_C6 extends AbstractMessageData {
	protected Calendar eventTime;
	protected int componentNumber;
	protected EventType eventType = EventType.BATCH;
	protected long eventId;
	protected String eventData;

	public MessageData_C6() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GENERIC_EVENT_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeTimestamp(reply, getEventTime());
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.writeShortInt(reply, getEventType().getValue());
		ProcessingUtils.writeLongInt(reply, getEventId());
		ProcessingUtils.writeLongString(reply, getEventData(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setEventTime(ProcessingUtils.readTimestamp(data));
		setComponentNumber(ProcessingUtils.readByteInt(data));
		try {
			setEventType(EventType.getByValue(ProcessingUtils.readShortInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setEventId(ProcessingUtils.readLongInt(data));
		setEventData(ProcessingUtils.readLongString(data, getCharset()));
	}

	public Calendar getEventTime() {
		return eventTime;
	}

	public void setEventTime(Calendar eventTime) {
		this.eventTime = eventTime;
	}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; eventTime=").append(MessageDataUtils.toString(getEventTime()))
			.append("; componentNumber=").append(getComponentNumber())
			.append("; eventType=").append(getEventType())
			.append("; eventId=").append(getEventId())
			.append("; eventData=").append(getEventData());
		sb.append("]");

		return sb.toString();
	}
}
