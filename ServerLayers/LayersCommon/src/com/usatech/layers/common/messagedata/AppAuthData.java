package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;

public interface AppAuthData extends Byteable {
	public EntryType getEntryType();

	public long getAppMessageId();

	public CardReaderType getCardReaderType();

	public CardReader getCardReader();
}
