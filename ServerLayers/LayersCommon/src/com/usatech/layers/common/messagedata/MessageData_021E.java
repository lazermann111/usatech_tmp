package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.messagedata.WS2TokenizeMessageData.ExpirationDateContainer;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS2_TOKENIZE_PLAIN_REQUEST - "Web Service - Tokenize Plain Request - 021Eh"
 */
public class MessageData_021E extends WS2TokenizeMessageData implements PlainCardReader, ExpirationDateContainer {
	protected String cardNumber;
	protected String expirationDate;
	protected String securityCode;
	protected String cardHolder;

	public MessageData_021E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_TOKENIZE_PLAIN_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getTranId());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getCardNumber()) : getCardNumber(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getExpirationDate()) : getExpirationDate(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getSecurityCode()) : getSecurityCode(), charset);
		ProcessingUtils.writeLongString(reply, getCardHolder(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getBillingPostalCode()) : getBillingPostalCode(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getBillingAddress()) : getBillingAddress(), charset);
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setTranId(data.getLong());
		setCardNumber(ProcessingUtils.readLongString(data, charset));
		setExpirationDate(ProcessingUtils.readLongString(data, charset));
		setSecurityCode(ProcessingUtils.readLongString(data, charset));
		setCardHolder(ProcessingUtils.readLongString(data, charset));
		setBillingPostalCode(ProcessingUtils.readLongString(data, charset));
		setBillingAddress(ProcessingUtils.readLongString(data, charset));
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public String getAccountData() {
		return getCardNumber();
	}

	public void setAccountData(String accountData) {
		setCardNumber(accountData);
	}

	public CardReaderType getCardReaderType() {
		return CardReaderType.GENERIC;
	}

	public CardReader getCardReader() {
		return this;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public MessageData_0310 createResponse() {
		MessageData_0310 response = new MessageData_0310();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", tranId=").append(getTranId())
			.append(", cardNumber=").append(MessageResponseUtils.maskTrackData(getCardNumber()))
			.append(", expirationDate=").append(MessageDataUtils.maskString(getExpirationDate()))
			.append(", securityCode=").append(MessageDataUtils.maskString(getSecurityCode()))
			.append(", cardHolder=").append(getCardHolder())
			.append(", billingPostalCode=").append(MessageDataUtils.maskString(getBillingPostalCode()))
			.append(", billingAddress=").append(MessageDataUtils.maskString(getBillingAddress()))
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
