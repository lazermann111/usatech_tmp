package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidValueException;

/**
 *  Message Data for PERMISSION_REQUEST_3_1 - "Permission Request 3.1 - AAh"
 */
public class MessageData_AA extends AbstractMessageData implements PlainCardReader, AuthorizeMessageData {
	protected long transactionId;
	protected EntryType entryType = EntryType.BAR_CODE;
	protected String validationData;
	protected String accountData;

	public MessageData_AA() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.PERMISSION_REQUEST_3_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getEntryType().getValue());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getValidationData()) : getValidationData(), getCharset());
		ProcessingUtils.writeString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData()) : getAccountData(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setEntryType(EntryType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setValidationData(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData(ProcessingUtils.readString(data, data.remaining(), getCharset()));
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public EntryType getEntryType() {
		return entryType;
	}

	public void setEntryType(EntryType entryType) {
		this.entryType = entryType;
	}

	public String getValidationData() {
		return validationData;
	}

	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}

	public String getAccountData() {
		return accountData;
	}

	public void setAccountData(String accountData) {
		this.accountData = accountData;
	}


	public String getPin() {
		return null;
	}
	public Number getAmount() {
		return null;
	}
    public PlainCardReader getCardReader() {
       return this;
    }
    public CardReaderType getCardReaderType() {
       return CardReaderType.GENERIC;
    }
	public PaymentActionType getPaymentActionType() {
       return PaymentActionType.PURCHASE;
    }
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; entryType=").append(getEntryType())
			.append("; validationData=").append(MessageResponseUtils.maskFully(getValidationData()))
			.append("; accountData=").append(MessageResponseUtils.maskTrackData(getAccountData()));
		sb.append("]");

		return sb.toString();
	}
}
