package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for INITIALIZATION_3_0 - "Initialization 3.0 - 8Eh"
 */
public class MessageData_8E extends AbstractMessageData implements InitMessageData {
	protected DeviceType deviceType = DeviceType.DEFAULT;
	protected byte[] deviceSerialNumBytes;

	public MessageData_8E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.INITIALIZATION_3_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getDeviceType().getValue());
		ProcessingUtils.writeBytesRemaining(reply, getDeviceSerialNumBytes());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setDeviceType(DeviceType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setDeviceSerialNumBytes(ProcessingUtils.readBytesRemaining(data));
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public byte[] getDeviceSerialNumBytes() {
		return deviceSerialNumBytes;
	}

	public void setDeviceSerialNumBytes(byte[] deviceSerialNumBytes) {
		this.deviceSerialNumBytes = deviceSerialNumBytes;
	}

	@Override
	public int getPropertyListVersion() {
		return -1;
	}
		public String getDeviceInfo() {
			return "";
		}
		public String getTerminalInfo() {
			return "";
		}
		public String getDeviceSerialNum(){
			if(deviceType == DeviceType.KIOSK){
				// dll serial number is 8 binary bytes and needs to be prefixed
				return "K1"+StringUtils.toHex(getDeviceSerialNumBytes()).toUpperCase();
			}
		return new String(getDeviceSerialNumBytes(), charset);
		}
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; deviceType=").append(getDeviceType())
			.append("; deviceSerialNumBytes=").append(new String(getDeviceSerialNumBytes(), ProcessingConstants.US_ASCII_CHARSET))
			.append("; deviceSerialNum=").append(getDeviceSerialNum());
		sb.append("]");

		return sb.toString();
	}
}
