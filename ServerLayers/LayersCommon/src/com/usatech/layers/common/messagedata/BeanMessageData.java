package com.usatech.layers.common.messagedata;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.io.IOUtils;
import simple.text.StringUtils;
import simple.util.ConversionSet;
import simple.util.MapBackedSet;

import com.usatech.layers.common.ProcessingUtils;

/**
 * Base MessageData for Beans
 * 
 * @author bkrug
 * 
 */
public abstract class BeanMessageData extends AbstractMessageData {
	protected final static Set<String> SENSITIVE_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	static {
		SENSITIVE_PROPERTIES.add("PASSWORD");
		SENSITIVE_PROPERTIES.add("NEWPASSWORD");
		SENSITIVE_PROPERTIES.add("CREDENTIAL");
		SENSITIVE_PROPERTIES.add("NEWCREDENTIAL");
		SENSITIVE_PROPERTIES.add("PASSCODE");
		SENSITIVE_PROPERTIES.add("NEWPASSCODE");
	}
	protected Object bean;
	public BeanMessageData() {
	}

	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#readData(java.nio.ByteBuffer)
	 */
	@Override
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		String classname = ProcessingUtils.readShortString(data, charset);
		if(StringUtils.isBlank(classname)) {
			bean = null;
			return;
		}
		try {
			Class<?> clazz = Class.forName(classname);
			Object bean = clazz.newInstance();
			for(BeanProperty bp : ReflectionUtils.getBeanProperties(clazz)) {
				if(bp.isWritable() && bp.isReadable()) {
					Object value;
					if(bp.getType().equals(byte.class))
						value = data.get();
					else if(bp.getType().equals(short.class))
						value = data.getShort();
					else if(bp.getType().equals(int.class))
						value = data.getInt();
					else if(bp.getType().equals(long.class))
						value = data.getLong();
					else if(bp.getType().equals(float.class))
						value = data.getFloat();
					else if(bp.getType().equals(double.class))
						value = data.getDouble();
					else if(bp.getType().equals(boolean.class))
						value = (data.get() != 0);
					else if(bp.getType().equals(char.class))
						value = data.getChar();
					else if(Date.class.isAssignableFrom(bp.getType())) {
						long l = data.getLong();
						if(l == Long.MAX_VALUE)
							value = null;
						else
							value = new Date(l);
					} else if(Number.class.isAssignableFrom(bp.getType()))
						value = ProcessingUtils.readShortString(data, charset);
					else if(bp.getType().equals(byte[].class))
						value = ProcessingUtils.readLongBytes(data);
					else if(bp.getType().equals(ByteBuffer.class))
						value = ByteBuffer.wrap(ProcessingUtils.readLongBytes(data));
					else if(DataHandler.class.isAssignableFrom(bp.getType())) {
						DataHandler dh = (DataHandler) bp.getValue(bean);
						if(dh == null) {
							dh = (DataHandler) bp.getType().newInstance();
							bp.setValue(bean, dh);
						}
						readStream(data, dh);
						continue;
					} else if(DataSource.class.isAssignableFrom(bp.getType())) {
						DataSource ds = (DataSource) bp.getValue(bean);
						if(ds == null) {
							ds = (DataSource) bp.getType().newInstance();
							bp.setValue(bean, ds);
						}
						readStream(data, ds);
						continue;
					} else
						value = ProcessingUtils.readLongString(data, charset);
					bp.setValue(bean, value);
				}
			}
			this.bean = bean;
		} catch(ClassNotFoundException e) {
			throw createParseException("Class " + classname + "' not found", data.position(), e);
		} catch(InstantiationException e) {
			throw createParseException("Could not instantiate " + classname + "'", data.position(), e);
		} catch(IllegalAccessException e) {
			throw createParseException("Could not set properties on " + classname + "'", data.position(), e);
		} catch(IllegalArgumentException e) {
			throw createParseException("Could not set properties on " + classname + "'", data.position(), e);
		} catch(IntrospectionException e) {
			throw createParseException("Could not set properties on " + classname + "'", data.position(), e);
		} catch(InvocationTargetException e) {
			throw createParseException("Could not set properties on " + classname + "'", data.position(), e);
		} catch(IOException e) {
			throw createParseException("Could not set properties on " + classname + "'", data.position(), e);
		}
	}
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	@Override
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		if(bean == null) {
			ProcessingUtils.writeShortString(reply, null, charset);
			return;
		}
		try {
			ProcessingUtils.writeShortString(reply, bean.getClass().getName(), charset);
			for(BeanProperty bp : ReflectionUtils.getBeanProperties(bean.getClass())) {
				if(bp.isWritable() && bp.isReadable()) {
					if(bp.getType().equals(byte.class))
						reply.put((Byte) bp.getValue(bean));
					else if(bp.getType().equals(short.class))
						reply.putShort((Short) bp.getValue(bean));
					else if(bp.getType().equals(int.class))
						reply.putInt((Integer) bp.getValue(bean));
					else if(bp.getType().equals(long.class))
						reply.putLong((Long) bp.getValue(bean));
					else if(bp.getType().equals(float.class))
						reply.putFloat((Float) bp.getValue(bean));
					else if(bp.getType().equals(double.class))
						reply.putDouble((Double) bp.getValue(bean));
					else if(bp.getType().equals(boolean.class))
						reply.put(((Boolean) bp.getValue(bean)) ? (byte) 1 : 0);
					else if(bp.getType().equals(char.class))
						reply.putChar((Character) bp.getValue(bean));
					else if(Date.class.isAssignableFrom(bp.getType())) {
						Date d = (Date) bp.getValue(bean);
						if(d == null)
							reply.putLong(Long.MAX_VALUE);
						else
							reply.putLong(d.getTime());
					} else if(Number.class.isAssignableFrom(bp.getType()))
						ProcessingUtils.writeShortString(reply, ConvertUtils.getString(bp.getValue(bean), false), charset);
					else if(bp.getType().equals(byte[].class)) {
						byte[] b = (byte[]) bp.getValue(bean);
						if(maskSensitiveData)
							b = MessageDataUtils.summarizeBytes(b);
						ProcessingUtils.writeLongBytes(reply, MessageDataUtils.summarizeBytes(b));
					} else if(bp.getType().equals(ByteBuffer.class)) {
						ByteBuffer bb = (ByteBuffer) bp.getValue(bean);
						if(maskSensitiveData)
							ProcessingUtils.writeLongBytes(reply, MessageDataUtils.summarizeBytes(bb));
						else
							ProcessingUtils.writeLongBytes(reply, bb);
					} else if(DataHandler.class.isAssignableFrom(bp.getType())) {
						if(maskSensitiveData)
							ProcessingUtils.writeLongBytes(reply, "<stream of bytes>".getBytes(charset));
						else
							writeStream(reply, (DataHandler) bp.getValue(bean));
					} else if(DataSource.class.isAssignableFrom(bp.getType())) {
						if(maskSensitiveData)
							ProcessingUtils.writeLongBytes(reply, "<stream of bytes>".getBytes(charset));
						else
							writeStream(reply, (DataSource) bp.getValue(bean));
					} else if(maskSensitiveData && isSensitive(bp.getName()))
						ProcessingUtils.writeLongString(reply, MessageDataUtils.maskString(ConvertUtils.getString(bp.getValue(bean), false)), charset);
					else
						ProcessingUtils.writeLongString(reply, ConvertUtils.getString(bp.getValue(bean), false), charset);
				}
			}
		} catch(ConvertException e) {
			throw new UndeclaredThrowableException(e, "Could not write properties from " + bean.getClass().getName() + "'");
		} catch(IllegalArgumentException e) {
			throw new UndeclaredThrowableException(e, "Could not write properties from " + bean.getClass().getName() + "'");
		} catch(IntrospectionException e) {
			throw new UndeclaredThrowableException(e, "Could not write properties from " + bean.getClass().getName() + "'");
		} catch(IllegalAccessException e) {
			throw new UndeclaredThrowableException(e, "Could not write properties from " + bean.getClass().getName() + "'");
		} catch(InvocationTargetException e) {
			throw new UndeclaredThrowableException(e, "Could not write properties from " + bean.getClass().getName() + "'");
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e, "Could not write properties from " + bean.getClass().getName() + "'");
		}
	}

	protected void writeStream(ByteBuffer reply, InputStream in) throws IOException {
		if(in == null) {
			reply.putInt(0);
			return;
		}
		int pos = reply.position();
		reply.putInt(0);
		long tot = IOUtils.readInto(in, reply, 1024);
		reply.putInt(pos, (int) tot);
	}

	protected void writeStream(ByteBuffer reply, DataSource in) throws IOException {
		writeStream(reply, in == null ? null : in.getInputStream());
	}

	protected void writeStream(ByteBuffer reply, DataHandler in) throws IOException {
		writeStream(reply, in == null ? null : in.getInputStream());
	}

	protected void readStream(ByteBuffer data, OutputStream out) throws IOException {
		long length = ProcessingUtils.readLongInt(data);
		if(length == 0)
			return;
		if(data.hasArray()) {
			out.write(data.array(), data.arrayOffset() + data.position(), (int) length);
		} else {
			byte[] bytes = new byte[1024];
			int target = data.position() + (int) length;
			for(int initial = data.position(); initial < target; initial = data.position()) {
				data.get(bytes);
				out.write(bytes, 0, data.position() - initial);
			}
		}
	}

	protected void readStream(ByteBuffer data, DataSource out) throws IOException {
		readStream(data, out.getOutputStream());
	}

	protected void readStream(ByteBuffer data, DataHandler out) throws IOException {
		readStream(data, out.getOutputStream());
	}

	protected boolean isSensitive(String name) {
		return SENSITIVE_PROPERTIES.contains(name);
	}

	public Object getBean() {
		return bean;
	}

	public void setBean(Object bean) {
		this.bean = bean;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType()).append(": [messageNumber=").append(getMessageNumber() & 0xFF).append("; bean=");
		if(bean != null) {
			sb.append(bean.getClass().getName()).append(": [");
			try {
				boolean first = true;
				for(BeanProperty bp : ReflectionUtils.getBeanProperties(bean.getClass())) {
					if(bp.isWritable() && bp.isReadable()) {
						if(first)
							first = false;
						else
							sb.append("; ");
						sb.append(bp.getName()).append('=');
						Object value = bp.getValue(bean);
						if(isSensitive(bp.getName()))
							sb.append(MessageDataUtils.maskString(ConvertUtils.getStringSafely(value, "")));
						else if(value instanceof byte[])
							sb.append('<').append(((byte[]) value).length).append(" bytes>");
						else if(value instanceof DataHandler || value instanceof DataSource)
							sb.append("<stream of bytes>");
						else
							sb.append(value);
					}
				}
				sb.append(']');
			} catch(IntrospectionException e) {
				sb.append("<ERROR>");
			} catch(IllegalArgumentException e) {
				sb.append("<ERROR>");
			} catch(IllegalAccessException e) {
				sb.append("<ERROR>");
			} catch(InvocationTargetException e) {
				sb.append("<ERROR>");
			}
		} else
			sb.append("null");
		sb.append("]");

		return sb.toString();
	}
}