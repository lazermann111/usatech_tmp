package com.usatech.layers.common.messagedata;

import java.util.List;
import java.util.TimeZone;

import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;


public abstract class WSChargeMessageData extends WSAuthorizeMessageData implements WSSale {
	protected final WSSaleDetail saleDetail = new WSSaleDetail();

	public WSChargeMessageData() {
		super();
		addValidators(WSRequestMessageData.INVALID_TRAN_ID, WSRequestMessageData.INVALID_AMOUNT);
		saleDetail.registerValidators(this);
	}
	public WSSaleDetail getSaleDetail() {
		return saleDetail;
	}

	public String getDeviceTranCd() {
		return String.valueOf(getTransactionId());
	}

	public long getBatchId() {
		return 0;
	}

	public SaleType getSaleType() {
		return SaleType.ACTUAL;
	}

	/**
	 * Returns the sale start time (UTC) or null to use the current time
	 * 
	 * @return
	 */
	public Long getSaleStartTime() {
		return null;
	}

	/**
	 * Returns the sale time zone or null to use the device's current timezone
	 * 
	 * @return
	 */
	public TimeZone getSaleTimeZone() {
		return null;
	}

	public SaleResult getSaleResult() {
		return getTransactionResult().getSaleResult();
	}

	public Number getSaleAmount() {
		return getAmount();
	}

	public Number getSaleTax() {
		return null;
	}

	public ReceiptResult getReceiptResult() {
		return ReceiptResult.UNAVAILABLE;
	}

	public TranDeviceResultType getTransactionResult() {
		return saleDetail.getTransactionResult();
	}

	public List<? extends LineItem> getLineItems() {
		return saleDetail.getLineItems();
	}

	public KioskLineItem addLineItem() {
		return saleDetail.addLineItem();
	}

	public String getTranDetails() {
		return saleDetail.getTranDetails();
	}

	public void setTranDetails(String tranDetails) {
		saleDetail.setTranDetails(tranDetails);
	}

	public String getTranResult() {
		return saleDetail.getTranResult();
	}

	public void setTranResult(String tranResult) {
		saleDetail.setTranResult(tranResult);
	}
}
