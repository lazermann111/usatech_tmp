package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for WS_CHARGE_V_3_1_REQUEST - "Web Service - Charge V 3 1 Request - 0203h"
 */
public class MessageData_0203 extends WSChargeMessageData {
	protected final WSEncryptingCardReader cardReader = new WSEncryptingCardReader();
	protected Long amount;
	protected String cardType;

	public MessageData_0203() {
		super();
		cardReader.registerValidators(this);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS_CHARGE_V_3_1_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getTranId());
		reply.putLong(getAmount());
		reply.putInt(getCardReaderTypeInt());
		reply.putInt(getDecryptedCardDataLen());
		ProcessingUtils.writeLongString(reply, getEncryptedCardDataHex(), charset);
		ProcessingUtils.writeLongString(reply, getKsnHex(), charset);
		ProcessingUtils.writeLongString(reply, getCardType(), charset);
		ProcessingUtils.writeLongString(reply, getTranResult(), charset);
		ProcessingUtils.writeLongString(reply, getTranDetails(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setTranId(data.getLong());
		setAmount(data.getLong());
		setCardReaderTypeInt(data.getInt());
		setDecryptedCardDataLen(data.getInt());
		setEncryptedCardDataHex(ProcessingUtils.readLongString(data, charset));
		setKsnHex(ProcessingUtils.readLongString(data, charset));
		setCardType(ProcessingUtils.readLongString(data, charset));
		setTranResult(ProcessingUtils.readLongString(data, charset));
		setTranDetails(ProcessingUtils.readLongString(data, charset));
	}

	public CardReaderType getCardReaderType() {
		return cardReader.getCardReaderType();
	}

	public CardReader getCardReader() {
		return cardReader;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public int getCardReaderTypeInt() {
		return cardReader.getCardReaderTypeInt();
	}

	public void setCardReaderTypeInt(int cardReaderTypeInt) {
		cardReader.setCardReaderTypeInt(cardReaderTypeInt);
	}

	public int getDecryptedCardDataLen() {
		return cardReader.getDecryptedCardDataLen();
	}

	public void setDecryptedCardDataLen(int decryptedCardDataLen) {
		cardReader.setDecryptedCardDataLen(decryptedCardDataLen);
	}

	public String getEncryptedCardDataHex() {
		return cardReader.getEncryptedCardDataHex();
	}

	public void setEncryptedCardDataHex(String encryptedCardDataHex) {
		cardReader.setEncryptedCardDataHex(encryptedCardDataHex);
	}

	public String getKsnHex() {
		return cardReader.getKsnHex();
	}

	public void setKsnHex(String ksnHex) {
		cardReader.setKsnHex(ksnHex);
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public MessageData_0301 createResponse() {
		MessageData_0301 response = new MessageData_0301();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", tranId=").append(getTranId())
			.append(", amount=").append(getAmount())
			.append(", cardReaderTypeInt=").append(getCardReaderTypeInt())
			.append(", decryptedCardDataLen=").append(getDecryptedCardDataLen())
			.append(", encryptedCardDataHex=<").append(getEncryptedCardDataHex() == null ? 0 : getEncryptedCardDataHex().length()).append(" bytes>")
			.append(", ksnHex=").append(getKsnHex())
			.append(", cardType=").append(getCardType())
			.append(", tranResult=").append(getTranResult())
			.append(", tranDetails=").append(getTranDetails());
		sb.append("]");

		return sb.toString();
	}
}
