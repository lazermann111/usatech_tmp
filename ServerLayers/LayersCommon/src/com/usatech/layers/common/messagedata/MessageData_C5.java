package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.SettlementFormat;
import com.usatech.layers.common.ProcessingUtils;
import java.math.BigDecimal;
import java.util.Calendar;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidIntValueException;

/**
 *  Message Data for SETTLEMENT_4_1 - "Settlement 4.1 - C5h"
 */
public class MessageData_C5 extends AbstractMessageData {
	public interface ContentFormatData extends Byteable {
	}
	protected long batchId;
	protected long newBatchId;
	protected long eventId;
	protected EventType eventType = EventType.BATCH;
	protected Calendar creationTime;
	public class Format0Content implements ContentFormatData {
	protected long cashItems;
	protected BigDecimal cashAmount;
	protected long cashlessItems;
	protected BigDecimal cashlessAmount;
		protected Format0Content() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setCashItems(ProcessingUtils.readLongInt(data));
		setCashAmount(ProcessingUtils.readStringAmountNull(data));
		setCashlessItems(ProcessingUtils.readLongInt(data));
		setCashlessAmount(ProcessingUtils.readStringAmountNull(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getCashItems());
		ProcessingUtils.writeStringAmount(reply, getCashAmount());
		ProcessingUtils.writeLongInt(reply, getCashlessItems());
		ProcessingUtils.writeStringAmount(reply, getCashlessAmount());
		}

	public long getCashItems() {
		return cashItems;
	}

	public void setCashItems(long cashItems) {
		this.cashItems = cashItems;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public long getCashlessItems() {
		return cashlessItems;
	}

	public void setCashlessItems(long cashlessItems) {
		this.cashlessItems = cashlessItems;
	}

	public BigDecimal getCashlessAmount() {
		return cashlessAmount;
	}

	public void setCashlessAmount(BigDecimal cashlessAmount) {
		this.cashlessAmount = cashlessAmount;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("cashItems=").append(getCashItems())
			.append("; cashAmount=").append(getCashAmount())
			.append("; cashlessItems=").append(getCashlessItems())
			.append("; cashlessAmount=").append(getCashlessAmount());
			sb.append(']');
			return sb.toString();
		}
	}
	public class Format1Content implements ContentFormatData {
	protected long cashItems;
	protected BigDecimal cashAmount;
	protected long cashlessItems;
	protected BigDecimal cashlessAmount;
	protected long appCashItems;
	protected BigDecimal appCashAmount;
	protected long appCashlessItems;
	protected BigDecimal appCashlessAmount;
		protected Format1Content() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setCashItems(ProcessingUtils.readLongInt(data));
		setCashAmount(ProcessingUtils.readStringAmountNull(data));
		setCashlessItems(ProcessingUtils.readLongInt(data));
		setCashlessAmount(ProcessingUtils.readStringAmountNull(data));
		setAppCashItems(ProcessingUtils.readLongInt(data));
		setAppCashAmount(ProcessingUtils.readStringAmountNull(data));
		setAppCashlessItems(ProcessingUtils.readLongInt(data));
		setAppCashlessAmount(ProcessingUtils.readStringAmountNull(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getCashItems());
		ProcessingUtils.writeStringAmount(reply, getCashAmount());
		ProcessingUtils.writeLongInt(reply, getCashlessItems());
		ProcessingUtils.writeStringAmount(reply, getCashlessAmount());
		ProcessingUtils.writeLongInt(reply, getAppCashItems());
		ProcessingUtils.writeStringAmount(reply, getAppCashAmount());
		ProcessingUtils.writeLongInt(reply, getAppCashlessItems());
		ProcessingUtils.writeStringAmount(reply, getAppCashlessAmount());
		}

	public long getCashItems() {
		return cashItems;
	}

	public void setCashItems(long cashItems) {
		this.cashItems = cashItems;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public long getCashlessItems() {
		return cashlessItems;
	}

	public void setCashlessItems(long cashlessItems) {
		this.cashlessItems = cashlessItems;
	}

	public BigDecimal getCashlessAmount() {
		return cashlessAmount;
	}

	public void setCashlessAmount(BigDecimal cashlessAmount) {
		this.cashlessAmount = cashlessAmount;
	}

	public long getAppCashItems() {
		return appCashItems;
	}

	public void setAppCashItems(long appCashItems) {
		this.appCashItems = appCashItems;
	}

	public BigDecimal getAppCashAmount() {
		return appCashAmount;
	}

	public void setAppCashAmount(BigDecimal appCashAmount) {
		this.appCashAmount = appCashAmount;
	}

	public long getAppCashlessItems() {
		return appCashlessItems;
	}

	public void setAppCashlessItems(long appCashlessItems) {
		this.appCashlessItems = appCashlessItems;
	}

	public BigDecimal getAppCashlessAmount() {
		return appCashlessAmount;
	}

	public void setAppCashlessAmount(BigDecimal appCashlessAmount) {
		this.appCashlessAmount = appCashlessAmount;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("cashItems=").append(getCashItems())
			.append("; cashAmount=").append(getCashAmount())
			.append("; cashlessItems=").append(getCashlessItems())
			.append("; cashlessAmount=").append(getCashlessAmount())
			.append("; appCashItems=").append(getAppCashItems())
			.append("; appCashAmount=").append(getAppCashAmount())
			.append("; appCashlessItems=").append(getAppCashlessItems())
			.append("; appCashlessAmount=").append(getAppCashlessAmount());
			sb.append(']');
			return sb.toString();
		}
	}
	protected SettlementFormat contentFormat = SettlementFormat.FORMAT_0;
	protected ContentFormatData contentFormatData;

	public MessageData_C5() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.SETTLEMENT_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getBatchId());
		ProcessingUtils.writeLongInt(reply, getNewBatchId());
		ProcessingUtils.writeLongInt(reply, getEventId());
		ProcessingUtils.writeByteInt(reply, getEventType().getValue());
		ProcessingUtils.writeTimestamp(reply, getCreationTime());
		ProcessingUtils.writeByte(reply, getContentFormat().getValue());
		if(getContentFormatData() != null)
			getContentFormatData().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setBatchId(ProcessingUtils.readLongInt(data));
		setNewBatchId(ProcessingUtils.readLongInt(data));
		setEventId(ProcessingUtils.readLongInt(data));
		try {
			setEventType(EventType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setCreationTime(ProcessingUtils.readTimestamp(data));
		try {
			setContentFormat(SettlementFormat.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getContentFormatData() != null)
			getContentFormatData().readData(data);
	}

	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public long getNewBatchId() {
		return newBatchId;
	}

	public void setNewBatchId(long newBatchId) {
		this.newBatchId = newBatchId;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public Calendar getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Calendar creationTime) {
		this.creationTime = creationTime;
	}

	public SettlementFormat getContentFormat() {
		return contentFormat;
	}

	public ContentFormatData getContentFormatData() {
		return contentFormatData;
	}

	public void setContentFormat(SettlementFormat contentFormat) {
		this.contentFormat = contentFormat;
		switch(contentFormat) {
			case FORMAT_0: this.contentFormatData = new Format0Content(); break;
			case FORMAT_1: this.contentFormatData = new Format1Content(); break;
			case FORMAT_2: this.contentFormatData = null; break;
			default: throw new IllegalArgumentException("The ContentFormat '" + contentFormat + "' is not supported");
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; batchId=").append(getBatchId())
			.append("; newBatchId=").append(getNewBatchId())
			.append("; eventId=").append(getEventId())
			.append("; eventType=").append(getEventType())
			.append("; creationTime=").append(MessageDataUtils.toString(getCreationTime()))
			.append("; contentFormat=").append(getContentFormat())
			.append("; contentFormatData=").append(getContentFormatData());
		sb.append("]");

		return sb.toString();
	}
}
