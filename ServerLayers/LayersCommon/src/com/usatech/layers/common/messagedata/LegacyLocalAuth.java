package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.CardType;

public interface LegacyLocalAuth extends Sale {
	public CardType getCardType();
	public String getCreditCardMagstripe();
}
