package com.usatech.layers.common.messagedata;

import java.util.List;
import java.util.TimeZone;

import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;


public abstract class WS2SaleMessageData extends WS2RequestMessageData implements WSSale, WSTran {
	protected final WSSaleDetail saleDetail = new WSSaleDetail();
	protected long tranId;
	protected Long amount;

	public WS2SaleMessageData() {
		addValidators(WSRequestMessageData.INVALID_TRAN_ID, WSRequestMessageData.INVALID_AMOUNT);
		saleDetail.registerValidators(this);
	}

	public long getTransactionId() {
		return getTranId();
	}

	public void setTransactionId(long transactionId) {
		setTranId(transactionId);
	}

	public String getDeviceTranCd() {
		return String.valueOf(getTransactionId());
	}

	public long getBatchId() {
		return 0;
	}

	public SaleType getSaleType() {
		return SaleType.ACTUAL;
	}

	/**
	 * Returns the sale start time (UTC) or null to use the current time
	 * 
	 * @return
	 */
	public Long getSaleStartTime() {
		return null;
	}

	/**
	 * Returns the sale time zone or null to use the device's current timezone
	 * 
	 * @return
	 */
	public TimeZone getSaleTimeZone() {
		return null;
	}

	public SaleResult getSaleResult() {
		return getTransactionResult().getSaleResult();
	}

	public Number getSaleAmount() {
		return getAmount();
	}

	public Number getSaleTax() {
		return null;
	}

	public ReceiptResult getReceiptResult() {
		return ReceiptResult.UNAVAILABLE;
	}

	public TranDeviceResultType getTransactionResult() {
		return saleDetail.getTransactionResult();
	}

	public List<? extends LineItem> getLineItems() {
		return saleDetail.getLineItems();
	}

	public KioskLineItem addLineItem() {
		return saleDetail.addLineItem();
	}

	public String getTranDetails() {
		return saleDetail.getTranDetails();
	}

	public void setTranDetails(String tranDetails) {
		saleDetail.setTranDetails(tranDetails);
	}

	public String getTranResult() {
		return saleDetail.getTranResult();
	}

	public void setTranResult(String tranResult) {
		saleDetail.setTranResult(tranResult);
	}

	public long getTranId() {
		return tranId;
	}

	public void setTranId(long tranId) {
		this.tranId = tranId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public WSSaleDetail getSaleDetail() {
		return saleDetail;
	}
}
