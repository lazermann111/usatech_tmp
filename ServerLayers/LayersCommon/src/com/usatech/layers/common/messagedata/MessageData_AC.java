package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;

/**
 *  Message Data for AUTH_REQUEST_3_1 - "Authorization Request 3.1 - ACh"
 */
public class MessageData_AC extends AbstractMessageData implements AuthorizeMessageData {
	public interface CardReaderTypeData extends Byteable, CardReader {
	}
	protected long transactionId;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected Long amount;
	public class EncryptingCardReaderImpl implements EncryptingCardReader, CardReaderTypeData {
	protected int decryptedLength;
	protected byte[] keySerialNum;
	protected byte[] encryptedData;
		protected EncryptingCardReaderImpl() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setDecryptedLength(ProcessingUtils.readByteInt(data));
		setKeySerialNum(ProcessingUtils.readShortBytes(data));
		setEncryptedData(ProcessingUtils.readShortBytes(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getDecryptedLength());
		ProcessingUtils.writeShortBytes(reply, getKeySerialNum());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData()) : getEncryptedData());
		}

	public int getDecryptedLength() {
		return decryptedLength;
	}

	public void setDecryptedLength(int decryptedLength) {
		this.decryptedLength = decryptedLength;
	}

	public byte[] getKeySerialNum() {
		return keySerialNum;
	}

	public void setKeySerialNum(byte[] keySerialNum) {
		this.keySerialNum = keySerialNum;
	}

	public byte[] getEncryptedData() {
		return encryptedData;
	}

	public void setEncryptedData(byte[] encryptedData) {
		this.encryptedData = encryptedData;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("decryptedLength=").append(getDecryptedLength())
			.append("; keySerialNum=").append(StringUtils.toHex(getKeySerialNum()))
			.append("; encryptedData=").append(new String(MessageResponseUtils.maskFully(getEncryptedData()), ProcessingConstants.US_ASCII_CHARSET));
			sb.append(']');
			return sb.toString();
		}
	}
	protected CardReaderType cardReaderType = CardReaderType.GENERIC;
	protected CardReaderTypeData cardReader;

	public MessageData_AC() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_REQUEST_3_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.writeLongInt(reply, getAmount());
		ProcessingUtils.writeByte(reply, getCardReaderType().getValue());
		getCardReader().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAmount(ProcessingUtils.readLongInt(data));
		try {
			setCardReaderType(CardReaderType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		getCardReader().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public CardReaderType getCardReaderType() {
		return cardReaderType;
	}

	public CardReaderTypeData getCardReader() {
		return cardReader;
	}

	public void setCardReaderType(CardReaderType cardReaderType) {
		this.cardReaderType = cardReaderType;
		switch(cardReaderType) {
			case MAGTEK_MAGNESAFE: case IDTECH_SECUREMAG: this.cardReader = new EncryptingCardReaderImpl(); break;
			default: throw new IllegalArgumentException("The CardReaderType '" + cardReaderType + "' is not supported");
		}
	}


	public EntryType getEntryType() {
		return getCardType().getEntryType();
	}
	public String getValidationData() {
		return null;
	}
	public String getPin() {
		return null;
	}
	public PaymentActionType getPaymentActionType() {
       return PaymentActionType.PURCHASE;
    }
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; cardType=").append(getCardType())
			.append("; amount=").append(getAmount())
			.append("; cardReaderType=").append(getCardReaderType())
			.append("; cardReader=").append(getCardReader());
		sb.append("]");

		return sb.toString();
	}
}
