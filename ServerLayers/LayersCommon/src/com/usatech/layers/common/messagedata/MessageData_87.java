package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for GX_PEEK - "Gx Peek - 87h"
 */
public class MessageData_87 extends AbstractMessageData {
	protected byte memoryCode;
	protected long startingAddress;
	protected long numberOfBytesToRead;

	public MessageData_87() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GX_PEEK;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getMemoryCode());
		ProcessingUtils.writeLongInt(reply, getStartingAddress());
		ProcessingUtils.writeLongInt(reply, getNumberOfBytesToRead());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setMemoryCode(ProcessingUtils.readByte(data));
		setStartingAddress(ProcessingUtils.readLongInt(data));
		setNumberOfBytesToRead(ProcessingUtils.readLongInt(data));
	}

	public byte getMemoryCode() {
		return memoryCode;
	}

	public void setMemoryCode(byte memoryCode) {
		this.memoryCode = memoryCode;
	}

	public long getStartingAddress() {
		return startingAddress;
	}

	public void setStartingAddress(long startingAddress) {
		this.startingAddress = startingAddress;
	}

	public long getNumberOfBytesToRead() {
		return numberOfBytesToRead;
	}

	public void setNumberOfBytesToRead(long numberOfBytesToRead) {
		this.numberOfBytesToRead = numberOfBytesToRead;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; memoryCode=").append(getMemoryCode() & 0xFF)
			.append("; startingAddress=").append(getStartingAddress())
			.append("; numberOfBytesToRead=").append(getNumberOfBytesToRead());
		sb.append("]");

		return sb.toString();
	}
}
