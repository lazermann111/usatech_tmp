package com.usatech.layers.common.messagedata;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.GenericResponseClientActionCode;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for GENERIC_RESPONSE_4_1 - "Generic Response 4.1 - CBh"
 */
public class MessageData_CB extends AbstractMessageData {
	public interface ServerActionCodeData extends Byteable {
	}
	protected GenericResponseResultCode resultCode = GenericResponseResultCode.OKAY;
	protected String responseMessage;
	public class ReconnectAction implements ServerActionCodeData {
	protected int reconnectTime;
		protected ReconnectAction() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setReconnectTime(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getReconnectTime());
		}

	public int getReconnectTime() {
		return reconnectTime;
	}

	public void setReconnectTime(int reconnectTime) {
		this.reconnectTime = reconnectTime;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("reconnectTime=").append(getReconnectTime());
			sb.append(']');
			return sb.toString();
		}
	}
	public class PropertyIndexListAction implements ServerActionCodeData {
	protected Set<Integer> propertyIndexes;
		protected PropertyIndexListAction() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPropertyIndexes(ProcessingUtils.readPropertyIndexList(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writePropertyIndexList(reply, getPropertyIndexes(), getCharset());
		}

	public Set<Integer> getPropertyIndexes() {
		return propertyIndexes;
	}

	public void setPropertyIndexes(Set<Integer> propertyIndexes) {
		this.propertyIndexes = propertyIndexes;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("propertyIndexes=").append(getPropertyIndexes());
			sb.append(']');
			return sb.toString();
		}
	}
	public class PropertyValueListAction implements ServerActionCodeData {
	protected Map<Integer,String> propertyValues;
		protected PropertyValueListAction() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPropertyValues(ProcessingUtils.readPropertyValueList(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
			ProcessingUtils.writePropertyValueList(reply, getPropertyValues(), getCharset(), maskSensitiveData);
		}

	public Map<Integer,String> getPropertyValues() {
			if(propertyValues == null)
				propertyValues = new LinkedHashMap<Integer, String>();
		return propertyValues;
	}

	public void setPropertyValues(Map<Integer,String> propertyValues) {
		this.propertyValues = propertyValues;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[').append("propertyValues=");
			try {
				MessageDataUtils.appendEfficientPropertyValueList(sb, getPropertyValues(), true, "; ");
			} catch(IOException e) {
				sb.append(getPropertyValues());
			}
			sb.append(']');
			return sb.toString();
		}
	}
	public class TraceEventsAction implements ServerActionCodeData {
	protected long maxBytes;
	protected long maxSeconds;
	protected int optionsBitmap;
		protected TraceEventsAction() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setMaxBytes(ProcessingUtils.readLongInt(data));
		setMaxSeconds(ProcessingUtils.readLongInt(data));
		setOptionsBitmap(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getMaxBytes());
		ProcessingUtils.writeLongInt(reply, getMaxSeconds());
		ProcessingUtils.writeShortInt(reply, getOptionsBitmap());
		}

	public long getMaxBytes() {
		return maxBytes;
	}

	public void setMaxBytes(long maxBytes) {
		this.maxBytes = maxBytes;
	}

	public long getMaxSeconds() {
		return maxSeconds;
	}

	public void setMaxSeconds(long maxSeconds) {
		this.maxSeconds = maxSeconds;
	}

	public int getOptionsBitmap() {
		return optionsBitmap;
	}

	public void setOptionsBitmap(int optionsBitmap) {
		this.optionsBitmap = optionsBitmap;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("maxBytes=").append(getMaxBytes())
			.append("; maxSeconds=").append(getMaxSeconds())
			.append("; optionsBitmap=").append(getOptionsBitmap());
			sb.append(']');
			return sb.toString();
		}
	}
	protected GenericResponseServerActionCode serverActionCode;
	protected ServerActionCodeData serverActionCodeData;
	protected GenericResponseClientActionCode clientActionCode = GenericResponseClientActionCode.NO_ACTION;

	public MessageData_CB() {
		super();
		setServerActionCode(GenericResponseServerActionCode.NO_ACTION);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GENERIC_RESPONSE_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getResultCode().getValue());
		ProcessingUtils.writeShortString(reply, getResponseMessage(), getCharset());
		switch(getDirection()) {
			case SERVER_TO_CLIENT: 
		ProcessingUtils.writeByte(reply, getServerActionCode().getValue());
		if(getServerActionCodeData() != null)
			getServerActionCodeData().writeData(reply, maskSensitiveData);

				break;
			case CLIENT_TO_SERVER: 
		ProcessingUtils.writeByte(reply, getClientActionCode().getValue());

				break;
			default: throw new IllegalArgumentException("The direction '" + getDirection() + "' is not supported");
		}
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setResultCode(GenericResponseResultCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setResponseMessage(ProcessingUtils.readShortString(data, getCharset()));
		switch(getDirection()) {
			case SERVER_TO_CLIENT: 
		try {
			setServerActionCode(GenericResponseServerActionCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getServerActionCodeData() != null)
			getServerActionCodeData().readData(data);

				break;
			case CLIENT_TO_SERVER: 
		try {
			setClientActionCode(GenericResponseClientActionCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}

				break;
			default: throw new IllegalArgumentException("The direction '" + getDirection() + "' is not supported");
		}
	}

	public GenericResponseResultCode getResultCode() {
		return resultCode;
	}

	public void setResultCode(GenericResponseResultCode resultCode) {
		this.resultCode = resultCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public GenericResponseServerActionCode getServerActionCode() {
		return serverActionCode;
	}

	public ServerActionCodeData getServerActionCodeData() {
		return serverActionCodeData;
	}

	public GenericResponseClientActionCode getClientActionCode() {
		return clientActionCode;
	}

	public void setServerActionCode(GenericResponseServerActionCode serverActionCode) {
		this.serverActionCode = serverActionCode;
		switch(serverActionCode) {
			case NO_ACTION: case CAPTURE_MDB_DUMP_INIT_SESSION: case CAPTURE_MDB_DUMP_VEND_SESSION: case REINITIALIZE: case PERFORM_SETTLEMENT: case STOP_FILE_TRANSFER: case  SEND_UPDATE_STATUS_REQUEST_IMMEDIATE: case SEND_UPDATE_STATUS_REQUEST: case SEND_COMP_IDENTITY: this.serverActionCodeData = null; break;
			case REBOOT: case DISCONNECT: this.serverActionCodeData = new ReconnectAction(); break;
			case UPLOAD_PROPERTY_VALUE_LIST: this.serverActionCodeData = new PropertyIndexListAction(); break;
			case PROCESS_PROP_LIST: this.serverActionCodeData = new PropertyValueListAction(); break;
			case TRACE_EVENTS: this.serverActionCodeData = new TraceEventsAction(); break;
			default: throw new IllegalArgumentException("The ServerActionCode '" + serverActionCode + "' is not supported");
		}
		setDirection(MessageDirection.SERVER_TO_CLIENT);
	}

	public void setClientActionCode(GenericResponseClientActionCode clientActionCode) {
		this.clientActionCode = clientActionCode;
		setDirection(MessageDirection.CLIENT_TO_SERVER);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; resultCode=").append(getResultCode())
			.append("; responseMessage=").append(getResponseMessage());
		switch(getDirection()) {
			case SERVER_TO_CLIENT: 
		sb
			.append("; serverActionCode=").append(getServerActionCode())
			.append("; serverActionCodeData=").append(getServerActionCodeData());
				break;
			case CLIENT_TO_SERVER: 
		sb
			.append("; clientActionCode=").append(getClientActionCode());
				break;
			default: throw new IllegalArgumentException("The direction '" + getDirection() + "' is not supported");
		}
		sb.append("");
		sb.append("]");

		return sb.toString();
	}
}
