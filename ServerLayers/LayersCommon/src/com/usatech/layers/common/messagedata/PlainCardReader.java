package com.usatech.layers.common.messagedata;

public interface PlainCardReader extends CardReader {

	public String getAccountData();

}