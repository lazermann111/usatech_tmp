package com.usatech.layers.common.messagedata;

public interface CardIdCardReader extends CardReader {
	public long getCardId();

	public String getSecurityCode();

	public String getBillingPostalCode();

	public String getBillingAddress();

	public String getCardHolder();

}
