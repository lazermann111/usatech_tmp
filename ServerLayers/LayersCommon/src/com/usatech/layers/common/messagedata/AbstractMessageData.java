package com.usatech.layers.common.messagedata;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.ParseException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
/**
 * Base MessageData.
 * @author yhe, bkrug
 *
 */
public abstract class AbstractMessageData implements MessageData {
	protected byte messageNumber;
	protected long messageId;
	protected long messageStartTime = System.currentTimeMillis();
	protected long messageEndTime = 0;
	protected Charset charset = ProcessingConstants.ISO8859_1_CHARSET;
	protected MessageDirection direction = MessageDirection.CLIENT_TO_SERVER;

	public AbstractMessageData() {
	}

	public byte getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(byte messageNumber) {
		this.messageNumber = messageNumber;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getMessageStartTime() {
		return messageStartTime;
	}
	public void setMessageStartTime(long messageStartTime) {
		this.messageStartTime = messageStartTime;
	}
	public long getMessageEndTime() {
		return messageEndTime;
	}
	public void setMessageEndTime(long messageEndTime) {
		this.messageEndTime = messageEndTime;
	}
	public static ParseException createParseException(String message, int errorOffset, Throwable cause) {
		ParseException pe = new ParseException(message, errorOffset);
		pe.initCause(cause);
		return pe;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#readData(java.nio.ByteBuffer)
	 */
	@Override
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		if(getMessageType().isMessageIdExpected())
			setMessageId(ProcessingUtils.readLongInt(data));
	}
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	@Override
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		reply.put(getMessageNumber());
		reply.put(getMessageType().getValue());
		if(getMessageType().isMessageIdExpected())
			ProcessingUtils.writeLongInt(reply, getMessageId());
	}



	public MessageDirection getDirection() {
		return direction;
	}

	public void setDirection(MessageDirection direction) {
		this.direction = direction;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}
}