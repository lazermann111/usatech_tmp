package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.ProcessingUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.TimeZone;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;

/**
 *  Message Data for SALE_4_1 - "Sale 4.1 - C4h"
 */
public class MessageData_C4 extends AbstractMessageData implements Sale {
	protected long transactionId;
	protected long batchId;
	protected SaleType saleType = SaleType.ACTUAL;
	protected Calendar saleSessionStart;
	protected SaleResult saleResult = SaleResult.SUCCESS;
	protected BigDecimal saleAmount;
	protected ReceiptResult receiptResult = ReceiptResult.PRINTED;
	public class Format0LineItem extends BaseLineItem implements LineItem {
	protected int componentNumber;
	protected int item;
	protected SaleResult saleResult = SaleResult.SUCCESS;
	protected int quantity;
	protected BigDecimal price;
	protected String description;
	protected int duration;
		protected final int index;
		protected Format0LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setComponentNumber(ProcessingUtils.readByteInt(data));
		setItem(ProcessingUtils.readShortInt(data));
		try {
			setSaleResult(SaleResult.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setQuantity(ProcessingUtils.readShortInt(data));
		setPrice(ProcessingUtils.readStringAmountNull(data));
		setDescription(ProcessingUtils.readShortString(data, getCharset()));
		setDuration(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.writeShortInt(reply, getItem());
		ProcessingUtils.writeByte(reply, getSaleResult().getValue());
		ProcessingUtils.writeShortInt(reply, getQuantity());
		ProcessingUtils.writeStringAmount(reply, getPrice());
		ProcessingUtils.writeShortString(reply, getDescription(), getCharset());
		ProcessingUtils.writeShortInt(reply, getDuration());
		}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public SaleResult getSaleResult() {
		return saleResult;
	}

	public void setSaleResult(SaleResult saleResult) {
		this.saleResult = saleResult;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("componentNumber=").append(getComponentNumber())
			.append("; item=").append(getItem())
			.append("; saleResult=").append(getSaleResult())
			.append("; quantity=").append(getQuantity())
			.append("; price=").append(getPrice())
			.append("; description=").append(getDescription())
			.append("; duration=").append(getDuration());
			sb.append(']');
			return sb.toString();
		}

		/**
		 * @see com.usatech.layers.common.messagedata.BaseLineItem#getPosition()
		 */
		@Override
		public String getPosition() {
            return getItem() == 200 && getDescription() != null && POSITION_PATTERN.matcher(getDescription()).matches() ? getDescription() : null;
		}
		
	}
	protected byte lineItemFormat;
	protected final List<LineItem> lineItems = new ArrayList<LineItem>();
	protected final List<LineItem> lineItemsUnmod = Collections.unmodifiableList(lineItems);

	public MessageData_C4() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.SALE_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeLongInt(reply, getBatchId());
		ProcessingUtils.writeByteInt(reply, getSaleType().getValue());
		ProcessingUtils.writeTimestamp(reply, getSaleSessionStart());
		ProcessingUtils.writeByte(reply, getSaleResult().getValue());
		ProcessingUtils.writeStringAmount(reply, getSaleAmount());
		ProcessingUtils.writeByteInt(reply, getReceiptResult().getValue());
		ProcessingUtils.writeByte(reply, getLineItemFormat());
		ProcessingUtils.writeByteInt(reply, (byte) getLineItems().size());
		for(LineItem lineItem : getLineItems())
			lineItem.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		setBatchId(ProcessingUtils.readLongInt(data));
		try {
			setSaleType(SaleType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setSaleSessionStart(ProcessingUtils.readTimestamp(data));
		try {
			setSaleResult(SaleResult.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setSaleAmount(ProcessingUtils.readStringAmountNull(data));
		try {
			setReceiptResult(ReceiptResult.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setLineItemFormat(ProcessingUtils.readByte(data));
		lineItems.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addLineItem().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public SaleType getSaleType() {
		return saleType;
	}

	public void setSaleType(SaleType saleType) {
		this.saleType = saleType;
	}

	public Calendar getSaleSessionStart() {
		return saleSessionStart;
	}

	public void setSaleSessionStart(Calendar saleSessionStart) {
		this.saleSessionStart = saleSessionStart;
	}

	public SaleResult getSaleResult() {
		return saleResult;
	}

	public void setSaleResult(SaleResult saleResult) {
		this.saleResult = saleResult;
	}

	public BigDecimal getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	public ReceiptResult getReceiptResult() {
		return receiptResult;
	}

	public void setReceiptResult(ReceiptResult receiptResult) {
		this.receiptResult = receiptResult;
	}

	public byte getLineItemFormat() {
		return lineItemFormat;
	}

	public List<LineItem> getLineItems() {
		return lineItemsUnmod;
	}

	public LineItem addLineItem() {
		LineItem lineItem;
		switch(getLineItemFormat()) {
			case 0: lineItem = new Format0LineItem(lineItems.size()); break;
			default: throw new IllegalArgumentException("The LineItemFormat '" + getLineItemFormat() + "' is not supported");
		}
		lineItems.add(lineItem);
		return lineItem;
	}

	public void setLineItemFormat(byte lineItemFormat) {
		this.lineItemFormat = lineItemFormat;
	}


	    protected static final Pattern POSITION_PATTERN = Pattern.compile("^[0-9A-Fa-f]+$");
	    
        public String getDeviceTranCd() {
            return String.valueOf(getTransactionId());
        }
        public Long getSaleStartTime() {
			return getSaleSessionStart().getTimeInMillis();
		}
		public TimeZone getSaleTimeZone() {
			return getSaleSessionStart().getTimeZone();
		}
		public TranDeviceResultType getTransactionResult() {
			return getSaleResult().getTransactionResult();
		}
		public BigDecimal getSaleTax() {
			return BigDecimal.ZERO;
		}	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; transactionId=").append(getTransactionId())
			.append("; batchId=").append(getBatchId())
			.append("; saleType=").append(getSaleType())
			.append("; saleSessionStart=").append(MessageDataUtils.toString(getSaleSessionStart()))
			.append("; saleResult=").append(getSaleResult())
			.append("; saleAmount=").append(getSaleAmount())
			.append("; receiptResult=").append(getReceiptResult())
			.append("; lineItemFormat=").append(getLineItemFormat() & 0xFF)
			.append("; lineItems=").append(getLineItems());
		sb.append("]");

		return sb.toString();
	}
}
