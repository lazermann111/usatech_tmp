package com.usatech.layers.common.messagedata;

import simple.text.StringUtils;

import com.usatech.layers.common.constants.AuthResultCode;

public abstract class WS2TokenizeResponseMessageData extends WS2ResponseMessageData {
	protected String tokenHex = "";

	public void setTokenBytes(byte[] token) {
		setTokenHex(StringUtils.toHex(token));
	}

	public void setAuthResultCd(AuthResultCode authResultCd) {
		setReturnCode(authResultCd.getAuthResponseCodeEC2());
		if(StringUtils.isBlank(getReturnMessage()))
			switch(authResultCd) {
				case APPROVED:
					setReturnMessage("Approved");
					break;
				case PARTIAL:
					setReturnMessage("Partially Approved");
					break;
				case DECLINED:
					setReturnMessage("Declined");
					break;
				case DECLINED_PERMANENT:
					setReturnMessage("Declined");
					break;
				case DECLINED_PAYMENT_METHOD:
					setReturnMessage("Invalid Card");
					break;
				case DECLINED_DEBIT:
					setReturnMessage("Debit Not Accepted");
					break;
				case FAILED:
					setReturnMessage("Failed");
					break;
				case AVS_MISMATCH:
					setReturnMessage("Address Does Not Match");
					break;
			}
		switch(authResultCd) {
			case APPROVED:
			case PARTIAL:
			case AVS_MISMATCH:
				break;
			default:
				setTokenHex(null);
		}
	}

	public void setOverrideResponseMessage(String overrideResponseMessage) {
		if(overrideResponseMessage != null)
			switch(getReturnCode()) {
				case 2:
				case 4:
					setReturnMessage(overrideResponseMessage);
			}
	}

	public String getTokenHex() {
		return tokenHex;
	}

	public void setTokenHex(String tokenHex) {
		this.tokenHex = tokenHex == null ? "" : tokenHex;
	}

}
