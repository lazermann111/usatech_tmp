package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ABPermissionActionCode;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PermissionResponseCodeAB;

/**
 *  Message Data for PERMISSION_RESPONSE_1_0 - "Permission Response 1.0 - ABh"
 */
public class MessageData_AB extends AbstractMessageData {
	public interface ActionCodeData extends Byteable {
	}
	protected long transactionId;
	protected PermissionResponseCodeAB responseCode = PermissionResponseCodeAB.SUCCESS;
	public class ConfigurableAction implements ActionCodeData {
	protected int actionBitmap;
		protected ConfigurableAction() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setActionBitmap(ProcessingUtils.readByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getActionBitmap());
		}

	public int getActionBitmap() {
		return actionBitmap;
	}

	public void setActionBitmap(int actionBitmap) {
		this.actionBitmap = actionBitmap;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("actionBitmap=").append(getActionBitmap());
			sb.append(']');
			return sb.toString();
		}
	}
	protected ABPermissionActionCode actionCode = ABPermissionActionCode.SHOW_ALL_SERVICE_FIELDS_NOT_CLEAR_COUNTERS;
	protected ActionCodeData actionCodeData;

	public MessageData_AB() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.PERMISSION_RESPONSE_1_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
		ProcessingUtils.writeByte(reply, getActionCode().getValue());
		if(getActionCodeData() != null)
			getActionCodeData().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setResponseCode(PermissionResponseCodeAB.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setActionCode(ABPermissionActionCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getActionCodeData() != null)
			getActionCodeData().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public PermissionResponseCodeAB getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(PermissionResponseCodeAB responseCode) {
		this.responseCode = responseCode;
	}

	public ABPermissionActionCode getActionCode() {
		return actionCode;
	}

	public ActionCodeData getActionCodeData() {
		return actionCodeData;
	}

	public void setActionCode(ABPermissionActionCode actionCode) {
		if(actionCode == null)
			actionCode = ABPermissionActionCode.SHOW_ALL_SERVICE_FIELDS_NOT_CLEAR_COUNTERS;
		this.actionCode = actionCode;
		switch(actionCode) {
			case SHOW_ALL_SERVICE_FIELDS_NOT_CLEAR_COUNTERS:
				this.actionCodeData = null;
				break;
			case SHOW_ALL_SERVICE_FIELDS_CLEAR_COUNTERS:
				this.actionCodeData = null;
				break;
			case SHOW_COUNTERS_ONLY_NOT_CLEAR_COUNTERS:
				this.actionCodeData = null;
				break;
			case SHOW_COUNTERS_ONLY_CLEAR_COUNTERS:
				this.actionCodeData = null;
				break;
			case SHOW_CASHLESS_COUNTERS_NOT_CLEAR_COUNTERS:
				this.actionCodeData = null;
				break;
			case SHOW_CASHLESS_COUNTERS_REDISPLAY:
				this.actionCodeData = new ConfigurableAction();
				break;
			default:
				throw new IllegalArgumentException("The ActionCode '" + actionCode + "' is not supported");
		}
	}


	public void setAuthResultCd(AuthResultCode authResultCd) {
		setResponseCode(authResultCd.getPermissionResponseCodeAB());
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; responseCode=").append(getResponseCode())
			.append("; actionCode=").append(getActionCode())
			.append("; actionCodeData=").append(getActionCodeData());
		sb.append("]");

		return sb.toString();
	}
}
