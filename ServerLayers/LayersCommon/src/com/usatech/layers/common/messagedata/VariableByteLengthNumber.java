package com.usatech.layers.common.messagedata;

public class VariableByteLengthNumber extends Number {
	private static final long serialVersionUID = -7282975390736768290L;
	protected final long value;
	protected final int byteLength;

	public VariableByteLengthNumber(long value, int byteLength) {
		super();
		this.value = value;
		this.byteLength = byteLength;
	}

	@Override
	public int intValue() {
		return (int) value;
	}

	@Override
	public long longValue() {
		return value;
	}

	@Override
	public float floatValue() {
		return value;
	}

	@Override
	public double doubleValue() {
		return value;
	}

	public int getByteLength() {
		return byteLength;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof VariableByteLengthNumber))
			return false;
		VariableByteLengthNumber vbln = (VariableByteLengthNumber) obj;
		return vbln.value == value && vbln.byteLength == byteLength;
	}

	@Override
	public String toString() {
		return Long.toString(value);
	}

	@Override
	public int hashCode() {
		return intValue();
	}
}
