package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ESudsRoomStatus;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.lang.InvalidIntValueException;

/**
 *  Message Data for ROOM_STATUS_WITH_STARTING_PORT_NUMBER - "Room Status With Starting Port Number - 9A41h"
 */
public class MessageData_9A41 extends AbstractMessageData implements ESudsRoomStatusMessage {
	protected int startingPortNumber;
	public class RoomStatuData implements ESudsRoomStatusData {
	protected ESudsRoomStatus top = ESudsRoomStatus.EQUIP_STATUS_NO_STATUS_AVAILABLE;
	protected ESudsRoomStatus bottom = ESudsRoomStatus.EQUIP_STATUS_NO_STATUS_AVAILABLE;
		protected final int index;
		protected RoomStatuData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		try {
			setTop(ESudsRoomStatus.getByValue(ProcessingUtils.read4BitInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setBottom(ESudsRoomStatus.getByValue(ProcessingUtils.readPrev4BitInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.write4BitInt(reply, getTop().getValue());
		ProcessingUtils.writePrev4BitInt(reply, getBottom().getValue());
		}

	public ESudsRoomStatus getTop() {
		return top;
	}

	public void setTop(ESudsRoomStatus top) {
		this.top = top;
	}

	public ESudsRoomStatus getBottom() {
		return bottom;
	}

	public void setBottom(ESudsRoomStatus bottom) {
		this.bottom = bottom;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("top=").append(getTop())
			.append("; bottom=").append(getBottom());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<RoomStatuData> roomStatus = new ArrayList<RoomStatuData>();
	protected final List<RoomStatuData> roomStatusUnmod = Collections.unmodifiableList(roomStatus);

	public MessageData_9A41() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.ROOM_STATUS_WITH_STARTING_PORT_NUMBER;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByteInt(reply, getStartingPortNumber());
		for(RoomStatuData roomStatu : getRoomStatus())
			roomStatu.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setStartingPortNumber(ProcessingUtils.readByteInt(data));
		roomStatus.clear();
		while(data.hasRemaining())
			addRoomStatu().readData(data);
	}

	public int getStartingPortNumber() {
		return startingPortNumber;
	}

	public void setStartingPortNumber(int startingPortNumber) {
		this.startingPortNumber = startingPortNumber;
	}

	public List<RoomStatuData> getRoomStatus() {
		return roomStatusUnmod;
	}

	public RoomStatuData addRoomStatu() {
		RoomStatuData roomStatu = new RoomStatuData(roomStatus.size());
		roomStatus.add(roomStatu);
		return roomStatu;
	}


			public boolean isFullRoomStatus(){
				return false;
			}
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; startingPortNumber=").append(getStartingPortNumber())
			.append("; roomStatus=").append(getRoomStatus());
		sb.append("]");

		return sb.toString();
	}
}
