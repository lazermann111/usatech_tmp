package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS_CHARGE_V_3_REQUEST - "Web Service - Charge V 3 Request - 0202h"
 */
public class MessageData_0202 extends WSChargeMessageData implements PlainCardReader {
	protected Long amount;
	protected String cardData;
	protected String cardType;

	public MessageData_0202() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS_CHARGE_V_3_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getTranId());
		reply.putLong(getAmount());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getCardData()) : getCardData(), charset);
		ProcessingUtils.writeLongString(reply, getCardType(), charset);
		ProcessingUtils.writeLongString(reply, getTranResult(), charset);
		ProcessingUtils.writeLongString(reply, getTranDetails(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setTranId(data.getLong());
		setAmount(data.getLong());
		setCardData(ProcessingUtils.readLongString(data, charset));
		setCardType(ProcessingUtils.readLongString(data, charset));
		setTranResult(ProcessingUtils.readLongString(data, charset));
		setTranDetails(ProcessingUtils.readLongString(data, charset));
	}

	public String getAccountData() {
		return getCardData();
	}

	public void setAccountData(String accountData) {
		setCardData(accountData);
	}

	public CardReaderType getCardReaderType() {
		return CardReaderType.GENERIC;
	}

	public CardReader getCardReader() {
		return this;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getCardData() {
		return cardData;
	}

	public void setCardData(String cardData) {
		this.cardData = cardData;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public MessageData_0301 createResponse() {
		MessageData_0301 response = new MessageData_0301();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", tranId=").append(getTranId())
			.append(", amount=").append(getAmount())
			.append(", cardData=").append(MessageResponseUtils.maskTrackData(getCardData()))
			.append(", cardType=").append(getCardType())
			.append(", tranResult=").append(getTranResult())
			.append(", tranDetails=").append(getTranDetails());
		sb.append("]");

		return sb.toString();
	}
}
