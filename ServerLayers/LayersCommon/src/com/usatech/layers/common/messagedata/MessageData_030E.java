package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.ec2.EC2DataHandler;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS2_PROCESS_UPDATES_RESPONSE - "Web Service - Process Updates Response - 030Eh"
 */
public class MessageData_030E extends WS2ResponseMessageData {
	protected EC2DataHandler fileContent = EMPTY_DATA_HANDLER;
	protected String fileName = "";
	protected long fileSize;
	protected int fileType;
	protected long newTranId;

	public MessageData_030E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_PROCESS_UPDATES_RESPONSE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getFileName(), charset);
		reply.putLong(getFileSize());
		reply.putInt(getFileType());
		reply.putInt(getActionCode());
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getNewPassword()) : getNewPassword(), charset);
		reply.putLong(getNewTranId());
		ProcessingUtils.writeLongString(reply, getNewUsername(), charset);
		reply.putInt(getReturnCode());
		ProcessingUtils.writeLongString(reply, getReturnMessage(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setFileName(ProcessingUtils.readLongString(data, charset));
		setFileSize(data.getLong());
		setFileType(data.getInt());
		setActionCode(data.getInt());
		setAttributes(ProcessingUtils.readLongString(data, charset));
		setNewPassword(ProcessingUtils.readLongString(data, charset));
		setNewTranId(data.getLong());
		setNewUsername(ProcessingUtils.readLongString(data, charset));
		setReturnCode(data.getInt());
		setReturnMessage(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
	}

	public EC2DataHandler getFileContent() {
		return fileContent;
	}

	public void setFileContent(EC2DataHandler fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public long getNewTranId() {
		return newTranId;
	}

	public void setNewTranId(long newTranId) {
		this.newTranId = newTranId;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", fileName=").append(getFileName())
			.append(", fileSize=").append(getFileSize())
			.append(", fileType=").append(getFileType())
			.append(", actionCode=").append(getActionCode())
			.append(", attributes=").append(getAttributes())
			.append(", newPassword=").append(MessageDataUtils.maskString(getNewPassword()))
			.append(", newTranId=").append(getNewTranId())
			.append(", newUsername=").append(getNewUsername())
			.append(", returnCode=").append(getReturnCode())
			.append(", returnMessage=").append(getReturnMessage())
			.append(", serialNumber=").append(getSerialNumber());
		sb.append("]");

		return sb.toString();
	}
}
