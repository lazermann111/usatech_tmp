package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  Message Data for WASHER_DRYER_DIAGNOSTICS - "Washer/Dryer Diagnostics - 9A62h"
 */
public class MessageData_9A62 extends AbstractMessageData {
	protected int portNumber;
	public class DiagBlockData {
	protected int hostDiagCode;
		protected final int index;
		protected DiagBlockData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setHostDiagCode(ProcessingUtils.readByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getHostDiagCode());
		}

	public int getHostDiagCode() {
		return hostDiagCode;
	}

	public void setHostDiagCode(int hostDiagCode) {
		this.hostDiagCode = hostDiagCode;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("hostDiagCode=").append(getHostDiagCode());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<DiagBlockData> diagBlocks = new ArrayList<DiagBlockData>();
	protected final List<DiagBlockData> diagBlocksUnmod = Collections.unmodifiableList(diagBlocks);

	public MessageData_9A62() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WASHER_DRYER_DIAGNOSTICS;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByteInt(reply, getPortNumber());
		for(DiagBlockData diagBlock : getDiagBlocks())
			diagBlock.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setPortNumber(ProcessingUtils.readByteInt(data));
		diagBlocks.clear();
		while(data.hasRemaining())
			addDiagBlock().readData(data);
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public List<DiagBlockData> getDiagBlocks() {
		return diagBlocksUnmod;
	}

	public DiagBlockData addDiagBlock() {
		DiagBlockData diagBlock = new DiagBlockData(diagBlocks.size());
		diagBlocks.add(diagBlock);
		return diagBlock;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; portNumber=").append(getPortNumber())
			.append("; diagBlocks=").append(getDiagBlocks());
		sb.append("]");

		return sb.toString();
	}
}
