package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS2_REPLENISH_CASH_REQUEST - "Web Service - Replenish Cash Request - 0218h"
 */
public class MessageData_0218 extends WS2AuthorizeByMessageData implements ReplenishMessageData {
	protected Long amount;
	protected long replenishCardId;
	protected long replenishConsumerId;

	public MessageData_0218() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_REPLENISH_CASH_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getTranId());
		reply.putLong(getAmount());
		reply.putLong(getReplenishCardId());
		reply.putLong(getReplenishConsumerId());
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setTranId(data.getLong());
		setAmount(data.getLong());
		setReplenishCardId(data.getLong());
		setReplenishConsumerId(data.getLong());
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.REPLENISHMENT;
	}

	public boolean isAuthOnly() {
		return false;
	}

	public long getBatchId() {
		return 0;
	}

	public TranDeviceResultType getTransactionResult() {
		return TranDeviceResultType.SUCCESS;
	}

	public ReceiptResult getReceiptResult() {
		return ReceiptResult.UNAVAILABLE;
	}

	public EntryType getEntryType() {
		return EntryType.CASH;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public long getReplenishCardId() {
		return replenishCardId;
	}

	public void setReplenishCardId(long replenishCardId) {
		this.replenishCardId = replenishCardId;
	}

	public long getReplenishConsumerId() {
		return replenishConsumerId;
	}

	public void setReplenishConsumerId(long replenishConsumerId) {
		this.replenishConsumerId = replenishConsumerId;
	}

	public MessageData_030C createResponse() {
		MessageData_030C response = new MessageData_030C();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", tranId=").append(getTranId())
			.append(", amount=").append(getAmount())
			.append(", replenishCardId=").append(getReplenishCardId())
			.append(", replenishConsumerId=").append(getReplenishConsumerId())
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
