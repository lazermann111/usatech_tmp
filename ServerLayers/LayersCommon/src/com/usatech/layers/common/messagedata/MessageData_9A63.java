package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.WasherDryerType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.lang.InvalidValueException;

/**
 *  Message Data for ROOM_MODEL_INFO_LAYOUT_2_0 - "Room Model Info/Layout 2.0 - 9A63h"
 */
public class MessageData_9A63 extends AbstractMessageData {
	public class ComponentData extends ESudsComponent {
	protected int componentNumber;
	protected String serialNumber;
	protected Integer modelCode;
	protected Integer firmwareVersion;
	protected int maxPrice;
	protected WasherDryerType washerDryerType = WasherDryerType.NO_WASHER_DRYER_ON_PORT;
	public class SupportedCycleData implements SupportedCycle {
	protected int cycleType;
	protected int cyclePrice;
	protected int cycleTime;
		protected final int index;
		protected SupportedCycleData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setCycleType(ProcessingUtils.readByteInt(data));
		setCyclePrice(ProcessingUtils.readShortInt(data));
		setCycleTime(ProcessingUtils.readByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getCycleType());
		ProcessingUtils.writeShortInt(reply, getCyclePrice());
		ProcessingUtils.writeByteInt(reply, getCycleTime());
		}

	public int getCycleType() {
		return cycleType;
	}

	public void setCycleType(int cycleType) {
		this.cycleType = cycleType;
	}

	public int getCyclePrice() {
		return cyclePrice;
	}

	public void setCyclePrice(int cyclePrice) {
		this.cyclePrice = cyclePrice;
	}

	public int getCycleTime() {
		return cycleTime;
	}

	public void setCycleTime(int cycleTime) {
		this.cycleTime = cycleTime;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("cycleType=").append(getCycleType())
			.append("; cyclePrice=").append(getCyclePrice())
			.append("; cycleTime=").append(getCycleTime());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<SupportedCycleData> supportedCycles = new ArrayList<SupportedCycleData>();
	protected final List<SupportedCycleData> supportedCyclesUnmod = Collections.unmodifiableList(supportedCycles);
		protected final int index;
		protected ComponentData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setComponentNumber(ProcessingUtils.readByteInt(data));
		setSerialNumber(ProcessingUtils.read10ByteString(data, getCharset()));
		setModelCode(ProcessingUtils.readByteInt(data));
		setFirmwareVersion(ProcessingUtils.readByteInt(data));
		setMaxPrice(ProcessingUtils.readShortInt(data));
		try {
			setWasherDryerType(WasherDryerType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		supportedCycles.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addSupportedCycle().readData(data);
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.write10ByteString(reply, getSerialNumber(), getCharset());
		ProcessingUtils.writeByteInt(reply, getModelCode());
		ProcessingUtils.writeByteInt(reply, getFirmwareVersion());
		ProcessingUtils.writeShortInt(reply, getMaxPrice());
		ProcessingUtils.writeByteInt(reply, getWasherDryerType().getValue());
		ProcessingUtils.writeByteInt(reply, getSupportedCycles().size());
		for(SupportedCycleData supportedCycle : getSupportedCycles())
			supportedCycle.writeData(reply, maskSensitiveData);
		}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getModelCode() {
		return modelCode;
	}

	public void setModelCode(Integer modelCode) {
		this.modelCode = modelCode;
	}

	public Integer getFirmwareVersion() {
		return firmwareVersion;
	}

	public void setFirmwareVersion(Integer firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public WasherDryerType getWasherDryerType() {
		return washerDryerType;
	}

	public void setWasherDryerType(WasherDryerType washerDryerType) {
		this.washerDryerType = washerDryerType;
	}

	public List<SupportedCycleData> getSupportedCycles() {
		return supportedCyclesUnmod;
	}

	public SupportedCycleData addSupportedCycle() {
		SupportedCycleData supportedCycle = new SupportedCycleData(supportedCycles.size());
		supportedCycles.add(supportedCycle);
		return supportedCycle;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("componentNumber=").append(getComponentNumber())
			.append("; serialNumber=").append(getSerialNumber())
			.append("; modelCode=").append(getModelCode())
			.append("; firmwareVersion=").append(getFirmwareVersion())
			.append("; maxPrice=").append(getMaxPrice())
			.append("; washerDryerType=").append(getWasherDryerType())
			.append("; supportedCycles=").append(getSupportedCycles());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<ComponentData> components = new ArrayList<ComponentData>();
	protected final List<ComponentData> componentsUnmod = Collections.unmodifiableList(components);

	public MessageData_9A63() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.ROOM_MODEL_INFO_LAYOUT_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		for(ComponentData component : getComponents())
			component.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		components.clear();
		while(data.hasRemaining())
			addComponent().readData(data);
	}

	public List<ComponentData> getComponents() {
		return componentsUnmod;
	}

	public ComponentData addComponent() {
		ComponentData component = new ComponentData(components.size());
		components.add(component);
		return component;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; components=").append(getComponents());
		sb.append("]");

		return sb.toString();
	}
}
