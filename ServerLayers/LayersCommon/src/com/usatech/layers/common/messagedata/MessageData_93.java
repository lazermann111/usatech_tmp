package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.bean.ConvertException;
import simple.lang.InvalidValueException;

/**
 *  Message Data for LOCAL_AUTH_BATCH_BCD_2_0 - "Local Authorization Batch 2.0 BCD Time Format - 93h"
 */
public class MessageData_93 extends LegacySale implements LegacyLocalAuth, DeviceTypeSpecific {
	public interface TransactionDetailData extends Byteable {
		public List<? extends LineItem> getLineItems() throws ConvertException;
	}
	protected DeviceType deviceType = DeviceType.DEFAULT;
	protected long transactionId;
	protected long saleStartDate;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected Integer saleAmount;
	protected Integer saleTax;
	protected String creditCardMagstripe;
	protected TranDeviceResultType transactionResult = TranDeviceResultType.AUTH_FAILURE;
	public class EportTransactionDetail implements TransactionDetailData {
	public class NetBatch0LineItem extends NetBatchLineItem implements LineItem {
	protected Integer positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch0LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.readByteAsBCD(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteAsBCD(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 1;
		}
		                
	}
	public class NetBatch1LineItem extends NetBatchLineItem implements LineItem {
	protected Integer positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch1LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.readShortInt(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 2;
		}
                        
	}
	public class NetBatch2LineItem extends NetBatchLineItem implements LineItem {
	protected Integer positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch2LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.read3ByteInt(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.write3ByteInt(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 3;
		}
                        
	}
	public class NetBatch3LineItem extends NetBatchLineItem implements LineItem {
	protected Long positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch3LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.readLongInt(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Long getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Long positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 4;
		}
                        
	}
	protected byte vendByteLength;
	protected final List<LineItem> lineItems = new ArrayList<LineItem>();
	protected final List<LineItem> lineItemsUnmod = Collections.unmodifiableList(lineItems);
		protected EportTransactionDetail() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setVendByteLength(ProcessingUtils.read2BitInt(data));
		lineItems.clear();
		for(int i = 0, n = ProcessingUtils.readPrev6BitInt(data); i < n; i++)
			addLineItem().readData(data);
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.write2BitInt(reply, getVendByteLength());
		ProcessingUtils.writePrev6BitInt(reply, (byte) getLineItems().size());
		for(LineItem lineItem : getLineItems())
			lineItem.writeData(reply, maskSensitiveData);
		}

	public byte getVendByteLength() {
		return vendByteLength;
	}

	public List<LineItem> getLineItems() {
		return lineItemsUnmod;
	}

	public LineItem addLineItem() {
		LineItem lineItem;
		switch(getVendByteLength()) {
			case 0: lineItem = new NetBatch0LineItem(lineItems.size()); break;
			case 1: lineItem = new NetBatch1LineItem(lineItems.size()); break;
			case 2: lineItem = new NetBatch2LineItem(lineItems.size()); break;
			case 3: lineItem = new NetBatch3LineItem(lineItems.size()); break;
			default: throw new IllegalArgumentException("The VendByteLength '" + getVendByteLength() + "' is not supported");
		}
		lineItems.add(lineItem);
		return lineItem;
	}

	public void setVendByteLength(byte vendByteLength) {
		this.vendByteLength = vendByteLength;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("vendByteLength=").append(getVendByteLength() & 0xFF)
			.append("; lineItems=").append(getLineItems());
			sb.append(']');
			return sb.toString();
		}
	}
	protected TransactionDetailData transactionDetailData;

	public MessageData_93() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.LOCAL_AUTH_BATCH_BCD_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeBCDTimestamp(reply, getSaleStartDate());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.write3ByteInt(reply, getSaleAmount());
		ProcessingUtils.writeShortInt(reply, getSaleTax());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getCreditCardMagstripe()) : getCreditCardMagstripe(), getCharset());
		ProcessingUtils.writeByteInt(reply, getTransactionResult().getValue());
		getTransactionDetailData().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		setSaleStartDate(ProcessingUtils.readBCDTimestamp(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setSaleAmount(ProcessingUtils.read3ByteInt(data));
		setSaleTax(ProcessingUtils.readShortInt(data));
		setCreditCardMagstripe(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setTransactionResult(TranDeviceResultType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		getTransactionDetailData().readData(data);
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getSaleStartDate() {
		return saleStartDate;
	}

	public void setSaleStartDate(long saleStartDate) {
		this.saleStartDate = saleStartDate;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Integer getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(Integer saleAmount) {
		this.saleAmount = saleAmount;
	}

	public Integer getSaleTax() {
		return saleTax;
	}

	public void setSaleTax(Integer saleTax) {
		this.saleTax = saleTax;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public TranDeviceResultType getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(TranDeviceResultType transactionResult) {
		this.transactionResult = transactionResult;
	}

	public TransactionDetailData getTransactionDetailData() {
		switch(getDeviceType()) {
			case DEFAULT: case G4: case GX: case MEI: case LEGACY_G4: case EDGE: 
				if(!(transactionDetailData instanceof EportTransactionDetail))
					transactionDetailData = new EportTransactionDetail();
				break;
			default: throw new IllegalArgumentException("The deviceType '" + getDeviceType() + "' is not supported");
		}
		return transactionDetailData;
	}


	public Long getSaleStartTime() {
		return getSaleStartDate();
	}
	public SaleResult getSaleResult() {
		return SaleResult.CANCELLED_BY_AUTH_FAILURE;
	}
	
	public List<? extends LineItem> getLineItems() {
		try {
			return getTransactionDetailData() == null ? null : getTransactionDetailData().getLineItems();
		} catch(ConvertException e) {
			throw new UndeclaredThrowableException(e);
		}
	}
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; saleStartDate=").append(getSaleStartDate())
			.append("; cardType=").append(getCardType())
			.append("; saleAmount=").append(getSaleAmount())
			.append("; saleTax=").append(getSaleTax())
			.append("; creditCardMagstripe=").append(MessageResponseUtils.maskTrackData(getCreditCardMagstripe()))
			.append("; transactionResult=").append(getTransactionResult())
			.append("; transactionDetailData=").append(getTransactionDetailData());
		sb.append("]");

		return sb.toString();
	}
}
