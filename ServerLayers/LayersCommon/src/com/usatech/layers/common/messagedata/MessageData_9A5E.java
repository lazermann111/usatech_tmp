package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ESudsTopOrBottomType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.lang.InvalidValueException;

/**
 *  Message Data for ACTUAL_PURCHASE_NETWORK_AUTH_BATCH - "Actual Purchase Network Auth Batch - 9A5Eh"
 */
public class MessageData_9A5E extends ESudsSale {
	protected long transactionId;
	public class LineItemData extends ESudsLineItem {
	protected int componentNumber;
	protected ESudsTopOrBottomType topOrBottom = ESudsTopOrBottomType.TOP;
	protected int item;
	protected int quantity;
	protected Integer price;
		protected final int index;
		protected LineItemData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setComponentNumber(ProcessingUtils.readByteInt(data));
		try {
			setTopOrBottom(ESudsTopOrBottomType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setItem(ProcessingUtils.readByteInt(data));
		setQuantity(ProcessingUtils.readByteInt(data));
		setPrice(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.writeByte(reply, getTopOrBottom().getValue());
		ProcessingUtils.writeByteInt(reply, getItem());
		ProcessingUtils.writeByteInt(reply, getQuantity());
		ProcessingUtils.writeShortInt(reply, getPrice());
		}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public ESudsTopOrBottomType getTopOrBottom() {
		return topOrBottom;
	}

	public void setTopOrBottom(ESudsTopOrBottomType topOrBottom) {
		this.topOrBottom = topOrBottom;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("componentNumber=").append(getComponentNumber())
			.append("; topOrBottom=").append(getTopOrBottom())
			.append("; item=").append(getItem())
			.append("; quantity=").append(getQuantity())
			.append("; price=").append(getPrice());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<LineItemData> lineItems = new ArrayList<LineItemData>();
	protected final List<LineItemData> lineItemsUnmod = Collections.unmodifiableList(lineItems);

	public MessageData_9A5E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.ACTUAL_PURCHASE_NETWORK_AUTH_BATCH;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getLineItems().size());
		for(LineItemData lineItem : getLineItems())
			lineItem.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		lineItems.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addLineItem().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public List<LineItemData> getLineItems() {
		return lineItemsUnmod;
	}

	public LineItemData addLineItem() {
		LineItemData lineItem = new LineItemData(lineItems.size());
		lineItems.add(lineItem);
		return lineItem;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; lineItems=").append(getLineItems());
		sb.append("]");

		return sb.toString();
	}
}
