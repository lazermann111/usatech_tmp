/**
 *
 */
package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.TranDeviceResultType;

/**
 * @author Brian S. Krug
 *
 */
public interface ReplenishMessageData extends AuthorizeMessageData {

	public long getBatchId();

	public TranDeviceResultType getTransactionResult() ;

	public ReceiptResult getReceiptResult();

	public boolean isAuthOnly();

}