package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ShutdownOption;
import com.usatech.layers.common.constants.ShutdownState;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;

/**
 *  Message Data for FORCE_DEVICE_SHUTDOWN - "Force the device to shutdown - 73h"
 */
public class MessageData_73 extends AbstractMessageData {
	protected ShutdownOption shutdownOption = ShutdownOption.SEND_TRAN_BEFORE_SHUTDOWN;
	protected ShutdownState shutdownState = ShutdownState.CAN_REACTIVATE;

	public MessageData_73() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.FORCE_DEVICE_SHUTDOWN;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getShutdownOption().getValue());
		ProcessingUtils.writeByte(reply, getShutdownState().getValue());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setShutdownOption(ShutdownOption.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setShutdownState(ShutdownState.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
	}

	public ShutdownOption getShutdownOption() {
		return shutdownOption;
	}

	public void setShutdownOption(ShutdownOption shutdownOption) {
		this.shutdownOption = shutdownOption;
	}

	public ShutdownState getShutdownState() {
		return shutdownState;
	}

	public void setShutdownState(ShutdownState shutdownState) {
		this.shutdownState = shutdownState;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; shutdownOption=").append(getShutdownOption())
			.append("; shutdownState=").append(getShutdownState());
		sb.append("]");

		return sb.toString();
	}
}
