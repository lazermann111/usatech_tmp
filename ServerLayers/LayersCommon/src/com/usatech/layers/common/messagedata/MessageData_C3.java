package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.AuthPermissionActionCode;
import com.usatech.layers.common.constants.AuthResponseActionCode;
import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.PermissionResponseCodeC3;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import java.math.BigDecimal;
import java.util.Map;
import simple.lang.InvalidByteValueException;

/**
 *  Message Data for AUTH_RESPONSE_4_1 - "Authorization Response 4.1 - C3h"
 */
public class MessageData_C3 extends AbstractMessageData {
	public interface AuthResponseTypeData extends Byteable {
	}
	public interface ActionCodeData extends Byteable {
	}
	public interface AdditionalData extends Byteable {
	}
	public interface AdditionalMobileData extends Byteable {
	}
	public interface CommandCodeData extends Byteable {
	}
	public class AuthorizationAuthResponse implements AuthResponseTypeData {
	protected AuthResponseCodeC3 responseCode = AuthResponseCodeC3.SUCCESS;
	protected BigDecimal balanceAmount;
	protected BigDecimal resultAmount;
	protected String authorizationCode;
	protected String resultMessage;
	protected String overrideResultMessage;
		protected AuthorizationAuthResponse() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		try {
			setResponseCode(AuthResponseCodeC3.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setBalanceAmount(ProcessingUtils.readStringAmountNull(data));
		setResultAmount(ProcessingUtils.readStringAmountNull(data));
		setAuthorizationCode(ProcessingUtils.readShortString(data, getCharset()));
		setResultMessage(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
		ProcessingUtils.writeStringAmount(reply, getBalanceAmount());
		ProcessingUtils.writeStringAmount(reply, getResultAmount());
		ProcessingUtils.writeShortString(reply, getAuthorizationCode(), getCharset());
		ProcessingUtils.writeShortString(reply, getResultMessage(), getCharset());
		}

	public AuthResponseCodeC3 getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(AuthResponseCodeC3 responseCode) {
		this.responseCode = responseCode;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public BigDecimal getResultAmount() {
		return resultAmount;
	}

	public void setResultAmount(BigDecimal resultAmount) {
		this.resultAmount = resultAmount;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getResultMessage() {
		return overrideResultMessage == null ? resultMessage : overrideResultMessage;
	}

	public String getOverrideResultMessage() {
			return overrideResultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public void setOverrideResultMessage(String overrideResultMessage) {
		this.overrideResultMessage = overrideResultMessage;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("responseCode=").append(getResponseCode())
			.append("; balanceAmount=").append(getBalanceAmount())
			.append("; resultAmount=").append(getResultAmount())
			.append("; authorizationCode=").append(getAuthorizationCode())
			.append("; resultMessage=").append(MessageResponseUtils.cleanNewlines(getResultMessage()));
			sb.append(']');
			return sb.toString();
		}
	}
	public class PermissionAuthResponse implements AuthResponseTypeData {
	protected PermissionResponseCodeC3 responseCode = PermissionResponseCodeC3.SUCCESS;
	public class ConfigurableAction implements ActionCodeData {
	protected long actionBitmap;
		protected ConfigurableAction() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setActionBitmap(ProcessingUtils.readLongInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getActionBitmap());
		}

	public long getActionBitmap() {
		return actionBitmap;
	}

	public void setActionBitmap(long actionBitmap) {
		this.actionBitmap = actionBitmap;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("actionBitmap=").append(getActionBitmap());
			sb.append(']');
			return sb.toString();
		}
	}
	protected AuthPermissionActionCode actionCode;
	protected ActionCodeData actionCodeData;
		protected PermissionAuthResponse() {
		setActionCode(AuthPermissionActionCode.DO_NOTHING);
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		try {
			setResponseCode(PermissionResponseCodeC3.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setActionCode(AuthPermissionActionCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getActionCodeData() != null)
			getActionCodeData().readData(data);
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
		ProcessingUtils.writeByte(reply, getActionCode().getValue());
		if(getActionCodeData() != null)
			getActionCodeData().writeData(reply, maskSensitiveData);
		}

	public PermissionResponseCodeC3 getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(PermissionResponseCodeC3 responseCode) {
		this.responseCode = responseCode;
	}

	public AuthPermissionActionCode getActionCode() {
		return actionCode;
	}

	public ActionCodeData getActionCodeData() {
		return actionCodeData;
	}

		public void setActionCode(AuthPermissionActionCode actionCode) {
			this.actionCode = actionCode;
			switch (actionCode) {
			case DO_NOTHING:
			case SHOW_ALL_PERMANENT_COUNTERS:
			case SHOW_ALL_INTERNAL_COUNTERS:
			case SHOW_PERMANENT_CASHLESS_COUNTERS:
			case SHOW_INTERNAL_CASHLESS_COUNTERS:
			case SHOW_RSSI:
			case SHOW_SERVICE_MENU:
			case SHOW_PREVIOUS:
				this.actionCodeData = null;
				break;
			case DRIVER_CARD:
			case FORCED_CARD:
			case AUDIT_CARD:
			case READ_DEX_WITH_DIAG_CODES:
				this.actionCodeData = new ConfigurableAction();
				break;
			default:
				throw new IllegalArgumentException("The ActionCode '" + actionCode + "' is not supported");
			}
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("responseCode=").append(getResponseCode())
			.append("; actionCode=").append(getActionCode())
			.append("; actionCodeData=").append(getActionCodeData());
			sb.append(']');
			return sb.toString();
		}
	}
	public class AuthorizationV2AuthResponse implements AuthResponseTypeData {
	protected AuthResponseCodeC3 responseCode = AuthResponseCodeC3.SUCCESS;
	protected BigDecimal balanceAmount;
	protected BigDecimal resultAmount;
	protected String authorizationCode;
	protected String resultMessage;
	protected String overrideResultMessage;
	public class BehaviorBitmap extends ShortLengthByteable implements AdditionalData {
	protected int behaviorBitmap;
		protected BehaviorBitmap() {
		}

		public void readContent(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setBehaviorBitmap(ProcessingUtils.readByteInt(data));
		}

		public void writeContent(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getBehaviorBitmap());
		}

	public int getBehaviorBitmap() {
		return behaviorBitmap;
	}

	public void setBehaviorBitmap(int behaviorBitmap) {
		this.behaviorBitmap = behaviorBitmap;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("behaviorBitmap=").append(getBehaviorBitmap());
			sb.append(']');
			return sb.toString();
		}
	}
	public class MobileAppData extends ShortLengthByteable implements AdditionalData {
	protected int behaviorBitmap;
	protected byte[] sessionKey;
	protected byte additionalMobileDataType;
	protected AdditionalMobileData additionalMobileData;
		protected MobileAppData() {
		}

		public void readContent(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setBehaviorBitmap(ProcessingUtils.readByteInt(data));
		setSessionKey(ProcessingUtils.readShortBytes(data));
		setAdditionalMobileDataType(ProcessingUtils.readByte(data));
		if(getAdditionalMobileData() != null)
			getAdditionalMobileData().readData(data);
		}

		public void writeContent(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getBehaviorBitmap());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageDataUtils.summarizeBytes(getSessionKey()) : getSessionKey());
		ProcessingUtils.writeByte(reply, getAdditionalMobileDataType());
		if(getAdditionalMobileData() != null)
			getAdditionalMobileData().writeData(reply, maskSensitiveData);
		}

	public int getBehaviorBitmap() {
		return behaviorBitmap;
	}

	public void setBehaviorBitmap(int behaviorBitmap) {
		this.behaviorBitmap = behaviorBitmap;
	}

	public byte[] getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(byte[] sessionKey) {
		this.sessionKey = sessionKey;
	}

	public byte getAdditionalMobileDataType() {
		return additionalMobileDataType;
	}

	public AdditionalMobileData getAdditionalMobileData() {
		return additionalMobileData;
	}

	public void setAdditionalMobileDataType(byte additionalMobileDataType) {
		this.additionalMobileDataType = additionalMobileDataType;
		switch(additionalMobileDataType) {
			case 0: this.additionalMobileData = null; break;
			default: throw new IllegalArgumentException("The AdditionalMobileDataType '" + additionalMobileDataType + "' is not supported");
		}
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("behaviorBitmap=").append(getBehaviorBitmap())
			.append("; sessionKey=").append(new String(MessageDataUtils.summarizeBytes(getSessionKey()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; additionalMobileDataType=").append(getAdditionalMobileDataType() & 0xFF)
			.append("; additionalMobileData=").append(getAdditionalMobileData());
			sb.append(']');
			return sb.toString();
		}
	}
	protected byte additionalDataType;
	protected AdditionalData additionalData;
		protected AuthorizationV2AuthResponse() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		try {
			setResponseCode(AuthResponseCodeC3.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setBalanceAmount(ProcessingUtils.readStringAmountNull(data));
		setResultAmount(ProcessingUtils.readStringAmountNull(data));
		setAuthorizationCode(ProcessingUtils.readShortString(data, getCharset()));
		setResultMessage(ProcessingUtils.readShortString(data, getCharset()));
		setAdditionalDataType(ProcessingUtils.readByte(data));
		if(getAdditionalData() != null)
			getAdditionalData().readData(data);
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
		ProcessingUtils.writeStringAmount(reply, getBalanceAmount());
		ProcessingUtils.writeStringAmount(reply, getResultAmount());
		ProcessingUtils.writeShortString(reply, getAuthorizationCode(), getCharset());
		ProcessingUtils.writeShortString(reply, getResultMessage(), getCharset());
		ProcessingUtils.writeByte(reply, getAdditionalDataType());
		if(getAdditionalData() != null)
			getAdditionalData().writeData(reply, maskSensitiveData);
		}

	public AuthResponseCodeC3 getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(AuthResponseCodeC3 responseCode) {
		this.responseCode = responseCode;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public BigDecimal getResultAmount() {
		return resultAmount;
	}

	public void setResultAmount(BigDecimal resultAmount) {
		this.resultAmount = resultAmount;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getResultMessage() {
		return overrideResultMessage == null ? resultMessage : overrideResultMessage;
	}

	public String getOverrideResultMessage() {
			return overrideResultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public void setOverrideResultMessage(String overrideResultMessage) {
		this.overrideResultMessage = overrideResultMessage;
	}

	public byte getAdditionalDataType() {
		return additionalDataType;
	}

	public AdditionalData getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalDataType(byte additionalDataType) {
		this.additionalDataType = additionalDataType;
		switch(additionalDataType) {
			case 0: this.additionalData = null; break;
			case 1: this.additionalData = new BehaviorBitmap(); break;
			case 2: this.additionalData = new MobileAppData(); break;
			default: throw new IllegalArgumentException("The AdditionalDataType '" + additionalDataType + "' is not supported");
		}
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("responseCode=").append(getResponseCode())
			.append("; balanceAmount=").append(getBalanceAmount())
			.append("; resultAmount=").append(getResultAmount())
			.append("; authorizationCode=").append(getAuthorizationCode())
			.append("; resultMessage=").append(MessageResponseUtils.cleanNewlines(getResultMessage()))
			.append("; additionalDataType=").append(getAdditionalDataType() & 0xFF)
			.append("; additionalData=").append(getAdditionalData());
			sb.append(']');
			return sb.toString();
		}

	    public boolean getBehaviorBit(int bitIndex) {
	        int mask = 1 << bitIndex;
            switch(additionalDataType) {
                case 1:
                    BehaviorBitmap bb = (BehaviorBitmap)getAdditionalData();
                    return (bb.getBehaviorBitmap() & mask) == mask;
                case 2:
                    MobileAppData mad = (MobileAppData)getAdditionalData();
                    return (mad.getBehaviorBitmap() & mask) == mask;
            }
            return false;    
	    }
        public void setBehaviorBit(int bitIndex, boolean on) {
            int mask = 1 << bitIndex;
            if(on) {
                switch(additionalDataType) {
                    case 0:
                        setAdditionalDataType((byte)1);
                    case 1:
                        BehaviorBitmap bb = (BehaviorBitmap)getAdditionalData();
                        bb.setBehaviorBitmap(bb.getBehaviorBitmap() | mask);
                        break;
                    case 2:
                        MobileAppData mad = (MobileAppData)getAdditionalData();
                        mad.setBehaviorBitmap(mad.getBehaviorBitmap() | mask);
                        break;
                }
            } else {
                switch(additionalDataType) {
                    case 1:
                        BehaviorBitmap bb = (BehaviorBitmap)getAdditionalData();
                        bb.setBehaviorBitmap(bb.getBehaviorBitmap() & ~mask);
                        break;
                    case 2:
                        MobileAppData mad = (MobileAppData)getAdditionalData();
                        mad.setBehaviorBitmap(mad.getBehaviorBitmap() & ~mask);
                        break;
                }
            }
        }
        public void setFreeTransaction(boolean freeTransaction) {
            setBehaviorBit(0, freeTransaction);
        }

        public void setNoConvenienceFee(boolean noConvenienceFee) {
            setBehaviorBit(1, noConvenienceFee);
        }
        
        public void setAppSessionKey(byte[] appSessionKey) {
            if(appSessionKey != null) {
                int behaviorBitmap = 0;
                switch(additionalDataType) {
                    case 1:
                        BehaviorBitmap bb = (BehaviorBitmap) getAdditionalData();
                        behaviorBitmap = bb.getBehaviorBitmap();
                    case 0:
                        setAdditionalDataType((byte) 2);
                    case 2:
                        MobileAppData mad = (MobileAppData) getAdditionalData();
                        if(behaviorBitmap != 0)
                            mad.setBehaviorBitmap(behaviorBitmap);
                        mad.setSessionKey(appSessionKey);
                        break;
                }
            }
        }
			    
	}
	protected AuthResponseType authResponseType;
	protected AuthResponseTypeData authResponseTypeData;
	public class CallInCommand implements CommandCodeData {
	protected int delaySeconds;
		protected CallInCommand() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setDelaySeconds(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getDelaySeconds());
		}

	public int getDelaySeconds() {
		return delaySeconds;
	}

	public void setDelaySeconds(int delaySeconds) {
		this.delaySeconds = delaySeconds;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("delaySeconds=").append(getDelaySeconds());
			sb.append(']');
			return sb.toString();
		}
	}
	public class RebootCommand implements CommandCodeData {
	protected byte optionBitmap;
		protected RebootCommand() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setOptionBitmap(ProcessingUtils.readByte(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByte(reply, getOptionBitmap());
		}

	public byte getOptionBitmap() {
		return optionBitmap;
	}

	public void setOptionBitmap(byte optionBitmap) {
		this.optionBitmap = optionBitmap;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("optionBitmap=").append(getOptionBitmap() & 0xFF);
			sb.append(']');
			return sb.toString();
		}
	}
	public class ProcessPropertyListCommand implements CommandCodeData {
	protected Map<Integer,String> propertyValues;
		protected ProcessPropertyListCommand() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPropertyValues(ProcessingUtils.readPropertyValueList(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writePropertyValueList(reply, getPropertyValues(), getCharset());
		}

	public Map<Integer,String> getPropertyValues() {
		return propertyValues;
	}

	public void setPropertyValues(Map<Integer,String> propertyValues) {
		this.propertyValues = propertyValues;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("propertyValues=").append(getPropertyValues());
			sb.append(']');
			return sb.toString();
		}
	}
	protected AuthResponseActionCode commandCode;
	protected CommandCodeData commandCodeData;

	public MessageData_C3() {
		super();
		setAuthResponseType(AuthResponseType.CONTROL);
		setCommandCode(AuthResponseActionCode.NO_ACTION);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_RESPONSE_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getAuthResponseType().getValue());
		if(getAuthResponseTypeData() != null)
			getAuthResponseTypeData().writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getCommandCode().getValue());
		if(getCommandCodeData() != null)
			getCommandCodeData().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setAuthResponseType(AuthResponseType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getAuthResponseTypeData() != null)
			getAuthResponseTypeData().readData(data);
		try {
			setCommandCode(AuthResponseActionCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getCommandCodeData() != null)
			getCommandCodeData().readData(data);
	}

	public AuthResponseType getAuthResponseType() {
		return authResponseType;
	}

	public AuthResponseTypeData getAuthResponseTypeData() {
		return authResponseTypeData;
	}

	public void setAuthResponseType(AuthResponseType authResponseType) {
		this.authResponseType = authResponseType;
		switch(authResponseType) {
			case CONTROL: this.authResponseTypeData = null; break;
			case AUTHORIZATION: this.authResponseTypeData = new AuthorizationAuthResponse(); break;
			case PERMISSION: this.authResponseTypeData = new PermissionAuthResponse(); break;
			case AUTHORIZATION_V2: this.authResponseTypeData = new AuthorizationV2AuthResponse(); break;
			default: throw new IllegalArgumentException("The AuthResponseType '" + authResponseType + "' is not supported");
		}
	}

	public AuthResponseActionCode getCommandCode() {
		return commandCode;
	}

	public CommandCodeData getCommandCodeData() {
		return commandCodeData;
	}

	public void setCommandCode(AuthResponseActionCode commandCode) {
		this.commandCode = commandCode;
		switch(commandCode) {
			case NO_ACTION: this.commandCodeData = null; break;
			case PERFORM_CALL_IN: this.commandCodeData = new CallInCommand(); break;
			case REBOOT: this.commandCodeData = new RebootCommand(); break;
			case PROCESS_PROP_LIST: this.commandCodeData = new ProcessPropertyListCommand(); break;
			default: throw new IllegalArgumentException("The CommandCode '" + commandCode + "' is not supported");
		}
	}


	public void setAuthResultCd(AuthResultCode authResultCd) {
        AuthResponseTypeData artd = getAuthResponseTypeData();
        if(artd instanceof PermissionAuthResponse) {
            PermissionAuthResponse par = (PermissionAuthResponse) artd;
            par.setResponseCode(authResultCd.getPermissionResponseCodeC3());
        } else if(artd instanceof AuthorizationAuthResponse) {
            AuthorizationAuthResponse aar = (AuthorizationAuthResponse) artd;
            aar.setResponseCode(authResultCd.getAuthResponseCodeC3());
            switch(authResultCd) {
                case PARTIAL:
                    break;
                case APPROVED:
                    aar.setResultAmount(null);
                    break;
                default:
                    aar.setResultAmount(null);
                    aar.setOverrideResultMessage(null);
                    break;
            }
        } else if(artd instanceof AuthorizationV2AuthResponse) {
            AuthorizationV2AuthResponse aar = (AuthorizationV2AuthResponse) artd;
            aar.setResponseCode(authResultCd.getAuthResponseCodeC3());
            switch(authResultCd) {
                case PARTIAL:
                    break;
                case APPROVED:
                    aar.setResultAmount(null);
                    break;
                default:
                    aar.setResultAmount(null);
                    aar.setOverrideResultMessage(null);
                    break;
            }
        }
    }
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; authResponseType=").append(getAuthResponseType())
			.append("; authResponseTypeData=").append(getAuthResponseTypeData())
			.append("; commandCode=").append(getCommandCode())
			.append("; commandCodeData=").append(getCommandCodeData());
		sb.append("]");

		return sb.toString();
	}
}
