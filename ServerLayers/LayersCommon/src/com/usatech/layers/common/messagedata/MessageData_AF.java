package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AFActionCode;
import com.usatech.layers.common.constants.AFAdditionalDataType;
import com.usatech.layers.common.constants.AuthResponseCodeAF;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for AUTH_RESPONSE_3_2 - "Authorization Response 3.2 - AFh"
 */
public class MessageData_AF extends AbstractMessageData {
	protected long transactionId;
	protected AuthResponseCodeAF responseCode = AuthResponseCodeAF.DENIED;
	protected AFActionCode actionCode = AFActionCode.NO_ACTION;
	protected String responseMessage;
	protected String overrideResponseMessage;
	protected AFAdditionalDataType additionalDataType = AFAdditionalDataType.NO_ADDITIONAL_DATA;
	protected String additionalData;
	protected Long approvedAmount;

	public MessageData_AF() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_RESPONSE_3_2;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
		ProcessingUtils.writeByte(reply, getActionCode().getValue());
		ProcessingUtils.writeShortString(reply, getResponseMessage(), getCharset());
		ProcessingUtils.writeByteInt(reply, getAdditionalDataType().getValue());
		ProcessingUtils.writeShortString(reply, getAdditionalData(), getCharset());
		ProcessingUtils.writeLongIntRemaining(reply, getApprovedAmount());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setResponseCode(AuthResponseCodeAF.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setActionCode(AFActionCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setResponseMessage(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setAdditionalDataType(AFAdditionalDataType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAdditionalData(ProcessingUtils.readShortString(data, getCharset()));
		setApprovedAmount(ProcessingUtils.readLongIntRemaining(data));
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public AuthResponseCodeAF getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(AuthResponseCodeAF responseCode) {
		this.responseCode = responseCode;
	}

	public AFActionCode getActionCode() {
		return actionCode;
	}

	public void setActionCode(AFActionCode actionCode) {
		this.actionCode = actionCode;
	}

	public String getResponseMessage() {
		return overrideResponseMessage == null || responseCode != AuthResponseCodeAF.APPROVED && responseCode != AuthResponseCodeAF.CONDITIONALLY_APPROVED
				? responseMessage : overrideResponseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getOverrideResponseMessage() {
		return overrideResponseMessage;
	}

	public void setOverrideResponseMessage(String overrideResponseMessage) {
		this.overrideResponseMessage = overrideResponseMessage;
	}

	public AFAdditionalDataType getAdditionalDataType() {
		return additionalDataType;
	}

	public void setAdditionalDataType(AFAdditionalDataType additionalDataType) {
		if (additionalDataType != null)
			this.additionalDataType = additionalDataType;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	public Long getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(Long approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public void setFreeTransaction(boolean freeTransaction) {
		if (freeTransaction) {
			byte behaviorBitmap = additionalData == null || additionalData.length() == 0 ? 0 : additionalData.getBytes()[0];
			behaviorBitmap |= 1;
			additionalData = new String(new byte[] {behaviorBitmap});
			if (additionalDataType == AFAdditionalDataType.NO_ADDITIONAL_DATA)
				additionalDataType = AFAdditionalDataType.BEHAVIOR_BITMAP;
		}
	}

	public void setNoConvenienceFee(boolean noConvenienceFee) {
		if (noConvenienceFee) {
			byte behaviorBitmap = additionalData == null || additionalData.length() == 0 ? 0 : additionalData.getBytes()[0];
			behaviorBitmap |= 2;
			additionalData = new String(new byte[] {behaviorBitmap});
			if (additionalDataType == AFAdditionalDataType.NO_ADDITIONAL_DATA)
				additionalDataType = AFAdditionalDataType.BEHAVIOR_BITMAP;
		}
	}

	public void setAuthResultCd(AuthResultCode authResultCd) {
		setResponseCode(authResultCd.getAuthResponseCodeAF());
		if(authResultCd != AuthResultCode.PARTIAL)
			setApprovedAmount(null);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; responseCode=").append(getResponseCode())
			.append("; actionCode=").append(getActionCode())
			.append("; responseMessage=").append(getResponseMessage() == null ? "" : getResponseMessage().replace("\r", "\\r").replace("\n", "\\n"))
			.append("; additionalDataType=").append(getAdditionalDataType())
			.append("; additionalData=").append(getAdditionalDataType().getReadableAdditionalData(getAdditionalData()))			
			.append("; approvedAmount=").append(getApprovedAmount());
		sb.append("]");

		return sb.toString();
	}
}
