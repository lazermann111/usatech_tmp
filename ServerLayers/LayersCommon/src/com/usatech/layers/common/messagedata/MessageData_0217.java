package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.bean.ConvertUtils;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PaymentActionType;

/**
 *  Message Data for WS2_REPLENISH_ENCRYPTED_REQUEST - "Web Service - Replenish Encrypted Request - 0217h"
 */
public class MessageData_0217 extends WS2ReplenishMessageData {
	protected final WSEncryptingCardReader cardReader = new WSEncryptingCardReader();
	protected Long amount;
	protected String entryTypeString;
	protected long replenishCardId;
	protected long replenishConsumerId;

	public MessageData_0217() {
		super();
		cardReader.registerValidators(this);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_REPLENISH_ENCRYPTED_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getTranId());
		reply.putLong(getAmount());
		reply.putInt(getCardReaderTypeInt());
		reply.putInt(getDecryptedCardDataLen());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getEncryptedCardDataHex()) : getEncryptedCardDataHex(), charset);
		ProcessingUtils.writeLongString(reply, getKsnHex(), charset);
		ProcessingUtils.writeLongString(reply, getEntryTypeString(), charset);
		reply.putLong(getReplenishCardId());
		reply.putLong(getReplenishConsumerId());
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setTranId(data.getLong());
		setAmount(data.getLong());
		setCardReaderTypeInt(data.getInt());
		setDecryptedCardDataLen(data.getInt());
		setEncryptedCardDataHex(ProcessingUtils.readLongString(data, charset));
		setKsnHex(ProcessingUtils.readLongString(data, charset));
		setEntryTypeString(ProcessingUtils.readLongString(data, charset));
		setReplenishCardId(data.getLong());
		setReplenishConsumerId(data.getLong());
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.REPLENISHMENT;
	}

	public boolean isAuthOnly() {
		return ConvertUtils.getBooleanSafely(getAttribute("authOnly"), false);
	}

	public void setAuthOnly(boolean authOnly) {
		setAttribute("authOnly", authOnly ? "true" : null);
	}

	public CardReaderType getCardReaderType() {
		return cardReader.getCardReaderType();
	}

	public CardReader getCardReader() {
		return cardReader;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public int getCardReaderTypeInt() {
		return cardReader.getCardReaderTypeInt();
	}

	public void setCardReaderTypeInt(int cardReaderTypeInt) {
		cardReader.setCardReaderTypeInt(cardReaderTypeInt);
	}

	public int getDecryptedCardDataLen() {
		return cardReader.getDecryptedCardDataLen();
	}

	public void setDecryptedCardDataLen(int decryptedCardDataLen) {
		cardReader.setDecryptedCardDataLen(decryptedCardDataLen);
	}

	public String getEncryptedCardDataHex() {
		return cardReader.getEncryptedCardDataHex();
	}

	public void setEncryptedCardDataHex(String encryptedCardDataHex) {
		cardReader.setEncryptedCardDataHex(encryptedCardDataHex);
	}

	public String getKsnHex() {
		return cardReader.getKsnHex();
	}

	public void setKsnHex(String ksnHex) {
		cardReader.setKsnHex(ksnHex);
	}

	public String getEntryTypeString() {
		return entryTypeString;
	}

	public void setEntryTypeString(String entryTypeString) {
		this.entryTypeString = entryTypeString;
	}

	public long getReplenishCardId() {
		return replenishCardId;
	}

	public void setReplenishCardId(long replenishCardId) {
		this.replenishCardId = replenishCardId;
	}

	public long getReplenishConsumerId() {
		return replenishConsumerId;
	}

	public void setReplenishConsumerId(long replenishConsumerId) {
		this.replenishConsumerId = replenishConsumerId;
	}

	public MessageData_030C createResponse() {
		MessageData_030C response = new MessageData_030C();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", tranId=").append(getTranId())
			.append(", amount=").append(getAmount())
			.append(", cardReaderTypeInt=").append(getCardReaderTypeInt())
			.append(", decryptedCardDataLen=").append(getDecryptedCardDataLen())
			.append(", encryptedCardDataHex=<").append(getEncryptedCardDataHex() == null ? 0 : getEncryptedCardDataHex().length()).append(" bytes>")
			.append(", ksnHex=").append(getKsnHex())
			.append(", entryTypeString=").append(getEntryTypeString())
			.append(", replenishCardId=").append(getReplenishCardId())
			.append(", replenishConsumerId=").append(getReplenishConsumerId())
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
