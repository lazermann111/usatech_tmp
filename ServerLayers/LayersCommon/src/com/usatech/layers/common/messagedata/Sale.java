/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.util.List;
import java.util.TimeZone;

import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;

/**
 * @author Brian S. Krug
 *
 */
public interface Sale {

	public long getTransactionId();

	public String getDeviceTranCd();

	public long getBatchId();

	public SaleType getSaleType();

	/** Returns the sale start time (UTC) or null to use the current time
	 * @return
	 */
	public Long getSaleStartTime() ;

	/** Returns the sale time zone or null to use  the device's current timezone
	 * @return
	 */
	public TimeZone getSaleTimeZone() ;

	public SaleResult getSaleResult();

	public TranDeviceResultType getTransactionResult() ;

	public Number getSaleAmount();

	public Number getSaleTax();

	public ReceiptResult getReceiptResult();

	public List<? extends LineItem> getLineItems();
}