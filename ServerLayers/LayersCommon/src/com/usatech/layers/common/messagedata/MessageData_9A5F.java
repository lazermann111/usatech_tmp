package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.ESudsTopOrBottomType;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.lang.InvalidValueException;

/**
 *  Message Data for ESUDS_LOCAL_AUTH_BATCH - "eSuds Local Auth Batch - 9A5Fh"
 */
public class MessageData_9A5F extends ESudsSale implements LegacyLocalAuth {
	protected long transactionId;
	protected long saleStartSeconds;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected String creditCardMagstripe;
	public class LineItemData extends ESudsLineItem {
	protected int componentNumber;
	protected ESudsTopOrBottomType topOrBottom = ESudsTopOrBottomType.TOP;
	protected int item;
	protected int quantity;
	protected Integer price;
		protected final int index;
		protected LineItemData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setComponentNumber(ProcessingUtils.readByteInt(data));
		try {
			setTopOrBottom(ESudsTopOrBottomType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setItem(ProcessingUtils.readByteInt(data));
		setQuantity(ProcessingUtils.readByteInt(data));
		setPrice(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.writeByte(reply, getTopOrBottom().getValue());
		ProcessingUtils.writeByteInt(reply, getItem());
		ProcessingUtils.writeByteInt(reply, getQuantity());
		ProcessingUtils.writeShortInt(reply, getPrice());
		}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public ESudsTopOrBottomType getTopOrBottom() {
		return topOrBottom;
	}

	public void setTopOrBottom(ESudsTopOrBottomType topOrBottom) {
		this.topOrBottom = topOrBottom;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("componentNumber=").append(getComponentNumber())
			.append("; topOrBottom=").append(getTopOrBottom())
			.append("; item=").append(getItem())
			.append("; quantity=").append(getQuantity())
			.append("; price=").append(getPrice());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<LineItemData> lineItems = new ArrayList<LineItemData>();
	protected final List<LineItemData> lineItemsUnmod = Collections.unmodifiableList(lineItems);

	public MessageData_9A5F() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.ESUDS_LOCAL_AUTH_BATCH;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeLongInt(reply, getSaleStartSeconds());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getCreditCardMagstripe()) : getCreditCardMagstripe(), getCharset());
		ProcessingUtils.writeByteInt(reply, getLineItems().size());
		for(LineItemData lineItem : getLineItems())
			lineItem.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		setSaleStartSeconds(ProcessingUtils.readLongInt(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setCreditCardMagstripe(ProcessingUtils.readShortString(data, getCharset()));
		lineItems.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addLineItem().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getSaleStartSeconds() {
		return saleStartSeconds;
	}

	public void setSaleStartSeconds(long saleStartSeconds) {
		this.saleStartSeconds = saleStartSeconds;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public List<LineItemData> getLineItems() {
		return lineItemsUnmod;
	}

	public LineItemData addLineItem() {
		LineItemData lineItem = new LineItemData(lineItems.size());
		lineItems.add(lineItem);
		return lineItem;
	}


		public Long getSaleStartTime() {
			return getSaleStartSeconds() * 1000;
		}
		public TranDeviceResultType getTransactionResult() {
			if(lineItems.size() == 0) {
				return TranDeviceResultType.CANCELLED;
			} else {
				return TranDeviceResultType.SUCCESS_NO_PRINTER;
			}
		}
		@Override
		public SaleType getSaleType() {
			if(getCardType()==CardType.CASH) {
				return SaleType.CASH;
			} else {
				return SaleType.ACTUAL;
			}
		}
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; saleStartSeconds=").append(getSaleStartSeconds())
			.append("; cardType=").append(getCardType())
			.append("; creditCardMagstripe=").append(MessageResponseUtils.maskTrackData(getCreditCardMagstripe()))
			.append("; lineItems=").append(getLineItems());
		sb.append("]");

		return sb.toString();
	}
}
