package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for GX_POKE - "Gx Poke - 88h"
 */
public class MessageData_88 extends AbstractMessageData {
	protected final int GX_KEY_STARTING_ADDRESS = 144;
	protected final int GX_KEY_ENDING_ADDRESS = 161;
	
	protected byte memoryCode;
	protected long startingAddress;
	protected byte[] dataToPoke;

	public MessageData_88() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GX_POKE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getMemoryCode());
		ProcessingUtils.writeLongInt(reply, getStartingAddress());
		ProcessingUtils.writeBytesRemaining(reply, getDataToPoke());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setMemoryCode(ProcessingUtils.readByte(data));
		setStartingAddress(ProcessingUtils.readLongInt(data));
		setDataToPoke(ProcessingUtils.readBytesRemaining(data));
	}

	public byte getMemoryCode() {
		return memoryCode;
	}

	public void setMemoryCode(byte memoryCode) {
		this.memoryCode = memoryCode;
	}

	public long getStartingAddress() {
		return startingAddress;
	}

	public void setStartingAddress(long startingAddress) {
		this.startingAddress = startingAddress;
	}

	public byte[] getDataToPoke() {
		return dataToPoke;
	}

	public void setDataToPoke(byte[] dataToPoke) {
		this.dataToPoke = dataToPoke;
	}
	
	public String getDataToPokeMaskedHex() {
		if (dataToPoke == null || dataToPoke.length < 1)
			return "";
		long dataStartingAddress = startingAddress * 2;
		long dataEndingAddress = dataStartingAddress + dataToPoke.length;
		if (dataStartingAddress <= GX_KEY_ENDING_ADDRESS && dataEndingAddress >= GX_KEY_STARTING_ADDRESS) {
			StringBuilder dataToPokeMaskedHex = new StringBuilder();
			for (int i = 0; i < dataToPoke.length; i++) {
				if (i + dataStartingAddress >= GX_KEY_STARTING_ADDRESS && i + dataStartingAddress <= GX_KEY_ENDING_ADDRESS)
					dataToPokeMaskedHex.append("**");
				else
					dataToPokeMaskedHex.append(StringUtils.toHex(dataToPoke[i]));					
			}
			return dataToPokeMaskedHex.toString();
		} else
			return StringUtils.toHex(dataToPoke);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; memoryCode=").append(getMemoryCode() & 0xFF)
			.append("; startingAddress=").append(getStartingAddress())
			.append("; dataToPoke=").append(getDataToPokeMaskedHex());
		sb.append("]");

		return sb.toString();
	}
}
