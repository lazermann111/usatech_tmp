package com.usatech.layers.common.messagedata;

public interface SupportedCycle {
	public int getCycleType() ;
	public void setCycleType(int cycleType);
	/**
	 * in pennies 2 bytes
	 * @return
	 */
	public int getCyclePrice() ;
	public void setCyclePrice(int cyclePrice);
	/**
	 * in minutes
	 * @return
	 */
	public int getCycleTime() ;
	public void setCycleTime(int cycleTime);

}
