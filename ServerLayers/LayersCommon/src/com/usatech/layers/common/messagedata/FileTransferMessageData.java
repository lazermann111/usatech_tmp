/**
 *
 */
package com.usatech.layers.common.messagedata;

/**
 * @author Brian S. Krug
 *
 */
public interface FileTransferMessageData extends MessageData {

	public byte getGroup() ;

	public int getPacketNum();

	public byte[] getContent();

	public void setContent(byte[] content);

}