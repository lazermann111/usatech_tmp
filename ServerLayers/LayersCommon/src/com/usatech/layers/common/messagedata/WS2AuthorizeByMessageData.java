package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.PaymentActionType;

public abstract class WS2AuthorizeByMessageData extends WS2RequestMessageData implements AuthorizeMessageData, WSTran {
	protected long tranId;
	
	public WS2AuthorizeByMessageData() {
		super();
		addValidators(INVALID_TRAN_ID, INVALID_AMOUNT);
	}

	public String getValidationData() {
		return null;
	}

	public long getTransactionId() {
		return getTranId();
	}

	public void setTransactionId(long transactionId) {
		setTranId(transactionId);
	}

	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.PURCHASE;
	}

	public String getPin() {
		return null;
	}

	public long getTranId() {
		return tranId;
	}

	public void setTranId(long tranId) {
		this.tranId = tranId;
	}

	public CardReaderType getCardReaderType() {
		return null;
	}

	public CardReader getCardReader() {
		return null;
	}
}
