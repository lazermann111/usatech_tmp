package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AEAdditionalDataType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PaymentActionType;

/**
 *  Message Data for AUTH_REQUEST_3_2 - "Authorization Request 3.2 - AEh"
 */
public class MessageData_AE extends AbstractMessageData implements AuthorizeMessageData {
	public interface CardReaderTypeData extends Byteable, CardReader {
	}
	protected int messageVersion;
	protected long transactionId;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected Long amount;
	protected String validationData;
	public class GenericCardReader implements PlainCardReaderV4, CardReaderTypeData {
	protected String accountData1;
	protected String accountData2;
	protected String accountData3;
		protected GenericCardReader() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setAccountData1(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData2(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData3(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData1()) : getAccountData1(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData2()) : getAccountData2(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData3()) : getAccountData3(), getCharset());
		}

	public String getAccountData1() {
		return accountData1;
	}

	public void setAccountData1(String accountData1) {
		this.accountData1 = accountData1;
	}

	public String getAccountData2() {
		return accountData2;
	}

	public void setAccountData2(String accountData2) {
		this.accountData2 = accountData2;
	}

	public String getAccountData3() {
		return accountData3;
	}

	public void setAccountData3(String accountData3) {
		this.accountData3 = accountData3;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("accountData1=").append(MessageResponseUtils.maskTrackData(getAccountData1()))
			.append("; accountData2=").append(MessageResponseUtils.maskTrackData(getAccountData2()))
			.append("; accountData3=").append(MessageResponseUtils.maskTrackData(getAccountData3()));
			sb.append(']');
			return sb.toString();
		}
	}
	protected CardReaderType cardReaderType = CardReaderType.GENERIC;
	protected CardReaderTypeData cardReader;
	protected AEAdditionalDataType additionalDataType = AEAdditionalDataType.NO_ADDITIONAL_DATA;
	protected String additionalData;

	public MessageData_AE() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_REQUEST_3_2;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByteInt(reply, getMessageVersion());
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.writeLongInt(reply, getAmount());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getValidationData()) : getValidationData(), getCharset());
		ProcessingUtils.writeByte(reply, getCardReaderType().getValue());
		getCardReader().writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getAdditionalDataType().getValue());
		ProcessingUtils.writeShortString(reply, getAdditionalData(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setMessageVersion(ProcessingUtils.readByteInt(data));
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAmount(ProcessingUtils.readLongInt(data));
		setValidationData(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setCardReaderType(CardReaderType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		getCardReader().readData(data);
		try {
			setAdditionalDataType(AEAdditionalDataType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAdditionalData(ProcessingUtils.readShortString(data, getCharset()));
	}

	public int getMessageVersion() {
		return messageVersion;
	}

	public void setMessageVersion(int messageVersion) {
		this.messageVersion = messageVersion;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getValidationData() {
		return validationData;
	}

	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}

	public CardReaderType getCardReaderType() {
		return cardReaderType;
	}

	public CardReaderTypeData getCardReader() {
		return cardReader;
	}

	public void setCardReaderType(CardReaderType cardReaderType) {
		this.cardReaderType = cardReaderType;
		switch(cardReaderType) {
			case GENERIC: this.cardReader = new GenericCardReader(); break;
			default: throw new IllegalArgumentException("The CardReaderType '" + cardReaderType + "' is not supported");
		}
	}

	public AEAdditionalDataType getAdditionalDataType() {
		return additionalDataType;
	}

	public void setAdditionalDataType(AEAdditionalDataType additionalDataType) {
		this.additionalDataType = additionalDataType;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}


    public EntryType getEntryType() {
        return getCardType().getEntryType();
    }
    public String getPin() {
        return null;
    }
    public PaymentActionType getPaymentActionType() {
       return PaymentActionType.PURCHASE;
    }
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageVersion=").append(getMessageVersion())
			.append("; transactionId=").append(getTransactionId())
			.append("; cardType=").append(getCardType())
			.append("; amount=").append(getAmount())
			.append("; validationData=").append(MessageResponseUtils.maskFully(getValidationData()))
			.append("; cardReaderType=").append(getCardReaderType())
			.append("; cardReader=").append(getCardReader())
			.append("; additionalDataType=").append(getAdditionalDataType())
			.append("; additionalData=").append(getAdditionalDataType().getReadableAdditionalData(getAdditionalData()));
		sb.append("]");

		return sb.toString();
	}
}
