package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ESudsTopOrBottomType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.lang.InvalidValueException;

/**
 *  Message Data for WASHER_DRYER_LABELS - "Washer/Dryer Labels - 9A6Ah"
 */
public class MessageData_9A6A extends AbstractMessageData {
	public class LabelData {
	protected int portNumber;
	protected String portLabel;
	protected ESudsTopOrBottomType topOrBottom = ESudsTopOrBottomType.TOP;
		protected final int index;
		protected LabelData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPortNumber(ProcessingUtils.readByteInt(data));
		setPortLabel(ProcessingUtils.read3ByteString(data, getCharset()));
		try {
			setTopOrBottom(ESudsTopOrBottomType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getPortNumber());
		ProcessingUtils.write3ByteString(reply, getPortLabel(), getCharset());
		ProcessingUtils.writeByte(reply, getTopOrBottom().getValue());
		}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getPortLabel() {
		return portLabel;
	}

	public void setPortLabel(String portLabel) {
		this.portLabel = portLabel;
	}

	public ESudsTopOrBottomType getTopOrBottom() {
		return topOrBottom;
	}

	public void setTopOrBottom(ESudsTopOrBottomType topOrBottom) {
		this.topOrBottom = topOrBottom;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("portNumber=").append(getPortNumber())
			.append("; portLabel=").append(getPortLabel())
			.append("; topOrBottom=").append(getTopOrBottom());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<LabelData> labels = new ArrayList<LabelData>();
	protected final List<LabelData> labelsUnmod = Collections.unmodifiableList(labels);

	public MessageData_9A6A() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WASHER_DRYER_LABELS;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		for(LabelData label : getLabels())
			label.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		labels.clear();
		while(data.hasRemaining())
			addLabel().readData(data);
	}

	public List<LabelData> getLabels() {
		return labelsUnmod;
	}

	public LabelData addLabel() {
		LabelData label = new LabelData(labels.size());
		labels.add(label);
		return label;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; labels=").append(getLabels());
		sb.append("]");

		return sb.toString();
	}
}
