package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for FILE_XFER_2_0 - "File Transfer 2.0 - 7Eh"
 */
public class MessageData_7E extends AbstractMessageData implements FileTransferMessageData {
	protected byte group;
	protected int packetNum;
	protected byte[] content;

	public MessageData_7E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.FILE_XFER_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getGroup());
		ProcessingUtils.writeByteInt(reply, getPacketNum());
		ProcessingUtils.writeBytesRemaining(reply, maskSensitiveData ? MessageDataUtils.summarizeBytes(getContent()) : getContent());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setGroup(ProcessingUtils.readByte(data));
		setPacketNum(ProcessingUtils.readByteInt(data));
		setContent(ProcessingUtils.readBytesRemaining(data));
	}

	public byte getGroup() {
		return group;
	}

	public void setGroup(byte group) {
		this.group = group;
	}

	public int getPacketNum() {
		return packetNum;
	}

	public void setPacketNum(int packetNum) {
		this.packetNum = packetNum;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; group=").append(getGroup() & 0xFF)
			.append("; packetNum=").append(getPacketNum())
			.append("; content=").append(new String(MessageDataUtils.summarizeBytes(getContent()), ProcessingConstants.US_ASCII_CHARSET));
		sb.append("]");

		return sb.toString();
	}
}
