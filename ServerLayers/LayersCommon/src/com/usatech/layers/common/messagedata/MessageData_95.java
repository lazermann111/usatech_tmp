package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for CASH_SALE_DETAIL_ACK_2_0 - "Cash Sale Detail ACK 2.0 - 95h"
 */
public class MessageData_95 extends AbstractMessageData {
	protected long cashSaleBatchId;

	public MessageData_95() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.CASH_SALE_DETAIL_ACK_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getCashSaleBatchId());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setCashSaleBatchId(ProcessingUtils.readLongInt(data));
	}

	public long getCashSaleBatchId() {
		return cashSaleBatchId;
	}

	public void setCashSaleBatchId(long cashSaleBatchId) {
		this.cashSaleBatchId = cashSaleBatchId;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; cashSaleBatchId=").append(getCashSaleBatchId());
		sb.append("]");

		return sb.toString();
	}
}
