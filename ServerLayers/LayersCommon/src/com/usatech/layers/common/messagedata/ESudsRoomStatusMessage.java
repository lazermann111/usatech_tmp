package com.usatech.layers.common.messagedata;

import java.util.List;

public interface ESudsRoomStatusMessage {
	public boolean isFullRoomStatus();
	public int getStartingPortNumber();
	public List<? extends ESudsRoomStatusData> getRoomStatus();
}
