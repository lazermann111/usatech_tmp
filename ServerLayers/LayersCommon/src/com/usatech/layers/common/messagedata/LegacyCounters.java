package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.BCDInt;

public interface LegacyCounters {
	public BCDInt getTotalCurrencyTransactionCounter();
	public BCDInt getTotalCurrencyMoneyCounter();
	public BCDInt getTotalCashlessTransactionCounter();
	public BCDInt getTotalCashlessMoneyCounter();
	public BCDInt getTotalPasscardTransactionCounter() ;
	public BCDInt getTotalPasscardMoneyCounter();
	public BCDInt getTotalBytesTransmitted() ;
	public BCDInt getTotalAttemptedSession();
}
