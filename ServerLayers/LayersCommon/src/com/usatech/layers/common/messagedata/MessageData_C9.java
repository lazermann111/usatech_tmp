package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for FILE_XFER_4_1 - "File Transfer 4.1 - C9h"
 */
public class MessageData_C9 extends AbstractMessageData implements FileTransferMessageData {
	protected int packetNum;
	protected byte[] content;

	public MessageData_C9() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.FILE_XFER_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeShortInt(reply, getPacketNum());
		ProcessingUtils.writeLongBytes(reply, maskSensitiveData ? MessageDataUtils.summarizeBytes(getContent()) : getContent());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setPacketNum(ProcessingUtils.readShortInt(data));
		setContent(ProcessingUtils.readLongBytes(data));
	}

	public int getPacketNum() {
		return packetNum;
	}

	public void setPacketNum(int packetNum) {
		this.packetNum = packetNum;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}


	/**
	 * @see com.usatech.layers.common.messagedata.FileTransferMessageData#getGroup()
	 */
	@Override
	public byte getGroup() {
		return 0;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; packetNum=").append(getPacketNum())
			.append("; content=").append(new String(MessageDataUtils.summarizeBytes(getContent()), ProcessingConstants.US_ASCII_CHARSET));
		sb.append("]");

		return sb.toString();
	}
}
