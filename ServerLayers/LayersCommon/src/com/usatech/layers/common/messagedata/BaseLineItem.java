package com.usatech.layers.common.messagedata;
/**
 * All device except eSuds should have host position num as 0
 * @author yhe
 *
 */

public abstract class BaseLineItem implements LineItem {
	
	@Override
	public int getComponentPosition(){
		return 0;
	}
}
