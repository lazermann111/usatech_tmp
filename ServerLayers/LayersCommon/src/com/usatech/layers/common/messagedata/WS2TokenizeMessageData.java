package com.usatech.layers.common.messagedata;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import simple.text.StringUtils;

import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;

/**
 * Message Data for Tokenize Requests
 */
public abstract class WS2TokenizeMessageData extends WS2RequestMessageData implements AuthorizeMessageData, Sale {
	protected long tranId;
	protected String billingPostalCode;
	protected String billingAddress;
	protected static final Validator<WS2TokenizeMessageData> EMPTY_BILLING_POSTAL_CODE = new ConditionValidator<WS2TokenizeMessageData>("ws.message.empty-billing-postal-code", "Billing postal code is required") {
		@Override
		protected boolean isValid(WS2TokenizeMessageData value) {
			return !StringUtils.isBlank(value.getBillingPostalCode());
		}
	};

	public static interface ExpirationDateContainer {
		public String getExpirationDate();
	}

	protected static final Validator<ExpirationDateContainer> INVALID_EXP_DATE = new Validator<ExpirationDateContainer>() {
		public String validate(ExpirationDateContainer value, simple.translator.Translator translator) {
			if(StringUtils.isBlank(value.getExpirationDate()))
				return translator.translate("ws.message.empty-exp-date", "Expiration Date is required", value);
			long expDateMs = InteractionUtils.expDateToMillis(value.getExpirationDate());
			if(expDateMs == 0L)
				return translator.translate("ws.message.invalid-exp-date", "Invalid Expiration Date (must be YYMM)", value);
			if(expDateMs < System.currentTimeMillis())
				return translator.translate("ws.message.expired-exp-date", "Expiration Date " + value.getExpirationDate().substring(2) + '/' + value.getExpirationDate().substring(0, 2) + " is in the past", value);
			return null;
		}
	};

	protected static final Validator<WS2TokenizeMessageData> INVALID_TRAN_ID = new ConditionValidator<WS2TokenizeMessageData>("ws.message.invalid-tran-id", "Invalid tranId: {0.tranId}") {
		protected boolean isValid(WS2TokenizeMessageData value) {
			return value.getTranId() > 0;
		}
	};
	public WS2TokenizeMessageData() {
		super();
		addValidator(INVALID_TRAN_ID);
		addValidator(EMPTY_BILLING_POSTAL_CODE);
		if(this instanceof PlainCardReader)
			addValidator(EMPTY_CARD_DATA);
		if(this instanceof ExpirationDateContainer)
			addValidator(INVALID_EXP_DATE);
	}

	public long getTransactionId() {
		return getTranId();
	}

	public void setTransactionId(long transactionId) {
		setTranId(transactionId);
	}

	public EntryType getEntryType() {
		return EntryType.TOKEN;
	}

	public Number getAmount() {
		return 0;
	}

	public String getValidationData() {
		return null;
	}

	public String getPin() {
		return null;
	}

	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.TOKENIZE;
	}

	public String getDeviceTranCd() {
		return String.valueOf(getTranId());
	}

	public long getBatchId() {
		return 0;
	}

	public SaleType getSaleType() {
		return SaleType.ACTUAL;
	}

	public Long getSaleStartTime() {
		return null;
	}

	public TimeZone getSaleTimeZone() {
		return null;
	}

	public SaleResult getSaleResult() {
		return SaleResult.CANCELLED_BY_USER;
	}

	public TranDeviceResultType getTransactionResult() {
		return TranDeviceResultType.CANCELLED;
	}

	public Number getSaleAmount() {
		return BigDecimal.ZERO;
	}

	public Number getSaleTax() {
		return null;
	}

	public ReceiptResult getReceiptResult() {
		return ReceiptResult.UNAVAILABLE;
	}

	public List<? extends LineItem> getLineItems() {
		return Collections.emptyList();
	}

	public long getTranId() {
		return tranId;
	}

	public void setTranId(long tranId) {
		this.tranId = tranId;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public abstract MessageData_0310 createResponse();
}
