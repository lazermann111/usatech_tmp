package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.ProcessingUtils;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

/**
 *  Message Data for CASH_SALE_DETAIL_2_0 - "Cash Sale Detail 2.0 - 94h"
 */
public class MessageData_94 extends LegacySale {
	protected long transactionId;
	public class CashVend extends LegacyLineItem {
	protected long itemStartTime;
	protected Integer price;
	protected VariableByteLengthNumber positionNumber;
		protected CashVend() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setItemStartTime(ProcessingUtils.readBCDTimestamp(data));
		setPrice(ProcessingUtils.read3ByteInt(data));
		setPositionNumber(ProcessingUtils.readVarIntRemaining(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeBCDTimestamp(reply, getItemStartTime());
		ProcessingUtils.write3ByteInt(reply, getPrice());
		ProcessingUtils.writeVarIntRemaining(reply, getPositionNumber());
		}

	public long getItemStartTime() {
		return itemStartTime;
	}

	public void setItemStartTime(long itemStartTime) {
		this.itemStartTime = itemStartTime;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public VariableByteLengthNumber getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(VariableByteLengthNumber positionNumber) {
		this.positionNumber = positionNumber;
	}


        @Override
        protected int getPositionLength() {
            return getPositionNumber() == null ? 1 : getPositionNumber().getByteLength();
        }
        @Override
        public String getDescription() {
            return getPosition();
        }
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("itemStartTime=").append(getItemStartTime())
			.append("; price=").append(getPrice())
			.append("; positionNumber=").append(getPositionNumber());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final CashVend lineItem = new CashVend();

	public MessageData_94() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.CASH_SALE_DETAIL_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		getLineItem().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		getLineItem().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public CashVend getLineItem() {
		return lineItem;
	}


	public Number getSaleTax(){
		return BigDecimal.ZERO;
	}
	
	public Long getSaleStartTime() {
		return getLineItem().getItemStartTime();
	}
	
	public SaleType getSaleType() {
		return SaleType.CASH;
	}	
	
	public TranDeviceResultType getTransactionResult() {
		return TranDeviceResultType.SUCCESS_NO_PRINTER;
	}
	
	public List<? extends LineItem> getLineItems() {
		return Collections.singletonList(getLineItem());
	}
	public Number getSaleAmount() {
		return getLineItem().getPrice();
	}
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; lineItem=").append(getLineItem());
		sb.append("]");

		return sb.toString();
	}
}
