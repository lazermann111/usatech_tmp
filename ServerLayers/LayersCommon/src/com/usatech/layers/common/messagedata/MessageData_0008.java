/*
 * (C) 2011 USA Technologies
 */
package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.AlertSourceCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simple.lang.InvalidIntValueException;

import static com.usatech.layers.common.ProcessingUtils.*;

/**
 *  Message Data for OUTGOING_ALERT_MESSAGE - "Alert Message - 0008h"
 *  
 *  Contains a variable number of device alerts.
 *  
 *  @author phorsfield
 */
public class MessageData_0008 extends AbstractMessageData {
		
	protected String session;
	
	public class AlertData {
		protected final int index;

		protected AlertSourceCode source;
		protected long databaseId;
		protected String deviceName;
		protected int eventType;
		protected int component;
		protected long timeStampMs;
		protected String description;
		
		protected AlertData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer buffer) throws ParseException, BufferUnderflowException, InvalidIntValueException {
			setSource(AlertSourceCode.getByValue((int)ProcessingUtils.readByte(buffer)));
			setDatabaseId(readLongInt(buffer));
			setDeviceName(readShortString(buffer, getCharset()));
			setEventType(readShortInt(buffer));
			setComponent(readShortInt(buffer));
			setTimeStampMs(readUncheckedLong(buffer));
			setDescription(readLongString(buffer, getCharset()));			
		}

		public void writeData(ByteBuffer buffer, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
			writeByte(buffer,(byte)getSource().getValue());
			writeLongInt(buffer,getDatabaseId());
			writeShortString(buffer,getDeviceName(),getCharset());
			writeShortInt(buffer,getEventType());
			writeShortInt(buffer,getComponent());
			writeUncheckedLong(buffer, getTimeStampMs());
			writeLongString(buffer,getDescription(),getCharset());
			
		}
	
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("source=").append(getSource().getDescription())
			.append(",databaseId=").append(getDatabaseId())
			.append(",deviceName=").append(getDeviceName())
			.append(",eventType=").append(getEventType())
			.append(",component=").append(getComponent())
			.append(",timeStampMs=").append(getTimeStampMs())
			.append(",description=").append(getDescription())
			;
			sb.append(']');
			return sb.toString();
		}

		public AlertSourceCode getSource() {
			return source;
		}

		public void setSource(AlertSourceCode source) {
			this.source = source;
		}

		public long getDatabaseId() {
			return databaseId;
		}

		public void setDatabaseId(long sourceId) {
			this.databaseId = sourceId;
		}

		public String getDeviceName() {
			return deviceName;
		}

		public void setDeviceName(String deviceName) {
			this.deviceName = deviceName;
		}

		public int getEventType() {
			return eventType;
		}

		public void setEventType(int eventType) {
			this.eventType = eventType;
		}

		public int getComponent() {
			return component;
		}

		public void setComponent(int component) {
			this.component = component;
		}

		public long getTimeStampMs() {
			return timeStampMs;
		}

		public void setTimeStampMs(long timeStampMs) {
			this.timeStampMs = timeStampMs;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}
	protected List<AlertData> alerts = new ArrayList<AlertData>();

	public List<AlertData> getAlerts() {
		return alerts;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public MessageData_0008() {
		super();
		this.setDirection(MessageDirection.INTRA_SERVER);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.APPLAYER_ALERT_MESSAGE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeShortString(reply, session, getCharset());
		ProcessingUtils.writeByteInt(reply, getAlerts().size());
		for(AlertData queue : getAlerts())
			queue.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setSession(ProcessingUtils.readShortString(data, getCharset()));
		alerts.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
		{
			try {
				addAlert().readData(data);
			} catch(InvalidIntValueException e) {
				createParseException("Unexpected integer format in app layer alert message, alert #", i, e);
			}
		}
	}

	public AlertData addAlert() {
		AlertData alert = new AlertData(alerts.size());
		alerts.add(alert);
		return alert;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; alerts=").append(getAlerts());
		sb.append("]");

		return sb.toString();
	}
}
