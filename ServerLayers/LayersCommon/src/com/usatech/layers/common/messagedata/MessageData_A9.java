package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ExternalTransferProtocol;
import com.usatech.layers.common.CRCLegacy;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;

/**
 *  Message Data for SERVER_FILE_TRANSFER_REQUEST - "External Server to Client File Transfer Request - A9h"
 */
public class MessageData_A9 extends AbstractMessageData implements DeviceTypeSpecific {
	protected DeviceType deviceType = DeviceType.DEFAULT;
	protected ExternalTransferProtocol externalTransferProtocol = ExternalTransferProtocol.FTP;
	protected byte[] serverIpAddress;
	protected int serverPortNumber;
	protected String serverLoginName;
	protected String serverLoginPassword;
	protected String filePathAndName;
	protected byte fileFormat;
	protected byte fileEncapsulation;
	protected CRCLegacy crc;
	protected byte[] encryptionKey;
	protected long transferStartTime;
	protected long transferEndTime;
	protected byte numberOfRetries;
	protected byte retryInterval;
	protected String kioskClientPath;

	public MessageData_A9() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.SERVER_FILE_TRANSFER_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getExternalTransferProtocol().getValue());
		ProcessingUtils.writeIP(reply, getServerIpAddress());
		ProcessingUtils.writeShortInt(reply, getServerPortNumber());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getServerLoginName()) : getServerLoginName(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getServerLoginPassword()) : getServerLoginPassword(), getCharset());
		ProcessingUtils.writeShortString(reply, getFilePathAndName(), getCharset());
		ProcessingUtils.writeByte(reply, getFileFormat());
		ProcessingUtils.writeByte(reply, getFileEncapsulation());
		ProcessingUtils.writeCRCLegacy(reply, getCrc());
		ProcessingUtils.writeFileTransferEncryptionKey(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptionKey()) : getEncryptionKey());
		ProcessingUtils.writeLongInt(reply, getTransferStartTime());
		ProcessingUtils.writeLongInt(reply, getTransferEndTime());
		ProcessingUtils.writeByte(reply, getNumberOfRetries());
		ProcessingUtils.writeByte(reply, getRetryInterval());
		if(getDeviceType()==DeviceType.KIOSK)
		ProcessingUtils.writeShortString(reply, getKioskClientPath(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setExternalTransferProtocol(ExternalTransferProtocol.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setServerIpAddress(ProcessingUtils.readIP(data));
		setServerPortNumber(ProcessingUtils.readShortInt(data));
		setServerLoginName(ProcessingUtils.readShortString(data, getCharset()));
		setServerLoginPassword(ProcessingUtils.readShortString(data, getCharset()));
		setFilePathAndName(ProcessingUtils.readShortString(data, getCharset()));
		setFileFormat(ProcessingUtils.readByte(data));
		setFileEncapsulation(ProcessingUtils.readByte(data));
		try {
			setCrc(ProcessingUtils.readCRCLegacy(data));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setEncryptionKey(ProcessingUtils.readFileTransferEncryptionKey(data));
		setTransferStartTime(ProcessingUtils.readLongInt(data));
		setTransferEndTime(ProcessingUtils.readLongInt(data));
		setNumberOfRetries(ProcessingUtils.readByte(data));
		setRetryInterval(ProcessingUtils.readByte(data));
		if(getDeviceType()==DeviceType.KIOSK)
		setKioskClientPath(ProcessingUtils.readShortString(data, getCharset()));
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public ExternalTransferProtocol getExternalTransferProtocol() {
		return externalTransferProtocol;
	}

	public void setExternalTransferProtocol(ExternalTransferProtocol externalTransferProtocol) {
		this.externalTransferProtocol = externalTransferProtocol;
	}

	public byte[] getServerIpAddress() {
		return serverIpAddress;
	}

	public void setServerIpAddress(byte[] serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}

	public int getServerPortNumber() {
		return serverPortNumber;
	}

	public void setServerPortNumber(int serverPortNumber) {
		this.serverPortNumber = serverPortNumber;
	}

	public String getServerLoginName() {
		return serverLoginName;
	}

	public void setServerLoginName(String serverLoginName) {
		this.serverLoginName = serverLoginName;
	}

	public String getServerLoginPassword() {
		return serverLoginPassword;
	}

	public void setServerLoginPassword(String serverLoginPassword) {
		this.serverLoginPassword = serverLoginPassword;
	}

	public String getFilePathAndName() {
		return filePathAndName;
	}

	public void setFilePathAndName(String filePathAndName) {
		this.filePathAndName = filePathAndName;
	}

	public byte getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(byte fileFormat) {
		this.fileFormat = fileFormat;
	}

	public byte getFileEncapsulation() {
		return fileEncapsulation;
	}

	public void setFileEncapsulation(byte fileEncapsulation) {
		this.fileEncapsulation = fileEncapsulation;
	}

	public CRCLegacy getCrc() {
		return crc;
	}

	public void setCrc(CRCLegacy crc) {
		this.crc = crc;
	}

	public byte[] getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public long getTransferStartTime() {
		return transferStartTime;
	}

	public void setTransferStartTime(long transferStartTime) {
		this.transferStartTime = transferStartTime;
	}

	public long getTransferEndTime() {
		return transferEndTime;
	}

	public void setTransferEndTime(long transferEndTime) {
		this.transferEndTime = transferEndTime;
	}

	public byte getNumberOfRetries() {
		return numberOfRetries;
	}

	public void setNumberOfRetries(byte numberOfRetries) {
		this.numberOfRetries = numberOfRetries;
	}

	public byte getRetryInterval() {
		return retryInterval;
	}

	public void setRetryInterval(byte retryInterval) {
		this.retryInterval = retryInterval;
	}

	public String getKioskClientPath() {
		return kioskClientPath;
	}

	public void setKioskClientPath(String kioskClientPath) {
		this.kioskClientPath = kioskClientPath;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; externalTransferProtocol=").append(getExternalTransferProtocol())
			.append("; serverIpAddress=").append(new String(getServerIpAddress(), ProcessingConstants.US_ASCII_CHARSET))
			.append("; serverPortNumber=").append(getServerPortNumber())
			.append("; serverLoginName=").append(MessageResponseUtils.maskFully(getServerLoginName()))
			.append("; serverLoginPassword=").append(MessageResponseUtils.maskFully(getServerLoginPassword()))
			.append("; filePathAndName=").append(getFilePathAndName())
			.append("; fileFormat=").append(getFileFormat() & 0xFF)
			.append("; fileEncapsulation=").append(getFileEncapsulation() & 0xFF)
			.append("; crc=").append(getCrc())
			.append("; encryptionKey=").append(new String(MessageResponseUtils.maskFully(getEncryptionKey()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; transferStartTime=").append(getTransferStartTime())
			.append("; transferEndTime=").append(getTransferEndTime())
			.append("; numberOfRetries=").append(getNumberOfRetries() & 0xFF)
			.append("; retryInterval=").append(getRetryInterval() & 0xFF)
			.append("; kioskClientPath=").append(getKioskClientPath());
		sb.append("]");

		return sb.toString();
	}
}
