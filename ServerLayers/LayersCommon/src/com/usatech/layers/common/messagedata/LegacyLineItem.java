package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.SaleResult;

public abstract class LegacyLineItem extends BaseLineItem {

	protected abstract Number getPositionNumber() ;

	protected abstract int getPositionLength();

	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getPosition()
	 */
	@Override
	public String getPosition() {
		Number pn = getPositionNumber();
		int pl = getPositionLength();
		return pn == null || pl < 1 ? null : ProcessingUtils.decimalToHex(pn, Math.max(2, pl) * 2);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getComponentNumber()
	 */
	public int getComponentNumber() {
		return 1;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getItem()
	 */
	public int getItem() {
		return 200;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getSaleResult()
	 */
	public SaleResult getSaleResult() {
		return SaleResult.SUCCESS;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getQuantity()
	 */
	public int getQuantity() {
		return 1;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getDescription()
	 */
	public String getDescription() {
		return "ePort Vend";
	}
	/**
	 * @see com.usatech.layers.common.messagedata.LineItem#getDuration()
	 */
	public int getDuration() {
		return 0;
	}
}
