package com.usatech.layers.common.messagedata;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.InvalidIntValueException;

import com.usatech.layers.common.constants.ComponentType;
import com.usatech.layers.common.constants.WasherDryerType;

public abstract class ESudsComponent implements Component {
	
	public abstract Integer getFirmwareVersion();
	public abstract void setFirmwareVersion(Integer firmwareVersion);
	public abstract Integer getModelCode();
	public abstract void setModelCode(Integer modelCode);
	public abstract WasherDryerType getWasherDryerType();
	public abstract void setWasherDryerType(WasherDryerType washerDryerType);
	
	protected String manufacturer, label;
	
	public String getRevision(){
		return String.valueOf(getFirmwareVersion());
	}
	
	public void setRevision(String revision){
		try{
			if(!(revision==null||revision.equals(""))){
				setFirmwareVersion(ConvertUtils.getInt(revision));
			}
		}catch(ConvertException e){}
	}
	
	public String getModel(){
		return String.valueOf(getModelCode());
	}
	
	public void setModel(String model){
		try{
			if(!(model==null||model.equals(""))){
				setModelCode(ConvertUtils.getInt(model));
			}
		}catch(ConvertException e){}
	}
	

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getManufacturer() {
		return getWasherDryerType().getManufacturer().getValue();
	}

	@Override
	public void setComponentType(ComponentType componentType) {
		// not implemented
	}

	@Override
	public void setLabel(String label) {
		this.label=label;
	}

	@Override
	public void setManufacturer(String manufacturer) {
		this.manufacturer=manufacturer;
	}
	
	@Override
	public ComponentType getComponentType(){
		try{
			return ComponentType.getByValue(getWasherDryerType().getHostType());
		}catch(InvalidIntValueException e){}
		return null;
	}
}
