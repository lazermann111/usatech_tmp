/**
 * (C) 2011 USA Technologies
 */
package com.usatech.layers.common.messagedata;

/**
 * Message flow
 * Server: USA Technology Servers
 * Client: Devices in the field
 * 
 * @author Brian S. Krug
 *
 */
public enum MessageDirection {
	SERVER_TO_CLIENT,
	CLIENT_TO_SERVER,
	INTRA_SERVER
}
