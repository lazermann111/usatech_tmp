package com.usatech.layers.common.messagedata;


public abstract class NetBatchLineItem extends LegacyLineItem {
	@Override
	public String getDescription() {
		return getPosition();
	}
	public Number getPrice() {
		if(getReportedPrice() == 0x00FFFFFF)
			return null;
		return getReportedPrice();
	}
	public abstract Number getPositionNumber() ;

	public abstract int getReportedPrice() ;
}
