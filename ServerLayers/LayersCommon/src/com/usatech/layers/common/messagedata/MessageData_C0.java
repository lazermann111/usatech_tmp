package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;

/**
 *  Message Data for INITIALIZATION_4_1 - "Initialization 4.1 - C0h"
 */
public class MessageData_C0 extends AbstractMessageData implements InitMessageData {
	protected InitializationReason reasonCode = InitializationReason.NEW_DEVICE;
	protected long protocolComplianceRevision;
	protected int propertyListVersion;
	protected DeviceType deviceType = DeviceType.DEFAULT;
	protected String deviceFirmwareVersion;
	protected String deviceSerialNum;
	protected String deviceInfo;
	protected String terminalInfo;

	public MessageData_C0() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.INITIALIZATION_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getReasonCode().getValue());
		ProcessingUtils.writeLongInt(reply, getProtocolComplianceRevision());
		ProcessingUtils.writeShortInt(reply, getPropertyListVersion());
		ProcessingUtils.writeByte(reply, getDeviceType().getValue());
		ProcessingUtils.writeShortString(reply, getDeviceFirmwareVersion(), getCharset());
		ProcessingUtils.writeShortString(reply, getDeviceSerialNum(), getCharset());
		ProcessingUtils.writeShortString(reply, getDeviceInfo(), getCharset());
		ProcessingUtils.writeShortString(reply, getTerminalInfo(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setReasonCode(InitializationReason.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setProtocolComplianceRevision(ProcessingUtils.readLongInt(data));
		setPropertyListVersion(ProcessingUtils.readShortInt(data));
		try {
			setDeviceType(DeviceType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setDeviceFirmwareVersion(ProcessingUtils.readShortString(data, getCharset()));
		setDeviceSerialNum(ProcessingUtils.readShortString(data, getCharset()));
		setDeviceInfo(ProcessingUtils.readShortString(data, getCharset()));
		setTerminalInfo(ProcessingUtils.readShortString(data, getCharset()));
	}

	public InitializationReason getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(InitializationReason reasonCode) {
		this.reasonCode = reasonCode;
	}

	public long getProtocolComplianceRevision() {
		return protocolComplianceRevision;
	}

	public void setProtocolComplianceRevision(long protocolComplianceRevision) {
		this.protocolComplianceRevision = protocolComplianceRevision;
	}

	public int getPropertyListVersion() {
		return propertyListVersion;
	}

	public void setPropertyListVersion(int propertyListVersion) {
		this.propertyListVersion = propertyListVersion;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceFirmwareVersion() {
		return deviceFirmwareVersion;
	}

	public void setDeviceFirmwareVersion(String deviceFirmwareVersion) {
		this.deviceFirmwareVersion = deviceFirmwareVersion;
	}

	public String getDeviceSerialNum() {
		return deviceSerialNum;
	}

	public void setDeviceSerialNum(String deviceSerialNum) {
		this.deviceSerialNum = deviceSerialNum;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getTerminalInfo() {
		return terminalInfo;
	}

	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; reasonCode=").append(getReasonCode())
			.append("; protocolComplianceRevision=").append(getProtocolComplianceRevision())
			.append("; propertyListVersion=").append(getPropertyListVersion())
			.append("; deviceType=").append(getDeviceType())
			.append("; deviceFirmwareVersion=").append(getDeviceFirmwareVersion())
			.append("; deviceSerialNum=").append(getDeviceSerialNum())
			.append("; deviceInfo=").append(getDeviceInfo())
			.append("; terminalInfo=").append(getTerminalInfo());
		sb.append("]");

		return sb.toString();
	}
}
