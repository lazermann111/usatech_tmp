package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  Message Data for APPLAYER_HEALTH_CHECK_RESPONSE - "App Layer Health Check Response - 0106h"
 */
public class MessageData_0106 extends AbstractMessageData {
	protected int checked;
	protected int success;
	public class ErrorData {
	protected String text;
		protected final int index;
		protected ErrorData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setText(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortString(reply, getText(), getCharset());
		}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("text=").append(getText());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<ErrorData> errors = new ArrayList<ErrorData>();
	protected final List<ErrorData> errorsUnmod = Collections.unmodifiableList(errors);

	public MessageData_0106() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.APPLAYER_HEALTH_CHECK_RESPONSE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeShortInt(reply, getChecked());
		ProcessingUtils.writeShortInt(reply, getSuccess());
		ProcessingUtils.writeByteInt(reply, getErrors().size());
		for(ErrorData error : getErrors())
			error.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setChecked(ProcessingUtils.readShortInt(data));
		setSuccess(ProcessingUtils.readShortInt(data));
		errors.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addError().readData(data);
	}

	public int getChecked() {
		return checked;
	}

	public void setChecked(int checked) {
		this.checked = checked;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public List<ErrorData> getErrors() {
		return errorsUnmod;
	}

	public ErrorData addError() {
		ErrorData error = new ErrorData(errors.size());
		errors.add(error);
		return error;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; checked=").append(getChecked())
			.append("; success=").append(getSuccess())
			.append("; errors=").append(getErrors());
		sb.append("]");

		return sb.toString();
	}
}
