package com.usatech.layers.common.messagedata;



public interface WSSale extends Sale {
	public WSSaleDetail getSaleDetail();
}
