/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * @author Brian S. Krug
 *
 */
public class ByteArrayContent implements Content {
	protected byte[] array;
	protected boolean truncateOnSensitive;
	protected int length = 0;

	public ByteArrayContent(boolean truncateOnSensitive) {
		this();
		this.truncateOnSensitive = truncateOnSensitive;
	}

	/**
	 *
	 */
	public ByteArrayContent() {
	}

	public void readBytes(ByteBuffer data, int length) throws IOException {
		if(data.remaining() < length) {
			if(isTruncateOnSensitive())
				array = null;
			else
				throw new BufferUnderflowException();
		} else {
			array = new byte[length];
			data.get(array, this.length, length);
		}
		this.length = length;
	}

	public void readBytes(InputStream in, int length) throws IOException {
		if(isTruncateOnSensitive() && in.available() == 0) {
			//test in.read() since in.available() may not be accurate
			int r = in.read();
			if(r < 0) {
				array = null;
			} else {
				array = new byte[length];
				array[0] = (byte) r;
				int tot = 1;
				while(tot < length && (r=in.read(array, tot, length - tot)) >= 0)
					tot += r;
			}
		} else {
			array = new byte[length];
			int tot = 0;
			int r;
			while(tot < length && (r=in.read(array, tot, length - tot)) >= 0)
				tot += r;
			if(tot < length) {
				throw new EOFException("Could only read " + tot + " bytes. Needed to read " + length);
			}
		}
		this.length = length;
	}

	public void writeBytes(ByteBuffer reply, boolean maskSensitiveData) throws IOException {
		if(maskSensitiveData) {
			if(!isTruncateOnSensitive())
				for(int i = 0; i < length; i++)
					reply.put(MessageDataUtils.MASK_BYTE);
		} else if(array == null) {
			throw new IOException("Content was truncated on read");
		} else
			reply.put(array, 0, length);
	}

	public void writeBytes(OutputStream out, boolean maskSensitiveData) throws IOException {
		if(maskSensitiveData) {
			if(!isTruncateOnSensitive())
				for(int i = 0; i < length; i++)
					out.write(MessageDataUtils.MASK_BYTE);
		} else if(array == null) {
			throw new IOException("Content was truncated on read");
		} else
			out.write(array, 0, length);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.Content#getLength()
	 */
	@Override
	public int getLength() {
		return length;
	}
	public boolean isTruncateOnSensitive() {
		return truncateOnSensitive;
	}

	public void setTruncateOnSensitive(boolean truncateOnSensitive) {
		this.truncateOnSensitive = truncateOnSensitive;
	}
}
