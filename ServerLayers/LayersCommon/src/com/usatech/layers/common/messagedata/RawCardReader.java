package com.usatech.layers.common.messagedata;

public interface RawCardReader extends CardReader {

	public byte[] getRawData();

}