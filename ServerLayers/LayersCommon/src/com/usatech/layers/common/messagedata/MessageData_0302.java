package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.ec.ECDataHandler;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for WS_PROCESS_UPDATES_RESPONSE - "Web Service - Process Updates Response - 0302h"
 */
public class MessageData_0302 extends WSResponseMessageData {
	protected ECDataHandler fileContent = EMPTY_DATA_HANDLER;
	protected String fileName = "";
	protected long fileSize;
	protected int fileType = -1;

	public MessageData_0302() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS_PROCESS_UPDATES_RESPONSE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getFileName(), charset);
		reply.putLong(getFileSize());
		reply.putInt(getFileType());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getNewPassword()) : getNewPassword(), charset);
		ProcessingUtils.writeLongString(reply, getNewUsername(), charset);
		reply.putInt(getReturnCode());
		ProcessingUtils.writeLongString(reply, getReturnMessage(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setFileName(ProcessingUtils.readLongString(data, charset));
		setFileSize(data.getLong());
		setFileType(data.getInt());
		setNewPassword(ProcessingUtils.readLongString(data, charset));
		setNewUsername(ProcessingUtils.readLongString(data, charset));
		setReturnCode(data.getInt());
		setReturnMessage(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
	}

	public ECDataHandler getFileContent() {
		return fileContent;
	}

	public void setFileContent(ECDataHandler fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", fileName=").append(getFileName())
			.append(", fileSize=").append(getFileSize())
			.append(", fileType=").append(getFileType())
			.append(", newPassword=").append(MessageDataUtils.maskString(getNewPassword()))
			.append(", newUsername=").append(getNewUsername())
			.append(", returnCode=").append(getReturnCode())
			.append(", returnMessage=").append(getReturnMessage())
			.append(", serialNumber=").append(getSerialNumber());
		sb.append("]");

		return sb.toString();
	}
}
