package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

/**
 *  Message Data for INITIALIZATION_3_1 - "Initialization 3.1 - ADh"
 */
public class MessageData_AD extends AbstractMessageData implements InitMessageData {
	protected DeviceType deviceType = DeviceType.DEFAULT;
	protected String deviceSerialNum;
	protected String deviceInfo;
	protected String terminalInfo;

	public MessageData_AD() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.INITIALIZATION_3_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getDeviceType().getValue());
		ProcessingUtils.writeShortString(reply, getDeviceSerialNum(), getCharset());
		ProcessingUtils.writeLongString(reply, getDeviceInfo(), getCharset());
		ProcessingUtils.writeLongString(reply, getTerminalInfo(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setDeviceType(DeviceType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setDeviceSerialNum(ProcessingUtils.readShortString(data, getCharset()));
		setDeviceInfo(ProcessingUtils.readLongString(data, getCharset()));
		setTerminalInfo(ProcessingUtils.readLongString(data, getCharset()));
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceSerialNum() {
		return deviceSerialNum;
	}

	public void setDeviceSerialNum(String deviceSerialNum) {
		this.deviceSerialNum = deviceSerialNum;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getTerminalInfo() {
		return terminalInfo;
	}

	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}


        public int getPropertyListVersion() {
            return -1;
        }
        	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; deviceType=").append(getDeviceType())
			.append("; deviceSerialNum=").append(getDeviceSerialNum())
			.append("; deviceInfo=").append(getDeviceInfo())
			.append("; terminalInfo=").append(getTerminalInfo());
		sb.append("]");

		return sb.toString();
	}
}
