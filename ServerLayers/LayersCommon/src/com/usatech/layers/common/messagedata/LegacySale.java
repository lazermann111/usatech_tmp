package com.usatech.layers.common.messagedata;

import java.math.BigDecimal;
import java.util.TimeZone;

import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;

public abstract class LegacySale extends AbstractMessageData implements Sale {
	@Override
	public String getDeviceTranCd() {
		return String.valueOf(getTransactionId());
	}
	public ReceiptResult getReceiptResult() {
		return ReceiptResult.UNAVAILABLE;
	}

	public long getBatchId() {
		return 0;
	}

	public SaleResult getSaleResult() {
		return getTransactionResult().getSaleResult();
	}

	public Number getSaleTax() {
		return BigDecimal.ZERO;
	}

	public SaleType getSaleType() {
		return SaleType.ACTUAL;
	}

	public Long getSaleStartTime() {
		return null;
	}

	public TimeZone getSaleTimeZone() {
		return null;
	}
}
