/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

/**
 * @author Brian S. Krug
 *
 */
public interface Byteable {
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException;
    public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException;
}