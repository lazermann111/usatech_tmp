package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.DeviceType;

public interface InitMessageData extends MessageData {
	public DeviceType getDeviceType();
	public String getDeviceSerialNum();
	public String getDeviceInfo();
	public String getTerminalInfo();
	public long getMessageId();

	public int getPropertyListVersion();
}
