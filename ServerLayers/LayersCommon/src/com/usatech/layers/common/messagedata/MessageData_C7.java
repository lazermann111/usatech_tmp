package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.Calendar;

import simple.lang.InvalidIntValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for SHORT_FILE_XFER_4_1 - "Short File Transfer 4.1 - C7h"
 */
public class MessageData_C7 extends AbstractMessageData implements FileTransferMessageData {
	protected Calendar creationTime;
	protected long eventId;
	protected int filesRemaining;
	protected FileType fileType = FileType.DEX_FILE;
	protected String fileName;
	protected byte[] content;

	public MessageData_C7() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.SHORT_FILE_XFER_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeTimestamp(reply, getCreationTime());
		ProcessingUtils.writeLongInt(reply, getEventId());
		ProcessingUtils.writeByteInt(reply, getFilesRemaining());
		ProcessingUtils.writeShortInt(reply, getFileType().getValue());
		ProcessingUtils.writeShortString(reply, getFileName(), getCharset());
		ProcessingUtils.writeLongBytes(reply, maskSensitiveData ? MessageDataUtils.maskFileContent(getContent(), getFileType()) : getContent());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setCreationTime(ProcessingUtils.readTimestamp(data));
		setEventId(ProcessingUtils.readLongInt(data));
		setFilesRemaining(ProcessingUtils.readByteInt(data));
		try {
			setFileType(FileType.getByValue(ProcessingUtils.readShortInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setFileName(ProcessingUtils.readShortString(data, getCharset()));
		setContent(ProcessingUtils.readLongBytes(data));
	}

	public Calendar getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Calendar creationTime) {
		this.creationTime = creationTime;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public int getFilesRemaining() {
		return filesRemaining;
	}

	public void setFilesRemaining(int filesRemaining) {
		this.filesRemaining = filesRemaining;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}


	/**
	 * @see com.usatech.layers.common.messagedata.FileTransferMessageData#getGroup()
	 */
	@Override
	public byte getGroup() {
		return 0;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.FileTransferMessageData#getPacketNum()
	 */
	@Override
	public int getPacketNum() {
		return 0;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; creationTime=").append(MessageDataUtils.toString(getCreationTime()))
			.append("; eventId=").append(getEventId())
			.append("; filesRemaining=").append(getFilesRemaining())
			.append("; fileType=").append(getFileType())
			.append("; fileName=").append(getFileName())
			.append("; content=").append(MessageDataUtils.toStringFileContent(getContent(), getFileType()));
		sb.append("]");

		return sb.toString();
	}
}
