package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS2_PROCESS_UPDATES_REQUEST - "Web Service - Process Updates Request - 0219h"
 */
public class MessageData_0219 extends WS2RequestMessageData {
	protected int updateStatus;
	protected int protocolVersion;
	protected String appType;
	protected String appVersion;

	public MessageData_0219() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_PROCESS_UPDATES_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putInt(getUpdateStatus());
		reply.putInt(getProtocolVersion());
		ProcessingUtils.writeLongString(reply, getAppType(), charset);
		ProcessingUtils.writeLongString(reply, getAppVersion(), charset);
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setUpdateStatus(data.getInt());
		setProtocolVersion(data.getInt());
		setAppType(ProcessingUtils.readLongString(data, charset));
		setAppVersion(ProcessingUtils.readLongString(data, charset));
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public int getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(int updateStatus) {
		this.updateStatus = updateStatus;
	}

	public int getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public MessageData_030E createResponse() {
		MessageData_030E response = new MessageData_030E();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", updateStatus=").append(getUpdateStatus())
			.append(", protocolVersion=").append(getProtocolVersion())
			.append(", appType=").append(getAppType())
			.append(", appVersion=").append(getAppVersion())
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
