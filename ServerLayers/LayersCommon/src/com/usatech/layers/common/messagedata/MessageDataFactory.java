package com.usatech.layers.common.messagedata;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.lang.InvalidByteArrayException;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageType;

public class MessageDataFactory {
	private static final Log log = Log.getLog();
	protected static final Cache<MessageType, Class<? extends MessageData>, ClassNotFoundException> messageDataTypes = new LockSegmentCache<MessageType, Class<? extends MessageData>, ClassNotFoundException>() {
		/**
		 * @see simple.util.concurrent.LockSegmentCache#createValue(java.lang.Object, java.lang.Object[])
		 */
		@Override
		protected Class<? extends MessageData> createValue(MessageType key, Object... additionalInfo) throws ClassNotFoundException {
			String className = MessageData.class.getPackage().getName() + ".MessageData_" + (key.getHex().length() == 0 ? "Ping" : key.getHex());
			try {
				return Class.forName(className).asSubclass(MessageData.class);
			} catch(ClassCastException e) {
				throw new ClassNotFoundException("Class '" + className + "' does not implement " + MessageData.class.getName(), e);
			}
		}
	};

	public static MessageData readMessage(ByteBuffer data, MessageDirection direction) throws BufferUnderflowException {
		return readMessage(data, direction, null);
	}

	public static MessageData readMessage(ByteBuffer data, MessageDirection direction, DeviceType deviceType) throws BufferUnderflowException {
		byte messageNumber = data.get();
		MessageType messageType;
		MessageData messageData;
		try {
			messageType = MessageType.getByValue(data);
			messageData = createMessageData(messageType);
		} catch(InvalidByteValueException e) {
			log.warn("Invalid message type " + StringUtils.toHex(e.getValue()) + "; using UNKNOWN type", e);
			messageData = new MessageData_Unknown(new byte[] {e.getValue()});
		} catch(InvalidByteArrayException e) {
			log.warn("Invalid message type " + StringUtils.toHex(e.getValue()) + "; using UNKNOWN type", e);
			messageData = new MessageData_Unknown(e.getValue());
		}
		if(messageData != null) {
			messageData.setDirection(direction);
			messageData.setMessageNumber(messageNumber);
			if(deviceType != null && messageData instanceof DeviceTypeSpecific)
				((DeviceTypeSpecific)messageData).setDeviceType(deviceType);
			int pos = data.position();
			
			try {
				messageData.readData(data);
			} catch(BufferUnderflowException e) {
				log.warn("Not enough data provided for message " + messageData.getMessageType() + "; using UNKNOWN type", e);
				MessageData_Unknown mdu = new MessageData_Unknown(messageData.getMessageType().getValue());
				mdu.setMessageNumber(messageNumber);
				data.position(pos);
				mdu.readData(data);
				return mdu;
			} catch(ParseException e) {
				log.warn("Invalid data provided for message " + messageData.getMessageType() + "; using UNKNOWN type", e);
				MessageData_Unknown mdu = new MessageData_Unknown(messageData.getMessageType().getValue());
				mdu.setMessageNumber(messageNumber);
				data.position(pos);
				mdu.readData(data);
				return mdu;
			}
		}
		return messageData;
	}

	public static MessageData createMessageData(MessageType messageType) {
		Class<? extends MessageData> messageDataClass;
		try {
			messageDataClass = messageDataTypes.getOrCreate(messageType);
		} catch(ClassNotFoundException e) {
			log.warn("Could not find MessageData class for " + messageType, e);
			return null;
		}
		try {
			return messageDataClass.newInstance();
		} catch(InstantiationException e) {
			log.warn("Could not instantiate " + messageDataClass.getName(), e);
		} catch(IllegalAccessException e) {
			log.warn("Could not instantiate " + messageDataClass.getName(), e);
		}
		return null;
	}

	public static MessageData readInboundMessage(String inboundHex, DeviceType deviceType) throws BufferUnderflowException, IllegalArgumentException {
		return readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(inboundHex)), MessageDirection.CLIENT_TO_SERVER, deviceType);
	}

	public static MessageData readInboundMessage(String inboundHex) throws BufferUnderflowException, IllegalArgumentException {
		return readInboundMessage(inboundHex, null);
	}

	public static MessageData readOutboundMessage(String outboundHex, DeviceType deviceType) throws BufferUnderflowException, IllegalArgumentException {
		return readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(outboundHex)), MessageDirection.SERVER_TO_CLIENT, deviceType);
	}

	public static MessageData readOutboundMessage(String outboundHex) throws BufferUnderflowException, IllegalArgumentException {
		return readOutboundMessage(outboundHex, null);
	}

	public static String readInboundMessageSafely(String inboundHex, DeviceType deviceType) {
		try {
			MessageData md = readInboundMessage(inboundHex, deviceType);
			return md == null ? "<UNKNOWN MessageType '" + inboundHex.substring(2,4) + "'>" : md.toString();
		} catch(BufferUnderflowException e) {
			return "ERROR: Message Data is truncated at position " + inboundHex.length() / 2;
		} catch(IllegalArgumentException e) {
			return StringUtils.exceptionToString(e);
		}
	}

	public static String readInboundMessageSafely(String inboundHex) {
		return readInboundMessageSafely(inboundHex, null);
	}

	public static String readOutboundMessageSafely(String outboundHex, DeviceType deviceType) {
		try {
			MessageData md = readOutboundMessage(outboundHex, deviceType);
			return md == null ? "<UNKNOWN MessageType '" + outboundHex.substring(2,4) + "'>" : md.toString();
		} catch(BufferUnderflowException e) {
			return "ERROR: Message Data is truncated at position " + outboundHex.length() / 2;
		} catch(IllegalArgumentException e) {
			return StringUtils.exceptionToString(e);
		}
	}

	public static String readOutboundMessageSafely(String outboundHex) {
		return readOutboundMessageSafely(outboundHex, null);
	}

}
