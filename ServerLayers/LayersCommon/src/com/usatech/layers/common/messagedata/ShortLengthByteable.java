package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.ProcessingUtils;

public abstract class ShortLengthByteable implements Byteable {
	protected abstract void readContent(ByteBuffer data) throws ParseException, BufferUnderflowException;

	protected abstract void writeContent(ByteBuffer data, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException;

	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		int oldLimit = data.limit();
		int length = ProcessingUtils.readByteInt(data);
		if(oldLimit > data.position() + length)
			data.limit(data.position() + length);
		readContent(data);
		data.limit(oldLimit);
	}

	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		int position = reply.position();
		reply.position(position + 1);
		writeContent(reply, maskSensitiveData);
		int length = reply.position() - position - 1;
		if(length < 0x00 || length > 0xFF)
			throw new IllegalArgumentException("Content length must be between 0 and 255, not " + length);
		reply.put(position, (byte) length);
	}
}
