package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS2_GET_CARD_ID_PLAIN_REQUEST - "Web Service - Get Card Id Plain Request - 021Ch"
 */
public class MessageData_021C extends WS2GetCardMessageData implements PlainCardReader {
	protected String cardData;
	protected String entryTypeString;

	public MessageData_021C() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_GET_CARD_ID_PLAIN_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getCardData()) : getCardData(), charset);
		ProcessingUtils.writeLongString(reply, getEntryTypeString(), charset);
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setCardData(ProcessingUtils.readLongString(data, charset));
		setEntryTypeString(ProcessingUtils.readLongString(data, charset));
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public String getAccountData() {
		return getCardData();
	}

	public void setAccountData(String accountData) {
		setCardData(accountData);
	}

	public CardReaderType getCardReaderType() {
		return CardReaderType.GENERIC;
	}

	public CardReader getCardReader() {
		return this;
	}

	public String getCardData() {
		return cardData;
	}

	public void setCardData(String cardData) {
		this.cardData = cardData;
	}

	public String getEntryTypeString() {
		return entryTypeString;
	}

	public void setEntryTypeString(String entryTypeString) {
		this.entryTypeString = entryTypeString;
	}

	public MessageData_030F createResponse() {
		MessageData_030F response = new MessageData_030F();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", cardData=").append(MessageResponseUtils.maskTrackData(getCardData()))
			.append(", entryTypeString=").append(getEntryTypeString())
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
