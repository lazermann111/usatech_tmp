package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCodeA1;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for AUTH_RESPONSE_3_0 - "Authorization Response 3.0 - A1h"
 */
public class MessageData_A1 extends AbstractMessageData {
	protected long transactionId;
	protected AuthResponseCodeA1 responseCode = AuthResponseCodeA1.DENIED;
	protected Long approvedAmount;

	public MessageData_A1() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_RESPONSE_3_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
		ProcessingUtils.writeLongIntRemaining(reply, getApprovedAmount());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setResponseCode(AuthResponseCodeA1.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setApprovedAmount(ProcessingUtils.readLongIntRemaining(data));
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public AuthResponseCodeA1 getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(AuthResponseCodeA1 responseCode) {
		this.responseCode = responseCode;
	}

	public Long getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(Long approvedAmount) {
		this.approvedAmount = approvedAmount;
	}


	public void setAuthResultCd(AuthResultCode authResultCd) {
		setResponseCode(authResultCd.getAuthResponseCodeA1());
		if(authResultCd != AuthResultCode.PARTIAL)
			setApprovedAmount(null);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; responseCode=").append(getResponseCode())
			.append("; approvedAmount=").append(getApprovedAmount());
		sb.append("]");

		return sb.toString();
	}
}
