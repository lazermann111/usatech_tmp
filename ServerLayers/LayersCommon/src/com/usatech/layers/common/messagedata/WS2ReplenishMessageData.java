package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.TranDeviceResultType;

public abstract class WS2ReplenishMessageData extends WS2AuthorizeMessageData implements ReplenishMessageData {
	public WS2ReplenishMessageData() {
		super(false);
	}
	public long getBatchId() {
		return 0;
	}
	public TranDeviceResultType getTransactionResult() {
		return TranDeviceResultType.SUCCESS;
	}
	public ReceiptResult getReceiptResult() {
		return ReceiptResult.UNAVAILABLE;
	}
}
