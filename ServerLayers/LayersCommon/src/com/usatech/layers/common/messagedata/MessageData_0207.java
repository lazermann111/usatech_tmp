package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;
import javax.activation.DataHandler;

/**
 *  Message Data for WS_UPLOAD_FILE_REQUEST - "Web Service - Upload File Request - 0207h"
 */
public class MessageData_0207 extends WSRequestMessageData {
	protected String fileName;
	protected int fileType;
	protected long fileSize;
	protected DataHandler fileContent;

	public MessageData_0207() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS_UPLOAD_FILE_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		ProcessingUtils.writeLongString(reply, getFileName(), charset);
		reply.putInt(getFileType());
		reply.putLong(getFileSize());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setFileName(ProcessingUtils.readLongString(data, charset));
		setFileType(data.getInt());
		setFileSize(data.getLong());
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public DataHandler getFileContent() {
		return fileContent;
	}

	public void setFileContent(DataHandler fileContent) {
		this.fileContent = fileContent;
	}

	public MessageData_0300 createResponse() {
		MessageData_0300 response = new MessageData_0300();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", fileName=").append(getFileName())
			.append(", fileType=").append(getFileType())
			.append(", fileSize=").append(getFileSize());
		sb.append("]");

		return sb.toString();
	}
}
