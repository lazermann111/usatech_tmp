package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.Map;
import java.util.Set;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidIntValueException;

/**
 *  Message Data for GENERIC_REQUEST_4_1 - "Generic Request 4.1 - CAh"
 */
public class MessageData_CA extends AbstractMessageData {
	public interface RequestTypeData extends Byteable {
	}
	public class UpdateStatusRequest implements RequestTypeData {
	protected int statusCodeBitmap;
		protected UpdateStatusRequest() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setStatusCodeBitmap(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getStatusCodeBitmap());
		}

	public int getStatusCodeBitmap() {
		return statusCodeBitmap;
	}

	public void setStatusCodeBitmap(int statusCodeBitmap) {
		this.statusCodeBitmap = statusCodeBitmap;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("statusCodeBitmap=").append(getStatusCodeBitmap());
			sb.append(']');
			return sb.toString();
		}
	}
	public class FileTransferRequest implements RequestTypeData {
	protected FileType fileType = FileType.DEX_FILE;
	protected String fileName;
		protected FileTransferRequest() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		try {
			setFileType(FileType.getByValue(ProcessingUtils.readShortInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setFileName(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getFileType().getValue());
		ProcessingUtils.writeShortString(reply, getFileName(), getCharset());
		}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("fileType=").append(getFileType())
			.append("; fileName=").append(getFileName());
			sb.append(']');
			return sb.toString();
		}
	}
	public class PropertyListRequest implements RequestTypeData {
	protected Set<Integer> propertyIndexes;
		protected PropertyListRequest() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPropertyIndexes(ProcessingUtils.readPropertyIndexList(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writePropertyIndexList(reply, getPropertyIndexes(), getCharset());
		}

	public Set<Integer> getPropertyIndexes() {
		return propertyIndexes;
	}

	public void setPropertyIndexes(Set<Integer> propertyIndexes) {
		this.propertyIndexes = propertyIndexes;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("propertyIndexes=").append(getPropertyIndexes());
			sb.append(']');
			return sb.toString();
		}
	}
	public class ActivationRequest implements RequestTypeData {
	protected Map<Integer,String> propertyValues;
		protected ActivationRequest() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPropertyValues(ProcessingUtils.readPropertyValueList(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writePropertyValueList(reply, getPropertyValues(), getCharset());
		}

	public Map<Integer,String> getPropertyValues() {
		return propertyValues;
	}

	public void setPropertyValues(Map<Integer,String> propertyValues) {
		this.propertyValues = propertyValues;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("propertyValues=").append(getPropertyValues());
			sb.append(']');
			return sb.toString();
		}
	}
	protected GenericRequestType requestType = GenericRequestType.UPDATE_STATUS_REQUEST;
	protected RequestTypeData requestTypeData;

	public MessageData_CA() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GENERIC_REQUEST_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getRequestType().getValue());
		getRequestTypeData().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setRequestType(GenericRequestType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		getRequestTypeData().readData(data);
	}

	public GenericRequestType getRequestType() {
		return requestType;
	}

	public RequestTypeData getRequestTypeData() {
		return requestTypeData;
	}

	public void setRequestType(GenericRequestType requestType) {
		this.requestType = requestType;
		switch(requestType) {
			case UPDATE_STATUS_REQUEST: this.requestTypeData = new UpdateStatusRequest(); break;
			case FILE_TRANSFER_REQUEST: this.requestTypeData = new FileTransferRequest(); break;
			case PROPERTY_LIST_REQUEST: this.requestTypeData = new PropertyListRequest(); break;
			case ACTIVATION_REQUEST: this.requestTypeData = new ActivationRequest(); break;
			default: throw new IllegalArgumentException("The RequestType '" + requestType + "' is not supported");
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; requestType=").append(getRequestType())
			.append("; requestTypeData=").append(getRequestTypeData());
		sb.append("]");

		return sb.toString();
	}
}
