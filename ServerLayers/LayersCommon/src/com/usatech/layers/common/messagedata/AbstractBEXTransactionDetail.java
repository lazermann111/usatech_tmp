/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.util.List;

import simple.bean.ConvertException;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractBEXTransactionDetail {
	

	public abstract String getPayload() ;

	public List<? extends LineItem> getLineItems() throws ConvertException{
		return KioskLineItem.parseLineItems(getPayload());
	}
}
