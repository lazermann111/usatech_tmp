package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS2_REPLENISH_RESPONSE - "Web Service - Replenish Response - 030Ch"
 */
public class MessageData_030C extends WS2AuthorizeResponseMessageData {
	protected long replenishBalanceAmount;
	protected long approvedAmount;
	protected long balanceAmount;
	protected long cardId;
	protected String cardType = "";
	protected long consumerId;
	protected long newTranId;

	public MessageData_030C() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_REPLENISH_RESPONSE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		reply.putLong(getReplenishBalanceAmount());
		reply.putLong(getApprovedAmount());
		reply.putLong(getBalanceAmount());
		reply.putLong(getCardId());
		ProcessingUtils.writeLongString(reply, getCardType(), charset);
		reply.putLong(getConsumerId());
		reply.putInt(getActionCode());
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getNewPassword()) : getNewPassword(), charset);
		reply.putLong(getNewTranId());
		ProcessingUtils.writeLongString(reply, getNewUsername(), charset);
		reply.putInt(getReturnCode());
		ProcessingUtils.writeLongString(reply, getReturnMessage(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setReplenishBalanceAmount(data.getLong());
		setApprovedAmount(data.getLong());
		setBalanceAmount(data.getLong());
		setCardId(data.getLong());
		setCardType(ProcessingUtils.readLongString(data, charset));
		setConsumerId(data.getLong());
		setActionCode(data.getInt());
		setAttributes(ProcessingUtils.readLongString(data, charset));
		setNewPassword(ProcessingUtils.readLongString(data, charset));
		setNewTranId(data.getLong());
		setNewUsername(ProcessingUtils.readLongString(data, charset));
		setReturnCode(data.getInt());
		setReturnMessage(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
	}

	public long getReplenishBalanceAmount() {
		return replenishBalanceAmount;
	}

	public void setReplenishBalanceAmount(long replenishBalanceAmount) {
		this.replenishBalanceAmount = replenishBalanceAmount;
	}

	public long getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(long approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public long getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public long getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	public long getNewTranId() {
		return newTranId;
	}

	public void setNewTranId(long newTranId) {
		this.newTranId = newTranId;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", replenishBalanceAmount=").append(getReplenishBalanceAmount())
			.append(", approvedAmount=").append(getApprovedAmount())
			.append(", balanceAmount=").append(getBalanceAmount())
			.append(", cardId=").append(getCardId())
			.append(", cardType=").append(getCardType())
			.append(", consumerId=").append(getConsumerId())
			.append(", actionCode=").append(getActionCode())
			.append(", attributes=").append(getAttributes())
			.append(", newPassword=").append(MessageDataUtils.maskString(getNewPassword()))
			.append(", newTranId=").append(getNewTranId())
			.append(", newUsername=").append(getNewUsername())
			.append(", returnCode=").append(getReturnCode())
			.append(", returnMessage=").append(getReturnMessage())
			.append(", serialNumber=").append(getSerialNumber());
		sb.append("]");

		return sb.toString();
	}
}
