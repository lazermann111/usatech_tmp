package com.usatech.layers.common.messagedata;

import simple.text.StringUtils;

import com.usatech.layers.common.constants.AuthResultCode;

public abstract class WS2CardInfoResponseMessageData extends WS2ResponseMessageData {
	public void setAuthResultCd(AuthResultCode authResultCd) {
		setReturnCode(authResultCd.getCardInfoResponseCodeEC2());
		if(StringUtils.isBlank(getReturnMessage()))
			switch(authResultCd) {
				case APPROVED:
					setReturnMessage("Approved");
					break;
				case PARTIAL:
					setReturnMessage("Partially Approved");
					break;
				case DECLINED:
					setReturnMessage("Card not accepted");
					break;
				case DECLINED_PERMANENT:
					setReturnMessage("Card not accepted");
					break;
				case DECLINED_PAYMENT_METHOD:
					setReturnMessage("Card not accepted");
					break;
				case DECLINED_DEBIT:
					setReturnMessage("Card not accepted");
					break;
				case FAILED:
					setReturnMessage("Failed");
					break;
				case AVS_MISMATCH:
					setReturnMessage("Address Does Not Match");
					break;
			}
	}

	public void setOverrideResponseMessage(String overrideResponseMessage) {
		if(overrideResponseMessage != null)
			switch(getReturnCode()) {
				case 1:
					setReturnMessage(overrideResponseMessage);
			}
	}
}
