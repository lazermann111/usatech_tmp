package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidValueException;

/**
 *  Message Data for AUTH_REQUEST_2_0 - "Authorization Request 2.0 - 5Eh"
 */
public class MessageData_5E extends AbstractMessageData implements PlainCardReader, AuthorizeMessageData {
	protected long transactionId;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected Integer amount;
	protected String accountData;

	public MessageData_5E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_REQUEST_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.write3ByteInt(reply, getAmount());
		ProcessingUtils.writeString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData()) : getAccountData(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAmount(ProcessingUtils.read3ByteInt(data));
		setAccountData(ProcessingUtils.readString(data, data.remaining(), getCharset()));
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getAccountData() {
		return accountData;
	}

	public void setAccountData(String accountData) {
		this.accountData = accountData;
	}


	public EntryType getEntryType() {
		return getCardType().getEntryType();
	}
	public String getPin() {
		return null;
	}
	public String getValidationData() {
		return null;
	}
	public PlainCardReader getCardReader() {
	   return this;
	}
    public CardReaderType getCardReaderType() {
       return CardReaderType.GENERIC;
    }
	public PaymentActionType getPaymentActionType() {
       return PaymentActionType.PURCHASE;
    }
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; cardType=").append(getCardType())
			.append("; amount=").append(getAmount())
			.append("; accountData=").append(MessageResponseUtils.maskTrackData(getAccountData()));
		sb.append("]");

		return sb.toString();
	}
}
