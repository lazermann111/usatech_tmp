package com.usatech.layers.common.messagedata;

import java.util.TimeZone;

import simple.bean.ConvertUtils;

import com.usatech.layers.common.constants.SaleType;


public abstract class WSCashMessageData extends WSSaleMessageData {
	protected static final Validator<WSCashMessageData> INVALID_TRAN_UTC_TIME_MS = new ConditionValidator<WSCashMessageData>("ws.message.invalid-tran-utc-time-ms", "Invalid tranUTCTimeMs: {0.tranUTCTimeMs}") {
		@Override
		protected boolean isValid(WSCashMessageData value) {
			return value.getTranUTCTimeMs() > 0;
		}
	};
	protected long tranUTCTimeMs;
	protected int tranUTCOffsetMs;

	public WSCashMessageData() {
		super();
		addValidators(INVALID_TRAN_UTC_TIME_MS);
	}
	@Override
	public SaleType getSaleType() {
		return SaleType.CASH;
	}
	
	/**
	 * Returns the sale start time (UTC) or null to use the current time
	 * 
	 * @return
	 */
	public Long getSaleStartTime() {
		return getTranUTCTimeMs();
	}

	/**
	 * Returns the sale time zone or null to use the device's current timezone
	 * 
	 * @return
	 */
	public TimeZone getSaleTimeZone() {
		return ConvertUtils.createTimeZone(getTranUTCOffsetMs());
	}

	public long getTranUTCTimeMs() {
		return tranUTCTimeMs;
	}

	public void setTranUTCTimeMs(long tranUTCTimeMs) {
		this.tranUTCTimeMs = tranUTCTimeMs;
	}

	public int getTranUTCOffsetMs() {
		return tranUTCOffsetMs;
	}

	public void setTranUTCOffsetMs(int tranUTCOffsetMs) {
		this.tranUTCOffsetMs = tranUTCOffsetMs;
	}
}
