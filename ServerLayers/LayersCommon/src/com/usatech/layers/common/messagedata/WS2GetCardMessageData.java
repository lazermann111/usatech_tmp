package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;

import simple.lang.InvalidValueException;
import simple.text.StringUtils;

public abstract class WS2GetCardMessageData extends WS2RequestMessageData implements AuthorizeMessageData {	
	// 0 was causing issues with storing auths in database. Using % 1000000000 to avoid collisions with epoch seconds.
	protected final long transactionId = (System.currentTimeMillis() / 1000) % 1000000000;
	
	protected static final Validator<PlainCardReader> VALID_CARD_DATA = new ConditionValidator<PlainCardReader>("ws.message.invalid-card-data", "Invalid card data") {
		@Override
		protected boolean isValid(PlainCardReader value) {
			return MessageResponseUtils.isValidCardNumber(value.getAccountData());
		}
	};
	
	protected static final ExceptionValidator<WS2GetCardMessageData, InvalidValueException> INVALID_ENTRY_TYPE = new ExceptionValidator<WS2GetCardMessageData, InvalidValueException>("ws.message.invalid-entry-type", "Invalid entryTypeString: {0.entryTypeString}") {
		public void attempt(WS2GetCardMessageData value) throws InvalidValueException {
			/*if(!ALLOWED_ENTRY_TYPES.contains(value.getEntryTypeString()))
				throw new InvalidValueException("Type not allowed", value.getEntryTypeString());
				*/
			if(StringUtils.isBlank(value.getEntryTypeString()))
				throw new InvalidValueException("Type not allowed", value.getEntryTypeString());
			value.entryType = EntryType.getByValue(value.getEntryTypeString().charAt(0));
		}
	};

	protected EntryType entryType;

	public WS2GetCardMessageData() {
		this(true);
	}

	protected WS2GetCardMessageData(boolean validateAmount) {
		super();
		addValidator(INVALID_ENTRY_TYPE);
		if(this instanceof PlainCardReader)
			addValidators(EMPTY_CARD_DATA, VALID_CARD_DATA);		
	}

	public abstract CardReader getCardReader();

	public abstract CardReaderType getCardReaderType();
	
	public abstract String getEntryTypeString();

	@Override
	public String getPin() {
		return null;
	}

	@Override
	public Number getAmount() {
		return null;
	}

	public EntryType getEntryType() {
		if(entryType == null)
			try {
				INVALID_ENTRY_TYPE.attempt(this);
			} catch(InvalidValueException e) {
				return EntryType.UNSPECIFIED;
			}
		return entryType;
	}

	@Override
	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.BALANCE_CHECK;
	}

	@Override
	public String getValidationData() {
		return null;
	}

	@Override
	public long getTransactionId() {
		return transactionId;
	}

	@Override
	public void setTransactionId(long transactionId) {
		// Do nothing
	}

	// TODO: authResultCd
}
