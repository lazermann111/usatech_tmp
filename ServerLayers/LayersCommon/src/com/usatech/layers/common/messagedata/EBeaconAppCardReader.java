package com.usatech.layers.common.messagedata;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.text.ParseException;
import java.util.Collection;
import java.util.Collections;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.security.crypt.CryptUtils;
import simple.text.StringUtils;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.CRCType.Calculator;

public class EBeaconAppCardReader implements CardReader, Byteable {
	private static final Log log = Log.getLog();
	protected static final IvParameterSpec DATA_DECRYPTION_IV_SPEC = new IvParameterSpec(new byte[16]);
	protected static String rsaPadding = "OAEPPADDING"; // "OAEPWITHSHA-512ANDMGF1PADDING";
	protected long appMessageId;
	protected int appMessageFormatId;
	protected byte[] appMessagePayload;
	protected AppAuthData appAuthMessage;
	protected byte[] sessionKey;

	public AppAuthData getAppAuthMessage(Collection<String> keyAliases) throws ParseException, BufferUnderflowException {
		if(appAuthMessage == null) {
			try {
				parseMessageData(keyAliases);
			} catch(IOException e) {
				throw new BufferUnderflowException();
			} catch(IllegalArgumentException e) {
				throw createParseException("IllegalArument", e);
			} catch(GeneralSecurityException e) {
				throw createParseException("Could not decrypt app message", e);
			}
		}
		return appAuthMessage;
	}

	protected void parseMessageData(Collection<String> keyAliases) throws IllegalArgumentException, ParseException, BufferUnderflowException, IOException, GeneralSecurityException {
		byte[] appMessagePayload = getAppMessagePayload();
		if(appMessagePayload == null || appMessagePayload.length == 0)
			return;
		ByteBuffer dataBuffer;
		byte[] sessionKey = null;
		switch(getAppMessageFormatId()) {
			case 0xE0:
				Calculator calc = CRCType.CRC16Server.newCalculator();
				calc.update(appMessagePayload, 0, appMessagePayload.length - 2);
				byte[] calcCrc = calc.getValue();
				if(!ByteArrayUtils.equals(calcCrc, 0, appMessagePayload, appMessagePayload.length - 2, 2))
					throw new ParseException("Calculated CRC 0x" + StringUtils.toHex(calcCrc) + " does not match reported crc 0x" + StringUtils.toHex(appMessagePayload, appMessagePayload.length - 2, 2), appMessagePayload.length - 2);
				dataBuffer = ByteBuffer.wrap(appMessagePayload, 0, appMessagePayload.length - 2);
				break;
			case 0xE1:
				int keyLength = ByteArrayUtils.readUnsignedShort(appMessagePayload, 0);
				if(keyLength > 0) {
					if(keyLength > appMessagePayload.length - 2)
						throw new ParseException("Key Length " + keyLength + " is larger than remaining data size " + (appMessagePayload.length - 2), 2);
					// decrypt the key
					GeneralSecurityException exception = null;
					for(String keyAlias : keyAliases) {
						try {
							sessionKey = CryptUtils.decrypt("RSA/ECB/" + rsaPadding, keyAlias, appMessagePayload, 2, keyLength);
							break;
						} catch(GeneralSecurityException e) {
							if(exception == null)
								exception = e;
							log.info("Could not decrypt app session key with " + keyAlias);
						}
					}
					if(sessionKey == null) {
						if(exception != null)
							throw exception;
						throw new IllegalArgumentException("No key aliases were provided");
					}
					// decrypt the data
					Key keySpec = new SecretKeySpec(sessionKey, "AES");
					Cipher cipher = Cipher.getInstance("AES/CBC/NOPADDING");
					cipher.init(Cipher.DECRYPT_MODE, keySpec, DATA_DECRYPTION_IV_SPEC);
					byte[] decryptedData = cipher.doFinal(appMessagePayload, 2 + keyLength, appMessagePayload.length - keyLength - 2);
					// dont' worry about padding because we will just ignore if when we prepare the command
					dataBuffer = prepareCommand(decryptedData, 0, decryptedData.length);
				} else
					dataBuffer = prepareCommand(appMessagePayload, 2, appMessagePayload.length - 2);
				break;
			case 0xEF: // special internal format
				keyLength = ByteArrayUtils.readUnsignedByte(appMessagePayload, 0);
				sessionKey = new byte[keyLength];
				System.arraycopy(appMessagePayload, 1, sessionKey, 0, sessionKey.length);
				dataBuffer = ByteBuffer.wrap(appMessagePayload, 1 + sessionKey.length, appMessagePayload.length - sessionKey.length - 1);
				break;
			default:
				throw new ParseException("AppMessageFormat " + getAppMessageFormatId() + " is not supported", 0);
		}
		MessageData messageData = MessageDataFactory.readMessage(dataBuffer, MessageDirection.CLIENT_TO_SERVER, null);
		if(!(messageData instanceof AppAuthData))
			throw new ParseException("Expected an Authorization Request Command but instead got " + messageData, 0);
		this.appAuthMessage = (AppAuthData) messageData;
		this.sessionKey = sessionKey;
		if(this.appMessageFormatId == 0xE1)
			this.appMessageFormatId = 0xEF;
	}

	protected ByteBuffer prepareCommand(byte[] decryptedData, int offset, int length) throws ParseException, IOException {
		int cmdLen = ByteArrayUtils.readUnsignedShort(decryptedData, 4 + offset);
		if(cmdLen > length - 8)
			throw new ParseException("Command length " + cmdLen + " is greater than the remaining decrypted data length", 4 + offset);
		if(cmdLen < length - 24)
			throw new ParseException("Command padding is " + (length - cmdLen - 8) + " bytes which is more than expected", cmdLen + 8 + offset);
		Calculator calc = CRCType.CRC16Server.newCalculator();
		calc.update(decryptedData, 6 + offset, cmdLen);
		byte[] calcCrc = calc.getValue();
		if(!ByteArrayUtils.equals(calcCrc, 0, decryptedData, 6 + cmdLen + offset, 2))
			throw new ParseException("Calculated CRC 0x" + StringUtils.toHex(calcCrc) + " does not match reported crc 0x" + StringUtils.toHex(decryptedData, 6 + cmdLen + offset, 2), 6 + cmdLen + offset);
		return ByteBuffer.wrap(decryptedData, 6 + offset, cmdLen);
	}

	protected ParseException createParseException(String message, Exception cause) {
		ParseException pe = new ParseException(message, 0);
		pe.initCause(cause);
		return pe;
	}

	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setAppMessageId(ProcessingUtils.readLongInt(data));
		setAppMessageFormatId(ProcessingUtils.readByteInt(data));
		setAppMessagePayload(ProcessingUtils.readLongBytes(data));
	}

	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getAppMessageId());
		ProcessingUtils.writeByteInt(reply, getAppMessageFormatId());
		if(getAppMessageFormatId() == 0xEF && appAuthMessage != null) {
			int position = reply.position();
			ProcessingUtils.writeShortInt(reply, 0);
			ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskEncryptionKey(getSessionKey()) : getSessionKey());
			appAuthMessage.writeData(reply, maskSensitiveData);
			reply.putShort(position, (short) (reply.position() - position - 2));
		} else
			ProcessingUtils.writeLongBytes(reply, getAppMessagePayload());
	}

	public long getAppMessageId() {
		return appMessageId;
	}

	public void setAppMessageId(long appMessageId) {
		this.appMessageId = appMessageId;
	}

	public int getAppMessageFormatId() {
		return appMessageFormatId;
	}

	public void setAppMessageFormatId(int appMessageFormatId) {
		this.appMessageFormatId = appMessageFormatId;
	}

	public byte[] getAppMessagePayload() {
		return appMessagePayload;
	}

	public void setAppMessagePayload(byte[] appMessagePayload) {
		this.appMessagePayload = appMessagePayload;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[')
		.append("appMessageId=").append(getAppMessageId())
		.append("; appMessageFormatId=").append(getAppMessageFormatId())
		.append("; appMessagePayload=");
		if(getAppAuthMessage() != null) {
			sb.append(getAppAuthMessage());
			sb.append("; sessionKey=").append(new String(MessageDataUtils.summarizeBytes(getSessionKey()), ProcessingConstants.US_ASCII_CHARSET));
		} else
			sb.append(new String(MessageDataUtils.summarizeBytes(getAppMessagePayload()), ProcessingConstants.US_ASCII_CHARSET));
		sb.append(']');
		return sb.toString();
	}

	public byte[] getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(byte[] sessionKey) {
		this.sessionKey = sessionKey;
	}

	public AppAuthData getAppAuthMessage() {
		if(appAuthMessage == null && getAppMessageFormatId() != 0xE1) {
			try {
				parseMessageData(Collections.EMPTY_SET);
			} catch(IOException e) {
				// ignore
			} catch(IllegalArgumentException e) {
				// ignore
			} catch(GeneralSecurityException e) {
				// ignore
			} catch(BufferUnderflowException e) {
				// ignore
			} catch(ParseException e) {
				// ignore
			}
		}
		return appAuthMessage;
	}

	public void setAppAuthMessage(AppAuthData appAuthMessage) {
		this.appAuthMessage = appAuthMessage;
	}

	public static String getRsaPadding() {
		return rsaPadding;
	}

	public static void setRsaPadding(String rsaPadding) {
		EBeaconAppCardReader.rsaPadding = rsaPadding;
	}
}
