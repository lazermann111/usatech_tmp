package com.usatech.layers.common.messagedata;

import java.util.LinkedHashMap;
import java.util.Map;


/**
 *  
 */
public abstract class WS2RequestMessageData extends WSRequestMessageData {
	protected final Map<String, String> attributeMap = new LinkedHashMap<String, String>();

	public WS2RequestMessageData() {
		super();
	}
	
	public String getAttribute(String key) {
		return attributeMap.get(key);
	}

	public void setAttribute(String key, String value) {
		if(key == null)
			throw new IllegalArgumentException("Null Keys are not allowed");
		if(value == null)
			attributeMap.remove(key);
		else
			attributeMap.put(key, value);
	}
	public String getAttributes() {
		return MessageDataUtils.toAttributeString(attributeMap);
	}
	
	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	public void setAttributes(String attributes) {
		MessageDataUtils.toAttributeMap(attributes, attributeMap);
	}

	public abstract WS2ResponseMessageData createResponse();

}
