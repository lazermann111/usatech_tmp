package com.usatech.layers.common.messagedata;

import simple.text.StringUtils;
import simple.translator.Translator;

public abstract class WSFileUploadMessageData extends WSRequestMessageData {
	protected static final Validator<WSFileUploadMessageData> EMPTY_FILE_NAME = new ConditionValidator<WSFileUploadMessageData>("ws.message.empty-file-name", "Empty fileName") {
		protected boolean isValid(WSFileUploadMessageData value) {
			return !StringUtils.isBlank(value.getFileName());
		}
	};
	protected static final Validator<WSFileUploadMessageData> INVALID_FILE_TYPE = new ConditionValidator<WSFileUploadMessageData>("ws.message.invalid-file-type", "Invalid fileType: {0.fileType}") {
		protected boolean isValid(WSFileUploadMessageData value) {
			return ALLOWED_UPLOAD_FILE_TYPES.contains(value.getFileType());
		}
	};
	protected static final Validator<WSFileUploadMessageData> TOO_LONG_FILE_SIZE = new Validator<WSFileUploadMessageData>() {
		public String validate(WSFileUploadMessageData value, Translator translator) {
			if(value.getFileSize() > WSRequestMessageData.getMaxFileSize())
				return translator.translate("ws.message.too-long-file-size", "File is too big (maximum is {1}): {0}", value.getFileSize(), WSRequestMessageData.getMaxFileSize());
			return null;
		}
	};
	protected static final Validator<WSFileUploadMessageData> EMPTY_FILE_CONTENT = new ConditionValidator<WSFileUploadMessageData>("ws.message.empty-file-content", "Empty fileContent") {
		protected boolean isValid(WSFileUploadMessageData value) {
			return value.getFileContent() != null && value.getFileContent().length > 0;
		}
	};
	protected String fileName;
	protected int fileType;
	protected long fileSize;
	protected byte[] fileContent;

	public WSFileUploadMessageData() {
		super();
		addValidators(EMPTY_FILE_NAME, INVALID_FILE_TYPE, TOO_LONG_FILE_SIZE, EMPTY_FILE_CONTENT);
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}
}
