package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;

/**
 *  Message Data for CHARGE_REQUEST_4_1 - "Charge Request 4.1 - CCh"
 */
public class MessageData_CC extends AbstractMessageData implements Sale, AuthorizeMessageData {
	public interface CardReaderTypeData extends Byteable, CardReader {
	}
	protected long transactionId;
	protected long batchId;
	protected EntryType entryType = EntryType.BAR_CODE;
	protected SaleResult saleResult = SaleResult.SUCCESS;
	protected ReceiptResult receiptResult = ReceiptResult.PRINTED;
	protected BigDecimal amount;
	protected String validationData;
	public class GenericCardReader implements PlainCardReaderV4, CardReaderTypeData {
	protected String accountData1;
	protected String accountData2;
	protected String accountData3;
		protected GenericCardReader() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setAccountData1(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData2(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData3(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData1()) : getAccountData1(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData2()) : getAccountData2(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData3()) : getAccountData3(), getCharset());
		}

	public String getAccountData1() {
		return accountData1;
	}

	public void setAccountData1(String accountData1) {
		this.accountData1 = accountData1;
	}

	public String getAccountData2() {
		return accountData2;
	}

	public void setAccountData2(String accountData2) {
		this.accountData2 = accountData2;
	}

	public String getAccountData3() {
		return accountData3;
	}

	public void setAccountData3(String accountData3) {
		this.accountData3 = accountData3;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("accountData1=").append(MessageResponseUtils.maskTrackData(getAccountData1()))
			.append("; accountData2=").append(MessageResponseUtils.maskTrackData(getAccountData2()))
			.append("; accountData3=").append(MessageResponseUtils.maskTrackData(getAccountData3()));
			sb.append(']');
			return sb.toString();
		}
	}
	public class EncryptingCardReader implements EncryptingCardReaderV4, CardReaderTypeData {
	protected byte[] keySerialNum;
	protected int decryptedLength1;
	protected int decryptedLength2;
	protected int decryptedLength3;
	protected byte[] encryptedData1;
	protected byte[] encryptedData2;
	protected byte[] encryptedData3;
		protected EncryptingCardReader() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setKeySerialNum(ProcessingUtils.readShortBytes(data));
		setDecryptedLength1(ProcessingUtils.readByteInt(data));
		setDecryptedLength2(ProcessingUtils.readByteInt(data));
		setDecryptedLength3(ProcessingUtils.readByteInt(data));
		setEncryptedData1(ProcessingUtils.readShortBytes(data));
		setEncryptedData2(ProcessingUtils.readShortBytes(data));
		setEncryptedData3(ProcessingUtils.readShortBytes(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortBytes(reply, getKeySerialNum());
		ProcessingUtils.writeByteInt(reply, getDecryptedLength1());
		ProcessingUtils.writeByteInt(reply, getDecryptedLength2());
		ProcessingUtils.writeByteInt(reply, getDecryptedLength3());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData1()) : getEncryptedData1());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData2()) : getEncryptedData2());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData3()) : getEncryptedData3());
		}

	public byte[] getKeySerialNum() {
		return keySerialNum;
	}

	public void setKeySerialNum(byte[] keySerialNum) {
		this.keySerialNum = keySerialNum;
	}

	public int getDecryptedLength1() {
		return decryptedLength1;
	}

	public void setDecryptedLength1(int decryptedLength1) {
		this.decryptedLength1 = decryptedLength1;
	}

	public int getDecryptedLength2() {
		return decryptedLength2;
	}

	public void setDecryptedLength2(int decryptedLength2) {
		this.decryptedLength2 = decryptedLength2;
	}

	public int getDecryptedLength3() {
		return decryptedLength3;
	}

	public void setDecryptedLength3(int decryptedLength3) {
		this.decryptedLength3 = decryptedLength3;
	}

	public byte[] getEncryptedData1() {
		return encryptedData1;
	}

	public void setEncryptedData1(byte[] encryptedData1) {
		this.encryptedData1 = encryptedData1;
	}

	public byte[] getEncryptedData2() {
		return encryptedData2;
	}

	public void setEncryptedData2(byte[] encryptedData2) {
		this.encryptedData2 = encryptedData2;
	}

	public byte[] getEncryptedData3() {
		return encryptedData3;
	}

	public void setEncryptedData3(byte[] encryptedData3) {
		this.encryptedData3 = encryptedData3;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("keySerialNum=").append(new String(getKeySerialNum(), ProcessingConstants.US_ASCII_CHARSET))
			.append("; decryptedLength1=").append(getDecryptedLength1())
			.append("; decryptedLength2=").append(getDecryptedLength2())
			.append("; decryptedLength3=").append(getDecryptedLength3())
			.append("; encryptedData1=").append(new String(MessageResponseUtils.maskFully(getEncryptedData1()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; encryptedData2=").append(new String(MessageResponseUtils.maskFully(getEncryptedData2()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; encryptedData3=").append(new String(MessageResponseUtils.maskFully(getEncryptedData3()), ProcessingConstants.US_ASCII_CHARSET));
			sb.append(']');
			return sb.toString();
		}
	}
	protected CardReaderType cardReaderType = CardReaderType.GENERIC;
	protected CardReaderTypeData cardReader;
	public class Format0LineItem extends BaseLineItem implements LineItem {
	protected int componentNumber;
	protected int item;
	protected SaleResult saleResult = SaleResult.SUCCESS;
	protected int quantity;
	protected BigDecimal price;
	protected String description;
	protected int duration;
		protected final int index;
		protected Format0LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setComponentNumber(ProcessingUtils.readByteInt(data));
		setItem(ProcessingUtils.readShortInt(data));
		try {
			setSaleResult(SaleResult.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setQuantity(ProcessingUtils.readShortInt(data));
		setPrice(ProcessingUtils.readStringAmountNull(data));
		setDescription(ProcessingUtils.readShortString(data, getCharset()));
		setDuration(ProcessingUtils.readShortInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.writeShortInt(reply, getItem());
		ProcessingUtils.writeByte(reply, getSaleResult().getValue());
		ProcessingUtils.writeShortInt(reply, getQuantity());
		ProcessingUtils.writeStringAmount(reply, getPrice());
		ProcessingUtils.writeShortString(reply, getDescription(), getCharset());
		ProcessingUtils.writeShortInt(reply, getDuration());
		}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public SaleResult getSaleResult() {
		return saleResult;
	}

	public void setSaleResult(SaleResult saleResult) {
		this.saleResult = saleResult;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("componentNumber=").append(getComponentNumber())
			.append("; item=").append(getItem())
			.append("; saleResult=").append(getSaleResult())
			.append("; quantity=").append(getQuantity())
			.append("; price=").append(getPrice())
			.append("; description=").append(getDescription())
			.append("; duration=").append(getDuration());
			sb.append(']');
			return sb.toString();
		}

        /**
         * @see com.usatech.layers.common.messagedata.BaseLineItem#getPosition()
         */
        @Override
        public String getPosition() {
            return null;
        }
        
	}
	protected byte lineItemFormat;
	protected final List<LineItem> lineItems = new ArrayList<LineItem>();
	protected final List<LineItem> lineItemsUnmod = Collections.unmodifiableList(lineItems);

	public MessageData_CC() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.CHARGE_REQUEST_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeLongInt(reply, getBatchId());
		ProcessingUtils.writeByteInt(reply, getEntryType().getValue());
		ProcessingUtils.writeByte(reply, getSaleResult().getValue());
		ProcessingUtils.writeByteInt(reply, getReceiptResult().getValue());
		ProcessingUtils.writeStringAmount(reply, getAmount());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getValidationData()) : getValidationData(), getCharset());
		ProcessingUtils.writeByte(reply, getCardReaderType().getValue());
		getCardReader().writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getLineItemFormat());
		ProcessingUtils.writeByteInt(reply, (byte) getLineItems().size());
		for(LineItem lineItem : getLineItems())
			lineItem.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		setBatchId(ProcessingUtils.readLongInt(data));
		try {
			setEntryType(EntryType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setSaleResult(SaleResult.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setReceiptResult(ReceiptResult.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAmount(ProcessingUtils.readStringAmountNull(data));
		setValidationData(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setCardReaderType(CardReaderType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		getCardReader().readData(data);
		setLineItemFormat(ProcessingUtils.readByte(data));
		lineItems.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addLineItem().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public EntryType getEntryType() {
		return entryType;
	}

	public void setEntryType(EntryType entryType) {
		this.entryType = entryType;
	}

	public SaleResult getSaleResult() {
		return saleResult;
	}

	public void setSaleResult(SaleResult saleResult) {
		this.saleResult = saleResult;
	}

	public ReceiptResult getReceiptResult() {
		return receiptResult;
	}

	public void setReceiptResult(ReceiptResult receiptResult) {
		this.receiptResult = receiptResult;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getValidationData() {
		return validationData;
	}

	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}

	public CardReaderType getCardReaderType() {
		return cardReaderType;
	}

	public CardReaderTypeData getCardReader() {
		return cardReader;
	}

	public void setCardReaderType(CardReaderType cardReaderType) {
		this.cardReaderType = cardReaderType;
		switch(cardReaderType) {
			case GENERIC: this.cardReader = new GenericCardReader(); break;
			case MAGTEK_MAGNESAFE: case IDTECH_SECUREMAG: this.cardReader = new EncryptingCardReader(); break;
			default: throw new IllegalArgumentException("The CardReaderType '" + cardReaderType + "' is not supported");
		}
	}

	public byte getLineItemFormat() {
		return lineItemFormat;
	}

	public List<LineItem> getLineItems() {
		return lineItemsUnmod;
	}

	public LineItem addLineItem() {
		LineItem lineItem;
		switch(getLineItemFormat()) {
			case 0: lineItem = new Format0LineItem(lineItems.size()); break;
			default: throw new IllegalArgumentException("The LineItemFormat '" + getLineItemFormat() + "' is not supported");
		}
		lineItems.add(lineItem);
		return lineItem;
	}

	public void setLineItemFormat(byte lineItemFormat) {
		this.lineItemFormat = lineItemFormat;
	}


		public String getDeviceTranCd() {
		   return String.valueOf(getTransactionId());
		}
		public SaleType getSaleType() {
		   return SaleType.ACTUAL;
		}
		public Long getSaleStartTime() {
		    return null;
		}
		public TimeZone getSaleTimeZone() {
		    return null;
		}
		public TranDeviceResultType getTransactionResult() {
		    return getSaleResult().getTransactionResult();
		}
		public BigDecimal getSaleTax() {
		    return BigDecimal.ZERO;
		}
	    public String getPin() {
	        return null;
	    }
	    public Number getSaleAmount() {
	        return getAmount();
	    }
	    public PaymentActionType getPaymentActionType() {
	        return PaymentActionType.PURCHASE;
	    }
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; transactionId=").append(getTransactionId())
			.append("; batchId=").append(getBatchId())
			.append("; entryType=").append(getEntryType())
			.append("; saleResult=").append(getSaleResult())
			.append("; receiptResult=").append(getReceiptResult())
			.append("; amount=").append(getAmount())
			.append("; validationData=").append(MessageResponseUtils.maskFully(getValidationData()))
			.append("; cardReaderType=").append(getCardReaderType())
			.append("; cardReader=").append(getCardReader())
			.append("; lineItemFormat=").append(getLineItemFormat() & 0xFF)
			.append("; lineItems=").append(getLineItems());
		sb.append("]");

		return sb.toString();
	}
}
