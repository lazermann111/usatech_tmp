/**
 *
 */
package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;

/**
 * @author Brian S. Krug
 *
 */
public interface AuthorizeMessageData extends MessageData {

	public long getTransactionId();

	public void setTransactionId(long transactionId);

	public EntryType getEntryType();

	public Number getAmount();

	public String getValidationData();

	public String getPin() ;

	public PaymentActionType getPaymentActionType() ;
	
	public CardReader getCardReader();

	public CardReaderType getCardReaderType();
}