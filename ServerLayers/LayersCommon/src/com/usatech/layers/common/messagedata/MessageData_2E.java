package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;

/**
 *  Message Data for GENERIC_DATA_LOG - "Generic Data Log - 2Eh"
 */
public class MessageData_2E extends AbstractMessageData implements DeviceTypeSpecific {
	protected byte[] genericData;
	protected DeviceType deviceType = DeviceType.DEFAULT;

	public MessageData_2E() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GENERIC_DATA_LOG;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeBytesRemaining(reply, getGenericData());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setGenericData(ProcessingUtils.readBytesRemaining(data));
	}

	public byte[] getGenericData() {
		return genericData;
	}

	public void setGenericData(byte[] genericData) {
		this.genericData = genericData;
	}
	
	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; genericData=");
		if (deviceType == DeviceType.ESUDS) {
			Map<String,Object> params = new HashMap<String, Object>();
			try {
				ProcessingUtils.parseESudsGenericLog(params, getGenericData());
				sb.append(ConvertUtils.getStringSafely(params.get("genericData")))
					.append("; objectName=")
					.append(ConvertUtils.getStringSafely(params.get("objectName")));
			} catch (ServiceException e) {
				sb.append(e.getMessage());
			}
		} else
			sb.append(new String(getGenericData(), ProcessingConstants.US_ASCII_CHARSET));
		sb.append("]");

		return sb.toString();
	}
}
