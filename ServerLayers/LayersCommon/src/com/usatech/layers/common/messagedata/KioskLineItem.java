package com.usatech.layers.common.messagedata;

import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;

public class KioskLineItem extends LegacyLineItem {
	protected static final char FS = '|';
	protected static final String FORMAT_CODE = "A0";
	protected int tranLineItemTypeId=200;

	protected BigDecimal price;
	protected int quantity=1;
	protected String description;
	protected Integer duration = 0;

	public static void parseLineItems(String payload, List<? super KioskLineItem> lineItems) throws ConvertException {
		lineItems.clear();
		String[] payLoadData = StringUtils.split(payload, FS);
		// Should we check format code?
		// formatcode|lineItemTypeId|amount|quantity|description
		for(int i = 1; i + 3 < payLoadData.length; i = i + 4) {
			KioskLineItem lineItem = new KioskLineItem();
			lineItem.setItem(ConvertUtils.getInt(payLoadData[i]));
			lineItem.setPrice(ConvertUtils.convert(BigDecimal.class, payLoadData[i + 1]));
			lineItem.setQuantity(ConvertUtils.getInt(payLoadData[i + 2]));
			lineItem.setDescription(payLoadData[i + 3]);
			if(lineItem.getItem() == 302) // Usage Minutes
				lineItem.setDuration(lineItem.getQuantity() * 60);
			lineItems.add(lineItem);
		}
	}

	public static List<? extends LineItem> parseLineItems(String payload) throws ConvertException {
		List<KioskLineItem> lineItems = new ArrayList<KioskLineItem>();
		parseLineItems(payload, lineItems);
		return lineItems;
	}

	public static String formatLineItems(List<? extends LineItem> lineItems) {
		StringBuilder sb = new StringBuilder();
		sb.append(FORMAT_CODE);
		for(LineItem li : lineItems) {
			sb.append(FS).append(li.getItem()).append(FS);
			if(li.getPrice() != null)
				sb.append(li.getPrice());
			sb.append(FS).append(li.getQuantity()).append(FS).append(li.getDescription());
		}
		return sb.toString();
	}

	@Override
	protected Number getPositionNumber() {
		return null;
	}

	protected int getPositionLength() {
		return 0;
	}
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {}
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		StringBuilder sb = new StringBuilder();
		sb.append(FS).append(getItem()).append(FS);
		if(getPrice() != null)
			sb.append(getPrice());
		sb.append(getPrice()).append(FS).append(getQuantity()).append(FS).append(getDescription());
		ProcessingUtils.writeString(reply, sb, ProcessingConstants.US_ASCII_CHARSET);
	}

	public void setPrice(BigDecimal price) {
		this.price=price;

	}

	@Override
	public BigDecimal getPrice() {
		return price;
	}

	@Override
	public int getItem() {
		return tranLineItemTypeId;
	}

	public void setItem(int tranLineItemTypeId){
		this.tranLineItemTypeId=tranLineItemTypeId;
	}

	@Override
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity){
		this.quantity=quantity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
}
