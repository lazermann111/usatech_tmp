/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * @author Brian S. Krug
 *
 */
public interface Content {
	public void readBytes(ByteBuffer data, int length) throws IOException ;
	public void readBytes(InputStream in, int length) throws IOException ;
	public void writeBytes(ByteBuffer reply, boolean maskSensitiveData) throws IOException ;
	public void writeBytes(OutputStream out, boolean maskSensitiveData) throws IOException ;
	public int getLength() ;
}
