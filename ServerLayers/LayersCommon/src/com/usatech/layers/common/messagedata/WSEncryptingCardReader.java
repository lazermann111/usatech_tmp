package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.messagedata.WSRequestMessageData.ConditionValidator;
import com.usatech.layers.common.messagedata.WSRequestMessageData.ExceptionValidator;
import com.usatech.layers.common.messagedata.WSRequestMessageData.Validator;

import simple.io.ByteArrayUtils;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

public class WSEncryptingCardReader implements EncryptingCardReader {
	protected static abstract class EncryptingCardReaderValidator<E extends Exception> extends ExceptionValidator<AuthorizeMessageData, E> {
		public EncryptingCardReaderValidator(String messageKey, String messageDefault) {
			super(messageKey, messageDefault);
		}

		public void attempt(AuthorizeMessageData value) throws E {
			attempt((WSEncryptingCardReader) value.getCardReader());
		}

		protected abstract void attempt(WSEncryptingCardReader value) throws E;
	}

	protected static final Validator<AuthorizeMessageData> INVALID_DECRYPTED_CARD_DATA_LEN = new ConditionValidator<AuthorizeMessageData>("ws.message.invalid-decrypted-card-data-len", "Invalid decryptedCardDataLen: {0.decryptedLength}") {
		protected boolean isValid(AuthorizeMessageData value) {
			return isValid((EncryptingCardReader) value.getCardReader());
		}
		protected boolean isValid(EncryptingCardReader value) {
			return value.getDecryptedLength() > 0;
		}
	};
	protected static final EncryptingCardReaderValidator<IllegalArgumentException> INVALID_ENCRYPTED_CARD_DATA_HEX = new EncryptingCardReaderValidator<IllegalArgumentException>("ws.message.invalid-encrypted-card-data-hex", "Invalid encryptedCardDataHex: {0.encryptedCardDataHex}") {
		public void attempt(WSEncryptingCardReader value) throws IllegalArgumentException {
			if(StringUtils.isBlank(value.getEncryptedCardDataHex()))
				throw new IllegalArgumentException("encryptedCardDataHex is blank");
			value.encryptedData = ByteArrayUtils.fromHex(value.getEncryptedCardDataHex());
		}
	};
	protected static final EncryptingCardReaderValidator<IllegalArgumentException> INVALID_KSN_HEX = new EncryptingCardReaderValidator<IllegalArgumentException>("ws.message.invalid-ksn-hex", "Invalid ksnHex: {0.ksnHex}") {
		public void attempt(WSEncryptingCardReader value) throws IllegalArgumentException {
			if(StringUtils.isBlank(value.getKsnHex()))
				return;
			value.keySerialNum = ByteArrayUtils.fromHex(value.getKsnHex());
		}
	};
	protected static final EncryptingCardReaderValidator<InvalidByteValueException> INVALID_CARD_READER_TYPE = new EncryptingCardReaderValidator<InvalidByteValueException>("ws.message.invalid-card-reader-type", "Invalid cardReaderType: {0.cardReaderTypeInt}") {
		public void attempt(WSEncryptingCardReader value) throws InvalidByteValueException {
			if(!WSRequestMessageData.isCardReaderTypeAllowed(value.getCardReaderTypeInt()))
				throw new InvalidByteValueException("Type not allowed", (byte) value.getCardReaderTypeInt());
			value.cardReaderType = CardReaderType.getByValue((byte) value.getCardReaderTypeInt());
		}
	};

	protected String encryptedCardDataHex;
	protected String ksnHex;
	protected int cardReaderTypeInt;
	protected CardReaderType cardReaderType;
	protected byte[] keySerialNum;
	protected byte[] encryptedData;
	protected int decryptedCardDataLen;

	public void registerValidators(WSRequestMessageData parent) {
		parent.addValidators(INVALID_CARD_READER_TYPE, INVALID_DECRYPTED_CARD_DATA_LEN, INVALID_ENCRYPTED_CARD_DATA_HEX, INVALID_KSN_HEX);
	}

	public String getEncryptedCardDataHex() {
		return encryptedCardDataHex;
	}

	public void setEncryptedCardDataHex(String encryptedCardDataHex) {
		this.encryptedCardDataHex = encryptedCardDataHex;
	}

	public String getKsnHex() {
		return ksnHex;
	}

	public void setKsnHex(String ksnHex) {
		this.ksnHex = ksnHex;
	}

	public int getCardReaderTypeInt() {
		return cardReaderTypeInt;
	}

	public void setCardReaderTypeInt(int cardReaderTypeInt) {
		this.cardReaderTypeInt = cardReaderTypeInt;
	}

	public int getDecryptedLength() {
		return getDecryptedCardDataLen();
	}

	public int getDecryptedCardDataLen() {
		return decryptedCardDataLen;
	}

	public CardReaderType getCardReaderType() {
		if(cardReaderType == null)
			try {
				INVALID_CARD_READER_TYPE.attempt(this);
			} catch(InvalidByteValueException e) {
				return null;
			}
		return cardReaderType;
	}

	public byte[] getKeySerialNum() {
		if(keySerialNum == null)
			INVALID_KSN_HEX.attempt(this);
		return keySerialNum;
	}

	public byte[] getEncryptedData() {
		if(encryptedData == null)
			INVALID_ENCRYPTED_CARD_DATA_HEX.attempt(this);
		return encryptedData;
	}

	public void setDecryptedCardDataLen(int decryptedCardDataLen) {
		this.decryptedCardDataLen = decryptedCardDataLen;
	}
}
