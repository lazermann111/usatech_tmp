package com.usatech.layers.common.messagedata;

import com.usatech.ec.ECByteArrayDataSource;
import com.usatech.ec.ECDataHandler;
import com.usatech.ec.ECResponse;


public abstract class WSResponseMessageData extends WSMessageData {
	protected String newPassword = "";
	protected String newUsername = "";
	protected int returnCode = ECResponse.RES_FAILED;
	protected String returnMessage = "";
	private static final byte[] EMPTY_BYTES = new byte[0];
	public static final ECDataHandler EMPTY_DATA_HANDLER = new ECDataHandler(new ECByteArrayDataSource(EMPTY_BYTES, "application/octet-stream")) /* {
		private static final long serialVersionUID = -1784523891818377536L;

		@Override
		public byte[] getFileBytes() {
			return EMPTY_BYTES;
		}

		public void setFileBytes(byte[] fileBytes) {
			throw new UnsupportedOperationException("You may not modify the empty data handler");
		}
	}*/;

	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewUsername() {
		return newUsername;
	}
	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}
	public int getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
}
