package com.usatech.layers.common.messagedata;

import java.util.LinkedHashMap;
import java.util.Map;

import com.usatech.ec2.EC2ByteArrayDataSource;
import com.usatech.ec2.EC2DataHandler;


/**
 *  
 */
public abstract class WS2ResponseMessageData extends WSResponseMessageData {
	protected final Map<String, String> attributeMap = new LinkedHashMap<String, String>();
	protected int actionCode;
	private static final byte[] EMPTY_BYTES = new byte[0];
	public static final EC2DataHandler EMPTY_DATA_HANDLER = new EC2DataHandler(new EC2ByteArrayDataSource(EMPTY_BYTES, "application/octet-stream")) /* {
		private static final long serialVersionUID = -1784523891818377536L;

		@Override
		public byte[] getFileBytes() {
			return EMPTY_BYTES;
		}

		public void setFileBytes(byte[] fileBytes) {
			throw new UnsupportedOperationException("You may not modify the empty data handler");
		}
	}*/;
	public WS2ResponseMessageData() {
		super();
	}
	
	public String getAttribute(String key) {
		return attributeMap.get(key);
	}

	public void setAttribute(String key, String value) {
		if(key == null)
			throw new IllegalArgumentException("Null Keys are not allowed");
		if(value == null)
			attributeMap.remove(key);
		else
			attributeMap.put(key, value);
	}

	public String getAttributes() {
		return MessageDataUtils.toAttributeString(attributeMap);
	}

	public void setAttributes(String attributes) {
		MessageDataUtils.toAttributeMap(attributes, attributeMap);
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	public int getActionCode() {
		return actionCode;
	}

	public void setActionCode(int actionCode) {
		this.actionCode = actionCode;
	}
}
