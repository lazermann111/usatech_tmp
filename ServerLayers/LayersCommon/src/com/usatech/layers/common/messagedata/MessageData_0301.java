package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for WS_AUTH_RESPONSE - "Web Service - Auth Response - 0301h"
 */
public class MessageData_0301 extends WSAuthorizeResponseMessageData {
	protected long approvedAmount;

	public MessageData_0301() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS_AUTH_RESPONSE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		reply.putLong(getApprovedAmount());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getNewPassword()) : getNewPassword(), charset);
		ProcessingUtils.writeLongString(reply, getNewUsername(), charset);
		reply.putInt(getReturnCode());
		ProcessingUtils.writeLongString(reply, getReturnMessage(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setApprovedAmount(data.getLong());
		setNewPassword(ProcessingUtils.readLongString(data, charset));
		setNewUsername(ProcessingUtils.readLongString(data, charset));
		setReturnCode(data.getInt());
		setReturnMessage(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
	}

	public long getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(long approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", approvedAmount=").append(getApprovedAmount())
			.append(", newPassword=").append(MessageDataUtils.maskString(getNewPassword()))
			.append(", newUsername=").append(getNewUsername())
			.append(", returnCode=").append(getReturnCode())
			.append(", returnMessage=").append(getReturnMessage())
			.append(", serialNumber=").append(getSerialNumber());
		sb.append("]");

		return sb.toString();
	}
}
