package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.BCDInt;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for GX_COUNTERS - "Gx Counters - 86h"
 */
public class MessageData_86 extends AbstractMessageData implements LegacyCounters {
	protected BCDInt totalCurrencyTransactionCounter;
	protected BCDInt totalCurrencyMoneyCounter;
	protected BCDInt totalCashlessTransactionCounter;
	protected BCDInt totalCashlessMoneyCounter;
	protected BCDInt totalPasscardTransactionCounter;
	protected BCDInt totalPasscardMoneyCounter;
	protected BCDInt totalBytesTransmitted;
	protected BCDInt totalAttemptedSession;
	protected long reservedCountersSpace;

	public MessageData_86() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.GX_COUNTERS;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeBCDInt(reply, getTotalCurrencyTransactionCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalCurrencyMoneyCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalCashlessTransactionCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalCashlessMoneyCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalPasscardTransactionCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalPasscardMoneyCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalBytesTransmitted());
		ProcessingUtils.writeBCDInt(reply, getTotalAttemptedSession());
		ProcessingUtils.writeLongInt(reply, getReservedCountersSpace());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTotalCurrencyTransactionCounter(ProcessingUtils.readBCDInt(data));
		setTotalCurrencyMoneyCounter(ProcessingUtils.readBCDInt(data));
		setTotalCashlessTransactionCounter(ProcessingUtils.readBCDInt(data));
		setTotalCashlessMoneyCounter(ProcessingUtils.readBCDInt(data));
		setTotalPasscardTransactionCounter(ProcessingUtils.readBCDInt(data));
		setTotalPasscardMoneyCounter(ProcessingUtils.readBCDInt(data));
		setTotalBytesTransmitted(ProcessingUtils.readBCDInt(data));
		setTotalAttemptedSession(ProcessingUtils.readBCDInt(data));
		setReservedCountersSpace(ProcessingUtils.readLongInt(data));
	}

	public BCDInt getTotalCurrencyTransactionCounter() {
		return totalCurrencyTransactionCounter;
	}

	public void setTotalCurrencyTransactionCounter(BCDInt totalCurrencyTransactionCounter) {
		this.totalCurrencyTransactionCounter = totalCurrencyTransactionCounter;
	}

	public BCDInt getTotalCurrencyMoneyCounter() {
		return totalCurrencyMoneyCounter;
	}

	public void setTotalCurrencyMoneyCounter(BCDInt totalCurrencyMoneyCounter) {
		this.totalCurrencyMoneyCounter = totalCurrencyMoneyCounter;
	}

	public BCDInt getTotalCashlessTransactionCounter() {
		return totalCashlessTransactionCounter;
	}

	public void setTotalCashlessTransactionCounter(BCDInt totalCashlessTransactionCounter) {
		this.totalCashlessTransactionCounter = totalCashlessTransactionCounter;
	}

	public BCDInt getTotalCashlessMoneyCounter() {
		return totalCashlessMoneyCounter;
	}

	public void setTotalCashlessMoneyCounter(BCDInt totalCashlessMoneyCounter) {
		this.totalCashlessMoneyCounter = totalCashlessMoneyCounter;
	}

	public BCDInt getTotalPasscardTransactionCounter() {
		return totalPasscardTransactionCounter;
	}

	public void setTotalPasscardTransactionCounter(BCDInt totalPasscardTransactionCounter) {
		this.totalPasscardTransactionCounter = totalPasscardTransactionCounter;
	}

	public BCDInt getTotalPasscardMoneyCounter() {
		return totalPasscardMoneyCounter;
	}

	public void setTotalPasscardMoneyCounter(BCDInt totalPasscardMoneyCounter) {
		this.totalPasscardMoneyCounter = totalPasscardMoneyCounter;
	}

	public BCDInt getTotalBytesTransmitted() {
		return totalBytesTransmitted;
	}

	public void setTotalBytesTransmitted(BCDInt totalBytesTransmitted) {
		this.totalBytesTransmitted = totalBytesTransmitted;
	}

	public BCDInt getTotalAttemptedSession() {
		return totalAttemptedSession;
	}

	public void setTotalAttemptedSession(BCDInt totalAttemptedSession) {
		this.totalAttemptedSession = totalAttemptedSession;
	}

	public long getReservedCountersSpace() {
		return reservedCountersSpace;
	}

	public void setReservedCountersSpace(long reservedCountersSpace) {
		this.reservedCountersSpace = reservedCountersSpace;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; totalCurrencyTransactionCounter=").append(getTotalCurrencyTransactionCounter())
			.append("; totalCurrencyMoneyCounter=").append(getTotalCurrencyMoneyCounter())
			.append("; totalCashlessTransactionCounter=").append(getTotalCashlessTransactionCounter())
			.append("; totalCashlessMoneyCounter=").append(getTotalCashlessMoneyCounter())
			.append("; totalPasscardTransactionCounter=").append(getTotalPasscardTransactionCounter())
			.append("; totalPasscardMoneyCounter=").append(getTotalPasscardMoneyCounter())
			.append("; totalBytesTransmitted=").append(getTotalBytesTransmitted())
			.append("; totalAttemptedSession=").append(getTotalAttemptedSession())
			.append("; reservedCountersSpace=").append(getReservedCountersSpace());
		sb.append("]");

		return sb.toString();
	}
}
