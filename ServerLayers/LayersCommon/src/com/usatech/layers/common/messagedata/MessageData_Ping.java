package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
/**
 *
 */
public class MessageData_Ping extends AbstractMessageData {
	public MessageData_Ping(){
		super();
	}
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	@Override
	public MessageType getMessageType() {
		return MessageType.PING;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	@Override
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer, boolean)
	 */
	@Override
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(']');

		return sb.toString();
	}
}
