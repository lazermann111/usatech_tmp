package com.usatech.layers.common.messagedata;

public interface EncryptingCardReaderV4 extends CardReader {

	public byte[] getKeySerialNum();

	public int getDecryptedLength1();

	public int getDecryptedLength2();

	public int getDecryptedLength3();

	public byte[] getEncryptedData1();

	public byte[] getEncryptedData2();

	public byte[] getEncryptedData3();

}