package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidValueException;

/**
 *  Message Data for AUTH_REQUEST_3_0 - "Authorization Request 3.0 - A0h"
 */
public class MessageData_A0 extends AbstractMessageData implements PlainCardReader, AuthorizeMessageData {
	protected long transactionId;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected Long amount;
	protected String pin;
	protected String accountData;

	public MessageData_A0() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_REQUEST_3_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.writeLongInt(reply, getAmount());
		ProcessingUtils.writeShortString(reply, getPin(), getCharset());
		ProcessingUtils.writeString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData()) : getAccountData(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAmount(ProcessingUtils.readLongInt(data));
		setPin(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData(ProcessingUtils.readString(data, data.remaining(), getCharset()));
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getAccountData() {
		return accountData;
	}

	public void setAccountData(String accountData) {
		this.accountData = accountData;
	}


	public EntryType getEntryType() {
		return getCardType().getEntryType();
	}
	public String getValidationData() {
		return null;
	}
    public PlainCardReader getCardReader() {
       return this;
    }
    public CardReaderType getCardReaderType() {
       return CardReaderType.GENERIC;
    }
	public PaymentActionType getPaymentActionType() {
       return PaymentActionType.PURCHASE;
    }
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; cardType=").append(getCardType())
			.append("; amount=").append(getAmount())
			.append("; pin=").append(getPin())
			.append("; accountData=").append(MessageResponseUtils.maskTrackData(getAccountData()));
		sb.append("]");

		return sb.toString();
	}
}
