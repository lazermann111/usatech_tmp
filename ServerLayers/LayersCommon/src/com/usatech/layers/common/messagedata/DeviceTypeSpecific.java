/**
 *
 */
package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.DeviceType;

/**
 * @author Brian S. Krug
 *
 */
public interface DeviceTypeSpecific {
	public DeviceType getDeviceType() ;
	public void setDeviceType(DeviceType deviceType) ;
}
