package com.usatech.layers.common.messagedata;

import java.math.BigDecimal;
import java.util.List;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

import com.usatech.layers.common.constants.TranDeviceResultType;

public abstract class ESudsSale extends LegacySale {
	public abstract List<? extends ESudsLineItem> getLineItems() ;
	//public abstract BigDecimal getPrice();
	//public abstract void setPrice(BigDecimal price);
	public TranDeviceResultType getTransactionResult() {
		return getLineItems().isEmpty() ? TranDeviceResultType.CANCELLED : TranDeviceResultType.SUCCESS_NO_PRINTER;
	}
	public Number getSaleAmount() {
		BigDecimal saleAmount = BigDecimal.ZERO;
		for(LineItem li : getLineItems()) {
			BigDecimal price;
			try {
				price = ConvertUtils.convert(BigDecimal.class, li.getPrice());
			} catch(ConvertException e) {
				throw new IllegalStateException("Convert to BigDecimal failed for " + li.getPrice() + (li.getPrice() == null ? "" : " (" + li.getPrice().getClass().getName() + ")"));
			}
			if(price != null)
				saleAmount = saleAmount.add(price.multiply(BigDecimal.valueOf(li.getQuantity())));
		}
		return saleAmount;
	}
}
