package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import simple.lang.InvalidValueException;

/**
 *  Message Data for LOCAL_AUTH_BATCH_2_0 - "Local Authorization Batch 2.0 - 2Bh"
 */
public class MessageData_2B extends ConvenienceFeeLegacySale implements LegacyLocalAuth, DeviceTypeSpecific {
	protected DeviceType deviceType = DeviceType.DEFAULT;
	protected long transactionId;
	protected long saleStartSeconds;
	protected CardType cardType = CardType.CREDIT_SWIPE;
	protected Integer saleAmount;
	protected int convenienceFee;
	protected String creditCardMagstripe;
	protected TranDeviceResultType transactionResult = TranDeviceResultType.AUTH_FAILURE;
	public class NetBatch0LineItem extends NetBatchLineItem implements LineItem {
	protected Integer positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch0LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.readByteAsBCD(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeByteAsBCD(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 1;
		}
                
	}
	public class NetBatch1LineItem extends NetBatchLineItem implements LineItem {
	protected Integer positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch1LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.readShortInt(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 2;
		}
                
	}
	public class NetBatch2LineItem extends NetBatchLineItem implements LineItem {
	protected Integer positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch2LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.read3ByteInt(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.write3ByteInt(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 3;
		}
                
	}
	public class NetBatch3LineItem extends NetBatchLineItem implements LineItem {
	protected Long positionNumber;
	protected int reportedPrice;
		protected final int index;
		protected NetBatch3LineItem(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setPositionNumber(ProcessingUtils.readLongInt(data));
		setReportedPrice(ProcessingUtils.read3ByteInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getPositionNumber());
		ProcessingUtils.write3ByteInt(reply, getReportedPrice());
		}

	public Long getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Long positionNumber) {
		this.positionNumber = positionNumber;
	}

	public int getReportedPrice() {
		return reportedPrice;
	}

	public void setReportedPrice(int reportedPrice) {
		this.reportedPrice = reportedPrice;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("positionNumber=").append(getPositionNumber())
			.append("; reportedPrice=").append(getReportedPrice());
			sb.append(']');
			return sb.toString();
		}

		protected int getPositionLength() {
			return 4;
		}
                
	}
	protected byte vendByteLength;
	protected final List<LineItem> actualLineItems = new ArrayList<LineItem>();
	protected final List<LineItem> actualLineItemsUnmod = Collections.unmodifiableList(actualLineItems);

	public MessageData_2B() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.LOCAL_AUTH_BATCH_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeLongInt(reply, getSaleStartSeconds());
		ProcessingUtils.writeByteInt(reply, getCardType().getValue());
		ProcessingUtils.write3ByteInt(reply, getSaleAmount());
		ProcessingUtils.writeShortInt(reply, getConvenienceFee());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getCreditCardMagstripe()) : getCreditCardMagstripe(), getCharset());
		ProcessingUtils.writeByteInt(reply, getTransactionResult().getValue());
		ProcessingUtils.write2BitInt(reply, getVendByteLength());
		ProcessingUtils.writePrev6BitInt(reply, (byte) getActualLineItems().size());
		for(LineItem actualLineItem : getActualLineItems())
			actualLineItem.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		setSaleStartSeconds(ProcessingUtils.readLongInt(data));
		try {
			setCardType(CardType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setSaleAmount(ProcessingUtils.read3ByteInt(data));
		setConvenienceFee(ProcessingUtils.readShortInt(data));
		setCreditCardMagstripe(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setTransactionResult(TranDeviceResultType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setVendByteLength(ProcessingUtils.read2BitInt(data));
		actualLineItems.clear();
		for(int i = 0, n = ProcessingUtils.readPrev6BitInt(data); i < n; i++)
			addActualLineItem().readData(data);
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getSaleStartSeconds() {
		return saleStartSeconds;
	}

	public void setSaleStartSeconds(long saleStartSeconds) {
		this.saleStartSeconds = saleStartSeconds;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Integer getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(Integer saleAmount) {
		this.saleAmount = saleAmount;
	}

	public int getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(int convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public TranDeviceResultType getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(TranDeviceResultType transactionResult) {
		this.transactionResult = transactionResult;
	}

	public byte getVendByteLength() {
		return vendByteLength;
	}

	public List<LineItem> getActualLineItems() {
		return actualLineItemsUnmod;
	}

	public LineItem addActualLineItem() {
		LineItem actualLineItem;
		switch(getVendByteLength()) {
			case 0: actualLineItem = new NetBatch0LineItem(actualLineItems.size()); break;
			case 1: actualLineItem = new NetBatch1LineItem(actualLineItems.size()); break;
			case 2: actualLineItem = new NetBatch2LineItem(actualLineItems.size()); break;
			case 3: actualLineItem = new NetBatch3LineItem(actualLineItems.size()); break;
			default: throw new IllegalArgumentException("The VendByteLength '" + getVendByteLength() + "' is not supported");
		}
		actualLineItems.add(actualLineItem);
		return actualLineItem;
	}

	public void setVendByteLength(byte vendByteLength) {
		this.vendByteLength = vendByteLength;
	}


	public Long getSaleStartTime() {
		return getSaleStartSeconds()*1000;
	}
	
	public SaleResult getSaleResult() {
		return SaleResult.CANCELLED_BY_AUTH_FAILURE;
	}
			/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; saleStartSeconds=").append(getSaleStartSeconds())
			.append("; cardType=").append(getCardType())
			.append("; saleAmount=").append(getSaleAmount())
			.append("; convenienceFee=").append(getConvenienceFee())
			.append("; creditCardMagstripe=").append(MessageResponseUtils.maskTrackData(getCreditCardMagstripe()))
			.append("; transactionResult=").append(getTransactionResult())
			.append("; vendByteLength=").append(getVendByteLength() & 0xFF)
			.append("; actualLineItems=").append(getActualLineItems());
		sb.append("]");

		return sb.toString();
	}
}
