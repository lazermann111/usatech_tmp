package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidIntValueException;

/**
 *  Message Data for FILE_XFER_START_3_0 - "File Transfer Start 3.0 - A4h"
 */
public class MessageData_A4 extends AbstractMessageData {
	protected int totalPackets;
	protected long totalBytes;
	protected byte group;
	protected FileType fileType = FileType.DEX_FILE;
	protected String fileName;

	public MessageData_A4() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.FILE_XFER_START_3_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeShortInt(reply, getTotalPackets());
		ProcessingUtils.writeLongInt(reply, getTotalBytes());
		ProcessingUtils.writeByte(reply, getGroup());
		ProcessingUtils.writeByteInt(reply, getFileType().getValue());
		ProcessingUtils.writeString(reply, getFileName(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTotalPackets(ProcessingUtils.readShortInt(data));
		setTotalBytes(ProcessingUtils.readLongInt(data));
		setGroup(ProcessingUtils.readByte(data));
		try {
			setFileType(FileType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setFileName(ProcessingUtils.readString(data, data.remaining(), getCharset()));
	}

	public int getTotalPackets() {
		return totalPackets;
	}

	public void setTotalPackets(int totalPackets) {
		this.totalPackets = totalPackets;
	}

	public long getTotalBytes() {
		return totalBytes;
	}

	public void setTotalBytes(long totalBytes) {
		this.totalBytes = totalBytes;
	}

	public byte getGroup() {
		return group;
	}

	public void setGroup(byte group) {
		this.group = group;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; totalPackets=").append(getTotalPackets())
			.append("; totalBytes=").append(getTotalBytes())
			.append("; group=").append(getGroup() & 0xFF)
			.append("; fileType=").append(getFileType())
			.append("; fileName=").append(getFileName());
		sb.append("]");

		return sb.toString();
	}
}
