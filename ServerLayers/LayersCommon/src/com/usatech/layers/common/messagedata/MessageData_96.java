package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.ProcessingUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

/**
 *  Message Data for CASH_SALE_DETAIL_3_0 - "Cash Sale Detail 3.0 - 96h"
 */
public class MessageData_96 extends AbstractMessageData {
	protected long transactionId;
	public class CashVendData extends LegacyLineItem implements Sale {
	protected long saleStartSeconds;
	protected Integer price;
	protected Integer positionNumber;
		protected final int index;
		protected CashVendData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setSaleStartSeconds(ProcessingUtils.readLongInt(data));
		setPrice(ProcessingUtils.read3ByteInt(data));
		setPositionNumber(ProcessingUtils.readByteAsBCD(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getSaleStartSeconds());
		ProcessingUtils.write3ByteInt(reply, getPrice());
		ProcessingUtils.writeByteAsBCD(reply, getPositionNumber());
		}

	public long getSaleStartSeconds() {
		return saleStartSeconds;
	}

	public void setSaleStartSeconds(long saleStartSeconds) {
		this.saleStartSeconds = saleStartSeconds;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getPositionNumber() {
		return positionNumber;
	}

	public void setPositionNumber(Integer positionNumber) {
		this.positionNumber = positionNumber;
	}


		public long getTransactionId() {
            return MessageData_96.this.getTransactionId();
        }   
        public String getDeviceTranCd() {
			return String.valueOf(MessageData_96.this.getTransactionId()) + ':' + (1 + getIndex());
		}	
		public Number getSaleTax(){
			return BigDecimal.ZERO;
		}	
		public Long getSaleStartTime() {
			return getSaleStartSeconds() * 1000;
		}	
		public long getBatchId() {
			return 0;
		}
		public TimeZone getSaleTimeZone() {
			return null;
		}
		public SaleType getSaleType() {
			return SaleType.CASH;
		}	
		public TranDeviceResultType getTransactionResult() {
			return TranDeviceResultType.SUCCESS_NO_PRINTER;
		}
		public ReceiptResult getReceiptResult() {
			return ReceiptResult.UNAVAILABLE;
		}
		public List<? extends LineItem> getLineItems() {
			return Collections.singletonList(this);
		}
		public Number getSaleAmount() {
			return getPrice();
		}
		@Override
		protected int getPositionLength() {
			return 1;
		}
        @Override
        public String getDescription() {
            return getPosition();
        }
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("saleStartSeconds=").append(getSaleStartSeconds())
			.append("; price=").append(getPrice())
			.append("; positionNumber=").append(getPositionNumber());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<CashVendData> cashVends = new ArrayList<CashVendData>();
	protected final List<CashVendData> cashVendsUnmod = Collections.unmodifiableList(cashVends);

	public MessageData_96() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.CASH_SALE_DETAIL_3_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		for(CashVendData cashVend : getCashVends())
			cashVend.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		cashVends.clear();
		while(data.hasRemaining())
			addCashVend().readData(data);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public List<CashVendData> getCashVends() {
		return cashVendsUnmod;
	}

	public CashVendData addCashVend() {
		CashVendData cashVend = new CashVendData(cashVends.size());
		cashVends.add(cashVend);
		return cashVend;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; cashVends=").append(getCashVends());
		sb.append("]");

		return sb.toString();
	}
}
