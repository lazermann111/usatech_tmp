package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

import simple.text.StringUtils;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.MessageType;
/**
 *
 */
public class MessageData_Unknown extends AbstractMessageData {
	protected final byte[] messageTypeBytes;
	protected byte[] contentBytes;
	public MessageData_Unknown(byte[] messageTypeBytes){
		super();
		this.messageTypeBytes = messageTypeBytes;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	@Override
	public MessageType getMessageType() {
		return MessageType.UNKNOWN;
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	@Override
	public void readData(ByteBuffer data) {
		contentBytes = new byte[data.remaining()];
		data.get(contentBytes);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer, boolean)
	 */
	@Override
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		reply.put(getMessageNumber());
		reply.put(messageTypeBytes);
		if(maskSensitiveData)
			reply.put(MessageResponseUtils.maskAnyCardNumbers(contentBytes));
		else
			reply.put(contentBytes);
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageType=");
		StringUtils.appendHex(sb, messageTypeBytes, 0, messageTypeBytes.length);
		sb.append("; content=");
		StringUtils.appendHex(sb, MessageResponseUtils.maskAnyCardNumbers(contentBytes), 0, contentBytes.length);
		sb.append(']');

		return sb.toString();
	}
	public byte[] getContentBytes() {
		return contentBytes.clone();
	}
	public void setContentBytes(byte[] contentBytes) {
		this.contentBytes = contentBytes.clone();
	}
	public byte[] getMessageTypeBytes() {
		return messageTypeBytes.clone();
	}
}
