package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import simple.lang.InvalidIntValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ComponentType;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for COMPONENT_ID_4_1 - "Component Identification 4.1 - C1h"
 */
public class MessageData_C1 extends AbstractMessageData {
	protected ComponentType baseComponentType = ComponentType.EPORT_EDGE_G9;
	protected Currency defaultCurrency;
	public class BaseComponent implements Component {
	protected String manufacturer;
	protected String model;
	protected String serialNumber;
	protected String revision;
	protected String label;
		protected BaseComponent() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setManufacturer(ProcessingUtils.readShortString(data, getCharset()));
		setModel(ProcessingUtils.readShortString(data, getCharset()));
		setSerialNumber(ProcessingUtils.readShortString(data, getCharset()));
		setRevision(ProcessingUtils.readShortString(data, getCharset()));
		setLabel(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortString(reply, getManufacturer(), getCharset());
		ProcessingUtils.writeShortString(reply, getModel(), getCharset());
		ProcessingUtils.writeShortString(reply, getSerialNumber(), getCharset());
		ProcessingUtils.writeShortString(reply, getRevision(), getCharset());
		ProcessingUtils.writeShortString(reply, getLabel(), getCharset());
		}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}


		public ComponentType getComponentType() {
			return getBaseComponentType();
		}
		public void setComponentType(ComponentType componentType) {
			setBaseComponentType(componentType);
		}
		public int getComponentNumber() {
			return 0;
		}
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("manufacturer=").append(getManufacturer())
			.append("; model=").append(getModel())
			.append("; serialNumber=").append(getSerialNumber())
			.append("; revision=").append(getRevision())
			.append("; label=").append(getLabel());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final BaseComponent baseComponent = new BaseComponent();
	public class ComponentData implements Component {
	protected ComponentType componentType = ComponentType.VENDING_MACHINE;
	protected int componentNumber;
	protected String manufacturer;
	protected String model;
	protected String serialNumber;
	protected String revision;
	protected String label;
		protected final int index;
		protected ComponentData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		try {
			setComponentType(ComponentType.getByValue(ProcessingUtils.readShortInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setComponentNumber(ProcessingUtils.readByteInt(data));
		setManufacturer(ProcessingUtils.readShortString(data, getCharset()));
		setModel(ProcessingUtils.readShortString(data, getCharset()));
		setSerialNumber(ProcessingUtils.readShortString(data, getCharset()));
		setRevision(ProcessingUtils.readShortString(data, getCharset()));
		setLabel(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortInt(reply, getComponentType().getValue());
		ProcessingUtils.writeByteInt(reply, getComponentNumber());
		ProcessingUtils.writeShortString(reply, getManufacturer(), getCharset());
		ProcessingUtils.writeShortString(reply, getModel(), getCharset());
		ProcessingUtils.writeShortString(reply, getSerialNumber(), getCharset());
		ProcessingUtils.writeShortString(reply, getRevision(), getCharset());
		ProcessingUtils.writeShortString(reply, getLabel(), getCharset());
		}

	public ComponentType getComponentType() {
		return componentType;
	}

	public void setComponentType(ComponentType componentType) {
		this.componentType = componentType;
	}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("componentType=").append(getComponentType())
			.append("; componentNumber=").append(getComponentNumber())
			.append("; manufacturer=").append(getManufacturer())
			.append("; model=").append(getModel())
			.append("; serialNumber=").append(getSerialNumber())
			.append("; revision=").append(getRevision())
			.append("; label=").append(getLabel());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<ComponentData> components = new ArrayList<ComponentData>();
	protected final List<ComponentData> componentsUnmod = Collections.unmodifiableList(components);

	public MessageData_C1() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.COMPONENT_ID_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeShortInt(reply, getBaseComponentType().getValue());
		ProcessingUtils.writeCurrency(reply, getDefaultCurrency(), getCharset());
		getBaseComponent().writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByteInt(reply, getComponents().size());
		for(ComponentData component : getComponents())
			component.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		try {
			setBaseComponentType(ComponentType.getByValue(ProcessingUtils.readShortInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setDefaultCurrency(ProcessingUtils.readCurrency(data, getCharset()));
		getBaseComponent().readData(data);
		components.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addComponent().readData(data);
	}

	public ComponentType getBaseComponentType() {
		return baseComponentType;
	}

	public void setBaseComponentType(ComponentType baseComponentType) {
		this.baseComponentType = baseComponentType;
	}

	public Currency getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(Currency defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public BaseComponent getBaseComponent() {
		return baseComponent;
	}

	public List<ComponentData> getComponents() {
		return componentsUnmod;
	}

	public ComponentData addComponent() {
		ComponentData component = new ComponentData(components.size());
		components.add(component);
		return component;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; baseComponentType=").append(getBaseComponentType())
			.append("; defaultCurrency=").append(getDefaultCurrency())
			.append("; baseComponent=").append(getBaseComponent())
			.append("; components=").append(getComponents());
		sb.append("]");

		return sb.toString();
	}
}
