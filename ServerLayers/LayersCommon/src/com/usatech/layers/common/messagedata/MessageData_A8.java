package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.BCDInt;
import com.usatech.layers.common.constants.CounterReasonCode;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;

/**
 *  Message Data for COUNTERS_2_0 - "Counters 2.0 - A8h"
 */
public class MessageData_A8 extends AbstractMessageData implements LegacyCounters {
	protected BCDInt totalCurrencyTransactionCounter;
	protected BCDInt totalCurrencyMoneyCounter;
	protected BCDInt totalCashlessTransactionCounter;
	protected BCDInt totalCashlessMoneyCounter;
	protected BCDInt totalPasscardTransactionCounter;
	protected BCDInt totalPasscardMoneyCounter;
	protected BCDInt totalBytesTransmitted;
	protected BCDInt totalAttemptedSession;
	protected long countersId;
	protected long dateAndTime;
	protected CounterReasonCode reasonCode = CounterReasonCode.BATCH_EVENT;
	protected String fillCardMagstripe;

	public MessageData_A8() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.COUNTERS_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeBCDInt(reply, getTotalCurrencyTransactionCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalCurrencyMoneyCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalCashlessTransactionCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalCashlessMoneyCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalPasscardTransactionCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalPasscardMoneyCounter());
		ProcessingUtils.writeBCDInt(reply, getTotalBytesTransmitted());
		ProcessingUtils.writeBCDInt(reply, getTotalAttemptedSession());
		ProcessingUtils.writeLongInt(reply, getCountersId());
		ProcessingUtils.writeLongInt(reply, getDateAndTime());
		ProcessingUtils.writeByte(reply, getReasonCode().getValue());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getFillCardMagstripe()) : getFillCardMagstripe(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTotalCurrencyTransactionCounter(ProcessingUtils.readBCDInt(data));
		setTotalCurrencyMoneyCounter(ProcessingUtils.readBCDInt(data));
		setTotalCashlessTransactionCounter(ProcessingUtils.readBCDInt(data));
		setTotalCashlessMoneyCounter(ProcessingUtils.readBCDInt(data));
		setTotalPasscardTransactionCounter(ProcessingUtils.readBCDInt(data));
		setTotalPasscardMoneyCounter(ProcessingUtils.readBCDInt(data));
		setTotalBytesTransmitted(ProcessingUtils.readBCDInt(data));
		setTotalAttemptedSession(ProcessingUtils.readBCDInt(data));
		setCountersId(ProcessingUtils.readLongInt(data));
		setDateAndTime(ProcessingUtils.readLongInt(data));
		try {
			setReasonCode(CounterReasonCode.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setFillCardMagstripe(ProcessingUtils.readShortString(data, getCharset()));
	}

	public BCDInt getTotalCurrencyTransactionCounter() {
		return totalCurrencyTransactionCounter;
	}

	public void setTotalCurrencyTransactionCounter(BCDInt totalCurrencyTransactionCounter) {
		this.totalCurrencyTransactionCounter = totalCurrencyTransactionCounter;
	}

	public BCDInt getTotalCurrencyMoneyCounter() {
		return totalCurrencyMoneyCounter;
	}

	public void setTotalCurrencyMoneyCounter(BCDInt totalCurrencyMoneyCounter) {
		this.totalCurrencyMoneyCounter = totalCurrencyMoneyCounter;
	}

	public BCDInt getTotalCashlessTransactionCounter() {
		return totalCashlessTransactionCounter;
	}

	public void setTotalCashlessTransactionCounter(BCDInt totalCashlessTransactionCounter) {
		this.totalCashlessTransactionCounter = totalCashlessTransactionCounter;
	}

	public BCDInt getTotalCashlessMoneyCounter() {
		return totalCashlessMoneyCounter;
	}

	public void setTotalCashlessMoneyCounter(BCDInt totalCashlessMoneyCounter) {
		this.totalCashlessMoneyCounter = totalCashlessMoneyCounter;
	}

	public BCDInt getTotalPasscardTransactionCounter() {
		return totalPasscardTransactionCounter;
	}

	public void setTotalPasscardTransactionCounter(BCDInt totalPasscardTransactionCounter) {
		this.totalPasscardTransactionCounter = totalPasscardTransactionCounter;
	}

	public BCDInt getTotalPasscardMoneyCounter() {
		return totalPasscardMoneyCounter;
	}

	public void setTotalPasscardMoneyCounter(BCDInt totalPasscardMoneyCounter) {
		this.totalPasscardMoneyCounter = totalPasscardMoneyCounter;
	}

	public BCDInt getTotalBytesTransmitted() {
		return totalBytesTransmitted;
	}

	public void setTotalBytesTransmitted(BCDInt totalBytesTransmitted) {
		this.totalBytesTransmitted = totalBytesTransmitted;
	}

	public BCDInt getTotalAttemptedSession() {
		return totalAttemptedSession;
	}

	public void setTotalAttemptedSession(BCDInt totalAttemptedSession) {
		this.totalAttemptedSession = totalAttemptedSession;
	}

	public long getCountersId() {
		return countersId;
	}

	public void setCountersId(long countersId) {
		this.countersId = countersId;
	}

	public long getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(long dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public CounterReasonCode getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(CounterReasonCode reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getFillCardMagstripe() {
		return fillCardMagstripe;
	}

	public void setFillCardMagstripe(String fillCardMagstripe) {
		this.fillCardMagstripe = fillCardMagstripe;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; totalCurrencyTransactionCounter=").append(getTotalCurrencyTransactionCounter())
			.append("; totalCurrencyMoneyCounter=").append(getTotalCurrencyMoneyCounter())
			.append("; totalCashlessTransactionCounter=").append(getTotalCashlessTransactionCounter())
			.append("; totalCashlessMoneyCounter=").append(getTotalCashlessMoneyCounter())
			.append("; totalPasscardTransactionCounter=").append(getTotalPasscardTransactionCounter())
			.append("; totalPasscardMoneyCounter=").append(getTotalPasscardMoneyCounter())
			.append("; totalBytesTransmitted=").append(getTotalBytesTransmitted())
			.append("; totalAttemptedSession=").append(getTotalAttemptedSession())
			.append("; countersId=").append(getCountersId())
			.append("; dateAndTime=").append(getDateAndTime())
			.append("; reasonCode=").append(getReasonCode())
			.append("; fillCardMagstripe=").append(MessageResponseUtils.maskTrackData(getFillCardMagstripe()));
		sb.append("]");

		return sb.toString();
	}
}
