package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.AbstractList;
import java.util.List;

import com.usatech.layers.common.constants.SaleResult;

public abstract class ConvenienceFeeLegacySale extends LegacySale {
	protected abstract int getConvenienceFee() ;
	protected abstract List<? extends LineItem> getActualLineItems() ;
	protected final LineItem convenienceFeeLineItem = new BaseLineItem() {
		@Override
		public int getComponentNumber() {
			return 0;
		}

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public int getDuration() {
			return 0;
		}

		@Override
		public int getItem() {
			return 203;
		}

		@Override
		public String getPosition() {
			return null;
		}

		@Override
		public Number getPrice() {
			return getConvenienceFee();
		}
		
		@Override
		public int getQuantity() {
			List<? extends LineItem> rlis = getActualLineItems();
			return (rlis == null ? 0 : rlis.size());
		}

		@Override
		public SaleResult getSaleResult() {
			return SaleResult.SUCCESS;
		}

		@Override
		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {		
		}
		@Override
		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		}	
	};
	protected final List<? extends LineItem> lineItemsUnmod = new AbstractList<LineItem>() {
		@Override
		public LineItem get(int index) {
			if(index >= getActualItemOffset())
				return getActualLineItems().get(index - getActualItemOffset());
			return getVirtualLineItem(index);
		}

		@Override
		public int size() {
			return getActualItemOffset() + getActualLineItems().size();
		}
	};
	protected LineItem getVirtualLineItem(int index) {
		switch(index) {
			case 0:
				return convenienceFeeLineItem;
			default:
				throw new IndexOutOfBoundsException("Virtual Line Item " + index + " does not exist");
		}
	}
	protected int getActualItemOffset() {
		return getConvenienceFee() > 0 ? 1 : 0;
	}
	public List<? extends LineItem> getLineItems() {
		return lineItemsUnmod;
	}
	
}
