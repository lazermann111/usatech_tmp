package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EBeaconAppOS;
import com.usatech.layers.common.constants.EBeaconAppType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;

/**
 *  Message Data for EBEACON_AUTHORIZATION_REQUEST - "eBeacon Authorization Request 1.0 - F1h"
 */
public class MessageData_F1 extends AbstractMessageData implements AppAuthData {
	public interface AdditionalData extends Byteable {
	}
	public class StandardPayload implements PlainCardReader, AppAuthData {
	protected long appMessageId;
	protected EBeaconAppType appType = EBeaconAppType.MORE_MOBILE_APP;
	protected String appVersion;
	protected EBeaconAppOS appOS = EBeaconAppOS.ANDROID;
	protected EntryType entryType = EntryType.UNSPECIFIED;
	protected String accountData;
	protected byte additionalDataType;
	protected AdditionalData additionalData;
		protected StandardPayload() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setAppMessageId(ProcessingUtils.readLongInt(data));
		try {
			setAppType(EBeaconAppType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAppVersion(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setAppOS(EBeaconAppOS.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setEntryType(EntryType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAccountData(ProcessingUtils.readShortString(data, getCharset()));
		setAdditionalDataType(ProcessingUtils.readByte(data));
		if(getAdditionalData() != null)
			getAdditionalData().readData(data);
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getAppMessageId());
		ProcessingUtils.writeByte(reply, getAppType().getValue());
		ProcessingUtils.writeShortString(reply, getAppVersion(), getCharset());
		ProcessingUtils.writeByte(reply, getAppOS().getValue());
		ProcessingUtils.writeByteInt(reply, getEntryType().getValue());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData()) : getAccountData(), getCharset());
		ProcessingUtils.writeByte(reply, getAdditionalDataType());
		if(getAdditionalData() != null)
			getAdditionalData().writeData(reply, maskSensitiveData);
		}

	public long getAppMessageId() {
		return appMessageId;
	}

	public void setAppMessageId(long appMessageId) {
		this.appMessageId = appMessageId;
	}

	public EBeaconAppType getAppType() {
		return appType;
	}

	public void setAppType(EBeaconAppType appType) {
		this.appType = appType;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public EBeaconAppOS getAppOS() {
		return appOS;
	}

	public void setAppOS(EBeaconAppOS appOS) {
		this.appOS = appOS;
	}

	public EntryType getEntryType() {
		return entryType;
	}

	public void setEntryType(EntryType entryType) {
		this.entryType = entryType;
	}

	public String getAccountData() {
		return accountData;
	}

	public void setAccountData(String accountData) {
		this.accountData = accountData;
	}

	public byte getAdditionalDataType() {
		return additionalDataType;
	}

	public AdditionalData getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalDataType(byte additionalDataType) {
		this.additionalDataType = additionalDataType;
		switch(additionalDataType) {
			case 0: this.additionalData = null; break;
			default: throw new IllegalArgumentException("The AdditionalDataType '" + additionalDataType + "' is not supported");
		}
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("appMessageId=").append(getAppMessageId())
			.append("; appType=").append(getAppType())
			.append("; appVersion=").append(getAppVersion())
			.append("; appOS=").append(getAppOS())
			.append("; entryType=").append(getEntryType())
			.append("; accountData=").append(MessageResponseUtils.maskTrackData(getAccountData()))
			.append("; additionalDataType=").append(getAdditionalDataType() & 0xFF)
			.append("; additionalData=").append(getAdditionalData());
			sb.append(']');
			return sb.toString();
		}

    public PlainCardReader getCardReader() {
       return this;
    }
    public CardReaderType getCardReaderType() {
       return CardReaderType.GENERIC;
    }               
                
	}
	protected byte payloadType;
	protected AppAuthData payload;

	public MessageData_F1() {
		super();
		setPayloadType((byte) 0);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.EBEACON_AUTHORIZATION_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getPayloadType());
		getPayload().writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setPayloadType(ProcessingUtils.readByte(data));
		getPayload().readData(data);
	}

	public byte getPayloadType() {
		return payloadType;
	}

	public AppAuthData getPayload() {
		return payload;
	}

	public void setPayloadType(byte payloadType) {
		this.payloadType = payloadType;
		switch(payloadType) {
			case 0: this.payload = new StandardPayload(); break;
			default: throw new IllegalArgumentException("The PayloadType '" + payloadType + "' is not supported");
		}
	}


	public EntryType getEntryType() {
	   return getPayload().getEntryType();
	}

    public long getAppMessageId() {
       return getPayload().getAppMessageId();
    }

    public CardReaderType getCardReaderType() {
       return getPayload().getCardReaderType();
    }

    public CardReader getCardReader() {
       return getPayload().getCardReader();
    }
	    	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; payloadType=").append(getPayloadType() & 0xFF)
			.append("; payload=").append(getPayload());
		sb.append("]");

		return sb.toString();
	}
}
