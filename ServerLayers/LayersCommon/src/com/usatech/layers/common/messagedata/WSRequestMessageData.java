package com.usatech.layers.common.messagedata;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import simple.io.Log;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;
import simple.translator.Translator;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.TranDeviceResultType;

public abstract class WSRequestMessageData extends WSMessageData {
	public static interface Validator<V> {
		public String validate(V value, Translator translator);
	}

	public static abstract class ConditionValidator<V> implements Validator<V> {
		protected final String messageKey;
		protected final String messageDefault;

		public ConditionValidator(String messageKey, String messageDefault) {
			this.messageKey = messageKey;
			this.messageDefault = messageDefault;
		}

		protected abstract boolean isValid(V value);

		public String validate(V value, Translator translator) {
			if(isValid(value))
				return null;
			return translator.translate(messageKey, messageDefault, value);
		}
	}

	public static abstract class ExceptionValidator<V, E extends Exception> implements Validator<V> {
		protected final String messageKey;
		protected final String messageDefault;

		public ExceptionValidator(String messageKey, String messageDefault) {
			this.messageKey = messageKey;
			this.messageDefault = messageDefault;
		}

		public abstract void attempt(V value) throws E;

		public String validate(V value, Translator translator) {
			try {
				attempt(value);
			} catch(Exception e) {
				return translator.translate(messageKey, messageDefault, value, e);
			}
			return null;
		}
	}

	protected static final Validator<WSTran> INVALID_TRAN_ID = new ConditionValidator<WSTran>("ws.message.invalid-tran-id", "Invalid tranId: {0.tranId}") {
		protected boolean isValid(WSTran value) {
			return value.getTranId() > 0;
		}
	};
	protected static final Validator<WSTran> INVALID_AMOUNT = new ConditionValidator<WSTran>("ws.message.invalid-amount", "Invalid amount: {0.amount}") {
		protected boolean isValid(WSTran value) {
			return value.getAmount() != null && value.getAmount().longValue() >= 0;
		}
	};
	protected static final Validator<PlainCardReader> EMPTY_CARD_DATA = new ConditionValidator<PlainCardReader>("ws.message.empty-card-data", "Empty card data") {
		@Override
		protected boolean isValid(PlainCardReader value) {
			return !StringUtils.isBlank(value.getAccountData());
		}
	};
	protected static long maxFileSize;
	protected static int maxTranDetailLength;
	protected static final Set<String> ALLOWED_CARD_TYPES = new TreeSet<String>();
	protected static final Set<Integer> ALLOWED_CARD_READER_TYPES = new TreeSet<Integer>();
	protected static final Set<String> ALLOWED_TRANSACTION_RESULTS = new TreeSet<String>();
	protected static final Set<Integer> ALLOWED_UPLOAD_FILE_TYPES = new TreeSet<Integer>();
	static {
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.CREDIT_SWIPE.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.CREDIT_CONTACTLESS.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.CREDIT_ISIS.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.CREDIT_MANUAL_ENTRY.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.SPECIAL_SWIPE.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.SPECIAL_CONTACTLESS.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.SPECIAL_ISIS.getValue()));
		ALLOWED_CARD_TYPES.add(String.valueOf(CardType.SPECIAL_MANUAL_ENTRY.getValue()));

		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.MAGTEK_MAGNESAFE.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_SECUREMAG.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_SECURED.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.ACS_ACR31.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.UIC_BEZEL.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.OTI_BEZEL.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_VEND3AR.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.OTI_EMV.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_VENDX_NON_ENCRYPTED.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.INGENICO_MOBILE.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.INGENICO_BEZEL.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_VENDX_PARSED_AES.getValue());
		ALLOWED_CARD_READER_TYPES.add((int) CardReaderType.IDTECH_VENDX_PARSED_3DES.getValue());

		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.AUTH_FAILURE.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.CANCELLED.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.FAILURE.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.SUCCESS.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.SUCCESS_NO_PRINTER.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.SUCCESS_NO_RECEIPT.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.SUCCESS_RECEIPT_ERROR.getValue()));
		ALLOWED_TRANSACTION_RESULTS.add(String.valueOf(TranDeviceResultType.TIMEOUT.getValue()));

		ALLOWED_UPLOAD_FILE_TYPES.add(FileType.AUTO_EMAIL.getValue());
		ALLOWED_UPLOAD_FILE_TYPES.add(FileType.CUSTOM_FILE_UPLOAD.getValue());
		ALLOWED_UPLOAD_FILE_TYPES.add(FileType.KIOSK_GENERIC_FILE.getValue());
		ALLOWED_UPLOAD_FILE_TYPES.add(FileType.LOG_FILE.getValue());
	}

	public static boolean isCardReaderTypeAllowed(int cardReaderType) {
		return ALLOWED_CARD_READER_TYPES.contains(cardReaderType);
	}
	protected String username;
	protected String password;
	protected final List<Validator<?>> validators = new ArrayList<Validator<?>>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	protected void addValidators(Validator<?>... validators) {
		for(Validator<?> validator : validators)
			addValidator(validator);
	}
	protected void addValidator(Validator<?> validator) {
		validators.add(validator);
	}

	@SuppressWarnings("unchecked")
	public void validate(Translator translator) throws InvalidValueException {
		Set<String> messages = new HashSet<String>();
		StringBuilder sb = null;
		for(Validator<?> validator : validators) {
			String error;
			try {
				error = ((Validator<Object>)validator).validate(this, translator);
			} catch(ClassCastException e) {
				Log.getLog().error("Could not validate " + getClass().getName() + " with " + validator.getClass().getName());
				continue;
			}
			if(error != null && messages.add(error.toUpperCase())) {
				if(sb == null)
					sb = new StringBuilder("Request failed validation: [");
				else
					sb.append(", ");
				sb.append(error);
			}
		}
		if(sb != null)
			throw new InvalidValueException(sb.append("]").toString(), this);
	}

	public abstract WSResponseMessageData createResponse();

	public static long getMaxFileSize() {
		return maxFileSize;
	}

	public static void setMaxFileSize(long maxFileSize) {
		WSRequestMessageData.maxFileSize = maxFileSize;
	}

	public static int getMaxTranDetailLength() {
		return maxTranDetailLength;
	}

	public static void setMaxTranDetailLength(int maxTranDetailLength) {
		WSRequestMessageData.maxTranDetailLength = maxTranDetailLength;
	}

}
