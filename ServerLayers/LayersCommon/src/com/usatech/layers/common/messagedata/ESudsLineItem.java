package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.ESudsTopOrBottomType;

public abstract class ESudsLineItem extends LegacyLineItem {
	protected String description;

	protected int getPositionLength() {
		return 0;
	}
	@Override
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public abstract ESudsTopOrBottomType getTopOrBottom();
	@Override
	protected Number getPositionNumber() {
		return 0;
	}
	
	@Override
	public int getComponentPosition(){
		return getTopOrBottom().getPosition();
	}

}
