package com.usatech.layers.common.messagedata;

import simple.io.ByteArrayUtils;
import simple.text.StringUtils;

/**
 * Message Data for Tokenize Requests
 */
public abstract class WS2UntokenizeMessageData extends WS2RequestMessageData {
	protected byte[] token;
	protected static final ExceptionValidator<WS2UntokenizeMessageData, IllegalArgumentException> INVALID_TOKEN_HEX = new ExceptionValidator<WS2UntokenizeMessageData, IllegalArgumentException>("ws.message.invalid-token-hex", "Invalid tokenHex: {0.tokenHex}") {
		public void attempt(WS2UntokenizeMessageData value) throws IllegalArgumentException {
			if(StringUtils.isBlank(value.getTokenHex()))
				throw new IllegalArgumentException("tokenHex is blank");
			value.token = ByteArrayUtils.fromHex(value.getTokenHex());
		}
	};

	public WS2UntokenizeMessageData() {
		super();
		addValidator(INVALID_TOKEN_HEX);
	}

	public byte[] getToken() {
		if(token == null)
			INVALID_TOKEN_HEX.attempt(this);
		return token;
	}

	public void setToken(byte[] token) {
		this.token = token;
	}

	public abstract String getTokenHex();
}
