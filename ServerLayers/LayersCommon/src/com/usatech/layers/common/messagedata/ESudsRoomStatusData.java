package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.ESudsRoomStatus;

public interface ESudsRoomStatusData {
	public ESudsRoomStatus getBottom();
	public ESudsRoomStatus getTop();
}
