package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  Message Data for QUEUE_TIMING_CHECK - "Queue Timing Check - 0007h"
 */
public class MessageData_0007 extends AbstractMessageData {
	protected long timeout;
	public class QueueData {
	protected String queueName;
		protected final int index;
		protected QueueData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setQueueName(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortString(reply, getQueueName(), getCharset());
		}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("queueName=").append(getQueueName());
			sb.append(']');
			return sb.toString();
		}
	}
	protected final List<QueueData> queues = new ArrayList<QueueData>();
	protected final List<QueueData> queuesUnmod = Collections.unmodifiableList(queues);

	public MessageData_0007() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.QUEUE_TIMING_CHECK;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTimeout());
		ProcessingUtils.writeByteInt(reply, getQueues().size());
		for(QueueData queue : getQueues())
			queue.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTimeout(ProcessingUtils.readLongInt(data));
		queues.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addQueue().readData(data);
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public List<QueueData> getQueues() {
		return queuesUnmod;
	}

	public QueueData addQueue() {
		QueueData queue = new QueueData(queues.size());
		queues.add(queue);
		return queue;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; timeout=").append(getTimeout())
			.append("; queues=").append(getQueues());
		sb.append("]");

		return sb.toString();
	}
}
