/**
 *
 */
package com.usatech.layers.common.messagedata;

import com.usatech.layers.common.constants.SaleResult;

/**
 * @author Brian S. Krug
 *
 */
public interface LineItem extends Byteable {

	public int getComponentNumber();
	
	public int getComponentPosition();

	//public void setComponentNum(int componentNum);

	public int getItem();

	//public void setItemPurchased(int itemPurchased);

	public SaleResult getSaleResult();

	//public void setSaleResult(SaleResult saleResult);

	public int getQuantity();

	//public void setQuantity(int quantity);

	public Number getPrice();

	//public void setPrice(BigDecimal price);

	public String getDescription();

	//public void setDescription(String description);

	public int getDuration();

	//public void setDuration(int duration);

	public String getPosition();

}