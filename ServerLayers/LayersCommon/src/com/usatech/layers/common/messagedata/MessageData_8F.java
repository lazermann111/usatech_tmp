package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for SET_ID_NUMBER_AND_KEY - "Set ID Number and Key - 8Fh"
 */
public class MessageData_8F extends AbstractMessageData {
	protected byte[] encryptionKey;
	protected byte[] deviceSerialNum;

	public MessageData_8F() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.SET_ID_NUMBER_AND_KEY;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeEncryptionKey(reply, maskSensitiveData ? MessageResponseUtils.maskEncryptionKey(getEncryptionKey()) : getEncryptionKey());
		ProcessingUtils.writeBytesRemaining(reply, getDeviceSerialNum());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setEncryptionKey(ProcessingUtils.readEncryptionKey(data));
		setDeviceSerialNum(ProcessingUtils.readBytesRemaining(data));
	}

	public byte[] getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public byte[] getDeviceSerialNum() {
		return deviceSerialNum;
	}

	public void setDeviceSerialNum(byte[] deviceSerialNum) {
		this.deviceSerialNum = deviceSerialNum;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; encryptionKey=").append(new String(MessageResponseUtils.maskEncryptionKey(getEncryptionKey()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; deviceSerialNum=").append(new String(getDeviceSerialNum(), ProcessingConstants.US_ASCII_CHARSET));
		sb.append("]");

		return sb.toString();
	}
}
