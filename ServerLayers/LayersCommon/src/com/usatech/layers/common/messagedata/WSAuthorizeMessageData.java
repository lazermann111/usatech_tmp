package com.usatech.layers.common.messagedata;

import simple.lang.InvalidValueException;

import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;

public abstract class WSAuthorizeMessageData extends WSRequestMessageData implements AuthorizeMessageData, WSTran {
	protected static final ExceptionValidator<WSAuthorizeMessageData, InvalidValueException> INVALID_CARD_TYPE = new ExceptionValidator<WSAuthorizeMessageData, InvalidValueException>("ws.message.invalid-card-type", "Invalid cardType: {0.cardType}") {
		public void attempt(WSAuthorizeMessageData value) throws InvalidValueException {
			if(!ALLOWED_CARD_TYPES.contains(value.getCardType()))
				throw new InvalidValueException("Type not allowed", value.getCardType());
			value.entryType = CardType.getByValue(value.getCardType().charAt(0)).getEntryType();
		}
	};

	protected long tranId;
	protected EntryType entryType;
	
	public WSAuthorizeMessageData() {
		super();
		addValidators(INVALID_TRAN_ID, INVALID_AMOUNT, INVALID_CARD_TYPE);
		if(this instanceof PlainCardReader)
			addValidator(EMPTY_CARD_DATA);
	}

	public abstract String getCardType();
	
	public abstract WSAuthorizeResponseMessageData createResponse();

	public String getValidationData() {
		return null;
	}

	public long getTransactionId() {
		return getTranId();
	}

	public void setTransactionId(long transactionId) {
		setTranId(transactionId);
	}

	public EntryType getEntryType() {
		if(entryType == null)
			try {
				INVALID_CARD_TYPE.attempt(this);
			} catch(InvalidValueException e) {
				return EntryType.UNSPECIFIED;
			}
		return entryType;
	}

	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.PURCHASE;
	}

	public String getPin() {
		return null;
	}

	public long getTranId() {
		return tranId;
	}

	public void setTranId(long tranId) {
		this.tranId = tranId;
	}
}
