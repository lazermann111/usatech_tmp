package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.CRC;
import com.usatech.layers.common.ProcessingUtils;
import java.util.Calendar;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidIntValueException;

/**
 *  Message Data for FILE_XFER_START_4_1 - "File Transfer Start 4.1 - C8h"
 */
public class MessageData_C8 extends AbstractMessageData {
	protected Calendar creationTime;
	protected long eventId;
	protected int filesRemaining;
	protected int totalPackets;
	protected long totalBytes;
	protected FileType fileType = FileType.DEX_FILE;
	protected CRC crc;
	protected String fileName;

	public MessageData_C8() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.FILE_XFER_START_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeTimestamp(reply, getCreationTime());
		ProcessingUtils.writeLongInt(reply, getEventId());
		ProcessingUtils.writeByteInt(reply, getFilesRemaining());
		ProcessingUtils.writeShortInt(reply, getTotalPackets());
		ProcessingUtils.writeLongInt(reply, getTotalBytes());
		ProcessingUtils.writeShortInt(reply, getFileType().getValue());
		ProcessingUtils.writeCRC(reply, getCrc());
		ProcessingUtils.writeShortString(reply, getFileName(), getCharset());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setCreationTime(ProcessingUtils.readTimestamp(data));
		setEventId(ProcessingUtils.readLongInt(data));
		setFilesRemaining(ProcessingUtils.readByteInt(data));
		setTotalPackets(ProcessingUtils.readShortInt(data));
		setTotalBytes(ProcessingUtils.readLongInt(data));
		try {
			setFileType(FileType.getByValue(ProcessingUtils.readShortInt(data)));
		} catch(InvalidIntValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		try {
			setCrc(ProcessingUtils.readCRC(data));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setFileName(ProcessingUtils.readShortString(data, getCharset()));
	}

	public Calendar getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Calendar creationTime) {
		this.creationTime = creationTime;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public int getFilesRemaining() {
		return filesRemaining;
	}

	public void setFilesRemaining(int filesRemaining) {
		this.filesRemaining = filesRemaining;
	}

	public int getTotalPackets() {
		return totalPackets;
	}

	public void setTotalPackets(int totalPackets) {
		this.totalPackets = totalPackets;
	}

	public long getTotalBytes() {
		return totalBytes;
	}

	public void setTotalBytes(long totalBytes) {
		this.totalBytes = totalBytes;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public CRC getCrc() {
		return crc;
	}

	public void setCrc(CRC crc) {
		this.crc = crc;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; creationTime=").append(MessageDataUtils.toString(getCreationTime()))
			.append("; eventId=").append(getEventId())
			.append("; filesRemaining=").append(getFilesRemaining())
			.append("; totalPackets=").append(getTotalPackets())
			.append("; totalBytes=").append(getTotalBytes())
			.append("; fileType=").append(getFileType())
			.append("; crc=").append(getCrc())
			.append("; fileName=").append(getFileName());
		sb.append("]");

		return sb.toString();
	}
}
