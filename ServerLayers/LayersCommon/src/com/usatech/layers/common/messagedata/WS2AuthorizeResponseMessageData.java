package com.usatech.layers.common.messagedata;

import com.usatech.ec.ECResponse;
import com.usatech.layers.common.constants.AuthResultCode;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

public abstract class WS2AuthorizeResponseMessageData extends WS2ResponseMessageData {
	protected boolean serviceCard = false;

	public abstract long getApprovedAmount();

	public abstract void setApprovedAmount(long approvedAmount);

	public void setAuthResultCd(AuthResultCode authResultCd) {
		if(isServiceCard()) {
			setApprovedAmount(0);
			switch(authResultCd) {
				case APPROVED:
				case PARTIAL:
				case AVS_MISMATCH:
				case CVV_MISMATCH:
				case CVV_AND_AVS_MISMATCH:
					setReturnCode(ECResponse.RES_OK);
					if(StringUtils.isBlank(getReturnMessage()))
						setReturnMessage("Service Recorded");
					break;
			}
			return;
		}
		setReturnCode(authResultCd.getAuthResponseCodeEC2());	
		if(StringUtils.isBlank(getReturnMessage()))			
			switch(authResultCd) {
				case APPROVED:
					setReturnMessage("Approved");
					break;
				case PARTIAL:
					setReturnMessage("Partially Approved");
					break;
				case DECLINED:
					setReturnMessage("Declined");
					break;
				case DECLINED_PERMANENT:
					setReturnMessage("Declined");
					break;
				case DECLINED_PAYMENT_METHOD:
					setReturnMessage("Invalid Card");
					break;
				case DECLINED_DEBIT:
					setReturnMessage("Debit Not Accepted");
					break;
				case FAILED:
					setReturnMessage("Failed");
					break;
				case AVS_MISMATCH:
					setReturnMessage("Address Does Not Match");
					break;
				case CVV_MISMATCH:
					setReturnMessage("Security Code Does Not Match");
					break;
				case CVV_AND_AVS_MISMATCH:
					setReturnMessage("Both Address and Security Code Do Not Match");
					break;
			}
		switch(authResultCd) {
			case APPROVED:
			case PARTIAL:
			case AVS_MISMATCH:
			case CVV_MISMATCH:
			case CVV_AND_AVS_MISMATCH:
				break;
			default:
				setApprovedAmount(0);
		}
	}

	public void setOverrideResponseMessage(String overrideResponseMessage) {
		if(overrideResponseMessage != null)
			switch(getReturnCode()) {
				case 2:
				case 4:
					setReturnMessage(overrideResponseMessage);
			}
	}

	public void setOriginalAmount(long originalAmount) {
		if(originalAmount > 0 && getReturnCode() == 2)
			setApprovedAmount(originalAmount);
	}

	public boolean getFreeTransaction() {
		return ConvertUtils.getBooleanSafely(getAttribute("freeTransaction"), false);
	}

	public void setFreeTransaction(boolean freeTransaction) {
		setAttribute("freeTransaction", freeTransaction ? "true" : null);
	}

	public boolean getNoConvenienceFee() {
		return ConvertUtils.getBooleanSafely(getAttribute("noConvenienceFee"), false);
	}

	public void setNoConvenienceFee(boolean noConvenienceFee) {
		setAttribute("noConvenienceFee", noConvenienceFee ? "true" : null);
	}

	public boolean isServiceCard() {
		return serviceCard;
	}

	public void setServiceCard(boolean serviceCard) {
		this.serviceCard = serviceCard;
		if(serviceCard) {
			setApprovedAmount(0);
			switch(getReturnCode()) {
				case ECResponse.RES_APPROVED:
				case ECResponse.RES_PARTIALLY_APPROVED:
				case ECResponse.RES_AVS_MISMATCH:
				case ECResponse.RES_CVV_MISMATCH:
				case ECResponse.RES_CVV_AND_AVS_MISMATCH:
					setReturnCode(ECResponse.RES_OK);
					if(StringUtils.isBlank(getReturnMessage()))
						setReturnMessage("Service Recorded");
					break;
			}
		}
	}
}
