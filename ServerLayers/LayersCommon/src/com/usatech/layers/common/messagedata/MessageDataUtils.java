/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.ProcessingUtils.PropertyValueHandler;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;

import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.lang.InvalidIntValueException;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.AbstractLookupMap;
import simple.util.CollectionUtils;

/**
 * @author Brian S. Krug
 *
 */
public class MessageDataUtils {
	public static byte[] summarizeBytes(byte[] bytes) {
		return summarizeBytes(bytes, 0);
	}

	public static byte[] summarizeBytes(byte[] bytes, int maxDisplayLength) {
		if(bytes == null)
			return IOUtils.EMPTY_BYTES;
		else if(bytes.length <= maxDisplayLength || (bytes.length < 20 && new String(bytes, ProcessingConstants.US_ASCII_CHARSET).matches("\\<\\d+ bytes\\>")))
			return bytes;
		else
			return ("<" + bytes.length + " bytes>").getBytes(ProcessingConstants.US_ASCII_CHARSET);
	}

	public static String toSummary(byte[] bytes, int maxDisplayLength) {
		String s;
		if(bytes == null)
			return "";
		else if(bytes.length <= maxDisplayLength)
			return new String(bytes, ProcessingConstants.US_ASCII_CHARSET);
		else if(bytes.length < 20 && (s = new String(bytes, ProcessingConstants.US_ASCII_CHARSET)).matches("\\<\\d+ bytes\\>"))
			return s;
		else
			return ("<" + bytes.length + " bytes>");
	}

	public static byte[] summarizeBytes(ByteBuffer buffer) {
		if(buffer == null || buffer.remaining() == 0)
			return IOUtils.EMPTY_BYTES;
		else if(buffer.remaining() < 20) {
			String s;
			if(buffer.hasArray()) {
				s = new String(buffer.array(), buffer.arrayOffset() + buffer.position(), buffer.remaining(), ProcessingConstants.US_ASCII_CHARSET);
				if(s.matches("\\<\\d+ bytes\\>"))
					return s.getBytes();
			} else {
				byte[] tmp = new byte[buffer.remaining()];
				int pos = buffer.position();
				buffer.get(tmp);
				buffer.position(pos);
				s = new String(tmp, ProcessingConstants.US_ASCII_CHARSET);
				if(s.matches("\\<\\d+ bytes\\>"))
					return tmp;
			}
		}
		return ("<" + buffer.remaining() + " bytes>").getBytes(ProcessingConstants.US_ASCII_CHARSET);
	}
	public static String summarizeString(String string) {
		if(string == null)
			return "";
		else if(string.length() < 20 && string.matches("\\<\\d+ chars\\>"))
			return string;
		else
			return ("<" + string.length() + " chars>");
	}

	protected static final Set<Integer> sensitivePropertyIndexes = Collections.singleton(DeviceProperty.ENCRYPTION_KEY.getValue());

	public static byte[] maskFileContent(byte[] content, FileType fileType) {
		switch(fileType) {
			case APPLICATION_SOFTWARE_UPGRADE:
			case EXECUTABLE_FILE:
			case EXECUTABLE_FILE_THAT_REQUIRES_APP_RESTART:
			case MULTIPLEXOR_APPLICATION:
			case GENERIC_BINARY_DATA:
			case CONFIGURATION_FILE:
				return summarizeBytes(content);
			case PROPERTY_LIST:
				String propertyValueList = new String(content, ProcessingConstants.US_ASCII_CHARSET);
				final StringBuilder sb = new StringBuilder();
				try {
					ProcessingUtils.parsePropertyList(propertyValueList, new PropertyValueHandler<RuntimeException>() {
						@Override
						public void handleProperty(int propertyIndex, String propertyValue) {
							if(sensitivePropertyIndexes.contains(propertyIndex))
								propertyValue = StringUtils.fillString(propertyValue.length(), "*");
							appendToPropertyValueList(sb, propertyIndex, propertyValue);
						}
					});
				} catch(IOException e) {
					sb.setLength(0);
					sb.append("INVALID PROPERTY VALUE LIST:").append(propertyValueList).append(':').append(e.getMessage());
				} catch(ParseException e) {
					sb.setLength(0);
					sb.append("INVALID PROPERTY VALUE LIST:").append(propertyValueList).append(':').append(e.getMessage());
				}
				return sb.toString().getBytes(ProcessingConstants.US_ASCII_CHARSET);
			default:
				// show plain text
				return content;
		}
	}

	public static String toStringFileContent(byte[] content, FileType fileType) {
		switch(fileType) {
			case APPLICATION_SOFTWARE_UPGRADE:
			case EXECUTABLE_FILE:
			case EXECUTABLE_FILE_THAT_REQUIRES_APP_RESTART:
			case MULTIPLEXOR_APPLICATION:
			case GENERIC_BINARY_DATA:
			case CONFIGURATION_FILE:
				return toSummary(content, 0);
			case PROPERTY_LIST:
				String propertyValueList = new String(content, ProcessingConstants.US_ASCII_CHARSET);
				final StringBuilder sb = new StringBuilder();
				try {
					ProcessingUtils.parsePropertyList(propertyValueList, new PropertyValueHandler<RuntimeException>() {
						@Override
						public void handleProperty(int propertyIndex, String propertyValue) {
							if(sensitivePropertyIndexes.contains(propertyIndex))
								propertyValue = StringUtils.fillString(propertyValue.length(), "*");
							sb.append(propertyIndex).append('=');
							if(propertyValue != null)
								sb.append(propertyValue);
							sb.append("; ");
						}
					});
				} catch(IOException e) {
					sb.setLength(0);
					sb.append("INVALID PROPERTY VALUE LIST:").append(propertyValueList).append(':').append(e.getMessage());
				} catch(ParseException e) {
					sb.setLength(0);
					sb.append("INVALID PROPERTY VALUE LIST:").append(propertyValueList).append(':').append(e.getMessage());
				}
				return sb.toString();
			default:
				// show plain text
				return toSummary(content, 1024);
		}
	}

	public static Map<Integer,String> maskPropertyValueList(final Map<Integer,String> propertyValueList) {
		if(propertyValueList == null || propertyValueList.isEmpty() || !CollectionUtils.containsAny(propertyValueList.keySet(), sensitivePropertyIndexes))
			return propertyValueList;
		return new AbstractLookupMap<Integer, String>() {
			/**
			 * @see simple.util.AbstractLookupMap#keySet()
			 */
			@Override
			public Set<Integer> keySet() {
				return propertyValueList.keySet();
			}
			/**
			 * @see simple.util.AbstractLookupMap#get(java.lang.Object)
			 */
			@Override
			public String get(Object key) {
				String value = propertyValueList.get(key);
				if(sensitivePropertyIndexes.contains(key))
					value = maskString(propertyValueList.get(key));
				return value;
			}
		};
	}
	public static String maskString(String string) {
		if(string == null) return null;
		return StringUtils.fillString(string.length(), "*");
	}
	protected static final byte MASK_BYTE = (byte) '*';

	public static String toString(Calendar cal) {
		return cal == null ? "" : ConvertUtils.formatObject(cal, "DATE:MM/dd/yyyy HH:mm:ss z", null, true);
	}

	protected static final char[] ILLEGAL_KEY_CHARS = "\n=".toCharArray();
	protected static final Pattern VALUE_CHAR_REPLACEMENT = Pattern.compile("(\\\n\r\t)");

	/**
	 * Format using '=' and '\n'
	 * 
	 * @param attributeMap
	 * @return
	 */
	public static String toAttributeString(Map<String, String> attributeMap) {
		if(attributeMap.isEmpty())
			return "";
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, String> entry : attributeMap.entrySet()) {
			if(entry.getValue() == null)
				continue;
			if(entry.getKey() == null)
				throw new IllegalArgumentException("Null keys not supported");
			if(StringUtils.indexOf(entry.getKey(), ILLEGAL_KEY_CHARS) >= 0)
				throw new IllegalArgumentException("Newlines and '=' not allowed in keys");
			sb.append(entry.getKey()).append('=');
			for(int i = 0; i < entry.getValue().length(); i++) {
				char ch = entry.getValue().charAt(i);
				switch(ch) {
					case '\\':
						sb.append("\\\\");
						break;
					case '\n':
						sb.append("\\n");
						break;
					case '\r':
						sb.append("\\r");
						break;
					case '\t':
						sb.append("\\t");
						break;
					case '\b':
						sb.append("\\b");
						break;
					case '\f':
						sb.append("\\f");
						break;
					default:
						sb.append(ch);
						break;
				}
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	public static void toAttributeMap(String attributeString, Map<String, String> attributeMap) {
		attributeMap.clear();
		if(StringUtils.isBlank(attributeString))
			return;
		StringBuilder sb = new StringBuilder();
		String key = null;
		for(int i = 0; i < attributeString.length(); i++) {
			char ch = attributeString.charAt(i);
			switch(ch) {
				case '\\':
					ch = attributeString.charAt(++i);
					switch(ch) {
						case 'r':
							sb.append('\r');
							break;
						case 't':
							sb.append('\t');
							break;
						case 'b':
							sb.append('\b');
							break;
						case 'f':
							sb.append('\f');
							break;
						case 'n':
							sb.append('\n');
							break;
						default:
							sb.append(ch);
							break;
					}
					break;
				case '\n':
					if(key != null)
						attributeMap.put(key, sb.toString());
					key = null;
					sb.setLength(0);
					break;
				case '=':
					if(key == null) {
						key = sb.toString();
						sb.setLength(0);
						break;
					}
				default:
					sb.append(ch);
					break;
			}
		}
		if(key != null && sb.length() > 0)
			attributeMap.put(key, sb.toString());
	}
	public static StringBuilder appendToPropertyValueList(StringBuilder appendTo, int propertyIndex, String propertyValue) {
		appendTo.append(propertyIndex);
		if(propertyValue != null)
			appendTo.append(PROPERTY_EQUALS).append(propertyValue);
		else
			appendTo.append(PROPERTY_DEFAULT);
		return appendTo.append(PROPERTY_VALUE_SEPARATOR);
	}
	/*
	
	    public static void writeResponse_BATCH_ACK_2_0(Message message, long transactionId) {
	        ByteBuffer reply = message.getReply();
	        reply.put(message.getMessageNumber());
	        reply.put(MessageType.BATCH_ACK_2_0.getValue());//0x71
	        ProcessingUtils.writeLongInt(reply, transactionId); //trans_id
	        ProcessingUtils.writeByteInt(reply, SaleResult71.PASS.getValue());
	    }
	
	    public static void writeGenericResponseWithPropertyListV4_1(Message message, GenericResponseResultCode resultCode, String text, String propertyList) {
	        ByteBuffer reply = message.getReply();
	        writeGenericResponseV4_1(message, resultCode, text, GenericResponseServerActionCode.PROCESS_PROP_LIST);
	        ProcessingUtils.writeLongString(reply, propertyList, getDeviceCharset(message));
	    }
	
	    public static void writeGenericResponseWithPropertyListV4_1(Message message, GenericResponseResultCode resultCode, String text, InputStream propertyListStream, int length) throws IOException {
	        ByteBuffer reply = message.getReply();
	        writeGenericResponseV4_1(message, resultCode, text, GenericResponseServerActionCode.PROCESS_PROP_LIST);
	        ProcessingUtils.writeShortInt(reply, length);
	        if(length > 0) {
	        	byte[] bytes;
	        	int offset;
	        	if(reply.hasArray()) {
	        		bytes = reply.array();
	        		offset = reply.arrayOffset() + reply.position();
	        	} else {
	        		bytes = new byte[length];
	        		offset = 0;
	        	}
	        	int tot = 0;
	    		while(tot < length) {
	    			int r = propertyListStream.read(bytes, offset + tot, length - tot);
	        		if(r == -1)
	        			throw new EOFException("Not enough bytes provided in inputstream");
	        		tot += r;
	    		}
	    		if(reply.hasArray()) {
	    			reply.position(reply.position() + length);
	    		} else {
	    			reply.put(bytes);
	    		}
	        }
	    }
	
	    public static void writeGenericResponseWithPropertyListV4_1(Message message, GenericResponseResultCode resultCode, String text, Map<Integer,String> propertyList) {
	        writeGenericResponseWithPropertyListV4_1(message, resultCode, text, buildPropertyValueList(propertyList));
	    }
	
	    public static void writeAuthResponseControlCallIn(Message message, int seconds) {
	    	ByteBuffer writeBuffer = message.getReply();
	    	writeBuffer.put(message.getMessageNumber());
			writeBuffer.put(MessageType.AUTH_RESPONSE_4_1.getValue());
			ProcessingUtils.writeLongInt(writeBuffer, message.getMessageId()); // messageId
			ProcessingUtils.writeByteInt(writeBuffer, 0); // Control
			ProcessingUtils.writeByteInt(writeBuffer, 1); // Perform Call-In
			ProcessingUtils.writeShortInt(writeBuffer, seconds); // Number of seconds
			if(message.getLog().isInfoEnabled())
				message.getLog().info("Responding with " + MessageType.AUTH_RESPONSE_4_1.getDescription() + " - Control: Perform Call-in");
	    }
	
	    public static String buildPropertyValueList(Map<Integer,String> propertyValues) {
	    	StringBuilder sb = new StringBuilder();
	        for(Map.Entry<Integer, String> entry : propertyValues.entrySet()) {
	        	appendToPropertyValueList(sb, entry.getKey(), entry.getValue());
	        }
	        return sb.toString();
	    }
	*/
	    public static String buildEfficientPropertyValueList(Map<Integer,String> propertyValues) {
	    	return buildEfficientPropertyValueList(propertyValues, false);
	    }
	public static String buildEfficientPropertyValueList(Map<Integer,String> propertyValues, boolean maskSensitiveData) {
		return buildEfficientPropertyValueList(propertyValues, maskSensitiveData, PROPERTY_VALUE_SEPARATOR);
	}
	public static String buildEfficientPropertyValueList(Map<Integer, String> propertyValues, boolean maskSensitiveData, String propertyValueSeparator) {
		try {
			return appendEfficientPropertyValueList(new StringBuilder(), propertyValues, maskSensitiveData, propertyValueSeparator).toString();
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e, "Got an error on appending to a StringBuilder! This should never happen");
		}
	}
	public static Appendable appendEfficientPropertyValueList(Appendable appendTo, Map<Integer,String> propertyValues, boolean maskSensitiveData) throws IOException {
		return appendEfficientPropertyValueList(appendTo, propertyValues, maskSensitiveData, PROPERTY_VALUE_SEPARATOR);
	}
	public static Appendable appendEfficientPropertyValueList(Appendable appendTo, Map<Integer, String> propertyValues, boolean maskSensitiveData, String propertyValueSeparator) throws IOException {
	    Map<String,List<Integer>> valueOrdered = new LinkedHashMap<String, List<Integer>>();
		for(Map.Entry<Integer, String> entry : propertyValues.entrySet()) {
			String value = entry.getValue();
			if(maskSensitiveData && value != null && value.length() > 0) {
				try {
					DeviceProperty dp = DeviceProperty.getByValue(entry.getKey());
					switch(dp) {
						case ENCRYPTION_KEY:
							value = StringUtils.fillString(value.length(), "*");
							break;
					}
				} catch(InvalidIntValueException e) {
					//Ignore
				}
			}
			List<Integer> list = valueOrdered.get(value);
			if(list == null) {
				list = new ArrayList<Integer>();
				valueOrdered.put(value, list);
			}
			list.add(entry.getKey());
	    }
		for(Map.Entry<String,List<Integer>> entry : valueOrdered.entrySet()) {
			appendPropertyIndexList(appendTo, entry.getValue());
			if(entry.getKey() == null)
				appendTo.append(PROPERTY_DEFAULT);
			else
				appendTo.append(PROPERTY_EQUALS).append(entry.getKey());
			appendTo.append(propertyValueSeparator);
		}
		return appendTo;
	}
	public static StringBuilder appendPropertyIndexList(StringBuilder appendTo, Collection<Integer> propertyIndexes) {
		try {
			appendPropertyIndexList((Appendable)appendTo, propertyIndexes);
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e, "Got an error on appending to a StringBuilder! This should never happen");
		}
		return appendTo;
	}
	public static Appendable appendPropertyIndexList(Appendable appendTo, Collection<Integer> propertyIndexes) throws IOException {
		if(propertyIndexes.size() == 1) {
			appendTo.append(propertyIndexes.iterator().next().toString());
		} else {
			Integer[] indexes = propertyIndexes.toArray(new Integer[propertyIndexes.size()]);
			Arrays.sort(indexes);
			appendTo.append(indexes[0].toString());
			boolean inrange = false;
			for(int i = 1; i < indexes.length; i++) {
				if(indexes[i] == indexes[i-1] + 1) {
					if(!inrange) {
						inrange = true;
						appendTo.append(PROPERTY_RANGE);
					}
				} else if(inrange) {
					inrange = false;
					appendTo.append(indexes[i - 1].toString()).append(PROPERTY_SEPARATOR).append(indexes[i].toString());
				} else {
					appendTo.append(PROPERTY_SEPARATOR).append(indexes[i].toString());
				}
			}
			if(inrange)
				appendTo.append(indexes[indexes.length-1].toString());
		}
		return appendTo;
	}
	public static String formatForDisplay(String text, DeviceInfo deviceInfo) {
		return formatForDisplay(text, deviceInfo.getDeviceType());
	}
	public static String formatForDisplay(String text, DeviceType deviceType) {
		switch(deviceType) {
			case EDGE:
			case GX:
				return formatForDisplay(text, 16, 2, Justification.CENTER);
			default:
				return StringUtils.prepareDisplayable(text, true);
		}
	}
	public static String formatForDisplay(String responseMessage, int charsPerLine, int linesPerPage, Justification justification) {
		if(!StringUtils.isBlank(responseMessage)) {
			StringBuilder sb = new StringBuilder();
			int start = 0;
			int pos = 0;
			int n = 0;
			while(start < responseMessage.length()) {
				boolean hyphen = false;
				SPLIT: {
					for(pos = start; pos < start + charsPerLine; pos++)
						if(pos >= responseMessage.length() - 1) {
							pos++;
							break SPLIT;
						} else if(responseMessage.charAt(pos) == '\n')
							break SPLIT;
					for(; pos > start + 1; pos--) {
						char ch = responseMessage.charAt(pos);
						if(Character.getNumericValue(ch) > -1 && (ch <= ' ' || ch > '~'))
							break SPLIT;
					}
					pos = start + charsPerLine;
					if(pos >= responseMessage.length())
						pos = responseMessage.length();
					else if (Character.isLetter(responseMessage.charAt(pos)))
						hyphen = true;
				}
				n++;
				if(n > 1)
					sb.append('\r');
				if((n % linesPerPage) == 1)
					sb.append("\nP").append((n + 1) / linesPerPage).append('=');
				if(!hyphen && justification != null && justification != Justification.LEFT) {
					int sp = charsPerLine + start - pos;
					if(justification == Justification.CENTER)
						sp = sp / 2;
					for(int i = 0; i < sp; i++)
						sb.append(' ');
				}
				for(; start < pos; start++) {
					char ch = responseMessage.charAt(start);
					if(Character.getNumericValue(ch) < 0 || StringUtils.isDisplayable(ch))
						sb.append(ch);
					else
						sb.append(' ');
				}
				if(hyphen)
					sb.append('-');
				else
					pos++;
				start = pos;
			}
			sb.append("\r\n");
			if(n > linesPerPage) {
				String s = sb.toString();
				sb.setLength(0);
				sb.append("\nDS=04\nNL=");
				int pages = (n + 1) / linesPerPage;
				if(pages < 10)
					sb.append('0').append(pages);
				else
					sb.append(pages);
				sb.append(s);
			}
			return sb.substring(1, sb.length());
		}
		return responseMessage;
	}

	public static final String PROPERTY_VALUE_SEPARATOR = "\n";
	public static final char PROPERTY_SEPARATOR = '|';
	public static final char PROPERTY_EQUALS = '=';
	public static final char PROPERTY_DEFAULT = '!';
	public static final char PROPERTY_RANGE = '-';
}
