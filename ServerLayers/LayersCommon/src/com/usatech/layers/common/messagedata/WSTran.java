package com.usatech.layers.common.messagedata;



public interface WSTran {

	public long getTranId();

	public Number getAmount();
}
