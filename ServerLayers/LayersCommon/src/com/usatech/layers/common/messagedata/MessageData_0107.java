package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for QUEUE_TIMING_CHECK_RESPONSE - "Queue Timing Check Response - 0107h"
 */
public class MessageData_0107 extends AbstractMessageData {
	protected long totalTime;
	protected long startTime;
	protected long endTime;
	public class QueueData {
	protected long queueTime;
	protected long endTime;
		protected final int index;
		protected QueueData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setQueueTime(ProcessingUtils.readLongInt(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongInt(reply, getQueueTime());
		}

	public long getQueueTime() {
		return queueTime;
	}

	public void setQueueTime(long queueTime) {
		this.queueTime = queueTime;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("queueTime=").append(getQueueTime());
			sb.append(']');
			return sb.toString();
		}

		public long getEndTime() {
			return endTime;
		}

		public void setEndTime(long endTime) {
			if(endTime > 0) {
				long prevEndTime = index == 0 ? getStartTime() : queues.get(index - 1).getEndTime();
				if(prevEndTime > 0 && endTime >= prevEndTime)
					setQueueTime(endTime - prevEndTime);
				if(index + 1 < queues.size()) {
					QueueData qd = queues.get(index + 1);
					if(qd.getEndTime() > 0 && qd.getEndTime() >= endTime)
						qd.setQueueTime(qd.getEndTime() - endTime);
				}
			}
			this.endTime = endTime;
		}
	}
	protected final List<QueueData> queues = new ArrayList<QueueData>();
	protected final List<QueueData> queuesUnmod = Collections.unmodifiableList(queues);

	public MessageData_0107() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.QUEUE_TIMING_CHECK_RESPONSE;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTotalTime());
		ProcessingUtils.writeByteInt(reply, getQueues().size());
		for(QueueData queue : getQueues())
			queue.writeData(reply, maskSensitiveData);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTotalTime(ProcessingUtils.readLongInt(data));
		queues.clear();
		for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)
			addQueue().readData(data);
	}

	public long getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	public List<QueueData> getQueues() {
		return queuesUnmod;
	}

	public QueueData addQueue() {
		QueueData queue = new QueueData(queues.size());
		queues.add(queue);
		return queue;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; totalTime=").append(getTotalTime())
			.append("; queues=").append(getQueues());
		sb.append("]");

		return sb.toString();
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
		if(startTime > 0 && endTime > 0 && endTime >= startTime)
			setTotalTime(endTime - startTime);
		if(startTime > 0 && queues.size() > 0) {
			QueueData qd = queues.get(0);
			if(qd.getEndTime() > 0 && qd.getEndTime() >= startTime)
				qd.setQueueTime(qd.getEndTime() - startTime);
		}
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
		if(startTime > 0 && endTime > 0 && endTime >= startTime)
			setTotalTime(endTime - startTime);
	}
	public void setEndDate(Date endDate) {
		if(endDate != null)
			setEndTime(endDate.getTime());
	}
}
