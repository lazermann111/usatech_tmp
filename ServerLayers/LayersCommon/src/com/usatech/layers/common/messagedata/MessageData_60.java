package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCode60;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for AUTH_RESPONSE_2_0 - "Authorization Response 2.0 - 60h"
 */
public class MessageData_60 extends AbstractMessageData {
	protected long transactionId;
	protected AuthResponseCode60 responseCode = AuthResponseCode60.FAIL;

	public MessageData_60() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_RESPONSE_2_0;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByte(reply, getResponseCode().getValue());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setResponseCode(AuthResponseCode60.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public AuthResponseCode60 getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(AuthResponseCode60 responseCode) {
		this.responseCode = responseCode;
	}


	public void setAuthResultCd(AuthResultCode authResultCd) {
		setResponseCode(authResultCd.getAuthResponseCode60());
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; transactionId=").append(getTransactionId())
			.append("; responseCode=").append(getResponseCode());
		sb.append("]");

		return sb.toString();
	}
}
