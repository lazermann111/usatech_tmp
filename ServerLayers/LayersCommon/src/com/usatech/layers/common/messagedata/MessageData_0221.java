package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for WS2_RETOKENIZE_REQUEST - "Web Service - Retokenize Request - 0221h"
 */
public class MessageData_0221 extends WS2RequestMessageData implements CardIdCardReader {
	protected long cardId;
	protected String securityCode;
	protected String cardHolder;
	protected String billingPostalCode;
	protected String billingAddress;
	protected static final Validator<MessageData_0221> EMPTY_SECURITY_CD = new ConditionValidator<MessageData_0221>("ws.message.empty-security", "Empty security code") {
		@Override
		protected boolean isValid(MessageData_0221 value) {
			return !StringUtils.isBlank(value.getSecurityCode());
		}
	};

	public MessageData_0221() {
		super();
		addValidator(EMPTY_SECURITY_CD);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_RETOKENIZE_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getCardId());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getSecurityCode()) : getSecurityCode(), charset);
		ProcessingUtils.writeLongString(reply, getCardHolder(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getBillingPostalCode()) : getBillingPostalCode(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getBillingAddress()) : getBillingAddress(), charset);
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setCardId(data.getLong());
		setSecurityCode(ProcessingUtils.readLongString(data, charset));
		setCardHolder(ProcessingUtils.readLongString(data, charset));
		setBillingPostalCode(ProcessingUtils.readLongString(data, charset));
		setBillingAddress(ProcessingUtils.readLongString(data, charset));
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public MessageData_0310 createResponse() {
		MessageData_0310 response = new MessageData_0310();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", cardId=").append(getCardId())
			.append(", securityCode=").append(MessageDataUtils.maskString(getSecurityCode()))
			.append(", cardHolder=").append(getCardHolder())
			.append(", billingPostalCode=").append(MessageDataUtils.maskString(getBillingPostalCode()))
			.append(", billingAddress=").append(MessageDataUtils.maskString(getBillingAddress()))
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
