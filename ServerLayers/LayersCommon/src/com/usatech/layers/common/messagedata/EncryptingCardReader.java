package com.usatech.layers.common.messagedata;

public interface EncryptingCardReader extends CardReader {

	public byte[] getKeySerialNum();

	public int getDecryptedLength();

	public byte[] getEncryptedData();
}