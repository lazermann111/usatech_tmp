package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.ProcessingUtils;

/**
 *  Message Data for TERMINAL_UPDATE_STATUS - "Terminal Update Status - 90h"
 */
public class MessageData_90 extends AbstractMessageData {
	protected byte terminalUpdateStatus;

	public MessageData_90() {
		super();
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.TERMINAL_UPDATE_STATUS;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeByte(reply, getTerminalUpdateStatus());
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTerminalUpdateStatus(ProcessingUtils.readByte(data));
	}

	public byte getTerminalUpdateStatus() {
		return terminalUpdateStatus;
	}

	public void setTerminalUpdateStatus(byte terminalUpdateStatus) {
		this.terminalUpdateStatus = terminalUpdateStatus;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; terminalUpdateStatus=").append(getTerminalUpdateStatus() & 0xFF);
		sb.append("]");

		return sb.toString();
	}
}
