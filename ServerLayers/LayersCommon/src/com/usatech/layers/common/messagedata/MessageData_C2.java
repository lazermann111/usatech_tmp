package com.usatech.layers.common.messagedata;

import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import simple.lang.InvalidByteValueException;
import simple.lang.InvalidIntValueException;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.Language;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PaymentActionType;

/**
 *  Message Data for AUTH_REQUEST_4_1 - "Authorization Request 4.1 - C2h"
 */
public class MessageData_C2 extends AbstractMessageData implements AuthorizeMessageData {
	public interface CardReaderData extends Byteable, CardReader {
	}
	protected long transactionId;
	protected EntryType entryType = EntryType.UNSPECIFIED;
	protected BigDecimal amount;
	protected String validationData;
	
	private List<VASTag> tlv;
	
	public class GenericTracksCardReader implements PlainCardReaderV4, CardReaderData {
	protected String accountData1;
	protected String accountData2;
	protected String accountData3;
		protected GenericTracksCardReader() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setAccountData1(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData2(ProcessingUtils.readShortString(data, getCharset()));
		setAccountData3(ProcessingUtils.readShortString(data, getCharset()));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData1()) : getAccountData1(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData2()) : getAccountData2(), getCharset());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getAccountData3()) : getAccountData3(), getCharset());
		}

	public String getAccountData1() {
		return accountData1;
	}

	public void setAccountData1(String accountData1) {
		this.accountData1 = accountData1;
	}

	public String getAccountData2() {
		return accountData2;
	}

	public void setAccountData2(String accountData2) {
		this.accountData2 = accountData2;
	}

	public String getAccountData3() {
		return accountData3;
	}

	public void setAccountData3(String accountData3) {
		this.accountData3 = accountData3;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("accountData1=").append(MessageResponseUtils.maskTrackData(getAccountData1()))
			.append("; accountData2=").append(MessageResponseUtils.maskTrackData(getAccountData2()))
			.append("; accountData3=").append(MessageResponseUtils.maskTrackData(getAccountData3()));
			sb.append(']');
			return sb.toString();
		}
	}
	public class EncryptingTracksCardReader implements EncryptingCardReaderV4, CardReaderData {
	protected byte[] keySerialNum;
	protected int decryptedLength1;
	protected int decryptedLength2;
	protected int decryptedLength3;
	protected byte[] encryptedData1;
	protected byte[] encryptedData2;
	protected byte[] encryptedData3;
		protected EncryptingTracksCardReader() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setKeySerialNum(ProcessingUtils.readShortBytes(data));
		setDecryptedLength1(ProcessingUtils.readByteInt(data));
		setDecryptedLength2(ProcessingUtils.readByteInt(data));
		setDecryptedLength3(ProcessingUtils.readByteInt(data));
		setEncryptedData1(ProcessingUtils.readShortBytes(data));
		setEncryptedData2(ProcessingUtils.readShortBytes(data));
		setEncryptedData3(ProcessingUtils.readShortBytes(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeShortBytes(reply, getKeySerialNum());
		ProcessingUtils.writeByteInt(reply, getDecryptedLength1());
		ProcessingUtils.writeByteInt(reply, getDecryptedLength2());
		ProcessingUtils.writeByteInt(reply, getDecryptedLength3());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData1()) : getEncryptedData1());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData2()) : getEncryptedData2());
		ProcessingUtils.writeShortBytes(reply, maskSensitiveData ? MessageResponseUtils.maskFully(getEncryptedData3()) : getEncryptedData3());
		}

	public byte[] getKeySerialNum() {
		return keySerialNum;
	}

	public void setKeySerialNum(byte[] keySerialNum) {
		this.keySerialNum = keySerialNum;
	}

	public int getDecryptedLength1() {
		return decryptedLength1;
	}

	public void setDecryptedLength1(int decryptedLength1) {
		this.decryptedLength1 = decryptedLength1;
	}

	public int getDecryptedLength2() {
		return decryptedLength2;
	}

	public void setDecryptedLength2(int decryptedLength2) {
		this.decryptedLength2 = decryptedLength2;
	}

	public int getDecryptedLength3() {
		return decryptedLength3;
	}

	public void setDecryptedLength3(int decryptedLength3) {
		this.decryptedLength3 = decryptedLength3;
	}

	public byte[] getEncryptedData1() {
		return encryptedData1;
	}

	public void setEncryptedData1(byte[] encryptedData1) {
		this.encryptedData1 = encryptedData1;
	}

	public byte[] getEncryptedData2() {
		return encryptedData2;
	}

	public void setEncryptedData2(byte[] encryptedData2) {
		this.encryptedData2 = encryptedData2;
	}

	public byte[] getEncryptedData3() {
		return encryptedData3;
	}

	public void setEncryptedData3(byte[] encryptedData3) {
		this.encryptedData3 = encryptedData3;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("keySerialNum=").append(StringUtils.toHex(getKeySerialNum()))
			.append("; decryptedLength1=").append(getDecryptedLength1())
			.append("; decryptedLength2=").append(getDecryptedLength2())
			.append("; decryptedLength3=").append(getDecryptedLength3())
			.append("; encryptedData1=").append(new String(MessageResponseUtils.maskFully(getEncryptedData1()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; encryptedData2=").append(new String(MessageResponseUtils.maskFully(getEncryptedData2()), ProcessingConstants.US_ASCII_CHARSET))
			.append("; encryptedData3=").append(new String(MessageResponseUtils.maskFully(getEncryptedData3()), ProcessingConstants.US_ASCII_CHARSET));
			sb.append(']');
			return sb.toString();
		}
	}
	public class AppCardReader extends EBeaconAppCardReader implements CardReaderData { }
	public class GenericRawCardReader implements RawCardReader, CardReaderData {
	protected byte[] rawData;
		protected GenericRawCardReader() {
		}

		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		setRawData(ProcessingUtils.readLongBytes(data));
		}

		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		ProcessingUtils.writeLongBytes(reply, maskSensitiveData ? MessageResponseUtils.maskAnyCardNumbers(getRawData(), false) : getRawData());
		}

	public byte[] getRawData() {
		return rawData;
	}

	public void setRawData(byte[] rawData) {
		this.rawData = rawData;
	}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("rawData=").append(StringUtils.toHex(MessageResponseUtils.maskAnyCardNumbers(getRawData(), false)));
			sb.append(']');
			return sb.toString();
		}
	}

	public class EnhancedRawCardReader implements RawCardReader, CardReaderData {
		protected int parserVersion;
		protected int readerCommand;
		protected byte[] readerFirmware;
		protected Language language = Language.ENGLISH;
		protected byte[] rawData;

		protected EnhancedRawCardReader() {
		}
	
		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
			setParserVersion(ProcessingUtils.readShortInt(data));
			setReaderCommand(ProcessingUtils.readShortInt(data));
			setReaderFirmware(ProcessingUtils.readShortBytes(data));
			try {
				setLanguage(Language.getByValue(ProcessingUtils.readByteInt(data)));
			} catch(InvalidIntValueException e) {
				throw createParseException(e.getMessage(), data.position() - 1, e);
			}
			setRawData(ProcessingUtils.readLongBytes(data));
		}
	
		public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
			ProcessingUtils.writeShortInt(reply, getParserVersion());
			ProcessingUtils.writeShortInt(reply, getReaderCommand());
			ProcessingUtils.writeShortBytes(reply, getReaderFirmware());
			ProcessingUtils.writeByteInt(reply, getLanguage().getValue());
			ProcessingUtils.writeLongBytes(reply, maskSensitiveData ? MessageResponseUtils.maskAnyCardNumbers(getRawData(), false) : getRawData());
		}
	
		public int getParserVersion() {
			return parserVersion;
		}

		public void setParserVersion(int parserVersion) {
			this.parserVersion = parserVersion;
		}

		public int getReaderCommand() {
			return readerCommand;
		}

		public void setReaderCommand(int readerCommand) {
			this.readerCommand = readerCommand;
		}

		public byte[] getReaderFirmware() {
			return readerFirmware;
		}

		public void setReaderFirmware(byte[] readerFirmware) {
			this.readerFirmware = readerFirmware;
		}

		public Language getLanguage() {
			return language;
		}

		public void setLanguage(Language language) {
			this.language = language;
		}

		public byte[] getRawData() {
			return rawData;
		}
	
		public void setRawData(byte[] rawData) {
			this.rawData = rawData;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('[')
			.append("parserVersion=").append(getParserVersion())
			.append("; readerCommand=").append(getReaderCommand())
			.append("; readerFirmware=");
			if (getReaderFirmware() != null)
				sb.append(new String(getReaderFirmware(), getCharset()));
			sb.append("; language=").append(getLanguage())
			.append("; rawData=").append(StringUtils.toHex(MessageResponseUtils.maskAnyCardNumbers(getRawData(), false)))
			.append(']');
			return sb.toString();
		}
	}
	
	public static class VASTag implements CardReaderData {
		public static final int MERCHANT_ID_LENGTH = 32;
		private TagType tagType;
		private byte[] merchantId;
		private byte[] vasData;

		public TagType getTagType() {
			return tagType;
		}

		public void setTagType(TagType tagType) {
			this.tagType = tagType;
		}

		public void setTagType(byte tagType) {
			this.tagType = TagType.get(tagType);
		}

		public byte[] getMerchantId() {
			return merchantId;
		}

		public void setMerchantId(byte[] merchantId) {
			this.merchantId = merchantId;
		}

		public byte[] getVasData() {
			return vasData;
		}

		public void setVasData(byte[] data) {
			this.vasData = data;
		}

		@Override
		public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
			setTagType(ProcessingUtils.readByte(data));
			int tagLength = ProcessingUtils.readShortInt(data);
			setMerchantId(ProcessingUtils.readBytes(data, MERCHANT_ID_LENGTH));
			int vasDataLength = tagLength - MERCHANT_ID_LENGTH;
			setVasData(ProcessingUtils.readBytes(data, vasDataLength));
		}

		@Override
		public void writeData(ByteBuffer reply, boolean maskSensitiveData)
				throws IllegalStateException, BufferOverflowException {
			// Tag type
			ProcessingUtils.writeByte(reply, this.getTagType().getValue());
			// Tag length
			ProcessingUtils.writeShortInt(reply, MERCHANT_ID_LENGTH + this.getVasData().length);
			// Merchant ID
			ProcessingUtils.writeBytesRemaining(reply, this.getMerchantId());
			// Data
			ProcessingUtils.writeBytesRemaining(reply, this.getVasData());
		}
		
		public int getTagLength() {
			if (this.getMerchantId() == null || this.getVasData() == null) {
				return 0;
			}
			// Need to take into account tagName byte and tagLength bytes.
			return 1 + 2 + MERCHANT_ID_LENGTH + this.getVasData().length;
		}

		@Override
		public String toString() {
			return "[" + "tagType=" + tagType.name() + ", merchantId=" + StringUtils.toHex(merchantId) +
					", vasData=" + StringUtils.toHex(vasData) + ']';
		}
	}
	
	protected CardReaderType cardReaderType;
	protected CardReaderData cardReader;

	public MessageData_C2() {
		super();
		setCardReaderType(CardReaderType.GENERIC);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.AUTH_REQUEST_4_1;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongInt(reply, getTransactionId());
		ProcessingUtils.writeByteInt(reply, getEntryType().getValue());
		ProcessingUtils.writeStringAmount(reply, getAmount());
		ProcessingUtils.writeShortString(reply, maskSensitiveData ? maskValidationData(getValidationData()) : getValidationData(), getCharset());
		ProcessingUtils.writeByte(reply, getCardReaderType().getValue());
		if(getCardReader() != null)
			getCardReader().writeData(reply, maskSensitiveData);
		if (getEntryType() == EntryType.VAS) {
			writeVASData(reply, getTlv());
		}
	}
	
	private void writeVASData(ByteBuffer output, List<VASTag> tags) {
		if (tags == null || tags.size() == 0) {
			return;
		}
		// Overall length
		ProcessingUtils.writeShortInt(output, calculateTagsLength(tags));
		for (VASTag tag : tags) {
			tag.writeData(output, false);
		}
	}
	
	private int calculateTagsLength(List<VASTag> tags) {
		int ret = 0;
		for (VASTag tag : tags) {
			ret += tag.getTagLength();
		}
		
		return ret;
	}

	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		try {
			setEntryType(EntryType.getByValue(ProcessingUtils.readByteInt(data)));
		} catch(InvalidValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		setAmount(ProcessingUtils.readStringAmountNull(data));
		setValidationData(ProcessingUtils.readShortString(data, getCharset()));
		try {
			setCardReaderType(CardReaderType.getByValue(ProcessingUtils.readByte(data)));
		} catch(InvalidByteValueException e) {
			throw createParseException(e.getMessage(), data.position() - 1, e);
		}
		if(getCardReader() != null)
			getCardReader().readData(data);
		if (getEntryType() == EntryType.VAS) {
			setTlv(readVASData(data));
		}
	}

	private List<VASTag> readVASData(ByteBuffer data) throws BufferUnderflowException, ParseException {
		// Try to get tags length
		int tagsLength = getTwoBytesLength(data);
		if (tagsLength == 0) {
			return null;
		}

		// On ok create
		List<VASTag> ret = new ArrayList<>(1);
		int bytesRead = 0;
		while (bytesRead < tagsLength) {
			VASTag vasTag = new VASTag();
			vasTag.readData(data);
			bytesRead += vasTag.getTagLength();
			if (vasTag.getTagType() != null) {
				ret.add(vasTag);
			}
		}
		return ret;
	}

	private int getTwoBytesLength(ByteBuffer data) {
		int ret = 0;
		try {
			ret = ProcessingUtils.readShortInt(data);
		} catch (BufferUnderflowException ex) {
			// Ignore because it's ok not to have any VAS info.
		}
		return ret;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public EntryType getEntryType() {
		return entryType;
	}

	public void setEntryType(EntryType entryType) {
		this.entryType = entryType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getValidationData() {
		return validationData;
	}

	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}

	public CardReaderType getCardReaderType() {
		return cardReaderType;
	}

	public CardReaderData getCardReader() {
		return cardReader;
	}

	public void setCardReaderType(CardReaderType cardReaderType) {
		this.cardReaderType = cardReaderType;
		if (cardReaderType.isRawCardReader()) {
			if (cardReaderType.getValue() >= CardReaderType.IDTECH_VENDX_UNPARSED.getValue())
				this.cardReader = new EnhancedRawCardReader();
			else
				this.cardReader = new GenericRawCardReader();
		} else {
			switch(cardReaderType) {
				case GENERIC: this.cardReader = new GenericTracksCardReader(); break;
				case MAGTEK_MAGNESAFE: case IDTECH_SECUREMAG: case IDTECH_SECURED: case ACS_ACR31: case UIC_BEZEL: case OTI_BEZEL: case OTI_BEZEL_PARSED_BCD: case INGENICO_MOBILE: this.cardReader = new EncryptingTracksCardReader(); break;
				case BLUETOOTH: this.cardReader = new AppCardReader(); break;
				default:
					this.cardReader = new EncryptingTracksCardReader();
			}
		}
	}

		public String getPin() {
			return null;
		}
		public PaymentActionType getPaymentActionType() {
		    return PaymentActionType.PURCHASE;
		}
		protected String maskValidationData(String validationData) {
		    if(validationData == null)
		        return "";
		    if(getEntryType() == EntryType.ISIS){
		        return StringUtils.toHex(validationData.getBytes());
		    }else if(getEntryType() == EntryType.VAS){
	        return validationData;
		    }
		    return MessageResponseUtils.maskFully(validationData);
		}

	public List<VASTag> getTlv() {
			return tlv;
		}

		public void setTlv(List<VASTag> tlv) {
			this.tlv = tlv;
		}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append("; messageId=").append(getMessageId())
			.append("; transactionId=").append(getTransactionId())
			.append("; entryType=").append(getEntryType())
			.append("; amount=").append(getAmount())
			.append("; validationData=").append(maskValidationData(getValidationData()))
			.append("; cardReaderType=").append(getCardReaderType())
			.append("; cardReader=").append(getCardReader())
		  .append("; tlvs=").append(getTlv() == null ? "" : getTlv());
		sb.append("]");

		return sb.toString();
	}

    public enum TagType {
		V((byte)0x56); // 'V'

		private final byte value;

		private TagType(byte value) {
			this.value = value;
		}

		public byte getValue() {
			return this.value;
		}

		public static TagType get(byte value) {
			for (TagType code: values()) {
				if (code.getValue() == value) {
					return code;
				}
			}
			return null;
		}
	}
}
