package com.usatech.layers.common.messagedata;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.MessageType;

/**
 *  Message Data for WS2_TOKENIZE_ENCRYPTED_REQUEST - "Web Service - Tokenize Encrypted Request - 021Fh"
 */
public class MessageData_021F extends WS2TokenizeMessageData {
	protected final WSEncryptingCardReader cardReader = new WSEncryptingCardReader();

	public MessageData_021F() {
		super();
		cardReader.registerValidators(this);
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.MessageData#getMessageType()
	 */
	public MessageType getMessageType() {
		return MessageType.WS2_TOKENIZE_ENCRYPTED_REQUEST;
	}
	
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#writeData(java.nio.ByteBuffer,boolean)
	 */
	public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
		super.writeData(reply, maskSensitiveData);
		ProcessingUtils.writeLongString(reply, getUsername(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getPassword()) : getPassword(), charset);
		ProcessingUtils.writeLongString(reply, getSerialNumber(), charset);
		reply.putLong(getTranId());
		reply.putInt(getCardReaderTypeInt());
		reply.putInt(getDecryptedCardDataLen());
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageResponseUtils.maskTrackData(getEncryptedCardDataHex()) : getEncryptedCardDataHex(), charset);
		ProcessingUtils.writeLongString(reply, getKsnHex(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getBillingPostalCode()) : getBillingPostalCode(), charset);
		ProcessingUtils.writeLongString(reply, maskSensitiveData ? MessageDataUtils.maskString(getBillingAddress()) : getBillingAddress(), charset);
		ProcessingUtils.writeLongString(reply, getAttributes(), charset);
	}
	/**
	 * @see com.usatech.layers.common.messagedata.AbstractMessageData#readData(java.nio.ByteBuffer)
	 */
	public void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {
		super.readData(data);
		setUsername(ProcessingUtils.readLongString(data, charset));
		setPassword(ProcessingUtils.readLongString(data, charset));
		setSerialNumber(ProcessingUtils.readLongString(data, charset));
		setTranId(data.getLong());
		setCardReaderTypeInt(data.getInt());
		setDecryptedCardDataLen(data.getInt());
		setEncryptedCardDataHex(ProcessingUtils.readLongString(data, charset));
		setKsnHex(ProcessingUtils.readLongString(data, charset));
		setBillingPostalCode(ProcessingUtils.readLongString(data, charset));
		setBillingAddress(ProcessingUtils.readLongString(data, charset));
		setAttributes(ProcessingUtils.readLongString(data, charset));
	}

	public CardReaderType getCardReaderType() {
		return cardReader.getCardReaderType();
	}

	public CardReader getCardReader() {
		return cardReader;
	}

	public int getCardReaderTypeInt() {
		return cardReader.getCardReaderTypeInt();
	}

	public void setCardReaderTypeInt(int cardReaderTypeInt) {
		cardReader.setCardReaderTypeInt(cardReaderTypeInt);
	}

	public int getDecryptedCardDataLen() {
		return cardReader.getDecryptedCardDataLen();
	}

	public void setDecryptedCardDataLen(int decryptedCardDataLen) {
		cardReader.setDecryptedCardDataLen(decryptedCardDataLen);
	}

	public String getEncryptedCardDataHex() {
		return cardReader.getEncryptedCardDataHex();
	}

	public void setEncryptedCardDataHex(String encryptedCardDataHex) {
		cardReader.setEncryptedCardDataHex(encryptedCardDataHex);
	}

	public String getKsnHex() {
		return cardReader.getKsnHex();
	}

	public void setKsnHex(String ksnHex) {
		cardReader.setKsnHex(ksnHex);
	}

	public MessageData_0310 createResponse() {
		MessageData_0310 response = new MessageData_0310();
		response.setCharset(charset);
		response.setDirection(MessageDirection.SERVER_TO_CLIENT);
		response.setProtocol(protocol);
		response.setSerialNumber(serialNumber);
		return response;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageType())
			.append(": [messageNumber=").append(getMessageNumber() & 0xFF)
			.append(", protocol=").append(getProtocol())
			.append(", username=").append(getUsername())
			.append(", password=").append(MessageDataUtils.maskString(getPassword()))
			.append(", serialNumber=").append(getSerialNumber())
			.append(", tranId=").append(getTranId())
			.append(", cardReaderTypeInt=").append(getCardReaderTypeInt())
			.append(", decryptedCardDataLen=").append(getDecryptedCardDataLen())
			.append(", encryptedCardDataHex=<").append(getEncryptedCardDataHex() == null ? 0 : getEncryptedCardDataHex().length()).append(" bytes>")
			.append(", ksnHex=").append(getKsnHex())
			.append(", billingPostalCode=").append(MessageDataUtils.maskString(getBillingPostalCode()))
			.append(", billingAddress=").append(MessageDataUtils.maskString(getBillingAddress()))
			.append(", attributes=").append(getAttributes());
		sb.append("]");

		return sb.toString();
	}
}
