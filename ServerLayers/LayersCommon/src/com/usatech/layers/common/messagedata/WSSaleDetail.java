package com.usatech.layers.common.messagedata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simple.bean.ConvertException;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;
import simple.translator.Translator;

import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.messagedata.WSRequestMessageData.ExceptionValidator;
import com.usatech.layers.common.messagedata.WSRequestMessageData.Validator;


public class WSSaleDetail {
	protected static abstract class SaleDetailExceptionValidator<E extends Exception> extends ExceptionValidator<WSSale, E> {
		public SaleDetailExceptionValidator(String messageKey, String messageDefault) {
			super(messageKey, messageDefault);
		}

		@Override
		public void attempt(WSSale value) throws E {
			attempt(value.getSaleDetail());
		}

		protected abstract void attempt(WSSaleDetail value) throws E;
	}

	protected static final SaleDetailExceptionValidator<InvalidValueException> INVALID_TRAN_RESULT = new SaleDetailExceptionValidator<InvalidValueException>("ws.message.invalid-tran-result", "Invalid tranResult: {0.tranResult}") {
		public void attempt(WSSaleDetail value) throws InvalidValueException {
			if(StringUtils.isBlank(value.getTranResult()))
				throw new InvalidValueException("TranResult is blank", value.getTranResult());
			value.tranResultType = TranDeviceResultType.getByValue(value.getTranResult().charAt(0));
		}
	};
	protected static final Validator<WSSale> TOO_LONG_TRAN_DETAILS = new Validator<WSSale>() {
		public String validate(WSSale value, Translator translator) {
			if(WSRequestMessageData.getMaxTranDetailLength() > 0 && value.getSaleDetail().getTranDetails() != null && value.getSaleDetail().getTranDetails().length() > WSRequestMessageData.getMaxTranDetailLength())
				return translator.translate("ws.message.too-long-tran-details", "tranDetails length is too long (maximum is {1}): {0}", value.getSaleDetail().getTranDetails().length(), WSRequestMessageData.getMaxTranDetailLength());
			return null;
		}
	};
	protected static final SaleDetailExceptionValidator<ConvertException> INVALID_TRAN_DETAILS = new SaleDetailExceptionValidator<ConvertException>("ws.message.invalid-tran-details", "Invalid tranDetails") {
		@Override
		public void attempt(WSSaleDetail value) throws ConvertException {
			KioskLineItem.parseLineItems(value.tranDetails, value.lineItems);
			value.lineItemsValidated = true;
		}
	};
	protected String tranResult;
	protected TranDeviceResultType tranResultType;
	protected String tranDetails;
	protected boolean lineItemsValidated = true;
	protected final List<KioskLineItem> lineItems = new ArrayList<KioskLineItem>();
	protected final List<KioskLineItem> lineItemsUnmod = Collections.unmodifiableList(lineItems);

	public void registerValidators(WSRequestMessageData parent) {
		parent.addValidators(INVALID_TRAN_RESULT, TOO_LONG_TRAN_DETAILS, INVALID_TRAN_DETAILS);
	}

	public String getTranResult() {
		return tranResult;
	}

	public void setTranResult(String tranResult) {
		this.tranResult = tranResult;
		tranResultType = null;
	}

	public TranDeviceResultType getTransactionResult() {
		if(tranResultType == null)
			try {
				INVALID_TRAN_RESULT.attempt(this);
			} catch(InvalidValueException e) {
				return TranDeviceResultType.INVALID;
			}
		return tranResultType;
	}

	public List<? extends LineItem> getLineItems() {
		if(!lineItemsValidated)
			try {
				INVALID_TRAN_DETAILS.attempt(this);
			} catch(ConvertException e) {
				// lineitems will have been cleared. so do nothing
			}
		return lineItemsUnmod;
	}

	public KioskLineItem addLineItem() {
		KioskLineItem li = new KioskLineItem();
		lineItems.add(li);
		tranDetails = null;
		lineItemsValidated = true;
		return li;
	}

	public String getTranDetails() {
		if(tranDetails == null)
			tranDetails = KioskLineItem.formatLineItems(lineItems);
		return tranDetails;
	}

	public void setTranDetails(String tranDetails) {
		this.tranDetails = tranDetails;
		lineItemsValidated = false;
	}
}
