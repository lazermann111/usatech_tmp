/**
 *
 */
package com.usatech.layers.common.messagedata;

import java.nio.charset.Charset;

import com.usatech.layers.common.constants.MessageType;

/**
 * @author Brian S. Krug
 *
 */
public interface MessageData extends Byteable {
	public byte getMessageNumber() ;
	public void setMessageNumber(byte messageNumber) ;
	public MessageType getMessageType() ;
	public MessageDirection getDirection() ;
	public void setDirection(MessageDirection direction) ;
	public long getMessageId() ;
	public void setMessageId(long messageId) ;
	public Charset getCharset() ;
	public void setCharset(Charset charset) ;
	public long getMessageStartTime() ;
	public void setMessageStartTime(long messageStartTime) ;
	public long getMessageEndTime() ;
	public void setMessageEndTime(long messageEndTime) ;
}