package com.usatech.layers.common.messagedata;

import simple.lang.InvalidValueException;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;

public abstract class WS2AuthorizeMessageData extends WS2RequestMessageData implements AuthorizeMessageData, WSTran {
	protected static final ExceptionValidator<WS2AuthorizeMessageData, InvalidValueException> INVALID_ENTRY_TYPE = new ExceptionValidator<WS2AuthorizeMessageData, InvalidValueException>("ws.message.invalid-entry-type", "Invalid entryTypeString: {0.entryTypeString}") {
		public void attempt(WS2AuthorizeMessageData value) throws InvalidValueException {
			/*if(!ALLOWED_ENTRY_TYPES.contains(value.getEntryTypeString()))
				throw new InvalidValueException("Type not allowed", value.getEntryTypeString());
				*/
			if(StringUtils.isBlank(value.getEntryTypeString()))
				throw new InvalidValueException("Type not allowed", value.getEntryTypeString());
			value.entryType = EntryType.getByValue(value.getEntryTypeString().charAt(0));
		}
	};

	protected long tranId;
	protected EntryType entryType;
	
	public WS2AuthorizeMessageData() {
		this(true);
	}

	protected WS2AuthorizeMessageData(boolean validateAmount) {
		super();
		addValidator(INVALID_TRAN_ID);
		if(validateAmount)
			addValidator(INVALID_AMOUNT);
		addValidator(INVALID_ENTRY_TYPE);
		if(this instanceof PlainCardReader)
			addValidator(EMPTY_CARD_DATA);
	}

	public abstract String getEntryTypeString();
	
	public String getValidationData() {
		return null;
	}

	public long getTransactionId() {
		return getTranId();
	}

	public void setTransactionId(long transactionId) {
		setTranId(transactionId);
	}

	public EntryType getEntryType() {
		if(entryType == null)
			try {
				INVALID_ENTRY_TYPE.attempt(this);
			} catch(InvalidValueException e) {
				return EntryType.UNSPECIFIED;
			}
		return entryType;
	}

	public PaymentActionType getPaymentActionType() {
		return PaymentActionType.PURCHASE;
	}

	public String getPin() {
		return null;
	}

	public long getTranId() {
		return tranId;
	}

	public void setTranId(long tranId) {
		this.tranId = tranId;
	}
}
