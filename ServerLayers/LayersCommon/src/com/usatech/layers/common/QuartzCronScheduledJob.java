/** 
 * USA Technologies, Inc. 2011
 * QuartzScheduledThread.java by phorsfield, Aug 26, 2011 3:01:15 PM
 */
package com.usatech.layers.common;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.beans.IntrospectionException;
import java.text.ParseException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;

/**
 * Quartz jobs are separate from Triggers,
 * but we're not so independent. For
 * QuartzScheduledService, a CronTrigger
 * is always tied to a Job triggers
 * 
 * @author phorsfield
 *
 */
public abstract class QuartzCronScheduledJob implements Job {

	/* CRON-style string for schedule */
	private String cron;
	
	/* CronTrigger built from cron string */
	private CronTrigger cronTrigger;
	
	/* Quartz JobDetail group */
	private String group;
	
	/* id is held by the caller */
	private String id;
	
	/* Whether to reset the job on startup */
	private boolean forceReset;
	
	/* properties used to reconstruct this object when instantiated by Quartz */
	protected final Properties jobDataMap = new Properties();
	
	protected int misfireInstruction=CronTrigger.MISFIRE_INSTRUCTION_FIRE_ONCE_NOW;

	public Properties getJobDataMap() {
		return jobDataMap;
	}

	/**
	 * @return the cron
	 */
	public String getCron() {
		return cron;
	}

	/**
	 * @param cron the cron to set
	 */
	public void setCron(String cron) {
		this.cron = cron;
	}

	/**
	 * @return the cronTrigger
	 */
	public CronTrigger getCronTrigger() {
		return cronTrigger;
	}

	/**
	 * @param cronTrigger the cronTrigger to set
	 */
	public void setCronTrigger(CronTrigger cronTrigger) {
		this.cronTrigger = cronTrigger;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
		
	
	public boolean isForceReset() {
		return forceReset;
	}

	public void setForceReset(boolean forceReset) {
		this.forceReset = forceReset;
	}

	public void initializeQuartz(String id) throws ParseException
	{
		this.setId(id);
		CronTrigger trigger;
		if(misfireInstruction==CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING){
			trigger = newTrigger()
			    .withIdentity(id + "_trigger",group)
			    .withSchedule(cronSchedule(cron).withMisfireHandlingInstructionDoNothing())			    
			    .forJob(id,group)
			    .build();
		}else if (misfireInstruction==CronTrigger.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY){
			trigger = newTrigger()
				    .withIdentity(id + "_trigger",group)
				    .withSchedule(cronSchedule(cron).withMisfireHandlingInstructionIgnoreMisfires())			    
				    .forJob(id,group)
				    .build();
		}else{
			trigger = newTrigger()
				    .withIdentity(id + "_trigger",group)
				    .withSchedule(cronSchedule(cron).withMisfireHandlingInstructionFireAndProceed())			    
				    .forJob(id,group)
				    .build();
		}
		
		this.setCronTrigger(trigger);
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException
	{
		PropertiesConfiguration cfg = new org.apache.commons.configuration.PropertiesConfiguration();
		cfg.setDelimiterParsingDisabled(true);

		JobDataMap quartzJobMap = context.getMergedJobDataMap();
		
		@SuppressWarnings("unchecked")
		Set<Map.Entry<String,Object>> entrySet = quartzJobMap.entrySet();
		
		for(Map.Entry<String,Object> kv : entrySet)
		{
			cfg.addProperty(kv.getKey(), kv.getValue());
		}

		try {
			BaseWithConfig.configureProperties(this, cfg, null, false);
			executePostConfigure(context);
		} catch (SecurityException e) {
			throw new JobExecutionException(e);
		} catch (IllegalArgumentException e) {
			throw new JobExecutionException(e);
		} catch (IntrospectionException e) {
			throw new JobExecutionException(e);
		} catch (ParseException e) {
			throw new JobExecutionException(e);
		} catch (InstantiationException e) {
			throw new JobExecutionException(e);
		} catch (ConvertException e) {
			throw new JobExecutionException(e);
		} catch (ServiceException e) {
			throw new JobExecutionException(e);
		}
		
	}
		
	public abstract void executePostConfigure(JobExecutionContext context) throws JobExecutionException;

	public void setMisfireInstruction(int misfireInstruction) {
		this.misfireInstruction = misfireInstruction;
	}
	
}
