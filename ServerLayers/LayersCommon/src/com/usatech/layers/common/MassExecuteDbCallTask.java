package com.usatech.layers.common;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.crypto.CipherInputStream;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetHandler;
import simple.results.DatasetUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.BatchUpdateDatasetHandler;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class MassExecuteDbCallTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected String callId;
	protected int maxBatchSize = 100;
	protected ResourceFolder resourceFolder;
	protected boolean autoCommit = false;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
			byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);

			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.BLOCKING_RETRY, true);
			Resource inputResource = rf.getResource(resourceKey, ResourceMode.READ);
			try {
				CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, inputResource.getInputStream());
				try {
					int rows = -1;
					Connection conn = DataLayerMgr.getConnectionForCall(getCallId(), isAutoCommit());
					try {
						rows = DatasetUtils.parseDataset(new InputStreamByteInput(cis), createDatasetHandler(conn, step));
						log.info("Processed " + rows + " for mass update");
					} finally {
						if(rows < 0 && !conn.getAutoCommit())
							conn.rollback();
						conn.close();
					}
				} finally {
					cis.close();
				}
			} finally {
				inputResource.release();
			}
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	/**
	 * Can be overwritten for different behavior
	 * 
	 * @param conn
	 * @param step
	 * @return
	 * @throws ServiceException
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws AttributeConversionException
	 */
	protected DatasetHandler<ServiceException> createDatasetHandler(Connection conn, MessageChainStep step) throws ServiceException, SQLException, DataLayerException, AttributeConversionException {
		return new BatchUpdateDatasetHandler(conn, getCallId(), maxBatchSize);
	}

	public String getCallId() {
		return callId;
	}
	public void setCallId(String callId) {
		this.callId = callId;
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public boolean isAutoCommit() {
		return autoCommit;
	}

	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}
}
