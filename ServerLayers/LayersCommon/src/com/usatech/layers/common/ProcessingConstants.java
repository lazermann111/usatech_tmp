package com.usatech.layers.common;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;


public class ProcessingConstants {
	public static final String LOAD_BALANCER_MACHINE_ID = "LD-BLNCR";

	public static final String GLOBAL_EVENT_CODE_PREFIX = "A";
	public static final String SESSION_ATTRIBUTE_PREFIX = "\377";
	public static final Charset ISO8859_1_CHARSET = Charset.forName("ISO8859-1");
	//Charset Charset.forName("US-ASCII") below was causing issues with accented French characters
	public static final Charset US_ASCII_CHARSET = Charset.forName("ISO8859-1");

	public static final int EPORT_ENCRYPTION_KEY_LENGTH = 16;
	public static final String HASH_TYPE = "SHA-256";
	public static final String RANDOM_TYPE = "SHA1PRNG";

	public static final int MAX_COMMAND_EXECUTE_ORDER = Integer.MAX_VALUE;
	
	public static final int BEZEL_HOST_TYPE_ID = 400;
	public static final int BEZEL_HOST_PORT_NUM = 3;
	public static final int GPRS_MODEM_HOST_TYPE_ID = 202;
	public static final int CDMA_MODEM_HOST_TYPE_ID = 204;
	public static final int NIC_HOST_TYPE_ID = 205;
	public static final int MODEM_HOST_PORT_NUM = 2;
	
	public static final int PARTIAL_CASH_ITEM_TYPE = 206;
	public static final int REFERENCE_ITEM_TYPE = 403;
	
	//Add prefix to AVAS tags internally to avoid collisions as Apple hijacked some standard EMV tags
	public static final String AVAS_TAG_PREFIX = "FFFFFFFFFF";

	public static final Collection<Integer> COMMUNICATION_PROPERTY_INDEXES_PRE_19 = Arrays.asList(10, 30, 31, 32, 33);
	public static final Collection<Integer> COMMUNICATION_PROPERTY_INDEXES_19 =     Arrays.asList(10, 30, 31, 32, 33,         400,401,405,406,410,411,415,416, 420,421,425,426,430,431,435,436, 440,441,442,444,446, 450);
	public static final Collection<Integer> COMMUNICATION_PROPERTY_INDEXES_20 =     Arrays.asList(10, 30, 31, 32, 33, 34, 35, 400,401,405,406,410,411,415,416, 420,421,425,426,430,431,435,436, 440,441,442,444,446, 450);
	public static final Collection<Integer> COMMUNICATION_PROPERTY_INDEXES_21 =     Arrays.asList(10, 30, 31, 32, 33, 34, 35, 400,401,405,406,410,411,415,416, 420,421,425,426,430,431,435,436, 440,441,442,444,446, 450,451);
}
