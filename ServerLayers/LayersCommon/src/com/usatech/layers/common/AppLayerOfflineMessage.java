package com.usatech.layers.common;

import java.nio.charset.Charset;
import java.sql.SQLException;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.DeviceInfoProperty;

public class AppLayerOfflineMessage extends AbstractAppLayerMessage implements OfflineMessage {
	protected String timeZoneGuid;
	protected final Charset deviceCharset;
	
	public AppLayerOfflineMessage(final MessageChainTaskInfo taskInfo) throws ServiceException, AttributeConversionException {
		super(taskInfo);
		MessageChainStep step = taskInfo.getStep();
		timeZoneGuid = step.getAttribute(DeviceInfoProperty.TIME_ZONE_GUID, String.class, false);
		deviceCharset = step.getAttributeDefault(DeviceInfoProperty.DEVICE_CHARSET, Charset.class, ProcessingConstants.US_ASCII_CHARSET);
	}

	@Override
	public String getTimeZoneGuid() throws ServiceException {
		if(timeZoneGuid == null) {
			try {
				DataLayerMgr.selectInto("GET_TIMEZONE_BY_DEVICE_NAME", this);
			} catch(SQLException e) {
				throw new ServiceException("Could not get timezone for device '" + getDeviceName() + "' from the database", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not get timezone for device '" + getDeviceName() + "' from the database", e);
			} catch(BeanException e) {
				throw new ServiceException("Could not get timezone for device '" + getDeviceName() + "' from the database", e);
			}
		}
		return timeZoneGuid;
	}
	@Override
	public Charset getDeviceCharset() {
		return deviceCharset;
	}

	public void setTimeZoneGuid(String timeZoneGuid) {
		this.timeZoneGuid = timeZoneGuid;
	}
}