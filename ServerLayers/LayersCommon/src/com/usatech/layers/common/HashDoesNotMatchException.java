package com.usatech.layers.common;

public class HashDoesNotMatchException extends Exception {
	private static final long serialVersionUID = -6347815041250103806L;
	public HashDoesNotMatchException() {
		super();
	}

	public HashDoesNotMatchException(String message) {
		super(message);
	}

	public HashDoesNotMatchException(Throwable cause) {
		super(cause);
	}

	public HashDoesNotMatchException(String message, Throwable cause) {
		super(message, cause);
	}

}
