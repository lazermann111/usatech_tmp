/**
 *
 */
package com.usatech.layers.common;

import java.util.concurrent.locks.ReentrantLock;

import simple.app.ServiceException;
import simple.lang.InterruptGuard;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

/**
 * @author Brian S. Krug
 *
 */
public class BasicDeviceInfoManager implements DeviceInfoManager {
	protected final Cache<String, DeviceInfo, ServiceException> deviceInfos = new LockSegmentCache<String, DeviceInfo, ServiceException>() {
		/**
		 * @see simple.util.concurrent.LockSegmentCache#createValue(java.lang.Object, java.lang.Object[])
		 */
		@Override
		protected DeviceInfo createValue(String key, Object... additionalInfo) throws ServiceException {
			return createDeviceInfo(key);
		}
	};
	/**
	 * @see com.usatech.layers.common.DeviceInfoManager#getDeviceInfo(java.lang.String, boolean, boolean)
	 */
	@Override
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource) throws ServiceException {
		return getDeviceInfo(deviceName, onMissingPolicy, refreshFromSource, null, false);
	}
	@Override
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource, InterruptGuard guard, boolean byDeviceSerialCd) throws ServiceException {
		//Ignore refresh b/c we are assuming always up-to-date
		return deviceInfos.getOrCreate(deviceName, guard);
	}
	protected DeviceInfo createDeviceInfo(String deviceName) throws ServiceException {
		return new BasicDeviceInfo(deviceName) {
			protected final ReentrantLock lock = new ReentrantLock();
			@Override
			protected void lock() {
				lock.lock();
			}
			@Override
			protected void unlock() {
				lock.unlock();
			}
		};
	}
}
