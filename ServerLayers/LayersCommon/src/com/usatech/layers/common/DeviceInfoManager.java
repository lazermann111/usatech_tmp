package com.usatech.layers.common;

import simple.app.ServiceException;
import simple.lang.InterruptGuard;

public interface DeviceInfoManager {
	public static enum OnMissingPolicy { CREATE_NEW, RETURN_NULL, THROW_EXCEPTION };
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource) throws ServiceException ;
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource, InterruptGuard guard, boolean byDeviceSerialCd) throws ServiceException ;
}
