package com.usatech.layers.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.security.SecureRandom;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.mail.MessagingException;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.ComponentType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ESudsCycleType;
import com.usatech.layers.common.constants.ESudsManufacturer;
import com.usatech.layers.common.constants.ESudsRoomStatus;
import com.usatech.layers.common.constants.ESudsTopOrBottomType;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventDetailType;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.HostCounterType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ResultCode;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.SessionControlAction;
import com.usatech.layers.common.constants.TranBatchType;
import com.usatech.layers.common.constants.WasherDryerType;
import com.usatech.layers.common.messagedata.ESudsRoomStatusData;
import com.usatech.layers.common.messagedata.ESudsRoomStatusMessage;
import com.usatech.layers.common.messagedata.LegacyCounters;
import com.usatech.layers.common.messagedata.LegacyLocalAuth;
import com.usatech.layers.common.messagedata.LineItem;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.Sale;

import simple.app.BasicQoS;
import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call.BatchUpdate;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.StringUtils;
import simple.util.ConvertingHashMap;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

/**
 * @author bkrug
 *
 */
/**
 * @author bkrug
 * 
 */
public class LegacyUtils {
	private static final Log log = Log.getLog();
	public static final String PENDING_COMMAND_PREFIX="pendingCommand.";
	public static final String PENDING_COMMAND_STATE_ATTR="pendingCommandState";
	public static final String LAST_PENDING_COMMAND_SENT="lastPendingCommandSent";
	public static final String NEW_USERNAME = "newUsername";
	public static final String NEW_PASSWORD = "newPassword";

	//@Todo make the following configurable
	protected static final String esudsCompleteCycleQueueKey="usat.esuds.cycle.complete";
	protected static int eSudsScheduleMin=30;
	protected static int eSudsMaxScheduleMin=120;
	protected static String checkCountersQueueKey = "usat.load.fill.check.counters";
	protected static String loadFillQueueKey = "usat.load.fill";
	protected static int validDeviceTimeDays = 365;
	
	protected static final Pattern errDexPattern = Pattern.compile("^Err\\.DEXFill", Pattern.CASE_INSENSITIVE);

	protected final static Cache<String, Pattern, PatternSyntaxException> regexCache = new LockSegmentCache<String, Pattern, PatternSyntaxException>() {
		/**
		 * @see simple.util.concurrent.LockSegmentCache#createValue(java.lang.Object,
		 *      java.lang.Object[])
		 */
		@Override
		protected Pattern createValue(String key, Object... additionalInfo) throws PatternSyntaxException {
			return Pattern.compile(key, Pattern.DOTALL);
		}
	};

	public static Calendar getCalendarBySeconds(long timeSeconds, String timeZoneGuid){
		Calendar returnCalendar = Calendar.getInstance();
		returnCalendar.clear();
		returnCalendar.setTimeInMillis(timeSeconds*1000);
		TimeZone deviceTimeZone=TimeZone.getTimeZone(timeZoneGuid);
		returnCalendar.set(Calendar.DST_OFFSET, 0);
		returnCalendar.set(Calendar.ZONE_OFFSET, deviceTimeZone.getOffset(returnCalendar.getTimeInMillis()));
		return returnCalendar;
	}

	public static Calendar getCalendarByDate(Date date, String timeZoneGuid){
		return getCalendarBySeconds(date.getTime()/1000, timeZoneGuid);
	}

	public static byte[] getMessageDataBytes(MessageData data){
		ByteBuffer bb=ByteBuffer.allocate(2048);
		data.writeData(bb, true);
		bb.flip();
		byte[] returnData=new byte[bb.remaining()];
		bb.get(returnData);
		return returnData;
	}

	public static byte[] generateLegacyEncryptionKey(){
		String key=new String();
		SecureRandom rGen = new SecureRandom();
		while(key.length()<16){
			key += rGen.nextInt(Integer.MAX_VALUE);
		}
		key=key.substring(0, 16);
		return key.getBytes();
	}

	public static void createKioskInitConfigCommand(Map<String, Object> params, Connection conn, String deviceName, long deviceId) throws SQLException, DataLayerException {
		params.clear();
		params.put("deviceName", deviceName);
		params.put("deviceId", deviceId);
		params.put("kioskConfigName", StringUtils.toHex("EportNW.ini"));
		DataLayerMgr.executeCall(conn, "UPSERT_KIOSK_CONFIG_UPDATE", params);
	}

	public static void createPeekRequest(Map<String,Object> params, Connection conn, String deviceName, String command, int executeOrder)throws SQLException, DataLayerException{
		params.clear();
        params.put("deviceName", deviceName);
        params.put("dataType", MessageType.GX_PEEK.getHex());//87
        params.put("command", command);
        params.put("executeOrder", executeOrder);
        DataLayerMgr.executeCall(conn, "CREATE_LEGACY_REQUEST", params);
	}

	public static void createServerToClientRequest(Map<String,Object> params, Connection conn, String deviceName, String command, int executeOrder)throws SQLException, DataLayerException{
		params.clear();
        params.put("deviceName", deviceName);
        params.put("dataType", MessageType.SERVER_TO_CLIENT_REQUEST.getHex());//83
        params.put("command", command);
        params.put("executeOrder", executeOrder);
        DataLayerMgr.executeCall(conn, "CREATE_LEGACY_REQUEST", params);
	}
	protected static final Pattern peekContentPattern = Pattern.compile("^USA-G(51v5|x)");
	public static final String CMD_FIRMWARE_VERSION = "4400007F200000000F";
	public static final String CMD_UPLOAD_CONFIG = "4200000000000000FF";
	public static final String CMD_WAVECOM_MODEM_INFO = "4400802C0000000190";
	public static final String CMD_BOOTLOAD_APPVER = "4400807E9000000021";
	public static final String CMD_BEZEL_INFO = "4400807EB20000004C";

	/**
	 * @param command
	 * @param response
	 * @param deviceId
	 * @param conn
	 * @param deviceName
	 * @param localTime
	 * @return Whether device now needs to call in or not
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static boolean processPeekResponse(String command, String response, long deviceId, DeviceType deviceType, String deviceName, long localTime, Connection conn) throws SQLException, DataLayerException {
		if(command != null) {
			//replaced with what setup in InboundFileTransferAckTask
			if(command.equals(CMD_BOOTLOAD_APPVER)) { // Boot Loader/Diagnostic App Rev
				String[] revs = StringUtils.split(response, new char[] { '\n', '\r' }, false);
				if(revs != null && revs.length > 0 && revs[0] != null) {
					Map<String,Object> params = new HashMap<String, Object>();
					params.put("deviceId", deviceId);
					params.put("name", "Bootloader Rev");
					params.put("value", revs[0].trim());
					DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);
				}
				if(revs != null && revs.length > 1 && revs[1] != null) {
					Map<String,Object> params = new HashMap<String, Object>();
					params.put("deviceId", deviceId);
					params.put("name", "Diagnostic App Rev");
					params.put("value", revs[1].trim());
					DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);
				}
				return true;
			} else if(command.equals(CMD_BEZEL_INFO)) {
				try {
					AppLayerUtils.parseBezelInfo(response, deviceId, new HashMap<String, Object>(), localTime, conn);
					return true;
				} catch(ConvertException e) {
					log.error("Could not parse bezel info: '" + response + "'", e);
				}
			} else if(command.equals(CMD_FIRMWARE_VERSION)) {
				return parseFirmwareVersionInfo(response, deviceId, deviceType, deviceName, localTime, conn);
			} else if(command.equals(CMD_WAVECOM_MODEM_INFO)) {
				try {
					return parseModemInfo(response, deviceId, deviceType, deviceName, localTime, conn);
				} catch(ConvertException e) {
					log.error("Could not parse modem info: '" + response + "'", e);
				}
			}
		} else if(response.startsWith("USA-") && (deviceType == DeviceType.G4 || deviceType == DeviceType.GX)) {
			return parseFirmwareVersionInfo(response, deviceId, deviceType, deviceName, localTime, conn);
		} else {
			try {
				return parseModemInfo(response, deviceId, deviceType, deviceName, localTime, conn);
			} catch(ConvertException e) {
				log.error("Could not parse modem info: '" + response + "'", e);
			}
		}
		return false;
	}
	
	protected static boolean parseFirmwareVersionInfo(String response, long deviceId, DeviceType deviceType, String deviceName, long localTime, Connection conn) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceId", deviceId);
		params.put("name", "Firmware Version");
		params.put("value", response.trim());
		DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);

		// change from rerix version 1.23.2.3
		if(peekContentPattern.matcher(response).find()) {
			// peek for bootloader and diagnostic versions
			LegacyUtils.createPeekRequest(params, conn, deviceName, CMD_BOOTLOAD_APPVER, -95);
			log.debug("Added peek command for device '" + deviceName + "' for Bootloader and Diagnostic Version");
			return true;
		}
		return false;
	}
	protected static final Pattern MODEM_INFO_REGEX = Pattern.compile("HARDWAREVERSION|^<MODEM>|CGMI:TELIT", Pattern.MULTILINE);
	protected static final Pattern CSQ_REGEX = Pattern.compile("^CSQ:", Pattern.MULTILINE);
	protected static final Pattern ETHERNET_REGEX = Pattern.compile("^Macaddress:", Pattern.MULTILINE);
	public static final Pattern VALID_CCID_REGEX = Pattern.compile("^\\d{19,22}$");
	protected static final String[] MODEM_CODE_SETTINGS = {"CIMI","CNUM","IP","CSQ"};
	
	public static boolean parseModemInfo(String response, long deviceId, DeviceType deviceType, String deviceName, long localTime, Connection conn) throws ConvertException, SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<>();
		Map<String,String> modemInfo = new ConvertingHashMap<String,String>() {
			private static final long serialVersionUID = 123874917414L;

			protected String convertKey(Object key) {
				String skey = ConvertUtils.getStringSafely(key);
				return skey == null ? null : skey.trim();
			}
			protected String convertValue(Object value) {
				String svalue = ConvertUtils.getStringSafely(value);
				return svalue == null ? null : svalue.trim();
			}
		};
		if(MODEM_INFO_REGEX.matcher(response).find() || deviceType == DeviceType.KIOSK) {
			parseMultitechModemInfoNew(modemInfo, response);
		} else if(CSQ_REGEX.matcher(response).find()) {
			parseMultitechModemInfo(modemInfo, response);
		} else if(ETHERNET_REGEX.matcher(response).find()) {
			Map<String,String> values = ProcessingUtils.parseEndpointSubInformation(response);
			long hostId = AppLayerUtils.upsertHost(conn, params, deviceId, ProcessingConstants.MODEM_HOST_PORT_NUM,
					0, ProcessingConstants.NIC_HOST_TYPE_ID, values.get("Macaddress"), "Unknown", "Unknown", null, localTime, response);
			AppLayerUtils.clearHostSetting(conn, params, hostId);
			String version = values.get("VersionNo");
			if(version != null && version.trim().length() > 0)
				AppLayerUtils.upsertHostSetting(conn, params, hostId, "Firmware Version", version);
			removeGprsInfo(deviceId, conn);
			return true;
		} else
			parseMultitechModemInfoNew(modemInfo, response);
		int hostTypeId = ConvertUtils.getIntSafely(modemInfo.get("TYPE"), 0);
		String model = ConvertUtils.getString(modemInfo.get("CGMM"), null);
		String serialNum;
		if(hostTypeId < 206 || hostTypeId > 299) {
			if(StringUtils.isBlank(model))
				return false;
			if(AppLayerUtils.isCdmaModem(model)) {
				hostTypeId = ComponentType.CDMA_VERIZON_MODEM.getValue();
				serialNum = ConvertUtils.getString(modemInfo.get("MEID"), null);
				removeGprsInfo(deviceId, conn);
			} else {
				serialNum = ConvertUtils.getString(modemInfo.get("CCID"), null);
				if(serialNum != null && serialNum.startsWith("894453"))
					hostTypeId = ComponentType.GPRS_ESEYE_MODEM.getValue();
				else {
					String cimi = ConvertUtils.getString(modemInfo.get("CIMI"), null);
					if(cimi == null)
						hostTypeId = ComponentType.GPRS_MODEM.getValue();
					else if(cimi.startsWith("310380"))
						hostTypeId = ComponentType.GPRS_ATT_MODEM.getValue();
					else if(cimi.startsWith("310410"))
						hostTypeId = ComponentType.GPRS_ATT_MODEM.getValue();
					else if(cimi.startsWith("302720"))
						hostTypeId = ComponentType.GPRS_ROGERS_MODEM.getValue();
					else if(cimi.startsWith("234"))
						hostTypeId = ComponentType.GPRS_ESEYE_MODEM.getValue();
					else
						hostTypeId = ComponentType.GPRS_MODEM.getValue();
				}
				if(serialNum != null && VALID_CCID_REGEX.matcher(serialNum = serialNum.trim()).matches()) {
					params.put("CCID", serialNum);
					params.put("deviceId", deviceId);
					params.put("rssiTsMs", localTime);
					params.put("modemInfo", response.length() > 4000 ? response.substring(0, 4000 - 1) : response);
					params.put("CGSN", ConvertUtils.convertSafely(Number.class, modemInfo.get("CGSN"), null));
					DataLayerMgr.executeCall(conn, "UPDATE_GPRS_SETTINGS", params);
				} else {
					log.warn("Multitech Info    : Invalid ICCID; Skipping Update. Params:" + modemInfo + "; Data:" + response);
				}
			}
		} else {
			if(StringUtils.isBlank(model))
				model = "Unknown";
			serialNum = ConvertUtils.getString(modemInfo.get("MEID"), null);
			if(StringUtils.isBlank(serialNum)) {
				serialNum = ConvertUtils.getString(modemInfo.get("CCID"), null);
				if(!StringUtils.isBlank(serialNum) && VALID_CCID_REGEX.matcher(serialNum = serialNum.trim()).matches()) {
					params.put("CCID", serialNum);
					params.put("deviceId", deviceId);
					params.put("rssiTsMs", localTime);
					params.put("modemInfo", response.length() > 4000 ? response.substring(0, 4000 - 1) : response);
					params.put("CGSN", ConvertUtils.convertSafely(Number.class, modemInfo.get("CGSN"), null));
					DataLayerMgr.executeCall(conn, "UPDATE_GPRS_SETTINGS", params);
				} else {
					log.warn("Multitech Info    : Invalid ICCID; Skipping Update. Params:" + modemInfo + "; Data:" + response);
				}
			} else
				removeGprsInfo(deviceId, conn);
		}

		String manufacturer = ConvertUtils.getString(modemInfo.get("CGMI"), "Unknown");
		String version = ConvertUtils.getString(modemInfo.get("CGMR"), null);
		String label = ConvertUtils.getString(modemInfo.get("CGSN"), null);

		long hostId = AppLayerUtils.upsertHost(conn, params, deviceId, ProcessingConstants.MODEM_HOST_PORT_NUM, 0, hostTypeId, serialNum, manufacturer, model, label, localTime, response);
		AppLayerUtils.clearHostSetting(conn, params, hostId);
		if(version != null && version.trim().length() > 0)
			AppLayerUtils.upsertHostSetting(conn, params, hostId, "Firmware Version", version);
		for(String code : MODEM_CODE_SETTINGS) {
			String value = ConvertUtils.getString(modemInfo.get(code), null);
			if(value != null && value.trim().length() > 0)
				AppLayerUtils.upsertHostSetting(conn, params, hostId, code, value);
		}
		// don't insert the file into device.file_transfer
		conn.commit();
		return true;
	}

	protected static void removeGprsInfo(long deviceId, Connection conn) throws SQLException, DataLayerException {
		Map<String, Object> params = Collections.singletonMap("deviceId", (Object) deviceId);
		DataLayerMgr.executeCall(conn, "REMOVE_COMM_INFO", params);
	}

	public static Map<String, String> parseMultitechModemInfoNew(Map<String, String> params, String msg) {
		params.clear();
		String[] modemInfo = StringUtils.split(msg, new char[] { '\n', '\r' }, new char[] { '"' });
		for(int i = 0; i < modemInfo.length; i++) {
			if(modemInfo[i] != null) {
				String[] keyValue = StringUtils.split(modemInfo[i], ':', 1);
				if(keyValue != null && keyValue.length > 1) {
					params.put(keyValue[0], unquote(keyValue[1]));
				}
			}
		}
		return params;
	}

	protected static String unquote(String value) {
		if(value != null && value.length() > 1 && value.charAt(0) == '"' && value.charAt(value.length() - 1) == '"')
			return value.substring(1, value.length() - 1);
		return value;
	}

	public static Map<String, String> parseMultitechModemInfo(Map<String, String> params, String msg) {
		params.clear();
		String[] modemInfo = StringUtils.split(msg, new char[] { '\n', '\r' }, false);
		ArrayList<String> modemInfoList = new ArrayList<String>();
		for(int i = 0; i < modemInfo.length; i++) {
			if(modemInfo[i] != null && !modemInfo[i].equals("")) {
				String result = modemInfo[i].replaceAll("^(\\+|\\s)+", "");
				result = result.replaceAll("(\\s|\\:)+$", "");
				if(!result.equals("")) {
					modemInfoList.add(result);
				}
			}
		}
		for(int i = 0; i < modemInfoList.size(); i++) {
			String item = modemInfoList.get(i);
			if(item.matches("^CSQ$")) {
				String value = unquote(modemInfoList.get(i + 1));
				if(value != null)
					value = value.replaceAll("(CSQ|:| |)", "");
				params.put("CSQ", value);
				i++;
			} else if(item.matches("^CGSN$") || item.matches("^CIMI$") || item.matches("^CGMR$") || item.matches("^CGMM$") || item.startsWith("CGMI")) {
				params.put(item.substring(0, 4), unquote(modemInfoList.get(i + 1)));
				i++;
			} else if(item.startsWith("CNUM")) {
				String value = unquote(modemInfoList.get(i + 1));
				if(value != null)
					value = value.replaceAll("(CNUM|:| |\")", "");
				params.put("CNUM", value);
				i++;
			} else if(item.startsWith("CCID")) {
				String value = unquote(modemInfoList.get(i + 1));
				if(value != null)
					value = value.replaceAll("(CCID|:| |\")", "");
				params.put("CCID", value);
				i++;
			}
		}
		return params;
	}

	public static void sendKioskInitEmail(Publisher<ByteInput> publisher, Connection conn, String queueKey, String emailTo, String emailFrom, String messageTitle, long deviceId, String deviceName, String deviceSerialCd, String deviceInfo, String terminalInfo) throws BeanException, SQLException, DataLayerException, MessagingException, ServiceException{
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("deviceId", deviceId);
		DataLayerMgr.selectInto(conn, "GET_DEVICE_CUSTOMER_LOCATION", params);
		StringBuilder subject = new StringBuilder("AppLayer ").append(messageTitle).append(": new device initialization");
		StringBuilder body = new StringBuilder(subject)
		.append("\n\nDevice ID: ").append(deviceId)
		.append("\nDevice Type: Kiosk")
		.append("\nSerial Number: ").append(deviceSerialCd)
		.append("\nDevice Name: ").append(deviceName)
		.append("\nCustomer: ").append(params.get("customerName"))
		.append("\nLocation: ").append(params.get("locationName")).append("\n");
		if(deviceInfo.length()>0)
			body.append("\nDevice Info: ").append(deviceInfo);
		if(terminalInfo.length()>0)
			body.append("\nTerminal Info: ").append(terminalInfo);
		body.append("\n");
		AppLayerUtils.sendEmail(publisher, queueKey, emailTo, emailFrom, subject.toString(), body.toString());

	}

	public static void processAckMessage(Message message){
		Log log = message.getLog();
		try {
			Long commandId = message.getLastCommandId();
			if(commandId != null) {
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("deviceName", message.getDeviceName());
				params.put("pendingCommandId", commandId);
				params.put("globalSessionCode", message.getGlobalSessionCode());
				DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_SUCCESS", params, true);
				if(params.get("dataType") == null) {
					log.warn("Had pending command that needs ack " + commandId + " but could not find in database");
				}
			}
		} catch(SQLException e) {
			log.warn("Error while trying to process ack", e);
		} catch(DataLayerException e) {
			log.warn("Error while trying to process ack", e);
		} catch(BufferOverflowException e) {
			log.warn("Error while trying to process ack", e);
		}
	}

	public static void configPoke(Map<String,Object> params, Connection conn, String deviceName, int deviceTypeId) throws ServiceException{
		//do everything in the package function.
		try{
	    	params.clear();
	        params.put("deviceName", deviceName);
	        params.put("deviceTypeId", deviceTypeId);
	        DataLayerMgr.executeCall(conn, "CONFIG_POKE", params);
		}catch(SQLException e) {
			throw new ServiceException("Failed to get config poke.", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Failed to get config poke.", e);
		}
	}

	public static void insertCounters(Log log, Connection conn, Map<String, Object> params, long eventId, int componentNumber, LegacyCounters data) throws Exception{
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.CURRENCY_TRANSACTION.getValue(), String.valueOf(data.getTotalCurrencyTransactionCounter().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.CURRENCY_MONEY.getValue(), String.valueOf(data.getTotalCurrencyMoneyCounter().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.CASHLESS_TRANSACTION.getValue(), String.valueOf(data.getTotalCashlessTransactionCounter().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.CASHLESS_MONEY.getValue(), String.valueOf(data.getTotalCashlessMoneyCounter().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.PASSCARD_TRANSACTION.getValue(), String.valueOf(data.getTotalPasscardTransactionCounter().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.PASSCARD_MONEY.getValue(), String.valueOf(data.getTotalPasscardMoneyCounter().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.TOTAL_BYTES.getValue(), String.valueOf(data.getTotalBytesTransmitted().getValue()));
		AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, componentNumber, HostCounterType.TOTAL_SESSIONS.getValue(), String.valueOf(data.getTotalAttemptedSession().getValue()));
	}

	public static void processLocalAuth(MessageChainTaskInfo taskInfo, OfflineMessage message, long traceNumber) throws ServiceException {
		Log log = message.getLog();
		LegacyLocalAuth data = (LegacyLocalAuth)message.getData();
		Map<String, Object> params = new HashMap<String, Object>();
		boolean hasMatch = false;
		long posPtaId = -1;
		String trackData = MessageResponseUtils.cleanTrack(data.getCreditCardMagstripe());
		int minorCurrencyFactor=1;
		String currencyCd = null;
		Connection conn = AppLayerUtils.getConnection(true);
		long tranId=0;
		char tranImportNeeded = 'N';
		char sessionUpdateNeeded = 'N';
		try {
			long authTime = message.getServerTime();
			TimeZone timeZone = data.getSaleTimeZone();
			if(timeZone == null)
				timeZone = TimeZone.getTimeZone(message.getTimeZoneGuid());
			//sale time is device local time in legacy local auth messages			
			long saleTimeLocal = data.getSaleStartTime();
			long saleTimeUTC = ConvertUtils.getMillisUTC(saleTimeLocal, timeZone);
			//MEI devices send saleStartTime 0 which is invalid. A Gx device may send invalid saleStartTime if its RTC is incorrect. 
			long messageStartTimeMs=message.getServerTime();
			if(messageStartTimeMs - validDeviceTimeDays*24*60*60*1000L > saleTimeUTC || messageStartTimeMs < saleTimeUTC){
				log.warn("Received invalid sale timestamp: "+saleTimeUTC+", using server message start time: "+messageStartTimeMs);
				saleTimeUTC=messageStartTimeMs;
				saleTimeLocal=ConvertUtils.getLocalTime(saleTimeUTC, timeZone);
			}
			int saleTimeOffsetMin = (int)(saleTimeLocal - saleTimeUTC) / (60*1000);
			
			CardType cardType = data.getCardType();
			if (cardType == CardType.ERROR || cardType == CardType.INVENTORY_CARD) {
				long eventLocalTsMs = ConvertUtils.getLocalTime(message.getServerTime(), message.getTimeZoneGuid()); /* We must use this b/c what really matters is not when the button was pressed but when the counters were read.*/
				long eventId;
				if (errDexPattern.matcher(data.getCreditCardMagstripe()).find()) {
					eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), message.getGlobalSessionCode(), message.getDeviceName(), 0 /* Base Host*/, EventType.FILL.getValue(), EventType.FILL.getHostNumber(), EventState.COMPLETE_FINAL, data.getDeviceTranCd(), BooleanType.TRUE, BooleanType.TRUE, eventLocalTsMs,
							eventLocalTsMs);
					AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.EVENT_TYPE_VERSION, "1", -1);

					// check for counters v1 and if found send to load fill
					MessageChain loadMessageChain = new MessageChainV11();
					MessageChainStep checkStep = loadMessageChain.addStep(getCheckCountersQueueKey());
					checkStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
			    	
					MessageChainStep loadStep = loadMessageChain.addStep(getLoadFillQueueKey());
			    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
			    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, eventLocalTsMs);
					loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_TYPE, message.getDeviceType());
			    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, checkStep, LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD);
			    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, checkStep, LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR);
			    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, 'N');
			    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, checkStep, LoadDataAttrEnum.ATTR_COUNTERS_REPORTED);
			    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_DISPLAYED, false);
			    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, checkStep, LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT);
                	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, checkStep, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT);
                	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, checkStep, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT);
                	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, checkStep, LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS);
                	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, checkStep, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS);
                	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS, checkStep, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS);          	

                	checkStep.setNextSteps(5, loadStep);
					MessageChainService.publish(loadMessageChain, taskInfo.getPublisher());
				} else {
					eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), message.getGlobalSessionCode(), message.getDeviceName(), 0 /* Base Host*/, EventType.ERROR.getValue(), EventType.ERROR.getHostNumber(), EventState.COMPLETE_FINAL, data.getDeviceTranCd(), BooleanType.TRUE, BooleanType.TRUE, eventLocalTsMs,
							eventLocalTsMs);
					AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.EVENT_DATA, data.getCreditCardMagstripe(), -1);					
				}
				AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.HOST_EVENT_TIMESTAMP, null, saleTimeLocal);
			} else {
				List<CardType> cardTypes = CardType.findAll(data.getCardType().getEntryType(), data.getCardType().getPaymentActionType());
				params.put("deviceName", message.getDeviceName());
				params.put("paymentTypes", cardTypes);
				params.put("authTime", authTime);
				BigDecimal saleAmount = ConvertUtils.convert(BigDecimal.class, data.getSaleAmount());
				Results results;
				boolean original = taskInfo.setInterruptible(false);
				try {
					results = DataLayerMgr.executeQuery("GET_PTA_LIST_OFFLINE", params);
				} finally {
					taskInfo.setInterruptible(original);
				}
				OUTER: while(results.next()) {
					//if(checkAndProcessMatch(currentStep, results, message, dataAuth, authTime, tracks, startTime, null, validTransactionId, false) != null)
					posPtaId = results.getValue("posPtaId", Long.class);
					String cardRegex = results.getValue("cardRegex", String.class);
					Pattern regex;
					try {
						regex = regexCache.getOrCreate(cardRegex);
					} catch(PatternSyntaxException e) {
						log.warn("Invalid regex '" + cardRegex + "' for POS_PTA_ID " + posPtaId + "; skipping this record", e);
						continue;
					}
					if(trackData != null && trackData.length() > 0) {
						Matcher matcher = regex.matcher(trackData);
						if(matcher.matches()) {
							hasMatch=true;
							String authorityName = results.getValue("authorityName", String.class);
							if(message.getLog().isDebugEnabled())
								message.getLog().debug("Card matched regex '" + cardRegex + "' for authority '" + authorityName + "'");
							// if the requested amount is less then prefLimit (or prefLimit is null) then use the lesser of requested amount and prefAmount, else use requested Amount
							BigDecimal prefLimit = results.getValue("maxAuthAmt", BigDecimal.class);
							BigDecimal prefAmount = results.getValue("prefAuthAmt", BigDecimal.class);

							BigDecimal requestedAmount = saleAmount;
							if(prefLimit == null || prefLimit.compareTo(requestedAmount) > 0) {
								if(prefAmount != null && prefAmount.compareTo(requestedAmount) < 0)
									requestedAmount = prefAmount;
							}
							
							minorCurrencyFactor = results.getValue("minorCurrencyFactor", Integer.class);
							currencyCd = results.getValue("currencyCd", String.class);
							break OUTER;
						}
						if(message.getLog().isDebugEnabled())
							message.getLog().debug("Card did NOT match regex '" + cardRegex + "'");
						continue;
					}
				}
				if(!hasMatch){
					//do create pta for tran to get posPtaId
					params.clear();
					params.put("deviceName", message.getDeviceName());
					params.put("authTime", authTime);
					params.put("paymentType", data.getCardType().getValue());
					params.put("tranUtcTsMs", saleTimeUTC);
					DataLayerMgr.executeCall(conn, "CREATE_PTA_FOR_TRAN", params);
					posPtaId=ConvertUtils.getLong(params.get("posPtaId"));
					minorCurrencyFactor= ConvertUtils.getInt(params.get("minorCurrencyFactor"));
					currencyCd = ConvertUtils.getString(params.get("currencyCd"), false);
				}
		        List<? extends LineItem> lineItems=data.getLineItems();
		        params.clear();
		        params.put("globalSessionCode", message.getGlobalSessionCode());
				params.put("deviceName", message.getDeviceName());
		        params.put("globalEventCdPrefix", EventCodePrefix.APP_LAYER.getValue());
				params.put("posPtaId", posPtaId);
				params.put("deviceTranCd", data.getDeviceTranCd());
				params.put("saleUtcTsMs", saleTimeUTC);
				params.put("saleUtcOffsetMin", saleTimeOffsetMin);
				params.put("authResultCd", 'F');
				params.put("entryMethod", data.getCardType().getEntryType());
				params.put("paymentType", data.getCardType().getValue());
				params.put("trackData", MessageResponseUtils.maskCardNumber(trackData));
				params.put("authTime", authTime);
				//params.put("authorityRespCd", authorityRespCd);
				//params.put("authorityRespDesc", authorityRespDesc);
				//params.put("authAuthorityTranCd", authAuthorityTranCd);
				//params.put("authAuthorityRefCd", authAuthorityRefCd);
				params.put("traceNumber", traceNumber);
				params.put("minorCurrencyFactor", minorCurrencyFactor);
				params.put("authAmount", saleAmount);
				//balanceAmount null;
				//params.put("requestedAmount", data.getPrice());
				//params.put("approvedAmount", data.getPrice());
				params.put("deviceBatchId", 0);
				params.put("saleTypeCd", data.getCardType() == CardType.CASH ? SaleType.CASH.getValue() : SaleType.ACTUAL.getValue());
				//params.put("saleUtcTsMs", saleUtcTsMs);
				//params.put("saleUtcOffsetMin", saleUtcOffsetMin);
				params.put("tranDeviceResultTypeCd", data.getTransactionResult().getValue());
				params.put("saleResultId", SaleResult.CANCELLED_BY_AUTH_FAILURE.getValue());
				params.put("hashTypeCd", ProcessingConstants.HASH_TYPE);
			    params.put("tranLineItemHash", AppLayerUtils.getDataHashHex(AppLayerUtils.getLineItemBytes(lineItems)));
			    char tranBatchTypeCd = TranBatchType.ACTUAL.getValue();

				if(data.getCardType() != CardType.CASH) {
					MessageChainStep step = taskInfo.getStep();
					Long consumerAcctId = step.getAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_ID, Long.class, false);
					if(consumerAcctId == null) {
						Long globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
						if(globalAccountId != null) {
							params.put("globalAccountId", globalAccountId);
							params.put("instance", step.getAttribute(AuthorityAttrEnum.ATTR_INSTANCE, Object.class, false));
							params.put("currencyCd", currencyCd);
							try {
								DataLayerMgr.executeCall(conn, "GET_CONSUMER_ACCT_FOR_TRAN", params);
								conn.commit();
							} catch(SQLException e) {
								throw DatabasePrerequisite.createServiceException("Could not get consumerAcctId for the auth", e);
							} catch(DataLayerException e) {
								throw new RetrySpecifiedServiceException("Could not get consumerAcctId for the auth", e, WorkRetryType.NONBLOCKING_RETRY);
							}
							// consumerAcctId = ConvertUtils.convertSafely(Long.class, params.get("consumerAcctId"), null);
						}
					} else
						params.put("consumerAcctId", consumerAcctId);
				}
	            Object[] ret = DataLayerMgr.executeCall(conn, "CREATE_LOCAL_AUTH_SALE", params);
	    		ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[2]));
	    		String errorMessage = ConvertUtils.convert(String.class, ret[3]);
	    		if (errorMessage != null)
					log.warn(errorMessage);
	    		
	    		if(resultCd == ResultCode.SUCCESS) {
	    			tranId = ConvertUtils.convert(Long.class, ret[1]);
	    			log.info("Created local auth sale for tranId: " + tranId);
	    		} else if(resultCd == ResultCode.ILLEGAL_STATE)
	    			throw new RetrySpecifiedServiceException(errorMessage, WorkRetryType.NONBLOCKING_RETRY);
	    		else if(resultCd != ResultCode.DUPLICATE)
	    			throw new ServiceException(errorMessage);
	
	            if(tranId > 0) {
	            	BatchUpdate batchUpdate = DataLayerMgr.createBatchUpdate("CREATE_TRAN_LINE_ITEM", false, 0);
		            int lineItemOffsetSec = 0;
		            Map<String, Object> tliParams = new HashMap<String, Object>();
		            tliParams.put("tranId", tranId);
		            tliParams.put("tranBatchTypeCd", tranBatchTypeCd);
					tliParams.put("saleAmount", saleAmount);
	        		for (LineItem lineItem: lineItems){
						lineItemOffsetSec += lineItem.getDuration();
		            	tliParams.put("hostPortNum", lineItem.getComponentNumber());
		            	tliParams.put("tliTypeId", lineItem.getItem());
		            	tliParams.put("tliQuantity", lineItem.getQuantity());
		            	tliParams.put("tliAmount", lineItem.getPrice());
		            	tliParams.put("tliTax", BigDecimal.ZERO);
		            	tliParams.put("tliDesc", lineItem.getDescription());
		            	tliParams.put("tliUtcTsMs", saleTimeUTC + (lineItemOffsetSec * 1000));
		            	tliParams.put("tliUtcOffsetMin", saleTimeOffsetMin);
		            	tliParams.put("tliPositionCd", lineItem.getPosition());
		            	tliParams.put("tliSaleResultId", lineItem.getSaleResult().getValue());
		            	tliParams.put("hostPositionNum", lineItem.getComponentPosition());
						batchUpdate.addBatch(tliParams);
		        	}
	        		Sale sale=(Sale)message.getData();
	        		if(sale.getSaleTax().intValue()>0){
	        			tliParams.put("hostPortNum", 0);
		            	tliParams.put("tliTypeId", 611);
		            	tliParams.put("tliQuantity", 1);
		            	tliParams.put("tliAmount", sale.getSaleTax());
		            	tliParams.put("tliTax", BigDecimal.ZERO);
		            	tliParams.put("tliDesc", "Sale Tax");
		            	tliParams.put("tliUtcTsMs", saleTimeUTC + (lineItemOffsetSec * 1000));
		            	tliParams.put("tliUtcOffsetMin", saleTimeOffsetMin);
		            	tliParams.put("tliPositionCd", null);
		            	tliParams.put("tliSaleResultId", SaleResult.SUCCESS);
		            	tliParams.put("hostPositionNum", 0);
		            	batchUpdate.addBatch(tliParams);
	        		}
		            if (batchUpdate.getPendingCount() > 0)
		                batchUpdate.executeBatch(conn);
	
		            params.put("saleTax", data.getSaleTax());
		    		params.put("saleUtcTsMs", authTime);
					params.put("tranBatchTypeCd", TranBatchType.ACTUAL.getValue());
					params.put("saleAmount", saleAmount);
					params.put("saleDurationSec", lineItemOffsetSec);
					params.put("saleResultId", SaleResult.SUCCESS.getValue());
	
		    		ret = DataLayerMgr.executeCall(conn, "FINALIZE_SALE", params);
		    		resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, ret[1]));
		    		errorMessage = ConvertUtils.convert(String.class, ret[2]);
		    		if(errorMessage != null)
						log.warn(errorMessage);
		    		try {
		    			tranImportNeeded = ConvertUtils.convertRequired(Character.class, params.get("tranImportNeeded"));
		    		} catch(ConvertException e) {
		    			throw new ServiceException("Could not convert tranImportNeeded to a char", e);
		    		}
		    		try {
						sessionUpdateNeeded = ConvertUtils.convertRequired(Character.class, params.get("sessionUpdateNeeded"));
					} catch(ConvertException e) {
						throw new ServiceException("Could not convert sessionUpdateNeeded to a char", e);
					}
	
		    		if(resultCd == ResultCode.SUCCESS)
		    			log.info("Finalized sale for tranId: " + tranId);
		    		else
		    			throw new ServiceException(errorMessage);
					if(sale.getSaleType() != SaleType.INTENDED && message.getDeviceType() == DeviceType.ESUDS) {
						LegacyUtils.processESudsSaleCommon(message.getLog(), taskInfo.getPublisher(), tranId, conn);
					}
		    	}
			}

            conn.commit();
			if(tranImportNeeded == 'Y' && tranId > 0) {
				InteractionUtils.publishTranImport(tranId, taskInfo.getPublisher(), log);
				InteractionUtils.checkForReplenishBonuses(tranId, conn, taskInfo.getPublisher(), log);
			}
            if (sessionUpdateNeeded == 'Y') {
				params.put("saleGlobalSessionCode", params.get("globalSessionCode"));
				params.put("saleSessionStartTime", message.getSessionStartTime());
				InteractionUtils.publishSessionControl(SessionControlAction.ADD_TRAN_TO_DEVICE_SESSION.toString(), params, taskInfo.getPublisher(), log);
			}
		} catch(ServiceException e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            throw e;
        } catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
        	throw new ServiceException(e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
	}

	public static boolean upsertHostSettingCompare(Connection conn, Map<String, Object> params, long hostId, String hostSettingParameter, String hostSettingValue) throws Exception {
		params.clear();
		params.put("hostId", hostId);
		params.put("hostSettingParameter", hostSettingParameter);
		params.put("hostSettingValue", hostSettingValue);
		DataLayerMgr.executeCall(conn, "UPSERT_HOST_SETTING_COMPARE", params);
		return !ConvertUtils.areEqual(hostSettingValue, params.get("oldHostSettingValue"));
	}

	public static String getCyclePriceString(int cyclePrice){
		NumberFormat nFormat = NumberFormat.getInstance();
        nFormat.setMinimumFractionDigits(2);
        nFormat.setMaximumFractionDigits(2);
        BigDecimal price=new BigDecimal(cyclePrice);
        return nFormat.format(price.divide(new BigDecimal(100)));
	}

	public static MessageResponse processESudsRoomStatus(MessageChainTaskInfo taskInfo, Message message, Map<String, String> emailProps) throws ServiceException{
		Log log = message.getLog();
		ESudsRoomStatusMessage data = (ESudsRoomStatusMessage)message.getData();
        int startingPortNum = data.getStartingPortNumber();
        List<? extends ESudsRoomStatusData> roomStatusList = data.getRoomStatus();
		Connection conn = null;
		boolean isRoomLayoutRequested=false;
        try {
        	conn = AppLayerUtils.getConnection(false);
        	roomStatusListLabel:{
	        	for(int i=0; i<roomStatusList.size(); i++){
	        		int hostPortNum = i+startingPortNum;
	        		ESudsRoomStatusData roomStatus = roomStatusList.get(i);
	        		ESudsRoomStatus topStatus = roomStatus.getTop();
	        		ESudsRoomStatus bottomStatus = roomStatus.getBottom();
	        		if(topStatus==ESudsRoomStatus.EQUIP_STATUS_NO_STATUS_AVAILABLE&&bottomStatus==ESudsRoomStatus.EQUIP_STATUS_NO_WASHER_OR_DRYER_ON_PORT){
	        			log.info("Skipping: Port "+hostPortNum+" is empty.");
	        			continue;
	        		}else{
	        			Map<String, Object> params = new HashMap<String, Object>();
	        			DeviceInfo deviceInfo = message.getDeviceInfo();
	        			params.put("deviceName", deviceInfo.getDeviceName());
	        			params.put("effectiveTime", message.getServerTime());
	        			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
	                    long deviceId = ConvertUtils.getLong(params.get("deviceId"));
	                    //for bottom then top
	                    for(int pos=0; pos<2; pos++){
	                    	ESudsRoomStatus positionStatus;
	                    	if(pos==0){
	                    		positionStatus=bottomStatus;
	                    	}else{
	                    		positionStatus=topStatus;
	                    	}
	                    	params.clear();
	                		params.put("deviceId", deviceId);
	                		params.put("hostPortNum", hostPortNum);
	                        params.put("hostPositionNum", pos);
	                        DataLayerMgr.executeQuery("GET_HOST_ID_BY_POSITION", params, true);
	                    	int hostExists = ConvertUtils.getInt(params.get("exists"));
	                        if(hostExists>0){
	                        	int hostId = ConvertUtils.getInt(params.get("hostId"));
	                        	roomStatusUpdateHost(log, conn,data.isFullRoomStatus(), deviceId, hostId, hostPortNum, pos, positionStatus, emailProps, message.getPublisher());
	                        }else{
	                        	if(positionStatus!=ESudsRoomStatus.EQUIP_STATUS_NO_STATUS_AVAILABLE){
	                        		isRoomLayoutRequested=requestRoomLayout(conn, deviceInfo.getDeviceName());
		                            log.info("ESuds: room_status: No host record found for deviceId "+deviceId+" port:"+hostPortNum+" position:"+pos);
		                            break roomStatusListLabel;
	                        	}
	                        }
	                    }
	        		}
	        	}
        	}
        	conn.commit();
        	if(isRoomLayoutRequested){
        		return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
        	} else {
        		MessageProcessingUtils.writeLegacy2FGenericAck(message);
        	}
        	log.info("Successfully process the room status message.");
        } catch(ServiceException e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
        	log.error("Could not process room status message. Replying with GenericAck anyway.", e);
        	MessageProcessingUtils.writeLegacy2FGenericAck(message);
        } catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
        	log.error("Could not process room status message. Replying with GenericAck anyway.", e);
        	MessageProcessingUtils.writeLegacy2FGenericAck(message);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
        return MessageResponse.CONTINUE_SESSION;
	}

	public static void getBaseCyclePrice(Map<String, Object> params, int hostId, int baseCycleTypeId) throws DataLayerException, ConvertException, SQLException{
		params.clear();
		params.put("hostId", hostId);
		params.put("tranLineItemTypeId", baseCycleTypeId);
		params.put("hostSetting", " Price");
		DataLayerMgr.executeQuery("GET_HOST_SETTING_VALUE", params);
    	int priceExists = ConvertUtils.getInt(params.get("exists"));
        if(priceExists==0){
        	params.put("hostSettingValue", -1);
        }
	}

	public static void roomStatusUpdateHost(Log log, Connection conn, boolean isFullRoomStatus, long deviceId, int hostId, int hostPortNum, int hostPositionNum, ESudsRoomStatus positionStatus, Map<String, String> emailProps, Publisher<ByteInput> publisher) throws ServiceException{
		Map<String, Object> params = new HashMap<String, Object>();
		try{
			params.put("hostId", hostId);
			DataLayerMgr.executeQuery("GET_HOST_STATUS", params);
			//Todo Do I need to check exists as Rerix do?
			int hostExists = ConvertUtils.getInt(params.get("exists"));
            if(hostExists>0){
            	WasherDryerType hostType = ConvertUtils.convert(WasherDryerType.class, params.get("hostTypeCd"));
            	int hostStatusCd = ConvertUtils.getInt(params.get("hostStatusCd"));
            	ESudsRoomStatus hostStatus= ESudsRoomStatus.getByValue(hostStatusCd);
            	if(hostStatus==positionStatus){
            		log.info("No change for host record with hostId "+hostId+" port:"+hostPortNum+" position:"+hostPositionNum);
            	}else{
            		String description= ConvertUtils.getString(params.get("hostGroupTypeName"), "")+" "+ConvertUtils.getString(params.get("hostLabelCd"), "");
            		if(hostType.isStacked()){
            			if(hostPositionNum==ESudsTopOrBottomType.BOTTOM.getPosition()){
            				description="Bottom "+description;
            			}else{
            				description="Top "+description;
            			}
            		}
            		int baseCycleTypeId = hostType.getCycleTypeIdByPosition(hostPositionNum);
            		if((hostStatus==ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_FIRST||hostStatus==ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_SECOND)&&(positionStatus!=ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_FIRST&&positionStatus!=ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_SECOND&&positionStatus!=ESudsRoomStatus.EQUIP_STATUS_TRANSACTION_IN_PROGRESS)){
            			Calendar hostLastStartTs=ConvertUtils.convert(Calendar.class, params.get("hostLastStartTs"));
            			log.info("Cycle Finished!   :"+description);
            			params.clear();
            			params.put("hostId", hostId);
            			params.put("hostStatusCd", positionStatus.getValue());
            			int updated=DataLayerMgr.executeUpdate(conn, "UPDATE_CYCLE_FINISHED", params);
            			if(updated>0){
            				log.info("Updated hostId:"+hostId+" port:"+hostPortNum+" position:"+hostPositionNum+" to status:"+positionStatus);
            				log.info("Email: Sending cycle finished alert for "+description);
            				//get location name
            				params.clear();
            				params.put("deviceId", deviceId);
            				DataLayerMgr.executeQuery("GET_LOCATION_NAME", params);
            				int locationExists = ConvertUtils.getInt(params.get("exists"));
            				if(locationExists>0){
            					String locationName=ConvertUtils.getStringSafely(params.get("locationName"));
            					getBaseCyclePrice(params, hostId, baseCycleTypeId);
                				double cyclePrice=ConvertUtils.getDouble(params.get("hostSettingValue"));
                				if(cyclePrice<0){
    	                        	log.info("Unable to determine free-vend mode on/off flag for deviceId:"+deviceId+" hostId:"+ hostId);
    	                        }else if(cyclePrice>0){
    	                        	//regular mode
    	                        	//lookup email address
    	                        	params.clear();
    	            				params.put("hostId", hostId);
    	            				params.put("tranLineItemId", baseCycleTypeId);
    	            				DataLayerMgr.executeQuery("GET_EMAIL_ADDRESS", params);
    	            				int emailExists = ConvertUtils.getInt(params.get("exists"));
    	            				if(emailExists>0){
    	            					//send email
    	            					String emailTo=ConvertUtils.getStringSafely(params.get("emailTo"));
                    					if(emailTo!=null&&!emailTo.trim().equals("")){
                    						String emailPrimaryTo=ConvertUtils.getStringSafely(params.get("emailPrimaryTo"));
        	            					String firstName=ConvertUtils.getStringSafely(params.get("firstName"));
                    						params.put("passcodeType", "NOTIF_CONFIG");
                    						DataLayerMgr.executeUpdate(conn, "GET_ROOM_STATUS_PASSCODE", params);
                    						String passcode=ConvertUtils.getStringSafely(params.get("passcode"));
                    						String body="Hi "+firstName+",\n\nYour laundry is finished. "+
                    						"Please pick up your laundry from "+description+" in "+locationName+"\n\nThank you!\n\n"+
                    						"P.S. - If you wish to stop receiving these notifications or if you would like to "+
                    						"change the email address to which these notifications are sent, please visit the following link:\n\n\t"+
                    						"http://www.esuds.net/RoomStatus/notifyConfig.do?action=get&primaryEmailAddr="+URLEncoder.encode(emailPrimaryTo, "UTF-8")+"&"+
                    		                "hostStatusNotifyTypeId=2&passCode="+URLEncoder.encode(passcode, "UTF-8")+"\n";
                    						emailProps.put("emailTo", emailTo);
                    						emailProps.put("body", body);
                    						AppLayerUtils.sendEmail(publisher, emailProps);
                    					}
    	            				}
    	                        }
                				//lookup email addresses of other users who have requested cycle complete status
                				params.clear();
	            				params.put("hostId", hostId);
                				Results results = DataLayerMgr.executeQuery("GET_EMAIL_ADDRESS_OTHER_USER", params);
                				String body= "Hi!\n\nThis is the notification you requested from "+description+" in "+locationName+".\n\nThank you!";
                				while(results.next()){
                					String emailTo=ConvertUtils.getStringSafely(results.getValue("emailTo"));
                					if(emailTo!=null&&!emailTo.trim().equals("")){
                						emailProps.put("emailTo", emailTo);
                						emailProps.put("body", body);
                						AppLayerUtils.sendEmail(publisher, emailProps);
                					}
                					int hostStatusNotifQueueId = ConvertUtils.getInt(results.getValue("hostStatusNotifQueueId"));
                					params.clear();
                					params.put("hostStatusNotifQueueId", hostStatusNotifQueueId);
                					DataLayerMgr.executeCall(conn, "DELETE_HOST_STATUS_NOTIF", params);
                				}
            				}else{
            					log.info("ESuds: location not found for deviceId:"+deviceId);
            				}

            			}else{
            				log.info("Race condition avoided: host status NOT updated to "+positionStatus+" and NO cycle finished alert sent for hostId:"+hostId+" port:"+hostPortNum+" position:"+hostPositionNum);
            			}
            			long currentTime=System.currentTimeMillis();
            			long cycleSeconds=(currentTime-hostLastStartTs.getTimeInMillis())/1000;
            			BigDecimal cycleSecondsTs=new BigDecimal(cycleSeconds);
            			long cycleMin=cycleSecondsTs.divide(new BigDecimal(60), RoundingMode.HALF_UP).longValue();
            	        log.info("Calculated Time in minutes:"+cycleMin+" currentTimeMili:"+currentTime+" hostLastStartTsMili:"+hostLastStartTs.getTimeInMillis());
            	        if(cycleMin<15||cycleMin>120){
            	        	log.info("Cycle Sanity Check: "+cycleMin+" looks goofy, ignoring it");
            	        }else{
            	        	params.clear();
                			params.put("hostId", hostId);
                			DataLayerMgr.executeQuery("GET_CYCLE_TYPE", params);
                			int cycleTypeExists = ConvertUtils.getInt(params.get("exists"));
                			String cycleDesc=ConvertUtils.getString(params.get("tranLineItemDesc"), "");
                			int cycleTypeId= ConvertUtils.getInt(params.get("tranLineItemTypeId"), -1);
	                        if(cycleTypeExists>0){
	                        	log.info("Cycle Type/Time: hostId:"+hostId+" cycleDesc:"+cycleDesc+" cycleTypeId:"+cycleTypeId+" cycleTime(in minutes):"+cycleMin);
	                        	//match TLI_GROUP_NON_REGULAR_WASH_CYCLE_REGEX
	                        	ESudsCycleType cycleType=ESudsCycleType.getByValueSafely(cycleTypeId);
	                        	if(cycleType == null) {
	                        		log.info("Ignoring cycle type " + cycleTypeId + " because it is not a wash or dry cycle.");
	                        	} else {
		                        	if(!cycleType.isRegularWashCycle()){
		                        		cycleType=ESudsCycleType.TLI_REGULAR_WASH_CYCLE;
		                        		cycleTypeId=cycleType.getValue();
		                        		log.info("Adjusting cycle estimate for Regular Wash Cycle...");
		                        	}
		                        	params.clear();
		                			params.put("hostId", hostId);
		                			params.put("tranLineItemTypeId", cycleTypeId);
		                			params.put("lastTranCompleteMin", cycleMin);
		                			DataLayerMgr.executeCall(conn, "UPSERT_LAST_TRAN_CYCLE_TIME", params);
	                        	}
	                        } else{
	                        	log.info("Cycle Type: Not found for hostId:"+hostId);
	                        }
            	        }
            		}else if(positionStatus==ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_FIRST&&hostStatus!=ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_FIRST){
            			if(isFullRoomStatus&&hostStatus==ESudsRoomStatus.EQUIP_STATUS_IDLE_NOT_AVAILABLE&&hostType.getManufacturer()==ESudsManufacturer.SPEEDQUEEN){
            				log.info("Race condition avoided: host status NOT updated to "+positionStatus+" for hostId "+hostId+" port:"+hostPortNum+" position:"+hostPositionNum);
            				return;
            			}else{
            				log.info("Cycle Started!    :"+description);
                			params.clear();
                			params.put("hostId", hostId);
                			params.put("hostStatusCd", ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_FIRST.getValue());
                			int updated=DataLayerMgr.executeUpdate(conn, "UPDATE_HOST_STATUS", params);
                			if(updated>0){
                				log.info("Updated hostId:"+hostId+" port:"+hostPortNum+" position:"+hostPositionNum+" to status:"+positionStatus);
                			}else{
                				log.warn("Error updating hostId:"+hostId+" port:"+hostPortNum+" position:"+hostPositionNum+" to status:"+positionStatus);
                			}

                			getBaseCyclePrice(params, hostId, baseCycleTypeId);
                			double cyclePrice=ConvertUtils.getDouble(params.get("hostSettingValue"));
	                        if(cyclePrice<0){
	                        	log.info("Unable to determine free-vend mode on/off flag for deviceId:"+deviceId+" hostId:"+ hostId+". Skipping estimated time setting for this host.");
	                        }else{
	                        	if(cyclePrice==0){//free-vend mode
	                        		log.info("In free-vend mode for host:"+hostId+" tranLineItemTypeId:"+baseCycleTypeId);
	                        		//if in new cycle, determine the appropriate completion minutes and set it

	            					//if we have an average for this host, use it as the est completion time
	            					//if this is a new host, use the reported time
	            					//if we don't have either, set est completion minutes to zero
	                        		params.clear();
	                    			params.put("hostId", hostId);
	                    			params.put("tranLineItemTypeId", baseCycleTypeId);
	                    			DataLayerMgr.executeQuery("GET_HOST_AVE_TRAN_COMPLETE_MIN", params);
	                    			int hostAveTimeExists = ConvertUtils.getInt(params.get("exists"));
	                    			int hostEstCompleteMin=0;
	    	                        if(hostAveTimeExists>0){
	    	                        	hostEstCompleteMin= ConvertUtils.getInt(params.get("hostAveTranCompleteMin"));
	    	                        }else{
	    	                        	params.put("hostSetting", " Time");
	    	                        	DataLayerMgr.executeQuery("GET_HOST_SETTING_VALUE", params);
	    	                        	int timeExists = ConvertUtils.getInt(params.get("exists"));
	    	                        	if(timeExists>0){
	    	                        		hostEstCompleteMin=ConvertUtils.getInt(params.get("hostSettingValue"));
	    	                        	}
	    	                        }
	    	                        params.clear();
                        			params.put("hostId", hostId);
                        			params.put("hostEstCompleteMin", hostEstCompleteMin);
                        			DataLayerMgr.executeCall(conn, "UPDATE_HOST_EST_COMPLETE_MINUT", params);
                        			log.info("Setting estimated minutes to "+hostEstCompleteMin+" for hostId:"+hostId+" port:"+hostPortNum+" position:"+hostPositionNum);
                        			if(hostType.getManufacturer()==ESudsManufacturer.SPEEDQUEEN&&hostEstCompleteMin>0){
                        				// this is speed queen equipment which does not send a cycle complete status change notif
                						// we need to schedule a simulated cycle change notif here
                						// don't need to lookup start time since start time is right now, or relatively close to it
                        				//NetworkServices/middleware/esuds_cycle_complete_simulator add _schedule_simulated_cycle_complete logic
										addHostCycleCompleteSchedule(esudsCompleteCycleQueueKey, eSudsScheduleMin, System.currentTimeMillis(), hostId, hostEstCompleteMin, publisher);
                        			}
	                        	}else{//regular mode
	                        		// if we started a new cycle, zero out the completion minutes.  it should get populated
	            					// when we receive the batch
	            					// but what if we already received the batch?  then the status should be transaction in
	            					// progress, so check for that and don't zero out the time if it is
	                        		if(hostStatus!=ESudsRoomStatus.EQUIP_STATUS_TRANSACTION_IN_PROGRESS){
	                        			params.clear();
	                        			params.put("hostId", hostId);
	                        			params.put("hostEstCompleteMin", 0);
	                        			DataLayerMgr.executeCall(conn, "UPDATE_HOST_EST_COMPLETE_MINUT", params);
	                        			log.info("Setting estimated minutes to 0 for hostId:"+hostId+" port:"+hostPortNum+" position:"+hostPositionNum);
	                        		}
	                        	}

	                        }
            			}
            		}else{
            			//just update the status
            			params.clear();
            			params.put("hostId", hostId);
            			params.put("hostStatusCd", positionStatus.getValue());
            			int updated=DataLayerMgr.executeUpdate(conn, "UPDATE_HOST_STATUS", params);
            			if(updated>0){
            				log.info("Updated deviceId "+deviceId+" port:"+hostPortNum+" position:"+hostPositionNum+" to status:"+positionStatus);
            			}else{
            				log.warn("Error updating deviceId "+deviceId+" port:"+hostPortNum+" position:"+hostPositionNum+" to status:"+positionStatus);
            			}
            		}
            	}
            }else{
            	log.info("No host record found for deviceId "+deviceId+" port:"+hostPortNum+" position:"+hostPositionNum);
            }
		}catch(Exception e){
			throw new ServiceException("Failed to update host for room status.", e);
		}
	}

	public static boolean requestRoomLayout(Connection conn, String deviceName) throws DataLayerException, SQLException, ConvertException{
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("deviceName",deviceName);
        params.put("dataType", MessageType.SERVER_TO_CLIENT_REQUEST.getHex());//83
        params.put("command", "0C");//12 room status
        params.put("executeOrder", 1);
        DataLayerMgr.executeCall(conn, "CREATE_LEGACY_REQUEST", params);
        return ConvertUtils.getInt(params.get("commandAdded"))==1;
	}

	public static void processESudsSaleCommon(Log log, Publisher<ByteInput> publisher, long tranId, Connection conn) throws ServiceException, SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("tranId", tranId);
		params.put("washerDryerTypes", WasherDryerType.values());
        //get all line items that are regular wash cycles
        //NOTE: original code used WasherDryerType.getCycleTypeIdByPosition(position) to determine cycle type
        // I'm not sure why we can't use TRAN_LINE_ITEM_TYPE_ID - so for now that's what we'll use
        Results results = DataLayerMgr.executeQuery(conn, "GET_REGULAR_CYCLES_STARTED", params);
        while(results.next()) {
        	long hostId = results.getValue("hostId", Long.class);
        	Integer hostCycleTime = results.getValue("cycleTime", Integer.class);
        	if(hostCycleTime == null || hostCycleTime <= 0) {
        		params.clear();
    			params.put("hostId", hostId);
    			params.put("tranLineItemTypeId", results.getValue("lineItemType"));
    			DataLayerMgr.executeCall(conn, "GET_HOST_AVE_TRAN_COMPLETE_MIN", params);
        		int hostAveTimeExists = ConvertUtils.getInt(params.get("exists"));
                if(hostAveTimeExists > 0){
                	hostCycleTime = ConvertUtils.getInt(params.get("hostAveTranCompleteMin"));
                }
        	}
			if(hostCycleTime != null && hostCycleTime > -1) {
				int quantity = results.getValue("quantity", Integer.class);
				int totalCycleTime = hostCycleTime * quantity;
				ESudsCycleType cycleType = results.getValue("lineItemType", ESudsCycleType.class);
				if(cycleType == ESudsCycleType.TLI_TOP_OFF_DRY) {
					log.info("TopOffDry for host:" + hostId + " totalCycleTime:" + totalCycleTime + " hostCycleTime:" + hostCycleTime + " quantity:" + quantity);
					params.clear();
					params.put("hostEstCompleteMin", totalCycleTime);
					params.put("hostId", hostId);
					int updateHostEstComplete = DataLayerMgr.executeUpdate(conn, "UPDATE_SALE_HOST_EST_COMPLETE_MINUT", params);
					if(updateHostEstComplete == 0) {
						log.warn("Top-Off UPDATE_SALE_HOST_EST_COMPLETE_MINUT failed. totalCycleTime:" + totalCycleTime + " hostId:" + hostId);
					}
				} else if(totalCycleTime > 999) {
					log.warn("Cycle Time too high: " + totalCycleTime + " for hostId:" + hostId + "; not setting cycle time");
				} else {
					// new cycle
					log.info("Cycle Time Adjust : Setting cycle time to :" + totalCycleTime + " for hostId:" + hostId + " item:" + cycleType);
					params.clear();
					params.put("hostId", hostId);
					params.put("hostEstCompleteMin", totalCycleTime);
					DataLayerMgr.executeCall(conn, "UPDATE_HOST_EST_COMPLETE_MINUT", params);
					WasherDryerType hostType = results.getValue("hostType", WasherDryerType.class);
					if(hostType.getManufacturer() == ESudsManufacturer.SPEEDQUEEN) {
						Long startTime = results.getValue("hostStartTime", Long.class);
						if(startTime != null && startTime > 0) {
							// add _schedule_simulated_cycle_complete logic
							addHostCycleCompleteSchedule(startTime, hostId, totalCycleTime, publisher);
						} else {
							log.warn("Speed Queen Equip : Failed to lookup last_start_ts for hostId:" + hostId);
						}
        			}
        		}
        	}
        }
        log.info("Successfully processed eSuds common part.");
	}

	public static void addHostCycleCompleteSchedule(long startTime, long hostId, int hostEstCompleteMin, Publisher<ByteInput> publisher) throws ServiceException{
		addHostCycleCompleteSchedule(esudsCompleteCycleQueueKey, eSudsScheduleMin, startTime, hostId, hostEstCompleteMin, publisher);
	}
	
	public static void addHostCycleCompleteSchedule(String esudsCompleteCycleQueueKey, int scheduleMin, long startTime, long hostId, int hostEstCompleteMin, Publisher<ByteInput> publisher)
			throws ServiceException {
		MessageChain mc = new MessageChainV11();
		BasicQoS qos = new BasicQoS(0, BasicQoS.DEFAULT_PRIORITY, true, BasicQoS.EMPTY_MESSAGE_PROPERTIES);
		long time;
		if(hostEstCompleteMin > 0 && hostEstCompleteMin <= eSudsMaxScheduleMin) {
			time = startTime + hostEstCompleteMin * 60 * 1000L;
		} else {
			time = startTime + scheduleMin * 60 * 1000L;
		}
		qos.setAvailable(time);
		MessageChainStep step2 = mc.addStep(esudsCompleteCycleQueueKey);
		step2.addLongAttribute("hostId", hostId);
		step2.addIntAttribute("hostEstCompleteMin", hostEstCompleteMin);
		step2.addLongAttribute("startTime", startTime);

		MessageChainService.publish(mc, publisher, null, qos);
	}

	public static long getTimeZoneOffsetDiff(long time, TimeZone deviceTimeZone){
		TimeZone serverTimeZone=Calendar.getInstance().getTimeZone();
		long serverOffset=serverTimeZone.getOffset(time);
		long deviceOffset=deviceTimeZone.getOffset(time);
		return deviceOffset-serverOffset;
	}

	public static void updatePendingCommandSent(Connection conn, String deviceName, String dataType) throws ServiceException{
		Map<String,Object> params = new HashMap<String, Object>();
        params.put("setExecuteCd", "A");
        params.put("deviceName", deviceName);
        params.put("dataType", dataType);
        try{
        	DataLayerMgr.executeCall(conn, "UPDATE_PENDING_COMMAND_SENT", params);
        }catch(Exception e){
    		throw new ServiceException("Failed to update pending command sents.", e);
        }
	}

	public static int getESudsScheduleMin() {
		return eSudsScheduleMin;
	}

	public static void setESudsScheduleMin(int sudsScheduleMin) {
		eSudsScheduleMin = sudsScheduleMin;
	}

	public static int getESudsMaxScheduleMin() {
		return eSudsMaxScheduleMin;
	}

	public static void setESudsMaxScheduleMin(int sudsMaxScheduleMin) {
		eSudsMaxScheduleMin = sudsMaxScheduleMin;
	}

	public static String getEsudsCompleteCycleQueueKey() {
		return esudsCompleteCycleQueueKey;
	}

	public static Integer calculateCounterDifference(int currentValue, Integer previousValue, DeviceType deviceType) {
		if(previousValue == null) {
			log.debug("Previous counter value not provided - returning null");
			return null;
		} else if(currentValue < 0) {
			log.debug("Current counter value is negative - returning null");
			return null;
		} else if(currentValue < previousValue) {
			int minLimit;
			switch(deviceType) {
				case G4: minLimit = 100000000; break;
				case GX: minLimit = 1000000; break;
				case MEI:minLimit = 100000000; break;
				default: minLimit = 100000000; break;
			}
			if(minLimit < currentValue) {
				if(log.isDebugEnabled())
					log.debug("Current value is less previous but more than minimum limit. Counter values [prev=" + previousValue + "; current=" + currentValue + "] do not make sense - returning null");
				return null;
			}
			//calculate limit by getting the next power of 10 greater than the prev
			int limit = nextPow10(previousValue);
			if(log.isDebugEnabled()) 
				log.debug("Calculated limit = " + limit);

			if(limit < minLimit) {
				if(log.isDebugEnabled()) 
					log.debug("Calculated limit is less than minimum allowed. Counter values [prev=" + previousValue + "; current=" + currentValue + "] do not make sense - returning null");
				return null;
			}
			if(previousValue * 2 < limit) {
				if(log.isDebugEnabled()) 
					log.debug("Previous counter value (" + previousValue + ") is greater than current value but less than half of the calculated limit (" + limit + ") - returning null");
				return null;
			}
			if(limit < previousValue){
				if(log.isDebugEnabled()) 
					log.debug("Previous counter value is greater than calculated limit (" + limit + ") - returning null");
				return null;
			}
			return limit - previousValue + currentValue;
		} else
			return currentValue - previousValue;
	}

	protected static int nextPow10(int n) {
		if(n < 0)
			return 0;
		int pow10 = 1;
		while(pow10 <= n) {
			if(pow10 >= 1000000000)
				return -1;
			pow10 *= 10;
		}
		return pow10;
	}
	
	public static String getCheckCountersQueueKey() {
		return checkCountersQueueKey;
	}

	public static void setCheckCountersQueueKey(String checkCountersQueueKey) {
		LegacyUtils.checkCountersQueueKey = checkCountersQueueKey;
	}

	public static String getLoadFillQueueKey() {
		return loadFillQueueKey;
	}

	public static void setLoadFillQueueKey(String loadFillQueueKey) {
		LegacyUtils.loadFillQueueKey = loadFillQueueKey;
	}
	
	public static int getValidDeviceTimeDays() {
		return validDeviceTimeDays;
	}

	public static void setValidDeviceTimeDays(int validDeviceTimeDays) {
		LegacyUtils.validDeviceTimeDays = validDeviceTimeDays;
	}
}
