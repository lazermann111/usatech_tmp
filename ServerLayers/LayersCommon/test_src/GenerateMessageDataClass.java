

import java.awt.BorderLayout;
import java.awt.Font;
import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.lang.InvalidByteArrayException;
import simple.lang.InvalidByteValueException;
import simple.swt.BeanTableModel;
import simple.swt.ChangeableComboBoxModel;
import simple.swt.EnumCellEditor;
import simple.swt.TrackableListModel;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveHashMap;
import simple.util.FilterMap;
import simple.util.MapBackedSet;
import simple.xml.ObjectBuilder;

import com.usatech.layers.common.BCDInt;
import com.usatech.layers.common.CRC;
import com.usatech.layers.common.CRCLegacy;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.AbstractMessageData;
import com.usatech.layers.common.messagedata.Byteable;
import com.usatech.layers.common.messagedata.DeviceTypeSpecific;
import com.usatech.layers.common.messagedata.LongLengthByteable;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.layers.common.messagedata.ShortLengthByteable;
import com.usatech.layers.common.messagedata.VariableByteLengthNumber;

/**
 * @author Brian S. Krug
 *
 */
public class GenerateMessageDataClass {
	private static final Log log = Log.getLog();
	private static final long serialVersionUID = 114873443023954282L;
	protected static final boolean supportGetSetSensitiveData = false;
	private JPanel buttonPanel = null;
	private JLabel messageTypeLabel = null;
	private JPanel messageTypePanel = null;
	private JButton generateButton = null;
	private JButton cancelButton = null;
	private JPanel buttonFieldPanel = null;
	private JPanel fieldsPanel = null;
	private JButton fieldDeleteButton = null;
	private JButton fieldAddButton = null;
	private JTable fieldsTable = null;
	private JComboBox messageTypeSelect = null;
	private JScrollPane fieldsScrollPane = null;
	private BeanTableModel fieldsTableModel = null;
	private JPanel generatorPanel = null;
	//private ChangeListModel listModel = null;   //  @jve:decl-index=0:
	protected final TrackableListModel<Field> fields = new TrackableListModel<Field>();

	public static enum FieldType {
		BYTE(byte.class, "Byte", false),
		BYTE_INT(int.class, "ByteInt", false),
		BYTE_BCD(int.class, "ByteAsBCD", false),		
		SHORT_INT(int.class, "ShortInt", false),
		BYTES3_INT(int.class, "3ByteInt", false),
		LONG_INT(long.class, "LongInt", false),
		LONG_INT_REMAINING(Long.class, "LongIntRemaining", false),
		SHORT_STRING(String.class, "ShortString", true),
		LONG_STRING(String.class, "LongString", true),
		STRING_10BYTE(String.class, "10ByteString", true),
		STRING_3BYTE(String.class, "3ByteString", true),
		CRC(CRC.class, "CRC", false),
		TIMESTAMP(Calendar.class, "Timestamp", false),
		STRING_AMOUNT(BigDecimal.class, "StringAmountNull", "StringAmount", false),
		SHORT_BYTES(byte[].class, "ShortBytes", false),
		LONG_BYTES(byte[].class, "LongBytes", false),
		VAR_INT_REMAINING(VariableByteLengthNumber.class, "VarIntRemaining", false),
		STRING_REMAINING(String.class, "String", true) {
			/**
			 * @see GenerateMessageDataClass.FieldType#appendReadValue(java.lang.StringBuilder, java.lang.String)
			 */
			@Override
			protected StringBuilder appendReadValue(StringBuilder appendTo) {
				appendTo.append("ProcessingUtils.readString(data, data.remaining(), getCharset())");
				return appendTo;
			}
			/**
			 * @see GenerateMessageDataClass.FieldType#appendRead(java.lang.StringBuilder, GenerateMessageDataClass.Field, java.util.Set)
			 */
			@Override
			public StringBuilder appendRead(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
				if(field.isMultiple()) {
					String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
					appendTo.append("\t\t").append(field.getJavaName()).append(".clear();\n\t\twhile(data.hasRemaining())\n\t\t\tadd")
						.append(StringUtils.singular(methodBase)).append("().readData(data);\n");
					return appendTo;
				} else
					return super.appendRead(appendTo, field, imports);
			}
			/**
			 * @see GenerateMessageDataClass.FieldType#appendWrite(java.lang.StringBuilder, GenerateMessageDataClass.Field)
			 */
			@Override
			public StringBuilder appendWrite(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
				if(field.isMultiple()) {
					String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
					String itemName = StringUtils.singular(field.getJavaName());
					appendTo.append("\t\tfor(").append(field.getDataTypeName())
						.append(" ").append(itemName).append(" : get").append(methodBase).append("())\n\t\t\t").append(itemName)
						.append(".writeData(reply, maskSensitiveData);\n");
					return appendTo;
				} else
					return super.appendWrite(appendTo, field, imports);
			}
		},
		BYTES_REMAINING(byte[].class, "BytesRemaining", false) {
			/**
			 * @see GenerateMessageDataClass.FieldType#appendRead(java.lang.StringBuilder, GenerateMessageDataClass.Field, java.util.Set)
			 */
			@Override
			public StringBuilder appendRead(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
				if(field.isMultiple()) {
					String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
					appendTo.append("\t\t").append(field.getJavaName()).append(".clear();\n\t\twhile(data.hasRemaining())\n\t\t\tadd")
						.append(StringUtils.singular(methodBase)).append("().readData(data);\n");
					return appendTo;
				} else
					return super.appendRead(appendTo, field, imports);
			}
			/**
			 * @see GenerateMessageDataClass.FieldType#appendWrite(java.lang.StringBuilder, GenerateMessageDataClass.Field)
			 */
			@Override
			public StringBuilder appendWrite(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
				if(field.isMultiple()) {
					String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
					String itemName = StringUtils.singular(field.getJavaName());
					appendTo.append("\t\tfor(").append(field.getDataTypeName())
						.append(" ").append(itemName).append(" : get").append(methodBase).append("())\n\t\t\t").append(itemName)
						.append(".writeData(reply, maskSensitiveData);\n");
					return appendTo;
				} else
					return super.appendWrite(appendTo, field, imports);
			}
		},
		BIT_0to5_INT(byte.class, "Prev6BitInt", false),
		BIT_6to7_INT(byte.class, "2BitInt", false),
		BIT_0to3_INT(byte.class, "Prev4BitInt", false),
		BIT_4to7_INT(byte.class, "4BitInt", false),
		CURRENCY(Currency.class, "Currency", true),
		PROPERTY_VALUE_LIST(Map.class, "PropertyValueList", true) {
			/**
			 * @see GenerateMessageDataClass.FieldType#getDataTypeName()
			 */
			@Override
			public String getDataTypeName() {
				return "Map<Integer,String>";
			}
		},
		PROPERTY_INDEX_LIST(Set.class, "PropertyIndexList", true) {
			/**
			 * @see GenerateMessageDataClass.FieldType#getDataTypeName()
			 */
			@Override
			public String getDataTypeName() {
				return "Set<Integer>";
			}
		},
		DEVICE_TYPE_CHOICE(int.class, null, false) {
			@Override
			public StringBuilder appendGetter(StringBuilder appendTo, Field field) {
				if(field instanceof Choice && !field.isMultiple()) {
					Choice choice = (Choice)field;
					String itemName = StringUtils.singular(field.getJavaName());
					String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
					appendTo.append("\tpublic ").append(field.getDataTypeName());
					appendTo.append(" get").append(methodBase)
						.append("() {\n\t\tswitch(getDeviceType()) {\n");
					for(Case cs : choice.getCases()) {
						appendTo.append("\t\t\t");
						for(String value : cs.getValues())
							appendTo.append("case ").append(value).append(": ");
						appendTo.append("\n\t\t\t\tif(!(").append(itemName).append(" instanceof ").append(cs.getName()).append("))\n\t\t\t\t\t")
							.append(itemName).append(" = new ").append(cs.getName()).append("();\n\t\t\t\tbreak;\n");
					}
					appendTo.append("\t\t\tdefault: throw new IllegalArgumentException(\"The deviceType '\" + getDeviceType() + \"' is not supported\");\n\t\t}\n\t\t")
						.append("return ").append(itemName);
					appendTo.append(";\n\t}\n\n");
				}
				return appendTo;
			}
			/**
			 * @see GenerateMessageDataClass.FieldType#appendSetter(java.lang.StringBuilder, GenerateMessageDataClass.Field, java.lang.CharSequence)
			 */
			@Override
			public StringBuilder appendSetter(StringBuilder appendTo, Field field, CharSequence additional) {
				if(field instanceof Choice && field.isMultiple()) {
					Choice choice = (Choice)field;
					String itemName = StringUtils.singular(field.getJavaName());
					String methodBase = StringUtils.capitalizeFirst(itemName);
					appendTo.append("\tpublic ").append(field.getDataTypeName()).append(" add").append(methodBase)
						.append("() {\n\t\t").append(field.getDataTypeName()).append(' ').append(itemName)
						.append(";\n\t\tswitch(getDeviceType()) {\n");
					for(Case cs : choice.getCases()) {
						appendTo.append("\t\t\t");
						for(String value : cs.getValues())
							appendTo.append("case ").append(value).append(": ");
						appendTo.append(itemName).append(" = new ").append(cs.getName()).append('(').append(field.getJavaName()).append(".size()); break;\n");
					}
					appendTo.append("\t\t\tdefault: throw new IllegalArgumentException(\"The deviceType '\" + getDeviceType() + \"' is not supported\");\n\t\t}\n\t\t")
						.append(field.getJavaName()).append(".add(").append(itemName).append(");\n\t\treturn ").append(itemName).append(";\n\t}\n\n");
				}
				return appendTo;
			}
			/**
			 * @see GenerateMessageDataClass.FieldType#getDataTypeName()
			 */
			@Override
			public String getDataTypeName() {
				return null;
			}
		},
		BCD_TIMESTAMP(long.class, "BCDTimestamp", false),
		BYTE_TIMESTAMP(long.class, "ByteTimestamp", false),
		BCD_INT(BCDInt.class, "BCDInt", false),
		ENCRYPTION_KEY(byte[].class, "EncryptionKey", false),
		FILETRANSFER_ENCRYPTION_KEY(byte[].class, "FileTransferEncryptionKey", false),
		STRING_ARRAY(String[].class, "StringArray", false),
		SUB_TYPE(Object.class, null, false),
		IP(byte[].class, "IP", false),
		CRCLegacy(CRCLegacy.class, "CRCLegacy", false), 
		DIRECTION(MessageDirection.class, null, false) {
			@Override
			public StringBuilder appendGetter(StringBuilder appendTo, Field field) {
				if(field instanceof Choice) {// && !field.isMultiple() && !StringUtils.isBlank(field.getJavaName())
					Choice choice = (Choice) field;
					for(Case cs : choice.getCases()) {
						for(Field subfield : cs.fields)
							subfield.getFieldType().appendGetter(appendTo, subfield);
					}
				}
				return appendTo;
			}

			/**
			 * @see GenerateMessageDataClass.FieldType#appendSetter(java.lang.StringBuilder, GenerateMessageDataClass.Field, java.lang.CharSequence)
			 */
			@Override
			public StringBuilder appendSetter(StringBuilder appendTo, Field field, CharSequence additional) {
				if(field instanceof Choice) {// && !field.isMultiple() && !StringUtils.isBlank(field.getJavaName())
					Choice choice = (Choice) field;
					for(Case cs : choice.getCases()) {
						for(Field subfield : cs.fields) {
							CharSequence currentAdditional;
							String[] values = cs.getValues();
							if(values != null && values.length == 1 && !StringUtils.isBlank(values[0])) {
								StringBuilder sb = new StringBuilder();
								if(additional != null && additional.length() > 0)
									sb.append(additional);
								sb.append("\t\tsetDirection(MessageDirection.").append(values[0]).append(");\n");
								currentAdditional = sb.toString();
							} else
								currentAdditional = additional;
							subfield.getFieldType().appendSetter(appendTo, subfield, currentAdditional);
						}
					}
				}
				return appendTo;
			}

			/**
			 * @see GenerateMessageDataClass.FieldType#getDataTypeName()
			 */
			@Override
			public String getDataTypeName() {
				return null;
			}
			
			@Override
			public StringBuilder appendDefine(StringBuilder appendTo, StringBuilder initAppendTo, Field field, Set<Class<?>> imports, boolean init) {
				if(field instanceof Choice) {
					Choice choice = (Choice) field;
					for(Case cs : choice.getCases()) {
						for(Field subfield : cs.fields) {
							if(subfield.getEnumType() != null)
								imports.add(subfield.getEnumType());
							if(subfield.getFieldType().getDataType() != null && shouldImport(subfield.getFieldType().getDataType()))
								imports.add(subfield.getFieldType().getDataType());
							if(subfield.getMaskMethodClass() != null && shouldImport(subfield.getMaskMethodClass()))
								imports.add(subfield.getMaskMethodClass());
							if(subfield.getToStringMethodClass() != null && shouldImport(subfield.getToStringMethodClass()))
								imports.add(subfield.getToStringMethodClass());
							subfield.getFieldType().appendDefine(appendTo, initAppendTo, subfield, imports, init);
						}
					}
				}
				return appendTo;
			}

			@Override
			public StringBuilder appendRead(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
				if(field instanceof Choice) {
					Choice choice = (Choice) field;
					appendTo.append("\t\tswitch(getDirection()) {\n");
					for(Case cs : choice.getCases()) {
						appendTo.append("\t\t\t");
						for(String value : cs.getValues())
							appendTo.append("case ").append(value).append(": ");
						appendTo.append("\n");
						for(Field subfield : cs.fields)
							subfield.getFieldType().appendRead(appendTo, subfield, imports);
						appendTo.append("\n\t\t\t\tbreak;\n");
					}
					appendTo.append("\t\t\tdefault: throw new IllegalArgumentException(\"The direction '\" + getDirection() + \"' is not supported\");\n\t\t}\n");
				}
				return appendTo;
			}

			@Override
			public StringBuilder appendWrite(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
				if(field instanceof Choice) {
					Choice choice = (Choice) field;
					appendTo.append("\t\tswitch(getDirection()) {\n");
					for(Case cs : choice.getCases()) {
						appendTo.append("\t\t\t");
						for(String value : cs.getValues())
							appendTo.append("case ").append(value).append(": ");
						appendTo.append("\n");
						for(Field subfield : cs.fields)
							subfield.getFieldType().appendWrite(appendTo, subfield, imports);
						appendTo.append("\n\t\t\t\tbreak;\n");
					}
					appendTo.append("\t\t\tdefault: throw new IllegalArgumentException(\"The direction '\" + getDirection() + \"' is not supported\");\n\t\t}\n");
				}
				return appendTo;
			}

			@Override
			public StringBuilder appendGetSensitive(StringBuilder appendTo, Field field, String suffix, Set<Class<?>> imports) {
				if(field instanceof Choice) {// && !field.isMultiple() && !StringUtils.isBlank(field.getJavaName())
					Choice choice = (Choice) field;
					for(Case cs : choice.getCases()) {
						for(Field subfield : cs.fields)
							subfield.getFieldType().appendGetSensitive(appendTo, subfield, suffix, imports);
					}
				}
				return appendTo;
			}

			@Override
			public StringBuilder appendSetSensitive(StringBuilder appendTo, Field field, String suffix, Set<Class<?>> imports) {
				if(field instanceof Choice) {// && !field.isMultiple() && !StringUtils.isBlank(field.getJavaName())
					Choice choice = (Choice) field;
					for(Case cs : choice.getCases()) {
						for(Field subfield : cs.fields)
							subfield.getFieldType().appendSetSensitive(appendTo, subfield, suffix, imports);
					}
				}
				return appendTo;
			}

			@Override
			public StringBuilder appendToString(StringBuilder appendTo, Field field, Set<Class<?>> imports, boolean first) {
				if(field instanceof Choice) {
					Choice choice = (Choice) field;
					appendTo.append(";\n\t\tswitch(getDirection()) {\n");
					for(Case cs : choice.getCases()) {
						appendTo.append("\t\t\t");
						for(String value : cs.getValues())
							appendTo.append("case ").append(value).append(": ");
						if(!cs.fields.isEmpty()) {
							appendTo.append("\n\t\tsb");
							for(Field subfield : cs.fields) {
								subfield.getFieldType().appendToString(appendTo, subfield, imports, first);
								first = false;
							}
							appendTo.append(";");
						}
						appendTo.append("\n\t\t\t\tbreak;\n");
					}
					appendTo.append("\t\t\tdefault: throw new IllegalArgumentException(\"The direction '\" + getDirection() + \"' is not supported\");\n\t\t}\n\t\tsb.append(\"\")");
				}
				return appendTo;
			}
		},
		;
		protected final Class<?> dataType;
		protected final String readName;
		protected final String writeName;
		protected final boolean usingCharset;
		private FieldType(Class<?> dataType, String readWriteName, boolean usingCharset) {
			this.dataType = dataType;
			this.readName = readWriteName;
			this.writeName = readWriteName;
			this.usingCharset = usingCharset;
		}
		private FieldType(Class<?> dataType, String readName, String writeName, boolean usingCharset) {
			this.dataType = dataType;
			this.readName = readName;
			this.writeName = writeName;
			this.usingCharset = usingCharset;
		}
		public Class<?> getDataType() {
			return dataType;
		}
		public String getDataTypeName() {
			return dataType.getSimpleName();
		}

		public StringBuilder appendDefine(StringBuilder appendTo, StringBuilder initAppendTo, Field field, Set<Class<?>> imports, boolean init) {
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				choice.generateSubClass(appendTo, imports);
				appendDefine(appendTo, initAppendTo, choice.getDeterminant(), imports, true);
			} else if(field instanceof Multiple) {
				((Multiple)field).generateSubClass(appendTo, imports);
			} else if(field instanceof SubType) {
				((SubType)field).generateSubClass(appendTo, imports);
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			}
			if(field.isMultiple()) {
				imports.add(List.class);
				imports.add(ArrayList.class);
				imports.add(Collections.class);
				appendTo.append("\tprotected final List<").append(field.getDataTypeName()).append("> ").append(field.getJavaName()).append(" = new ArrayList<").append(field.getDataTypeName()).append(">();\n");
				appendTo.append("\tprotected final List<").append(field.getDataTypeName()).append("> ").append(field.getJavaName()).append("Unmod = Collections.unmodifiableList(").append(field.getJavaName()).append(");\n");
			} else {
				appendTo.append("\tprotected ");
				if(field instanceof SubType && !((SubType)field).isUpdatable()) {
					appendTo.append("final ");
				}
				appendTo.append(field.getDataTypeName()).append(' ').append(field.getJavaName());
				if(field.getDefaultExpression() != null) {
					if(init)
						initAppendTo.append("\n\t\tset").append(StringUtils.capitalizeFirst(field.getJavaName())).append("(").append(field.getDefaultExpression()).append(");");
					else
						appendTo.append(" = ").append(field.getDefaultExpression());
				} else if(field instanceof SubType)
					appendTo.append(" = new ").append(field.getDataTypeName()).append("()");

				appendTo.append(";\n");
				if(!StringUtils.isBlank(field.getOverrideName())) {
					appendTo.append("\tprotected ");
					appendTo.append(field.getDataTypeName()).append(' ').append(field.getOverrideName());
					appendTo.append(";\n");
				}
			}
			return appendTo;
		}
		public StringBuilder appendGetter(StringBuilder appendTo, Field field) {
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				appendGetter(appendTo, choice.getDeterminant());
			}
			if(field.getDataTypeName() != null) {
				String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
				appendTo.append("\tpublic ");
				if(field.isMultiple())
					appendTo.append("List<").append(field.getDataTypeName()).append('>');
				else
					appendTo.append(field.getDataTypeName());
				appendTo.append(" get").append(methodBase).append("() {\n\t\treturn ");
				if(!StringUtils.isBlank(field.getOverrideName()))
					appendTo.append(field.getOverrideName()).append(" == null ? ");
				appendTo.append(field.getJavaName());
				if(field.isMultiple())
					appendTo.append("Unmod");
				if(!StringUtils.isBlank(field.getOverrideName()))
					appendTo.append(" : ").append(field.getOverrideName());
				appendTo.append(";\n\t}\n\n");
				
				if(!StringUtils.isBlank(field.getOverrideName())) {
					appendTo.append("\tpublic ");
					appendTo.append(field.getDataTypeName()).append(" get").append(StringUtils.capitalizeFirst(field.getOverrideName())).append("() {\n\t\t\treturn ").append(field.getOverrideName());
					appendTo.append(";\n\t}\n\n");
				}
			}
			return appendTo;
		}

		public StringBuilder appendSetter(StringBuilder appendTo, Field field, CharSequence additional) {
			if(StringUtils.isBlank(field.getJavaName()))
				return appendTo;
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				if(!field.isMultiple()) {
					StringBuilder choose = new StringBuilder();
					choose.append("\t\tswitch(").append(choice.getDeterminant().getJavaName())
						.append(") {\n");
					for(Case cs : choice.getCases()) {
						choose.append("\t\t\t");
						for(String value : cs.getValues())
							choose.append("case ").append(value).append(": ");
						choose.append("this.").append(choice.getJavaName()).append(" = ");
						if(!cs.fields.isEmpty())
							choose.append("new ").append(cs.getName()).append("(); break;\n");
						else if(cs.superClass != null && Byteable.class.isAssignableFrom(cs.superClass) && !Modifier.isAbstract(cs.superClass.getModifiers()))
							choose.append("new ").append(cs.getName()).append("(); break;\n");
						else
							choose.append("null; break;\n");
					}
					choose.append("\t\t\tdefault: throw new IllegalArgumentException(\"The ")
						.append(StringUtils.capitalizeFirst(choice.getDeterminant().getJavaName()))
						.append(" '\" + ").append(choice.getDeterminant().getJavaName())
						.append(" + \"' is not supported\");\n\t\t}\n");
					if(additional != null)
						choose.append(additional);
					additional = choose;
				} else {
					String itemName = StringUtils.singular(field.getJavaName());
					String methodBase = StringUtils.capitalizeFirst(itemName);
					appendTo.append("\tpublic ").append(field.getDataTypeName()).append(" add").append(methodBase)
						.append("() {\n\t\t").append(field.getDataTypeName()).append(' ').append(itemName)
						.append(";\n\t\tswitch(get").append(StringUtils.capitalizeFirst(choice.getDeterminant().getJavaName()))
						.append("()) {\n");
					for(Case cs : choice.getCases()) {
						appendTo.append("\t\t\t");
						for(String value : cs.getValues())
							appendTo.append("case ").append(value).append(": ");
						appendTo.append(itemName).append(" = new ").append(cs.getName()).append('(').append(field.getJavaName()).append(".size()); break;\n");
					}
					appendTo.append("\t\t\tdefault: throw new IllegalArgumentException(\"The ")
						.append(StringUtils.capitalizeFirst(choice.getDeterminant().getJavaName()))
						.append(" '\" + get").append(StringUtils.capitalizeFirst(choice.getDeterminant().getJavaName()))
						.append("() + \"' is not supported\");\n\t\t}\n\t\t")
						.append(field.getJavaName()).append(".add(").append(itemName).append(");\n\t\treturn ").append(itemName).append(";\n\t}\n\n");
				}
				appendSetter(appendTo, choice.getDeterminant(), additional);
			} else if(field instanceof SubType && !((SubType)field).isUpdatable()) {
				return appendTo;
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			} else if(field.isMultiple()) {
				String itemName = StringUtils.singular(field.getJavaName());
				String methodBase = StringUtils.capitalizeFirst(itemName);
				appendTo.append("\tpublic ").append(field.getDataTypeName()).append(" add").append(methodBase)
					.append("() {\n\t\t").append(field.getDataTypeName()).append(' ').append(itemName)
					.append(" = new ").append(field.getDataTypeName()).append('(').append(field.getJavaName()).append(".size());\n\t\t")
					.append(field.getJavaName()).append(".add(").append(itemName).append(");\n\t\treturn ").append(itemName).append(";\n\t}\n\n");
			} else {
				String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
				appendTo.append("\tpublic void set").append(methodBase).append('(').append(field.getDataTypeName()).append(' ').append(field.getJavaName())
					.append(") {\n\t\tthis.").append(field.getJavaName()).append(" = ").append(field.getJavaName()).append(";\n");
				if(additional != null)
					appendTo.append(additional);
				if(field.getOnUpdate() != null)
					appendTo.append(field.getOnUpdate());
				appendTo.append("\t}\n\n");
				if(!StringUtils.isBlank(field.getOverrideName())) {
					appendTo.append("\tpublic void set").append(StringUtils.capitalizeFirst(field.getOverrideName())).append('(').append(field.getDataTypeName()).append(' ').append(field.getOverrideName())
						.append(") {\n\t\tthis.").append(field.getOverrideName()).append(" = ").append(field.getOverrideName()).append(";\n");
					appendTo.append("\t}\n\n");
				}
			}
			return appendTo;
		}
		public StringBuilder appendRead(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
			String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				appendRead(appendTo, choice.getDeterminant(), imports);
				if(field.isMultiple()) {
					appendTo.append("\t\t").append(field.getJavaName()).append(".clear();\n\t\tfor(int i = 0, n = ");
					choice.getMultipleFieldType().appendReadValue(appendTo);
					appendTo.append("; i < n; i++)\n\t\t\tadd").append(StringUtils.singular(methodBase)).append("().readData(data);\n");
				} else {
					if(choice.isNullable())
						appendTo.append("\t\tif(get").append(methodBase).append("() != null)\n\t");
					appendTo.append("\t\tget").append(methodBase).append("().readData(data);\n");
				}
			} else if(field.isMultiple()) {
				appendTo.append("\t\t").append(field.getJavaName()).append(".clear();\n\t\tfor(int i = 0, n = ");
				appendReadValue(appendTo);
				appendTo.append("; i < n; i++)\n\t\t\tadd").append(StringUtils.singular(methodBase)).append("().readData(data);\n");
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			} else if(field instanceof SubType) {
				appendTo.append("\t\tget").append(methodBase).append("().readData(data);\n");
			} else if(field.getEnumType() != null) {
				Method method = ReflectionUtils.findMethod(field.getEnumType(), "getByValue", new Class<?>[] {getDataType()});
				if(method == null)
					throw new IllegalArgumentException("Could not find method 'getByValue(" + getDataType().getName() + ")' on " + field.getEnumType().getName());
				if(method.getExceptionTypes().length > 0)
					appendTo.append("\t\ttry {\n\t");
				appendTo.append("\t\tset").append(methodBase).append("(").append(field.getEnumType().getSimpleName()).append(".getByValue(");
				appendReadValue(appendTo);
				appendTo.append("));\n");
				if(method.getExceptionTypes().length > 0) {
					for(Class<?> ex : method.getExceptionTypes()) {
						appendTo.append("\t\t} catch(");
						appendTo.append(ex.getSimpleName());
						imports.add(ex);
						appendTo.append(" e) {\n\t\t\tthrow createParseException(e.getMessage(), data.position() - 1, e);\n");
					}
					appendTo.append("\t\t}\n");
				}
			} else {
				if(field.getFieldType() == CRC||field.getFieldType() == CRCLegacy) {
					imports.add(InvalidByteValueException.class);
					appendTo.append("\t\ttry {\n\t");
				}
				appendTo.append("\t\tset").append(methodBase).append('(');
				appendReadValue(appendTo);
				appendTo.append(");\n");
				if(field.getFieldType() == CRC||field.getFieldType() == CRCLegacy) {
					appendTo.append("\t\t} catch(InvalidByteValueException e) {\n\t\t\tthrow createParseException(e.getMessage(), data.position() - 1, e);\n\t\t}\n");
				}

			}
			return appendTo;
		}
		protected StringBuilder appendReadValue(StringBuilder appendTo) {
			appendTo.append("ProcessingUtils.read").append(readName).append("(data");
			if(usingCharset)
				appendTo.append(", getCharset()");
			appendTo.append(')');
			return appendTo;
		}

		public StringBuilder appendWrite(StringBuilder appendTo, Field field, Set<Class<?>> imports) {
			String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				appendWrite(appendTo, choice.getDeterminant(), imports);
				if(field.isMultiple()) {
					String itemName = StringUtils.singular(field.getJavaName());
					appendTo.append("\t\tProcessingUtils.write").append(choice.getMultipleFieldType().writeName).append("(reply, ");
					if(byte.class.equals(field.getFieldType().getDataType()))
						appendTo.append("(byte) ");
					else if(short.class.equals(field.getFieldType().getDataType()))
						appendTo.append("(short) ");
					appendTo.append("get").append(methodBase).append("().size());\n\t\tfor(").append(field.getDataTypeName())
						.append(" ").append(itemName).append(" : get").append(methodBase).append("())\n\t\t\t").append(itemName)
						.append(".writeData(reply, maskSensitiveData);\n");
				} else {
					if(choice.isNullable())
						appendTo.append("\t\tif(get").append(methodBase).append("() != null)\n\t");
					appendTo.append("\t\tget").append(methodBase).append("().writeData(reply, maskSensitiveData);\n");
				}
			} else if(field instanceof SubType) {
				appendTo.append("\t\tget").append(methodBase).append("().writeData(reply, maskSensitiveData);\n");
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			} else {
				appendTo.append("\t\tProcessingUtils.write").append(writeName).append("(reply, ");
				if(field.isMultiple()) {
					String itemName = StringUtils.singular(field.getJavaName());
					if(byte.class.equals(field.getFieldType().getDataType()))
						appendTo.append("(byte) ");
					else if(short.class.equals(field.getFieldType().getDataType()))
						appendTo.append("(short) ");
					appendTo.append("get").append(methodBase).append("().size());\n\t\tfor(").append(field.getDataTypeName())
						.append(" ").append(itemName).append(" : get").append(methodBase).append("())\n\t\t\t").append(itemName)
						.append(".writeData(reply, maskSensitiveData);\n");
				} else {
					if(field.maskMethodName != null) {
						appendTo.append("maskSensitiveData ? ");
						if(field.maskMethodClass != null)
							appendTo.append(field.maskMethodClass.getSimpleName()).append('.');
						appendTo.append(field.maskMethodName).append("(get").append(methodBase).append("()");
						if(field.getEnumType() != null)
							appendTo.append(".getValue()");
						appendTo.append(") : ");
					}
					appendTo.append("get").append(methodBase).append("()");
					if(field.getEnumType() != null)
						appendTo.append(".getValue()");
					if(usingCharset)
						appendTo.append(", getCharset()");
					appendTo.append(");\n");
				}
			}
			return appendTo;
		}
		public StringBuilder appendToString(StringBuilder appendTo, Field field, Set<Class<?>> imports, boolean first) {
			Class<?> dataType;
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				appendToString(appendTo, choice.getDeterminant(), imports, first);
				first = false;
				dataType = Byteable.class;
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			} else
				dataType = field.getFieldType().getDataType();
			String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
			appendTo.append("\n\t\t\t.append(\"");
			if(!first)
				appendTo.append("; ");
			appendTo.append(field.getJavaName()).append("=\").append(");
			if(field.getToStringMethodName() != null) {
				if(field.getToStringMethodClass() != null)
					appendTo.append(field.getToStringMethodClass().getSimpleName()).append('.');
				appendTo.append(field.getToStringMethodName()).append('(');
			} else if(!field.isMultiple() && dataType.equals(byte[].class)) {
				appendTo.append("new String(");
				imports.add(ProcessingConstants.class);
			}
			if(field.maskMethodName != null) {
				if(field.maskMethodClass != null)
					appendTo.append(field.maskMethodClass.getSimpleName()).append('.');
				appendTo.append(field.maskMethodName).append('(');
			}
			if(!field.isMultiple() && dataType.equals(Calendar.class)) {
				appendTo.append("MessageDataUtils.toString(get").append(methodBase).append("())");
			} else {
				appendTo.append("get").append(methodBase).append("()");
				if(dataType.equals(byte.class) && field.getEnumType() == null && !field.isMultiple() && !field.isWrapped())
					appendTo.append(" & 0xFF");
			}
			if(field.maskMethodName != null) {
				appendTo.append(')');
			}
			if(field.getToStringMethodName() != null) {
				appendTo.append(')');
			} else if(!field.isMultiple() && dataType.equals(byte[].class)) {
				appendTo.append(", ProcessingConstants.US_ASCII_CHARSET)");
			}
			appendTo.append(')');

			return appendTo;
		}
		/**
		 * @param sensitiveFields
		 * @param field
		 * @param imports
		 */
		public StringBuilder appendGetSensitive(StringBuilder appendTo, Field field, String suffix, Set<Class<?>> imports) {
			String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				appendGetSensitive(appendTo, choice.getDeterminant(), suffix, imports);
				if(field.isMultiple()) {
					imports.add(FilterMap.class);
					appendTo.append("\t\tfor(int i = 0, n = ").append("get").append(methodBase).append("().size(); i < n; i++)\n\t\t\tget")
						.append(methodBase).append("().get(i).getSensitiveData(new FilterMap<Object>(attributes, null, \"[\" + i + ']'));\n");
				} else {
					if(choice.isNullable())
						appendTo.append("\t\tif(get").append(methodBase).append("() != null)\n\t");
					appendTo.append("\t\tget").append(methodBase).append("().getSensitiveData(attributes);\n");
				}
			} else if(field instanceof SubType) {
				appendTo.append("\t\tget").append(methodBase).append("().getSensitiveData(attributes);\n");
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			} else {
				if(field.isMultiple()) {
					imports.add(FilterMap.class);
					appendTo.append("\t\tfor(int i = 0, n = ").append("get").append(methodBase).append("().size(); i < n; i++)\n\t\t\tget")
						.append(methodBase).append("().get(i).getSensitiveData(new FilterMap<Object>(attributes, null, \"[\" + i + ']'));\n");
				} else {
					if(field.getSensitiveAttributeName() != null) {
						appendTo.append("\t\tattributes.put(\"").append(field.getSensitiveAttributeName()).append("\", get").append(methodBase).append("());\n");
					}
				}
			}
			return appendTo;
		}
		public StringBuilder appendSetSensitive(StringBuilder appendTo, Field field, String suffix, Set<Class<?>> imports) {
			String methodBase = StringUtils.capitalizeFirst(field.getJavaName());
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				appendSetSensitive(appendTo, choice.getDeterminant(), suffix, imports);
				if(field.isMultiple()) {
					imports.add(FilterMap.class);
					appendTo.append("\t\tfor(int i = 0, n = ").append("get").append(methodBase).append("().size(); i < n; i++)\n\t\t\tget")
						.append(methodBase).append("().get(i).setSensitiveData(new FilterMap<Object>(attributes, null, \"[\" + i + ']'));\n");
				} else {
					if(choice.isNullable())
						appendTo.append("\t\tif(get").append(methodBase).append("() != null)\n\t");
					appendTo.append("\t\tget").append(methodBase).append("().setSensitiveData(attributes);\n");
				}
			} else if(field instanceof SubType) {
				appendTo.append("\t\tget").append(methodBase).append("().setSensitiveData(attributes);\n");
			} else if(field.getDataTypeName() == null) {
				return appendTo;
			} else {
				if(field.isMultiple()) {
					imports.add(FilterMap.class);
					appendTo.append("\t\tfor(int i = 0, n = ").append("get").append(methodBase).append("().size(); i < n; i++)\n\t\t\tget")
						.append(methodBase).append("().get(i).setSensitiveData(new FilterMap<Object>(attributes, null, \"[\" + i + ']'));\n");
				} else {
					if(field.getSensitiveAttributeName() != null) {
						imports.add(ConvertUtils.class);
						appendTo.append("\t\tif(attributes.containsKey(\"").append(field.getSensitiveAttributeName())
							.append("\"))\n\t\t\tset").append(methodBase).append("(ConvertUtils.convertSafely(").append(field.getDataTypeName()).append(".class, attributes.get(\"").append(field.getSensitiveAttributeName()).append("\"), ");
						if(byte.class.equals(field.getFieldType().getDataType())) {
							appendTo.append('0');
						} else if(short.class.equals(field.getFieldType().getDataType())) {
							appendTo.append('0');
						} else if(int.class.equals(field.getFieldType().getDataType())) {
							appendTo.append('0');
						} else if(long.class.equals(field.getFieldType().getDataType())) {
							appendTo.append('0');
						} else if(boolean.class.equals(field.getFieldType().getDataType())) {
							appendTo.append("false");
						} else if(float.class.equals(field.getFieldType().getDataType())) {
							appendTo.append("0.0f");
						} else if(double.class.equals(field.getFieldType().getDataType())) {
							appendTo.append("0.0");
						} else {
							appendTo.append("null");
						}
						appendTo.append("));\n");
					}
				}
			}
			return appendTo;
		}
	}
	protected static class Field {
		protected String sensitiveAttributeName;
		protected String javaName;
		protected String description;
		protected Class<? extends Enum<?>> enumType;
		protected FieldType fieldType;
		protected String defaultExpression;
		protected Class<?> maskMethodClass;
		protected String maskMethodName;
		protected Class<?> toStringMethodClass;
		protected String toStringMethodName;
		protected boolean wrapped;
		protected String onUpdate;
		protected String overrideName;

		public String getOnUpdate() {
			return onUpdate;
		}

		public void setOnUpdate(String onUpdate) {
			this.onUpdate = onUpdate;
		}

		public boolean isWrapped() {
			return wrapped;
		}

		public String getDataTypeName() {
			if(getEnumType() != null)
				return getEnumType().getSimpleName();
			else if(isWrapped() && fieldType.getDataType().isPrimitive())
				return ConvertUtils.convertToWrapperClass(fieldType.getDataType()).getSimpleName();
			return fieldType.getDataTypeName();
		}

		public String getJavaName() {
			return javaName;
		}
		public void setJavaName(String javaName) {
			this.javaName = javaName;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Class<? extends Enum<?>> getEnumType() {
			return enumType;
		}
		public void setEnumType(Class<? extends Enum<?>> enumType) {
			this.enumType = enumType;
		}
		public FieldType getFieldType() {
			return fieldType;
		}
		public void setFieldType(FieldType fieldType) {
			this.fieldType = fieldType;
		}
		public String getDefaultExpression() {
			return defaultExpression;
		}
		public void setDefaultExpression(String defaultExpression) {
			this.defaultExpression = defaultExpression;
		}
		public Class<?> getMaskMethodClass() {
			return maskMethodClass;
		}
		public void setMaskMethodClass(Class<?> maskMethodClass) {
			this.maskMethodClass = maskMethodClass;
		}
		public String getMaskMethodName() {
			return maskMethodName;
		}
		public void setMaskMethodName(String maskMethodName) {
			this.maskMethodName = maskMethodName;
		}
		public boolean isMultiple() {
			return false;
		}
		public String getSensitiveAttributeName() {
			return sensitiveAttributeName;
		}
		/**
		 * @param fieldBuilder
		 * @throws ConvertException
		 * @throws ConvertException
		 * @throws ClassNotFoundException
		 */
		public void readXML(ObjectBuilder fieldBuilder) throws ConvertException, ClassNotFoundException  {
			String javaName = fieldBuilder.getAttrValue("javaName");
			if(!StringUtils.isBlank(javaName))
				setJavaName(StringUtils.toJavaName(javaName));
			setDescription(fieldBuilder.getAttrValue("description"));
			setFieldType(fieldBuilder.getAttr("fieldType", FieldType.class));
			//setOnUpdate(fieldBuilder.getAttrValue("onUpdate"));
			String defExp = fieldBuilder.getAttrValue("defaultExpression");
			if(defExp != null && (defExp=defExp.trim()).length() > 0)
				setDefaultExpression(defExp);
			else
				defExp = null;
			String enumType = fieldBuilder.getAttrValue("enumType");
			if(enumType != null && (enumType=enumType.trim()).length() > 0) {
				Class<?> enumClass;
				try {
					enumClass = ConvertUtils.convert(Class.class, enumType);
				} catch(ConvertException e) {
					enumClass = ConvertUtils.convert(Class.class, MessageType.class.getPackage().getName() + '.' + enumType);
				}
				if(!enumClass.isEnum())
					throw new ConvertException(Enum.class, enumType);
				setEnumType((Class<? extends Enum<?>>)enumClass);
				if(defExp == null)
					setDefaultExpression(enumClass.getSimpleName() + '.' + enumClass.getEnumConstants()[0]);
			}
			String maskMethodExp = fieldBuilder.getAttrValue("maskMethod");
			if(maskMethodExp != null && (maskMethodExp=maskMethodExp.trim()).length() > 0) {
				int pos = maskMethodExp.lastIndexOf('.');
				Class<?> methodClass;
				String methodName;
				if(pos < 0) {
					methodClass = AbstractMessageData.class;
					methodName = maskMethodExp;
				} else {
					methodClass = Class.forName(maskMethodExp.substring(0, pos));
					methodName = maskMethodExp.substring(pos + 1);
				}
				Method method = ReflectionUtils.findMethod(methodClass, methodName, new Class<?>[] {getFieldType().getDataType()});
				if(method == null)
					log.warn("Method '" + methodName + "' not found on class " + methodClass.getName());
				setMaskMethodName(methodName);
				if(!methodClass.isAssignableFrom(AbstractMessageData.class))
					setMaskMethodClass(methodClass);
				//TODO: Add a toString() method also
				sensitiveAttributeName = fieldBuilder.getAttrValue("sensitiveAttribute");
				if(sensitiveAttributeName == null || (sensitiveAttributeName=sensitiveAttributeName.trim()).length() == 0)
					sensitiveAttributeName = getJavaName();
			}

			String toStringMethodExp = fieldBuilder.getAttrValue("toStringMethod");
			if(toStringMethodExp != null && (toStringMethodExp = toStringMethodExp.trim()).length() > 0) {
				int pos = toStringMethodExp.lastIndexOf('.');
				Class<?> methodClass;
				String methodName;
				if(pos < 0) {
					methodClass = AbstractMessageData.class;
					methodName = toStringMethodExp;
				} else {
					methodClass = Class.forName(toStringMethodExp.substring(0, pos));
					methodName = toStringMethodExp.substring(pos + 1);
				}
				Method method = ReflectionUtils.findMethod(methodClass, methodName, new Class<?>[] { getFieldType().getDataType() });
				if(method == null)
					log.warn("Method '" + methodName + "' not found on class " + methodClass.getName());
				setToStringMethodName(methodName);
				if(!methodClass.isAssignableFrom(AbstractMessageData.class))
					setToStringMethodClass(methodClass);
			}
			wrapped = ConvertUtils.getBooleanSafely(fieldBuilder.getAttrValue("wrapped"), false);
			setOnUpdate(fieldBuilder.getAttr("onUpdate", String.class));
			setOverrideName(fieldBuilder.getAttrValue("overrideName"));
		}

		public Class<?> getToStringMethodClass() {
			return toStringMethodClass;
		}

		public void setToStringMethodClass(Class<?> toStringMethodClass) {
			this.toStringMethodClass = toStringMethodClass;
		}

		public String getToStringMethodName() {
			return toStringMethodName;
		}

		public void setToStringMethodName(String toStringMethodName) {
			this.toStringMethodName = toStringMethodName;
		}

		public String getOverrideName() {
			return overrideName;
		}

		public void setOverrideName(String overrideName) {
			this.overrideName = overrideName;
		}
	}
	protected static char[] DELIMS = ":,;| ".toCharArray();
	protected static class SubType extends Field {
		protected final List<Field> fields = new ArrayList<Field>();
		protected String subJavaName;
		protected final List<Class<?>> parents = new ArrayList<Class<?>>();
		protected Class<?> superClass;
		protected String subClassName;
		protected String subClassCode;
		protected boolean updatable;
		protected final Set<Class<?>> subCodeImports = new HashSet<Class<?>>();

		public List<Field> getFields() {
			return fields;
		}
		/**
		 * @see GenerateMessageDataClass.Field#getFieldType()
		 */
		@Override
		public FieldType getFieldType() {
			return FieldType.SUB_TYPE;
		}
		/**
		 * @see GenerateMessageDataClass.Case#readXML(simple.xml.ObjectBuilder)
		 */
		@Override
		public void readXML(ObjectBuilder fieldBuilder) throws ConvertException, ClassNotFoundException {
			super.readXML(fieldBuilder);
			fields.clear();
			readFields(fieldBuilder, fields);
			subJavaName = StringUtils.capitalizeFirst(StringUtils.toJavaName(fieldBuilder.getAttrValue("subJavaName")));
			String[] parentArray = StringUtils.split(fieldBuilder.getAttrValue("parents"), DELIMS, false);
			parents.clear();
			superClass = null;
			if(parentArray != null) {
				for(String p : parentArray) {
					Class<?> clazz = Class.forName(p);
					if(!clazz.isInterface()) {
						if(superClass != null)
							throw new IllegalArgumentException("More than one class specified as a super class (parent) to this multiple");
						superClass = clazz;
					} else
						parents.add(clazz);
				}
			}
			ObjectBuilder[] subBuilders = fieldBuilder.getSubNodes("sub-class-code");
			subClassCode = null;
			subCodeImports.clear();
			if(subBuilders != null) {
				for(ObjectBuilder subBuilder : subBuilders) {
					if(subClassCode == null)
						subClassCode = "";
					subClassCode += subBuilder.getText();
					Class<?>[] addImports = subBuilder.get("imports", Class[].class);
					if(addImports != null) {
						subCodeImports.addAll(Arrays.asList(addImports));
					}
				}
			}
			updatable = ConvertUtils.getBooleanSafely(fieldBuilder.getAttrValue("updatable"), false);
		}
		/**
		 * @see GenerateMessageDataClass.Field#getDataTypeName()
		 */
		@Override
		public String getDataTypeName() {
			return subJavaName;
		}
		public boolean isUpdatable() {
			return updatable;
		}
		public void generateSubClass(StringBuilder appendTo, Set<Class<?>> imports) {
			StringBuilder defineFields = new StringBuilder();
			StringBuilder writeFields = new StringBuilder();
			StringBuilder readFields = new StringBuilder();
			StringBuilder toStringFields = new StringBuilder();
			StringBuilder publishFields = new StringBuilder();
			StringBuilder initFields = new StringBuilder();
			StringBuilder getSensitiveFields = new StringBuilder();
			StringBuilder setSensitiveFields = new StringBuilder();
			boolean first = true;
			for(Field field : fields) {
				if(field.getEnumType() != null)
					imports.add(field.getEnumType());
				if(shouldImport(field.getFieldType().getDataType()))
					imports.add(field.getFieldType().getDataType());
				if(field.getMaskMethodClass() != null && shouldImport(field.getMaskMethodClass()))
					imports.add(field.getMaskMethodClass());
				if(field.getToStringMethodClass() != null && shouldImport(field.getToStringMethodClass()))
					imports.add(field.getToStringMethodClass());
				field.getFieldType().appendDefine(defineFields, initFields, field, imports, false);
				field.getFieldType().appendRead(readFields, field, imports);
				field.getFieldType().appendWrite(writeFields, field, imports);
				field.getFieldType().appendGetter(publishFields, field);
				field.getFieldType().appendSetter(publishFields, field, null);
				field.getFieldType().appendToString(toStringFields, field, imports, first);
				if(supportGetSetSensitiveData) {
					field.getFieldType().appendGetSensitive(getSensitiveFields, field, null, imports);
					field.getFieldType().appendSetSensitive(setSensitiveFields, field, null, imports);
				}
				first = false;
			}
			appendTo.append("\tpublic class ").append(getDataTypeName());

			imports.addAll(subCodeImports);
			if(superClass != null) {
				if(shouldImport(superClass))
					imports.add(superClass);
				appendTo.append(" extends ").append(superClass.getSimpleName());
			}
			first = true;
			for(Class<?> p : parents) {
				if(shouldImport(p))
					imports.add(p);
				if(first) {
					appendTo.append(" implements ");
					first = false;
				} else
					appendTo.append(", ");
				appendTo.append(p.getSimpleName());
			}
			appendTo.append(" {\n");
			appendTo.append(defineFields);
			appendTo.append("\t\tprotected ").append(getDataTypeName()).append("() {");
			appendTo.append(initFields).append("\n\t\t}\n\n\t\tpublic void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {\n");
			appendTo.append(readFields);
			appendTo.append("\t\t}\n\n\t\tpublic void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {\n");
			appendTo.append(writeFields);
			appendTo.append("\t\t}\n\n");
			if(supportGetSetSensitiveData) {
				imports.add(Map.class);
				appendTo.append("\t\tpublic void getSensitiveData(Map<String,Object> attributes) {\n");
				appendTo.append(getSensitiveFields);
				appendTo.append("\t\t}\n\n");
				appendTo.append("\t\tpublic void setSensitiveData(Map<String,Object> attributes) {\n");
				appendTo.append(setSensitiveFields);
				appendTo.append("\t\t}\n\n");
			}
			appendTo.append(publishFields);
			if(subClassCode != null)
				appendTo.append(subClassCode).append("\n");
			appendTo.append("\t\tpublic String toString() {\n\t\t\tStringBuilder sb = new StringBuilder();\n\t\t\tsb.append('[')");
			appendTo.append(toStringFields);
			appendTo.append(";\n\t\t\tsb.append(']');\n\t\t\treturn sb.toString();\n\t\t}\n");
			appendTo.append("\t}\n");
		}
	}
	protected static class Case {
		protected final List<Field> fields = new ArrayList<Field>();
		protected String[] values;
		protected String name;
		protected final List<Class<?>> parents = new ArrayList<Class<?>>();
		protected Class<?> superClass;
		protected String subClassCode;
		protected final Set<Class<?>> subCodeImports = new HashSet<Class<?>>();

		/**
		 * @throws ClassNotFoundException
		 * @see GenerateMessageDataClass.Field#readXML(simple.xml.ObjectBuilder)
		 */
		public void readXML(ObjectBuilder fieldBuilder) throws ConvertException, ClassNotFoundException {
			name = fieldBuilder.getAttrValue("name");
			if(!StringUtils.isBlank(name))
				name = StringUtils.capitalizeFirst(StringUtils.toJavaName(name));
			String value = fieldBuilder.getAttrValue("value");
			if(value != null)
				values = new String[] {value};
			else
				values = fieldBuilder.getAttr("values", String[].class);
			String[] parentArray = StringUtils.split(fieldBuilder.getAttrValue("parents"), DELIMS, false);
			parents.clear();
			superClass = null;
			if(parentArray != null) {
				for(String p : parentArray) {
					Class<?> clazz = Class.forName(p);
					if(!clazz.isInterface()) {
						if(superClass != null)
							throw new IllegalArgumentException("More than one class specified as a super class (parent) to this multiple");
						superClass = clazz;
					} else
						parents.add(clazz);
				}
			}
			ObjectBuilder[] subBuilders = fieldBuilder.getSubNodes("sub-class-code");
			subClassCode = null;
			subCodeImports.clear();
			if(subBuilders != null) {
				for(ObjectBuilder subBuilder : subBuilders) {
					if(subClassCode == null)
						subClassCode = "";
					subClassCode += subBuilder.getText();
					Class<?>[] addImports = subBuilder.get("imports", Class[].class);
					if(addImports != null) {
						subCodeImports.addAll(Arrays.asList(addImports));
					}
				}
			}
			fields.clear();
			readFields(fieldBuilder, fields);
		}
		/**
		 * @return the fields
		 */
		public List<Field> getFields() {
			return fields;
		}
		public String[] getValues() {
			return values;
		}
		public String getName() {
			return name;
		}
		protected String getSubClassName() {
			return name;
		}
		public void generateSubClass(StringBuilder appendTo, String parentClassName, Set<Class<?>> imports, boolean multiple) {
			boolean first = true;
			if(fields.isEmpty()) {
				if(superClass != null && Byteable.class.isAssignableFrom(superClass) && !Modifier.isAbstract(superClass.getModifiers())) {
					appendTo.append("\tpublic class ").append(getSubClassName());
					imports.addAll(subCodeImports);
					boolean useContent = false;
					if(superClass != null) {
						if(shouldImport(superClass))
							imports.add(superClass);
						appendTo.append(" extends ").append(superClass.getSimpleName());
						if(ShortLengthByteable.class.isAssignableFrom(superClass))
							useContent = true;
						else if(LongLengthByteable.class.isAssignableFrom(superClass))
							useContent = true;
					}
					first = true;
					for(Class<?> p : parents) {
						if(shouldImport(p))
							imports.add(p);
						if(first) {
							appendTo.append(" implements ");
							first = false;
						} else
							appendTo.append(", ");
						appendTo.append(p.getSimpleName());
					}
					if(first)
						appendTo.append(" implements ");
					else
						appendTo.append(", ");
					appendTo.append(parentClassName);
					appendTo.append(" { }\n");
				}
				return; // no need to create subclass
			}
			StringBuilder defineFields = new StringBuilder();
			StringBuilder initFields = new StringBuilder();
			StringBuilder writeFields = new StringBuilder();
			StringBuilder readFields = new StringBuilder();
			StringBuilder toStringFields = new StringBuilder();
			StringBuilder publishFields = new StringBuilder();
			StringBuilder getSensitiveFields = new StringBuilder();
			StringBuilder setSensitiveFields = new StringBuilder();
			for(Field field : fields) {
				if(field.getEnumType() != null)
					imports.add(field.getEnumType());
				if(field.getFieldType().getDataType() != null && shouldImport(field.getFieldType().getDataType()))
					imports.add(field.getFieldType().getDataType());
				if(field.getMaskMethodClass() != null && shouldImport(field.getMaskMethodClass()))
					imports.add(field.getMaskMethodClass());
				if(field.getToStringMethodClass() != null && shouldImport(field.getToStringMethodClass()))
					imports.add(field.getToStringMethodClass());
				field.getFieldType().appendDefine(defineFields, initFields, field, imports, false);
				field.getFieldType().appendRead(readFields, field, imports);
				field.getFieldType().appendWrite(writeFields, field, imports);
				field.getFieldType().appendGetter(publishFields, field);
				field.getFieldType().appendSetter(publishFields, field, null);
				field.getFieldType().appendToString(toStringFields, field, imports, first);
				if(supportGetSetSensitiveData) {
					field.getFieldType().appendGetSensitive(getSensitiveFields, field, null, imports);
					field.getFieldType().appendSetSensitive(setSensitiveFields, field, null, imports);
				}
				first = false;
			}
			appendTo.append("\tpublic class ").append(getSubClassName());
			imports.addAll(subCodeImports);
			boolean useContent = false;
			if(superClass != null) {
				if(shouldImport(superClass))
					imports.add(superClass);
				appendTo.append(" extends ").append(superClass.getSimpleName());
				if(ShortLengthByteable.class.isAssignableFrom(superClass))
					useContent = true;
				else if(LongLengthByteable.class.isAssignableFrom(superClass))
					useContent = true;
			}
			first = true;
			for(Class<?> p : parents) {
				if(shouldImport(p))
					imports.add(p);
				if(first) {
					appendTo.append(" implements ");
					first = false;
				} else
					appendTo.append(", ");
				appendTo.append(p.getSimpleName());
			}
			if(first)
				appendTo.append(" implements ");
			else
				appendTo.append(", ");
			appendTo.append(parentClassName);
			appendTo.append(" {\n");
			appendTo.append(defineFields);
			if(multiple) {
				appendTo.append("\t\tprotected final int index;\n");
				appendTo.append("\t\tprotected ").append(getSubClassName()).append("(int index) {\n\t\t\tthis.index = index;\n\t\t}\n\n");
				appendTo.append("\t\tpublic int getIndex() {\n\t\t\treturn index;\n\t\t}\n\n");
			} else {
				appendTo.append("\t\tprotected ").append(getSubClassName()).append("() {").append(initFields).append("\n\t\t}\n\n");
			}
			appendTo.append("\t\tpublic void read");
			if(useContent)
				appendTo.append("Content");
			else
				appendTo.append("Data");
			appendTo.append("(ByteBuffer data) throws ParseException, BufferUnderflowException {\n");
			appendTo.append(readFields);
			appendTo.append("\t\t}\n\n\t\tpublic void write");
			if(useContent)
				appendTo.append("Content");
			else
				appendTo.append("Data");
			appendTo.append("(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {\n");
			appendTo.append(writeFields);
			appendTo.append("\t\t}\n\n");
			if(supportGetSetSensitiveData) {
				imports.add(Map.class);
				appendTo.append("\t\tpublic void getSensitiveData(Map<String,Object> attributes) {\n");
				appendTo.append(getSensitiveFields);
				appendTo.append("\t\t}\n\n");
				appendTo.append("\t\tpublic void setSensitiveData(Map<String,Object> attributes) {\n");
				appendTo.append(setSensitiveFields);
				appendTo.append("\t\t}\n\n");
			}
			appendTo.append(publishFields);
			appendTo.append("\t\tpublic String toString() {\n\t\t\tStringBuilder sb = new StringBuilder();\n\t\t\tsb.append('[')");
			appendTo.append(toStringFields);
			appendTo.append(";\n\t\t\tsb.append(']');\n\t\t\treturn sb.toString();\n\t\t}\n");
			if(subClassCode != null)
				appendTo.append(subClassCode).append("\n");
			appendTo.append("\t}\n");
		}
	}
	protected static class Choice extends Field {
		protected final List<Case> cases = new ArrayList<Case>();
		protected final List<Class<?>> parents = new ArrayList<Class<?>>();
		protected String subClassName;
		protected boolean createSubClass;
		protected FieldType multipleFieldType;
		protected Field determinant;
		protected String subClassCode;
		protected final Set<Class<?>> subCodeImports = new HashSet<Class<?>>();
		@Override
		public boolean isMultiple() {
			return multipleFieldType != null;
		}
		/**
		 * @return
		 */
		public boolean isNullable() {
			for(Case cs : cases)
				if(cs.fields.isEmpty())
					return true;
			return false;
		}
		/**
		 * @throws ClassNotFoundException
		 * @see GenerateMessageDataClass.Field#readXML(simple.xml.ObjectBuilder)
		 */
		@Override
		public void readXML(ObjectBuilder fieldBuilder) throws ConvertException, ClassNotFoundException {
			determinant = new Field();
			determinant.readXML(fieldBuilder);
			String[] parentArray = StringUtils.split(fieldBuilder.getAttrValue("parents"), DELIMS, false);
			parents.clear();
			if(parentArray != null) {
				for(String p : parentArray) {
					Class<?> clazz = Class.forName(p);
					if(!clazz.isInterface()) {
						throw new IllegalArgumentException("Concrete class is not allowed for <choice>. Only use interfaces for <choice>");
					} else
						parents.add(clazz);
				}
			}
			cases.clear();
			for(ObjectBuilder caseBuilder : fieldBuilder.getSubNodes("case")) {
				Case cs = new Case();
				cs.readXML(caseBuilder);
				cases.add(cs);
			}
			javaName = fieldBuilder.getAttrValue("subJavaName");
			multipleFieldType = fieldBuilder.getAttr("multipleFieldType", FieldType.class);
			if(javaName == null) {
				if(multipleFieldType != null)
					javaName = determinant.getJavaName() + "Datum";
				else
					javaName = determinant.getJavaName() + "Data";
			}
			if(parents.size() == 1 && Byteable.class.isAssignableFrom(parents.get(0))) {
				subClassName = parents.get(0).getSimpleName();
				createSubClass = false;
			} else {
				subClassName = StringUtils.singular(StringUtils.capitalizeFirst(javaName));
				for(Class<?> parent : parents)
					if(subClassName.equals(parent.getSimpleName())) {
						if(!subClassName.endsWith("Data"))
							subClassName += "Data";
						else if(!subClassName.startsWith("Byteable"))
							subClassName = "Byteable" + subClassName;
						else if(!subClassName.startsWith("Inner"))
							subClassName = "Inner" + subClassName;

					}

				createSubClass = true;
			}
			ObjectBuilder[] subBuilders = fieldBuilder.getSubNodes("sub-class-code");
			subClassCode = null;
			subCodeImports.clear();
			if(subBuilders != null) {
				for(ObjectBuilder subBuilder : subBuilders) {
					if(subClassCode == null)
						subClassCode = "";
					subClassCode += subBuilder.getText();
					Class<?>[] addImports = subBuilder.get("imports", Class[].class);
					if(addImports != null) {
						subCodeImports.addAll(Arrays.asList(addImports));
					}
				}
			}
		}

		protected List<Class<?>> getParents() {
			return parents;
		}
		@Override
		public FieldType getFieldType() {
			return determinant.getFieldType();
		}
		@Override
		public Class<? extends Enum<?>> getEnumType() {
			return determinant.getEnumType();
		}
		public List<Case> getCases() {
			return cases;
		}
		@Override
		public String getDataTypeName() {
			return subClassName;
		}
		public Field getDeterminant() {
			return determinant;
		}
		public FieldType getMultipleFieldType() {
			return multipleFieldType;
		}
		public String getSubClassCode() {
			return subClassCode;
		}
		public void generateSubClass(StringBuilder appendTo, Set<Class<?>> imports) {
			imports.addAll(subCodeImports);
			for(Case cs : cases) {
				cs.generateSubClass(appendTo, subClassName, imports, isMultiple());
			}
		}
		public StringBuilder appendInterface(StringBuilder appendTo, Set<Class<?>> imports) {
			if(createSubClass) {
				appendTo.append("\tpublic interface ").append(subClassName).append(" extends Byteable");
				for(Class<?> p : parents) {
					if(shouldImport(p))
						imports.add(p);
					appendTo.append(", ");
					appendTo.append(p.getSimpleName());
				}
				appendTo.append(" {\n");
				if(subClassCode != null)
					appendTo.append(subClassCode).append("\n");
				appendTo.append("\t}\n");
			}
			return appendTo;
		}
	}
	protected static class Multiple extends Field {
		protected final List<Field> fields = new ArrayList<Field>();
		protected final List<Class<?>> parents = new ArrayList<Class<?>>();
		protected Class<?> superClass;
		protected String subClassName;
		protected String subClassCode;
		protected final Set<Class<?>> subCodeImports = new HashSet<Class<?>>();

		@Override
		public String getDataTypeName() {
			return subClassName;
		}
		/**
		 * @see GenerateMessageDataClass.Field#isMultiple()
		 */
		@Override
		public boolean isMultiple() {
			return true;
		}
		/**
		 * @throws ClassNotFoundException
		 * @see GenerateMessageDataClass.Field#readXML(simple.xml.ObjectBuilder)
		 */
		@Override
		public void readXML(ObjectBuilder fieldBuilder) throws ConvertException, ClassNotFoundException {
			super.readXML(fieldBuilder);
			String[] parentArray = StringUtils.split(fieldBuilder.getAttrValue("parents"), DELIMS, false);
			parents.clear();
			superClass = null;
			if(parentArray != null) {
				for(String p : parentArray) {
					Class<?> clazz = Class.forName(p);
					if(!clazz.isInterface()) {
						if(superClass != null)
							throw new IllegalArgumentException("More than one class specified as a super class (parent) to this multiple");
						superClass = clazz;
					} else
						parents.add(clazz);
				}
			}
			ObjectBuilder[] subBuilders = fieldBuilder.getSubNodes("sub-class-code");
			subClassCode = null;
			subCodeImports.clear();
			if(subBuilders != null) {
				for(ObjectBuilder subBuilder : subBuilders) {
					if(subClassCode == null)
						subClassCode = "";
					subClassCode += subBuilder.getText();
					Class<?>[] addImports = subBuilder.get("imports", Class[].class);
					if(addImports != null) {
						subCodeImports.addAll(Arrays.asList(addImports));
					}
				}
			}
			fields.clear();
			readFields(fieldBuilder, fields);
			subClassName = StringUtils.singular(StringUtils.capitalizeFirst(javaName)) + "Data";
		}
		/**
		 * @return the fields
		 */
		public List<Field> getFields() {
			return fields;
		}
		protected List<Class<?>> getParents() {
			return parents;
		}
		public void generateSubClass(StringBuilder appendTo, Set<Class<?>> imports) {
			StringBuilder defineFields = new StringBuilder();
			StringBuilder initFields = new StringBuilder();
			StringBuilder writeFields = new StringBuilder();
			StringBuilder readFields = new StringBuilder();
			StringBuilder toStringFields = new StringBuilder();
			StringBuilder publishFields = new StringBuilder();
			StringBuilder getSensitiveFields = new StringBuilder();
			StringBuilder setSensitiveFields = new StringBuilder();
			boolean first = true;
			for(Field field : fields) {
				if(field.getEnumType() != null)
					imports.add(field.getEnumType());
				if(shouldImport(field.getFieldType().getDataType()))
					imports.add(field.getFieldType().getDataType());
				if(field.getMaskMethodClass() != null && shouldImport(field.getMaskMethodClass()))
					imports.add(field.getMaskMethodClass());
				if(field.getToStringMethodClass() != null && shouldImport(field.getToStringMethodClass()))
					imports.add(field.getToStringMethodClass());
				field.getFieldType().appendDefine(defineFields, initFields, field, imports, false);
				field.getFieldType().appendRead(readFields, field, imports);
				field.getFieldType().appendWrite(writeFields, field, imports);
				field.getFieldType().appendGetter(publishFields, field);
				field.getFieldType().appendSetter(publishFields, field, null);
				field.getFieldType().appendToString(toStringFields, field, imports, first);
				if(supportGetSetSensitiveData) {
					field.getFieldType().appendGetSensitive(getSensitiveFields, field, null, imports);
					field.getFieldType().appendSetSensitive(setSensitiveFields, field, null, imports);
				}
				first = false;
			}
			imports.addAll(subCodeImports);
			appendTo.append("\tpublic class ").append(subClassName);
			if(superClass != null) {
				if(shouldImport(superClass))
					imports.add(superClass);
				appendTo.append(" extends ").append(superClass.getSimpleName());
			}
			first = true;
			for(Class<?> p : parents) {
				if(shouldImport(p))
					imports.add(p);
				if(first) {
					appendTo.append(" implements ");
					first = false;
				} else
					appendTo.append(", ");
				appendTo.append(p.getSimpleName());
			}
			appendTo.append(" {\n");
			appendTo.append(defineFields);
			appendTo.append("\t\tprotected final int index;\n");
			appendTo.append("\t\tprotected ").append(subClassName).append("(int index) {\n\t\t\tthis.index = index;").append(initFields).append("\n\t\t}\n\n");
			appendTo.append("\t\tpublic int getIndex() {\n\t\t\treturn index;\n\t\t}\n\n");
			appendTo.append("\t\tpublic void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {\n");
			appendTo.append(readFields);
			appendTo.append("\t\t}\n\n\t\tpublic void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {\n");
			appendTo.append(writeFields);
			appendTo.append("\t\t}\n\n");
			if(supportGetSetSensitiveData) {
				appendTo.append("\t\tpublic void getSensitiveData(Map<String,Object> attributes) {\n");
				appendTo.append(getSensitiveFields);
				appendTo.append("\t\t}\n\n");
				appendTo.append("\t\tpublic void setSensitiveData(Map<String,Object> attributes) {\n");
				appendTo.append(setSensitiveFields);
				appendTo.append("\t\t}\n\n");
			}
			appendTo.append(publishFields);
			if(subClassCode != null)
				appendTo.append(subClassCode).append("\n");
			appendTo.append("\t\tpublic String toString() {\n\t\t\tStringBuilder sb = new StringBuilder();\n\t\t\tsb.append('[')");
			appendTo.append(toStringFields);
			appendTo.append(";\n\t\t\tsb.append(']');\n\t\t\treturn sb.toString();\n\t\t}\n");
			appendTo.append("\t}\n");
		}
	}
	protected static class MessageTypeSelection {
		protected final MessageType messageType;
		protected final boolean existing;
		public MessageTypeSelection(MessageType messageType) {
			super();
			this.messageType = messageType;
			this.existing = (null != MessageDataFactory.createMessageData(messageType));
		}

		@Override
		public String toString() {
			return (existing ? "* " : "")  + messageType.getDescription();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Throwable {
		if(args == null || args.length < 1 || args[0] == null || args[0].trim().length() == 0) {
			System.err.print("Please provide the target directory in which to create the MessageData java files");
			return;
		}
		Set<String> include;
		String tmp;
		if(args.length > 1 && args[1] != null && (tmp = args[1].trim()).length() > 0) {
			include = new MapBackedSet<String>(new CaseInsensitiveHashMap<Object>());
			include.addAll(ConvertUtils.asCollection(tmp, String.class));
		} else
			include = null;
		GenerateMessageDataClass generator = new GenerateMessageDataClass();
	    /*
		JFrame jframe = new JFrame("GenerateMessageDataClass");
        jframe.getContentPane().add(generator.getGeneratorPanel());
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setSize(300,200);
        jframe.setVisible(true);
        */
		File baseDir = new File(args[0].trim());
		/*
		URL url = generator.getClass().getResource("");
		if(url != null)
    		baseDir = new File(url.toURI());
    	else
    		baseDir = null;
		 */

		generator.generateFromXML(new File(baseDir, MessageData.class.getPackage().getName().replace('.', '/')), true, include);
	}

	/**
	 *
	 */
	public GenerateMessageDataClass() {
		super();
	}
	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private JPanel getGeneratorPanel() {
		if(generatorPanel == null) {
			generatorPanel = new JPanel();
			generatorPanel.setLayout(new BorderLayout());
			generatorPanel.add(getMessageTypePanel(), java.awt.BorderLayout.NORTH);
			generatorPanel.add(getFieldsPanel(), java.awt.BorderLayout.CENTER);
			generatorPanel.add(getButtonPanel(), java.awt.BorderLayout.SOUTH);
		}
		return generatorPanel;
	}


    /**
     * This method initializes targetsScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getFieldsScrollPane() {
        if(fieldsScrollPane == null) {
        	fieldsScrollPane = new JScrollPane();
        	fieldsScrollPane.setViewportView(getFieldsTable());
        }
        return fieldsScrollPane;
    }

    private JComboBox getMessageTypeSelect() {
    	if(messageTypeSelect == null) {
    		messageTypeSelect = new JComboBox();
    		messageTypeSelect.setEditable(false);
    		int initialIndex = -1;
    		MessageType[] messageTypes = MessageType.values();
    		MessageTypeSelection[] selectionValues = new MessageTypeSelection[messageTypes.length];
    		for(int i = 0; i < messageTypes.length; i++) {
    			MessageTypeSelection selection = new MessageTypeSelection(messageTypes[i]);
    			selectionValues[i] = selection;
    			if(initialIndex < 0 && !selection.existing) {
					initialIndex = i;
    			}
    		}
    		ChangeableComboBoxModel<MessageTypeSelection> model = new ChangeableComboBoxModel<MessageTypeSelection>();
    		model.setItems(selectionValues);
    		messageTypeSelect.setModel(model);
    		messageTypeSelect.setSelectedIndex(initialIndex);
    	}
    	return messageTypeSelect;
    }

	/**
	 * This method initializes buttonPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel();
			buttonPanel.add(getSubmitButton(), null);
			buttonPanel.add(getCancelButton(), null);
		}
		return buttonPanel;
	}

	/**
	 * This method initializes buttonPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getMessageTypePanel() {
		if (messageTypePanel == null) {
			messageTypePanel = new JPanel();
			messageTypeLabel = new JLabel();
			messageTypeLabel.setText("Message Type:");
			messageTypePanel.add(messageTypeLabel, null);
			messageTypePanel.add(getMessageTypeSelect(), null);
		}
		return messageTypePanel;
	}
	/**
	 * This method initializes selectButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getSubmitButton() {
		if (generateButton == null) {
			generateButton = new JButton();
			generateButton.setText("Generate");
			generateButton.setEnabled(true);
			generateButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					MessageType messageType = getMessageType();
					List<Field> fields = getFields();
					report("Generating class for " + messageType.getDescription() + " ...");
					try {
						generateMessageDataClass(messageType, null, null, null, fields, new PrintWriter(System.out));
					} catch(IOException e1) {
						log.warn("", e1);
					} catch(IntrospectionException e1) {
						log.warn("", e1);
					} catch(IllegalAccessException e1) {
						log.warn("", e1);
					} catch(InvocationTargetException e1) {
						log.warn("", e1);
					} catch(ParseException e1) {
						log.warn("", e1);
					}
					report("Done generating class for " + messageType.getDescription() + " ...");
				}
			});
		}
		return generateButton;
	}

	/**
	 * This method initializes selectButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getFieldDeleteButton() {
		if (fieldDeleteButton == null) {
			fieldDeleteButton = new JButton();
			fieldDeleteButton.setText("Delete Field");
			fieldDeleteButton.setEnabled(false);
			fieldDeleteButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					int[] rows = getFieldsTable().getSelectedRows();
					Arrays.sort(rows);
					for(int i = rows.length; i <= 0; i--) {
						getFields().remove(rows[i]);
					}
				}
			});
		}
		return fieldDeleteButton;
	}
	/**
	 * This method initializes selectButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getFieldAddButton() {
		if (fieldAddButton == null) {
			fieldAddButton = new JButton();
			fieldAddButton.setText("Add Field");
			fieldAddButton.setEnabled(true);
			fieldAddButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					getFields().add(new Field());
				}
			});
		}
		return fieldAddButton;
	}
	/**
	 * This method initializes buttonPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getFieldButtonPanel() {
		if (buttonFieldPanel == null) {
			buttonFieldPanel = new JPanel();
			buttonFieldPanel.add(getFieldDeleteButton(), null);
			buttonFieldPanel.add(getFieldAddButton(), null);
		}
		return buttonFieldPanel;
	}

	private JPanel getFieldsPanel() {
		if (fieldsPanel == null) {
			fieldsPanel = new JPanel();
			fieldsPanel.setLayout(new BorderLayout());
        	fieldsPanel.add(getFieldsScrollPane(), BorderLayout.CENTER);
			fieldsPanel.add(getFieldButtonPanel(), BorderLayout.EAST);
		}
		return fieldsPanel;
	}
	/**
	 * This method initializes beanTableModel
	 *
	 * @return simple.swt.BeanTableModel
	 */
	private BeanTableModel getFieldsTableModel() {
		if (fieldsTableModel == null) {
			fieldsTableModel = new BeanTableModel();
			fieldsTableModel.setBeanClass(Field.class);
			//fieldsTableModel.setColumns( new String[] {"Java Name", "Description", "Field Type", "Enum Type" });
			fieldsTableModel.setColumns( new String[] {"javaName", "description", "fieldType", "enumType" });
			fieldsTableModel.setColumnsEditable(new boolean[] {true, true, true, true});
			fieldsTableModel.setListModel(fields);
		}
		return fieldsTableModel;
	}

	/**
     * This method initializes targetsTable
     *
     * @return javax.swing.JTable
     */
    private JTable getFieldsTable() {
        if(fieldsTable == null) {
        	fieldsTable = new JTable();
            fieldsTable.setModel(getFieldsTableModel());
            fieldsTable.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 11));
            fieldsTable.getColumnModel().getColumn(2).setCellEditor(new EnumCellEditor(FieldType.class));
            try {
            	Class<?> baseClass = MessageType.class;
            	URL url = baseClass.getResource("");
            	if(url != null) {
            		File dir = new File(url.toURI());
            		String[] fileNames = dir.list(new FilenameFilter() {
            			public boolean accept(File dir, String name) {
            				return (name.endsWith(".class") && name.indexOf('$') < 0);
            			}
            		});
            		List<Class<?>> constants = new ArrayList<Class<?>>();
            		for(String fileName : fileNames) {
            			String className = baseClass.getPackage().getName() + '.' + fileName.substring(0, fileName.length() - 6);
            			Class<?> clazz;
            			try {
							clazz = Class.forName(className);
							if(Enum.class.isAssignableFrom(clazz)) {
	            				constants.add(clazz);
	            			}
            			} catch(ClassNotFoundException e1) {
							log.warn("Could not instantiate class '" + className + '\'', e1);
						}
            		}
            		if(!constants.isEmpty()) {
            			ChangeableComboBoxModel<Class<?>> cbm = new ChangeableComboBoxModel<Class<?>>();
                		cbm.setItems(constants.toArray(new Class<?>[constants.size()]));
                		fieldsTable.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(new JComboBox(cbm)));
            		}
            	}
			} catch(URISyntaxException e) {
				log.warn("Could not get package directory for constants; using free-form text box", e);
			}
            /*
            TableCellEditor tce = new DefaultCellEditor(new JComboBox(cbm)) {
                @Override
                public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
                    String dataSourceName;
                    String schemaName;
                    String tableName;
                    try {
                        switch(column) {
                            case 0:
                                cbm.setItems();
                                break;
                            case 1:
                                dataSourceName = (String)table.getValueAt(row, 0);
                                cbm.setDataSourceName(dataSourceName);
                                break;
                            case 2:
                                dataSourceName = (String)table.getValueAt(row, 0);
                                schemaName = (String)table.getValueAt(row, 1);
                                cbm.setSchemaName(dataSourceName, schemaName);
                                break;
                            case 3:
                                dataSourceName = (String)table.getValueAt(row, 0);
                                schemaName = (String)table.getValueAt(row, 1);
                                tableName = (String)table.getValueAt(row, 2);
                                cbm.setTableName(dataSourceName, schemaName, tableName);
                                break;
                        }
                    } catch(SQLException e) {
                        reportException("Could not load names from database", e);
                    } catch(DataLayerException e) {
                        reportException("Could not load names from database", e);
                    }
                    cbm.setSelectedItem(value);
                    return super.getTableCellEditorComponent(table, value, isSelected, row, column);
                }
            };

            fieldsTable.getColumnModel().getColumn(0).setCellEditor(tce);
            fieldsTable.getColumnModel().getColumn(1).setCellEditor(tce);
            fieldsTable.getColumnModel().getColumn(2).setCellEditor(tce);
            fieldsTable.getColumnModel().getColumn(3).setCellEditor(tce);
*/
            fieldsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    if(e.getFirstIndex() > -1 && e.getFirstIndex() < fields.getSize()) {
                        getFieldDeleteButton().setEnabled(true);
                    } else {
                        getFieldDeleteButton().setEnabled(false);
                    }
                }
            });
        }
        return fieldsTable;
    }
	/**
	 * @return
	 */
	protected List<Field> getFields() {
		return fields;
	}

	/**
	 * @return
	 */
	protected MessageType getMessageType() {
		MessageTypeSelection mts = (MessageTypeSelection) getMessageTypeSelect().getSelectedItem();
		if(mts == null)
			return null;
		return mts.messageType;
	}

	protected void generateFromXML(File directory, boolean allowOverride, Set<String> include) throws IllegalArgumentException, InvalidByteValueException, InvalidByteArrayException, ConvertException, FileNotFoundException, IOException, IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException, SAXException, ParserConfigurationException, ClassNotFoundException {
		ObjectBuilder builder = new ObjectBuilder(getClass().getResourceAsStream("message-data.xml"));
		generateFromXML(builder, directory, allowOverride, include);
	}

	protected void generateFromXML(ObjectBuilder builder, File directory, boolean allowOverride, Set<String> include) throws IllegalArgumentException, InvalidByteValueException, InvalidByteArrayException, ConvertException, FileNotFoundException, IOException, IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException, ClassNotFoundException {
		int cnt = 0;
		for(ObjectBuilder mdBuilder : builder.getSubNodes("message-data")) {
			String hex = mdBuilder.getAttrValue("dataType");
			if(hex != null)
				hex = hex.trim().toUpperCase();
			if(include != null && !include.contains(hex))
				continue;
			MessageType messageType = MessageType.getByValue(ByteArrayUtils.fromHex(hex), 0);
			List<Field> fields = new ArrayList<Field>();
			readFields(mdBuilder, fields);
			ObjectBuilder[] subBuilders = mdBuilder.getSubNodes("class-code");
			StringBuilder classCode = new StringBuilder();
			Set<Class<?>> additionalImports = new HashSet<Class<?>>();
			if(subBuilders != null) {
				for(ObjectBuilder subBuilder : subBuilders) {
					classCode.append(subBuilder.getText());
					Class<?>[] addImports = subBuilder.get("imports", Class[].class);
					if(addImports != null) {
						additionalImports.addAll(Arrays.asList(addImports));
					}
				}
			}
			Class<?>[] parents = mdBuilder.get("parents", Class[].class);
			File file = new File(directory, "MessageData_" + hex + ".java");
			if(file.exists()) {
				if(!allowOverride)
					throw new IOException("File '" + file.getAbsolutePath() + "' already exists");
			} else if(!file.getParentFile().exists())
				file.getParentFile().mkdirs();
			log.info("Generating class 'MessageData_" + hex + "' at '" + file.getAbsolutePath() + "'");
			generateMessageDataClass(messageType, parents == null ? null : new HashSet<Class<?>>(Arrays.asList(parents)), additionalImports, classCode.toString(), fields, new PrintWriter(file));
			cnt++;
		}
		log.info("Generated " + cnt + " classes");

	}
	protected static void readFields(ObjectBuilder builder, List<Field> fields) throws ConvertException, IllegalArgumentException, ClassNotFoundException {
		for(ObjectBuilder fieldBuilder : builder.getSubNodes()) {
			Field field;
			if(fieldBuilder.getTag().equalsIgnoreCase("choice")) {
				field = new Choice();
			} else if(fieldBuilder.getTag().equalsIgnoreCase("multiple")) {
				field = new Multiple();
			} else if(fieldBuilder.getTag().equalsIgnoreCase("field")) {
				field = new Field();
			} else if(fieldBuilder.getTag().equalsIgnoreCase("class-code") || fieldBuilder.getTag().equalsIgnoreCase("sub-class-code")) {
				continue;
			} else if(fieldBuilder.getTag().equalsIgnoreCase("subtype")) {
				field = new SubType();
			} else {
				throw new IllegalArgumentException("field tag '" + fieldBuilder.getTag() + "' is not supported");
			}
			field.readXML(fieldBuilder);
			fields.add(field);
		}
	}
	/**
	 * @param messageType
	 * @param fields
	 * @throws IOException
	 * @throws ParseException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IntrospectionException
	 */
	protected void generateMessageDataClass(MessageType messageType, Set<Class<?>> parents, Set<Class<?>> additionalImports, String additionalClassCode, List<Field> fields, PrintWriter writer) throws IOException, IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("messageType", messageType);
		StringBuilder ancestry = new StringBuilder();
		StringBuilder defineFields = new StringBuilder();
		StringBuilder initFields = new StringBuilder();
		StringBuilder writeFields = new StringBuilder();
		StringBuilder readFields = new StringBuilder();
		StringBuilder toStringFields = new StringBuilder();
		StringBuilder publishFields = new StringBuilder();
		StringBuilder getSensitiveFields = new StringBuilder();
		StringBuilder setSensitiveFields = new StringBuilder();
		Set<Class<?>> imports = new TreeSet<Class<?>>(new Comparator<Class<?>>() {
			/**
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(Class<?> o1, Class<?> o2) {
				if(o1 == o2)
					return 0;
				if(o1 == null)
					return -1;
				if(o2 == null)
					return 1;
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		});
		if(additionalImports != null)
			imports.addAll(additionalImports);
		if(messageType.isMessageIdExpected()) {
			toStringFields.append("\n\t\t\t.append(\"; messageId=\").append(getMessageId())");
		}
		appendInterfaces(fields, defineFields, imports);
		boolean useDeviceTypeSpecific = hasDeviceTypeChoice(fields);
		if(useDeviceTypeSpecific || (parents != null && parents.contains(DeviceTypeSpecific.class))) {
			Set<Class<?>> p = new HashSet<Class<?>>();
			p.add(DeviceTypeSpecific.class);
			if(parents != null && !parents.isEmpty()) {
				p.addAll(parents);
			}
			parents = p;
			imports.add(DeviceType.class);
			defineFields.append("\tprotected DeviceType deviceType = DeviceType.DEFAULT;\n");
			publishFields.append("\tpublic DeviceType getDeviceType() {\n\t\treturn deviceType;\n\t}\n\n");
			publishFields.append("\tpublic void setDeviceType(DeviceType deviceType) {\n\t\tthis.deviceType = deviceType;\n\t}\n\n");
		}
		if(parents != null && !parents.isEmpty()) {
			Class<?> superClass = null;
			for(Class<?> p : parents) {
				if(!p.isInterface()) {
					if(superClass != null)
						throw new IllegalArgumentException("More than one class specified as a super class (parent) to this multiple");
					if(!AbstractMessageData.class.isAssignableFrom(p))
						throw new IllegalArgumentException("Concrete parent '" + p.getName() + "' must extend AbstractMessageData");
					superClass = p;
				}
			}
			if(superClass == null)
				superClass = AbstractMessageData.class;
			ancestry.append(" extends ").append(superClass.getSimpleName());
			boolean first = true;
			for(Class<?> p : parents) {
				if(shouldImport(p))
					imports.add(p);
				if(p.isInterface()) {
					if(first) {
						ancestry.append(" implements ");
						first = false;
					} else
						ancestry.append(", ");
					ancestry.append(p.getSimpleName());
				}
			}
		} else {
			ancestry.append(" extends ").append(AbstractMessageData.class.getSimpleName());
		}

		for(Field field : fields) {
			imports.add(ProcessingUtils.class);
			if(field.getEnumType() != null)
				imports.add(field.getEnumType());
			if(shouldImport(field.getFieldType().getDataType()))
				imports.add(field.getFieldType().getDataType());
			if(field.getMaskMethodClass() != null && shouldImport(field.getMaskMethodClass()))
				imports.add(field.getMaskMethodClass());
			if(field.getToStringMethodClass() != null && shouldImport(field.getToStringMethodClass()))
				imports.add(field.getToStringMethodClass());
			field.getFieldType().appendDefine(defineFields, initFields, field, imports, false);
			field.getFieldType().appendRead(readFields, field, imports);
			field.getFieldType().appendWrite(writeFields, field, imports);
			field.getFieldType().appendGetter(publishFields, field);
			field.getFieldType().appendSetter(publishFields, field, null);
			field.getFieldType().appendToString(toStringFields, field, imports, false);
			if(supportGetSetSensitiveData) {
				field.getFieldType().appendGetSensitive(getSensitiveFields, field, null, imports);
				field.getFieldType().appendSetSensitive(setSensitiveFields, field, null, imports);
			}
		}

		if(additionalClassCode != null)
			publishFields.append(additionalClassCode);
		params.put("ancestry", ancestry.toString());
		params.put("defineFields", defineFields.toString());
		params.put("writeFields", writeFields.toString());
		params.put("readFields", readFields.toString());
		params.put("toStringFields", toStringFields.toString());
		params.put("publishFields", publishFields.toString());
		if(supportGetSetSensitiveData) {
			params.put("getSensitiveFields", getSensitiveFields.toString());
			params.put("setSensitiveFields", setSensitiveFields.toString());
		}
		params.put("initFields", initFields.toString());
		StringBuilder importString = new StringBuilder();
		for(Class<?> clazz : imports) {
			if(shouldImport(clazz)) {
				String cn = clazz.getCanonicalName();
				if(cn != null)
					importString.append("import ").append(cn).append(";\n");
			}
		}
		params.put("imports", importString.toString());
		BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("MessageDataTemplate.txt")));
		Pattern replacePattern = Pattern.compile("\\$\\{([A-Za-z0-9_.\\(\\)\\[\\]]+)\\}");
		String line;
		while((line=reader.readLine()) != null) {
			Matcher matcher = replacePattern.matcher(line);
			int pos = 0;
	        while(matcher.find()) {
	        	writer.print(line.substring(pos, matcher.start()));
	        	writer.print(ReflectionUtils.getProperty(params, matcher.group(1)));
	            pos = matcher.end();
	        }
	        writer.println(line.substring(pos));
		}
		writer.flush();
	}

	/**
	 * @param fields2
	 * @return
	 */
	protected boolean hasDeviceTypeChoice(List<Field> fields) {
		for(Field field : fields) {
			if(field instanceof Choice) {
				if(field.getFieldType() == FieldType.DEVICE_TYPE_CHOICE)
					return true;
				Choice choice = (Choice)field;
				for(Case cs : choice.getCases()) {
					if(hasDeviceTypeChoice(cs.getFields()))
						return true;
				}
			} else if(field instanceof Multiple) {
				Multiple multiple = (Multiple)field;
				if(hasDeviceTypeChoice(multiple.getFields()))
					return true;
			} else if(field instanceof SubType) {
				SubType subtype = (SubType)field;
				if(hasDeviceTypeChoice(subtype.getFields()))
					return true;
			}

		}
		return false;
	}
	/**
	 * @param fields2
	 * @return
	 */
	protected StringBuilder appendInterfaces(List<Field> fields, StringBuilder appendTo, Set<Class<?>> imports) {
		for(Field field : fields) {
			if(field instanceof Choice) {
				Choice choice = (Choice)field;
				if(choice.getDeterminant().getFieldType() != FieldType.DIRECTION)
					choice.appendInterface(appendTo, imports);
				for(Case cs : choice.getCases()) {
					appendInterfaces(cs.getFields(), appendTo, imports);
				}
			} else if(field instanceof Multiple) {
				Multiple multiple = (Multiple)field;
				appendInterfaces(multiple.getFields(), appendTo, imports);
			} else if(field instanceof SubType) {
				SubType subtype = (SubType)field;
				appendInterfaces(subtype.getFields(), appendTo, imports);
			}
		}
		return appendTo;
	}

	protected static boolean shouldImport(Class<?> clazz) {
		if(clazz.getPackage() != null && !clazz.getPackage().getName().equals("java.lang")
				&& !clazz.getPackage().getName().equals(MessageData.class.getPackage().getName()))
			return true;
		return false;
	}
	protected void closeDialog() {
		getGeneratorPanel().getParent().setVisible(false);
	}

	protected void report(String message) {

	}
	/**
	 * This method initializes cancelButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton();
			cancelButton.setText("Cancel");
			cancelButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					log.debug("Closing program");
					closeDialog();
				}
			});
		}
		return cancelButton;
	}

	protected static MessageType chooseMessageType() {
		List<MessageTypeSelection> selectionValues = new ArrayList<MessageTypeSelection>();
		MessageTypeSelection initialSelectionValue = null;
		for(MessageType messageType : MessageType.values()) {
			MessageTypeSelection selection = new MessageTypeSelection(messageType);
			selectionValues.add(selection);
			if(initialSelectionValue == null) {
				MessageData md = MessageDataFactory.createMessageData(messageType);
				if(md == null)
					initialSelectionValue = selection;
			}
		}
		MessageTypeSelection selection = (MessageTypeSelection)JOptionPane.showInputDialog(null, "Select the Message Type", "Generate Message Data", JOptionPane.QUESTION_MESSAGE, null, selectionValues.toArray(), initialSelectionValue);
		if(selection != null)
			return selection.messageType;
		return null;
	}
}
